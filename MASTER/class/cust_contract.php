<?php
/**
 * To manage the CONTRACT object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class CNTRCT_CONTRACT extends CNTRCT {
    
	protected $object_id = 0;
	protected $object_details = array();
    
    protected $progress_status_field = "_status_id";
	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "";
	protected $name_field = "_name";
	protected $deadline_field = "_planned_date_completed";
	protected $owner_field = "_owner_id";
	protected $manager_field = "_manager";
	protected $authoriser_field = "_authoriser";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $progress_field = "_progress";
	
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "CONTRACT";
    const TABLE = "contract";
    const TABLE_FLD = "contract";
    const REFTAG = "C";
	const LOG_TABLE = "contract";
	/**
	 * STATUS CONSTANTS
	 */
	const CONFIRMED = 32;
	const ACTIVATED = 64;

	const FINANCE_INITIATED = 256;
	/**
	 * ASSESSMENT TYPE CONSTANTS
	 */
	const QUALITATIVE = 1024;
	const QUANTITATIVE = 2048;
	const OTHER = 4096;
    
    public function __construct($contract_id=0) {
        parent::__construct();
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->manager_field = self::TABLE_FLD.$this->manager_field;
		$this->authoriser_field = self::TABLE_FLD.$this->authoriser_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;
		if($contract_id>0) {
	    	$this->object_id = $contract_id;
			$this->object_details = $this->getRawObject($contract_id);  //$this->arrPrint($this->object_details);
		}
		$this->object_form_extra_js = "
		//variable to remember the previous progress value when defaulting to 100
		var old_progress = $('#".$this->getProgressFieldName()."').val();
		 
		//on change of the status drop down
		$('#".$this->getProgressStatusFieldName()."').change(function() {
		                //get the selected option value
		                var v = ($(this).val())*1;
		                //if it is set to \"Completed\" (always id=3 except deliverable which is 5)
		                if(v==3) {
		                                //if the progress field is not already set to 100, then remember the current value
		                                if(($('#".$this->getProgressFieldName()."').val())*1!=100) {
		                                                old_progress = $('#".$this->getProgressFieldName()."').val();
		                                }
		                                //update progress field to 100 and disable
		                                $('#".$this->getProgressFieldName()."').val('100').prop('disabled',true);
		                //else if the status field is not Completed, check if the progress field was previously disabled
		                } else if($('#".$this->getProgressFieldName()."').prop('disabled')==true) {
		                                //cancel the disable and reset the value back to the last remembered value
		                                $('#".$this->getProgressFieldName()."').val(old_progress).prop('disabled',false);
		                }
		});";
    }
    
	/*******************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		unset($var['attachments']);
		unset($var['contract_id']);	//remove incorrect contract_id from generic form
		unset($var['object_id']);	//remove incorrect contract_id from generic form
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		$exclude = $var['exclude'];
		foreach($var as $key=>$val){
			if(strpos($key, "excluder") !== false){
				unset($var[$key]);
			}
		}
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		unset($var['recipients']);
		unset($var['includer']);
		unset($var['exclude']);
		/*/ New stuff for SMS notifications */
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_progress'] = 0;
		$var[$this->getTableField().'_status_id'] = 1;
		$var[$this->getTableField().'_status'] = CNTRCT::ACTIVE;
		
		$last_deliverable_status = $var['last_deliverable_status'];
		unset($var['last_deliverable_status']);
		$var['contract_assess_status'] = 0;
		for($ds = 1;$ds<=$last_deliverable_status;$ds++) {
			if(isset($var['contract_assess_status-'.$ds])) {
				if(($var['contract_assess_status-'.$ds])==1) {
					$var['contract_assess_status'] += pow(2,$ds);
				}
				unset($var['contract_assess_status-'.$ds]);
			}
		}
		$var['contract_assess_type'] = 0;
		if(isset($var['contract_assess_qual'])) {
			if(($var['contract_assess_qual'])==1) {
				$var['contract_assess_type'] += self::QUALITATIVE;
			}
			unset($var['contract_assess_qual']);
		}
		if(isset($var['contract_assess_quan'])) {
			if(($var['contract_assess_quan'])==1) {
				$var['contract_assess_type'] += self::QUANTITATIVE;
			}
			unset($var['contract_assess_quan']);
		}
		if(isset($var['contract_assess_other'])) {
			if(($var['contract_assess_other'])==1) {
				$var['contract_assess_type'] += self::OTHER;
			}
			unset($var['contract_assess_other']);
		}
		/* REMOVED DUE TO SHIFT OF ALL FINANCES TO FINANCE SECTION - JC @ 19 Nov 2014
		$supplier_id = $var['cs_supplier_id'];
		$project_value = $var['cs_project_value'];
		unset($var['contract_supplier_id']);
		unset($var['cs_supplier_id']);
		unset($var['cs_project_value']);*/
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			/* REMOVED DUE TO SHIFT OF ALL FINANCES TO FINANCE SECTION - JC @ 19 Nov 2014
			//process supplier details here
			$csObject = new CNTRCT_CONTRACT_SUPPLIER();
			foreach($supplier_id as $key => $supp) {
				if($supp>0) {
					$value = $project_value[$key];
					$csObject->addObject($id,$supp,$value);
				}
			}
			unset($csObject);*/
			$changes = array(
				'response'=>"|contract| created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::CREATE,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
			//$this->addActivityLog("contract", $log_var);
			$log_var['recipients']=$recipients;
			$log_var['exclude']=$exclude;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			unset($log_var['recipients']);
			unset($log_var['exclude']);
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$result = array(
				0=>"ok",
				1=>"New ".$this->getContractObjectName()." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}

	public function confirmObject($var) {
		$contract_id = $var['contract_id'];
		/* New stuff for SMS notifications /*/
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		$sms = $var['sms']==1?"BOTH":"MAIL";
		unset($var['sms']);
		/*/ New stuff for SMS notifications */
		$sql = "SELECT ".$this->getStatusFieldName()." as status, contract_authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$old = $this->mysql_fetch_one($sql);
		$old_status = $old['status'];
		$new_status = $old_status + self::CONFIRMED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			//LOG!!!
			$changes = array(
				'response'=>"|contract| confirmed.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $contract_id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::CONFIRM,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			unset($log_var['recipients']);
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$this->addActivityLog("contract", $log_var);
			//EMAIL TO NOTIFY AUTHORISER
			//$contract_authoriser = $old['contract_authoriers'];
			return array("ok","".$this->getContractObjectName()." ".self::REFTAG.$contract_id." confirmed successfully.  It has gone to UUUUU for activation.");
		} else {
			return array("error","An error occurred while trying to confirm ".$this->getContractObjectName()." ".self::REFTAG.$contract_id.". Please reload the page and try again.");
		}
	}
	
	
	public function activateObject($var) {
		$contract_id = $var['contract_id'];
		/* New stuff for SMS notifications /*/
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		$sms = $var['sms']==1?"BOTH":"MAIL";
		unset($var['sms']);
		/*/ New stuff for SMS notifications */
		$sql = "SELECT ".$this->getStatusFieldName()." as status FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$old = $this->mysql_fetch_one($sql);
		$old_status = $old['status'];
		$new_status = $old_status + self::ACTIVATED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			//LOG!!!
			$changes = array(
				'response'=>"|contract| activated.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $contract_id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::ACTIVATE,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			unset($log_var['recipients']);
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$this->addActivityLog("contract", $log_var);
			//return array("info",$note);
			return array("ok","".$this->getContractObjectName()." ".self::REFTAG.$contract_id." activated successfully. It is now available in Manage and Admin.");
		} else {
			return array("error","An error occurred while trying to activate ".$this->getContractObjectName()." ".self::REFTAG.$contract_id.". Please reload the page and try again.");
		}
	}
		
	public function deactivateObject($var) {
		$contract_id = $var['contract_id'];
		/* New stuff for SMS notifications /*/
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		$sms = $var['sms']==1?"BOTH":"MAIL";
		unset($var['sms']);
		/*/ New stuff for SMS notifications */
		$sql = "SELECT ".$this->getStatusFieldName()." as status FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$old = $this->mysql_fetch_one($sql);
		$old_status = $old['status'];
		$new_status = $old_status - self::ACTIVATED - self::CONFIRMED;
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$contract_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			//LOG!!!
			$changes = array(
				'response'=>"|contract| activation and confirmation reversed.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $contract_id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::DEACTIVATE,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			unset($log_var['recipients']);
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$this->addActivityLog("contract", $log_var);
			return array("ok","Activation and Confirmation of ".$this->getContractObjectName()." ".self::REFTAG.$contract_id." successfully reversed. It is no longer available in Manage and Admin.");
		} else {
			return array("error","An error occurred while trying to deactivate ".$this->getContractObjectName()." ".self::REFTAG.$contract_id.". Please reload the page and try again.");
		}
	}
	
	
	public function updateObject($var) {
		$object_id = $var['object_id'];
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$dc_field = $this->getTableField()."_date_completed";
		$progress = $var[$p_field];
		$status_id = $var[$si_field];
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$update_data = array(
			$p_field=>$progress,
			$si_field=>$status_id,
		);
		if($status_id == 3 && $progress == 100 && (strtotime($var['action_on']) != strtotime($old_data[$dc_field]))) {
			$update_data[$dc_field] = date("Y-m-d",strtotime($var['action_on']));
			$c[$dc_field] = array('to'=>date("Y-m-d",strtotime($var['action_on'])),'from'=>$old_data[$dc_field]);
		}
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		
		if($mar>0) {
			$changes = array(
				'response'=>$var['response'],
				'user'=>$this->getUserName(),
				'action_on'=>$var['action_on'],
			);
			if($old_data[$p_field] != $progress) {
				$changes[$p_field] = array('to'=>$progress,'from'=>$old_data[$p_field]);
			}
			if($old_data[$si_field] != $status_id) {
				$listObject = new CNTRCT_LIST(substr($si_field,0,-3));
				$items = $listObject->getAListItemName(array($status_id,$old_data[$si_field]));
				$changes[$si_field] = array('to'=>$items[$status_id],'from'=>$items[$old_data[$si_field]]);
			}
			if(isset($c[$dc_field])) {
				$changes[$dc_field] = $c[$dc_field];
			}
			$x = array();
			foreach($attach as $a) {
				$x[] = "|attachment| ".$a['original_filename']." has been added.";
			}
			$changes[$this->getUpdateAttachmentFieldName()] = $x;
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> CNTRCT_LOG::UPDATE,
				'progress'	=> $progress,
				'status_id'	=> $status_id,		
			);
			$note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$this->addActivityLog(self::LOG_TABLE, $log_var);
	//		ASSIST_HELPER::arrPrint($note);
	//		return array("info",$note);
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." has been updated successfully.");
		}
		
		return array("error","An error occurred.  Please try again.");
		
		
	}
	
	public function editObject($var,$attach=array()) {
		unset($var['contract_id']);	//remove incorrect contract_id from generic form
		$object_id = $var['object_id'];
		
		if($this->checkIntRef($object_id)) {
			$old_object = $this->getRawObject($object_id);
			
			$last_deliverable_status = $var['last_deliverable_status'];
			unset($var['last_deliverable_status']);
			$var['contract_assess_status'] = 0;
			for($ds = 1;$ds<=$last_deliverable_status;$ds++) {
				if(isset($var['contract_assess_status-'.$ds])) {
					if(($var['contract_assess_status-'.$ds])==1) {
						$var['contract_assess_status'] += pow(2,$ds);
					}
					unset($var['contract_assess_status-'.$ds]);
				}
			}
			$var['contract_assess_type'] = 0;
			if(isset($var['contract_assess_qual'])) {
				if(($var['contract_assess_qual'])==1) {
					$var['contract_assess_type'] += self::QUALITATIVE;
				}
				unset($var['contract_assess_qual']);
			}
			if(isset($var['contract_assess_quan'])) {
				if(($var['contract_assess_quan'])==1) {
					$var['contract_assess_type'] += self::QUANTITATIVE;
				}
				unset($var['contract_assess_quan']);
			}
			if(isset($var['contract_assess_other'])) {
				if(($var['contract_assess_other'])==1) {
					$var['contract_assess_type'] += self::OTHER;
				}
				unset($var['contract_assess_other']);
			}
			/* REMOVED DUE TO SHIFT OF ALL FINANCES TO FINANCE SECTION - JC @ 19 Nov 2014
			$cs_id = $var['contract_supplier_id'];
			$supplier_id = $var['cs_supplier_id'];
			$project_value = $var['cs_project_value'];
			unset($var['contract_supplier_id']);
			unset($var['cs_supplier_id']);
			unset($var['cs_project_value']);
			 */
			//$result = array("info",serialize($var));
			$result = $this->editMyObject($var,$attach);
			/* REMOVED DUE TO SHIFT OF ALL FINANCES TO FINANCE SECTION - JC @ 19 Nov 2014
			//process supplier details here
			$listObject = new CNTRCT_LIST("contract_supplier");
			$cs_list_items = $listObject->getActiveListItemsFormattedForSelect();
			unset($listObject);
			$csObject = new CNTRCT_CONTRACT_SUPPLIER();
			$cs_changes = array();
			$cs_mar = 0;
			//return array("error","check console",$cs_id,$supplier_id,$project_value);
			foreach($cs_id as $key => $i) {
				$supp = $supplier_id[$key];
				$value = $project_value[$key];
				if($this->checkIntRef($i)) {
					$cs_old = $old_object['contract_supplier'][$i];
					if(!$this->checkIntRef($supp)) {
						$cs_mar=$csObject->deactivateObject($i,$cs_old);
						if($cs_mar>0) {
							$cs_changes[$i]['log_display_comment'] = "Removed |contract_supplier| ".($key+1)." (System Ref: $i): ".$cs_list_items[$cs_old['cs_supplier_id']]." with |cs_project_value| ".ASSIST_HELPER::format_currency($cs_old['cs_project_value']);
						}
					} else {
						$cs_mar=$csObject->editObject($i,$cs_old,$supp,$value);
						if($cs_mar>0) {
							$cs_changes[$i] = array(
								'log_display_comment'=>"Edited |contract_supplier| ".($key+1)." (System Ref: $i)",
							);
							if($supp!=$cs_old['cs_supplier_id']) {
								$cs_changes[$i]['cs_supplier_id'] = array('to'=>$cs_list_items[$supp],'from'=>$cs_list_items[$cs_old['cs_supplier_id']]);
							}
							if($value!=$cs_old['cs_project_value']) {
								$cs_changes[$i]['cs_project_value'] = array('to'=>$value,'from'=>$cs_old['cs_project_value']);
							}
						}
					}
				} else {
					if($this->checkIntRef($supp)) {
						$csi = $csObject->addObject($object_id,$supp,$value);
						if($this->checkIntRef($csi)) {
							$cs_changes[$csi]['log_display_comment'] = "Added new |contract_supplier| ".($key+1)." (System Ref: $csi): ".$cs_list_items[$supp]." with |cs_project_value| ".ASSIST_HELPER::format_currency($value);
						}
					}
				}
			}
//return array("error",$object_id." :c: ".count($cs_changes));
			unset($csObject);
			if(count($cs_changes)>0) {
				$root_changes = array(
					'user'=>$this->getUserName(),
				);
				$log_var = array(
					'object_id'	=> $object_id,
					'changes'	=> array(),
					'log_type'	=> CNTRCT_LOG::EDIT,
					'progress'	=> $old_object[$this->getTableField()."_progress"],
					'status_id'	=> $old_object[$this->getProgressStatusFieldName()],		
				);
			 //return array("error",$cs_changes);
				foreach($cs_changes as $c) {
					if(count($c)>0) {
						$changes = array_merge($root_changes,$c);
						$log_var['changes'] = $changes;
						$this->addActivityLog("contract", $log_var);
					}
				}
			} */
			 //if($result[0]=="info" && count($cs_changes)) {
			 //	return array("ok","Changes saved successfully.");	
			 //} else {
			 	return $result;
			 //}
		}
		return array("error","An error occurred.  Please try again.");
			 
		//return $result;
	}	
	
	

	
    /*************************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRootTableName() { return $this->getDBRef(); }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
	public function getManagerFieldName() {
		return $this->manager_field;
	}

	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "")."contract_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}
    
	public function checkIfIsAContractManager() {
		$sql = "SELECT count(contract_id) as count FROM ".$this->getTableName()." WHERE contract_manager = '".$this->getUserID()."' AND ".$this->getActiveSQLScript("");
		$row = $this->mysql_fetch_one($sql);
		return ($row['count']>0);
	}
    public function checkIfIsAContractAuthoriser() {
		$sql = "SELECT count(contract_id) as count FROM ".$this->getTableName()." WHERE contract_authoriser = '".$this->getUserID()."' AND ".$this->getActiveSQLScript("");
		$row = $this->mysql_fetch_one($sql);
		return ($row['count']>0);
    }
    
	public function getList($section,$options=array()) {
		return $this->getMyList("CONTRACT", $section,$options); 
	}
	
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject("CONTRACT", $id,$options);
	}
	
	
	
	
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE contract_id = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		
		//moved to finance section
		//$csObject = new CNTRCT_CONTRACT_SUPPLIER();
		//$data['contract_supplier'] = $csObject->getRawObjectByContractID($obj_id);

		return $data;
	}
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		$data = array(
			$this->getUpdateAttachmentFieldName()=>$raw[$this->getUpdateAttachmentFieldName()],
			$this->getProgressFieldName()=>$raw[$this->getProgressFieldName()],
			$this->getProgressStatusFieldName()=>$raw[$this->getProgressStatusFieldName()],
		);
		return $data;
	}	
	
	
	
	
	
	
	
	public function getSummary($contract_id=0) {
		if($contract_id==0) { $contract_id = $this->object_id; }
		$delObject = new CNTRCT_DELIVERABLE();
			$del_id = $delObject->getIDFieldName();
			$del_status = $delObject->getStatusFieldName();
			$del_parent = $delObject->getParentFieldName();
		$actObject = new CNTRCT_ACTION();
			$act_id = $actObject->getIDFieldName();
			$act_status = $actObject->getStatusFieldName();
			$act_parent = $actObject->getParentFieldName();
		$sql = "SELECT D.".$delObject->getTableField()."_type as type, count(D.".$del_id.") as dels
				, AVG( D.del_progress ) as del_prog
				FROM ".$delObject->getTableName()." D 
				WHERE D.".$del_status." & ".CNTRCT::DELETED." <> ".CNTRCT::DELETED." AND D.".$del_parent." = ".$contract_id." 
				GROUP BY D.".$delObject->getTableField()."_type"; 
				//echo $sql;
		$results = $this->mysql_fetch_all_by_id($sql, "type");  //$this->arrPrint($results);
		$sql = "SELECT D.".$delObject->getTableField()."_type as type, count(A.".$act_id.") as actions
				, AVG( A.action_progress ) as action_prog
				FROM ".$delObject->getTableName()." D 
				LEFT OUTER JOIN ".$actObject->getTableName()." A ON A.".$act_parent." = D.".$del_id." AND A.".$act_status." & ".CNTRCT::DELETED." <> ".CNTRCT::DELETED." 
				WHERE D.".$del_status." & ".CNTRCT::DELETED." <> ".CNTRCT::DELETED." AND D.".$del_parent." = ".$contract_id." 
				GROUP BY D.".$delObject->getTableField()."_type"; 
				//echo $sql;
		$a_results = $this->mysql_fetch_all_by_id($sql, "type");  //$this->arrPrint($results);
				
		$types = $delObject->getAllDeliverableTypes();
		$res = array();	
		foreach($types as $t => $x){
			$d = isset($results[$t]) ? $results[$t]['dels'] : 0; 
			$a = isset($a_results[$t]) ? $a_results[$t]['actions'] : 0; 
			$ap = isset($a_results[$t]) && !is_null($a_results[$t]['action_prog']) ? $a_results[$t]['action_prog'] : 0;
			$dp = isset($results[$t]) && !is_null($results[$t]['del_prog']) ? $results[$t]['del_prog'] : 0;
			$res[$t] = array('deliverable'=>$d,'action'=>$a,'action_prog'=>$ap,'del_prog'=>$dp);
		}
		return $res;	
	}
    
    /**
	 * Function to fetch the recipients of the notifications which are about to go out
	 */
	public function getObjectRecipients($data){
		$id = $data['id'];
		$mailObj = new ASSIST_MODULE_EMAIL("Contract","C");
		$smsObj = new ASSIST_SMS();
		if($id==0){
			$x = json_decode(stripslashes($data['extra']));
		//return $data;
			if(count($x)>0){
				//return $x;
				$usrxs = new CNTRCT_USERACCESS();
				foreach($x as $key=>$val){
						//If they have create deliverable or action access
						if($val !== "X"){
							//$mini_rcp[$key] = "here's my error $key :: $val ";
						switch (strtolower($key)) {
							case 'manager':
								if($val != $this->getUserID() && $usrxs->canICreateDeliverables($val)){
									$manager = ucwords($this->getObjectName(self::OBJECT_TYPE))." Manager";
									$adds = $mailObj->getEmailAddresses(array($val), array());
									$phone = $smsObj->getMobilePhones(array($val));
									//$phone[$val]['num'] = "MAN: here's the problem :: ".$val;
									$adds['to']['mobile'] = $phone[$val]['num'];
									$adds['to']['tkid'] = $val;
									foreach($adds['to'][0] as $a=>$b){
										$adds['to'][$a] = $b;
									}
									unset($adds['to'][0]);
									$mini_rcp[$manager] = $adds['to']; 
								}
								break;
							case 'authoriser':
								if($val != $this->getUserID() && $usrxs->canICreateDeliverables($val)){
									$authoriser = ucwords($this->getObjectName(self::OBJECT_TYPE))." Authoriser";
									$adds = $mailObj->getEmailAddresses(array($val), array());
									$phone = $smsObj->getMobilePhones(array($val));
									//$phone[$val]['num'] = "AUTH: here's the problem :: ".$val;
									$adds['to']['mobile'] = $phone[$val]['num'];
									$adds['to']['tkid'] = $val;
									foreach($adds['to'][0] as $a=>$b){
										$adds['to'][$a] = $b;
									}
									unset($adds['to'][0]);
									$mini_rcp[$authoriser] = $adds['to']; 
								}
								break;
							case 'owner':
								$ownerObj = new CNTRCT_CONTRACT_OWNER();
								$owners = $ownerObj->getActiveAdminsForOwner($val);
								$own_fld = ucwords($this->getObjectName(self::OBJECT_TYPE))." Owner";
								
								foreach($owners as $index=>$item){
									if($item != $this->getUserID() && $usrxs->canICreateDeliverables($item)){
										$adds = $mailObj->getEmailAddresses(array($index), array());
										$phone = $smsObj->getMobilePhones(array($index));
										//$phone[$val]['num'] = "OWN: here's the problem :: ".$index;
										$adds['to']['mobile'] = $phone[$index]['num'];
										$adds['to']['tkid'] = $index;
										foreach($adds['to'][0] as $a=>$b){
											$adds['to'][$a] = $b;
										}
										unset($adds['to'][0]);
										$mini_rcp[$own_fld][] = $adds['to']; 
									}
								}
								break;
						}
					}
				}	
				return $mini_rcp;
			}
		} else {
			$action = $data['activity'];
			$extra = strlen($data['extra'])>0 ? $data['extra'] : "";
			$recipients = $this->getRecipients($id, $action,true);
			$owner = ucwords($this->getObjectName(self::OBJECT_TYPE))." Owner";
			if(strlen($extra)>0 && $extra !== $recipients[$owner] && $extra !== $this->getUserID()){
				$recipients["Old ".$owner]=$recipients[$owner];
				$recipients["New ".$owner]=$extra;
				unset($recipients[$owner]);
			}
			//return $recipients;
			foreach($recipients as $key=>$val){
				if(is_array($val)){
					$val = array_unique($val);
					foreach($val as $index=>$thing){
						if($thing != $this->getUserID()){
							$adds = $mailObj->getEmailAddresses(array($thing), array());
							$phone = $smsObj->getMobilePhones(array($thing));
							$adds['to']['mobile'] = $phone[$thing]['num'];
							$adds['to']['tkid'] = $thing;
							foreach($adds['to'][0] as $a=>$b){
								$adds['to'][$a] = $b;
							}
							unset($adds['to'][0]);
							$recipes[$key][] = $adds['to'];
						}
					}
				}else{
					if($val != $this->getUserID()){
						$adds = $mailObj->getEmailAddresses(array($val), array());
						$phone = $smsObj->getMobilePhones(array($val));
						$adds['to']['mobile'] = $phone[$val]['num'];
						$adds['to']['tkid'] = $val;
						foreach($adds['to'][0] as $a=>$b){
							$adds['to'][$a] = $b;
						}
						unset($adds['to'][0]);
						$recipes[$key] = $adds['to'];
					}
				}
			}
			//$recipes = array_unique($recipes);
			return $recipes;
		}
	}
     
	public function getRecipients($id,$action){
		switch (strtoupper($action)) {
			case 'SPECIAL':	
				$sql = "SELECT ".$this->getOwnerFieldName()." as dept_id, ".$this->getManagerFieldName()." as manager , ".$this->getAuthoriserFieldName()." as authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = $id ";
				$res_obj = $this->mysql_fetch_all($sql);
				$res = $res_obj[0];
				$ownerObj = new CNTRCT_CONTRACT_OWNER();
				$owners = $ownerObj->getActiveAdminsForOwner($res['dept_id']);
				$manager = $res['manager'];
				$authoriser = $res['authoriser'];
				$tkid['manager']=$manager;
				$tkid['authoriser']=$authoriser;
				$tkid['owner']=$owners;
				break;
			case 'UPDATE':
			case 'EDIT':
				$sql = "SELECT ".$this->getOwnerFieldName()." as dept_id, ".$this->getManagerFieldName()." as manager , ".$this->getAuthoriserFieldName()." as authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = $id ";
				$res_obj = $this->mysql_fetch_all($sql);
				$res = $res_obj[0];
				$ownerObj = new CNTRCT_CONTRACT_OWNER();
				$owners = $ownerObj->getActiveAdminsForOwner($res['dept_id']);
				$manager = $res['manager'];
				$authoriser = $res['authoriser'];
				foreach($owners as $key=>$val){
					$tkid[]=$key;
				}
				$tkid[]=$manager;
				$tkid[]=$authoriser;
				break;
			case 'NEW':
				$sql = "SELECT ".$this->getOwnerFieldName()." as dept_id, ".$this->getManagerFieldName()." as manager , ".$this->getAuthoriserFieldName()." as authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = $id ";
				$res_obj = $this->mysql_fetch_all($sql);
				$res = $res_obj[0];
				$ownerObj = new CNTRCT_CONTRACT_OWNER();
				$owners = $ownerObj->getActiveAdminsForOwner($res['dept_id']);
				$usrxs = new CNTRCT_USERACCESS();
				//If they have create deliverable or action access
				$manager = $usrxs->canICreateDeliverables($res['manager'])?$res['manager']:"";
				if($res['authoriser'] != $res['manager']){
					$authoriser=$usrxs->canICreateDeliverables($res['authoriser'])?$res['authoriser']:"";
				}else{
					$authoriser="";
				};
				foreach($owners as $key=>$val){
					if($val['can_create_deliverable'] == true){
						$tkid[]=$key;
					}
				}
				$tkid[]=$manager;
				$tkid[]=$authoriser;
				break;
			case 'ACTIVATE':
			case 'DEACTIVATE':
				$sql = "SELECT ".$this->getOwnerFieldName()." as dept_id, ".$this->getManagerFieldName()." as manager , ".$this->getAuthoriserFieldName()." as authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = $id ";
				$res_obj = $this->mysql_fetch_all($sql);
				$res = $res_obj[0];
				$ownerObj = new CNTRCT_CONTRACT_OWNER();
				$owners = $ownerObj->getActiveAdminsForOwner($res['dept_id']);
				$usrxs = new CNTRCT_USERACCESS();
				//If they have create deliverable or action access
				$manager = $res['manager'];
				$authoriser = $res['authoriser'];
				foreach($owners as $key=>$val){
					$tkid['Owner']=$key;
				}
				$tkid['Manager']=$manager;
				$tkid['Authoriser']=$authoriser;
				//Fetch all of the Action & Deliverable owners
				$sql = "SELECT del_id as id, del_owner as tkid FROM ".$this->getRootTableName()."_deliverable WHERE del_contract_id = $id";
				$dels = $this->mysql_fetch_all($sql);
				//echo json_encode($dels);
				if(count($dels)>0){
					foreach($dels as $index=>$thing){
						$del_ids[]=$thing['id'];
						$del_owns[]=$thing['tkid'];
					}
					$sql = "SELECT action_owner as tkid FROM ".$this->getRootTableName()."_action WHERE action_deliverable_id IN (".implode(",",$del_ids).")";
					$acts = $this->mysql_fetch_all($sql);
					$del_owner_fld = $this->getObjectName("Deliverable")." Owner";
					$tkid[$del_owner_fld]=$del_owns;
				}
				if(count($acts)>0){
					foreach($acts as $index=>$thing){
						$act_owns[]=$thing['tkid'];
					}
					$act_owner_fld = $this->getObjectName("Action")." Owner";
					$tkid[$act_owner_fld]=$act_owns;
				}
				break;
			case 'CONFIRM':
				$sql = "SELECT ".$this->getAuthoriserFieldName()." as authoriser FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = $id ";
				$res_obj = $this->mysql_fetch_all($sql);
				$res = $res_obj[0];
				$authoriser = $res['authoriser'];
				$tkid['Authoriser']=$authoriser;
				break;
		}	
		return $tkid;
	}
	 
     
	/***************************
	 * STATUS SQL script generations
	 */
	/**
	 * Returns status check for Contracts which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on New > Activation page
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status = confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATION",$t,false);
	}
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
     
	 
	 
	/****
	 * functions to check on the status of a contract
	 */ 
	public function getDeadlineDate() {
		return $this->object_details['contract_date_completed'];
	}
	public function hasDeadline() { return true; }
	public function getDeadlineField() { return $this->deadline_field; }
	public function doIHaveSubDeliverables() {
		//echo ($this->object_details['contract_have_subdeliverables']*1)." => ".($this->object_details['contract_have_subdeliverables']*1==1);
		//$this->arrPrint($this->object_details);
		if($this->object_details['contract_have_subdeliverables']*1==1){
			return true;
		}
		return false;
	} 
	public function doIHaveDeliverables() {
		//if($this->object_details['contract_have_deliverables']*1==1){
			return true;
		//}
		//return false;
	} 
	public function mustIDoAssessment($x=-1) {
		$x*=1;
		if($x<0) { $x = $this->object_details['contract_do_assessment']*1; }
		if($x==1){
			return true;
		}
		return false;
	} 
	public function mustIAssignWeights($x=-1) {
		$x*=1;
		if($x<0) { $x = $this->object_details['contract_weights']*1; }
		if($x==1){
			return true;
		}
		return false;
	}
	public function mustIAssessQuality($x=-1) {
		$x*=1;
		if($x<0) { $x = $this->object_details['contract_assess_type']*1; }
		if( ($x & self::QUALITATIVE) == self::QUALITATIVE){
			return true;
		}
		return false;
	} 
	public function mustIAssessQuantity($x=-1) {
		$x*=1;
		if($x<0) { $x = $this->object_details['contract_assess_type']*1; }
		if( ($x & self::QUANTITATIVE) == self::QUANTITATIVE){
			return true;
		}
		return false;
	} 
	public function mustIAssessOther($x=-1) {
		$x*=1;
		if($x<0) { $x = $this->object_details['contract_assess_type']*1; }
		if( ($x & self::OTHER) == self::OTHER){
			return true;
		}
		return false;
	} 
	public function canIConfirm($contract_id=0) {
		$delObject = new CNTRCT_DELIVERABLE();
		$actObject = new CNTRCT_ACTION();
		$sql = "SELECT D.".$delObject->getIDFieldName().", count(A.".$actObject->getIDFieldName().") as actions 
				FROM ".$delObject->getTableName()." D 
				LEFT OUTER JOIN ".$actObject->getTableName()." A
				ON A.".$actObject->getParentFieldName()." = D.".$delObject->getIDFieldName()." AND A.".$actObject->getStatusFieldName()." & ".CNTRCT::DELETED." <> ".CNTRCT::DELETED."
				WHERE D.".$delObject->getParentFieldName()." = $contract_id
				AND D.".$delObject->getStatusFieldName()." & ".CNTRCT::DELETED." <> ".CNTRCT::DELETED."
				ORDER BY count(A.".$actObject->getIDFieldName().")";
		$results = $this->mysql_fetch_all($sql);
		if(count($results)==0) {
			return false;
		} else {
			foreach($results as $d) {
				if($d['actions']==0) {
					return false;
				}
			}
		}
		return true;
	} 
	 
	 
	 
	 
	 
     
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /*************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /***********************
     * PRIVATE functions: functions only for use within the class
     */
    
}


?>