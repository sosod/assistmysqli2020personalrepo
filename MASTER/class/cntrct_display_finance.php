<?php
/**
 * To manage the display of forms / tables and to provide a link to the Assist_module_display class
 * 
 * Created on: 1 December 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
class CNTRCT_DISPLAY_FINANCE extends CNTRCT_DISPLAY {

	private $form_random_id = 0;
	private $form_random_id_used = array(0);
	
	public function __construct() {
		parent::__construct();
	}







	/**
	 * Function generate the object form
	 * @param $form_object_type
	 * @param $formObject
	 * @param $form_object_id
	 * @param $parent_object_type
	 * @param $parentObject
	 * @param $parent_object_id
	 * @param $page_action
	 * @param $page_redirect_path 
	 */
	public function getFinanceObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path) {
		
		$ot = explode("_",$form_object_type);
		unset($ot[0]);
		$object_type = implode("_",$ot);
		
		$headingObject = new CNTRCT_HEADINGS();
		
		$attachment_form = false;
		
		$data = array('display'=>"",'js'=>"");
		$js = $formObject->getExtraObjectFormJS();
		$echo = "";
		if($page_action=="UPDATE") {
			$headings = $headingObject->replaceObjectNames($headingObject->getUpdateObjectHeadings($form_object_type,"FORM"));
		} else {
			if(stripos($form_object_type,"ASSURANCE")===false) {
				$head_object_type = $form_object_type;
			} else {
				$head_object_type = "ASSURANCE";
			}
			$fld_prefix = $formObject->getTableField()."_";
			$headings = $headingObject->replaceObjectNames($headingObject->getMainObjectHeadings($head_object_type,"FORM","",$fld_prefix));
		}
		//ASSIST_HELPER::arrPrint($headings);
		//ASSIST_HELPER::arrPrint($headingObject->getHeadingsForLog());
		
		$pa = ucwords($form_object_type).".".$page_action;
		$pd = $page_redirect_path;
		
		$is_edit_page = (strrpos(strtoupper($page_action),"EDIT")!==FALSE);
		$is_update_page = (strrpos(strtoupper($page_action),"UPDATE")!==FALSE);
		

		if($is_edit_page) {
			$form_object = $formObject->getRawObject($form_object_id);
			$form_activity = "EDIT";
		} elseif($is_update_page) {
			$form_object = $formObject->getRawUpdateObject($form_object_id);
			$form_activity = "UPDATE";
		} else {
			$form_object = array();
			$form_activity = "NEW";
		}
		//ASSIST_HELPER::arrPrint($form_object);

		$js.="
				var page_action = '".$pa."';
				var page_direct = '".$pd."';
				
		".$this->getAttachmentDownloadJS($form_object_type, $form_object_id,$form_activity);
		
		while(!in_array($this->form_random_id,$this->form_random_id_used)) {
			$this->form_random_id = rand(100, 999999999999999);
		}
		$echo.="
			<div id=div_error class=div_frm_error>
				
			</div>
			<form name=frm_finance_object_".$this->form_random_id." method=post language=jscript enctype=\"multipart/form-data\">
				<input type=hidden name=object_id value=".$form_object_id." />
				<input type=hidden name=contract_id value=".$parent_object_id." />
				<table class=form width=100% id=tbl_object_form>";
			$form_valid8 = true;
			$form_error = array();
			foreach($headings['rows'] as $fld => $head) { //echo "<p>".$fld;
				if($head['parent_id']==0) {
					$val = "";
					$h_type = $head['type']; //echo $h_type;
					if($h_type!="HEADING") {
						$display_me = true;
						$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
						if($head['required']==1) { $options['title'] = "This is a required field."; }
						if($h_type=="CALC") {
							$options['extra'] = $head['list_table'];
							$options['form_name'] = "frm_finance_object_".$this->form_random_id;
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : 0;
							} else {
								$val = 0;
							}
						} elseif($h_type=="LIST") {
							if($head['section']=="DELIVERABLE_ASSESS") {
								if($parentObject->mustIDoAssessment()===FALSE || $parentObject->mustIAssignWeights()===FALSE) {
									$display_me = false;
								} else {
									//validate the assessment status field here
									$lt = explode("_",$head['list_table']);
									switch($lt[1]) {
										case "quality": $display_me = $parentObject->mustIAssessQuality(); break;
										case "quantity": $display_me = $parentObject->mustIAssessQuantity(); break;
										case "other": $display_me = $parentObject->mustIAssessOther(); break;
									}
								}
							}
							if($display_me) {
								$list_items = array();
								$listObject = new CNTRCT_LIST($head['list_table']); //echo " ".$head['list_table'];
								$list_items = $listObject->getActiveListItemsFormattedForSelect();
								if(isset($list_items['list_num'])) {
									$list_parent_association = $list_items['list_num'];
									$list_items = $list_items['options'];
									$options['list_num'] = $list_parent_association;
								}
								$options['options'] = $list_items;
								if(count($list_items)==0 && $head['required']==1) {
									$form_valid8 = false;
									$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}
								if($is_edit_page || $is_update_page) {
									$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
								} else {
									$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
								}
								unset($listObject);
							}
						} elseif(in_array($h_type,array("MASTER","USER","OWNER","TEMPLATE","DEL_TYPE","DELIVERABLE"))) {
							$list_items = array();
							switch($h_type) {
								case "USER":
									$listObject = new CNTRCT_USERACCESS();
									$list_items = $listObject->getActiveUsersFormattedForSelect();
									break; 
								case "TEMPLATE":
									$listObject = new CNTRCT_TEMPLATE();
									$list_items = $listObject->getActiveTemplatesFormattedForSelect();
									break;
								case "OWNER":
									$listObject = new CNTRCT_CONTRACT_OWNER();
									$list_items = $listObject->getActiveOwnersFormattedForSelect();
									break;
								case "MASTER":
									$listObject = new CNTRCT_MASTER($head['list_table']);
									$list_items = $listObject->getActiveItemsFormattedForSelect();
									break; 
								case "DELIVERABLE":
									$listObject = new CNTRCT_DELIVERABLE();
									$list_items = $listObject->getDeliverablesForParentSelect($parent_object_id);
									break; 
								case "DEL_TYPE":
									$listObject = new CNTRCT_DELIVERABLE();
									$list_items = $listObject->getDeliverableTypes($parent_object_id);
									break; 
								default:
									echo $h_type; 
									break;
							}
							$options['options'] = $list_items;
							$h_type = "LIST";
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}
						} elseif($h_type=="DATE") {
							$options['options'] = array();
							$options['class'] = "";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 && strtotime($form_object[$fld])>0 ? date("d-M-Y",strtotime($form_object[$fld])) : "";
								if($is_update_page) {
									$options['options']['maxDate'] = "'+0D'";
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
							$options['extra'] = "processCurrency";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="BOOL"){
							$h_type = "BOOL_BUTTON";
							$options['yes'] = 1;
							$options['no'] = 0;
							$options['extra'] = "boolButtonClickExtra";
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "0";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
							}
						} elseif($h_type=="REF") {
							if(strtoupper($page_action)=="ADD") {
								$val = "System Generated";
							} else {
								$val = $formObject->getRefTag().$form_object_id;
							}
						} elseif($h_type=="ATTACH") {
							$attachment_form = true;
							$options['action'] = $pa;
							$options['page_direct'] = $pd;
							$options['can_edit'] = ($is_edit_page || $is_update_page);
							$options['object_type'] = $form_object_type;
							$options['object_id'] = $form_object_id;
							$options['page_activity'] = $form_activity;
							$val = isset($form_object[$fld]) ? $form_object[$fld] : ""; 
							if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
						} else {
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) ? $head['default_value'] : "";
							}
						}
						if($display_me) {
							$display = $this->createFormField($h_type,$options,$val);
							$js.=$display['js'];
							if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
								if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
									$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
								}
							}
						}
					} else {//if($fld=="contract_supplier") {
						if($is_edit_page || $is_update_page) {
							$pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
						} else {
							$pval = isset($head['default_value']) ? $head['default_value'] : "";
						}//echo "<br />".$fld."::".$pval.":";
						$sub_head = $headings['sub'][$head['id']];
						if(isset($form_object[$fld]) && is_array($form_object[$fld])) {
							$sub_form_object = $form_object[$fld];
						} else {
							$sub_form_object = isset($form_object[$fld]) ? array($form_object[$fld]) : array();
						}
						if(count($sub_form_object)==0) {
							foreach($sub_head as $shead) {
								$sub_form_object[0][$shead['field']] = "";
							}	
						}
						$td = "
						<div class=".$fld."><span class=spn_".$fld.">";
						$add_another[$fld] = false;
						foreach($sub_form_object as $sfo) {
							$td.="<span>";
							if($fld=="contract_supplier") {
								$td.="<input type=hidden value='".(isset($sfo['cs_id']) ? $sfo['cs_id'] : 0)."' name=".$fld."_id[] />";
							}
							$td.="<table class=sub_form width=100%>";
							foreach($sub_head as $shead) {
								//ASSIST_HELPER::arrPrint($shead);
								$sh_type = $shead['type'];
								$sfld = $shead['field'];
								if($fld=="contract_supplier") {
									$options = array('name'=>$sfld."[]",'req'=>$head['required']);
								} else {
									$options = array('id'=>$sfld,'name'=>$sfld,'req'=>$head['required']);
								}
								$val = "";
								if($sh_type=="LIST") {
									$list_items = array();
									$listObject = new CNTRCT_LIST($shead['list_table']);
									$list_items = $listObject->getActiveListItemsFormattedForSelect();
									$options['options'] = $list_items;
									if(count($list_items)==0 && $shead['required']==1) {
										$form_valid8 = false;
										$form_error[] = "The ".$shead['name']." list has not been populated but is a required field.";
										$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
									}
									if(count($list_items)>1) {
										$add_another[$fld] = true;
									}
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : 0;
									}
									unset($listObject);
								} elseif($sh_type=="BOOL"){
									$sh_type = "BOOL_BUTTON";
									$options['yes'] = 1;
									$options['no'] = 0;
									$options['extra'] = "boolButtonClickExtra";
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									}
									if(strlen($val)==0) {
										$val = (isset($shead['default_value']) && strlen($shead['default_value'])>0) ? $shead['default_value'] : 0;
									}
								} elseif($sh_type=="CURRENCY") {
									$options['extra'] = "processCurrency";
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
									}
								} else {
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
									}
								}
								$sdisplay = $this->createFormField($sh_type,$options,$val);
								$js.= $sdisplay['js'];
								$td.="
								<tr ".(strlen($shead['parent_link'])>0 ? "class='tr_".$shead['parent_link']."'" : "").">
									<th width=40% class=th2>".$shead['name'].":".($shead['required']==1?"*":"")."</th>
									<td>".$sdisplay['display']."</td>
								</tr>";
							}
						$td.= "
							</table></span>
							";
						}
						$td.="
							</span>
							".($add_another[$fld] ? "<p><input type=button value='Add Another' id=btn_".$fld." /></p>" : "")."
						</div>";
						$td_blank = "";
						if($add_another[$fld]) {
											$td_blank="<div id=".$fld."_blank><span>";
											if($fld=="contract_supplier") {
												$td_blank.="<input type=hidden value=\"0\" name=".$fld."_id[] />";
											}
											$td_blank.="<table class=sub_form width=100%>";
											foreach($sub_head as $shead) {
												$sh_type = $shead['type'];
												$sfld = $shead['field'];
												if($fld=="contract_supplier") {
													$options = array('name'=>$sfld."[]",'req'=>$head['required']);
												} else {
													$options = array('id'=>$sfld,'name'=>$sfld,'req'=>$head['required']);
												}
												$val = "";
												if($sh_type=="LIST") {
													$list_items = array();
													$listObject = new CNTRCT_LIST($shead['list_table']);
													$list_items = $listObject->getActiveListItemsFormattedForSelect();
													$options['options'] = $list_items;
													$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : 0;
													unset($listObject);
												} elseif($sh_type=="BOOL"){
													$sh_type = "BOOL_BUTTON";
													$options['yes'] = 1;
													$options['no'] = 0;
													$options['extra'] = "boolButtonClickExtra";
													$val = (isset($shead['default_value']) && strlen($shead['default_value'])>0) ? $shead['default_value'] : 0;
												} elseif($sh_type=="CURRENCY") {
													$options['extra'] = "processCurrency";
													$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
												}
												$sdisplay = $this->createFormField($sh_type,$options,$val);
												$td_blank.="
												<tr ".(strlen($shead['parent_link'])>0 ? "class=\"tr_".$shead['parent_link']."\"" : "").">
													<th width=40% class=th2>".$shead['name'].":".($shead['required']==1?"*":"")."</th>
													<td>".$sdisplay['display']."</td>
												</tr>";
											}
										$td_blank.= "
											</table></span>
										</div>
											";		
						} 
						$display = array('display'=>$td.$td_blank);
					} 
					if($display_me) {
						$echo.= "
						<tr id=tr_".$fld." ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
							<th width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
							<td>".$display['display']."
							</td>
						</tr>";
					}
				}
			}
			if(!$form_valid8) {
				$js.="
				$('#div_error').html(error_html).dialog({modal: true,dialogClass:\"dlg_frm_error\"}).addClass('ui-corner-all').css({\"border\":\"2px solid #cc0001\",\"background-color\":\"#fefefe\"});
				AssistHelper.hideDialogTitlebar('id','div_error');
				AssistHelper.hideDialogCSS('class','dlg_frm_error');
				$('#div_error #btn_error').button().click(function() {
					$('#div_error').dialog(\"close\");
				}).blur().parent('p').addClass('float');
				$('html, body').animate({scrollTop:0});
				";
			} //$formObject->displayObjectNames();
			//SAVE button not required in form - only at end of deliverable list
				/*<tr>
					<th></th>
					<td>
						<input type=button value=\"Save ".$formObject->getObjectName($object_type)."\" class=isubmit id=btn_save />
						".($form_object_type=="CONTRACT" ? "<input type=hidden name=last_deliverable_status value='".$last_deliverable_status."' />" : "")."
						
						".(($form_object_type!="CONTRACT" && strrpos(strtoupper($page_action), "ADD")!==FALSE) ? "<input type=hidden name=".$formObject->getParentFieldName()." value=\"".$parent_object_id."\" />" : "")."
					</td>
				</tr>*/
			$echo.="
			</table>
			<table class=form width=100% id=tbl_button_form style='margin-top: 10px'>
				<tr>
					<td class=center><button id=btn_save class=isubmit>".$formObject->getActivityName("save")."</button></td>
				</tr>
			</table>
		</form>";
		$data['js'].="
				
				//debugger;
				var error_html = \"<p>The following errors need to be attended to before any ".$formObject->getObjectName($object_type)."s can be added:</p> <ul><li>".implode('</li><li>',$form_error)."</li></ul><p>Where required lists have not been populated, please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>\"
				
				
				".$js."
				
				";
		if($formObject->hasDeadline() && !is_null($parentObject)) {
			$parent_deadline = $parentObject->getDeadlineDate();
			$now = strtotime(date("Y-m-d"));
			$then  = strtotime($parent_deadline);
			$diff = ($then-$now)/(3600*24);
			$data['js'].=" $('#".$formObject->getDeadlineField()."').datepicker(\"option\",\"maxDate\",\"+".$diff."D\");";
		}
		$data['js'].="
				
				
				$(\"form[name=frm_finance_object_".$this->form_random_id."] select\").each(function() {
					if($(this).children(\"option\").length<=1 && !$(this).hasClass(\"required\")) {
					}
				});
				$(\"form[name=frm_finance_object_".$this->form_random_id."] #btn_save\").button({icons: { primary: \"ui-icon-disk\" } }).addClass('i-am-the-submit-button').removeClass('ui-state-default').addClass('ui-state-ok').click(function() {
					//alert('save has been pushed');
					$"."form = $(\"form[name=frm_finance_object_".$this->form_random_id."]\");
					console.log(AssistForm.serialize($"."form));";
					//alert(AssistForm.serialize($"."form));";
		if($attachment_form) {
			$data['js'].="
					var f = 0;
					$('#firstdoc input:file').each(function() {
						if($(this).val().length>0) {
							f++;
						}
					});
					//alert(f);
					if(f>0) {
						CntrctHelper.processObjectFormWithAttachment($"."form,page_action,page_direct);
					} else {
						$('#has_attachments').val(0);
						//alert(AssistForm.serialize($"."form));
						CntrctHelper.processObjectForm($"."form,page_action,page_direct);
					}
					";
		} else {
			$data['js'].="
					CntrctHelper.processObjectForm($"."form,page_action,page_direct);
					";
		}
		$data['js'].="
					return false;
				});
				
				function boolButtonClickExtra($"."btn) {
					var i = $"."btn.prop(\"id\");
					var us = i.lastIndexOf(\"_\");
					var tr = \"tr_\"+i.substr(0,us);
					var act = i.substr(us+1,3);
					if(act==\"yes\") {
						$(\"tr.\"+tr).show();
					} else {
						$(\"tr.\"+tr).hide();
					}
				}
				
				function processCurrency($"."inpt) {
					var h = $"."inpt.parents(\"tr\").children(\"th\").html();
					h = h.substr(0,h.lastIndexOf(\":\"));
					alert(\"Only numbers (0-9) and a period (.) are permitted in the \"+h+\" field.\");		
				}
				";
				if(isset($add_another) && count($add_another)>0) {
					foreach($add_another as $key => $aa) {
						if($aa==true) {
							$data['js'].= "
							var ".$key." = $('#".$key."_blank').html();
							$('#".$key."_blank').hide();   
							$('#btn_".$key."').click(function() {
								$('div.".$key." span').children('span:last').after(".$key.");
							});
							";
						}
					}
				}
				
				$data['js'].="
				$(\"button\").each(function() {
						if($(this).attr(\"button_status\")==\"active\") {
							$(this).trigger(\"click\");
						}
				});
				
				//alert(AssistForm.serialize($(\"form[name=frm_deliverable]\")));
				
				$(\"form[name=frm_finance_object_".$this->form_random_id."] select\").each(function() {
					//alert($(this).children(\"option\").length);
					if($(this).children(\"option\").length==2) {
						if($(this).children(\"option:first\").val()==\"X\") {
							$(this).children(\"option:last\").prop(\"selected\",true);
						}
					}
				});
				
			";
				
				
		$data['display'] = $echo;
		return $data;		
		
		
		
		
	}




	/**
	 * Function to create the summary table for finances
	 * @param (String) $form_object_type = what section of finances
	 * @param (Object) $formObject = object of relevant finance class
	 * @param (Int) $parent_object_id = Contract ID
	 * 
	 * @return (ECHO) Html
	 * @return (String) JS
	 */
	public function drawSummaryTable($form_object_type,$formObject,$parent_object_id) {
		$display = $this->getSummaryTable($form_object_type, $formObject, $parent_object_id);
		echo $display['display'];
		return $display['js'];
	}

	/**
	 * Function to create the summary table for finances
	 * @param (String) $form_object_type = what section of finances
	 * @param (Object) $formObject = object of relevant finance class
	 * @param (Int) $parent_object_id = Contract ID
	 * 
	 * @return (Array) ('display'=>HTML,'js'=>jquery)
	 */
	public function getSummaryTable($form_object_type,$formObject,$parent_object_id) {
		$helper = new CNTRCT_HELPER();
		if(strtoupper(substr($form_object_type,0,7))=="FINANCE") {
			$ot = explode("_",$form_object_type);
			unset($ot[0]);
			$object_type = implode("_",$ot);
		} else {
			$object_type = $form_object_type;
		}
		$display = array('display'=>"",'js'=>"");
		
		$data = $formObject->getSummaryData($parent_object_id);
		
		foreach($data['rows'] as $key => $d) {
			$data['rows'][$key] = ASSIST_HELPER::format_currency($d);
		}
		
		$display['display'] = "
		<h2>Current ".$helper->getObjectName($object_type)."</h2>
		<table id=tbl_finance_summary class=finance>";
		foreach($data['head'] as $fld=>$head) {
			$display['display'].="
			<tr>
				<th class=".$head['format'].">".$head['name'].":</th>
				<td class='right ".$head['format']."'>".$data['rows'][$fld]."</td>
			</tr>";
		}
		$display['display'].="
		</table>
		";
		
		switch($form_object_type) {
			case "BUDGET":
			case "FINANCE_BUDGET":
				break;
		}
		
		$display['js'] = "
		$('#tbl_finance_summary td.TOTAL').addClass('b');
		";
		
		
		return $display;
	}













}


?>