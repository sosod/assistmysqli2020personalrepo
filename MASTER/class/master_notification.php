<?php
/**
 * To process and trigger SMS and email notifications
 * 
 * Created on: 14 October 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
class MASTER_NOTIFICATION extends ASSIST_MODULE_NOTIFICATION{
	
	
	protected $data;
	protected $type = "EMAIL";
	protected $users;
	protected $assist_email;
	protected $logObject;
	protected $obj;
	protected $acronym;
	private $assist;
	

	public function __construct($obj){
		parent::__construct("","","MASTER");
		switch(strtoupper($obj)){
			case "ACTION":
			$this->assist_email = new ASSIST_MODULE_EMAIL("Action","CA");
			$this->acronym = "CA";
				break;
			case "DELIVERABLE":
			$this->assist_email = new ASSIST_MODULE_EMAIL("Deliverable","CD");
			$this->acronym = "CD";
				break;
			case "CONTRACT":
			$this->assist_email = new ASSIST_MODULE_EMAIL("Contract","C");
			$this->acronym = "C";
				break;
			default:
				return "Incorrect object type fed in - please use 'action', 'deliverable', or 'contract'. You fed in '$obj'";
				break;
    	}
		$this->logObject = new MASTER_LOG();
		$this->assist = new ASSIST();
		$this->helper = new MASTER_HELPER();
	}
	
	public function formatNote($data, $mode, $object){

			$id = $data['object_id'];	
			switch (strtolower($object)) {
				case 'contract':
					switch (strtoupper($mode)) {
						case 'MINI':
							//SMS format
							$listObj = new MASTER_LIST();
							$user = explode(" ",$data['changes']['user']);
							$user[0]=substr($user[0],0,1);
							switch($data['log_type']){
								case MASTER_LOG::UPDATE:
									$changed = $this->helper->getActivityName("updated");
									break;
								case MASTER_LOG::EDIT:
									$changed = $this->helper->getActivityName("edited");
									break;
								case MASTER_LOG::UNLOCK:
									$changed = $this->helper->getActivityName("unlocked");
									break;
								case MASTER_LOG::APPROVE:
									$changed = $this->helper->getActivityName("approved");
									break;
								case MASTER_LOG::AWAITING_APPROVAL:
									$changed = "marked as complete";
									break;
								case MASTER_LOG::DECLINE:
									$changed = $this->helper->getActivityName("declined");
									break;
								case MASTER_LOG::DEACTIVATE:
									$changed = $this->helper->getActivityName("deactivated");
									break;
								case MASTER_LOG::CREATE:
									$sms = "A new ".$listObj->getContractObjectName()." (".$this->acronym."".$id.") on ".$this->assist->getModTitle()." has been ".$this->helper->getActivityName("created")." by ".$user[0]." ".$user[1].". Go to http://assist.action4u.co.za";
									return array($sms,$data['recipients']);
									break;
								case MASTER_LOG::CONFIRM:
									$sms = "The ".$listObj->getContractObjectName()." (".$this->acronym."".$id.") on ".$this->assist->getModTitle()." has been ".$this->helper->getActivityName("confirmed")." by ".$user[0]." ".$user[1]." and awaits activation. Go to http://assist.action4u.co.za";
									return array($sms,$data['recipients']);
									break;
								case MASTER_LOG::ACTIVATE:
									$sms = "The ".$listObj->getContractObjectName()." (".$this->acronym."".$id.") on ".$this->assist->getModTitle()." has been ".$this->helper->getActivityName("activated")." by ".$user[0]." ".$user[1].". Go to http://assist.action4u.co.za";
									return array($sms,$data['recipients']);
									break;
									
							}
							$sms = $listObj->getContractObjectName()." (".$this->acronym."".$id.") on ".$this->assist->getModTitle()." has been ".$changed." by ".$user[0]." ".$user[1].". Go to http://assist.action4u.co.za";
							return array($sms,$data['recipients']);
							break;
						default:					
							$conObj = new MASTER_CONTRACT();
							$this->logObject->setLogTable($conObj->getMyLogTable());
							$var = array("type"=>"EMAIL","id"=>$id);
							$details = $conObj->getObject($var);
				//			return $details;
							//Child details
							foreach($details['email_fields']['child'] as $key=>$val){
								if($key != "id"){
									$value=(is_array($details['rows']['child'][$val])?$details['rows']['child'][$val]['display']:$details['rows']['child'][$val]);
									$object_name = strlen($this->helper->getObjectName($details['head'][$val]['name']))>0?$this->helper->getObjectName($details['head'][$val]['name']):$details['head'][$val]['name'];
									$fld[$object_name]=$value;
								}
							}
							$cname = $details['email_fields']['child']['name'];
							$child_name = $this->helper->getObjectName(strtolower($details['head'][$cname]['section']));
							$child_ref = $details['email_fields']['child']['id'];
							$log_result = $this->logObject->processAuditLogs("NOTIFICATION",$data['changes']);
							switch($data['log_type']){
								case MASTER_LOG::ACTIVATE:
								//$con_name = $details['rows']['child']['contract_name'];
								$con_name = is_array($details['rows']['child']['contract_name'])?$details['rows']['child']['contract_name']['display']:$details['rows']['child']['contract_name'];
								//return $con_name;
								$users = $conObj->getObjectRecipients(array("id"=>$id,"activity"=>"activate"));
								//$users = array_unique($users);
								
								foreach($users as $a=>$b){
									foreach($b as $c=> $d){
										if($c === "tkid"){
											$user[]=$b[$c];
										}else if(is_array($d)){
											$user[]=$d['tkid'];	
										}
									}
								}
								if(count($user) === 0){
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
								}else{
									$mail = $this->assist_email->sendActivationEmail($id, $user, array(), $con_name,"items");
								}						
									break;
								case MASTER_LOG::DEACTIVATE:
								$con_name = is_array($details['rows']['child']['contract_name'])?$details['rows']['child']['contract_name']['display']:$details['rows']['child']['contract_name'];
								$users = $conObj->getObjectRecipients(array("id"=>$id,"activity"=>"deactivate"));
								//$user = $conObj->getRecipients($id,"deactivate");
								$user = array_unique($user);
								foreach($users as $a=>$b){
									foreach($b as $c=> $d){
										if($c === "tkid"){
											$user[]=$b[$c];
										}else if(is_array($d)){
											$user[]=$d['tkid'];	
										}
									}
								}
								if(count($user) === 0){
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
								}else{
									$mail = $this->assist_email->sendDeactivationEmail($id, $user, array(), $con_name,"items");
								}						
									break;
								case MASTER_LOG::CREATE:
									/* This case should be processed when the contract is ready for confirmation */
								$user = $conObj->getRecipients($id,"new");
								//return array($user,$data['exclude']);
								if(isset($data['exclude']) && count($data['exclude'])>0){
									foreach($data['exclude'] as $tkid){
										unset($user[$tkid]);
									}
								}
								$user = array_unique($user);
								if(count($user) == 0){
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
								}else{
									$mail = $this->assist_email->sendNewEmail($id, $user, array(), $fld, array());
								}						
									break;
								case MASTER_LOG::CONFIRM:
								$user = $conObj->getRecipients($id,"confirm");
								$user = array_unique($user);
								if(count($user) == 0){
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
								}else{
									$mail = $this->assist_email->sendConfirmationEmail($id, $user, array());
								}						
									break;
								case MASTER_LOG::UPDATE:
								$user = $conObj->getRecipients($id,"update");
								$user = array_unique($user);
					//				return $log_result;
								if(count($user) == 0){
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
								}else{
									$mail = $this->assist_email->sendUpdateEmail($id, $user, array(), $log_result, $fld);
								}
									break;
								case MASTER_LOG::EDIT:
								$user = $conObj->getRecipients($id,"edit");
								$user = array_unique($user);
					//				return $log_result;
								if(count($user) == 0){
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
								}else{
									$mail = $this->assist_email->sendEditEmail($id, $user, array(), $log_result, $fld, $fld);
								}
									break;
							}

							break;
					}
					break;
				
				case 'deliverable':
					switch ($mode) {
						case 'MINI':
						//SMS format
						$listObj = new MASTER_LIST();
						$user = explode(" ",$data['changes']['user']);
						$user[0]=substr($user[0],0,1);
						switch($data['log_type']){
							case MASTER_LOG::UPDATE:
								$changed = $this->helper->getActivityName("updated");
								break;
							case MASTER_LOG::EDIT:
								$changed = $this->helper->getActivityName("edited");
								break;
							case MASTER_LOG::UNLOCK:
								$changed = $this->helper->getActivityName("unlocked");
								break;
							case MASTER_LOG::APPROVE:
								$changed = $this->helper->getActivityName("approved");
								break;
							case MASTER_LOG::AWAITING_APPROVAL:
								$changed = "marked as complete";
								break;
							case MASTER_LOG::DECLINE:
								$changed = $this->helper->getActivityName("declined");
								break;
								
						}
						$sms = $listObj->getDeliverableObjectName()." (".$this->acronym."".$id.") on ".$this->assist->getModTitle()." has been ".$changed." by ".$user[0]." ".$user[1].". Go to http://assist.action4u.co.za";
						return array($sms,$data['recipients']);
							break;
						
						default:
						//Email format
						$delObj = new MASTER_DELIVERABLE();
						$this->logObject->setLogTable($delObj->getMyLogTable());
						$var = array("type"=>"EMAIL","id"=>$id);
						$details = $delObj->getObject($var);
	//		return $details;			
						//Child details
						foreach($details['email_fields']['child'] as $key=>$val){
							if($key != "id"){
								$value=(is_array($details['rows']['child'][$val])?$details['rows']['child'][$val]['display']:$details['rows']['child'][$val]);
								$object_name = strlen($this->helper->getObjectName($details['head'][$val]['name']))>0?$this->helper->getObjectName($details['head'][$val]['name']):$details['head'][$val]['name'];
								$fld[$object_name]=$value;
							}
						}
						$cname = $details['email_fields']['child']['name'];
						$child_name = $this->helper->getObjectName(strtolower($details['head'][$cname]['section']));
						//Parent details
						foreach($details['email_fields']['parent'] as $key=>$val){
							if($key != "id"){
								$value=(is_array($details['rows']['parent'][$val])?$details['rows']['parent'][$val]['display']:$details['rows']['parent'][$val]);
								$object_name = strlen($this->helper->getObjectName($details['head'][$val]['name']))>0?$this->helper->getObjectName($details['head'][$val]['name']):$details['head'][$val]['name'];
								$parent_details[$object_name]=$value;
							}
						}
						$pname = $details['email_fields']['parent']['name'];
						$parent_name = $this->helper->getObjectName(strtolower($details['head'][$pname]['section']));
						$parent_ref = $details['email_fields']['parent']['id'];
						//Grandparent details
						foreach($details['email_fields']['grandparent'] as $key=>$val){
							if($key != "id"){
								$value=(is_array($details['rows']['grandparent'][$val])?$details['rows']['grandparent'][$val]['display']:$details['rows']['grandparent'][$val]);
								$object_name = strlen($this->helper->getObjectName($details['head'][$val]['name']))>0?$this->helper->getObjectName($details['head'][$val]['name']):$details['head'][$val]['name'];
								$gparent_details[$object_name]=$value;
							}
						}
						$gpname = $details['email_fields']['grandparent']['name'];
						$gparent_name = $this->helper->getObjectName(strtolower($details['head'][$gpname]['section']));
						$gparent_ref = $details['email_fields']['grandparent']['id'];
						//All changes
						$all_parents=array(array($parent_name,$parent_ref,$parent_details));
	//		return $all_parents;			
						//User making the changes
						$changer = $data['changes']['user'];
						unset($data['changes']['user']);
						//Get details in nice clear format
						$log_result = $this->logObject->processAuditLogs("NOTIFICATION",$data['changes']);
	//		return $log_result;
						switch($data['log_type']){
							case MASTER_LOG::UPDATE:
							$user = $delObj->getRecipients($id,"update");
							if(isset($data['changes']['deliverable_owner']['raw']['from'])){
								$user[]=$data['changes']['deliverable_owner']['raw']['from'];
							}
							if($user[0] == $this->getUserID()){
	//REMOVE tkid from $user TO NOT NOTIFY SELF	
	//		return "You are owner. Remove self from array if appropriate";
								$user = array_unique($user);
							}else{
							/*	
								$ownerObj = new MASTER_CONTRACT_OWNER();
								$conObject = new MASTER_CONTRACT();
								$par_id = $delObj->getParentID($id);
								$CO = $conObject->getRawObject($par_id);
								$owner = $conObject->getOwnerFieldName();
								$dept_id = $CO[$owner];
								$owners = $ownerObj->getActiveAdminsForOwner($dept_id);
								foreach($owners as $key=>$val){
									$user[]=$key;
								}
							 */
							 //Adding on the old owner if it's set
								$user = array_unique($user);
							}
			//return ($user);				
							if(count($user) == 0){
								return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
							}else{
								//$t[]=$user;
								if($data['progress'] == 100){
									$mail[] = $this->assist_email->sendApprovalRequestEmail($id, $user, array());
									//$mail[] = $this->assist_email->sendApprovalRequestEmail($id, $t, array(), $log_result, $fld, $all_parents);
									$data['log_type'] = MASTER_LOG::AWAITING_APPROVAL;
									$data['changes']['user'] = $changer;
									$mail[] = $this->prepareNote($data, "SMS", $object);
								}
								$mail[] = $this->assist_email->sendUpdateEmail($id, $user, array(), $log_result, $fld, $all_parents);
							}
								break; 
								
							case MASTER_LOG::EDIT:
								$user = $delObj->getRecipients($id,"edit");
								if(isset($data['include'])){
									if(is_array($data['include'])){
										foreach($data['include'] as $person){
											$user[]=$person;
										}
									}else{
										$user[]=$data['include'];
									}	
								}
								if(isset($data['changes']['deliverable_owner']['raw']['from'])){
									//$user[]=$data['changes']['deliverable_owner']['raw']['from'];
								}
								if(in_array($this->getUserID(),$user)){
//									$key = array_search($this->getUserID(), $user);
//Don't notify	self				unset($user[$key]);
									$user = array_unique($user);
								}else{
									$user = array_unique($user);
								}
							//	return $user;
							//	return ($this->assist_email->getEmailAddresses($user));				
								if(count($user) == 0){
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
								}else{
									$mail = $this->assist_email->sendEditEmail($id, $user, array(), $log_result, $fld, $all_parents);
								}
							//	return $mail;
								break;
							case MASTER_LOG::CREATE:
								$user = $delObj->getRecipients($id,"new");
								if($user[0] == $this->getUserID()){
									$user = array_unique($user);
								}else{
									$user = array_unique($user);
								}
								if(count($user) == 0){
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
								}else{
									$mail = $this->assist_email->sendNewEmail($id, $user, array(), $fld, $all_parents);
									return array($fld, $user,$all_parents);
								}
								break;
							case MASTER_LOG::APPROVE:
								$user = $delObj->getRecipients($id,"edit");
								foreach($user as $key=>$tk){
									if($this->getUserID() == $tk){
										unset($user[$key]);	
									}
								}
								if(count($user) == 0){
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
								}
								//Adding on the old owner if it's set
								if(isset($data['changes']['del_owner']['raw']['from'])){
									$user[]=$data['changes']['del_owner']['raw']['from'];
								}
									/* NOT SURE IF WANT
									//User making the changes
									$changer = $data['changes']['user'];
									unset($data['changes']['user']);
									*/
								//Send Mail with argument [3] as message, [4] as parent details
								$mail = $this->assist_email->sendApproveEmail($id, $user, array(), $data['changes']['response'],array($parent_name,$parent_ref,$parent_details));
								break;
							case MASTER_LOG::DECLINE:
								$user = $delObj->getRecipients($id,"edit");
								foreach($user as $key=>$tk){
									if($this->getUserID() == $tk){
										unset($user[$key]);	
									}
								}
								if(count($user) == 0){
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
								}
								//Adding on the old owner if it's set
								if(isset($data['changes']['del_owner']['raw']['from'])){
									$user[]=$data['changes']['del_owner']['raw']['from'];
								}
									/* NOT SURE IF WANT
									//User making the changes
									$changer = $data['changes']['user'];
									unset($data['changes']['user']);
									*/
								//Send Mail with argument [3] as message, [4] as parent details
								$mail = $this->assist_email->sendDeclineEmail($id, $user, array(), $data['changes']['response'],array($parent_name,$parent_ref,$parent_details));
								break;
							case MASTER_LOG::UNLOCK:
								$user = $delObj->getRecipients($id,"edit");
								foreach($user as $key=>$tk){
									if($this->getUserID() == $tk){
										unset($user[$key]);	
									}
								}
								if(count($user) == 0){
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
								}
								//Adding on the old owner if it's set
								if(isset($data['changes']['del_owner']['raw']['from'])){
									$user[]=$data['changes']['del_owner']['raw']['from'];
								}
									/* NOT SURE IF WANT
									//User making the changes
									$changer = $data['changes']['user'];
									unset($data['changes']['user']);
									*/
								//Send Mail with argument [3] as message, [4] as parent details
								$mail = $this->assist_email->sendUnlockEmail($id, $user, array(), $data['changes']['response'],array($parent_name,$parent_ref,$parent_details));
								break;
						}
							break;
					}
				$mail = is_array($mail)?implode(" | ",$mail):$mail;
				return array("cmpcode"=>$this->getCmpCode(),"tkid"=>$this->getUserID(),"modref"=>$this->getModRef(),"mode"=>$mode,"id"=>$id,"object"=>$object,"mail_notification"=>$mail,"recipients"=>$this->assist_email->getEmailAddresses($user,array()));
					break;
				
				case 'action':
					switch ($mode) {
						case 'MINI':
						//SMS format
						$listObj = new MASTER_LIST();
						$user = explode(" ",$data['changes']['user']);
						$user[0]=substr($user[0],0,1);
						switch($data['log_type']){
							case MASTER_LOG::UPDATE:
								$changed = $this->helper->getActivityName("updated");
								break;
							case MASTER_LOG::EDIT:
								$changed = $this->helper->getActivityName("edited");
								break;
							case MASTER_LOG::UNLOCK:
								$changed = $this->helper->getActivityName("unlocked");
								break;
							case MASTER_LOG::APPROVE:
								$changed = $this->helper->getActivityName("approved");
								break;	
							case MASTER_LOG::AWAITING_APPROVAL:
								$changed = "marked as complete";
								break;
							case MASTER_LOG::DECLINE:
								$changed = $this->helper->getActivityName("declined");
								break;
								
						}
						$sms = $listObj->getActionObjectName()." (".$this->acronym."".$id.") on ".$this->assist->getModTitle()." has been ".$changed." by ".$user[0]." ".$user[1].". Go to http://assist.action4u.co.za";
					//	$log_result = $this->logObject->processAuditLogs("NOTIFICATION",$data['changes']);
					//	$log_result = $data['changes'];
						return array($sms,$data['recipients']);
							break;
						
						default:
						//Email format
						//Milestone REACHED
							$actObj = new MASTER_ACTION();
							$this->logObject->setLogTable($actObj->getMyLogTable());
							//--------Details for all cases:-------------//
							$var = array("type"=>"EMAIL","id"=>$id);
							$details = $actObj->getObject($var);
		//return $details;
								//Child details
								foreach($details['email_fields']['child'] as $key=>$val){
									if($key != "id"){
										$value=(is_array($details['rows']['child'][$val])?$details['rows']['child'][$val]['display']:$details['rows']['child'][$val]);
										$object_name = strlen($this->helper->getObjectName($details['head'][$val]['name']))>0?$this->helper->getObjectName($details['head'][$val]['name']):$details['head'][$val]['name'];
										$fld[$object_name]=$value;
									}
								}
								$cname = $details['email_fields']['child']['name'];
								$child_name = $this->helper->getObjectName(strtolower($details['head'][$cname]['section']));
								//Parent details
								foreach($details['email_fields']['parent'] as $key=>$val){
									if($key != "id"){
										$value=(is_array($details['rows']['parent'][$val])?$details['rows']['parent'][$val]['display']:$details['rows']['parent'][$val]);
										$object_name = strlen($this->helper->getObjectName($details['head'][$val]['name']))>0?$this->helper->getObjectName($details['head'][$val]['name']):$details['head'][$val]['name'];
										$parent_details[$object_name]=$value;
									}
								}
								$pname = $details['email_fields']['parent']['name'];
								$parent_name = $this->helper->getObjectName(strtolower($details['head'][$pname]['section']));
								$parent_ref = $details['email_fields']['parent']['id'];
								//Grandparent details
								foreach($details['email_fields']['grandparent'] as $key=>$val){
									if($key != "id"){
										$value=(is_array($details['rows']['grandparent'][$val])?$details['rows']['grandparent'][$val]['display']:$details['rows']['grandparent'][$val]);
										$object_name = strlen($this->helper->getObjectName($details['head'][$val]['name']))>0?$this->helper->getObjectName($details['head'][$val]['name']):$details['head'][$val]['name'];
										$gparent_details[$object_name]=$value;
									}
								}
								$gpname = $details['email_fields']['grandparent']['name'];
								$gparent_name = $this->helper->getObjectName(strtolower($details['head'][$gpname]['section']));
								$gparent_ref = $details['email_fields']['grandparent']['id'];
							switch($data['log_type']){
								case MASTER_LOG::UPDATE:
								//	return $data;
									//$prog = ($data['progress']==100?true:false);
									$user = $actObj->getRecipients($id,"update");
									foreach($user as $key=>$tk){
								//		return $tk;
										if($this->getUserID() == $tk){
											unset($user[$key]);	
										}
									}
						//		return $user;
									if(count($user) == 0){
										return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
									}
									$all_parents=array(array($parent_name,$parent_ref,$parent_details),array($gparent_name,$gparent_ref,$gparent_details));
					//				return $all_parents;
					//				return $parent_details;
					//				return $gparent_details;

									$t=$user;
									$changer = $data['changes']['user'];
									unset($data['changes']['user']);
									$log_result = $this->logObject->processAuditLogs("NOTIFICATION",$data['changes']);
					//				return "mail";
									//$mail = $this->assist_email->sendUpdateEmail($id, $t, $t, $log_result, $fld, "" ,$parent_details,$gparent_details);
									if($data['progress'] == 100){
										$mail[] = $this->assist_email->sendApprovalRequestEmail($id, $t, array(), $log_result, $fld, $all_parents);
										$data['log_type'] = MASTER_LOG::AWAITING_APPROVAL;
										$data['changes']['user'] = $changer;
										$mail[] = $this->prepareNote($data, "SMS", $object);
									}
									$mail[] = $this->assist_email->sendUpdateEmail($id, $t, array(), $log_result, $fld, $all_parents);
									//return json_encode($mail);
									//$mail = $log_result;
									break;
								case MASTER_LOG::CREATE:
									return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
						//ASSIST_HELPER::arrPrint($data);
						//return $data;	
									$user = $actObj->getRecipients($id,"create");
									$var = array("type"=>"EMAIL","id"=>$id);
									$details = $actObj->getObject($var);
									foreach($user as $key=>$tk){
										if($this->getUserID() == $tk){
											unset($user[$key]);	
										}
									}
									if(count($user) == 0){
										return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
									}
									
									$all_parents=array(array($parent_name,$parent_ref,$parent_details),array($gparent_name,$gparent_ref,$gparent_details));
									$t=$user;
									$changer = $data['changes']['user'];
									unset($data['changes']['user']);
									$log_result = $this->logObject->processAuditLogs("NOTIFICATION",$data['changes']);
									//No emails should be sent on creation
									$mail = $this->assist_email->sendNewEmail($id, $t, array(), $fld, $all_parents);
									
									break;
								case MASTER_LOG::EDIT:
									$user = $actObj->getRecipients($id,"edit");
						//			return $user;
									$var = array("type"=>"EMAIL","id"=>$id);
									$details = $actObj->getObject($var);
						//			return $details;
									foreach($user as $key=>$tk){
										if($this->getUserID() == $tk){
											unset($user[$key]);	
										}
									}
									if(count($user) == 0){
										return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
									}
									//Adding on the old owner if it's set
									if(isset($data['changes']['action_owner']['raw']['from'])){
										$user[]=$data['changes']['action_owner']['raw']['from'];
									}
									$t=$user;
						//return $user;
									//Child details
									foreach($details['email_fields']['child'] as $key=>$val){
										if($key !== "id"){
											$value=(is_array($details['rows']['child'][$val])?$details['rows']['child'][$val]['display']:$details['rows']['child'][$val]);
											$fld[$details['head'][$val]['name']]=$value;
										$dbg[]=$value;
										}
									}
						//return $fld;			
						//return $dbg;			
									$cname = $details['email_fields']['child']['name'];
									$child_name = ucwords(strtolower($details['head'][$cname]['section']));
									$child_ref = $details['email_fields']['child']['id'];
									//Parent details
									foreach($details['email_fields']['parent'] as $key=>$val){
										if($key != "id"){
											$value=(is_array($details['rows']['parent'][$val])?$details['rows']['parent'][$val]['display']:$details['rows']['parent'][$val]);
											$parent_details[$details['head'][$val]['name']]=$value;
										}
									}
									$pname = $details['email_fields']['parent']['name'];
									$parent_name = ucwords(strtolower($details['head'][$pname]['section']));
									$parent_ref = $details['email_fields']['parent']['id'];
									//Grandparent details
									foreach($details['email_fields']['grandparent'] as $key=>$val){
										if($key != "id"){
											$value=(is_array($details['rows']['grandparent'][$val])?$details['rows']['grandparent'][$val]['display']:$details['rows']['grandparent'][$val]);
											$gparent_details[$details['head'][$val]['name']]=$value;
										}
									}
									$gpname = $details['email_fields']['grandparent']['name'];
									$gparent_name = ucwords(strtolower($details['head'][$gpname]['section']));
									$gparent_ref = $details['email_fields']['grandparent']['id'];
									//All changes
							//idk why not		$all_parents=array(array($child_name,$child_ref,$fld),array($parent_name,$parent_ref,$parent_details),array($gparent_name,$gparent_ref,$gparent_details));
									$all_parents=array(array($parent_name,$parent_ref,$parent_details),array($gparent_name,$gparent_ref,$gparent_details));
					//return $all_parents;
									//User making the changes
									$changer = $data['changes']['user'];
									unset($data['changes']['user']);
									//Get details in nice clear format
									$log_result = $this->logObject->processAuditLogs("NOTIFICATION",$data['changes']);
									//Send Mail
									$mail = $this->assist_email->sendEditEmail($id, $t, array(), $log_result, $fld, $all_parents);
									break;
								case MASTER_LOG::APPROVE:
									$user = $actObj->getRecipients($id,"edit");
									foreach($user as $key=>$tk){
										if($this->getUserID() == $tk){
											unset($user[$key]);	
										}
									}
									if(count($user) == 0){
										return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
									}
									//Adding on the old owner if it's set
									if(isset($data['changes']['action_owner']['raw']['from'])){
										$user[]=$data['changes']['action_owner']['raw']['from'];
									}
										/* NOT SURE IF WANT
										//User making the changes
										$changer = $data['changes']['user'];
										unset($data['changes']['user']);
										*/
									//Fetch Parental details
									$var = array("type"=>"EMAIL","id"=>$id);
									$details = $actObj->getObject($var);
									foreach($details['email_fields']['parent'] as $key=>$val){
										if($key != "id"){
											$value=(is_array($details['rows']['parent'][$val])?$details['rows']['parent'][$val]['display']:$details['rows']['parent'][$val]);
											$parent_details[$details['head'][$val]['name']]=$value;
										}
									}
									$pname = $details['email_fields']['parent']['name'];
									$parent_name = ucwords(strtolower($details['head'][$pname]['section']));
									$parent_ref = $details['email_fields']['parent']['id'];
									//Send Mail with argument [3] as message, [4] as parent details
									$mail = $this->assist_email->sendApproveEmail($id, $user, array(), $data['changes']['response'],array($parent_name,$parent_ref,$parent_details));
									break;
								case MASTER_LOG::DECLINE:
									$user = $actObj->getRecipients($id,"edit");
									foreach($user as $key=>$tk){
										if($this->getUserID() == $tk){
											unset($user[$key]);	
										}
									}
									if(count($user) == 0){
										return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
									}
									//Adding on the old owner if it's set
									if(isset($data['changes']['action_owner']['raw']['from'])){
										$user[]=$data['changes']['action_owner']['raw']['from'];
									}
										/* NOT SURE IF WANT
										//User making the changes
										$changer = $data['changes']['user'];
										unset($data['changes']['user']);
										*/
									//Fetch Parental details
									$var = array("type"=>"EMAIL","id"=>$id);
									$details = $actObj->getObject($var);
									foreach($details['email_fields']['parent'] as $key=>$val){
										if($key != "id"){
											$value=(is_array($details['rows']['parent'][$val])?$details['rows']['parent'][$val]['display']:$details['rows']['parent'][$val]);
											$parent_details[$details['head'][$val]['name']]=$value;
										}
									}
									$pname = $details['email_fields']['parent']['name'];
									$parent_name = ucwords(strtolower($details['head'][$pname]['section']));
									$parent_ref = $details['email_fields']['parent']['id'];
									//Send Mail with argument [3] as message, [4] as parent details
									$mail = $this->assist_email->sendDeclineEmail($id, $user, array(), $data['changes']['response'],array($parent_name,$parent_ref,$parent_details));
									break;
								case MASTER_LOG::UNLOCK:
									$user = $actObj->getRecipients($id,"edit");
									foreach($user as $key=>$tk){
										if($this->getUserID() == $tk){
											unset($user[$key]);	
										}
									}
									if(count($user) == 0){
										return array("MODE"=>$mode,"ID"=>$id,"OBJECT"=>$object,"NONE"=>true);
									}
									//Adding on the old owner if it's set
									if(isset($data['changes']['action_owner']['raw']['from'])){
										$user[]=$data['changes']['action_owner']['raw']['from'];
									}
										/* NOT SURE IF WANT
										//User making the changes
										$changer = $data['changes']['user'];
										unset($data['changes']['user']);
										*/
									//Fetch Parental details
									$var = array("type"=>"EMAIL","id"=>$id);
									$details = $actObj->getObject($var);
									foreach($details['email_fields']['parent'] as $key=>$val){
										if($key != "id"){
											$value=(is_array($details['rows']['parent'][$val])?$details['rows']['parent'][$val]['display']:$details['rows']['parent'][$val]);
											$parent_details[$details['head'][$val]['name']]=$value;
										}
									}
									$pname = $details['email_fields']['parent']['name'];
									$parent_name = ucwords(strtolower($details['head'][$pname]['section']));
									$parent_ref = $details['email_fields']['parent']['id'];
									//Send Mail with argument [3] as message, [4] as parent details
									$mail = $this->assist_email->sendUnlockEmail($id, $user, array(), $data['changes']['response'],array($parent_name,$parent_ref,$parent_details));
									break;
							}
						//return array("cmpcode"=>$this->getCmpCode(),"tkid"=>$this->getUserID(),"modref"=>$this->getModRef(),"mode"=>$mode,"id"=>$id,"object"=>$object,"mail_notification"=>$mail,"recipients"=>$this->assist_email->getEmailAddresses($user,array()));
						break;		
					}
					break;
				
				case 'budget':
					
					break;
				
				case 'payment':
					
					break;
				
				default:
					return"invalid parameter 2 - use 'contract', 'deliverable', etc. You fed in $object";
					break;
			}	
			$mail = is_array($mail)?implode(" | ",$mail):$mail;
			foreach($mail as $m){
				if(is_array($m)){
					$m = implode(" | ",$m); 
				}
			}
			return array("cmpcode"=>$this->getCmpCode(),"tkid"=>$this->getUserID(),"modref"=>$this->getModRef(),"mode"=>$mode,"id"=>$id,"object"=>$object,"mail_notification"=>$mail,"recipients"=>$this->assist_email->getEmailAddresses($user,array()));
			//return $mail;
		}
	
	public function sendNote($dta, $object){
		
		//$subject = $dta['changes']['response'];
		//foreach($users as $tkid){
			//$this->sendEmail($subject, $tkid);
		//}
		return $dta;
	}
	
	/**
	 * Function to tell you who notifications will be sent to
	 * @author Duncan Cosser [duncancosser@gmail.com]
	 * @param {int} The ID of the object
	 * @param {string} The type of the object (action, deliverable, contract)
	 * @param {string} The action being performed on the object
	 * @return {array} The list of users in the (currently unknown) format
	 * @version 1.0.0
	 */
	 
	public function getRecipients($id,$object,$action){
		switch (strtolower($object)) {
			case 'contract':
				$obj = new MASTER_CONTRACT();
				
				break;
			
			case 'deliverable':
				$obj = new MASTER_DELIVERABLE();
				
				break;
			
			case 'action':
				$obj = new MASTER_ACTION();
				break;
		}
		$recips = $obj->getRecipients($id,strtolower($action));
		return $recips;
	}
	
	public function prepareNote($data, $type, $object){
		$today_file = date("Y-m-d").".txt";
		$nownow = date("H:i:s");
		$handle = fopen("../logs/mail/responsive/".$today_file, "a");
		switch ($type) {
			case 'SMS':
				$sms_obj = new ASSIST_SMS();
				$res = $this->formatNote($data, "MINI", $object);
				$sms_res = $sms_obj->sendPolySMS($res[0], $res[1]);
				$write = fwrite($handle, PHP_EOL.$nownow.PHP_EOL."SMS:".json_encode($res)." --> ".$sms_res.PHP_EOL);
				$res = array("info",implode(", ", $res));
				if(is_array($sms_res)&& $sms_res[0]==false){
					return array(false,"SMS balance too low");
				}
				break;
			case 'BOTH':
				$sms_obj = new ASSIST_SMS();
				//Prepare and send mail
				$res[0] = $this->formatNote($data, "MAIL", $object);
				//Prepare SMS
				$res[1] = $this->formatNote($data, "MINI", $object);
				//Send SMS
				$sms_res = $sms_obj->sendPolySMS($res[1][0], $res[1][1]);
				//Log notifications
				$write = fwrite($handle, PHP_EOL.$nownow.PHP_EOL."Mail:".json_encode($res[0]).PHP_EOL."SMS:".PHP_EOL.json_encode($res[1])." --> ".json_encode($sms_res[1]).PHP_EOL);
				//$res = array("info",implode(",", $res[0])." | ".$res[1]);

				if(isset($res[0]['NONE'])){
					$res = array("ok","No emails to be sent");
				}else if(strlen($write) == 0 || $write == false || $handle == false){
					//$res = array("info","Notifications sent successfully, but system logging stopped.");
					foreach($res[0]['recipients']['to'] as $r){
						$recips[] = implode(" - ",$r);
					}
					$res = implode(", ",$recips);
					$res = array("info",$res);
				}else{
					//Get names and email addresses
					foreach($res[0]['recipients']['to'] as $r){
						$recips[] = implode(" - ",$r);
					}
					if($sms_res[0]===true){
						foreach($res[1][1] as $number){
							$recips[] = "SMS to ".$number;
						}
					}
					$res = implode(", ",$recips);
				} 
				
				if(is_array($sms_res)&& $sms_res[0]==false){
					return array(false,"SMS balance too low");
				}
				//$write = fwrite($handle, PHP_EOL.json_encode($res).PHP_EOL);
				break;
			case 'EMAIL':
			case 'MAIL':
			case '0':
			default:
				$res = $this->formatNote($data, "MAIL", $object);
				
				$write = fwrite($handle, PHP_EOL.$nownow.PHP_EOL."Mail:".json_encode($res).PHP_EOL);
				
				if(isset($res['NONE'])){
					$res = array("ok","No emails to be sent");
				}else if(strlen($write) == 0 || $write == false || $handle == false){
					//$res = array("info","Notifications sent successfully, but system logging stopped.");
					foreach($res['recipients']['to'] as $r){
						$recips[] = implode(" - ",$r);
					}
					$res = implode(", ",$recips);
					$res = array("info",$res);
				}else{
					foreach($res['recipients']['to'] as $r){
						$recips[] = implode(" - ",$r);
					}
					$res = implode(", ",$recips);
				} 
				break;
		}
				fclose($handle);
		return $res;
	}

}

?>