<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 *  
 * 
 * LOG TYPE:
 * 	A=Approve
 * 	C=Create
 * 	D=Delete
 * 	E=Edit
 *  F=conFirm
 * 	L=unLock
 * 	N=decliNe
 * 	R=Restore
 * 	T=deacTivate
 * 	U=Update
 *  V=actiVate
 *  
 */
 
class MASTER_LOG extends MASTER {
	private $log;
	private $log_table;
	private $field_prefix;
	private $fields;
	private $has_status;
	private $status_table;
	private $is_setup_log;
	
	private $headings;
	private $replace_headings;
	private $headObject;
	private $displayObject;
	
	const APPROVE 	= "A";
	const CREATE	= "C";
	const DELETE 	= "D";
	const EDIT 		= "E";
	const CONFIRM	= "F";
	const UNLOCK 	= "L";
	const DECLINE 	= "N";
	const RESTORE 	= "R";
	const DEACTIVATE= "T";
	const UPDATE 	= "U";
	const ACTIVATE	= "V";

	public function __construct($log_table="") {
		parent::__construct();
		if(strlen($log_table)>0) {
			$this->setLogTable($log_table);
		}
		$this->headObject = new MASTER_HEADINGS();
		$this->displayObject = new MASTER_DISPLAY();
	}
	
	public function setLogTable($log_table){
        	$this->log = $log_table;
        	$this->log_table = $log_table;
        	$this->setLogDetails();
	}
	
	/**********************************
	 * CONTROLLER functions
	 */
	public function getObject($var){
		return array($this->getAuditLogHTML($var));
		//return array("getobject",serialize($var));
	}
	public function addObject($var,$mode=false){
		if(!isset($var['insertuser'])) { $var['insertuser'] = $this->getUserID(); }
		if(!isset($var['insertdate'])) { $var['insertdate'] = date("Y-m-d H:i:s"); }
		if(isset($var['changes'])){		$var['changes'] = $this->encodeLogArray($var['changes']); }
		if(isset($var['attachment'])){		$var['attachment'] = $this->encodeLogArray($var['attachment']); }
		$data = $this->convertGenericToSpecific($var,$mode);
		//return $data;
		$insert_data = $this->convertArrayToSQL($data);
		$sql = "INSERT INTO ".$this->log_table." SET ".$insert_data;
		//return $sql;
		$result = $this->db_insert($sql);
		return $result;
	}
	
	
	
	
	/**
	 * Function to convert changes array into something that can be displayed in a table or in email
	 */
	public function processAuditLogs($type,$c,$html_tags=false) {
		//ASSIST_HELPER::arrPrint($c);	
		//echo $type."<br>";
		$changes = array();
		$this->prepAuditLogHeadings();
		$headObject = $this->headObject;
		$displayObject = $this->displayObject;
		$headings = $this->headings; //return $this->headings;
		$replace_headings = $this->replace_headings; //$this->arrPrint($replace_headings);
				foreach($c as $fld => $x) {
					//echo $x;
					if(is_array($x) && is_numeric(key($x))){
					//ASSIST_HELPER::arrPrint($x);	
						foreach($x as $num=>$dat){
							$name="";
							if($fld=="log_display_comment") {
								$name = "";
								$type = "TEXT";
							} elseif(substr($fld,0,1)=="|" && substr($fld,-1)=="|") {
								//$a = $this->replaceObjectNames(array($fld));
								$name = $fld;
								$type = "TEXT";
							} elseif(!$this->is_setup_log && isset($headings[$fld])) {
								$name = $headings[$fld]['name']." (".$num.")";
								$type = $headings[$fld]['type'];
								if($headObject->isListField($type)) {
									$type="TEXT";
								}
							} else {
								$name = ($fld!="response" && isset($headings[$fld])) ? $headings[$fld]." (".$num.")" : "!!".$fld."!!";
								$type = "TEXT";
								if($fld=="response" && (!isset($headings[$fld]) || $type=="HTML" )) {
									$name="";
								} else {
									$name = isset($headings[$fld]) ? $headings[$fld]." (".$num.")" :"!_!".$fld."!_!";
								}
							}
							//$name.=":".substr($name,1,1).":";
							if(substr($name,0,1)=="!") {
								$name = ucwords(str_ireplace("_", " ", $fld))." (".$num.")";
							}
							if($fld=="log_display_comment") {
								//if($type!="NOTIFICATION") {
									$changes[] = $dat;
								//}
							} elseif($type=="ATTACH") {
								if(is_array($dat)) {
									foreach($dat as $r) {
										$changes[] = $r;
									}
								} else {
									$changes[] = $dat;
								}
							} elseif(is_array($dat)) {
								unset($dat['raw']);
								foreach($dat as $y=>$z) {
									if(!is_array($z) && strlen($z)==0) {
										$dat[$y] = $this->getUnspecified();
									} else {
										$dat[$y] = $displayObject->getDataFieldNoJS($type, $z,array('html'=>$html_tags));
									}
								}
								if(!isset($dat['to'])){
									$dat['to'] = $this->getUnspecified();
								}
								if(!isset($dat['from'])){
									$dat['from'] = $this->getUnspecified();
								}
								if($html_tags) {
									$changes[] = "<span class=u>".$name."</span> was changed to <span class=i>".$dat['to']."</span> from <span class=i>".$dat['from']."</span>";
								} else {
									$changes[] = "".$name." was changed to ".$dat['to']." from ".$dat['from']."";
								}
							} elseif((($fld=="response") && ($name=="" || count($c)==1 || $this->log=="list")) || $fld=="status") {
								$changes[] = $dat;
							} else {
								//$changes[] = $type;
								$changes[] = $name.": ".$displayObject->getDataFieldNoJS($type, $dat,array('html'=>$html_tags));
							}
							
						}
					}else{
						$name="";
						if($fld=="log_display_comment") {
							$name = "";
							$type = "TEXT";
						} elseif(substr($fld,0,1)=="|" && substr($fld,-1)=="|") {
							//$a = $this->replaceObjectNames(array($fld));
							$name = $fld;
							$type = "TEXT";
						} elseif(!$this->is_setup_log && isset($headings[$fld])) {
							$name = $headings[$fld]['name'];
							$type = $headings[$fld]['type'];
							if($headObject->isListField($type)) {
								$type="TEXT";
							}
						} else {
							$name = ($fld!="response" && isset($headings[$fld])) ? $headings[$fld] : "!!".$fld."!!";
							$type = "TEXT";
							if($fld=="response" && (!isset($headings[$fld]) || $type=="HTML" )) {
								$name="";
							} else {
								$name = isset($headings[$fld]) ? $headings[$fld] :"!_!".$fld."!_!";
							}
						}
						//$name.=":".substr($name,1,1).":";
						if(substr($name,0,1)=="!") {
							$name = ucwords(str_ireplace("_", " ", $fld));
						}
						if($fld=="log_display_comment") {
							//if($type!="NOTIFICATION") {
								$changes[] = $x;
							//}
						} elseif($type=="ATTACH") {
							if(is_array($x)) {
								foreach($x as $r) {
									$changes[] = $r;
								}
							} else {
								$changes[] = $x;
							}
						} elseif(is_array($x)) {
							unset($x['raw']);
							foreach($x as $y=>$z) {
								if(!is_array($z) && strlen($z)==0) {
									$x[$y] = $this->getUnspecified();
								} else {
									$x[$y] = $displayObject->getDataFieldNoJS($type, $z,array('html'=>$html_tags));
								}
							}
							if(!isset($x['to'])){
								$x['to'] = $this->getUnspecified();
							}
							if(!isset($x['from'])){
								$x['from'] = $this->getUnspecified();
							}
							if($html_tags) {
								$changes[] = "<span class=u>".$name."</span> was changed to <span class=i>".$x['to']."</span> from <span class=i>".$x['from']."</span>";
							} else {
								$changes[] = "".$name." was changed to ".$x['to']." from ".$x['from']."";
							}
						} elseif((($fld=="response") && ($name=="" || count($c)==1 || $this->log=="list")) || $fld=="status") {
							$changes[] = $x;
						} else {
							//$changes[] = $type;
							$changes[] = $name.": ".$displayObject->getDataFieldNoJS($type, $x,array('html'=>$html_tags));
						}
					}
				}
				$changes = $this->replaceHeadingNames($replace_headings,$changes);
				$changes = $this->replaceAllNames($changes);	
				//$changes = $this->replaceActivityNames($changes);	
				//ASSIST_HELPER::arrPrint($changes);	
		return $changes;
	}
	
	
	/***********************************
	 * GET functions
	 */
	public function getAuditLogs($var){
		//return array("ba"=>"humbug");
		$data = $this->convertGenericToSpecific($var);
		$where = $this->convertArrayToSQL($data," AND ");
        $this->field_prefix = isset($this->field_prefix) ? $this->field_prefix :"cl_";
		if($this->has_status) {
			$sql = "SELECT L.*, IF(LENGTH(S.client_name)>0,S.client_name,S.default_name) as status FROM ".$this->log_table." L LEFT OUTER JOIN ".$this->status_table." S ON S.id = L.".$this->fields['status_id']." ".(strlen($where)>0 ? " WHERE  ".$where : "")." ORDER BY ".$this->field_prefix."id DESC";
		} else {
			$sql = "SELECT * FROM ".$this->log_table.(strlen($where)>0 ? " WHERE  ".$where : "")." ORDER BY ".$this->field_prefix."id DESC";
		}
		$result = $this->mysql_fetch_all($sql);
		//return $result;
		$c = $this->fields['changes'];		
		$d = $this->fields['insertdate'];
		$rows = array();
		foreach($result as $res){
		    $res_c = isset($res[$c]) ? $res[$c]  : "";
			$changes = $this->decodeLogArray($res_c);
			$user = isset($changes['user'])?$changes['user']:$this->getUnspecified();
			unset($changes['user']);
			if($this->has_status && !is_null($res['status'])) {
				$p = $this->fields['progress'];
				$status = $res['status'];
			} else {
				$status = "";
			}
			$res_d = isset($res[$d]) ? $res[$d] : "";
			$rows[] = array(
				'insertdate'=>date("d-M-Y H:i",strtotime($res_d)),
				'user'=>$user,
				'changes'=>$changes,
				'status'=>$status,
			);
		}
		return $rows;
	}
	public function getAuditLogHTML($var) {
		$echo = "";
		$displayObject = $this->displayObject; //new MASTER_DISPLAY();
		$rows = array();
		$rows = $this->getAuditLogs($var);
		//return $rows;
		//return "<h2>HI</h2>";
		//ASSIST_HELPER::arrPrint($rows);
		$echo.="
		<table class=tbl_audit_log>
			<tr>
				<th class=th_audit_log_date width=100px>Date</th>
				<th class=th_audit_log_user width=100px>User</th>
				<th class=th_audit_log_changes>Changes</th>
				".($this->has_status ? "<th class=th_audit_log_status width=100px>Status</th>" : "")."
			</tr>";
		if(count($rows)>0){
			foreach($rows as $r){
				$c = $r['changes'];
				$changes = $this->processAuditLogs("HTML", $c,true); //return $changes;
				//$changes = array();
                if (is_array($changes)) {
                    $echo .= "
				<tr>
					<td class=center>" . $r['insertdate'] . "</td>
					<td class=center>" . $r['user'] . "</td>
					<td>" . implode("<br />", $changes) . "</td>
					" . ($this->has_status ? "<td class=center>" . $r['status'] . "</td>" : "") . "
				</tr>
				";
                }
			}	
		} else {
			$colspan = 3+($this->has_status?1:0);
				$echo.="
				<tr>
					<td colspan=".$colspan.">No Activity Logs found.</td>
				</tr>
				";
		}
		$echo.="</table>";
		return $echo;
	}
	/***********************************
	 * SET functions
	 */
	/**********************************
	 * PROTECTED functions
	 */
	/***********************************
	 * PRIVATE functions
	 */
	/**
	 * Function to set $this->headings and $this->replace_headings for use to convert raw logs into user-readable format
	 */
	private function prepAuditLogHeadings() {
		if(count($this->headings)==0 || count($this->replace_headings)==0) {
			$headings = array();
			$replace_headings = array();
			switch($this->log){
				case "list":
                    $var_section = isset($var['section']) ? $var['section'] : "";
					$listObj = new MASTER_LIST($var_section);
					$headings = $listObj->getFieldNames();
					$replace_headings = $headings;
					break;
				case "user":
					$userObj = new MASTER_USERACCESS();
					$headings = $userObj->getUserAccessFields();
					$replace_headings = $headings;
					break;
				case "preferences":
					$prefObj = new MASTER_SETUP_PREFERENCES();
					$headings = $prefObj->getQuestionForLogs();
					$replace_headings = $headings;
					break;
				case "setup":
				    $var_section = isset($var['section']) ? $var['section'] : "";
					if($var_section=="MENU") {
						$menuObj = new MASTER_MENU();
						$headings = $menuObj->getDefaultMenuNamesForLog();
						$replace_headings = $headings;
					} elseif($var_section=="HEAD") {
						//$headObj = new MASTER_HEADINGS();
						$headings = $this->headObject->getDefaultHeadingNamesForLog();
						$replace_headings = $headings;
					}
					break;
				default:
					//$headObject = new MASTER_HEADINGS();
					$headings = $this->headObject->getHeadingsForLog();
					$replace_headings = array();
					foreach($headings as $fld=>$h) {
						$replace_headings[$fld] = $h['name'];
					}
					break;
			}
			$this->headings = $headings;
			$this->replace_headings = $replace_headings;
		}		
	}
	/**
	 * Encode given array field for storage in database
	 */
	private function encodeLogArray($a) {
		if(!isset($a['user'])){
			$a['user'] = $this->getUserName();
		}
		return base64_encode(serialize($a));
	} 
	/**
	 * Decode given array from the database for processing
	 */
	private function decodeLogArray($a) {
		return unserialize(base64_decode($a));
	} 
	/**
	 * Convert generic field names to log specific field names
	 */
	public function convertGenericToSpecific($var,$mode=false){
		$data = array();
		if($mode === false){
			$data = array();
			foreach($var as $key => $x) {
				if(isset($this->fields[$key])) {
					$data[$this->fields[$key]] = $x;
				} else {
					$data[$key] = $x;
				}
			}
			return $data;
		}
		if(is_array($var)){
			if(count($var)>0){
				if($mode){$var['changes'] = $this->decodeLogArray($var['changes']);}
				//return array("error",$var);
					if(isset($var['changes']['duplicates']) && is_array($var['changes']['duplicates'])){
						foreach($var['changes']['duplicates'] as $a=>$b){
							unset($var['changes']['duplicates']['customer_c_certificate']);
							foreach($b as $key=>$x){
								if(isset($this->fields[$key])) {
									$new_head = $this->fields[$key]." ".$a;
									$tmp[$new_head] = $x;
								} else {
									$tmp[$key][$a] = $x;
								}
							}
						}
						unset($var['changes']);
						$var['changes'] = $this->encodeLogArray($tmp);
						//$var['changes']=$tmp;
						$data = $var;
						//return array("error",$data);
						//$data = $this->encodeLogArray($data);
					}
					foreach($var as $key => $x) {
						if(isset($this->fields[$key])) {
							$data[$this->fields[$key]] = $x;
							if($mode){
								unset($data[$key]);
							}
						} else {
							if(!$mode){
								$data[$key] = $x;
							}
						}
					}
					
					if(!isset($var['changes']['duplicates'])){$data['cl_changes']=$this->encodeLogArray($data);}
			}else{
				$data = array();
			}
		}else{
			$data = array();
		}
			
		return $data;
	}
		/**
		 * Set the details of the log table
		 */
	private function setLogDetails(){
		$this->is_setup_log = false;
		switch($this->log_table){
			case "setup":
			case "list":
			case "preferences":
			case "notifications":
			case "user":
			case $this->getDBRef()."_setup_log":
				$this->log_table = $this->getDBRef()."_setup_log";
				$this->has_status = false;
				$this->field_prefix = "sl_";
				$this->fields = array(
					'ref'		=> $this->field_prefix."id",
					'section'	=> $this->field_prefix."section",
					'object_id'	=> $this->field_prefix."item_id",
					'insertuser'=> $this->field_prefix."insertuser",
					'insertdate'=> $this->field_prefix."insertdate",
					'changes'	=> $this->field_prefix."changes",
					'log_type'	=> $this->field_prefix."log_type",
				);
				$this->is_setup_log = true;
				break;	
			case "userprofile":
			case $this->getDBRef()."_userprofile_log":
				$this->log_table = $this->getDBRef()."_userprofile_log";
				$this->has_status = false;
				$this->field_prefix = "upl_";
				$this->fields = array(
					'ref'		=> $this->field_prefix."id", 
					'section'	=> $this->field_prefix."section",
					'object_id'	=> $this->field_prefix."item_id",
					'insertuser'=> $this->field_prefix."insertuser",
					'insertdate'=> $this->field_prefix."insertdate",
					'changes'	=> $this->field_prefix."changes",
					'log_type'	=> $this->field_prefix."log_type",
				);
				break;	
			case "contract":
			case $this->getDBRef()."_contract_log":
				$this->log_table = $this->getDBRef()."_contract_log";
				$this->has_status = true;
				$this->status_table = $this->getDBRef()."_list_customer_status";
				$this->field_prefix = "cl_";
				$this->fields = array(
					'ref'		=> $this->field_prefix."id",
					'object_id'	=> $this->field_prefix."contract_id",
					'insertuser'=> $this->field_prefix."insertuser",
					'insertdate'=> $this->field_prefix."insertdate",
					'changes'	=> $this->field_prefix."changes",
					'progress'	=> $this->field_prefix."progress",
					'status_id'	=> $this->field_prefix."status_id",
					'attachment'=> $this->field_prefix."attachment",
					'log_type'	=> $this->field_prefix."log_type",
				);
				break;	
			case "action":
			case $this->getDBRef()."_action_log":
				$this->log_table = $this->getDBRef()."_action_log";
				$this->has_status = true;
				$this->status_table = $this->getDBRef()."_list_action_status";
				$this->field_prefix = "al_";
				$this->fields = array(
					'ref'		=> $this->field_prefix."id",
					'object_id'	=> $this->field_prefix."action_id",
					'insertuser'=> $this->field_prefix."insertuser",
					'insertdate'=> $this->field_prefix."insertdate",
					'changes'	=> $this->field_prefix."changes",
					'progress'	=> $this->field_prefix."progress",
					'status_id'	=> $this->field_prefix."status_id",
					'attachment'=> $this->field_prefix."attachment",
					'log_type'	=> $this->field_prefix."log_type",
				);
				break;	
			case "assurance":
			case $this->getDBRef()."_assurance_log":
				$this->log_table = $this->getDBRef()."_assurance_log";
				$this->has_status = false;
				$this->field_prefix = "asl_";
				$this->fields = array(
					'ref'		=> $this->field_prefix."id",
					'section'	=> $this->field_prefix."section",
					'object_id'	=> $this->field_prefix."item_id",
					'changes'	=> $this->field_prefix."changes",
					'insertuser'=> $this->field_prefix."insertuser",
					'insertdate'=> $this->field_prefix."insertdate",
					'attachment'=> $this->field_prefix."attachment",
					'log_type'	=> $this->field_prefix."log_type",
				);
				break;	
			case "finance":
			case $this->getDBRef()."_finance_log":
				$this->log_table = $this->getDBRef()."_finance_log";
				$this->has_status = false;
				$this->field_prefix = "fl_";
				$this->fields = array(
					'ref'		=> $this->field_prefix."id",
					'section'	=> $this->field_prefix."section",
					'object_id'	=> $this->field_prefix."item_id",
					'changes'	=> $this->field_prefix."changes",
					'insertuser'=> $this->field_prefix."insertuser",
					'insertdate'=> $this->field_prefix."insertdate",
					'log_type'	=> $this->field_prefix."log_type",
				);
				break;	
		}
	}



}
?>