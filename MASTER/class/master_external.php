<?php
/**
 *
 * Created on: 9 October 2015
 * Authors: Duncan Cosser <duncan@igniteassist.co.za>
 * Owner: Action Assist <Action iT>
 *
 */

class MASTER_EXTERNAL extends ASSIST_DB {

    const TABLE = "list_type";
	const TABLE_FIELD = "value";

    protected $modref;

    /*************
     * CONSTANTS
     */

    public function __construct($mr="") {
        parent::__construct();           //construct assist_db class
        if(strlen($mr)>0) {              //if a modref has been fed in
              $this->setDBRef($mr);      //change the default dbref
              $this->modref = strtoupper($mr);
        }
    }


	/***********************************
	 * CONTROLLER functions
	 */
	public function editObject($var) {
		$section = $var['section'];
		unset($var['section']);
		$mar = $this->editNames($section,$var);
		if($mar>0) {
			if($section=="object_names"){
				return array("ok","Object names updated successfully.");
			} else {
				return array("ok","Activity names updated successfully.");
			}
		} else {
			return array("info","No changes were found to be saved.");
		}
		return array("error",$mar);
	}


    /*******************
     * GET functions
     * */
    public function getTableName() { return "assist_".$this->getCmpCode()."_mdoc_".self::TABLE; }

    public function getTablePrefix() { return $this->getDBRef(); }

    public function getObjectTypeID($id){
        $sql = "SELECT ".self::TABLE_FIELD." FROM ".$this->getTableName()." WHERE id = '$id'";
        return $this->mysql_fetch_one_value($sql, self::TABLE_FIELD);
    }

	protected function getFormattedNames($type) {
		$names = array();
		$sql = "SELECT name_section as section, IF(LENGTH(name_client)>0,name_client,name_default) as name
				FROM ".$this->getTableName()."
				WHERE (name_status & ".$type." = ".$type.")";
		$rows = $this->mysql_fetch_all($sql);
		foreach($rows as $r){
			$s = explode("_",$r['section']);
			$names[end($s)] = $r['name'];
		}
		return $names;
	}

    public function getListForExternalModule($is_active_only=true){

		$sql = "SELECT customer_id as id
				, CONCAT(customer_p_name,' ',customer_p_surname) as person_name
				, customer_o_name as org_name
				FROM ".$this->getTablePrefix()."_customers
				".($is_active_only?"WHERE customer_user_status = ".MASTER::ACTIVE."":"");
		$rows = $this->mysql_fetch_all($sql);
		$options = array();
		foreach($rows as $r) {
			if(strlen($r['org_name'])>0) {
				$o = $r['org_name'];
				if(strlen($r['person_name'])>0) {
					$o.=" (".$r['person_name'].")";
				}
			} else {
				$o = $r['person_name'];
			}
			$options[$r['id']] = $o;
		}
    	asort($options);
		/* REMOVED ON 29 AUGUST 2017 BY JC
        $sql = "SELECT customer_id as id,
                    IF(customer_type = 1, CONCAT(IFNULL(CONCAT(pt.value,' '),''),customer_p_name,' ',customer_p_surname), customer_o_name) as name
                    FROM ".$this->getTablePrefix()."_customers
                    LEFT JOIN assist_".$this->getCmpCode()."_list_ppltitle pt
					ON customer_p_prefix = pt.id
			WHERE customer_user_status = ".MASTER::ACTIVE."
ORDER BY
IF(customer_type = 1, customer_p_name, customer_o_name)
,IF(customer_type = 1, customer_p_surname, customer_o_name)";
        $options = $this->mysql_fetch_value_by_id($sql, "id","name");
        //ASSIST_HELPER::arrPrint($options);*/
        return $options;
    }

    public function getObjectNameForExternalModule($id){
        $sql = "SELECT customer_id as id,
                    IF(customer_type = 1, CONCAT(customer_p_prefix,' ',customer_p_name,' ',customer_p_surname), customer_o_name) as name
                    FROM ".$this->getTablePrefix()."_customers WHERE customer_user_status = ".MASTER::ACTIVE." AND customer_id = '".$id."'";
        $options = $this->mysql_fetch_one_value($sql,"name");
        if(strlen($options)>0){
            return $options;
        }else{
            return ASSIST_HELPER::UNSPECIFIED;
        }
        //ASSIST_HELPER::arrPrint($options);
    }

   /**
    * Get the object names from the table in easy readable format
    */
	public function fetchObjectNames() {
		return $this->getFormattedNames(MASTER_NAMES::OBJECT_HEADING);
	}
   /**
    * Get the object names from the table in easy readable format
    */
	public function fetchActivityNames() {
		return $this->getFormattedNames(MASTER_NAMES::ACTIVITY_HEADING);
	}





	protected function getNamesFromDB($type,$sections=array()){
		$sql = "SELECT name_id as id, name_section as section, name_default as mdefault, name_client as client
				FROM ".$this->getTableName()."
				WHERE (name_status & ".$type." = ".$type.")"
				.(count($sections)>0 ? " AND name_section IN ('".implode("','",$sections)."')" : "");
		$rows = $this->mysql_fetch_all_by_id($sql,"section");
		return $rows;
		//return array($sql);
	}

	/**
	 * Get the object names from the table for editing in Setup
	 */
	public function getObjectNamesFromDB($sections=array()){
		return $this->getNamesFromDB(MASTER_NAMES::OBJECT_HEADING,$sections);
	}
	/**
	 * Get the activity names from the table for editing in Setup
	 */
	public function getActivityNamesFromDB($sections=array()){
		return $this->getNamesFromDB(MASTER_NAMES::ACTIVITY_HEADING,$sections);
	}


	 /**
	 * Get the URLs to the Tasks which are referenced by a particular MASTER object
	  * @param ID (int) ID number of the object you're checking for
	 */
	public function getLinksToTasks($objID){
		$help = new ASSIST_MODULE_HELPER();
		$mods = $help->getAllMenuModules();
		foreach($mods as $key=>$val){
			if($help->isMasterLinkModule($val['modlocation'])){
				$checkMods[$key]=$val;
			}
		}
		//ASSIST_HELPER::arrPrint($checkMods);
		if(count($checkMods)>0){
			foreach($checkMods as $key=>$val){
				if($val['modlocation']=="ACTION"){
					//Temporary soultion for old ACTION module, not using classes but fetching directly
					$sql = "SELECT * FROM assist_".$this->getCmpCode()."_udf udf
						INNER JOIN assist_".$this->getCmpCode()."_udfindex ind
						ON udf.udfindex = ind.udfiid
						WHERE udf.udfref = '".$val['modref']."' ".		//this line makes the search only run through items in "TASK"
						"AND ind.udfilist = '".$this->getModRef()."'
						AND udf.udfvalue = $objID
					";
					$response = array();
					//echo $sql;
					//udfid, udfindex, udfvalue
					//AND udf.udfnum = 1
					$items = $this->mysql_fetch_all($sql);
					//return array($objID,$this->getModRef(),$checkMods,$items);
					foreach($items as $a=>$b){
						$taskSQL = "SELECT taskaction, taskdeadline, taskadduser
							FROM assist_".$this->getCmpCode()."_task_task
							WHERE taskid = ".$b['udfnum'];
						$row = $this->mysql_fetch_one($taskSQL);
						if (count($row) > 0) { //[SD] 16 June 2021 - spotted during standardisation. If there are no tasks don't display data.
                            $deadline_date = date("d-M-Y", $row['taskdeadline']);
                            $response[] = array('module' => $b['udfref'],
                                'object_id' => $b['udfnum'],
                                'object_name' => $row['taskaction'],
                                'object_deadline' => $deadline_date !== "01-Jan-1970" ? $deadline_date : "00-00-0000",
                                'object_deadline_raw' => $row['taskdeadline'],
                                'object_adder' => $help->getAUserName($row['taskadduser'])
                            );        //"extra"=>$row
                        }
					}
					return $response;
				}
			}

		}else{
			return array();
		}
	}




    /****************************************
     * SET / UPDATE Functions
     */

   public function makeLinkToTask($module,$taskID,$value){
		$this->setDBRef($module);
		$this->modref = strtoupper($module);
   }








    /**********************************************
     * PROTECTED functions: functions available for use in class heirarchy
     */











     /***********************
     * PRIVATE functions: functions only for use within the class
     */



    /**
	 * Function to edit object name / menu heading
	 */
	 private function editNames($section,$var) {
    	if($section=="object_names") {
	   		$original_names = $this->getObjectNamesFromDB(array_keys($var));
	   		$log_section = "OBJECT";
			$field = "name_section";
    	} else {
    		$original_names = $this->getActivityNamesFromDB(array_keys($var));
			$log_section = "ACTIVITY";
			$field = "name_section";
    	}
    	$mar = 0;
    	$sql_start = "UPDATE ".$this->getTableName()." SET name_client = '";
		$sql_mid = "' WHERE $field = '";
		$sql_end = "' LIMIT 1";
		$c = array();
    	foreach($var as $section => $val){
    		$m = 0;
			$to = (strlen($val)>0 ? $val : $original_names[$section]['mdefault']);
			$from = $original_names[$section]['client'];
			if($to!=$from) {
    			$sql = $sql_start.$val.$sql_mid.$section.$sql_end;
				$m=$this->db_update($sql);
				if($m>0){
					if($menu_section=="object_names") {
						$s = explode("_",$section);
						$key = "|".end($s)."|";
					} else {
						$s = explode("_",$section);
						$key = "|".end($s)."|";
					}
					$c[$key] = array('to'=>$to,'from'=>$from);
					$mar+=$m;
				}
			}
    	}
		if($mar>0 && count($c)>0) {
			$changes = array_merge(array('user'=>$this->getUserName()),$c);
			$log_var = array(
				'section'	=> $log_section,
				'object_id'	=> $original_names[$section]['id'],
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::EDIT,
			);
			$me = new MASTER();
			$me->addActivityLog("setup", $log_var);
		}

		return $mar;
    }



    public function __destruct() {
    	parent::__destruct();
    }



}


?>