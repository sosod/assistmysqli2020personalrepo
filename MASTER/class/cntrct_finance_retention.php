<?php

class CNTRCT_FINANCE_RETENTION extends CNTRCT_FINANCE {
	
	//private $table_name = "_finance_retention";
	//private $field_prefix = "fr";

	protected $object_type = "FINANCE_RETENTION";
	protected $ref_tag = "CFR";

	protected $table_name = "_finance_retention";
	protected $field_prefix = "fr";
	protected $id_field = "_id";
	protected $parent_field = "_contract_id";
	protected $status_field = "_status";
	protected $primary_finance_field = "_retention_paid";
	protected $secondary_finance_field = "";
	protected $extra_finance_field = "";
	
	protected $del_table_name = "_deliverable_retention";
	protected $del_field_prefix = "dr";
	protected $del_parent_field = "_fr_id";
	protected $del_status_field = "_status";
	protected $del_primary_finance_field = "_amount";
	protected $del_secondary_finance_field = "";
	protected $del_extra_finance_field = "";
	
	protected $js = "";
	
	const LOG_TABLE = "finance";
	
	public function __construct() {
		parent::__construct();
	}
	
	
	
	
	/********************
	 * CONTROLLER Functions
	 */
	public function getObject() {
		return array("error","function not programmed");
	}
	public function addObject($var) {
		//return array("error","function not programmed");
		return $this->addMyObject($var);
	}
	public function editObject() {
		return array("error","function not programmed");
	}
	public function deleteObject() {
		return array("error","function not programmed");
	}
	
	
	
	
	
	
	
	
	
	/***************************
	 * FINANCE functions
	 */

	
	/** 
	 * Function to determine the data to be displayed in the Finance Summary table
	 * @param (INT) $parent_object_id = Contract ID
	 * 
	 * @return (Array)  ('head'=>array of headings,'rows'=>array of values)
	 */
	public function getSummaryData($parent_object_id) {
		$data = array('head'=>array(),'rows'=>array());
		
		$headings = array(
			'total'=>array(
				'name'=>"Total Retention",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'deliverables'=>array(
				'name'=>"Retention Allocated to Deliverables",
				'type'=>"CURRENCY",
				'format'=>"DEFAULT",
			),
			'unallocated'=>array(
				'name'=>"Unallocated Retention",
				'type'=>"CURRENCY",
				'format'=>"TOTAL",
			),
		);
		
		$data['head'] = $headings;
		
		
		$total = $this->getRawRows($parent_object_id,"SUMMARY");
		$data['rows']['total'] = $total[0][$this->getPrimaryFinanceFieldName()];
		
		$deliverable_total = $this->getDeliverableRawRows($parent_object_id,"SUMMARY");
		$data['rows']['deliverables'] = $deliverable_total[0][$this->getDeliverablePrimaryFinanceFieldName()];
		$data['rows']['unallocated'] = $data['rows']['total'] - $data['rows']['deliverables'];
		
		
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	public function __destruct() {
		parent::__destruct();
	}
}




?>