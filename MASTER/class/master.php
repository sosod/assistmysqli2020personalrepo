<?php
/**
 * To manage any centralised variables and functions and to create a bridge to the centralised ASSIST classes
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class MASTER extends MASTER_HELPER {
    /***
     * Module Wide variables
     */	
	protected $object_form_extra_js = "";

	protected $copy_function_protected_heading_types = array("ATTACH","DEL_TYPE");
	
	protected $has_target = false;
	
    /**
     * Module Wide Constants
     * */
    const SYSTEM_DEFAULT = 1;
    const ACTIVE = 2;
    const INACTIVE = 4;
    const DELETED = 8;
	
	const AWAITING_APPROVAL = 16;
	const APPROVED = 128;
	
	
	/**********************
	 * CONSTRUCTOR
	 */
	public function __construct() {
		parent::__construct();
	}



	/*
	public function getObjectName($o,$plural=false) {
    	$x = explode("_",$o);
		if(count($x)>1 && $x[0]=="TEMPLATE"){
			$o=$x[1];
		}	
		if(isset($this->object_names[strtolower($o)])){
	    	return $this->object_names[strtolower($o)].($plural?"s":""); 
		}else{
	    	return $o;	
		}
	}

	public function getContractObjectName($plural=false) { return $this->getObjectName("customer").($plural?"s":""); }

	*/
	
	
/*********************
 * MODULE OBJECT functions
 * for CONTRACT, DELIVERABLE AND ACTION
 */
	public function getObject($var) {
		//$this->arrPrint($var);
		switch($var['type']) {
			case "LIST":
			case "FILTER":
			case "ALL":
				$options = $var;
				unset($options['section']);
				unset($options['type']);
				//unset($options['page']);
				$data = $this->getList($var['section'],$options);
				if($var['type']=="LIST" || $var['type']=="ALL") {
					return $data;
				} else {
					return $this->formatRowsForSelect($data,$this->getNameFieldName());
				}
				break;
			case "DETAILS":
				if(count($this->object_details)>0) {
					return $this->object_details;
				} else {
					unset($var['type']);
					return $this->getAObject($var['id'],$var);
				}
				break;
			case "EMAIL":
				//unset($var['type']);
				return $this->getAObject($var['id'],$var);
				break;
			case "FRONTPAGE_STATS":
				unset($var['type']);
				return $this->getDashboardStats($var);
				break;
			case "FRONTPAGE_TABLE":
				unset($var['type']);
				return $this->getDashboardTable($var);
				break;
		}
	}

	public function hasTarget() {
		return $this->has_target;
	}
	public function getStatusFieldName() {
		return $this->status_field;
	}
	public function getIDFieldName() {
		return $this->id_field;
	}
	public function getParentFieldName() {
		return $this->parent_field;
	}
	public function getNameFieldName() {
		return $this->name_field;
	}
	public function getTypeFieldName() {
		return $this->type_field;
	}
	public function getAttachmentFieldName() {
		return $this->attachment_field;
	}
	public function getUpdateAttachmentFieldName() {
		return $this->update_attachment_field;
	}
	public function getCopyProtectedHeadingTypes() {
		return $this->copy_function_protected_heading_types;
	}
	
	public function getPageLimit() {
		$profileObject = new MASTER_PROFILE();
		return $profileObject->getProfileTableLength();
	}
	
	public function getDateOptions($fld) {
		if(stripos($fld,"action_on")!==FALSE) { $fld = "action_on"; }
		switch($fld) {
			case "action_on":
				return array('maxDate'=>"'+0D'");
				break;
			default:
				return array();
		}
	}
	
	public function getExtraObjectFormJS() { return $this->object_form_extra_js; }

	public function getAllDeliverableTypes() { return $this->deliverable_types; }
	
	public function getAllDeliverableTypesForGloss() {
		$types = $this->deliverable_types;
		foreach($types as $key=>$t){
			$arr[]=$t;
		}
		return $arr;
	}
	
	public function getDeliverableTypes($contract_id) {
		$contractObject = new MASTER_CONTRACT($contract_id);
		if($contractObject->doIHaveSubDeliverables()==FALSE) {
			unset($this->deliverable_types['SUB']);
			unset($this->deliverable_types['MAIN']);
		}
		return $this->deliverable_types; 
	}
	public function getDeliverableTypesForLog($ids) {
		foreach($this->deliverable_types as $dt => $d) {
			if(!in_array($dt,$ids)) {
				unset($this->deliverable_types[$dt]);
			}
		}
		return $this->deliverable_types;
	}

	public function getDashboardStats($options) {
		$deadline_name = $this->getDeadlineFieldName();
		$options['page'] = "LOGIN_STATS";
		$data = $this->getList("MANAGE",$options);
		//$this->arrPrint($data);
		$result = array(
			'past'=>0,
			'present'=>0,
			'future'=>0,
		);
		$now = strtotime(date("d F Y")); //echo "<p>".$now;
		foreach($data['rows'] as $x) {
			$d = $x[$deadline_name]['display'];
			$z = strtotime($d);
			//echo "<br />".$z."=".$d."=";
			if($z<$now) {
				$result['past']++; //echo "past";
			} elseif($z>$now) {
				$result['future']++; //echo "future";
			} else {
				$result['present']++; //echo "present";
			}
		}
		return $result;
		//return $data;
	}

	public function getDashboardTable($options) {
		$options['page'] = "LOGIN_TABLE";
		$data = $this->getList("MANAGE",$options);
		return $data;
	}

	public function getObjectNameForMdoc($obj_type,$obj_id){
		$cObject = new MASTER_CONTRACT(); 
		$c_tblref = "C";
		$c_table = $cObject->getDeepTableName();
		$c_id = $c_tblref.".".$cObject->getIDFieldName();
		$c_status = $c_tblref.".".$cObject->getStatusFieldName();
		$c_name = $cObject->getNameFieldName();
		$c_field = $c_tblref.".".$cObject->getTableField();
		$from = $c_table." ".$c_tblref." ";
		$sql = "SELECT customer_type, customer_p_name, customer_p_surname, customer_o_name FROM $from WHERE $c_id = '$obj_id'";
		$res = $this->mysql_fetch_one($sql);
		if($res['customer_type'] == 1){
			return $res['customer_p_name']." ".$res['customer_p_surname'];
		}else{
			return $res['customer_o_name'];
		}
	}
	
	public function getMyList($obj_type,$section,$options=array()) {
		//echo "<P>section: ".$obj_type;
		//echo "<P>options: "; $this->arrPrint($options);
		if(isset($options['only_new'])) {
			$only_new = $options['only_new'];
			unset($options['only_new']);
		} else {
			$only_new = false;
		}

		if(isset($options['inactive_too'])) {
			$inactive_too = true;
			unset($options['inactive_too']);
		} else {
			$inactive_too = false;
		}
		
		if(isset($options['page'])) {
			$page = strtoupper($options['page']);
			unset($options['page']);
		} else {
			$page = "DEFAULT";
		}	
		$trigger_rows = true; 
		if(isset($options['trigger'])) {
			$trigger_rows = $options['trigger'];
			unset($options['trigger']);
		}
		//echo $page;
		if($page=="LOGIN_STATS" || $page=="LOGIN_TABLE") {
			$future_deadline = $options['deadline'];
			unset($options['deadline']);
			if(!isset($options['limit'])) { $options['limit'] = false; }
		}
		$compact_details = ($page == "CONFIRM" || $page == "ALL");
		if(isset($options['limit']) && ($options['limit'] === false || $options['limit']=="ALL")){
			$pagelimit = false;
			$start = 0;
			$limit = false;
			unset($options['start']);
			unset($options['limit']);
		}else{
			$pagelimit = $this->getPageLimit();
			$start = isset($options['start']) ? $options['start'] : 0;  unset($options['start']);
			$limit = isset($options['limit']) ? $options['limit'] : $pagelimit;	unset($options['limit']);
			if($start<0) {
				$start = 0;
				$limit = false;
			}
		}	
		$order_by = ""; //still to be processed
		$left_joins = array();
		
		$headObject = new MASTER_HEADINGS();
		$final_data = array('head'=>array(),'rows'=>array());
		
		//set up contract variables
		//This code asks the headings object for the appropriate headings
		$c_headings = $headObject->getMainObjectHeadings("CONTRACT",($obj_type=="CONTRACT"?"LIST":"FULL"),$section);
		//$c_dump = array("CONTRACT",($obj_type=="CONTRACT"?"LIST":"FULL"),$section); 
//ASSIST_HELPER::arrPrint($c_dump);
//ASSIST_HELPER::arrPrint($c_headings);
//return "HI"; 
		$cObject = new MASTER_CONTRACT(); 
		$c_tblref = "C";
		$c_table = $cObject->getTableName();
		$c_id = $c_tblref.".".$cObject->getIDFieldName();
		$c_status = $c_tblref.".".$cObject->getStatusFieldName();
		$c_name = $cObject->getNameFieldName();
		$c_field = $c_tblref.".".$cObject->getTableField();
		$from = $c_table." ".$c_tblref." ";
		if($only_new){
			$where = $cObject->getActivationStatusSQL($c_tblref);
		}else if($inactive_too){
			$where = $cObject->getBothStatusSQL($c_tblref);
		}else{
			$where = $cObject->getActiveStatusSQL($c_tblref);
		}
		//$where = $only_new ? $cObject->getActivationStatusSQL($c_tblref):$cObject->getActiveStatusSQL($c_tblref);
		//echo $where;
		$id_fld = $cObject->getIDFieldName();
		$sql = "SELECT DISTINCT $c_status as my_status";
		$final_data['head'] = $c_headings;
		$where_tblref = $c_tblref;
		$ref_tag = $cObject->getRefTag();
		$where_status_fld = $c_tblref.".".$cObject->getStatusFieldName();
		$group_by = " GROUP BY ".$c_id;
		if(count($options)>0) {
			foreach($options as $key => $filter) {
				if(substr($key,0,3)!=substr($this->getTableField(),0,3) && strrpos($key,".")===FALSE) {
					$key = $this->getTableField()."_".$key;
				}
				$where.= " AND ".(strrpos($key,".")===FALSE ? $where_tblref."." : "").$key." = '".$filter."' ";
			}
		}
		 
		
			$all_headings = $c_headings; //ASSIST_HELPER::arrPrint($all_headings);
			
			$listObject = new MASTER_LIST();
			
			//$list_headings = $headObject->getAllListHeadings();
			//$list_tables = $listObject->getAllListTables(array_keys($list_headings));
		
			
			foreach($all_headings as $fld => $head) {
				$lj_tblref = substr($head['section'],0,1);
				if($head['type']=="LIST") {
					$listObject->changeListType($head['list_table']);
					//echo "<h2>".$head['list_table']."</h2>";
					//ASSIST_HELPER::arrPrint($listObject->getFieldNames());
					$sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
					$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])." 
										AS ".$head['list_table']." 
										ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld." 
										".($listObject->hasStatus() ? " AND (".$head['list_table'].".status & ".MASTER::DELETED.") <> ".MASTER::DELETED : "");
				} elseif($head['type']=="MASTER") {
					$tbl = strtoupper($head['list_table']);
					//$masterObject = new MASTER_MASTER($fld);
					$masterObject = new $tbl();
					$tbl = $head['list_table'];
					$sql.= ", ".$fld.".".$masterObject->getFld("NAME")." as ".$tbl;
					$left_joins[] = "LEFT OUTER JOIN ".$masterObject->getTable()." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$masterObject->getFld("ID");
				} elseif($head['type']=="USER") {
					unset($c_headings['customer_type']);
					$sql.= ", ".implode(", ",array_keys($c_headings)).", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
					$left_joins[] = "LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
				}
			}
			$sql.= " FROM ".$from.implode(" ",$left_joins);
			$sql.= " WHERE ".$where." ";
			$sql.= isset($group_by) && strlen($group_by)>0 ? $group_by : "";
//$sql.=" ORDER BY
//IF(customer_type = 1, CONCAT(customer_p_name,' ',customer_p_surname), customer_o_name)
//";

			//echo $where;
			
			$mnr = $this->db_get_num_rows($sql);
		if($trigger_rows==true) {
			$start = ($start!=false && is_numeric($start) ? $start : "0");	
			$sql.=($limit !== false?" LIMIT ".$start." , $limit ":"");
	
			$rows = $this->mysql_fetch_all_by_id($sql,$id_fld);
			$final_data = $this->formatRowDisplay($mnr,$rows, $final_data,$id_fld,$headObject,$ref_tag,"",array('limit'=>$limit,'pagelimit'=>$pagelimit,'start'=>$start));
	
		} else {
			$final_data['rows'] = array();
		}
//ASSIST_HELPER::arrPrint($rows);
//$dump = array($mnr,$rows, $final_data,$id_fld,$headObject,$ref_tag,"",array('limit'=>$limit,'pagelimit'=>$pagelimit,'start'=>$start));
//ASSIST_HELPER::arrPrint($dump);
																//echo $sql;
//echo "<h3>FINAL DATA:</h3>";
//ASSIST_HELPER::arrPrint($all_headings);

		return $final_data;
	}
	function formatRowDisplay($mnr,$rows,$final_data,$id_fld,$headObject,$ref_tag,$status_id_fld,$paging) {
		$limit = $paging['limit'];
		$pagelimit = $paging['pagelimit'];
		$start = $paging['start'];
			//ASSIST_HELPER::arrPrint($rows);
			//ASSIST_HELPER::arrPrint($final_data['head']);
			$keys = array_keys($rows);
			$displayObject = new MASTER_DISPLAY();
			foreach($rows as $r) {
				$row = array();
				$i = $r[$id_fld];
				foreach($final_data['head'] as $fld=> $head) {
					if($head['parent_id']==0){
						if($this->isDateField($fld)) {
							$row[$fld] = $displayObject->getDataField("DATE", $r[$fld],array('include_time'=>false));
						} else if($headObject->isListField($head['type'])){
							if(is_null($r[$fld]) ||$r[$fld]=='0'){
								$row[$fld]=$this->getUnspecified();
							}else{
								$row[$fld]=$r[$head['list_table']];
							}	
						} else {
							//$row[$fld] = $r[$fld];
							$row[$fld] = $displayObject->getDataField($head['type'], $r[$fld],array('right'=>true,'html'=>true,'reftag'=>$ref_tag));
						}
					}
				}
				$final_data['rows'][$i] = $row;
				//$mnr++;
			}
		
			if($mnr==0 || $limit === false) {
				$totalpages = 1;
				$currentpage = 1;
			} else {
				$totalpages = round(($mnr/$pagelimit),0);
				$totalpages += (($totalpages*$pagelimit)>=$mnr ? 0 : 1);
				if($start==0) {
					$currentpage = 1;
				} else {
					$currentpage = round($start/$pagelimit,0);
					$currentpage += (($currentpage*$pagelimit)>$start ? 0 : 1);
				}
			}
			$final_data['paging'] = array(
				'totalrows'=>$mnr,
				'totalpages'=>$totalpages,
				'currentpage'=>$currentpage,
				'pagelimit'=>$pagelimit,
				'first'=>($start==0 ? false : 0),
				'prev'=>($start==0 ? false : ($start-$pagelimit)),
				'next'=>($totalpages==$currentpage ? false : ($start+$pagelimit)),
				'last'=>($currentpage==$totalpages ? false : ($totalpages-1)*$pagelimit),
			);
		return $final_data;
	}


	public function getDetailedObject($obj_type,$id,$options=array()) {
		//ASSIST_HELPER::arrPrint($options);
		$left_joins = array();
		
		if(isset($options['compact_view'])) {
			$compact_view = $options['compact_view'];
		} else {
			$compact_view = false;
		}
		
		if(isset($options['type'])) {
			if($options['type']=="EMAIL"){
				$format_for_emails = true;
			}
			unset($options['type']);
		} else {
			$format_for_emails = false;
		}
		if(isset($options['attachment_buttons'])) {
			$attachment_buttons = $options['attachment_buttons'];
			unset($options['attachment_buttons']);
		} else {
			$attachment_buttons = true;
		}
		
		$headObject = new MASTER_HEADINGS();
		$final_data = array('head'=>array(),'rows'=>array());
		
		//set up contract variables
		$c_headings = $headObject->getMainObjectHeadings("CONTRACT",($compact_view==true ? "COMPACT" : "DETAILS")); 
		$all_headings = $c_headings['rows'];
		$cObject = new MASTER_CONTRACT(); 
		$c_tblref = "C";
		$c_table = $cObject->getTableName();
		$c_id = $c_tblref.".".$cObject->getIDFieldName();
		$c_id_fld = $cObject->getIDFieldName();
		$c_status = $c_tblref.".".$cObject->getStatusFieldName();
		$c_name = $cObject->getNameFieldName();
		$c_manager = $cObject->getManagerFieldName();
		$c_field = $c_tblref.".".$cObject->getTableField();
		
		$where = $cObject->getActiveStatusSQL($c_tblref);
			$id_fld = $cObject->getIDFieldName();
			$a_headings = array('rows'=>array());
			$d_headings = array('rows'=>array());
			if($format_for_emails) {
				$final_data['head'][$id_fld] = $d_headings['rows'][$id_fld];
				$final_data['head'][$c_name] = $c_headings['rows'][$c_name];
				$final_data['head'][$c_id_fld] = $c_headings['rows'][$c_id_fld];
				$final_data['head'][$c_manager] = $c_headings['rows'][$c_manager];
				$sql ="SELECT ".$c_tblref.".".$id_fld." as id 
						, CONCAT('".$cObject->getRefTag()."',".$c_id.") as ".$c_id_fld."
						, CONCAT(".$c_tblref.".".$c_name.",' [".$cObject->getRefTag()."',".$c_id.",']') as ".$c_name."
						, ".$c_tblref.".".$c_manager."";
			} else {
				$sql = "SELECT ".$c_tblref.".*";
				$a_headings = array('rows'=>array());
				$final_data['head'] = $c_headings['rows'];
				$group_by = " GROUP BY ".$c_tblref.".".$id_fld;
			}
			$where_tblref = $c_tblref;
			$ref_tag = $cObject->getRefTag();
		
		$where.= " AND ".$where_tblref.".".$id_fld." = ".$id;
		//echo $where;
		
		//return $d_headings;
		$listObject = new MASTER_LIST();
		
		//$list_headings = $headObject->getAllListHeadings();
		//$list_tables = $listObject->getAllListTables(array_keys($list_headings));
		
		foreach($all_headings as $fld => $head) {
			$lj_tblref = substr($head['section'],0,1);
			if($head['type']=="LIST" && $head['parent_id']==0) { 
				$listObject->changeListType($head['list_table']);  
				$sql.= ", ".$listObject->getSQLName($head['list_table'])." as ".$head['list_table'];
				$left_joins[] = "LEFT OUTER JOIN ".$listObject->getListTable($head['list_table'])." 
									AS ".$head['list_table']." 
									ON ".$head['list_table'].".id = ".$lj_tblref.".".$fld." 
									".($listObject->hasStatus() ? " AND (".$head['list_table'].".status & ".MASTER::DELETED.") <> ".MASTER::DELETED : "");
			} elseif($head['type']=="MASTER") {
				$tbl = strtoupper($head['list_table']);
				//$masterObject = new MASTER_MASTER($fld);
				$masterObject = new $tbl();
				$tbl = $head['list_table'];
				$sql.= ", ".$fld.".".$masterObject->getFld("NAME")." as ".$tbl;
				$left_joins[] = "LEFT OUTER JOIN ".$masterObject->getTable()." AS ".$fld." ON ".$lj_tblref.".".$fld." = ".$fld.".".$masterObject->getFld("ID");
			} elseif($head['type']=="USER") {
				$sql.=", CONCAT(".$fld.".tkname,' ',".$fld.".tksurname) as ".$head['list_table'];
				$left_joins[] = "LEFT OUTER JOIN assist_".$this->getCmpCode()."_timekeep ".$fld." ON ".$fld.".tkid = ".$lj_tblref.".".$fld." AND ".$fld.".tkstatus = 1";
			} elseif($fld=="16") {
				$left_joins[] = "LEFT OUTER JOIN ".$this->getDBRef()."_certificates AS c_certs ON ".$c_tblref.".".$id_fld." = c_certs.customer_c_parent";
			}
		}
		$from = " $c_table $c_tblref ";
		$sql.= " FROM ".$from.implode(" ",$left_joins);
		$sql.= " WHERE ".$where.(isset($group_by) ? $group_by : "");		
			//return $sql;
			//echo $sql; 
		$r = $this->mysql_fetch_one($sql);
			//echo json_encode($r);
			//ASSIST_HELPER::arrPrint($r);
			//echo"<h1>INCOMING:</h1>";
			//echo $r; 
			//echo"<h1>DONE</h1>";
		//Fetch the children
		$parentArr = array();
		foreach($final_data['head'] as $k => $f){
			if(is_numeric($f['field'])){
				$parentArr[]=$f['field'];
			}
		}
		//$parentArr = implode(", ",$parentArr);
		//echo $parentArr;
		$sqlCh = "SELECT * FROM ".$this->getDBRef()."_setup_headings WHERE h_parent_id IN(11,12,13,14,15,17) ORDER BY h_parent_id";
		$children = $this->mysql_fetch_all($sqlCh);
//ASSIST_HELPER::arrPrint($children); 
		foreach($children as $cid=>$c){
			$prnt = $c['h_parent_id'];	
			if(isset($final_data['head'][$prnt])){
				$c_id = $c['h_id'];
				$final_data['head'][$prnt]['sub'][$c_id] = $c;
			}
		}
//ASSIST_HELPER::arrPrint($final_data['head']); 
		$row = array();
		$displayObject = new MASTER_DISPLAY();
		
		//Process the final data to deal with numeric headings
		foreach($final_data['head'] as $fld=> $head) {
			if(is_numeric($fld)){
				
			}
		}
		//End processing
		
		
//		foreach($rows as $r) {
			//$i = $r[$id_fld]=="Ccustomer_id"?"customer_id":$r[$id_fld];
			$i = $r[$id_fld];
			foreach($final_data['head'] as $fld=> $head) {
				$val = isset($r[$fld]) ? $r[$fld] : $fld;
				//format by type
				if($headObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
					$row[$fld] = ( (!isset($r[$head['list_table']]) || is_null($r[$head['list_table']])) ? $this->getUnspecified() : $r[$head['list_table']]);
				//} elseif($head['type']=="BOOL") {
					//$row[$fld] = $displayObject->getDataField("BOOL", $val);
				} elseif($head['type']=="HEADING"){
					//echo "<h3>KID:".$fld."</h3>"; 
					$row[$fld] = $children[$fld]; 
				} elseif($head['type']=="DELIVERABLE") {
					if($r["del_type"]=='SUB') {
						if($r[$fld]==0) {
							$row[$fld] = $this->getUnspecified();
						} else {
							if(!isset($deliverable_names_for_subs)) {
								$deliverable_names_for_subs = $dObject->getDeliverableNamesForSubs($i);
								//$this->arrPrint($deliverable_names_for_subs);
							}
							$row[$fld] = $deliverable_names_for_subs[$r[$fld]];
						}
					} else {
						unset($final_data['head'][$fld]);
					}
				} elseif($head['section']=="DELIVERABLE_ASSESS") {
					$assess_status = $r['contract_assess_type'];
					$display_me = true;
						if($cObject->mustIDoAssessment($r['contract_do_assessment'])===FALSE) {
							$display_me = false;
						} else {
							//validate the assessment status field here
							$lt = explode("_",$head['list_table']);
							switch($lt[1]) {
								case "quality": $display_me = $cObject->mustIAssessQuality($assess_status); break;
								case "quantity": $display_me = $cObject->mustIAssessQuantity($assess_status); break;
								case "other": $display_me = $cObject->mustIAssessOther($assess_status); break;
							}
						}
					if($display_me) {
						$row[$fld] = is_null($r[$head['list_table']]) ? $this->getUnspecified() : $r[$head['list_table']];
						if($lt[1]=="other" && strlen($r['contract_assess_other_name'])>0) {
							$final_data['head'][$fld]['name'] = str_ireplace("Other", $r['contract_assess_other_name'], $final_data['head'][$fld]['name']);
						}
					} else {
						unset($final_data['head'][$fld]);
						//$row[$fld] = $assess_status;
					}
				} elseif($this->isDateField($fld) || $head['type']=="DATE" ) {
					//echo "<h3>".ASSIST_HELPER::arrPrint($head)."</h3>";
					if($val == "0000-00-00 00:00:00" || $val == "0" || $val ==0){
						$row[$fld] = $this->getUnspecified();
					}else{
						$row[$fld] = $displayObject->getDataField("DATE", $val,array('include_time'=>false));
					}
				//format by fld
				} elseif($fld==$id_fld){//} || ($obj_type=="ACTION" && $fld==$a_id_fld) || ($obj_type=="DELIVERABLE" && $fld==$d_id_fld) || ($obj_type=="CONTRACT" && $fld==$c_id_fld)) {
					if($fld==$id_fld && !$format_for_emails) {
						//$row[$fld] = $ref_tag.$val;
						$row[$fld] = $val;
					} else {
						$row[$fld] = $val;
					}
				} elseif($head['type']=="REF") {
					$row[$fld] = $val;
					//echo "<P>".$fld." = ".$val;
				} elseif($head['type']=="ATTACH") {
					$row[$fld] = $val;
					//echo "<P>".$fld." = ".$val;
					$attach_display_options = array(
						'can_edit'=>false,
						'object_type'=>$obj_type,
						'object_id'=>$id,
						'buttons'=>$attachment_buttons,
					);
					$row[$fld] = $displayObject->getDataField($head['type'], $val,$attach_display_options);
				} else if(($fld == 16 || $fld == "16") ){
					
				}else {
					//$row[$fld] = $val;
					//if($val == "0000-00-00 00:00:00"){
					//	$row[$fld] = $this->getUnspecified();
					//}else{
						$row[$fld] = $displayObject->getDataField($head['type'], $val);
					//}
				}
			}
			if($format_for_emails) {
				switch($obj_type) {
					case "ACTION":
						$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$sorted_row['child'][$a_name] = $row[$a_name]; 
						$sorted_row['child'][$a_owner] = $row[$a_owner]; 
						$sorted_row['child'][$a_deadline] = $row[$a_deadline]; 
						$sorted_row['parent'][$d_name] = $row[$d_name]; 
						$sorted_row['parent'][$d_owner] = $row[$d_owner]; 
						$sorted_row['parent'][$d_deadline] = $row[$d_deadline]; 
						$sorted_row['grandparent'][$c_name] = $row[$c_name]; 
						$sorted_row['grandparent'][$c_owner] = $row[$c_owner]; 
						$sorted_row['grandparent'][$c_manager] = $row[$c_manager]; 
						$sorted_row['grandparent'][$c_deadline] = $row[$c_deadline]; 
						$field_names['child']['id'] = $row[$a_id_fld]; 
						$field_names['child']['name'] = $a_name; 
						$field_names['child']['owner'] = $a_owner; 
						$field_names['child']['deadline'] = $a_deadline; 
						$field_names['parent']['id'] = $row[$d_id_fld]; 
						$field_names['parent']['name'] = $d_name; 
						$field_names['parent']['owner'] = $d_owner; 
						$field_names['parent']['deadline'] = $d_deadline; 
						$field_names['grandparent']['id'] = $row[$c_id_fld]; 
						$field_names['grandparent']['name'] = $c_name;
						$field_names['grandparent']['owner'] = $c_owner; 
						$field_names['grandparent']['manager'] = $c_manager; 
						$field_names['grandparent']['deadline'] = $c_deadline; 
						break;
					case "DELIVERABLE":
						$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$sorted_row['child'][$d_name] = $row[$d_name]; 
						$sorted_row['child'][$d_owner] = $row[$d_owner]; 
						$sorted_row['child'][$d_deadline] = $row[$d_deadline]; 
						$sorted_row['parent'][$c_name] = $row[$c_name]; 
						$sorted_row['parent'][$c_owner] = $row[$c_owner]; 
						$sorted_row['parent'][$c_manager] = $row[$c_manager]; 
						$sorted_row['parent'][$c_deadline] = $row[$c_deadline]; 
						$field_names['child']['id'] = $row[$d_id_fld]; 
						$field_names['child']['name'] = $d_name; 
						$field_names['child']['owner'] = $d_owner; 
						$field_names['child']['deadline'] = $d_deadline; 
						$field_names['parent']['id'] = $row[$c_id_fld]; 
						$field_names['parent']['name'] = $c_name;
						$field_names['parent']['owner'] = $c_owner; 
						$field_names['parent']['manager'] = $c_manager; 
						$field_names['parent']['deadline'] = $c_deadline; 
						break;
					case "CONTRACT":
						$sorted_row = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$field_names = array('child'=>array(),'parent'=>array(),'grandparent'=>array());
						$sorted_row['child'][$c_name] = $row[$c_name]; 
						$sorted_row['child'][$c_owner] = $row[$c_owner]; 
						$sorted_row['child'][$c_manager] = $row[$c_manager]; 
						$sorted_row['child'][$c_deadline] = $row[$c_deadline]; 
						$field_names['child']['id'] = $row[$c_id_fld];
						$field_names['child']['name'] = $c_name;
						$field_names['child']['owner'] = $c_owner; 
						$field_names['child']['manager'] = $c_manager; 
						$field_names['child']['deadline'] = $c_deadline; 
						break;
				}				
				$final_data['rows'] = $sorted_row;
				$final_data['email_fields'] = $field_names;
				//$final_data['email_full'] = $r;
			} else {
				//echo"<h1>INCOMING:</h1>";
				//ASSIST_HELPER::arrPrint($row);
				//echo"<h1>DONE</h1>";
				$final_data['rows'] = $row;
			}
		//}
		return $final_data;
	}


	public function getObjectForUpdate($obj_id) {
		$sql = "SELECT ".$this->getTableField()."_progress, ".$this->getProgressStatusFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		return $this->mysql_fetch_one($sql);
	}
	
	/**
	 * Function to interface with MDOC module - built to check if an object is valid and active before displaying its associated mDoc
	 * @param (String) object_type = string containing the type of object, such as CONTRACT, DELIVERABLE, CUSTOMER
	 * @param (Int) object_id = the unique reference number for the object, such as 75
	 */
	public function isActiveObject($object_type,$object_id){
		$cObject = new MASTER_CONTRACT(); 
		$c_tblref = "C";
		$c_table = $cObject->getTableName();
		$c_table_fld = $cObject->getTableField();
		$c_status = $c_tblref.".".$cObject->getStatusFieldName();
		$c_raw_status = $cObject->getStatusFieldName();
		$c_type = $cObject->getTypeFieldName();
		$id_fld = $cObject->getIDFieldName();
		
		$sql = "SELECT ".$c_status;
		
		$from = " $c_table $c_tblref ";
		$sql.= " FROM ".$from;
		
		$where_tblref = $c_tblref;
		$where = $where_tblref.".".$id_fld." = ".$object_id;
		//Next line commented out for single-object modules
		//$where .= " AND ".$where_tblref.".".$c_table_fld.$c_type." = '".$object_type."'";
		$sql.= " WHERE ".$where;
		
		$result = $this->mysql_fetch_one($sql);
		//return $result;
		
		if($result[$c_raw_status] == MASTER::ACTIVE){
			return true;
		}else{
			return false;
		}
	}
	
    
    /***********************************
     * GET functions
     */
    
    /**
	 * (ECHO) Function to display the activity log link onscreen
	 * 
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
    public function drawPageFooter($left="",$log_table="",$var="") {
//    	$data = $this->getPageFooter($left,$log_table,$var);
//		echo $data['display'];
//		return $data['js'];
echo "WRONG PAGE FOOTER";
return "";
    }
    /**
	 * Function to create the activity log link onscreen
	 * 
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
    public function getPageFooter($left="",$log_table="",$var="") {
/*    	$echo = "";
		$js = "";
    	if(is_array($var)) {
    		$x = $var;
			$d = array();
			unset($var);
			foreach($x as $f => $v) {
				$d[] = $f."=".$v;
			}
			$var = implode("&",$d);
    	}
		$echo = "
		<table width=100% class=tbl-subcontainer><tr>
			<td width=50%>".$left."</td>
			<td width=50%>".(strlen($log_table)>0 ? "<span id=disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
				<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
			</span>" : "")."</td>
		</tr></table><div id=div_audit_log></div>";
		if(strlen($log_table)>0){
			$js = "
			$(\"#disp_audit_log\").click(function() {
				var state = $(this).attr('state');
				if(state==\"show\"){
					$(this).find('img').prop('src','/pics/tri_down.gif');
					$(this).attr('state','hide');
					$(\"#div_audit_log\").html(\"\");
					$(\"#log_txt\").html('Display Activity Log');
				} else {
					$(this).find('img').prop('src','/pics/tri_up.gif');
					$(this).attr('state','show');
					var dta = '".$var."&log_table='+$(this).attr('table');
					var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
					$(\"#div_audit_log\").html(result[0]);
					$(\"#log_txt\").html('Hide Activity Log');
				}
				
			});
			";
		}
    	$data = array('display'=>$echo,'js'=>$js);
		return $data;
 **/
		return array('display'=>"WRONG PAGE FOOTER",'js'=>"");
    }
 
	public function drawActivityLog() {
		
	}
 
 
 
   
    /*********************************************
     * SET/UPDATE functions
     */
	public function addActivityLog($log_table,$var,$mode=false) {
		$logObject = new MASTER_LOG($log_table);
		return $logObject->addObject($var,$mode);
	}
	
    
    public function notify($data,$type,$object){
    	$noteObj = new MASTER_NOTIFICATION($object);
		$result = $noteObj->prepareNote($data, $type, $object);
		return $result;
    }
   
   
   

	public function editMyObject($var,$attach=array(),$recipients=array(),$noter="",$include=array()) {
		$extra  ="";
		$object_id = $var['object_id'];
		unset($var['object_id']);
		if(isset($var['customer_user_status']) && $var['customer_user_status'] == 2){
			$activation = true;
		}else{
			$activation = false;
		}	
			
		//Removing certificates for seperate processing
		$certs = array();
		foreach($var as  $key=>$val){
			if(strpos($key,"customer_c_")!== false){
				unset($var[$key]);
				$certs[$key]=$val;
			}// else if(isset($var['customer_type']) && $var['customer_type'] == 1 && strpos($key,"customer_o_")!== false){
			//	unset($var[$key]);
			//}
		}	
		//Removing extra user access for seperate processing
		$extraAccess = array();
		$varU = array();
		foreach($var as  $key=>$val){
			if(strpos($key,"extra_access_")!== false){
				unset($var[$key]);
				if($val !== "X" && !in_array($val, $varU)){
					$extraAccess[]=$val;
					$varU[]=$val;
					$extraLog[] = $this->getAUserName($val);
				}
			}
		}
		//return array("testting",$var,$varU);
		//$extraLog
		$resultAx = 0;
		$mdoc = new MDOC();
		$oldAx = $mdoc->getSpecialAccessForObject($object_id,true);
		if(count($oldAx)>0){
			$oldAx = reset($oldAx);
			$oldAx = json_decode($oldAx);
			foreach($oldAx as $key=>$val){
				$oldAx[$key] = $this->getAUserName($val);
			}
		}else{
			$oldAx[] = ASSIST_HELPER::UNSPECIFIED;
		}
		//return array("olds processed"=>$oldAx,"extras new"=>$extraAccess);
		if(count($extraAccess)>0){
			$resultAx = $mdoc->setSpecialAccessForObject($object_id,$extraAccess);
		}else if(count($extraAccess)==0 && count($oldAx >0)){
			$resultAx = $mdoc->setSpecialAccessForObject($object_id,$extraAccess);
			$extraLog[]=ASSIST_HELPER::UNSPECIFIED;
		}
		
		//return array("resultAx"=>$resultAx,"logso"=>$extraLog);
		
		//return array("testesing",json_encode($extraAccess));
		//return array("testes",json_encode($resultAx));
		$headObject = new MASTER_HEADINGS();
		$headings = $headObject->getMainObjectHeadings($this->getMyObjectType(),"EDIT_FORM");
		$mar=0;
		$insert_data = array();
		foreach($var as $fld=>$v) {
			if(isset($headings[$fld])  || in_array($fld,array("contract_assess_status","contract_assess_type","contract_assess_other_name"))) {
				if($this->isDateField($fld) || $headings[$fld]['type']=="DATE") {
					if(strlen($v)>0){
						$insert_data[$fld] = date("Y-m-d",strtotime($v));
					}
				} elseif($fld=="del_type") {
					if($v=="DEL") {
						$var['del_parent_id'] = 0;
						$insert_data['del_parent_id'] = 0;
					}
					$insert_data[$fld] = $v;
				} else {
					$insert_data[$fld] = $v;
				}
			}else{
				$error_arr[]=$fld;
			}
		}
		//return array("info",$this->convertArrayToSQL($insert_data));
		
		//$old = $this->getRawCertObject($object_id);
		$old = $this->getRawObject($object_id);
		//return array("info",$old);
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		
		//$certs = $this->processCertificates($certs,$object_id,"EDIT");
		//return array("info",$certs['changes']);

		$usr = $this->getUserID();
		$changes = array(
			//'user'=>$this->getAUserName($usr),
		);
		if($mar>0 && $resultAx<=0) {
			//echo"<h2>".json_encode($headings)."</h2>";
			foreach($insert_data as $fld => $v) {
					
				
				if($old[$fld]!=$v || $headings[$fld]['type']=="HEADING") {
					$h = isset($headings[$fld]) ? $headings[$fld] : array('type'=>"TEXT");
					if(in_array($h['type'],array("LIST","MASTER","USER","OWNER","DEL_TYPE","DELIVERABLE"))) {
						$list_items = array();
						$ids = array($v,$old[$fld]); 
						switch($h['type']) {
							case "LIST":
								$listObject = new MASTER_LIST($h['list_table']);
								$list_items = $listObject->getAListItemName($ids);
								//return array("error","Tons of changes were found to be saved.  Please try again.");
								break;
							case "USER":
								$userObject = new MASTER_USERACCESS();
								$list_items = $userObject->getActiveUsersFormattedForSelect($ids);
								break; 
							case "OWNER":
								$ownerObject = new MASTER_CONTRACT_OWNER();
								$list_items = $ownerObject->getActiveOwnersFormattedForSelect($ids);
								break;
							case "MASTER":
								//$listObject = new MASTER_MASTER($head['list_table']);
								$hdlist = strtoupper($h['list_table']);
								$listObject = new $hdlist();
								$list_items = $listObject->getActiveItemsFormattedForSelect();
						//		$list_items = array($v=>$hdlist,$old[$fld]=>"old variable");
								break;
							case "DELIVERABLE":
								$delObject = new MASTER_DELIVERABLE();
								$list_items = $delObject->getDeliverableNamesForSubs($ids);
								break; 
							case "DEL_TYPE":
								$delObject = new MASTER_DELIVERABLE();
								$list_items = $delObject->getDeliverableTypes($ids);
								break; 
						} 
						//echo json_encode($list_items);
						unset($listObject);
						$changes[$fld] = array('to'=>$list_items[$v],'from'=>$list_items[$old[$fld]], 'raw'=>array('to'=>$v,'from'=>$old[$fld]));
					} elseif($h['type']=="HEADING") {
						if($fld=="contract_assess_type") {
							$shead = $headings['sub'][$h['id']];
							foreach($shead as $sh) {
								$sfld = $sh['field'];
								switch($sh['type']) {
									case "BOOL":
										$bitwise_value = 0;
										$b = explode("_",$sfld);
										$f = end($b);
										switch($f) {
											case "other":	$bitwise_value = MASTER_CONTRACT::OTHER;		break;
											case "qual":	$bitwise_value = MASTER_CONTRACT::QUALITATIVE;	break;
											case "quan":	$bitwise_value = MASTER_CONTRACT::QUANTITATIVE;	break;
										}
										$new_val = 0;
										if(($v & $bitwise_value) == $bitwise_value ) {
											$new_val = 1;
										}
										$old_val = 0;
										if(($old[$fld] & $bitwise_value) == $bitwise_value) {
											$old_val = 1;
										}
										if($old_val != $new_val) {
											$changes[$sfld] = array('to'=>$new_val,'from'=>$old_val);
										}
										break;
									default:
										if($old[$sfld]!=$insert_data[$sfld]) {
											$changes[$sfld] = array('to'=>$insert_data[$sfld],'from'=>$old[$sfld]);
										}
										break;
								}
							}
						}
					} else {
						$changes[$fld] = array('to'=>$v,'from'=>$old[$fld]);
					}
				}
			}
			$x = array();
			$attach = is_array($attach)?$attach:array();
			foreach($attach as $a) {
				$x[] = "|attachment| ".$a['original_filename']." has been added.";
			}
			if(count($x)>0){
				$changes[$this->getAttachmentFieldName()] = $x;
			}
		}else if($resultAx<=0){
			return array("info","No changes were found to be saved.  Please try again.");
		}else{
			$changes['Extra Access'] = array('to'=>implode(", ",$extraLog),'from'=>implode(", ",$oldAx));
		}
		/*
		if($certs['failer']!==true && $certs['empty']!==true){
			//-------------------------------------------------CERTIFICATE LOGGING START---------------------------------------------------//
			foreach($certs['changes'] as $index => $arr) {
				$cert_id = $arr['customer_c_certificate'];
				foreach($old['customer_c_certificate'] as $olk=>$olv){
					if($olv['customer_c_certificate'] == $cert_id){
						foreach($arr as $fld => $v) {
							$h = isset($headings[$fld]) ? $headings[$fld] : array('type'=>"TEXT");
							if(in_array($h['type'],array("LIST","MASTER","USER","OWNER","DEL_TYPE","DELIVERABLE"))) {
								$list_items = array();
								$ids = array($v,$olv[$fld]); 
								switch($h['type']) {
									case "LIST":
										$listObject = new MASTER_LIST($h['list_table']);
										$list_items = $listObject->getAListItemName($ids);
										break;
									case "USER":
										$userObject = new MASTER_USERACCESS();
										$list_items = $userObject->getActiveUsersFormattedForSelect($ids);
										break; 
									case "OWNER":
										$ownerObject = new MASTER_CONTRACT_OWNER();
										$list_items = $ownerObject->getActiveOwnersFormattedForSelect($ids);
										break;
									case "MASTER":
										$masterObject = new MASTER_MASTER($head['list_table']);
										$list_items = $masterObject->getActiveItemsFormattedForSelect($ids);
										break; 
									case "DELIVERABLE":
										$delObject = new MASTER_DELIVERABLE();
										$list_items = $delObject->getDeliverableNamesForSubs($ids);
										break; 
									case "DEL_TYPE":
										$delObject = new MASTER_DELIVERABLE();
										$list_items = $delObject->getDeliverableTypes($ids);
										break; 
								} 
								
								unset($listObject);
								$changes['duplicates'][$cert_id][$fld] = array('to'=>$list_items[$v],'from'=>$list_items[$old[$fld]], 'raw'=>array('to'=>$v,'from'=>$old['customer_c_certificate'][$fld]));
							} elseif($h['type']=="HEADING") {
								if($fld=="contract_assess_type") {
									$shead = $headings['sub'][$h['id']];
									foreach($shead as $sh) {
										$sfld = $sh['field'];
										switch($sh['type']) {
											case "BOOL":
												$bitwise_value = 0;
												$b = explode("_",$sfld);
												$f = end($b);
												switch($f) {
													case "other":	$bitwise_value = MASTER_CONTRACT::OTHER;	break;
													case "qual":	$bitwise_value = MASTER_CONTRACT::QUALITATIVE;	break;
													case "quan":	$bitwise_value = MASTER_CONTRACT::QUANTITATIVE;	break;
												}
												$new_val = 0;
												if(($v & $bitwise_value) == $bitwise_value ) {
													$new_val = 1;
												}
												$old_val = 0;
												if(($old['customer_c_certificate'][$fld] & $bitwise_value) == $bitwise_value) {
													$old_val = 1;
												}
												if($old_val != $new_val) {
													$changes[$sfld] = array('to'=>$new_val,'from'=>$old_val);
												}
												break;
											default:
												if($old[$sfld]!=$insert_data[$sfld]) {
													$changes[$sfld] = array('to'=>$insert_data[$sfld],'from'=>$old['customer_c_certificate'][$sfld]);
												}
												break;
										}
									}
								}
							} else {
								$changes['duplicates'][$cert_id][$fld] = array('to'=>$v,'from'=>$olv[$fld]);
							}
						}
					}
				}
			}
				//return array("info",$changes);
			//-------------------------------------------------CERTIFICATE LOGGING END---------------------------------------------------|//
		}else if($certs['failer']===true && $certs['empty']!==true){
			return array("error","The certificates you created could not be saved. Please try again - then contact your Assist Administrator.");
		}

		if($mar<=0 && $certs['empty']===true){
			return array("info","No changes were found to be saved.  Please try again.");
		}
		
		//No-certs ^V
		
		if($mar<=0){
			return array("info","No changes were found to be saved.  Please try again.");
		}
		*/
		
		if($activation){
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::ACTIVATE,
				'status_id'	=> $old[$this->getTableField()."_status"],		
			);
		}else{
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::EDIT,
				'status_id'	=> $old[$this->getTableField()."_status"],		
			);
		}
		
		
		
		//return array("error",$log_var);
		//return array("info",array($this->getMyLogTable(), $log_var));
		$this->addActivityLog($this->getMyLogTable(), $log_var,false);
		//return array("error",$this->addActivityLog($this->getMyLogTable(), $log_var,false));
		if($activation){
			if($resultAx>0){
				if($extraLog[0]==ASSIST_HELPER::UNSPECIFIED){
					//$extra = " Extra access has been removed.";
				}else{
					//$extra = " Extra access has been granted.";
				}
			}else{
				$extra = "";
			}
			return array("ok",$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$object_id." has been activated. It is now available in the rest of the module.".$extra,array($log_var,$this->getMyObjectType()));
		}else{
			/*
			if($resultAx>0){
				$extra = " Extra access has been granted.";
			}else{
				$extra = "";
			}	
			*/
			
			return array("info",$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$object_id." has been successfully edited.".$extra,array($log_var,$this->getMyObjectType()));
		}
		//$response = array("ok",$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$object_id." has been successfully edited.",array($log_var,$this->getMyObjectType()));
	}

   
   
   
   
   
   
   

    
    /********************************************
     * PROTECTED functions
     */


	/**
	 * Function to generate SQL statement for bitwise status check
	 */
	protected function generateStatusSQL($status,$compare,$t="",$fld="") {
		return "( ".(strlen($t)>0 ? $t."." : "")."".$this->getTableField($fld)."_status & ".$status.") ".$compare." ".$status;
	}
	/**
	 * Generate Status SQL statement dependent on page
	 */
	protected function getStatusSQL($section="REPORT",$t="",$deleted=false,$is_contract=true) {
		$str = "";	
		$statuses = array(0=>array(),1=>array());
		$fourth_field = "";
		if($deleted) {
			$statuses[1][] = MASTER::DELETED; 
			$statuses[0][] = MASTER::ACTIVE; 
		} else {
			$statuses[0][] = MASTER::DELETED; 
			if($section !="ACTIVATION" && $section != "BOTH"){
				$statuses[1][] = MASTER::ACTIVE; 
			}
		}
		switch($section) {
			case "CONFIRMATION":
			case "NEW":
				if($is_contract){
					$statuses[0][] = MASTER_CONTRACT::CONFIRMED; 
					$statuses[0][] = MASTER_CONTRACT::ACTIVATED;
				} 
				break;
			case "ACTIVATION":
				if($is_contract){
					$statuses[1][] = MASTER::SYSTEM_DEFAULT; 
					$statuses[0][] = MASTER::ACTIVE;
				} 
				$fourth_field = "yes";
				break;
			case "ALL":
				$fourth_field = "yes";
				$statuses[0][] = MASTER::SYSTEM_DEFAULT; 
				$statuses[0][] = MASTER::INACTIVE; 
				break;
			case "BOTH":
				$fourth_field = "yes";
				//$statuses[1][] = MASTER::SYSTEM_DEFAULT; 
				$statuses[2][] = MASTER::ACTIVE;
				$statuses[2][] = MASTER::INACTIVE;
				$str.= "";
				break;
			case "DELETED":
				break;
			case "REPORT":
			default:
				if($is_contract) {
					$statuses[1][] = MASTER_CONTRACT::CONFIRMED; 
					$statuses[1][] = MASTER_CONTRACT::ACTIVATED;
				} 
				break;
		}
		$sql = array();  //ASSIST_HELPER::arrPrint($statuses);
		foreach($statuses[0] as $not) {
			$sql[] = $this->generateStatusSQL($not,"<>",$t,$fourth_field);
		}
		foreach($statuses[1] as $yes) {
			$sql[] = $this->generateStatusSQL($yes,"=",$t,$fourth_field);
		}
		if(isset($statuses[2])){
			foreach($statuses[2] as $yes) {
				$sql2[] = $this->generateStatusSQL($yes,"=",$t,$fourth_field);
			}
			$sql[] .= " (".implode(" OR ",$sql2).") ";
		}
		$str .= " (".implode(" AND ",$sql).") "; //echo "<P>".$str."</p>";
		return $str;
	}    
     
	 
	 
	 
	 
	 
	 
	 
     
     
     
     
     
   
   
   
   
   
   
   
   

     
     public function __destruct() {
     	parent::__destruct();
     }
     
     
     
	
}






?>