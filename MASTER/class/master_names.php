<?php
/**
 * To manage the NAMES of items
 * 
 * INCOMPLETE!!!!! as at 16 Oct 2014
 * 
 * 
 * This class cannot be a child of the MASTER class because that class instantiates this class which in turn creates in infinite loop.
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class MASTER_NAMES extends ASSIST_MODULE_HELPER {
    
	private $additional_headings = array(
		'admins'=>'contract_owner_id',
		'menu'=>"Menu",
		'headings'=>"Headings",
		'lists'=>"Lists",
	);
	
	
    
    const TABLE = "setup_names";
	const TABLE_FIELD = "name";
    /*************
     * CONSTANTS
     */
    /**
     * Can a heading be renamed by the client
     */
    const CAN_RENAME = 16;  
    /**
     * Is a heading the name of an object
     */
    const OBJECT_HEADING = 32;
    /**
     * Is a heading the name of an activity
     */
    const ACTIVITY_HEADING = 64;
         
    
    
    public function __construct() {
        parent::__construct();
    }
    
    
	
	
	
	
	

	
	
	/***********************************
	 * CONTROLLER functions
	 */
	public function editObject($var) {
		$section = isset($var['section']) ? $var['section'] : "";
		unset($var['section']);
		$mar = $this->editNames($section,$var);
		if($mar>0) {
			if($section=="object_names"){
				return array("ok","Object names updated successfully.");
			} else {
				return array("ok","Activity names updated successfully.");
			}
		} else {
			return array("info","No changes were found to be saved.");
		}
		return array("error",$mar);
	}
	
	
	
	
    /*******************
     * GET functions
     * */
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    
	protected function getFormattedNames($type) {
		$names = array(); 
		$sql = "SELECT name_section as section, IF(LENGTH(name_client)>0,name_client,name_default) as name 
				FROM ".$this->getTableName()."
				WHERE (name_status & ".$type." = ".$type.")";
		$rows = $this->mysql_fetch_all($sql);
		foreach($rows as $r){
			$s = explode("_",$r['section']);
			$names[end($s)] = $r['name'];
		}
		return $names;
	}
	
   /**
    * Get the object names from the table in easy readable format
    */
	public function fetchObjectNames() {
		return $this->getFormattedNames(MASTER_NAMES::OBJECT_HEADING);
	}
   /**
    * Get the object names from the table in easy readable format
    */
	public function fetchActivityNames() {
		return $this->getFormattedNames(MASTER_NAMES::ACTIVITY_HEADING);
	}
	
	     
	    
		
		
	protected function getNamesFromDB($type,$sections=array()){
		$sql = "SELECT name_id as id, name_section as section, name_default as mdefault, name_client as client 
				FROM ".$this->getTableName()." 
				WHERE (name_status & ".$type." = ".$type.")"
				.(count($sections)>0 ? " AND name_section IN ('".implode("','",$sections)."')" : "");
		$rows = $this->mysql_fetch_all_by_id($sql,"section");
		return $rows;
		//return array($sql);
	}
		 
	/** 
	 * Get the object names from the table for editing in Setup
	 */
	public function getObjectNamesFromDB($sections=array()){
		return $this->getNamesFromDB(MASTER_NAMES::OBJECT_HEADING,$sections);
	}
	/** 
	 * Get the activity names from the table for editing in Setup
	 */
	public function getActivityNamesFromDB($sections=array()){
		return $this->getNamesFromDB(MASTER_NAMES::ACTIVITY_HEADING,$sections);
	}
		

	 
	 
	 
	 
	 
    /****************************************
     * SET / UPDATE Functions
     */
    
    
    /**
	 * Function to edit object name / menu heading
	 */
    private function editNames($section,$var) {
    	if($section=="object_names") {
	   		$original_names = $this->getObjectNamesFromDB(array_keys($var));
	   		$log_section = "OBJECT";
			$field = "name_section";
    	} else {
    		$original_names = $this->getActivityNamesFromDB(array_keys($var));
			$log_section = "ACTIVITY";
			$field = "name_section";
    	}
    	$mar = 0;
    	$sql_start = "UPDATE ".$this->getTableName()." SET name_client = '";
		$sql_mid = "' WHERE $field = '";
		$sql_end = "' LIMIT 1";
		$c = array();
	    $menu_section = isset($menu_section) ? $menu_section : "";
    	foreach($var as $section => $val){
    		$m = 0;
			$to = (strlen($val)>0 ? $val : $original_names[$section]['mdefault']);
			$from = $original_names[$section]['client'];
			if($to!=$from) {
    			$sql = $sql_start.$val.$sql_mid.$section.$sql_end;
				$m=$this->db_update($sql);
				if($m>0){
					if($menu_section=="object_names") {
						$s = explode("_",$section);
						$key = "|".end($s)."|";
					} else {
						$s = explode("_",$section);
						$key = "|".end($s)."|";
					}
					$c[$key] = array('to'=>$to,'from'=>$from);
					$mar+=$m;
				}
			}
    	}
		if($mar>0 && count($c)>0) {
			$changes = array_merge(array('user'=>$this->getUserName()),$c);
			$log_var = array(
				'section'	=> $log_section,
				'object_id'	=> $original_names[$section]['id'],
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::EDIT,		
			);
			$me = new MASTER();
			$me->addActivityLog("setup", $log_var);
		}
		
		return $mar;
    }
    
    
    
    
    
    
    
    
    /**********************************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    
	 
     
	 
	 
	 
	 
	 
	 
	 
     
     /***********************
     * PRIVATE functions: functions only for use within the class
     */	
	
	
	
	
	    
    public function __destruct() {
    	parent::__destruct();
    }
    
    
    
}


?>