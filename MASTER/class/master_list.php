<?php
//error_reporting(-1);
/**
 * To manage the various list items
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class MASTER_LIST extends MASTER {
    
	private $no_status = false;
    private $my_list = "";
    private $list_table = "";
    private $fields = array();
    private $field_names = array();
    private $field_types = array();
    private $associatedObject;
    private $object_table = "";
    private $object_field = "";
	private $sort_by = "sort, name";
	private $sort_by_array = array("sort","name");
	private $my_name = "value";
	private $sql_name = "|X|name";
    
    private $lists = array();   
    private $default_field_names = array(
        'id'=>"Ref",
        'name'=>"Name",
        'tkname'=>"Name",
        'tksurname'=>"Surname",
        'tkid'=>"ID",
        'tktitle'=>"Title",
        'value'=>"Value",
        'default_name'=>"Default Name",
        'client_name'=>"Your Name",
        'shortcode'=>"Short Code",
        'description'=>"Description",
        'status'=>"Status",
        'colour'=>"Colour",
        'rating'=>"Score",
        'list_num'=>"Supplier Category",
        'code'=>"Code",
    );
    private $default_field_types = array(
        'id'=>"REF",
        'tkid'=>"REF",
        'default_name'=>"LRGVC",
        'client_name'=>"LRGVC",
        'name'=>"LRGVC",
        'shortcode'=>"SMLVC",
        'description'=>"TEXT",
        'status'=>"STATUS",
        'colour'=>"COLOUR",
        'rating'=>"RATING",
        'list_num'=>"LIST",
        'value'=>"MEDVC",
        'tktitle'=>"MEDVC",
        'tkname'=>"MEDVC",
        'tksurname'=>"MEDVC",
        'code'=>"SMLVC",
    );
    private $required_fields = array(
        'id'=>false,
        'name'=>true,
        'default_name'=>false,
        'client_name'=>true,
        'shortcode'=>true,
        'description'=>false,
        'status'=>false,
        'colour'=>true,
        'rating'=>true,
        'list_num'=>true,
    );
    
    
/**    
 * @param {String} list     The list identifier
 */
    public function __construct($list=""){
        parent::__construct();
        //$this->no_status = false;
		if(strlen($list)>0) {
        	$this->my_list = $list;
        	$this->setListDetails();
		}
    }
	public function changeListType($list="",$fld="") {
        //$this->no_status = false;
		if(strlen($list)>0) {
        	$this->my_list = $list;
        	$this->setListDetails($fld);
		}
	}
        
	/**
	 * CONTROLLER functions
	 */
	public function addObject($var){
		if(!isset($var['status'])) { $var['status'] = self::ACTIVE; }
		//if(!isset($var['ua_insertuser'])) { $var['ua_insertuser'] = $this->getUserID(); }
		//if(!isset($var['ua_insertdate'])) { $var['ua_insertdate'] = date("Y-m-d H:i:s"); }
		return $this->addListItem($var);
	}
	public function editObject($var){
		$id = $var['id'];
		return $this->editListItem($id,$var);
	}
	public function deleteObject($var){
		$id = $var['id'];
		return $this->deleteListItem($id);
	}
	public function deactivateObject($var){
		$id = $var['id'];
		return $this->deactivateListItem($id);
	}
	public function restoreObject($var){
		$id = $var['id'];
		return $this->restoreListItem($id);
	}
	
	
	
    /***************************
     * GET functions
     **************************/
	public function getMyName(){
		return $this->my_name;
	}
	public function getSQLName($t="") {
		return str_ireplace("|X|", (strlen($t)>0?$t.".":""), $this->sql_name);
	}
	public function getListTable(){
		return $this->list_table;
	}
	public function hasStatus() { return !($this->no_status); }
	public function getSortBy($as_array=true) {
		if($as_array) {
			return $this->sort_by_array;
		} else {
			return $this->sort_by;
		}
	}
	public function getAllListTables($lists=array()) {
		$data = array();
		foreach($lists as $l) {
        	$this->my_list = $l;
        	$this->setListDetails();
			$data[$l] = $this->getListTable();			
		}
		return $data;
	}
	/**
	 * Returns the field names for the given list
	 */
	public function getFieldNames() {
		return $this->field_names;
	}
	/**
	 * Returns the required fields for the given list
	 */
	public function getRequredFields() {
		return $this->required_fields;
	}
	/**
	 * Returns the field names for the given list
	 */
	public function getFieldTypes() {
		return $this->field_types;
	}
    /**
     * Returns list items in an array with the id as key depending on the input options
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */ 
    public function getListItems($options="",$type="DEF") {
    	//echo "<P>SORT: ".$this->sort_by;
	    if(strlen($this->getListTable())>0){
	        $sql = "SELECT * ";
			if(in_array("client_name",$this->fields)) {
				$sql.=", IF(LENGTH(client_name)>0,client_name,default_name) as name ";
			}
			if(!$this->hasStatus()){
		        $sql.= " FROM ".$this->getListTable()." ".(strlen($options)>0 ? " WHERE ".$options : "")." ORDER BY ".$this->sort_by;
			}else{
				if(in_array("status",$this->getFieldNames())) {
					$options = "(status & ".MASTER::DELETED.") <> ".MASTER::DELETED.(strlen($options)>0 ? " AND ".$options : "")."";
				}
		        $sql.= " FROM ".$this->getListTable().(strlen($options)>0 ? " WHERE ".$options : "")." ORDER BY ".$this->sort_by;
			}
			if($type=="LOL"){
		        return $sql;
			}
	        $res = $this->mysql_fetch_all_by_id($sql, "id");
			//if($type=="REP") {
		    //	echo $sql."<br>";
		     //   ASSIST_HELPER::arrPrint($res);
			//}
			$this->no_status = false;
	        return $res;
	    }else{
	    	//echo $this->my_list;
	    	return array("info","No table set up for ".$this->my_list);
	    }    	
    }   
    /**
     * Returns all list items which have been used formatted for reporting purposes
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getItemsForReport($options="") {
    	//echo "<P>SORT: ".$this->sort_by;
        $options=(strlen($options)>0 ? " (".$options.") AND " : "")
        	.($this->hasStatus() ? "(status & ".MASTER::DELETED." <> ".MASTER::DELETED." ) AND " : "")
        	." (id IN (SELECT DISTINCT ".$this->object_field." FROM ".$this->object_table."))";
        $items = $this->getListItems($options,"REP");
		$data = array();
		foreach($items as $id => $i) {
			$data[$id] = $i[$this->my_name];
		}
		return $data;
    } 
    /**
     * Returns all active list items in an array with the id as key
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getActiveListItems($options="") {
        $options=(strlen($options)>0 ? " (".$options.") AND " : "")
        .($this->hasStatus() ? "(status & ".MASTER::ACTIVE." = ".MASTER::ACTIVE." )" : "");
        return $this->getListItems($options);
    } 
    /**
     * Returns all active list items formatted for display in SELECT => array with the id as key and value as element
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getActiveListItemsFormattedForSelect($options="") {
		//return $this->my_list;
        $rows = $this->getActiveListItems($options);
		$dta = $this->formatRowsForSelect($rows);
		if(in_array("list_num",$this->fields)) {
			$data = array();
			$data['options'] = $dta;
			$data['list_num'] = array();
			foreach($rows as $key => $r) {
				$data['list_num'][$key] = $r['list_num'];
			}
		} else {
			$data = $dta;
		}
		return $data;
    } 
    /**
     * Returns all list items not deleted
     * @return (Array of Records)
     */
    public function getAllListItems() {
        return $this->getListItems();
    }
    /**
     * Returns a specific list item determined by the input id
     * @param (INT) id = The ID of the list item 
     * @return (One Record)
	 * OR
	 * @param (Array) id = array of IDs to be found
	 * @return (Array of Arrays) records found
     */    
    public function getAListItem($id) {
    	if(is_array($id)) {
        	$data = $this->getListItems("id IN (".implode(",",$id).")");
			return $data;
    	} else {
        	$data = $this->getListItems("id = ".$id);
			//echo $id;
			//ASSIST_HELPER::arrPrint($data);
	        return $data[$id];
        }
    }    
	/**
	 * returns the names of specific list items determined by the input id(s)
	 * @param (int) id = id of the list item to be found
	 * @return (String) name of list item
	 * OR
	 * @param (array) id = array of ids to be found
	 * @return (Array of Strings) names of the list items with the ID as index
	 */
	public function getAListItemName($id) {
    	$data = $this->getAListItem($id);
    	if(is_array($id)) {
    		$items = array();
			foreach($data as $i=>$d) {
				$items[$i] = $d[$this->my_name];
			}
			return $items;
    	} else {
	        return !isset($data['name'])?$data['value']:$data['name'];
        }
	}
	
    /**
     * Returns a list items with the addition of a can_delete element which indicates if the item is in use = for Setup to know whether or not the item can be deleted
     * @param (INT) id = The ID of the list item
     * @return (One record)
     */
    public function getAListItemForSetup($id) {
        $data = $this->getAListItem($id);
        /*****
         * TO BE PROGRAMMED: 
         *     $sql = SELECT count(O.$this->object_field) as in_use 
         *      FROM $this->object_table O
         *      ON L.id = O.$this->object_field
         *      WHERE O.$this->object_field = $id
         *      AND O.$this->associatedObject->getTableField().status & DELETED <> DELETED
         * $row = $this->mysql_fetch_one_value($sql,"in_use")
         * parse results: if in_use > 0 then $data['can_delete'] = false else = true 
         * 
         */
         return $data;
    }

    /**
     * Returns  list items with the addition of a can_delete element which indicates if the item is in use = for Setup to know whether or not the item can be deleted
     * @return (records)
     */
	public function getListItemsForSetup() {
		$data = $this->getListItems();
		/*****
		 * TO BE PROGRAMMED: 
		 *     $sql = SELECT count(O.$this->object_field) as in_use 
		 *      FROM $this->object_table O
		 *      ON L.id = O.$this->object_field
		 *      WHERE O.$this->object_field = $id
		 *      AND O.$this->associatedObject->getTableField().status & DELETED <> DELETED
		 * $row = $this->mysql_fetch_one_value($sql,"in_use")
		 * parse results: if in_use > 0 then $data['can_delete'] = false else = true 
		 * 
		 */
		if(count($data)>0) {
			$keys = array_keys($data);
			if(is_array($this->object_field)) {
				$u = array();
				$used = array();
				foreach($this->object_field as $k => $of) {
					$ot = $this->object_table[$k];
					$sql = "SELECT ".$of." as fld, count(".$of.") as in_use FROM ".$ot." WHERE ".$of." IN (".implode(",",$keys).") GROUP BY ".$of;
					$u[] = $this->mysql_fetch_all_by_id($sql, "fld");
					foreach($u as $f => $s) {
						$s['in_use'] = isset($s['in_use']) ? $s['in_use'] : 0;
						if(isset($used[$f])) {
							$used[$f]['in_use']+= $s['in_use'];
						} else {
							$used[$f] = $s;
						}
					}
				}
			} else {
				$sql = "SELECT ".$this->object_field." as fld, count(".$this->object_field.") as in_use FROM ".$this->object_table." WHERE ".$this->object_field." IN (".implode(",",$keys).") GROUP BY ".$this->object_field;
				$used = $this->mysql_fetch_all_by_id($sql, "fld");
			}
			foreach($data as $key=>$item) {
				if(!isset($used[$key]) || $used[$key]['in_use']==0) {
					$data[$key]['can_delete'] = true;
				} else {
					$data[$key]['can_delete'] = false;
				}
			}
		}
		return $data;
	}




    /****************************
     * SET/UPDATE functions
     *****************************/
    /**
     * Create a new list item
     * @param (Array) var = the variables to be saved to the database with the field name as the key and the field value as the element
     * @return TBD (INT) id of new value? or true/false to indicate success or failure
     */
    public function addListItem($var) {
        //return array("error",json_encode($var));	
        $insert_data = $this->convertArrayToSQL($var);
		$sql = "INSERT INTO ".$this->getListTable()." SET ".$insert_data;
		$id = $this->db_insert($sql);
		if($id>0){
			$result = array("ok","List item added successfully.");
			$name = isset($var[$this->getMyName()]) ? $var[$this->getMyName()] :"";
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Added a new item " . $id . ": " . $name,
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::CREATE,		
			);
			//return array("info",json_encode($log_var));
			$this->addActivityLog("setup", $log_var);
		} else {
			$result = array("error","Sorry, something went wrong while trying to add the list item. Please try again.");
		}
		return $result;
    }
    /**
     * Edit a list item details
     * @param (Array) var = the variables to be saved to the database with the field name as the key and the field value as the element
     * @return TBD (INT) id of new value? or true/false to indicate success or failure
     */
    public function editListItem($id,$var) {
        $old_sql = "SELECT * FROM " . $this->getListTable() . " WHERE id = $id";
		$old_result = $this->mysql_fetch_one($old_sql);
        $insert_data = $this->convertArrayToSQL($var);
		$sql = "UPDATE ".$this->getListTable()." SET ".$insert_data . " WHERE id = $id";
		$mar = $this->db_update($sql);
		if($mar>0){
		    $name = isset($var[$this->getMyName()]) ? $var[$this->getMyName()] : "";
			$result = array("ok","List item modified successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " modified (".$name.").",
			);
			foreach($old_result as $key=>$value){
				if(isset($var[$key]) && $value != $var[$key]){
					if($key=="list_num") {
						$listObject2 = new MASTER_LIST("contract_supplier_category");
						$item_names = $listObject2->getAListItemName(array($var[$key],$value));
						$item_names[0] = $this->getUnspecified();
						$changes[$key] =array('to'=>$item_names[$var[$key]], 'from'=>$item_names[$value]);
					} else {
						$changes[$key] =array('to'=>$var[$key], 'from'=>$value);
					}
				}
			}
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::EDIT,		
			);
			$this->addActivityLog("setup", $log_var);
		
		} else {
			$result = array("error","Sorry, something went wrong while trying to change the list item. Please try again.");
		}
		return $result;
    }
    /**
     * Delete a list item by removing the ACTIVE status and adding a DELETED status - the item should no longer show anywhere within the module
     * @param (INT) id = id of list item to be deleted
     * @return (BOOL) status of success as true/false
     */
    public function deleteListItem($id) {
        /****
         * TO BE PROGRAMMED: UPDATE ... SET status = (status - ACTIVE + DELETED) WHERE id = $id
         */
        $sql = "UPDATE ".$this->getListTable()." SET status = (status - ".MASTER::ACTIVE." + ".MASTER::DELETED.") WHERE id = $id";
		$mar = $this->db_update($sql);
		
		if($mar>0){
	        $sql = "SELECT ".$this->getMyName()." FROM ".$this->getListTable()." WHERE id = $id";
			$name = $this->mysql_fetch_one_value($sql,$this->getMyName());
			$result = array("ok","List item removed successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " deleted (".$name.").",
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::DELETE,	
			);
			$this->addActivityLog("setup", $log_var);
		
		} else {
			$result = array("error","Sorry, something went wrong while trying to remove the list item. Please try again.");
		}
		
		return $result;
    }
    
    /**
     * Deactivate a list item by removing the ACTIVE status and adding INACTIVE status - the item should no longer be availavble in new or edit
     * @param (INT) id = id of list item to be deactivated
     * @return (BOOL) status of success as true/false
     */
    public function deactivateListItem($id) {
        /****
         * TO BE PROGRAMMED: UPDATE ... SET status = (status - ACTIVE + INACTIVE) WHERE id = $id
         */
        $sql = "UPDATE ".$this->getListTable()." SET status = (status - ".MASTER::ACTIVE." + ".MASTER::INACTIVE.") WHERE id = $id";
		$mar = $this->db_update($sql);
		
		if($mar>0){
			$sql = "SELECT ".$this->getMyName()." FROM ".$this->getListTable()." WHERE id = $id";
			$name = $this->mysql_fetch_one_value($sql,$this->getMyName());	
			$result = array("ok","List item deactivated successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " deactivated (".$name.").",
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::DEACTIVATE,	
			);
			$this->addActivityLog("setup", $log_var);
		
		} else {
			$result = array("error","Sorry, something went wrong while trying to deactivate the list item. Please try again.");
		}
		
		return $result;
    }
    /**
     * Restore a deactivated list item by removing the INACTIVE status and adding ACTIVE - the list item should be available for use throughout the module
     * @param (INT) id = id of list item to be restored
     * @return (BOOL) status of success as true/false
     */
    public function restoreListItem($id) {
		
        $sql = "UPDATE ".$this->getListTable()." SET status = (status - ".MASTER::INACTIVE." + ".MASTER::ACTIVE.") WHERE id = $id";
		$mar = $this->db_update($sql);
		
		if($mar>0){
			$sql = "SELECT ".$this->getMyName()." FROM ".$this->getListTable()." WHERE id = $id";
			$name = $this->mysql_fetch_one_value($sql,$this->getMyName());		
			$result = array("ok","List item restored successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'=> "Item " . $id . " restored (".$name.").",
			);
			$log_var = array(
				'section'	=> $this->my_list,
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> MASTER_LOG::RESTORE,		
			);
			$this->addActivityLog("setup", $log_var);
		
		} else {
			$result = array("error","Sorry, something went wrong while trying to restore the list item. Please try again.");
		}
		
		return $result;
		 
    }
        
        
        
        
        
        
        
    /********************************
     * Protected functions: functions available for use in class heirarchy
     ********************************/
     
     
     
     
     
     
     
         
    /****************************
     * Private functions: functions only for use within the class
     *********************/
     
    /**
     * Sets the various variables needed by the class depending on the list name provided
     */
    private function setListDetails($fld="") {
		$this->no_status = false;	
		$this->sort_by = "sort, name";
		$this->sort_by_array = array("sort","name");
		$this->my_name = "name";
		$this->sql_name = "|X|name";
        $this->associatedObject = new MASTER_CONTRACT();
        $this->object_table = $this->associatedObject->getTableName();
		
		//echo "<P>SORT: ".$this->sort_by;
        switch($this->my_list) {
			//New hacky one for customer originator - we want 
            case "customer_originator": 
				$this->sort_by = "tksurname";
                $this->list_table =  "assist_".$this->getCmpCode()."_timekeep";
                $this->fields = array(
                    "tkid",
                    "tktitle",
                    "tkname",
                    "tksurname",
                );
				$this->my_name = "tkname";
				$this->no_status = true;
//              	$this->associatedObject = new MASTER_CONTRACT();
//                $this->object_table =  "assist_".$this->getCmpCode()."_timekeep";
               	$this->object_field = "tkname";
                //$this->object_table = $this->getDBRef()."_list_customer_genders"; 
               	//$this->object_field = "client_name";
                break; 
            case "customer_genders": 
                $this->list_table = $this->getDBRef()."_list_customer_genders"; 
                $this->fields = array(
                    "id",
                    "shortcode",
                    "default_name",
                    "client_name",
                    "description",
                );
				$this->my_name = "client_name";
				$this->sort_by = "sort, if(length(client_name) > 0, client_name, default_name)";
				$this->sort_by_array = array("sort","if(length(|X|.client_name) > 0, |X|.client_name, |X|.default_name)");
//                $this->associatedObject = new MASTER_CONTRACT();
//                $this->object_table = $this->getDBRef()."_list_customer_genders"; 
                $this->object_field = "customer_p_gender";
                break; 
            case "customer_certificate_types": 
                $this->list_table = $this->getDBRef()."_list_customer_certificate_types"; 
                 $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_customer_certificate_types"; 
                $this->object_field = "customer_c_type";
                break; 
            case "assist_master_country":  
            case "list_country":  
            	$this->sort_by = "id";
				$this->sort_by_array = array("id");
                $this->list_table = "assist_".$this->getCmpCode()."_list_country";
                $this->fields = array(
                    "id",
                    "value",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = "assist_".$this->getCmpCode()."_list_country";
                $this->object_field = "customer_p_nation";
                $this->no_status = true;
				$this->my_name = "value";
				$this->sql_name = "|X|value";
                break; 
            case "customer_prefixes":  
			case "list_ppltitle": 
				$this->sort_by_array = array("id");
            	$this->sort_by = "id";
                $this->list_table = "assist_".$this->getCmpCode()."_list_ppltitle";
                $this->fields = array(
                    "id",
                    "value"
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->associatedObject->getTableName();
                //$this->object_table = "assist_".$this->getCmpCode()."_list_ppltitle";
                $this->object_field = $fld;
				$this->no_status = true; 
				$this->my_name = "value";
				$this->sql_name = "|X|value";
				break; 
            case "customer_bee_lvls":  
            	$this->sort_by = "id";
                $this->list_table = $this->getDBRef()."_list_bee_lvls";
                $this->fields = array(
                    "id",
                    "value",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_bee_lvls";
                $this->object_field = "customer_leg_bee_lvl"; 
                break; 
            case "assist_master_finyear_end_months":  
            	$this->sort_by = "id";
				$this->sort_by_array = array("id");
                $this->list_table = "assist_".$this->getCmpCode()."_master_finyear_end_months";
                $this->fields = array(
                    "id",
                    "value",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = "assist_".$this->getCmpCode()."_list_country";
                $this->object_field = "customer_leg_finyear_end";
                //$this->no_status = true;
				$this->my_name = "value";
				$this->sql_name = "|X|value";
                break; 
            /*case "customer_finyear_end_month":  
            	$this->sort_by = "id";
                $this->list_table = $this->getDBRef()."_list_finyear_end_months";
                $this->fields = array(
                    "id",
                    "value",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_finyear_end_months";
                $this->object_field = "customer_leg_finyear_end"; 
                break;*/ 
            case "customer_sectors":  
            	$this->sort_by_array = array("id");
            	$this->sort_by = "id";
                $this->list_table = $this->getDBRef()."_list_business_sectors";
                $this->fields = array(
                    "id",
                    "value",
                    "status",
                );
                $this->my_name = "value";
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_business_sectors";
                $this->object_field = "customer_leg_sector"; 
                break; 
            case "customer_currencies":  
            	$this->sort_by = "id";
                $this->list_table = "assist_".$this->getCmpCode()."_list_currency";
                $this->fields = array(
                    "value",
                    "code",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = "assist_".$this->getCmpCode()."_list_currency";
                $this->object_field = "customer_leg_currency"; 
                break; 
            case "customer_apt_types":  
            	$this->sort_by = "id";
                $this->list_table = $this->getDBRef()."_list_apt_types";
                $this->fields = array(
                    "id",
                    "value",
                    "status",
                );
				$this->my_name = "value";
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_apt_types";
                $this->object_field = "customer_leg_apt_type"; 
                break; 
            case "customer_legal_entities":  
            	$this->sort_by = "id";
                $this->list_table = $this->getDBRef()."_list_legal_entities";
                $this->fields = array(
                    "id",
                    "value",
                    "status",
                );
                $this->my_name = "value";
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_legal_entities";
                $this->object_field = "customer_leg_id";
                break; 
            case "customer_type": 
                $this->list_table = $this->getDBRef()."_list_customer_type"; 
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_customer_type";
                $this->object_field = "customer_type";
                break; 
            case "list_organisation_type": 
                $this->list_table = $this->getDBRef()."_list_organisation_type"; 
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_organisation_type"; 
                $this->object_field = "customer_o_type";
                break; 
            case "customer_category": 
                $this->list_table = $this->getDBRef()."_list_customer_category"; 
                $this->fields = array(
                    "id",
                    "shortcode",
                    "name",
                    "description",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->associatedObject->getTableName(); 
                $this->object_field = $this->associatedObject->getTableField()."_category_id"; 
                break; 
            case "customer_user_status": 
                $this->list_table = $this->getDBRef()."_list_customer_status"; 
				$this->sort_by = "sort, if(length(client_name) > 0, client_name, default_name)";
				$this->sort_by_array = array("sort","if(length(|X|.client_name) > 0, |X|.client_name, |X|.default_name)");
				$this->my_name = "client_name";
                $this->fields = array(
                    "id",
                    "default_name",
                    "client_name",
                    "colour",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_customer_status"; 
                $this->object_field = "customer_user_status";
                break; 
             case "customer_drivecodes":  
            	$this->sort_by = "sort";
                $this->list_table = $this->getDBRef()."_list_drivecodes";
                $this->fields = array(
                    "id",
                    "name",
                    "description",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_drivecodes";
                $this->object_field = "customer_p_drivecode";
                break; 
				
             case "customer_races":  
            	$this->sort_by = "sort";
                $this->list_table = $this->getDBRef()."_list_races";
                $this->fields = array(
                    "id",
                    "name",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_races";
                $this->object_field = "customer_p_race";
                break; 
				
             case "customer_earnings":  
            	$this->sort_by = "sort";
                $this->list_table = $this->getDBRef()."_list_earnings";
                $this->fields = array(
                    "id",
                    "name",
                    "status",
                );
                //$this->associatedObject = new MASTER_CONTRACT();
                //$this->object_table = $this->getDBRef()."_list_earnings";
                $this->object_field = "customer_p_earnings";
                break; 
			case "users": 
				$this->sort_by = "tksurname";
                $this->list_table =  "assist_".$this->getCmpCode()."_timekeep";
                $this->fields = array(
                    "tkid",
                    "tktitle",
                    "tkname",
                    "tksurname",
                );
				$this->my_name = "tkname";
				$this->sql_name = "tkname";
				$this->no_status = true;
//              	$this->associatedObject = new MASTER_CONTRACT();
//                $this->object_table =  "assist_".$this->getCmpCode()."_timekeep";
               	$this->object_field = "tkname";
                //$this->object_table = $this->getDBRef()."_list_customer_genders"; 
               	//$this->object_field = "client_name";
                break; 
        }
		if($this->my_name!="name" && $this->my_name!="value") { $this->sql_name = " if(length(|X|client_name) > 0, |X|client_name, |X|default_name) "; } else {}
		foreach($this->fields as $key){
			if(!isset($this->field_names[$key])) {
				$this->field_names[$key] = $this->default_field_names[$key];
			}
			if(!isset($this->field_types[$key])) {
				$this->field_types[$key] = $this->default_field_types[$key];
			}
		}
		//echo "<P>SORT: ".$this->sort_by;
    }
    
} 

?>