<?php 
//error_reporting(-1);
require_once("../module/autoloader.php");
$result = array("info","Sorry, I couldn't figure out what you wanted me to do.");

/**
 * my_action[0] represents the class
 * my_action[1] represents the activity (add, edit etc)
 */
//echo "<pre>";print_r($_REQUEST);echo "</pre>";

$my_action = explode(".",$_REQUEST['action']);
$class = strtoupper($my_action[0]);
$activity = strtoupper($my_action[1]);
unset($_REQUEST['action']);

$page_direct = isset($_REQUEST['page_direct']) ? $_REQUEST['page_direct'] : false;
unset($_REQUEST['page_direct']);
//$has_attachments = (isset($_REQUEST['has_attachments']) && $has_attachments!=false && $has_attachments!="false") ? $_REQUEST['has_attachments'] : false;
$has_attachments = isset($_REQUEST['has_attachments']) ? $_REQUEST['has_attachments'] : false;
unset($_REQUEST['has_attachments']);
$attach = array();


switch($class) {
	case "DISPLAY":
		$object = new MASTER_DISPLAY();
		break;
	/**
	 * Miscellaneous objects
	 */
	case "LOG":
		$log_table = $_REQUEST['log_table'];
		unset($_REQUEST['log_table']);
		$object = new MASTER_LOG($log_table);
		break;
	case "CONTRACT_ASSURANCE":
		$object = new MASTER_ASSURANCE_CONTRACT();
		break;
	case "DELIVERABLE_ASSURANCE":
		$object = new MASTER_ASSURANCE_DELIVERABLE();
		break;
	case "ACTION_ASSURANCE":
		$object = new MASTER_ASSURANCE_ACTION();
		break;
	/**
	 * Setup objects
	 */
	case "CONTRACTOWNER":
		$object = new MASTER_CONTRACT_OWNER();
		break;
	case "USERACCESS":
		$object = new MASTER_USERACCESS();
		break;
	case "LISTS":
		$list_id = $_REQUEST['list_id'];
		unset($_REQUEST['list_id']);
		$object = new MASTER_LIST($list_id);
		break;
	case "MENU":
		$object = new MASTER_MENU();
		break;
	case "NAMES":
		$object = new MASTER_NAMES();
		break;
	case "GLOSSARY":
		$object = new MASTER_GLOSSARY();
		break;
	case "HEADINGS":
		$object = new MASTER_HEADINGS();
		break;
	case "PROFILE":
		$object = new MASTER_PROFILE();
		break;
	case "SETUPPREFERENCES":
		$object = new MASTER_SETUP_PREFERENCES();
		break;
	case "SETUPNOTIFICATIONS":
		$object = new MASTER_SETUP_NOTIFICATIONS();
		break;
	/**
	 * Module objects
	 */
	case "CONTRACT":
		$object = new MASTER_CONTRACT();
		break;
	case "CERTS":
		$object = new MASTER_CONTRACT();
		break;
	case "DELIVERABLE":
		$object = new MASTER_DELIVERABLE();
		break;
	case "ACTION":
		$object = new MASTER_ACTION();
		break;
	case "FINANCE":
		$object = new MASTER_FINANCE();
		break;
	case "TEMPLATE":
		$object = new MASTER_TEMPLATE();
		break;
	case "FINANCE_BUDGET":
		$object = new MASTER_FINANCE_BUDGET();
		break;
	case "FINANCE_ADJUSTMENT":
		$object = new MASTER_FINANCE_ADJUSTMENT();
		break;
	case "FINANCE_EXPENSE":
		$object = new MASTER_FINANCE_EXPENSE();
		break;
	case "FINANCE_INCOME":
		$object = new MASTER_FINANCE_INCOME();
		break;
	case "FINANCE_RETENTION":
		$object = new MASTER_FINANCE_RETENTION();
		break;
}
$result[1].=":" . (is_array($my_action) ? implode(".", $my_action) : $my_action ) . ":";
if(isset($object)) {
	switch($activity) {
		case "DELETE_ATTACH":
			$object_id = $_REQUEST['object_id'];
			$i = $_REQUEST['i'];
			$page_activity = $_REQUEST['activity'];
			$attach = $object->getAttachmentDetails($object_id,$i,$page_activity);
			$folder = $object->getParentFolderPath();
			$old = $folder."/".$attach['location']."/".$attach['system_filename'];
			$dest_folder = $object->getDeletedFolder();
			if(file_exists($old)) {
				$new_path = $dest_folder."/".$object->getDeletedFileName($class,$attach['system_filename']);
				if(copy($old,$new_path)) {
					unlink($old);
				}
				//activity log
				$result = $object->deleteAttachment($object_id,$i,$page_activity);
				//$result = array("info","file_exists");
			} else {
				$result = array("error","File could not be found.  Please reload the page and try again.",$old);
			} 
			//$result = array("error",$page_activity);
			break;
		case "GETLISTTABLEHTML":
			unset($_REQUEST['options']['set']);
			$button = $_REQUEST['button'];
			$data = $object->getObject($_REQUEST['options']);
			//$result = $data;
			//$result = array(serialize($data));
			$displayObj = new MASTER_DISPLAY();
			$display = $displayObj->getListTableRows($data, $button);
			$result = array('display'=>$display['display'],'paging'=>$data['paging'],'spage'=>serialize($data['paging']));
			break;
		case "GET":
			//if($class=="LOG") {
			//	$result = array("ok","log.get");
			//} else {
				$result = $object->getObject($_REQUEST);
			//}
			break;
		case "ADD":
			$data = $object->addObject($_REQUEST);
			$result=$data;
			if($data[0]=="ok") {
				if($has_attachments) {
					$attach = processAttachments($activity, $object, $data);
					//attachments aren't logged on add - only the fact that the object was created is logged
					//$data['log_var']['changes']['attachments'] = $attach['attachments'];
				//} else {
				}
				$result[] = $attach;
				if(!isset($data['add_log']) || $data['add_log']===true) {
					$object->addActivityLog(strtolower($object->getMyLogTable()),$data['log_var']);
				}
			} else {
				$result = $data;
			}
			break;
		case "SIMPLEADD":
			$result = $object->addObject($_REQUEST);
			break;
		case "SIMPLEEDIT":
			$result = $object->editObject($_REQUEST,$attach);
			break;
		case "CERT_ADD":
			$result = $object->addCertObject($_REQUEST,$attach);
			break;
		case "CERT_EDIT":
			$result = $object->editCertObject($_REQUEST,$attach);
			break;
		case "CERT_DEL":
			$result = $object->delCertObject($_REQUEST);
			break;
		case "EDIT":
			if($has_attachments) {
				$res = processAttachments($activity, $object,array('object_id'=>$_REQUEST['object_id']));
				$attach = $res['attachments'];
			}
			$result = $object->editObject($_REQUEST,$attach);
			break;
		case "EDIT|ED":
			$result = $object->editObject($_REQUEST,$attach);
			break;
		case "ADD|ED":
			$result = $object->editObject($_REQUEST,$attach);
			break;
		case "UPDATE":
			if($has_attachments) {
				$res = processAttachments($activity, $object,array('object_id'=>$_REQUEST['object_id']));
				$attach = $res['attachments'];
			}
			$result = $object->updateObject($_REQUEST,$attach);
			break;
		case "APPROVE":
			$result = $object->approveObject($_REQUEST);
			break;
		case "DECLINE":
			$result = $object->declineObject($_REQUEST);
			break;
		case "UNLOCK_APPROVE":
			$result = $object->unlockApprovedObject($_REQUEST);
			break;
		case "UNLOCK":
			$result = $object->unlockObject($_REQUEST);
			break;
		case "DELETE":
			$result = $object->deleteObject($_REQUEST);
			break; 
		case "DEACTIVATE":
			$result = $object->deactivateObject($_REQUEST);
			break; 
		case "RESTORE":
			$result = $object->restoreObject($_REQUEST);
			break;
		case "RECIPIENTS":
			$result = $object->getObjectRecipients($_REQUEST);
			break;
		default:
			$result = call_user_func(array($object, $activity),$_REQUEST);
			break; 
	}
}

//echo ":".$has_attachments.":";
if($has_attachments==false) {
	echo json_encode($result);
} else {
	$page_direct.="r[]=".$result[0]."&r[]=".$result[1];
	//echo "java script redirect to: ".$page_direct;
	echo "<script type=text/javascript>window.parent.location.href = '".$page_direct."';</script>";
	//$object->arrPrint($result);
}










/*************** Attachment processing ******************/
function processAttachments($activity,$object,$data) {
	//ASSIST_HELPER::arrPrint($data);
	$object_id = $data['object_id'];
	$default_system_filename = $object_id."_".date("YmdHis")."_".($activity=="UPDATE" ? "update_" : "");
	$result = array();
			$original_filename = "";
			$system_filename = "";
			if(isset($_FILES)) {
				$result[] = "files are set";
				$save_folder = $object->getStorageFolder();
				$object->checkFolder($save_folder);
				$result[] = $save_folder." folder check complete";
				$folder = $object->getFullFolderPath();
				$result[] = $folder;
				//Check for existing attachments
				switch($activity) {
					case "ADD":
						$i = 0;
						$current_attach = array();
						break;
					case "UPDATE":
					case "EDIT":
						$current_attach = $object->getAttachmentDetails($object_id,"all",$activity);
						//ASSIST_HELPER::arrPrint($current_attach);
						if(count($current_attach)>0) {
							$keys = array_keys($current_attach);
							$i = max($keys);
							$i++;
						} else {
							$i = 0;
						}
						break;
				}
				$new_attach = array();
				/*$record = $object->getUpdateRecord($obj_id,$time_id);
				if(!isset($record['attachment']['attach']) || count($record['attachment']['attach'])==0) {
					$i = 0;
				} else {
					foreach($record['attachment']['attach'] as $i=>$a) {
						//
					}
					$i++;
				}*/
				$result[] = $i;
				$result[]=count($_FILES);
				$result['FILES']=$_FILES;
				foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
					$x = array('id'=>$key,'tmp_name'=>$tmp_name);
					$x['error'] = $_FILES['attachments']['error'][$key];
					if($_FILES['attachments']['error'][$key] == 0) {
						$original_filename = $_FILES['attachments']['name'][$key];
						$ext = substr($original_filename, strrpos($original_filename, '.') + 1);
						$parts = explode(".", $original_filename);
						$file = $parts[0];
						$system_filename = $default_system_filename.$i.".".$ext; 
						$x['sys'] = $system_filename;
						$full_path = $folder."/".$system_filename;
						$x['path'] = $full_path;
						$new_attach[$i] = array('status'=>MASTER::ACTIVE,'location'=>$save_folder,'system_filename'=>$system_filename,'original_filename'=>$original_filename,'ext'=>$ext);
						$x['result'] = move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
						$i++;
						$result[] = $x;
					}
				}
				$result['attachments'] = $new_attach;
				$object->saveAttachments($activity,$object_id,array_merge($current_attach,$new_attach));
			}
//$object->arrPrint($result);
	return $result;
}


function getSaveFolder($object) {
	return $object->getStorageFolder();
}

function getFullFolderPath($object) {
	$folder = $object->getFullFolderPath();
	return $folder;	
}


function getDeletedFolder($object) {
	$folder = $object->getDeletedFolder();
}


function getParentFolderPath($object) {
	$folder = $object->getParentFolderPath();
	return $folder;	
}
?>