<?php

$tab = strtoupper($_REQUEST['tab']);

$form_object_type = "FINANCE_".$tab;
$object_type = $tab;

switch($tab) {
	case "BUDGET":
		$formObject = new CNTRCT_FINANCE_BUDGET();
		break;
	case "ADJUSTMENT":
		$formObject = new CNTRCT_FINANCE_ADJUSTMENT();
		break;
	case "EXPENSE":
		$formObject = new CNTRCT_FINANCE_EXPENSE();
		break;
	case "INCOME":
		$formObject = new CNTRCT_FINANCE_INCOME();
		break;
	case "RETENTION":
		$formObject = new CNTRCT_FINANCE_RETENTION();
		break;
}

$form_object_id = 0;
$parent_object_type = "CONTRACT";
$parentObject = new CNTRCT_CONTRACT();
$parent_object_id = $object_id;
$page_action = "ADD";
$page_redirect_path = $page_redirect_path."&tab=".strtolower($tab)."&";

$edit_list_objects = $formObject->getListObjects($parent_object_id);


$delObject = new CNTRCT_DELIVERABLE();
$add_deliverables = $delObject->getOrderedObjects($parent_object_id);

//$headObject = new CNTRCT_HEADINGS();
//$del_headings = $headObject->getMainObjectHeadings("FINANCE_DELIVERABLE_".$object_type,"ALL","MANAGE","",true);


?>
<div id="accordion" class=accordion>
	<h3 id=head_initiate next_index=1>Initiate</h3>
	<div id=initiate>
		<?php ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array()); ?>
		<table class=tbl-container>
			<tr>
				<td width=48%>
					<?php
					$contract = $displayObject->getDetailedView("CONTRACT", $object_id,false,false,false,false,true);
					echo $contract['display'];
					$js.=$contract['js'];
					$summary_table = $displayObject->getSummaryTable($form_object_type, $formObject, $parent_object_id);
					echo $summary_table['display'];
					$js.=$summary_table['js'];
					?>
				</td>
				<td width=4%>
				</td>
				<td width=48%><h2><?php echo $helper->getActivityName("add")." ".$helper->getObjectName($form_object_type); ?></h2>
					<?php
					$display = $displayObject->getFinanceObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path);
					echo $display['display'];
					$js.=$display['js'];
					?>
					
					<?php
					echo "<h2>".$helper->getDeliverableObjectName()."</h2>";
					
					//$js.=$displayObject->drawListTable($edit_list_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_list"));
					?>
					<table id=tbl_finance_deliverables class=finance-list width=100%>
						<tr>
							<th></th>
							<th width=120px>Current <?php echo $helper->getObjectName($form_object_type); ?></th>
							<th width=120px>New <?php echo $helper->getObjectName($form_object_type); ?></th>
							<th width=120px>New Total <?php echo $helper->getObjectName($form_object_type); ?></th>
						</tr>
						<?php
						foreach($add_deliverables[0] as $i => $del) {
							if($del['del_type']=="DEL") {
								echo "
								<tr>
									<td class=b>".$del['reftag'].": ".$del['name']."</td>
									<td>0.00</td>
									<td><input type=text value=0 size=15 /></td>
									<td>0.00</td>
								</tr>";
							} else {
								echo "
								<tr>
									<td class=b>".$del['reftag'].": ".$del['name']."</td>
									<td>0.00</td>
									<td>0.00</td>
									<td>0.00</td>
								</tr>";
								if(isset($add_deliverables[$i])) {
									foreach($add_deliverables[$i] as $ci => $cdel) {
										echo "
										<tr>
											<td class=i>&nbsp;&nbsp;+&nbsp;".$cdel['reftag'].": ".$cdel['name']."</td>
											<td>0.00</td>
											<td><input type=text value=0 size=15 /></td>
											<td>0.00</td>
										</tr>";
									}
								}
							}
						}
						?>
					</table>
					
				</td>
			</tr>
			<tr>
				<td colspan=3>

				</td>
			</tr>
		</table>
	</div>

	<h3 id=head_edit next_index=0>Edit</h3>
	<div id=edit>
		<table class=tbl-container>
			<tr>
				<td width=48%>
					<?php
					//$contract = $displayObject->getDetailedView("CONTRACT", $object_id,false,false,false,false,true);
					echo $contract['display'];
					//$js.=$contract['js'];
					?>

				</td>
				<td width=4%>
				</td>
				<td width=48%><?php
					echo $summary_table['display'];
				?></td>
			</tr>
			<tr>
				<td colspan=3>
					<?php
					$js.=$displayObject->drawListTable($edit_list_objects,array('value'=>$helper->getActivityName("edit"),'class'=>"btn_list"));
					?>
				</td>
			</tr>
		</table>
	</div>
</div>
<script type="text/javascript">
$(function() {	
	$('#accordion').accordion({
		active: 0,
		event: false,
		heightStyle: 'fill'
	}).children('h3').css({'font-family':'Verdana','font-weight':'bold','font-size':'110%','line-height':'125%'});
	$('h3').click(function() {
		if($(this).parent().prop('id')=='accordion') {
			var next_index = $(this).attr('next_index');
			$relatedContent = $(this).next();
			if($relatedContent.is(':visible')) {
				if(next_index==0) {
					$("#accordion").accordion("option","active",0);
				} else {
					$("#accordion").accordion("option","active",1);
				}
			} else {
				if(next_index==0) {
					$("#accordion").accordion("option","active",1);
				} else {
					$("#accordion").accordion("option","active",0);
				}
			}
		}
	});
});
</script>