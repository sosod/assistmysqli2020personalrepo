<?php
$page_redirect_path = "new_contract_copy.php?";
$page_action = "COPY";

include("inc_header.php");

$displayObject->disableAttachButtons();

$contract_id = $_REQUEST['contract_id'];
			$options = array(
				'type'=>"LIST",
				'section'=>"NEW",
				'page'=>"COPY",
			);
$contractObject = new MASTER_CONTRACT($contract_id);
$deliverableObject = new MASTER_DELIVERABLE();
		$deliverables = $deliverableObject->getOrderedObjects($contract_id);
		$del_name = $helper->getDeliverableObjectName(true);
		$del_types = $deliverableObject->getAllDeliverableTypes();
$actionObject = new MASTER_ACTION();
		$action_name = $helper->getActionObjectName(true);



?>

<table class=tbl-container>
	<tr>
		<td width=47%>
			<?php
			$displayContract = $displayObject->getObjectForm("CONTRACT", $contractObject, $contract_id, "", null, 0, $page_action, $page_redirect_path,false,$contractObject->getRefTag().$contract_id);
			$js.= $displayContract['js'];
			echo $displayContract['display'];
			?>
		</td>
		<td width=5%></td>
		<td width=48%>summary</td>
	</tr>
	<tr>
		<td colspan=3>
			<hr />
			<h2><?php echo $del_name." & ".$action_name; ?></h2>
			<hr />
		</td>
	</tr>
			<?php
			foreach($deliverables[0] as $key=>$item){
				//1 detail display per deliverable
				$displayDel = $displayObject->getObjectForm("DELIVERABLE",$deliverableObject,$key,"CONTRACT",$contractObject,$contract_id,$page_action,$page_redirect_path,false,$deliverableObject->getRefTag().$key);
				$js.=$displayDel['js'];
				if($item['del_type'] !== "MAIN"){
					$actions = $actionObject->getObject(array('type'=>"ALL",'section'=>"ACTIVE",'deliverable_id'=>$key,'limit'=>false,'page'=>"ALL")); 
					echo "<tr><td colspan=3>
					<div class=left_div style='float: left; border:1px solid #cc0001;'>
						<h3>".$del_types[$item['del_type']]."</h3>
						".$displayDel['display']."
					</div>
					<div class=right_div style='float: right; border:1px solid #fe9900;'>
						<h3 style='margin-bottom: 5px;'>".$action_name."</h3>";
							$js.=$displayObject->drawListTable($actions,false, "", array(), true);
							echo "
					</div>";
					//$gchild_objects = $gchildObject->getObject(array('type'=>"ALL",'section'=>"ACTIVE",'deliverable_id'=>$key,'limit'=>false,'page'=>"ALL")); 
					//$js.=$displayObject->drawListTable($gchild_objects,false);
				}else{
					echo "
					<tr><td>
						<h3>".$del_types[$item['del_type']]."</h3>
						".$displayDel['display']."
					</td><td></td><td>";
					if(isset($deliverables[$key])){
						foreach($deliverables[$key] as $key2=>$item2){
							$displayDel2 = $displayObject->getObjectForm("SUB_DELIVERABLE",$deliverableObject,$key2,"CONTRACT",$contractObject,$contract_id,$page_action,$page_redirect_path,false,$deliverableObject->getRefTag().$key2);
							$actions = $actionObject->getObject(array('type'=>"ALL",'section'=>"ACTIVE",'deliverable_id'=>$key2,'limit'=>false,'page'=>"ALL")); 
							$js.=$displayDel2['js'];
							echo "</td></tr><tr><td colspan=3>
							<div class=left_div style='float: left; border:1px solid #cc0001;'>
								<h3>".$del_types[$item2['del_type']]."</h3>
								".$displayDel2['display']."
							</div>
							<div class=right_div style='float: right; border:1px solid #fe9900;'>
								<h3 style='margin-bottom: 5px;'>".$action_name."</h3>";
							$js.=$displayObject->drawListTable($actions,false, "", array(), true);
							echo "
							</div>";
						}
					}
				}
				echo "</td></tr>";
			}
			?>
		</td>
	</tr>
	
</table>






<style type=text/css>

</style>
<script type="text/javascript" >
$(function(){
	<?php echo $js; ?>
	
	var left_div = 0;
	var right_div = 0;
	$(window).resize(function() {
		var screen = AssistHelper.getWindowSize();
		left_div = Math.round(screen['width']*0.47);
		if(left_div<500) { left_div = 500; }
		right_div = Math.round(screen['width']*0.48);
		if(right_div<550) { right_div = 550; }
		$(".left_div").css("width",left_div+"px");
		$(".right_div").css("width",right_div+"px");
	});
	
	$(window).trigger("resize");
	$("table.sub_indent").css("width",(left_div-20)+"px").parent().css("padding-left","20px");
});
</script>