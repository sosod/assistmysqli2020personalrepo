<?php
include("inc_header.php");

//$helper->arrPrint($_REQUEST);

$object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "ACTION";
$object_id = $_REQUEST['object_id'];

$page_action = $_REQUEST['page_action'];
//ASSIST_HELPER::arrPrint($page_action);
$real_action = strtoupper($_REQUEST['page_action'])=="APPROVED"?"unlock":$_REQUEST['page_action'];

$page_redirect = "manage_approve_".strtolower($object_type).".php?page_action=".$page_action;

switch($object_type) {
	case "CONTRACT":
		$child_type = "DELIVERABLE";
		$childObject = new MASTER_DELIVERABLE();
		$child_options = array('type'=>"LIST",'section'=>"MANAGE",'contract_id'=>$object_id);
		$child_objects = $childObject->getObject($child_options); 
		$child_redirect = "manage_view_object.php?object_type=DELIVERABLE&object_id=";
		$child_name = $helper->getDeliverableObjectName(true);
		break;
	case "DELIVERABLE":
		$child_type = "ACTION";
		$childObject = new MASTER_ACTION();
		$child_options = array('type'=>"LIST",'section'=>"MANAGE",'deliverable_id'=>$object_id);
		$child_objects = $childObject->getObject($child_options); 
		$child_redirect = "manage_view_object.php?object_type=ACTION&object_id=";
		$child_name = $helper->getActionObjectName(true);
		break;
	case "ACTION":
		$child_redirect = "";
		break;
}
$logObject = new MASTER_LOG(strtolower($object_type));
$audit_log = $logObject->getObject(array('object_id'=>$object_id));
$audit_log = $audit_log[0];

$approve = $helper->getActivityName("approve");
$decline = $helper->getActivityName("decline");
switch($page_action) {
	case "APPROVED":
		$form_heading = $decline;
		$edit = $helper->getActivityName("edit");
		$restore = $helper->getActivityName("restore");
		$update = $helper->getActivityName("update");
		$form_message = $helper->getDisplayResult(array("warn","This will reverse the previous approval of the ".$helper->getActionObjectName()." and $restore it for $edit and $update."));
		$form_buttons = "<input type=button value='".$decline."' class='isubmit unlock' />";
		break;
	case "APPROVE":
	default:
		$form_heading = $approve." / ".$decline;
		$form_message = "";
		$form_buttons = "<input type=button value='".$approve."' class='isubmit approve' /> <span class=float><input type=button value='".$decline."' class='idelete decline'></span>";
		break;
}



?>
<table class=tbl-container>
	<tr>
		<td width=47%><?php 
			$js.= $displayObject->drawDetailedView($object_type, $object_id, !($object_type=="CONTRACT"),false,($object_type=="CONTRACT")); 
			echo "<h2>Activity Log</h2>".$audit_log;
		?></td>
		<td width=5%>&nbsp;</td>
		<td ><h2><?php echo $form_heading; ?></h2><table class='tbl-container not-max'><tr><td>
			<form name=frm_approve>
				<input type=hidden name=object_id value=<?php echo $object_id; ?> />
				<input type=hidden name=page_action value=<?php echo $page_action; ?> />
			<?php echo $form_message; ?>
			<table class=form>
				<tr>
					<th>Response:</th>
					<td><?php $js.=$displayObject->drawFormField("TEXT",array('id'=>"approve_response",'name'=>"approve_response",'rows'=>10,'cols'=>75)); ?></td>
				</tr>
				<tr>
					<th>Notifications:</th>
					<td>
						<input hidden req='1' title='This is a required field.' id='notification_chooser' name='notification_chooser' value=0 />
						<div id='notification_btns' style=''>
							<input type='radio' checked='checked' name='radio' id='mail_btn'><label for='mail_btn'>Email</label>
							<input type='radio' name='radio' id='both_btn'><label for='both_btn'>Email and SMS</label>
						<!--		<input type='radio' name='radio' id='sms_btn'><label for='sms_btn'>SMS</label> &nbsp; -->
							<div id='why_no_sms' style='display:inline; width:110px;'>
							</div>
						</div>
						<div id='notification_recipients'>
							<p>Recipients go here:</p>
						</div>
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<?php echo $form_buttons; ?>
					</td>
				</tr>
			</table>
			<?php ASSIST_HELPER::displayResult(array("info","A response is required when if you choose to ".$helper->getActivityName("decline")." the ".$helper->getObjectName($object_type).".")); ?>
		</form></td></tr></table>
		</td>
	</tr>
	<?php if(isset($childObject)) { ?>
	<tr>
		<td colspan=3>
			<h2><?php echo $child_name; ?></h2>
			<?php $js.=$displayObject->drawListTable($child_objects,array('value'=>"View",'class'=>"btn_view"),$child_type,$child_options); ?>
		</td>
	</tr>
	<?php } ?>
</table>
<script type=text/javascript>
var url = "<?php echo $child_redirect; ?>";
var page_redirect = "<?php echo $page_redirect; ?>";
var ot = "<?php echo strtoupper($object_type); ?>";
$(function() {
	<?php 	echo $js;	
	echo"
	
	$('#notification_btns').buttonset();
				$('#mail_btn').button({
					icons:{
						primary: 'ui-icon-mail-closed'
					}
				});
				$('#both_btn').button({
					icons:{
						primary: 'ui-icon-mail-closed',
						secondary: 'ui-icon-signal'
					}
				});
		//		$('#sms_btn').button({
		//			icons:{
		//				secondary: 'ui-icon-signal'
		//			}
		//		});
				var useSMS = AssistHelper.doAjax('inc_controller.php?action=SetupNotifications.Approve&activity=".$real_action."&object=action');
				console.log(useSMS);
				var recipes = AssistHelper.doAjax('inc_controller.php?action=Action.Recipients&id=".$object_id."&activity=".$real_action."');
				console.log(recipes);
				$('#notification_recipients').html('');
				
				for(x in recipes){
					if(recipes[x] instanceof Array){
						$('#notification_recipients').append('<p>'+x+'s:');
						for(var i=0; i< recipes[x].length; i++){
							$('#notification_recipients').append('&nbsp;&nbsp;&nbsp;'+recipes[x][i].name+' - '+recipes[x][i].email+'<br>');
						}
						$('#notification_recipients').append('</p>');
					}else{
						$('#notification_recipients').append('<p>'+x+':</p> &nbsp;&nbsp;&nbsp;'+recipes[x].name+' - '+recipes[x].email+'<br>');
					}
				}
				
				$('#mail_btn').click(function(){
					$('#notification_chooser').val('email');
					$('#notification_recipients').html('');
					if(recipes[x] instanceof Array){
						$('#notification_recipients').append('<p>'+x+'s:');
						for(var i=0; i< recipes[x].length; i++){
							$('#notification_recipients').append('&nbsp;&nbsp;&nbsp;'+recipes[x][i].name+' - '+recipes[x][i].email+'<br>');
						}
						$('#notification_recipients').append('</p>');
					}else{
						$('#notification_recipients').append('<p>'+x+':</p> &nbsp;&nbsp;&nbsp;'+recipes[x].name+' - '+recipes[x].email+'<br>');
					}
				});
				$('#both_btn').click(function(){
					$('#notification_chooser').val('both');
					$('#notification_recipients').html('');
					for(x in recipes){
						if(recipes[x] instanceof Array){
							$('#notification_recipients').append('<p>'+x+'s:');
							for(var i=0; i< recipes[x].length; i++){
								$('#notification_recipients').append('&nbsp;&nbsp;&nbsp;<input hidden id=\"recipient_mobile_'+recipes[x][i].tkid+'\" name=\"recipient_mobile['+recipes[x][i].tkid+']\" value=\"'+recipes[x][i].mobile+'\" />'+recipes[x][i].name+' - '+recipes[x][i].email+' - '+recipes[x][i].mobile+'<br>');
							}
							$('#notification_recipients').append('</p>');
						}else{
							$('#notification_recipients').append('<input hidden id=\"recipient_mobile_'+recipes[x].tkid+'\" name=\"recipient_mobile['+recipes[x].tkid+']\" value=\"'+recipes[x].mobile+'\" /><p>'+x+':</p> &nbsp;&nbsp;&nbsp;'+recipes[x].name+' - '+recipes[x].email+' - '+recipes[x].mobile+'<br>');
						}
					}
				});
				if(useSMS[0] == false){
					$('#sms_btn, #both_btn').button('option', 'disabled', true);
					$('#mail_btn').attr('checked','checked');
					$('#notification_btns').buttonset('refresh');
					//$('#why_no_sms').text('SMSs have not been enabled for this situation. Contact your Assist Administrator for assistance.');
				}else{
					//$('#why_no_sms').text(useSMS[1]);
				}";
	?>
	
	$("input:button.isubmit, input:button.idelete").click(function() {
		AssistHelper.processing();
		var valid8 = true;
		if($(this).hasClass("decline") || $(this).hasClass("unlock")) {
			if($("#approve_response").val().length==0) {
				valid8 = false;
				AssistHelper.finishedProcessing("error","Please enter a response.");
			} else {
				var action = ot+"."+($(this).hasClass("unlock") ? "UNLOCK_APPROVE" : "DECLINE");
			}
		} else {
			var action = ot+".APPROVE";
		}
		if(valid8==true) {
			var dta = AssistForm.serialize($("form[name=frm_approve]"));
			//console.log(dta);
			var result = AssistHelper.doAjax("inc_controller.php?action="+action,dta);
			if(result[0]=="ok") {
				document.location.href = page_redirect+"&r[]="+result[0]+"&r[]="+result[1];
			} else {
				AssistHelper.finisedProcessing(result[0],result[1]);
			}
		}
	});
});
</script>
<?php
//markTime("end");
?>