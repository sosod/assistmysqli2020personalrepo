<?php



/**************
 * CODE TO MEASURE PAGE LOAD TIME 
 * ******************
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

function markTime($s) {
	global $start;
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p>'.$s.' => '.$total_time.' seconds.';
	
}
*/

//error_reporting(-1);

require_once ("../module/autoloader.php");
//marktime("autoloader");
/*********
 * Get file name to process breadcrumb path within module.
 */
$self = $_SERVER['PHP_SELF'];
$self = explode("/",$self);
$self = explode(".",end($self));
$page = $self[0];
if(isset($my_page)) {
	$page.="_".$my_page;
}
$navigation_path = explode("_",$page); //ASSIST_HELPER::arrPrint($navigation_path);
//marktime("file name");
/********
 * Check for redirects
 * Note: Admin redirect is determined within the admin.php file as it requires a user access validation check
 */
$redirect = array(
    'new'				=> "new_contract_create",
    'new_contract'		=> "new_contract_create",
    'new_activate'		=> "new_activate_waiting",
    'manage'			=> "manage_view",
    'manage_update'		=> "manage_update_action",
    'manage_edit'		=> "manage_edit_deliverable",
    'manage_approve'	=> "manage_approve_action",
    'manage_assurance'	=> "manage_assurance_contract",
    'admin'				=> "admin_update",
    'admin_update'		=> "admin_update_action",
    'admin_template'	=> "admin_template_new",
    'report'			=> "report_generate_contract",
    'report_generate'	=> "report_generate_contract",
    'search'			=> "search_contract",
    'setup'				=> "setup_defaults"
);
if(isset($redirect[$page])) {
    header("Location:".$redirect[$page].".php");
}
//marktime("redirect");
/************
 * Process Navigation buttons and Breadcrumbs
 */
$menuObject = new MASTER_MENU();
$menu = $menuObject->getPageMenu($page); 
//ASSIST_HELPER::arrPrint($menu);
 
if($navigation_path[0]=="admin" && $navigation_path[1]=="update" && $menu['buttons'][1][0]['link']!="admin_update.php") {
	header("Location:".$menu['buttons'][1][0]['link']);
} elseif($navigation_path[0]=="new" && $navigation_path[1]=="contract" && $menu['buttons'][1][0]['link']!="new_contract.php") {
	header("Location:".$menu['buttons'][1][0]['link']);
}
 //marktime("menu");
  
/***********
 * Start general header
 */

$headingObject = new MASTER_HEADINGS();
$displayObject = new MASTER_DISPLAY();
$nameObject = new MASTER_NAMES();
$helper = new MASTER();

$s = array("/library/js/assist.ui.paging.js","/library/jquery-plugins/jscolor/jscolor.js","/".$helper->getModLocation()."/common/cntrcthelper.js","/".$helper->getModLocation()."/common/cntrct.js","/".$helper->getModLocation()."/common/hoverIntent.js");
if(isset($scripts)) {
	$scripts = array_merge(
		$s,
		$scripts
	);
} else {
	$scripts = $s;
}

ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);

//$helper->arrPrint($_REQUEST);

$contract_object_name = $helper->getContractObjectName();
$deliverable_object_name = $helper->getDeliverableObjectName();
$action_object_name = $helper->getActionObjectName();
$finance_object_name = $helper->getObjectName("FINANCE");
//$template_object_name = $helper->getObjectName("TEMPLATE");

$js = ""; //to catch any js put out by the various display classes

//marktime("general");

/*********
 * Module-wide Javascript
 */
?>
<script type="text/javascript" >
$(document).ready(function() {
    window.contract_object_name = AssistString.decode("<?php echo ASSIST_HELPER::code($contract_object_name); ?>");
    window.deliverable_object_name = AssistString.decode("<?php echo ASSIST_HELPER::code($deliverable_object_name); ?>");
    window.action_object_name = AssistString.decode("<?php echo ASSIST_HELPER::code($action_object_name); ?>");
    window.finance_object_name = AssistString.decode("<?php echo ASSIST_HELPER::code($finance_object_name); ?>");
    

	//$("table.tbl-container, table.tbl-container td:first").addClass("noborder");
	//$("table.tbl-container tr").find("td:first").addClass("noborder");
	//$("table.tbl-subcontainer").parent("td").addClass("noborder");
	//$("table.sub_table").addClass("noborder").find("td").addClass("noborder");
	$("table.tbl-container").addClass("noborder");
	$("table.tbl-container:not(.not-max)").css("width","100%");
	$("table.tbl-container td").addClass("noborder").find("table:not(.tbl-container) td").removeClass("noborder");
	$("table.th2").find("th").addClass("th2");
	$("table tr.th2").find("th").addClass("th2");
	$("table.tbl_audit_log").css("width","100%").find("td").removeClass("noborder");
	//$("h2").css("margin-top","0px");
	$("table.tbl-subcontainer, table.tbl-subcontainer td").addClass("noborder");



	//FINANCE specific formatting
	$("table.finance").addClass("form");
	$("table.finance th.DEFAULT").css({"background-color":"#FFFFFF","color":"#000000","border":"1px solid #ababab"});
	
	$("table.finance th.SUB_TOTAL").addClass("th2");
	$("table.finance td.SUB_TOTAL").css({"font-weight":"bold"});
	
	$("table.finance th.TOTAL").parent().addClass("total");
	//$("table.finance td.TOTAL").addClass("th2");
	
	$("table.finance-list tr:gt(0)").find("td:gt(0)").addClass("right");

});

</script>
<style type="text/css">
	//.noborder { border: 2px dashed #009900; }
	//table.tbl_audit_log { width: 100%; }
	//table.attach { border: 0px dashed #009900; }
	//table.attach td { 
		border: 0px dashed #009900;
		vertical-align: middle;
		padding: 1px; 
	}
	.button_class, .ui-widget.button_class {
		font-size: 75%;
		padding: 1px;
	}
	tr.total th {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
		border: 1px solid #ababab;
	}
	tr.total td {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
	}
		
	
</style>
<!-- hover-popup styles -->
<style type="text/css">
	.help-tip{
		//position: absolute;
		top: 18px;
		right: 18px;
		text-align: center;
		background-color: #FC9A04;
		border-radius: 50%;
		width: 20px;
		height: 20px;
		font-size: 14px;
		line-height: 21px;
		cursor: pointer;
	}
	
	.help-tip:before{
		content:'?';
		font-weight: bold;
		color:#fff;
	}
	
	.help-tip:hover p{
		display:block;
		//transform-origin: 100% 0%;
	
		-webkit-animation: fadeIn 0.3s ease-in-out;
		animation: fadeIn 0.3s ease-in-out;
	
	}
	
	.help-tip p{	/* The tooltip */
		display: none;
		text-align: left;
		background-color: #1E2021;
		padding: 20px;
		width: 300px;
		//position: absolute;
	    margin-left: 80px;
   		margin-top: -48px;
		border-radius: 3px;
		box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
		right: -4px;
		color: #FFF;
		font-size: 13px;
		line-height: 1.4;
	}
	
	.help-tip p:before{ /* The pointer of the tooltip */
		//position: absolute;
		content: '';
		width:0;
		height: 0;
		border:6px solid transparent;
		border-bottom-color:#1E2021;
	    margin-left: -24px;
		//right:10px;
		//top:-12px;
	}
	
	.help-tip p:after{ /* Prevents the tooltip from being hidden */
		width:100%;
		height:40px;
		content:'';
		//position: absolute;
		top:-40px;
		left:0;
	}
	
	/* CSS animation */
	
	@-webkit-keyframes fadeIn {
		0% { 
			opacity:0; 
			transform: scale(0.6);
		}
	
		100% {
			opacity:100%;
			transform: scale(1);
		}
	}
	
	@keyframes fadeIn {
		0% { opacity:0; }
		100% { opacity:100%; }
	}
	
	/* thank you, http://tutorialzine.com/2014/07/css-inline-help-tips/ -Duncan [4 November 2015] */
	
</style>
<?php
//marktime("module wide");
/**********
 * Navigation buttons
 */
$active_button_label = $menuObject->drawPageTop($menu,(isset($display_navigation_buttons) ? $display_navigation_buttons : true));
 /*
$active_button_label = "";
if(!isset($display_navigation_buttons) || $display_navigation_buttons!==false) {
	foreach($menu['buttons'] as $level => $buttons) {
		//$helper->arrPrint($buttons);
	    echo $helper->generateNavigationButtons($buttons, $level);
		if($level==1) {
			foreach($buttons as $b) {
				if($b['active']==true) {
					$active_button_label = $b['name'];
					break;
				}
			}
		}
	}
}
//marktime("navigation buttons");
/**********
 * Breadcrumbs
 */ /*
$breadcrumbs = array();
    foreach($menu['breadcrumbs'] as $link=>$text) {
    	if(substr_count($link, ".php")>0) {
        	$breadcrumbs[] = "<a href='".$link."' class=breadcrumb>".$text."</a>";
		} else {
        	$breadcrumbs[] = "".$text."";
		}
    }
echo "<h1>".implode(" >> ",$breadcrumbs)."</h1>";
  */
//marktime("end header");
?>

<!------------Hint Box ------------>
<!-- <div id="hint_box" style="padding:8px; width:170px; box-shadow: 2px 2px 5px #333333; background-color:#25bb25; border:1px solid #66cf66; display:none; position:fixed; top:25px; right:10px; z-index:99;">
	<h2 style="font-family:Verdana, Arial, Helvetica, sans-serif; font-weight:normal; color:white">Information</h2>
	<p style="font-family:Verdana, Arial, Helvetica, sans-serif;"></p>
</div> -->
