<?php
$get_rtk = true;
require_once 'inc_header.php';

	echo "<div id=my_result>";
if(isset($_REQUEST['r'])) {
	$me->displayResult($_REQUEST['r']);
}
	echo "</div>";

?>
<table>
	<thead>
		<tr>
<?php 
foreach($dbheadings['R_CALL'] as $fld => $h) {
	if(!in_array($fld,array("rsl_cmpcode"))) {
		echo "<th>".$h['h_text']."</th>";
	}
}
?>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php
//$me->arrPrint($dbheadings);
$sql = "SELECT * FROM help_reseller_call 
	INNER JOIN help_list_status ON rsl_statusid = id 
	WHERE rsl_active = true 
	AND rsl_admin = '".strtoupper($cmpcode)."' 
	".($page_id=="mine" ? "AND rsl_admin_tkid = '$tkid' " : "AND rsl_admin_tkid <> '' ")."
	".($page_id=="open" || $page_id=="mine" ? " AND rsl_statusid <> 5 " : "")."
	ORDER BY sort ASC, rsl_date DESC";
$rs = db_query("H",$sql);
while($call = mysql_fetch_assoc($rs)) {
	echo "<tr>";
	foreach($dbheadings['R_CALL'] as $fld => $h) {
		if(!in_array($fld,array("rsl_cmpcode"))) {
			echo "<td>";
			switch($h['h_type']) {
				case "DATE": echo date("d M Y H:i:s",strtotime($call[$fld])); break;
				case "LIST": 
					$l = $lists['help_list_'.$h['h_table']]['data'][$call[$fld]];
					if(isset($l['color']) && strlen($l['color'])==6) { echo "<div class=center style=\"padding: 5px; background-color: #".$l['color']."; color: #FFFFFF;\">"; }
					echo $l['value']."</div>";
					if($fld=="rsl_statusid") { echo "<div class=center style='margin-top: 10px;'><input type=button value='Mark R".$call['rsl_id']." Completed' class=close_me id='".$call['rsl_id']."' ref='".$call['rsl_ref']."' /></div>"; }
					break;
				case "RESP":
					if(strlen($call[$fld])>0) {
						echo $rtk[$call[$fld]]['tkn'];
					} else {
						echo "<span class=idelete>Unassigned</span>";
					}
					break;
				default: echo str_replace(chr(10),"<br />",$call[$fld]); break;
			}
			echo "</td>";
		}
	}
		echo "<td><input type=button value=View id=".$call['rsl_id']." /></td>";
	echo "</tr>";
}
?>
	</tbody>
</table>
<script type=text/javascript>
	$(function() {
		var scrollto = <?php echo isset($_REQUEST['pos']) ? $_REQUEST['pos'] : 0; ?>;
		var response = $('#my_result').html();
if(scrollto>0 && $(document).height()>scrollto) {
	$(window).scrollTop(scrollto);
				$('<div />',{id:'response_msg', html:response}).dialog({
					autoOpen	: true,
					modal 		: true,
					width		: 300,
					buttons		: [{ text: 'Ok', click: function() { 
										$('#response_msg').dialog('destroy'); 
										$('#response_msg').remove(); 
									} 
								}],
					});
}
		$("input:button").click(function() {
			i = $(this).prop("id");
			if($(this).hasClass("close_me")) {
				var r = $(this).attr("ref");
//alert($(window).scrollTop()+' : '+$(window).height()+' : '+$(document).height());
				if(confirm("Are you sure you wish to quietly mark "+r+" as completed? \n\nThis will NOT notify the business partner.")==true) {
					var dta = "act=QUIET_CLOSE&i="+i;
					$.ajax({                                      
						url: 'ajax/manage_reseller.php', 		  type: 'POST',		  data: dta,		  dataType: 'json', 
						success: function(d) { 
							//console.log(d[1]);
							result = d[0]; 
							response = d[1];
							document.location.href = 'manage_reseller.php?page_id=<?php echo $page_id; ?>&pos='+$(window).scrollTop()+'&r[]='+result+'&r[]='+response;
						},
						error: function(d) { console.log(d); }
					});
				} else {
				}
			} else {
				document.location.href = 'manage_reseller_view.php?page_id=<?php echo $page_id; ?>&i='+i;
			}
		});
	});
</script>
</body>
</html>