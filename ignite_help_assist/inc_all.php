<?php
function getHeadings($get,$sec) {
	$headings = array();
	
	$sql = "SELECT * FROM help_headings WHERE h_src IN ('".implode("','",$get)."') AND active = true ORDER BY h_src, h_sort";
	$headings = mysql_fetch_all2("H",$sql,"h_src","h_field");
	
	return $headings;
}

function getLists($active) {
	$lists = array(
		'help_list_status'=>array(
			'table'=>"help_list_status",
			'text'=>"Status",
			'data'=>array(),
		),
		'help_list_modules' => array(
			'table'=>"help_list_modules",
			'text'=>"Modules",
			'data'=>array(),
		),
		'help_list_reseller_topics' => array(
			'table'=>"help_list_reseller_topics",
			'text'=>"Topics",
			'data'=>array(),
		),
	);
	
	foreach($lists as $l) {
				$sql = "SELECT * FROM ".$l['table'].($active ? " WHERE active = true " : "")." ORDER BY sort, value, id";
				$data = mysql_fetch_all1("H",$sql,"id");
				$lists[$l['table']]['data'] = $data;
	}
	
	return $lists;
}

?>