<?php
require_once '../inc_session.php';
require_once '../inc_db_help.php';
require_once 'inc_all.php';
require_once 'inc.php';
error_reporting(-1);

include_once("../module/loader.php");
@session_start();
spl_autoload_register("Loader::autoload");

$me = new ASSIST();

if(isset($get_co) && $get_co == true) {
	$sql = "SELECT cmpcode, cmpname FROM assist_company WHERE cmpreseller = '".strtoupper($cmpcode)."' ORDER BY cmpcode, cmpname";
	$cmp_objects = mysql_fetch_all1("A",$sql,"cmpcode");
} else {
	$cmp_objects = array();
}

if(isset($get_rtk) && $get_rtk == true) {
	$sql = "SELECT tkid, CONCAT_WS(' ',tkname, tksurname) as tkn FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 ORDER BY tkname, tksurname";
	$rtk = mysql_fetch_all1("C",$sql,"tkid");
} else {
	$rtk = array();
}

$active_respondants = array("0001","0002","0008");

$dbheadings = getHeadings(array("R_CALL","R_LOG"),"R");

$lists = getLists(false);


$_REQUEST['jquery_version'] = "1.10.0";
$me->displayPageHeader();
?>
    
<script type ="text/javascript" src="main.js"></script>

<style type=text/css>
.noborder { border: 0px dashed #fe9900; }
h2.notop { margin-top: 0px; }
</style>

<?php
$menu = array(
	'new' => array(
		//'client'=>"New Client Requests Received",
		'reseller'=>"New Reseller Requests Received",
	),
	'manage' => array(
		//'client'=>"Client Requests",
		'reseller'=>"Reseller Requests",
	)
);
$redirect = array(
	'new'	 		=> "new_reseller.php",
	'manage' 		=> "manage_reseller.php",
);

$self = $_SERVER['PHP_SELF'];
$page = explode("/",$self);
$self = $page[count($page)-1];
$page = substr($self,0,-4);
$base = explode("_",$page);
if(!isset($src)) {
	$src = isset($base[1]) ? $base[1] : "";
}


if(isset($redirect[$page])) {
	echo "<script type=text/javascript>document.location.href = '".$redirect[$page]."';</script>";
}

if(isset($menu[$base[0]])) {
	$submenu = $menu[$base[0]];
	$nav = array();
	foreach($submenu as $id => $display) {
		$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$id.".php",'active'=>(($src==$id) ? "Y" : "N"),'display'=>$display);
	}
	echoNavigation(1,$nav);
}

switch($base[0]) {
	case "manage":
			$page_id = isset($_REQUEST['page_id']) ? $_REQUEST['page_id'] : "mine";
			$menu2 = array(
				'mine'	=> "View Mine",
				'open'	=> "View All Open",
				'all' => "View All",
				//'update' => "Update",
			);
			$nav = array();
			foreach($menu2 as $id => $display) {
				$nav[$id] = array('id'=>$id,'url'=>$base[0]."_".$base[1].".php?page_id=".$id,'active'=>(($page_id==$id) ? "Y" : "N"),'display'=>$display);
			}
			echoNavigation(2,$nav);
			$page_title = $menu2[$page_id];
		
		break;
}

if($base[0]=="main") {
	echo "<h1>Ignite Assist Help for Ignite Assist</h1>";
} else {
	echo "<h1>".breadCrumb($base[0].".php").(isset($menu['base_pages'][$base[0]]) ? $menu['base_pages'][$base[0]] : ucfirst($base[0]));
	if(isset($base[1])) {
		$breadcrumb = $base[0]."_".$base[1].".php";
		echo "</a> >> ".breadCrumb($breadcrumb).(isset($menu[$base[0]][$base[1]]) ? $menu[$base[0]][$base[1]] : ucfirst($base[1]));
	}
	if(isset($page_title)) {
		echo "</a> >> ".$page_title;
	}
	echo "</a></h1>";
}
?>