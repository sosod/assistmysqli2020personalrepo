<?php

class IDP2_SETUP_NOTIFICATIONS extends IDP2 {
	
	private $questions = array();
	
	private $table_name = "_setup_notifications";
	private $field_prefix = "notif";
	
	private $fields = array(
		'notif_id'=>"id",
		'notif_activity'=>"activity",
		'notif_object'=>"object",
		'notif_recipient'=>"recipient",
		'notif_value'=>"value",
		'notif_default'=>"default_value",
		'notif_sort'=>"sort",
		'notif_status'=>"status",
	);
	private $field_names = array();

	const LOG_TABLE = "setup";
	
	public function __construct() {
		parent::__construct();
		$this->questions = $this->getQuestionsFromDB();
		$this->field_names = array_flip($this->fields);
	}
	
	
	
	
	/********************
	 * CONTROLLER Functions
	 */
	public function updateObject($var,$attach=array()) {
		unset($attach);	//remove unnecessary array from the inc_controller
		$questions = $this->getQuestions();
		$mar = 0;
		$r_err = array();
		$r_ok = array();
		$err = false;
		$changes = array();
		foreach($questions as $key => $q) {
			$new = $var['Q_'.$key];
			
			$old = $q['value'];
			if($new != $old) {
				//update database
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->field_names['value']." = '".$new."' WHERE ".$this->field_names['id']." = ".$key;
				$mar = $this->db_update($sql);
				if($mar>0) {
					$options = explode("|",$q['options']);
					$changes[$key] = array('to'=>"",'from'=>"");
					foreach($options as $opt) {
						$opts = explode("_",$opt);
						if($new==$opts[0]) {
							$changes[$key]['to'] = $opts[1];
						} elseif($old==$opts[0]) {
							$changes[$key]['from'] = $opts[1];
						}
						if($changes[$key]['to']!="" && $changes[$key]['from']!="") { break; }
					}
					$r_ok[] = $key;
				} else {
					$r_err[] = $key;
					$err = true;
				}
			}
		}
		if(count($changes)>0) {
			$changes['user']=$this->getUserName();
			
			$log_var = array(
				'section'	=> "NOTIF",
				'object_id'	=> "",
				'changes'	=> $changes,
				'log_type'	=> IDP2_LOG::EDIT,
			);
			$this->addActivityLog("setup", $log_var);
			if(!$err) {
				return array("ok","Changes saved successfully.");
			} elseif(count($r_ok)==0) {
				return array("error","An error occurred while trying to save changes.");
			} elseif(count($r_err)>0) {
				$return = array("info",count($r_ok)." Changes saved successfully, but the following changes did not save: NOTIF".implode(", PREF",$r_err).".");
				return $return;
			} else {
				return $array("okerror","An unknown error occurred.  Please review the page and confirm that your changes have been saved.");
			}
		} elseif($err) {
			return array("error","An error occurred while trying to save changes.");
		}
		return array("info","No changes made.");
	}
	
	
	
	
	
	
	
	
	
	
	
	/***************************
	 * GET functions
	 */
	public function getTableName() { return $this->getDBRef().$this->table_name; }
	public function getTableFieldPrefix() { return $this->field_prefix; }
	/**
	 * Returns the list of questions to be displayed on the Setup > Preferences page
	 */
	public function getQuestions() {
		return $this->questions;
	}
	
	/**
	 * Returns the questions from the database
	 */
	public function getQuestionsFromDB() {
		$sql = "SELECT ";
		$f = array();
		foreach($this->fields as $fld => $key) {
			$f[] = $fld." as ".$key;
		}
		$sql.=implode(", ",$f)." FROM ".$this->getTableName()." WHERE ".$this->getTableFieldPrefix()."_status & ".IDP2::ACTIVE." = ".IDP2::ACTIVE." ORDER BY ".$this->getTableFieldPrefix()."_sort ASC, ".$this->getTableFieldPrefix()."_id ASC";
		$rows = $this->mysql_fetch_all_by_id($sql, "id");
		
		$questions = array();
		
		foreach($rows as $key => $r) {
			$questions[$key] = $r;
			if($r['object']=="default") {
				$questions[$key]['name'] = "Allow users to send SMS notifications within the module?";
				$questions[$key]['description'] = "Selecting No here will disable SMS notifications within ".$this->getModTitle().". Selecting Yes here will only allow SMS notifications to be sent if there is a positive SMS balance available.  Additional SMS bundles can be purchased via the 'SMS Notification' function available to the Assist Administrator.  Please note that selecting Yes here will not disable the normal Email notifications which will still be sent as per usual.";
			} else {
				$questions[$key]['name'] = $this->replaceAllNames("Allow users to send SMS notifications when performing a".(in_array(strtolower(substr($r['activity'],1,1)),array("a","e","i","o","u","h")) ? "n" : "")." ".$r['activity']." activity on a".(in_array(strtolower(substr($r['activity'],1,1)),array("a","e","i","o","u","h")) ? "n" : "")." ".$r['object'].".");
				$questions[$key]['description'] = $this->replaceAllNames("Selecting Yes here will allow the user to choose whether or not to send an SMS notification to the ".$r['recipient']." when performing the activity.");
			}
		}
		return $questions;
	}
	
	public function getQuestionForLogs()  {
		$data = array();
		foreach($this->questions as $key => $q) {
			$data[$key] = $q['name'];
		}
		return $data;
	}
	
	/**
	 * Function used as a placeholder for the controller: used to check if SMSs can be used in a specific situation
	 */
	public function approveObject($data){
		$smsObj = new ASSIST_SMS();
		if($smsObj->canIUseSMS()){
			$specific = $this->getSpecificAnswer($data);
			if($specific['value'] == 1){
				$recipient = ucwords(str_replace("|","",$specific['recipient']));
				return array(true, $recipient);
			}else{
				return array(false, "can't use SMSs in this situation");		
			}
		}else{
			return array(false, "can't use SMSs at all");		
		}
	}
	
	/**
	 * Returns the SMS answers from the database
	 */
	public function getAnswerSMS($q=""){
		$where = ($q > 0)?" WHERE ".$this->getTableFieldPrefix()."_id = $q":"";
		$sql = "SELECT ".$this->getTableFieldPrefix()."_id as id, ".$this->getTableFieldPrefix()."_value as value FROM ".$this->getTableName()." $where";
		return $this->mysql_fetch_all_by_id($sql,"id");
	}

		/**
	 * Returns the SMS answers from the database according to the fed in activity and object
	 */
	public function getSpecificAnswer($q){
		$activity = "|".$q['activity']."|";
		$object = "|".$q['object']."|";
		$sql = "SELECT ".$this->getTableFieldPrefix()."_value as value, ".$this->getTableFieldPrefix()."_recipient as recipient FROM ".$this->getTableName()." WHERE ".$this->getTableFieldPrefix()."_activity = '$activity' AND ".$this->getTableFieldPrefix()."_object = '$object'";
		return $this->mysql_fetch_one($sql);
	}
	
	/******************
	 * SET functions
	 */
	private function updateQuestionsWithAnswersFromDB() {
		$questions = $this->getQuestions();
		foreach($questions as $key => $q) {
			
		}
	}
	
	
	
	
	
	
	
	
	
	public function __destruct() {
		parent::__destruct();
	}
}




?>