<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class IDP2_SEGMENTS extends IDP2 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "status";
	protected $id_field = "id";
	protected $parent_field = "parent_id";
	protected $secondary_parent_field = "idp_id";
	protected $name_field = "name";
	protected $description_field = "description";
	protected $has_children_field = "has_child";
	protected $can_post_field = "can_post";
	protected $attachment_field = "attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "";
	
	protected $segments;
	protected $section;
	protected $section_set = false;
	
	protected $dynamic_object_type;
	protected $dynamic_parent_object_type;
	protected $dynamic_object_name;
	protected $dynamic_table;
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = false; 
    const OBJECT_NAME = false; 
	const PARENT_OBJECT_TYPE = false;
     
    const TABLE = false;
    const TABLE_FLD = "";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "segment";
	
	/**
	 * Status Constants
	 */
    //const CONFIRMED = 32;
	//const ACTIVATED = 64;
    
    public function __construct($section="") {
        parent::__construct();
		
		$nameObj = new IDP2_NAMES();
		$this->segments = $nameObj->fetchSegmentNames(true);
		
		$this->object_form_extra_js = "";
		
		if(strlen($section)>0 && $section!==false) {
			$this->setSection($section);
		}
		
    }
    
	public function setSection($section) {
			$this->dynamic_table = self::LOG_TABLE."_".strtolower($section);
			$this->dynamic_parent_object_type = strtolower($section);
			$this->dynamic_object_type = strtolower($section);
			$this->dynamic_object_name = strtolower($section);
			$this->section = strtolower($section);
			$this->ref_tag = "SEG/".strtoupper($section);
			$this->section_set = true;
	}
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		unset($var['object_id']); //remove incorrect field value from add form
		$segment_section = $var['segment_section'];
		unset($var['segment_section']);
		$this->setSection($segment_section);
		/*foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}*/
		$var['status'] = IDP2::ACTIVE;
		$var['insertdate'] = date("Y-m-d H:i:s");
		$var['insertuser'] = $this->getUserID();


		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
			);
				//'log_var'=>$log_var
			return $result;
		}

		return array("error","Testing: ".$sql." :: ".$this->dynamic_table." :: ".$segment_section);
	}






	public function editObject($var,$attach=array()) {
		$segment_section = $var['segment_section'];
		unset($var['segment_section']);
		$this->setSection($segment_section);
		$var['is_segment'] = true;
		$result = $this->editMyObject($var,$attach);
		return $result;
	}

	/**
	 * Check whether an object is in use 
	 */
	public function checkUsage($var) {
		//TO BE PROGRAMMED - 17 APRIL 2017 [JC]
		return false;
	}
	/**
	 * Check whether an object can be deleted
	 * 	calls this->checkUsage
	 */
	public function canIBeDeleted($var) {
		$err = false;
		if(is_array($var) && isset($var['object_id']) && ASSIST_HELPER::checkIntRef($var['object_id']) && (isset($var['section']) || $this->section_set)==true) {
			$object_id = $var['object_id'];
			if(isset($var['section'])) {
				$this->setSection($var['section']);
			}
		} elseif(!is_array($var) && ASSIST_HELPER::checkIntRef($var) && $this->section_set == true) {
			$object_id = $var;
		} else {
			$err = true;
			return "error";
		}
		
		if(!$err) {
			//CHECK HERE FOR EACH SEGMENT TYPE
			$this->checkUsage($object_id);
		}
		return $can_delete===true ? 1 : 0;
	}

	public function deleteObject($var) {
		$id = $var['object_id'];
		$this->setSection($var['section']);
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::DELETED." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deleted.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::DELETE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deleted.",
				'object_id'=>$id,
			);
			return $result;
		
	}

	public function deactivateObject($var) {
		$id = $var['object_id'];
		$this->setSection($var['section']);
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deactivated.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::DEACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deactivated.",
				'object_id'=>$id,
			);
			return $result;
	}
	
	public function restoreObject($var) {
		$id = $var['object_id'];
		$this->setSection($var['section']);
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::ACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." restored.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::RESTORE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully restored.",
				'object_id'=>$id,
			);
			return $result;
	}

	public function confirmObject($var) {
		$result = array("info","functionality not required");
		/*
		$obj_id = $var['object_id'];
		$new_status = self::ACTIVE + self::CONFIRMED;
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);
		
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag.$obj_id." confirmed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::CONFIRM,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully confirmed.",
				'object_id'=>$obj_id,
			);
		
		*/
		return $result;
	}



	public function undoConfirmObject($var) {
		$result = array("info","functionality not required");
/*		$obj_id = $var['object_id'];
		$new_status = self::ACTIVE;
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);
		
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag.$obj_id." unconfirmed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::UNDOCONFIRM,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully unconfirmed.",
				'object_id'=>$obj_id,
			);
		/*
		*/
		
		return $result;
	}


	public function activateObject($var) {
		$result = array("info","functionality not required");
		/*
		$obj_id = $var['object_id'];
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		$new_status = $old_status + self::ACTIVATED;
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);
		
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag.$obj_id." activated.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::ACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully activated.",
				'object_id'=>$obj_id,
			);
		
		*/
		return $result;
	}	
	

	public function undoActivateObject($var) {
		$result = array("info","functionality not required");
		/*
		$obj_id = $var['object_id'];
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		$new_status = $old_status - self::ACTIVATED;
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);
		
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag.$obj_id." activation reversed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::UNDOACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"Reversal of prior activation of ".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." successfull.",
				'object_id'=>$obj_id,
			);
		
		*/
		return $result;
	}	
    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".(self::TABLE!==false?self::TABLE:$this->dynamic_table); }
    public function getDescriptionFieldName() { return $this->description_field; }
    public function getHasChildrenFieldName() { return $this->has_children_field; }
    public function getCanPostFieldName() { return $this->can_post_field; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return (self::OBJECT_TYPE!==false?self::OBJECT_TYPE:$this->dynamic_object_type); }
    public function getMyObjectName() { return (self::OBJECT_NAME!==false?self::OBJECT_NAME:$this->dynamic_object_name); }
    public function getMyParentObjectType() { return (self::PARENT_OBJECT_TYPE!==false?self::PARENT_OBJECT_TYPE:$this->dynamic_parent_object_type); }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}
		
	}
    
	public function getSegmentDetails($i="") {
		if($i===true && strlen($this->section)>0) {
			return $this->segments[$this->section];
		} elseif($i!==true && strlen($i)>0) {
			return $this->segments[$i];
		} elseif($i!==true) {
			return $this->segments;
		} else {
			return false;
		}
	}
    
	public function getList($parent=0,$active_only=true,$idp_id=0) {
		//ADD FUNCTIONALITY TO DIFFER BETWEEN NOT DELETED AND ACTIVE
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, ".$this->getDescriptionFieldName()." as description
				, ".$this->getParentFieldName()." as parent
				, ".$this->getHasChildrenFieldName()." as has_children
				, ".$this->getCanPostFieldName()." as can_assign
				FROM ".$this->getTableName()." S 
				WHERE ".$this->getActiveStatusSQL("S")."
				".(ASSIST_HELPER::checkIntRef($parent) ? " AND ".$this->getParentFieldName()." = ".$parent : "")." 
				".(ASSIST_HELPER::checkIntRef($idp_id) ? " AND ".$this->getSecondaryParentFieldName()." = ".$idp_id : "")." 
				ORDER BY ".$this->getParentFieldName().", ".$this->getNameFieldName();
		$raw = $this->mysql_fetch_all($sql);
		
		$data = array();
		foreach($raw as $row) {
			$data[$row['parent']][$row['id']] = $row;
		}
		return $data;
	}

	public function getAllListItemsFormattedForSelect($return_no_posting=false) {
		$data = $this->getList(0,false);
		$no_can_post = array();
		$items = array();
		if(isset($data[0]) && count($data[0])>0) {
			foreach($data[0] as $key => $d) {
				$title = array();
				$ret = $this->processList($data, $key, $d, $title, $items, $no_can_post);
				//$title = $ret[0];
				$items = $ret[1];
				$no_can_post = $ret[2];
			}
		}
		if($return_no_posting==true) {
			return array('options'=>$items,'disabled'=>$no_can_post);
		} else {
			return $items;
		}
	}


     
	public function getActiveListItemsFormattedForSelect($return_no_posting=false,$idp_id=0) { //echo $idp_id;
		$data = $this->getList(0,true,$idp_id);
		$no_can_post = array();
		$items = array();
		if(isset($data[0]) && count($data[0])>0) {
			foreach($data[0] as $key => $d) {
				$title = array();
				$ret = $this->processList($data, $key, $d, $title, $items, $no_can_post);
				//$title = $ret[0];
				$items = $ret[1];
				$no_can_post = $ret[2];
			}
		}
		if($return_no_posting==true) {
			return array('options'=>$items,'disabled'=>$no_can_post);
		} else {
			return $items;
		}
	}
	
	private function processList($data,$key,$d,$title,$items,$no_can_post) {
		$title[] = $d['name'];
		if($d['can_assign']==false) { $no_can_post[] = $key; }
		if($d['can_assign']==true) {
			$items[$key] = implode(": ",$title);
		}
		if(isset($data[$key]) && count($data[$key])>0) {
			foreach($data[$key] as $k => $e) {
				$ret = $this->processList($data, $k, $e, $title, $items, $no_can_post);
				//$title = $ret[0];
				$items = $ret[1];
				$no_can_post = $ret[2];
			}
		}
		return array($title,$items,$no_can_post);
	}
	
	public function getNameForAnObject($id) {
		$row = $this->getRawObject($id);
		if(is_array($id)) {
			$d = array();
			foreach($row as $k => $r) {
				$d[] = $r[$this->getNameFieldName()];
			}
			$data = implode(chr(10),$d);
		} else {
			$data = $row[$this->getNameFieldName()];
		}
		return $data;
	}
	public function getAObject($id=0,$options=array()) {
		$row = $this->getRawObject($id);
		return $row;
	}

	public function getAListItemName($id,$name_only = true) {
		$list = $this->getAllListItemsFormattedForSelect();
		$data = array();
		if(is_array($id)) {
			foreach($id as $i) {
				$data[$i] = isset($list[$i]) ? $list[$i] : "Not available";
			}
			return $data;
		} elseif($name_only) {
			return isset($list[$i]) ? $list[$i] : $this->getUnspecified();
		} else {
			$data[$i] = isset($list[$i]) ? $list[$i] : "Not available";
			return $data;
		}
	}

	public function getHeading($id) {
		$row = $this->getRawObject($id);
		return $row[$this->getNameFieldName()].(strlen($row[$this->getTableField()."_ref"])>0 ? " (".$row[$this->getTableField()."_ref"].")":"");
	}

	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		if(is_array($obj_id)) {
			$where = $this->getIDFieldName()." IN (".implode(",",$obj_id).")";
			$multi = true;
		} else {
			$where = $this->getIDFieldName()." = ".$obj_id;
			$multi = false;
		}
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$where;
		if($multi) {
			$data = $this->mysql_fetch_all_by_id($sql,$this->getIDFieldName());
		} else {
			$data = $this->mysql_fetch_one($sql);
		}
		return $data;
	}
	
	

	public function getOrderedObjects($parent_parent_id=0,$parent_id=0){
		$res = array();
	/*	if($parent_id==0) {
			$parentObject = new IDP2_PARENT();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
		} else {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");*/
		
		return $res2;
	}



	public function getListOfObjectsFormattedForSelect($active=true,$activated=true) {
		//$sql = "SELECT * FROM ".$this->getTableName()." WHERE "
	}


	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false,false,false);
	}
	/**
	 * Returns status check for Actions are fully activated & available outside of NEW 
	 */
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW / ACTIVATE / WAITING
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where 
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATE",$t,false);
	}
		 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>