<?php
/**
 * To manage the user access of the IDP module
 * 
 * Created on: 9 November 2016
 * Authors: Janet Currie
 * 
 * 
 * 26 Feb 2015 [JC]: Create Deliverables option removed due to introduction of tree hierarchy in New.  Contract Manager will now be responsible for adding deliverables and actions.
 * 
 * 
 */

class IDP2_USERACCESS extends IDP2 {
	
	private $table_name = "_useraccess";
	
		//'ua_create_deliverable'	=> "Create |deliverable|",
	private $field_names = array(
		'ua_create_idp'			=> "Create |idp|",
		'ua_confirm_idp'		=> "Confirm |idp|",
		'ua_activate_idp'		=> "Activate |idp|",
		'ua_edit_all'			=> "Edit",
		'ua_admin_fin'			=> "Financial Integration Management",
		'ua_report'				=> "Reports",
		'ua_assurance'			=> "Assurance Provider",
		'ua_setup'				=> "Setup",
	);
	
		//'ua_create_deliverable'	=> 0,
	private $field_defaults = array(
		'ua_create_idp'			=> 0,
		'ua_confirm_idp'		=> 0,
		'ua_activate_idp'		=> 0,
		'ua_edit_all'			=> 0,
		'ua_admin_fin'			=> 0,
		'ua_report'				=> 1,
		'ua_assurance'			=> 0,
		'ua_setup'				=> 0,
	);
	
	private $extra_sections = array(
		'ua_new'=>0,
		'ua_admin'=>0,
	);
	
	private $access_sections = array(
		'ua_new'=>array(
			'ua_create_idp',
			'ua_confirm_idp',
			'ua_activate_idp',
		),
		'ua_admin'=>array(
			'ua_admin_fin',
		),
	);

	public function __construct() {
		parent::__construct();
		$this->field_names = $this->replaceObjectNames($this->field_names);
		$this->table_name = $this->getDBRef().$this->table_name;
		//Module admin is depreciated
		unset($this->field_names['ua_module']);
		unset($this->field_defaults['ua_module']);
	}	
	
	/**
	 * CONTROLLER functions
	 */
	public function addObject($var){
		if(!isset($var['ua_status'])) { $var['ua_status'] = self::ACTIVE; }
		if(!isset($var['ua_insertuser'])) { $var['ua_insertuser'] = $this->getUserID(); }
		if(!isset($var['ua_insertdate'])) { $var['ua_insertdate'] = date("Y-m-d H:i:s"); }
		return $this->addUser($var);
		//return array("info","No changes were found to be saved.");
	}
	public function editObject($var){
		$id = $var['ua_id'];
		unset($var['ua_id']);
		return $this->editUser($id,$var);
	}
	
	
	
	/**
	 * GET functions
	 */
	public function getUserAccessFields() { return $this->field_names; }
	public function getUserAccessDefaults()	{ return $this->field_defaults; }
	public function getTableName() { return $this->table_name; }
	/**
	 * Get the list of users who have not yet had their user access defined.
	 */
	public function getUsersWithNoAccess() {
		$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users 
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				LEFT JOIN ".$this->getTableName()."
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				AND ua_status IS NULL 
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_value_by_id($sql, "id", "name");
				
	}
	/**
	 * Get the list of users with access
	 */
	public function getActiveUsers($ids="") {
		$sql = "SELECT tkid, CONCAT(tkname, ' ',tksurname) as name, UA.*
				FROM assist_".$this->getCmpCode()."_timekeep TK
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users MMU 
				ON usrtkid = tkid AND usrmodref = '".$this->getModRef()."'
				INNER JOIN ".$this->getTableName()." UA
				ON ua_tkid = tkid
				WHERE tkstatus = 1
				";
		if(is_array($ids)) {
			$sql.=" AND TK.tkid IN ('".implode("','",$ids)."') ";
		} elseif(strlen($ids)>0) {
			$sql.=" AND TK.tkid = '".$ids."' ";
		}
		$sql.= "
				ORDER BY tkname, tksurname";
		return $this->mysql_fetch_all_by_id($sql, "tkid");
			//return $sql;	
	}
	public function getActiveUsersFormattedForSelect($ids="") {
		$rows = $this->getActiveUsers($ids);
		$data = $this->formatRowsForSelect($rows);
		return $data;
		//return $rows;
	}
	/**
	 * Get the current user's access
	 */
	public function getMyUserAccess($ui="") {
		$ui = strlen($ui)==0 ? $this->getUserID() : $ui;
		$sql = "SELECT UA.* FROM ".$this->getTableName()." UA WHERE ua_tkid = '".$ui."'";
		$row = $this->mysql_fetch_one($sql);
		if(!isset($row['ua_status'])) {
			$row = array_merge($this->field_defaults,$this->extra_sections);
			$row['ua_status'] = 0;
			$row['ua_tkid'] = $ui;
		} else {
			foreach($this->extra_sections as $a => $v) {
				$row[$a] = $v;
				if(isset($this->access_sections[$a])) {
					$c = count($this->access_sections[$a]);
					$x = 0;
					while(($x<$c) && ($row[$a]!=1)) {
						$f = $this->access_sections[$a][$x];
						if($row[$f]==1) {
							$row[$a]=1;
						}
						$x++;
					}
				}
			}
		}
		$user_access = array();
		foreach($row as $key => $val){
			$user_access[substr($key,3)] = $val;
		}
		return $user_access;
	}
	
	
	
	/**
	 * Gets all users with ua_confirm_idp access
	 */
	public function getUsersWithConfirmationAccess() {
		$users = $this->getActiveUsers();
		$data = array();
		foreach($users as $u) {
			if($u['ua_confirm_idp']==1) {
				$data[$u['tkid']] = $u['name'];
			}
		}
		return $data;
	}
	/**
	 * SET / UPDATE functions
	 */
	/**
	 * Adds a new user
	 */
	private function addUser($var){
		$result = array("info","No change was found to be saved.");
		$insert_data = $this->convertArrayToSQL($var);
		$sql = "INSERT INTO ".$this->getTableName()." SET ".$insert_data;
		$id = $this->db_insert($sql);
		if($id>0) {
			$result = array("ok","User added successfully.");
			$changes = array(
				'user'=>$this->getUserName(),
				'response'	=> "Added user access for user: ".$this->getAUserName($var['ua_tkid']),
			);
			$log_var = array(
				'section'	=> "USER",
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> IDP2_LOG::CREATE,		
			);
			$this->addActivityLog("setup", $log_var);
		} else {
		/*
		 * */
			$result = array("error","Sorry, something went wrong while trying to add the user.  Please try again.");
		}
		return $result;
	}
	
	
	private function editUser($id,$var) {
		if(ASSIST_HELPER::checkIntRef($id)) {
			$old = $this->mysql_fetch_one("SELECT * FROM ".$this->getTableName()." WHERE ua_id = ".$id);
			//$edits = array();
			//foreach($var as $fld=>$v){
			//	$edits[substr($fld,5)] = $v;
			//}
			$edits = $var;
			$update_data = $this->convertArrayToSQL($edits);
			$sql = "UPDATE ".$this->getTableName()." SET ".$update_data." WHERE ua_id = ".$id;
			$mar = $this->db_update($sql);
			if($mar>0){
				$un = $this->getAUserName($old['ua_tkid']);
				//$user_access_fields = $this->getUserAccessFields();
				$changes = array(
					'user'=>$this->getUserName(),
					'response'	=> "Edited user access for user: ".$un,
				);
				foreach($old as $key=>$value){
					if(isset($edits[$key]) && $value != $edits[$key]){
						//$changes[$user_access_fields[$key]] =array('to'=>($edits[$key]==1?"Yes":"No"), 'from'=>($value==1?"Yes":"No"));
						$changes[$key] =array('to'=>($edits[$key]==1?"Yes":"No"), 'from'=>($value==1?"Yes":"No"));
					}
				}
							
				$log_var = array(
					'section'	=> "USER",
					'object_id'	=> $id,
					'changes'	=> $changes,
					'log_type'	=> IDP2_LOG::EDIT,		
				);
				$this->addActivityLog("setup", $log_var);
				return array("ok","Changes to ".$un." have been saved successfully.");
			} else {
				return array("info","No change was found to be saved.");
			}
		}
		return array("error","An error occurred while trying to save the changes. Please try again.");
	}
	
	
	
	
	
	
	
	
}



?>