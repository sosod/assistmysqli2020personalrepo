<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class IDP2_PMKPI_PROJECT extends IDP2 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_kpi_id";
	protected $secondary_parent_field = "_proj_id";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "PKP";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "PMKPI_PROJECT"; 
    const OBJECT_NAME = "kpi_project"; 
	const PARENT_OBJECT_TYPE = "PMKPI";
	const SECONDARY_PARENT_OBJECT_TYPE = "PROJECT";
     
    const TABLE = "pm_kpi_project";
    const TABLE_FLD = "kp";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "object";
    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->has_secondary_parent = true;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->secondary_parent_field = self::TABLE_FLD.$this->secondary_parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		//$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($insert_data=array(),$parent_id=0,$secondary_id=0,$indicator_code="") {
		
		if(count($insert_data)>0) {
			$var = array();
			foreach($insert_data as $key => $val) {
				$var[$this->getTableField()."_".$key] = $val;
			}
		} else {
			$var = array(
				$this->getParentFieldName() => $parent_id,
				$this->getSecondaryParentFieldName() => $secondary_id,
				$this->getTableField()."_indicator"=>$indicator_code,
			);
		}
		
		$var[$this->getTableField().'_status'] = JAL1::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
	}




	public function editObject($var) {
//		$sql = "UPDATE ".$this->getTableName()." 
//				SET ".$this->getSecondaryParentFieldName()." = '".$var['secondary']."' 
//				WHERE ".$this->getParentFieldName()." = ".$var['parent']." 
//				  AND ".$this->getIDFieldName()." = ".$var['id'];
//		$this->db_update($sql);
		return false;
	}


	public function deleteObject($insert_data,$parent_id=0,$secondary_id=0) {
		if(count($insert_data)>0) {
			$parent_id = $insert_data[$this->getParentFieldName()]; 
			$secondary_id = $insert_data[$this->getSecondaryParentFieldName()];
		}
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->getStatusFieldName()." = ".self::DELETED." 
				WHERE ".$this->getParentFieldName()." = ".$parent_id." 
				  AND ".$this->getSecondaryParentFieldName()." = ".$secondary_id;
		$this->db_update($sql);
		return true;
	}


    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyLogTable() { return self::LOG_TABLE; }
  /*  
  Function from IDP2 - removed for JAL1
  public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}
		
	}*/
    
    
	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	
	
	public function getRawObjectGroupedByParent($parent_id,$include_indicator=false) {
		$sql = "SELECT ".$this->getIDFieldName()." as id
		 			, ".$this->getParentFieldName()." as parent
		 			, ".$this->getSecondaryParentFieldName()." as secondary
		 			".($include_indicator ? ", ".$this->getTableField()."_indicator as indicator" :"")."
		 FROM ".$this->getTableName()." AC WHERE AC.".$this->getParentFieldName()." = ".$parent_id." AND ".$this->getActiveStatusSQL("AC");
		$rows = $this->mysql_fetch_all_by_id($sql,"id");
		$data = array();
		
		foreach($rows as $i => $r) {
			if($include_indicator) {
				$data['proj_id'][$r['parent']][$r['secondary']] = $r['secondary'];
				$data['indicator'][$r['parent']][$r['secondary']] = $r['indicator'];
			} else {
				$data[$r['parent']][$r['secondary']] = $r['secondary'];
			}
		}
		
		return $data;
	}	
	
	
	public function getOrderedObjects($object_id=0) {
			$sql = "SELECT ".$this->getIDFieldName()." as id
					, ".$this->getNameFieldName()." as name
					, ".$this->getTableField()."_ref as ref
					, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
					FROM ".$this->getTableName()." A
					WHERE  ".($this->checkIntRef($object_id) ? $this->getIDFieldName()." = ".$object_id." AND " : "")." ".$this->getActiveStatusSQL("A");
				$res2 = $this->mysql_fetch_all_by_id($sql, "id");
		return $res2;
	}

/**
 * Function to get details for viewing / confirmation
 */
	public function getOrderedObjectsWithChildren($object_id=0){
		$res = $this->getOrderedObjects($object_id);
		return $data;
	}


	
	public function getSimpleDetails($id=0) {
		$obj = $this->getRawObject($id);
		$data = array(
			'id'=>$id,
			'name' => $obj[$this->getNameFieldName()].(strlen($obj[$this->getTableField()."_ref"])>0 ? " (".$obj[$this->getTableField()."_ref"].")" : ""),
			'reftag' => $this->getRefTag().$obj[$this->getIDFieldName()],
		);
		return $data;
	}
	



	/**
	 * Function to check if Projects have been assigned to more than 1 KPI
	 * REQUIRED BY IDP2_PROJECT.getObjectForExpectedResult for linking PROJECTs to KPIs
	 */
	public function getCountOfKPIsPerProject($id_array) {
		$kpi_count = array();
		if(count($id_array)>0) {
			$sql = "SELECT 
						".$this->getSecondaryParentFieldName()." as id,
						".$this->getParentFieldName()." as kpi
					FROM ".$this->getTableName()."
					WHERE ".$this->getSecondaryParentFieldName()." IN (".implode(",",$id_array).")
					AND ( ".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
					GROUP BY ".$this->getSecondaryParentFieldName()."
					";
			$kpi_count = $this->mysql_fetch_array_by_id($sql, "id","kpi",true);
		}
		return $kpi_count;
	}
	/**
	 * Function to check if Projects have been assigned to more than 1 KPI
	 * REQUIRED BY IDP2_PROJECT.getObjectForExpectedResult for linking PROJECTs to KPIs
	 */
	public function getCountOfProjectsPerKPI($id_array) {
		$project_count = array();
		if(count($id_array)>0) {
			$sql = "SELECT 
						".$this->getParentFieldName()." as id
						, COUNT(".$this->getSecondaryParentFieldName().") as c
					FROM ".$this->getTableName()."
					WHERE ( ".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
					GROUP BY ".$this->getParentFieldName()."
					";
			$kpi_count = $this->mysql_fetch_value_by_id($sql, "id", "c");
			$sql = "SELECT 
						".$this->getSecondaryParentFieldName()." as project
						, ".$this->getParentFieldName()." as kpi
					FROM ".$this->getTableName()."
					WHERE ".$this->getSecondaryParentFieldName()." IN (".implode(",",$id_array).")
					AND ( ".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE."
					";
			$project_kpi = $this->mysql_fetch_array_by_id($sql, "project", "kpi",true);
			foreach($id_array as $p) {
				if(isset($project_kpi[$p]) && count($project_kpi[$p])>0) {
					$pc = FALSE;
					foreach($project_kpi[$p] as $k) {
						if(isset($kpi_count[$k]) && $kpi_count[$k]>1) {
							$pc = TRUE;
						}
					}
					$project_count[$p] = $pc;
				} else {
					$project_count[$p] = FALSE;
				}
			}
		}
		return $project_count;
	}

	/**
	 * Get records for display on COMMON/IDP_OBJECT page
	 */
	public function getRecordsForListDisplay($kpi_id=array(),$project_id=array(),$get_kpis=false,$get_projects=false) {
		$sql = "SELECT 
				".$this->getParentFieldName()." as kpi, 
				".$this->getSecondaryParentFieldName()." as project, 
				".$this->getTableField()."_indicator as indicator 
				FROM ".$this->getTableName()."
				WHERE (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		if(count($kpi_id)>0) {
			$sql.=" AND ".$this->getParentFieldName()." IN (".implode(",",$kpi_id).")";
		} elseif(count($project_id)>0) {
			$sql.=" AND ".$this->getSecondaryParentFieldName()." IN (".implode(",",$project_id).")";
		}
		$rows = $this->mysql_fetch_all_by_id2($sql, "kpi","project");
		if($get_kpis) {
			if(count($kpi_id)==0) {
				$kpi_id = $kpi_id + array_keys($rows);
			}
			$kpiObject = new IDP2_PMKPI();
			$kpis = $kpiObject->getRawObject($kpi_id);
			foreach($rows as $k => $r) {
				foreach($r as $p => $pr) {
					$rows[$k][$p]['kpi'] = $kpis[$k][$kpiObject->getNameFieldName()];
				}
			}
		}
		if($get_projects) {
			if(count($project_id)==0) {
				foreach($rows as $k => $r) {
					$project_id = $project_id + array_keys($r);
				}
			}
			$projectObject = new IDP2_PROJECT();
			$projects = $projectObject->getRawObject($project_id);
			foreach($rows as $k => $r) {
				foreach($r as $p => $pr) {
					$rows[$k][$p]['project'] = $projects[$p][$projectObject->getNameFieldName()];
				}
			}
		}
		return $rows;
	}
	
		
	
	/*
	public function getRecordsForListDisplay($comp_id=array()) {
		$comp_id = ASSIST_HELPER::removeBlanksFromArray($comp_id);
		$rows = array();
		if(count($comp_id)>0) {
			
			$listObject = new JAL1_LIST("proficiency");
			$prof_objects = $listObject->getActiveListItemsFormattedForSelect(); //ASSIST_HELPER::arrPrint($prof_objects);
			if(count($prof_objects)>0) {
				$sql = "SELECT CP.".$this->getIDFieldName()." as id
							, CP.".$this->getNameFieldName()." as name 
							, CP.".$this->getParentFieldName()." as comp 
							, CP.".$this->getSecondaryParentFieldName()." as prof 
						FROM ".$this->getTableName()." CP 
						WHERE (".$this->getActiveStatusSQL("CP").") 
						AND ".$this->getParentFieldName()." IN (".implode(",",$comp_id).")
						AND ".$this->getSecondaryParentFieldName()." IN (".implode(",",array_keys($prof_objects)).")";
				$res = $this->mysql_fetch_all_by_id($sql, "id");
				$res2 = array(); //ASSIST_HELPER::arrPrint($res);
				foreach($res as $key => $r) { //ASSIST_HELPER::arrPrint($r); echo "<P>".$r['prof']."</p>";
					if(isset($prof_objects[$r['prof']])) {
						$res2[$r['comp']][$r['prof']] = array(
							'id'=>$key,
							'name'=>"<span class=b>".$prof_objects[$r['prof']].":</span> ".$r['name']
						);
					}
				} //ASSIST_HELPER::arrPrint($res2);
				foreach($comp_id as $ci) {
					if(isset($res2[$ci])) {
						$ci_rows = $res2[$ci];
						foreach($prof_objects as $pi => $p) {
							if(isset($ci_rows[$pi])) {
								$r = $ci_rows[$pi];
								$rows[$ci][$r['id']] = $r['name'];
							}
						}
					}
				}
			}
		} //ASSIST_HELPER::arrPrint($rows);
		return $rows;
		
	}
	
	
	
	*/
	
	
	
	
	
	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>