<?php
/**
 * To manage the display of forms / tables and to provide a link to the Assist_module_display class
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */
class IDP2_DISPLAY extends ASSIST_MODULE_DISPLAY {

    //protected $mod_ref;
	protected $default_attach_buttons = true;
	protected $is_edit_page = true;

    public function __construct() {
    	$me = new IDP2();
		$an = $me->getAllActivityNames();
		$on = $me->getAllObjectNames();
		//$this->mod_ref = $me->getModRef();
		unset($me);

        parent::__construct($an,$on);
    }

	public function disableAttachButtons() { $this->default_attach_buttons = false; }
	public function enableAttachButtons() { $this->default_attach_buttons = true; }


	/***********************************************************************************************************************
	 * FORM FIELDS & specific data display
	 */


	public function getDataFieldNoJS($type,$val,$options=array()) {
		$d = $this->getDataField($type, $val,$options);
		return $d['display'];
	}
	/**
	 * (ECHO) displays the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 *
	 * @return JS (Echos HTML)
	 */
	public function drawDataField($type,$val,$options=array()) {
		$d = $this->getDataField($type,$val,$options);
		if(is_array($d)) {
			echo $d['display'];
		} else {
			echo $d;
		}
	}
	/**
	 * (RETURN) generates the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 * 	 *
	 * @return (Array) array('display'=>HTML,'js'=>javascript)
	 */
	public function getDataField($type,$val,$options=array()) {
		$data = array('display'=>"",'js'=>"");
		switch($type) {
			case "PERC":
				$data['display'] = $this->getPercentageForDisplay($val);
				break;;
			case "NUM":
			case "CALC":
				$data['display'] = $this->getNumberForDisplay($val,2);
				break;;
			case "CURRENCY":
				if(strlen($val)==0 || is_null($val)) {  $val = 0;  }
				if(isset($options['symbol'])) {
					$data['display'] = $this->getCurrencyForDisplay($val,$options['symbol']);
				} else {
					$data['display'] = $this->getCurrencyForDisplay($val);
				}
				if(isset($options['right']) && $options['right']==true) {
					$data['display'] = "<div class=right >".$data['display']."</div>";
				}
				break;
			case "DATE":
				if(isset($options['include_time'])) {
					$data['display'] = $this->getDateForDisplay($val,$options['include_time']);
				} else {
					$data['display'] = $this->getDateForDisplay($val,false);
				}
				break;
			case "BOOL":
			case "BOOL_BUTTON":
				$data['display'] = $this->getBoolForDisplay($val,(isset($options['html']) ? $options['html'] : true));
				break;
			case "REF":
				$data['display'] = (isset($options['reftag']) ? $options['reftag'] : "").$val;
				break;
			case "ATTACH":
				$can_edit = isset($options['can_edit']) ? $options['can_edit'] : false;
				$object_type = isset($options['object_type']) ? $options['object_type'] : "X";
				$object_id = isset($options['object_id']) ? $options['object_id'] : "0";
				$buttons = isset($options['buttons']) ? $options['buttons'] : true;
				$data['display'] = $this->getAttachForDisplay($val,$can_edit,$object_type,$object_id,$buttons);
				break;
			//JS TO BE PROGRAMMED FOR LARGE TEXT - HIDE / SHOW
			default:
				if(isset($options['html']) && $options['html']==true) {
					$val = str_ireplace(chr(10), "<br />", $val);
				} else {
					$val = $val;
				}
				$data = array('display'=>$val,'js'=>"");
				//$data = array('display'=>$type);
		}
		return $data;
	}


	/**
	 * (ECHO) displays html of selected form field and returns any required js
	 */
	public function drawFormField($type,$options=array(),$val="") {
		//echo "dFF VAL:-".$val."- ARR: "; print_r($options);
		$ff = $this->createFormField($type,$options,$val);
		//if(is_array($ff['display'])) {
			//print_r($ff);
		//}
		echo $ff['display'];
		return $ff['js'];
	}
	/**
	 * Returns string of selected form field
	 *
	 * @param *(String) type = the type of form field
	 * @param (Array) options = any additional properties
	 * @param (String) val = any existing value to be displayed
	 * @return (String) echo
	 */
	public function createFormField($type,$options=array(),$val="") {
		//echo "<br />cFF VAL:+".$val."+ ARR: "; print_r($options);
		switch($type){
			case "REF":
				$data = array('display'=>$val,'js'=>"");
				break;
			case "LABEL":
				$data=$this->getLabel($val,$options);
				break;
			case "SMLVC":
				$data=$this->getSmallInputText($val,$options);
				break;
			case "MEDVC":
				$data=$this->getMediumInputText($val,$options);
				break;
			case "LRGVC":
				$data=$this->getLimitedTextArea($val,$options);
				break;
			case "TEXT":
				if(isset($options['rows']) && isset($options['cols'])) {
					$rows = $options['rows']; unset($options['rows']);
					$cols = $options['cols']; unset($options['cols']);
					$data=$this->getTextArea($val,$options,$rows,$cols);
				} else {
					$data=$this->getTextArea($val,$options);
				}
				break;
			case "LIST":
				$items = $options['options'];
				unset($options['options']);
				$data=$this->getSelect($val,$options,$items);
				break;
			case "MULTILIST":
				$items = $options['options'];
				unset($options['options']);
				if(!is_array($val)) {
					$val2 = array();
					if(strlen($val)>0) {
						$val2[] = $val;
					}
				} else {
					$val2 = $val;
				}
				$data=$this->getMultipleSelect($val2,$options,$items);
				break;
			case "DATE": //echo "date!";
				$extra = $options['options'];
				unset($options['options']);
				$data=$this->getDatePicker($val,$options,$extra);
				break;
			case "COLOUR":
				$data=$this->getColour($val,$options);
				break;
			case "RATING":
				$data=$this->getRating($val, $options);
				break;
			case "CURRENCY": $size = 15; $class="right";
			case "PERC":
			case "PERCENTAGE": $size = !isset($size) ? 5 : $size;
			case "NUM": $size = !isset($size) ? 0 : $size;
				if(isset($options['symbol'])) {
					$symbol = $options['symbol'];
					$has_sym = true;
					unset($options['symbol']);
					$symbol_postfix = isset($options['symbol_postfix']) ? $options['symbol_postfix'] : false;
					unset($options['symbol_postfix']);
				} else {
					$has_sym = false;
				} //ASSIST_HELPER::arrPrint($options);
				if(isset($class)){ $options['class'] = (isset($options['class']) ? $options['class'] : "")." ".$class; }
                $data=$this->getNumInputText($val,$options,0,$size);
                if($type=="CURRENCY") {
					if(!$has_sym) {
						$data['display']="R&nbsp;".$data['display'];
					} elseif(strlen($symbol)>0) {
						if($symbol_postfix==true) {
							$data['display']=$data['display']."&nbsp;".$symbol;
						} else {
							$data['display']=$symbol."&nbsp;".$data['display'];
						}
					} else {
						//don't add a symbol
					}
                } elseif($type=="PERC" || $type=="PERCENTAGE") {
                      $data['display'].="&nbsp;%";
                }
                break;
			case "BOOL_BUTTON":
				//echo "<br />cFF BB VAL:".$val." ARR: "; print_r($options);
				$data = $this->getBoolButton($val,$options);
				break;
			case "ATTACH"://echo "attach: ".$val;
				//$data = array('display'=>$val,'js'=>"");
				$data = $this->getAttachmentInput($val,$options);
				break;
			case "CALC" :
				$options['extra'] = explode("|",$options['extra']);
				$data = $this->getCalculationForm($val,$options);
				break;
			default:
				$data = array('display'=>$val,'js'=>"");
				break;
		}
		return $data;
	}


















    /**
	 * (ECHO) Function to display the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
    public function drawPageFooter($left="",$log_table="",$var="",$log_id="") {
    	$data = $this->getPageFooter($left,$log_table,$var,$log_id);
		echo $data['display'];
		return $data['js'];
    }
    /**
	 * Function to create the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
    public function getPageFooter($left="",$log_table="",$var="",$log_id="") {
    	$echo = "";
		$js = "";
    	if(is_array($var)) {
    		$x = $var;
			$d = array();
			unset($var);
			foreach($x as $f => $v) {
				$d[] = $f."=".$v;
			}
			$var = implode("&",$d);
    	}
		$echo = "
		<table width=100% class=tbl-subcontainer><tr>
			<td width=50%>".$left."</td>
			<td width=50%>".(strlen($log_table)>0 ? "<span id=".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
				<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
			</span>" : "")."</td>
		</tr></table><div id=".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log></div>";
		if(strlen($log_table)>0){
			$js = "
			$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log\").click(function() {
				var state = $(this).attr('state');
				if(state==\"show\"){
					$(this).find('img').prop('src','/pics/tri_down.gif');
					$(this).attr('state','hide');
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(\"\");
					$(this).find(\"#log_txt\").html('Display Activity Log');
				} else {
					$(this).find('img').prop('src','/pics/tri_up.gif');
					$(this).attr('state','show');
					var dta = '".$var."&log_table='+$(this).attr('table');
					//console.log(dta);
					var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
					//console.log(result);
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(result[0]);
					$(this).find(\"#log_txt\").html('Hide Activity Log');
				}

			});
			";
		}
    	$data = array('display'=>$echo,'js'=>$js);
		return $data;
    }









	/***********************************************************************************************************************
	 * DETAILED OBJECT VIEWS
	 */




	/*****
	 * returns the details table for the object it is fed.
	 *
	 * @param (String) object_type = CONTRACT, DELIVERABLE, ACTION
	 * @param (INT) object_id = primary key
	 * @param (BOOL) include_parent_button = must the detailed view include an option to see the parent object details in a pop-up dialog
	 *
	 */
	public function getParentDetailedView($var) {
		$object_type = $var['object_type'];
		$child_type = $var['child_type'];
		$child_id = $var['child_id'];
		switch($object_type) {
			case "DELIVERABLE":
				$myObject = new IDP2_ACTION();
				break;
			case "CONTRACT":
				if($child_type=="ACTION") {
					$myObject = new IDP2_ACTION();
					$child_id = $myObject->getParentID($child_id);
				}
				$myObject = new IDP2_DELIVERABLE();
				break;
			case "FINANCE":
				$myObject = new IDP2_FINANCE();
				break;
		}
		$object_id = ($object_type == "FINANCE" ? $child_id : $myObject->getParentID($child_id));
		return $this->getDetailedView($object_type, $object_id,false,false,false,array(),false,false);
	}
	/**
	 * Draw the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 *
	 * @echo HTML
	 * @return JS
	 */
	public function drawDetailedView($object_type,$object_id,$include_parent_button=false,$sub_table=false,$view_all_button=false,$button=array(),$compact_view=false,$attachment_buttons="X") {
		if($attachment_buttons=="X") { $attachment_buttons = $this->default_attach_buttons; }
		$me = $this->getDetailedView($object_type, $object_id,$include_parent_button,$sub_table,$view_all_button,$button,$compact_view,$attachment_buttons);
		echo $me['display'];
		return $me['js'];
	}
	/**
	 * Generate the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 *
	 * @return HTML
	 * @return JS
	 */
	public function getDetailedView($object_type,$object_id,$include_parent_button=false,$sub_table=false,$view_all_button=false,$button=array(),$compact_view=false,$attachment_buttons=true) {
		if(is_array($object_type)) {
			$var = $object_type;
			$object_id = $var['object_id'];
			$object_type = $var['object_type'];
		}
		//echo $object_type." :: ".$object_id;
		$js = "";
		$echo = "";
		//$echo = "abc :: ".$object_type." :: ".$object_id;
		//add code to get js from assist_module_display to trigger on attachment click for download
		if($attachment_buttons){
			$js.=$this->getAttachmentDownloadJS($object_type,$object_id);
		}
		$parent_buttons = "";
/*		switch(strtoupper($object_type)) {
			case "IDP":
				$myObject = new IDP2_ACTION();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getObjectName("deliverable")."' class='float btn_parent' id=DELIVERABLE /> <input type=button value='".$myObject->getContractObjectName()."' class='float btn_parent' id=CONTRACT />";
				}
				break;
			case "SUB-DELIVERABLE":
			case "DELIVERABLE":
				$myObject = new IDP2_DELIVERABLE();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getObjectName("contract")."' class='float btn_parent' id=CONTRACT />";
				}
				break;
			case "CONTRACT":
				$myObject = new IDP2_CONTRACT();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getObjectName("FINANCE")."' class='float btn_parent' id=FINANCE />";
				}
				break;
			case "FINANCE":
				$myObject = new IDP2_FINANCE();
				break;
			case "TEMPCON":
				$myObject = new IDP2_TEMPLATE();
				break;
			default:
				$myObject = new IDP2();
				break;
		}
*/
$class_name = "IDP2_".$object_type;
		$myObject = new $class_name();

		if($view_all_button===true) {
			$parent_buttons.="<input class='float btn_view_all' type=button id='view_all' value='View All' />";
		}
		if($object_type=="FINANCE") {
			$result = $myObject->getDetailedObjectForDisplay($object_id);
			$js = $result['js'];
			$echo = $result['display'];
		} else {
			if($include_parent_button || $view_all_button) {
				$js.= "
						$('input:button.btn_view_all').button();
						$('input:button.btn_parent').button().click(function() {
							var my_window = AssistHelper.getWindowSize();
							var w = (my_window.width*".($object_type!="CONTRACT" ? "0.5" :"0.9").").toFixed(0);
							var h = (my_window.height*0.9).toFixed(0);
							var i = $(this).prop('id');
							var dta = 'child_type=".$object_type."&object_type='+i+'&child_id='+".$object_id."
							var x = AssistHelper.doAjax('inc_controller.php?action=Display.getParentDetailedView',dta);
							$('<div />',{html:x.display,title:x.title}).dialog({
								width: w,
								height: h,
								modal: true
							}).find('table.th2 th').addClass('th2');
						});";
			} //echo "cv: ".$compact_view.":";
			//if($object_type != "TEMPCON"){
				$get_object_options = array('type'=>"DETAILS",'id'=>$object_id,'attachment_buttons'=>$attachment_buttons,'compact_view'=>($compact_view));
//				$object = $myObject->getObject(array('type'=>"DETAILS",'id'=>$object_id,'attachment_buttons'=>$attachment_buttons),($compact_view===true?array('page'=>"COMPACT"):array()));
				$object = $myObject->getObject($get_object_options);
				//ASSIST_HELPER::arrPrint($object);
			//}else{
			//	$object = $myObject->getListObject($object_id, "CON");
			//}

			$echo.="
				$parent_buttons
			<h2 class='".($sub_table !== false ? "sub_head":"")."'>".($object_type=="DELIVERABLE" && $sub_table ? "Sub-" : "").$myObject->getObjectName($object_type)." Details</h2>
					<table class='form ".($sub_table !== false ? "th2":"")."' width=100%>";
					foreach($object['head'] as $fld => $head) { //echo $head['type'];
						if(isset($object['rows'][$fld])) {
							$val = $object['rows'][$fld];
							if(is_array($val)) {
								if(isset($val['display'])) {
									$js.=$val['js'];
									$val = $val['display'];
								} else {
									$v = "<table class=th2 width=100%>";
									foreach($val as $a) {
										foreach($a as $b => $c)
										$v.="<tr><th width=40%>".$b.":</th><td>".$c."</td></tr>";
									}
									$v.="</table>";
									$val = $v;
								}
							}
						} else { $val = $fld; }
						$echo.= "
						<tr>
							<th width=40%>".$myObject->replaceObjectNames($head['name']).":</th>
							<td ".($val==$fld ? "class=idelete" : "").">".$val."</td>
						</tr>";
					}
					//ASSIST_HELPER::arrPrint($button);
			if($button !== false && is_array($button) && count($button)>0){
				$echo.="
						<tr>
							<th width=40%></th>
							<td><input type=button value='".$button['value']."' class='".$button['class']."' ref=".$object_id." /></td>
						</tr>";
			}
			$echo.="</table>";
		}

		return array('display'=>$echo,'js'=>$js,'title'=>$myObject->getObjectName($object_type).($object_type!="FINANCE" ? " ".$myObject->getRefTag().$object_id : ""));
		//return array('display'=>$echo,'js'=>$js,'title'=>$myObject->getObjectName($object_type));
	}


















	/***********************************************************************************************************************
	 * LIST VIEWS
	 */




	/**
	 * Paging....
	 */
	public function drawPaging() {
		$me = $this->getPaging();
		echo $me['display'];
		return $me['js'];
	}
	public function getPaging($i,$options,$button,$object_type="",$object_options=array(),$add_button=array(false,"","")) {
		/**************
		$options = array(
			'totalrows'=> mysql_num_rows(),
			'totalpages'=> totalrows / pagelimit,
			'currentpage'=> start / pagelimit,
			'first'=> start==0 ? false : 0,
			'next'=> totalpages*pagelimit==start ? false : (start + pagelimit),
			'prev'=> start==0 ? false : (start - pagelimit),
			'last'=> start==totalpages*pagelimit ? false : (totalpages*pagelimit),
			'pagelimit'=>pagelimit,
		);
		 **********************************/
		$data = array('display'=>"",'js'=>"");
		$data['display'] = "
			<table width=100% style='margin-bottom: 5px;'>
				<tr>
					<td class='page_identifier center'><span id='$i'>
					<span style='float: left' id=spn_paging_buttons>
						<button class=firstbtn></button>
						<button class=prevbtn></button>
						Page <select class='page_picker'>";
			for($p=1;$p<=$options['totalpages'];$p++) {
				$data['display'].="<option ".($p==$options['currentpage'] ? "selected" : "")." value=$p>$p</option>";
			}
		$data['display'].="<option value=ALL>All</option></select> / ".$options['totalpages']."
						<button class=nextbtn></button>
						<button class=lastbtn></button>
					</span>";
		if(isset($add_button [0]) && $add_button[0]==true) {
			$ab_label = $this->replaceAllNames(isset($add_button[1]) && strlen($add_button[1])>0 ? $add_button[1] : "|add|");
			//$data['display'].="<span style='margin: 0 auto' id=spn_paging_add><button id=btn_paging_add class=abutton>".$ab_label."</button></span>";
			$data['display'].="<span style='float:right' id=spn_paging_add><button id=btn_paging_add class=abutton>".$ab_label."</button></span>";
			$data['js'].="
			$('#btn_paging_add').button({
				icons: {
					primary: \"ui-icon-circle-plus\"
				}
			}).click(function(e) {
				e.preventDefault();
				".$add_button[2]."
			}).removeClass(\"ui-state-default\").addClass(\"ui-button-state-ok\").children(\".ui-button-text\").css({\"padding-top\":\"0px\",\"padding-bottom\":\"0px\"})
			.hover(function() { $(this).removeClass(\"ui-button-state-ok\").addClass(\"ui-button-state-info\"); }, function() { $(this).removeClass(\"ui-button-state-info\").addClass(\"ui-button-state-ok\"); });
			";
		}
		$data['display'].="<!-- <span class=float id=spn_paging_search>
							Quick Search: <input type=text /> <input type=button value=Go disabled=true onclick='alert(\"whoops! still need to program the backend for this\")' />
						</span>
						<span style='float:right' class=idelete>STILL UNDER DEVELOPMENT</span> -->
					</span></td>
				</tr>
			</table>";
			$op = "options[set]=true";
			foreach($object_options as $key => $v) {
				$op.="&options[$key]=$v";
			}
		$data['js'].= "
		var paging_url = 'inc_controller.php?action=".$object_type.".GetListTableHTML';
		var paging_page_options = 'button[value]=".$button['value']."&button[class]=".$button['class']."&".$op."';
		var paging_parent_id = '$i';
		var paging_table_id = paging_parent_id+'_list_view';
		var paging_records = [];
		paging_records.totalrows = '".($options['totalrows'])."';
		paging_records.totalpages = '".($options['totalpages'])."';
		paging_records.currentpage = '".($options['currentpage'])."';
		paging_records.pagelimit = '".($options['pagelimit'])."';
		paging_records.first = '".($options['first']!== false ? $options['first'] : "false")."';
		paging_records.prev = '".($options['prev']!== false ? $options['prev'] : "false")."';
		paging_records.next = '".($options['next']!== false ? $options['next'] : "false")."';
		paging_records.last = '".($options['last']!== false ? $options['last'] : "false")."';

		$('#".$i."').paging({url:paging_url,page_options:paging_page_options,parent_id:paging_parent_id,table_id:paging_table_id,paging_options:paging_records});
		$('#".$i."').children('#spn_paging_buttons').find('button').click(function() {
			$('#".$i."').paging('buttonClick',$(this));
		});
		$('#".$i." select.page_picker').change(function() {
			$('#".$i."').paging('changePage',$(this).val());
		});
		";
		return $data;
	}


	/*****
	 * List View
	 */
	/**
	 * Function to draw the list table onscreen
	 * @param (Array) $child_objects = array of objects to display in table
	 * @param (Array) $button = details of button to display in last column
	 * @param (String) $object_type = the name of the object
	 * @param (Array) $object_options = getPaging options
	 * @param (Bool) $sub_table = is this table a child of another object true/false
	 * @param (Array) $add_button = array(0=>true/false, 1=>label if true)
	 */
	public function drawListTable($child_objects,$button=array(),$object_type="",$object_options=array(),$sub_table=false,$add_button=array(false,"","")) {
		//ASSIST_HELPER::arrPrint($child_objects);
		$me = $this->getListTable($child_objects,$button,$object_type,$object_options, $sub_table,$add_button);
		echo $me['display'];
		return $me['js'];
	}
	public function getListTable($child_objects,$button,$object_type="",$object_options=array(), $sub_table=false, $add_button = array(false,"","")) {
		//ASSIST_HELPER::arrPrint($child_objects);
		$data = array('display'=>"<table class=tbl-container><tr><td>",'js'=>"");
		//$items_total = $contractObject->getPageLimit();
		//$these_items = array_chunk($child_objects['rows'], $items_total);
		$page_id = (isset($button['pager']) && $button['pager'] !== false ? $button['pager'] : "paging_obj");
		$page_options = $child_objects['paging'];
		$display_paging = true;
		if($button===false) {
			$display_paging = false;
		} elseif(isset($button['pager']) && $button['pager']===false) {
			$display_paging = false;
		}
		if($display_paging){
			$paging = $this->getPaging($page_id,$page_options,$button,$object_type,$object_options,$add_button);
			$data['display'].=$paging['display'];
			$data['js'].=$paging['js'];
		}
		unset($button['pager']);
		$data['display'].="
			<table class='list ".($sub_table !== false ? "th2":"")."' id='".$page_id."_list_view' width=100%>
				<tr id='head_row'>
					";
					foreach($child_objects['head'] as $fld=>$head) {
						$data['display'].="<th>".$head['name']."</th>";
					}

				$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<th></th>" : "")."
				</tr>";
			$rows = $this->getListTableRows($child_objects, $button);
			$data['display'].=$rows['display']."</table>";
			$data['display'].="</td></tr></table>";
			$data['js'].=$rows['js'];
		return $data;
	}
	public function getListTableRows($child_objects,$button) {
		$data = array('display'=>"",'js'=>"");
		if(count($child_objects['rows'])==0) {
			$data['display'] = "<tr><td colspan=".(count($child_objects['head'])+1).">No items found to display.</td></tr>";
		} else {
			foreach($child_objects['rows'] as $id => $obj) {
				$data['display'].="<tr>";
				foreach($obj as $fld=>$val) {
					if(is_array($val)) {
						if(isset($val['display'])) {
							$data['display'].="<td>".$val['display']."</td>";
							$data['js'].=isset($val['js']) ? $val['js'] : "";
						} else {
							$data['display'].="<td class=idelete>".$fld."</td>";
						}
					} else {
						$data['display'].="<td>".$val."</td>";
					}
				}
				if(isset($button['type']) && $button['type']=="button") {
						$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><button ref=".$id." class='".$button['class']." ui_btn' />".$button['value']."</button></td>" : "");
					$data['js'].="
					$('button.ui_btn').button({
						icons: {primary: \"ui-icon-".(isset($button['icon']) ? $button['icon'] : "pencil")."\"},
					}).children(\".ui-button-text\").css({\"font-size\":\"80%\"}).click(function(e) {
						e.preventDefault();

					}); //.removeClass(\"ui-state-default\").addClass(\"ui-state-info\").css(\"color\",\"#fe9900\")
					";
				} else {
					$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><input type=button value='".$button['value']."' ref=".$id." class='".$button['class']."' /></td>" : "");
				}
				$data['display'].="</tr>";
			}
		}
		return $data;
	}















	/**
	 * Function generate the object form
	 * @param $form_object_type
	 * @param $formObject
	 * @param $form_object_id
	 * @param $parent_object_type
	 * @param $parentObject
	 * @param $parent_object_id
	 * @param $page_action
	 * @param $page_redirect_path
	 * @param $display_form = true (include form tag)
	 * @param $tbl_id = 0 (table id to differentiate between multiple forms on a single page)
	 */
	public function getObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path,$display_form=true,$tbl_id=0) {
		//echo $page_redirect_path;
		$last_deliverable_status = 0 ;
		$is_segment_form = false;
		//$idp_object_id = 0;
		//IF NEED TO RECORD PARENT ID AND IDP ID THEN TRANSMIT AS PARENTID_IDPID
		if(strpos($parent_object_id,"_")!==false) {
			$poi = explode("_",$parent_object_id);
			$parent_object_id = $poi[0];
			$idp_object_id = $poi[1];
		} //echo $idp_object_id;
		if(is_array($form_object_type)) {
			$var = $form_object_type;
			$form_object_type = $var['object_type'];
			$form_object_id = $var['object_id'];
			$page_action = $var['page_action'];
			$page_redirect_path = $var['page_redirect_path'];
			$parent_object_id = $var['parent_id'];
			switch($form_object_type) {
				case "IDP":
					$parent_object_type = "";
					$formObject = new IDP2_IDP($form_object_id);
					$parentObject = null;
					break;
				case "STRATOBJ":
					$parent_object_type = "IDP";
					$formObject = new IDP2_STRATOBJ($form_object_id);
					$parentObject = null;
					break;
				case "STRATFOCUS":
					$parent_object_type = "STRATOBJ";
					$formObject = new IDP2_STRATFOCUS($form_object_id);
					$parentObject = null;
					break;
				case "STRATGOAL":
					$parent_object_type = "STRATFOCUS";
					$formObject = new IDP2_STRATGOAL($form_object_id);
					$parentObject = null;
					break;
				case "STRATRESULT":
					$parent_object_type = "STRATGOAL";
					$formObject = new IDP2_STRATRESULT($form_object_id);
					$parentObject = null;
					break;
				case "PMKPA":
					$parent_object_type = "IDP";
					$formObject = new IDP2_PMKPA($form_object_id);
					$parentObject = null;
					break;
				case "PMPROG":
					$parent_object_type = "PMKPA";
					$formObject = new IDP2_PMPROG($form_object_id);
					$parentObject = null;
					break;
				case "PMKPI":
					$parent_object_type = "PMPROG";
					$formObject = new IDP2_PMKPI($form_object_id);
					$parentObject = new IDP2_PMPROG();
					break;
				case "PROJECT":
					$parent_object_type = "IDP";
					$formObject = new IDP2_PROJECT($form_object_id);
					$parentObject = null;
					break;
				case "SEGMENT": $is_segment_form = true;
				case "FUNCTION":
					$parent_object_type = "IDP";
					$formObject = new IDP2_SEGMENTS($form_object_type);
					$parentObject = null;
					break;
				/*
				 * OLD CODE FROM CNTRCT MODULE FOR REFERENCE PURPOSES ONLY!
				case "DELIVERABLE":
				case "SUB-DELIVERABLE":
					$parent_object_type = "CONTRACT";
					$formObject = new IDP2_DELIVERABLE();
					$parentObject = new IDP2_CONTRACT($parent_object_id);
					break;
				case "ACTION":
					$parent_object_type = "DELIVERABLE";
					$formObject = new IDP2_ACTION;
					$parentObject = new IDP2_DELIVERABLE();
					break;*/
			}
		} elseif($parent_object_type=="SEGMENT") {

		}
		//echo "|".$formObject->getMyObjectName();
		if(stripos($page_action,".")!==false) {
			$pa = explode(".",$page_action);
			$page_action = $pa[1];
			$page_section = strtoupper($pa[0]);
		} else {
			$page_section = "MANAGE";
		}

		$headingObject = new IDP2_HEADINGS();

		$th_class = "";
		$tbl_class = "";

		$attachment_form = false;

		$data = array('display'=>"",'js'=>"");
		$js_end = "";
		$js = $formObject->getExtraObjectFormJS();
		$echo = "";//$parent_object_id.$parent_object_type."";
		if($page_action=="UPDATE") {
			$headings = $headingObject->replaceObjectNames($headingObject->getUpdateObjectHeadings($form_object_type,"FORM"));
		} else {
			if($parent_object_type=="SEGMENT") {
				$is_segment_form = true;
				$head_object_type = $parent_object_type;
			} elseif(stripos($form_object_type,"ASSURANCE")===false) {
				$head_object_type = $form_object_type;
			} else {
				$head_object_type = "ASSURANCE";
			}
			$fld_prefix = $formObject->getTableField()."_";
			$headings = $headingObject->replaceObjectNames($headingObject->getMainObjectHeadings($head_object_type,($page_action=="COMPACT" ? $page_action : "FORM"),((strrpos(strtoupper($page_action),"ADD")!==FALSE)?"NEW":""),$fld_prefix));
		}
		//ASSIST_HELPER::arrPrint($headings);
		//ASSIST_HELPER::arrPrint($headingObject->getHeadingsForLog());

		if($is_segment_form) {
			$pa = "SEGMENTS.".$page_action;
		} else {
			$pa = ucwords($form_object_type).".".$page_action;
		}
		$pd = $page_redirect_path;

		$is_view_page = (strrpos(strtoupper($page_action),"VIEW")!==FALSE) || (strrpos(strtoupper($page_action),"COMPACT")!==FALSE);
		$is_edit_page = (strrpos(strtoupper($page_action),"EDIT")!==FALSE) || (strrpos(strtoupper($page_action),"COPY")!==FALSE);
		$is_update_page = (strrpos(strtoupper($page_action),"UPDATE")!==FALSE);
		$is_add_page = (strrpos(strtoupper($page_action),"ADD")!==FALSE);
		$is_copy_page = (strrpos(strtoupper($page_action),"COPY")!==FALSE);

		$copy_protected_fields = array();
		$copy_protected_heading_types = array();

		if($is_edit_page || $is_view_page) {
			$form_object = $formObject->getRawObject($form_object_id);
			$form_activity = "EDIT";
			if($is_copy_page) {
				$copy_protected_fields = $formObject->getCopyProtectedFields();
				$copy_protected_heading_types = $formObject->getCopyProtectedHeadingTypes();
			}
		} elseif($is_update_page) {
			$form_object = $formObject->getRawUpdateObject($form_object_id);
			$form_activity = "UPDATE";
		} else {
			$form_object = array();
			$form_activity = "NEW";
		}
		//ASSIST_HELPER::arrPrint($form_object);
		$form_name = "frm_object_".$form_object_type."_".$form_object_id."_".strtolower(str_replace(".","",$page_action));
//echo ":form_name:".$form_name.":";
		$js.="  //console.log('".$form_name."');
				var ".$form_name."_page_action = '".$pa."';
				var ".$form_name."_page_direct = '".$pd."';

		".$this->getAttachmentDownloadJS($form_object_type, $form_object_id,$form_activity);

		$echo.="
			<div id=div_error class=div_frm_error>

			</div>
			".($display_form ? "<form name=".$form_name." method=post language=jscript enctype=\"multipart/form-data\">" : "")."
			<input type=hidden name=autof autofocus='autofocus' value='' />
				<table class='form $tbl_class' width=100% id=tbl_object_form_".$tbl_id.">";
			$form_valid8 = true;
			$form_error = array();
			if($parent_object_type!="SEGMENT") {
				foreach($headings['rows'] as $fld => $head) {
					if($head['section']!=$form_object_type) {
						unset($headings['rows'][$fld]);
					}
				}
			}
			unset($headings['rows']['kp_indicator_code']);
			foreach($headings['rows'] as $fld => $head) {
							//echo "<P>".$fld;ASSIST_HELPER::arrPrint($head);
				if($head['parent_id']==0) {
					$val = "";
					$h_type = $head['type'];
					if($h_type!="HEADING" && !in_array($h_type,$copy_protected_heading_types) && !in_array($fld,$copy_protected_fields)) {
						$display_me = true;
						$display_my_row = false;
						$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
						if($head['required']==1) { $options['title'] = "This is a required field."; }
						if($h_type=="LIST" || $h_type=="MULTILIST") {
							$options['class'] = $fld;
							if($display_me) {
								$list_items = array();
								$listObject = new IDP2_LIST($head['list_table']);
								//if($is_view_page) {
									//$list_items = $listObject->getAllListItemsFormattedForSelect();
								//} else {
									$list_items = $listObject->getActiveListItemsFormattedForSelect();
								//}
								if(isset($list_items['list_num'])) {
									$list_parent_association = $list_items['list_num'];
									$list_items = $list_items['options'];
									$options['list_num'] = $list_parent_association;
								}
								$options['options'] = $list_items;
								if(count($list_items)==0 && $head['required']==1) {
									$form_valid8 = false;
									$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}

								if($h_type=="MULTILIST") {
									$options['name'].="[]";
									if($is_edit_page || $is_update_page || $is_view_page) {
										$val = isset($form_object[$fld]) ? $form_object[$fld] : array();
									} else {
										$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : array();
									}
									if(!is_array($val)) {
										$val = explode(";",$val);
										foreach($val as $key => $x) {
											if(strlen($x)==0) { unset($val[$key]); }
										}
									}
									if($is_view_page && is_array($val) && count($val)>0) {
										foreach($val as $key => $x) {
											if($formObject->checkIntRef($x) && isset($options['options'][$x])) {
												$val[$key] = $options['options'][$x];
											} else {
												unset($val[$key]);
											}
										}
										if(count($val)>0) {
											$val = implode("; <br />",$val);
										} else {
											$val = $formObject->getUnspecified();
										}
									}
								} else {
									if($is_edit_page || $is_update_page) {
										$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
									} elseif($is_view_page) {
										$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : $formObject->getUnspecified();
										if($formObject->checkIntRef($val)) {
											$val = isset($options['options'][$val]) ? $options['options'][$val] : $formObject->getUnspecified();
										}
									} else {
										$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
									}
								}
								unset($listObject);
							}
						} elseif(in_array($h_type,array("MASTER","USER","FORECAST","PROJECT","SEGMENT","MULTISEGMENT"))) {
							$list_items = array();
							switch($h_type) {
								case "SEGMENT":
								case "MULTISEGMENT":
									if($h_type=="MULTISEGMENT") {
										$options['name'].="[]";
										if(isset($form_object[$fld])) {
											$v = explode(";",$form_object[$fld]);
											$form_object[$fld] = $v;
										} else {
											$form_object = array();
										}
									}
									$list_object_type = $head['list_table'];
									$listObject = new IDP2_SEGMENTS($list_object_type);
									$list_items = $listObject->getActiveListItemsFormattedForSelect(false,(isset($idp_object_id) ? $idp_object_id : $parent_object_id));

									//ASSIST_HELPER::arrPrint($list_items);
									break;
								case "USER":
									$listObject = new IDP2_USERACCESS();
									$list_items = $listObject->getActiveUsersFormattedForSelect();
									break;
								case "PROJECT":
									$listObject = new IDP2_PROJECT();
									if($is_view_page) {
										$list_items = array();
										if(isset($form_object[$fld])) {
											//ASSIST_HELPER::arrPrint($form_object[$fld]);
											$p_rows = $listObject->getObjectsForKPIs($form_object[$fld]);
											$i_rows = $form_object['kp_indicator'];
											//ASSIST_HELPER::arrPrint($p_rows);
											$vl = array();
											foreach($form_object[$fld] as $v) {
												if(isset($p_rows[$v])) {
													$vl[] = $p_rows[$v]['name']." (".$i_rows[$v].")";
												}
											}
											if(count($vl)>1) {
												$val = "<ul><li>".implode("</li><li>",$vl)."</li></ul>";
											} else if(count($vl)==1) {
												$val = $vl[0];
											} else {
												$val = $formObject->getUnspecified();
											}
										} else {
											$val = $formObject->getUnspecified();
										}
										//echo $val;
									} else {
										$list_items = array(
											'X'=>array(
												'value'=>"Please select ".$parentObject->getObjectName("EXPECTEDRESULT")." above",
												'prop'=>array('disabled'=>"disabled")
											)
										);
										$options['class'] = "project_".$fld;
										$options['name'].="[]";
										$js.="
										$('#kpi_cap_op_no, #kpi_cap_op_yes').click(function () {
											$('#kpi_expresult_id').trigger('change');
										});
										var kpi_proj_list;
										$('#kpi_expresult_id').change(function() {
											var er = $(this).val();
											$('.project_".$fld.":gt(0)').remove();
											$('.project_line_break').remove();
											$"."proj_sel = $('.project_".$fld.":first');
											$"."proj_sel.children('option').remove();
											if(er=='X' || er=='0' || er==0 || er.length==0) {
												$"."proj_sel.append('<option value=X>Please select ".$parentObject->getObjectName("EXPECTEDRESULT")." above</option>');
												$('#btn_project_".$fld."_add').hide();
											} else {
												var cap_op = $('#kpi_cap_op').val();
												var dta = 'expresult_id='+er+'&cap_op='+cap_op+'&is_edit=".($is_edit_page ? "1" : "0")."&kpi_id=".$form_object_id."';
								//console.log(dta);
												var list0 = AssistHelper.doAjax('inc_controller.php?action=PROJECT.getObjectForExpectedResult',dta);
												var list = list0[0];
												kpi_proj_list = list;
												var keys = Object.keys(list);
												var list_len = keys.length;
												//console.log('list');
												//console.log(list0[1]);
												if(list==null || list.length==0) {
													$"."proj_sel.append('<option value=X>ERROR: No ".$parentObject->getObjectName("PROJECT")." linked to that ".$parentObject->getObjectName("EXPECTEDRESULT")." are available for selection</option>');
													$('#btn_project_".$fld."_add').hide();
												} else {
													//console.log(cap_op);
													//console.log(list_len);
													if(parseInt(cap_op)==1 && list_len>1) {
														//console.log('add another');
														$('#btn_project_".$fld."_add').show();
													} else {
														//console.log('not another');
														$('#btn_project_".$fld."_add').hide();
													}
													$"."proj_sel.append('<option value=X>--- Please select ---</option>');
													for(i in list) {
														$"."proj_sel.append('<option value='+i+'>'+list[i]+'</option>');
													}
												}
											}
										});
										";
										if(isset($form_object[$fld]) && is_array($form_object[$fld]) && count($form_object[$fld])>0) {
											$js_end.="
											//console.log('start kpi_expresult & kpi_proj_id check');
												$('#kpi_expresult_id').trigger('change');
												//$"."backup = $('#kpi_proj_id').clone();
												//console.log('kpi_proj_list');
												//console.log(kpi_proj_list);
											";
											$c=0;
											//$v = 4;
											//ASSIST_HELPER::arrPrint($form_object[$fld]);
											foreach($form_object[$fld] as $vk => $v) {
												//if($c>0) {
													if($c>0) { $n = $c; } else { $n = ""; }
												$js_end.="
												//console.log('".$c." :: ".$v." :: ".$vk."');
												$('#".$fld.$n."').children('option').remove();

												//$"."backup.find('option').each(function() {
												for(i in kpi_proj_list) {
													//console.log($(this).prop('value')+' :: '+$(this).text());
													//console.log(i+' :: '+kpi_proj_list[i]);
													if(i==".$v.") {
														//console.log('found');
														$('#".$fld.$n."').append('<option value='+i+' selected>'+kpi_proj_list[i]+'</option>');
													} else {
														$('#".$fld.$n."').append('<option value='+i+'>'+kpi_proj_list[i]+'</option>');
													}
												}
												";
												//}
												$c++;
												$js_end.="
												//console.log('$c');
												//console.log('".count($form_object[$fld])."');
												";
												if($c<count($form_object[$fld])) {
													$js_end.="
													//console.log('trigger');
													$('#btn_project_".$fld."_add').trigger('click');
													";
												}
												//break;
											}
											$js_end.="
												//console.log('kpi_expresult & kpi_proj end');
											";
										}
									}
									break;
								case "FORECAST":
									$options['class'] = "forecast_".$fld;
									$options['name'].="[]";
								case "MASTER":
									$listObject = new IDP2_MASTER($head['list_table']);
									$list_items = $listObject->getActiveItemsFormattedForSelect();
									break;
								default:
									echo $h_type;
									break;
							}
							$options['options'] = $list_items;
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if($is_edit_page || $is_update_page) {
								$val =
									isset($form_object[$fld])
									&& (
										(is_array($form_object[$fld]) && count(ASSIST_HELPER::removeBlanksFromArray($form_object[$fld]))>0)
										||
										(!is_array($form_object[$fld]) && strlen($form_object[$fld])>0 && ASSIST_HELPER::checkIntRef($form_object[$fld]))
									) ? $form_object[$fld] : "X";
							} elseif($is_view_page) { //echo $h_type.$fld;
								if($h_type=="SEGMENT") {
									$val = $form_object[$fld];
									if($formObject->checkIntRef($val)) {
										$val = isset($options['options'][$val]) ? $options['options'][$val] : $formObject->getUnspecified();
									} else {
										$val = $formObject->getUnspecified();
									}
								} elseif($h_type=="MULTISEGMENT") {
									$v = explode(";",$val);
									$y = array();
									foreach($v as $x){
										if($formObject->checkIntRef($x) && isset($options['options'][$x])) {
											$y[] = $options['options'][$x];
										}
									}
									if(count($y)>0) {
										$val = implode("; ",$y);
									} else {
										$val = $formObject->getUnspecified();
									}
								} elseif($h_type!="PROJECT") {
									$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : $formObject->getUnspecified();
									if($formObject->checkIntRef($val)) {
										$val = isset($options['options'][$val]) ? $options['options'][$val] : $formObject->getUnspecified();
									}
									//$val = $form_object[$fld];
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}
							if($h_type=="MULTISEGMENT") {
								$h_type = "MULTILIST";
							} else {
								$h_type = "LIST";
							}
						} elseif(in_array($h_type,array("OBJECT","MULTIOBJECT"))) {
							$list_items = array();
							$list_object_type = $head['list_table'];
							$listObject = new $list_object_type(); //echo $list_object_type;
//							$list_items = $listObject->getLimitedActiveObjectsFormattedForSelect(array($parent_object_type=>$parent_object_id));
							if($list_object_type=="IDP2_STRATRESULT") {
								if($parent_object_type=="IDP" && ASSIST_HELPER::checkIntRef($parent_object_id)) {
									$idp_object_id = $parent_object_id;
								} elseif(ASSIST_HELPER::checkIntRef($parent_object_id)) {
									$idp_object_id = $formObject->getIDPidFromParentID($parent_object_id);
								} else {
									$idp_object_id = $formObject->getIDPidFromMyID($form_object_id);
								} //echo $parent_object_type.":".$parent_object_id.":".$idp_object_id.":";
								$obj1Object = new IDP2_STRATOBJ();
									$obj1_objects = $obj1Object->getOrderedObjects($idp_object_id);
								$focus2Object = new IDP2_STRATFOCUS();
									$focus2_objects = $focus2Object->getOrderedObjects($idp_object_id);
								$goal3Object = new IDP2_STRATGOAL();
									$goal3_objects = $goal3Object->getOrderedObjects($idp_object_id);
								$result4Object = $listObject;
									$result4_objects = $result4Object->getOrderedObjects($idp_object_id);
//ASSIST_HELPER::arrPrint($obj1_objects);
								$os = array();

								$blank_count = 0;
								if($is_view_page) {
									$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
									if($val!="X") {
										$tier1 = "";
										$tier2 = "";
										$tier3 = "";
										$ok = false;
										//echo $val;
										if(isset($obj1_objects[$idp_object_id])) {
											foreach($obj1_objects[$idp_object_id] as $obj1_id => $obj) {
												$tier1 = $obj['name']; //echo "<p>".$tier1;
												if(isset($focus2_objects[$obj1_id])) {
													foreach($focus2_objects[$obj1_id] as $focus2_id => $focus) {
														$tier2 = $focus['name']; //echo "/".$tier2;
														if(isset($goal3_objects[$focus2_id])) {
															foreach($goal3_objects[$focus2_id] as $goal3_id => $goal) {
																$tier3 = $goal['name']; //echo $tier3."/";
																if(isset($result4_objects[$goal3_id])) {
																	foreach($result4_objects[$goal3_id] as $result4_id => $result) {
																		if($result4_id==$val) {
																			$val = $tier1." / ".$tier2." / ".$tier3." / ".$result['name'];
																			$ok = true;
																			break;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
										if(!$ok) {
											$val = $formObject->getUnspecified();
										}
									} else {
										$val = $formObject->getUnspecified();
									}
								} else {
									if(isset($obj1_objects[$idp_object_id])) {
										foreach($obj1_objects[$idp_object_id] as $obj1_id => $obj) {
											$os['OBJ'.$obj1_id] = array('value'=>$obj['name'],'prop'=>array('disabled'=>'disabled'));
											if(isset($focus2_objects[$obj1_id])) {
												foreach($focus2_objects[$obj1_id] as $focus2_id => $focus) {
													$os['FOCUS'.$focus2_id] = array('value'=>"&nbsp;- ".$focus['name'],'prop'=>array('disabled'=>'disabled'));
													if(isset($goal3_objects[$focus2_id])) {
														foreach($goal3_objects[$focus2_id] as $goal3_id => $goal) {
															$os['GOAL'.$goal3_id] = array('value'=>"&nbsp;&nbsp;&ordm; ".$goal['name'],'prop'=>array('disabled'=>'disabled'));
															if(isset($result4_objects[$goal3_id])) {
																foreach($result4_objects[$goal3_id] as $result4_id => $result) {
																	$os[$result4_id] = array('value'=>"&nbsp;&nbsp;&nbsp;+ ".$result['name']);
																	$list_items[$result4_id] = $result['name'];
																}
															} else {
																$os["RESULT".$blank_count."BLANK"] = array('value'=>"&nbsp;&nbsp;&nbsp;+ N/A",'prop'=>array('disabled'=>"disabled"));
																$blank_count++;
															}
														}
													} else {
														$os['GOAL'.$goal3_id."BLANK"] = array('value'=>"&nbsp;&nbsp;&ordm; N/A",'prop'=>array('disabled'=>'disabled'));
														$blank_count++;
													}
												}
											} else {
												$os['FOCUS'.$blank_count."BLANK"] = array('value'=>"&nbsp;- N/A",'prop'=>array('disabled'=>'disabled'));
												$blank_count++;
											}
										}
									}
								}
								//ASSIST_HELPER::arrPrint($os);
								$options['options'] = $os;
							//} elseif($list_object_type=="IDP2_PROJECT") {
								//$options['options'] = array('value'=>array('X'=>"Please select ".$parentObject->getObjectName("RESULT")." above"),'prop'=>array('disabled'=>"disabled"));
							} else {
								$options['options'] = $listObject->getLimitedActiveObjectsFormattedForSelect(array('IDP'=>$parent_object_id));
							}

							//$options['options'] = $list_items;
							/*foreach($list_items as $i => $d) {
								$options['options'][$i] = array('value'=>$d);
							}*/
							$h_type = "LIST";
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.:".implode(";",$list_items).":";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							//echo $form_object[$fld]; echo $fld;
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
							} elseif($is_view_page) {
								//$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : $formObject->getUnspecified();
								//echo $val;
								//ASSIST_HELPER::arrPrint($options);
								//if($formObject->checkIntRef($val)) {
								//	$val = isset($options['options'][$val]) ? $options['options'][$val] : $formObject->getUnspecified();
								//}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}
//echo $val;
						//} elseif($h_type=="SEGMENT") {

						} elseif($h_type=="PMKPI_YEAR" || $h_type == "PMKPI_TARGET") {
							$display_me = false;
							$display_my_row = true;
							//echo "[]".$formObject->getMyObjectName()."{}".$formObject->getMyObjectType();
							if(!is_null($parent_object_id) && ASSIST_HELPER::checkIntRef($parent_object_id)) {
								$idp_id = $formObject->getIDPidFromParentID($parent_object_id);
							} else {
								$idp_id = $formObject->getIDPidFromMyID($form_object_id);
							}
							//$idp_id = $parent_object_id;
							$this->is_edit_page = !$is_view_page;
							$display = $this->getKPITargetTable($idp_id,$form_object_id,!$is_view_page,(isset($form_object[$fld]) ? array($form_object_id=>$form_object[$fld]) : array()));
							/*
							$echo.= $row['display'];
							$js = $row['js'];
							*/







						} elseif($h_type=="DATE") {
							$options['options'] = array();
							$options['class'] = "";
							if($is_edit_page || $is_update_page || $is_view_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 && strtotime($form_object[$fld])>0 ? date("d-M-Y",strtotime($form_object[$fld])) : "";
								if($is_update_page) {
									$options['options']['maxDate'] = "'+0D'";
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
							$options['extra'] = "processCurrency";
							if($is_edit_page || $is_update_page || $is_view_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="NUM") {
							if($is_edit_page || $is_update_page || $is_view_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
								if(intval($val)==$val*1) {
									$val = intval($val);
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="BOOL"){
							$h_type = "BOOL_BUTTON";
							$options['yes'] = 1;
							$options['no'] = 0;
							$options['extra'] = "boolButtonClickExtra";
							if($is_edit_page || $is_update_page || $is_view_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "0";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
							}
						} elseif($h_type=="REF") {
							if($page_action=="Add") {
								$val = "System Generated";
							} else {
								$val = $formObject->getRefTag().$form_object_id;
							}
						} elseif($h_type=="MSCOAREF") {
							if($is_add_page || $is_edit_page) {
								$val = "System Generated";
							} else {
								$val = $form_object[$fld];
							}
						} elseif($h_type=="ATTACH") {
							$attachment_form = true;
							$options['action'] = $pa;
							$options['page_direct'] = $pd;
							$options['can_edit'] = ($is_edit_page || $is_update_page);
							$options['object_type'] = $form_object_type;
							$options['object_id'] = $form_object_id;
							$options['page_activity'] = $form_activity;
							$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
							if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
						} else {
							if($is_edit_page || $is_update_page || $is_view_page) {
								$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
								if($is_view_page) {
									$val = str_replace(chr(10), "<br />", $val);
								}
							} else {
								$val = isset($head['default_value']) ? $head['default_value'] : "";
							}
						}

						if($display_me) {
							if($is_view_page) {
								$display = $this->getDataField($h_type, $val);
							} else {
								$display = $this->createFormField($h_type,$options,$val);
							}
							$js.=$display['js'];
							if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
								if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
									$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
								}
							}

						}
					} elseif(in_array($fld,$copy_protected_fields) || in_array($h_type,$copy_protected_heading_types)) {
						$val = "";
						if($h_type=="ATTACH") {
							$val = "ATTACH";
						} else {
							$val = isset($form_object[$fld]) ? $form_object[$fld] : $fld;

							if($headingObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
								$val = ( (!isset($form_object[$head['list_table']]) || is_null($form_object[$head['list_table']])) ? $this->getUnspecified() : $form_object[$head['list_table']]);
							} else {
								$v = $this->getDataField($head['type'], $val);
								$val = $v['display'];
							}
						}
						$display = array('display'=>$val);
					} else {
						if($is_edit_page || $is_update_page) {
							$pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
						} else {
							$pval = isset($head['default_value']) ? $head['default_value'] : "";
						}
						$sub_head = $headings['sub'][$head['id']];
						if(isset($form_object[$fld]) && is_array($form_object[$fld])) {
							$sub_form_object = $form_object[$fld];
						} else {
							$sub_form_object = isset($form_object[$fld]) ? array($form_object[$fld]) : array();
						}
						if(count($sub_form_object)==0) {
							foreach($sub_head as $shead) {
								$sub_form_object[0][$shead['field']] = "";
							}
						}
						$td = "
						<div class=".$fld."><span class=spn_".$fld.">";
						$add_another[$fld] = false;
						foreach($sub_form_object as $sfo) {
							$td.="<span>";
							$td.="<table class=sub_form width=100%>";
							foreach($sub_head as $shead) {
								$sh_type = $shead['type'];
								$sfld = $shead['field'];
								$options = array('id'=>$sfld,'name'=>$sfld,'req'=>$head['required']);
								$val = "";
								if($sh_type=="LIST") {
									$list_items = array();
									$listObject = new IDP2_LIST($shead['list_table']);
									$list_items = $listObject->getActiveListItemsFormattedForSelect();
									$options['options'] = $list_items;
									if(count($list_items)==0 && $shead['required']==1) {
										$form_valid8 = false;
										$form_error[] = "The ".$shead['name']." list has not been populated but is a required field.";
										$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
									}
									if(count($list_items)>1) {
										$add_another[$fld] = true;
									}
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : 0;
									}
									unset($listObject);
								} elseif($sh_type=="BOOL"){
									$sh_type = "BOOL_BUTTON";
									$options['yes'] = 1;
									$options['no'] = 0;
									$options['extra'] = "boolButtonClickExtra";
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									}
									if(strlen($val)==0) {
										$val = (isset($shead['default_value']) && strlen($shead['default_value'])>0) ? $shead['default_value'] : 0;
									}
								} elseif($sh_type=="CURRENCY") {
									$options['extra'] = "processCurrency";
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
									}
								} else {
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
									}
								}
								$sdisplay = $this->createFormField($sh_type,$options,$val);
								$js.= $sdisplay['js'];
								$td.="
								<tr ".(strlen($shead['parent_link'])>0 ? "class='tr_".$shead['parent_link']."'" : "").">
									<th width=40% class=th2>".$shead['name'].":".($shead['required']==1?"*":"")."</th>
									<td>".$sdisplay['display']."</td>
								</tr>";
							}
						$td.= "
							</table></span>
							";
						}
						$td.="
							</span>
							".($add_another[$fld] ? "<p><input type=button value='Add Another' id=btn_".$fld." /></p>" : "")."
						</div>";
						$td_blank = "";
						if($add_another[$fld]) {
											$td_blank="<div id=".$fld."_blank><span>";
											$td_blank.="<table class=sub_form width=100%>";
											foreach($sub_head as $shead) {
												$sh_type = $shead['type'];
												$sfld = $shead['field'];
												if($fld=="contract_supplier") {
													$options = array('name'=>$sfld."[]",'req'=>$head['required']);
												} else {
													$options = array('id'=>$sfld,'name'=>$sfld,'req'=>$head['required']);
												}
												$val = "";
												if($sh_type=="LIST") {
													$list_items = array();
													$listObject = new IDP2_LIST($shead['list_table']);
													$list_items = $listObject->getActiveListItemsFormattedForSelect();
													$options['options'] = $list_items;
													$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : 0;
													unset($listObject);
												} elseif($sh_type=="BOOL"){
													$sh_type = "BOOL_BUTTON";
													$options['yes'] = 1;
													$options['no'] = 0;
													$options['extra'] = "boolButtonClickExtra";
													$val = (isset($shead['default_value']) && strlen($shead['default_value'])>0) ? $shead['default_value'] : 0;
												} elseif($sh_type=="CURRENCY") {
													$options['extra'] = "processCurrency";
													$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
												}
												$sdisplay = $this->createFormField($sh_type,$options,$val);
												$td_blank.="
												<tr ".(strlen($shead['parent_link'])>0 ? "class=\"tr_".$shead['parent_link']."\"" : "").">
													<th width=40% class=th2>".$shead['name'].":".($shead['required']==1?"*":"")."</th>
													<td>".$sdisplay['display']."</td>
												</tr>";
											}
										$td_blank.= "
											</table></span>
										</div>
											";
						}
						$display = array('display'=>$td.$td_blank);
					}
					if(($head['type']=="FORECAST" || $head['type']=="PROJECT") && !$is_view_page) {
						$code = strtolower($head['type']);
						//var forecast_".$fld."_dom = \"".$display['display']."<br />\";
						$js.="
						var ifyear_count = 1;
						$('#btn_".$code."_".$fld."_add').button().click(function(e) {
							e.preventDefault();
							//console.log(forecast_".$fld."_dom);

							//GET COPY OF ORIGINAL SELECT
							var $"."newObj = $('.".$code."_".$fld.":first').clone();
							/*
							//GET LIST OF ITEMS ALREADY SELECTED
							var keys = new Array();
							$('.".$code."_".$fld."').each(function(){
								if($(this).val()!='X') {
									keys.push(parseInt($(this).val()));
								}
							});
							console.log(keys);

							//DISABLE ITEMS PREVIOUSLY SELECTED
							$"."newObj.find('option').each(function() {
								if($(this).val()!='X') {
									var k = parseInt($(this).val());
									console.log(k);
									if($.inArray(k,keys)>=0) {
										console.log('found');
										$(this).prop('disabled',true);
									}
								}
							});
							*/
							//SET PROPERTIES
							$"."newObj.attr('req',0);
							$"."newObj.attr('title','');
							$"."newObj.prop('id',$"."newObj.prop('id')+ifyear_count);
							";
							if($head['type']=="FORECAST") {
								$js.="$"."newObj.on('change',checkForSelectOptionsToDisable($(this)));";
							} elseif($head['type']=="PROJECT") {
								//$js.="$"."newObj.on('change',$"."newObj,checkForSelectOptionsToDisable($(this),'".$code."_".$fld."'));";
							}
							$js.="
							ifyear_count++;
							$"."newObj.insertBefore( $(this) );
							$('<br class=".$code."_line_break />').insertBefore( $(this) );
							";
							if($head['type']=="FORECAST") {
								$js.="checkForSelectOptionsToDisable($(this));";
							} elseif($head['type']=="PROJECT") {
								$js.="
								checkForSelectOptionsToDisable('".$code."_".$fld."');
								";
							}
							$js.="
						});
						$('.".$code."_".$fld."').css({'margin-top':'2px','margin-botton':'2px'});
						";
						if($head['type']=="PROJECT") {
							$data['js'].="
						$('.".$code."_".$fld."').on('change',{ sel_class : '".$code."_".$fld."' },function(event) {
							//console.log('on change '+$(this).prop('id'));
							checkForSelectOptionsToDisable('".$code."_".$fld."');
						});
										";
						}
						$display['display'].="<br /><button id=btn_".$code."_".$fld."_add fld='$fld' class=float>Add Another</button>";
					}
					if($display_me || $display_my_row) { //echo $fld;
						//if($h_type=="PROJECT") { $this->arrPrint($display); }
						//if(is_array($display['display'])){ echo $fld." :: ".$h_type; ASSIST_HELPER::arrPrint($display); }
						$echo.= "
						<tr id=tr_".$fld." ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
							<th class='".$th_class."' width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
							<td>".$display['display']."
							</td>
						</tr>";
					}
				}
			}
			if(!$form_valid8 && !$is_view_page) {
				$js.="
				$('#div_error').html(error_html).dialog({modal: true,dialogClass:\"dlg_frm_error\"}).addClass('ui-corner-all').css({\"border\":\"2px solid #cc0001\",\"background-color\":\"#fefefe\"});
				AssistHelper.hideDialogTitlebar('id','div_error');
				AssistHelper.hideDialogCSS('class','dlg_frm_error');
				$('#div_error #btn_error').button().click(function() {
					$('#div_error').dialog(\"close\");
				}).blur().parent('p').addClass('float');
				$('html, body').animate({scrollTop:0});
				";
			}
				if(!$is_copy_page && !$is_view_page) {
					$echo.="
					<tr id=tr_save>
						<td></td>
						<td >
							<button class='save-btn' id=btn_save >Save ".$formObject->getObjectName($formObject->getMyObjectName())."</button>
							<input type=hidden name=object_id id=object_id value=".$form_object_id." />
							".(($form_object_type!="IDP" && strrpos(strtoupper($page_action), "ADD")!==FALSE) ? "
								<input type=hidden name=".$formObject->getParentFieldName()." value=\"".$parent_object_id."\" />
								" : "")."
							".(($is_segment_form) ? "
							<input type=hidden name=idp_id value='".$idp_object_id."' />
							<input type=hidden name=segment_section value='".$form_object_type."' />
							" : "")."
			".(($form_object_type!="IDP" && strrpos(strtoupper($page_action), "ADD")===FALSE) ? "
				<span id=spn_deactivate style='position:absolute;right:5px'>
					<button class='deactivate-btn' id=btn-deactivate >Deactivate</button>
				</span>
					" : "")."

						</td>
					</tr>";
				}
			$echo.="
			</table>
		".($display_form ? "</form>" : "");
		$data['js'].="
				var error_html = \"<p>The following errors need to be attended to before any ".$formObject->getObjectName($formObject->getMyObjectName())."s can be added:</p> <ul><li>".implode('</li><li>',$form_error)."</li></ul><p>Where required lists have not been populated, please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>\"


				".$js."


				";

		if($formObject->hasDeadline() && !is_null($parentObject)) {
			$parent_deadline = $parentObject->getDeadlineDate();
			$now = strtotime(date("Y-m-d"));
			$then  = strtotime($parent_deadline);
			$diff = ($then-$now)/(3600*24);
			$data['js'].=" $('#".$formObject->getDeadlineField()."').datepicker(\"option\",\"maxDate\",\"+".$diff."D\");";
		}
		$data['js'].="
			function checkIfValueIsInArray(arr,val) {
				for(i in arr) {
					if(arr[i]==val) {
						return true;
						break;
					}
				}
				return false;
			}
		";
		if($form_object_type=="PMKPI") {
			$data['js'].="
			function preCheckForSelectOptionsToDisable(event) {
				var sel_class = event.data.sel_class;
				var i = event.data.i;
				console.log('pre check '+i);
				checkForSelectOptionsToDisable(sel_class);
			}
			function checkForSelectOptionsToDisable(sel_class) {
				//alert('check!');
				/**
					GET ALL SELECTED YEARS
				**/
				var selValues = new Array();
				selValues.push(0);
				$('.'+sel_class).each(function() {
						if($(this).val()!='X') {
							var v = ($(this).val())*1;
							selValues.push(v);
						}
				});
				/**
					DISABLE SELECTED YEARS
				**/
				$('.'+sel_class).each(function() {
					$(this).children('option').prop('disabled',false);
					var v = $(this).val();
					if(selValues.length>1) {
						$(this).children('option').each(function() {
							var vc = $(this).val();
							if(vc!=v && checkIfValueIsInArray(selValues,vc)==true) {
								$(this).prop('disabled',true);
							}
						});
					}
				});

			}
			";
		}
		if($form_object_type=="IDP") {
			//$echo.="THIS IS IDP FORM";
			$data['js'].="




			$('select:not(.idp_finsys_id)').change(function() {
				//console.log($(this).prop('id'));
				checkForSelectOptionsToDisable($(this));
			});

			function checkForSelectOptionsToDisable($"."sel) {
				/**
					GET ALL SELECTED YEARS
				**/
				var selValues = new Array();
				selValues.push(0);
				$('select:not(.idp_finsys_id)').each(function() {
					if($(this).prop('id').length>0) {
						//console.log($(this).prop('id')+' = '+$(this).val());
						if($(this).val()!='X') {
							var v = ($(this).val())*1;
							selValues.push(v);
						}
					}
				});
				//console.log(selValues);
				/**
					DISABLE SELECTED YEARS
				**/
				$('select:not(.idp_finsys_id)').each(function() {
					$(this).children('option').prop('disabled',false);
					var v = $(this).val();
					if(selValues.length>1) {
						$(this).children('option').each(function() {
							var vc = $(this).val();
							if(vc!=v && checkIfValueIsInArray(selValues,vc)==true) {
								$(this).prop('disabled',true);
								//console.log(vc + ' = found in the selValues array');
							}
						});
					}
				});

			}

			";
		}
		$data['js'].="


				$(\"form[name=".$form_name."] select\").each(function() {
					$(this).trigger('change');

					//if select only has 1 option + unspecified then auto select the second option
					if($(this).children(\"option\").length==2) {
						if($(this).children(\"option:first\").val()==\"X\") {
							$(this).children(\"option:last\").prop(\"selected\",true);
						}
					}
				});

				$(\"form[name=".$form_name."] #tr_save\").find('td').css({'border-left':'1px solid #fff','border-right':'1px solid #fff','border-bottom':'1px solid #fff'})

				$(\"form[name=".$form_name."] button.deactivate-btn\").button({
					icons: {primary: \"ui-icon-trash\"}
				}).removeClass('ui-state-default').addClass('ui-button-state-red').css({'font-size':'90%'})
				.click(function() {
					if(confirm('Are you sure you wish to deactivate this object? All children will be deactivated as well.')==true) {
						AssistHelper.processing();
						var oi = $('form[name=".$form_name."] #object_id').val();
						dta = 'object_id='+oi";
						if($is_segment_form) {
							$a = "SEGMENTS";
							$d = "&section=".$form_object_type;
						} else {
							$d = "";
							$a = $form_object_type;
						}
						$data['js'].="+'".$d."';
						//console.log(dta);
						var result = AssistHelper.doAjax('inc_controller.php?action=".$a.".DEACTIVATE',dta);
						if(result[0]=='ok') {
							window.parent.dialogFinished(result[0],result[1]);
						} else {
							AssistHelper.finishedProcessing(result[0],result[1]);
						}
					}
				});
				$(\"form[name=".$form_name."] button.save-btn\").button({
					icons: {primary: \"ui-icon-circle-plus\"}
				})
				.removeClass('ui-state-default').addClass('ui-button-state-green')
				.click(function() {
					$"."form = $(\"form[name=".$form_name."]\");
					//console.log(AssistForm.serialize($"."form));
					//console.log(".$form_name."_page_action);
					//console.log(".$form_name."_page_direct);";
					//alert(AssistForm.serialize($"."form));";
		if($attachment_form) {
			$data['js'].="
					var f = 0;
					$('#firstdoc input:file').each(function() {
						if($(this).val().length>0) {
							f++;
						}
					});
					//alert(f);
					if(f>0) {
						IDP2Helper.processObjectFormWithAttachment($"."form,".$form_name."_page_action,".$form_name."_page_direct);
					} else {
						$('#has_attachments').val(0);

						//alert(AssistForm.serialize($"."form));
						IDP2Helper.processObjectForm($"."form,".$form_name."_page_action,".$form_name."_page_direct);
					}
					";
		} else {
			$data['js'].="
			//console.log('no attachment form');
					IDP2Helper.processObjectForm($"."form,".$form_name."_page_action,".$form_name."_page_direct);
					";
		}
		$data['js'].="
					return false;
				});

				function boolButtonClickExtra($"."btn) {
					var i = $"."btn.prop(\"id\");
					var us = i.lastIndexOf(\"_\");
					var tr = \"tr_\"+i.substr(0,us);
					var act = i.substr(us+1,3);
					if(act==\"yes\") {
						$(\"tr.\"+tr).show();
					} else {
						$(\"tr.\"+tr).hide();
					}
				}

				function processCurrency($"."inpt) {
					var h = $"."inpt.parents(\"tr\").children(\"th\").html();
					h = h.substr(0,h.lastIndexOf(\":\"));
					alert(\"Only numbers (0-9) and a period (.) are permitted in the \"+h+\" field.\");
				}
				";
				if(isset($add_another) && count($add_another)>0) {
					foreach($add_another as $key => $aa) {
						if($aa==true) {
							$data['js'].= "
							var ".$key." = $('#".$key."_blank').html();
							$('#".$key."_blank').hide();
							$('#btn_".$key."').click(function() {
								$('div.".$key." span').children('span:last').after(".$key.");
							});
							";
						}
					}
				}

				$data['js'].="

				$(\"button\").each(function() {
						if($(this).attr(\"button_status\")==\"active\") {
							$(this).trigger(\"click\");
						}
				});



			";
				$data['js'].="".$js_end."";

		$data['display'] = $echo;
		return $data;




	}





















	/*******************
	 * JS code for handling forms displayed inside iframe inside dialogs
	 *
	 * @param (String) object_type
	 * @param (Array) additional variables
	 * @return (String) JQuery
	 */
	public function getIframeDialogJS($object_type,$parent_dlg_id,$var,$manage_height=true) {
		$echo = "";

		$echo.= "
		//console.log('getIframeDialogJS');
		var dlg_size_buffer = 70;
		var ifr_size_buffer = 20;
		window.parent.$('#".$parent_dlg_id."').dialog('open');
		";
		if($manage_height===true) {

			$echo.= "
			var my_height = $('table.tbl-container').css('height');
			if(AssistString.stripos(my_height,'px')>0) {
				my_height = parseInt(AssistString.substr(my_height,0,-2))+ifr_size_buffer;
				window.parent.$('#".$parent_dlg_id."').find('iframe').prop('height',(my_height)+'px');
				var dlg_height = window.parent.$('#".$parent_dlg_id."').dialog('option','height');
				var test_height = my_height+dlg_size_buffer;
				if(dlg_height > test_height) {
					window.parent.$('#".$parent_dlg_id."').dialog('option','height',(test_height));
					var check = !(AssistHelper.hasScrollbar(window.parent.$('#".$parent_dlg_id."')));
					while(!check) {
						window.parent.$('#".$parent_dlg_id."').dialog('option','height',(test_height));
						test_height+=dlg_size_buffer;
						if(!(dlg_height > test_height) || !(AssistHelper.hasScrollbar(window.parent.$('#".$parent_dlg_id."')))) {
							check = true;
						}
					}
				}
			}
			";
		}
		return $echo;
	}
















































































	/**
	 * Returns filter for selecting parent objects according to section it is fed
	 *
	 * @param *(String) section = the current object whose parents need to be found
	 * @param (Array) options = an array of options to be displayed in the filter
	 * @return (String) echo
	 */
	public function getFilter($section, $array = array()) {
        $data = array('display'=>"",'js'=>"");
		$data['display']="
			<div id='filter'>";

		switch (strtoupper($section)) {
			case 'CONTRACT':
				$data['display'].="
							<table>
								<tr>
									<th>Select a financial year</th>
										<form id='filterform'>
											<td>
											<select id='yearpicker'>";
				foreach($array as $index=>$key){
					$data['display'].="<option start='".$key['start_date']."' end='".$key['end_date']."' value=".$key['id'].">".$key['value']."</option>";
				}
				$data['js']="
					fyear = 0;

					$('#yearpicker').change(function(){
						//window.fin_year = ($(this).prop('value'));
						var fyear = $(this).prop('value');
						refreshContract(fyear);
					});
				";
				break;

			case 'DELIVERABLE':
				$dsp = "contracts get";

				break;

			case 'ACTION':
				$dsp = "deliverables get";

				break;

			default:
				$dsp = "Invalid arguments supplied";
				break;
		}

		$data['display'].="
							</select>
						</td>
					</form>
				</table>
			</div>";

        return $data;
    }


	/**
	 * Returns list table for the object it is fed - still in early phase, only works for CONTRACTS for now -> doesn't return the javascript
	 *
	 * @param *(String) section = the current object whose list table you want to draw
	 * @param (Array) fyears = an array of financial years taken from IDP2_MASTER->getActiveItems();
	 * @param (Array) headings = an array of the headings to use for the top row of the table, taken from IDP2_HEADINGS->getMainObjectHeadings(eg "CONTRACT", "LIST", "NEW");
	 * @return (Array) array with ['display'] containing the html to be echo'd, and ['js'] to be put into the script (coming soon...)
	 */
    public function getListView($section, $fyears=array(), $headings=array()){
    	$data = array('display'=>"",'js'=>"");
			switch(strtoupper($section)){
				case "CONTRACT":
					$filter = $this->getFilter($section, $fyears);
					//For the financial year filter
					$data['display'] = $filter['display'];
					//For the list view table
					$data['display'].=
									'<table class=tbl-container><tr><td>
										<table class=list id=master_list>
											<tbody>
												<tr>
													<td colspan=42>
														<div id="page_selector">
															<input type="image" src="../pics/tri_left.gif" id=page_first />
															<input type="image" value="back" id=page_back />
															<select id="pages_list">
																<option value=1>1</option>
															</select>
															<input type="image" value="next" id=page_next />
															<input type="image" src="../pics/tri_right.gif" id=page_last />
															<input type="image" src="../pics/tri_down.gif" id=show_all />
														</div>
													</td>
												</tr>
												<tr id=head_list>';

												foreach($headings as $key=>$val){
													$string = "<th id='".$val['field']."' >".$val['name']."</th>";
													$data['display'].= $string;
												}
					$data['display'].=
													"<th class='last_head'></th>
												</tr>
												<tr id=hidden_row style=\"visibility: hidden\">
												</tr>
											</tbody>
										</table>
									</td></tr></table>";
					//For the javascript

					break;
				default:
					$data['display'] = "Lol, not for you";
					break;
			}

		return $data;
    }



	public function drawTreeTier($object_type,$level,$obj,$page_action,$extra_btn_class="",$can_i_view=true,$can_i_edit=true,$text="") {
		switch($page_action) {
			case "CONFIRM":
			case "VIEW":
				$this->drawTreeTierForView($object_type, $level, $obj,$extra_btn_class, $can_i_view,$text);
				break;
			default:
				$this->drawTreeTierForEdit($object_type, $level, $obj,$extra_btn_class, $can_i_view,$can_i_edit);
				break;
		}
	}

	public function drawTreeTierForEdit($object_type,$level,$obj,$extra_btn_class="",$can_i_view=true,$can_i_edit=true) {
		$placeholder_td = "";
		$pos_class = "";
		$icon_class = "";
		$has_btn = true;
		$btn_class = "expand-btn";
		$tr_class = "";
		switch($level) {
			case 1:
				$pos_class = "uni-pos";
				break;
			case 2:
				$pos_class = "bi-pos";
				$icon_class = "uni-icon";
				$placeholder_td = "";
				break;
			case 3:
				$pos_class = "tri-pos";
				$icon_class = "bi-icon";
				$placeholder_td = "<td></td>";
				break;
			case 4:
				$pos_class = "quad-pos";
				$icon_class = "tri-icon";
				$placeholder_td = "<td></td><td></td>";
				break;
		}
		switch($object_type) {
			case "tier1":
				$has_btn = false;
				$td_btn = "";
				$tr_class = "grand-parent";
				break;
			case "tier2":
				$tr_class = "parent";
				break;
			case "tier3":
				$tr_class = "sub-parent";
				break;
			case "tier4":
				$btn_class = "sub-btn";
				$tr_class = "child";
				break;
		}
		if($has_btn) {
			$td_btn = "<td class='tree-icon ".$icon_class."'><button class='ibutton ".$btn_class."'>X</button></td>";
		}
		echo "
		<tr class=".$tr_class.">
			".$placeholder_td."
			".$td_btn."
			<td class='".$pos_class."'>".$obj['name']." <span class=float style='font-size:75%; margin-left: 20px;'>[".$obj['reftag']."]</span></td>
			<td class='center td-button' object_type=".$object_type." object_id=".$obj['id'].">
				".($can_i_view ? "<button class='action-button $extra_btn_class view-btn' parent_id=".$obj['id'].">View</button>":"")."
				".($can_i_edit ? "<button class='action-button $extra_btn_class edit-btn' parent_id=".$obj['id'].">Edit</button>":"")."
			</td>
		</tr>
		";

	}

	public function drawTreeTierForView($object_type,$level,$obj,$extra_btn_class="",$can_i_view=true,$text="") {
		$placeholder_td = "";
		$pos_class = "";
		$icon_class = "";
		$has_btn = true;
		$btn_class = "expand-btn";
		$tr_class = "";
		switch($level) {
			case 1:
				$pos_class = "uni-pos";
				break;
			case 2:
				$pos_class = "bi-pos";
				$icon_class = "uni-icon";
				$placeholder_td = "";
				break;
			case 3:
				$pos_class = "tri-pos";
				$icon_class = "bi-icon";
				$placeholder_td = "<td></td>";
				break;
			case 4:
				$pos_class = "quad-pos";
				$icon_class = "tri-icon";
				$placeholder_td = "<td></td><td></td>";
				break;
		}
		switch($object_type) {
			case "tier1":
				$has_btn = false;
				$td_btn = "";
				$tr_class = "grand-parent";
				break;
			case "tier2":
				$tr_class = "parent";
				break;
			case "tier3":
				$tr_class = "sub-parent";
				break;
			case "tier4":
				$btn_class = "sub-btn";
				$tr_class = "child";
				break;
		}
		if($has_btn) {
			$td_btn = "<td class='tree-icon ".$icon_class."'><button class='ibutton ".$btn_class."'>X</button></td>";
		}
		echo "
		<tr class=".$tr_class.">
			".$placeholder_td."
			".$td_btn."
			<td class='".$pos_class."'>".$obj['name']." <span class=float style='font-size:75%;margin-left: 20px;'>$text".(strlen($text)>0?"&nbsp;\&nbsp;":"")."[".$obj['reftag']."]</span></span></td>
			<td class='center td-button' object_type=".$object_type." object_id=".$obj['id'].">
				".($can_i_view ? "<button class='action-button $extra_btn_class view-btn' parent_id=".$obj['id'].">View</button>":"")."
			</td>
		</tr>
		";

	}



































	/************************************8
	 * Project details table functions
	 */
	public function drawProjectDetailTable($tier,$object_type,$project_id,$edit_button=true,$add_row=true,$summary=false,$objects=array(),$idp_id=0,$years=array(),$get_parent_field=false) {
		$display = $this->getProjectDetailTable($tier, $object_type, $project_id,$edit_button,$add_row,$summary,$objects,$idp_id,$years,$get_parent_field);
		echo $display['display'];
		return $display['js'];
	}

	public function getProjectDetailTable($tier,$object_type,$project_id,$edit_button=true,$add_row=true,$summary=false,$objects=array(),$idp_id=0,$years=array(),$get_parent_field=false) {
		//$tier = "";
		if($idp_id==0) {
			$t1Object = new IDP2_PROJECT();
			$t1_object_details = $t1Object->getSimpleDetails($project_id);
			$idp_object_id = $t1_object_details['parent_id'];
		} else {
			$idp_object_id = $idp_id;
		}

		$idpObject = new IDP2_IDP();
		$idpyearsObject = new IDP2_IDPYEARS();
		$idpYears = $idpyearsObject->getYearsForSpecificIDP($idp_object_id);

		$headingObject = new IDP2_HEADINGS();

		switch($object_type) {
			case "PROJECTINCOME":
				$tObject = new IDP2_PROJECTINCOME();
				break;
			case "PROJECTCOST":
				$tObject = new IDP2_PROJECTCOST();
				break;
			case "PROJECTKPI":
				$tObject = new IDP2_PROJECTKPI();
				break;
		}
		$t_object_name = $tObject->getMyObjectName();
		$t_headings = $headingObject->getMainObjectHeadings($object_type,"FULL","NEW");
		$t_parent_name = $tObject->getParentFieldName();

		if(!$summary) {
			$t_row_headings = $headingObject->getMainObjectHeadings($object_type."_YEAR","FULL","NEW");
		} else {
			$t_row_headings = $headingObject->getMainObjectHeadings($object_type."_YEAR","COMPACT","NEW");
			$t_row_headings = $t_row_headings['rows'];
		}
		$t_objects = (!is_array($objects) || count($objects)==0) && !is_null($project_id) ? $tObject->getChildObjectsForTable($project_id) : $objects;
		$t_object_type = $object_type;

		//ASSIST_HELPER::arrPrint($t_headings);

		if($get_parent_field) {
			$t_headings = array(
				$t_parent_name=>array(
'id' => null,
'field' => $t_parent_name,
'name' => $tObject->getObjectName(IDP2_PROJECT::OBJECT_NAME),
'section' => $tObject->getMyObjectType(),
'type' => "TEXT",
'list_table' => "",
'max' => 0,
'parent_id' => 0,
'parent_link' => "",
'default_value' => "",
'required' => 1,
'help' => "",
				)
			)+$t_headings;
		}
		$t_headings = $headingObject->replaceAllNames($t_headings);
		$echo = "";
		$js = "";
		//ASSIST_HELPER::arrPrint($t_headings);
$echo.= "<form name=frm_".$tier.">
<table id=tbl_".$tier." class=tbl_project_elements>
	<tr>
	";
	foreach($t_headings as $fld=>$head) {
		$echo.= "<th>".$head['name']."</th>";
	}
	if(!$summary) {
		$echo.= "<th></th>";
	}
	foreach($idpYears as $iy_id => $iy) {
		$echo.= "<th>".$iy."</th>";
	}
	$echo.= ($add_row || $edit_button ? "
		<th></th>" : "")."
	</tr>";

	$all_list_items = array();

	//ADD ROW
	if($add_row) {
		$echo.= "
		<tr id=tr_".$tier."_add>
		";
		foreach($t_headings as $fld=>$head) {
			$echo.= "<td class=center rowspan=".count($t_row_headings).">";
			switch($head['type']) {
				case "LIST":
				case "SEGMENT":
				case "OBJECT":
					$options = array(
						'id'=>$fld,
						'name'=>$fld,
						'options'=>array(),
						'req'=>true,
						'unspecified'=>false,
					);
					if($head['type']=="LIST") {
						$listObject = new IDP2_LIST($head['list_table']);
						$options['options'] = $listObject->getActiveListItemsFormattedForSelect();
						$all_list_items[$fld] = $listObject->getAllListItemsFormattedForSelect();
					} elseif($head['type']=="SEGMENT") {
						$listObject = new IDP2_SEGMENTS($head['list_table']);
						$options['options'] = $listObject->getActiveListItemsFormattedForSelect();
						$all_list_items[$fld] = $listObject->getAllListItemsFormattedForSelect();
					} else {
						$list_table_class = $head['list_table'];
						$listObject = new $list_table_class; //echo $list_table_class;
						if($list_table_class=="IDP2_PMKPI") {
							$kpaObject = new IDP2_PMKPA();
							$kpa_objects = $kpaObject->getOrderedObjects($idp_object_id);
							$progObject = new IDP2_PMPROG();
							$prog_objects = $progObject->getOrderedObjects($idp_object_id);
							$kpi_objects = $listObject->getOrderedObjects($idp_object_id);
							//ASSIST_HELPER::arrPrint($kpa_objects);
							//ASSIST_HELPER::arrPrint($prog_objects);
							//ASSIST_HELPER::arrPrint($kpi_objects);
							$os = array();
							foreach($kpa_objects[$idp_object_id] as $kpa_id => $kpa) {
								$os['KPA'.$kpa_id] = array('value'=>$kpa['name'],'prop'=>array('disabled'=>'disabled'));
								foreach($prog_objects[$kpa_id] as $prog_id => $prog) {
									$os['PROG'.$prog_id] = array('value'=>"- ".$prog['name'],'prop'=>array('disabled'=>'disabled'));
									foreach($kpi_objects[$prog_id] as $kpi_id => $kpi) {
										$os[$kpi_id] = "-- ".$kpi['name'];
									}
								}
							}
							$options['options'] = $os;
						} else {
							$options['options'] = $listObject->getLimitedActiveObjectsFormattedForSelect(array('IDP'=>$idp_object_id));
						}
						$all_list_items[$fld] = $listObject->getAllObjectsFormattedForSelect();
					}
					unset($listObject);
					$display = $this->createFormField("LIST",$options);
					$echo.= $display['display'];
					$js.=$display['js'];
					break;
				case "REF":
					$echo.= "#<input type=hidden name=".$t_parent_name." value=".$project_id." />";
					break;
				default:
					$options = array(
						'id'=>$fld,
						'name'=>$fld,
						'req'=>($head['required']==1?true:false),
						'size'=>10,
					);
					$display = $this->createFormField($head['type'],$options);
					$echo.= $display['display'];
					$js.=$display['js'];
					break;
			}
			$echo.= "</td>";
		}
		$c=1;
		$k = array_keys($t_row_headings);
		$final_key = $k[count($k)-1];
		foreach($t_row_headings as $rfld => $rhead) {
			if($c>1) {
				$echo.= "</tr><tr ".($rhead['type']=="CALC" ? "class=sub-total" : "").">";
			}
			if(!$summary) {
				$echo.= "<td class='right b'>".$rhead['name'].":</td>";
			}
			foreach($idpYears as $iy_id => $iy) {
				$echo.= "<td class=right>";
					$options = array(
						'id'=>$rfld."_".$iy_id,
						'name'=>$rfld."[".$iy_id."]",
						'warn'=>false,
					);
					if($rhead['type']!="CALC") {
						$options['class'] = "CALC CALC_".$final_key."_".$iy_id;
						$options['calc_field'] = "CALC_".$final_key."_".$iy_id;
						$display = $this->createFormField($rhead['type'],$options,"0");
					} else {
						$options['class'] = "CALC_".$final_key."_".$iy_id;
						$display = $this->createFormField("LABEL",$options,"0");
						$display['display']="".$display['display']."<input type=hidden name=".$rfld."[".$iy_id."] id="."CALC_".$final_key."_".$iy_id."_hide value=0 />";
						$display['js'].="
						$('.CALC').blur(function() {
							var i = $(this).attr('calc_field');
							var total = 0;
							$(\"input:text.\"+i).each(function() {
								total+=parseFloat($(this).val());
							});
							//var i = $(this).prop('id');
							//console.log(i);
							//console.log(total);
							$('#'+i+'_hide').val(total);
						});
						";
					}
					$echo.= $display['display'];
					$js.=$display['js'];
				$echo.= "</td>";
			}
			if($c==1) {
				$echo.= "
				<td rowspan=".count($t_row_headings).">
					<button id=btn_".$tier."_add class=btn_add object_type=".$t_object_type." tier=".$tier." >Add</button>
				</td>";
			}
			$c++;
		}
		$echo.= "
		</tr>";
	}else{
		//if no add row then still get list items for child objects
		foreach($t_headings as $fld=>$head) {
			switch($head['type']) {
				case "LIST":
				case "SEGMENT":
				case "OBJECT":
					$options = array(
						'id'=>$fld,
						'name'=>$fld,
						'options'=>array(),
						'req'=>true,
						'unspecified'=>false,
					);
					if($head['type']=="LIST") {
						$listObject = new IDP2_LIST($head['list_table']);
						$options['options'] = $listObject->getActiveListItemsFormattedForSelect();
						$all_list_items[$fld] = $listObject->getAllListItemsFormattedForSelect();
					} elseif($head['type']=="SEGMENT") {
						$listObject = new IDP2_SEGMENTS($head['list_table']);
						$options['options'] = $listObject->getActiveListItemsFormattedForSelect();
						$all_list_items[$fld] = $listObject->getAllListItemsFormattedForSelect();
					} else {
						$list_table_class = $head['list_table'];
						$listObject = new $list_table_class;
						$options['options'] = $listObject->getActiveObjectsFormattedForSelect();
						$all_list_items[$fld] = $listObject->getAllObjectsFormattedForSelect();
					}
					unset($listObject);
					break;
				case "REF":
				default:
					break;
			}
		}

	}
	//Child records
	foreach($t_objects as $t_id => $t_row) {
		$row_class = "tr_".$tObject->getMyObjectType()."_".$t_id."";
		$echo.= "
		<tr class='".$row_class."'>
		";
		foreach($t_headings as $fld=>$head) {
			$echo.= "<td class=left rowspan=".count($t_row_headings).">";
			switch($head['type']) {
				case "LIST":
				case "SEGMENT":
				case "OBJECT":
					$echo.= $all_list_items[$fld][$t_row[$fld]];
					break;
				case "REF":
					$echo.= $tObject->getRefTag().$t_id;
					break;
				default:
					$echo.= $t_row[$fld];
					break;
			}
			$echo.= "</td>";
		}
		$c=1;
		$k = array_keys($t_row_headings);
		$final_key = $k[count($k)-1];
		foreach($t_row_headings as $rfld => $rhead) {
			if($c>1) {
				$echo.= "</tr><tr  class='".$row_class."'>";
			}
			if(!$summary) {
				$echo.= "<td class='right b'>".$rhead['name'].":</td>";
			}
			foreach($idpYears as $iy_id => $iy) {
				$echo.= "<td class=right>";
				$val = isset($t_row['years'][$iy_id][$rfld]) ? $t_row['years'][$iy_id][$rfld] : 0;
				$display = $this->getDataField($rhead['type'], $val);
				$echo.= $display['display'];
				$echo.= "</td>";
			}
			if($c==1 && $edit_button) {
				$echo.= "
				<td rowspan=".count($t_row_headings).">
					<button id=btn_".$tier."_delete class=btn_delete object_type=".$t_object_type." tier=".$tier." object_id=".$t_id." object_ref='".$tObject->getRefTag().$t_id."'>Delete</button>
				</td>";
			}
			$c++;
		}

		$echo.= "
		</tr>";
	}
	$echo.= "
</table>
</form>";


		$_SESSION[$this->getModRef()]['PROJECT'][$project_id]['DETAILS'][$object_type] = array(
			'headings' =>$t_headings,
			'row_headings'=>$t_row_headings,
			'list_items'=>$all_list_items,
			'idp_years'=>$idpYears,
		);


		return array('display'=>$echo,'js'=>$js);

	}


	public function getProjectDetailChildRow($tier,$object_type,$project_id,$t_row) {
		$t_headings = $_SESSION[$this->getModRef()]['PROJECT'][$project_id]['DETAILS'][$object_type]['headings'];
		$t_row_headings = $_SESSION[$this->getModRef()]['PROJECT'][$project_id]['DETAILS'][$object_type]['row_headings'];
		$all_list_items = $_SESSION[$this->getModRef()]['PROJECT'][$project_id]['DETAILS'][$object_type]['list_items'];
		$idpYears = $_SESSION[$this->getModRef()]['PROJECT'][$project_id]['DETAILS'][$object_type]['idp_years'];
		$edit_button = false;

		$class_name = "IDP2_".$object_type;
		$classObject = new $class_name();
		//$t_row = $classObject->getMostRecentRawObject($project_id);

		$echo = "";
		$js = "";

		$echo.= "
		<tr>
		";
		foreach($t_headings as $fld=>$head) {
			$echo.= "<td class='".($head['type']=="REF"?"center":"")."' rowspan=".count($t_row_headings).">";
			switch($head['type']) {
				case "LIST":
				case "OBJECT":
				case "SEGMENT":
					$echo.= $all_list_items[$fld][$t_row[$fld]];
					break;
				case "REF":
					$echo.= $classObject->getRefTag().$t_row['object_id'];
					break;
				default:
					$echo.= $t_row[$fld];
					break;
			}
			$echo.= "</td>";
		}
		$c=1;
		$k = array_keys($t_row_headings);
		$final_key = $k[count($k)-1];
		foreach($t_row_headings as $rfld => $rhead) {
			if($c>1) {
				$echo.= "</tr><tr ".($rhead['type']=="CALC" ? "class=sub-total" : "").">";
			}
			$echo.= "<td class='right b'>".$rhead['name'].":</td>";
			foreach($idpYears as $iy_id => $iy) {
				$echo.= "<td class=right>";
				$val = $t_row[$rfld][$iy_id];
				$display = $this->getDataField("CURRENCY", $val);
				$echo.= $display['display'];
				$echo.= "</td>";
			}
			if($c==1 ) {
				$echo.= "<td rowspan=".count($t_row_headings)." class=td_edit>";
				if($edit_button) {
					"<button id=btn_".$tier."_edit class=btn_edit object_type=".$t_object_type." tier=".$tier." object_id=".$t_id.">Edit</button>";
				}
				$echo.="</td>";
			}
			$c++;
		}

		$echo.= "
		</tr>";
		return array(0=>"ok",'display'=>$echo,'js'=>$js);
	}











	public function getKPITargetTable($idp_id=0,$kpi_id=0,$is_edit_page=true,$rows=array()) {//,$objects=array(),$years=array(),$get_parent_field=false) {
		//$is_edit_page = $this->is_edit_page;
		$tier = "kpi_target";
		$object_type = "PMKPI";
		$project_id = 0;
		$edit_button = false;
		$add_row = false;
		$summary = false;

		$idp_object_id = $idp_id;

		$idpObject = new IDP2_IDP();
		$idpyearsObject = new IDP2_IDPYEARS();
		$idpYears = $idpyearsObject->getYearsForSpecificIDP($idp_object_id);

		$headingObject = new IDP2_HEADINGS();

		$parentObject = new IDP2_PMKPI();
		$tObject = new IDP2_PMKPI_TARGET();
		$multiple_kpis = false;
		$line_ids = array();
			//ASSIST_HELPER::arrPrint($kpi_id);
		if(is_array($kpi_id)) {
			$multiple_kpis = true;
			$summary = true;
			//$kpi_id = ASSIST_HELPER::removeBlanksFromArray($kpi_id);
			if(count($kpi_id)>0) {
				//Check to see if only kpi ids has been sent through or if additional details have been added
				$a_keys = array_keys($kpi_id);
				if(is_array($kpi_id[$a_keys[0]])) {
					$parent_details = $kpi_id;
					$kpi_id = $a_keys;
					$line_ids = $kpi_id;
				}
				//get target details
				if(count($rows)==0) {
					$objects = $tObject->getRawObjectGroupedByParent($kpi_id);
				} else {
					$objects = $rows;
				}
			} else {
				$objects = array();
			}
		} else if(ASSIST_HELPER::checkIntRef($kpi_id)) {
			$line_ids = array($kpi_id);
				if(count($rows)==0) {
					$objects = $tObject->getRawObjectGroupedByParent($kpi_id);
				} else {
					$objects = $rows;
				}
		} else {
			$objects = array();
			$line_ids = array("0");
		}

		$t_object_name = $tObject->getMyObjectName();
		$t_headings = $headingObject->getMainObjectHeadings($object_type,"COMPACT","NEW");
		$t_headings = $t_headings['rows'];
		$t_parent_name = $tObject->getParentFieldName();
		//ASSIST_HELPER::arrPrint($t_headings);

		/*if($multiple_kpis) {
			$t_okay_headings = array(
									$parentObject->getNameFieldName(),
									$parentObject->getIDFieldName(),
									$parentObject->getTableField()."_ref",
									$parentObject->getTableField()."_unit_id",
									$parentObject->getTableField()."_baseline",
								);
			foreach($t_headings as $t_key => $h_head) {
				if(!in_array($t_key,$t_okay_headings)) {
					unset($t_headings[$t_key]);
				}
			}
		}*/


		if(!$summary) {
			$t_row_headings = $headingObject->getMainObjectHeadings($object_type."_YEAR","FULL","NEW");
		} else {
			$t_row_headings = $headingObject->getMainObjectHeadings($object_type."_YEAR","COMPACT","NEW");
			$t_row_headings = $t_row_headings['rows'];
		}
		$t_object_type = $object_type;

		if($multiple_kpis) {
			unset($t_row_headings['pky_original']);
			unset($t_row_headings['pky_adjustment']);
		}
		//ASSIST_HELPER::arrPrint($t_row_headings);
		//ASSIST_HELPER::arrPrint($t_headings);

		$echo = "";
		$js = "

	$('input:text.CALC').blur(function() {
		var cf = $(this).attr('calc_field');
		var total = 0;
		$('input:text.'+cf).each(function() {
			total+=parseFloat($(this).val());
		});
		$('label.'+cf).text(total);
	});

		";

		//ASSIST_HELPER::arrPrint($t_headings);
$echo.= "
<table id=tbl_".$tier." class=tbl_project_elements>
	<tr>
	";

	if($multiple_kpis) {
		foreach($t_headings as $t_key => $t_head) {
			$echo.= "<th>".$t_head['name']."</th>";
		}
	}

	if(!$summary) {
		$echo.= "<th></th>";
	}
	foreach($idpYears as $iy_id => $iy) {
		$echo.= "<th>".$iy."</th>";
	}
	$echo.= ($add_row || $edit_button ? "
		<th></th>" : "")."
	</tr>";
	//ASSIST_HELPER::arrPrint($line_ids);
	//ASSIST_HELPER::arrPrint($parent_details);
	$all_list_items = array();
	foreach($line_ids as $kpi_id) {
		$total = array();
		$echo.= "
		<tr id=tr_".$tier."_add>
		";
		if($multiple_kpis) {
			//ASSIST_HELPER::arrPrint($t_headings);
			foreach($t_headings as $t_key => $t_head) {
				$echo.= "<td class=b>".$parent_details[$kpi_id][$t_key]."</td>";
			}
		}

		$c=1;
		$k = array_keys($t_row_headings);
		$final_key = $k[count($k)-1];
		foreach($t_row_headings as $rfld => $rhead) {
			if($c>1) {
				$echo.= "</tr><tr ".($rhead['type']=="CALC" ? "class=sub-total" : "").">";
			}
			if(!$summary) {
				$echo.= "<td class='right b'>".$rhead['name'].":</td>";
			}
			foreach($idpYears as $iy_id => $iy) {
				$echo.= "<td class=right>";
					$options = array(
						'id'=>$rfld."_".$iy_id,
						'name'=>$rfld."[".$iy_id."]",
						'warn'=>false,
					);
					if($rhead['type']!="CALC") {
						$options['class'] = "right CALC CALC_".$final_key."_".$iy_id;
						$options['calc_field'] = "CALC_".$final_key."_".$iy_id;
						$val = isset($objects[$kpi_id][$iy_id][$rfld]) ? $objects[$kpi_id][$iy_id][$rfld] : "0";
						if($is_edit_page) {
							$display = $this->createFormField($rhead['type'],$options,$val);
						} else {
							$display = $this->getDataField($rhead['type'],$val);
						}
						$total[$iy_id][$rfld] = ($val*1);
					} else {
						$lt = explode("|",$rhead['list_table']);
						$type = $lt[(count($lt)-1)];
						unset($lt[(count($lt)-1)]);
						unset($lt[0]);
						$val = 0;
						foreach($lt as $l) {
							$val+=isset($objects[$kpi_id][$iy_id][$l]) ? $objects[$kpi_id][$iy_id][$l] : 0;
						}
						$options['class'] = "CALC_".$final_key."_".$iy_id;
						if($is_edit_page) {
							$display = $this->createFormField("LABEL",$options,$val);
						} else {
							$display = $this->getDataField($type, $val);
						}
					}
					$echo.= $display['display'];//.$rhead['list_table'];
					$js.=$display['js'];
					//ASSIST_HELPER::arrPrint($total);
				$echo.= "</td>";
			}
			$c++;
		}
		$echo.= "
		</tr>";
	}

	$echo.= "
</table>";


		return array('display'=>$echo,'js'=>$js);

	}







}
?>