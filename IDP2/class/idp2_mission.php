<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class IDP2_MISSION extends IDP2 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_idp_id";
	protected $secondary_parent_field = "_object_type";
	protected $name_field = "_object";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "MISSION"; 
    const OBJECT_NAME = "mission"; 
     
    const TABLE = "strategy";
    const TABLE_FLD = "strat";
    const REFTAG = "IDP/M";
	
	const LOG_TABLE = "object";
    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->has_secondary_parent = true;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->secondary_parent_field = self::TABLE_FLD.$this->secondary_parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
	//	unset($var['attachments']);
	//	unset($var['action_id']); //remove incorrect field value from add form
	//	unset($var['object_id']); //remove incorrect field value from add form
		/*foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}*/
		
		$mark_final = false;
		if($var[$this->getTableField().'_status']==IDP2::STRATEGY_FINAL) {
			$mark_final = true;
		}
		
		$var[$this->getTableField().'_status']+= IDP2::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			if($mark_final) {
				$this->markMeAsFinal($id);
			}
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'ref'=>$this->getRefTag().$id,
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}


	public function markMeAsFinal($id) {
		if(is_array($id)) { $id = $id['id']; }
		$sql = "SELECT *, ".$this->getIDFieldName()." as id
				FROM ".$this->getTableName()." 
				WHERE (".$this->getStatusFieldName()." & ".IDP2::STRATEGY_FINAL.") = ".IDP2::STRATEGY_FINAL." 
				AND ".$this->getSecondaryParentFieldName()." = '".$this->getMyObjectType()."'";
		$rows = $this->mysql_fetch_all_by_id($sql, "id");
		$already_final = false;
		foreach($rows as $i => $r) {
			if($i==$id) {
				$already_final = true;
			} else {
				$this->markMeAsDraft($i);
			}
		}
		if(!$already_final) {
			$sql = "UPDATE ".$this->getTableName()." 
					SET ".$this->getStatusFieldName()." = (".(IDP2::STRATEGY_FINAL+IDP2::ACTIVE).") 
					WHERE ".$this->getIDFieldName()." = ".$id;
				$this->db_update($sql);
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." marked as final.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::EDIT,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
		}
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully marked as final.",
				'object_id'=>$id,
				'ref'=>$this->getRefTag().$id,
			);
			return $result;
		
	}

	public function markMeAsDraft($id) {
		if(is_array($id)) { $id = $id['id']; }
		$sql = "UPDATE ".$this->getTableName()." 
				SET ".$this->getStatusFieldName()." = (".(IDP2::STRATEGY_DRAFT+IDP2::ACTIVE).") 
				WHERE ".$this->getIDFieldName()." = ".$id;
			$this->db_update($sql);
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." marked as draft.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::EDIT,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
		//return true;
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully marked as draft.",
				'object_id'=>$id,
				'ref'=>$this->getRefTag().$id,
			);
			return $result;
				
	}


	public function editObject($var,$attach=array()) {
		$result = $this->editMyObject($var,$attach);
		return $result;
	}

	public function deleteObject($var) {
		$id = $var['id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = (".$this->getStatusFieldName()." - ".IDP2::ACTIVE.") WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." deleted.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::DELETE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully deleted.",
				'object_id'=>$id,
				'ref'=>$this->getRefTag().$id,
			);
			return $result;
	}

    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}
		
	}
    
    
	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	

	public function getOrderedObjects($parent_id){
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ".$this->getTableField()."_object_type = '".$this->getMyObjectType()."' AND ";
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, ".$this->getTableField()."_comment as comment
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				, IF(((".$this->getStatusFieldName()." & ".IDP2::STRATEGY_FINAL.") = ".IDP2::STRATEGY_FINAL."), 1,0) as is_final
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A")."
				ORDER BY ".$this->getStatusFieldName()." DESC, ".$this->getTableField()."_insertdate ASC
				";
		$res2 = $this->mysql_fetch_all_by_id($sql, "id");
		return $res2;
	}


	public function getMostRecentObject($parent_id,$final_only=false){
		$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." 
					AND ".$this->getTableField()."_object_type = '".$this->getMyObjectType()."' AND ";
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, IF(((".$this->getStatusFieldName()." & ".IDP2::STRATEGY_FINAL.") = ".IDP2::STRATEGY_FINAL."), 1,0) as is_final
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A")."
				".($final_only===true ? " AND ((".$this->getStatusFieldName()." & ".IDP2::STRATEGY_FINAL.") = ".IDP2::STRATEGY_FINAL.")" : "")."
				ORDER BY ".$this->getStatusFieldName()." DESC, ".$this->getTableField()."_insertdate ASC
				";
		$res2 = $this->mysql_fetch_one($sql);
		return $res2;
	}



	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>