<?php
/**
 * To manage the various object headings within the module
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 * 
 * HEADING FIELD TYPES:
 * TEXT = unlimited textarea
 * DATE = datepicker
 * LIST = select - get contents from associated h_table
 * SMLVC = small varchar, 10-20 char e.g. short codes
 * MEDVC = medium varchar, 50-100 char
 * LRGVC = large varchar, up to 255 - display as character limited textareas
 * MASTER = select - get contents from associated MASTER h_table
 * CURRENCY = numeric with R in front, formatted with 2 decimal places
 * NUM = numeric
 * USER = select - get contents from timekeep table
 * BOOL = true/false, display as select with yes/no
 * PERC = numeric with %
 * ATTACH = attachment function
 * HEADING = no field but with sub-headings
 * 
 * --Module specific--
 * TEMPLATE = associated with template table
 * OWNER = associated with admins table which is associated with MASTER directorate table
 * ASSESSMENT = associated with deliverable statuses
 * SUPPLIER = associated with contract_supplier table which is associated with supplier list 
 * 
 */

 
class IDP2_HEADINGS extends IDP2 {
	
	private $list_heading_types = array("LIST","USER","MASTER","OWNER","DEL_TYPE","DELIVERABLE","SEGMENT","MULTILIST","MULTISEGMENT");
	private $text_heading_types = array("TEXT","SMLVC","MEDVC","LRGVC");
	private $num_heading_types = array("NUM","CURRENCY","PERC");
	private $object_heading_types = array("PROJECT","EXPECTEDRESULT");
	
	private $heading_names_for_log = array(
		'name'=>"Name",
		'new'=>"New List Page",
		'manage'=>"Manage List Page",
		'admin'=>"Admin List Page",
		'required'=>"Required"
	);
	
	private $heading_type_names = array(
		'REF'=>"System Reference",
		'MEDVC'=>"Medium Text",
		'LRGVC'=>"Large Text",
		'TEXT'=>"Unlimited Text",
		'BOOL'=>"Yes/No",
		'ATTACH'=>"Attachment",
		'PERC'=>"Percentage",
		'DEL_TYPE'=>"Deliverable Type",
		'CONTRACT_SUPPLIER'=>"Contract Supplier",
	);
    
    const TABLE = "setup_headings"; 
    /**
     * Can a heading be renamed by the client
     */
    const CAN_RENAME = 16;  
    /**
     * Is a heading the name of an object - MENU setting not required for HEADINGS but keeping here to mark the bitwise setting as used
     */
    //const OBJECT_HEADING = 32;
    /**
	 * Can a heading be displayed in the list view?
	 */
	const CAN_LIST_VIEW = 64;
	/**
	 * Display in New, Manage or Admin list pages
	 */
	const DISPLAY_NEW = 128;
	const DISPLAY_MANAGE = 256;
	const DISPLAY_ADMIN = 1024;
    /**
	 * Display in frontpage dashboard
	 */
	const DISPLAY_DASHBOARD = 512;
	/**
	 * Is a normal list field
	 */
	const IS_STANDARD_LIST = 2048;
	/**
	 * System managed field? i.e. Do not display in Add/Edit form (Used to disable Date Completed field which is updated automatically by system on update where status_id = 3)
	 */
	const SYSTEM_MANAGED = 4096;
	/**
	 * Display in compact/summary view
	 */
	const COMPACT_VIEW = 8192;
	
	
	/********************************
	 * CONSTRUCTOR
	 */
    public function __construct() {
        parent::__construct();
    }
    
    
	
	
	/****************
	 * CONTROLLER functions
	 */
	public function editObject($var) {
		//$h_section = $var['section'];
		unset($var['section']);
		$old_data = $this->getRenameHeadings("",array_keys($var['ref']));
		$result = 0;
		foreach($var['ref'] as $field => $id) {
			$c = array('response'=>$old_data[$field]['mdefault']." updated:");
			$client = $var['client'][$field];
			if($client!=$old_data[$field]['client']){
				$data = array(
					'h_client'=>$client,
				);
				$c['name']=array('to'=>$client,'from'=>$old_data[$field]['client']);
			}
			if(isset($var['list_new'][$field])){
				$list_new = $var['list_new'][$field];
				$list_manage = $var['list_manage'][$field];
				$list_admin = $var['list_admin'][$field];
				$list_display = ($old_data[$field]['list'] & self::DISPLAY_DASHBOARD) + ($list_new == 1 ? self::DISPLAY_NEW : 0) + ($list_manage == 1 ? self::DISPLAY_MANAGE : 0) + ($list_admin == 1 ? self::DISPLAY_ADMIN : 0);
				if($list_display!=$old_data[$field]['list']){
					$data['h_list_display']=$list_display;
				}
				$c['new']=array('to'=>($list_new==1?"Yes":"No"),'from'=>($old_data[$field]['list_new']==1?"Yes":"No"));
				$c['manage']=array('to'=>($list_manage==1?"Yes":"No"),'from'=>($old_data[$field]['list_manage']==1?"Yes":"No"));
				$c['admin']=array('to'=>($list_admin==1?"Yes":"No"),'from'=>($old_data[$field]['list_admin']==1?"Yes":"No"));
				
			}
			if(isset($var['required'][$field])) {
				$required = $var['required'][$field];
				if($required!=$old_data[$field]['required']){
					$data['h_client_required']=$required;
				}
				$c['required']=array('to'=>($required==1?"Yes":"No"),'from'=>($old_data[$field]['required']==1?"Yes":"No"));
			}
			
			if(count($data)>0){
				$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($data)." WHERE h_id = ".$id." AND h_field = '$field'";
				$mar = $this->db_update($sql);
				if($mar>0) {
					$result+=$mar;
					$changes = array_merge(array('user'=>$this->getUserName()),$c);
					$log_var = array(
						'section'	=> "HEAD",
						'object_id'	=> $old_data[$field]['id'],
						'changes'	=> $changes,
						'log_type'	=> IDP2_LOG::EDIT,		
					);
					$this->addActivityLog("setup", $log_var);
				}
			}
		}
		if($result>0) {
			return array("ok","Changes saved successfully.");
		}
		return array("info","No change found to be saved.");
	} 
	
	
	
	/****************************
	 * CHECK functions
	 */
	public function isListField($type) {
		return in_array($type,$this->list_heading_types);
	} 
	public function isTextField($type) {
		return in_array($type,$this->text_heading_types);
	} 
	public function isNumField($type) {
		return in_array($type,$this->num_heading_types);
	} 
	 	 
	
    /***
     * GET Functions
     */
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; } 
     
    /**
     * Get the custom heading for a specific drop down list associated with a _list_table
     * @param (String) tbl = Table name to search for in the headings table
     * @return (String) 
     */ 
    public function getAListHeading($tbl) {
        $sql = "SELECT IF(LENGTH(h_client)>0,h_client,h_default) as name FROM ".$this->getTableName()." WHERE h_table = '".$tbl."'";
        return $this->mysql_fetch_one_value($sql, "name");
        //return "MAKE THE TABLE!!";
    }
     
	/**
	 * Get the headings for all list fields that can be edited in Setup
	 * 
	 * @return (Array) rows[h_table] = name
	 */ 
	public function getStandardListHeadings() {
        $sql = "SELECT h_table as id
        		, IF(LENGTH(h_client)>0,h_client,h_default) as name
        		, h_status 
        		FROM ".$this->getTableName()." 
        		WHERE h_type IN ('LIST','MULTILIST') 
        		AND (h_status & ".IDP2::ACTIVE." = ".IDP2::ACTIVE.") 
        		AND (h_status & ".self::IS_STANDARD_LIST." = ".self::IS_STANDARD_LIST.")
        		ORDER BY h_table";
		//echo $sql;
		$data = $this->mysql_fetch_value_by_id($sql, "id", "name");
		//ASSIST_HELPER::arrPrint($data);
		//unset($data['deliverable_type']); 
		//unset($data['deliverable_quality_weight']); 
		//unset($data['deliverable_quantity_weight']); 
		//unset($data['deliverable_other_weight']);
		$data = $this->replaceAllNames($data); 
        return $data;
	}
	/**
	 * Get the headings for all list fields 
	 * 
	 * @return (Array) rows[h_table] = name
	 */ 
	public function getAllListHeadings() {
        $sql = "SELECT h_table as id, IF(LENGTH(h_client)>0,h_client,h_default) as name 
        		FROM ".$this->getTableName()." WHERE h_type = 'LIST' 
        		AND (h_status & ".self::IS_STANDARD_LIST." = ".self::IS_STANDARD_LIST.")";
		$data = $this->mysql_fetch_value_by_id($sql, "id", "name");
		unset($data['deliverable_type']); 
        return $data;
	}
	
	/**
	 * Get a single heading based on field
	 * 
	 * @param (String) field = the field name of the heading to be found.
	 */
	public function getAHeadingNameByField($field){
		$sql = "SELECT IF(LENGTH(h_client)>0,h_client,h_default) as name, h_field as fld FROM ".$this->getTableName()." WHERE h_field ";
		if(is_array($field)) {
			$sql.= " IN ('".implode("','",$field)."')";
			return $this->mysql_fetch_value_by_ID($sql, "fld","name");
		} else {
			$sql.= " = '".$field."'";
			return $this->mysql_fetch_one_value($sql, "name");
		}
	}
	 
	 
	 
	public function getMainObjectHeadingsByType($object="",$specific_type=array(),$replace_names=false) {
		return $this->getMainObjectHeadings($object,"","","",$replace_names,array(),$specific_type);
	}
    /**
     * Get all the headings used by a particular object for a specific use
     * @param (String) object name = contract, deliverable, action
     * @param (String) fn = LIST, VIEW (DETAILS), FORM 
     * @param (String) section = NEW / MANAGE / ADMIN 
     */ 
    public function getMainObjectHeadings($object="",$fn="DETAILS",$section="",$fld_prefix="",$replace_names=false,$specific_fields = array(),$specific_type=array()) {
    	//echo "<P>".$object.":".$fn.":",$section."<p>";
		$sql = "SELECT h_id as id 
				, h_field as field
				, IF(LENGTH(h_client)>0,h_client,h_default) as name
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF(h_assist_required>0,1,h_client_required) as required
				, h_system_glossary as help 
				FROM ".$this->getTableName()." 
				WHERE (h_status & ".IDP2::ACTIVE.") = ".IDP2::ACTIVE."
				";
				if(count($specific_fields)>0) {
					$order_by = "h_details_order";
					$sql.=" AND h_field IN ('".implode("','",$specific_fields)."') ";
				} elseif(count($specific_type)>0) {
					$order_by = "h_details_order";
					$sql.=" AND h_type IN ('".implode("','",$specific_type)."') ";
					if(strlen($object)>0) {
						$sql.=" AND h_section = '".strtoupper($object)."' ";
					}
				} else {
					$sql.="
							".($fn=="FORM" ? "AND h_section NOT LIKE '".strtoupper($object."_UPDATE")."%' AND ( (h_status & ".self::SYSTEM_MANAGED.") <> ".self::SYSTEM_MANAGED.")" : "")." 
							".($fn=="LIST" || $fn=="FULL" ? "AND ( (h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW.")" : "")." 
							".($fn=="COMPACT" ? "AND ( (h_status & ".self::COMPACT_VIEW.") = ".self::COMPACT_VIEW.")" : "")." 
							AND h_section LIKE '".($section=="NEW"?strtoupper($object):strtoupper($object)."%")."' ";
					if($fn=="LIST" || $fn == "FULL" || $fn == "COMPACT") {
						$order_by = "h_list_order";
						if($fn=="LIST"){
							switch($section) {
								case "NEW":
									$sql.= " AND ( (h_list_display & ".self::DISPLAY_NEW.") = ".self::DISPLAY_NEW.") ";
									break;
								case "MANAGE":
									$sql.= " AND ( (h_list_display & ".self::DISPLAY_MANAGE.") = ".self::DISPLAY_MANAGE.") ";
									break;
								case "ADMIN":
									$sql.= " AND ( (h_list_display & ".self::DISPLAY_ADMIN.") = ".self::DISPLAY_ADMIN.") ";
									break;
								case "DASHBOARD":
									$sql.= " AND ( (h_list_display & ".self::DISPLAY_DASHBOARD.") = ".self::DISPLAY_DASHBOARD.") ";
									break;
								default:
									$sql.= " AND ( h_list_display > 0) ";
									break;
							}
						}
					} else {
						$order_by = "h_details_order";
						if($fn!="FORM") {
								$sql.= " AND ( (h_section NOT LIKE '%".strtoupper($object."_UPDATE")."%') OR ( h_section LIKE '%".strtoupper($object."_UPDATE")."%' AND (h_list_display > 0 && (h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW."  ) ) ) ";
						}
					}
				}
		$sql.= " ORDER BY ".$order_by;  //echo $sql;
		$rows = $this->mysql_fetch_all_by_id($sql, ($fn=="REPORT" ? "id" :"field"));
		if($replace_names===true) {
			$rows = $this->replaceObjectNames($rows);
		}
		if($object=="ASSURANCE") {
			$original = $rows;
			$rows = array();
			foreach($original as $fld=>$r){
				$r['field'] = $fld_prefix.$r['field'];
				$rows[$fld_prefix.$fld] = $r;
			}
		}
		if($fn=="LIST" || $fn=="FULL" || $fn == "REPORT" ) {
			return $rows;
		} else {
			$data = array('rows'=>$rows,'sub'=>array());
			foreach($rows as $fld=>$r){
				if($r['parent_id']>0) {
					$data['sub'][$r['parent_id']][] = $r;
					unset($data['rows'][$r['field']]);
				}
			}
			return $data;
		}
    } 



    public function getUpdateObjectHeadings($object,$fn="DETAILS",$section="") {
		$sql = "SELECT h_id as id 
				, h_field as field
				, IF(LENGTH(h_client)>0,h_client,h_default) as name
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF(h_assist_required>0,1,h_client_required) as required 
				, h_system_glossary as help
				FROM ".$this->getTableName()." 
				WHERE (h_status & ".IDP2::ACTIVE.") = ".IDP2::ACTIVE."
				AND h_section LIKE '%".strtoupper($object."_UPDATE")."%' ";
		if($fn=="LIST") {
			$order_by = "h_list_order";
			switch($section) {
				case "NEW":
					$sql.= " AND ( (h_list_display & ".self::DISPLAY_NEW.") = ".self::DISPLAY_NEW.") ";
					break;
				case "MANAGE":
					$sql.= " AND ( (h_list_display & ".self::DISPLAY_MANAGE.") = ".self::DISPLAY_MANAGE.") ";
					break;
				case "ADMIN":
					$sql.= " AND ( (h_list_display & ".self::DISPLAY_ADMIN.") = ".self::DISPLAY_ADMIN.") ";
					break;
				default:
					$sql.= " AND ( h_list_display > 0) ";
					break;
			}
		} else {
			$order_by = "h_details_order";
		}
		$sql.= " ORDER BY ".$order_by; 
		$rows = $this->mysql_fetch_all_by_id($sql, "field");
		if($fn=="LIST") {
			return $rows;
		} else {
			$data = array('rows'=>$rows,'sub'=>array());
			foreach($rows as $fld=>$r){
				if($r['parent_id']>0) {
					$data['sub'][$r['parent_id']][] = $r;
				}
			}
			return $data;
		}

    } 
     
     
	public function getHeadingsForLog() {
		$sql = "SELECT C.h_id as id 
				, C.h_field as field
				, IF(LENGTH(C.h_client)>0,C.h_client,C.h_default) as head_name
				, C.h_section as section
				, C.h_type as type
				, C.h_table as list_table
				, C.h_client_length as max
				, C.h_parent_id as parent_id
				, P.h_field as parent_field
				, IF(LENGTH(P.h_client)>0,P.h_client,P.h_default) as parent_name
				, IF(P.h_field IS NULL
					, IF(LENGTH(C.h_client)>0,C.h_client,C.h_default)
					, CONCAT(IF(LENGTH(P.h_client)>0,P.h_client,P.h_default),': ',IF(LENGTH(C.h_client)>0,C.h_client,C.h_default))
				  ) as name				
				, C.h_parent_link as parent_link
				, C.h_default_value as default_value
				, IF(C.h_assist_required>0,1,C.h_client_required) as required 
				FROM ".$this->getTableName()." C
				LEFT OUTER JOIN ".$this->getTableName()." P
				  ON P.h_id = C.h_parent_id
				WHERE (C.h_status & ".IDP2::ACTIVE.") = ".IDP2::ACTIVE."
				ORDER BY C.h_field";
		return $this->mysql_fetch_all_by_id($sql, "field");
	}
	
	public function getReportObjectHeadings($object_type) {
		$headings = $this->replaceAllNames($this->getMainObjectHeadings($object_type,"REPORT"));
		if($object_type=="CONTRACT") {
			$listObject = new IDP2_LIST("deliverable_status");
			$items = $listObject->getAllListItems();
			$headings['deliverable_status'] = $items;
		}
		return $headings;
	}
	
	
	
	/**
	 * Get Headings for renaming in Setup
	 */
	public function getRenameHeadings($hs="",$fields=array()) {
		$sql = "SELECT h_id as id 
				, h_field as field
				, h_client as client
				, h_default as mdefault
				, h_list_display as list
				, h_assist_required as assist_required
				, h_client_required as required
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF( ((h_list_display & ".self::DISPLAY_NEW.") = ".self::DISPLAY_NEW."),1,0) as list_new 
				, IF( ((h_list_display & ".self::DISPLAY_MANAGE.") = ".self::DISPLAY_MANAGE."),1,0) as list_manage 
				, IF( ((h_list_display & ".self::DISPLAY_ADMIN.") = ".self::DISPLAY_ADMIN."),1,0) as list_admin
				, h_status as status
				, IF( ((h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW."),1,0) as can_list
				FROM ".$this->getTableName()." 
				WHERE (h_status & ".IDP2::ACTIVE.") = ".IDP2::ACTIVE."
				  AND (h_status & ".self::CAN_RENAME.") = ".self::CAN_RENAME."
				  ".(strlen($hs)>0 ? "AND h_section LIKE '".$hs."%' AND h_section NOT LIKE '".$hs."_UPDATE%'" : "")."
				  ".(count($fields)>0 ? " AND h_field IN ('".implode("','",$fields)."') " : "")."
				ORDER BY h_details_order";
		return $this->mysql_fetch_all_by_id($sql, "field");
	}
	/**
	 * Get Headings for renaming in Setup
	 */
	public function getAllHeadingsForRenaming($hs="",$fields=array()) {
		$sql = "SELECT h_id as id 
				, h_field as field
				, h_client as client
				, h_default as mdefault
				, h_list_display as list
				, h_assist_required as assist_required
				, h_client_required as required
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF( ((h_list_display & ".self::DISPLAY_NEW.") = ".self::DISPLAY_NEW."),1,0) as list_new 
				, IF( ((h_list_display & ".self::DISPLAY_MANAGE.") = ".self::DISPLAY_MANAGE."),1,0) as list_manage 
				, IF( ((h_list_display & ".self::DISPLAY_ADMIN.") = ".self::DISPLAY_ADMIN."),1,0) as list_admin
				, h_status as status
				, IF( ((h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW."),1,0) as can_list
				FROM ".$this->getTableName()." 
				WHERE (h_status & ".IDP2::ACTIVE.") = ".IDP2::ACTIVE."
				  ".(strlen($hs)>0 ? "AND h_section LIKE '".$hs."%' AND h_section NOT LIKE '".$hs."_UPDATE%'" : "")."
				  ".(count($fields)>0 ? " AND h_field IN ('".implode("','",$fields)."') " : "")."
				ORDER BY h_details_order";
		return $this->mysql_fetch_all_by_id($sql, "field");
	}
	
	/**
	 * Get Headings for ordering list columns in Setup
	 */
	public function getListColumnHeadings($hs="",$fields=array()) {
		$sql = "SELECT h_id as id 
				, h_field as field
				, h_client as client
				, h_default as mdefault
				, h_list_display as list
				, h_assist_required as assist_required
				, h_client_required as required
				, h_section as section
				, h_type as type
				, h_table as list_table
				, h_client_length as max
				, h_parent_id as parent_id
				, h_parent_link as parent_link
				, h_default_value as default_value
				, IF( ((h_list_display & ".self::DISPLAY_NEW.") = ".self::DISPLAY_NEW."),1,0) as list_new 
				, IF( ((h_list_display & ".self::DISPLAY_MANAGE.") = ".self::DISPLAY_MANAGE."),1,0) as list_manage 
				, IF( ((h_list_display & ".self::DISPLAY_ADMIN.") = ".self::DISPLAY_ADMIN."),1,0) as list_admin
				, h_status as status
				, IF( ((h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW."),1,0) as can_list
				FROM ".$this->getTableName()." 
				WHERE (h_status & ".IDP2::ACTIVE.") = ".IDP2::ACTIVE."
				  AND (h_status & ".self::CAN_LIST_VIEW.") = ".self::CAN_LIST_VIEW."
				  AND h_list_display > 0
				  ".(strlen($hs)>0 ? "AND h_section LIKE '".$hs."%'" : "")."
				  ".(count($fields)>0 ? " AND h_field IN ('".implode("','",$fields)."') " : "")."
				ORDER BY h_list_order"; //echo "<P>".$sql;
		return $this->mysql_fetch_all_by_id($sql, "field");
	}
	
	
	/**
	 * Save list column changes
	 */
	public function saveListColumns($var) {
		$sections = $var['section'];
		$mar = 0;
		foreach($sections as $sec) {
			$m = 0;
			$order = $var[strtolower($sec)];
			foreach($order as $i => $id) {
				$sql = "UPDATE ".$this->getTableName()." SET h_list_order = ".($i+2)." WHERE h_id = '".$id."'";
				$m+=$this->db_update($sql);
			}
			if($m>0) {
				$s = explode("_",$sec);
				$sec = $s[0];
				$changes = array(
					'response'=>"|".strtolower($sec)."| list column order updated.",
					'user'=>$this->getUserName(),
				);
				$log_var = array(
					'section'	=> "COL",
					'object_id'	=> $this->getObjectNameMenuID($sec),
					'changes'	=> $changes,
					'log_type'	=> IDP2_LOG::EDIT,		
				);
				$this->addActivityLog("setup", $log_var);
			}
			$mar+=$m;
		}
		if($mar>0) {
			return array("ok","Changes to list columns saved successfully.");
		} else {
			return array("info","No list column changes found to be saved.");
		}
	}
	
		
	/**
	 * Get heading types to be displayed for the client to understand
	 */
	public function getHeadingTypeFormattedForClient($type) {
		if($this->isListField($type)) {
			return "List";
		} elseif(isset($this->heading_type_names[$type])) {
			return $this->heading_type_names[$type];
		} else {
			return ucwords(strtolower($type));
		}
	}
	
	/**
	 * Get heading names for display in log
	 */
	public function getDefaultHeadingNamesForLog() {
		return $this->heading_names_for_log;
	}
     
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
    
}

?>