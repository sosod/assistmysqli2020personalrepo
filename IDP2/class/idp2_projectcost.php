<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 * 
 * COST
 * pc_id
 * pc_project_id
 * pc_account_id
 * pc_costtype_id
 * pc_status
 * pc_insertdate
 * pc_insertuser
 * 
 * 
 * COST_YEAR
 * pcy_id			AUTO ID
 * pcy_pi_id		INCOME ID
 * pcy_iy_id		IDP_YEAR ID
 * pcy_original		CURRENCY Original budget (imported from prior year)	- only allowed if first IDP
 * pcy_adjustment	CURRENCY Adjustments (changes to original budget)	- only allowed if not first IDP
 * pcy_total		CURRENCY Total
 * pcy_status		AUTO status
 * pcy_insertdate	AUTO timestamp
 * pcy_insertuser	AUTO user
 * 
 */
 
class IDP2_PROJECTCOST extends IDP2 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_project_id";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "IDP/PC";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "PROJECTCOST"; 
    const OBJECT_NAME = "cost"; 
     
    const TABLE = "project_cost";
    const TABLE_FLD = "pc";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "object";
    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
	//	unset($var['attachments']);
	//	unset($var['action_id']); //remove incorrect field value from add form
	//	unset($var['object_id']); //remove incorrect field value from add form
	/*	foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}*/
		
		$original = $var['pcy_original'];
		unset($var['pcy_original']);
		$adjustment = $var['pcy_adjustment'];
		unset($var['pcy_adjustment']);
		unset($var['pcy_total']);
		
		$var[$this->getTableField().'_status'] = IDP2::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$yearObject = new IDP2_PROJECTCOSTYEARS();
			$yearObject->addYears($id, $original, $adjustment);
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}



	public function editObject($var,$attach=array()) {
		unset($var['pcy_total']);
		$result = $this->editMyObject($var,$attach);
		return $result;
	}



	public function deactivateObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deactivated.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::DEACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deactivated.",
				'object_id'=>$id,
			);
			return $result;
	}


	public function deleteObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::DELETED." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deleted.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::DELETE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deleted.",
				'object_id'=>$id,
			);
			return $result;
	}


    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}
		
	}
    
    
	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	

	public function getOrderedObjects($parent_parent_id=0,$parent_id=0){
		if($parent_id==0) {
			$parentObject = new IDP2_PARENT();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
		} else {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		return $res2;
	}



	/*public function getChildObjectsForTable($project_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getParentFieldName()." = $project_id AND (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		$keys = array_keys($rows);
		$yearObject = new IDP2_PROJECTCOSTYEARS();
		$budget = $yearObject->getRawObjectsByPI($keys);
		foreach($keys as $key) {
			$rows[$key]['years'] = isset($budget[$key]) ? $budget[$key] : array();
		}
		return $rows;
	}*/
	public function getChildObjectsForTable($project_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." 
		WHERE 
		".(is_array($project_id) ? $this->getParentFieldName()." IN (".implode(",",$project_id).") " : $this->getParentFieldName()." = $project_id ")
		." AND (".$this->getStatusFieldName()." & ".self::ACTIVE.") = ".self::ACTIVE;
		$rows = $this->mysql_fetch_all_by_id($sql, $this->getIDFieldName());
		if(count($rows)>0) {
			$keys = array_keys($rows);
			$yearObject = new IDP2_PROJECTCOSTYEARS();
			$budget = $yearObject->getRawObjectsByPI($keys);
			foreach($keys as $key) {
				$rows[$key]['years'] = isset($budget[$key]) ? $budget[$key] : array();
			}
			if(is_array($project_id)) {
				$rows2 = $rows;
				$rows = array();
				foreach($rows2 as $r_id => $r) {
					$rows[$r[$this->getParentFieldName()]][$r_id] = $r;
				}
			}
		}
		return $rows;
	}
	


	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>