<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class IDP2_STRATRESULT extends IDP2 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_parent_sg_id";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "IDP/SOI";
	
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "STRATRESULT"; 
    const OBJECT_NAME = "expectedresult"; 
	const PARENT_OBJECT_TYPE = "STRATGOAL";
     
    const TABLE = "strategic_expresult";
    const TABLE_FLD = "ser";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "object";
	
    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
	//	unset($var['attachments']);
		unset($var['object_id']); //remove incorrect field value from add form
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		$var[$this->getTableField().'_status'] = IDP2::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}




	public function editObject($var,$attach=array()) {
		$result = $this->editMyObject($var,$attach);
		return $result;
	}


	public function deactivateObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deactivated.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::DEACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deactivated.",
				'object_id'=>$id,
			);
			return $result;
	}



    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyParentObjectType() { return self::PARENT_OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}
		
	}
    
    
	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		//return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
		return $this->getRawObject($id);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	

	/**
	 * Get list of active objects limited to specific IDP ready to populate a SELECT element 
	 */
	public function getLimitedActiveObjectsFormattedForSelect($options=array()) { //$this->arrPrint($options);
		$rows = $this->getOrderedObjects($options['IDP']);
		//$items = $this->mysql_fetch_value_by_id($sql, "id", "name");
		//$this->arrPrint($items);
		$items = array();
		foreach($rows as $i => $r) {
			foreach($r as $i2 => $r2) {
				$items[$r2['id']] = $r2['name'];
			}
		}
		asort($items);
		return $items;
		//return $items;
	}
	
//parent4 = IDP; parent3 = stratobj; parent2 = stratfocus; parent = stratgoal
	public function getOrderedObjects($parent4=0,$parent3=0,$parent_parent_id=0,$parent_id=0){
		$sql2 = $this->getOrderedObjectSQL($parent4,$parent3,$parent_parent_id,$parent_id);
		$sql_from = $sql2['from'];
		$sql_status = $sql2['status'];
		if($sql2['err']===true) {
			$res2 = array();
		} else {
			$sql = "SELECT ".$this->getIDFieldName()." as id
					, CONCAT(".$this->getNameFieldName().",' (',".$this->getTableField()."_ref,')') as name
					, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
					, ".$this->getParentFieldName()." as parent_id
					FROM ".$this->getTableName()." A
					".$sql_from."  ".$sql_status."
					ORDER BY ".$this->getNameFieldName();
			$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		}
		return $res2;
	}

	public function getRawOrderedObjects($parent4=0,$parent3=0,$parent_parent_id=0,$parent_id=0){
		$sql2 = $this->getOrderedObjectSQL($parent4,$parent3,$parent_parent_id,$parent_id);
		$sql_from = $sql2['from'];
		$sql_status = $sql2['status'];
		if($sql2['err']===true) {
			$res2 = array();
		} else {
			$sql = "SELECT A.*
					, ".$this->getIDFieldName()." as id
					, ".$this->getParentFieldName()." as parent_id
					FROM ".$this->getTableName()." A
					".$sql_from."  ".$sql_status."
					ORDER BY ".$this->getNameFieldName();
			$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		}
		return $res2;
	}


	private function getOrderedObjectSQL($parent4=0,$parent3=0,$parent_parent_id=0,$parent_id=0){
		$sql_status = $this->getActiveStatusSQL("A");
		$err = false;
		//If PARENT_ID = immediate sent then find all records with that ID using $this->parent field
		if(is_array($parent_id)) {
			$p2 = $parent_id;
			$parent_id = array();
			foreach($p2 as $p) {
				if($this->checkIntRef($p)) {
					$parent_id[] = $p;
				}
			}
			if(count($parent_id)>0) {
				$sql_from = "WHERE ".$this->getParentFieldName()." IN (".implode(",",$parent_id).") AND ";
			} else {
				$err = true;
			}
		} elseif($this->checkIntRef($parent_id)) {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		} else {
			//Else join up with immediate parent
			$parentObject = new IDP2_STRATGOAL();
			$sql_status.=" AND ".$parentObject->getActiveStatusSQL("B");
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B 
						  ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." ";
			if(is_array($parent_parent_id)) {
				$p2 = $parent_parent_id;
				$parent_parent_id = array();
				foreach($p2 as $p) {
					if($this->checkIntRef($p)) {
						$parent_parent_id[] = $p;
					}
				}
				if(count($parent_parent_id)>0) {
					$sql_from.=" AND B.".$parentObject->getParentFieldName()." IN (".implode(",",$parent_parent_id).") WHERE ";
				} else {
					$err = true;
				}
			} elseif($this->checkIntRef($parent_parent_id)) {
				$sql_from.=" AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
			} else {
				$parent2Object = new IDP2_STRATFOCUS();
				$sql_status.=" AND ".$parent2Object->getActiveStatusSQL("C");
				$sql_from.= "INNER JOIN ".$parent2Object->getTableName()." C 
							  ON B.".$parentObject->getParentFieldName()." = C.".$parent2Object->getIDFieldName()." ";
				if(is_array($parent3)) {
					$p2 = $parent3;
					$parent3 = array();
					foreach($p2 as $p) {
						if($this->checkIntRef($p)) {
							$parent3[] = $p;
						}
					}
					if(count($parent3)>0) {
						$sql_from.=" AND C.".$parent2Object->getParentFieldName()." IN (".implode(",",$parent3).") WHERE ";
					} else {
						$err = true;
					}
				} elseif($this->checkIntRef($parent3)) {
					$sql_from.=" AND C.".$parent2Object->getParentFieldName()." = ".$parent3." WHERE ";
				} else {
					$parent3Object = new IDP2_STRATOBJ();
					$sql_status.=" AND ".$parent3Object->getActiveStatusSQL("D");
					$sql_from.= "INNER JOIN ".$parent3Object->getTableName()." D 
								  ON C.".$parent2Object->getParentFieldName()." = D.".$parent3Object->getIDFieldName()." ";
					if(is_array($parent4)) {
						$p2 = $parent4;
						$parent4 = array();
						foreach($p2 as $p) {
							if($this->checkIntRef($p)) {
								$parent4[] = $p;
							}
						}
						if(count($parent4)>0) {
							$sql_from.=" AND D.".$parent3Object->getParentFieldName()." IN (".implode(",",$parent4).") WHERE ";
						} else {
							$err = true;
						}
					} elseif($this->checkIntRef($parent4)) {
						$sql_from.=" AND D.".$parent3Object->getParentFieldName()." = ".$parent4." WHERE ";
					} else {
						$err = true;
					}
				}
			}
		}
		
		return array('err'=>$err,'from'=>$sql_from,'status'=>$sql_status);
	}



	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>