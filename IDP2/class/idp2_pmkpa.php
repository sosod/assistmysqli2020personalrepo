<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class IDP2_PMKPA extends IDP2 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_idp_id";
	protected $name_field = "_name";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "IDP/KPA";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "PMKPA"; 
    const OBJECT_NAME = "kpa"; 
	const PARENT_OBJECT_TYPE = "IDP";
     
    const TABLE = "pm_kpa";
    const TABLE_FLD = "kpa";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "object";
    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
	//	unset($var['attachments']);
	//	unset($var['action_id']); //remove incorrect field value from add form
		unset($var['object_id']); //remove incorrect field value from add form
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		$var[$this->getTableField().'_status'] = IDP2::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag().$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}




	public function editObject($var,$attach=array()) {
		$result = $this->editMyObject($var,$attach);
		return $result;
	}



	public function deactivateObject($var) {
		$id = $var['object_id'];
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);
		
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deactivated.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP2_LOG::DEACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deactivated.",
				'object_id'=>$id,
			);
			return $result;
	}


    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}
		
	}
    
    
	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	
	public function getOrderedObjects($parent_id=0,$group_by_parent=true){
		/*if($parent_id==0) {
			$parentObject = new IDP2_PARENT();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
		} else {*/
		//}
		$err = false;
		if(is_array($parent_id)) {
			$parent_id = ASSIST_HELPER::removeBlanksFromArray($parent_id);
			if(count($parent_id)>0) {
				$sql_from = "WHERE ".$this->getParentFieldName()." IN (".implode(",",$parent_id)." AND ";
			} else {
				$err = true;
			}
		} elseif($this->checkIntRef($parent_id)) {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		} else {
			$err = true;
		}
		if($err) {
			$res2 = array();
		} else {
			$sql = "SELECT ".$this->getIDFieldName()." as id
					, ".$this->getNameFieldName()." as name
					, ".$this->getTableField()."_ref as ref
					, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
					, ".$this->getParentFieldName()." as parent_id
					FROM ".$this->getTableName()." A
					".$sql_from."  ".$this->getActiveStatusSQL("A");
			if($group_by_parent) {
				$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
			} else {
				$res2 = $this->mysql_fetch_all_by_id($sql, "id");
			}
		}
		return $res2;
	}

/**
 * Function to get details for viewing / confirmation
 */
	public function getOrderedObjectsWithChildren($parent_id=0){
		/*$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, ".$this->getTableField()."_ref as ref
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				WHERE ".$this->getParentFieldName()." = ".$parent_id." AND 
				".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id($sql, "id");*/
		$res = $this->getOrderedObjects($parent_id,false);
		$data = array(
			$this->getMyObjectType()=>$res,
		);
		
		$goalObject = new IDP2_PMPROG();
		$goal_objects = $goalObject->getOrderedObjects($parent_id);
		$data[$goalObject->getMyObjectType()] = $goal_objects;
		
		$resultsObject = new IDP2_PMKPI();
		$results_objects = $resultsObject->getRawOrderedObjects($parent_id);
		$data[$resultsObject->getMyObjectType()] = $results_objects;
		
		/*$resultsObject = new IDP2_PMKPIYEARS();
		$results_objects = $resultsObject->getRawOrderedObjects($parent_id);
		$data[$resultsObject->getMyObjectType()] = $results_objects;
		*/
		
		return $data;
	}


	
	public function getSimpleDetails($id=0) {
		$obj = $this->getRawObject($id);
		$data = array(
			'parent_id'=>$obj[$this->getParentFieldName()],
			'id'=>$id,
			'name' => $obj[$this->getNameFieldName()].(strlen($obj[$this->getTableField()."_ref"])>0 ? " (".$obj[$this->getTableField()."_ref"].")" : ""),
			'reftag' => $this->getRefTag().$obj[$this->getIDFieldName()],
		);
		return $data;
	}
	

	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>