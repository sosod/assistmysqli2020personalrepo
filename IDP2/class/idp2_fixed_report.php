<?php
/**
 * To manage the Fixed reports on IDP2
 * 
 * Created on: 5 May 2017
 * Authors: Janet Currie
 * 
 */
 
class IDP2_FIXED_REPORT extends IDP2 {

	private $reports = array();


	public function __construct() {
		parent::__construct();
		$kpi_name = $this->getObjectName("KPI");
		$project_name = $this->getObjectName("PROJECT");
		$kpa_name = $this->getObjectName("KPA");
		$this->reports = array(
			'kpa'=>array(
				'ref'=>"kpa",
				'name'=>$kpi_name." & ".$project_name." Per ".$kpa_name,
			),
		);
			/*'mscoa_projects'=>array(
				'ref'=>"mscoa_projects",
				'name'=>"IDP mSCOA report: Projects, Wards & Functions"
			),
			'projects_wards'=>array(
				'ref'=>"projects_wards",
				'name'=>"Projects per Ward"
			),
			'projects_functions'=>array(
				'ref'=>"projects_functions",
				'name'=>"Projects per Function"
			),
			'projects_dir'=>array(
				'ref'=>"projects_dir",
				'name'=>"Projects per Sub-Directorate"
			),
			'pmkpi_functions'=>array(
				'ref'=>"pmkpi_functions",
				'name'=>"Performance Measures per Function"
			),
			'pmkpi_dir'=>array(
				'ref'=>"pmkpi_dir",
				'name'=>"Performance Measures per Sub-Directorate"
			),
		);*/
		
	}


	public function getReportDetails($ref="") {
		if(strlen($ref)==0) {
			return $this->reports;
		} else {
			return $this->reports[$ref];
		}
	}


	public function getReport($ref,$idp_id) {
		$data = array();
		switch($ref) {
			case "kpa":
				$data = $this->getKPAReport($idp_id);
				break;
			case "admin_fin_indicator_push":
				$data = $this->getAFIndicatorPushReport($idp_id);
				break;
			default:
				$data = array("error","unknown report code");
				break;
		}
		return $data;
	}



/** 
 * FIXED REPORT FUNCTIONS
 */
	private function getKPAReport($idp_id) {
	//Prepare blank return array
		$data = array(
			'group'=>array(),
			'head'=>array(),
			'rows'=>array(),
			'count'=>array(),
		);
	//Get IDP data
		$idpObject = new IDP2_IDPYEARS();
		$idp_details = $idpObject->getYearsForSpecificIDP($idp_id);
	//Get grouping data - PMKPA	
		$kpaObject = new IDP2_PMKPA();
		$kpa_details = $kpaObject->getOrderedObjectsWithChildren($idp_id);
		foreach($kpa_details['PMKPA'] as $kpa_id => $kpa) {
			$data['group'][$kpa_id] = $kpa['name'].(isset($kpa['ref']) && strlen($kpa['ref'])>0 ? " (".$kpa['ref'].")" : "");
		}
		//Get list of KPI IDs for getting of targets
		$kpi_ids = array();
		foreach($kpa_details['PMKPI'] as $prog_id => $prog_kpis) {
			foreach($prog_kpis as $ki => $k) {
				if(!in_array($ki,$kpi_ids)) {
					$kpi_ids[] = $ki;
				}
			}
		}
	//Get heading data
		$headObject = new IDP2_HEADINGS();
		$head1 = $headObject->getMainObjectHeadings("PMKPI"); 
		$head2 = $headObject->getMainObjectHeadings("PROJECT");
		//Set 0 = true to indicate more than 1 row of headings
		$head_final = array(0=>true);
		$head_final[1]['STRATOBJ'] = $this->getObjectName("OBJECTIVE");
		$head_final[1]['STRATFOCUS'] = $this->getObjectName("FOCUSAREA");
		$head_final[1]['STRATGOAL'] = $this->getObjectName("GOAL");
		$head_final[1]['STRATRESULT'] = $this->getObjectName("EXPECTEDRESULT");
		$head_final[1]['PROG'] = $this->getObjectName("PROG");
		$head_final[1]['kpi_ref'] = $head1['rows']['kpi_ref']['name'];
		$head_final[1]['kpi_name'] = $head1['rows']['kpi_name']['name'];
		$head_final[1]['kpi_baseline'] = $head1['rows']['kpi_baseline']['name'];
		$head_final[1]['kpi_soe'] = $head1['rows']['kpi_soe']['name'];
		$head_final[1]['kpi_owner_id'] = $head1['rows']['kpi_owner_id']['name'];
		$head_final[1]['kpi_target'] = $head1['rows']['pky_kpi_id']['name'];
		$head_final[2]['kpi_target'] = $idp_details;
		$head_final[1]['kp_indicator_code'] = $this->getObjectName("INDICATORCODE");
		$head_final[1]['proj_ref'] = $head2['rows']['proj_ref']['name'];
		$head_final[1]['proj_name'] = $head2['rows']['proj_name']['name'];
		$head_final[1]['proj_budget'] = "Budget";
		$head_final[2]['proj_budget'] = $idp_details;
		$data['head'] = $head_final;
		$head_final[1]['kpi_cap_op'] = "IS CAPITAL!!";
		$sort = array(
			"STRATOBJ",
			"STRATFOCUS",
			"STRATGOAL",
			"STRATRESULT",
			"PROG",
		);
		$kpi_field = array(
			"kpi_ref",
			"kpi_name",
			"kpi_baseline",
			"kpi_soe",
			"kpi_owner_id",
			"kpi_target",
		);
		$proj_field = array(
			"proj_ref",
			"proj_name",
			"proj_budget",
		);
		
	//Get Lists
		$list_data = array();
		$list = "owner";
		$listObject = new IDP2_LIST($list);
		$list_data[$list] = $listObject->getActiveListItemsFormattedForSelect();
	//Get records	
		$sObject = new IDP2_STRATOBJ();
		$s_details = $sObject->getOrderedObjectsWithChildren($idp_id);
		$strat_details = array();
		//Process results to reverse parentage
		foreach($s_details as $obj_code => $sd) {
			if($obj_code!="STRATOBJ") {
				foreach($sd as $parent_id => $children) {
					foreach($children as $i => $o) {
						//$o['parent'] = $parent_id;
						$strat_details[$obj_code][$i] = $o;
					}
				}
			} else {
				$strat_details[$obj_code] = $sd;
			}
		}
		//Process KPIs to find related Projects
		$project_id = array();
		foreach($kpa_details['PMKPI'] as $prog => $prog_kpi) {
			foreach($prog_kpi as $kpi_id => $kpi) {
				foreach($kpi['kp_proj_id'] as $proj_id => $proj) {
					if(!in_array($proj_id,$project_id)) {
						$project_id[] = $proj_id;
					}
				}
			}
		}
		$projObject = new IDP2_PROJECT();
		$projects = $projObject->getRawObject($project_id);
		//Get Project budgets
		$projectCostObject = new IDP2_PROJECTCOST();
		$project_budgets = $projectCostObject->getChildObjectsForTable($project_id);
		//get KPI targets
		$targetObject = new IDP2_PMKPI_TARGET();
		$targets = $targetObject->getRawObjectGroupedByParent($kpi_ids);
		
		
		//Process records
		$records = array();
		foreach($kpa_details['PMKPA'] as $kpa_id => $kpa) {
			$records[$kpa_id][1] = array();//capital projects
			$records[$kpa_id][0] = array();//operational
			foreach($kpa_details['PMPROG'][$kpa_id] as $prog_id => $prog) {
				foreach($kpa_details['PMKPI'][$prog_id] as $kpi_id => $kpi) {
					$d = array();
					//$indicator_code = "";
					//$indi = $this->getBlankIndicatorCodeArray();
					//if(isset($indi['KPA'])) { $indi['KPA'] = $kpa_id; }
					//if(isset($indi['KPI'])) { $indi['KPI'] = (strlen($kpi['kpi_ref'])>0?$kpi['kpi_ref']:$kpi_id); }
					foreach($head_final[1] as $fld => $h) {
						if($fld == "kpi_target") {
							if(isset($targets[$kpi_id])) {
								foreach($targets[$kpi_id] as $year => $t) {
									$d[$fld][$year] = array_sum($t);
								}
							}
						} elseif(isset($kpi[$fld]) && $fld!="kp_indicator_code") {
							if($head1['rows'][$fld]['type']=="LIST") {
								$d[$fld] = isset($list_data[$head1['rows'][$fld]['list_table']][$kpi[$fld]]) ? $list_data[$head1['rows'][$fld]['list_table']][$kpi[$fld]] : $this->getUnspecified();
							} else {
								$d[$fld] = $kpi[$fld];
							}
						} elseif($fld == "PROG") {
							$d[$fld] = $prog['name'];
						} elseif($fld=="STRATRESULT") {
							$result_id = $kpi['kpi_expresult_id'];
							$d[$fld] = $strat_details[$fld][$result_id]['ser_name'];
							$next_id = $strat_details[$fld][$result_id]['parent_id'];	//STRATGOAL
							$fld = "STRATGOAL";
							$d[$fld] = $strat_details[$fld][$next_id]['name'];
							$next_id = $strat_details[$fld][$next_id]['parent_id']; //STRATFOCUS
							$fld = "STRATFOCUS";
							$d[$fld] = $strat_details[$fld][$next_id]['name'];
							$next_id = $strat_details[$fld][$next_id]['parent_id']; //STRATOBJ
							$fld = "STRATOBJ";
							$d[$fld] = $strat_details[$fld][$next_id]['name'];
							$indi['STRATOBJ'] = $next_id;
						} elseif(in_array($fld,array("STRATOBJ","STRATFOCUS","STRATGOAL"))) {
							//DO NOTHING - HANDLED BY STRATRESULT ABOVE
						}
					}
					foreach($kpi['kp_proj_id'] as $p_id => $p) {
						$d['proj_ref'] = $projects[$p_id]['proj_ref'];
						$d['proj_name'] = $p;
						$d['proj_budget'] = array();
						foreach($head_final[2]['proj_budget'] as $year => $y) {
							$d['proj_budget'][$year] = 0;
							if(isset($project_budgets[$p_id])) {
								foreach($project_budgets[$p_id] as $pc_id => $pc) {
									$d['proj_budget'][$year]+=$pc['years'][$year]['pcy_total'];
								}
							}
						}
						$indi['PROJ'] = $projects[$p_id]['proj_ref'];
						//$indicator_code = implode("_",$indi);
						$indicator_code = $kpi['kp_indicator_code'][$p_id];
						$d['kp_indicator_code'] = $indicator_code;
						$records[$kpa_id][$kpi['kpi_cap_op']][$indicator_code] = $d;
					}
				}
			}
		}
	//sort data
	foreach($records as $kpa_id => $rows1) {
		foreach($rows1 as $cap_op => $rows) {
			foreach($sort as $fld) {
				array_sort_by_column($rows, $fld);
			}
			if($cap_op==true) {
				array_sort_by_column($rows, "kpi_ref");
				array_sort_by_column($rows, "proj_ref");
			} else {
				array_sort_by_column($rows, "proj_ref");
				array_sort_by_column($rows, "kpi_ref");
			}
			$records[$kpa_id][$cap_op] = $rows;
		}
		$records[$kpa_id] = array_merge($records[$kpa_id][1],$records[$kpa_id][0]);
	}
	//check for merge cells
	$x = array();	//count
	$a = array();	//last value recorded
	$i = array();	//first kpi to reflect value
	$count = array();
	foreach($records as $kpa_id => $rows) {
		$count[$kpa_id] = array();
		foreach($sort as $fld) {
			$x[$fld] = 0;
			$a[$fld] = "";
			$i[$fld] = "";
		}
		$x['kpi_ref'] = 0;
		$x['proj_ref'] = 0;
		$a['kpi_ref'] = "";
		$a['proj_ref'] = "";
		$i['kpi_ref'] = "";
		$i['proj_ref'] = "";
		foreach($rows as $ki => $kpi) {
			foreach($sort as $fld) {
				$v = $kpi[$fld];
				if($a[$fld]==$v) {
					//set count for current row to 0 so that report generator knows not to display cell
					$count[$kpa_id][$ki][$fld] = 0;
					//increment count of first instance to set rowspan
					$x[$fld]++;
				} else {
					//set count for previous instance
					$count[$kpa_id][$i[$fld]][$fld] = $x[$fld];
					//restart count
					$x[$fld] = 1;
					$a[$fld] = $v;
					$i[$fld] = $ki;
				}
			}
			if($kpi['kpi_cap_op']==true) {
				$sort_field = "kpi_ref";
				$sort_fields = $kpi_field;
			} else {
				$sort_field = "proj_ref";
				$sort_fields = $proj_field;
			}
			$fld = $sort_field;

//			foreach($sort_fields as $fld) {
				$v = $kpi[$fld];
				if($a[$fld]==$v) {
					//set count for current row to 0 so that report generator knows not to display cell
					$count[$kpa_id][$ki][$fld] = 0;
					foreach($sort_fields as $fld2) {
						$count[$kpa_id][$ki][$fld2] = 0;
					}
					//increment count of first instance to set rowspan
					$x[$fld]++;
				} else {
					//set count for previous instance
					$count[$kpa_id][$i[$fld]][$fld] = $x[$fld];
					foreach($sort_fields as $fld2) {
						$count[$kpa_id][$i[$fld]][$fld2] = $x[$fld];
					}
					
					//restart count
					$x[$fld] = 1;
					$a[$fld] = $v;
					$i[$fld] = $ki;
				}
			//}
			$count[$kpa_id][$ki]['indicator_code'] = 1;
		}
		//set count for final instance
		foreach($sort as $fld) {
			$count[$kpa_id][$i[$fld]][$fld] = $x[$fld];
		}
		if($kpi['kpi_cap_op']==true) {
			$sort_fields = $kpi_field;
			$source = "kpi_ref";
		} else {
			$sort_fields = $proj_field;
			$source = "proj_ref";
		}
		foreach($sort_fields as $fld) {
			$count[$kpa_id][$i[$source]][$fld] = $x[$source];
		}
	}
	
	//prep for final return
	$data['rows'] = $records;
	$data['count'] = $count;
	//return data	
		return $data;
	}

/*****************************************************************************************************************
 * ADMIN / FINANCIAL INTEGRATION reports
 */
 
 
	private function getAFIndicatorPushReport($idp_id) {
		$data = array(
			'head'=>array(),
			'rows'=>array()
		);
		
	//SET HEADING	
		$head_used = array();
		$head_used['financial_year'] = "Financial_year";
		$head_used['indicator_code'] = "Indicator Code";
		$head_used['indicator'] = "Indicator";
		$data['head'] = $head_used;
		
		$headObject = new IDP2_HEADINGS();
		$head1 = $headObject->getMainObjectHeadings("PMKPI"); 
		$head2 = $headObject->getMainObjectHeadings("PROJECT");
		
		$head_final =array();
		$head_final['STRATOBJ'] = $this->getObjectName("OBJECTIVE");
		$head_final['STRATFOCUS'] = $this->getObjectName("FOCUSAREA");
		$head_final['STRATGOAL'] = $this->getObjectName("GOAL");
		$head_final['STRATRESULT'] = $this->getObjectName("EXPECTEDRESULT");
		$head_final['PROG'] = $this->getObjectName("PROG");
		$head_final['kpi_ref'] = $head1['rows']['kpi_ref']['name'];
		$head_final['kpi_name'] = $head1['rows']['kpi_name']['name'];
		$head_final['indicator_code'] = "Indicator Code";
		$head_final['proj_ref'] = $head2['rows']['proj_ref']['name'];
		$head_final['proj_name'] = $head2['rows']['proj_name']['name'];
		$head_final['kpi_cap_op'] = "IS CAPITAL!!";
		
		$sort = array(
			"STRATOBJ",
			"STRATFOCUS",
			"STRATGOAL",
			"STRATRESULT",
			"PROG",
		);
		$kpi_field = array(
			"kpi_ref",
			"kpi_name",
			"kpi_baseline",
			"kpi_soe",
			"kpi_owner_id",
			"kpi_target",
		);
		$proj_field = array(
			"proj_ref",
			"proj_name",
			"proj_budget",
		);
		
		
	//GET FIN YEAR DATA
		$idpObject = new IDP2_IDP();
		$i_object = $idpObject->getAObject($idp_id);
		$fin_year = $i_object['rows']['idp_finyear_id'];
		
	//GET RECORD DATA	
		$kpaObject = new IDP2_PMKPA();
		$kpa_details = $kpaObject->getOrderedObjectsWithChildren($idp_id);
		//Get list of KPI IDs for getting of targets
		$kpi_ids = array();
		foreach($kpa_details['PMKPI'] as $prog_id => $prog_kpis) {
			foreach($prog_kpis as $ki => $k) {
				if(!in_array($ki,$kpi_ids)) {
					$kpi_ids[] = $ki;
				}
			}
		}
		
		$sObject = new IDP2_STRATOBJ();
		$s_details = $sObject->getOrderedObjectsWithChildren($idp_id);
		$strat_details = array();
		//Process results to reverse parentage
		foreach($s_details as $obj_code => $sd) {
			if($obj_code!="STRATOBJ") {
				foreach($sd as $parent_id => $children) {
					foreach($children as $i => $o) {
						//$o['parent'] = $parent_id;
						$strat_details[$obj_code][$i] = $o;
					}
				}
			} else {
				$strat_details[$obj_code] = $sd;
			}
		}
		//Process KPIs to find related Projects
		$project_id = array();
		foreach($kpa_details['PMKPI'] as $prog => $prog_kpi) {
			foreach($prog_kpi as $kpi_id => $kpi) {
				foreach($kpi['kp_proj_id'] as $proj_id => $proj) {
					if(!in_array($proj_id,$project_id)) {
						$project_id[] = $proj_id;
					}
				}
			}
		}
		$projObject = new IDP2_PROJECT();
		$projects = $projObject->getRawObject($project_id);
		
		//Process records
		$records = array();
		foreach($kpa_details['PMKPA'] as $kpa_id => $kpa) {
			$records[$kpa_id] = array();
			foreach($kpa_details['PMPROG'][$kpa_id] as $prog_id => $prog) {
				foreach($kpa_details['PMKPI'][$prog_id] as $kpi_id => $kpi) {
					$d = array();
					$indicator_code = "";
					$indi = $this->getBlankIndicatorCodeArray();
					if(isset($indi['KPA'])) { $indi['KPA'] = $kpa_id; }
					if(isset($indi['KPI'])) { $indi['KPI'] = (strlen($kpi['kpi_ref'])>0?$kpi['kpi_ref']:$kpi_id); }
					foreach($head_final as $fld => $h) {
						if(isset($kpi[$fld])) {
							if($head1['rows'][$fld]['type']=="LIST") {
								$d[$fld] = isset($list_data[$head1['rows'][$fld]['list_table']][$kpi[$fld]]) ? $list_data[$head1['rows'][$fld]['list_table']][$kpi[$fld]] : $this->getUnspecified();
							} else {
								$d[$fld] = $kpi[$fld];
							}
						} elseif($fld == "PROG") {
							$d[$fld] = $prog['name'];
						} elseif($fld=="STRATRESULT") {
							$result_id = $kpi['kpi_expresult_id'];
							$d[$fld] = $strat_details[$fld][$result_id]['ser_name'];
							$next_id = $strat_details[$fld][$result_id]['parent_id'];	//STRATGOAL
							$fld = "STRATGOAL";
							$d[$fld] = $strat_details[$fld][$next_id]['name'];
							$next_id = $strat_details[$fld][$next_id]['parent_id']; //STRATFOCUS
							$fld = "STRATFOCUS";
							$d[$fld] = $strat_details[$fld][$next_id]['name'];
							$next_id = $strat_details[$fld][$next_id]['parent_id']; //STRATOBJ
							$fld = "STRATOBJ";
							$d[$fld] = $strat_details[$fld][$next_id]['name'];
							$indi['STRATOBJ'] = $next_id;
						} elseif(in_array($fld,array("STRATOBJ","STRATFOCUS","STRATGOAL"))) {
							//DO NOTHING - HANDLED BY STRATRESULT ABOVE
						}
					}
					foreach($kpi['kp_proj_id'] as $p_id => $p) {
						$d['proj_ref'] = $projects[$p_id]['proj_ref'];
						$d['proj_name'] = $p;
						$d['proj_budget'] = array();
						$indi['PROJ'] = $projects[$p_id]['proj_ref'];
						$indicator_code = implode("_",$indi);
						$d['indicator_code'] = $indicator_code;
						$records[$kpa_id][$indicator_code] = $d;
					}
				}
			}
		}
	//sort data
		$rows = array();
		foreach($records as $kpa_id => $rows1) {
			foreach($rows1 as $i => $r) {
				$rows[$i] = array(
					'financial_year'=>$fin_year,
					'indicator_code'=>$r['indicator_code'],
					'indicator'=>$r['kpi_name']." : ".$r['proj_name'],
				);
			}
				/*foreach($sort as $fld) {
					array_sort_by_column($rows, $fld);
				}
				if($cap_op==true) {
					array_sort_by_column($rows, "kpi_ref");
					array_sort_by_column($rows, "proj_ref");
				} else {
					array_sort_by_column($rows, "proj_ref");
					array_sort_by_column($rows, "kpi_ref");
				}
				$records[$kpa_id] = $rows;*/
		}
		array_sort_by_column($rows, "indicator");
	//prep for final return
		$data['rows'] = $rows;
	//return data	
		return $data;
		
		
		
	}

}

function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}


?>