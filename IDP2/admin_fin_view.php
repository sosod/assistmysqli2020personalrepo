<?php
require_once("inc_header.php");

//ASSIST_HELPER::arrPrint($_REQUEST);
$reportObject = new IDP2_FIXED_REPORT();
$data = $reportObject->getReport("admin_fin_".$_REQUEST['ref'], $_REQUEST['object_id']);

//ASSIST_HELPER::arrPrint($data);

echo "
	<table class=list>
		<tr>
";

$head = $data['head'];
	foreach($head as $fld => $h) {
		$rowspan = 1;
		$colspan = 1;
		echo "<th rowspan=".$rowspan." colspan=".$colspan.">".$h."</th>";
	}

echo "</tr>";

$rows = $data['rows'];

foreach($rows as $i => $r) {
	echo "
	<tr>";
	foreach($head as $fld => $h) {
		echo "<td >".(isset($r[$fld]) ? $r[$fld] : "")."</td>";
	}
	echo "
	</tr>";
}

echo "</table>";
?>