<?php

$child_object_id = $_REQUEST['object_id'];

$section = "NEW";
$page_redirect_path = "new_create_idp.php?idp_object_id=".$object_id."&tab=".$my_tab."&tab_act=tree&";
$page_action = "Edit";
$dialog_url = "new_stratprojects_object.php";





require_once("inc_header.php");
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());



//TIER1 = PROJECT
$t1Object = new IDP2_PROJECT();
$object_type = $t1Object->getMyObjectType();
$object_name = $t1Object->getMyObjectName();
$t1_object_type = $object_type;
$t1_object_name = $object_name;
$t1_id = $child_object_id;
$t1_object_id = $child_object_id;
$t1_object_details = $t1Object->getSimpleDetails($t1_object_id);
$t1_segment_details = $t1Object->getSegmentDetails($t1_object_id);
//ASSIST_HELPER::arrPrint($t1_segment_details);

$idp_object_id = $t1_object_details['parent_id'];
$idpObject = new IDP2_IDP();
$idpyearsObject = new IDP2_IDPYEARS();
$idpYears = $idpyearsObject->getYearsForSpecificIDP($idp_object_id);
$iy_id_fld = $idpyearsObject->getIDFieldName();


$t_headings = array();
$t_row_headings = array();
$t_objects = array();
$t_parent_name = array();
$t_object_type = array();

//TIER2 = INCOME
$t2Object = new IDP2_PROJECTINCOME();
$t2_object_type = $t2Object->getMyObjectType();
$t2_object_name = $t2Object->getMyObjectName();

//TIER3 = COST
$t3Object = new IDP2_PROJECTCOST();
$t3_object_type = $t3Object->getMyObjectType();
$t3_object_name = $t3Object->getMyObjectName();
/*
//TIER4 = KPI / INDICATORS
$t4Object = new IDP2_PROJECTKPI();
$t4_object_type = $t4Object->getMyObjectType();
$t4_object_name = $t4Object->getMyObjectName();
*/




echo "
<h2>".$t1_object_details['name']."
	<span style='padding-left:20px'>
		<button class=btn_project_view object_id=".$t1_object_id." object_type=".$t1_object_type.">View ".$helper->getObjectName($t1_object_name)."</button>&nbsp;
		<button class=btn_project_edit object_id=".$t1_object_id." object_type=".$t1_object_type.">Edit ".$helper->getObjectName($t1_object_name)."</button>
	</span>
</h2>
";

?>
<h3>Project Summary</h3>
<?php
$child_object_type = $object_type;
$childObject = $t1Object;
$page_action = "COMPACT";
$parentObject = $idpObject;
$parent_object_type = $parentObject->getMyObjectType();
$parent_object_id = $idp_object_id;
$data = $displayObject->getObjectForm($child_object_type, 
										$childObject, 
										$child_object_id, 
										$parent_object_type, 
										$parentObject, 
										$parent_object_id, 
										$page_action, 
										$page_redirect_path);
//ASSIST_HELPER::arrPrint($data);										
echo $data['display'];
$js.= $data['js'];

?>
<h3>Segment Details</h3>
<table id=tbl_segment_details class=form>
	<?php
	foreach($t1_segment_details as $head => $val) {
		echo "
		<tr>
			<th>".$head.":</th>
			<td>".ASSIST_MODULE_DISPLAY::convertStringToHTML($val)."</td>
		</tr>";
	}
	?>
</table>

<?php



echo "<h3>".$helper->getObjectName($t2_object_name)."</h3>";

$tier = "t2";

$js.=$displayObject->drawProjectDetailTable($tier, $t2_object_type, $t1_object_id,true,true);



echo "<h3>".$helper->getObjectName($t3_object_name)."</h3>";

$tier = "t3";

$js.=$displayObject->drawProjectDetailTable($tier, $t3_object_type, $t1_object_id,true,true);

/*
echo "<h3>".$helper->getObjectName($t4_object_name)."</h3>";

$tier = "t4";

$js.=$displayObject->drawProjectDetailTable($tier, $t4_object_type, $t1_object_id,true,true);
*/

?>

<div id=stratproject_dlg_child class=dlg-child title="">
		<iframe id=stratplan_ifr_form_display style="border:0px solid #000000;" src="">
			
		</iframe>
</div>

<script type=text/javascript>
$(function() {

	//Formatting - not object_type specific
	$("table.form").attr("width","");
	$("table.form tr th").attr("width","");
	var my_window = AssistHelper.getWindowSize();
	//alert(my_window['height']);
	if(my_window['width']>800) {
		var my_width = 850;
	} else {
		var my_width = 800;
	}
	var my_height = my_window['height']-50;	

		
	
	$("div.dlg-child").dialog({
		autoOpen: false,
		modal: true,
		width: my_width,
		height: my_height,
		beforeClose: function() {
			AssistHelper.closeProcessing();
		},
		open: function() {
			$(this).dialog('option','width',my_width);
			$(this).dialog('option','height',my_height);
		}
	});	
	
	
	$(".btn_add").button({
		icons:{primary:"ui-icon-circle-plus"}
	}).removeClass("ui-state-default").addClass("ui-button-state-green")
	.click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var ot = $(this).attr("object_type");
		var tier = $(this).attr("tier");
		var $form = $("form[name=frm_"+tier+"]");
		var error = false;
		$form.find("input:text").each(function() {
			if($(this).hasClass("required")) {
				//alert($(this).prop("name"));
				error = true;
			} else if($(this).val().length==0) {
				$(this).val("0");
			}
		});
		$form.find("select").each(function() {
			$(this).removeClass("required");
			if($(this).val()=="X") {
				$(this).addClass("required");
				error = true;
			}
		});
		if(error) {
			AssistHelper.finishedProcessing("error","Please correct the errors highlighted in red.");
		} else {
				
			var dta = AssistForm.serialize($form);
			//console.log(dta);
			
			var result = AssistHelper.doAjax("inc_controller.php?action="+ot+".Add",dta);
			//console.log(result);
			if(result[0]=="ok") {
				//FINISH THIS FUNCTION!!!!
				//$("<tr><td></td></tr>").append($(this).parent().parent().parent());
				var htmlDTA = "&tier="+tier+"&object_type="+ot+"&project_id=<?php echo $t1_object_id; ?>&object_id="+result['object_id'];
				//console.log(htmlDTA);
				var insertHTML = AssistHelper.doAjax("inc_controller.php?action=Display.getProjectDetailChildRow"+htmlDTA, dta);
				//console.log(insertHTML);
				var display = ""+insertHTML['display']+"";
				//console.log($(this).closest("table").prop("id"));
				//$(this).parent().parent().parent().find("tr:last").insertAfter(display);
				$(this).closest("table").append(display);
				var $edit_btn = $('<button />',
					{
						text: 'Edit',
						id: 'btn_'+tier+'_edit',
						object_type: ot,
						tier: tier,
						click: function (e) { 
							e.preventDefault();
							editLineItem($(this));
						}
					}).button({
						icons:{primary:"ui-icon-pencil"}
					}).addClass('btn_edit');
				$(this).closest("table").find("td.td_edit:last").append($edit_btn);
				
				
				$form.find("input:text").each(function() {
					$(this).val("0");
				});
				$form.find("select").each(function() {
					$(this).val("X");
				});
				$form.find("label").each(function() {
					$(this).text("0");
				});
			}
			AssistHelper.finishedProcessing(result[0],result[1]);
		}
	});
	$("input:text.CALC").blur(function() {
		var cf = $(this).attr("calc_field");
		var total = 0;
		$("input:text."+cf).each(function() {
			total+=parseFloat($(this).val());
		});
		$("label."+cf).text(total);
	});

	$(".btn_edit").button({
		icons:{primary:"ui-icon-pencil"}
	}).click(function(e) {
		e.preventDefault();
		editLineItem($(this));
	});

	$(".btn_delete").button({
		icons:{primary:"ui-icon-trash"}
	}).removeClass("ui-state-default").addClass("ui-button-state-red").css({"font-size":"85%","border":"1px solid #FFF"})
	.click(function(e) {
		e.preventDefault();
		deleteLineItem($(this));
	});



	
	$(".btn_view").button({
		icons:{primary:"ui-icon-newwin"}
	}).click(function(e) {
		e.preventDefault();
		editLineItem($(this));
	});
	


	$(".btn_project_edit").button({
		icons:{primary:"ui-icon-pencil"}
	});
	$(".btn_project_view").button({
		icons:{primary:"ui-icon-newwin"}
	});
	
	$(".btn_project_edit, .btn_project_view")
	.removeClass("ui-state-default").addClass("ui-button-state-grey")
	.css({"border":"1px solid #FFFFFF","font-size":"60%"})
	.hover(function() {
		$(this).removeClass("ui-button-state-grey").addClass("ui-button-state-info").css({"border":"1px solid #FE9900"});
	},function() {
		$(this).removeClass("ui-button-state-info").addClass("ui-button-state-grey").css({"border":"1px solid #FFFFFF"});
	})
	.click(function(e) {
		e.preventDefault();
		editLineItem($(this));
	});

	
	
	function deleteLineItem($btn) {
		var i = $btn.attr("object_id");
		var r = $btn.attr("object_ref");
		var t = $btn.attr("object_type");
		if(confirm("Are you sure you wish to delete line item: "+r)==true) {
			AssistHelper.processing();
			var dta = "object_id="+i;
			var result = AssistHelper.doAjax("inc_controller.php?action="+t+".DELETE",dta);
			AssistHelper.finishedProcessing(result[0],result[1]);
			if(result[0]=="ok") {
				//btn.parent(td).parent(tr)
				var tr_class = $btn.parent().parent().attr('class');
				$("tr."+tr_class).hide();
			}
		}
	}

	function editLineItem($btn) {
		//console.log($btn);
		AssistHelper.processing();
		if($btn.hasClass("btn_project_view")) {
			var act = "view";
			//AssistHelper.finishedProcessing("ok","display project view dialog");
		} else if($btn.hasClass("btn_project_edit")) {
			act = "edit";
			//AssistHelper.finishedProcessing("error","display project edit dialog");
		} else if($btn.hasClass("btn_edit")) {
			act = "edit";
			//AssistHelper.finishedProcessing("info","Edit form");
		} else {
			var act = "view";
			//AssistHelper.finishedProcessing("info","View form");
		}
		
		var i = $btn.attr("object_id"); 
		var t = $btn.attr("object_type");
		//var dta = "object_type="+t+"&object_id="+i; 
		$dlg = $("div#stratproject_dlg_child");
		//var obj = t.toLowerCase();
		var heading = AssistString.ucwords(act);
		var url = "<?php echo $dialog_url; ?>?display_type=dialog&page_action="+act+"&object_type="+t+"&object_id="+i;
		$dlg.dialog("option","title",heading);
		if(act=="view") {
			$dlg.dialog("option","buttons",[ { text: "Close", click: function() { $( this ).dialog( "close" ); } } ]);
			$dlg.css("overflow","hidden");
			$dlg.find("iframe").prop("width",(my_width-15)+"px").prop("height",(my_height-85)+"px").prop("src",url);
		} else {
			$dlg.dialog("option","buttons",[ ]);
			$dlg.css("overflow","hidden");
			$dlg.find("iframe").prop("width",(my_width-20)+"px").prop("height",(my_height-35)+"px").prop("src",url);
		}
	//FOR DEVELOPMENT PURPOSES ONLY - DIALOG SHOULD BE OPENED BY PAGE INSIDE IFRAME ONCE IFRAME FULLY LOADED
		//$dlg.dialog("open");
			
			
	}


	
	/*
	 * Adjust tables to display equally
	 */
	/*
	var table = new Array();
	var eq = 0;
	$("table.tbl_project_elements:first tr:first th").each(function() {
		table[eq] = 0;
		eq++;
	});
	//console.log(table);
	$("table.tbl_project_elements").each(function() {
		eq = 0;
		//console.log($(this).prop("id"));
		//$(this).find("tr:first th").each(function() { 
		$(this).find("tr:eq(1) td").each(function() { 
			//console.log($(this).css("width")+" = "+$(this).html());
			var w = parseInt(AssistString.substr($(this).css("width"),0,-2)); console.log(w);
			if(w>table[eq]) { table[eq] = w; }
			eq++;
		});
		//console.log(table);
	});
	
	$("table.tbl_project_items").each(function() {
		eq = 0;
		$(this).find("tr:first th").each(function() {
			$(this).css("width",table[eq]+"px");
			eq++;
		});
	});
	*/
	
	
	
});

function dialogFinished(icon,result) {
	if(icon=="ok") {
		document.location.href = '<?php echo $page_redirect_path."object_id=".$t1_id; ?>&r[]=ok&r[]='+result;
	} else {
		AssistHelper.processing();
		AssistHelper.finishedProcessing(icon,result);
	}
}
</script>
<?php

//ASSIST_HELPER::arrPrint($_SESSION[$helper->getModRef()]['PROJECT']);
?>