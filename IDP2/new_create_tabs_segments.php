<?php
$page_action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "LIST";
$_REQUEST['action'] = $page_action;
$idp_object_id = $_REQUEST['object_id'];
//ASSIST_HELPER::arrPrint($_REQUEST);
 
$dialog_url = "new_segment_object.php";
$query_string = array();
foreach($_REQUEST as $key => $r) {
	if($key!="r") {
		$query_string[] = $key."=".$r;
	}
}
$dialog_redirect = "new_create_idp.php?".implode("&",$query_string);

//needed for colspan counting
function drawTextTree($i,$s,$indent,$level) {
	global $segment_data;
	global $total_levels;
	if($total_levels<$level) {
		$total_levels = $level;
	}
		if($s['has_children']==true) {
			if(isset($segment_data[$i])) {
				$icon = "+";
			} else {
				$icon = "-";
			}
		} else {
			$icon = "*";
		} 
	
		$echo= "<p>".$icon."".$s['name']."&nbsp;V&nbsp;E&nbsp;";
		if($s['has_children']==true) {
			$echo.= "AC";
			if(isset($segment_data[$i])) {
				$indent.="----";
				$level++;
				foreach($segment_data[$i] as $i2 => $s2) {
					drawTextTree($i2,$s2,$indent,$level);
				}
				$level--;
			}
		}
		$echo.= "</p>";
		//echo $echo;
	
}
/*
function drawTableTree($i,$s,$indent) {
	global $segment_data;
	global $total_levels;
	global $segmentObject;
	global $idp_object_id;
		if($s['has_children']==true) {
			if(isset($segment_data[$i])) {
				$icon = "ui-icon-triangle-1-se";
			} else {
				$icon = "ui-icon-triangle-1-e";
			}
		} else {
			$icon = "ui-icon-carat-1-sw";
		} 
	$echo = "";
		$echo.= "<tr>";
		for($x=0;$x<$indent;$x++) {
			$echo.="<td>&nbsp;</td>";
		}
		$echo.= "<td>".$segmentObject->getDisplayIcon($icon)."</td><td colspan=".($total_levels-$indent).">".$s['name'];
		if($s['has_children']==true) {
			$echo.= "<span class=float><button class='abutton action-button btn_add' parent_id=".$s['id']."_".$idp_object_id." object_id='0'>Add Child</button></span>";
		}
		$echo.="</td><td>&nbsp;<button class='abutton action-button btn_view' parent_id=".$s['parent']."_".$idp_object_id." object_id=".$s['id'].">View</button>&nbsp;<button class='abutton action-button btn_edit' parent_id=".$s['parent']."_".$idp_object_id." object_id=".$s['id'].">Edit</button>&nbsp;";
		$echo.= "</td></tr>";
		if($s['has_children']==true) {
			if(isset($segment_data[$i])) {
				$indent++;
				foreach($segment_data[$i] as $i2 => $s2) {
					$echo.=drawTableTree($i2,$s2,$indent);
				}
				$indent--;
			}
		}
		return $echo;
	
}
*/


function drawTableTree($i,$s,$indent) {
	global $segment_data;
	global $total_levels;
	global $segmentObject;
	global $idp_object_id;
		if($s['has_children']==true) {
			if(isset($segment_data[$i])) {
				$icon = "ui-icon-triangle-1-se";
			} else {
				$icon = "ui-icon-triangle-1-e";
			}
		} else {
			$icon = "ui-icon-carat-1-sw";
		} 
	$echo = "";
		$echo.= "<tr>";
		for($x=0;$x<$indent;$x++) {
			$echo.="<td>&nbsp;</td>";
		}
		$echo.= "<td>".$segmentObject->getDisplayIcon($icon)."</td><td colspan=".($total_levels-$indent).">".$s['name']."</td><td class=right>";
		if($s['has_children']==true) {
			$echo.= "<button class='abutton action-button btn_add' parent_id=".$s['id']."_".$idp_object_id." object_id='0'>Add Child</button>";
		}
		$echo.="<span class=float>";
		if($s['can_assign']==1) {
			$echo.=$segmentObject->getDisplayIcon("ui-icon-circle-check","","009900");
		}
		$echo.="</span></td><td>&nbsp;<button class='abutton action-button btn_view' parent_id=".$s['parent']."_".$idp_object_id." object_id=".$s['id'].">View</button>&nbsp;<button class='abutton action-button btn_edit' parent_id=".$s['parent']."_".$idp_object_id." object_id=".$s['id'].">Edit</button>&nbsp;";
		$echo.= "</td></tr>";
		if($s['has_children']==true) {
			if(isset($segment_data[$i])) {
				$indent++;
				foreach($segment_data[$i] as $i2 => $s2) {
					$echo.=drawTableTree($i2,$s2,$indent);
				}
				$indent--;
			}
		}
		return $echo;
	
}

if($page_action=="SEGMENT") {
	$section = $_REQUEST['section']; //echo $section;
	$segmentObject = new IDP2_SEGMENTS($section);
	//echo "getList start"; 
	$segment_data = $segmentObject->getList(0,TRUE,$idp_object_id);
	//echo "getList end";
	$segment_details = $segmentObject->getSegmentDetails(true);
	//ASSIST_HELPER::arrPrint($segment_data);

	//$total_levels = 0;
	
	/***
	 * Basic text version
	 */
	//needed for colspan counting
		$indent = "";
		$levels = 1;
if(isset($segment_data[0]) && count($segment_data[0])>0) {
	foreach($segment_data[0] as $i => $s) {
		drawTextTree($i,$s,$indent,$levels);
	}
} else {
	
}

	echo "
	<span class=float style='position:relative;top:10px;right:10px'><button class='abutton action-button btn_add top-level' parent_id='0_".$idp_object_id."' object_id=0>Add ".$segment_details['name']."</button></span>
	<h1>".$segment_details['name']."</h1>
	<p class=i>".$segment_details['help']."</p>";

	/**
	 * Table version
	 */
	echo "<table id=tbl_segment_tree>";
	$echo = "";
if(isset($segment_data[0]) && count($segment_data[0])>0) {
	foreach($segment_data[0] as $i => $s) {
		$indent = 0;
		$echo.= drawTableTree($i,$s,$indent);
	}
} else {
	$echo.= "<tr><td>Please click the Add button at the top right of your screen to start.</td></tr>";
}
	echo $echo."</table>";


	?>
<div id=dlg_child class=dlg-child title="">
		<iframe id=ifr_form_display style="border:0px solid #000000;" src="">
			
		</iframe>
</div>
	<script type="text/javascript" >
		$(function() {
			$("#tbl_segment_tree, #tbl_segment_tree td").css("border","0px solid #000000");
			$("#tbl_segment_tree tr").hover(function() {
				$(this).css("background-color","#dedede");
			},function() {
				$(this).css("background-color","");
			});
			$("button.btn_add").button({
				icons:{primary:"ui-icon-plus"}
			}).removeClass("ui-state-default")
			.addClass("ui-button-state-ok")
			.children(".ui-button-text")
			.css({"padding-top":"2px","padding-bottom":"0px"})
			.hover(
				function() { $(this).removeClass("ui-button-state-ok").addClass("ui-button-state-info"); }, 
				function() { $(this).removeClass("ui-button-state-info").addClass("ui-button-state-ok"); }
			).click(function(e) {
				e.preventDefault();
				
			});
			$("button.btn_view").button({
				icons:{primary:"ui-icon-extlink"}
			}).removeClass("ui-state-default")
			.addClass("ui-button-state-grey")
			.children(".ui-button-text")
			.css({"padding-top":"0px","padding-bottom":"0px"})
			.hover(
				function() { $(this).removeClass("ui-button-state-grey").addClass("ui-button-state-info"); }, 
				function() { $(this).removeClass("ui-button-state-info").addClass("ui-button-state-grey"); }
			).click(function(e) {
				e.preventDefault();
				
			});
			$("button.btn_edit").button({
				icons:{primary:"ui-icon-pencil"}
			}).removeClass("ui-state-default")
			.addClass("ui-button-state-grey")
			.children(".ui-button-text")
			.css({"padding-top":"2px","padding-bottom":"0px"})
			.hover(
				function() { $(this).removeClass("ui-button-state-grey").addClass("ui-button-state-info"); }, 
				function() { $(this).removeClass("ui-button-state-info").addClass("ui-button-state-grey"); }
			).click(function(e) {
				e.preventDefault();
				
			});
			
			
			var my_window = AssistHelper.getWindowSize();
			//alert(my_window['height']);
			if(my_window['width']>800) {
				var my_width = 850;
			} else {
				var my_width = 800;
			}
			var my_height = my_window['height']-50;	
			
			$("div.dlg-child").dialog({
				autoOpen: false,
				modal: true,
				width: my_width,
				height: my_height,
				beforeClose: function() {
					AssistHelper.closeProcessing();
				},
				open: function() {
					$(this).dialog('option','width',my_width);
					$(this).dialog('option','height',my_height);
				}
			});	
			
			$("button.action-button").click(function() {
				AssistHelper.processing();
				var idp_id = <?php echo $idp_object_id; ?>;
				var i = $(this).attr("object_id");
				var p = $(this).attr("parent_id");
				var t = "<?php echo $section; ?>";
				var dta = "idp_object_id="+idp_id+"&object_type="+t+"&object_id="+i+"&parent_id="+p;
					$dlg = $("div#dlg_child");
					var act = "view";
					if($(this).hasClass("btn_edit")) {
						act = "edit";
					} else if($(this).hasClass("btn_add")) {
						act = "add";
					}
					var obj = t.toLowerCase();
					var heading = AssistString.ucwords(act);
					var url = "<?php echo $dialog_url; ?>?display_type=dialog&page_action="+act+"&"+dta;
					//console.log(url);
					$dlg.dialog("option","title",heading);
					if(act=="view") {
						$dlg.dialog("option","buttons",[ { text: "Close", click: function() { $( this ).dialog( "close" ); } } ]);
					}
					$dlg.find("iframe").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").prop("src",url);
					//FOR DEVELOPMENT PURPOSES ONLY - DIALOG SHOULD BE OPENED BY PAGE INSIDE IFRAME ONCE IFRAME FULLY LOADED
					//$dlg.dialog("open");
/*				
				*/
			});			
			
			
			
			
		});
	function dialogFinished(icon,result) {
		if(icon=="ok") {
			//alert(result)
			//document.location.href = '<?php echo $dialog_redirect; ?>&r[]=ok&r[]='+result;
			$dlg = $("div#dlg_child");
			$dlg.dialog("close");
			AssistHelper.processing();
			AssistHelper.finishedProcessingWithRedirect(icon,result,'<?php echo $dialog_redirect; ?>');
		} else {
			AssistHelper.processing();
			AssistHelper.finishedProcessing(icon,result);
		}
	}
		
	</script>
	<?php



} else { //else if page action == segment
	
	$segmentObject = new IDP2_SEGMENTS(); 
	$segments = $segmentObject->getSegmentDetails();
	
	$segment_page = "new_create_idp.php?".$_SERVER['QUERY_STRING']."&action=SEGMENT&section=";
	
	function drawVisibleBlock($title,$lot,$text) {
		
	?>
	
	<div style='width:500px;' id=div_<?php echo $lot; ?> class=div_strategy page=<?php echo $lot; ?> my_btn=btn_<?php echo $lot; ?>_open>
		<?php 
		echo "<h3 class=object_title>".$title."</h3>";
		?>
		<Table class=form width=100%>
			<tr>
				<td class=b><?php 
							echo $text;
					?></td>
			</tr>
				<?php
				?>
		</Table>
		<p class=right><button class=btn_open id=btn_<?php echo $lot; ?>_open>Open</button></p>
	</div>
	<?php	
		
		
		
	}
	
	
	
	foreach($segments as $key => $s) {
		drawVisibleBlock($s['name'],$key, $s['help']);
	}
	
	
	
	
	?>
	
	<script type="text/javascript">
		
	$(function() {
		var div_h = 0;
		var tbl_h = 0;
		$("div.div_strategy").button().click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var page = $(this).attr("page");
			var url = '<?php echo $segment_page; ?>'+page.toLowerCase();
			//alert(url);
			document.location.href = url;
			
			//var btn_id = $(this).attr('my_btn');
			//$(btn_id).trigger('click');
			//$(this).find("button.btn_open").trigger("click");
		});
		$("div.div_strategy").css({"margin":"10px","background":"url()","padding-bottom":"30px"});


		var changes_since_reload = 0;
		var scr_dimensions = AssistHelper.getWindowSize();
		//console.log(scr_dimensions);
	
		$("button.btn_open").button({
			icons:{primary:"ui-icon-newwin"}
		}).css({
			"position":"absolute","bottom":"5px","right":"5px"
		}).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var page = $(this).closest("div").attr("page");
			//alert(page.toLowerCase());
			document.location.href = '<?php echo $segment_page; ?>'+page.toLowerCase();
		});
		function formatButtons() {
			$("button.xbutton").children(".ui-button-text").css({"font-size":"80%","padding":"4px","padding-left":"24px"});
			$("button.xbutton").css({"margin":"2px"});
		}
		formatButtons();
		<?php
		if(isset($_REQUEST['section'])) {
			echo "
			$('#btn_".$_REQUEST['section']."_open').trigger('click');
			";
		}
		?>
	});
	</script>
	<?php
	
	
	
	
	
	

} //end if page action == segment











?>