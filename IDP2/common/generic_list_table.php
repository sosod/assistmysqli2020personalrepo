<?php
/*******
 * Pre-requisites:
 * $page_section = MANAGE || ADMIN
 * $page_action = UPDATE || EDIT
 */

if(!isset($_REQUEST['object_type'])) {
	if($page_action=="MANAGE") {
		$_REQUEST['object_type'] = "ACTION";
	} else {
		$_REQUEST['object_type'] = "CONTRACT";
	}
}
$object_type = $_REQUEST['object_type'];
$my_page = strtolower($_REQUEST['object_type']);



ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());





$page_type = "LIST";
//$button_label = !isset($button_label) ? ucwords(strtolower($page_action)) : $button_label;
if(!isset($button_label) || strlen($button_label)==0) {
	//$b = $helper->getActivityName($page_action);
	//if(strlen($b)>0) {
		//$button_label = $b;
	//} else {
		//$button_label = strlen($active_button_label)>0 ? $active_button_label : ucwords(strtolower($page_action));
	$button_label = $helper->getActivityName("open");
	//}
} elseif($button_label=="breadcrumbs") {
	$button_label = implode(" ",$menu['breadcrumbs']);
} elseif(substr($button_label,0,1)=="|" && substr($button_label,-1)=="|") {
	$button_label = $helper->getActivityName(substr($button_label,1,-1));
}

if($page_section=="NEW") {
	$page_action.= ($page_action!="CONFIRM" && substr($page_action,0,8)!="ACTIVATE" ? "_".$child_object_type : "");
} elseif($page_action!="VIEW"){
	$page_action.= "_".$object_type;
} 
if(isset($page_direct)) {
	$page_direct = strtolower($page_direct)."?object_id=";
} elseif($page_section!="NEW") {
	$page_direct = strtolower($page_section)."_".strtolower($page_action)."_object.php?object_id=";
} else {
	$pa = substr($page_action,0,strpos($page_action,"_"));
	$page_direct = strtolower($page_section)."_".strtolower($object_type)."_".strtolower($pa)."_object.php?object_id=";
}

$js.= " var page_direct = '".$page_direct."';
";

switch($object_type) {
	case "FINANCE":
		$myObject = new IDP2_FINANCE($child_object_id);
		break;
	case "CONTRACT":
		$myObject = new IDP2_CONTRACT();
		break;
	case "DELIVERABLE":
		$myObject = new IDP2_DELIVERABLE();
		break;
	case "ACTION":
	default:
		$myObject = new IDP2_ACTION();
		break;
}
$options = array(
	'type'=>$page_type,
	'section'=>$page_section,
	'page'=>$page_action,
);
//$helper->arrPrint($options);

$js.=$displayObject->drawListTable($myObject->getObject($options),array('value'=>$button_label,'class'=>"btn_list"),$object_type,$options);


if($object_type!="FINANCE") {
	$js.="
		$(\"input:button.btn_list\").click(function() {
			//var r = $(this).attr(\"ref\");
			//document.location.href = page_direct+r;
			tableButtonClick($(this));
		});
		";
} else {
	$js.="
		$(\"input:button.btn_list\").click(function() {
			var r = $(this).attr(\"ref\");
			$('<div />',{html:\"Editing \"+r}).dialog();
		});
	";
}


?>
<script type=text/javascript>
var url = "<?php echo $page_direct; ?>";
var section = "<?php echo $object_type; ?>";
	$(function() {
		<?php echo $js; ?>
	});
	function tableButtonClick($me) {
		<?php 
		if(isset($alternative_button_click_function) && $alternative_button_click_function!==false && strlen($alternative_button_click_function)>0) {
			echo $alternative_button_click_function."($"."me);";
		} else {
			echo "
			if(section!=\"FINANCE\") {
				var r = $"."me.attr(\"ref\");
				document.location.href = url+r;
			} else {
				alert(\"UNDER DEVELOPMENT\");
			}";
		}
		?>
		
	}
</script>