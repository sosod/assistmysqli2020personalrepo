<?php
//require_once("inc_header.php");
//ASSIST_HELPER::arrPrint($_REQUEST);

/*****************
 * Plan:
 * 
 * + Get object id from REQUEST
 * + Get parent details and populate H1_page_heading with IDP details
 * + Draw tree view
 * 
 * object/contract = STRATOBJ
 * parent = IDP
 * child/deliverable = STRATFOCUS
 * child's child/sub-deliverable = STRATGOAL
 * child's child's child/action = STRATRESULT
 * 
 */


if(!isset($can_i_add)) {
	$can_i_add = true;
}

if(!isset($can_i_edit)) {
	$can_i_edit = true;
}

if(!isset($can_i_view)) {
	$can_i_view = true;
}

if(!isset($do_echo_js)) {
	$do_echo_js = true;
}

/*function getTierLevel($object_type) {
	switch($object_type) {
		case "STRATOBJ": return "tier1"; break;
		case "STRATFOCUS": return "tier2"; break;
		case "STRATGOAL": return "tier3"; break;
		case "STRATRESULT": 
		default: 
			return "tier4"; break;
	}
}*/


$stratplan_can_confirm = true;

$view_actions = array("CONFIRM","VIEW");

/**
 * Set Tier objects
 */
if(!isset($idpObject)) {
	$idpObject = new IDP2_IDP();
}
//TIER1 = STRATOBJ
$t1Object = new IDP2_STRATOBJ();
$object_type = $t1Object->getMyObjectType();
$object_name = $t1Object->getMyObjectName();
$t1_object_type = $object_type;
$t1_object_name = $object_name;
//$t1_id = $child_object_id;
$t1_object_id = $child_object_id;
if(!isset($t1_object_details)) {
	$t1_object_details = $t1Object->getSimpleDetails($t1_object_id);
	if(!is_array($t1_object_id)) {
		$t1_object_details = array(
			$t1_object_id=>$t1_object_details
		);
	}
}
if(count($t1_object_details)==0) { $stratplan_can_confirm = false; }
if(!isset($idp_object_id)) {
	$idp_object_id = $t1_object_details['parent_id'];
}


//TIER2 = STRATFOCUS
$t2Object = new IDP2_STRATFOCUS();
$t2_object_type = $t2Object->getMyObjectType();
$t2_object_name = $t2Object->getMyObjectName();
$t2_objects = $t2Object->getOrderedObjects(0,$t1_object_id); 
//$t2_objects = isset($t2_objects[$t1_object_id]) ? $t2_objects[$t1_object_id] : array();

//TIER3 = STRATGOAL
$t3Object = new IDP2_STRATGOAL();
$t3_object_type = $t3Object->getMyObjectType();
$t3_object_name = $t3Object->getMyObjectName();
$t3_objects = $t3Object->getOrderedObjects(0,$t1_object_id);
//ASSIST_HELPER::arrPrint($t3_objects);

//TIER4 = STRATFOCUS
$t4Object = new IDP2_STRATRESULT();
$t4_object_type = $t4Object->getMyObjectType();
$t4_object_name = $t4Object->getMyObjectName();
$t4_objects = $t4Object->getOrderedObjects(0,$t1_object_id);
 
//HEADER

echo "
<table class='tbl-container'><tr><td>
<table class=tbl-tree>
	<tr>
		<td class=uni-pos></td>
		<td></td>
	</tr>";
foreach($t1_object_details as $t1_id => $t1_details) {

		//TIER1 DETAILS
		$count = count(isset($t2_objects[$t1_id]) ? $t2_objects[$t1_id] : array());
		$stratplan_can_confirm = $count>0 ? $stratplan_can_confirm : false;
		$count = ($count>0 ? $count." ".$helper->getObjectName($t2_object_name,($count!=1)) : "<span class=required>0 ".$helper->getObjectName($t2_object_name,true)."</span>");
		//$t1_details['name'].= strlen($t1_details['ref'])>0 ? " (".$t1_details['ref'].")" : "";
		$displayObject->drawTreeTier("tier1",1,$t1_details,$page_action,"stratplan-btn",$can_i_view,$can_i_edit,(isset($t2_objects[$t1_id]) ? $count : ""));
		if(!in_array($page_action,$view_actions)) {
			echo "
				<tr class='sub_detail_row'>
					<td class='uni-pos' object_id=0 object_type=".$t2_object_type.">
						".($can_i_add ? "<button class='action-button stratplan-btn add-btn' parent_id=".$t1_id.">Add ".$helper->getObjectName($t2_object_name)."</button>" : "")."
						&nbsp;<span class='count float'>".$count."</span></td>
					<td></td>
				</tr>
			";
		}
				//TIER2
				if(isset($t2_objects[$t1_id])) {
					foreach($t2_objects[$t1_id] as $t2_id => $t2_obj) {
							$count = (isset($t3_objects[$t2_id]) ? count($t3_objects[$t2_id]) : 0);
							$stratplan_can_confirm = $count>0 ? $stratplan_can_confirm : false;
							$count = ($count==0 ? "<span class=required>" : "").$count." ".$helper->getObjectName($t3_object_name,$count!=1).($count==0 ? "</span>" : "");
						$displayObject->drawTreeTier("tier2",2,$t2_obj,$page_action,"stratplan-btn",$can_i_view,$can_i_edit,$count);
							if(!in_array($page_action,$view_actions)) {
								echo "
								<tr class='sub_detail_row'>
									<td></td>
									<td class=bi-pos object_id=0 object_type=".$t3_object_type.">
										".($can_i_add ? "<button class='action-button stratplan-btn add-btn' parent_id='".$t2_id."' >Add ".$helper->getObjectName($t3_object_name)."</button>" : "")."
										&nbsp;<span class='float count'>$count</span></td>
									<td></td>
								</tr>
								";
							}
							if(isset($t3_objects[$t2_id])) {
								foreach($t3_objects[$t2_id] as $t3_id => $t3_obj) {
									$count = (isset($t4_objects[$t3_id]) ? count($t4_objects[$t3_id]) : 0);
									$stratplan_can_confirm = $count>0 ? $stratplan_can_confirm : false;
									$count = ($count==0 ? "<span class=required>" : "").$count." ".$helper->getObjectName($t4_object_name,$count!=1).($count==0 ? "</span>" : "");
									$displayObject->drawTreeTier("tier3",3,$t3_obj,$page_action,"stratplan-btn",$can_i_view,$can_i_edit,$count);
									if(!in_array($page_action,$view_actions)) {
										echo "
										<tr class='sub_detail_row'>
											<td></td>
											<td></td>
											<td class=tri-pos object_id=0 object_type=".$t4_object_type.">
												".($can_i_add ? "<button class='action-button stratplan-btn add-btn' parent_id=".$t3_id.">Add ".$helper->getObjectName($t4_object_name)."</button>" : "")."
												&nbsp;<span class='float count'>$count</span>
											</td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										";
									}
									if(isset($t4_objects[$t3_id])) {
										foreach($t4_objects[$t3_id] as $t4_id => $t4_obj){
											$displayObject->drawTreeTier("tier4",4,$t4_obj,$page_action,"stratplan-btn",$can_i_view,$can_i_edit,"");
										}
									}
								}
							}
					}
				} /*else {//if isset t2 objects t1
					echo "
						<tr>
							<td class=uni-pos object_id=0 object_type=".$t2_object_type.">
								".($can_i_add ? "<button class='action-button stratplan-btn add-btn' parent_id=".$t1_id.">Add ".$helper->getObjectName($t2_object_name)."</button>" : "")."
								&nbsp;<span class='count float'>".$count."</span></td>
							<td></td>
						</tr>
					";
				}*/




} //endforeach t1_object_details
?>	
	
</table>
</td></tr><tr><td>
	<div id=confirmation_status>
	
</div>

</td></tr>
<tr><td>
	
<?php
//$footer = $displayObject->getPageFooter($helper->getGoBack("new_contract_edit.php"));
//echo $footer['display'];
//$js.=$footer['js'];
?>

</td></tr>
</table>

<?php

/*******************
 * PARENT
 */
if($can_i_edit) {
	
$view_object_type = $t1_object_type;
$view_object_name = $t1_object_name;
$view_object_id = $t1_object_details[$t1_object_id]['id'];
$view_object = $t1_object_details;
$viewObject = $t1Object;
$parentObject = null;
$parent_object_id = 0;
$parent_object_type = "";
?>
<div id=stratplan_dlg_edit_parent class=dlg-child title="Edit <?php echo $helper->getObjectName($t1_object_name); ?>">
	<span class=float><button class=close-btn id=edit_parent>Close</button></span>
	<h2><?php echo $helper->getObjectName($view_object_name); ?> Details</h2><?php 
	$data = $displayObject->getObjectForm($view_object_type, $viewObject, $view_object_id, $parent_object_type, $parentObject, $parent_object_id, $page_action, $page_redirect_path);
	echo $data['display'];
	$js.=$data['js'];
	?>
</div>
<?php
}

?>



<div id=stratplan_dlg_child class=dlg-child title="">
		<iframe id=stratplan_ifr_form_display style="border:0px solid #000000;" src="">
			
		</iframe>
</div>


<script type=text/javascript>
$(function() {
	<?php 	
	if($do_echo_js) {
		echo $js;	
	}
	
	?>
	
	
	
	
	var stratplan_object_names = [];
	stratplan_object_names['tier1'] = "<?php echo $helper->getObjectName($t1_object_name); ?>";
	stratplan_object_names['<?php echo $t1_object_type; ?>'] = "<?php echo $helper->getObjectName($t1_object_name); ?>";
	stratplan_object_names['tier2'] = "<?php echo $helper->getObjectName($t2_object_name); ?>";
	stratplan_object_names['<?php echo $t2_object_type; ?>'] = "<?php echo $helper->getObjectName($t2_object_name); ?>";
	stratplan_object_names['tier3'] = "<?php echo $helper->getObjectName($t3_object_name); ?>";
	stratplan_object_names['<?php echo $t3_object_type; ?>'] = "<?php echo $helper->getObjectName($t3_object_name); ?>";
	stratplan_object_names['tier4'] = "<?php echo $helper->getObjectName($t4_object_name); ?>";
	stratplan_object_names['<?php echo $t4_object_type; ?>'] = "<?php echo $helper->getObjectName($t4_object_name); ?>";
	
	var stratplan_object_types = [];
	stratplan_object_types['tier1'] = "<?php echo $t1_object_type; ?>";
	stratplan_object_types['tier2'] = "<?php echo $t2_object_type; ?>";
	stratplan_object_types['tier3'] = "<?php echo $t3_object_type; ?>";
	stratplan_object_types['tier4'] = "<?php echo $t4_object_type; ?>";
	
	
	
	
	//Formatting - not object_type specific
	
	var my_window = AssistHelper.getWindowSize();
	//alert(my_window['height']);
	if(my_window['width']>800) {
		var my_width = 850;
	} else {
		var my_width = 800;
	}
	var my_height = my_window['height']-50;	

	
	$("table.tbl-tree").find("td.tree-icon").addClass("right").prop("width","20px");
	//Set the column spanning for each object description cell based on their position in the hierarchy but excluding the last two columns
	$("table.tbl-tree").find("td.cate-pos, td.td-line-break").prop("colspan","7");
	$("table.tbl-tree").find("td.uni-pos").not("td.center").prop("colspan","4");
	$("table.tbl-tree").find("td.bi-pos").not("td.center").prop("colspan","3");
	$("table.tbl-tree").find("td.tri-pos").not("td.center").prop("colspan","2");
	$("table.tbl-tree").find("td.quad-pos").not("td.center").prop("colspan","1");
	
	//Action positional buttons
	$("button.sub-btn").button({
		icons: {primary: "ui-icon-carat-1-sw"},
		text: false
	}).css("margin-right","-10px").removeClass("ui-state-default").addClass("ui-state-icon-button ui-state-icon-button-black")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"0px","padding-right":"0px"});
	//Parent positional buttons
	$("button.expand-btn").button({
		icons: {primary: "ui-icon-triangle-1-se"},
		text: false
	}).css("margin-right","-10px").removeClass("ui-state-default").addClass("ui-state-icon-button ui-state-icon-button-black")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"0px","padding-right":"0px"});
	
	//Edit button
	$("button.edit-btn").button({
		icons: {primary: "ui-icon-pencil"},
	}).removeClass("ui-state-default").addClass("ui-button-state-grey").css({"color":"#999","border":"1px solid #ffffff"})
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px"});
/*			}).click(function() {
				$("#dlg_view").prop("title","Edit Object").html("<p>This is where the form to edit a new object will display.").dialog("open");
*/
	//View button
	$("button.view-btn").button({
		icons: {primary: "ui-icon-extlink"},
	}).removeClass("ui-state-default").addClass("ui-button-state-grey").css({"color":"#999","border":"1px solid #ffffff"})
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px"});
/*			}).click(function() {
				$("#dlg_view").prop("title","View Object").html("<p>This is where the full object details will display.").dialog("open");
*/
	//Add button
	$("button.add-btn").button({
		icons: {primary: "ui-icon-circle-plus"},
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css("color","#009900")
		.children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px","padding-left":"25px"});
/*			}).click(function() {
				$("#dlg_view").prop("title","View Object").html("<p>This is where the full object details will display.").dialog("open");
*/
	//Button styling and positioning
	$("button.ibutton:not(.sub-btn)").hover(
		function() {
			$(this).css({"border-width":"0px","background":"url()"}).removeClass("ui-state-hover");
		},
		function() {}
	);
	$("button.ibutton").each(function() {
		if($(this).prev().hasClass("ibutton")) {
			$(this).css("margin-left","-5px");
		}
		if($(this).next().hasClass("ibutton")) {
			$(this).css("margin-right","-5px");
		}
	});
	
	//Styling based on object type
	$("table.tbl-tree tr.grand-parent").css({"font-size":"125%","line-height":"150%","background-color":"#FFFFFF"}).find("button").css("font-size","65%");
	$("table.tbl-tree tr.sub_detail_row").css({"vertical-align":"bottom"}).find("button").css("font-size","90%");
	//$("table.tbl-tree tr.parent").css({"background-color":"#7C8DCD","color":"#fff"}).find("button.edit-btn, button.view-btn").css({"color":"#fff","border":"1px solid #7C8DCD"}).removeClass("ui-button-state-grey").addClass("ui-button-state-white");
	$("table.tbl-tree tr.parent").css("background-color","#7C8DCD").find("td.td-button").css("background-color","#FFFFFF");
	$("table.tbl-tree tr.sub-parent").css("background-color","#DEDEDE").find("td.td-button").css("background-color","#FFFFFF");
	
	$("table.tbl-tree tr.sub-parent, table.tbl-tree tr.grand-parent, table.tbl-tree tr.child, table.tbl-tree tr.parent").find("button.action-button").hover(
		function() {
			$(this).removeClass("ui-button-state-grey").addClass("ui-button-state-info").css({"color":"#fe9900","border":"1px solid #fe9900"});
		},
		function () {
			$(this).removeClass("ui-button-state-info").addClass("ui-button-state-grey").css({"color":"#999","border":"1px solid #ffffff"});
		}
	);
	
	/*
	$("table.tbl-tree tr.parent").find("button").hover(
		function() {
			$(this).removeClass("ui-button-state-white").addClass("ui-button-state-info").css({"color":"#fe9900","border":"1px solid #fe9900"});
		},
		function () {
			$(this).removeClass("ui-button-state-info").addClass("ui-button-state-white").css({"color":"#fff","border":"1px solid #7C8DCD"});
		}
	);*/

	//General styling
	$("span.count").css({"font-size":"80%","margin-top":"-3px"});
	$("table.tbl-tree th").css({"padding":"10px"});

	
	$("div button.close-btn").button({
		icons: { primary: "ui-icon-closethick" },
		text: false
	}).click(function(e) {
		e.preventDefault();
		$(this).parent("span").parent("div").dialog("close");
	}).removeClass("ui-state-default").addClass("ui-button-state-default");
	
	
	$("div.dlg-child").dialog({
		autoOpen: false,
		modal: true,
		width: my_width,
		height: my_height,
		beforeClose: function() {
			AssistHelper.closeProcessing();
		},
		open: function() {
			$(this).dialog('option','width',my_width);
			$(this).dialog('option','height',my_height);
		}
	});	
	
	
	
	
	/**
	 *Object Type specific - attach section type (STRATPLAN) to each object to differentiate from other trees 
	 */
	
	
	$("button.action-button.stratplan-btn").click(function() {
		AssistHelper.processing();
		var i = $(this).parent("td").attr("object_id");
		var t = $(this).parent("td").attr("object_type");
		if(!$(this).hasClass("add-btn")) {
			t = stratplan_object_types[t];
		}
		var dta = "object_type="+t+"&object_id="+i;
//		AssistHelper.hideProcessing();
		//if(t==stratplan_object_types['tier1'] && !$(this).hasClass("view-btn")) {
		//	$("div#stratplan_dlg_edit_contract").dialog("open");
		//	$("div#stratplan_dlg_edit_contract button.close-btn").blur();
		//} else {
			$dlg = $("div#stratplan_dlg_child");
			var act = "view";
			if($(this).hasClass("edit-btn")) {
				act = "edit";
			} else if($(this).hasClass("add-btn")) {
				act = "add";
			}
			var obj = t.toLowerCase();
			//var obj = "deliverable";
			//if(t=="contract" || t=="action") { obj = t; }
			//var obj = (t=="action" ? "action" : "deliverable");
			var heading = AssistString.ucwords(act)+" "+stratplan_object_names[t];
			var parent_id = $(this).attr("parent_id"); 
			var url = "<?php echo $dialog_url; ?>?display_type=dialog&page_action="+act+"&object_type="+t+"&object_id="+parent_id;
			console.log(url);
			$dlg.dialog("option","title",heading);
			if(act=="view") {
				$dlg.dialog("option","buttons",[ { text: "Close", click: function() { $( this ).dialog( "close" ); } } ]);
			}
			//$dlg.find("iframe").prop("src",url);
			$dlg.find("iframe").prop("width",(my_width-50)+"px").prop("height",(my_height-50)+"px").prop("src",url);
			//FOR DEVELOPMENT PURPOSES ONLY - DIALOG SHOULD BE OPENED BY PAGE INSIDE IFRAME ONCE IFRAME FULLY LOADED
			//$dlg.dialog("open");
		//}
	});
	
	
	//AssistHelper.hideDialogTitlebar("class","dlg-view");
	
	
	
});


function dialogFinished(icon,result) {
	if(icon=="ok") {
		document.location.href = '<?php echo $page_redirect_path."object_id=".$t1_id; ?>&r[]=ok&r[]='+result;
	} else {
		AssistHelper.processing();
		AssistHelper.finishedProcessing(icon,result);
	}
}

</script>

