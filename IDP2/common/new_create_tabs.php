<?php
$page_action = "EDIT";
$page_section = "NEW";
$button_label = "|build|";
$button_icon = "circlesmall-plus";



$add_button = true;
$add_button_label = "|add| |".$childObject->getMyObjectName()."|"; //echo $add_button_label;
$add_button_function = "showAddDialog();";

//$page_direct = "new_create_stratplan.php";
$page_direct = "new_create_idp.php?idp_object_id=".$idp_object_id."&tab=".$my_tab."&tab_act=tree&";
//echo "<h2>".$page_direct."</h2>";
//echo $idp_object_id;
$options = array(
	$childObject->getParentFieldName()=>$idp_object_id
);
include("common/generic_list_page.php");


?>


<div id=dlg_add_idp title="<?php echo $helper->replaceAllNames($add_button_label); ?>">
	<?php
	
	$section = "NEW";
	$page_redirect_path = "new_create_idp.php?object_id=".$object_id."&tab=".$my_tab."&";
	$page_action = "Add";
	
	$uaObject = new IDP2_USERACCESS();
	
	$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, $parent_object_type, null, $idp_object_id, $page_action, $page_redirect_path);
	echo $data['display'];

?>
</div>
<script type="text/javascript">
$(function () {
	$("#dlg_add_idp").dialog({
		autoOpen: false,
		modal: true
	});
	<?php echo $data['js']; ?>
	
});
function showAddDialog() {
	$(function() {
		$("#dlg_add_idp").dialog("open");
		var my_window = AssistHelper.getWindowSize();
		if(my_window['width']>700) {
			$("#dlg_add_idp table.form").css("width","700px");
			$("#dlg_add_idp").dialog("option","width",750);
		} else {
			$("#dlg_add_idp table.form").css("width",(my_window['width']-100)+"px");
			$("#dlg_add_idp").dialog("option","width",(my_window['width']-50));
		}
		if((parseInt(AssistString.substr($("#dlg_add_idp table.form").css("height"),0,-2)))>my_window['height']-50) {
			$("#dlg_add_idp").dialog("option","height",my_window['height']-50);
		}
	});
}
</script>