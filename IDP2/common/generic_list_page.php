<?php
/*******
 * Pre-requisites:
 * $page_section = MANAGE || ADMIN
 * $page_action = UPDATE || EDIT
 */

if(!isset($_REQUEST['object_type'])) {
	if($page_action=="MANAGE") {
		$_REQUEST['object_type'] = "IDP";
	} else {
		$_REQUEST['object_type'] = "IDP";
	}
}
$object_type = $_REQUEST['object_type'];
$my_page = strtolower($_REQUEST['object_type']);

require_once("inc_header.php");


ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());





$page_type = "LIST";
if(!isset($button_label) || strlen($button_label)==0) {
	$button_label = $helper->getActivityName("open");
} elseif($button_label=="breadcrumbs") {
	$button_label = implode(" ",$menu['breadcrumbs']);
} elseif(substr($button_label,0,1)=="|" && substr($button_label,-1)=="|") {
	$button_label2 = $helper->getActivityName(substr($button_label,1,-1));
	if(strlen($button_label2)==0 || $button_label2==false) {
		$button_label = $helper->getObjectName(substr($button_label,1,-1));
	} else {
		$button_label = $button_label2;
	}
}

if($page_section=="NEW") {
	$page_action.= ($page_action!="CONFIRM" && substr($page_action,0,8)!="ACTIVATE" ? "_".$child_object_type : "");
} elseif($page_action!="VIEW"){
	$page_action.= "_".$object_type;
} 
if(isset($page_direct)) {
	$page_direct = strtolower($page_direct).(substr($page_direct,-1)!="&"?"?":"")."object_id=";
} elseif($page_section!="NEW") {
	$page_direct = strtolower($page_section)."_".strtolower($page_action)."_object.php?object_id=";
} else {
	$pa = substr($page_action,0,strpos($page_action,"_"));
	$page_direct = strtolower($page_section)."_".strtolower($object_type)."_".strtolower($pa)."_object.php?object_id=";
}

$js.= " var page_direct = '".$page_direct."';
";

$class_name = "IDP2_".$object_type;
$myObject = new $class_name();
if(isset($options)) {
	$options = array(
		'type'=>$page_type,
		'section'=>$page_section,
		'page'=>$page_action,
	)+$options;
} else {
	$options = array(
		'type'=>$page_type,
		'section'=>$page_section,
		'page'=>$page_action,
	);
}

$button = array('value'=>$button_label,'class'=>"btn_list",'type'=>"button");
if(isset($button_icon)) {
	$button['icon'] = $button_icon;
}
//ASSIST_HELPER::arrPrint($options);

$js.=$displayObject->drawListTable($myObject->getObject($options),$button,$object_type,$options,false,array( (isset($add_button) ? $add_button : false), (isset($add_button_label) ? $add_button_label : ""), (isset($add_button_function) ? $add_button_function : "")));


	$js.="
		$(\"button.btn_list\").click(function() {
			//var r = $(this).attr(\"ref\");
			//document.location.href = page_direct+r;
			tableButtonClick($(this));
		});
		";


?>
<script type=text/javascript>
var url = "<?php echo $page_direct; ?>";
var section = "<?php echo $object_type; ?>";
	$(function() {
		<?php echo $js; ?>
	});
	function tableButtonClick($me) {
		<?php 
		if(isset($alternative_button_click_function) && $alternative_button_click_function!==false && strlen($alternative_button_click_function)>0) {
			echo $alternative_button_click_function."($"."me);";
		} else {
			echo "
			AssistHelper.processing();
				var r = $"."me.attr(\"ref\");
				document.location.href = url+r;
				";
		}
		?>
		
	}
</script>