<?php
//include("inc_ignite.php");
$page = array("setup","access");
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('url'=>"setup_access.php",'txt'=>"User Access"),
	array('txt'=>"Edit"),
);
require_once "inc_header.php";



//GET EDIT VARIABLES
$result = array();
$aid = $_REQUEST['id'];
$type = isset($_REQUEST['t']) ? $_REQUEST['t'] : "form";
$access = isset($_REQUEST['a']) ?$_REQUEST['a'] : 0;
$setupObject = new PDP2_SETUP();


switch($type) {
	case "edit":
		$result = $setupObject->editUserAccess($aid,$access);
		break;
}

?>
<script language=JavaScript>
function editUser() {
    var acc = document.edit.access.value;
    document.location.href = "setup_access_edit.php?id=<?php echo $aid; ?>&t=edit&a="+acc;
}
</script>
<?php
ASSIST_HELPER::displayResult($result);
//IF NO ACTION WAS TAKEN THEN DISPLAY EDIT FORM
if(count($result) == 0 || (isset($result[0]) && $result[0]!="ok")) {
	$user_access_options = $setupObject->getUserAccessOptions();
	$data = $setupObject->getUserAccessSettingsByID($aid);
	$name = $setupObject->getAUserName($data['tkid']);
	$data['user_name'] = $name;
	?>
	<form name=edit>
		<table>
			<tr>
				<th>ID</td>
				<th>User</td>
				<th>Access</td>
			</tr>
			<tr>
				<td><?php echo($data['tkid']); ?></td>
				<td><?php echo($data['user_name']); ?></td>
				<td>Add/Update/View&nbsp;
					<select name=access><?php
						foreach($user_access_options as $access => $option_text) {
							echo "<option value=".$access." ".($data['act'] == $access ? "selected" : "")." >".$option_text."</option>";
						}
					?></select>
				</td>
			</tr>
			<tr>
				<td colspan=3>
					<input type=button value="Save Changes" class=isubmit onclick="editUser()">
				</td>
			</tr>
		</table>
	</form>
<?php
} elseif(isset($result[0]) && $result[0]=="ok") {
	?>
	<script type="text/javascript">
		document.location.href = "setup_access.php?r[]=<?php echo $result[0]; ?>&r[]=<?php echo urlencode($result[1]); ?>";
	</script>
	<?php
}
?>
</body>
</html>