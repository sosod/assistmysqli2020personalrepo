<?php

class PDP2 extends ASSIST_MODULE_HELPER {

	protected $headings = array();
	protected $actname = "Action";
	protected $my_modref;

    /**
     * Module Wide Constants
     * */
    const SYSTEM_DEFAULT = 1;
    const ACTIVE = 2;
    const INACTIVE = 4;
    const DELETED = 8;

    const AWAITING_APPROVAL = 16;
    const APPROVED = 128;

	public function __construct() {
		parent::__construct();
		$this->my_modref = $this->getModRef();

		$this->headings['action'] = array('taskadddate'   => "Created On",
										  'taskadduser'   => "Assigned By",
										  'tasktkid'      => "Assigned To",
										  'tasktopicid'   => "Topic",
										  'taskurgencyid' => "Priority",
										  'taskstatusid'  => "Status",
										  'taskdeadline'  => "Completion Date (Deadline)",
										  'taskaction'    => "Skill required / Performance Gap",
										  'taskdeliver'   => "Outcome Expected (What will I achieve)",
										  'taskattach'    => "Attachment Details");
		$this->headings['short_action'] = array('taskadddate'   => "Created On",
												'taskadduser'   => "Assigned By",
												'tasktkid'      => "Assigned To",
												'tasktopicid'   => "Topic",
												'taskurgencyid' => "Priority",
												'taskstatusid'  => "Status",
												'taskdeadline'  => "Deadline",
												'taskaction'    => "Skill required",
												'taskdeliver'   => "Outcome Expected",
												'taskattach'    => "Attachment");
		$this->headings['update'] = array('logdate'     => "Date logged",
										  'logtkid'     => "Logged By",
										  'logupdate'   => "Message",
										  'logstatusid' => "Status",
										  'logactdate'  => "Date of Activity",
										  'logattach'   => "Attachment details",);
	}


	/*************************************************
	 * GET functions
	 */
	public function getActionName($plural=false) {
		return $this->actname.($plural?"s":"");
	}
	public function getRefTag() { return $this->getModRef()."/"; }

	/****************************************************
	 * LIST FUNCTIONS
	 */

	private function getListItemsFormattedForSelect($sql, $key_field = "id", $value_field = "value") {
		return $this->mysql_fetch_value_by_id($sql, $key_field, $value_field);
	}

	public function getAllAvailableUsersToReceiveTasksFormattedForSelect() {
		$sql = "SELECT DISTINCT t.tkid as id, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
									FROM assist_".$this->getCmpCode()."_timekeep t
									INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users mmu
									  ON t.tkid = mmu.usrtkid AND mmu.usrmodref = '".strtoupper($this->getModRef())."'
									LEFT OUTER JOIN ".$this->getDBRef()."_list_access a
									  ON t.tkid = a.tkid AND a.yn = 'Y'
									WHERE t.tkid <> '0000' AND t.tkuser <> 'support' AND t.tkstatus = 1
									ORDER BY t.tkname, t.tksurname";
		return $this->getListItemsFormattedForSelect($sql, "id", "name");
	}

	public function getAllAvailableUsersToAddTasksFormattedForSelect() {
		$sql = "SELECT DISTINCT t.tkid as id, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
									FROM assist_".$this->getCmpCode()."_timekeep t
									INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users mmu
									  ON t.tkid = mmu.usrtkid AND mmu.usrmodref = '".strtoupper($this->getModRef())."'
									LEFT OUTER JOIN ".$this->getDBRef()."_list_access a
									  ON t.tkid = a.tkid AND a.yn = 'Y'
									WHERE t.tkid <> '0000' AND t.tkstatus = 1 AND a.view > 20
									ORDER BY t.tkname, t.tksurname";
		return $this->getListItemsFormattedForSelect($sql, "id", "name");
	}

	public function getAllActiveTopicsFormattedForSelect() {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_topic WHERE yn = 'Y' ORDER BY value";
		return $this->getListItemsFormattedForSelect($sql);
	}

	public function getAllActiveUrgencyFormattedForSelect() {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_urgency WHERE yn = 'Y' ORDER BY sort,value";
		return $this->getListItemsFormattedForSelect($sql);
	}

	public function getAllActiveStatusesFormattedForSelect() {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_status WHERE yn = 'Y' ORDER BY sort";
		return $this->getListItemsFormattedForSelect($sql, "pkey");
	}

    public function getAllActiveManagersFormattedForSelect() {
        $sql = "SELECT * FROM ".$this->getDBRef()."_list_manager WHERE yn = 'Y' ORDER BY value";
        return $this->getListItemsFormattedForSelect($sql);
    }
    public function getAllActiveDeliveryModesFormattedForSelect() {
        $sql = "SELECT * FROM ".$this->getDBRef()."_list_delivery_mode WHERE yn = 'Y' ORDER BY value";
        return $this->getListItemsFormattedForSelect($sql);
    }

    public function getAllActiveTimeFramesFormattedForSelect() {
        $sql = "SELECT * FROM ".$this->getDBRef()."_list_time_frame WHERE yn = 'Y' ORDER BY value";
        return $this->getListItemsFormattedForSelect($sql);
    }

    public function getAllActiveSupportPersonsFormattedForSelect() {
        $sql = "SELECT * FROM ".$this->getDBRef()."_list_support_person WHERE yn = 'Y' ORDER BY value";
        return $this->getListItemsFormattedForSelect($sql);
    }

    public function getAllActiveCompetenciesFormattedForSelect() {
        $sql = "SELECT * FROM ".$this->getDBRef()."_list_competency WHERE yn = 'Y' ORDER BY value";
        return $this->getListItemsFormattedForSelect($sql);
    }

    public function getAllActiveConfirmFormattedForSelect() {
        $sql = "SELECT * FROM ".$this->getDBRef()."_list_confirm WHERE yn = 'Y' ORDER BY value";
        return $this->getListItemsFormattedForSelect($sql);
    }

	public function getAllActiveStatusesForView() {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_status WHERE yn = 'Y' AND state >=0 ORDER BY sort";
		return $this->getListItemsFormattedForSelect($sql, "pkey");
	}

	protected function getAListItemName($table_name,$filter,$field="value",$id_field="id") {
		$sql = "SELECT $field FROM ".$this->getDBRef()."_".$table_name." WHERE ".$id_field." = ".$filter;
		return $this->mysql_fetch_one_value($sql,$field);
	}

	public function getATopicNameByID($object_id) {
		return $this->getAListItemName("list_topic",$object_id);
	}
	public function getAUrgencyNameByID($object_id) {
		return $this->getAListItemName("list_urgency",$object_id);
	}
	public function getAStatusNameByID($object_id) {
		return $this->getAListItemName("list_status",$object_id,"value","pkey");
	}
	/*************************************************
	 * UDF functions
	 */

	public function getUDFDetails() {
		$sql = "SELECT udfiid as id
				, udfivalue as udf_value
				, udfilist as list_type
				, udfiref as ref
				, udficustom as custom
				, udfisort as sort
				, udfiyn as yn
				, udfirequired as required
				, udfidefault as default_setting
				, udfiobject as udf_object
				, udfilinkfield as link_field
				, udfilinkref as link_ref
				FROM assist_".$this->getCmpCode()."_udfindex 
				WHERE udfiref = '".$this->getModRef()."' AND udfiyn = 'Y' AND (udfiobject = 'action' OR udfiobject = '') 
				ORDER BY udfisort, udfivalue";
		return $this->mysql_fetch_all_by_id($sql, "id");
	}

	public function getUDFListItems($udf_id) {
		$sql = "SELECT udfvid as id
				, udfvindex as udf_index
				, udfvvalue as value 
				, udfvcode as code 
				, udfvcomment as comment
				, udfvyn as yn
				FROM assist_".$this->getCmpCode()."_udfvalue 
				WHERE udfvindex = ".$udf_id." 
				AND udfvyn = 'Y' 
				ORDER BY udfvvalue";
		return $this->mysql_fetch_all_by_id($sql, "id");
	}

	public function getAllUDFListItems($type="action") {
		if($type=="action") {
			$filter = "i.udfiobject = 'action' OR i.udfiobject = ''";
		} else {
			$filter = "i.udfiobject = '$type'";
		}
		$sql = "SELECT  udfvid as id
				, udfvindex as udf_index
				, udfvvalue as value 
				, udfvcode as code 
				, udfvcomment as comment
				, udfvyn as yn
				FROM assist_".$this->getCmpCode()."_udfvalue v
				INNER JOIN assist_".$this->getCmpCode()."_udfindex i
				ON i.udfiid = v.udfvindex
				WHERE v.udfvyn = 'Y' AND i.udfiref = '".$this->getModRef()."' AND i.udfiyn = 'Y' AND ($filter) 
				ORDER BY v.udfvvalue";
		$rows = $this->mysql_fetch_all($sql);
		$data = array();
		foreach($rows as $row) {
			$data[$row['udf_index']][$row['id']] = $row['value'];
		}
		return $data;
	}
















	/****************************************************
	 * MODULE LOADING FUNCTIONS
	 */


	/**
	 * Function to get the user id of the main administrator of the module - old style module administrator functionality
	 */
	public function getAdministratorUserID() {
		$sql = "SELECT value FROM assist_".$this->getCmpCode()."_setup WHERE ref = '".$this->getModRef()."' AND refid = 0 ORDER BY id DESC LIMIT 1";
		$row = $this->mysql_fetch_one($sql);
		if(is_array($row) && isset($row['value']) && !is_null($row['value']) && strlen($row['value']) > 0) {
			$taadmin = $row['value'];
		} else {
			$taadmin = "0000";
		}
		return $taadmin;
	}

	/**
	 * Function to get current user's user access permissions - old style user access functionality
	 */
	public function getMyUserAccess() {
		if(!isset($_SESSION[$this->getModRef()]['my_user_access']['expiration']) || $_SESSION[$this->getModRef()]['my_user_access']['expiration'] < time()) {
			$row = $this->getUserAccess($this->getUserID());
			$_SESSION[$this->getModRef()]['my_user_access']['expiration'] = time()+(10*60);	//expires in 10 minutes
			$_SESSION[$this->getModRef()]['my_user_access']['row'] = $row;
		} else {
			$row = $_SESSION[$this->getModRef()]['my_user_access']['row'];
		}
		if(is_array($row) && isset($row['act']) && !is_null($row['act']) && strlen($row['act']) > 0) {
			return $row['act'];
		} else {
			return 20;    //20 is the minimum value - only create tasks to myself & see my own tasks
		}
	}
	/**
	 * Function to get any user's access
	 */
	public function getUserAccess($user_id=false) {
		if($user_id===false) {
			$user_id = $this->getUserID();
		}
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_access WHERE tkid = '".$user_id."' AND yn = 'Y' ORDER BY id DESC LIMIT 1";
		$row = $this->mysql_fetch_one($sql);
		if(is_array($row) && count($row)>0) {
			return $row;
		} else {
			return array('id'=>false,'act'=>20,'view'=>20);
		}
	}
	/**
	 * Get full headings
	 */
	public function getFullHeadings() {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_display WHERE yn = 'Y' AND type = 'S' AND field <> 'taskadddate'";
		$rows = $this->mysql_fetch_all_by_id($sql, "field");
		foreach($rows as $field => $row) {
			if(isset($this->headings['action'][$field])) {
				$rows[$field]['headingfull'] = $this->headings['action'][$field];
				$rows[$field]['headingshort'] = $this->headings['short_action'][$field];
			} elseif(isset($this->headings['update'][$field])) {
				$rows[$field]['headingfull'] = $this->headings['update'][$field];
				$rows[$field]['headingshort'] = $this->headings['update'][$field];
			}
		}
		return $rows;
	}

	public function getHeadingNames() {
		return $this->headings;
	}





	/************************************************
	 * User Access functions
	 */
	/**
	 * @param int $user_access_level
	 * @return array
	 */
	public function getCountOfActions($user_access_level=20) {
		$actions=array();
		$dbref = $this->getDBRef();
		$tkid = $this->getUserID();
		//GET MY ACTIONS
		$sql = "SELECT s.pkey, count(t.taskid) as tcount
				FROM ".$dbref."_task t
				INNER JOIN ".$dbref."_list_status s
				  ON s.pkey = t.taskstatusid AND s.yn <> 'N'
				INNER JOIN ".$dbref."_task_recipients tr
				  ON tr.taskid = t.taskid 
				  AND tr.tasktkid = '".$tkid."'
				GROUP BY s.pkey";
		$actions['MY'] = $this->mysql_fetch_fld2_one($sql,"pkey","tcount");
		//GET MY ASSIGNED ACTIONS
		$sql = "SELECT s.pkey, count(t.taskid) as tcount
				FROM ".$dbref."_task t
				INNER JOIN ".$dbref."_list_status s
				  ON s.pkey = t.taskstatusid AND s.yn <> 'N'
				WHERE t.taskadduser = '".$tkid."'
				GROUP BY s.pkey";
		$actions['OWN'] = $this->mysql_fetch_fld2_one($sql,"pkey","tcount");
		//IF TACT > 20 GET ALL ACTIONS
		if($user_access_level>20) {
			$sql = "SELECT s.pkey, count(t.taskid) as tcount
					FROM ".$dbref."_task t
					INNER JOIN ".$dbref."_list_status s
					  ON s.pkey = t.taskstatusid AND s.yn <> 'N'
					GROUP BY s.pkey";
			$actions['ALL'] = $this->mysql_fetch_fld2_one($sql,"pkey","tcount");
		}
		return $actions;
	}














	/*************************************************
	 * EMAIL functions
	 */

	/**
	 * Function to send email out from module.  Subject & message must be generated else where
	 * @param $to
	 * @param $subject
	 * @param $message
	 * @param string $content_type
	 * @param string $reply_to
	 * @param string $cc
	 * @param string $bcc
	 */
	public function sendActionEmail($to,$subject,$message,$content_type="TEXT",$reply_to="",$cc="",$bcc="") {
		//Convert user IDs to recipient format - $to
		//If Reply_to is set then convert to recipient format
		$emailObject = new ASSIST_EMAIL();
		$emailObject->setRecipient($to);
		$emailObject->setSubject($subject);
		$emailObject->setBody($message);
		$emailObject->setCC($cc);
		$emailObject->setBCC($bcc);
		$emailObject->setSender($reply_to);
		$emailObject->setContentType($content_type);
		$emailObject->sendEmail();
	}


	/**
	 * Old Navigation function from /inc_assist.php
	 * @param $level
	 * @param $menu
	 */
	function echoNavigation($level,$menu) {
		/*** variables:
			$level = 1 or 2;
			$menu[id] = array('id'=>id,'url'=>path_to,'active'=>checked_or_not,'display'=>heading);
		***/
		$id = "m".$level;
		switch($level) {
		case 2:
			echo "<div id=m".$level." style=\"padding-top: 5px; font-size: 7pt\">";
			foreach($menu as $m) {
				$key = $m['id'];
				echo "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" "; if($m['active']=="Y") { echo "checked=checked"; } echo " />";
				echo "<label for=\"$key\" style=\"background: #fafaff url();margin-right: 3px;\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>";
			}
			echo "</div>";
			break;
		case 1:
		default:
			echo "<div id=m".$level." style=\"font-size: 8pt; font-weight: bold;\">";
			foreach($menu as $m) {
				$key = $m['id'];
				echo "<input type=\"radio\" id=\"$key\" name=\"radio".$id."\" value=\"".$m['url']."\" "; if($m['active']=="Y") { echo "checked=checked"; } echo " />";
				echo "<label for=\"$key\">&nbsp;&nbsp;".$m['display']."&nbsp;&nbsp;</label>&nbsp;&nbsp;";
			}
			echo "</div>";
			break;
		}
		echo "<script type=text/javascript>
			  $(function() {
				$(\"#m".$level."\").buttonset();
				$(\"#m".$level." input[type='radio']\").click( function() {
				  document.location.href = $(this).val();
				});
			  });
		</script>";
	}	//end drawNav($nav)


}


?>