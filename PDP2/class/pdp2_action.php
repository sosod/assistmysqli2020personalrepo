<?php


class PDP2_ACTION extends PDP2 {

    public $return_array = array();

	public function __construct() {
		parent::__construct();
	}

	public function addObject($var, $udf_index = array(),$send_email = true) {
		//Set data
		$insert_data = array();
		$tasktkid = $var['tasktkid'];
		$taskaction = $var['taskaction'];
		$taskdeliver = $var['taskdeliver'];
		$insert_data['taskurgencyid'] = $var['taskurgencyid'];
		$insert_data['tasktopicid'] = $var['tasktopicid'];
		$insert_data['taskaction'] = ASSIST_HELPER::code($var['taskaction']);
		$insert_data['taskdeliver'] = ASSIST_HELPER::code($var['taskdeliver']);
		$insert_data['taskstatusid'] = $var['taskstatusid'];
		$insert_data['taskadduser'] = $var['taskadduser'];
		$insert_data['taskstate'] = 0;
		$insert_data['taskdeadline'] = strtotime($var['taskdeadline']);

        $insert_data['taskmanagerid'] = $var['taskmanagerid'];
        $insert_data['taskdeliverymodeid'] = $var['taskdeliverymodeid'];
        $insert_data['tasktraining'] = ASSIST_HELPER::code($var['tasktraining']);
        $insert_data['tasktimeframeid'] = $var['tasktimeframeid'];
        $insert_data['taskworkopportunity'] = ASSIST_HELPER::code($var['taskworkopportunity']);
        $insert_data['tasksupportpersonid'] = $var['tasksupportpersonid'];
        $insert_data['taskcompetencyid'] = $var['taskcompetencyid'];

		$insert_data['taskadddate'] = time();
		$insert_data['taskadduser'] = $this->getUserID();

		//Insert main object record
		$sql = "INSERT INTO ".$this->getDBRef()."_task SET ".$this->convertArrayToSQLForSave($insert_data);
		$taskid = $this->db_insert($sql);

		//Add Recipients
		$this->addActionRecipients($taskid, $tasktkid);

		//Add log record
		$logupdate = "New ".strtolower($this->actname)." added.".chr(10).$this->actname." instructions: ".$taskaction.chr(10).$this->actname." deliverables: ".$taskdeliver;
		$sql = "INSERT INTO ".$this->getDBRef()."_log SET ";
		$sql .= "logdate = '".time()."', ";
		$sql .= "logtkid = '', ";
		$sql .= "logupdate = '".ASSIST_HELPER::code($logupdate)."', ";
		$sql .= "logstatusid = '".$insert_data['taskstatusid']."', ";
		$sql .= "logstate = ".$insert_data['taskstate'].", ";
		$sql .= "logemail = 'N', ";
		$sql .= "logsubmittkid = '".$insert_data['taskadduser']."', ";
		$sql .= "logtaskid = '".$taskid."', ";
		$sql .= "logtasktkid = '".(is_array($tasktkid)?implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$tasktkid):$tasktkid)."', logtype = 'C', logactdate  = ".time();
		$sqllog = $sql;
		$logid = $this->db_insert($sql);

		//Add UDF records
		$udfval = array();
		if(count($udf_index['action']['ids']) > 0) {
			foreach($udf_index['action']['ids'] as $udfi) {
				$udfval[$udfi] = ASSIST_HELPER::code($var[$udfi]);
			}
		}
		$udfs_for_emails = array();
		foreach($udf_index['action']['ids'] as $udfi) {
			if($udfval[$udfi] != "X") {
				$sql = "INSERT INTO assist_".$this->getCmpCode()."_udf SET
						udfindex = ".$udfi."
						, udfvalue = '".$udfval[$udfi]."'
						, udfnum = ".$taskid."
						, udfref = '".$this->getModRef()."'";
				$this->db_insert($sql);
				$udfs_for_emails[$udfi] = $udf_index['action']['index'][$udfi]['udfivalue'].": ".ASSIST_HELPER::decode($udfval[$udfi]);
			}
		}

		//only send email if item being added one by one - same as original task functionality - TBD is still applicable [JC] 10 May 2020
		if($send_email) {

			//Notify recipients
			$to = array();
			if(is_array($tasktkid) && count($tasktkid) == 1) {
				$tt = array_keys($tasktkid);
				$tasktkid = $tasktkid[$tt[0]];
			}
			if(!is_array($tasktkid)) {
				$tx = $this->getUserNameWithEmail($tasktkid);
				$to['name'] = $tx['tkn'];
				$to['email'] = $tx['email'];
			} else {
				foreach($tasktkid as $i => $tt) {
					$tx = $this->getUserNameWithEmail($tt);
					$to[$i]['name'] = $tx['tkn'];
					$to[$i]['email'] = $tx['email'];
				}
			}

			$reply_to = array('name' => $this->getUserName(), 'email' => $this->getUserEmail($this->getUserID()));
			$urgency = $this->getAUrgencyNameByID($insert_data['taskurgencyid']);
			$topic = $this->getATopicNameByID($insert_data['tasktopicid']);
			$message = $this->generateNewActionEmail($reply_to, $taskid, $urgency, $topic, $taskaction, $taskdeliver, $insert_data['taskdeadline'], $udfs_for_emails);
			$subject = ASSIST_HELPER::decode("New ".$_SESSION['modtext']." ".$this->actname." $taskid on Assist");
			$content_type = "TEXT";
			if($var['sendEmail']==true) {
				$cc = $reply_to;
			} else {
				$cc = "";
			}
			$bcc = "";
			$this->sendActionEmail($to, $subject, $message, $content_type, $reply_to, $cc, $bcc);
		}

		return $taskid;
	}


	public function addActionRecipients($taskid, $tasktkid) {
		$sql = "INSERT INTO ".$this->getDBRef()."_task_recipients (taskid,tasktkid) VALUES ";
		if(!is_array($tasktkid)) {
			$sql .= "($taskid,'$tasktkid')";
		} else {
			$sql .= "($taskid,'".implode("'),($taskid,'", $tasktkid)."')";
		}
		$this->db_insert($sql);
	}

	public function addObjectAttachment($object_id, $original_filename, $system_filename, $type = "action") {
		$sql = "INSERT INTO ".$this->getDBRef()."_task_attachments (taskid,logid,original_filename,system_filename,file_location) 
				VALUES (".($type == "action" ? $object_id : 0).",".($type != "action" ? $object_id : 0).",'".$original_filename."','".$system_filename."','".$type."')";
		$this->db_insert($sql);
	}


	public function getNextAttachmentID() {
		$sql = "SELECT id FROM ".$this->getDBRef()."_task_attachments ORDER BY id DESC LIMIT 1";
		$i = $this->mysql_fetch_one_value($sql, "id");
		if(ASSIST_HELPER::checkIntRef($i)) {
			return $i + 1;
		} else {
			return 1;
		}
	}


	/**
	 * @param $adduser
	 * @param $taskid
	 * @param $urgency
	 * @param $topic
	 * @param $taskaction
	 * @param $taskdeliver
	 * @param $taskdeadline
	 * @param $udfs
	 * @return string
	 */
	protected function generateNewActionEmail($adduser, $taskid, $urgency, $topic, $taskaction, $taskdeliver, $taskdeadline, $udfs) {
		$headings = $this->getHeadingNames();
		$message = $adduser['name']." has assigned a new ".$_SESSION['modtext']." ".strtolower($this->actname)." to you.\n";
		$message .= "Ref: ".$this->getRefTag().$taskid."\n";
		$message .= $headings['action']['taskurgencyid'].": ".$urgency."\n";
		$message .= $headings['action']['tasktopicid'].": ".$topic."\n";
		//html_entity_decode is just not working for single quotes in some clients like gmail
		$taskaction = preg_replace('/(&#39)/', "'", stripslashes($taskaction));
		$taskdeliver = preg_replace('/(&#39)/', "'", stripslashes($taskdeliver));
		$message .= $headings['action']['taskaction'].":\n".$taskaction."\n";
		$message .= (empty($taskdeliver) ? "" : $headings['action']['taskdeliver'].":\n".$taskdeliver."\n");
		$message .= $headings['action']['taskdeadline'].": ".date("d F Y", $taskdeadline)."\n";
		foreach($udfs as $u) {
			$message .= $u."\n";
		}
		$message .= "\nPlease log onto Assist in order to update this ".strtolower($this->actname).".\n";
		return $message;
	}











	public function getTask($object_id) {
		$cmpcode = $this->getCmpCode();
		$dbref = $this->getDBRef();
		$sql = "SELECT t.*
			, top.value as topic
			, urg.value as urgency
			, man.value as manager
			, del.value as delivery_mode
			, tframe.value as time_frame
			, supp.value as support_person
			, comp.value as competency
			, conf.value as confirmed
			, stat.value as status
			, CONCAT_WS(' ',tk.tkname,tk.tksurname) as adduser
			FROM ".$dbref."_task t
			LEFT OUTER JOIN ".$dbref."_list_topic top
			  ON top.id = t.tasktopicid
			LEFT OUTER JOIN ".$dbref."_list_urgency urg
			  ON urg.id = t.taskurgencyid
			LEFT OUTER JOIN ".$dbref."_list_status stat
			  ON stat.pkey = t.taskstatusid
            LEFT OUTER JOIN ".$dbref."_list_manager man
			  ON man.id = t.taskmanagerid
            LEFT OUTER JOIN ".$dbref."_list_delivery_mode del
			  ON del.id = t.taskdeliverymodeid
            LEFT OUTER JOIN ".$dbref."_list_time_frame tframe
			  ON tframe.id = t.tasktimeframeid
            LEFT OUTER JOIN ".$dbref."_list_support_person supp
			  ON supp.id = t.tasksupportpersonid
            LEFT OUTER JOIN ".$dbref."_list_competency comp
			  ON comp.id = t.taskcompetencyid
            LEFT OUTER JOIN ".$dbref."_list_confirm conf
			  ON conf.id = t.taskconfirmid
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep tk
			  ON tk.tkid = t.taskadduser
			WHERE taskid = ".$object_id;
		$task = $this->mysql_fetch_one($sql);
		return $task;

	}








	public function saveUpdate($var) {
		//set variables
		$logdate = time();
		$logtkid = $this->getUserID();
		$logupdate = ASSIST_HELPER::code($var['logupdate']);
		$logstatusid = $var['logstatusid'];
		$logstate = $var['logstatusid']==1 ? 100 : $var['logstate'];
		$logactdate = strtotime($var['logactdate']);
		$logemail = "Y";
		$logsubmittkid = $this->getUserID();
		$logtaskid = $var['logtaskid'];
		$logtasktkid = "";
		$logtype = "U";

        $tasklessons = ASSIST_HELPER::code($var['tasklessons']);
        $taskconsequence = ASSIST_HELPER::code($var['taskconsequence']);
        $taskcdppoints = ASSIST_HELPER::code($var['taskcdppoints']);
        $tasktimetaken = ASSIST_HELPER::code($var['tasktimetaken']);
        $taskconfirmid = isset($var['taskconfirmid']) ? $var['taskconfirmid'] : 0;//AA-689 PDP Assist - Update function fault. [SD]

        $task_before_change = $this->getTask($logtaskid);

        $sql = "UPDATE ".$this->getDBRef()."_task SET tasklessons='".$tasklessons."',taskconsequence='".$taskconsequence."',taskcdppoints='".$taskcdppoints."',tasktimetaken='".$tasktimetaken."',
        taskconfirmid=".$taskconfirmid."
              WHERE taskid=".$logtaskid;
        $this->db_update($sql);

		//insert log record & get update ID
		$sql = "INSERT INTO ".$this->getDBRef()."_log 
				(logid, logdate, logtkid, logupdate, logstatusid, logstate, logactdate, logemail, logsubmittkid, logtaskid, logtasktkid, logtype) 
				VALUES 
				(null, $logdate, '$logtkid', '$logupdate', $logstatusid, $logstate, $logactdate, '$logemail', '$logsubmittkid', $logtaskid, '$logtasktkid', '$logtype')";
		$logid = $this->db_insert($sql);

		//update task
		$sql = "UPDATE ".$this->getDBRef()."_task SET taskstatusid = $logstatusid , taskstate = $logstate WHERE taskid = $logtaskid ";
		$this->db_update($sql);

        /*****************************************************************************************************************/
        /*************************************** NEW STUFF - August 2020 *************************************************/
        /*****************************************************************************************************************/

        $headings_object = new PDP2_HEADINGS();
        $headings = $headings_object->getHeadingNames();
        $updates = "";
        $fld = "tasklessons";
        if(decode($task_before_change[$fld]) != $tasklessons) {
            if($task_before_change[$fld])
                $updates .= "<br />" .  $headings['update'][$fld] . " changed from  <i> ".$task_before_change[$fld]." </i>  to <i>$tasklessons</i>";
            else
                $updates .= "<br />" .  $headings['update'][$fld] . " changed to <i>$tasklessons</i>";
        }

        $fld = "taskconsequence";
        if(decode($task_before_change[$fld]) != $taskconsequence) {
            if($task_before_change[$fld])
                $updates .= "<br />" .  $headings['update'][$fld] . " changed from  <i> ".$task_before_change[$fld]." </i>  to <i>$taskconsequence</i>";
            else
                $updates .= "<br />" .  $headings['update'][$fld] . " changed to <i>$taskconsequence</i>";
        }

        $fld = "taskcdppoints";
        if(decode($task_before_change[$fld]) != $taskcdppoints) {
            if($task_before_change[$fld])
                $updates .= "<br />" .  $headings['update'][$fld] . " changed from  <i> ".$task_before_change[$fld]." </i>  to <i>$taskcdppoints</i>";
            else
                $updates .= "<br />" .  $headings['update'][$fld] . " changed to <i>$taskcdppoints</i>";
        }

        $fld = "tasktimetaken";
        if(decode($task_before_change[$fld]) != $tasktimetaken) {
            if($task_before_change[$fld])
                $updates .= "<br />" .  $headings['update'][$fld] . " changed from  <i> ".$task_before_change[$fld]." </i>  to <i>$tasktimetaken</i>";
            else
                $updates .= "<br />" .  $headings['update'][$fld] . " changed to <i>$tasktimetaken</i>";
        }

        $fld = "taskconfirmid";
		$task_before_change_fld = isset($task_before_change[$fld]) ? $task_before_change[$fld] : 0;
        if($task_before_change_fld != $taskconfirmid) {
            $sql = "SELECT * FROM ".$this->getDBRef()."_list_urgency WHERE id=".$task_before_change[$fld];
            $row0 = $this->mysql_fetch_one($sql);
            $sql = "SELECT * FROM ".$this->getDBRef()."_list_urgency WHERE id=".$taskconfirmid;
            $row1 = $this->mysql_fetch_one($sql);
            $updates .= "<br />" .  $headings['action'][$fld] . " changed from <i>".$row0['value']."</i> to <i>".$row1['value']."</i>";
        }

        if($updates != ""){
            $logupdate = " The following changes were made: <br />$updates";
            $logupdate = addslashes(htmlentities($logupdate));

            $sql = "UPDATE ".$this->getDBRef()."_log SET logupdate=CONCAT_WS('<br />',logupdate,'$logupdate') WHERE logid=$logid";
            $this->db_update($sql);
        }

		//Update UDFs
		$udftypes = unserialize($var['udf_types']);
		$udf_email = array();
		$var_udf = isset($var['udf']) ? $var['udf'] : array();
		foreach($var_udf as $key => $r) {
			$udfs = array();
			if(strlen($r)>0 && (!in_array($key,$udftypes['Y']) || checkIntRef($r))) {
				if(in_array($key,$udftypes['D'])) { $r = strtotime($r); }
				$udfs[$key] = "(null,$key,'".ASSIST_HELPER::code($r)."',$logid,'".$this->getModRef()."')";
			}
			if(count($udfs)>0) {
				$sql = "INSERT INTO assist_".$this->getCmpCode()."_udf VALUES ".implode(",",$udfs);
				$this->db_insert($sql);
			}
			if(strlen($r)>0) {	$udf_email[$key] = $r; }
		}

		//send email!!!!


        $return_array = array();
        $return_array['logid'] = $logid;
        $return_array['logdate'] = $logdate;
        $return_array['logtkid'] = $logtkid;
        $return_array['logupdate'] = $logupdate;
        $return_array['logstatusid'] = $logstatusid;
        $return_array['logstate'] = $logstate;
        $return_array['logactdate'] = $logactdate;
        $return_array['logemail'] = $logemail;
        $return_array['logsubmittkid'] = $logsubmittkid;
        $return_array['logtaskid'] = $logtaskid;
        $return_array['logtasktkid'] = $logtasktkid;
        $return_array['logtype'] = $logtype;
        $this->return_array = $return_array;

		return $logid;
	}






}