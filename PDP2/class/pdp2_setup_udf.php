<?php


class PDP2_SETUP_UDF extends PDP2_SETUP {

	private $table_name;
	private $id_field = "udfiid";
	private $name = "UDF";
	private $field_prefix = "udfi";

	private $list_field = "udfv";
	private $list_table_name;
	private $list_name = "UDF List Item";
	private $list_id_field = "udfvid";

	public function __construct() {
		parent::__construct();

		$this->table_name = "assist_".$this->getCmpCode()."_udfindex";
		$this->list_table_name = "assist_".$this->getCmpCode()."_udfvalue";
	}

	public function getTableName() { return $this->table_name; }

	public function getAnItem($id) {
		return $this->mysql_fetch_one("SELECT * FROM ".$this->table_name." WHERE ".$this->id_field." = ".$id);
	}

	public function getObject($var) {
		$row = $this->getAnItem($var['id']);
		$row['object_link'] = strlen($row['udfilinkfield'])>0 ? $row['udfiobject']."_".$row['udfilinkfield'] : $row['udfiobject'];
		return $row;
	}

	public function getAllUDFsForSetup() {
		$sql = "SELECT * FROM ".$this->table_name." WHERE udfiref = '".$this->getModRef()."' AND udfiyn = 'Y' ORDER BY udfisort";
		return $this->mysql_fetch_all_by_id($sql,"udfiid");
	}



	public function addObject($var) {
		$new_data = $var;
		$var = array();
		foreach($new_data as $fld => $value){
			$var[$this->field_prefix.$fld] = $value;
		}
		if(!isset($var[$this->field_prefix.'yn'])) {
			$var[$this->field_prefix.'yn'] = "Y";
		}
		$sql = "INSERT INTO ".$this->table_name." SET ".$this->convertArrayToSQLForSave($var);
		$id = $this->db_insert($sql);

		$transaction = "New ".$this->name." '".$new_data['value']."' ($id) added";
		$this->addSetupLog($transaction,$sql);

		return array("ok","New ".$this->name." added successfully.");
	}


	public function editObject($id,$var) {
		$old = $this->getAnItem($id);
		$update_data = array();
		foreach($var as $fld => $v) {
			$fld = $this->field_prefix.$fld;
			if($v!=$old[$fld]) {
				$update_data[$fld] = $v;
			}
		}
		if(count($update_data)>0) {
			$sql = "UPDATE ".$this->table_name." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->id_field." = ".$id;
			$this->db_update($sql);

			$transaction = "Updated ".$this->name." $id to ".$var['value'];
			$this->addSetupLog($transaction,$sql,serialize($old));

			return array("ok","Changes saved successfully.");
		} else {
			return array("info","No changes found to be saved.");
		}
	}

	public function deleteObject($id) {
		$update_data = array('udfiyn'=>"N");
		$sql = "UPDATE ".$this->table_name." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->id_field." = ".$id;
		$this->db_update($sql);

		$transaction = "Deleted ".$this->name." $id";
		$this->addSetupLog($transaction,$sql);

		return array("ok","Deleted ".$this->name." $id successfully.");
	}

	public function restoreObject($id) {
		$update_data = array('udfiyn'=>"Y");
		$sql = "UPDATE ".$this->table_name." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->id_field." = ".$id;
		$this->db_update($sql);

		$transaction = "Restored ".$this->name." $id";
		$this->addSetupLog($transaction,$sql);

		return array("ok","Restored ".$this->name." $id successfully.");
	}







	/*************************************************
	 * LIST ITEM FUNCTIONS
	 */

	/**
	 * @param $id
	 * @return array
	 */
	public function getAllListObjectsForSetup($id) {
		$sql = "SELECT udfvid as id, udfvvalue as value, udfvyn as yn, udfvindex as parent FROM ".$this->list_table_name." WHERE udfvindex = ".$id." ORDER BY udfvvalue";
		return $this->mysql_fetch_all_by_id($sql,"id");
	}

	/**
	 * @param array $var
	 * @return array $result
	 */
	public function addListObject($var) {
		$new_data = $var;
		$var = array();
		foreach($new_data as $fld => $value){
			$var[$this->list_field.$fld] = $value;
		}
		if(!isset($var[$this->list_field.'yn'])) {
			$var[$this->list_field.'yn'] = "Y";
		}
		$sql = "INSERT INTO ".$this->list_table_name." SET ".$this->convertArrayToSQLForSave($var);
		$id = $this->db_insert($sql);

		$transaction = "New ".$this->list_name." '".$new_data['value']."' ($id) added";
		$this->addSetupLog($transaction,$sql);

		return array("ok","New ".$this->list_name." added successfully.");
	}


	/**
	 * @param int $id
	 * @param array $var
	 * @return array result
	 */
	public function editListObject($id,$var) {
		$old = $this->getAListItem($id);
		$update_data = array();
		foreach($var as $fld => $v) {
			$fld = $this->list_field.$fld;
			if($v!=$old[$fld]) {
				$update_data[$fld] = $v;
			}
		}
		if(count($update_data)>0) {
			$sql = "UPDATE ".$this->list_table_name." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->list_id_field." = ".$id;
			$this->db_update($sql);

			$transaction = "Updated ".$this->list_name." $id to ".$var['value'];
			$this->addSetupLog($transaction,$sql,serialize($old));

			return array("ok","Changes saved successfully.");
		} else {
			return array("info","No changes found to be saved.");
		}
	}


	public function deleteListObject($id) {
		$update_data = array('udfvyn'=>"N");
		$sql = "UPDATE ".$this->list_table_name." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->list_id_field." = ".$id;
		$this->db_update($sql);

		$transaction = "Deleted ".$this->list_name." $id";
		$this->addSetupLog($transaction,$sql);

		return array("ok","Deleted ".$this->list_name." $id successfully.");
	}

	public function restoreListObject($id) {
		$update_data = array('udfvyn'=>"Y");
		$sql = "UPDATE ".$this->list_table_name." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->list_id_field." = ".$id;
		$this->db_update($sql);

		$transaction = "Restored ".$this->list_name." $id";
		$this->addSetupLog($transaction,$sql);

		return array("ok","Restored ".$this->list_name." $id successfully.");
	}

	/**
	 * @param int $id
	 * @return array
	 */
	public function getAListItem($id) {
		return $this->mysql_fetch_one("SELECT * FROM ".$this->list_table_name." WHERE ".$this->list_id_field." = ".$id);
	}

}