<?php


class PDP2_SETUP_LISTS extends PDP2_SETUP {

	private $my_list;
	private $table_name;
	private $name;
	private $order_by_field = "value";
	private $id_field = "id";

	public function __construct($list) {
		parent::__construct();
		$this->my_list = $list;
		$this->table_name = $this->getDBRef();


		switch($list) {
			case "topic":
				$this->table_name.="_list_topic";
				$this->name = "Topic";
				break;
			case "status":
				$this->table_name.="_list_status";
				$this->name = "Status";
				$this->order_by_field = "sort, state, value";
				$this->id_field = "pkey";
				break;
            case "manager":
                $this->table_name.="_list_manager";
                $this->name = "Name of Manager";
                break;
            case "delivery_mode":
                $this->table_name.="_list_delivery_mode";
                $this->name = "Mode of Delivery";
                break;
            case "time_frame":
                $this->table_name.="_list_time_frame";
                $this->name = "Time Frame";
                break;
            case "support_person":
                $this->table_name.="_list_support_person";
                $this->name = "Support Person";
                break;
            case "competency":
                $this->table_name.="_list_competency";
                $this->name = "Competency";
                break;
            case "urgency":
                $this->table_name.="_list_urgency";
                $this->name = "Priority";
                break;
            case "confirm":
                $this->table_name.="_list_confirm";
                $this->name = "Confirm";
                break;
		}

	}
	public function getTableName() { return $this->table_name; }

	public function addObject($var) {
		if(!isset($var['yn'])) {
			$var['yn'] = "Y";
		}
		$sql = "INSERT INTO ".$this->table_name." SET ".$this->convertArrayToSQLForSave($var);
		$id = $this->db_insert($sql);

		$transaction = "New ".$this->name." '".$var['value']."' ($id) added to module ".$this->getModRef();
		$this->addSetupLog($transaction,$sql);

		return array("ok","New ".$this->name." added successfully.");
	}


	public function editObject($id,$var) {
		$old = $this->getAnItem($id);
		$update_data = array();
		foreach($var as $fld => $v) {
			if($v!=$old[$fld]) {
				$update_data[$fld] = $v;
			}
		}
		if(count($update_data)>0) {
			$sql = "UPDATE ".$this->table_name." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->id_field." = ".$id;
			$this->db_update($sql);

			$transaction = "Updated ".$this->name." $id to ".$var['value'];
			$this->addSetupLog($transaction,$sql);

			return array("ok","Changes saved successfully.");
		} else {
			return array("info","No changes found to be saved.");
		}
	}

	public function deleteObject($id) {
		$update_data = array('yn'=>"N");
		$sql = "UPDATE ".$this->table_name." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->id_field." = ".$id;
		$this->db_update($sql);

		$transaction = "Deleted ".$this->name." $id";
		$this->addSetupLog($transaction,$sql);

		return array("ok","Deleted ".$this->name." $id successfully.");
	}

	public function restoreObject($id) {
		$update_data = array('yn'=>"Y");
		$sql = "UPDATE ".$this->table_name." SET ".$this->convertArrayToSQLForSave($update_data)." WHERE ".$this->id_field." = ".$id;
		$this->db_update($sql);

		$transaction = "Restored ".$this->name." $id";
		$this->addSetupLog($transaction,$sql);

		return array("ok","Restored ".$this->name." $id successfully.");
	}

	public function sortObjects($var) {
		$sort = $var['sort'];
		$log_sql = array();
		foreach($sort as $key => $id) {
			$sql = "UPDATE ".$this->table_name." SET sort = ".($key+2)." WHERE ".$this->id_field." = ".$id;
			$this->db_update($sql);
			$log_sql[] = $sql;
		}
		$sql = "UPDATE ".$this->table_name." SET sort = 1 WHERE ".$this->id_field." = 4";	//Set NEW as first item
			$this->db_update($sql);
			$log_sql[] = $sql;
		$sql = "UPDATE ".$this->table_name." SET sort = ".(count($sort)+99)." WHERE ".$this->id_field." = 1";	//Set Completed/Closed as last item
			$this->db_update($sql);
			$log_sql[] = $sql;
		$sql = "UPDATE ".$this->table_name." SET sort = 0 WHERE ".$this->id_field." = 2";	//Remove Cancelled from order
			$this->db_update($sql);
			$log_sql[] = $sql;

		$transaction = "Reordered ".$this->name." list";
		$this->addSetupLog($transaction,implode(";|#|",$log_sql).";");

		return array("ok","Display order saved successfully.");
	}

	public function getAnItem($id) {
		return $this->mysql_fetch_one("SELECT * FROM ".$this->table_name." WHERE ".$this->id_field." = ".$id);
	}

	public function getAllObjectsForSetup() {
		if($this->my_list=="status") {
			$rows = $this->mysql_fetch_all_by_id("SELECT * FROM ".$this->table_name." WHERE yn IN ('Y','N') AND id <> 'ON' ORDER BY ".$this->order_by_field,$this->id_field);
			$sql = "SELECT taskstatusid as status_id, COUNT(taskid) as count FROM ".$this->getDBRef()."_task GROUP BY taskstatusid";
			$count = $this->mysql_fetch_value_by_id($sql,"status_id","count");
			foreach($rows as $id => $row) {
				$rows[$id]['count'] = (isset($count[$id]) ? $count[$id] : 0);
				if($this->id_field!="id" && isset($row['id'])) {
					$rows[$id]['old_id'] = $row['id'];
					$rows[$id]['id'] = $row[$this->id_field];
				}
			}
			return $rows;
		} else {
			return $this->mysql_fetch_all_by_id("SELECT * FROM ".$this->table_name." ORDER BY ".$this->order_by_field,$this->id_field);
		}
	}




}