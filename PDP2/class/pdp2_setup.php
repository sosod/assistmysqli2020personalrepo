<?php


class PDP2_SETUP extends PDP2 {

	private $user_access_types = array();

	public function __construct() {
		parent::__construct();
		$this->user_access_types[20] = "own ".$this->actname."s only";
		$this->user_access_types[90] = "all ".$this->actname."s";
	}
	public function getUserAccessOptions() { return $this->user_access_types; }

	/**********************************************
	 * SETUP DEFAULTS FUNCTIONS
	 */
	public function getCurrentAdminUser() {
		$sql = "SELECT value FROM assist_".$this->getCmpCode()."_setup WHERE ref = '".$this->my_modref."' AND refid = 0";
		$row = $this->mysql_fetch_one($sql);
		return $row['value'];
	}
	
	public function setNewAdminUser($admin_user_id) {
		//get Current admin user
		$current_admin_user_id = $this->getCurrentAdminUser();
		//if the current admin != new admin then save changes
		if($current_admin_user_id != $admin_user_id) {
			//Update admin user settings
			$sql = "UPDATE assist_".$this->getCmpCode()."_setup SET value = '".$admin_user_id."' WHERE ref = '".$this->my_modref."' AND refid = 0";
			$this->db_update($sql);
			//Log change
			$transaction = "Updated ".$this->my_modref." Setup Administrator to ".$this->getAUserName($admin_user_id)." (User ID: ".$admin_user_id.")";
			$this->addSetupLog($transaction,$sql,$current_admin_user_id);
			//if previous admin user is not The Assist Admin user then downgrade their access
			if(!$this->isAdminUser($current_admin_user_id)) {
				$old = $this->getUserAccess($current_admin_user_id);
				//downgrade access of previous admin user to 90 = "all" from 100 = "all + setup"
				$sql = "UPDATE ".$this->getDBRef()."_list_access SET act = 90, view = 90 WHERE tkid = '".$current_admin_user_id."' AND yn = 'Y'";
				$this->db_update($sql);
				//log change
				$transaction = "Updated ".$this->my_modref." user access for the previous setup admin ".$this->getAUserName($current_admin_user_id)." (User ID: ".$current_admin_user_id.") down to 90 due to change of setup admin.";
				$this->addSetupLog($transaction,$sql,$old['act']);
			}
			//If new admin user is not the Assist Admin user then upgrade the user's access to 100
			if($this->isAdminUser($admin_user_id)) {
				$current_user_access = $this->getUserAccess($admin_user_id);
				//if no record then add new one
				if(!isset($current_user_access['id']) || $current_user_access['id']===false) {
					$sql = "INSERT INTO ".$this->getDBref()."_list_access SET tkid = '".$admin_user_id."', act = 100, view = 100, yn = 'Y'";
					$this->db_insert($sql);
					//Set the transaction log values
					$transaction = "Added ".$this->my_modref." user access for ".$this->getAUserName($admin_user_id)." (User ID: ".$admin_user_id.") due to setting as setup admin.";
					$this->addSetupLog($transaction,$sql,20);
				} else {
					//else update existing record
					$sql = "UPDATE ".$this->getDBref()."_list_access SET act = 100, view = 100 WHERE tkid = '".$admin_user_id."' AND yn = 'Y'";
					$this->db_update($sql);
					//Set the transaction log values
					$transaction = "Updated ".$this->my_modref." user access for ".$this->getAUserName($admin_user_id)." (User ID: ".$admin_user_id.") due to setting as setup admin.";
					$this->addSetupLog($transaction,$sql,$current_user_access['act']);
				}
			}
			return array("ok","Admin user updated successfully.");
		} else {
			return array("info","No change found to be saved.  User selected is already the Admin user.");
		}
	}


	public function getAvailableAdminUsers() {
		$sql = "SELECT * 
				FROM assist_".$this->getCmpCode()."_timekeep t 
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users m 
				ON t.tkid = m.usrtkid
				WHERE t.tkstatus = 1 AND t.tkid <> '0000'  
				AND m.usrmodref = '".$this->getModRef()."' 
				ORDER BY t.tkname, t.tksurname";
		return $this->mysql_fetch_all($sql);

	}










	/**********************************************
	 * SETUP > USER ACCESS FUNCTIONS
	 */
	public function getUsersWithoutDefinedAccess($got=array()) {
		$sql = "SELECT t.tkid as id, CONCAT_WS(' ',t.tkname, t.tksurname) as name
				FROM assist_".$this->getCmpCode()."_timekeep t
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users m 
				ON t.tkid = m.usrtkid 
				WHERE m.usrmodref = '".$this->getModRef()."' ";
		if(count($got)>0) {
			$sql.= " AND t.tkid NOT IN ('".implode("','",$got)."')";
		}
		return $this->mysql_fetch_all($sql);
	}


	public function getModuleAdminName($admin_user_id) {
		if($this->isAdminUser($admin_user_id)) {
			return $this->getAssistAdminName();
		} else {
			return $this->getAUserName($admin_user_id);
		}
	}


	public function getCurrentUserAccessSettings() {
		$sql = "SELECT a.tkid, t.tkname, t.tksurname, a.act, r.ruletext, a.id 
				FROM ".$this->getDBRef()."_list_access a
				INNER JOIN ".$this->getDBRef()."_list_access_rules r
					ON r.ruleint = a.act AND r.rulefn = 'act' AND r.ruleint < 100
				INNER JOIN assist_".$this->getCmpCode()."_timekeep t
					ON t.tkid = a.tkid AND t.tkstatus = 1
				INNER JOIN assist_".$this->getCmpCode()."_menu_modules_users mmu 
					ON t.tkid = mmu.usrtkid AND mmu.usrmodref = '".$this->my_modref."' 
				WHERE yn = 'Y'
				ORDER BY t.tkname, t.tksurname, a.id ASC";
		return $this->mysql_fetch_all_fld($sql,"tkid");
	}

	public function saveNewUserAccess($tkid,$access) {
		//Get current access to check if already exists before adding again
		$existing = $this->getUserAccess($tkid);
		//if id = false then no record exists and user can be added
		if($existing['id']===false) {
			//insert record
			$sql = "INSERT INTO ".$this->getDBRef()."_list_access SET tkid = '".$tkid."', view = ".$access.", act = ".$access.", yn = 'Y'";
			$this->db_insert($sql);
			//log
			$transaction = "User ".$this->getAUserName($tkid)." (ID: ".$tkid.") added with user access $access for module '".$this->getModRef()."'.";
			$this->addSetupLog($transaction,$sql);

			$result = array("ok","User added successfully.");
		} else {
			$result = array("info","User already exists in the User Access table.  Please try again.");
		}
		return $result;
	}



	public function getUserAccessSettingsByID($id) {
		$sql = "SELECT * FROM ".$this->getDBRef()."_list_access WHERE id = ".$id;
		return $this->mysql_fetch_one($sql);
	}


	public function editUserAccess($id,$access) {
		$old = $this->getUserAccessSettingsByID($id);
		//only save changes if different from existing
		if($old['act']!=$access) {
			//PERFORM EDIT UPDATE
			$sql = "UPDATE ".$this->getDBRef()."_list_access SET view = ".$access.", act = ".$access." WHERE id = ".$id;
			$this->db_update($sql);
			//Log
			$transaction = "Updated user ".$old['tkid']." access from ".$old['act']." to ".$access.".";
			$this->addSetupLog($transaction,$sql,$old['act']);
			return array("ok","User access settings updated successfully.");
		} else {
			return array("info","No changes found to be saved.");
		}
	}




	/**********************************************
	 * SETUP LOG FUNCTIONS
	 */

	/**
	 * Function to record logs
	 * @param string $transaction
	 * @param string $tsql
	 * @param string $told
	 */
	public function addSetupLog($transaction,$tsql,$told="") {
		$tsql = str_replace("'","|",$tsql);
		$trans = str_replace("'","&#39",$transaction);
		$told = str_replace("'","&#39",$told);
		$today = time();
		$tkid = $this->getUserID();
		$tref = $this->getModRef();
		$sql = "INSERT INTO assist_".$this->getCmpCode()."_log SET date = '".$today."', tkid = '".$tkid."', ref = '".$tref."', transaction = '".$trans."', tsql = '".$tsql."', told = '".$told."'";
		$this->db_insert($sql);
	}

}