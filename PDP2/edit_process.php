<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	array('txt'=>"Edit"),
);
$page = array("edit");
$get_udf_link_headings = false;
require_once("inc_header.php");

$taskid = $_REQUEST['taskid'];

    //this is just to maintain the code in the form it is...
    $tref = "$modref";
    $udfFields = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
    $these_rows = $ah->mysql_fetch_all($sql);

    foreach($these_rows as $row_index => $row){
        $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
        $more_rows = $ah->mysql_fetch_all($sql2);

        $udf = count($more_rows);
        $row2 = $ah->mysql_fetch_one($sql2);
        $udfFields[$row['udfivalue']] = '';
        switch($row['udfilist']) {
            case "Y":
                if(checkIntRef($row2['udfvalue'])) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                    $more_rows = $ah->mysql_fetch_all($sql2);
                    if(count($more_rows)>0) {
                        $row3 = $ah->mysql_fetch_one($sql2);
                    }
                }

                $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                $more_more_rows = $ah->mysql_fetch_all($sql2);
                foreach($more_more_rows as $more_row_index => $row_3){
                    if($row3['udfvid'] == $row_3['udfvid'])
                        $udfFields[$row['udfivalue']] = $row_3['udfvvalue'];
                }
                break;
            case "T":
			case "M":
			case "D":
                $udfFields[$row['udfivalue']] = $row2['udfvalue'];
                break;
            default:
                break;
        }
    }//crazy code ends here..

    //get the original task record before editing.
    $sql = "SELECT * FROM ".$dbref."_task WHERE taskid=$taskid"; $linenumber = __LINE__;
    $linenumber = 0;
    $row = $ah->mysql_fetch_one($sql);
    $taskstate = $row['taskstate'];
    if($_POST['taskstatusid'] == 4) {
        $taskstate = 0;
    }else if($_POST['taskstatusid'] == 1) {
        $taskstate = 100;
    }
    /*
     * If the Task Status is changed from �On-going?? to something else the taskstate field must be set to 0.
    */
    if($row['taskstatusid'] == 5 && $_POST['taskstatusid'] != 5) {
        $taskstate = 0;
    }
    /*
     * If the Task status is changed from �Complete?? to any other status then the taskstate field must be returned to the previous state
     * (as per the most recent _log record before the one that closed the task)
    */
    $statusid = $_POST['taskstatusid'];
    if($row['taskstatusid'] == 1 && $_POST['taskstatusid'] != 1) {
        $sql = "SELECT * FROM ".$dbref."_log WHERE logtaskid=$taskid ORDER BY logdate DESC LIMIT 2";
        $log_rows = $ah->mysql_fetch_all($sql);
        $row_count = 1;
        $rowR = array();
        foreach($log_rows as $the_id => $the_row){
            if($row_count == 2){
                $rowR = $the_row;
            }
            $row_count++;
        }
        $taskstate = $rowR['logstate'];
    }
    $taskdeadline = strtotime($_POST['datepicker']);
    $taskdeliver = code($_POST['taskdeliver']);
    $taskaction = code($_POST['taskaction']);
    $tasktraining = code($_POST['tasktraining']);
    $taskworkopportunity = code($_POST['taskworkopportunity']);
    $sql = "UPDATE ".$dbref."_task SET tasktkid='',tasktopicid=".$_POST['tasktopicid'].",
        taskurgencyid=".$_POST['taskurgencyid'].",taskstatusid=".$_POST['taskstatusid'].",taskdeadline='".$taskdeadline."',
        taskmanagerid=".$_POST['taskmanagerid'].",taskdeliverymodeid=".$_POST['taskdeliverymodeid'].",tasktraining='".$tasktraining."',
        tasktimeframeid=".$_POST['tasktimeframeid'].",tasksupportpersonid=".$_POST['tasksupportpersonid'].",taskcompetencyid=".$_POST['taskcompetencyid'].",taskworkopportunity='".$taskworkopportunity."',
          taskaction='".$taskaction."',taskdeliver='".$taskdeliver."',taskstate=$taskstate
              WHERE taskid=".$_POST['taskid'];
    $ah->db_update($sql);
    /*updating recipients
     * first get the original assignees into aarray,then delete them from the recipients table,
     * insert the new recipients
    */
    //get the old assignees to a task,this will be used later when updating somewhere below.

    $oldRecipients = array();
    $sql = "SELECT * FROM ".$dbref."_task_recipients WHERE taskid=".$_POST['taskid'];
    $these_rows = $ah->mysql_fetch_all($sql);

    foreach($these_rows as $row_index => $rowO){
        array_push($oldRecipients,$rowO['tasktkid']);
    }

    //get the new assignees to the task,this will be used later when updating somewhere below.
    $newRecipients = array();
    if(is_array($_POST['tasktkid'])) {
        $newRecipients = $_POST['tasktkid'];
    }else {
        $newRecipients[] = $_POST['tasktkid'];
    }
    //remove the old recipients to this task.
    $sql = "DELETE FROM ".$dbref."_task_recipients WHERE taskid=".$_POST['taskid'];
    $ah->db_query($sql);

    //insert the new recipients for this task
    $sql = "INSERT INTO ".$dbref."_task_recipients (taskid,tasktkid) VALUES ";
    foreach($newRecipients as $value) {
        $sql .= "($taskid,'$value'),";
    }
    $sql = substr($sql, 0,-1);
    $ah->db_insert($sql);

    $taskaction = $_POST['taskaction'];
    $taskdeliver = $_POST['taskdeliver'];
    $logdate = $today;
    $logtkid = $tkid;
    $updator = getTK($tkid, $cmpcode, 'tkn');

    $updates = "";
    $usersChanges ="";
    $logupdate = "The ".strtolower($actname)." has been edited by <i>$updator</i>.<br/>";
    /*construct logupdate bits for the changes in recipients
      * making use of @newRecipients array and @oldRecipients array
    */

    $removedUsers = array_diff($oldRecipients,$newRecipients);
    if(!empty($removedUsers)) {
        $usersChanges .= "The following user(s) were removed from working on ".strtolower($actname)." id ".$taskid."<br />";
        foreach($removedUsers as $key => $tasktkid) {
            $usersChanges .= "<i>".getTK($tasktkid, $cmpcode, 'tkn')."</i><br />";
        }
    }
    $newUsers = array_diff($newRecipients,$oldRecipients);
    if(!empty($newUsers)) {
        $usersChanges .= "The following user(s) were added to work on ".strtolower($actname)." id ".$taskid."<br />";
        foreach($newUsers as $key => $tasktkid) {
            $usersChanges .= "<i>".getTK($tasktkid, $cmpcode, 'tkn')."</i><br />";
        }
    }
    //send emails to all affected users
    $toEmails = array();
    foreach ($newRecipients as $key => $value) {
        //if(isset($_POST['sendEmail'])) {
        //    array_push($toEmails,$value);
        //}else {
            if($tkid != $value) {//if the logged in user is one of the recipients, do not send email to that user
                array_push($toEmails,$value);
            }
       // }
    }
    foreach ($oldRecipients as $key => $value) {
      //  if(isset($_POST['sendEmail'])) {
      //      array_push($toEmails,$value);
     //   }else {
            if($tkid != $value) {//if the logged in user is one of the recipients, do not send email to that user
                array_push($toEmails,$value);
            }
       // }
    }
    if(isset($_POST['sendEmail'])){
            array_push($toEmails,$tkid);
        }
    if(strtolower($cmpcode) != 'wcc0001' && !empty($toEmails)) {
        $sql = "SELECT tkemail FROM assist_".$cmpcode."_timekeep t WHERE t.tkid=";
        for ($i = 0; $i < count($toEmails);$i++) {
            if($i < count($toEmails) - 1)
                $sql .= "'".$toEmails[$i]."' OR t.tkid=";
            else
                $sql .= "'".$toEmails[$i]."'";
        }

        $strTo = "";
        $toEmails = array();
        $these_rows = $ah->mysql_fetch_all($sql);

        foreach($these_rows as $row_index => $rowRusers){
            $strTo .= $rowRusers['tkemail'].",";
			$toEmails[] = $rowRusers['tkemail'];
        }
        $strToEmails = substr($strTo,0,-1);

        $sql = "SELECT * FROM assist_".$cmpcode."_timekeep t WHERE t.tkid=".$row['taskadduser'];
        $rowadduser = $ah->mysql_fetch_one($sql);
        $from = $rowadduser['tkemail'];
        $sql = "SELECT * FROM ".$dbref."_list_urgency u WHERE id=".$row['taskurgencyid'];
        $rowurg = $ah->mysql_fetch_one($sql);

        $sql = "SELECT * FROM ".$dbref."_list_topic t WHERE id=".$row['taskurgencyid'];
        $rowtopic = $ah->mysql_fetch_one($sql);
        $userFrom = $rowadduser['tkname']." ".$rowadduser['tksurname'];
        $subject = "Task with id ".$taskid." on Ignite Assist edited.";


        $message .= $rowadduser['tkname']." ".$rowadduser['tksurname']." has made some changes on ".strtolower($actname)." with id ".$taskid.":\n";
        $message .= "Prority: ".$rowurg['value']."\n";
        $message .= "Topic: ".$rowtopic['value']."\n";
        $taskaction = preg_replace('/(&#39)/', "'", stripslashes($taskaction));
        $taskdeliver = preg_replace('/(&#39)/', "'", stripslashes($taskdeliver));
        //$taskaction = html_entity_decode(stripslashes($taskaction));
       // $taskdeliver = html_entity_decode(stripslashes($taskdeliver));
        $message .= ucfirst($actname)." Instructions:\n".$taskaction."\n";
        $message .= ucfirst($actname)." Deliverables:".(empty($taskdeliver) ? "N/A" : "\n".$taskdeliver)."\n";
        $message .= "Deadline: ".date("d F Y",$taskdeadline)."\n";
        $message .= "Please log onto Assist in order to update this ".strtolower($actname).".\n";
        $message = nl2br($message);
        //Send email
if(strtoupper($cmpcode)!="IASSIST") {
        $headers = 'MIME-Version:1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: '.$userFrom.' <'.$from.'>' . "\r\n";
        $headers .= 'Reply-to: '.$userFrom.' <'.$from.'>' . "\r\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();
} else {
        $headers = 'MIME-Version:1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: no-reply@ignite4u.co.za' . "\r\n";
        $headers .= 'Reply-to: '.$userFrom.' <'.$from.'>' . "\r\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();
}
//        ini_set("sendmail_from",$from);
if(strtoupper($cmpcode)!="IASSIST") {
        //mail($strToEmails,decode($subject),$message,$headers);
	$cc = "";
    $bcc = "";
	$reply_to = array('name'=>$userFrom,'email'=>$from);
	//$toEmails= $recipients['rec'];
    $mail = new ASSIST_EMAIL($toEmails, $subject, $message, "TEXT", $cc, $bcc, $reply_to);
	$mail->sendEmail();
        
}
        // echo ( mail($strToEmails,$subject,$message,$headers) ? "success" : "failed");
    }

    $fld = "tasktopicid";
    if($row[$fld] != $_POST[$fld]) {
        $sql = "SELECT * FROM ".$dbref."_list_topic WHERE id=".$row[$fld];
        $row0 = $ah->mysql_fetch_one($sql);
        $sql = "SELECT * FROM ".$dbref."_list_topic WHERE id=".$_POST[$fld];
        $row1 = $ah->mysql_fetch_one($sql);
        $updates .= "<br />" .  $headings['action'][$fld] . " changed from <i>".$row0['value']." </i>to <i>".$row1['value']."</i>";
    }

    $fld = "taskurgencyid";
    if($row[$fld] != $_POST[$fld]) {
        $sql = "SELECT * FROM ".$dbref."_list_urgency WHERE id=".$row[$fld];
        $row0 = $ah->mysql_fetch_one($sql);
        $sql = "SELECT * FROM ".$dbref."_list_urgency WHERE id=".$_POST[$fld];
        $row1 = $ah->mysql_fetch_one($sql);
        $updates .= "<br />" .  $headings['action'][$fld] . " changed from <i>".$row0['value']."</i> to <i>".$row1['value']."</i>";
    }

    $fld = "taskaction";
    if(decode($row[$fld]) != $taskaction) {
        if($row[$fld])
            $updates .= "<br />" .  $headings['action'][$fld] . " changed from  <i> ".$row[$fld]." </i>  to <i>$taskaction</i>";
        else
            $updates .= "<br />" .  $headings['action'][$fld] . " changed to <i>$taskaction</i>";
    }

    $fld = "taskdeliver";
    if(decode($row[$fld]) != $taskdeliver) {
        if($row[$fld])
            $updates .= "<br />" .  $headings['action'][$fld] . " changed from  <i> ".$row[$fld]." </i>  to <i>$taskdeliver</i>";
        else
            $updates .= "<br />" .  $headings['action'][$fld] . " changed to <i>$taskdeliver</i>";
    }

    $fld = "taskstatusid";
    if($row[$fld] != $_POST[$fld]) {
        $sql = "SELECT * FROM ".$dbref."_list_status WHERE pkey=".$row[$fld];
        $row0 = $ah->mysql_fetch_one($sql);
        $taskstate0 = $row0['state'];
        $sql = "SELECT * FROM ".$dbref."_list_status WHERE pkey=".$_POST[$fld];
        $row1 = $ah->mysql_fetch_one($sql);
        $taskstate1 = $row0['state'];
        $updates .= "<br />" .  $headings['action'][$fld] . " has been changed from <i>".$row0['value']."</i> to <i>".$row1['value']."</i>";
        //$updates .= "<br />Task state changed from $taskstate0 to $taskstate1";
    }
    $fld = "taskdeadline";
    if($row[$fld] != $taskdeadline) {
        $updates .= "<br />" .  $headings['action'][$fld] . " changed from ".date("d-M-Y",$row[$fld])." to ".date("d-M-Y",$taskdeadline);
    }

    /*****************************************************************************************************************/
    /*************************************** NEW STUFF - August 2020 *************************************************/
    /*****************************************************************************************************************/
    $fld = "taskmanagerid";
    if($row[$fld] != $_POST[$fld]) {
        $sql = "SELECT * FROM ".$dbref."_list_manager WHERE id=".$row[$fld];
        $row0 = $ah->mysql_fetch_one($sql);
        $sql = "SELECT * FROM ".$dbref."_list_manager WHERE id=".$_POST[$fld];
        $row1 = $ah->mysql_fetch_one($sql);
        $updates .= "<br />" .  $headings['action'][$fld] . " changed from <i>".$row0['value']." </i>to <i>".$row1['value']."</i>";
    }

    $fld = "taskdeliverymodeid";
    if($row[$fld] != $_POST[$fld]) {
        $sql = "SELECT * FROM ".$dbref."_list_delivery_mode WHERE id=".$row[$fld];
        $row0 = $ah->mysql_fetch_one($sql);
        $sql = "SELECT * FROM ".$dbref."_list_delivery_mode WHERE id=".$_POST[$fld];
        $row1 = $ah->mysql_fetch_one($sql);
        $updates .= "<br />" .  $headings['action'][$fld] . " changed from <i>".$row0['value']." </i>to <i>".$row1['value']."</i>";
    }

    $fld = "tasktraining";
    if(decode($row[$fld]) != $tasktraining) {
        if($row[$fld])
            $updates .= "<br />" .  $headings['action'][$fld] . " changed from  <i> ".$row[$fld]." </i>  to <i>$tasktraining</i>";
        else
            $updates .= "<br />" .  $headings['action'][$fld] . " changed to <i>$taskaction</i>";
    }

$fld = "tasktimeframeid";
if($row[$fld] != $_POST[$fld]) {
    $sql = "SELECT * FROM ".$dbref."_list_time_frame WHERE id=".$row[$fld];
    $row0 = $ah->mysql_fetch_one($sql);
    $sql = "SELECT * FROM ".$dbref."_list_time_frame WHERE id=".$_POST[$fld];
    $row1 = $ah->mysql_fetch_one($sql);
    $updates .= "<br />" .  $headings['action'][$fld] . " changed from <i>".$row0['value']." </i>to <i>".$row1['value']."</i>";
}

$fld = "taskworkopportunity";
if(decode($row[$fld]) != $taskworkopportunity) {
    if($row[$fld])
        $updates .= "<br />" .  $headings['action'][$fld] . " changed from  <i> ".$row[$fld]." </i>  to <i>$taskworkopportunity</i>";
    else
        $updates .= "<br />" .  $headings['action'][$fld] . " changed to <i>$taskaction</i>";
}

$fld = "tasksupportpersonid";
if($row[$fld] != $_POST[$fld]) {
    $sql = "SELECT * FROM ".$dbref."_list_support_person WHERE id=".$row[$fld];
    $row0 = $ah->mysql_fetch_one($sql);
    $sql = "SELECT * FROM ".$dbref."_list_support_person WHERE id=".$_POST[$fld];
    $row1 = $ah->mysql_fetch_one($sql);
    $updates .= "<br />" .  $headings['action'][$fld] . " changed from <i>".$row0['value']." </i>to <i>".$row1['value']."</i>";
}

$fld = "taskcompetencyid";
if($row[$fld] != $_POST[$fld]) {
    $sql = "SELECT * FROM ".$dbref."_list_competency WHERE id=".$row[$fld];
    $row0 = $ah->mysql_fetch_one($sql);
    $sql = "SELECT * FROM ".$dbref."_list_competency WHERE id=".$_POST[$fld];
    $row1 = $ah->mysql_fetch_one($sql);
    $updates .= "<br />" .  $headings['action'][$fld] . " changed from <i>".$row0['value']." </i>to <i>".$row1['value']."</i>";
}

    //uploads
    $original_filename = "";
    $system_filename = "";
    $attachments = "";
    if(isset($_FILES)) {
        $folder = "../files/$cmpcode/".$_SESSION['modref']."/action";
        checkFolder($_SESSION['modref']."/action");
        foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
            if($_FILES['attachments']['error'][$key] == 0) {
                $original_filename = $_FILES['attachments']['name'][$key];
                $ext = substr($original_filename, strrpos($original_filename, '.') + 1);
                $sql = "INSERT INTO ".$dbref."_task_attachments (taskid,logid,original_filename,system_filename,file_location) VALUES ($taskid,0,'$original_filename','','action')";
                $docid = $ah->db_insert($sql);
					$system_filename = $taskid."_".$docid."_".date("YmdHis").".$ext";
					$full_path = $folder."/".$system_filename;
					move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
                $ah->db_update("UPDATE ".$dbref."_task_attachments SET system_filename = '$system_filename' WHERE id = $docid");
				$attachments.= $original_filename."<br />";
            }
        }
    }
    if($attachments != "") {
        $updates .= "<br />".ucfirst($actname)." Attachment(s):<br /> $attachments";
    }
    if($usersChanges != "") {
        $updates = $usersChanges.$updates;
    }

//    echo '<pre style="font-size: 18px">';
//    echo '<p>$updates</p>';
//    print_r($updates);
//    echo '</pre>';

    //echo $updates;die;
    $logstatusid = $_POST['taskstatusid'];
    $logstate = $taskstate;
    $logemail = 'N';
    $logsubmittkid = $tkid;
    $logtaskid = $_POST['taskid'];
    $logtasktkid = '';

    /*
     *  20200619 - PAY ATTENTION!!!!:
     * I'm moving the following insert statement down to the UDF section, so that I can do a check for all updates instead of
     * assuming that there's an update to be made as it stood before, cause that just F*cks up the history logs
     * */
//    $sql = "INSERT INTO ".$dbref."_log
//            (logdate,logtkid,logupdate,logstatusid,logstate,logemail,logsubmittkid,logtaskid,logtasktkid,logactdate,logtype) VALUES
//            ($logdate,$logtkid,'$logupdate',$logstatusid,$logstate,'$logemail','$logsubmittkid',$logtaskid,'$logtasktkid','$logdate','E')";
//    $logid = $ah->db_insert($sql);

    //&*!@!12891edw&*7821
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";

    $these_rows = $ah->mysql_fetch_all($sql);

    if($udf>0) {
        $u = 0;
        foreach($these_rows as $row_index => $row){
            $udfindex[$u] = $row['udfiid'];
            $u++;
        }
    }

    //GET UDF DATA
    if($udf>0) {
        foreach($udfindex as $udfi) {
            if(isset($_POST[$udfi])){
                $udfval[$udfi] = htmlspecialchars($_POST[$udfi],ENT_QUOTES);//str_replace("'","&339",$_POST[$udfi]);
            }
        }
    }
    
    //$udfidArr = array();
    //echo '<pre>';
   // print_r($_POST);die;
    foreach($_POST as $key => $value) {
        if(is_int($key)) {
            $sql = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfindex=$key AND udfnum=".$_POST['taskid']." AND udfref='".$modref."'";
            $these_rows = $ah->mysql_fetch_all($sql);
            if(count($these_rows) > 0){
                $sql = "UPDATE assist_".$cmpcode."_udf SET";
                $sql.= " udfvalue='".$_POST[$key]."' WHERE udfindex=$key AND udfnum=".$_POST['taskid']." AND udfref='".$modref."'";
                $ah->db_update($sql);
            }else{
                $sql = "INSERT INTO assist_".$cmpcode."_udf SET udfindex=$key,udfvalue='".$_POST[$key]."',udfnum=".$_POST['taskid'].",udfref='".$modref."'";
                $ah->db_insert($sql);
            }
           /*
            if($key == -1) {
                $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
                include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                 $udfindex = $row['udfiid'];
                $sql = "INSERT INTO assist_".$cmpcode."_udf SET udfindex=$udfindex,udfvalue='".$_POST[$key]."',udfnum=".$_POST['taskid'].",udfref='".$modref."'";
                include("inc_db_con.php");
            }else {
                $sql = "UPDATE assist_".$cmpcode."_udf SET";
                $sql.= " udfvalue = '".$value."' WHERE udfid=$key";               
                include("inc_db_con.php");
            }*/
        }
    }
    
    $afterUdfFields = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
    $these_rows = $ah->mysql_fetch_all($sql);
    foreach($these_rows as $row_index => $row){
        $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
        $more_rows = $ah->mysql_fetch_all($sql2);
        $udf = count($more_rows);
        $row2 = $ah->mysql_fetch_one($sql2);
        $afterUdfFields[$row['udfivalue']] = '';
        switch($row['udfilist']) {
            case "Y":
                if(checkIntRef($row2['udfvalue'])) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                    $more_sql2_rows = $ah->mysql_fetch_all($sql2);

                    if(count($more_sql2_rows)>0) {
                        $row3 = $ah->mysql_fetch_one($sql2);
                        //echo($row3['udfvvalue']);
                    }
                }
                //echo("<select name=".($row2['udfid'] == "" ? "-1" : $row2['udfid'])."><option value=X>---SELECT---</option>");
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                $more_rows = $ah->mysql_fetch_all($sql2);
                foreach($more_rows as $row_index => $row2){

                    //echo("<option ".($row3['udfvid'] == $row2['udfvid'] ? "selected=selected" : "")." value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                    if(isset($row3) && $row3['udfvid'] == $row2['udfvid'])
                        $afterUdfFields[$row['udfivalue']] = $row2['udfvvalue'];
                }
                //mysql_close($con2);
                //echo("</select>");
                break;
            case "T":
			case "D":
			case "M":
                $afterUdfFields[$row['udfivalue']] = $row2['udfvalue'];
                break;
            default:
                break;
        }
    }

//    echo '<pre style="font-size: 18px">';
//    echo '<p>$udfFields</p>';
//    print_r($udfFields);
//    echo '</pre>';
//
//    echo '<pre style="font-size: 18px">';
//    echo '<p>$afterUdfFields</p>';
//    print_r($afterUdfFields);
//    echo '</pre>';

    $udf_logupdate = "";
    $udf_logs_to_be_made = false;
    foreach($udfFields as $key => $value) {
        if($value != $afterUdfFields[$key]){
            $udf_logupdate .= $key." changed ".($value != null ? "from " : "" )."<i>".$value."</i> to <i>".$afterUdfFields[$key]."</i><br />";
            $udf_logs_to_be_made = true;
        }
    }

    if($updates != "" || $udf_logs_to_be_made === true){
        $logupdate .= " The following changes were made: <br />$updates";
        $logupdate = addslashes(htmlentities($logupdate));

        $sql = "INSERT INTO ".$dbref."_log
                (logdate,logtkid,logupdate,logstatusid,logstate,logemail,logsubmittkid,logtaskid,logtasktkid,logactdate,logtype) VALUES
                ($logdate,$logtkid,'$logupdate',$logstatusid,$logstate,'$logemail','$logsubmittkid',$logtaskid,'$logtasktkid','$logdate','E')";
        $logid = $ah->db_insert($sql);

        if($udf_logs_to_be_made === true){
            $sql = "UPDATE ".$dbref."_log SET logupdate=CONCAT_WS('<br />',logupdate,'$udf_logupdate') WHERE logid=$logid";
            //echo $sql;die;
            $ah->db_update($sql);
        }
    }


    /*$referral_page = $_POST['referal_page'];
        if(strlen($referral_page)==0 || !file_exists($referral_page.".php")) { $referral_page = "view"; }	//Due to error found in some browsers
    echo "<script type='text/javascript'>document.location.href='$referral_page.php?i='+$taskid</script>";*/
	if(isset($_SESSION['ACTION'][strtoupper($modref)]['VIEW']['LIST'])) {
		$url = "view_list.php?get_details=1";
	} else {
		$url = "view.php";
	}
	echo "<script type='text/javascript'>document.location.href='$url';</script>";
?>