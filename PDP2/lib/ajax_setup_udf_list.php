<?php 
$cmpcode = $_SESSION['cc'];
$dbref = $_SESSION['dbref'];
require_once("../../inc_db.php");
require_once("../../inc_db_conn.php");
require_once("../lib/setup_udf_log.php");


$data = array();
switch($_REQUEST['act']) {
case "getListItem":
	$data = getListItem($_REQUEST);
	break;
case "ADD":
	$data = addListItem($_REQUEST);
	break;
case "EDIT":
	$data = editListItem($_REQUEST);
	break;
case "DELETE":
	$data = delListItem($_REQUEST);
	break;
}
//echo "abc";
//$data = array('id'=>0,'value'=>"TEST");
echo json_encode($data);

  

function getListItem($var) {
	$cmpcode = $_SESSION['cc'];
	$id = $var['vid'];
	$sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvid = ".$id;
	$row = mysql_fetch_one($sql);
	$data['value'] = dbdecode($row['udfvvalue']);
	$data['id'] = $id;
	return $data;
}  

  
function addListItem($var) {
	$cmpcode = $_SESSION['cc'];
	$var['udfvvalue'] = dbencode($var['udfvvalue']);
	//$var['udfvindex']
	$var['udfvcode'] = "";
	$var['udfvcomment'] = "";
	$var['udfvyn'] = "Y";
	
	$sql = "INSERT INTO assist_".$cmpcode."_udfvalue
			(udfvid, udfvindex, udfvvalue, udfvcode, udfvcomment, udfvyn) 
		   VALUES 
			(null, ".$var['udfvindex'].", '".$var['udfvvalue']."', '".$var['udfvcode']."', '".$var['udfvcomment']."', '".$var['udfvyn']."')";
	$id = db_insert($sql);
	$flds = array_keys($var);
	logMe($id,serialize($flds),"Added new UDF List Item \'".$var['udfvvalue']."\' (Ref: ".$id.").",serialize(array()),serialize($var),$var['udfvindex'],"C",$sql,"udf");
	
	$data = array('id'=>$id,'value'=>$var['udfvvalue'],'index'=>$var['udfvindex']);
	
	return $data;
}
  
function editListItem($var) {
	$cmpcode = $_SESSION['cc'];
	$udfvid = $var['udfvid'];
	if(is_numeric($udfvid) && strlen($udfvid)>0 && $udfvid>0) {	
		$sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvid = ".$udfvid;
		$old = mysql_fetch_one($sql);
		$sql = "UPDATE assist_".$cmpcode."_udfvalue SET
				udfvvalue = '".dbencode($var['udfvvalue'])."'
				WHERE udfvid = ".$udfvid; 
		$mar = db_update($sql);
		if($mar>0) {
			$var['udfvvalue'] = dbencode($var['udfvvalue']);
			$trans = "Updated List Item $udfvid to \'".$var['udfvvalue']."\' from \'".$old['udfvvalue']."\'";
			logMe($udfvid,"udfvvalue",$trans,$old['udfvvalue'],$var['udfvvalue'],$var['udfvindex'],"E",$sql,"udf");
		}
		$data = array('mar'=>$mar,'id'=>$udfvid,'value'=>$var['udfvvalue'],'index'=>$var['udfvindex']);
	} else {
		$data = array('mar'=>-1,'id'=>$udfvid,'value'=>$var['udfvvalue'],'index'=>$var['udfvindex']);
	}
	return $data;
}
  
  
function delListItem($var) {
	$cmpcode = $_SESSION['cc'];
	$udfvid = $var['udfvid'];
	if(is_numeric($udfvid) && strlen($udfvid)>0 && $udfvid>0) {	
		$sql = "SELECT udfvvalue FROM assist_".$cmpcode."_udfvalue WHERE udfvid = ".$udfvid;
		$row = mysql_fetch_one($sql);
		$sql = "UPDATE assist_".$cmpcode."_udfvalue SET
				udfvyn = 'N'
				WHERE udfvid = ".$udfvid; 
		$mar = db_update($sql);
		if($mar>0) {
			$trans = "Deleted List Item \'".$row['udfvvalue']."\' (Ref: ".$udfvid.").";
			logMe($udfvid,"udfvyn",$trans,"Y","N",$var['udfvindex'],"D",$sql,"udf");
		}
		$data = array('mar'=>$mar,'id'=>$udfvid,'value'=>dbdecode($row['udfvvalue']),'index'=>$var['udfvindex']);
	} else {
		$data = array('mar'=>-1,'index'=>$var['udfvindex']);
	}
	return $data;
}
  
 
  
  
  

  ?>