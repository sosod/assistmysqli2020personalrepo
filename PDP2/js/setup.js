function ValidateStatus(me) {
    var addtxt = me.addtxt.value;
    var astate = me.addstate.value;
    var err = "";
    var valid8 = "true";

    if(addtxt.length > 0)
    {
    }
    else
    {
        err = err + "- Please enter a status description.";
        var target2 = document.getElementById('s2');
        target2.className = "req";
        target2.focus();
        valid8 = "false";
    }


    if(astate.length == 0)
    {
        me.addstate.value = 0;
    }
    else
    {
        if(astate != escape(astate))
        {
            var target1 = document.getElementById('s1');
            target1.className = "req";
            if(err.length > 0)
            {
                err = err+"\n";
            }
            else
            {
                target1.focus();
            }
            err = err+"- Please enter only numbers as the 'Default %'.";
            valid8 = "false";
        }
        else
        {
            if(astate > 99 || astate < 0)
            {
                var target1 = document.getElementById('s1');
                target1.className = "req";
                if(err.length > 0)
                {
                    err = err+"\n";
                }
                else
                {
                    target1.focus();
                }
                err = err+"- 'Default %' must be a number between 0 and 99 (inclusive).";
                valid8 = "false";
            }
        }
    }

    if(err.length>0 || valid8 == "false")
    {
        alert("Please complete all the fields as indicated below:\n"+err);
        return false;
    }
    else
    {
        if(valid8 == "true")
        {
            return true;
        }
    }
return false;
}

function editStatus(id,val) {
    while(val.indexOf("_")>0)
    {
        val = val.replace("_"," ");
    }
    while(val.indexOf("|")>0)
    {
        val = val.replace("|","'");
    }
    if(confirm("Warning: Editing a status will affect all task updates associated with the status.\n\nAre you sure you wish to continue editing '"+val+"'?") == true)
    {
        document.location.href = "setup_status_edit.php?i="+id;
    }
}

function presetAct(id,src) {
	document.location.href = "setup_"+src+"_preset.php?i="+id;
}



function ValidatePresetStatus(me) {
//Required fields: Owner[s]   Recipients   Topic[s]   Urgency[s]   Deadline   Action   UDFs[s]   Attachments[s]   Link[s]

	var message = "";
	
	if(me.title.value.length==0) {
		message += "\n - Please give the Preset a title/name.";
	}
	
	if(me.deadline.value.length==0) {
		message += "\n - Please enter a deadline.";
	} else if(isNaN(parseInt(me.deadline.value))) {
		message += "\n - Please enter a number for the deadline days";
	}

	if(me.action.value.length == 0) { 
		message += "\n - Please enter an instruction.";
	}
	
	if(me.recipients.selectedIndex<0) {
		message += "\n - Please select at least one recipient.";
	}

	if(message.length==0) {
		return true;
	} else {
		alert("Please complete the required fields:"+message);
		return false;
	}
}