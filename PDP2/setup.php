<?php


$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('txt'=>"Defaults"),
);


require_once 'inc_header.php';
$setupObject = new PDP2_SETUP();



//IF SETUP ADMIN TKID WAS SENT THEN PERFORM UPDATE
if(isset($_REQUEST['TA0']) && strlen($_REQUEST['TA0']) > 0) {
	$ta0 = $_REQUEST['TA0'];
    //UPDATE Admin settings & change user access settings as required
    $result = $setupObject->setNewAdminUser($ta0);
}
else
{
    $result = array();
}



?>
<style type=text/css>
table th { text-align: left; vertical-align: top; }
table td { vertical-align: top; border-left-width: 0px; border-right-width: 0px;}
.tdbutton { text-align: right; padding-left: 15px; border-right-width: 1px;}
</style>
<script type=text/javascript>
function goTo(src,page) {
	switch(src) {
		case "L":
			document.location.href = page;
			break;
		case "U":
			//var loc = document.getElementById(page).value;
			document.location.href = "setup_"+page+".php";
			break;
		case "S":
			var loc = document.getElementById(page).value;
			if(page=='columns' && loc == 'HOME') {
				page = page + "_home";
			}
			document.location.href = "setup_"+page+".php?section="+loc;
			break;
	}
}
</script>
<style type="text/css">
    #div_left, #div_right {
        padding: 0px 20px 20px 20px;
        width: 45%;
        height: 100%;
    }
    #div_container {
        padding: 0px 10px 0px 10px;
    }
</style>
<?php ASSIST_HELPER::displayResult($result); ?>
<?php

$headingObject = new PDP2_HEADINGS();
$list_names = $headingObject->getStandardListHeadings();
//ASSIST_HELPER::arrPrint($list_names);
?>
<div id=div_container>
    <div id=div_right>
        <h2>Lists</h2>
        <table class="form middle" >
            <tr>
                <th>Status:</th>
                <td>Configure the Status list items.</td>
                <td class=tdbutton><input type=button value=Configure onclick="goTo('L','setup_status.php');"></td>
            </tr>
            <?php foreach($list_names as $key => $val){ ?>
                <?php if($key != "status"){ ?>
                    <tr>
                        <th><?php echo $val; ?>:</th>
                        <td>Configure the <?php echo ucfirst(str_replace("_", " ", $key)); ?> list items.</td>
                        <td class=tdbutton><input type=button value=Configure class=btn_list id=<?php echo $key; ?> /></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </table>
    </div>
    <div id=div_left>
        <h2>Module Settings</h2>
        <form name=update method=post action=setup.php>
            <table class="form middle" >
                <tr>
                    <th width=150>Administrator:</th>
                    <td><select name=TA0>
                            <?php
                            //GET SETUP ADMIN TKID FROM ASSIST-SETUP
                            $sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = '$modref' AND refid = 0";
                            $row = $setupObject->mysql_fetch_one($sql);
                            $t = isset($row['value']) ? 1 : 0;
                            if($t > 0) {
                                //IF RESULT THEN GET TKID
                                $taadmin = $row['value'];
                            } else {
                                //IF NO RESULT THEN SET SQL VARIABLE TO ADD DEFAULT 0000
                                $sql = "INSERT INTO assist_".$cmpcode."_setup SET ref = '$modref', refid = 0, value = '0000', comment = 'Setup administrator', field = ''";
                                $setupObject->db_insert($sql);
                                $taadmin = "0000";
                            }
                            //SET THE SELECTED OPTION IF SETUP ADMIN = 0000
                            if($taadmin == "0000") {
                                echo "<option selected value=0000>Assist Administrator</option>";
                            } else {
                                echo "<option value=0000>Assist Administrator</option>";
                            }
                            //GET USERS (NOT 0000) THAT HAVE ACCESS TO TA AND COULD POTENTIALLY BE SETUP ADMIN
                            $rows = $setupObject->getAvailableAdminUsers();
                            foreach($rows as $row) {
                                $tid = $row['tkid'];
                                if($tid == $taadmin)    //IF THE USER IS SETUP ADMIN THEN SELECT THAT OPTION
                                {
                                    echo("<option selected value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
                                }
                                else
                                {
                                    echo("<option value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
                                }
                            }
                            ?>
                        </select></td>
                    <td class=tdbutton><input type=submit value="  Save  "></td>
                </tr>
                <tr>
                    <th>User Defined Fields<br />(UDFs):</th>
                    <td>Configure additional fields.</td>
                    <td class=tdbutton>
                        <input type=button value=Configure onclick="goTo('U','udf');">
                    </td>
                <tr>
                    <th>Sort User Defined Fields (UDFs):</th>
                    <td>Configure the order of User Defined Fields.</td>
                    <td class=tdbutton>
                        <input type=button value="Configure Order" onclick="goTo('U','udf_headings_sort');">
                    </td>
                </tr>
                <tr>
                    <th>List Columns:</th>
                    <td>Configure which columns display on List pages.</td>
                    <td class=tdbutton><select id=columns>
                            <option selected value=my>My <?php echo ucfirst($actname);?>s</option>
                            <option value=own><?php echo ucfirst($actname);?>s Owned</option>
                            <option value=all>All <?php echo ucfirst($actname);?>s</option>
                            <option value=HOME>Home Page List</option>
                        </select><br /><input type=button value=Configure onclick="goTo('S','columns');"></td>
                </tr>
            </table>
        </form>
    </div>
</div> <!-- end div container -->
<script type=text/javascript>
    $(function() {
        $("h2").css("margin-top","15px");

        $("input:button").button().css("font-size","75%");
        $("input:button.btn_setup").click(function() {
            document.location.href = "setup_defaults_"+$(this).prop("id")+".php";
        });
        $("input:button.btn_list").click(function() {
            document.location.href = "setup_list.php?list="+$(this).prop("id");
        });
        $("input:button.btn_report").click(function() {
            document.location.href = "setup_defaults_"+$(this).prop("id")+".php";
        });
        $("#div_right").css("float","right");
        $("#div_left, #div_right").addClass("ui-widget-content ui-corner-all");
        $("table.form").css({"width":"100%"});
        $(window).resize(function() {
            var x = 0;
            $("#div_right").children().each(function() {
                x+=AssistString.substr($(this).css("height"),0,-2)*1;
            });
            $("#div_container").css("height",(x+50)+"px");
        });
        $(window).trigger("resize");
    });
</script>

<script type=text/javascript>
$(function() {
	$("#setup_datareport").click(function() {
		document.location.href = 'setup_datareport.php';
	});
});
</script>
</body>

</html>
