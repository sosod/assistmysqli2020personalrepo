<?php 
/*
require_once '../inc_db_conn.php';
require_once '../inc_codehelper.php';
require_once 'inc_ta.php';
require_once "../library/class/autoload.php";
require_once "inc_ignite.php";
*/

require_once ("../module/autoloader.php");
$ah = new PDP2();
$taadmin = $ah->getAdministratorUserID();
$taact = $ah->getMyUserAccess();
$rows_per_page = 15;
$dbref = $ah->getDBRef();
$cmpcode = $ah->getCmpCode();
$modref = $ah->getModRef();
$tkid = $ah->getUserID();
$actname = $ah->getActionName();
$tkname = $ah->getUserName();
$today = time();


require_once "inc.php"; //contains display functions


//$adb = new ASSIST_DB("client");
//error_reporting(-1);

//UDF DEFAULTS
$udf_options = array(
	'udfilist' => array(
		'Y' => array('txt'=>"List",'type'=>"select",'align'=>"left"),
		'T' => array('txt'=>"Small Text",'type'=>"text",'align'=>"left"),
		'M' => array('txt'=>"Large Text",'type'=>"textarea",'align'=>"left"),
		'N' => array('txt'=>"Number",'type'=>"number",'align'=>"right"),
		'D' => array('txt'=>"Date",'type'=>"date",'align'=>"center"),
	),
	'udfirequired' => array(
		0 => "No",
		1 => "Yes",
	),
	'udfiobject' => array(
		'action' => $actname,
		'update' => "Update",
	),
	'udfilinkfield' => array(
		'all'		=> array("tasktopicid","taskstatusid"),
		'action'	=> array("tasktopicid"),
		'update'	=> array("tasktopicid","taskstatusid"),
		'headings'	=> array(),
		'tables'	=> array(
			'tasktopicid'	=> array(
				'tbl'		=> "list_topic",
				'id'			=> "pkey",
				'value'			=> "value",
				'active_fld'	=> "yn",
				'active'		=> "Y",
			),
			'taskstatusid'	=> array(
				'tbl'		=> "list_status",
				'id'			=> "id",
				'value'			=> "value",
				'active_fld'	=> "yn",
				'active'		=> "Y",
			),
		),
	),
);

if(isset($get_udf_link_headings) && $get_udf_link_headings) {
	$sql = "SELECT headingshort as display, field 
			FROM ".$dbref."_list_display 
			WHERE yn = 'Y' AND field IN ('".implode("','",$udf_options['udfilinkfield']['all'])."')";
	$udf_options['udfilinkfield']['headings'] = $ah->mysql_fetch_all_fld($sql,"field");
}
//GET VALID UDFS
$udf_index = array('action'=>array('ids'=>array(),'index'=>array()),'update'=>array('ids'=>array(),'index'=>array()));
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$modref."' AND udfiyn = 'Y'";
$rows = $ah->mysql_fetch_all($sql);
foreach($rows as $row) {
	$obj = strlen($row['udfiobject'])>0 ? $row['udfiobject'] : "action";
	$udf_index[$obj]['ids'][] = $row['udfiid'];
	$udf_index[$obj]['index'][$row['udfiid']] = $row;
}

$headings_object = new PDP2_HEADINGS();
$headings = $headings_object->getHeadingNames();

if(!isset($page) || !in_array(implode("_",$page),array("report_process","new_multiple_generate"))) {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Action4u.co.za</title>
</head>
		<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
		<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>        
		<script type ="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery.min.js"></script>
		<script type ="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery-ui.min.js"></script>
		<link href="/library/jquery-ui-1.8.24/css/jquery-ui.css" rel="stylesheet" type="text/css" />

		<script type ="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery-ui-timepicker-addon-0.9.8.js"></script>		
		<link href="/library/jquery/css/jquery-timepicker.css" rel="stylesheet" type="text/css" />

		<script type ="text/javascript" src="/library/js/assisthelper.js"></script>
		<script type ="text/javascript" src="/library/js/assiststring.js"></script>
		<script type ="text/javascript" src="/library/js/assistarray.js"></script>
		<script type ="text/javascript" src="/library/js/assistform.js"></script>
		<script type ="text/javascript" src="/assist.js"></script>

	<link rel="stylesheet" media="screen" type="text/css" href="../lib/jquery/css/colorpicker.css" />
	<script type="text/javascript" src="../lib/jquery/js/colorpicker.js"></script>

		<link rel="stylesheet" href="/assist.css?<?php echo time(); ?>" type="text/css" />
<?php 
	include("inc_css.php"); 
	include("inc_js.php");
?>

<?php /**********************************************************************************************************************/ ?>
<?php

if(!isset($page)) {
    $page = explode("/",$_SERVER['PHP_SELF']);
    $page = $page[count($page)-1];
    if(strpos($page,".php")!==false) {
        $page = substr($page,0,-4);
    }
    $page = explode("_",$page);
}
$available_menu = array('setup');
?>
<?php if(in_array($page[0],$available_menu)) {//START STYLES FOR THE UDF ORDER PAGE TAKEN FROM SDBIP ?>
    <?php
    $s = array("/library/js/assist.ui.paging.js?".time(),
        "/library/jquery-plugins/jscolor/jscolor.js");
    if(isset($scripts)) {
        $scripts = array_merge($s, $scripts);
    } else {
        $scripts = $s;
    }
    ASSIST_HELPER::echoPageHeader("1.10.0", $scripts, array("/assist_jquery.css?".time(), "/assist3.css?".time()));
    ?>
    <script type="text/javascript">
        $(document).ready(function() {
            //format container and sub-container tables to hide borders
            //$("table.tbl-container").addClass("noborder");
            $("table.tbl-container:not(.not-max)").css("width", "100%");
            //$("table.tbl-container td").addClass("noborder").find("table:not(.tbl-container) td").removeClass("noborder");
            $("table.th2").find("th").addClass("th2");
            $("table tr.th2").find("th").addClass("th2");
            $("table.tbl_audit_log").css("width", "100%").find("td").removeClass("noborder");
            $("table.tbl-subcontainer, table.tbl-subcontainer td").addClass("noborder");

            //$("select").css("padding","1px");

            //jquery ui button formatting
            $("button.abutton").children(".ui-button-text").css({"padding-top": "0px", "padding-bottom": "0px"});


            $("div.tbl-error").addClass("ui-state-error").find("h1").addClass("red");
            $("div.tbl-ok").addClass("ui-state-ok").find("h1").addClass("green");
            $("div.tbl-info").addClass("ui-state-info").find("h1").addClass("orange");
        });

    </script>

    <style type="text/css">
        /* jquery ui button styling */
        /* used by  setup > useraccess */
        #tbl_useraccess .button_class, #tbl_useraccess .ui-widget.button_class {
            font-size: 75%;
            padding: 1px;
        }

        table.tbl-container, table.tbl-container > tbody > tr > td {
            border: 0px solid #ffffff;
        }

        .small-button {
            font-size: 75%;
            padding: 1px;
        }


        /* tbl-ok/error styling - used by Create for user notifications */
        /* generic settings */
        table.tbl-ok, table.tbl-error, div.tbl-ok, div.tbl-error, div.tbl-info {
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
            margin: 50px auto;
            border-collapse: inherit;
        }

        table.tbl-ok td, table.tbl-error td {
            border: 0px solid #ffffff;
        }

        div.tbl-ok, div.tbl-error, div.tbl-info {
            padding: 0px 15px 10px 15px;
        }

        div.tbl-ok p, div.tbl-error p, div.tbl-info p {
            font-size: 150%;
            line-height: 165%;
        }


        /* green = ok */
        table.tbl-ok {
            border: 2px solid #009900;
        }

        div.tbl-ok {
        }

        /* red = error */
        table.tbl-error {
            border: 2px solid #009900;
        }

        div.tbl-error {
        }

        /* orange = info */
        table.tbl-info {
            border: 2px solid #fe9900;
        }

        div.tbl-info {
        }


        button.cancel_attach {
            font-size: 75%;
            padding: 1px;
        }


        /* Results colours */
        .result {
            font-weight: bold;
            color: #ffffff;
            padding: 2px 4px 2px 4px;
            text-align: center;
            margin: 0px;
        }

        .result0 {
            background-color: #777777;
        }

        .result1 {
            background-color: #cc0001;
        }

        .result2 {
            background-color: #fe9900;
        }

        .result3 {
            background-color: #009900;
        }

        .result4 {
            background-color: #005500;
        }

        .result5 {
            background-color: #000077;
        }

    </style>
<?php } //END STYLES FOR THE UDF ORDER PAGE TAKEN FROM SDBIP?>
<?php /**********************************************************************************************************************/ ?>

<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><?php

	if(!isset($page)) {
		$page = explode("/",$_SERVER['PHP_SELF']);
		$page = $page[count($page)-1];
		if(strpos($page,".php")!==false) {
			$page = substr($page,0,-4);
		}
		$page = explode("_",$page);
	}
	$available_menu = array('setup', 'new');

	if(in_array($page[0],$available_menu)) {
        $menuObject = new PDP2_MENU();
        $menu = $menuObject->getPageMenuArrayForPDP($page);
		$level = 1;
		$ah->echoNavigation($level,$menu);
	}	//end if in available menu

	if(isset($title)) {
		echo "<h1 id=h_title>";
		$new_title = array();
		foreach($title as $t) {
			$nt = "";
			if(isset($t['url']) && strlen($t['url'])>0) {
				$nt.="<a href='".$t['url']."' class=breadcrumb>";
			}
			$nt.=$t['txt']."</a>";
			$new_title[] = $nt;
		}
		echo implode(" ".$ah->getBreadcrumbDivider()." ",$new_title);
		echo "</h1>";
	}

}	//end not if report_process
?>