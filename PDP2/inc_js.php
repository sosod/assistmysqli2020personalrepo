<script type=text/javascript>
$(function() {
	$("tr").hover(
		function(){ $(this).addClass("trhover"); },
		function(){ $(this).removeClass("trhover"); }
	);
	$(".datetime").datetimepicker({
					timeFormat: 'hh:mm',
					dateFormat: 'dd-M-yy',
					maxDate: new Date(),
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    changeMonth:true,
                    changeYear:true		
				});

});
function goNext(url,s,f) {
    f = escape(f);
    document.location.href = url+"s="+s+"&f="+f;
}
function hovCSS(me) {
	document.getElementById(me).className = 'tdhover';
}
function hovCSS2(me) {
	document.getElementById(me).className = 'blank';
}
</script>