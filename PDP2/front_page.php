<?php

function tableAct($dbref,
	$cmpcode = $_SESSION['cc'];
	$action_profile = $_SESSION['USER_PROFILE'][1];

				$head = array(
					'ref'=>array('text'=>"Ref"),
					'taskdeadline'=>array('text'=>"Deadline"),
					'tasktopicid'=>array('text'=>""),
					'taskstatusid'=>array('text'=>""),
					'taskaction'=>array('text'=>""),
					'taskdeliver'=>array('text'=>""),
					'link'=>array('text'=>""),
				);
				$sql = "SELECT field, headingshort FROM ".$dbref."_list_display WHERE yn = 'Y' AND field in ('tasktopicid','taskstatusid','taskaction','taskdeliver') ORDER BY mysort";
				$rs = getRS($sql);
					while($row = mysql_fetch_array($rs)) {
						$head[$row['field']]['text'] = $row['headingshort'];
					}
				unset($rs);
unset($head['taskdeliver']);
				$actions = array();
				
					$sql = "SELECT t.*, tkname, tksurname, top.value as topic, stat.value as status ";
					$sql.= "FROM ".$dbref."_task t";
					$sql.= ", ".$dbref."_task_recipients tr";
					$sql.= ", ".$dbref."_list_topic top";
					$sql.= ", ".$dbref."_list_status stat";
					$sql.= ", assist_".$cmpcode."_timekeep ";
					$sql.= "WHERE t.taskadduser = tkid ";
					$sql.= "AND tr.tasktkid = '$tkid' AND tr.taskid = t.taskid ";
					$sql.= "AND t.tasktopicid = top.id ";
					$sql.= "AND t.taskstatusid = stat.pkey ";
					$sql.= "AND t.taskstatusid > 2 AND t.taskdeadline <= ($today + 3600*24*7) ORDER BY ";
					switch($action_profile['field3']) {
						case "dead_desc":	$sql.= " t.taskdeadline DESC "; break;
						default:
						case "dead_asc":	$sql.= " t.taskdeadline ASC "; break;
					}
					$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
					$rs = getRS($sql);
						$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
						while($task = mysql_fetch_array($rs)) {
							$obj_id = $task['taskid'];
							$actions[$obj_id] = array(
								'ref'=>$obj_id,
								'taskdeadline'=>"",
								'tasktopicid'=>"",
								'taskstatusid'=>"",
								'taskaction'=>"",
								'taskdeliver'=>"",
								'link'=>""
							);
									if($task['taskstatusid']!=5) {
										$deaddiff = $today - $task['taskdeadline'];
										$diffdays = floor($deaddiff/(3600*24));
										if($deaddiff<0) {
											$diffdays*=-1;
											$days = $diffdays > 1 ? "days" : "day";
											$actions[$obj_id]['taskdeadline'] = "<span class=soon>Due in $diffdays $days</span>";
										} elseif($deaddiff==0) {
											$actions[$obj_id]['taskdeadline'] = "<span class=today><b>Due today</b></span>";
										} else {
											$days = $diffdays > 1 ? "days" : "day";
											$actions[$obj_id]['taskdeadline'] = "<span class=overdue><b>$diffdays $days</b> overdue</span>";
										}
									}
								foreach($head as $fld => $h) {
									if(!in_array($fld,array("ref","link","taskdeadline"))) {
										$val = $task[$fld];
										switch($fld) {
											case "taskstatusid":
												$val = $task['status'];
												if($task['taskstate']>=0) { $val.="<br />(".$task['taskstate']."%)"; }
												break;
											case "tasktopicid":
												$val = $task['topic'];
												break;
											case "taskadduser":
												$val = $task['tkname']." ".$task['tksurname'];
												break;
											default:
												$val = decode($val);
												if(strlen($val)>100) { $val = strFn("substr",$val,0,75)."..."; }
												$val = str_replace(chr(10),"<br />",$val);
												break;
										}
										$actions[$obj_id][$fld] = $val;
									}
								}
							//On button click, call function viewTask & pass variables ($mod, $modloc, page_address)
							$actions[$obj_id]['link'] = "view_task_update.php?i=".$task['taskid'];
						}	//while end
					unset($rs);
				
}	//end table function
?>