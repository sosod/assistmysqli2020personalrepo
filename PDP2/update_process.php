<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
	array('txt'=>"Update"),
);
$page = array("update");
$get_udf_link_headings = false;

$redirect_me = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : "module";

require_once 'inc_header.php';

$actionObject = new PDP2_ACTION();
//arrPrint($_REQUEST);
echo "<p>Starting update...";
$taskid = $_REQUEST['logtaskid'];
$logtaskid = $taskid;

echo "<P>1. Saving update</P>";
$logid = $actionObject->saveUpdate($_REQUEST);

$return_array = $actionObject->return_array;
$logupdate = $return_array['logupdate'];
$logstate = $return_array['logstate'];
$logactdate = $return_array['logactdate'];
//$udf_email = $return_array[''];

echo "<P>2. Uploading attachments.</p>";
$path = $_SESSION['modref']."/update";
$actionObject->checkFolder($path);
$path = "../files/".$cmpcode."/".$path;
$attach_count = 0;
$next_docid = $actionObject->getNextAttachmentID();
foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
	if(strlen($tmp_name)>0 && $_FILES['attachments']['error'][$key] == 0) {
		$original_filename = $_FILES['attachments']['name'][$key];
		$parts = explode(".", $original_filename);
		$file = $parts[0];
		$ext = isset($parts[count($parts)-1]) ? ".".$parts[count($parts)-1] : "";
		$system_filename = $taskid."_update_".$logid."_".$next_docid."_".date("YmdHis").$ext;
		$full_path = $path."/".$system_filename;
		move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
		$actionObject->addObjectAttachment($logid,$original_filename,$system_filename,"update");
		$next_docid++;
	}
}




$task = getTask($taskid);


$recipients = array('ids'=>array(),'rec'=>array());
//get task owner details
if($task['taskadduser']!=$tkid) {
	$sql = "SELECT tkid, tkemail, CONCAT_WS(' ',tkname,tksurname) as name FROM assist_".$cmpcode."_timekeep WHERE tkid = ".$task['taskadduser']." AND tkstatus = 1 AND tkemail <> ''";
	$row = $ah->mysql_fetch_one($sql);
	if(isset($row['tkemail'])) {
		$recipients['rec'][] = $row['name']." <".$row['tkemail'].">";
		$recipients['ids'][] = $row['tkid'];
	}
}
//get other recipient details
$sql = "SELECT tkid, tkemail, CONCAT_WS(' ',tkname,tksurname) as name
		FROM assist_".$cmpcode."_timekeep 
		INNER JOIN ".$dbref."_task_recipients 
		  ON tkid = tasktkid
		  AND taskid = ".$logtaskid."
		  AND tasktkid <> '".$tkid."'
		  AND tasktkid <> '".$task['taskadduser']."'
		WHERE tkstatus = 1 AND tkemail <> ''";
$rows = $ah->mysql_fetch_all($sql);
if(count($rows)>0) {
	foreach($rows as $row) {
		$recipients['rec'][] = array('name'=>$row['name'],'email'=>$row['tkemail']);
		$recipients['ids'][] = $row['tkid'];
	}
}

//arrPrint($recipients);

if(count($recipients['rec'])>0) {
	echo "<P>4. Sending notifications</p>";
	//$userFrom = mysql_fetch_one("SELECT CONCAT(tkname,' ',tksurname,' <',tkemail,'>') as userfrom FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$tkid."' AND tkstatus = 1 AND tkemail <> ''");
	$userF = $ah->mysql_fetch_one("SELECT CONCAT(tkname,' ',tksurname) as name, tkemail as email FROM  assist_".$cmpcode."_timekeep WHERE tkid = '".$tkid."' AND tkstatus = 1 AND tkemail <> ''");

	$sql = "SELECT * 
			FROM assist_".$cmpcode."_udfindex
			 LEFT OUTER JOIN assist_".$cmpcode."_udf
			 ON udfindex = udfiid 
			 AND udfref = '".strtoupper($_SESSION['modref'])."'
			 AND udfnum = $logtaskid
			WHERE udfiobject = 'action'
			AND udfiyn = 'Y'
			AND udfiref = '".strtoupper($_SESSION['modref'])."'
			AND (
			  udfilinkfield = '' 
			  OR (udfilinkfield = 'tasktopicid' AND udfilinkref = ".$task['tasktopicid'].")
			)
			ORDER BY udfisort, udfivalue";
	$task_udfs = $ah->mysql_fetch_all($sql);
	$udf_lists = getUDFListItems("");

	$subject = decode($_SESSION['modtext']." $actname $logtaskid Updated");
$message = $_SESSION['modtext']." $actname $logtaskid has been updated by ".$_SESSION['tkn'].".\n
Update details
Message: ".str_replace(chr(10),chr(10)."   ",decode($logupdate))."
Status: ".$task['status']." (".$logstate."%) 
Date of Activity: ".date("d-M-Y H:i",$logactdate);
	foreach($udf_email as $key => $r) {
		$message.="\n".$udf_index['update']['index'][$key]['udfivalue'].": ";
		switch($udf_index['update']['index'][$key]['udfilist']) {
		case "N": $message.=number_format($r,2,".",","); break;
		case "D": $message.= (strlen($r)>0 && is_numeric($r) ? date("d-M-Y",$r) : $r); break;
		case "Y": $message.=(isset($udf_lists[$key][$r]) ? decode($udf_lists[$key][$r]['udfvvalue']) : "[Unspecified]"); break;
		default: $message.=$r;
		}
	}
$message.=($attach_count>0 ? "\n".$attach_count." file(s) were attached to this update.  Please access the $actname on Assist in order to view the attachment(s)." : "")."

$actname details
Created: ".date("d-M-Y H:i:s",$task['taskadddate'])."
Topic: ".$task['topic']."
Deadline: ".date("d-M-Y",$task['taskdeadline'])."
Instructions: ".str_replace(chr(10),chr(10)."   ",decode($task['taskaction']))."
Deliverables: ".str_replace(chr(10),chr(10)."   ",decode($task['taskdeliver']));
	foreach($task_udfs as $tu) {
		$message.="\n".$tu['udfivalue'].": ";
		if(strlen($tu['udfvalue'])==0 || ($tu['udfilist']=="Y" && !checkIntRef($tu['udfvalue']))) {
			$message.="[Unspecified]";
		} else {
			$r = $tu['udfvalue'];
			$key = $tu['udfindex'];
			switch($tu['udfilist']) {
			case "N": $message.=number_format($r,2,".",","); break;
			case "D": $message.= (strlen($r)>0 && is_numeric($r) ? date("d-M-Y",$r) : $r); break;
			case "Y": $message.=(isset($udf_lists[$key][$r]) ? decode($udf_lists[$key][$r]['udfvvalue']) : "[Unspecified]"); break;
			default: $message.=$r;
			}
		}
	}
$message.="

Please log onto Assist to view the full history of this $actname.";
	$cc = "";
    $bcc = "";
	$reply_to = $userF;//array('name'=>$userFrom,'email'=>$from);
	$toEmails= $recipients['rec'];
    $mail = new ASSIST_EMAIL($toEmails, $subject, $message, "TEXT", $cc, $bcc, $reply_to);
	$mail->sendEmail();
		
	//}
}

echo "<P>Update complete!</p>";
if(isset($_REQUEST['src']) && strlen($_REQUEST['src'])>0) {
	if($redirect_me=="dashboard" || $_REQUEST['src']=="home") {
		echo "
		<script type=text/javascript>
			parent.header.$('#backHome').trigger('click');
		</script>";
	} else {
		echo "<script type=text/javascript>document.location.href = 'view_list.php?get_details=1';</script>";
	}
}


?>