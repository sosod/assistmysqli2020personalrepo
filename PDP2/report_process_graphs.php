<?php
$tasktkid = $_POST['graphtasktkid'];
$taskadduser = $_POST['graphtaskadduser'];
$taskurgencyid = $_POST['graphtaskurgencyid'];
$tasktopicid = $_POST['graphtasktopicid'];
$datecreatedopt = $_POST['graphdatecreatedopt'];
$datecreatedFrom = $_POST['graphdatecreatedFrom'];
$datecreatedTo = $_POST['graphdatecreatedTo'];
$deadlinedateopt = $_POST['graphdeadlinedateopt'];
$deadlineFrom = $_POST['graphdeadlineFrom'];
$deadlineTo = $_POST['graphdeadlineTo'];
$graphtype = $_POST['graphtype'];
$groupby = $_POST['groupby'];
$field_filter = @$_POST['field_filter'];
$udfs = $_POST['udfs'];
$udfvalue = @$_POST['udfvalue'];

$param = "tasktkid=$tasktkid&taskadduser=$taskadduser&taskurgencyid=$taskurgencyid&tasktopicid=$tasktopicid&datecreatedopt=$datecreatedopt";
$param .= "&datecreatedFrom=$datecreatedFrom&datecreatedTo=$datecreatedTo&deadlinedateopt=$deadlinedateopt&deadlineFrom=$deadlineFrom";
$param .= "&deadlineTo=$deadlineTo&graphtype=$graphtype&groupby=$groupby&field_filter=$field_filter&udf=$udfs";
$url = 'http://'.$_SERVER['SERVER_NAME'].dirname($_SERVER['REQUEST_URI'])."/report_process_graphs.xml.php";

$data = array(
    'tasktkid'=>$tasktkid,
    'taskadduser'=>$taskadduser,
    'taskurgencyid'=>$taskurgencyid,
    'tasktopicid'=>$tasktopicid,
    'datecreatedopt'=>$datecreatedopt,
    'datecreatedFrom'=>$datecreatedFrom,
    'datecreatedTo'=>$datecreatedTo,
    'deadlinedateopt'=>$deadlinedateopt,
    'deadlineFrom'=>$deadlineFrom,
    'deadlineTo'=>$deadlineTo,
    'graphtype'=>$graphtype,
    'groupby'=>$groupby,
    'field_filter'=>$field_filter,
    'udf'=>$udfs
);
$options = array(
  'http' => array(
      'method'=>'POST',
      'content'=>http_build_query($data)
      )
);
file_get_contents($url,false,stream_context_create($options));
if(isset($udfvalue))
    $param .= "&udfvalue=$udfvalue";
header("Location:report_process_graphs.xml.php?$params");
$folder = "../files/$cmpcode/$modref";
define("AMCHARTS_DIR","../lib/amcharts-php-0.2.1/amcharts");
define("AMCHART_SETTINGS",AMCHARTS_DIR."/chart_settings");
/*
 * configuration for the graph.
 * 
*/
$graph_settings = array
        (
        '3dcolumn'=>array
        (
                'name_path'=>AMCHARTS_DIR."/amcolumn_1.6.4.2/amcolumn",
                'settings_file'=>AMCHART_SETTINGS.'/vert_amcolumn_settings.xml',
                'swf_file'=>'amcolumn.swf'
        ),
        '3dbarchart'=>array
        (
                'name_path'=>AMCHARTS_DIR."/amcolumn_1.6.4.2/amcolumn",
                'settings_file'=>AMCHART_SETTINGS.'/hor_amcolumn_settings.xml',
                'swf_file'=>'amcolumn.swf'
        ),
        'pie'=>array
        (
                'name_path'=>AMCHARTS_DIR."/ampie_1.6.4.1/ampie",
                'settings_file'=>AMCHART_SETTINGS.'/ampie_settings1.xml',
                'swf_file'=>'ampie.swf'
        )

);
/*
echo "<script type='text/javascript' src='".$graph_settings[$graphtype]['name_path']."/swfobject.js'></script>
        <!-- this id must be unique! -->
	<div id='flashcontent1'>
		<strong>You need to upgrade your Flash Player</strong>
	</div>
	<script type='text/javascript'>
		// <![CDATA[
		var so = new SWFObject('".$graph_settings[$graphtype]['name_path']."/".$graph_settings[$graphtype]['swf_file']."', '".$graphtype."', '100%', '100%', '8', '#FFFFFF');
		so.addVariable('path', '".$graph_settings[$graphtype]['name_path']."/');
		so.addVariable('settings_file', encodeURIComponent('".$graph_settings[$graphtype]['settings_file']."'));
		so.addVariable('data_file', encodeURIComponent('report_process_graphs.xml.php?".$param."'));
		so.write('flashcontent1');   // this id must match the div id above
		// ]]>
	</script>";*/

class Query
{
    private $where = "taskstatusid <> 2";
    private $tasktkid;
    private $taskadduser;
    private $taskurgencyid;
    private $tasktopicid;
    private $datecreatedFrom;
    private $datecreatedTo;
    private $datecreatedopt;
    private $deadlinedateopt;
    private $deadlineFrom;
    private $deadlineTo;
    private $graphtype;
    private $groupby;
    private $field_filter;
    private $from;
    private $dbref;
    private $sql;
    private $cmpcode;
    private $statuses = array();
    private $data;
    private $result;
    private $udf;
    public function __construct()
    {
        // echo '<pre>';
        // print_r($_POST);
        $this->tasktkid = $_POST['graphtasktkid'];
        // var_dump($this->tasktkid);
        $this->taskadduser = $_POST['graphtaskadduser'];
        $this->taskurgencyid = $_POST['graphtaskurgencyid'];
        $this->tasktopicid = $_POST['graphtasktopicid'];
        $this->datecreatedFrom = $_POST['graphdatecreatedFrom'];
        $this->datecreatedTo = $_POST['graphdatecreatedTo'];
        $this->datecreatedopt = $_POST['graphdatecreatedopt'];
        $this->deadlinedateopt = $_POST['deadlinedateopt'];
        $this->deadlineFrom = $_POST['graphdatecreatedFrom'];
        $this->deadlineTo = $_POST['graphdeadlineTo'];
        $this->graphtype = $_POST['graphtype'];
        $this->groupby = $_POST['groupby'];
        $this->field_filter = $_POST['field_filter'];
        $this->udf = $_POST['udfs'];
        $this->udfvalue = $_POST['udfvalue'];
        $this->dbref=$_SESSION['dbref'];
        $this->setWhereClause();
        $this->setFromClause();
        $this->sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt ";
        $this->cmpcode = $_SESSION['cc'];
    }
    function getData()
    {
        return $this->data;
    }
    function getGraphType()
    {
        return $this->graphtype;
    }
    public function getStatuses()
    {
        $sql = "SELECT s.value, s.pkey FROM ".$this->dbref."_list_status s WHERE pkey <> 2 ORDER BY s.sort";
        $this->execQuery($sql);
        while($row = mysql_fetch_array($this->result))
        {
            $this->statuses[$row['pkey']] = $row['value'];
        }
        return $this->statuses;
    }
    public function setFromClause()
    {
        if(isset ($this->tasktkid) AND $this->tasktkid != 'X')
        {
            $this->from  .= " INNER JOIN ".$this->dbref."_task_recipients r ON (t.taskid=r.taskid) ";
        }
        if(isset($this->udf) AND $this->udf != 'X')
        {
            $this->from .= " INNER JOIN assist_".$this->cmpcode."_udf udf ON(t.taskid=udf.udfnum) ";
        }
    }
    public function getFromClause()
    {
        return $this->from;
    }
    public function setWhereClause()
    {

        if(isset ($this->tasktkid) AND $this->tasktkid != 'X')
        {
            $this->where .= " AND r.tasktkid = '".$this->tasktkid."' ";
        }
        if(isset($this->taskadduser) AND $this->taskadduser != 'X')
        {
            $this->where .= " AND t.taskadduser = '".$this->taskadduser."' ";
        }
        if(isset($this->taskurgencyid) AND $this->taskurgencyid != 'X')
        {
            $this->where .= " AND t.taskurgencyid = $this->taskurgencyid ";
        }
        if(isset($this->tasktopicid) AND $this->tasktopicid != 'X')
        {
            $this->where .= " AND t.tasktopicid = $this->tasktopicid ";
        }

        if(isset($this->datecreatedopt) AND $this->datecreatedopt == 'Exact')
        {
            $fromTimeStamp = strtotime($this->datecreatedFrom);
            $toTimeStamp = strtotime($this->datecreatedTo);
            $begin = mktime(0,0,0,date("n",$fromTimeStamp),date("j",$fromTimeStamp),date("y",$fromTimeStamp));
            $end = mktime(23,59,59,date("n",$toTimeStamp),date("j",$toTimeStamp),date("y",$toTimeStamp));
            $this->where .=" AND t.taskadddate >='".$begin."' AND t.taskadddate <='".$end."' ";
        }
        if(isset($this->deadlinedateopt) AND $this->deadlinedateopt == 'Exact')
        {
            $fromTimeStamp = strtotime($this->deadlineFrom);
            $toTimeStamp = strtotime($this->deadlineTo);
            $begin = mktime(0,0,0,date("n",$fromTimeStamp),date("j",$fromTimeStamp),date("y",$fromTimeStamp));
            $end = mktime(23,59,59,date("n",$toTimeStamp),date("j",$toTimeStamp),date("y",$toTimeStamp));
            $this->where .= " AND t.taskdeadline >='".$begin."' AND t.taskdeadline <= '".$end."' ";
        }
        if($this->udf != 'X' )
        {
            $sql = "SELECT * FROM assist_".$this->cmpcode."_udfindex x WHERE x.udfiid=".$this->udf;
            $this->execQuery($sql);
            $row = mysql_fetch_array($this->result);
            switch ($row['udfilist'])
            {
                case 'T':
                case 'M':
                    $this->where .= " AND udf.udfvalue LIKE '%".$this->udfvalue."%'";
                    break;
                case 'Y':
                    $this->where .= " AND udf.udfvalue = $this->udfvalue";
                    break;
            }
        }

    }
    public function getWhereClause()
    {
        return $this->where;
    }
    public function execQuery($sql)
    {
        $cmpcode = $this->cmpcode;
        include("inc_db_con.php");
        $this->result = $rs;
    }
    public function checkNumRecords()
    {
        if(mysql_num_rows($this->result) > 0)
        {
            return true;
        }
        return false;
    }
    private function getPieData()
    {
        $this->getWhereClause();
        $this->from = $this->getFromClause();
        $this->data = $this->getStatuses();
        $sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt
                        FROM ".$this->dbref."_list_status s
                        LEFT JOIN ".$this->dbref."_task t ON ( s.pkey = t.taskstatusid ) ".$this->from."
                        WHERE 1 ".$this->where." GROUP BY s.pkey";
        echo $sql;
        die;
        $this->execQuery($sql);
        while($row = mysql_fetch_array($this->result))
        {
            if(array_key_exists($row['pkey'], $this->data))
            {
                $this->data[$row['pkey']] = array("title"=>$row['value'],"count"=>$row['cnt']);
            }
        }
    }
    private function getActionTopicData()
    {
        $sql = "SELECT * FROM ".$this->dbref."_list_topic WHERE yn='Y'";
        $this->execQuery($sql);
        while($row = mysql_fetch_array($this->result))
        {
            $this->data[$row['id']] = array('name'=>$row['value']);
        }
        $this->getStatuses();
        foreach($this->data as $tasktopicid => $value)
        {
            $this->data[$tasktopicid] = $this->statuses;
            $this->data[$tasktopicid]['name'] = $value['name'];
        }
        $where = $this->getWhereClause();
        $from = $this->getFromClause();
        foreach($this->data as $tasktopicid => $cntData)
        {
            $sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt
                        FROM ".$this->dbref."_list_status s
                        LEFT JOIN ".$this->dbref."_task t ON ( s.pkey = t.taskstatusid ) ".(empty($from) ? "":$from)."
                        WHERE t.tasktopicid=$tasktopicid AND ".(empty($where) ? " (1=1) ": " (1=1) $where ")." GROUP BY s.pkey";
            $this->execQuery($sql);
            while($row = mysql_fetch_array($this->result))
            {
                $this->data[$tasktopicid][$row['pkey']] = array('title'=> str_replace("&#039;", "'", $row['value']),'count'=>$row['cnt']);
            }
        }
    }
    private function getActionOwnerData()
    {
        $this->getWhereClause();
        $this->from = $this->getFromClause();
        $sql = "SELECT DISTINCT(t.taskadduser),CONCAT_WS(' ',k.tkname,k.tksurname) AS name
                    FROM ".$this->dbref."_task t ".$this->from." INNER JOIN assist_".$this->cmpcode."_timekeep k ON (t.taskadduser=k.tkid)
                    WHERE  1 ".$this->where;
        $this->execQuery($sql);
        while($row = mysql_fetch_array($this->result))
        {
            $this->data[$row['taskadduser']] = array('name'=>$row['name']);
        }
        $this->getStatuses();
        foreach($this->data as $taskadduser => $value)
        {
            $this->data[$taskadduser] = $this->statuses;
            $this->data[$taskadduser]['name'] = $value['name'];
        }
        foreach($this->data as $taskadduser => $cntData)
        {
            $sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt
                        FROM ".$this->dbref."_list_status s
                        LEFT JOIN ".$this->dbref."_task t ON ( s.pkey = t.taskstatusid )
                        INNER JOIN  ".$this->dbref."_task_recipients r ON ( t.taskid = r.taskid )
                        WHERE t.taskadduser ='".$taskadduser."' ".$this->where." GROUP BY s.pkey";
            $this->execQuery($sql);
            while($row = mysql_fetch_array($this->result))
            {
                $this->data[$taskadduser][$row['pkey']] = array('title'=> str_replace("&#039;", "'", $row['value']),'count'=>$row['cnt']);
            }
        }
    }
    private function getPersonActionedData()
    {
        $this->getWhereClause();
        $sql = "SELECT DISTINCT(r.tasktkid),CONCAT_WS(' ',k.tkname,k.tksurname) AS name
                    FROM ".$this->dbref."_task t,".$this->dbref."_task_recipients r,assist_".$this->cmpcode."_timekeep k
                    WHERE k.tkid=r.tasktkid AND t.taskid=r.taskid ".$this->where;
        $this->execQuery($sql);
        while($row = mysql_fetch_array($this->result))
        {
            $this->data[$row['tasktkid']] = array('name'=>$row['name']);
        }
        $this->getStatuses();
        foreach($this->data as $tasktkid => $value)
        {
            $this->data[$tasktkid] = $this->statuses;
            $this->data[$tasktkid]['name'] = $value['name'];
        }
        foreach($this->data as $tasktkid => $cntData)
        {
            $sql = "SELECT s.value, s.pkey, COUNT( t.taskid ) AS cnt
                        FROM ".$this->dbref."_list_status s
                        LEFT JOIN ".$this->dbref."_task t ON ( s.pkey = t.taskstatusid )
                        INNER JOIN  ".$this->dbref."_task_recipients r ON ( t.taskid = r.taskid )
                        WHERE r.tasktkid ='".$tasktkid."' ".$this->where." GROUP BY s.pkey";

            $this->execQuery($sql);
            while($row = mysql_fetch_array($this->result))
            {
                $this->data[$tasktkid][$row['pkey']] = array('title'=> str_replace("&#039;", "'", $row['value']),'count'=>$row['cnt']);
            }
        }
    }
    public function getDataBy()
    {
        switch($this->groupby)
        {
            case 'tasktkid':
                $this->getPersonActionedData();
                break;
            case 'taskadduser':
                if($this->graphtype == 'pie')
                {
                    $this->getPieData();
                }else if($this->graphtype == '3dcolumn')
                {
                    $this->getActionOwnerData();
                }else if($this->graphtype == '3dbarchart')
                {
                    $this->getActionOwnerData();
                }
                break;
            case 'taskurgencyid':
                break;
            case 'tasktopicid':
                if($this->graphtype == 'pie')
                {
                    $this->getPieData();
                }else if($this->graphtype == '3dcolumn' || $this->graphtype == '3dbarchart')
                {
                    $this->getActionTopicData();
                }
                break;
            default:
                if($this->groupby == "X")
                {//data for pie chart
                    $this->getPieData();
                }
                break;
        }
    }

}
class Graph
{
    private $type;
    private $query;
    private $data;
    private $statuses;
    public function  __construct()
    {
        $this->query = new Query();
        $this->type = $this->query->getGraphType();
        $this->statuses = $this->query->getStatuses();
        $this->query->getDataBy();
        $this->data = $this->query->getData();
        $this->getGraph();
    }
    public function getGraph()
    {
        $xml = "<?xml version='1.0' encoding='UTF-8'?>\n";
        switch($this->type)
        {
            case '3dcolumn':
                if(!empty($this->data))
                {
                    header("Content-type: text/xml");
                    $xml .= "<chart>\n";
                    $xml .= "<series>\n";
                    foreach($this->data as $id => $value)
                    {
                        $xml .= "<value xid='".$id."'>".$value['name']."</value>\n";
                    }
                    $xml .= "</series>\n";
                    $xml .= "<graphs>\n";
                    foreach($this->statuses as $statusId => $heading)
                    {
                        $xml .= "<graph gid='".$statusId."' title='".$heading."'>\n";
                        foreach($this->data as $id => $countData)
                        {
                            if(is_array($countData[$statusId]))
                                $xml .= "<value xid='".$id."'>".$countData[$statusId]['count']."</value>\n";
                            else
                                $xml .= "<value xid='".$id."'>0</value>\n";
                        }
                        $xml .= "</graph>\n";
                    }
                    $xml .= "</graphs>\n";
                    $xml .= "</chart>";
                    echo $xml;
                }
                break;
            case '3dbarchart':
                if(!empty($this->data))
                {
                    $xml .= "<chart>\n";
                    $xml .= "<series>\n";
                    foreach($this->data as $id => $value)
                    {
                        $xml .= "<value xid='".$id."'>".$value['name']."</value>\n";
                    }
                    $xml .= "</series>\n";
                    $xml .= "<graphs>\n";
                    foreach($this->statuses as $statusId => $heading)
                    {
                        $xml .= "<graph gid='".$statusId."' title='".$heading."'>\n";
                        foreach($this->data as $id => $countData)
                        {
                            if(is_array($countData[$statusId]))
                                $xml .= "<value xid='".$id."'>".$countData[$statusId]['count']."</value>\n";
                            else
                                $xml .= "<value xid='".$id."'>0</value>\n";
                        }
                        $xml .= "</graph>\n";
                    }
                    $xml .= "</graphs>\n";
                    $xml .= "</chart>";
                    echo $xml;
                }
                break;
            case 'pie':
            //  echo '<pre>';
            //print_r($this->data);
                $xml .= "<pie>\n";
                foreach($this->data as $statusId => $cntData)
                {
                    if(is_array($cntData))
                        $xml .= "<slice title='".$cntData['title']."'>".$cntData['count']."</slice>\n";
                }
                $xml .= "</pie>";
                echo $xml;
                break;
            default :
                break;
        }
    }
    public function getData()
    {
        return $this->data;
    }
}
//new Graph();


?>


