<?php
$title = array(
	array('txt'=>"Advanced Search"),
	array('txt'=>"Results"),
);
$get_udf_link_headings = false;
require_once 'inc_header.php';

$order_by = (isset($_REQUEST['orderby']) && strlen($_REQUEST['orderby'])>0) ? $_REQUEST['orderby']  : "taskid";
$current = isset($_REQUEST['start']) && checkIntRef($_REQUEST['start']) ? $_REQUEST['start'] : 0;

//SEARCH SETTINGS
$search_where = array();
//taskadddate
$fld = "taskadddate";
if(strlen($_REQUEST[$fld]['from'])>0 && strlen($_REQUEST[$fld]['to'])>0) {
	$from = $_REQUEST[$fld]['from'];
	$from = strtotime($from." 00:00:00");
	$to = $_REQUEST[$fld]['to'];
	$to = strtotime($to." 23:59:59");
	$search_where[] = "($fld >= $from AND $fld <= $to)";
} elseif( (strlen($_REQUEST[$fld]['from']) + strlen($_REQUEST[$fld]['to'])) > 0) {
	$v = strlen($_REQUEST[$fld]['from'])>0 ? $_REQUEST[$fld]['from'] : $_REQUEST[$fld]['to'];
	$from = strtotime($v." 00:00:00");
	$to = strtotime($v." 23:59:59");
	$search_where[] = "($fld >= $from AND $fld <= $to)";
}
//taskadduser
$fld = "taskadduser";
if(isset($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
	$search_where[] = "($fld IN ('".implode("','",$_REQUEST[$fld])."'))";
}
//tasktkid






//tasktopicid
$fld = "tasktopicid";
if(isset($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
	$search_where[] = "($fld IN (".implode(",",$_REQUEST[$fld])."))";
}
//taskurgencyid
$fld = "taskurgencyid";
if(isset($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
	$search_where[] = "($fld IN (".implode(",",$_REQUEST[$fld])."))";
}
//taskstatusid
$fld = "taskstatusid";
if(isset($_REQUEST[$fld]) && count($_REQUEST[$fld])>0) {
	$search_where[] = "($fld IN (".implode(",",$_REQUEST[$fld])."))";
}
//taskdeadline
$fld = "taskdeadline";
if(strlen($_REQUEST[$fld]['from'])>0 && strlen($_REQUEST[$fld]['to'])>0 && $_REQUEST[$fld]['from']!=$_REQUEST[$fld]['to']) {
	$from = $_REQUEST[$fld]['from'];
	$from = strtotime($from);
	$to = $_REQUEST[$fld]['to'];
	$to = strtotime($to);
	$search_where[] = "($fld >= $from AND $fld <= $to)";
} elseif( (strlen($_REQUEST[$fld]['from']) + strlen($_REQUEST[$fld]['to'])) > 0) {
	$v = strlen($_REQUEST[$fld]['from'])>0 ? $_REQUEST[$fld]['from'] : $_REQUEST[$fld]['to'];
	$from = strtotime($v);
	$search_where[] = "($fld = $from)";
}
//taskaction
$fld = "taskaction";
if(isset($_REQUEST[$fld]) && strlen($_REQUEST[$fld]['search'])>0) {
	$v = code($_REQUEST[$fld]['search']);
	$v = explode(' ',$v);
	switch($_REQUEST[$fld]['type']) {
	case "ANY":
		$search_where[] = "($fld LIKE '%".implode("%' OR $fld LIKE '%",$v)."%')";
		break;
	case "ALL":
		$search_where[] = "($fld LIKE '%".implode("%' AND $fld LIKE '%",$v)."%')";
		break;
	case "EXACT":
		$search_where[] = "($fld LIKE '%".code($_REQUEST[$fld]['search'])."%')";
		break;
	}
}
//taskdeliver
$fld = "taskdeliver";
if(isset($_REQUEST[$fld]) && strlen($_REQUEST[$fld]['search'])>0) {
	$v = code($_REQUEST[$fld]['search']);
	$v = explode(' ',$v);
	switch($_REQUEST[$fld]['type']) {
	case "ANY":
		$search_where[] = "($fld LIKE '%".implode("%' OR $fld LIKE '%",$v)."%')";
		break;
	case "ALL":
		$search_where[] = "($fld LIKE '%".implode("%' AND $fld LIKE '%",$v)."%')";
		break;
	case "EXACT":
		$search_where[] = "($fld LIKE '%".code($_REQUEST[$fld]['search'])."%')";
		break;
	}
}
//udfs
echo "<p class=b>--- START UDFS --- </p>";
$fld = "udf";
$udf = array(); $udf[] = 0;
if(isset($_REQUEST[$fld])) {
	$var = $_REQUEST[$fld];
	foreach($udf_index['action']['ids'] as $i) {
		if(isset($var[$i])) {
			$u = $udf_index['action']['index'][$i];
			$v = $var[$i];
			switch($u['udfilist']) {
			case "Y":
				if(count($v)>0) {
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							LEFT OUTER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$_SESSION['modref']."'
							  AND u.udfindex = $i
							WHERE ";
					if($v[0]==0) {
						$sql.= " u.udfnum is NULL OR ";
					}
					$sql.= " u.udfvalue IN (".implode(",",$v).")";
					$ids = mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			case "T":
			case "M":
				if(strlen($v['search'])>0) {
					$sql = "";
					$vs = code($v['search']);
					$vs = explode(' ',$vs);
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							INNER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$_SESSION['modref']."'
							  AND u.udfindex = $i
							WHERE ";
					switch($v['type']) {
					case "ANY":
						$sql.= "(u.udfvalue LIKE '%".implode("%' OR u.udfvalue LIKE '%",$vs)."%')";
						break;
					case "ALL":
						$sql.= "(u.udfvalue LIKE '%".implode("%' AND u.udfvalue LIKE '%",$vs)."%')";
						break;
					case "EXACT":
						$sql.= "(u.udfvalue LIKE '%".code($v['search'])."%')";
						break;
					}
					$ids = mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			case "D":
				if(strlen($v['from'])>0 || strlen($v['to'])>0) {
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							INNER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$_SESSION['modref']."'
							  AND u.udfindex = $i
							WHERE ";
					if($v['from']!=$v['to'] && $v['from']!='' && $v['to']!='') {
						$from = $v['from'];
						$from = strtotime($from);
						$to = $v['to'];
						$to = strtotime($to);
						$sql.= "(u.udfvalue >= $from AND u.udfvalue <= $to)";
					} else {
						$from = strtotime(strlen($v['from'])>0 ? $v['from'] : $v['to']);
						$sql.= "(u.udfvalue = $from)";
					}
					$ids = mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			case "N":
				if(strlen($v['from'])>0 || strlen($v['to'])>0) {
					$sql = "SELECT DISTINCT t.taskid
							FROM ".$dbref."_task t
							INNER JOIN assist_".$cmpcode."_udf u
							  ON u.udfnum = t.taskid
							  AND u.udfref = '".$_SESSION['modref']."'
							  AND u.udfindex = $i
							WHERE ";
					if($v['from']!=$v['to'] && $v['from']!='' && $v['to']!='') {
						$from = $v['from'];
						$to = $v['to'];
						$sql.= "(u.udfvalue >= $from AND u.udfvalue <= $to)";
					} else {
						$from = strlen($v['from'])>0 ? $v['from'] : $v['to'];
						$sql.= "(u.udfvalue = $from)";
					}
					$ids = mysql_fetch_fld_one($sql,"taskid");
					$udf = array_merge($udf,$ids);
				}
				break;
			}
		}
	}
	$udf = array_unique($udf,SORT_NUMERIC);
	$search_where[] = "(t.taskid IN (".implode(",",$udf).") )";
}
echo "<P class=b>--- END UDF ---</p>";
//attach
$fld = "attach";
$attach = array();  $attach[] = 0;
if(isset($_REQUEST[$fld]) && $_REQUEST[$fld]!="X") {
	$attach_tasks = mysql_fetch_fld_one("SELECT taskid FROM ".$dbref."_task_attachments WHERE taskid > 0","taskid");
	$attach_logs = mysql_fetch_fld_one("SELECT l.logtaskid FROM ".$dbref."_log l INNER JOIN ".$dbref."_task_attachments a ON a.logid = l.logid","logtaskid");
	$attach = $attach_tasks;
	foreach($attach_logs as $a) {
		if(!in_array($a,$attach)) { $attach[] = $a; }
	}
	if(count($attach)>0) {
		$search_where[] = "(t.taskid ".($_REQUEST[$fld]==0 ? "NOT" : "")." IN (".implode(",",$attach).") )";
	}
}

$search_where = implode(" AND ",$search_where);

//TASKS
$view_type = $_REQUEST['act'];
switch($view_type) {
case "MY":	
	$title = "My ".$actname."s"; 
	$display = array('field'=>"mydisp",'sort'=>"mysort");
	$where = "FROM ".$dbref."_task t
			INNER JOIN ".$dbref."_task_recipients tr
			  ON tr.taskid = t.taskid 
			  AND tr.tasktkid = '".$tkid."'
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep ta
			  ON t.taskadduser = ta.tkid
			LEFT OUTER JOIN ".$dbref."_list_topic topic
			  ON t.tasktopicid = topic.id
			LEFT OUTER JOIN ".$dbref."_list_urgency urgency
			  ON t.taskurgencyid = urgency.id
			LEFT OUTER JOIN ".$dbref."_list_status status
			  ON t.taskstatusid = status.pkey
			WHERE ";
	$rows_available = mysql_fetch_fld_one("SELECT count(t.taskid) as tc ".$where." ".$search_where,"tc");
	//$rows_available = $rows_available['tc'];
	$sql = "SELECT t.taskid
			, topic.value as tasktopicid
			, CONCAT_WS(' ',ta.tkname,ta.tksurname) as taskadduser
			, status.value as taskstatusid
			, status.pkey as status
			,t.taskdeadline
			, urgency.value as taskurgencyid
			,t.taskaction
			,t.taskdeliver
			,t.taskadddate
			".$where." ".$search_where."
			ORDER BY ".$order_by."
			LIMIT $current, ".(isset($rowsPerPage) && checkIntRef($rowsPerPage) ? $rowsPerPage : 20);
	break;
case "OWN":	$title = "".$actname."s I Assigned"; break;
case "ALL":	$title = "All ".$actname."s"; break;
}
echo $sql;
$tasks = mysql_fetch_all($sql);
$tact = getUserAccess();
//arrPrint($_REQUEST);

//echo $search_where;

arrPrint($tasks);

?>
<script type=text/javascript>
$(function() {
	$("#h_title").prepend("<?php echo "<a href=view_list.php?s=".$_REQUEST['s']."&act=".$_REQUEST['act']." class=breadcrumb>".$title."</a> >> "; ?>");
});
</script>

</body>

</html>
