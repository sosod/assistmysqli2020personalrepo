<?php
//include("inc_ignite.php");
$page = array("setup","access");
$title = array(
	array('url'=>"setup.php",'txt'=>"Setup"),
	array('txt'=>"User Access"),
);

require_once "inc_header.php";

$setupObject = new PDP2_SETUP();

$result = array();
//GET VARIABLES PASSED BY FORM
if(isset($_REQUEST['atkid']) && isset($_REQUEST['access'])) {
	$atkid = $_POST['atkid'];
	$access = $_POST['access'];
	//IF THE VARIABLES WERE PASSED CORRECTLY THEN PERFORM UPDATE
	if($atkid != "X" && $access != "X" && strlen($access) > 0) {
		$result = $setupObject->saveNewUserAccess($atkid,$access);
	}
} elseif(isset($_REQUEST['r'])) {
	$result = $_REQUEST['r'];
}
?>
<script language=JavaScript>
function editUser(id) {
    document.location.href="setup_access_edit.php?id="+id;
}
</script>
<?php ASSIST_HELPER::displayResult($result); ?>
<table>
	<tr>
		<th>ID</th>
		<th>User</th>
		<th>Access</th>
		<th>&nbsp;</th>
	</tr>
<?php
//GET Existing user access settings
$users = $setupObject->getCurrentUserAccessSettings();
if(isset($users[$taadmin])) {
	unset($users[$taadmin]);
}
$got = array_keys($users); $got[] = $taadmin;

//get any users who have menu access to the module but no user access set
$new = $setupObject->getUsersWithoutDefinedAccess($got);

	//Display Add new form
if(count($new) > 0) {
?>
<form method=POST action=setup_access.php>
	<tr>
		<td>&nbsp;</td>
		<td><select name=atkid>
                <option selected value=X>--- SELECT ---</option>
                <?php
                    foreach($new as $row) {
                        echo("<option value=".$row['id'].">".$row['name']."</option>");
                    }
                ?>
            </select>
        </td>
		<td>Add/Update/View&nbsp;
            <select name=access>
                <option selected value=X>--- SELECT ---</option>
                <option value=20>own <?php echo ucfirst($actname);?>s only</option>
                <option value=90>all <?php echo ucfirst($actname);?>s</option>
            </select>
        </td>
		<td><input type=submit value=Add class=isubmit /></td>
	</tr>
</form>
<?php
}

//Display module administrator details
	$module_administrator = $setupObject->getModuleAdminName($taadmin);
?>
		<tr>
			<td><?php echo($taadmin); ?></td>
			<td><?php echo($module_administrator); ?></td>
			<td colspan=2>Module Administrator (Full access)</td>
		</tr>
<?php
//display user access details
	foreach($users as $row) {
		echo "	<tr>
					<td>".$row['tkid']."</td>
					<td>".$row['tkname']." ".$row['tksurname']."</td>
					<td>".$row['ruletext']."</td>
					<td class=center><input type=button value=Edit onclick='editUser(".$row['id'].")'></td>
				</tr>";
	}
?>
</table>
</body>

</html>
