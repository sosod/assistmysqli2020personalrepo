<?php
$title = array(
	array('url'=>"view.php",'txt'=>"View"),
);
$page = array("view","list");
$get_udf_link_headings = false;
require_once 'inc_header.php';
//arrPrint($_REQUEST);
//Check for redirect from EDIT
if(isset($_REQUEST['get_details']) && $_REQUEST['get_details']==1 && isset($_SESSION['ACTION'][strtoupper($modref)]['VIEW']['LIST'])) {
	if(isset($_REQUEST['start'])) { $_SESSION['ACTION'][strtoupper($modref)]['VIEW']['LIST']['start'] = $_REQUEST['start']; }
	$_REQUEST = $_SESSION['ACTION'][strtoupper($modref)]['VIEW']['LIST'];
} else {
	//Update the SESSION[ACTION][MODREF][VIEW][LIST] array with the current REQUEST so that EDIT/UPDATE can redirect back here after saving changes
	$_SESSION['ACTION'][strtoupper($modref)]['VIEW']['LIST'] = $_REQUEST;
}


$order_by = (isset($_REQUEST['orderby']) && strlen($_REQUEST['orderby'])>0) ? $_REQUEST['orderby']  : "taskdeadline";

//GET VARIABLES
$tact = $ah->getMyUserAccess();
$actname.="s";
$me = explode("/",$_SERVER['PHP_SELF']);
$me = $me[count($me)-1];
if((!isset($_REQUEST['s']) || !ASSIST_HELPER::checkIntRef($_REQUEST['s'])) && !isset($_REQUEST['search']['type'])) {
	echo "<script type=text/javascript>document.location.href = 'view.php'; </script>";
	//echo "<h1 class=idelete>REDIRECTING!</h1>";  arrPrint($_REQUEST);
} else {
	$statusid = $_REQUEST['s'];
}
$view_type = $_REQUEST['act'];
//SEARCH VARIABLES
$search_where = "";
$search = array();
if(isset($_REQUEST['search']['type'])) {
	switch($_REQUEST['search']['type']) {
	case "QUICK":
		//if(!isset($_REQUEST['get_details'])) { 
		$_REQUEST['get_details'] = 1; 
		//$_SESSION['ACTION'][strtoupper($modref)]['VIEW']['LIST'] = $_REQUEST; }
		if(isset($_REQUEST['search']) && strlen($_REQUEST['search']['field'])>0 && strlen($_REQUEST['search']['value'])>0) {
			$search = array(
				'field' => $_REQUEST['search']['field'],
				'value' => $_REQUEST['search']['value'],
			);
			switch($search['field']) {
			case "taskid":			if(ASSIST_HELPER::checkIntRef($search['value'])) { $search_where = "t.taskid = ".$search['value']; } break;
			case "taskadddate":		$v1 = strtotime($search['value']." 00:00:00"); $v2 = strtotime($search['value']." 23:59:59"); $search_where = "t.taskadddate >= $v1 AND t.taskadddate <= $v2"; break;
			case "taskdeadline":	$search_where = "t.taskdeadline = ".strtotime($search['value']); break;
			case "taskadduser":		$search_where = "t.taskadduser = '".$search['value']."'"; break;
			case "tasktopicid":		if(ASSIST_HELPER::checkIntRef($search['value'])) { $search_where = "t.tasktopicid = ".$search['value'].""; } break;
			case "taskurgencyid":	if(ASSIST_HELPER::checkIntRef($search['value'])) { $search_where = "t.taskurgencyid = ".$search['value'].""; } break;
			case "taskaction":		$search_where = "t.taskaction LIKE '%".code($search['value'])."%'"; break;
			case "taskdeliver":		$search_where = "t.taskdeliver LIKE '%".code($search['value'])."%'"; break;
			}
			//arrPrint($_REQUEST);
			if(strlen($search_where)>0) { $search_where = " AND ".$search_where; }
		}
		break;
	case "ADVANCED":
		if(!isset($_REQUEST['get_details'])) { $_REQUEST['get_details'] = 1; }
		include("inc_adv_search_filter.php");
		if(strlen($search_where)==0) { }//unset($_REQUEST['search']['type']); }
		break;
	}
}
//START NAVIGATION SETTINGS
$current = isset($_REQUEST['start']) && ASSIST_HELPER::checkIntRef($_REQUEST['start']) ? $_REQUEST['start'] : 0;
//GET STATUS DETAILS
//arrPrint($_REQUEST);
if(!isset($_REQUEST['search']['type']) || ($_REQUEST['search']['type']) == "QUICK") {
	//echo "<h1>Error</h1>";
	$status = $ah->getAStatusNameByID($statusid);//mysql_fetch_one("SELECT * FROM ".$dbref."_list_status WHERE pkey = ".$statusid);
	if(isset($_REQUEST['search']['type']) && $_REQUEST['search']['type']=="QUICK") {
		$status.=" ".ASSIST_MODULE_HELPER::BREADCRUMB_DIVIDER." Quick Search";
	}
} else {
	$status = "Advanced Search Results";
}
//GET UDFS
$udf_index = $ah->mysql_fetch_all_fld("SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = '".$modref."' AND udfiobject = 'action'","udfiid");
$udf_keys = array_keys($udf_index);
$sql = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfref = '".$modref."'";
$udfs = $ah->mysql_fetch_all_fld2($sql,"udfnum","udfindex");
$udf_list_values = array();
if(count($udf_keys)>0) {
	$sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvyn = 'Y' AND udfvindex IN (".implode(",",$udf_keys).") ORDER BY udfvindex, udfvvalue";
	$udf_list_values = $ah->mysql_fetch_all_fld2($sql,"udfvindex","udfvid");
}
//GET TASKS
$task_attachments = $ah->mysql_fetch_all_fld("SELECT taskid, count(id) FROM ".$dbref."_task_attachments WHERE taskid > 0 AND file_location <> 'deleted' GROUP BY taskid","taskid");
$log_attachments = $ah->mysql_fetch_all_fld("SELECT t.taskid, count(a.id) FROM ".$dbref."_task t INNER JOIN ".$dbref."_log l ON t.taskid = l.logtaskid INNER JOIN ".$dbref."_task_attachments a ON l.logid = a.logid AND a.file_location <> 'deleted' GROUP BY t.taskid","taskid");
switch($view_type) {
case "MY":
	$title = "My ".$actname;
	$display = array('field'=>"mydisp",'sort'=>"mysort");
	$where = "FROM ".$dbref."_task t
			INNER JOIN ".$dbref."_task_recipients tr
			  ON tr.taskid = t.taskid 
			  AND tr.tasktkid = '".$tkid."'
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep ta
			  ON t.taskadduser = ta.tkid
			LEFT OUTER JOIN ".$dbref."_list_topic topic
			  ON t.tasktopicid = topic.id
			LEFT OUTER JOIN ".$dbref."_list_urgency urgency
			  ON t.taskurgencyid = urgency.id
			LEFT OUTER JOIN ".$dbref."_list_status status
			  ON t.taskstatusid = status.pkey AND status.pkey <> 2 ".
			(!isset($_REQUEST['search']['type']) || $_REQUEST['search']['type']=="QUICK" || strlen($search_where)>0 ? "WHERE " : "").
			(!isset($_REQUEST['search']['type']) || $_REQUEST['search']['type']=="QUICK" ? "t.taskstatusid = ".$statusid."" : "");
	$rows_available = $ah->mysql_fetch_one("SELECT count(t.taskid) as tc ".$where." ".$search_where);
	$rows_available = $rows_available['tc'];
	$sql = "SELECT t.taskid
			, topic.value as tasktopicid
			, CONCAT_WS(' ',ta.tkname,ta.tksurname) as taskadduser
			, status.value as taskstatusid
			, status.pkey as status
			,t.taskdeadline
			, urgency.value as taskurgencyid
			,t.taskaction
			,t.taskdeliver
			,t.taskadddate
			".$where." ".$search_where."
			ORDER BY ".$order_by."
			LIMIT $current, ".(isset($rows_per_page) && ASSIST_HELPER::checkIntRef($rows_per_page) ? $rows_per_page : 20);
	break;
case "OWN":
	$title = $actname." I Assigned";
	$display = array('field'=>"owndisp",'sort'=>"ownsort");
	$where = "FROM ".$dbref."_task t
			INNER JOIN ".$dbref."_task_recipients tr
			  ON tr.taskid = t.taskid 
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep ta
			  ON t.taskadduser = ta.tkid
			LEFT OUTER JOIN ".$dbref."_list_topic topic
			  ON t.tasktopicid = topic.id
			LEFT OUTER JOIN ".$dbref."_list_urgency urgency
			  ON t.taskurgencyid = urgency.id
			LEFT OUTER JOIN ".$dbref."_list_status status
			  ON t.taskstatusid = status.pkey AND status.pkey <> 2
			  LEFT OUTER JOIN ".$dbref."_list_manager man
			  ON man.id = t.taskmanagerid
            LEFT OUTER JOIN ".$dbref."_list_delivery_mode del
			  ON del.id = t.taskdeliverymodeid
            LEFT OUTER JOIN ".$dbref."_list_time_frame tframe
			  ON tframe.id = t.tasktimeframeid
            LEFT OUTER JOIN ".$dbref."_list_support_person supp
			  ON supp.id = t.tasksupportpersonid
            LEFT OUTER JOIN ".$dbref."_list_competency comp
			  ON comp.id = t.taskcompetencyid
            LEFT OUTER JOIN ".$dbref."_list_confirm conf
			  ON conf.id = t.taskconfirmid
			WHERE t.taskadduser = '$tkid' ".
			(!isset($_REQUEST['search']['type']) || $_REQUEST['search']['type']=="QUICK" ? " AND t.taskstatusid = ".$statusid."" : "");
	$rows_available = $ah->mysql_fetch_one("SELECT count(t.taskid) as tc ".$where." ".$search_where);
	$rows_available = $rows_available['tc'];
	$sql = "SELECT t.taskid
			, topic.value as tasktopicid
			, CONCAT_WS(' ',ta.tkname,ta.tksurname) as taskadduser
			, status.value as taskstatusid
			, status.pkey as status
			,t.taskdeadline
			, urgency.value as taskurgencyid
			, man.value as taskmanagerid
			, del.value as taskdeliverymodeid
			, tframe.value as tasktimeframeid
			, supp.value as tasksupportpersonid
			, comp.value as taskcompetencyid
			, conf.value as taskconfirmid
			,t.taskaction
			,t.taskdeliver
			,t.taskadddate
			".$where." ".$search_where."
			ORDER BY ".$order_by."
			LIMIT $current, ".(isset($rows_per_page) && ASSIST_HELPER::checkIntRef($rows_per_page) ? $rows_per_page : 20);
	break;
case "ALL":
	$title = "All ".$actname;
	$display = array('field'=>"alldisp",'sort'=>"allsort");
	$where = "FROM ".$dbref."_task t
			INNER JOIN ".$dbref."_task_recipients tr
			  ON tr.taskid = t.taskid 
			LEFT OUTER JOIN assist_".$cmpcode."_timekeep ta
			  ON t.taskadduser = ta.tkid
			LEFT OUTER JOIN ".$dbref."_list_topic topic
			  ON t.tasktopicid = topic.id
			LEFT OUTER JOIN ".$dbref."_list_urgency urgency
			  ON t.taskurgencyid = urgency.id
			LEFT OUTER JOIN ".$dbref."_list_status status
			  ON t.taskstatusid = status.pkey AND status.pkey <> 2 ".
			(!isset($_REQUEST['search']['type']) || $_REQUEST['search']['type']=="QUICK" || strlen($search_where)>0 ? "WHERE " : "").
			(!isset($_REQUEST['search']['type']) || $_REQUEST['search']['type']=="QUICK" ? "t.taskstatusid = ".$statusid."" : "");
	$rows_available = $ah->mysql_fetch_one("SELECT count(t.taskid) as tc ".$where." ".$search_where);
	$rows_available = $rows_available['tc'];
	$sql = "SELECT t.taskid
			, topic.value as tasktopicid
			, CONCAT_WS(' ',ta.tkname,ta.tksurname) as taskadduser
			, status.value as taskstatusid
			, status.pkey as status
			,t.taskdeadline
			, urgency.value as taskurgencyid
			,t.taskaction
			,t.taskdeliver
			,t.taskadddate
			".$where." ".$search_where."
			ORDER BY ".$order_by."
			LIMIT $current, ".(isset($rows_per_page) && ASSIST_HELPER::checkIntRef($rows_per_page) ? $rows_per_page : 20);
	break;
}
//echo $sql;
$tasks = $ah->mysql_fetch_all_fld($sql,"taskid");
$task_ids = array_keys($tasks);
if(count($task_ids)>0) {
	$sql = "SELECT tr.id, tr.taskid, CONCAT_WS(  ' ', tk.tkname, tk.tksurname ) as tkname
			FROM ".$dbref."_task_recipients tr
			INNER JOIN assist_".$cmpcode."_timekeep tk 
			  ON tk.tkid = tr.tasktkid
			WHERE tr.taskid IN (".implode(",",$task_ids).") ";
	$task_recipients = $ah->mysql_fetch_all_fld2($sql,"taskid","id");
}

//GET HEADINGS
$sql = "SELECT * FROM ".$dbref."_list_display WHERE yn = 'Y' AND ".$display['field']." = 'Y' ".(count($udf_keys)>0 ? "AND (type = 'S' OR field IN (".implode(",",$udf_keys)."))" : "AND type = 'S'")." ORDER BY ".$display['sort']."";
$head = $ah->mysql_fetch_all_fld($sql,"field");
foreach($head as $fld => $h) {
	$head[$fld]['headingfull'] = $headings['action'][$fld];
	$head[$fld]['headingshort'] = $headings['short_action'][$fld];
}


//FINISH NAVIGATION SETTINGS
$pageY = ceil($rows_available / $rows_per_page);
$pageX = $current > 0 ? ($current / $rows_per_page) + 1 : 1;
$argv = $_REQUEST;
unset($argv['start']);
$request_arg = array();
foreach($argv as $key => $v) {
	if(!is_array($v)) {
		$request_arg[] = $key."=".$v;
	} else {

	}
}
$arg = implode("&",$request_arg);
//.ui-state-white {background-image: url("images/ui-icons_ffffff_256x240.png"); }
?>
<style type=text/css>
</style>
<table class=noborder width="100%"><tr class='no-highlight'><td class=noborder>
<table id=tbl_paging width=100% style='margin-bottom: 20px; '>
	<tr>
		<td><span id=paging>
				<input type=button value='|<' id=first class=btn_nav />
				<input type=button value='<' id=prev class=btn_nav />
				<?php echo "Page ".$pageX." of ".$pageY; ?>
				<input type=button value='>' id=next class=btn_nav />
				<input type=button value='>|' id=last class=btn_nav />
			</span>
			<span id=search class=float>
			<?php if(!isset($_REQUEST['search']['type']) || $_REQUEST['search']['type']=="QUICK") { ?>
				Quick Search:&nbsp;
				<select id=search_item>
					<option selected value=taskid search_type='text'>Ref</option>
					<option value=taskadddate search_type='date'>Date Added</option>
					<option value=taskadduser search_type='list'>Assigned By</option>
					<option value=tasktopicid search_type='list'>Topic</option>
					<option value=taskurgencyid search_type='list'>Priority</option>
					<option value=taskaction search_type='text'>Instruction</option>
					<option value=taskdeliver search_type='text'>Deliverable</option>
					<option value=taskdeadline search_type='date'>Deadline</option>
				</select>&nbsp;
				<input type=text id=txt_search /><span id=spn_date_search><input type=text size=10 id=date_search class=jdate2012 /></span><select id=sel_search></select>&nbsp;
				<input type=button value=Go id=search_go />&nbsp;
			<?php } ?>
				<a href=javascript:void(0) id=adv_search>Advanced Search</a>
			<?php if(isset($_REQUEST['search']['type'])) { echo "&nbsp;<input type=button value='Clear' id=clear_search />"; } ?>
			</span>
		</td>
	</tr>
</table>
<table width="100%">
	<tr>
		<th class=head>Ref</th>
		<th class=attach><img src=/pics/attach_head.png /></th><?php
foreach($head as $key => $h) {
	echo "<th class=head>".$h['headingfull']."</th>";
}
		?><th class=head></th>
	</tr>
<?php 
foreach($tasks as $t) {
	echo "
		<tr>
			<th width='35px'>".$t['taskid']."</th>
			<td class='center middle'>".(isset($task_attachments[$t['taskid']]) || isset($log_attachments[$t['taskid']]) ? "<img src=/pics/attach.png />" : "")."</td>";
	foreach($head as $key => $h) {
		switch($h['type']) {
		case "S":
			$v = (isset($t[$key]) ? $t[$key] : "N/A");
			if($key=="taskdeadline") {
				$v = is_numeric($v) ? date("d M Y",$v) : $v; 
				$now = strtotime(date("d F Y"));
				if($t[$key]<$now && $t['status']!=1) { $class = "overdue"; } elseif(date("d F Y",$t[$key])==date("d F Y")) { $class="today"; } else { $class = ""; }
				echo "<td class='$class center' >".$v."</td>";
			} elseif($key=="taskadddate") {
				$v = is_numeric($v) ? date("d M Y",$v) : $v; 
				echo "<td class=center>".$v."</td>";
			} elseif($key=="tasktkid") {
				$v = isset($task_recipients[$t['taskid']]) ? $task_recipients[$t['taskid']] : array();
				if(count($v)>0) {
					$vtr = array();
					foreach($v as $r) {
						$vtr[] = $r['tkname'];
					}
					$txt = implode("<br />",$vtr);;
				} else {
					$txt = "N/A";
				}
				echo "<td>".$txt."</td>";
			} else {
				echo "<td>".$v."</td>";
			}
			break;
		case "U":
			$v = (isset($udfs[$t['taskid']][$key]) ? $udfs[$t['taskid']][$key]['udfvalue'] : "N/A");
			switch($udf_index[$key]['udfilist']) {
			case "Y":
				$v = (ASSIST_HELPER::checkIntRef($v) && isset($udf_list_values[$key][$v])) ? $udf_list_values[$key][$v]['udfvvalue'] : $v;
				$v = ($v!="0" && $v!="X") ? $v : "";
				break;
			case "M":
				$v = str_replace(chr(10),"<br />",$v);
				break;
			case "D":
				$v = is_numeric($v) ? date("d M Y",$v) : $v;
				break;
			case "N":
				$v = is_numeric($v) ? number_format($v,2) : $v;
				break;
			}
			echo "<td>$v</td>";
			break;
		}
	}
	echo "	<td width='65px' style='text-align: center'><input type=button value=Update id=".$t['taskid']." class=update /></td>
		</tr>";
} 
?>
</table>
</td></tr></table> <!-- end container table -->
<script type=text/javascript>
$(function() {
	$("#h_title").append("<?php echo " ".ASSIST_MODULE_HELPER::BREADCRUMB_DIVIDER." ".$title; ?>");
	$("#h_title").append("<?php echo " ".ASSIST_MODULE_HELPER::BREADCRUMB_DIVIDER." ".$status; ?>");
	$("#tbl_paging tr, .no-highlight").unbind("mouseenter mouseleave");
	$("th.head").css("padding","5 10 5 10");
	$("input:button.update").click(function() {
		var i = $(this).attr("id");
		document.location.href = 'update.php?act=<?php echo $view_type; ?>&i='+i;
	});
	//NAVIGATION
	var current = <?php echo $current; ?>;
	var rpp = <?php echo $rows_per_page; ?>;
	var ra = <?php echo $rows_available; ?>;
	var next = current + rpp;
	var prev = current - rpp;
	var last = ra - (ra % rpp); 
	if(last <= 0) { last = ra - rpp; } 
	var self = '<?php echo $me."?".$arg; ?>';
	if(current==0) { $("#paging #first, #paging #prev").attr("disabled","true"); }
	if(next>=ra) { $("#paging #last, #paging #next").attr("disabled","true"); }
	$("#paging input:button").click(function() {
		var page = 0;
		switch($(this).attr("id")) {
			case "first":	page = 0;    break;
			case "prev":	page = prev; break;
			case "next":	page = next; break;
			case "last":	page = last; break;
		}
		var st = $("form[name=my_search] #search_type").val();
		if(st=="ADVANCED") {
			$("form[name=my_search] #search_start").val(page);
			$("form[name=my_search]").submit();
		} else {
			document.location.href = self+"&start="+page;
		}
	});

	$("#search #search_item").change(function() {
		var st = $("#search #search_item option:selected").attr("search_type");
		switch(st) {
		case "text":
			$("#search input:text#txt_search").show();
			$("#search #sel_search").hide();
			$("#search #spn_date_search").hide();
			break;
		case "date":
			$("#search input:text#txt_search").hide();
			$("#search #sel_search").hide();
			$("#search #spn_date_search").show();
			break;
		case "list":
			$("#search input:text#txt_search").val('');
			$("#search input:text#txt_search").hide();
			$("#search input:text#date_search").val('');
			$("#search #spn_date_search").hide();
			$("#search select#sel_search").show();
			var v = $("#search select#search_item").val();
			ajax_viewList("SEARCH","field="+v);
			break;
		}
	});
	$("#search #search_item").trigger("change");
	$("#search input:button#search_go").click(function() {
		$("form[name=my_search] #get_details").val('0');  
		var si = $("#search #search_item").val();  
		var st = $("#search #search_item option:selected").attr("search_type");
		var sv = "";
		switch(st) {
		case "text":
			sv = $("#search input:text#txt_search").val();
			break;
		case "date":
			sv = $("#search input:text#date_search").val();
			break;
		case "list":
			sv = $("#search select#sel_search").val();
			break;
		}
		if(sv.length>0) {
			$("form[name=my_search] #si").val(si);
			$("form[name=my_search] #sv").val(sv);
			$("form[name=my_search] #search_type").val("QUICK");
			$("form[name=my_search]").submit();
		} else {
			alert("Please enter/select a search value.");
		}
	});
	$("#search #adv_search").click(function() {
		$("form[name=my_search]").attr("action","search.php");
		$("form[name=my_search]").submit();
	});
	$("#search #clear_search").click(function() {
		//$("form[name=my_search] #search_type").remove();
		//$("form[name=my_search] #get_details").val('0');  
		//$("form[name=my_search]").submit();
		var s = $("form[name=my_search] #s").val();
		var act = $("form[name=my_search] #act").val();
		var url = $("form[name=my_search]").attr("action");
		document.location.href = url+"?s="+s+"&act="+act;
	});
	//dev purposes only $(".noborder").css("border-color","#009900").css("border-width","2px").css("border-style","dashed");
});
</script>
<form name=my_search method=post action='<?php echo $me; ?>'>
	<input type=hidden name=get_details value='<?php echo (isset($_REQUEST['get_details']) ? $_REQUEST['get_details'] : 0); ?>' id=get_details />
	<input type=hidden name=orderby value='<?php echo $order_by; ?>' />
	<input type=hidden name=s value='<?php echo $statusid; ?>' id=s />
	<input type=hidden name=act value='<?php echo $view_type; ?>' id=act />
	<input type=hidden name=search[type] value='<?php echo isset($_REQUEST['search']['type'])?$_REQUEST['search']['type']:""; ?>' id=search_type />
	<input type=hidden name=start value=0 id=search_start />
	<?php if(!isset($_REQUEST['search']['type']) || $_REQUEST['search']['type']!="ADVANCED") { ?>
	<input type=hidden name=search[field] id=si value='<?php echo isset($_REQUEST['search']['field'])?$_REQUEST['search']['field']:""; ?>' />
	<input type=hidden name=search[value] id=sv value='<?php echo isset($_REQUEST['search']['value'])?$_REQUEST['search']['value']:""; ?>' />
	<?php 
	} elseif(isset($_REQUEST['search']['type']) && $_REQUEST['search']['type']=="ADVANCED") {
		$var = $_REQUEST;
		unset($var['s']); unset($var['act']); unset($var['orderby']); unset($var['search']);
		echo "<input type=hidden name=search[src] value=SERIAL />
				<input type=hidden name=search[value] value='".serialize($var)."' />";
	}
	?>
</form>
<?php
//phpinfo();
//arrPrint($_REQUEST);
?>
</body>

</html>
