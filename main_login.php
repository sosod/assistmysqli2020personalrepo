<?php
error_reporting(0);
/**************
 * CODE TO MEASURE PAGE LOAD TIME
 * ******************/
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;
$cc = $_SESSION['cc'];
function markTime($s) {
	global $start;
	global $cc;
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	//if($cc == "ait555d") {
	//echo '<p>'.$s.' => '.$total_time.' seconds.';
	//}
}


require_once("module/autoloader.php");
//require 'inc_session.php';
//require_once 'inc_assist.php';
//require_once 'inc_codehelper.php';
//require_once 'inc_db.php';
//require_once 'inc_db_conn.php';
require_once 'main/shortcuts.php';

markTime("includes");

if(in_array(strtolower($_SESSION['cc']), array("ait555d", "test987", "ipmsdev")) || strpos($_SERVER['HTTP_HOST'], "localhost") !== false) {
	error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
}

//arrPrint($_SESSION);
//$db = new ASSIST_DB();
$my_assist = new ASSIST_MODULE_HELPER();
$cmpcode = $my_assist->getCmpCode();
$tkid = $my_assist->getUserID();
//needed for main/stats(2) pages
$split_modules = $my_assist->getSplitModules();
$action_modules = $my_assist->getActionModules();
//needed for managing popups
$have_i_seen_the_fpd_today = $my_assist->haveISeenTheFPDYetToday();
$my_assist->IveSeenTheFPD();

/* Update SESSION with UserProfile */
$my_assist->getUserProfile();
/* Check if user wants the grid (old) main_login */
if(isset($_SESSION['USER_PROFILE'][4]['field1']) && $_SESSION['USER_PROFILE'][4]['field1'] == "grid") {
	echo "<script type=text/javascript>document.location.href = 'main_login_v1.php';</script>";
}

/* which module to display in table list */
$menu_mod = array('amenu' => "", 'docmenu' => "",);

if(isset($_REQUEST['amenu'])) {
	$menu_mod['amenu'] = $_REQUEST['amenu'];
	$_SESSION['MAIN']['amenu'] = $menu_mod['amenu'];
} elseif(isset($_SESSION['MAIN']['amenu'])) {
	$menu_mod['amenu'] = $_SESSION['MAIN']['amenu'];
} elseif(isset($_SESSION['USER_PROFILE'][1]['field1']) && strlen($_SESSION['USER_PROFILE'][1]['field1']) > 0) {
	if(strpos($_SESSION['USER_PROFILE'][1]['field1'],"_")!==false) {
		$up = explode("_",$_SESSION['USER_PROFILE'][1]['field1']);
		$_SESSION['USER_PROFILE'][1]['field1'] = $up[0];
		$_SESSION['USER_PROFILE'][1]['field1_mod_obj'] = $up[1];
	} else {
		$_SESSION['USER_PROFILE'][1]['field1_mod_obj'] = 0;
	}
	$menu_mod['amenu'] = $_SESSION['USER_PROFILE'][1]['field1'];
	$_SESSION['MAIN']['amenu'] = $menu_mod['amenu'];
}
if(isset($_REQUEST['docmenu'])) {
	$menu_mod['docmenu'] = $_REQUEST['docmenu'];
	$_SESSION['MAIN']['docmenu'] = $menu_mod['docmenu'];
} elseif(isset($_SESSION['MAIN']['docmenu'])) {
	$menu_mod['docmenu'] = $_SESSION['MAIN']['docmenu'];
} elseif(isset($_SESSION['USER_PROFILE'][3]['field1']) && strlen($_SESSION['USER_PROFILE'][3]['field1']) > 0) {
	$menu_mod['docmenu'] = $_SESSION['USER_PROFILE'][3]['field1'];
	$_SESSION['MAIN']['docmenu'] = $menu_mod['docmenu'];
}
if(isset($_REQUEST['mod_obj'])) {
	$menu_mod['mod_obj'] = $_REQUEST['mod_obj'];
	$_SESSION['MAIN']['mod_object'] = $menu_mod['mod_obj'];
} elseif(isset($_SESSION['MAIN']['mod_object'])) {
	$menu_mod['mod_obj'] = $_SESSION['MAIN']['mod_object'];
} elseif(isset($_SESSION['USER_PROFILE'][1]['field1_mod_obj']) && ($_SESSION['USER_PROFILE'][1]['field1_mod_obj']) > 0) {
	$menu_mod['mod_obj'] = $_SESSION['USER_PROFILE'][1]['field1_mod_obj'];
	$_SESSION['MAIN']['mod_object'] = $menu_mod['mod_obj'];
} elseif(isset($_SESSION['USER_PROFILE'][1]['field1']) && strlen($_SESSION['USER_PROFILE'][1]['field1']) > 0) {
	if(strpos($_SESSION['USER_PROFILE'][1]['field1'],"_")!==false) {
		$up = explode("_",$_SESSION['USER_PROFILE'][1]['field1']);
		$_SESSION['USER_PROFILE'][1]['field1_mod_obj'] = $up[1];
	} else {
		$_SESSION['USER_PROFILE'][1]['field1_mod_obj'] = 0;
	}
	$menu_mod['mod_obj'] = $_SESSION['USER_PROFILE'][1]['field1_mod_obj'];
	$_SESSION['MAIN']['mod_object'] = $menu_mod['mod_obj'];
} else {
	$menu_mod['mod_obj'] = 0;
	$_SESSION['MAIN']['mod_object'] = $menu_mod['mod_obj'];
}



/* Check menu items for Notification panel */

$sql = "SELECT m.* FROM assist_menu_modules m
		INNER JOIN assist_".$cmpcode."_menu_modules_users 
		ON modref = usrmodref AND usrtkid = '$tkid'
		WHERE modyn = 'Y'
		ORDER BY modtext";
$modules = $my_assist->mysql_fetch_all_by_id($sql, "modref");
$has_notifications = false;
$has_sdbp5b = false;
$has_ikpi1 = false;
$notification_modules = array('SDBP5B' => array(), 'IKPI1' => array());
foreach($modules as $r => $m) {
	if($m['modlocation'] == "SDBP5B") {
		$has_notifications = true;
		$has_sdbp5b = true;
		$notification_modules['SDBP5B'][$r] = $m;
	} elseif($m['modlocation'] == "IKPI1") {
		$has_notifications = true;
		$has_ikpi1 = true;
		$notification_modules['IKPI1'][$r] = $m;
	}
}
if($has_sdbp5b == true) {
	if(file_exists("SDBP5B/front_page.php")) {
		require_once "SDBP5B/front_page.php";
	}
}
if($has_ikpi1 == true) {
	if(file_exists("IKPI1/front_page.php")) {
		require_once "IKPI1/front_page.php";
	}
}

/********************************
 * TEMPORARY CODE FOR SECURITY QUESTIONS
 ******************************/

$temp_security = array('display_dialog'=>false);
if(!$my_assist->isAdminUser() && !$my_assist->isSupportUser() && !$have_i_seen_the_fpd_today) {
	$sql = "SELECT TS.datetime, TS.action 
			FROM assist_".$my_assist->getCmpCode()."_timekeep_temp_security TS 
			WHERE TS.tkid = '".$my_assist->getUserID()."'
			AND TS.action!='layout'
			ORDER BY TS.datetime DESC
			";
	$temp_security_rows = $my_assist->mysql_fetch_all_by_id($sql,"action");
	//$my_assist->arrPrint($temp_security);
//}
//if(!$my_assist->isAdminUser() && !$my_assist->isSupportUser() && is_null($temp_security['datetime'])) {
/*******************************************
 * POPI ACT pop up #AA-642
 */
	if(!isset($temp_security_rows['popia'])) {
			$temp_security['dialog_type'] = "popia";
			$temp_security['display_dialog'] = true;
	} elseif(!$my_assist->doPasswordSecurityAnswersPassInspection()) {
		$temp_security['dialog_type'] = "fix";
		$temp_security['display_dialog'] = true;
	//Disabled default pop up of the reminder to check the security password answers - no longer required & accidentally triggering for new users due to faulty logic on the display side	#AA-642
	//} else {
	//	$temp_security['dialog_type'] = "notice";
	}
	}
/*
	//Display pop-up with Company closure due to non-payment warning
	$closure_company = array("company_code_to_display_here");
	$show_warning = false;
	if($show_warning && in_array(strtolower($cmpcode), $closure_company) && !isset($_SESSION['account_close_notice'])) {
		$temp_security['dialog_type'] = "closure_warning";
		$temp_security['display_dialog'] = true;
	}
*/

markTime("temp_security");

/*********************************
 * END TEMPORARY CODE FOR SECURITY QUESTIONS
 **********************************/


/********************************
 * TEMPORARY CODE FOR FRONT PAGE LAYOUT CHANGE
 ******************************/
/*
 //REMOVED AS NO LONGER REQUIRED #AA-642
$layout_notice = array();
if(!$my_assist->isAdminUser() && !$my_assist->isSupportUser()) {
	$sql = "SELECT TK.tkid, TK.tkadddate, TS.datetime, TS.action 
			FROM assist_".$my_assist->getCmpCode()."_timekeep TK 
			LEFT JOIN assist_".$my_assist->getCmpCode()."_timekeep_temp_security TS 
			ON TK.tkid = TS.tkid
			WHERE TK.tkid = '".$my_assist->getUserID()."'
			AND TS.action='layout'
			ORDER BY TS.datetime DESC
			LIMIT 1";
	$layout_notice = $my_assist->mysql_fetch_one($sql);
	$layout_notice['display_dialog'] = false;
	//only display the layout notice to users who haven't seen it and who were created before the new main_login went live on 3 March 2019 6pm
	if(is_null($layout_notice['datetime']) && $layout_notice['tkadddate'] < strtotime("3 March 2019 18:00:00")) {
		$layout_notice['display_dialog'] = true;
	}
} else {
	$layout_notice['display_dialog'] = false;
}

*/
$layout_notice = array('display_dialog'=>false);
markTime("layout_notice");

/*********************************
 * END TEMPORARY CODE FOR SECURITY QUESTIONS
 **********************************/

$_REQUEST['jquery_version'] = "1.10.0";
$my_assist->echoPageHeader("1.10.0",array(),array("/assist_jquery.css"));


/* If Admin user then get display Admin user front page */
if($my_assist->getUserID() == "0000" || $my_assist->isAdminUser()) {
	echo "<script type=text/javascript>document.location.href = 'main_login_v1.php';</script>";


	/* else display normal user front page */
} else {
	markTime("start of divs");
	/* find out how far into the future to look */
	if(isset($_SESSION['USER_PROFILE'][1]['field4']) && is_numeric($_SESSION['USER_PROFILE'][1]['field4'])) {
		$next_due = $_SESSION['USER_PROFILE'][1]['field4'];
		$next_due_head = "Due within next ".$next_due." day".($next_due > 1 ? "s" : "");
	} else {
		$next_due = 7;
		$next_due_head = "Due this week";
	}
	require_once 'main/main.php';
	?>
	<table id="tbl_container">
		<tr>
			<td id="td_shortcuts" class="div_1">
				<DIV class=acc_display_left>
					<H3><A href="#" class=accord>Shortcuts</A></H3>
					<DIV id=div_short_container style="padding: 5px; text-align:center" class=''>
						<?php echo drawShortcuts($modules); ?>
					</DIV>
				</div>
			</td>
			<td id="td_actions" class=div_1>
				<div class="acc_display_middle_notices">
					<h3><a href="#" class="accord">Notifications <span id="spn_notices_count"></span></a></h3>
					<div id="div_notifications">
						<?php
						$have_echoed = false;
						$notices_count = 0;
						if($has_notifications !== false) {
							foreach($notification_modules['SDBP5B'] as $r => $m) {
								$echo = SDBP5frontpage($r, $m['modtext'], $next_due);
								if($echo !== false && strlen($echo) > 0 && strpos($echo, "No ") === false) {
									echo $echo;
									$have_echoed = true;
									$notices_count++;
								}
							}
							foreach($notification_modules['IKPI1'] as $r => $m) {
								$echo = IKPI1frontpage($r, $m['modtext'], $next_due);
								if($echo !== false && strlen($echo) > 0 && strpos($echo, "No ") === false) {
									echo $echo;
									$have_echoed = true;
									$notices_count++;
								}
							}
						}
						if($has_notifications === false || $have_echoed === false) {
							echo "<P>No reminders are awaiting your attention.</P>";
						}
						?>
					</div>
				</div>
				<div class="acc_display_middle_actions">
					<h3><a href="#" class="accord">Action Dashboard</a></h3>
					<div id="div_actions">
						<div id="div_action_container">
							<table width=100% class=noborder>
								<tr>
									<td class=noborder><?php include("main/stats2.php"); ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</td>
			<td id="td_table" class=div_1>
				<div class="acc_display_right">
					<?php
					if(strlen($_SESSION['MAIN']['modref'])>0 && strlen($_SESSION['MAIN']['modlocation'])>0) {
						?><h3><a href="#" class="accord"><?php echo $_SESSION['MAIN']['modtext']; ?>:
							Actions <?php echo $next_due_head; ?></a></h3>
					<?php
					} else {
						echo "<h3><a href='#' class='accord'>Actions ".$next_due_head."</a></h3>";
					}
					?>
					<div id="div_table">
						<div id="div_table_container">
							<?php include("main/tables2.php"); ?>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<?php

	markTime("end of divs");
}


//ASSIST_HELPER::arrPrint($_SESSION);


?>
<style type="text/css">
	h3 {
		font-family: "Trebuchet MS", "Helvetica", "Arial", "Verdana", "sans-serif";
	}

	/* format container table */
	#tbl_container {
		border: 0px solid #cc0001;
		margin: 0px;
	}

	#tbl_container td.div_1 {
		border: 0px solid #cc0001;
		padding: 2px;
	}

	/* format accordions */
	.ui-accordion .ui-accordion-content {
		padding: 0 0 0 0;
	}

	.ui-state-active {
		background: #e6e6e6 url(/library/jquery/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x;
		font-weight: bold;
	}

	.ui-accordion .ui-accordion-header {
		border-bottom: 1px;
	}

	/* shortcuts */
	.sortablefolders {
		list-style-type: none;
		margin: 0;
		padding: 0;
		list-style-image: url();
	}

	.sortablefolders li {
		margin: 3px 3px 3px 0;
		padding: 0px;
		float: left;
		width: 100px;
		height: 100px;
		text-align: center;
		vertical-align: middle;
		font-size: 7pt;
		border: 1px solid #ffffff;
	}

	/* action */
	#div_action_container {
		overflow: auto;
	}

	#stats, #stats td, #stats th {
		font-size: 7pt;
	}

	#stats a, #stats a.visited {
		text-decoration: none;
		font-weight: bold;
	}

	#stats a.hover {
		color: #fe9900;
	}

	/* action table */
	#div_table_container {
		padding: 3px;
		overflow: auto;
	}

	#div_table_container td.td_ref {
		font-weight: bold;
		color: #000099;
		text-align: center;
	}
</style>
<script type="text/javascript">
	$(function() {
		<?php
		if($notices_count > 0) {
			echo "$('#spn_notices_count').text('(".$notices_count.")');";
		}
		?>
		/*
		Format container divs
		 */
		$(window).resize(function() {
			var screen_width = AssistHelper.getWindowSize();
			var w = screen_width.width;
			var h = screen_width.height;
			h -= 2; //account for margins
			var shortcuts_width = 120;
			var actions_width = (w - shortcuts_width) * 0.35;
			var table_width = w - shortcuts_width - actions_width - 30;
			$("#td_shortcuts").width(shortcuts_width);
			$("#td_actions").width(actions_width);
			$("#td_table").width(table_width);
			$("#tbl_container").height(h);
			var action_container_height = h - 3 - 40 - $(".acc_display_middle_notices").height();
			if(action_container_height < 0) {
				action_container_height = 150;
			}
			$("#div_action_container").height(action_container_height).css("border", "0px solid #ffffff");
			var table_container_height = h - 7 - 40;
			$("#div_table_container").height(table_container_height);
			//$(".acc_display_right").accordion("refresh");
			var short_container_height = h - 11 - 40;
			$("#div_short_container").height(short_container_height);
			//$(".acc_display_left").accordion("refresh");
		});
		/*
		Shortcuts
		 */
		$(".acc_display_left").accordion({
			collapsible: false,
			heightStyle: "content"
		});
		$("ul.sortablefolders").find("li").each(function() {
			$(this).hover(function() {
				$(this).css("border", "1px solid #fe9900").css("color", "#fe9900");
			}, function() {
				$(this).css("border", "1px solid #ffffff").css("color", "");
			});
			$(this).click(function() {
				otherChangeMod($(this));
			});
		});

		/*$( ".sortablefolders" ).sortable();
		$( ".sortablefolders" ).disableSelection();


		/*
		Format middle
		*/
		$(".acc_display_middle_notices").accordion({
			collapsible: true,
			heightStyle: "content",
			activate: function() {
				$(window).trigger("resize");
			}
		});
		$(".acc_display_middle_actions").accordion({
			collapsible: true,
			heightStyle: "content"
		});

		/*
		Actions
		*/
		$("#stats a").click(function() {
			var modref = $(this).attr("modref");
			modref = modref.toUpperCase();
			var mod_obj = $(this).attr("mod_obj");
			chngStatsMod(modref,mod_obj);
		});
		var click_array = new Array("", "class", "style", "shape", "aria-disabled");
		$("#action_tables button").click(function(e) {
			e.preventDefault();
			if(!($(this).hasClass("accord")) && !($(this).hasClass("no_click"))) {
				otherChangeMod($(this));
			}
		});
		$("#action_tables button").not("#btn_action_new").button({
			icons: {primary: "ui-icon-check"}
		}).css("font-size", "7pt");
		$("#action_tables #btn_action_new").button({
			icons: {primary: "ui-icon-plus"}
		}).css("font-size", "7pt");

		$("#stats tr").hover(function() {
			$(this).find("td").css("background-color", "#eeeeee");
		}, function() {
			$(this).find("td").css("background-color", "");
		});

		/*
		Table
		*/
		$(".acc_display_right").accordion({
			collapsible: false,
			heightStyle: "content"
		});

		function chngMod(i, v) {
			AssistHelper.processing();
			var u = new Array();
			u['amenu'] = '<?php echo $menu_mod['amenu']; ?>';
			u['docmenu'] = '<?php echo $menu_mod['docmenu']; ?>';
			if(v.length > 0) {
				u[i] = v;
			}
			url = 'main_login.php?amenu=' + u['amenu'] + '&docmenu=' + u['docmenu'];
			document.location.href = url;
		}

		function chngStatsMod(modref,mod_obj) {
			AssistHelper.processing();
			var docmenu = '<?php echo $menu_mod['docmenu']; ?>';
			url = 'main_login.php?amenu=' + modref + '&docmenu=' + docmenu + '&mod_obj=' + mod_obj;
			document.location.href = url;
		}

		function otherChangeMod($me) {
			AssistHelper.processing();
			var attrs = $me[0].attributes;
			var url = "";
			for(var i = 0; i < attrs.length; i++) {
				nn = attrs[i].nodeName;
				//alert(attrs[i].nodeName + ' => ' + $.inArray(attrs[i].nodeName,click_array));
				if(($.inArray(attrs[i].nodeName, click_array)) < 0 && nn.substr(0, 6) != "jQuery") {
					url = url + "&" + attrs[i].nodeName + "=" + attrs[i].nodeValue;
				}
			}
			url = 'main/link.php?a=click' + url;
			//console.log(url);
			document.location.href = url;
		}

		/* trigger resize function to format container divs for the first time */
		$(window).trigger("resize");
	});
</script>

<?php
if($layout_notice['display_dialog'] == true) {
	?>
	<div id=dlg_layout_notice title="Important Notice">
		<h1>New Front Page Dashboard Layout</h1>
		<p>We have updated the Front Page Dashboard to have a more Action focus.</p>
		<p>In the new layout you will find your Shortcuts on the left, Notifications & Action Dashboard in the middle
			and Actions Due on the right.</p>
		<p>If you would like to go back to the old layout, please go to Main Menu > My Profile and choose "Grid" in the
			Front Page Layout section.</p>
		<p><input type=button value=OK id=btn_layout_notice_close class=no_click/></p>
		<p class=iinform>This message will not appear again once you click the button above.</p>
	</div>
	<script type="text/javascript">
		$(function() {
			//$("<div />",{id:"security_notice",title:"Important Notice",html:"<h1>Notice</h1><p>We've updated the 'Change Password' page (accessed from the Main Menu).</p><p>You can now update your answers to the Password Security Questions.</p><p>It is important that these answers are correct as you will be required to provide a correct answer to one of the questions in order to reset your password via the Forgotten Password function now available from the login page.</p>"})
			$("#dlg_layout_notice")
					.dialog({
						modal: true,
						dialogClass: "temp_security",
						width: "500px"
					}).addClass("ui-corner-all").css({
				"border": "2px solid #fe9900",
				"background-color": "#fefefe"
			}).children("h1").css("color", "#fe9900");
			AssistHelper.hideDialogTitlebar("id", "dlg_layout_notice");
			AssistHelper.hideDialogCSS("class", "temp_security");
			//$(".temp_security").css({border:"0px solid #ffffff","background-color":"#ffffff","padding":"0px"});
			$("#dlg_layout_notice input:button").button().css({
				"color": "#009900",
				"border": "1px solid #009900"
			}).eq(0).blur();
			$("#dlg_layout_notice input:button").click(function() {
				AssistHelper.doAjax("TK/ajax_controller.php", "act=LOG_THAT_USER_HAS_SEEN_LAYOUT_CHANGE_NOTICE");
				$("#dlg_layout_notice").dialog("close");
			});
		});
	</script>
	<?php
} elseif($temp_security['display_dialog'] === true) { ?>
	<div id=dlg_security_notice title="Important Notice">
		<?php
		if($temp_security['dialog_type'] == "popia") {
			?>
			<h1 class=center>Important information - POPI Act</h1>
			<p>Action iT appreciates your business and is dedicated to continuing providing you with our performance management solution.</p>
			<p>As part of our ongoing commitment to protecting your personal information and ensuring that, at all times, you have access to a safe and secure system, we would like to assure you that your data privacy is our top priority.</p>
			<p>For all matters relating to the Protection of Personal Information Act, 4 of 2013 please contact our information officers:<ul>
				<li>Robert Klein - robert@actionassist.co.za</li>
				<li>Janet Currie - janet@actionassist.co.za</li>
			</ul></p>
			<p>To find out more on how we protect your interests please review our Privacy Policy and Terms and Conditions on our website at <a href="http://www.actionassist.co.za" target="_blank" >www.actionassist.co.za</a>.</p>
			<p>Should you have any questions or concerns regarding our compliance to the POPIA, please do not hesitate to contact our offices. </p>
			<p class="i">Kind regards, <br />
			Robert Klein<br />
			Action iT CEO</p>
			<p class=center>
				<button id=btn_popia>I acknowledge this message</button>
			</p>
			<?php
		} elseif($temp_security['dialog_type'] == "closure_warning") {
			?>
			<h1 class=center>Alert</h1>
			<p class=center>Please be advised that this account will be closed shortly
				<br/>due to non-renewal of contract.</p>
			<p class=center>
				<button id=btn_closure_warning>OK</button>
			</p>
			<?php
		} elseif($temp_security['dialog_type'] == "fix") {
			?>
			<h1>Notice</h1>
			<p class=b>Please read this carefully.</p>
			<p>We've updated the 'Change Password' page (accessed from the Main Menu).</p>
			<p>You can now update your answers to the Password Security Questions.</p>
			<p>It is important that these answers are correct as you will be required to provide a correct answer to one
				of the questions in order to reset your password via the Forgotten Password function now available from
				the login page.</p>
			<?php
			if($temp_security['dialog_type'] == "fix") {
				ASSIST_HELPER::displayResult(array("error", "Analysis of your security answers shows that they were possibly pre-set by your Business Partner when your login was created.  It is critical that you review your password security answers and update if necessary."));
			}
			?>
			<p><input type=button value=Close id=btn_security_notice_close class=no_click/><span class=float><input
							type=button value='Review Security Answers' id=btn_security_notice_go
							class=no_click/></span></p>
			<p class=iinform>Clicking one of the buttons above will prevent this notice from appearing again.</p>
		<?php } ?>
	</div>
	<script type="text/javascript">
		$(function() {
			<?php
			if($temp_security['dialog_type'] == "popia") {
			?>
			$("#dlg_security_notice")
					.dialog({
						modal: true,
						dialogClass: "temp_security",
						width: "500px"
					}).addClass("ui-corner-all").css({
				"border": "2px solid #009900",
				"background-color": "#fefefe"
			}).children("h1").css("color", "#009900");
			AssistHelper.hideDialogTitlebar("id", "dlg_security_notice");
			AssistHelper.hideDialogCSS("class", "temp_security");
			$("#dlg_security_notice #btn_popia").button({
				icons: {primary:"ui-icon-check"}
			}).removeClass("ui-state-default").addClass("ui-button-state-green").click(function() {
				AssistHelper.processing();
				AssistHelper.doAjax("TK/ajax_controller.php", "act=LOG_THAT_USER_HAS_SEEN_POPIA");
				$("#dlg_security_notice").dialog("close");
				//document.location.href = url;
				AssistHelper.closeProcessing();
			});
			$("#dlg_security_notice a:first").blur();

			<?php
			} elseif($temp_security['dialog_type'] == "closure_warning") {
			?>
			//$("<div />",{id:"security_notice",title:"Important Notice",html:"<h1>Notice</h1><p>We've updated the 'Change Password' page (accessed from the Main Menu).</p><p>You can now update your answers to the Password Security Questions.</p><p>It is important that these answers are correct as you will be required to provide a correct answer to one of the questions in order to reset your password via the Forgotten Password function now available from the login page.</p>"})
			$("#dlg_security_notice")
					.dialog({
						modal: true,
						dialogClass: "temp_security",
						width: "500px"
					}).addClass("ui-corner-all").css({
				"border": "2px solid #cc0001",
				"background-color": "#fff9f9"
			}).children("h1").css("color", "#cc0001");
			AssistHelper.hideDialogTitlebar("id", "dlg_security_notice");
			AssistHelper.hideDialogCSS("class", "temp_security");
			//$(".temp_security").css({border:"0px solid #ffffff","background-color":"#ffffff","padding":"0px"});
			$("#dlg_security_notice #btn_closure_warning").button().css({
				"color": "#000000",
				"border": "1px solid #000000"
			}).eq(0).blur();
			$("#dlg_security_notice #btn_closure_warning").click(function() {
				AssistHelper.processing();
				AssistHelper.doAjax("TK/ajax_controller.php", "act=LOG_THAT_USER_HAS_SEEN_CLOSURE_WARNING");
				$("#dlg_security_notice").dialog("close");
				//document.location.href = url;
				AssistHelper.closeProcessing();
			});
			<?php
			} elseif($temp_security['dialog_type'] == "fix") {
			?>
			//$("<div />",{id:"security_notice",title:"Important Notice",html:"<h1>Notice</h1><p>We've updated the 'Change Password' page (accessed from the Main Menu).</p><p>You can now update your answers to the Password Security Questions.</p><p>It is important that these answers are correct as you will be required to provide a correct answer to one of the questions in order to reset your password via the Forgotten Password function now available from the login page.</p>"})
			$("#dlg_security_notice")
					.dialog({
						modal: true,
						dialogClass: "temp_security",
						width: "500px"
					}).addClass("ui-corner-all").css({
				"border": "2px solid #fe9900",
				"background-color": "#fefefe"
			}).children("h1").css("color", "#fe9900");
			AssistHelper.hideDialogTitlebar("id", "dlg_security_notice");
			AssistHelper.hideDialogCSS("class", "temp_security");
			//$(".temp_security").css({border:"0px solid #ffffff","background-color":"#ffffff","padding":"0px"});
			$("#dlg_security_notice input:button").button().css({
				"color": "#000000",
				"border": "1px solid #555555"
			}).eq(0).blur();
			$("#dlg_security_notice input:button").click(function() {
				var i = $(this).prop("id");
				switch(i) {
					case "btn_security_notice_close":
						var url = "main_login.php";
						var action = "Close";
						break;
					case "btn_security_notice_go":
						var url = "TK/password.php";
						var action = "Review";
						break;
				}
				AssistHelper.processing();
				AssistHelper.doAjax("TK/ajax_controller.php", "act=LOG_THAT_USER_HAS_SEEN_SECURITY_NOTICE&action=" + action);
				//$("#dlg_security_notice").dialog("close");
				document.location.href = url;
			});
			<?php
			}

			?>
		});
	</script>
<?php } ?>
<?php markTime("end of page"); ?>
</body>

</html>
