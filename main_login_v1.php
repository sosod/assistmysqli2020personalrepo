<?php 

/**************
 * CODE TO MEASURE PAGE LOAD TIME 
 * ******************/
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;
$cc = $_SESSION['cc'];
function markTime($s) {
	global $start;
global $cc;
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
if($cc=="ait555d") {
//	echo '<p>'.$s.' => '.$total_time.' seconds.';
}	
}



//echo "<Pre>"; print_r($_SESSION);


//require_once("library/class/autoload.php");
require_once("module/autoloader.php");

//echo "<pre>"; print_r($_SESSION);
//    require 'inc_session.php';
//require_once 'inc_assist.php';
//require_once 'inc_codehelper.php';
//require_once 'inc_db.php';
//require_once 'inc_db_conn.php';
require_once 'main/shortcuts.php';

markTime("includes");

//arrPrint($_SESSION);
$db = new ASSIST_DB();
$my_assist = new ASSIST_MODULE_HELPER();
$cmpcode = $my_assist->getCmpCode();
$tkid = $my_assist->getUserID();
$today = time();
//needed for main/stats(2) pages
$split_modules = $my_assist->getSplitModules();
$action_modules = $my_assist->getActionModules();
//needed for managing popups
$have_i_seen_the_fpd_today = $my_assist->haveISeenTheFPDYetToday();
$my_assist->IveSeenTheFPD();

//if(strtolower($_SESSION['cc'])=="ait555d") {
//	error_reporting(-1);
//}

/********************************
TEMPORARY CODE FOR SECURITY QUESTIONS
******************************/

$temp_security = array('display_dialog'=>false);
if(!$my_assist->isAdminUser() && !$my_assist->isSupportUser() && !$have_i_seen_the_fpd_today) {
	$sql = "SELECT TS.datetime, TS.action 
			FROM assist_".$my_assist->getCmpCode()."_timekeep_temp_security TS 
			WHERE TS.tkid = '".$my_assist->getUserID()."'
			AND TS.action!='layout'
			ORDER BY TS.datetime DESC
			";
	$temp_security_rows = $my_assist->mysql_fetch_all_by_id($sql,"action");
	//$my_assist->arrPrint($temp_security);
//}
//if(!$my_assist->isAdminUser() && !$my_assist->isSupportUser() && is_null($temp_security['datetime'])) {
/*******************************************
 * POPI ACT pop up #AA-642
 */
	if(!isset($temp_security_rows['popia'])) {
			$temp_security['dialog_type'] = "popia";
			$temp_security['display_dialog'] = true;
	} elseif(!$my_assist->doPasswordSecurityAnswersPassInspection()) {
		$temp_security['dialog_type'] = "fix";
		$temp_security['display_dialog'] = true;
	//Disabled default pop up of the reminder to check the security password answers - no longer required & accidentally triggering for new users due to faulty logic on the display side	#AA-642
	//} else {
	//	$temp_security['dialog_type'] = "notice";
	}
	}
/*
	//Display pop-up with Company closure due to non-payment warning
	$closure_company = array("company_code_to_display_here");
	$show_warning = false;
	if($show_warning && in_array(strtolower($cmpcode),$closure_company) && !isset($_SESSION['account_close_notice'])) {
		$temp_security['dialog_type'] = "closure_warning";
		$temp_security['display_dialog'] = true;
	}
*/


markTime("temp_security");


/*********************************
END TEMPORARY CODE FOR SECURITY QUESTIONS
**********************************/

if($my_assist->getUserID()=="0000") {
	require_once("main/admin_user.php");
}

//arrPrint($_SESSION);


$my_assist->getUserProfile();

//arrPrint($_SESSION);

/* which module to display in table list */
$menu_mod = array('amenu' => "", 'docmenu' => "",);

if(isset($_REQUEST['amenu'])) {
	$menu_mod['amenu'] = $_REQUEST['amenu'];
	$_SESSION['MAIN']['amenu'] = $menu_mod['amenu'];
} elseif(isset($_SESSION['MAIN']['amenu'])) {
	$menu_mod['amenu'] = $_SESSION['MAIN']['amenu'];
} elseif(isset($_SESSION['USER_PROFILE'][1]['field1']) && strlen($_SESSION['USER_PROFILE'][1]['field1']) > 0) {
	if(strpos($_SESSION['USER_PROFILE'][1]['field1'],"_")!==false) {
		$up = explode("_",$_SESSION['USER_PROFILE'][1]['field1']);
		$_SESSION['USER_PROFILE'][1]['field1'] = $up[0];
		$_SESSION['USER_PROFILE'][1]['field1_mod_obj'] = $up[1];
	} else {
		$_SESSION['USER_PROFILE'][1]['field1_mod_obj'] = 0;
	}
	$menu_mod['amenu'] = $_SESSION['USER_PROFILE'][1]['field1'];
	$_SESSION['MAIN']['amenu'] = $menu_mod['amenu'];
}
if(isset($_REQUEST['docmenu'])) {
	$menu_mod['docmenu'] = $_REQUEST['docmenu'];
	$_SESSION['MAIN']['docmenu'] = $menu_mod['docmenu'];
} elseif(isset($_SESSION['MAIN']['docmenu'])) {
	$menu_mod['docmenu'] = $_SESSION['MAIN']['docmenu'];
} elseif(isset($_SESSION['USER_PROFILE'][3]['field1']) && strlen($_SESSION['USER_PROFILE'][3]['field1']) > 0) {
	$menu_mod['docmenu'] = $_SESSION['USER_PROFILE'][3]['field1'];
	$_SESSION['MAIN']['docmenu'] = $menu_mod['docmenu'];
}
if(isset($_REQUEST['mod_obj'])) {
	$menu_mod['mod_obj'] = $_REQUEST['mod_obj'];
	$_SESSION['MAIN']['mod_object'] = $menu_mod['mod_obj'];
} elseif(isset($_SESSION['MAIN']['mod_object'])) {
	$menu_mod['mod_obj'] = $_SESSION['MAIN']['mod_object'];
} elseif(isset($_SESSION['USER_PROFILE'][1]['field1_mod_obj']) && ($_SESSION['USER_PROFILE'][1]['field1_mod_obj']) > 0) {
	$menu_mod['mod_obj'] = $_SESSION['USER_PROFILE'][1]['field1_mod_obj'];
	$_SESSION['MAIN']['mod_object'] = $menu_mod['mod_obj'];
} elseif(isset($_SESSION['USER_PROFILE'][1]['field1']) && strlen($_SESSION['USER_PROFILE'][1]['field1']) > 0) {
	if(strpos($_SESSION['USER_PROFILE'][1]['field1'],"_")!==false) {
		$up = explode("_",$_SESSION['USER_PROFILE'][1]['field1']);
		$_SESSION['USER_PROFILE'][1]['field1_mod_obj'] = $up[1];
	} else {
		$_SESSION['USER_PROFILE'][1]['field1_mod_obj'] = 0;
	}
	$menu_mod['mod_obj'] = $_SESSION['USER_PROFILE'][1]['field1_mod_obj'];
	$_SESSION['MAIN']['mod_object'] = $menu_mod['mod_obj'];
} else {
	$menu_mod['mod_obj'] = 0;
	$_SESSION['MAIN']['mod_object'] = $menu_mod['mod_obj'];
}

//arrPrint($_SESSION);


//error_reporting(-1);

$sql = "SELECT m.* FROM assist_menu_modules m
		INNER JOIN assist_".$cmpcode."_menu_modules_users 
		ON modref = usrmodref AND usrtkid = '$tkid'
		WHERE modyn = 'Y'
		ORDER BY modtext";
$modules = $db->mysql_fetch_all_by_id($sql,"modref");	
//if($_SESSION['cc']=="aud999d" && $my_assist->isSupportUser()) { ASSIST_HELPER::arrPrint($modules); }
$menu = array();
$has = array(
	'SDBP4'=>false,
	'SDBP5'=>false,
	'DOC'=>false,
	'MDOC'=>false,
	'DSHP'=>false,
	'ACTION'=>false,
	'DOC0'=>false,
	'SDBP5B'=>false,
);
$docref = $menu_mod['docmenu'];  
$mdocref = "MDOC";
$actref = $menu_mod['amenu'];  
foreach($modules as $m) {
	$ml = $m['modlocation'];
	if(in_array($ml,$action_modules)) { 
		$has['ACTION'] = true;
		if(strlen($actref)==0) {
			$actref = $m['modref'];
			$menu_mod['amenu'] = $actref;
			$_SESSION['MAIN']['amenu'] = $actref;
		}
	} 
	switch($ml) {
	//case "SDBP4":	//Removed older modules v4 & v5 to avoid conflicts..
	//case "SDBP5": //..with SDBP5B & IKPI1 [JC 2019-03-25 AA-65]
	case "SDBP5B":
	case "DSHP":
		if($m['modref']!="SDP15I") {
			if(file_exists($ml."/front_page.php")) { require_once $ml."/front_page.php"; }
			$has[$ml] = true;
			$menu[$ml] = array('ref'=>$m['modref'],'title'=>$m['modtext']);
		} else {
			$menu[$m['modref']] = array('ref'=>$m['modref'],'title'=>$m['modtext']);
		}		
		//if(file_exists($ml."/front_page.php")) { require_once $ml."/front_page.php"; }
		//$has[$ml] = true;
		//$menu[$ml] = array('ref'=>$m['modref'],'title'=>$m['modtext']);
		break;
	case "ACTION":
	case "ACTION0":
		$has['ACTION'] = true;
		break;
	case "MDOC":
	case "DOC":
	case "DOC0":
		require_once $ml."/front_page.php";
		$has[$ml] = true;
		if(($ml=="DOC" || $ml=="DOC0") && strlen($docref)==0) {
			$docref = $m['modref'];
			$menu_mod['docmenu']=$docref;
			$_SESSION['MAIN']['docmenu'] = $docref;
		} elseif($ml=="MDOC" && strlen($mdocref)==0) {
			$mdocref = $m['modref'];
			$menu_mod['mdocmenu']=$mdocref;
			$_SESSION['MAIN']['mdocmenu'] = $mdocref;
		}
		$menu[$ml][$m['modref']] = array('ref'=>$m['modref'],'title'=>$m['modtext']);
		break;
	}
}
/*
if($_SESSION['cc']=="ait0001" && $_SESSION['tku']=="janet") { 
	arrPrint($has); 
	arrPrint($menu);
	echo ":".$docref.":";
}*/

markTime("action dashboard module validation");

function displayWelcome() {
	global $tkname, $tkid;
		echo "<span id=spn_name><h1 style='margin-top: 0px;'>Welcome $tkname</h1><input type=hidden name=tkid value='".$tkid."' />
				<p>If you are not $tkname, please click <a href=logout.php>here</a> to logout.</p></span>";
}

$_REQUEST['jquery_version'] = "1.10.0";
//$my_assist->displayPageHeader();
$my_assist->echoPageHeader("1.10.0",array(),array("/assist_jquery.css"));

markTime("displayPageHeader");
?>

<!--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd"> 
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>assist.Ignite4u.co.za</title>
<base target="main">
</head>
<link rel="stylesheet" href="assist.css" type="text/css">
		<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
		<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>        
<link href="/library/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type ="text/javascript" src="/library/jquery/js/jquery.min.js"></script>
<script type ="text/javascript" src="/library/jquery/js/jquery-ui.min.js"></script> -->
<style type=text/css>
//table .noborder, table td .noborder { border: 2px dashed #009900; }
#stats, #stats td, #stats th { font-size: 7pt;}
//#list { border: 1px solid #fe9900; }

.ui-widget-content p { 
	padding-bottom: 10px;
}
p.nopad {
	padding-bottom: 0px;
}

#div_dshp span { text-decoration: none;  cursor: pointer; }
#div_dshp span.hover { color: #fe9900; cursor: pointer; }
#div_dshp td {font-size: 7pt; }

#stats a, #stats a.visited { color: #000099; text-decoration: none; font-weight: bold; }
#stats a.hover { color: #fe9900; }

.ui-accordion .ui-accordion-content { padding: 0 0 0 0; }
.ui-state-active { background: #e6e6e6 url(/library/jquery/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: bold; }
.ui-accordion .ui-accordion-header { border-bottom: 1; }

.a_hover { color: #FE9900; border: 1px solid #FE9900; cursor: pointer;}
.no_hover { border: 1px solid #ffffff;  }

div.acc_height_normal { height: 400px; }
div.acc_height_sdbip { height: 200px; }
div.acc_height_dash { height: 270px; }

div.short_min { height: 100px; }

div.acc_display h3, div.acc_display2 h3 { font-family: Verdana; }

.more_text { cursor: pointer; }

	.sortablefolders { list-style-type: none; margin: 0; padding: 0; list-style-image: url(); }
	.sortablefolders li {  margin: 3px 3px 3px 0; padding: 0px; float: left; width: 100px; height: 100px;  text-align: center; vertical-align: middle; font-size: 7pt; }
	</style>
	<script>
	$(function() {
		$( ".sortablefolders" ).sortable();
		$( ".sortablefolders" ).disableSelection();
	});
	</script>

<!--<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>-->

<?php
if($tkid=="0000") {

	echo displayWelcome()
		."<div id=div_right style=\"background-color: #ffffff; width: 64%;  float: right; \">";
	drawAdminUserModuleShortcuts();
	echo "</div>"
		."<div id=div_left style=\"background-color: #ffffff; width: 35%; \">";
	drawAdminSystemShortcuts();
	echo "</div>";
	



} else {
	markTime("start of divs");
?>
<div id=div_right style="background-color: #ffffff; width: 64%; height: 800px; float: right; margin: -15 0; padding: 0 0;">
<?php 
if($has['ACTION']) {
	if(isset($_SESSION['USER_PROFILE'][1]['field4']) && is_numeric($_SESSION['USER_PROFILE'][1]['field4'])) {
		$next_due = $_SESSION['USER_PROFILE'][1]['field4'];
		$next_due_head = "Due within next ".$next_due." day".($next_due>1 ? "s" : "");
	} else {
		$next_due = 7;
		$next_due_head = "Due this week";
	}

	require_once 'main/main.php';
	require_once 'main/graph.php';
		echo "
			<DIV class=acc_display id=acc_dash>
				<H3><A href=\"#\" class=accord>Action Dashboard</A></H3>
				<DIV >
					<table width=100% class=noborder><tr><td class=noborder><span style=\"float: left\">";
						include("main/stats.php"); 
				echo "</span></td><td class=noborder><span class=float>";
				if(isset($graph) && is_array($graph) && array_sum($graph)>0) {
					echo "<input type=hidden name=show_graph value='Y' />";
					drawMyActionDashboardGraph($graph);
				} else {
					echo "<input type=hidden name=show_graph value='N' />";
				}
				echo "</span></td></tr></table>";
		echo "</DIV>
			</DIV>";
		markTime("summary loaded");
		echo "
			<DIV class=acc_display>
				<H3><A href=\"#\" class=accord>Actions ".$next_due_head."</A></H3>
				<DIV id=action_tables>";
				include("main/tables.php");
			echo "</DIV>
			</DIV>
		";
		markTime("table loaded");
} elseif($tkid=="0000") {
	drawAdminUserModuleShortcuts();
}
?>
</div>
<div id=div_left style="background-color: #ffffff; width: 35%; height: 800px; margin-top: 0px;">
<?php
echo displayWelcome();
markTime("tables done, welcome displayed");
//echo strtotime("21-Feb-2012 12:00:00")."  >>  ".strtotime("19-Feb-2012 0:00:00");
if($tkid!="0000") {
	echo "<DIV class=acc_display>
			<H3><A href=\"#\" class=accord>My Shortcuts</A></H3>
			<DIV id=div_short style=\"padding: 5px;\" class=''>".drawShortcuts($modules)."</DIV>
		</div>";
markTime("shortcuts displayed");
/*if($has['SDBP4']) {
	$m = $menu['SDBP4']; //arrPrint($m);
	$echo = SDBP4frontpage($m['ref'],$m['title']);
	if($echo!==false && strlen($echo)>0) {
			echo "<DIV class=acc_display>
					<H3><A href=\"#\" class=accord>".$m['title']." Reminders</A></H3>
					<DIV>".$echo."</DIV>
				</div>";
	}
}*/
markTime("end sdbp 4");
if($has['SDBP5']) {
	$m = $menu['SDBP5']; //arrPrint($m);
	$echo = SDBP5frontpage($m['ref'],$m['title']);
	if($echo!==false && strlen($echo)>0) {
			echo "<DIV class=acc_display>
					<H3><A href=\"#\" class=accord>".$m['title']." Reminders</A></H3>
					<DIV>".$echo."</DIV>
				</div>";
	}
	if(isset($menu['SDP15I'])) {
		$m = $menu['SDP15I']; //arrPrint($m);
		$echo = SDBP5frontpage($m['ref'],$m['title']);
		if($echo!==false && strlen($echo)>0) {
				echo "<DIV class=acc_display>
						<H3><A href=\"#\" class=accord>".$m['title']." Reminders</A></H3>
						<DIV>".$echo."</DIV>
					</div>";
		}
	}
}
markTime("end sdbp 5");
if($has['DSHP']) {
	$m = $menu['DSHP'];
	$ds = 0;
	$echo = DSHPfrontpage($m,$ds);
	if($echo!== false) {
			echo "<DIV class=acc_display>
					<H3><A href=\"#\" class=accord>".$m['title']."</A></H3>
					<DIV id=div_dshp ds=".$ds." style=\"\">".$echo."</DIV>
				</div>";
	}
}
markTime("dshp");
if($has['DOC'] && strlen($docref)>0 && isset($menu['DOC'][$docref])) {
	$m = $menu['DOC'][$docref];
	$dc = 0;
	$echo = DOCfrontpage($m,$menu['DOC'],$dc);
	if($echo!==false) {
			echo "<DIV class=acc_display>
					<H3><A href=\"#\" class=accord>Document Management</A></H3>
					<DIV id=div_doc dc=".$dc." style=\"height: 180px\">".$echo."</DIV>
				</DIV>";
			echo DOCcomment($m);
	}
} elseif($has['DOC0'] && strlen($docref)>0 && isset($menu['DOC0'][$docref])) {
	$m = $menu['DOC0'][$docref];
	$dc = 0;
	$echo = DOCfrontpage($m,$menu['DOC0'],$dc);
	if($echo!==false) {
			echo "<DIV class=acc_display>
					<H3><A href=\"#\" class=accord>Document Management</A></H3>
					<DIV id=div_doc dc=".$dc." style=\"height: 180px\">".$echo."</DIV>
				</DIV>";
			echo DOCcomment($m);
	}
}
markTime("doc");
if($has['MDOC'] && strlen($mdocref)>0) {
	$m = $menu['MDOC'][$mdocref];
	$dc = 0;
	$echo = MDOCfrontpage($m,$menu['MDOC'],$dc);
	if($echo!==false) {
			echo "<DIV class=acc_display>
					<H3><A href=\"#\" class=accord>Module Document Management</A></H3>
					<DIV id=div_doc dc=".$dc." style=\"height: 180px\">".$echo."</DIV>
				</DIV>";
			echo MDOCcomment($m);
	}
}
markTime("mdoc");
} else {
	drawAdminSystemShortcuts();
}

?>
</div>
<?php

} //end if admin user

?>

<SCRIPT type=text/javascript>
	$(document).ready(function() {
	
	var is_admin = $("input:hidden[name=tkid]").val()=="0000" ? true : false;

	var acc_dash_size_max = 245;
	var acc_dash_size_min = 58;
	var show_graph = $("#acc_dash input:hidden[name=show_graph]").val();
	var stats_rows = $("#acc_dash #stats tr").size() - 1;
	var stats_table = stats_rows * 27 + 42;	//25 px per row + 42 px for header
	var stats_h = 37;
	var stats_height = acc_dash_size_min + stats_table + stats_h; 
	if(show_graph=="Y") {
		if(stats_height > acc_dash_size_max) {
			acc_dash_size_max = stats_height;
		}
	} else {
		acc_dash_size_max = stats_height;
	}
	var acc_dash_size = acc_dash_size_max;
	

	$( ".acc_display" ).accordion({
	collapsible: true,
	autoHeight: false,
	activate: function() {
		$(window).trigger("resize"); 
		
		$me = $(this);
		if($me.prop("id")=="acc_dash") {
			if($me.accordion( "option", "active" )===false) { acc_dash_size = acc_dash_size_min; } else { acc_dash_size = acc_dash_size_max; }
			$(window).trigger("resize");
		}
		//alert("resizing "+$(this).attr("id"));
	}

});
$(".acc_display2").accordion({ 
	fillSpace: true,
	collapsible: true
});

<?php 
if((!isset($_SESSION['USER_PROFILE'][2]['field1']) || $_SESSION['USER_PROFILE'][2]['field1']!="ALL")) { 
	echo "$('#div_short, #div_short_system, #div_short_user').css('height','100px');";
}
?>

var ds = $("#div_dshp").attr("ds");
if(ds > 7) { $("#div_dshp").css("height","250px"); }

$("#div_dshp input:button").button();
$("#div_doc input:button").button();
$("#div_doc input:button").css("font-size","7pt");
	$("#div_doc li, #div_short li, #div_short_user li, #div_short_system li, #div_dshp span.doclink").hover(
		function(){ $(this).removeClass("no_hover").addClass("a_hover"); },
		function(){ $(this).removeClass("a_hover").addClass("no_hover"); }
	);
$("#action_tables input:button").button();
$("#action_tables input:button").css("font-size","7pt");
	$("#stats a").hover(
		function(){ $(this).addClass("a_hover").css("color","#FE9900"); },
		function(){ $(this).removeClass("a_hover").css("color","#000099"); }
	);
$("#action_tables img.more_text").click(function() {
		var v = $(this).attr("alt");
		if($(this).attr("src")=="/pics/minus.gif") {
			var s = "/pics/plus.gif";
			v = v.substr(0,75)+"...";
		} else {
			var s = "/pics/minus.gif";
		}
		var i = $(this).attr("ref");
		$("#lbl_"+i).html(v);
		$(this).attr("src",s);
	
});
/*
$.fn.togglepanels = function(){
  return this.each(function(){
    $(this).addClass("ui-accordion ui-accordion-icons ui-widget ui-helper-reset")
  .find("h3")
    .addClass("ui-accordion-header ui-helper-reset ui-state-active ui-corner-top ui-corner-bottom")
    .hover(function() { $(this).toggleClass("ui-state-hover"); })
    .prepend('<span class="ui-icon ui-icon-triangle-1-e"></span>')
    .click(function() {
      $(this)
        .toggleClass("ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom")
        .find("> .ui-icon").toggleClass("ui-icon-triangle-1-e ui-icon-triangle-1-s").end()
        .next().slideToggle();
      return false;
    })
    .next()
      .addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom")
      .show();
  });
};
*/
//$(".acc_display").togglepanels();
//var my_browser = 'IE9';
//if(my_browser != 'IE8') {
	$(window).resize(function() { 
		//alert($("#spn_name").height());
		//if($("input:hidden[name=tkid]").val()=="0000") {
		//	var acc_user_margin = $("#spn_name").height() + 10;
		//	$("#acc_user").css("margin-top",acc_user_margin);
		//}
	//var h = screen.height;
	//h = h - 380;	//header + graph
	//h = h - 70;		//footer + status bar
	//h = h - 155;	//toolbars

		<!--    
		var viewportwidth;  
		var viewportheight;     // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight     
		if (typeof window.innerWidth != 'undefined')  {       
			viewportwidth = window.innerWidth,       
			viewportheight = window.innerHeight  
		}    // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)    
		else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)  {
			viewportwidth = document.documentElement.clientWidth,
			viewportheight = document.documentElement.clientHeight  
		}     // older versions of IE     
		else {        
			viewportwidth = document.getElementsByTagName('body')[0].clientWidth,        
			viewportheight = document.getElementsByTagName('body')[0].clientHeight  
		} 
		//document.write('<p>Your viewport width is '+viewportwidth+'x'+viewportheight+'</p>'); 
		h = viewportheight-5;
		//h -=380;
		//h-=40;
		h = h - acc_dash_size;//275;
		//--> 
//alert(h);
		if(!is_admin) { 
			$("#div_right, #div_left").css("height",(viewportheight-5)+"px"); 
		}
		//alert($("#div_left").css("height"));
		$("#action_tables").css("height",(h>100?h:100)+"px");
		var dc = $("#div_doc").attr("dc");
		//alert($("#doc_div").css("height"));
		//alert(dc+" :: "+(viewportwidth*0.35/dc));
		if(dc==0) {
			$("#div_doc").css("height","80px");
		} else if((viewportwidth*0.35/dc)>100) {
			$("#div_doc").css("height","185px");
		} else {
			$("#div_doc").css("height","295px");
		}
		//alert($("#doc_div").css("height"));
	});
	$(window).trigger("resize");
//}
	
	var click_array = new Array("","class","style","shape","aria-disabled");
	$("span.doclink, input:button, li.doclink, #btn_action_new").click(function(e) {
		e.preventDefault();
		if(!($(this).hasClass("accord")) && !($(this).hasClass("no_click"))) {
			var attrs = $(this)[0].attributes;
			var url = "";
			for(var i=0;i<attrs.length;i++) {
				nn = attrs[i].nodeName;
				//alert(attrs[i].nodeName + ' => ' + $.inArray(attrs[i].nodeName,click_array));
				if(($.inArray(attrs[i].nodeName,click_array))<0 && nn.substr(0,6)!="jQuery") {
					url = url+"&"+attrs[i].nodeName + "=" + attrs[i].nodeValue;
				}
			}
			url = 'main/link.php?a=click'+url;
			//console.log(url);
			document.location.href = url;
		}
	});
	$("#stats a").click(function() {
		var modref = $(this).attr("modref");
		modref = modref.toUpperCase();
		var mod_obj = $(this).attr("mod_obj");
		chngStatsMod(modref,mod_obj);
	});
	$(".doc_comment").click(function() {	
		var extra = 20;
		$(".doc_dialog").dialog("open");
		//alert($(".doc_dialog #commtext").width());
		var s = $(".doc_dialog select[name=cateid]").width();
		var w = $(window).width();
		var d = $(".doc_dialog").width();
		if(s>d) {
			if(s+extra<w) {
				//$(".doc_dialog").width(s+extra);
				$( ".doc_dialog" ).dialog( "option", "width", s+extra );
				$(".doc_dialog #commtext").width(s-extra);
			} else {
				$( ".doc_dialog" ).dialog( "option", "width", w-extra );
				$(".doc_dialog #commtext").width(w-(extra*3));
			}
		}
		$( ".doc_dialog" ).dialog( "option", "position", "center" );
		//alert($(".doc_dialog").width());
	});
	$("select").change(function() {
		if(!($(this).hasClass("no_change"))) {
			var i = $(this).attr("id");
			var v = $(this).val();
			v = v.toUpperCase();
			chngMod(i,v);
		}
	});
	function chngMod(i,v) {
		//alert("change");
			var u = new Array();
			u['amenu'] = '<?php echo $menu_mod['amenu']; ?>';
			u['docmenu'] = '<?php echo $menu_mod['docmenu']; ?>';
			if(v.length>0) { u[i] = v; }
			url = 'main_login.php?amenu='+u['amenu']+'&docmenu='+u['docmenu'];
			document.location.href = url;
	}
	function chngStatsMod(modref,mod_obj) {
		AssistHelper.processing();
		var docmenu = '<?php echo $menu_mod['docmenu']; ?>';
		url = 'main_login.php?amenu=' + modref + '&docmenu=' + docmenu + '&mod_obj=' + mod_obj;
		document.location.href = url;
	}

	$("#btn_action_new").button({
		icons: {
			primary: "ui-icon-circle-plus"
		}
	}).css({"font-size":"7.5pt","border-color":"#009900","color":"#009900"}).addClass("float").find('.ui-icon').css({"background-image": "url(/library/images/ui-icons_009900_256x240.png)"});
});

function changeMod() {
	var newmod = document.getElementById('amenu').value;
	if(newmod.length>0) {
		document.location.href = "main_login.php?mod="+newmod;
	}
}
function viewTask(mod,ml,p) {
	parent.header.location.href = "title_login.php?m="+mod;
	parent.main.location.href = "/"+ml+"/"+p;
}
</script>
<?php //arrPrint($_SESSION['USER_PROFILE']);
markTime("end js"); 
?>
	<?php
if($temp_security['display_dialog'] === true) { ?>
	<div id=dlg_security_notice title="Important Notice">
		<?php
		if($temp_security['dialog_type'] == "popia") {
			?>
			<h1 class=center>Important information - POPI Act</h1>
			<p>Action iT appreciates your business and is dedicated to continuing providing you with our performance management solution.</p>
			<p>As part of our ongoing commitment to protecting your personal information and ensuring that, at all times, you have access to a safe and secure system, we would like to assure you that your data privacy is our top priority.</p>
			<p>For all matters relating to the Protection of Personal Information Act, 4 of 2013 please contact our information officers:<ul>
				<li>Robert Klein - robert@actionassist.co.za</li>
				<li>Janet Currie - janet@actionassist.co.za</li>
			</ul></p>
			<p>To find out more on how we protect your interests please review our Privacy Policy and Terms and Conditions on our website at <a href="http://www.actionassist.co.za" target="_blank" >www.actionassist.co.za</a>.</p>
			<p>Should you have any questions or concerns regarding our compliance to the POPIA, please do not hesitate to contact our offices. </p>
			<p class="i">Kind regards, <br />
			Robert Klein<br />
			Action iT CEO</p>
			<p class=center>
				<button id=btn_popia>I acknowledge this message</button>
			</p>
	<?php
		} elseif($temp_security['dialog_type'] == "closure_warning") {
			?>
			<h1 class=center>Alert</h1>
			<p class=center>Please be advised that this account will be closed shortly
				<br/>due to non-renewal of contract.</p>
			<p class=center>
				<button id=btn_closure_warning>OK</button>
			</p>
			<?php
		} elseif($temp_security['dialog_type'] == "fix") {
			?>
			<h1>Notice</h1>
			<p class=b>Please read this carefully.</p>
			<p>We've updated the 'Change Password' page (accessed from the Main Menu).</p>
			<p>You can now update your answers to the Password Security Questions.</p>
			<p>It is important that these answers are correct as you will be required to provide a correct answer to one
				of the questions in order to reset your password via the Forgotten Password function now available from
				the login page.</p>
			<?php
			if($temp_security['dialog_type'] == "fix") {
				ASSIST_HELPER::displayResult(array("error", "Analysis of your security answers shows that they were possibly pre-set by your Business Partner when your login was created.  It is critical that you review your password security answers and update if necessary."));
			}
			?>
			<p><input type=button value=Close id=btn_security_notice_close class=no_click/><span class=float><input
							type=button value='Review Security Answers' id=btn_security_notice_go
							class=no_click/></span></p>
			<p class=iinform>Clicking one of the buttons above will prevent this notice from appearing again.</p>
		<?php } ?>
	</div>
	<script type="text/javascript">
		$(function() {
			<?php
			if($temp_security['dialog_type'] == "popia") {
			?>
			$("#dlg_security_notice")
					.dialog({
						modal: true,
						dialogClass: "temp_security",
						width: "500px"
					}).addClass("ui-corner-all").css({
				"border": "2px solid #009900",
				"background-color": "#fefefe"
			}).children("h1").css("color", "#009900");
			AssistHelper.hideDialogTitlebar("id", "dlg_security_notice");
			AssistHelper.hideDialogCSS("class", "temp_security");
			$("#dlg_security_notice #btn_popia").button({
				icons: {primary:"ui-icon-check"}
			}).removeClass("ui-state-default").addClass("ui-button-state-green").click(function() {
				AssistHelper.processing();
				AssistHelper.doAjax("TK/ajax_controller.php", "act=LOG_THAT_USER_HAS_SEEN_POPIA");
				$("#dlg_security_notice").dialog("close");
				//document.location.href = url;
				AssistHelper.closeProcessing();
			});
			$("#dlg_security_notice a:first").blur();

			<?php
			} elseif($temp_security['dialog_type'] == "closure_warning") {
	?>
	//$("<div />",{id:"security_notice",title:"Important Notice",html:"<h1>Notice</h1><p>We've updated the 'Change Password' page (accessed from the Main Menu).</p><p>You can now update your answers to the Password Security Questions.</p><p>It is important that these answers are correct as you will be required to provide a correct answer to one of the questions in order to reset your password via the Forgotten Password function now available from the login page.</p>"})
	$("#dlg_security_notice")
		.dialog({
			modal: true,
			dialogClass:"temp_security",
			width: "500px"
					}).addClass("ui-corner-all").css({
				"border": "2px solid #cc0001",
				"background-color": "#fff9f9"
			}).children("h1").css("color", "#cc0001");
		AssistHelper.hideDialogTitlebar("id","dlg_security_notice");
		AssistHelper.hideDialogCSS("class","temp_security");
		//$(".temp_security").css({border:"0px solid #ffffff","background-color":"#ffffff","padding":"0px"});
			$("#dlg_security_notice #btn_closure_warning").button().css({
				"color": "#000000",
				"border": "1px solid #000000"
			}).eq(0).blur();
		$("#dlg_security_notice #btn_closure_warning").click(function() {
			AssistHelper.processing();
			AssistHelper.doAjax("TK/ajax_controller.php","act=LOG_THAT_USER_HAS_SEEN_CLOSURE_WARNING");
			$("#dlg_security_notice").dialog("close");
			//document.location.href = url;
			AssistHelper.closeProcessing();
		});
	<?php
			} elseif($temp_security['dialog_type'] == "fix") {
	?>
	//$("<div />",{id:"security_notice",title:"Important Notice",html:"<h1>Notice</h1><p>We've updated the 'Change Password' page (accessed from the Main Menu).</p><p>You can now update your answers to the Password Security Questions.</p><p>It is important that these answers are correct as you will be required to provide a correct answer to one of the questions in order to reset your password via the Forgotten Password function now available from the login page.</p>"})
	$("#dlg_security_notice")
		.dialog({
			modal: true,
			dialogClass:"temp_security",
			width: "500px"
					}).addClass("ui-corner-all").css({
				"border": "2px solid #fe9900",
				"background-color": "#fefefe"
			}).children("h1").css("color", "#fe9900");
		AssistHelper.hideDialogTitlebar("id","dlg_security_notice");
		AssistHelper.hideDialogCSS("class","temp_security");
		//$(".temp_security").css({border:"0px solid #ffffff","background-color":"#ffffff","padding":"0px"});
			$("#dlg_security_notice input:button").button().css({
				"color": "#000000",
				"border": "1px solid #555555"
			}).eq(0).blur();
		$("#dlg_security_notice input:button").click(function() {
			var i = $(this).prop("id");
			switch(i) {
				case "btn_security_notice_close":
					var url = "main_login.php";
					var action = "Close";
					break;
				case "btn_security_notice_go":
					var url = "TK/password.php";
					var action = "Review";
					break;
			}
			AssistHelper.processing();
			AssistHelper.doAjax("TK/ajax_controller.php","act=LOG_THAT_USER_HAS_SEEN_SECURITY_NOTICE&action="+action);
			//$("#dlg_security_notice").dialog("close");
			document.location.href = url;
		});
	<?php
		}

	?>
});
</script>
<?php } ?>
<?php markTime("end of page"); ?>
</body>

</html>
