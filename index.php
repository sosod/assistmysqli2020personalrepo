<?php
//check if redirect to HTTPS is required
$my_site = $_SERVER['HTTP_HOST'];
//check for localhost
if(strpos($my_site,"localhost")===false) {
	if(!isset($_SERVER['HTTPS'])) {
		$site_code = strlen($_SERVER['QUERY_STRING'])>0 ? "?".$_SERVER['QUERY_STRING'] : "";
		header("Location: https://".$my_site."/index.php".$site_code);
	}
}

// IF PAGE IS ON LIVE CLIENT SERVER THEN INCLUDE DATADOG
$client_sites = array("assist.action4u.co.za", "ignite.assist.action4u.co.za", "pwc.assist.action4u.co.za");
if (in_array($my_site, $client_sites) === true) {

	# Require the datadogstatsd.php library file
	require 'library/datadogstatsd.php';

	$datadog_apis = array(
		'assist.action4u.co.za'        => "1e73d8cc8246676e371124aec41801e3",
		'ignite.assist.action4u.co.za' => "1e73d8cc8246676e371124aec41801e3",
		'pwc.assist.action4u.co.za'    => "b9508331c2d67526d6b83fa674f934c2",
	);
	//set name of stat to send to DataDog
	$stat_name = "assist.index.visits";
	//adjust name depending on client site
	$x = explode(".", $my_site);
	if ($x[0] != "assist") {
		$stat_name = $x[0] . "-" . $stat_name;
	}

	# Increment a counter.
	$apiKey = $datadog_apis[$my_site];
	$appKey = "f4cc83d5e3eb5cf3ad958253f51d96838764a0a0";
	DataDogStatsD::configure($apiKey, $appKey);
	DataDogStatsD::increment($stat_name);

}


//phpinfo();

$default_site_code = "actionassist";


@session_destroy();
@session_start();
        header('Expires: 0');
        header('Cache-Control: private');
        header('Pragma: cache');
@ini_set(default_charset, "");
//error_reporting(-1);

require_once("library/class/autoload.php");
$alogin = new ASSIST_LOGIN(array(),false);
//$me = new ASSIST_HELPER();

//ASSIST_HELPER::arrPrint($_REQUEST);
if(isset($_REQUEST['token'])) {
	$token = unserialize(base64_decode($_REQUEST['token']));
	$token['auto'] = true;
//	ASSIST_HELPER::arrPrint($token);
} else {
	$token = array(
		'uid'=>"",
		'cc'=>"",
		'pid'=>"",
		'auto'=>false,
	);
}


/** REQUIRE inc_status:
maintenance_start_time
maintenance_end_time
maintenance_notice_start
current_time
maintenance_name
**/
require 'inc_status.php';

$mdb = new ASSIST_DB("master");


$mods = $mdb->mysql_fetch_all("SELECT * FROM www_modules WHERE active = 1 ORDER BY code");

  $sections = array();
  
  foreach($mods as $m) {
	$descrip = strip_tags($m['blurb']);
	$o = $descrip;
	$descrip = substr($descrip,0,120);
	if($descrip!=$o) { $descrip.="..."; }
	
	$sections[]= array(
		'link'=>"/modules/view_module.php?code=".$m['code'],
		'name'=>$m['function'],
		'desc'=>$descrip,
	);
  }
  $random = array();
  $random_module = rand( 0, count( $sections ) - 1 );
  $random[] = $random_module;
while(in_array($random_module, $random)) { $random_module = rand(0,count($sections)-1); }
  $random[] = $random_module;
while(in_array($random_module, $random)) { $random_module = rand(0,count($sections)-1); }
  $random[] = $random_module;


$site_code = strlen($_SERVER['QUERY_STRING'])>0 ? $_SERVER['QUERY_STRING'] : $default_site_code;
//echo $site_code;

$sql = "SELECT r.*, c.cmpname FROM assist_reseller r INNER JOIN assist_company c ON c.cmpcode = r.cmpcode WHERE site_code = '".$site_code."' AND active = 1";
$site = $mdb->mysql_fetch_one($sql);

if(!isset($site['site_name'])) {
	$site_code = $default_site_code;
	$sql = "SELECT r.*, c.cmpname FROM assist_reseller r INNER JOIN assist_company c ON c.cmpcode = r.cmpcode WHERE r.id = 1 ";
	$site = $mdb->mysql_fetch_one($sql);
}

$site_name = $site['site_name'];
$site_logo = "pics/bp_logos/".$site['assist_logo'];
if(!file_exists($site_logo)) {
	$site_logo = "pics/bp_logos/actionassist.png";
}
$site_logo = "/".$site_logo;
//if($assiststatus!="U") {
	$sql = "SELECT cmpcode, cmpreseller, cmp_logo_reseller, cmpname FROM assist_company WHERE cmpstatus = 'Y' ORDER BY cmpcode";
	//$rs = $mdb->db_query($sql); //AgetRS($sql);
	$rows = $mdb->mysql_fetch_all($sql);
	$cmp = array();
	//while($row = mysql_fetch_assoc($rs)) {
	foreach($rows as $row) {
		$cmp[$row['cmpcode']] = $row;
	}
	unset($rs);
	
	$sql = "SELECT * FROM assist_reseller WHERE active = 1 ORDER BY cmpcode";
//	$rs = $mdb->db_query($sql);
	//$rs = AgetRS($sql);
	$rows = $mdb->mysql_fetch_all($sql);
	$res = array();
//	while($row = mysql_fetch_assoc($rs)) {
	foreach($rows as $row) {
		$res[$row['cmpcode']] = array();
		$a = explode("|",$row['help_contact']);
		$d = array();
		foreach($a as $b) {
			$c = explode("_",$b);
			$d[] = "<br />&nbsp;&nbsp;&nbsp;&nbsp;<span class=i>".$c[0]."</span>: ".($c[0]=="Email" ? "<a href=mailto:".$c[1]." class=fc>" : "").$c[1].($c[0]=="Email" ? "</a>" : "");
//			$d[] = $c[1];
		}
		$res[$row['cmpcode']] = implode("",$d);
//		$res[$row['cmpcode']] = implode(" or ",$d);
	}
	unset($rs);
//}

?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Action4u.co.za</title>
    <link rel="shortcut icon" href="/favicon.ico?v=2" type="image/x-icon" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="/pics/frontpage/include/screen.css?ac=3" media="screen,projector" />
    <link rel="stylesheet" type="text/css" href="/pics/frontpage/include/print.css?ac=1" media="print" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="/pics/frontpage/include/screen-ie.css?ac=1" /><![endif]-->
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="/pics/frontpage/include/screen-ie6.css?ac=1" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="/pics/frontpage/include/screen-ie7.css?ac=1" /><![endif]-->
    <!--[if IE 8]><link rel="stylesheet" type="text/css" href="/pics/frontpage/include/screen-ie8.css?ac=1" /><![endif]-->    
<script type ="text/javascript" src="library/jquery/js/jquery.min.js"></script>
<script type ="text/javascript" src="library/jquery/js/jquery-ui.min.js"></script>
<script type ="text/javascript" src="library/jquery/js/jquery-ui-timepicker-addon.js"></script>
			<link href="/library/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type ="text/javascript" src="library/js/assisthelper.js"></script>
<script type ="text/javascript" src="library/js/assiststring.js"></script>
<script type ="text/javascript" src="library/js/assistform.js"></script>

	
    <script type="text/javascript" src="/pics/frontpage/include/ignite.js?ac=1"></script>
    <!--[if IE]><script type="text/javascript" src="/pics/frontpage/include/ignite-ie.js?ac=1"></script><![endif]-->
	<script type="text/javascript" src="/pics/frontpage/include/js/jquery.lightbox-0.5.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/pics/frontpage/include/css/jquery.lightbox-0.5.css" />	
  </head>


<style type=text/css>
.notice { font-weight: bold; text-decoration: none; }
.center { text-align: center; }
.required { color: white; background-color: red; }
.b { font-weight: bold; }
.u { text-decoration: underline; }

#tbl_social td {
	font-size: 8pt;
	text-align: center;
	vertical-align: center;
}
.section_module {
	border: 1px solid #bcbcbc;
	margin: 10px auto;
	padding: 5px;
	width: 80%;
	font-size: 90%;
	background-color: #ffffff;
}
.section_module h4 {
	font-weight: bold;
	color: #555555;
	font-size: 110%;
	margin-bottom: 3px;
}
#active_user_count {
	padding-right: 5px;
	position: absolute;
	bottom: 10px;
	right: 5px;
	border: 0px dashed #990000;
}
#active_user_count p {
	font-size: 85%;
	font-style: italic;
	color: #555555;
}
#header-top-row { border: 0px dashed #fe9900; }
#header { border: 0px dashed #009900; }
table.xnoborder, table.xnoborder td, td.xnoborder {
	border: 0px dashed #009900;
}
</style> 
  <body>
    <div id="site-frame">
      <div id="header" >
 	<a id="logo" href="/"><img src="<?php echo $site_logo; ?>" alt="<?php echo $site_name; ?>" /></a>
	  <div style="" id=header-top-row>
<!-- <span style='font-weight: bold;'>Cash Management:</span> Ignite Reporting Systems (Pty) Ltd earns interest at <span style='color: #fe9900;'>4.5%</span> per annum | Current Repo Rate is <span style='color: #fe9900;'>5.0%</span> per annum | Current Prime Rate is <span style='color: #fe9900;'>8.5%</span> per annum | Cash Investment options: Call Deposit, Prime Saver Call Deposit, Term Deposit or Notice Deposit -->
<!--
<marquee scrollamount="2">

<span style='font-weight: bold;'>Cash Investment Options</span> =>
<span style='font-style: italic;'>Up to R 50 000:</span> Call Deposit: 4.15% ; Prime Saver: 4.70% ; 30 Day Fixed Deposit: 4.40% ; 60 Day Fixed Deposit: 4.49% ; 32 Day Notice Deposit (0% Access): 2.81% ; 60 Day Notice Deposit (0% Access): 4.80% 
<span style='font-style: italic;'>Over R 50 000 to R 5 mil.:</span> Call Deposit: 4.15% ; Prime Saver: 4.70% ; 30 Day Fixed Deposit: 4.40% ; 60 Day Fixed Deposit: 4.49% ; 32 Day Notice Deposit (0% Access): 2.81% ; 60 Day Notice Deposit (0% Access): 4.80% 
<span style='font-style: italic;'>Over R 5 mil to R 10 mil.:</span> Call Deposit: 4.25% ; Prime Saver: 4.40% ; 30 Day Fixed Deposit: 4.65% ; 60 Day Fixed Deposit: 4.74% ; 32 Day Notice Deposit (0% Access): 4.70% ; 60 Day Notice Deposit (0% Access): 4.95% 
<span style='font-style: italic;'>Over R 10 mil.:</span> Call Deposit: 4.35% ; Prime Saver: 4.50% ; 30 Day Fixed Deposit: 4.75% ; 60 Day Fixed Deposit: 4.84% ; 32 Day Notice Deposit (0% Access): 4.80% ; 60 Day Notice Deposit (0% Access): 5.05% &nbsp;|&nbsp;
<span style='font-weight: bold;'><?php echo $site_name; ?></span> => Ask your business partner about the Individual Performance Management Module.&nbsp;|&nbsp;
</marquee>
-->
</div>
<div id=active_user_count><p><?php include "inc_count_active_users.php"; ?></p></div>
</div>
      <div id="content-frame">
        
 
      
        <img id="column-divider-top" src="/pics/frontpage/images/bg-hr-column.png" alt="divider" />
<div id="column-wrapper">
  <div id="content-column">
    <div class="spacer">
      <h1>Welcome to <?php echo $site_name; ?></h1>
			<p><?php echo $site_name; ?> is an innovative online subscription offering which provides organisations with a secure encrypted web-based platform for the management of operational processes and data storage on the internet. The tool reduces the administration burden on stakeholders in the day to day operational running of the organisation and clearly assigns responsibility and accountability to all users.</p>
			<?php
			if($site_code!=$default_site_code) {
				echo "<p>".$site_name." is hosted on the Action Assist platform owned and managed by <a href=http://www.actionassist.co.za>Action iT (Pty) Ltd</a>.</p>";
			}
			?>
			
			
	  <div id=login style='margin-bottom: 20px;'>
	  
	  <form name=frm_login method=post action=login.php>
<input type=hidden name=screen_h value="" id=h />
<input type=hidden name=screen_w value="" id=w />
<input type=hidden name=second_validation value="0" />
				<table style="padding: 5px; border: 2px solid #034C96; margin: 0 auto;"><tr><td style="padding: 5px; ">

<?php

/** @var BOOL $display_special_notice - src = inc_status.php */
/** @var DATETIME $special_notice_start_time */
/** @var DATETIME $special_notice_end_time */
if($display_special_notice && $current_time > $special_notice_start_time && $current_time < $special_notice_end_time) {
?>
					<div  id="div_special_notice" width=350px style='border: 2px solid #fe9900; background-color: #ffffee; padding: 10px 10px 10px 10px; width: 350px; margin: 0 auto 10 auto; margin-bottom:20px'>
						<h2 class=center style='color: #fe9900;'><?php /** @var STRING $special_notice_title */
							echo $special_notice_title; ?></h2>
						<?php /** @var HTMLSTRING $special_notice_text */
						echo $special_notice_text; ?>
					</div>
<?php
}


/** @var BOOL $special_maintenance - originates in /inc_status.php */
/** @var STRING $maintenance_name - originates in /inc_status.php */
if($current_time > $maintenance_notice_start && $current_time < $maintenance_end_time) { 
?>
					<div width=350px style='border: 2px solid #fe9900; background-color: #ffffee; padding: 10px 10px 10px 10px; width: 350px; margin: 0 auto 10 auto; margin-bottom:20px'>
						<h2 class=center style='color: #fe9900;'>Maintenance Notice</h2>
						<p>Please be advised that <?php echo $site_name; ?> will be unavailable due to <?php echo $maintenance_name; ?> maintenance from <?php echo date("d F Y H:i",$maintenance_start_time); ?> to <?php echo date("d F Y H:i",$maintenance_end_time); ?>.</p>
						<p>We apologise for any inconvenience.</p>
						<p>Kind regards</p>
						<p>Action iT (Pty) Ltd</p>
					</div>
<?php 
}

/** @var STRING $assiststatus - originates in /inc_status.php */
if($assiststatus=="U") { // || ($current_time > $maintenance_start_time && $current_time < $maintenance_end_time)) { 
	echo "<p class=center style='padding-top: 10px;'>".$site_name." is currently undergoing maintenance.<br />Please try again later.</p><input type=hidden id=u />";
} else {
?>
<!--					<div width=90% style='border: 2px solid #fe9900; background-color: #ffffee; padding: 10px 10px 10px 10px; width: 90%; margin: 0 auto;'>
						<h2 class=center style='color: #fe9900;'>Annual Maintenance</h2>
						<p>Please be advised that <?php echo $site_name; ?> will be undergoing annual maintenance and hardware upgrades from 28 December 2016 at 20h00 until 31 December 2016 at 23h59 and will be unavailable during this time.</p>
						<p>We apologise for any inconvenience.</p>
						<p>Kind regards</p>
						<p>Action iT (Pty) Ltd</p>
					</div> -->
				<!--	<div width=90% style='border: 2px solid #fe9900; background-color: #ffffee; padding: 10px 10px 10px 10px; width: 90%; margin: 0 auto;'>
						<h2 class=center style='color: #fe9900;'>Maintenance Notice</h2>
						<p>Please be advised that our service provider will be performing maintenance on their network on Friday, 24 March 2017, from 00h01 to 07h00.<br /><?php echo $site_name; ?> will be unavailable during this time.</p>
						<p>We apologise for any inconvenience.</p>
						<p>Kind regards</p>
						<p>Action iT (Pty) Ltd</p>
					</div> 
-->

<table style="margin:0 auto"><tr><td><h2 style='text-align: center;'>Login</h2>
						<table style="padding: 5px" class=xnoborder>
							<tr>
								<td style='font-weight: bold;'>User:</td>
								<td  class=xnoborder style="padding: 5px"><input type=text size=22 name=uid id=u class='req' style='padding: 2px;' value='<?php echo $token['uid']; ?>' /></td>
							</tr>
							<tr>
								<th style='font-weight: bold;'>Company Code:</th>
								<td  class=xnoborder style="padding: 5px"><input type=text size=22 name=cc class='req' style='padding: 2px;' value='<?php echo $token['cc']; ?>' /></td>
							</tr>
							<tr>
								<th style='font-weight: bold;'>Password:</th>
								<td  class=xnoborder style="padding: 5px"><input type=password size=22 name=pid class='req' style='padding: 2px;'  value='<?php echo $token['pid']; ?>' /></td>
							</tr>
							<tr>
								<td colspan=2 style='padding: 3px; text-align: center'><span style='font-size:8pt;font-style: italic;'>By logging in you accept our latest Terms and Conditions.</span></td>
							</tr>
							<tr>
								<td style='text-align: center; font-size: 8pt;'><a href=forgot.php>Forgot your password?</a></td>
								<td  class="xnoborder" style="padding: 5px; text-align: center"><input type=button name=btn value=Login style='font-weight: bold; color: #009900; padding: 2px 8px 2px 8px;' /></td>
							</tr>
<!--							<tr>
								<td style='font-weight: bold;'>User:</td>
								<td  class=noborder style="padding: 5px"><input type=text size=15 name=uid id=u class='req' value='<?php echo $token['uid']; ?>' /></td>
							</tr>
							<tr>
								<th style='font-weight: bold;'>Company Code:</th>
								<td  class=noborder style="padding: 5px"><input type=text size=15 name=cc class='req' value='<?php echo $token['cc']; ?>' /></td>
							</tr>
							<tr>
								<th style='font-weight: bold;'>Password:</th> 
								<td  class=noborder style="padding: 5px"><input type=password size=15 name=pid class='req' value='<?php echo $token['pid']; ?>' /></td>
							</tr>
							<tr>
								<td style='text-align: center; font-size: 8pt;'><a href=forgot.php>Forgot your password?</a></td>
								<td  class="noborder" style="padding: 5px; text-align: center"><input type=button name=btn value=Login style='font-weight: bold; color: #009900; padding: 2px 8px 2px 8px;' /></td>
							</tr>-->
						</table>
			<!-- </td><td width=20>&nbsp;</td><td> -->
						<p style='text-align: center; font-size: 8pt; margin-top: 5px'>For assistance please contact your <br /><a class="color u" style="cursor:pointer;" id=partner>Business Partner</a></p>
<!--
<table id=tbl_social style='width: 100%'>
	<tr>
		<td colspan=2 >For Assist news and updates:</td>
	</tr>
	<tr>
		<td><?php if(isset($_SERVER['HTTPS'])) { ?>
			<a href="https://twitter.com/actionIT_sa" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		<?php } else { echo "TWITTER"; } ?></td><td><?php if(isset($_SERVER['HTTPS'])) { ?>
			<div id="fb-root"></div>

			<div class="fb-like" data-href="https://www.facebook.com/actionassistSA" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
		<?php } else { echo "FACEBOOK"; } ?></td>
	</tr>
</table> -->
		</td></tr></table>
						
<?php }
?>
				</td></tr></table></form>
	  
	  </div>
	  
	  
	  
	  
		<h2>Recent Updates</h2>
			<p>To view the recent updates, please <a id=updates style='cursor: pointer'>click here</a>.  Follow us on Twitter to keep up to date.</p>
<!--		<h2>Implementation Methods</h2>
		<p>To view alternative implementation methods, please <a href=http://www.actionassist.co.za/implementation.php target=_blank>click here</a>.</p>
-->
	</div>
  </div>
<div id="side-column">
  <div class="spacer">
  

  
<!--    <div class="section">
      <h3 class=section>Cash Management</h3>
      <p>Action iT (Pty) Ltd has partnered with Investec Specialist Bank to enable us to provide our clients with a competitive and efficient money management offering.  The Action iT Money Market affords investors <span id=cm_expand>...[+]</span><span id=cm_extra>the opportunity to earn the levels of interest that previously only were available to financial institutions and high net worth investors through a secure online banking system that was specifically designed for third party fund administration.</span></p>
      <span style='float: right;'><a href="http://www.actionassist.co.za/investiments/offering.php"><img src="/pics/frontpage/images/btn-more.png" alt="Read more" width=75%  /></a></span>
	  <p style='font-size: 8pt;'>Please feel free to contact us on <a href='invest@igniteassist.co.za'>invest@igniteassist.co.za</a> for your personalised quote on our cash management offering.</p>
      <img src="/pics/frontpage/images/bg-hr-side-column.png" alt="Devider" class="divider" />
    </div>
-->


<div class=section>
     <span style='float: right; margin-top: -5px'> <a href="https://www.actionassist4u.com/modules"><img src="/pics/frontpage/images/btn-more.png" alt="Read more" width=100px /></a></span>
      <h3><?php echo $site_name; ?> Modules</h3>
<p><?php echo $site_name; ?> can help your organisation in:</p>
<?php foreach($random as $random_module) { ?>
<div class=section_module>
<?php
echo "<h4>".$sections[$random_module]['name']."</h4>
<p>".$sections[$random_module]['desc']."</p>";
?>
</div>
<?php } ?>
<!--<p>Contact Action iT or your business partner to find out more.</p> -->
      <img src="/pics/frontpage/images/bg-hr-side-column.png" alt="Divider" class="divider" />
</div>

<!--
    <div class="section">
     <span style='float: right'> <a href="http://www.actionassist.co.za/modules/overview.php"><img src="/pics/frontpage/images/btn-more.png" alt="Read more" width=75% /></a></span>
      <h3><?php echo $site_name; ?> Modules</h3>
      <p>For more information, please contact your business partner.</p>
      <img src="/pics/frontpage/images/bg-hr-side-column.png" alt="Devider" class="divider" />
    </div>

<div class=section>
     <span style='float: right'> <a href="http://www.actionassist.co.za/modules/overview.php"><img src="/pics/frontpage/images/btn-more.png" alt="Read more" width=75% /></a></span>
<?php
echo "<h3>".$sections[$random_module]['name']."</h3>
<p>".$sections[$random_module]['desc']."</p>";
?>
      <img src="/pics/frontpage/images/bg-hr-side-column.png" alt="Devider" class="divider" />
</div>
<div class=section>
     <span style='float: right'> <a href="http://www.actionassist.co.za/modules/overview.php"><img src="/pics/frontpage/images/btn-more.png" alt="Read more" width=75% /></a></span>
<?php
echo "<h3>".$sections[$random_module2]['name']."</h3>
<p>".$sections[$random_module2]['desc']."</p>";
?>
      <img src="/pics/frontpage/images/bg-hr-side-column.png" alt="Devider" class="divider" />
</div>
 
	-->
<!--    <div class="section">
     <span style='float: right'> <a href="http://www.actionassist.co.za/faq.php"><img src="/pics/frontpage/images/btn-more.png" alt="Read more" width=100px /></a></span>
      <h3>Frequently Asked Questions</h3>
      <img src="/pics/frontpage/images/bg-hr-side-column.png" alt="Devider" class="divider" />
    </div>
-->
	
<!--
	<div style='border: 0px solid #ffffff;'>
		<table width="100%" style='border: 0px solid #ffffff;'>
			<tr><td colspan=2 style='padding-bottom: 10px;border: 0px solid #ffffff;font-style: italic; font-size: 90%; text-align: center'>In Partnership:</td></tr>
			<tr><td colspan=2 style='border:0px solid #ffffff; text-align: right'>	
<?php if(isset($_SERVER['HTTPS'])) { ?><div id="thawteseal" title="Click to Verify - This site chose Thawte SSL for secure e-commerce and confidential communications.">
<div style='float:right'><script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=<?php echo $_SERVER["SERVER_NAME"]; ?>&amp;size=S&amp;lang=en"></script></div>
</div><?php } ?>	
	</td></tr><tr>

				<td width="50%" style='border: 0px solid #ffffff;'><img src=/pics/is_logo.png style='width: 150px;' /></td>
				<td width="50%" style='border: 0px solid #ffffff;vertical-align: middle'><img src=/pics/attix5-logo.png style='width: 150px;float:right' /></td>
			</tr>
		</table>	
	</div>	
	
	
-->

<div id=side-partners-support>
	<?php include("index_partners.php"); ?>




</div>

  </div>
</div>
<!--[if IE 6]><br class="clear"/><![endif]-->
</div>
<img id="column-divider-bottom" src="/pics/frontpage/images/bg-hr-column.png" alt="divider" />

      </div><!-- #content-frame -->
<!--      <div id="footer">
        <div id="footer-top-row">
          <a href="http://www.actionassist.co.za/" style='float:right'>www.actionassist.co.za</a>
          <a id="action-bookmark" href="#" onclick="return bookmark();" class="action-bookmark">Bookmark</a>
        </div>
        <div id="footer-middle-row">
          <a href="http://www.actionassist.co.za/terms.php">Terms &amp; Conditions</a> |          
          <a href="http://www.actionassist.co.za/privacy.php">Privacy Policy</a> |
          <a href="http://www.actionassist.co.za/legal.php">Legal</a> |
          <a href="http://www.actionassist.co.za/IRS - Promotion of Access to Information 20111206.pdf">PAIA Manual</a>
        </div>        
		<div>Action iT (Pty) Ltd is an authorised Financial Services Provider (FSP no. 40003).</div>
        <div id="footer-bottom-row"><p>&copy; <?php echo date("Y"); ?> Action iT (Pty) Ltd. All Rights Reserved.</p></div>        
      </div><!-- #footer -->  

<?php include("index_footer.php"); ?>
    </div><!-- #site-frame -->
	<?php
	if(strpos($_SERVER['HTTP_HOST'],"action4u.co.za")===false) {
		echo "<hr /><h1 style='color:#009900'>Helllloooo!!!  Happy days!  Second validation will not be required as you are on a dev site.</h1><hr />";
	}
	?>
       
<script type=text/javascript>
$(function() {
	$("#cm_extra").hide();
	$("#cm_expand").click(function() {
		$("#cm_extra").show();
		$("#cm_expand").hide();
	});
	$("#h").val(screen.height);
	$("#w").val(screen.width);
	var cmp = new Array();
	<?php foreach($cmp as $c => $r) {
		echo "
			cmp['$c'] = new Array();			cmp['$c']['r'] = '".$r['cmpreseller']."';			cmp['$c']['n'] = '".$r['cmpname']."';";
	} echo chr(10); ?>
	var res = new Array();
	<?php foreach($res as $c => $r) {
		echo "
		res['$c'] = '$r';";
	} echo chr(10); ?>
	$("#partnerhelp, #dlg_updates, #dlg_methods").dialog({
		autoOpen: false, 
		width: 500, 
		height: 400,
		modal: true,
		buttons: { "Close": function() { $( this ).dialog( "close" ); } } 
	});
	$("#partner").click(function() {
		$("#step2").hide();
		$("#step1").show();
		$("#h_cc").val("");
		$("#res_name").attr("innerText","");
		$("#res_help").attr("innerText","");
		$("#partnerhelp").dialog("open");
		$("#h_cc").focus();
	});
	$("#partnerhelp #h_cc").keypress(function(e) { 
		if(e.keyCode == 13) { 
			$("#partnerhelp #next").trigger("click"); 
		} 
	}); 
	$("#partnerhelp #next").click(function() {
		var cc = $("#h_cc").val();
		if(cc.length!=7) {
			alert("Please enter a valid 7 character company code.");
		} else {
			cc = cc.toUpperCase();
			if(!(cc in cmp)) {
				alert("You have entered an invalid company code.  Please try again.");
			} else {
				var r = cmp[cc]['r'];
				$("#res_name").attr("innerText",cmp[r]['n']);
				//alert((r in res)+" :: "+res[r]);
				if(!(r in res)) {
					r = "AIT0001";
				}
				var h = res[r];
				$("#contact").attr("innerHTML",h);
				$("#step1").hide();
				$("#step2").show();
			}
		}
	});
	$("#back").click(function() {
		$("#partnerhelp").dialog("close");
		$("#partner").trigger("click");
	});
	$("#updates").css("cursor","hand").click(function() { $("#dlg_updates").dialog("open"); });
	$("#methods").css("cursor","hand").click(function() { $("#dlg_methods").dialog("open"); });
	$("#u").focus();
	
	$("form[name=frm_login] input:button").click(function(e) {
		e.preventDefault();
		//AssistHelper.processing();
		var dta = "";
		var d = "";
		var err = false;
		$("form[name=frm_login] input").each(function() {	
			if($(this).hasClass("req")) {
				if($(this).val().length==0) {
					err = true;
					$(this).addClass("required");
				} else {
					$(this).removeClass("required");
				}
			}
			if(!err) {
				var v = encodeURIComponent($(this).val());		//removed conversion below and added encodeURIComponent to catch all special characters [JC 2019-03-04 #AA-4 ]
				//v = v.replace("&","|agus|ait|suga|");           //replace ampersand with placeholder to catch and &'s used in password [JC 2019-02-11 YT#AA-4]
				//v = v.replace("+","|sulp|ait|plus|");           //replace plus with placeholder to catch and +'s used in password [JC 2019-03-04 YT#AA-4]
				d = $(this).attr("name")+"="+v;
				if(dta.length>0) { dta+="&"; }
				dta+=d;
			} //console.log(dta);
		});
		if(!err) {
/*****
* noisses required for login process - do not delete!
*/
			dta+="&site_code=<?php echo $site_code; ?>&niosses=<?php echo isset($_REQUEST['PHPSESSID']) ? $_REQUEST['PHPSESSID'] : "X"; ?>";
			//alert(dta);
			validateForm(dta);
		} else {
			alert("Please enter all the required information highlighted in red.");
		}
	});
	$("form[name=frm_login] input").keypress(function(e) {
		 var code = (e.keyCode ? e.keyCode : e.which); 
		 if(code == 13) { //Enter keycode 
		   $("form[name=frm_login] input:button").trigger("click");
		 } 
	});
<?php
if($token['auto']===true) {
	echo "
AssistHelper.processing();
//$('form[name=frm_login] input:button').trigger('click');
";
}
?>
});
function triggerLogin() {
	$(function() {
		$('form[name=frm_login] input:button').trigger('click');
	});
}

function validateForm(dta) {
/*		$.ajax({                                      
		  url: 'login_ajax.php', 		  type: 'POST',		  data: dta,		  dataType: 'json', 
		  success: nextSteps
		});*/
	var r = AssistHelper.doAjax("login_ajax.php",dta);
	nextSteps(r);
}
function nextSteps(data) {
//console.log(data);
//AssistHelper.closeProcessing();

if(data===null || typeof data === "undefined") {
		AssistHelper.closeProcessing();
		executeKillMe("");
} else {
	if(data[0]!="0" && data[5]*1!=1) {
		AssistHelper.closeProcessing();
		executeKillMe(data[1]);
	} else if((data[5]*1)==1) {	//REDIRECT TO CORRECT SERVER
		var redirect = data[6];
		var token = data[1];
		document.location.href = "https://"+redirect+"/index.php?token="+token;
	} else if((data[3]*1)==1) {
		$("#dlg_second_validation").dialog("open");
		$("#dlg_second_validation #u2").focus();
	} else {
		var url = data[2];
		document.location.href = url;
	}
}
}
</script>
<style type=text/css>
#partnerhelp p { font-style: Verdana; font-size: 9pt; line-height: 12pt; margin-top: 5px; margin-bottom: 5px;}
#partnerhelp .b { font-weight: bold; }
#partnerhelp p input:button { padding: 5px; font-weight: bold; line-height: 15pt; color: #009900; }
</style>
<div id=partnerhelp title="Help">
<span id=step1>
<p class=b>Please enter your company code: <input type=text name=help_cc id=h_cc size=8 /></p>
<p><input type=button value="Next -->"  id=next style="padding: 5px; font-weight: bold; line-height: 15pt; color: #009900;" /></p>
</span>
<span id=step2>
<p id=answer>For assistance please contact <span class=b id=res_name></span> via the following channels:</p>
<p id=contact></p>
<p><input type=button value="<-- Back" class=idelete id=back /></p>
</span>
</div>
<style type=text/css>
#dlg_updates table td { padding: 5pt; } 
#dlg_updates table th { background-color: #000066; color: #FFFFFF; text-align: center; padding: 5pt; }
#dlg_updates table { padding: 0px; margin-bottom: 0px; font-size: 9pt; line-height: 11pt; }

</style>
<div id=dlg_updates title="Recent Updates">
		<table cellpadding=5 cellmargin=0>
			<tr>
				<th>Date</th>
				<th>Module</th>
				<th>Update</th>
			</tr>
			<?php 
$rd = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),1,date("Y")-1));
			$sql = "SELECT * FROM assist_updates WHERE active = true AND udate > DATE('$rd') ORDER BY udate DESC"; 
			//$rs = $mdb->db_query($sql);
			//$rs = AgetRS($sql);
			$rows = $mdb->mysql_fetch_all($sql);
			//while($row = mysql_fetch_assoc($rs)) {
			foreach($rows as $row) {
			?>
			<tr>
				<td class=center><?php echo date("d M Y",strtotime($row['udate'])); ?></td>
				<td class=center><?php echo $row['umodule']; ?></td>
				<td><?php echo str_replace(chr(10),"<br />",$row['utext']); ?></td>
			</tr>
			<?php
			}
			?>
		</table>
</div>

<!--
<style type=text/css>
#dlg_methods p, #dlg_methods li { font-style: Verdana; font-size: 9pt; line-height: 12pt; margin-top: 5px; margin-bottom: 5px;}
#dlg_methods .b { font-weight: bold; }
#dlg_methods ul {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	margin-top: 5px;
	margin-bottom: 10px;
}
#dlg_methods ul li {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 10pt;
	margin: 0px 5px 5px 25px;
	padding: 0px;
	text-align: left;
	font-weight: normal;
}
</style>

<div id=dlg_methods title="Implementation Methods">
<?php //include "http://www.actionassist.co.za/assist_implementation.php"; ?>
</div>
-->

<?php
$alogin->drawKillMe();

?>

<div id=dlg_second_validation title="Security Validation">
	<h2>Security Validation</h2>
	<p style='font-size: 80%'>You are attempting to login as the Admin or Support user.<br />For security purposes please provide your personal login credentials to verify that you have the necessary permissions to login as the Admin or Support user.</p>
	<form name=frm_security>
		<input type=hidden name=second_validation value=1 />
<?php 
if($assiststatus=="U") { 
	echo "<p class=center>".$site_name." is currently undergoing maintenance.<br />Please try again later.</p><input type=hidden id=u />";
} else {
?>
						<table style="padding: 5px" class=noborder>
							<tr>
								<td style='font-weight: bold; font-size:80%'>User:</td>
								<td  class=noborder style="padding: 5px"><input type=text size=15 name=uid2 id=u2 class='req2' /></td>
							</tr>
							<tr>
								<th style='font-weight: bold; font-size:80%'>Company Code:</th>
								<td  class=noborder style="padding: 5px"><input type=text size=15 name=cc2 class='req2' /></td>
							</tr>
							<tr>
								<th style='font-weight: bold; font-size:80%'>Password:</th> 
								<td  class=noborder style="padding: 5px"><input type=password size=15 name=pid2 class='req2' /></td>
							</tr>
							<tr>
								<td style='text-align: center; font-size: 8pt;'></td>
								<td  class="noborder" style="padding: 5px; text-align: center"><button name=btn_second id=btn_second>Login</button></td>
							</tr>
						</table>
						<p class=float><button id=btn_cancel>Cancel</button></p>
		</td></tr></table>
						
<?php }
?></form>
</div>
<script type="text/javascript" >
$(function() {
	$("#dlg_second_validation").dialog({
		autoOpen: false,
		modal: true
	});
	AssistHelper.hideDialogTitlebar("id","dlg_second_validation");
	$("#btn_second").button({
	}).click(function(e) {
		e.preventDefault();
		var dta = "";
		var d = "";

		var err = false;
		$("form[name=frm_security] input").each(function() {  //console.log($(this).attr("name"));	
			if($(this).hasClass("req2")) {
				if($(this).val().length==0) {
					err = true;
					$(this).addClass("required");
				} else {
					$(this).removeClass("required");
				}
			}
			if(!err) {
                var v = encodeURIComponent($(this).val());
                //v = v.replace("&","|agus|ait|suga|");           //replace ampersand with placeholder to catch and &'s used in password [JC 2019-02-11 YT#AA-4]
                //v = v.replace("+","|sulp|ait|plus|");           //replace plus with placeholder to catch and +'s used in password [JC 2019-03-04 YT#AA-4]
				d = $(this).attr("name")+"="+v;
				if(dta.length>0) { dta+="&"; }
				dta+=d;
			}
		});
		if(!err) {
			dta+="&site_code=<?php echo $site_code; ?>&niosses=<?php echo isset($_REQUEST['PHPSESSID']) ? $_REQUEST['PHPSESSID'] : "X"; ?>";
			//console.log(dta);
			var r = AssistHelper.doAjax("login_ajax.php",dta);
			//console.log(r);
			nextSteps(r);
		} else {
			alert("Please enter all the required information highlighted in red.");
		}
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	}).children(".ui-button-text").css({"font-size":"80%"});
	
	$("#btn_cancel").button().click(function(e) {
		e.preventDefault();
		//dta="";
		//console.log(dta);
		//var r = AssistHelper.doAjax("login_ajax.php",dta);
		document.location.href = "logout.php?r=<?php echo base64_encode("SUPER_FAIL"); ?>";
	}).children(".ui-button-text").css({"font-size":"70%"});

	$("form[name=frm_security] input").keypress(function(e) {
		 var code = (e.keyCode ? e.keyCode : e.which); 
		 if(code == 13) { //Enter keycode 
		   $("form[name=frm_security] #btn_second").trigger("click");
		 } 
	});

});



<?php
if($token['auto']===true) {
	echo "
setTimeout(triggerLogin(),5000);
";
}
?>


</script>
  </body>
</html> 
