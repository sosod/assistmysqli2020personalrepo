<style type=text/css>
<!--
.fc {color: #006600}
.bc {background-color: #006600}
.grey {color: #444444;}
.greybc {background-color: #444444;}
a {color: #006600;}
a:hover {color: #006600;}
a:active {color: #006600;}
hr {color: #006600;}

th {
	background-color: #006600;
}

h1 {
	color: $006600;
}
h2 {
	color: $006600;
}
h3 {
	color: $006600;
}
h4 {
	color: $006600;
}


.tdheader {
	color: #ffffff;
	text-align: center;
	background-color: #006600;
	font-weight: bold;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
}
.tdheaderl {
	color: #ffffff;
	text-align: left;
	background-color: #006600;
	font-weight: bold;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
}
.butlight {
	font-family: Arial, sans-serif;
	font-size: 10px;
	padding: 1px;
	margin: 0px 0px 0px 0px;
	border: 1px solid #006600;
	line-height: 13px;
	color: #000000;
	background-color: #ffffff;
}

-->
</style>
<?php $help_icon = "<img src=/pics/help_green.gif align=absmiddle border=0>"; ?>
