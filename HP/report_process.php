<?php
include("inc_ignite.php");
include("inc_logfile.php");
//include("inc_errorlog.php");

$field = $_POST['field'];
$sfilter = $_POST['statusfilter'];
$csvfile = $_POST['csvfile'];
$dfilter = $_POST['datefilter'];
$dfiltert = $_POST['datefiltertype'];
$pfilter = $_POST['pplfilter'];

foreach($field as $f)
{
    $farray[$f] = "Y";
}


$sql = "SELECT s.id sid, a.callid tid, t.tkname tname, t.tksurname tsurname, l.value subject, a.callmessage cmsg,";
$sql .= " s.value tstatus, a.calldate cdate, k.tkname toname, k.tksurname tosurname, o.logdate, o.logusertext, o.logadmintext, o.logutype";
$sql .= " FROM assist_".$cmpcode."_hp_call a, assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_hp_list_subject l,";
$sql .= " assist_".$cmpcode."_timekeep k, assist_".$cmpcode."_hp_list_status s, assist_".$cmpcode."_hp_list_ppl p, assist_".$cmpcode."_hp_log o";
$sql .= " WHERE a.calltkid = t.tkid";
$sql .= " AND a.callstatusid = s.id";
$sql .= " AND a.callsubjectid = l.id";
$sql .= " AND a.callpplid = p.id AND p.tkid = k.tkid AND a.callid = o.logcallid";
if($dfiltert == "L")
{
    switch($sfilter)
    {
        case 1:
            $sql .= " AND (s.id <> 4 AND s.id <> 5 AND s.id <> 9)";
            break;
        case 2:
            $sql .= " AND (s.id = 4) AND o.logutype = 'A'";
            break;
        case 3:
            $sql .= " AND (s.id = 3)";
            break;
        case 4:
            $sql .= " AND (s.id <> 5) AND ((s.id = 4 AND o.logutype = 'A') OR (s.id = 2 AND (o.logutype = 'N' OR o.logutype = 'T')) OR ((s.id = 3 OR s.id = 6 OR s.id = 7 OR s.id = 8) AND o.logutype = 'A'))";
            break;
        default:
            $sql .= " AND (s.id <> 5)";
            break;
    }
}

switch($pfilter)
{
    case "All":
        break;
    default:
        $sql.= " AND (p.tkid = ".$pfilter.")";
        break;
}

switch($dfilter)
{
    case "ALL":
        break;
    case "SOME":
        $aday = $_POST['aday'];
        $amon = $_POST['amon'];
        $ayear = $_POST['ayear'];
        $zday = $_POST['zday'];
        $zmon = $_POST['zmon'];
        $zyear = $_POST['zyear'];
        $adate = getdate(mktime(0,0,0,$amon,$aday,$ayear));
        $zdate = getdate(mktime(0,0,0,$zmon,$zday,$zyear));
        $bdate = $adate[0]-1;
        $ydate = $zdate[0]+1;
        if($dfiltert == "L")
        {
            $sql .= " AND a.calldate > '".$bdate."' AND a.calldate < '".$ydate."'";
        }
        else
        {
            $sql .= " AND o.logdate > '".$bdate."' AND o.logdate < '".$ydate."' AND (s.id = 4) AND o.logutype = 'A'";
        }
        break;
    default:
        break;
}
$sql .= " ORDER BY a.callid ASC, o.logid DESC";
include("inc_db_con.php");


if($csvfile == "Y")
{
$fdata = "\"Ref\"";
            if($farray['calldate'] == "Y") {$fdata .= ",\"Date logged\"";}
            if($farray['tkid'] == "Y") {$fdata .= ",\"Logged by\"";}
            if($farray['pplid'] == "Y") {$fdata .= ",\"Assigned to\"";}
            if($farray['subjectid'] == "Y") {$fdata .= ",\"Subject\"";}
            if($farray['callmessage'] == "Y") {$fdata .= ",\"Message\"";}
            if($farray['logdate'] == "Y") {$fdata .= ",\"Date resolved/last updated\"";}
            if($farray['loguser'] == "Y") {$fdata .= ",\"Most recent update sent to user\"";}
            if($farray['logadmin'] == "Y") {$fdata .= ",\"Most recent admin update\"";}
            $fdata .= ",\"Call status\"";
    $lid = 0;
    while($row = mysql_fetch_array($rs))
    {
        $lid2 = $row['tid'];
        if($lid != $lid2)
        {
            $lid = $lid2;
            $lid2 = 0;
            $fdata .= "\r\n";
		    $fdata .= "\"".$row['tid']."\"";
		    if($farray['calldate'] == "Y") {$fdata .= ",\"".date("d-M-Y H:i",$row['cdate'])."\"";}
		    if($farray['tkid'] == "Y")
            {
                $tname = $row['tname']." ".$row['tsurname'];
                $tname = str_replace("&#39","'",$tname);
                $fdata .= ",\"".$tname."\"";
            }
		    if($farray['pplid'] == "Y")
            {
	       	    $toname = $row['toname']." ".$row['tosurname'];
    		    $toname = str_replace("&#39","'",$toname);
                $fdata .= ",\"".$toname."\"";
            }
		    if($farray['subjectid'] == "Y")
            {
	       	    $topic = $row['subject'];
    		    $topic = str_replace("&#39","'",$topic);
                $fdata .= ",\"".$topic."\"";
            }
		    if($farray['callmessage'] == "Y")
            {
	       	    $topic = $row['cmsg'];
    		    $topic = str_replace("&#39","'",$topic);
    		    $topic = str_replace("\"","'",$topic);
                $fdata .= ",\"".$topic."\"";
            }
		    if($farray['logdate'] == "Y")
            {
                if($row['sid'] > 2 && ($row['logutype'] == "A" || $row['logutype'] == "T"))
                {
                    $ldate = $row['logdate'];
                    $fdata .= ",\"".date("d-M-Y H:i",$ldate)."\"";
                }
                else
                {
                    $fdata .= ",\"\"";
                }
            }
		    if($farray['loguser'] == "Y")
            {
                if($row['sid'] > 2 && ($row['logutype'] == "A" || $row['logutype'] == "T"))
                {
	       	    $topic = $row['logusertext'];
    		    $topic = str_replace("&#39","'",$topic);
    		    $topic = str_replace("\"","'",$topic);
                $fdata .= ",\"".$topic."\"";
                }
                else
                {
                    $fdata .= ",\"\"";
                }
            }
		    if($farray['logadmin'] == "Y")
            {
                if($row['sid'] > 2 && ($row['logutype'] == "A" || $row['logutype'] == "T"))
                {
	       	    $topic = $row['logadmintext'];
    		    $topic = str_replace("&#39","'",$topic);
    		    $topic = str_replace("\"","'",$topic);
                $fdata .= ",\"".$topic."\"";
                }
                else
                {
                    $fdata .= ",\"\"";
                }
            }
    		$fdata .= ",\"".$row['tstatus']."\"";
        }
    }
    mysql_close();
    $filename = "../files/".$cmpcode."/hp_report_".date("Ymd_Hi",$today).".csv";
    $newfilename = "help_report_".date("Ymd_Hi",$today).".csv";
    $file = fopen($filename,"w");
    fwrite($file,$fdata."\n");
    fclose($file);
    header('Content-type: text/plain');
    header('Content-Disposition: attachment; filename="'.$newfilename.'"');
    readfile($filename);
}
else
{
$display = "<html><link rel=stylesheet href=/default.css type=text/css>";
include("inc_style.php");
$display .= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 class=fc><b>Help Assist: Report</b></h1>";

$hpr = mysql_num_rows($rs);
if($hpr == 0)
{
    $display .= "<p>There are no Help Assist calls that meet your criteria.<br>Please go back and select different criteria.</p>";
}
else
{
    $display .= "<table border=1 cellspacing=0 cellpadding=2>	<tr>		<td class=tdheaderblue>&nbsp;Ref&nbsp;</td>";
            if($farray['calldate'] == "Y") {$display .= "<td class=tdheaderblue>&nbsp;Date logged&nbsp;</td>";}
    		if($farray['tkid'] == "Y") {$display .= "<td class=tdheaderblue>&nbsp;Logged by&nbsp;</td>";}
            if($farray['pplid'] == "Y") {$display .= "<td class=tdheaderblue>&nbsp;Assigned to&nbsp;</td>";}
            if($farray['subjectid'] == "Y") {$display .= "<td class=tdheaderblue>&nbsp;Subject&nbsp;</td>";}
            if($farray['callmessage'] == "Y") {$display .= "<td class=tdheaderblue>&nbsp;Message&nbsp;</td>";}
            if($farray['logdate'] == "Y") {$display .= "<td class=tdheaderblue>&nbsp;Date resolved/&nbsp;<br>last updated</td>";}
            if($farray['duration'] == "Y") {$display .= "<td class=tdheaderblue>&nbsp;Duration&nbsp;</td>";}
            if($farray['loguser'] == "Y") {$display .= "<td class=tdheaderblue>Most recent update<br>sent to the user</td>";}
            if($farray['logadmin'] == "Y") {$display .= "<td class=tdheaderblue>Most recent update<br>admin update</td>";}
            $display .= "<td class=tdheaderblue>Call status</td>";
	$display .= "</tr>";
    $lid = 0;
    while($row = mysql_fetch_array($rs))
    {
        $lid2 = $row['tid'];
        if($lid != $lid2)
        {
            $lid = $lid2;
            $lid2 = 0;
            $display .= "<tr>";
		    $display .= "<td class=tdheaderblue valign=top>".$row['tid']."</td>";
		    if($farray['calldate'] == "Y") {$display .= "<td class=tdgeneral valign=top align=center>".date("d-M-Y H:i",$row['cdate'])."</td>";}
		    if($farray['tkid'] == "Y") {$display .= "<td class=tdgeneral valign=top>".$row['tname']." ".$row['tsurname']."</td>";}
		    if($farray['pplid'] == "Y") {$display .= "<td class=tdgeneral valign=top>".$row['toname']." ".$row['tosurname']."</td>";}
		    if($farray['subjectid'] == "Y") {$display .= "<td class=tdgeneral valign=top>".$row['subject']."</td>";}
		    if($farray['callmessage'] == "Y") {$display .= "<td class=tdgeneral valign=top>".str_replace(chr(10),"<br>",$row['cmsg'])."</td>";}
		    if($farray['logdate'] == "Y")
            {
                $display .= "<td class=tdgeneral valign=top align=center>";
                if($row['sid'] > 2 && ($row['logutype'] == "A" || $row['logutype'] == "T"))
                {
                    $duration = $row['logdate'] - $row['cdate'];
                    $display .= date("d-M-Y H:i",$row['logdate']);
                }
                else
                {
                    $duration = "&nbsp;";
                    $display .= "&nbsp;";
                }
                $display .= "</td>";
            }
		    if($farray['duration'] == "Y")
            {
                $display .= "<td class=tdgeneral valign=top align=center>";
                if($duration != "&nbsp;")
                {
                    //$durdate =
                    //$min1 = $duration/60;
                    $min2 = round($duration/60);
                    $hr1 = $min2/60;
                    $hr2 = round($hr1);
                    if($hr2 > $hr1) { $hr2 = $hr2 - 1; }
                    $mindiff = round(($hr1 - $hr2) * 60);
                    if($hr2 < 10) { $hr2 = "0".$hr2; }
                    if($mindiff < 10) { $mindiff = "0".$mindiff; }
                    $display .= $hr2.":".$mindiff;
                    //$display .= date("d H:i",$duration);
                }
                else
                {
                    $display .= $duration;
                }
                $display .= "</td>";
            }
		    if($farray['loguser'] == "Y")
            {
                $display .= "<td class=tdgeneral valign=top>";
                if($row['logutype']=="A" || $row['logutype'] == "T")
                {
                    $display .= str_replace(chr(10),"<br>",$row['logusertext']);
                }
                $display .= "&nbsp;</td>";
            }
		    if($farray['logadmin'] == "Y")
            {
                $display .= "<td class=tdgeneral valign=top>";
                if($row['logutype']=="A" || $row['logutype'] == "T")
                {
                    $display .= str_replace(chr(10),"<br>",$row['logadmintext']);
                }
                $display .= "&nbsp;</td>";
            }
    		$display .= "<td class=tdgeneral valign=top>".$row['tstatus'];
            $display .= "</td>	</tr>";
        }
    }
    $display .= "</table>";
}
mysql_close();
$display .= "</body></html>";

echo($display);


}
?>

