<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: 20px"><b>Help Assist: Statistics</b></h1>
<p style="margin-top: -15px; margin-left: 10px"><small><a href=stats_ppl.php>Display statistics per person</a></small></p>
<h2 class=fc>Summary:
<table border="1" id="table1" cellspacing="0" cellpadding="3">
	<tr>
		<td class="tdheader">Status</td>
		<td class="tdheader">Num.</td>
		<td class="tdheader">% of total</td>
	</tr>
<?php
$htot = 0;
$h = 0;
$sql = "SELECT s.id, s.value, count(c.callid) as cc FROM assist_".$cmpcode."_hp_call c, assist_".$cmpcode."_hp_list_status s WHERE c.callstatusid = s.id GROUP BY s.id, s.value ORDER BY s.sort";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $sid[$h] = $row['id'];
    $sval[$h] = $row['value'];
    $scount[$h] = $row['cc'];
    $h++;
    $htot = $htot + $row['cc'];
}
mysql_close();
?>
<?php
$h2 = 0;
for($h2=0;$h2<$h;$h2++)
{
?>
	<tr>
		<td class="tdgeneral"><?php echo($sval[$h2]); ?>&nbsp;</td>
		<td class="tdgeneral" align=center><?php echo($scount[$h2]); ?></td>
		<td class="tdgeneral" align=right><?php echo(number_format(($scount[$h2]/$htot)*100,2));?>%</td>
	</tr>
<?php
}
?>
	<tr>
		<td class="tdgeneral"><b>Total</b></td>
		<td class="tdgeneral" align=center><b><?php echo($htot); ?></b></td>
		<td class="tdgeneral" align=right><b>100%</b></td>
	</tr>
</table>

<h2 class=fc><b>Details:</b>
<table border="1" id="table2" cellspacing="0" cellpadding="3">
<?php
$p = 0;
$sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_hp_list_ppl p, assist_".$cmpcode."_timekeep t WHERE p.tkid = t.tkid AND p.yn = 'Y' ORDER BY tkname, tksurname";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $tk[$row['tkid']] = $row['tkname']." ".$row['tksurname'];
    $tid[$p] = $row['tkid'];
    $p++;
}
mysql_close();

$h2 = 0;
for($h2=0;$h2<$h;$h2++)
{
$tcount = "";
    if($h2 > 0)
    {
        ?>
        <tr>
            <td colspan="3" class="tdgeneral" ><img src=../pics/blank.gif height=1></td>
        </tr>
        <?php
    }
    ?>
	<tr>
		<td colspan="3" class="tdheaderl"><?php echo($sval[$h2].": ".$scount[$h2]." (".number_format(($scount[$h2]/$htot)*100,2)."% of total)");?>&nbsp;</td>
	</tr>
    <?php
    $sql = "SELECT t.tkid, t.tkname, t.tksurname, count(c.callid) as cc FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_hp_list_ppl p, assist_".$cmpcode."_hp_call c, assist_".$cmpcode."_hp_list_status s WHERE s.id = c.callstatusid AND s.id = ".$sid[$h2]." AND p.tkid = t.tkid AND p.yn = 'Y' AND c.callpplid = p.id GROUP BY t.tkname, t.tksurname ORDER BY t.tkname, t.tksurname";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
//        $tname[$row['tkid']] = $row['tkname']." ".$row['tksurname'];
        $tcount[$row['tkid']] = $row['cc'];
    }
    mysql_close();

    foreach($tid as $t)
    {
        if($tcount[$t] > 0)
        {
        }
        else
        {
            $tcount[$t] = 0;
        }
        ?>
    	<tr>
            <td class="tdgeneral">&nbsp;&nbsp;&nbsp;<?php echo($tk[$t]); ?>&nbsp;</td>
            <td class="tdgeneral" align=center><?php echo($tcount[$t]); ?>&nbsp;</td>
            <td class="tdgeneral" align=right><?php echo(number_format(($tcount[$t]/$scount[$h2])*100,2)); ?>%&nbsp;</td>
        </tr>
        <?php
    }
}
?>
</table>

</body>

</html>
