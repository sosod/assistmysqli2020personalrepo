<?php include("inc_ignite.php"); ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    return true;
}
function dateFiler(me) {
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Help Assist: Report</b></h1>
<form name=taskreport method=post action=report_process.php language=jscript onsubmit="return Validate(this);">
<h3 class=fc>1. Select the details you want in your report:</h3>
<div style="margin-left: 17px">
<table border="0" id="table1" cellpadding="3" cellspacing="0" class=noborder>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="calldate"></td>
		<td class="tdgeneral">Date logged</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="tkid"></td>
		<td class="tdgeneral">Logged by...</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="pplid"></td>
		<td class="tdgeneral">Assigned to</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="subjectid"></td>
		<td class="tdgeneral">Subject</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="callmessage"></td>
		<td class="tdgeneral">Message</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="logdate"></td>
		<td class="tdgeneral">Date resolved/last updated (where available)</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="duration"></td>
		<td class="tdgeneral">Duration (where available)</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="loguser"></td>
		<td class="tdgeneral">Most recent update sent to the user (where available)</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="logadmin"></td>
		<td class="tdgeneral">Most recent admin update (where available)</td>
	</tr>
</table>
</div>
<h3 class=fc>2. Select the filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table border="1" id="table2" cellspacing="0" cellpadding="3">
<?php
            $sql = "SELECT hp.id i, hp.tkid t, tk.tkname n, tk.tksurname s FROM assist_".$cmpcode."_hp_list_ppl hp, assist_".$cmpcode."_timekeep tk WHERE tk.tkid = hp.tkid AND hp.yn = 'Y' AND hp.respondyn = 'Y' ORDER BY tk.tkname, tk.tksurname ASC";
            include("inc_db_con.php");
            $r = mysql_num_rows($rs);
            if($r > 1)
            {
                ?>
	<tr>
		<td class="tdgeneral">Person:</td>
		<td class="tdgeneral">
            <select size="1" name="pplfilter"><option selected value=All>All respondants</option>
                <?php
                    while($row = mysql_fetch_array($rs))
                    {
                        $id = $row['i'];
                        $tid = $row['t'];
                        $val = $row['n']." ".$row['s'];
                        if($tid != "0000")
                        {
                            echo("<option value=\"".$tid."\">".$val."</option>");
                        }
                    }
                ?>
            </select>
        </td>
	</tr>
<?php
            }
            else
            {
                $row = mysql_fetch_array($rs);
                echo("<input type=hidden name=pplfilter value=\"All\"");
            }

?>
	<tr>
		<td class="tdgeneral">Status:</td>
		<td class="tdgeneral"><select size="1" name="statusfilter">
			<option selected value=1>All calls not closed</option>
			<option value=2>Closed calls</option>
			<option value=3>Calls in progress only</option>
			<option value=4>All calls</option>
		</select></td>
	</tr>
	<tr>
		<td class="tdgeneral" valign=top>Date <select name=datefiltertype><option selected value=L>logged</option><option value=C>closed</option></select>*:</td>
		<td class="tdgeneral">
        <input type=radio name=datefilter value=ALL checked> All dates<br>
		<input type=radio name=datefilter value=SOME> from <input type=text size=2 name=aday value=1><select name=amon>
		<?php
		$mmon = date("n");
		for($m=1;$m<13;$m++)
		{
            $mdate = getdate(mktime(0,0,0,$m,1,0));
            $mmon2 = date("n",$mdate[0]);
            $msel = date("M",$mdate[0]);
            if($mmon == $mmon2)
            {
                echo("<option selected value=".$m.">".$msel."</option>");
            }
            else
            {
                echo("<option value=".$m.">".$msel."</option>");
            }
		}
        ?>
        </select><input type=text name=ayear size=4 value=<?php echo(date("Y")); ?>> to <?php
        $feb = 28 + date("L");
        $mdays = array(0,31,$feb,31,30,31,30,31,31,30,31,30,31);
        echo("<input type=text size=2 name=zday value=".$mdays[$mmon].">");
        ?>
        <select name=zmon>
		<?php
		for($m=1;$m<13;$m++)
		{
            $mdate = getdate(mktime(0,0,0,$m,1,0));
            $mmon2 = date("n",$mdate[0]);
            $msel = date("M",$mdate[0]);
            if($mmon == $mmon2)
            {
                echo("<option selected value=".$m.">".$msel."</option>");
            }
            else
            {
                echo("<option value=".$m.">".$msel."</option>");
            }
		}
        ?>
        </select><input type=text name=zyear size=4 value=<?php echo(date("Y")); ?>>
        </td>
	</tr>
	<tr>
    <td class=tdgeneral colspan=2>* Note: If you select "date closed" only closed calls will display,<br>irrespective of what status you select.</td>
	</tr>
</table>
</div>
<h3 class=fc>3. Select the report output method:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3">
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="csvfile" value="N" checked></td>
		<td class="tdgeneral">Onscreen display</td>
	</tr>
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="csvfile" value="Y"></td>
		<td class=tdgeneral>Save to file (Microsoft Excel file)</td>
	</tr>
</table>
</div>
<h3 class=fc>4. Click the button:</h3>
<p style="margin-left: 17px"><input type="submit" value="Generate Report" name="B1">
<input type="reset" value="Reset" name="B2"></p>
</form>
<p>&nbsp;</p>


</body>

</html>
