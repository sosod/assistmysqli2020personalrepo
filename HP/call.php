<?php
    include("inc_ignite.php");
    include("inc_hp.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate() {
    var tkid = document.logcall.calltkid.value;
    var pplid = document.logcall.callpplid.value;
    var urgencyid = document.logcall.callurgencyid.value;
    var subjectid = document.logcall.callsubjectid.value;
    var message = document.logcall.callmessage.value;
    var valid8 = "false";
    //alert("T-"+tkid+"\nP-"+pplid+"\nU-"+urgencyid+"\nS-"+subjectid+"\nM-"+message);
    if(pplid == "X")
    {
        alert("Please select the person to respond to this call.");
    }
    else
    {
        if(urgencyid == "X")
        {
            alert("Please select the urgency of this call.");
        }
        else
        {
            if(subjectid == "X")
            {
                alert("Please select the subject of this call.");
            }
            else
            {
                if(message.length == 0)
                {
                    alert("Please enter a description of your problem.");
                }
                else
                {
                    //alert("true");
                    return true;
                }
            }
        }
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Help Assist: Log a call</b></h1>
<form name=logcall action=call_process.php method=POST onsubmit="return Validate();" language=jscript>
<table border="1" id="table1" cellspacing="0" cellpadding="5">
	<tr>
		<td class="tdgeneral" valign="top"><b>Your name:</b></td>
		<td class="tdgeneral" valign="top"><?php echo($tkname); ?><input type=hidden name=calltkid value=<?php echo($tkid);?>></td>
	</tr>
<?php
$sql = "SELECT tkloc FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$tkid."'";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$tkloc = $row['tkloc'];
mysql_close();
$row = "";

$sql = "SELECT * FROM assist_".$cmpcode."_list_loc ORDER BY value";
include("inc_db_con.php");
$l = mysql_num_rows($rs);
if($l > 1)
{
    ?>
	<tr>
		<td class="tdgeneral" valign="top"><b>Your location:</b></td>
		<td class="tdgeneral" valign="top"><select name=calllocationid>
    <?php
    while($row = mysql_fetch_array($rs))
    {
        $id = $row['id'];
        $val = $row['value'];
        if($id == $tkloc)
        {
            echo("<option selected value=".$id.">".$val."</option>");
        }
        else
        {
            echo("<option value=".$id.">".$val."</option>");
        }
    }
    ?>
        </select></td>
	</tr>
<?php
}
else
{
    $row = mysql_fetch_array($rs);
    echo("<input type=hidden name=calllocationid value=".$row['id'].">");
}
mysql_close();

if($ignitehp[3] == "U" && $ignitehp[2] == "Y")
{
?>
	<tr>
		<td class="tdgeneral" valign="top"><b>Respondant:</b></td>
		<td class="tdgeneral" valign="top">
            <?php
            $sql = "SELECT hp.id i, hp.tkid t, tk.tkname n, tk.tksurname s FROM assist_".$cmpcode."_hp_list_ppl hp, assist_".$cmpcode."_timekeep tk WHERE tk.tkid = hp.tkid AND hp.yn = 'Y' AND hp.respondyn = 'Y' ORDER BY tk.tkname, tk.tksurname ASC";
            include("inc_db_con.php");
            $r = mysql_num_rows($rs);
            if($r > 1)
            {
                ?>
                <select size="1" name="callpplid"><option selected value=X>--- SELECT ---</option>
                <?php
                    while($row = mysql_fetch_array($rs))
                    {
                        $id = $row['i'];
                        $tid = $row['t'];
                        $val = $row['n']." ".$row['s'];
                        if($tid != "0000")
                        {
                            echo("<option value=".$id.">".$val."</option>");
                        }
                    }
                ?>
                </select>
                <?php
            }
            else
            {
                $row = mysql_fetch_array($rs);
                echo("<input type=hidden name=callpplid value=".$row['i'].">".$row['n']." ".$row['s']);
            }
            ?>
        </td>
	</tr>
<?php
}
else
{
    if($ignitehp[2] == "Y")
    {
        echo("<input type=hidden name=callpplid value=0>");
    }
    else
    {
        echo("<input type=hidden name=callpplid value=".$ignitehp[0].">");
    }
}
?>
	<tr>
		<td class="tdgeneral" valign="top"><b>Urgency:</b></td>
		<td class="tdgeneral" valign="top"><select size="1" name="callurgencyid"><option selected value=X>--- SELECT ---</option>
            <?php
                $sql = "SELECT * FROM assist_".$cmpcode."_hp_list_urgency WHERE yn = 'Y' ORDER BY sort ASC";
                include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['id'];
                    $val = $row['value'];
                    echo("<option value=".$id.">".$val."</option>");
                }
            ?>
        </select></td>
	</tr>
	<tr>
		<td class="tdgeneral" valign="top"><b>Subject:</b></td>
		<td class="tdgeneral" valign="top"><select size="1" name="callsubjectid"><option selected value=X>--- SELECT ---</option>
            <?php
                $sql = "SELECT * FROM assist_".$cmpcode."_hp_list_subject WHERE yn = 'Y' ORDER BY value ASC";
                include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['id'];
                    $val = $row['value'];
                    echo("<option value=".$id.">".$val."</option>");
                }
            ?></select></td>
	</tr>
	<tr>
		<td class="tdgeneral" valign="top"><b>Message:</b></td>
		<td class="tdgeneral" valign="top">
		<textarea rows="6" name="callmessage" cols="33"></textarea></td>
	</tr>
	<tr>
		<td colspan="2" class="tdgeneral" valign="top">
		<input type="submit" value="Submit" name="B1">
		<input type="reset" value="Reset" name="B2"></td>
	</tr>
</table>

</form>
<?php
    include("inc_back.php");
?>
</body>

</html>
