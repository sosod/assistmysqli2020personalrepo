<?php
    include("inc_ignite.php");
    include("inc_hp.php");
    
    $st = $_GET['st'];
    if(strlen($st) > 0 && $st == "cl")
    {
        $sqlst = "cl.callstatusid = 4";
        $title = "<h1 class=fc><b>Help Assist: View status - Closed calls</b></h1>";
        $button = "<p><input type=button value='View Current Calls' class=but onclick=".chr(34)."viewCalls('cur')".chr(34)."></p>";

    }
    else
    {
        $sqlst = "cl.callstatusid <> 4 AND cl.callstatusid <> 5";
        $title = "<h1 class=fc><b>Help Assist: View status - Current calls</b></h1>";
        $button = "<p><input type=button value='View Closed Calls' class=but onclick=".chr(34)."viewCalls('cl')".chr(34)."></p>";
    }
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate() {
    return false;
}

function viewCalls(st) {
    document.location.href="view.php?st="+st;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<?php
echo($title);

$sql = "SELECT cl.callid i, cl.calldate d, st.value stval, sub.value sval FROM assist_".$cmpcode."_hp_call cl, assist_".$cmpcode."_hp_list_status st, assist_".$cmpcode."_hp_list_subject sub WHERE cl.callsubjectid = sub.id AND cl.callstatusid = st.id AND cl.calltkid = '".$tkid."' AND ".$sqlst." ORDER BY cl.callid ASC";
include("inc_db_con.php");
$n = mysql_num_rows($rs);
if($n > 0)
{
?>
<div align=left>
<table border=1 cellpadding=5 cellspacing=0>
    <tr>
        <td class=tdheaderblue>Ref</td>
        <td class=tdheaderblue>Date</td>
        <td class=tdheaderblue>Subject</td>
        <td class=tdheaderblue>Status</td>
    </tr>
    <?php
    while($row = mysql_fetch_array($rs))
    {
        ?>
    <tr>
        <td class=tdgeneral><?php echo($row['i']); ?></td>
        <td class=tdgeneral><?php echo("<a href=view_call.php?i=".$row['i'].">".date('d-M-Y',$row['d'])."</a>"); ?></td>
        <td class=tdgeneral><?php echo($row['sval']); ?></td>
        <td class=tdgeneral><?php echo($row['stval']); ?></td>
    </tr>
        <?php
    }
    ?>
</table>
<?php
}
else
{
	echo("<P>There are no calls to view.</p>");
}
echo($button);
    include("inc_back.php");
?>
</body>

</html>
