<?php
    include("inc_ignite.php");
    include("inc_hp.php");
    
    $id = $_GET['u'];
    if(strlen($id) > 0)
    {
        $sql = "SELECT * FROM assist_".$cmpcode."_hp_list_status WHERE id = ".$id." ORDER BY sort DESC";
        //---echo($sql);
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        mysql_close();

        $sql = "SELECT count(callid) as c FROM assist_".$cmpcode."_hp_call WHERE callstatusid = ".$id." ";
        //---echo($sql);
        include("inc_db_con.php");
        $crow = mysql_fetch_array($rs);
        mysql_close();
        $c = $crow['c'];
    }
    else
    {
        $id = $_POST["id"];
        $val = $_POST["val"];
        $sort = $_POST["sort"];
        $subbut = $_POST["subbut"];
        if($subbut == "Del")
        {
            $sql = "UPDATE assist_".$cmpcode."_hp_list_status SET yn = 'N' WHERE id = ".$id;
//            echo($sql);
            include("inc_db_con.php");
        }
        else
        {
            $sql = "UPDATE assist_".$cmpcode."_hp_list_status SET value = '".$val."', sort = ".$sort." WHERE id = ".$id;
//            echo($sql);
            include("inc_db_con.php");
        }
        header('Location: setup_status.php');
    }
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
<base target="main">
</head>
<script language=JavaScript>
function Validate() {
    if(confirm("Editing this record will affect all calls associated with it.\n\nAre you sure you wish to continue?"))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function butClick(subbut) {
    document.edit.subbut.value = subbut;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 style="margin-bottom: 20px" class=fc><b>Help Assist: Setup - Status</b></h1>
<p>Please note that you may only delete a status once there are no calls associated with that status.</p>
<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td class="tdheader">ID</td>
		<td class="tdheader">Value</td>
		<td class="tdheader">Display Order</td>
	</tr>
<form name=edit method=post action=setup_status_edit.php onsubmit="return Validate()" language=jscript>
	<tr>
		<td class="tdgeneral" align=center><?php echo $row['id'];?><input type=hidden name=id value=<?php echo $row['id'];?>></td>
		<td class="tdgeneral"><input type=text name=val value="<?php echo $row['value'];?>"></td>
		<td class="tdgeneral"><input type=text name=sort value=<?php echo $row['sort'];?>></td>
	</tr>
	<tr>
		<td class="tdgeneral" colspan=3><input type=hidden value="" name=subbut><input type=submit value=Edit name=but onclick="butClick('Edit')"> <?php if($c == 0) {?><input type=submit value=Del name=but onclick="butClick('Del')"><?php } ?>&nbsp;
        <i>Calls affected: <?php echo($c); ?></i></td>
	</tr>
</form>
</table>
<?php
    include("inc_back.php");
?>

</body>

</html>
