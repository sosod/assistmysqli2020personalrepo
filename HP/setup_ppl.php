<?php
    include("inc_ignite.php");
    include("inc_hp.php");
    
if(isset($_POST['tkid'])) {
    $tkid = $_POST['tkid'];
    $access = $_POST['access'];
    $respondyn = $_POST['respondyn'];
    
    if($tkid != "X" && $access != "X" && strlen($tkid) > 0 && strlen($access) > 0)
    {
        //ADD RECORD
        $sql = "INSERT INTO assist_".$cmpcode."_hp_list_ppl SET tkid = '".$tkid."', access = ".$access.", yn = 'Y', respondyn = '".$respondyn."'";
        //echo($sql);
        include("inc_db_con.php");
    }
}
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
<base target="main">
</head>
<script language=JavaScript>
function Edit(id) {
    document.location.href = "setup_ppl_edit.php?u="+id;
}
function Del(id,tn,cc) {
    while(tn.indexOf("_")>0)
    {
        tn = tn.replace("_"," ");
    }
    if(cc > 0)
    {
        alert("ERROR:\n\n"+tn+" can not be removed from the Help Assist Administrators as he/she still has calls in progress.\n\nThose calls must be closed or transfered to other Administrators before "+tn+" can be removed.");
    }
    else
    {
        if(confirm("Are you sure you wish to remove "+tn+" from the Help Assist Administrators?")==true)
        {
            document.location.href = "setup_ppl_del.php?u="+id;
        }
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 style="margin-bottom: 20px" class=fc><b>Help Assist: Setup - Admin Users</b></h1>
<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td class="tdheader">Ref</td>
		<td class="tdheader">User</td>
		<td class="tdheader">Calls in<br>progress</td>
		<td class="tdheader">Respond<br>to calls</td>
		<td class="tdheader">Admin<br>Access</td>
		<td class="tdheader">&nbsp;</td>
	</tr>
<?php
$sql = "SELECT hp.id h, tk.tkid i, hp.access a, hp.respondyn ryn, tk.tkname n, tk.tksurname s ";
$sql.= "FROM assist_".$cmpcode."_hp_list_ppl hp, assist_".$cmpcode."_timekeep tk ";
$sql.= "WHERE hp.tkid = tk.tkid ";
$sql.= "AND hp.yn = 'Y' ";
//$sql.= "AND hp.access < 100 ";
$sql.= "AND tk.tkstatus = 1 ";
$sql.= "ORDER BY tk.tkname, tk.tksurname ASC";
//echo $sql;
include("inc_db_con.php");
$n = mysql_num_rows($rs);
if($n > 0)
{
    $p = 0;
    while($row = mysql_fetch_array($rs))
    {
        $rowppl[$p] = $row;
        $p++;
    }
}
mysql_close();

if($n > 0)
{
    $sql = "SELECT count(callid) as cc, hp.id h ";
    $sql.= "FROM assist_".$cmpcode."_hp_list_ppl hp, assist_".$cmpcode."_hp_call c ";
    $sql.= "WHERE hp.yn = 'Y' ";
    $sql.= "AND hp.id = c.callpplid ";
    $sql.= "AND c.callstatusid <> 4 ";
    $sql.= "AND c.callstatusid <> 5 ";
    $sql.= "AND c.callstatusid <> 9 ";
    $sql.= "AND hp.access < 100 ";
    $sql.= "GROUP BY hp.id ";
    $sql.= "ORDER BY hp.id ASC";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $rowcc[$row['h']] = $row['cc'];
    }
    //mysql_close();

    foreach($rowppl as $ppl)
    {
        switch($ppl['a'])
        {
            case 0:
                $a = "Basic";
                break;
            case 50:
                $a = "Dept";
                break;
            case 99:
                $a = "Full";
                break;
            case 100:
                $a = "Module Admin";
                break;
            default:
                $a = "Unknown";
                break;
        }
        $tns = $ppl['n']." ".$ppl['s'];
        $tns = str_replace(" ","_",$tns);
        $cc = isset($rowcc[$ppl['h']]) ? $rowcc[$ppl['h']] : 0;
        if($cc < 1)
        {
            $cc = 0;
        }
    ?>
	<tr>
		<td class="tdgeneral" align=center><?php echo $ppl['h'];?></td>
		<td class="tdgeneral"><?php echo $ppl['n']." ".$ppl['s'];?></td>
		<td class="tdgeneral" align=center><?php echo $cc;?></td>
		<td class="tdgeneral" align=center><?php echo $ppl['ryn'];?></td>
		<td class="tdgeneral" align=center><?php echo $a;?></td>
		<td class="tdgeneral"><?php if($ppl['a'] < 100) { ?><input type=button value=Edit name=B4 onclick="Edit(<?php echo($ppl['h'])?>)"><?php if($cc > 0){}else{echo("<input type=button value=Del name=B4 onclick=\"Del(".$ppl['h'].",'".$tns."',".$cc.")\">");}?><?php } else { echo("&nbsp;"); }  ?></td>
	</tr>
    <?php
    }
}

$sql = "SELECT t.tkname, t.tksurname, t.tkid FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
$sql .= "WHERE t.tkstatus = 1 AND t.tkid <> '0000' ";
$sql .= "AND m.usrtkid = t.tkid AND m.usrmodref = 'HP' ";
$sql .= "AND tkid NOT IN (SELECT tkid FROM assist_".$cmpcode."_hp_list_ppl WHERE yn ='Y') ";
$sql .= "ORDER BY t.tkname, t.tksurname ASC";
include("inc_db_con.php");
$t = mysql_num_rows($rs);
if($t > 0)
{
?>
<form name=add method=post action=setup_ppl.php>
	<tr>
		<td class="tdgeneral" align=center>#</td>
		<td class="tdgeneral"><select name=tkid><option selected value=X>--- SELECT ---</option>
<?php
while($row = mysql_fetch_array($rs))
{
    if($row['tkid'] != $hpadmin)
    {
        $arr = ":".array_search($row['tkid'],$zarr).":";
        if(strlen($arr) == 2)
        {
            echo("<option value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
        }
    }
}
?>
        </select></td>
		<td class="tdgeneral" align=center>&nbsp;</td>
		<td class="tdgeneral"><select name=respondyn>
<option selected value=Y>Yes</option>
<option value=N>No</option>
        </select></td>
		<td class="tdgeneral"><select name=access><option selected value=X>--- SELECT ---</option>
<option value=0>Basic access</option>
<option value=99>Full access</option>
        </select></td>
		<td class="tdgeneral"><input type="submit" value="Add" name="B3"></td>
	</tr>
</form>
<?php
mysql_close();
}
?>
</table>
</body>

</html>
