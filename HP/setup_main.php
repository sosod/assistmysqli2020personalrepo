<?php
    include("inc_ignite.php");

$hp0 = $_POST["hp0"];
$hp1a = $_POST["hp1a"];
$hp1b = $_POST["hp1b"];
$hp3 = $_POST["hp3"];
$hpupdate = "N";

if(strlen($hp1a) > 0 && strlen($hp0) > 0)
{
    $hpupdate = "Y";
    if(strlen($hp0) > 0)
    {
        $sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE refid = 0 AND ref = 'HP'";
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        mysql_close();
        $tid = $row['value'];
        if($tid != $hp0)
        {
            $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$hp0."' WHERE ref = 'HP' AND refid = 0";
            include("inc_db_con.php");
            $tsql = $sql;
            $trans = "Changed setup admin from ".$tid." to ".$hp0.".";
            $tref = "HP";
            include("inc_transaction_log.php");
            $sql = "UPDATE assist_".$cmpcode."_hp_list_ppl SET access = 99 WHERE tkid = '".$hp0."'";
            include("inc_db_con.php");
            $tsql = $sql;
            $trans = "Set module admin access to 99 for new setup admin ".$hp0.".";
            $tref = "HP";
            include("inc_transaction_log.php");
        }
    }
    if($hp1a == "N")
    {
        $hparr[1] = $hp1a;
    }
    else
    {
        $hparr[1] = $hp1b;
    }
    $hparr[2] = "Y";
    $hparr[3] = $hp3;
    $h = 0;
    for($h=1;$h<4;$h++)
    {
        $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$hparr[$h]."' WHERE ref = 'HP' AND refid = ".$h;
        //echo($sql);
        include("inc_db_con.php");
//        $ignitehp[$h] = $hparr[$h];
    }
}

    include("inc_hp.php");

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
<base target="main">
</head>
<script language=JavaScript>
function Email() {
    var hp1a = document.hpsetup.hp1a.value;
    if(hp1a == "N")
    {
        document.hpsetup.hp1b.style.visibility = "hidden";
    }
    else
    {
        document.hpsetup.hp1b.value = "";
        document.hpsetup.hp1b.style.visibility = "visible";
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 style="margin-bottom: 20px" class=fc><b>Help Assist: Setup</b></h1>
<form name=hpsetup method=POST action=setup_main.php><input type=hidden name=hpupdate value=<?php echo $hpupdate; ?>>
<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td class="tdgeneral">0</td>
		<td class="tdgeneral">Help Assist Setup Administrator</td>
		<td class="tdgeneral"><select size="1" name="hp0"><?php
		$sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_hp_list_ppl p WHERE t.tkstatus = 1 AND t.tkid <> '0000' AND t.tkid = p.tkid AND p.yn = 'Y' ORDER BY t.tkname, t.tksurname";
		include("inc_db_con.php");
		$t = mysql_num_rows($rs);
        if($t == 0)
        {
            echo("<option value=0000 selected>Administrator</option>");
        }
        else
        {
            if($ignitehp[0] == "0000")
            {
                echo("<option value=0000 selected>Administrator</option>");
            }
            else
            {
                echo("<option value=0000>Administrator</option>");
            }
            while($row = mysql_fetch_array($rs))
            {
                $id = $row['tkid'];
                $value = $row['tkname']." ".$row['tksurname'];
                if($id == $ignitehp[0])
                {
                    echo("<option selected value=".$id.">".$value."</option>");
                }
                else
                {
                    echo("<option value=".$id.">".$value."</option>");
                }
            }
        }
?>
</select></td>
	</tr>
	<tr>
		<td class="tdgeneral">1</td>
		<td class="tdgeneral">Email notification on new calls logged?</td>
		<td class="tdgeneral"><select size="1" name="hp1a" onchange=Email()>
<?php
if($ignitehp[1] == "N" || strlen($ignitehp[1]) < 5)
{
?>
            <option selected value=N>No</option><option value=Y>Yes</option>
<?php
}
else
{
?>
            <option value=N>No</option><option selected value=Y>Yes</option>
<?php
}
?>
        </select><input type="text" name="hp1b" size="30" value=<?php echo($ignitehp[1]);?>></td>
	</tr>
	<tr>
		<td class="tdgeneral">3</td>
		<td class="tdgeneral">Does the user choose who responds to their call,<br>or does an administrator allocate the calls?</td>
		<td class="tdgeneral"><select size="1" name="hp3"><?php
if($ignitehp[3] == "A")
{
?>
            <option value=U>User chooses</option><option selected value=A>Admin allocated</option>
<?php
}
else
{
?>
            <option selected value=U>User chooses</option><option value=A>Admin allocates</option>
<?php
}
?></select></td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral"><input type="submit" value="Submit" name="B1"> <input type="reset" value="Reset" name="B2"></td>
	</tr>
</table>
</form>
<?php
    if($hpupdate == "Y")
    {
        echo("<P>Update done...</p>");
    }
    else
    {
        include("inc_back.php");
    }
?>
<script language=JavaScript>
    var hp1a = document.hpsetup.hp1a.value;
    if(hp1a == "N")
    {
        document.hpsetup.hp1b.style.visibility = "hidden";
    }
    else
    {
        document.hpsetup.hp1b.style.visibility = "visible";
    }

</script>
</body>

</html>
