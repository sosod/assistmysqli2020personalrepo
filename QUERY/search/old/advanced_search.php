<?php
$scripts = array( 'advanced_search.js'  );
$styles = array();
$page_title = "Advanced Search";
require_once("../inc/header.php");
$naming 	= new Naming();
$rowNames   = $naming -> rowLabel();
function nameFields( $fieldId , $defaultName ){
	global $naming, $rowNames;
	if( isset( $rowNames[$fieldId] )) {
		echo $rowNames[$fieldId];
	}	else {
		echo $defaultName;
	}
}
?>
<div style="clear:both" id="resultsBox">
	<?php JSdisplayResultObj(""); ?>  
	<div id="searchresults" style="clear:both"></div>
	<div id="span_again" style="display:none; clear:both; padding-top:30px;"><a href="#" id="search_again">Search Again</a></div>
</div>
<div style="clear:both" id="searchBox">
	<form id="advanced-search-form" method="post">
		<table id="risk_new">
			 <tr>
			        <th><?php nameFields('query_item','Query Item'); ?>:</th>
			        <td><span id="idrisk"></span></td>
			      </tr>
			      <tr>
			        <th><?php nameFields('query_reference','Query Reference'); ?>:</th>
			        <td><input type="text" id="query_reference" name="query_reference" value="" /></td>
			      </tr>      
			      <tr>
			        <th><?php nameFields('query_type','Query Type'); ?>:</th>
			        <td>
			            <select id="type" name="type">
			                <option value=""><?php echo $select_box_text ?></option>
			            </select>
			        </td>
			      </tr>
			      <tr>
			        <th><?php nameFields('query_category','Query Category'); ?>:</th>
			        <td>
			            <select id="category" name="category">
			                <option value=""><?php echo $select_box_text ?></option>
			            </select>    
			        </td>
			      </tr>
			      <tr>
			        <th><?php nameFields('query_date','Query Raised Date'); ?>:</th>
			        <td><input type="text" name="query_date" id="query_date" class="datepicker" readonly="readonly" /></td>
			      </tr>      
			      <tr>
			        <th><?php nameFields('query_description','Query Description'); ?>:</th>
			        <td><textarea name="description" id="description" cols="35" rows="7"></textarea></td>
			      </tr>
			      <tr>
			        <th><?php nameFields('query_background','Background Of Query'); ?>:</th>
			        <td><textarea name="background" id="background" cols="35" rows="7"></textarea></td>
			      </tr>
			     <tr>
			        <th><?php nameFields('query_owner','Directorate responsible'); ?>:</th>
			        <td>
			        <select id="sub_id" name="sub_id" class="responsibility">
			                <option value=""><?php echo $select_box_text ?></option>
			         </select>        
			        </td>
			      </tr>
			      <tr>
			      	<th><?php nameFields('query_deadline_date','Query Deadline Date'); ?>:</th>
			      	<td><input type="text" name="query_deadline_date" id="query_deadline_date" class="datepicker" readonly="readonly" /></td>
			      </tr>
				  <tr class="more_details">
			        <th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
			        <td>
			        	<select id='financial_exposure' name='financial_exposure'>
			            	<option value=""><?php echo $select_box_text ?></option>
			        	</select>
			        </td>
			      </tr>
			      <tr class="more_details">
			        <th><?php nameFields('monetary_implication','Monetary Implication'); ?>:</th>
			        <td><input type='text' id='monetary_implication' name='monetary_implication' value='' /></td>
			      </tr>
			      <tr class="more_details">
			        <th><?php nameFields('risk_level','Risk Level'); ?>:</th>
			        <td>
			        <select id="risk_level" name="risk_level">
			                <option value=""><?php echo $select_box_text ?></option>
			         </select>            
			        </td>
			      </tr>      
			      <tr class="more_details">
			        <th><?php nameFields('risk_type','Risk Type'); ?>:</th>
			        <td>
			        <select id="risk_type" name="risk_type">
			                <option value=""><?php echo $select_box_text ?></option>
			         </select>            
			        </td>
			      </tr>
			      <tr class="more_details">
			        <th><?php nameFields('risk_detail','Risk Detail'); ?>:</th>
			        <td>
			        	<textarea class="maxlimit" title="200" id='risk_detail' name='risk_detail' cols="35" rows="5"></textarea>
			      </tr>      
			      <tr class="more_details">
			        <th><?php nameFields('finding','Add Finding'); ?>:</th>
			        <td>
			        	<textarea class="find" title="200" id='finding' name='finding' cols="35" rows="5"></textarea> 
			        </td>
			      </tr>
			      <tr class="more_details">
			        <th><?php nameFields('recommendation','Add Recommendation'); ?>:</th>
			        <td>
			        	<textarea class="recommend" title="200" id='recommendation' name='recommendation' cols="35" rows="5"></textarea>
			        </td>
			      </tr>
			      <tr class="more_details">
			        <th><?php nameFields('client_response','Add Client Response'); ?>:</th>
			        <td>
			            <textarea class="clientresponse" title="200" id='client_response' name='client_response' cols="35" rows="5"></textarea>                
			        </td>
			      </tr>      
			      <tr>
			        <th></th>
			        <td align="right"> <input type="submit" name="search" id="search" value=" Search " /></td>
			      </tr>
		 </table>
	</form>
</div>