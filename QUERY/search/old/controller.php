<?php
@session_start();
include_once("../../inc/init.php");
switch( $_REQUEST['action'] ) {
	
	case "search":
		$search  = new Search();
		echo json_encode( $search -> searchText() );
		break;
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getTypeCategory( $_REQUEST['id']) );
		break;
	case "getFinancialExposure":
		$finExp = new FinancialExposure( "", "", "", "", "");
		echo json_encode( $finExp -> getFinancialExposure() );
		break;		
	case "getRiskLevel":
		$level = new Risklevel( "", "", "", "" );
		echo json_encode( $level-> getLevel() );
		break;		
	case "getDirectorate":
		$dir = new Directorate( "", "", "" ,"");
		echo json_encode( $dir -> getDirectorate() );
		break;
	case "getSubDirectorate":
		$dir = new SubDirectorate( "", "", "" ,"");
		//$ditc = $dir -> getDirectorateResponsible();
		echo json_encode( $dir -> getDirectorateResponsible() );
		break;
	case "getRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getType() );
		break;
	case "getActiveRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getActiveRiskType() );
		break;
    case "getQueryType":
		$type = new QueryType( "", "", "", "" );
		echo json_encode( $type-> getActiveRiskType() );
		break;
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getTypeCategory( $_REQUEST['id']) );
		break;	
	case "advancedSearch":
		$adv   = new Search();
		echo json_encode( $adv -> advancedSearch() );
		break;	
	case "getUsers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUsers() );
		break;
}
?>
