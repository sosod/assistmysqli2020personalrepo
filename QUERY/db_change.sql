ALTER TABLE `assist_demo001_qry_query_register` CHANGE `attachment` `attachment` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE  `assist_admrdev_qry_query_register` ADD  `financial_year` INT NOT NULL AFTER  `id`;

ALTER TABLE  `assist_demo001_qry_actions` ADD  `action_on` DATE NOT NULL AFTER  `deadline`;


INSERT INTO `dev_iadmrdev`.`assist_admrdev_qry_menu` (`id`, `parent_id`, `name`, `client_name`, `description`, `folder`, `viewby`, `insertdate`, `insertuser`) VALUES (NULL, '1', 'Activate', 'Activate', 'Activate', 'activate', '', CURRENT_TIMESTAMP, '');


CREATE TABLE IF NOT EXISTS `assist_demo001_qry_action_assurance_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `client_terminology` text NOT NULL,
  `color` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `insertuser` varchar(10) NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `assist_demo001_qry_action_assurance_status_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_id` int(11) NOT NULL,
  `changes` text NOT NULL,
  `insertuser` varchar(10) NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE  `assist_demo001_qry_action_assurance_status` ADD  `color` VARCHAR( 10 ) NOT NULL AFTER  `client_terminology`;


CREATE TABLE IF NOT EXISTS `assist_demo001_qry_query_assurance_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `client_terminology` text NOT NULL,
  `color` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `insertuser` varchar(10) NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `assist_demo001_qry_query_assurance_status_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_id` int(11) NOT NULL,
  `changes` text NOT NULL,
  `insertuser` varchar(10) NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE  `assist_demo001_qry_assurance` CHANGE  `attachment`  `attachment` TEXT NOT NULL;

ALTER TABLE  `assist_demo001_qry_action_assurance` CHANGE  `attachment`  `attachment` TEXT NOT NULL;


INSERT INTO `dev_idemo001`.`assist_demo001_qry_menu` (`id`, `parent_id`, `name`, `client_name`, `description`, `folder`, `viewby`, `insertdate`, `insertuser`) VALUES (NULL, '0', 'Admin', 'Admin', 'Admin', 'admin', 'admin', CURRENT_TIMESTAMP, '0002');

INSERT INTO `dev_idemo001`.`assist_demo001_qry_menu` (`id`, `parent_id`, `name`, `client_name`, `description`, `folder`, `viewby`, `insertdate`, `insertuser`) VALUES (NULL, '25', 'Edit', 'Edit', 'Edit', 'edit', 'edit', CURRENT_TIMESTAMP, '0002'), (NULL, '25', 'Update', 'Update', 'Update', 'update', 'Update', CURRENT_TIMESTAMP, '0002');


ALTER TABLE  `assist_alf0003_qry_query_register` ADD  `query_source` INT NOT NULL AFTER  `type`

INSERT INTO `dev_ialf0003`.`assist_alf0003_qry_header_names` (`id`, `name`, `ignite_terminology`, `client_terminology`, `ordernumber`, `type`, `active`, `insertdate`, `insertuser`) VALUES (NULL, 'query_source', 'Query Source', 'Query Source', '2', 'query', '1', CURRENT_TIMESTAMP, '0002');

CREATE TABLE IF NOT EXISTS `assist_alf0003_qry_query_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertuser` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `assist_alf0003_qry_query_source_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setup_id` int(11) NOT NULL,
  `changes` text NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertuser` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO `dev_ialf0003`.`assist_alf0003_qry_menu` (`id`, `parent_id`, `name`, `client_name`, `description`, `folder`, `viewby`, `insertdate`, `insertuser`) VALUES (NULL, '2', 'My Profile', 'My Profile', 'My Profile', 'myprofile', 'myprofile', CURRENT_TIMESTAMP, '0002'), (NULL, '25', 'Notifications', 'Notifications', 'Notifications', 'notifications', 'Notifications', CURRENT_TIMESTAMP, '0002');

ALTER TABLE  `assist_alf0003_qry_query_register` ADD  `remind_on` DATE NOT NULL AFTER  `query_deadline_date`


CREATE TABLE IF NOT EXISTS `assist_alf0003_qry_summary_notifications_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_id` int(11) NOT NULL,
  `changes` text NOT NULL,
  `insertuser` varchar(10) NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `assist_alf0003_qry_summary_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recieve_email` int(11) NOT NULL,
  `recieve_when` varchar(255) NOT NULL,
  `recieve_what` varchar(255) NOT NULL,
  `recieve_day` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `inserdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertuser` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `assist_alf0003_qry_user_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification` varchar(255) NOT NULL,
  `risk_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `recieve_email` int(11) NOT NULL,
  `recieve_when` varchar(255) NOT NULL,
  `recieve_what` varchar(255) NOT NULL,
  `user_id` text NOT NULL,
  `recieve_day` varchar(255) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `inserdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertuser` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `assist_alf0003_qry_user_notifications_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_id` int(11) NOT NULL,
  `changes` text NOT NULL,
  `insertuser` varchar(10) NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `assist_alf0003_qry_reminders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_days` int(11) NOT NULL,
  `query_days` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertuser` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

