<?php
/* requires $sdb as object of ASSIST_DB class => created in main/stats.php */
if(isset($sdb))
{
    $now    = strtotime(date("d F Y"));
    $future = date("d-M-Y", ($today+($next_due*24*3600)));
    $sql = "SELECT 
		    A.deadline AS taskdeadline,
		    A.status,
		    count(A.id) as c 
		    FROM assist_".$cmpcode."_".$modref."_actions A 
		    INNER JOIN assist_".$cmpcode."_".$modref."_query_register Q ON Q.id = A.risk_id
		    WHERE STR_TO_DATE(A.deadline, '%d-%b-%Y') < STR_TO_DATE('".$future."', '%d-%b-%Y') 
		    AND A.action_owner = '$tkid' 
		    AND A.status <> 3 AND Q.active & 4 = 4 AND A.active = 1
		    GROUP BY A.deadline";
    $rows = $sdb->mysql_fetch_all($sql);
    foreach($rows as $row) {
	    $d = strtotime($row['taskdeadline']);
	    if($d < $now) {
		    $count['past']+=$row['c'];
	    } elseif($d==$now) {
		    $count['present']+=$row['c'];
	    } else {
		    $count['future']+=$row['c'];
	    }
    }
}
?>
