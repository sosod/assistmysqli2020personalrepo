$(function(){
	
	$(".textcounter").textcounter({maxLength:100});
	
	if( $("#risktype_id").val() == undefined)
	{
		RiskType.get();		
	}
	
	$("#add_risktype").click( function() {
		RiskType.save();
		return false;
	})
	
	$("#edit_risktype").click(function(){
		RiskType.edit();
		return false;
	})
				
	$("#changes_risktype").click(function() {
		RiskType.change();
		return false;
	})
	
	
	
});

var RiskType		= {
		
		get				: function()
		{
			$.getJSON( "controller.php?action=getRiskType", function( data ) {
				$.each( data, function( index, val){
					RiskType.display( val )					
				});				
			})
		} , 
		
		save			: function()
		{
			var shortcode 	= $("#risktype_shortcode").val();
			var type 		= $("#risktype").val();
			var description	= $("#risktype_descri").val();
			
			if ( shortcode == "" ) {
				jsDisplayResult("error", "error", "Please the short code for this risk type");
				return false;
			} else if( type == "") {
				jsDisplayResult("error", "error", "Please enter the risk type");
				return false;
			} else {
			jsDisplayResult("info", "info", "Saving risk type  ...  <img src='../images/loaderA32.gif' />");
				$.post( "controller.php?action=newRiskType", 
					   { 
						   shortcode	: shortcode, 
						   name			: type, 
						   description	: description 
					   }, function( retData ) {
					   if( retData > 0 ) {
						   
						 $("#risktype_shortcode").val("");  
						 $("#risktype").val("");
						 $("#risktype_descri").val("");
						 jsDisplayResult("ok", "ok", "Risk type succesfully saved ...");
						 $("#risk_type_table")
						  .append($("<tr />",{id:"tr_"+retData})
							.append($("<td />",{html:retData}))
							.append($("<td />",{html:shortcode}))
							.append($("<td />",{html:type}))
							.append($("<td />",{html:description}))
							.append($("<td />")
							  .append($("<input />",{type:"submit", value:"Edit", id:"typeedit_"+retData, name:"typeedit_"+retData }))
							  .append($("<input />",{type:"submit", value:"Del", id:"typedel_"+retData, name:"typedel_"+retData }))			  
							 )
							.append($("<td />",{html:"<b>Active</b>"}))
							.append($("<td />")
									.append($("<input />",{type:"submit", id:"change_"+retData, name:"change_"+retData, value:"Change status"}))
							)
						  )

						  
							$("#typedel_"+retData).live( "click", function() {
								if( confirm(" Are you sure you want to delete this risk type ")) {														  
								jsDisplayResult("info", "info", "Deleting  . . .  <img src='../images/loaderA32.gif' />");
									$.post( "controller.php?action=changetypestatus", 
										   {id		: retData,
											active  : 2
										   }, function( typDelData ) {
										if( typDelData == 1) {
											$("#tr_"+retData).fadeOut();
											jsDisplayResult("ok", "ok", "Query Type deleted");
										} else if( typDelData == 0) {
											$("#tr_"+retData).fadeOut();
											jsDisplayResult("info", "info", "No change was made to the query type");
										} else {
											jsDisplayResult("error", "error", "Error saving , please try again");
										}										 
									});
								}
								return false;										  	
							});					
							
							$("#typeedit_"+retData).live("click", function() {
								document.location.href = "edit_risk_type.php?id="+retData;
								return false;										  
							});
							
						  $("#change_"+retData).live( "click", function() {
							document.location.href = "change_type_status.php?id="+retData;
							return false;										  
						  });
						 
				   } else {
						jsDisplayResult("error", "error", "Error saving risk type, please try again ...");
					}
				  
				});
			}
		} ,
		
		change			: function()
		{
			jsDisplayResult("info", "info", "Updating risk type  ...  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=changetypestatus",
				   { id		: $("#risktype_id").val() ,
				   	 active : $("#type_statuses :selected").val()
				   }, function( typDedeactData ) {
				if( typDedeactData == 1) {
					jsDisplayResult("ok", "ok", "Risk Type status updated . . . ");
				} else if( typDedeactData == 0) {
					jsDisplayResult("info", "info", "No change was made to the risk type status . . . ");
				} else {
					jsDisplayResult("info", "info", "Error saving , please try again . . . ");
				}										 
			})
		} , 
		
		
		edit			: function()
		{
			var shortcode 	= $("#risktype_shortcode").val();
			var type 		= $("#risktype").val();
			var description	= $("#risktype_descri").val();
			
			if ( shortcode == "" ) {
				jsDisplayResult("error", "error", "Please the short code for this risk type");
				return false;
			} else if( type == "") {
				jsDisplayResult("error", "error", "Please enter the risk type");
				return false;
			} else {
				jsDisplayResult("info", "info", "Updating risk type  ...  <img src='../images/loaderA32.gif' />");
				$.post( "controller.php?action=updateRiskType", 
			   { 
				   id			: $("#risktype_id").val(), 
				   shortcode	: shortcode, 
				   name			: type, 
				   description	: description 
			   }, function( retData ) {
					if( retData == 1 ) {
						jsDisplayResult("ok", "ok", "Risk type successfully updated . . . ");
					} else if( retData == 0 ){
						jsDisplayResult("info", "info", "Risk type not changed . . . ");
					} else {
						jsDisplayResult("error", "error", "Error updating the risk type . . . ");
					}
				});
			}
		} , 
		
		display			: function( val )
		{
			$("#risk_type_table")
			  .append($("<tr />",{id:"tr_"+val.id})
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.shortcode}))
				.append($("<td />",{html:val.name}))
				.append($("<td />",{html:val.description}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"typeedit_"+val.id, name:"typeedit_"+val.id }))
				  .append($("<input />",{type:"submit", value:"Del", id:"typedel_"+val.id, name:"typedel_"+val.id }))			  
				)
				.append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", id:"change_"+val.id, name:"change_"+val.id, value:"Change Status"}))		  
				)
			  )
			$("#typeedit_"+val.id).live( "click", function() {
				document.location.href = "edit_risk_type.php?id="+val.id;
				return false;										  
			});
			
			$("#change_"+val.id).live( "click", function() {
				document.location.href = "change_risk_type_status.php?id="+val.id;
				return false;										  
			});
			
			$("#typedel_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this risk type ")) {														  
				jsDisplayResult("info", "info", "Deleting  . . .  <img src='../images/loaderA32.gif' />");
					$.post( "controller.php?action=changetypestatus", 
						   {id		:	val.id,
						   	active  : 2
						   }, function( typDelData ) {
						if( typDelData == 1) {
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("ok", "ok", "Query Type deleted");
						} else if( typDelData == 0) {
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("info", "info", "No change was made to the query type");
						} else {
							jsDisplayResult("error", "error", "Error saving , please try again");
						}										 
					});
				}
				return false;										  
			});
			
			$("#typeactive_"+val.id).live( "click", function() {
			jsDisplayResult("info", "info", "Updating  . . .  <img src='../images/loaderA32.gif' />");
				$.post( "controller.php?action=typeactivate", {id:val.id}, function( typDeactData ) {
					if( typDeactData == 1) {
						jsDisplayResult("ok", "ok", "Query Type activated");
					} else if( typDeactData == 0) {
						jsDisplayResult("info", "info", "No change was made to the query type");
					} else {
						jsDisplayResult("error", "error", "Error saving , please try again");
					}										 
				})															 
				return false;										  
			});
			
		}
		
		
		
}