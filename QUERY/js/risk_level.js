// JavaScript Document
$(function(){
	
	$(".textcounter").textcounter();
	
	$("#add_risklevel").click(function(){
		RiskLevel.save();
		return false;
	});	
	
	$("#edit_risklevel").click(function(){
		RiskLevel.edit();
		return false;
	});	
	$("#changes_risklevel").click(function(){
		RiskLevel.update();
		return false;
	});		

	if( $("#risklevel_id").val() == undefined) {
		RiskLevel.get();
	}
	$("#cancel_risklevel").click(function(){
		history.back();
		return false;
	})
		   
});

var RiskLevel	= {
	
	get	: function()
	{
		$.post("controller.php?action=getRiskLevels", function( data ){
			console.log(data);
			$.each( data, function( index, level){
				RiskLevel.display( level );				   				   
			})												   
																		   
		}, "json");
	} , 
	
	save	: function()
	{
		var data = {};
		data.shortcode  = $("#risklevel_shortcode").val();
		data.name		= $("#risklevel").val();
		data.description= $("#risklevel_description").val();
		data.status 	= 1;

		if( data.shortcode == ""){
			jsDisplayResult("error", "error", "Please enter the shortcode");
			return false;
		} else if(data.name == "") {
			jsDisplayResult("error", "error", "Please enter the risk level name");
			return false;
		} else if(data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");			
			return false;
		} else {
			jsDisplayResult("info", "info", "Saving  . . .  <img src='../images/loaderA32.gif' />");
			$.post("controller.php?action=saveRiskLevel", data, function( response ){
				if( response > 0){
					$("#risklevel_shortcode").val("");
					$("#risklevel").val("");
					$("#risklevel_description").val("");
					data.ref = response;
					jsDisplayResult("ok", "ok", "Risk level has been saved");
					RiskLevel.display( data );	
				} else {
					jsDisplayResult("error", "error", "Error occured saving the risk level");			
				}												   
			}, "json");		
			
		}
	} , 
	
	change_status 	: function()
	{
			
	} , 
	
	edit			: function()
	{
		var data = {};
		data.shortcode  = $("#risklevel_shortcode").val();
		data.name		= $("#risklevel").val();
		data.description= $("#risklevel_description").val();
		data.status 	= $("#risklevel_status").val();
		data.id 		= $("#risklevel_id").val();		

		if( data.shortcode == ""){
			jsDisplayResult("error", "error", "Please enter the shortcode");
			return false;
		} else if(data.name == "") {
			jsDisplayResult("error", "error", "Please enter the risk level name");
			return false;
		} else if(data.description == ""){
			jsDisplayResult("error", "error", "Please enter the description");			
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating  . . .  <img src='../images/loaderA32.gif' />");
			$.post("controller.php?action=updateRiskLevel", data, function( response ){
				if( response == 1){
					data.ref = response;
					jsDisplayResult("ok", "ok", "Risk level has been updated");
					RiskLevel.display( data );	
				} else if(response == 0){
				    jsDisplayResult("info", "info", "No changes were made to the risk level");			
				}else {
					jsDisplayResult("error", "error", "Error occured saving the risk level");			
				}												   
			}, "json");		
			
		}
	} , 
	
	update			: function()
	{
			var data = {};
			data.status 	= $("#level_statuses").val();
			data.id 		= $("#risklevel_id").val();			
			jsDisplayResult("info", "info", "Updating  . . .  <img src='../images/loaderA32.gif' />");
			$.post("controller.php?action=changelevelstatus", data, function( response ){
				if(response == 1)
				{
					data.ref = response;
					jsDisplayResult("ok", "ok", "Risk level has been updated");
					RiskLevel.display( data );	
				} else if(response == 0){
				   jsDisplayResult("info", "info", "No changes made to the risk level");			
				} else {
					jsDisplayResult("error", "error", "Error occured saving the risk level");			
				}												   
			}, "json");	
	} , 
	
	display	: function( level )
	{
		var ref = (level.ref == undefined ? level.id : level.ref);
		$("#risk_level_table")
		 .append($("<tr />",{id:"row_"+ref})
			.append($("<td />",{html:ref}))		   
			.append($("<td />",{html:level.shortcode}))				   
			.append($("<td />",{html:level.name}))
			.append($("<td />",{html:level.description}))
			.append($("<td />")
				.append($("<input />",{type:"submit", name:"edit_"+ref, id:"edit_"+ref, value:"Edit"}))		
				.append($("<input />",{type:"submit", value:"Del", id:"del_level_"+ref, name:"del_level_"+ref}))				
			 )				   			
			.append($("<td />",{html:"<b>"+((level.status & 1) == 1 ? "Active" : "Inactive")+"</b>"})		  
			 )				   			
			.append($("<td />")
				.append($("<input />",{type:"submit", name:"change_"+ref, id:"change_"+ref, value:"Change Status"}))
			 )				   						
		 )
		 
		 if((level.status & 4) == 4)
		 {
		 	$("#del_level_"+ref).disabled("disabled", "disabled");
		 	$("#change_"+ref).disabled("disabled", "disabled");
		 }

		 $("#edit_"+ref).live("click",function(){
			document.location.href = "edit_risk_level.php?id="+ref;						   
			return false;							   
		})
		 
		 $("#change_"+ref).live("click",function(){
			document.location.href = "change_level_status.php?id="+ref;						   
			return false;							   
		})		 
		
		$("#del_level_"+ref).live( "click" ,function(){
			var message = $("#risk_level_message");
			if( confirm(" Are you sure you want to delete this risk level")) {
				jsDisplayResult("info", "info", "Delete  . . .  <img src='../images/loaderA32.gif' />");
				$.post("controller.php?action=deActivateLevel", { id:ref, status : 2 }, function( delData ) {
					if( delData == 1){
						jsDisplayResult("ok", "ok", "Risk level deleted");
						$("#row_"+ref).fadeOut();
					} else if( delData == 0){
						jsDisplayResult("info", "info", "No change was made to the status");
					} else {
						jsDisplayResult("error", "error", "Error saving , please try again");
					}		   
				});				
			}
			return false;											 
		});		
	}
	
	
	
	
	
}

