<?php
$scripts = array( 'query.js','menu.js', 'ajaxfileupload.js' );
$styles = array();
$page_title = "Add New Query";
require_once("../inc/header.php");
$naming 	= new Naming();
$rowNames   = $naming -> rowLabel();
function nameFields( $fieldId , $defaultName ){
	global $naming, $rowNames;
	if( isset( $rowNames[$fieldId] )) {
		echo $rowNames[$fieldId];
	}	else {
		echo $defaultName;
	}
}
?>
<div>
<h4>Add a New Query</h4>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form id="new-risk-form" method="post" enctype="multipart/form-data">
<table width="" border="0" id="risk_new">
 <tr>
        <th><?php nameFields('query_item','Query Item'); ?>:</th>
        <td><span id="idrisk"></span></td>
      </tr>
      <tr>
        <th><?php nameFields('query_reference','Query Reference'); ?>:</th>
        <td><input type="text" id="query_reference" name="query_reference" value="" /></td>
      </tr>      
      <tr>
        <th><?php nameFields('query_type','Query Type'); ?>:</th>
        <td>
            <select id="query_type" name="query_type">
                <option value=""><?php echo $select_box_text ?></option>
            </select>
        </td>
      </tr>
      <tr>
        <th><?php nameFields('query_category','Query Category'); ?>:</th>
        <td>
            <select id="query_category" name="query_category">
                <option value=""><?php echo $select_box_text ?></option>
            </select>    
        </td>
      </tr>
      <tr>
        <th><?php nameFields('query_date','Query Date'); ?>:</th>
        <td><input type="text" name="query_date" id="query_date" class="datepicker" /></td>
      </tr>      
      <tr>
        <th><?php nameFields('query_description','Query Description'); ?>:</th>
        <td><textarea name="query_description" id="query_description"></textarea></td>
      </tr>
      <tr>
        <th><?php nameFields('query_background','Background Of Query'); ?>:</th>
        <td><textarea name="query_background" id="query_background"></textarea></td>
      </tr>
    
      <?php 
        //if ($setups['risk_is_to_directorate'] == 1 ) { 
      ?>
     <tr>
        <th><?php nameFields('query_owner','Directorate responsible'); ?>:</th>
        <td>
        <select id="directorate" name="directorate" class="responsibility">
                <option value=""><?php echo $select_box_text ?></option>
         </select>        
        </td>
    <!--  </tr>
      <?php
      //}	else if ($setups['risk_is_to_subdirectorate'] == 1) {
      ?>
                     <tr>
        <th><?php nameFields('','Sub Directorate Responsible'); ?>:</th>
        <td>
        <select id="subdirectorate" name="subdirectorate" class="responsibility">
                <option value="">--sub directorate--</option>
         </select>  

        </td>
      </tr>
      <?php 
     // } else {
      ?>
      <tr>
        <th><?php nameFields('risk_owner','Responsibility of risk'); ?>:</th>
        <td>
        <select id="risk_user_responsible" name="risk_user_responsible" class="responsibility">
                <option value="">--user responsible--</option>
         </select>        
        </td>
      </tr> -->
      <?php
        //}
      ?>
      <tr>
        <th><?php nameFields('','Attach a document'); ?>:</th>
        <td>
        	<input id="file_upload_<?php echo time(); ?>" name="file_upload" type="file" onChange="fileUploader(this)" />
        	<input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="16000"  />
        	<input type="hidden" id="actionname" name="actionname" value="saveAttachement"  />
        	<span id="loading"></span>
        &nbsp;
        </td>
      </tr> 
	  <tr class="more_details">
        <th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
        <td>
        	<select id='financial_exposure' name='financial_exposure'>
            	<option value=""><?php echo $select_box_text ?></option>
        	</select>
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('monetary_implication','Monetary Implication'); ?>:</th>
        <td><input type='text' id='monetary_implication' name='monetary_implication' value='' /></td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('risk_level','Risk Level'); ?>:</th>
        <td>
        <select id="risk_level" name="risk_level">
                <option value=""><?php echo $select_box_text ?></option>
         </select>            
        </td>
      </tr>      
      <tr class="more_details">
        <th><?php nameFields('risk_type','Risk Type'); ?>:</th>
        <td>
        <select id="risk_type" name="risk_type">
                <option value=""><?php echo $select_box_text ?></option>
         </select>            
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('risk_detail','Risk Detail'); ?>:</th>
        <td><input type='text' id='risk_detail' name='risk_detail' value='' /></td>
      </tr>      
      <tr class="more_details">
        <th><?php nameFields('finding','Add Finding'); ?>:</th>
        <td>
        	<input type='text' id='finding' name='finding' value='' class="find"/>
            <input type="submit" name="add_another_finding" id="add_another_finding" value="Add Another"  /> 
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('recommendation','Add Recommendation'); ?>:</th>
        <td>
            <input type="text" name="recommendation" id="recommendation" value='' class="recommend" />
            <input type="submit" name="add_another_recommendation" id="add_another_recommendation" value="Add Another"  />         
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('client_response','Add Client Response'); ?>:</th>
        <td><input type='text' name='client_response' id='client_response' value='' class="clientresponse" />
        <input type="submit" name="add_another_clientresponse" id="add_another_clientresponse" value="Add Another"  />                 
        </td>
      </tr>      
      <tr>
        <td>
            <!-- <?php if($setups['actions_aplied_to_risks'] == 1 ) { ?>
                <input type="submit" name="show_add_action" id="show_add_action" value="Add Action " />
            <?php } ?>  -->                      
       </td>
       <td>
		<span style='float:left'><input type="submit" name="add_query" id="add_query" value="Add Query " class="isubmit" /></span>
       	<span style='float:right'><input type="submit" name="more_detail" id="more_detail" value="More Detail" class="iinform" /></span>
       </td>
      </tr>
   </table>
</form>
</div>

