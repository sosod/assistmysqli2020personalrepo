// JavaScript Document
$(function(){
		   

	$.get( "controller.php?action=getAAction", { id:$("#action_id").val() }, function( actionData ){
		$("#action_information_table")
			.append($("<tr />")
				.append($("<th />",{html:"Action #:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.id}))		  				
			)
			.append($("<tr />")
				.append($("<th />",{html:"Action:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.action}))		  				
			)
			.append($("<tr />")
				.append($("<th />",{html:"Deliverable:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.deliverable}))		  		  
			)
			.append($("<tr />")
				.append($("<th />",{html:"Action Owner:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.tkname}))		  
			)
			.append($("<tr />")
				.append($("<th />",{html:"Time Scale:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.timescale}))		  		  
			)
			.append($("<tr />")
				.append($("<th />",{html:"Deadline:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.deadline}))		  		  
			)
			.append($("<tr />")
				.append($("<th />",{html:"Status:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.status}))		  		  
			)
			.append($("<tr />")
				.append($("<th />",{html:"Progress:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:(actionData.progress=="" ? "0" : actionData.progress)+"%"}))		  		  
			)
			.append($("<tr />")
				.append($("<th />",{html:"Remind On:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.remindon}))		  		  
			)
	},"json");
		
		
	$.get( "controller.php?action=getActionAssurance", {id : $("#action_id").val() }, function( actionAssuranceData ){															
		if( $.isEmptyObject( actionAssuranceData ) ){
		} else{	
		$.each( actionAssuranceData, function(index, val){
			$("#list_assurance_action_table")
			.append($("<tr />")
				.append($("<td />"))		  
				.append($("<td />"))		  
				.append($("<td />"))		  
				.append($("<td />"))		  
				.append($("<td />"))		  
				.append($("<td />"))		  
				.append($("<td />"))	
				)								 
		});
		}	
		$("#list_approval_action_table")
		.append($("<tr />")
			.append($("<td />",{colspan:"7"})
			  .append($("<input />",{type:"submit", name:"showadd_new_action_approval", id:"showadd_new_action_approval", value:"Add New"}))		
				)		  
			)
		$("#showadd_new_action_assurance").live( "click", function(){
			$("#new_action_approval").show();
			return false;
		});
		
	}, "json");
	
	$("#add_newAction_approval").live( "click", function(){

		var message 	= $("#newapproval_message").slideDown()
		var response  	= $("#approval_response").val();
		var signoff  	= $("#approval_signoff :selected").val();
		var attachment 	= $("#approval_attachment").val();		
		var notification= ( $("#approval_notification").is(":checked") ? "on" : "");
		
		if( response == "" ) {
			message.html(" Please enter the response for the action")
			return false;
		} else if( signoff == "") {
			message.html(" Please select the sign off for the action")
			return false;
		} else {
			message.html( "Saving risk...  <img src='../images/loaderA32.gif' />" )
			$.post( "controller.php?action=newApprovalAction", {
				   id			:	$("#action_id").val(), 
				   response		:	response, 
				   signoff		: 	signoff,
				   attachment	: 	attachment,
				   notification :   notification
				   }, function( retApproval ){
					if( retApproval >0 ) {
						message.addClass("ui-state-highlight").html("Action approval saved ...")			  
					} else if( retApproval == 0){
						message.addClass("ui-state-highlight").html("Action was not  saved ...")			  
					} else {
						message.addClass("ui-state-error").html("There was an error saving , please try again")			  	
					}
				  })	
		}
		return false;											 
	});
		/**
		Add another browse button to attach files
	**/
	$("#attach_another").click(function(){
		$(this)
		.parent()
		.prepend($("<br />"))		
		.prepend($("<input />",{type:"file", name:"attachment"}).addClass('attach'))
		return false;									
	});
	
	
	$("#cancel_newAction_approval").click(function(){
		history.back();
		return false;
	});
	
})