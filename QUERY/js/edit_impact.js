// JavaScript Document
$(function(){
	$("#edit_impact").click(function(){
		var message 	= $("#impact_message")						
		var rating_from = $("#imprating_from").val();
		var rating_to 	= $("#imprating_to").val();
		var assessment	= $("#impact_assessment").val();
		var definition  = $("#impact_definition").val();
		var color 		= $("#impact_color").val();
	
		if ( rating_from == "") {
			message.html("Please enter the rating feom for this impact")
			return false;
		} else if( rating_to == "" ) {
			message.html("Please enter the rating to for this impact");
			return false;
		} else if ( assessment == "" ) {
			message.html("Please enter the asssessment for this impact");
			return false;
		} else if( definition == "" ) {
			message.html("Please enter the definition for this impact");
			return false;			
		} else {
			message.html("");
			$.post( "controller.php?action=updateImpact", 
				   {
					  id		: $("#impact_id").val(),
					  from		: rating_from,
					  to		: rating_to,
					  assmnt	: assessment,
					  dfn		: definition,
					  clr		: color
					}
				   , function( retData ) {
					if( retData == 1 ) {
						message.html("Impact successifully updated .. ");						
					} else if( retData == 0 ){
						message.html("Impact not changed");
					} else {
						message.html("Error updating the impact");
					}
			},"json")	
		}
		return false;					 
	});	
	
	$("#change_impact").click(function(){
		var message 	= $("#impact_message")
		$.post("controller.php?action=changeImpstatus",
			   {
					id 		: $("#impact_id").val(),
					status  : $("#impact_status :selected").val() 
				},
			   function( retData ) {
				if( retData == 1 ) {
					message.html("Impact status updated succesifully")	
				} else if( retData == 0 ) {
					message.html("No change in status was made")					
				} else {
					message.html("Error updating status")					
				}											  
		});
		return false;
	});
	
	$("#cancel_impact").click(function(){
		history.back();
		return false;								   
	});
})