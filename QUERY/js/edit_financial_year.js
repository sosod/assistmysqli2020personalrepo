// JavaScript Document
$(function(){
	 /**
      Pops a jquery date picker
    **/
    $(".date").live("focus",function(){
      $(this).datepicker({ changeMonth : true, changeYear : true, dateFormat : "d-MM-yy" });
    });
	
	$("#save_changes_fin_year").click(function(){
		var message = $("#editfinancial_year_message")
		$.post( "controller.php?action=editFinancialYear",  {
			id  			: $("#fini_year_id").val(),
			last_day		: $("#edit_last_year").val(),
			start_date		: $("#edit_start_date").val(),
			end_date		: $("#edit_end_date").val() 
		},
			function( retFinancial ){
				if( retFinancial == 1 ){
					message.show().html( "Financial year successifully updated " )	
				} else if( retFinancial == 0 ) {
					message.show().html("There was no made to the financial years");	
				}else {
					$.each(retFinancial, function( index, val){
						message.show().html("Error type :"+index+"\r\n "+val.error);
					});
				}
		},"json")
		return false;
	});
	
	$("#cancel_changes_fin_year").click(function(){
		history.back();
		return false;										 
	});
	
});