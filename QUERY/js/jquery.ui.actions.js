// JavaScript Document
$.widget("ui.actions", {
	options : {
		paging				: true,	
		limit				: 10,
		approved_start 		: 0,
		approved_current	: 1,
		approved_pages		: 10,
		approved_limit		: 10,		
		approved_total		: 10,				
		tobeapproved_start	: 0,
		tobeapproved_current: 1,
		tobeapproved_pages	: 10,
		tobeapproved_limit	: 10,
		tobeapproved_total	: 10		
	} , 
	
	_init 					: function(){
		this._getUnApprovedActions();
		this._getApprovedActions();
		this._getUsers();
	} , 
	
	_create	   				: function(){
		$this = this;
		$("#test").append($("<table />",{ border:"5", width:"100%"})
			.append($("<tr />")
			  .append($("<td />",{html:"<b>Actions to be approved</b>"}))
			  .append($("<td />",{html:"<b>Actions Approved</b>"}))			  
			)
		   .append($("<tr />")
				.append($("<td />")
					.append($("<select />",{id : "to_users"})
						.addClass("users")
						.append($("<option />",{text:"--select user--", value:""}))		  
					)	
				  .append($("<span />",{id:"toactionsfor", html:"<em>You are viewing all actions</em>"}))
				)
				.append($("<td />")
					.append($("<select />",{id : "approved_users"})
						.addClass("users")
						.append($("<option />",{text:"--select user--", value:""}))		  
					)
				  .append($("<span />",{id:"approvedactionsfor", html:"<em>You are viewing all approved actions</em>"}))					
				)				
			)
		  .append($("<tr />")
			 .append($("<td />",{valign:"top", width:"50%"})
				.append($("<table />",{id:"table_toapproved", width:"100%"}).addClass("noborder"))	   					   
			  )		
			 .append($("<td />",{valign:"top", width:"50%"})
				.append($("<table />",{id:"table_approved", width:"100%"}).addClass("noborder"))	   
			  )					 
		   )
		)
	
		$("#to_users").live("change", function(){
			$this._getUnApprovedActions();
			$("#toactionsfor").html("").append($("<em>You are viewing actions for actions for "+$("#to_users :selected").text()+"</em>").css({"color":"red", "font-size":"9"}))
			return false;								   
		});
		
		$("#approved_users").live("change", function(){
			$this._getApprovedActions();													 
			$("#approvedactionsfor").html("").append($("<em>You are viewing actions for actions for "+$("#approved_users :selected").text()+"</em>").css({"color":"red", "font-size":"9"}))			
			return false;								   
		});		
		$("em").css({"color":"red", "font-size":"9"})
	} ,
	
	_displayPaging			: function( ref, table, cols, total){
		$this = this;
		var pages;
		if( total.total%$this.options[ref+"_limit"] > 0){
			pages   = Math.ceil(total.total/$this.options[ref+"_limit"]); 
		} else {
			pages   = Math.floor(total.total/$this.options[ref+"_limit"]); 
		}
		
		//console.log( "When were are create the pager for "+ref+" => "+$this.options[ref+"_current"] );
		$("#"+table)
			.append($("<tr />")
			  .append($("<td />")
				 .append($("<input />",{type:"submit", name:"first_"+ref, id:"first_"+ref, value:"|<", disabled:($this.options[ref+"_current"] < 2 ? 'disabled' : '' )}))		
				 .append($("<input />",{type:"submit", name:"previous_rec_"+ref, id:"previous_rec_"+ref, value:"<", disabled:($this.options[ref+"_current"] < 2 ? 'disabled' : '' )}))					 
			   )
			  .append($("<td />",{html:"Page "+$this.options[ref+"_current"] +" / "+pages+"of "+total.total+" records " , align:"center"}))
			  .append($("<td />")
				 .append($("<input />",{type:"submit", name:"next_"+ref, id:"next_"+ref, value:">", disabled:(($this.options[ref+"_current"] ==pages || pages == 0) ? 'disabled' : '' )}))						
				 .append($("<input />",{type:"submit", name:"last_"+ref, id:"last_"+ref, value:">|", disabled:(($this.options[ref+"_current"] ==pages || pages == 0) ? 'disabled' : '' )}))				 
			   )
			  .append($("<td />", {colspan:cols-1})						
			   )			  
			)
			$("#next_"+ref).live("click", function(){
				$this._getNext( $this ,ref );
				return false;
			});
			$("#last_"+ref).live("click",  function(){
				$this._getLast( $this , ref);
				return false;
			});
			$("#previous_rec_"+ref).live("click", function(){
					//console.log( ref)
				 $this._getNext( $this ,ref );
				//$this._getPrevious( $this, ref );\
				//$this.options[ref+"_current"]  = parseFloat( $this.options[ref+"_current"] ) - 1;
				//console.log("Current is => ")
				//console.log( $this.options[ref+"_current"] )
				return false;
			});
			$("#first_"+ref).live("click",  function(){
				$this._getFirst( $this, ref );
				return false;
			});	
	} , 
	_getNext  			: function( $this, ref ) {
		//console.log("Next called")
		$this.options[ref+"_current"] = parseFloat( parseFloat($this.options[ref+"_current"]) + 1 );	
		$this.options[ref+"_start"]   = parseFloat( $this.options[ref+"_current"] - 1) * parseFloat( $this.options[ref+"_limit"] );
		if( ref == "approved"){
			$this._getApprovedActions();
		} else {
			$this._getUnApprovedActions();
		}
	},	
	_getLast  			: function( $this, ref ) {
		//var rk 			   = this;
		$this.options[ref+"_current"] =  Math.ceil( parseFloat( $this.options[ref+"_total"] )/ parseFloat( $this.options[ref+"_limit"] ));
		$this.options[ref+"_start"] = parseFloat($this.options[ref+"_current"]-1) * parseFloat( $this.options[ref+"_limit"] );			
		if( ref == "approved"){
			$this._getApprovedActions();
		} else {
			$this._getUnApprovedActions();
		}
	},	
	_getPrevious    	: function( $this, ref ) {
		//console.log( ref )
		//console.log( "Current "+$this.options[ref+"_current"] )
		$this.options[ref+"_current"]  = parseFloat( $this.options[ref+"_current"] ) - 1;
		//console.log( $this.options[ref+"_current"] )		
		$this.options[ref+"_start"] 	= ($this.options[ref+"_current"]-1)*$this.options[ref+"_limit"];
		if( ref == "approved"){
			$this._getApprovedActions();
		} else {
			$this._getUnApprovedActions();
		}
	},	
	_getFirst  			: function( $this ) {
		$this.options[ref+"_current"]  = 1;
		$this.options[ref+"_start"]    = 0;
		if( ref == "approved"){
			$this._getApprovedActions();
		} else {
			$this._getUnApprovedActions();
		}
	},	
		
	_getApprovedActions 	: function(){
		$this 	 = this;
		var user = $("#approved_users :selected").val();
		$.getJSON("controller.php?action=getApprovedActions",{ 
				 	 user	: user ,
					 start	: $this.options.approved_start,
					 limit	: $this.options.approved_limit				 
				  }, function( apprActions ){					  
			$("#table_approved").html("");
			$this.options["approved_total"] = apprActions.total.total
			$this._displayPaging( "approved", "table_approved", 5, apprActions.total )
			$this._displayActions( apprActions.data, false, "table_approved");																
		});
	} , 
	
	_getUnApprovedActions 	: function(){
		$this 	 = this;
		var user = $("#to_users :selected").val();		
		$.getJSON("controller.php?action=getActionToApprove", {
				  	user	: user,
					start	: $this.options.tobeapproved_start,
					limit	: $this.options.tobeapproved_limit					
				  }, function( actionsToApprove ){	
			$("#table_toapproved").html("");		
			//console.log( "Total => "+actionsToApprove.total.total );
			$this.options["tobeapproved_total"] = actionsToApprove.total.total			
			$this._displayPaging( "tobeapproved", "table_toapproved", 6 , actionsToApprove.total)			
			$this._displayActions( actionsToApprove.data, true, "table_toapproved");												   
		});
	} ,
	 _displayActions	 	: function( actions, toApprove , table){
		 $this = this;
		$("#"+table)															   
		.append($("<tr />")
			.append($("<th />",{html:"Action #"}))
			.append($("<th />",{html:"Action Description"}))
			.append($("<th />",{html:"Status"}))
			.append($("<th />",{html:"Progress"}))
			.append($("<th />",{html:"Attachment"}))
			.append($("<th />",{html:"&nbsp;"}).css({"display":( toApprove == true ? "block" : "none")}))				
		)				 
		$.each( actions, function( index, action ){		   
			$("#"+table)															   
			.append($("<tr />",{id:"tr_"+action.id})
				.append($("<td />",{html:action.id}))
				.append($("<td />",{html:action.action}))
				.append($("<td />",{html:action.status}))
				.append($("<td />",{html:(action.progress=="" ? "0" : action.progress)+"%"}))
				.append($("<td />",{html:"&nbsp;&nbsp;"}))
				.append($("<td />")
					.css({"display":( toApprove == true ? "inline" : "none")})
					.append($("<input />",{type:"submit", name:"approve_"+action.id, id:"approve_"+action.id, value:"Approve"}).addClass("iinform"))	  
				)				
			)	
			
			$("#approve_"+action.id).live("click", function(){
				if( $this._isValidToApprove( action.progress ) ) {
					$this._processApprove( action );	
				} else {
					return alert("This action cannot be approved, it must be completed/addressed for it to be approved");	
				}
				return false;								   
			});
		});
	} , 

	_isValidToApprove		: function( progress ){
		if( progress == 100 || progress == "100"){
			return true;	
		} else {
			return false;	
		}		
	} ,
	
	_processApprove			: function( action ){
		$this = this;
		$this._displayApproveDeclineDialog( action )
	} ,
	
		
	_displayApproveDeclineDialog	: function( action ){
		$this = this;
		var dialog = $("<div />",{id:"dialog"})
						.css({
							"background-color" : "#000000",
							"color"			   : "white",
							"z-index"		   : 999,
							"position"		   : "absolute",
							"width"			   : "auto",							 
							top  			   : $("#approve_"+action.id).position().top,
							left 			   : $("#approve_"+action.id).position().left-20,
							opacity 		   : 0.78
						})
						.append($("<form />")
							.append($("<table />")
							  .append($("<tr />")
								.append($("<td />",{html:"<font color='white'>Reponse : </font>"}))
								.append($("<td />")
								  .append($("<textarea />",{id:"response", name:"response"}))		  
								)							
							  )		  
							  .append($("<tr />")
								.append($("<td />",{html:"<font color='white'>Sign Off:</font>"}))
								.append($("<td />")
									.append($("<select />")
										.append($("<option />",{value:"yes", text:"Yes"}))
										.append($("<option />",{value:"no", text:"No"}))										
									)		  
								)							
							  )	
							  .append($("<tr />")
								.append($("<td />",{colspan:"2", align:"right"})
								   .append($("<input />",{type:"submit", name:"approve_action_"+action.id, id:"approve_action_"+action.id, value:"Approve"}).addClass("iinform"))										  
								   .append($("<input />",{type:"submit", name:"decline_action_"+action.id, id:"decline_action_"+action.id, value:"Decline"}).addClass("idelete"))			
								)							
							  )	
							  .append($("<tr />")
								.append($("<td />",{colspan:"2", align:"right"})
								  .append($("<a />",{href:"#", id:"close_"+action.id, text:"Close" }))									
								)							
							  )								    
							)
						)

						 
		$("#approve_action_"+action.id).live("click", function(){	
			$this._processApproved( action );
			return false;												  
		});
		
		$("#decline_action_"+action.id).live("click", function(){
			$this._processDeclince( action.id );
			return false;												  
		});
		
		$("#close_"+action.id).live("click", function(){	
			$("#dialog").fadeOut().remove();
			return false;											 
		})
		$("body").append( dialog );				
				
	} , 
	
	_processApproved			: function( actionObject ){
		var message		= $("#approve_message");		
		var response  	= $("#approval_response").val();
		var signoff  	= $("#approval_signoff :selected").val();
		message.slideDown("slow").html("Saving approval . . .. <img src='../images/loaderA32.gif'>")										
		
		$.post( "controller.php?action=newApprovalAction", {
				id			: actionObject.id, 
				userrespo	: actionObject.action_owner,
				response	: response, 
				signoff		: signoff
			}, function( retApproval ){	
				message.html("");
				if( ! $.isEmptyObject( retApproval.success ) ) {
					if( signoff == "yes" ) {
					$("#table_approved")
					 .append($("<tr />")
					  .append($("<td />",{html:actionObject.id}))
					  .append($("<td />",{html:actionObject.action}))
					  .append($("<td />",{html:actionObject.status}))
					  .append($("<td />",{html:actionObject.progress+"%"}))
					  .append($("<td />",{html:""}))								  
					)
					$("#tr_"+actionObject.id).fadeOut("slow");
					$("#dialog").fadeOut("slow").remove();					
					}	
					$.each( retApproval.success , function( index, val ) {
						message.append( val+"<br />" );
					});
					$.each( retApproval.error , function( i, valu ) {
						message.append( valu+"<br />" );
					});	
				} else {
					$.each( retApproval.error , function( i, valu ) {
					message.append( "<br />"+valu+"<br />" );
				});
				}
			},"json")	
	} , 
	
	_processDeclince		: function( actionid ){
		var message		= $("#approve_message");				
		message.slideDown("slow").html("Sending action decline email . . .. <img src='../images/loaderA32.gif'>")											
		$.post("controller.php?action=sendDecline", { id : actionid }, function( response ){
			if( !response.error )
			{
				message.html( response.text ).addClass("ui-state-highlight")
			} else{
				message.html( response.text ).addClass("ui-state-error")
			}	
			$("#approve_dialog").fadeOut();
		},"json")															  
		return false;
	} , 
	
	_getUsers			    : function(){
		$.getJSON("controller.php?action=getManagerUsers", function( users ){
			$.each( users, function( index, user ){
				$(".users")
				 .append($("<option />",{value:user.user , text : user.tkname+" "+user.tksurname }))			 
			})														
		});		
	} , 
	
	_displayUsers			: function(){

	} , 
	
	approveAction			: function(){
		
	}	 
		 
});