//jquery textcounter jquery plugin
(function($){
	$.fn.textcounter = function( options ){
		
		var settings = {
			maxLength	: 200,
			minLength	: 10
		};
		return this.each(function() {			
			if( options ) {
				$.extend( settings , options);
			}
			var id 	   = this.id;
			var maxLen = settings.maxLength;
			var cuurentVal = $("#"+id).val();
			
			if( cuurentVal == ""){
				$(this).parent().append($("<br />")).append($("<span />",{id:"counter_"+id, html:maxLen+" characters allowed"})).append($("<br />"))
			} else {
				$(this).parent().append($("<br />")).append($("<span />",{id:"counter_"+id, html:(maxLen - cuurentVal.length)+" characters left . . ."})).append($("<br />"))
			}
			$(this).bind("keyup", function() {
				var textVal = $("#"+id).val();
				chars 	    = textVal.length;
				if( chars > maxLen ) {
					var text = textVal.substr(0, maxLen)
					$("#"+id).val( text );
					return false;
				} else {
					charsLeft = maxLen - chars
					$("#counter_"+id).html(charsLeft+" characters left . . .")				
					return true;
				}
			});
			
		});
	} 	
})(jQuery);