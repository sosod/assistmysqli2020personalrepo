// JavaScript Document
$(function(){
		   
	$.getJSON("controller.php?action=getManagerUsers" , function( managerUsers ) {
		$("#awaiting_approval").empty();
		$("#awaiting_approval").append($("<option />",{ text:"--please select--"}))
		
		$("#users_approved").empty();
		$("#users_approved").append($("<option />",{ text:"--please select--"}))		
		$.each( managerUsers, function( index, user ){
			$("#awaiting_approval")
			 .append($("<option />",{value:user.user , text : user.tkname+" "+user.tksurname }))
			 
			$("#users_approved")
			 .append($("<option />",{value:user.user , text : user.tkname+" "+user.tksurname }))			 
		})
	});
// ====================================================================================================================	
/**
	Get all actions wating to be approved 
**/
	$.getJSON("controller.php?action=getActionToApprove", function( actionsToApprove ) {
												
		if( $.isEmptyObject( actionsToApprove ) ) {
			$("#actions_awaiting_approve>tbody")
			.empty("")
			.append($("<td />",{colspan:"7", html:"No actions to  approve for this user"}))
		} else {
			populateAwaitingApproval( actionsToApprove )
		}															   
	});	
/**
	Get al the actions approved 
**/
	$.getJSON("controller.php?action=getApprovedActions", function( apprActions ){
		if( $.isEmptyObject( apprActions ) ) {
			$("#approved_actions>tbody")
			.empty("")
			.append($("<td />",{colspan:"7", html:"No actions approved"}))
		} else {
			populateApproved( apprActions );
		}														
	});
//========================================================================================================================
/**
	Get all the actions to approve for a selected user
**/	
	$("#awaiting_approval").live( "change", function() {
		$("#loadingActions").ajaxStart(function(){
			$("#loadingActions")
				.css({
					 	"position" : "absolute",
					 	"top" 	   : "300px",
						"left"	   : "200px",
						"z-index"  : "9999"
					 })
				.html("<img src='../images/loaderA32.gif' />");												
		});											 
		$("#loadingActions").ajaxComplete(function(){
			$("#loadingActions").html("");									   
		});									
											
	  $.getJSON("controller.php?action=getUserActionsToApprove", { userid : $(this).val() }, function( userActions ){	
		if( $.isEmptyObject( userActions ) ) {
			$("#actions_awaiting_approve>tbody")
			 .empty("")
			 .append($("<td />",{colspan:"7", html:"No actions to approve for this user"}))
		} else {
			populateAwaitingApproval( userActions )
		}																		
      });
	  return false;
	});
// ==================================================================================================================
 /**
	Get all actions approved for the selected person
 **/
 	$("#users_approved").live( "change", function() {
		$("#loadingAActions").ajaxStart(function(){
			$("#loadingAActions")
				.css({
					 	"position" : "absolute",
					 	"top" 	   : "300px",
						"left"	   : "900px",
						"z-index"  : "9999"
					 })
				.html("<img src='../images/loaderA32.gif' />");												
		});											 
		$("#loadingAActions").ajaxComplete(function(){
			$("#loadingAActions").html("");									   
		});														  
												  
		$.getJSON("controller.php?action=getUserApprovedActions", { userid : $(this).val() }, function( approvedActions ) { 																					
			if( $.isEmptyObject( approvedActions ) ) {
				$("#approved_actions>tbody")
				.empty("")
				.append($("<td />",{colspan:"7", html:"No actions approved for this user"}))
			} else {
				populateApproved( approvedActions )
			}																		
		});
		return false;
	});
 
});
/**
	Fill the awaiting to be approved table with data
**/
function populateAwaitingApproval( actionObject ) {
	$("#actions_awaiting_approve>tbody")
	.empty("")
	
	 $("#actions_awaiting_approve")
	  .append($("<tr />")
		.append($("<th />",{html:"Action #"}))
		.append($("<th />",{html:"Action Description"}))
		.append($("<th />",{html:"Status"}))
		.append($("<th />",{html:"Status"}))
		.append($("<th />",{html:"Attachment"}))
		.append($("<th />",{html:""}))				
	  )
	$.each( actionObject, function( index, action) {
		$("#actions_awaiting_approve>tbody")
		.append($("<tr />",{id:"tr_awaiting"+action.id})
			.append($("<td />",{html:action.id}))
			.append($("<td />",{html:action.action}))
			.append($("<td />",{html:action.status}))
			.append($("<td />",{html:(action.progress=="" ? "0" : action.progress)+"%"}))				
			.append($("<td />",{html:""}))
			.append($("<td />")
			  .append($("<input />",{type:"button", name:"approve_"+action.id, id:"approve_"+action.id, value:"Approve"}))		
			 )							  
		)
		$("#approve_"+action.id).live("click", function() {													
			if( action.progress !== "100") {
				return alert( "This action cannot be approved, it must be completed/addressed for it to be approved" );
			} else {
				approveAction( action )	;
			}
		});
	});
}
/**
	Fill the approved actions table with data
**/
function populateApproved( approvedObject ) {	
	$("#approved_actions>tbody")
	.empty("")
	
	$("#approved_actions")
	.append($("<tr />")
	.append($("<th />",{html:"Action #"}))
	.append($("<th />",{html:"Action Description"}))
	.append($("<th />",{html:"Status"}))
	.append($("<th />",{html:"Status"}))
	.append($("<th />",{html:"Attachment"}))			
	)
	
	$.each( approvedObject, function( index, action ) {
		$("#approved_actions")									   
		.append($("<tr />",{id:"tr_awaiting"+action.id})
		.append($("<td />",{html:action.id}))
		.append($("<td />",{html:action.action}))
		.append($("<td />",{html:action.status}))
		.append($("<td />",{html:(action.progress=="" ? "0" : action.progress)+"%"}))				
		.append($("<td />",{html:""}))			
		)
	});
}

function approveAction( actionObject ) {
	var message = $("#approve_message");
	$("#approve_dialog").css({
							 	top  	: $("#approve_"+actionObject.id).position().top,
								left 	: $("#approve_"+actionObject.id).position().left-20,
								opacity : 0.98 
							})
						 .fadeIn("fast");
		
		$("#approve_action").live("click", function(){
			message.slideDown("slow").html("Saving approval . . .. <img src='../images/loaderA32.gif'>")										
			var response  	= $("#approval_response").val();
			var signoff  	= $("#approval_signoff :selected").val();
			
			$.post( "controller.php?action=newApprovalAction", {
				id			: actionObject.id, 
				userrespo	: actionObject.action_owner,
				response	: response, 
				signoff		: signoff
			}, function( retApproval ){	
				message.html("");
				if( ! $.isEmptyObject( retApproval.success ) ) {
					if( signoff == "yes" ) {
					$("#approved_actions")
					 .append($("<tr />")
					  .append($("<td />",{html:actionObject.id}))
					  .append($("<td />",{html:actionObject.action}))
					  .append($("<td />",{html:actionObject.status}))
					  .append($("<td />",{html:actionObject.progress}))
					  .append($("<td />",{html:""}))								  
					)
					$("#tr_awaiting"+actionObject.id).fadeOut("slow");
					}	
					$.each( retApproval.success , function( index, val ) {
						message.append( val+"<br />" );
					});
					$.each( retApproval.error , function( i, valu ) {
						message.append( valu+"<br />" );
					});	
					
					$("#approve_dialog").fadeOut();		
				} else {
					$.each( retApproval.error , function( i, valu ) {
					message.append( "<br />"+valu+"<br />" );
				});
				}
			},"json")
			return false;
		});
		
		$("#decline_action").live("click", function(){
			message.slideDown("slow").html("Sending action decline email . . .. <img src='../images/loaderA32.gif'>")											
			$.post("controller.php?action=sendDecline", { id : actionObject.id }, function( response ){
				if( !response.error )
				{
					message.html( response.text ).addClass("ui-state-highlight")
				} else{
					message.html( response.text ).addClass("ui-state-error")
				}	
				$("#approve_dialog").fadeOut();
			},"json")															  
			return false;							 
		});	
		
		$("#closedialog").live("click", function(){
			$("#approve_dialog").fadeOut();								 
			return false;							 
		});
}
