// JavaScript Document
$(function(){
		   
	$(".textcounter").textcounter({maxLength:100});
	
	$("#add").click(function(){
		FinancialExposure.save();											
		return false;										
	});	   

	$("#edit").click(function(){
		FinancialExposure.edit();											
		return false;										
	});	   

	$("#change").click(function(){
		FinancialExposure.change();											
		return false;										
	});
	$("#cancel_financial_exposure").click(function(){
		history.back();
		return false;
	})
	
	if( $("#financial_exposure_id").val() == undefined ){
		FinancialExposure.get();		
	}
});


var FinancialExposure	= {
	
	get				: function()
	{
		$.post("controller.php?action=getFinancialExposure", function( data ){
			$.each( data, function( index, finYear){
				FinancialExposure.display( finYear );					   
			});																						 
		} ,"json");		
	} , 
	
	save 			: function()
	{
		var data 			= {};
		data.from			= $("#from").val();
		data.to				= $("#to").val();		
		data.name			= $("#name").val();
		data.definition		= $("#definition").val();
		data.color			= $("#color").val();
		
		if( data.from == ""){
			jsDisplayResult("error", "error", "Please enter the from");			
			return false;
		} else if( data.to == ""){
			jsDisplayResult("error", "error", "Please enter the to");
			return false;
		} else if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the name");			
			return false;			
		} else if( data.definition == ""){
			jsDisplayResult("error", "error", "Please enter the definition");
			return false;			
		} else if( data.color == ""){
			jsDisplayResult("error", "error", "Please enter the color");			
			return false;			
		} else {
			jsDisplayResult("info", "info", "Saving  . . .  <img src='../images/loaderA32.gif' />");
			$.post("controller.php?action=newFinancialExposure", data , function( response ){
				if( response > 0){
					 $("#from").val("");
					 $("#to").val("");
					 $("#name").val("");
					 $("#definition").val("");
					jsDisplayResult("ok", "ok", "Financial exposure saved");
					data.id = response;
					data.status = 1;
					FinancialExposure.display( data );
				} else{
					jsDisplayResult("error", "error", "Error saving financial year");				
				}																																	 
			} ,"json");			
		}
		
	}  ,
	
	change			: function()
	{
		
		var data = {}
		data.id 	= $("#financial_exposure_id").val();
		data.status = $("#financial_exposure_status").val();
		jsDisplayResult("info", "info", "Updating  . . .  <img src='../images/loaderA32.gif' />");
		$.post("controller.php?action=changeFinancialExposureStatus", data , function( response ){
			if(response == 1){
				jsDisplayResult("ok", "ok", "Financial exposure updated");
			} else if(response == 0){
				jsDisplayResult("info", "info", "No change was made to the financial exposure status");
			} else{
				jsDisplayResult("error", "error", "Error updating financial exposure");				
			}																																	
		} ,"json")
	} , 
	
	edit			: function()
	{
		var data 			= {};
		data.from			= $("#from").val();
		data.to				= $("#to").val();		
		data.name			= $("#name").val();
		data.definition		= $("#definition").val();
		data.color			= $("#color").val();
		data.id 			= $("#financial_exposure_id").val()
		
		if( data.from == ""){
			jsDisplayResult("error", "error", "Please enter the from");			
			return false;
		} else if( data.to == ""){
			jsDisplayResult("error", "error", "Please enter the to");
			return false;
		} else if( data.name == ""){
			jsDisplayResult("error", "error", "Please enter the name");			
			return false;			
		} else if( data.definition == ""){
			jsDisplayResult("error", "error", "Please enter the definition");
			return false;			
		} else if( data.color == ""){
			jsDisplayResult("error", "error", "Please enter the color");			
			return false;			
		} else {
			jsDisplayResult("info", "info", "Saving  . . .  <img src='../images/loaderA32.gif' />");
			$.post("controller.php?action=updateFinancialExposure", data , function( response ){
				if(response == 1){
				  jsDisplayResult("ok", "ok", "Financial exposure updated");
				} else if(response == 0){
				  jsDisplayResult("info", "info", "No change was made to the financial exposure");
				} else{
					jsDisplayResult("error", "error", "Error updating financial exposure")				
				}																																	
			} ,"json");			
		}
	} , 
	
	display 		: function( finExposure )
	{
		var from = (finExposure.exp_from == undefined ? finExposure.from : finExposure.exp_from)
		var to   = (finExposure.exp_to == undefined ? finExposure.to : finExposure.exp_to)
		
		$("#financial_exposure_table")
		 .append($("<tr />",{id:"tr_"+finExposure.id})
			.append($("<td />",{html:finExposure.id}))							   
			.append($("<td />",{html:from+" to "+to}))	   
			.append($("<td />",{html:finExposure.name}))
			.append($("<td />",{html:finExposure.definition}))			
			.append($("<td />")
			  .append($("<span />",{html:finExposure.color}).css({"background-color":"#"+finExposure.color, "padding":"5px"}))		
			)
			.append($("<td />")
			   .append($("<input />",{type:"button", name:"edit_"+finExposure.id, id:"edit_"+finExposure.id, value:"Edit"}))
			   .append($("<input />",{type:"button", name:"del_"+finExposure.id, id:"del_"+finExposure.id, value:"Del"}))
			 )
			.append($("<td />",{html:((finExposure.status & 1) == 1 ? "<b>Active</b>" : "<b>InActive</b>")}))			
			.append($("<td />")
			   .append($("<input />",{type:"button", name:"change_"+finExposure.id, id:"change_"+finExposure.id, value:"Change Status"}))					  
			 )						
		  )
		 $("#del_"+finExposure.id).live("click", function(){
			if(confirm("Are you sure you want to delete this financial exposure")){
				var data = {}
				data.id 	= finExposure.id;
				data.status = 2
				jsDisplayResult("info", "info", "Deleting  . . .  <img src='../images/loaderA32.gif' />");
				$.post("controller.php?action=changeFinancialExposureStatus", data , function( response ){
					if( response == 0 || response == 1){
						$("#tr_"+finExposure.id).fadeOut();
						jsDisplayResult("ok", "ok", "Financial exposure deleted");
					} else{
						jsDisplayResult("error", "error", "Error deleting financial exposure");				
					}																																	
				} ,"json")
			}											   
			return false;											   
		});
		  
		 $("#edit_"+finExposure.id).live("click", function(){
			document.location.href = "edit_financial_exposure.php?id="+finExposure.id;											   
			return false;											   
		});
		 
		 $("#change_"+finExposure.id).live("click", function(){
			document.location.href = "change_financial_exposure.php?id="+finExposure.id;											
			return false;											   
		});		 
		
	}
}
	
