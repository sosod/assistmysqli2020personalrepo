// JavaScript Document
$(function(){
		  
	$("#edit_riskcategory").click(function(){
		
		var message     = $("#editriskcategory_message");
		var category    = $("#risk_category").val();
		var shortcode   = $("#category_shortcode").val();
		var description = $("#category_description").val();
		var c_risktype  = $("#category_risktype :selected").val();

		if ( shortcode == "") {
			message.html("Please enter the short code for this category")	
			return false;
		} else if( category == "") {
			message.html("Please enter the category ")	
			return false;
		} else if ( c_risktype == "") {
			messsage.html("Please select the risk type for this category")
			return false;
		} else {
			$.post( "controller.php?action=updateCategory", 
				  	{ 
					id				: $("#risk_category_id").val(),
					category		: category,
					shortcode		: shortcode,
					description		: description,
					type_id			: c_risktype },
					function( retData ){
						if( retData == 1 ) {
							message.html("Category successifully updated .. ");						
						} else if( retData == 0 ){
							message.html("Category not changed");
						} else {
							message.html("Error updating the category");
						}
			}, "json");	
		}
		return false;				   
					 
	});		  

	$("#cancel_riskcategory").click(function(){
		history.back();
		return false;								   
	})
	
});