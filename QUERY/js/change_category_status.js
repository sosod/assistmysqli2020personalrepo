// JavaScript Document
$(function(){
		$("#change_riskcategory").click(function(){
			message = $("#editriskcategory_message");
			$.post( "controller.php?action=catdeactivate", 
				   { id		: $("#risk_category_id").val(),
				   	 status : $("#category_status :selected").val()
				   }, function( catdeaData ) {
				if( catdeaData == 1){
					message.html("Category status changed").animate({opacity:"0.0"},4000);
				} else if( catdeaData == 0){
					message.html("No change was made to the category ").animate({opacity:"0.0"},4000);
				} else {
					message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
				}									 
			});	
		  	return false;
		  });

})