// JavaScript Document
$(function(){
	
	$("#edit_subdir").click(function(){
	 	var message 	   = $("#subdirectorate_message");
		var sub_shortcode  = $("#sub_shortcode").val();
		var sub_department = $("#subdir_dpt :selected");
		var sub_division   = $("#subdir_division").val();
		var sub_user	   = $("#subdir_user :selected");
	
		if( sub_shortcode == "") {
			message.html("Please enter the short code for this  sub directorate");
			return false;
		} else if( sub_department.val() == "") {
			message.html("Please select department name for this sub directorate");
			return false;			
		} else if( sub_division == "" ) {
			message.html("Please enter the division for this sub directorate");
			return false;			
		} else if( sub_user.val() == "" ) {
			message.html("Please select user responsible for this sub directorate");
			return false;			
		} else {
			$.post( "controller.php?action=updateSubDirectorate",
				   {
					   	id			: $("#subdir_id").val(),
					   	shortcode	: sub_shortcode,
						dpt			: sub_department.val(),
						fxn			: sub_division,
						user		: sub_user.val()
					},
				   function( retData ) {
						if( retData == 1 ) {
							message.html("Sub directorate successifully updated .. ");						
						} else if( retData == 0 ){
							message.html("Sub directorate not changed");
						} else {
							message.html("Error updating the sub directorate");
						}
			}, "json")
		}
		return false;									 
	});
	
	$("#change_subdir").click(function(){
	 	var message 	   = $("#subdirectorate_message");										   
		$.post( "controller.php?action=changeSubDirStatus",
			   {
				   id		:  $("#subdir_id").val(), 
			   	   status   : $("#subdir_status :selected").val()
			   }, function( deactData ) {
			if( deactData == 1){
				message.html("Sub Directorate structure updated").animate({opacity:"0.0"},4000);
			} else if( deactData == 0){
				message.html("No change was made to the  sub directorate structure").animate({opacity:"0.0"},4000);
			} else {
				message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
			}									 
		})									
		return false;									   
	});
	
	$("#cancel_subdir").click(function(){
		history.back();
		return false;									   
	});
	
});