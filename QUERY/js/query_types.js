$(function(){
	
	$(".textcounter").textcounter({maxLength:100});
	
	if( $("#risktype_id").val() == undefined)
	{
		QueryType.get();		
	}
	
	$("#add_risktype").click( function(){
		QueryType.save();
		return false;
	})
	
	$("#edit_risktype").click(function(){
		QueryType.edit();
		return false;
	})
	
	$("#changes_risktype").click(function() {
		QueryType.change();
		return false;
	})

	$("#cancel_risktype").click(function(){
		history.back();
		return false;								   
	})
	
});

var QueryType 	= {
		
		
		get				: function()
		{
			$.getJSON( "controller.php?action=getQueryType", function( data ) {
				$.each( data, function( index, val){
					QueryType.display( val );					
				});
			})
		} ,
		
		save			: function()
		{
			var shortcode 	= $("#risktype_shortcode").val();
			var type 		= $("#risktype").val();
			var description	= $("#risktype_descri").val();
			
			if ( shortcode == "" ) {
				jsDisplayResult("error", "error", "Please the short code for this query type");
				return false;
			} else if( type == "") {
				jsDisplayResult("error", "error", "Please enter the risk type");
				return false;
			} else {
				jsDisplayResult("info", "info", "Saving . . .  <img src='../images/loaderA32.gif' />");
				$.post( "controller.php?action=newQueryType",
				{ 
					shortcode	: shortcode, 
					name			: type, 
					description	: description 
				}, function( retData ) {
					if( retData > 0 ) {
						 $("#risktype_shortcode").val("");  
						 $("#risktype").val("");
						 $("#risktype_descri").val("");
						jsDisplayResult("ok", "ok", "Query type succesfully saved . . . ");
						var data  = { id : retData, shortcode : shortcode, name : type, description : description , active : 1 };
						QueryType.display(data)
				   } else {
						jsDisplayResult("error", "error", "Error Saving query type . . . ");
				  }  
				});
			}
		} , 
		
		edit			: function()
		{
			var shortcode 	= $("#risktype_shortcode").val();
			var type 		= $("#risktype").val();
			var description	= $("#risktype_descri").val();
			
			if ( shortcode == "" ) {
				jsDisplayResult("error", "error", "Please the short code for this query type");
				return false;
			} else if( type == "") {
				jsDisplayResult("error", "error", "Please enter the risk type");
				return false;
			} else {
				jsDisplayResult("info", "info", "Updating . . .  <img src='../images/loaderA32.gif' />");
				$.post( "controller.php?action=updateQueryType",
			   { 
				   id			: $("#risktype_id").val(), 
				   shortcode	: shortcode, 
				   name			: type, 
				   description	: description 
			   }, function( retData ) {
					if( retData == 1 ) {
						jsDisplayResult("ok", "ok", "Query type successfully updated . . . ");
					} else if( retData == 0 ){
						jsDisplayResult("info", "info", "Query type not changed . . . ");
					} else {
						jsDisplayResult("error", "error", "Error updating the query type . . . ");
					}
				});
			}
		} , 
		
		
		change			: function()
		{
			jsDisplayResult("info", "info", "Updating . . .  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=changeQueryStatus",
				   { id		: $("#risktype_id").val() ,
				   	 status : $("#type_statuses :selected").val(),
				   	 active	: $("#type_statuses :selected").val()
				   }, function( typDedeactData ) {
				if( typDedeactData == 1) {
					jsDisplayResult("ok", "ok", "Query Type status changed . . . ");
				} else if( typDedeactData == 0) {
					jsDisplayResult("info", "info", "No change was made to the query type status . . . ");
				} else {
					jsDisplayResult("error", "error", "Error saving , please try again . . . ");
				}										 
			})	
		} ,
		
		display			: function( val )
		{
			$("#risk_type_table")
			  .append($("<tr />",{id:"tr_"+val.id})
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.shortcode}))
				.append($("<td />",{html:val.name}))
				.append($("<td />",{html:val.description}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"typeedit_"+val.id, name:"typeedit_"+val.id }))
				  .append($("<input />",{type:"submit",value:"Del", id:"typedel_"+val.id, name:"typedel_"+val.id }))			  
				 )
				.append($("<td />",{html:"<b>"+(val.active == 1 ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
					.append($("<input />",{type:"submit", id:"change_"+val.id, name:"change_"+val.id, value:"Change Status"}))		  
				)
			  )
			  
			$("#typeedit_"+val.id).live( "click", function() {
				document.location.href = "edit_query_type.php?id="+val.id;
				return false;										  
			});
			
			$("#change_"+val.id).live( "click", function() {
				document.location.href = "change_query_type_status.php?id="+val.id;
				return false;										  
			});
			
			$("#typedel_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this query type ")) {
					jsDisplayResult("info", "info", "Deleting . . .  <img src='../images/loaderA32.gif' />");
					$.post( "controller.php?action=changeQueryStatus",
						   {id		: val.id,
						   	status  : 2,
						   	active	: 2						   	
						   }, function( typDelData ) {
						if( typDelData == 1) {
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("ok", "ok", "Query Type deleted");
						} else if( typDelData == 0) {
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("info", "info", "No change was made to the query type");
						} else {
							jsDisplayResult("error", "error", "Error saving , please try again");
						}										 
					});
				}
				return false;										  
			});			
			
		}
		
		
		
}