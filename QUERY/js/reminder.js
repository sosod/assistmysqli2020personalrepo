$(function(){

    $("#save_reminders").live("click", function(e){
      Reminder.saveReminders(); 
      e.preventDefault();
    });

    $("#update_reminders").live("click", function(e){
      Reminder.updateActionReminders(); 
      e.preventDefault();       
    });
});

var Reminder = {
		
    saveReminders            : function()
    {
		jsDisplayResult("info", "info", "saving ... <img src='../images/loaderA32.gif' />");
		$.post( "controller.php?action=saveReminder", {
			action_days : $("#action_days").val(),
			query_days  : $("#query_days").val()
		}, function( response ) {
			if( response.error ) {
				jsDisplayResult("error", "error", response.text );
			} else {
				jsDisplayResult("ok", "ok",  response.text );
			}
		},"json");	
    } ,
    
    updateActionReminders       : function()
    {
		jsDisplayResult("info", "info", "updating ... <img src='../images/loaderA32.gif' />");
		$.post( "controller.php?action=updateReminder", {
			action_days : $("#action_days").val(),
			query_days  : $("#query_days").val(),
			id          : $("#id").val()
		}, function( response ) {
			if( response.error ) {
				jsDisplayResult("error", "error", response.text );
			} else {
			    if(response.updated)
			    {
			       jsDisplayResult("ok", "ok",  response.text );
			    } else {
			        jsDisplayResult("info", "info",  response.text );
			    }
			}
		},"json");	
    }
	
		
}
