$.widget("ui.approve",{
	
	options		: {
		limit				: 10,
		approvedStart 		: 0,
		approvedCurrent		: 1,
		approvedPages		: 10,
		approvedLimit		: 10,		
		approvedTotal		: 10,				
		tobeapprovedStart	: 0,
		tobeapprovedCurrent	: 1,
		tobeapprovedPages	: 10,
		tobeapprovedLimit	: 10,
		tobeapprovedTotal	: 10,
		loggedUserId		: 0, 
		url					: "controller.php?action=getApprovalData",
		headers				: []
	} , 
	
	_init		: function()
	{
		this._getActions()		
	} , 
	
	_create		: function()
	{
		var self = this;
		var html = [];
		
		html.push("<table width='100%' class='noborder'>");
		  html.push("<tr>");
		    html.push("<td width='50%' class='noborder'>");
		        html.push("<table id='awaiting_approval' width='100%'>");
		          html.push("<tr>");
		            html.push("<td  colspan='8'>Actions Awaiting Approval</td>");
		          html.push("</tr>");
		        html.push("</table>");		        
		    html.push("</td>");
		    html.push("<td width='50%' class='noborder'>");
		        html.push("<table id='actions_approved' width='100%'>");
		          html.push("<tr>");
		            html.push("<td colspan='8'>Actions Approved</td>");
		          html.push("</tr>");
		        html.push("</table>");		        
		    html.push("</td>");		    
		  html.push("</tr>");
		html.push("</table>");
		$(self.element).append(html.join(' '));
	} , 
	
	_getActions		: function()
	{
		var self = this;
	     $("body").append($("<div />",{id:"actionLoadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
	     );			
		$.getJSON( self.options.url , {
			start		: self.options.approvedStart,
			limit		: self.options.limit, 
			user		: self.options.loggedUserId
		} , function(responseData){
		    $("#actionLoadingDiv").remove();
			$(".actions_approved").html("");
			$(".awaiting_approval").html("");
			self.options.headers = responseData.headers;
			self._display(responseData.awaiting.actions, "awaiting_approval", responseData.awaiting);
			self._display(responseData.approved.actions, "actions_approved", responseData.awaiting);
		});
		
	} ,
	
	_display		: function(actions, table, data)
	{
		var self = this;
        var html = [];
        html.push("<tr class='"+table+"'>");
	      html.push("<th>Ref</th>");
	      html.push("<th>"+data.headers.query_action+"</th>");
	      html.push("<th>"+data.headers.deliverable+"</th>");
	      html.push("<th>"+data.headers.action_owner+"</th>");
	      html.push("<th>"+data.headers.timescale+"</th>");
	      html.push("<th>"+data.headers.progress+"</th>");
	      html.push("<th>"+data.headers.action_status+"</th>");
        html.push("<th></th>");
        html.push("</tr>");
        
        if($.isEmptyObject(actions))
        {   
            html.push("<tr class='"+table+"'>");
              if(table == "awaiting_approval")
              {
                html.push("<td colspan='8'>No actions awaiting approval</td>");
              } else {
                html.push("<td colspan='8'>No actions approved</td>");
              }
            html.push("</tr>");        
        } else {
            $.each(actions, function(index, action){
               html.push("<tr class='"+table+"' id='tr_"+index+"'>");
	              html.push("<td>"+index+"</td>");
	              html.push("<td>"+action.query_action+"</td>");
	              html.push("<td>"+action.deliverable+"</td>");
	              html.push("<td>"+action.action_owner+"</td>");
	              html.push("<td>"+action.timescale+"</td>");
	              html.push("<td>"+action.progress+"</td>");
	              html.push("<td>"+action.action_status+"</td>");
                 /*
                 $.each(action, function(key, val){
                    html.push("<td>"+val+"</td>");
                 });
                 */
               if(table == "awaiting_approval")
               {
                 html.push("<td>");
                   html.push("<input type='button' name='approve_"+index+"' id='approve_"+index+"' value='Approve' />");
                   html.push("<input type='button' name='view_log_"+index+"' id='view_log_"+index+"' value='View Logs' />");
                    $("#approve_"+index).live("click", function(){
                        self._approvalDialog(index, action);
                    });			                   
                    $("#view_log_"+index).live("click", function(){
                        self._updateLogsDialog(index, action);
                    });	
                 html.push("</td>");
               }  
               if(table == "actions_approved")
               {
                 html.push("<td>");
                   html.push("<input type='button' name='unlock_"+index+"' id='unlock_"+index+"' value='Unlock' />");		
				    $("#unlock_"+index).live("click", function(){
					    self._unlockAction(index);  
					    return false;									   
				    });
                 html.push("</td>");
               }
                 
               html.push("</tr>");
            });
        }
		$("#"+table).append(html.join(' '));
	} ,
	
	_approvalDialog             : function(index, action)
	{
        var self 	    = this;	    
        if($("#approvedialog_"+index).length > 0)
        {
           $("#approvedialog_"+index).remove();
        }
        $("<div />",{id:"approvedialog_"+index})
        .append($("<table />", {width:"100%"})
            .append($("<tr />")
                .append($("<th />",{html:"Response:"}))
                .append($("<td />")
                    .append($("<textarea />",{cols:"30", rows:"8", id:"response",name:"response"}))	  
                )
            )
            .append($("<tr />")
                .append($("<td />",{colspan:2})
                .append($("<span />",{id:"loader_"+index}))	 
                )	   
            )
        )
        .dialog({
            autoOpen	: true,
            modal		: true,
            width       : "auto",
            position    : "top",
            title		: "Approve action #"+index,
            buttons		: {
                "Approve" : function()
                {

		            var response  	= $("#response").val();
		            $("#loader_"+index).html("approving action ... <img src='../images/loaderA32.gif' />");
		            $.post("controller.php?action=approveAction", {
				            id			: index, 
				            response	: response
			            }, function(response){	
				            if(response.error)
				            {
				               jsDisplayResult("error", "error", response.text);
				               self._getActions(); 
				            } else {
				               jsDisplayResult("ok", "ok", response.text);
				               self._getActions(); 
				            }
				            $("#approvedialog_"+index).dialog("destroy").remove();
			            },"json")	
                }, 
                "Decline" : function()
                {
		            var response  	= $("#response").val();		            
		            $("#loader_"+index).html("declining action ... <img src='../images/loaderA32.gif' />");
		            if(response == "")
		            {
		               $("#loader_"+index).addClass("ui-state-error").css({padding:"5px"}).html("Please enter the repsonse");
		            } else {
		                $.post("controller.php?action=declineAction", {id:index, response : response},
		                   function(response ){
			                if(response.error )
			                {
				                jsDisplayResult("error", "error", response.text);			                    
			                } else {
				                $("#tr_"+actionid).fadeOut("slow");
				                jsDisplayResult("ok", "ok", response.text);				                
			                }
			                self._getActions();			
			                $("#approvedialog_"+index).dialog("destroy").remove();
		                },"json");
		            }														  
                },
                "Cancel" : function()
                {
	                $("#approvedialog_"+index).dialog("destroy").remove();
                }
            },
            close        : function(event, ui)
            {
                $("#approvedialog_"+index).dialog("destroy").remove();
            },
            open         : function(event, ui)
            {
                var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                var approveBtn = btns[0];
                var declineBtn = btns[1];          

                $(approveBtn).css({"color":"#090"});
                $(declineBtn).css({"color":"red"});
	                           
            }
        });
        return false;
	
	},

    _updateLogsDialog       : function(index)
    {
        var self 	    = this;	    
        if($("#viewlogdialog_"+index).length > 0)
        {
           $("#viewlogdialog_"+index).remove();
        }
        
        $("<div />",{id:"viewlogdialog_"+index})
            .dialog({
                autoOpen	: true,
                modal		: true,
                width       : "auto",
                position    : "top",
                title		: "Activity Log for Action #"+index,
                buttons		: {
                    "Ok" : function()
                    {
	                    $("#viewlogdialog_"+index).dialog("destroy").remove();											  
                    }
                },
                close        : function(event, ui)
                {
                    $("#viewlogdialog_"+index).dialog("destroy").remove();
                },
                open         : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var approveBtn = btns[0];
                    var declineBtn = btns[1];          

                    $(approveBtn).css({"color":"#090"});
                    $(declineBtn).css({"color":"red"});
                    $("#viewlogdialog_"+index).append("<span id='loader'>Loading activity log ....</span>")
                    $.get("controller.php?action=getActionActiviyLog", {id:index}, function(responseData){
                        $("#loader").remove();
                        $("#viewlogdialog_"+index).append("<div>"+responseData+"<div>");
                    });
	                               
                }
            });
    },
    
	_unlockAction			: function(id)
	{
    	var self = this;
    	if($("#unlock_action_"+id).length > 0)
    	{
    	    $("#unlock_action_"+id).remove();
    	}
    	
    	$("<div />",{id:"unlock_action_"+id}).append($("<table />",{width:"100%"})
    	   .append($("<tr />")
    	     .append($("<th />",{html:"Response:"}))
    	     .append($("<td />")
    	       .append($("<textarea />",{cols:"40", rows:"10"}))
    	     )
    	   )
    	   .append($("<tr />")
    	     .append($("<td />",{colspan:"2"})
    	        .append($("<p />",{id:"message"}))
    	     )
    	   )
    	).dialog({
    	    autoOpen    : true,
    	    modal       : true,
    	    position    : "top",
    	    title       : "Unlock Action #"+id,
    	    width       : "auto",
    	    buttons     : {
    	                    "Ok"    : function()
    	                    {
		                        jsDisplayResult("info", "info", "Unlocking the action ...<img src='../images/loaderA32.gif' />");
		                        $.post("controller.php?action=unlockAction", { id : id }, function( response )
		                        {
			                        if(response.error )
			                        {
			                           jsDisplayResult("error", "error", response.text);
			                        } else{
                                       jsDisplayResult("ok", "ok", response.text);	
                                       $("#unlock_"+id).attr("disabled", "disabled")
                                       $("#unlock_action_"+id).remove();
                                       $("#unlock_action_"+id).dialog("destroy");
                                       self._getActions();
			                        }																  
		                        },"json");	    	                        
    	                    } ,
    	                    
    	                    "Cancel"    : function()
    	                    {
                               $("#unlock_action_"+id).remove();
                               $("#unlock_action_"+id).dialog("destroy");    	                    
    	                    }
    	    } ,
    	    close       : function(ui, event)
    	    {
               $("#unlock_action_"+id).remove();
               $("#unlock_action_"+id).dialog("destroy");    	    
    	    } ,
    	    open        : function(ui, event)
    	    {
    	        
    	    }
    	});	
	} 
	
	
	
});
