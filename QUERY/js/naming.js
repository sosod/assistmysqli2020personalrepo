// JavaScript Document
$(function(){
	var namingItems = [
		{name:"Risk Item" , id:"risk_item" } ,
		{name:"Risk type" , id:"risk_type" } ,
		{name:"Risk Category" , id:"risk_category" } ,
		{name:"Resk description" , id:"risk_description" } , 
		{name:"Background of the risk" , id:"risk_background" } ,
		{name:"impact" , id:"impact" } , 
		{name:"Impact Rating" , id:"impact_rating" } , 
		{name:"Likelihood" , id:"likelihood" } , 
		{name:"Likelihood Rating" , id:"likelihood_rating" } ,
		{name:"Inherent Risk Exposure" , id:"inherent_risk_exposure" } , 
		{name:"Inherent Risk Rating" , id:"inherent_risk_rating" } ,
		{name:"Current Controls" , id:"current_controls" } ,
		{name:"Percieved Control of effectiveness" , id:"percieved_control_effective" } ,
		{name:"Control Effectiveness Rating" , id:"control_rating" } ,
		{name:"Residual Risk" , id:"residual_risk" } ,
		{name:"Residual Risk Exposure" , id:"residual_risk_exposure" } ,
		{name:"Risk Owner" , id:"risk_owner" } ,
		{name:"Actions to improve management of risk" , id:"actions_to_improve" } ,
		{name:"Action Owner" , id:"action_owner" } ,
		{name:"Time Scale" , id:"timescale" } 
  	];
	
	$.get( "controller.php?action=getHeaderNames", function( headerNamesData ){												
		forms.generateNamingForm( namingItems , headerNamesData);											   
	},"json");
});

var forms = {
	generateNamingForm  : function( items, namingData ) {
	$("#container").append($("<table />",{id:"contents" , border:"1"})
		.append($("<tr />")
			.append($("<th />",{html:"Ref"}))
			.append($("<th />",{html:"Default Terminology"}))
			.append($("<th />",{html:"Your Terminology"}))
			.append($("<th />",{html:""}))			
		)						 
	);
	$.each( namingData, function( index, value) {
		$("#contents")
		 .append($("<tr />",{id:"tr_"+value.id})
			.append($("<td />",{html:value.id}))
			.append($("<td />",{html:value.ignite_terminology}))
			.append($("<td />")
			  .append($("<input />",{type:"text", name:"client_name_"+value.id, id:"client_name_"+value.id, value:value.client_terminology, size:"50" }))	
			  .append($("<span />",{id:"updating_"+value.id })
				.append($("<img />",{src:"../images/arrows16.gif"}))
				.hide()
			  )
			)
			.append($("<td />",{align:"center"})
				.append($("<input />",{type:"submit" , name:"save_"+value.id , id:"save_"+value.id , value:"Update" }))	  
			)			
		  )
		$("#"+value.id).live( "focus", function(){
			$("#tr_"+value.id).addClass("tdhover")						   
		});
		
		$("#"+value.id).live( "focusout", function(){
			$("#tr_"+value.id).addClass("tdhover")							   
		});	
	
		$("#save_"+value.id).live( "click" , function() {
			if( $("#"+value.id).val() == "" ){
				jsDisplayResult("ok", "ok", "No menu changes were made ");
			} else {
			$("#updating_"+value.id).show();
			$.post("controller.php?action=updateHeaderNames" , 
				   {
					 fieldname : value.name ,
					 value 	   : $("#client_name_"+value.id).val()
				   } , function( data ) {
						if(data == 1) 
						{
						  jsDisplayResult("ok", "ok", "Changes saved");				
						} else if(data == 0){
						  jsDisplayResult("info", "info", "No changes we made");				
						} else {
							jsDisplayResult("error", "error", "There was an error saving the custom name");	
						}
			})
		  }
		  $("#updating_"+value.id).remove()
		  return false
		});				
	});

	}
	
}
