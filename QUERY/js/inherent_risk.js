// JavaScript Document
	$(function(){
	/**
		get all the inherent risk
	**/
	$("#inherent_risk_message").html("loading ... <img src='../images/loaderA32.gif' >");
	$.get( "controller.php?action=getInherentRisk", function( data ) {
		$("#inherent_risk_message").html("")
		var message = $("#inherent_risk_message")
		$.each( data , function( index, val) {
			$("#inherent_risk_table")
			.append($("<tr />",{id:"tr_"+val.id})
			  .append($("<td />",{html:val.id}))
			  .append($("<td />",{html:val.rating_from+" to "+val.rating_to}))
			  .append($("<td />",{html:val.magnitude}))
			  .append($("<td />",{html:val.response}))
			  .append($("<td />",{css:{"background-color":"#"+val.color}, html:val.color }))
			  .append($("<td />")
				.append($("<input />",{type:"submit", value:"Edit", id:"inherentactedit_"+val.id, name:"inherentactedit_"+val.id}))	
				.append($("<input />",{type:"submit", value:"Del", id:"inherentactdel_"+val.id, name:"inherentactdel_"+val.id}))					
			   )
			  .append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"})
				//.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ), value:"Activate", id:"inherentactact_"+val.id, name:"inherentactact_"+val.id}))	
				///.append($("<input />",{type:(val.active==1 ? "hidden"  : "submit" ),value:"Inactivate", id:"inherentactdeact_"+val.id, name:"inherentactdeact_"+val.id}))							
			   )	
			  .append($("<td />")
				.append($("<input />",{type:"button", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))		
				)
			)
			
			$("#change_"+val.id).live("click", function(){
				document.location.href = "change_inherent_status.php?id="+val.id;
				return false;										
			});
			
			$("#inherentactedit_"+val.id).live( "click", function() {
				document.location.href = "edit_inhrentrisk.php?id="+val.id;
				return false;										  
			});
			$("#inherentactdel_"+val.id).live( "click", function() {
				if( confirm(" Are you sure you want to delete this inherent risk ")) {													
				$.post( "controller.php?action=changeInherent", 
					   {
						   id		: val.id,
						   status	: 0
						}, function( delData ) {
					if( delData == 1){
						message.html("Inherent risk deactivated").animate({opacity:"0.0"},4000);
						$("#tr_"+val.id).fadeOut();
					} else if( delData == 0){
						message.html("No change was made to the inherent risk").animate({opacity:"0.0"},4000);
					} else {
						message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
					}										 
				});
				}
				return false;										  
			});
			$("#inherentactact_"+val.id).live( "click", function() {
				$.post( "controller.php?action=ihractactivate", {id:val.id}, function( actData ) {
					if( actData == 1){
						message.html("Inherent risk activated").animate({opacity:"0.0"},4000);
					} else if( actData == 0){
						message.html("No change was made to the inherent risk").animate({opacity:"0.0"},4000);
					} else {
						message.html("Error saving , please try again ").animate({opacity:"0.0"},4000);
					}										 
				})															 
				return false;										  
			});

		});
	}, "json");
	
	/**
		Adding a new inherent risk
	**/
	$("#add_inherent_risk").click( function() {
		var message 	= $("#inherent_risk_message")						
		var rating_from = $("#inherent_rating_from").val();
		var rating_to 	= $("#inherent_rating_to").val();
		var magnitude	= $("#inherent_risk_magnitude").val();
		var response	= $("#inherent_response").val();
		var color 		= $("#inherent_color").val();
		
		if ( rating_from == "") {
			message.html("Please enter the rating feom for this impact")
			return false;
		} else if( rating_to == "" ) {
			message.html("Please enter the rating to for this impact");
			return false;
		} else if ( magnitude == "" ) {
			message.html("Please enter the asssessment for this impact");
			return false;
		} else if( response == "" ) {
			message.html("Please enter the definition for this impact");
			return false;			
		} else {
			message.html("");
			$.post( "controller.php?action=newInherentRisk",
				   {
					   from		: rating_from,
					   to		: rating_to,
					   magnitude: magnitude,
					   response : response,
					   clr		: color
				}
				   , function( retData  ) {
					   if( retData > 0 ) {
						   $("#inherent_rating_from").val("")
						   $("#inherent_rating_to").val("")
						   $("#inherent_risk_magnitude").val("")
						   $("#inherent_response").val("")
						   message.html("Inherent risk exposure saved successifully ... ")
							$("#inherent_risk_table")
								.append($("<tr />")
								  .append($("<td />",{html:retData}))
								  .append($("<td />",{html:rating_from+" to "+rating_to}))
								  .append($("<td />",{html:magnitude}))
								  .append($("<td />",{html:response}))
								  .append($("<td />",{css:{"background-color":"#"+color}, html:color }))
								  .append($("<td />")
									.append($("<input />",{type:"submit", value:"Edit", id:"inherentactedit", name:"inherentactedit"}))	
									.append($("<input />",{type:"submit", value:"Del", id:"inherentactdel", name:"inherentactdel"}))					
								   )
								  .append($("<td />",{html:"<b>Active</b>"})
									//.append($("<input />",{type:"submit", value:"Activate", id:"inherentactact", name:"inherentactact"}))	
									//.append($("<input />",{type:"submit",value:"Inactivate", id:"inherentactdeact", name:"inherentactdeact"}))							
								   )
								  .append($("<td />")
										.append($("<input />",{type:"button", name:"change_"+retData, id:"change_"+retData, value:"Change Status"}))		
									)
								)										
							$("#change_"+retData).live("click", function(){
								document.location.href = "change_inherent_status.php?id="+retData;
								return false;										
							});	
					   } else {
						 message.html("There was an error saving the inherent risk exposure")  
						}
			},"json")
		}
		return false;							 
	});	   
			   
	})
