// JavaScript Document
$(function(){
	
	$.getJSON("controller.php?action=getGlossary", function( glossaryData ){
		var message = $("#glossary_message");
		if( $.isEmptyObject( glossaryData ) ) {
			message.html("No result ")
		} else {
			$.each( glossaryData , function( index, glossary ) {
				$("#glossary_table")
				 .append($("<tr />",{id:"tr_"+glossary.id})
					.append($("<td />",{html:glossary.category}))	   
					.append($("<td />",{html:glossary.terminology}))					
					.append($("<td />",{html:glossary.explanation}))
					.append($("<td />")
						.append($("<input />",{type:"button", name:"edit_"+glossary.id, id:"edit_"+glossary.id, value:"Edit"}))		  
						.append($("<input />",{type:"button", name:"del_"+glossary.id, id:"del_"+glossary.id, value:"Delete"}))							
					)
				  )
				 
				$("#edit_"+glossary.id).live("click", function(){
					document.location.href = "edit_glossary.php?id="+glossary.id;
					return false;
				});
				
				$("#del_"+glossary.id).live("click", function(){
					if( confirm("Are you sure you want to delete this glossary term") ) {
						$.post("controller.php?action=deleteGlossary", { id : glossary.id }, function( retData ) {
							if( retData = 1 ) {
								message.html("Glossary term deleted successifully ... ");	
								$("#tr_"+glossary.id).fadeOut();
							} else if( retData == 0 ) {
								message.html("There was no change made to the glossary term");
							} else {
								message.html("There was an error deleting this glossary item")	;
							}																	  
						});		
					}
					return false;
				});
				
			});
		} 	
		 $("#new_term")
			.append($("<tr />")
				.append($("<td />",{colspan:"4", align:"left"})
				 .append($("<input />",{type:"button", id:"new_glossary", name:"new_glossary", value:"New Glossary Term"}))		  
				)		  
			)
			$("#new_glossary").live("click", function(){
				$("#new_term_table").fadeIn();
				return false;									  
			});
			$("#cancel_glossary").live("click", function(){
				$("#new_term_table").fadeOut();
				return false;									  
			});
	});
	
	$("#save_glossary").live("click", function(){
		
		var message  	= $("#glossary_message");
		var category 	= $("#category").val(); 
		var terminology	= $("#terminolgy").val();
		var explanation = $("#explanation").val()
		
		if( category == "" ) {
			message.html("Please select category")
			return false;
		} else if ( terminology == "" ) {
			message.html("Please enter the termilogy")
			return false;
		} else if ( explanation == "" ){
			message.html("Please enter the explanation")
			return false;
		} else  {
			
			$.post("controller.php?action=saveGlossary",
			  {
				category 	: category,
				terminology : terminology,
				explanation	: explanation
			  }, function( retData ){
				if( retData > 0  ) {
					$("#category").val("")
					$("#terminolgy").val("")
					$("#explanation").val("")
					
					$("#glossary_table")
					.append($("<tr />",{id:"tr_"+retData})
						.append($("<td />",{html:category}))
						.append($("<td />",{html:terminology}))
						.append($("<td />",{html:explanation}))
						.append($("<td />")
							.append($("<input />",{type:"button", name:"edit_"+retData, id:"edit_"+retData, value:"Edit"}))	  
							.append($("<input />",{type:"button", name:"del_"+retData, id:"del_"+retData, value:"Delete"}))								
						)
					)
					

				$("#del_"+retData).live("click", function(){
					if( confirm("Are you sure you want to delete this glossary term") ) {
						$.post("controller.php?action=deleteGlossary", { id : retData }, function( resData ) {
							if( resData = 1 ) {
								message.html("Glossary term deleted successifully ... ");	
								$("#tr_"+retData).fadeOut();
							} else if( resData == 0 ) {
								message.html("There was no change made to the glossary term");
							} else {
								message.html("There was an error deleting this glossary item")	;
							}																	  
						});		
					}
					return false;
				});
				
				$("#edit_"+retData).live("click", function(){
					document.location.href = "edit_glossary.php?id="+retData;
					return false;
				});
					message.html("Glossary term saved");
				} else {
					message.html("Error saving the glossary term ");
				}
			});
		 }	
		 return false;
	})
	
});