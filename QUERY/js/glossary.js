// JavaScript Document
$(function(){
	
	$.getJSON("controller.php?action=getGlossary", function( glossaryData ){
		var message = $("#glossary_message");
		if( $.isEmptyObject( glossaryData ) ) {
			message.html("No result ")
		} else {
			$.each( glossaryData , function( index, glossary ) {
				$("#glossary_table")
				 .append($("<tr />",{id:"tr_"+glossary.id})
					.append($("<td />",{html:glossary.category}))	   
					.append($("<td />",{html:glossary.terminology}))					
					.append($("<td />",{html:glossary.explanation}))
					//.append($("<td />")
						//.append($("<input />",{type:"button", name:"edit_"+glossary.id, id:"edit_"+glossary.id, value:"Edit"}))
						//.append($("<input />",{type:"button", name:"del_"+glossary.id, id:"del_"+glossary.id, value:"Del"}))
					//)						
				  )
				 
				$("#edit_"+glossary.id).live("click", function(){
					document.location.href = "edit_glossary.php?id="+glossary.id;
					return false;
				});
				
				$("#del_"+glossary.id).live("click", function(){
					if( confirm("Are you sure you want to delete this glossary term") ) {
						jsDisplayResult("info", "info", "Deleting  ...  <img src='../images/loaderA32.gif' />");
						$.post("controller.php?action=deleteGlossary", { id : glossary.id }, function( retData ) {
							if( retData = 1 ) {
								jsDisplayResult("ok", "ok", "Glossary term deleted successfully . . .");
								$("#tr_"+glossary.id).fadeOut();
							} else if( retData == 0 ) {
								jsDisplayResult("info", "info", "There was no change made to the glossary term");
							} else {
								jsDisplayResult("error", "error", "There was an error deleting this glossary item");
							}																	  
						});		
					}
					return false;
				});
				
			});
		} 	
	});
	
	
	$("#save_glossary").live("click", function(){	
		var message  	= $("#glossary_message");
		var category 	= $("#category").val(); 
		var terminology	= $("#terminolgy").val();
		var explanation = $("#explanation").val()
		
		if( category == "" ) {
			jsDisplayResult("error", "error", "Please enter category");
			return false;
		} else if ( terminology == "" ) {
			jsDisplayResult("error", "error", "Please enter the termilogy");
			return false;
		} else if ( explanation == "" ){
			jsDisplayResult("error", "error", "Please enter the explanation");
			return false;
		} else  {
			jsDisplayResult("info", "info", "Saving  ...  <img src='../images/loaderA32.gif' />");
			$.post("controller.php?action=saveGlossary",
			  {
				category 	: category,
				terminology : terminology,
				explanation	: explanation
			  }, function( retData ){
				if( retData > 0  ) {
					$("#category").val("")
					$("#terminolgy").val("")
					$("#explanation").val("")
					
					$("#glossary_table")
					.append($("<tr />",{id:"tr_"+retData})
						.append($("<td />",{html:category}))
						.append($("<td />",{html:terminology}))
						.append($("<td />",{html:explanation}))
						.append($("<td />")
							.append($("<input />",{type:"button", name:"edit_"+retData, id:"edit_"+retData, value:"Edit"}))
							.append($("<input />",{type:"button", name:"del_"+retData, id:"del_"+retData, value:"Del"}))
						)
					)

				$("#del_"+retData).live("click", function(){
					if( confirm("Are you sure you want to delete this glossary term") ) {
						jsDisplayResult("info", "info", "Deleting  ...  <img src='../images/loaderA32.gif' />");
						$.post("controller.php?action=deleteGlossary", { id : retData }, function( response ) {
							if( response = 1 ) {
								jsDisplayResult("ok", "ok", "Glossary term deleted successfully . . .");
								$("#tr_"+retData).fadeOut();
							} else if( response == 0 ) {
								jsDisplayResult("info", "info", "There was no change made to the glossary term");
							} else {
								jsDisplayResult("error", "error", "There was an error deleting this glossary item");
							}																	  
						});		
					}
					return false;
				});
									
				$("#edit_"+retData).live("click", function(){
					document.location.href = "edit_glossary.php?id="+retData;
					return false;
				});					
					jsDisplayResult("ok", "ok", "Glossary term saved");
				} else {
					jsDisplayResult("error", "error", "Error saving the glossary term");
				}
			});
		 }
				return false;
	});
	
	
	$("#edit_glossary").click(function(){	
		var message  	= $("#glossary_messsage");
		var category 	= $("#category").val(); 
		var terminology	= $("#terminolgy").val();
		var explanation = $("#explanation").val()
		
		if( category == "" ) {
			jsDisplayResult("error", "error", "Please enter category");
			return false;
		} else if ( terminology == "" ) {
			jsDisplayResult("error", "error", "Please enter the termilogy");
			return false;
		} else if ( explanation == "" ){
			jsDisplayResult("error", "error", "Please enter the explanation");
			return false;
		} else  {
			jsDisplayResult("info", "info", "Updating  . . .   <img src='../images/loaderA32.gif' />");
			$.post("controller.php?action=updateGlossary",
			  {
				id			: $("#glossary_id").val(),
				category 	: category,
				terminology : terminology,
				explanation	: explanation
			  }, function( retData ){
				if( retData == 1 ) {
					jsDisplayResult("ok", "ok", "Glossary term updated");
				} else {
					jsDisplayResult("error", "error", "Error updating the glossary term ");
				}
			});
		 }
				return false;
	});		
	
	
	$("#cancel_glossary").live("click", function(){
		//$("#new_term_table").fadeOut();
		history.back();
		return false;										 
	});
});
