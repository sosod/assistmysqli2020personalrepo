// JavaScript Document
$(function(){
	
	$("#column_default").sortable({ disabled: true });
	
	$("em").css({"color":"#FF0000", "font-size":"0.8em"})	
	$("#activeColumns, #inactiveColumns").sortable({
		connectWith	: ".connectedSortable",
		update 		: function() {
						var activeColumnsOrder 		= $("#activeColumns").sortable("serialize");
						var inactiveColumnsOrder 	= $("#inactiveColumns").sortable("serialize");
						
						jsDisplayResult("info", "info", "Saving  ...  <img src='../images/loaderA32.gif' />");
						$.post("controller.php?action=saveColumnOrder",
							{
								active 		: activeColumnsOrder,
								inactive	: inactiveColumnsOrder
							},
						function( retData ){
								jsDisplayResult("ok", "ok", "Change saved . . .");
						});
		}
	}).disableSelection();   
			   
	$("#columnsMessage").html("Loading active columns ... <img src='/images/loaderA32.gif>'")
	$.getJSON("controller.php?action=getColumns" ,function( colData ) {
	$("#columnsMessage").html("");
		$("#columnsContent").html("");
		$("#columnsContent").append($("<ul />",{id:"activeColumns"}).addClass('connectedSortable'))
		$("#columnsContent").append($("<ul />",{id:"inactiveColumns"}).addClass('connectedSortable'))			
														   
		$("#activeColumns").html("");
		$.each( colData.active , function( index, column){
			if( column.name != "query_progress"){
				$("#activeColumns").append($("<li />",{html:column.client_terminology, id:"column_"+column.id}).addClass('ui-state-default'))
			}
		});

		$.each( colData.inactive , function( index, column){
			$("#inactiveColumns").append($("<li />",{html:column.client_terminology,id:"column_"+column.id}).addClass('ui-state-highlight'))					   
		});
		
	});
	
	$("#saveupdate").click( function() {
	$("#activeColumns, #inactiveColumns").sortable({
		connectWith	: ".connectedSortable",
		update 		: function() {
		}
	}).disableSelection();   				
		return false;							
	});
});