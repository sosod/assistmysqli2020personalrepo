$(function(){

    FinancialYear.get();

    $("#add_fin_year").click(function(e){
        FinancialYear.save();
        e.preventDefault();
    });
    
    $("#save_changes_fin_year").click(function(e){
        FinancialYear.update();
        e.preventDefault();
    });    
    

});

var FinancialYear   = {
    
    save        : function()
    {
	    var self = this;
		var message = $("#financial_year_message");
		var year_ending = $("#year_ending").val()
		var start_date  = $("#start_date").val()
		var end_date 	= $("#end_date").val()
		
		if(end_date == "" ){
			jsDisplayResult("error", "error", "Please select the end date for the year");
			return false;
		} else if( start_date ==  "") {
			jsDisplayResult("error", "error", "Please select the start date for the year");
			return false;			
		} else if( year_ending == "" ) {
			jsDisplayResult("error", "error", "Please select the year ending");
			return false;			
		} else {
			jsDisplayResult("info", "info", "Saving  ...  <img src='../images/loaderA32.gif' />");
		    $.post( "controller.php?action=financial_year", {
			   last_day		: year_ending,
			   start_date	: start_date,
			   end_date		: end_date 
			  }, function(response) {
                if(response.error) 
                {
                  jsDisplayResult("error", "error", response.text);
                } else {
                  //$("#tr_"+retFinancial).fadeOut();	
                  jsDisplayResult("ok", "ok", response.text);									
                  self.get();
		          $("#year_ending").val("");
		          $("#start_date").val("");
		          $("#end_date").val("");
                }
			    
		    },"json");
		}
		
    } ,
    
    update       : function()
    {
		var message = $("#editfinancial_year_message")
		jsDisplayResult("info", "info", "Updating  . . .  <img src='../images/loaderA32.gif' />");
		$.post( "controller.php?action=editFinancialYear",  {
			id  		: $("#fini_year_id").val(),
			last_day	: $("#edit_last_year").val(),
			start_date	: $("#edit_start_date").val(),
			end_date	: $("#edit_end_date").val() 
		}, function(response){
            if(response.error) 
            {
              jsDisplayResult("error", "error", response.text);
            } else {
              if(response.updated)
              {
                jsDisplayResult("ok", "ok", response.text);
              } else {
                jsDisplayResult("info", "info", response.text);
              }
              //$("#tr_"+retFinancial).fadeOut();	
              //self.get();
            }
		},"json")
    } ,
    
    get         : function()
    {
        var self = this;
        $(".financial_year").remove();
	    $.get("controller.php?action=getFinYear", function(finYear) {
           if($.isEmptyObject(finYear))
           {
              $("#financial_year_table").append("<tr class='financial_year'><td colspan='5'>There are no financial years setup.</td></tr>");
           } else {
              var html = []
              $.each(finYear, function(index, fin){
                  html = self._display(fin);
                  $("#financial_year_table").append(html.join(' '));
              });
           }
           $("#display_message").remove();
	    },"json");
    } ,
    
    _display    : function(fin)
    {
        var self = this;
        var html = [];
        html.push("<tr class='financial_year' id='tr_"+fin.id+"'>");
          html.push("<td>"+fin.id+"</td>");
          html.push("<td>"+fin.last_day+"</td>");
          html.push("<td>"+fin.start_date+"</td>");
          html.push("<td>"+fin.end_date+"</td>");
          html.push("<td>");
            html.push("<input type='button' name='edit_"+fin.id+"' id='edit_"+fin.id+"' value='Edit' />");
            html.push("<input type='button' name='delete_"+fin.id+"' id='delete_"+fin.id+"' value='Delete' />");
		
		    $("#edit_"+fin.id).live("click",function(){
			    document.location.href = "edit_financial_year.php?id="+fin.id;
			    return false;											  
		    });           
		    

			$("#delete_"+fin.id).live("click",function(){
			    if(confirm(" Are you sure you want to delete this financial year"))
			    {
				    jsDisplayResult("info", "info", "Updating  ...  <img src='../images/loaderA32.gif' />");				
				    $.post("controller.php?action=delFinYear",{id : fin.id}, function(response){
                        if(response.error) 
                        {
                          jsDisplayResult("error", "error", response.text);
                        } else {
                          jsDisplayResult("ok", "ok", response.text);
                          $("#tr_"+fin.id).fadeOut();	
                          self.get();
                        }
				    },"json");
			    }
			    return false;											  
			});
          html.push("</td>");
        html.push("</tr>");
        return html;
    }
    

}
