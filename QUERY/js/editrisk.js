// JavaScript Document
// Edit riks
$(function(){

	$("#add_another_recommendation").click( function(){
		$(this)
		.parent()
		.prepend($("<br /><br />"))
		.prepend($("<textarea />",{cols:"35", rows:"5", title:"200", name:"reco" }).addClass("recommend")
		)
		return false;									  
	});	
	
	$("#add_another_clientresponse").click( function(){
		$(this)
		.parent()
		.prepend($("<br /><br />"))
		.prepend($("<textarea />",{cols:"35", rows:"5", title:"200",  name:"client" }).addClass("clientresponse")
		)
		return false;									  
	});	
	
	$("#add_another_finding").click( function(){
		$(this)
		.parent()
		.prepend($("<br /><br />"))
		.prepend($("<textarea />",{cols:"35", rows:"5", title:"200", name:"find"}).addClass("find")
		)
		return false;									  
	});	
	/**
		Add another browse button to attach files
	**/
	$("#attach_another").click(function(){
		$(this)
		.parent()
		.prepend($("<br />"))
		.prepend($("<input />",{type:"file", name:"attachment"}).addClass('attach'))
		return false;									
	});
	//================================================================================================================
	/**
		Add New Risk
	**/
	$("#edit_risk").click(function(){
		var message 				= $("#risk_message").slideDown();
		var r_type 					= $("#query_type :selected").val()
		var r_risk_type				= $("#risk_type :selected").val()
		var r_category 				= $("#query_category :selected").val()
		var r_description 			= $("#query_description").val()
		var r_background			= $("#query_background").val();	
		var r_financial_exposure    = $("#financial_exposure :selected").val();
		var r_monetary_implication	= $("#monetary_implication").val();
		var r_detail 				= $("#risk_detail").val();
		var r_finding			 	= $("#finding").val();
		var r_recommendation		= $("#recommendation").val();
		var r_client_response		= $("#client_response").val();
		var r_query_reference 		= $("#query_reference").val();
		var r_directorate 			= $("#directorate :selected").val();
		var r_risk_level 			= $("#risk_level :selected").val()

		var r_responsiblity = "";
		$(".responsibility").each( function( index, val){
			if( $(this).val() == "" ){
				jsDisplayResult("error", "error", "Please select the one responsible");
				return false;
			} else {
				r_responsiblity  = $(this).val();	
			}									
		});
		var r_attachements = "";
		$(".find").each( function( index, val){
			if( index > 0 ) {
				r_finding += ","+$(this).val();
			}else{
				r_finding = $(this).val();	
			}
		});

		$(".recommend").each( function( index, val){
			if( index > 0 ) {
				r_recommendation += ","+$(this).val();
			}else{
				r_recommendation = $(this).val();	
			}
		});

		$(".clientresponse").each( function( index, val){
			if( index > 0 ) {
				r_client_response += ","+$(this).val();
			}else{
				r_client_response = $(this).val();	
			}
		});	
		
		if( r_type == "" ) {
			jsDisplayResult("error", "error", "Please select the type for this query");
			return false;
		} else if( r_category == "" ) {
			jsDisplayResult("error", "error", "Please select the query category for this query");
			return false;
		} else if( r_query_reference == "" ) {
			jsDisplayResult("error", "error", "Please enter the query reference for this query");
			return false;
		} else if( r_description == "" ) {
			jsDisplayResult("error", "error", "Please enter the description for this query");
			return false;
		} else if( r_background == "" ) {
			jsDisplayResult("error", "error", "Please enter the background for this query");
			return false;
		} else {
			jsDisplayResult("info", "info", "Updating query...  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=updateQuery",
			  {	id 					  : $("#riskid").val(),
				type 			 	  : parseInt(r_type),
				type_text			  : $("#query_type :selected").text(),
                risk_type	 	      : r_risk_type ,
				category		 	  : r_category,
				category_text		  : $("#query_category :selected").text(),				
				description 	 	  : r_description ,
			    background		 	  : r_background ,				
				financial_exposure	  : parseInt( r_financial_exposure ),	
				monetary_implication  : r_monetary_implication ,
				risk_detail		  	  : r_detail,				
				finding	  			  : r_finding , 
				recommendation		  : r_recommendation,					
				query_reference 	  :	r_query_reference,
				user_responsible 	  : r_directorate ,
				risk_level 	  		  : r_risk_level ,				
				client_response	  	  : r_client_response,
				user_responsible_text : $("#directorate :selected").text()
			  } ,
			  function( response ) {
					if( response.updated ) {
						if( response.error )
						{
							jsDisplayResult("error", "error", response.text );
						} else {
							jsDisplayResult("ok", "ok", response.text );
						}
					} else {
						jsDisplayResult("error", "error", response.text );					}
			  } ,
			 "json");
		}				  
		return false;						  
	});
	//================================================================================================================
	$("#save_update").click(function(){
		var message 				= $("#risk_message").slideDown("fast");
		var r_type 					= $("#query_type :selected").val()		
		var r_financial_exposure    = $("#financial_exposure :selected").val();
		var r_monetary_implication	= $("#monetary_implication").val();
		var r_detail 				= $("#risk_detail").val();
		var r_finding			 	= $("#finding").val();
		var r_recommendation		= $("#recommendation").val();
		var r_client_response		= $("#client_response").val();
		var r_risk_level 			= $("#risk_level :selected").val()
						
		var r_user_responsible 		= $("#userrespoid").val();
		var r_response		 		= $("#risk_response").val();	
		var r_status		 		= $("#risk_status :selected").val();
		
		$(".find").each( function( index, val){
			if( index > 0 ) {
				r_finding += ","+$(this).val();
			}else{
				r_finding = $(this).val();	
			}
		});

		$(".recommend").each( function( index, val){
			if( index > 0 ) {
				r_recommendation += ","+$(this).val();
			}else{
				r_recommendation = $(this).val();	
			}
		});

		$(".clientresponse").each( function( index, val){
			if( index > 0 ) {
				r_client_response += ","+$(this).val();
			}else{
				r_client_response = $(this).val();	
			}
		});	

		if( r_response == "" ) {
			jsDisplayResult("error", "error", "Please select the response");
			return false;
		}  else {
			jsDisplayResult("info", "info", "Saving query update...  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=newRiskUpdate",
			  {
				id					  : $("#riskid").val(),
				response 			  : r_response,
			    status		 	  	  : r_status ,
				type 			 	  : r_type ,
				financial_exposure	  : r_financial_exposure ,	
				monetary_implication  : r_monetary_implication ,
				risk_detail		  	  : r_detail,				
				finding	  			  : r_finding , 
				recommendation		  : r_recommendation,					
				risk_level 	  		  : r_risk_level ,				
				client_response	  	  : r_client_response,
				user_responsible 	  : r_user_responsible 
			  } ,
			  function( response ) {		  
				 if( response.updated )
				 {
					 if( response.error )
					 {
						 jsDisplayResult("error", "error", response.text ); 
					 } else {
						 jsDisplayResult("ok", "ok", response.text );
					 }
				 } else {
					 jsDisplayResult("error", "error", response.text );
				 }
			  } , "json");
		}		
		return false;
	});
	
	$("#delete_risk").click(function(){
		var message 	= $("#risk_message").slideDown();	
		var userRespo 	=  $("#risk_user_responsible").val()
		if( confirm( "Are you sure you want to delete this risk ") ) {
			jsDisplayResult("info", "info", "Deleting query ...  <img src='../images/loaderA32.gif' />");
			$.post("controller.php?action=deleteRisk" ,
				{ 
					id 			: $("#riskid").val(),
					userRespo	: $("#risk_user_responsible :selected").val()
					}	 ,
				function( response ) {
					if( response.deleted )
					{
						if( response.error )
						{
							jsDisplayResult("error", "error", response.text );
						} else {
							jsDisplayResult("ok", "ok", response.text );
						}							   								
					} else {
						jsDisplayResult("error", "error", response.text );
					} 
				}
			,"json")
		}
		return false;							 
	});
	
	$("#view_risk_log").click( function(){
		$("#view_log").html("<img src='../images/loaderA32.gif' />");
		$.getJSON("controller.php?action=getRiskUpdates",{ id : $("#riskid").val()},function( riskUpdateData ){
			if( $.isEmptyObject( riskUpdateData ) ){
				$("#view_log").html("There were no updates logged yet");
			} else {
				$("#view_log").html("");
				$("#view_log")
				 .append($("<table />",{id:"riskupdate_log_table", width:"70%"})
				  .append($("<tr />")
					.append($("<th />",{html:"Date"}))
					.append($("<th />",{html:"Audit Log"}))
					//.append($("<th />",{html:"Progress"}))
					.append($("<th />",{html:"Status"}))	
				   )		   
			     )
				// .toggle();

				$.each( riskUpdateData, function( index, update){
				 $("#riskupdate_log_table")
				  .append($("<tr />")
					.append($("<td />",{html:update.date}))
					.append($("<td />",{html:update.changeMessage}))
					//.append($("<td />",{html:""}))
					.append($("<td />",{html:update.status}))
				  )
				});
			}
		});
		return false;
	});
	
	$("#viewEditsLog").click( function() {
		$("#editLogs").html("<img src='../images/loaderA32.gif' />");
		$.post("controller.php?action=getRiskEdits",{ id : $("#riskid").val() }, function( riskData ) {
																						  
																						  
			$("#editLogs").html("");
			if( $.isEmptyObject( riskData ) ){
				$("#editLogs").append($("<tr />")
				 .append($("<td />",{colspan:"9",html:"No risk edits have been done on this risk item"}))						
				);
			} else {
			$("#editLogs").append($("<table />",{id:"risk_edit_logs"})
					.append($("<tr />")
					 .append($("<th />",{html:"Date"}))
					 .append($("<th />",{html:"Audit Log"}))
					 .append($("<th />",{html:"Status"}))									 
				)
			)
			$.each( riskData, function( index, riskEdit ) {
				$("#risk_edit_logs").append($("<tr />")
					.append($("<td />",{html:riskEdit.date}))
					.append($("<td />",{html:riskEdit.changeMessage}))
					.append($("<td />",{html:riskEdit.status}))	
				)					   
			});
		  }
		},"json");	
		return false;
	});
	
	$("#cancel_update").click(function(){
		history.back();							   
		return false;
	});
	
	$(".remove_attach").click(function(){
		if(confirm("Are you sure you want to remove this attachement")){
			var id 		 = this.id;
			var file 	 = $(this).attr('file');
			var original = $(this).attr('alt'); 
			
			$.post("controller.php?action=removeFile",{ 
				   filename 	: file, 
				   originalname : original,
				   timeid		: id
				   },function( response ){
				$("#risk_message").slideDown().html( response.text )
					$("#parent_"+id).remove();
			},'json');									
		}
		return false;							   
	});
	
});



//JavaScript Document
$(function(){ 
	//set the with of the select to be almost uniform	   
   
	//set the table th text to align to the left
	$("table#risk_new").find("th").css({"text-align":"left"})


	$(".more_details").hide();

	$("#more_detail").click(function(){			 
		$(".more_details").toggle();
		if($(this).val() == "Less Detail"){
			$(this).val("More Detail");
		} else {
			$(this).val("Less Detail");
		}
		return false;
	});
	
	$(".datepicker").live("focus", function(){
		$(this).datepicker({	
			changeMonth:true,
			changeYear:true,
			dateFormat : "dd-M-yy"
		});						   
	});	
	//===============================================================================================================
	/**
		get all the risk categories
	**/									
		$.get( "controller.php/?action=getRiskLevel", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				$("#risk_level")
				.append($("<option />",{text:val.name, value:val.id, selected:((val.status & 4) == 4 ?  "selected" : "")}));
			})											 
		},"json");	
	//================================================================================================================
	/**
		get all the risk impacts
	**/
	$.get( "controller.php/?action=getFinancialExposure", function( data ) {
		$.each( data ,function( index, val ) {
			$("#financial_exposure")
			.append($("<option />",{text:val.name, value:val.id,  selected:((val.status & 4) == 4 ?  "selected" : "")}));
		})			
	},"json");
  //======================================================================================================================
	/**
		get all the risk types
	**/
	$.get( "controller.php/?action=getQueryType", function( data ) {
		$.each( data ,function( index, val ) {
			$("#query_type")
			.append($("<option />",{text:val.name, value:val.id}));
		})
	},"json");
  //======================================================================================================================
	/**
		get all the risk types
	**/
	$.get( "controller.php/?action=getActiveRiskType", function( data ) {
		$.each( data ,function( index, val ) {
			$("#risk_type")
			.append($("<option />",{text:val.name, value:val.id}));
		})											 
	},"json");
	//===============================================================================================================
	/**
		get all the risk categories
	**/
	$("#query_type").live("change", function() {
		$("#query_category").html("");
		$("#query_category").append($("<option />",{value:"", text:"--query category--"}))											
		$.get( "controller.php/?action=getCategory", { id : $(this).val() }, function( data ) {
			$.each( data ,function( index, val ) {
				$("#query_category")
				.append($("<option />",{text:val.name, value:val.id}));
			})											 
		},"json");	
		return false;
	});

	//================================================================================================================	
	/**
		get all the risk percieved control effectiveness
	**/
	$.get( "controller.php/?action=getUsers", function( data ) {
		$.each( data ,function( index, val ) {
			$("#query_user_responsible")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
			
			$("#action_owner")
			.append($("<option />",{text:val.tkname, value:val.tkid}));
		})											 
	},"json");	
	//================================================================================================================	
	$.get("controller.php?action=getDirectorate", function( direData ){
	$.each( direData ,function( index, directorate ) {
			$("#directorate")
			.append($("<option />",{text:directorate.dirtxt	, value:directorate.subid}));
		})
	}, "json");
	/*================================================================================================================	
	$.get("controller.php?action=getSubDirectorate", function( subdireData ){													  
		$.each( subdireData ,function( index, subdirectorate ) {
			$("#subdirectorate")
			.append($("<option />",{text:subdirectorate.shortcode, value:subdirectorate.tkid}));
		})
	}, "json");
	*///================================================================================================================
	
	$("#add_another_recommendation").click( function(){
		$(this)
		.parent()
		.prepend($("<br /><br />"))
		.prepend($("<textarea />",{cols:"35", rows:"5", title:"200", name:"reco" }).addClass("recommend")
		)
		return false;									  
	});	
	
	$("#add_another_clientresponse").click( function(){
		$(this)
		.parent()
		.prepend($("<br /><br />"))
		.prepend($("<textarea />",{cols:"35", rows:"5", title:"200",  name:"client" }).addClass("clientresponse")
		)
		return false;									  
	});	
	
	$("#add_another_finding").click( function(){
		$(this)
		.parent()
		.prepend($("<br /><br />"))
		.prepend($("<textarea />",{cols:"35", rows:"5", title:"200", name:"find"}).addClass("find")
		)
		return false;									  
	});		
	//================================================================================================================	
	/**
		Add another browse button to attach files
	**/
	$("#another_attach").click(function(){
		$(this)
		.parent()
		.prepend($("<br /><br />"))		
		.prepend($("<input />",{type:"file", name:"attachment"}).addClass('attach'))
		return false;									
	});
	//================================================================================================================
	$("#upload").click(function(){
				/**
			Ajax file uploading
		**/

		/**
			End ajax file upload
		**/
		return false;
	});
	//================================================================================================================
	/**
		Add New Risk
	**/
	$("#add_query").click(function() {

		var message 				= $("#risk_message").slideDown("fast");
		var r_type 					= $("#query_type :selected").val()
        var r_risk_type 		    = $("#risk_type :selected").val()
		var r_category 				= $("#query_category :selected").val()
		var r_description 			= $("#query_description").val()
		var r_background			= $("#query_background").val();	
		var r_financial_exposure    = $("#financial_exposure").val();
		var r_monetary_implication	= $("#monetary_implication").val();
		var r_detail 				= $("#risk_detail").val();
		var r_risk_level		 	= $("#risk_level :selected").val();
		var r_query_date		 	= $("#query_date").val();
		var r_query_deadline_date	= $("#query_deadline_date").val();
		//var r_client_response		= $("#client_response").val();
		var r_query_reference 		= $("#query_reference").val();
		var r_directorate 			= $("#directorate :selected").val();
		//var r_subdirectorate 		= $("#subdirectorate").val()
		
	
		var r_responsiblity = "";
		$(".responsibility").each( function( index, val){
			if( $(this).val() == "" ){
				message.html("Please select the one responsible ");
				return false;
			} else {
				r_responsiblity  = $(this).val();	
			}									
		});
		var r_attachements = "";
		$(".find").each( function( index, val){
			if( index > 0 ) {
				r_finding += ","+$(this).val();
			}else{
				r_finding = $(this).val();	
			}
		});

		$(".recommend").each( function( index, val){
			if( index > 0 ) {
				r_recommendation += ","+$(this).val();
			}else{
				r_recommendation = $(this).val();	
			}
		});

		$(".clientresponse").each( function( index, val){
			if( index > 0 ) {
				r_client_response += ","+$(this).val();
			}else{
				r_client_response = $(this).val();	
			}
		});			
		if( r_type == "" ) {
			jsDisplayResult("error", "error", "Please select the type for this query");
			return false;
		} else if( r_category == "" ) {
			jsDisplayResult("error", "error", "Please select the risk category for this query");			
			return false;
		} else if( r_query_reference == "" ) {
			jsDisplayResult("error", "error", "Please enter the query reference for this query");			
			return false;
		} else if( r_description == "" ) {
			jsDisplayResult("error", "error", "Please enter the description for this query");			
			return false;
		} else if( r_background == "" ) {
			jsDisplayResult("error", "error", "Please enter the background for this query");			
			return false;
		} else if( r_query_date == "" ) {
			jsDisplayResult("error", "error", "Please enter the query date for  this query");			
			return false;
		} else if( r_query_deadline_date == "") {
			jsDisplayResult("error", "error", "Please enter the query deadline date for this query");			
			return false;				
		} else  {
			jsDisplayResult("info", "info", "Saving Query . . . <img src='../images/loaderA32.gif' >");
			$.post( "controller.php?action=saveQuery",
			  {
				type 			 	  : r_type ,
				type_text			  : $("#query_type :selected").text(),
				category		 	  : r_category ,
				category_text		  : $("#query_category :selected").text(),				
				description 	 	  : r_description ,
				risk_level 	 	  	  : r_risk_level ,
                risk_type             : r_risk_type,
				query_date 	 	  	  : r_query_date ,
				query_deadline_date	  : r_query_deadline_date,
			    background		 	  : r_background ,				
				financial_exposure	  : r_financial_exposure ,	
				monetary_implication  : r_monetary_implication ,
				risk_detail		  	  : r_detail,				
				finding	  			  : r_finding , 
				recommendation		  : r_recommendation,					
				query_reference 	  :	r_query_reference,
				user_responsible 	  : r_directorate ,
				client_response	  	  : r_client_response,
				user_responsible_text : $("#directorate :selected").text(),					
				attachment			  : r_attachements
			  } ,
			  function( response ) {				  
				 if( response.saved  ) {
					
					$("#riskid").val( response.id )
					$("#idrisk").html( response.id )					
					$("select#query_type option[value='']").attr("selected", "selected");
                     $("select#risk_type option[value='']").attr("selected", "selected");
					$("select#query_category option[value='']").attr("selected", "selected");					
					$("select#directorate option[value='']").attr("selected", "selected");					
					$("#query_description").val("");
					$("select#risk_level option[value='']").attr("selected", "selected");					
					$("#query_date").val("");					
					$("#query_background").val("");
					$("#financial_exposure").val("");
					$("#monetary_implication").val("");
					$("#risk_detail").val("");
					$("#finding").val("");
					$("#recommendation").val("");
					$("#client_response").val("");
					$("#query_reference").val("");	
					$(".controls").val("");
					
					if( response.error )
					{
						jsDisplayResult("error", "error", response.text);
					} else {
						jsDisplayResult("ok", "ok", response.text);						
					}
				} else {
					jsDisplayResult("error", "error", response.text);				}
			  } ,
			 "json");
		}				  
		return false;						  
	});
	//================================================================================================================		
	/**
		Show form to add action for this risk 
	**/
	$("#show_add_action").click(function(){
		if( $("#riskid").val() == "") {
			$("#risk_message").html("There was no risk enter associated with this action")	
			return false;			
		} else {
			document.location.href = "action.php";
		}
		//$("#actions_table").fadeIn();	
		return false;
	});
	/**
		Adding action
	**/
	$("#add_action").click( function() {	
		var message 		= $("#riskaction_message")
		var rs_action 		= $("#action").val()
		var rs_action_owner = $("#action_owner :selected").val()		
		var rs_deliverable  = $("#deliverable").val()
		var rs_timescale    = $("#timescale").val()		
		var rs_deadline     = $("#deadline").val()
		var rs_remindon     = $("#remindon").val()	
		
		 if( rs_action == "" ) {
			message.html("Please enter the action ")	
			return false;
		} else if( rs_action_owner == "" ) {
			message.html("Please select the action owner for this risk ")	
			return false;				
		} else if( rs_deliverable == "" ) {
			message.html("Please enter the deliverable for this action ")	
			return false;				
		} else if( rs_timescale == "" ) {
			message.html("Please enter the time scale for this action ")	
			return false;					
		} else if( rs_deadline == "" ) {
			message.html("Please enter the deadline for this action ")	
			return false;					
		} else if( rs_remindon == "" ) {
			message.html("Please enter the remond on for this action ")	
			return false;					
		} else {
			message.html( "<img src='../images/loaderA32.gif' />" )
			$.post( "controller.php?action=newRiskAction" , 
				   {
					   	id 				: $("#riskid").val(),
						r_action 		: rs_action ,
						action_owner    : rs_action_owner ,
						deliverable     : rs_deliverable ,
						timescale       : rs_timescale ,
						deadline     	: rs_deadline ,
						remindon     	: rs_remindon
				   } ,
				   function( retData ) {
					  if( retData > 0 ) {
					   $("#action").val("")
					   $("#deliverable").val("")
					   $("#timescale").val("")
					   $("#deadline").val("")
					   $("#remindon").val("")
					   $("select#action_owner option[value='']").attr("selected", "selected");
					   
					  $("#actions_table")
					  .append($("<tr />")
						.append($("<td />",{html:retData}))
						.append($("<td />",{html:rs_action}))
						.append($("<td />",{html:$("#action_owner :selected").text()}))
						.append($("<td />",{html:rs_deliverable}))
						.append($("<td />",{html:rs_timescale}))
						.append($("<td />",{html:rs_deadline}))
						.append($("<td />",{html:rs_remindon}))	
						.append($("<td />")
							.append($("<input />",{type:"submit", value:"Edit", id:"edit_action_"+retData}))
							.append($("<input />",{type:"submit", value:"Del", id:"delete_action_"+retData}))							
						)	
						//.append($("<td />")
							//.append($("<input />",{type:"submit", value:"InActivate", id:"deactivate_action_"+retData}))							
						//)							
					   )
						message.html( "Risk Action Saved" )	.animate({
							opacity:"0.76"
						},5000).html( "" )
										
						$("#edit_action_"+data).live( "click", function(){
							document.location.href = "../actions/edit_action.php?id="+retData;									
							return false;									   
						});
						
						$("#delete_action_"+retData).live( "click", function(){
							if(confirm("Are you sure you want to delete this action")){
								$.post("controller.php?action=deleteAction",{ id : retData}, function( del ){
									if( del == 1 ) {
										message.html( "Risk action deleted" )																								
									} else if( del == 0 ){
										message.html( "Risk action already deactivated" )	
									} else {
										message.html( "Error deleting risk action" )	
									}
								},"json");
							}
							//document.location.href = "../actions/action_edit.php?id="+$("#riskid").val();									
							return false;									   
						});			
						
						$("#activate_action_"+retData).live( "click", function(){
							$.post("controller.php?action=activateAction",{ id : retData}, function( act ){
								if( act == 1) {
									message.html( "Risk action activated" );
								} else if( act == 0) {
									message.html( "Risk action is already active" );
								} else {
									message.html( "Errod activating risk action" );
								}														
							},"json");																			
   							return false;									   
						});	

						$("#deactivate_action_"+retData).live( "click", function(){
							$.post("controller.php?action=deleteAction",{ id : retData}, function( deact ){
								if( deact == 1) {	
									message.html( "Risk deactivated" );																
								} else  if( deact == 0 ){
									message.html( "Risk action is already deactivated" )
								} else {
									message.html( "Error deactivating risk action " )
								}
							},"json");																			  
							return false;									   
						});	
						
					  } else {
						 if( !$.isEmptyObject( retData ) ) {
							message.html("Error : "+retData);  
						 } else {
							message.html-->("There was an error saving risk , please try again");  
						 }
					  }
				   } ,
			"json");
		}
		return false;	
	});
	$("#btn").live( "", function(){					 
	})
});

