$(function(){
	
	$(".textcounter").textcounter({maxLength:100});
	
	if( $("#risk_category_id").val() == undefined )
	{
		QueryCategory.get();
		QueryCategory.getQueryType();
	}
	
	$("#change_riskcategory").click(function(){
		QueryCategory.change();
		return false;
	})
	
	$("#edit_riskcategory").click(function(){
		QueryCategory.edit();
		return false;
	})
	
	$("#add_riskcategory").click( function() {
		QueryCategory.save();
		return false;
	})
	
});


var QueryCategory		= {
		
		getQueryType	: function()
		{
			$.getJSON("controller.php?action=getQueryType", function( data) {
				$.each( data, function( index , val) {
					//$("#category_risktype").append($("<option />",{html:val.name, value:val.id}))					
				});				
			});
	
		} ,
		
		get				: function()
		{
			$.getJSON( "controller.php?action=getCategory", function( data ) {
				$.each( data, function( index, val){
					QueryCategory.display( val );					
				});				
			})				
		} , 
		
		edit			: function()
		{
			var category    = $("#risk_category").val();
			var shortcode   = $("#category_shortcode").val();
			var description = $("#category_description").val();

			if ( shortcode == "") {
				jsDisplayResult("error", "error", "Please enter the short code for this category");	
				return false;
			} else if( category == "") {
				jsDisplayResult("error", "error", "Please enter the category");	
				return false;
			} else {
				jsDisplayResult("info", "info", "Saving  . . .  <img src='../images/loaderA32.gif' />");
				$.post( "controller.php?action=updateCategory", 
					  	{ 
						  id				: $("#risk_category_id").val(),
						  category		: category,
						  name			: category,
						  shortcode		: shortcode,
						  description		: description
					    },
						function( retData ){
							if( retData == 1 ) {
								jsDisplayResult("ok", "ok", "Category successfully updated . . .");						
							} else if( retData == 0 ){
								jsDisplayResult("info", "info", "Category not changed . . .");
							} else {
								jsDisplayResult("info", "info", "Error updating the category . . .");
							}
				}, "json");	
			}
		} ,
		
		save			: function()
		{
			var category    = $("#risk_category").val();
			var shortcode   = $("#category_shortcode").val();
			var description = $("#category_description").val();

			if ( shortcode == "") {
				jsDisplayResult("error", "error", "Please enter the short code for this category");	
				return false;
			} else if( category == "") {
				jsDisplayResult("error", "error", "Please enter the category");	
				return false;
			} else {
				jsDisplayResult("info", "info", "Saving  . . .  <img src='../images/loaderA32.gif' />");
				$.post( "controller.php?action=newCategory", 
					  	{ 
						  category		: category,
					 	  shortcode		: shortcode,
						  description   : description
						},
						function( retData ){
						if( retData > 0 ) {
							$("#risk_category").val("");
							$("#category_shortcode").val("");
							$("#category_description").val("");
							jsDisplayResult("ok", "ok", "Risk category succesfully added . . .");
							var data = { id : retData, shortcode:shortcode, name : category, descr : description , risktype : $("#category_risktype :selected").text(), active : 1}
							QueryCategory.display(data)
						} else {
							jsDisplayResult("error", "error", "There was an error , please try again . . .");
						}
							
					}, "json");	
			}
		} ,
		
		change			: function()
		{
			jsDisplayResult("info", "info", "Updating  . . .  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=catdeactivate", 
				   { id		: $("#risk_category_id").val(),
				   	 active : $("#category_status :selected").val()
				   }, function( catdeaData ) {
				if( catdeaData == 1){
					jsDisplayResult("ok", "ok", "Category status changed . . .");
				} else if( catdeaData == 0){
					jsDisplayResult("info", "info", "No change was made to the category . . .");
				} else {
					jsDisplayResult("error", "error", "Error saving , please try again . . .");
				}									 
			});	
		} , 
		
		display			: function( val )
		{
			 $("#riskcategory_table")
			  .append($("<tr />",{id:"tr_"+val.id})
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.shortcode}))
				.append($("<td />",{html:val.name}))
				.append($("<td />",{html:val.descr}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"catedit_"+val.id, name:"catedit_"+val.id }))
				  .append($("<input />",{type:"submit", value:"Del", id:"catdel_"+val.id, name:"catdel_"+val.id }))			  
				 )
				.append($("<td />",{html:"<b>"+(val.active==1 ? "Active"  : "Inactive" )+"</b>"}))	
				.append($("<td />")
				   .append($("<input />",{type:"submit", name:"change_"+val.id, id:"change_"+val.id, value:"Change Status"}))
				)
			  )	
			  
			  $("#change_"+val.id).live("click", function(){
				document.location.href = "change_category_status.php?id="+val.id;
			  	return false;													  
			  });
			  
			  $("#catedit_"+val.id).live( "click", function(){
				document.location.href = "edit_risk_category.php?id="+val.id;
			  	return false;
			  });
			  
			  $("#catdel_"+val.id).live( "click", function(){
				if( confirm(" Are you sure you want to delete this query category ")) {
					jsDisplayResult("info", "info", "Deleting  . . .  <img src='../images/loaderA32.gif' />");
					$.post( "controller.php?action=catdeactivate", {id:val.id, active:2}, function( catdelData ) {
						if( catdelData == 1){
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("ok", "ok", "Category deleted");
						} else if( catdelData == 0){
							$("#tr_"+val.id).fadeOut();
							jsDisplayResult("info", "info", "No change was made to the category");
						} else {
							jsDisplayResult("error", "error", "Error saving , please try again");
						}								 
					});	
				}
			  	return false;
			  });		  
			  	  
		}
		
		
		
}
