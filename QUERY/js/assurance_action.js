// JavaScript Document
$(function(){

	$(".datepicker").datepicker({	
		showOn			: "button",
		buttonImage 	: "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		:true,
		changeYear		:true,
		dateFormat 		: "dd-M-yy"
	});		

	$.get( "controller.php?action=getAAction", { id : $("#action_id").val() }, function( actionData ){
			//$("#action_information_table")
		
			$("#goback").before($("<tr />")
				.append($("<th />",{html:"Action #:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.id}))		  				
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:"Action:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.action}))		  				
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:"Deliverable:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.deliverable}))		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:"Action Owner:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.tkname+" "+actionData.tksurname}))		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:"Time Scale:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.timescale}))		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:"Deadline:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.deadline}))		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:"Status:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.status}))		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:"Progress:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:(actionData.progress == "" ? "0" : actionData.progress)+"%"}))		  		  
			)
			$("#goback").before($("<tr />")
				.append($("<th />",{html:"Remind On:"}).css({"text-align":"left"}))		  
				.append($("<td />",{html:actionData.remindon}))		  		  
			)
	},"json");
		
/*	$.post( "controller.php?action=getActionAssurances",{id:$("#action_id").val()}, function( actionAssuranceData ){																	 
		if( $.isEmptyObject( actionAssuranceData ) ){
		} else{	
		date = new Date();
		$.each( actionAssuranceData, function(index, val){
			$("#list_assurance_action_table")
			.append($("<tr />")
				.append($("<td />",{html:date.getYear()+"-"+date.getMonth()+"-"+date.getDay()}))		  
				.append($("<td />",{html:val.insertdate}))		  
				.append($("<td />",{html:val.response}))		  
				.append($("<td />",{html:(val.signoff == 0 ? "no" : "yes") }))		  
				.append($("<td />",{html:val.tkname+" "+val.tksurname}))		  
				.append($("<td />",{html:val.attachment}))		  
				)								 
		});
		}	

		
		$("#showadd_new_action_assurance").live( "click", function(){
			$("#new_action_assurance").show();
			return false;
		});
	}, "json");
	*/
	$("#add_newAction_assurance").live( "click", function(){
		
		var message 	= $("#newassurance_message")
		var response  	= $("#assurance_response").val();
		var signoff  	= $("#assurance_signoff :selected").val();
		var attachment 	= $("#assurance_attachment").val();	
		var date_tested = $("#date_tested").val()
		
		if( response == "" ) {
			message.html(" Please enter the response for the assurance")
			return false;
		} else if( signoff == "") {
			message.html(" Please select the signoff for the assurance")
			return false;
		} else {
			$.post( "controller.php?action=newAssuranceAction", {
				   id			:	$("#action_id").val(), 
				   response		:	response, 
				   signoff		: 	signoff,
				   attachment	: 	attachment,
				   date_tested	:   date_tested
				   }, function( retAssurance ){
					  if( retAssurance > 0 ) {
						  $("#assurance_response").val("");
						  $("select#assurance_signoff options[value='']").attr("selected","selected");
						  $("#assurance_attachment").val("");
						  
						  date = new Date();
						  var dateTested = date_tested.split("/");
						 $("#list_assurance_action_table")
						  .append($("<tr />")
							.append($("<td />",{html:date.getFullYear()+" - "+date.getMonth()+" - "+date.getDate()}))
							.append($("<td />",{html: dateTested[2]+"-"+dateTested[0]+"-"+dateTested[1]  }))
							.append($("<td />",{html:response}))
							.append($("<td />",{html:$("#assurance_signoff :selected").text()}))
							.append($("<td />",{html:response}))
							.append($("<td />",{html:""}))
						   )
						message.html("Action assurance saved successfully ...")
					  } else {
						message.html("Error updating action assurance.")						  
					  }
				  })	
		}
		return false;											 
	});
	
	/**
		Add another browse button to attach files
	**/
	$("#attach_another").click(function(){
		$(this)
		.parent()
		.prepend($("<br />"))		
		.prepend($("<input />",{type:"file", name:"attachment"}).addClass('attach'))
		return false;									
	});
	
	$("#cancel_newAction_assurance").live("click", function(){
		$("#new_action_assurance").fadeOut();
		return false;														
	})
})