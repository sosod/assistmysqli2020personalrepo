// JavaScript Document
$(function(){
		   
			$("#changes_risktype").click(function() {
				var message = $("#change_risk_type_message")
				$.post( "controller.php?action=changeQueryStatus",
					   { id		: $("#risktype_id").val() ,
					   	 status : $("#type_statuses :selected").val()
					   }, function( typDedeactData ) {
					if( typDedeactData == 1) {
						message.html("Risk Type status changed ").stop().animate({opacity:"0.0"},4000);
					} else if( typDedeactData == 0) {
						message.html("No change was made to the risk type status ").stop().animate({opacity:"0.0"},4000);
					} else {
						message.html("Error saving , please try again ").stop().animate({opacity:"0.0"},4000);
					}										 
				})									
				return false;										  
			});
			
})