$(function(){
	$("table#edit_action_table").find("th").css({"text-algn":"left"})

	$(".datepicker").datepicker({	
		showOn			: "both",
		buttonImage 	: "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		: true,
		changeYear		: true,
		dateFormat 		: "dd-M-yy"
	});
	$("#cancel_action").click(function(){
		history.back();
		return false;
	})
	
	$("#status").change(function(e) {
	    var thisValue = $(this).val();
	    if(thisValue == 3)
	    {
	      $("#progress").val(100);
	      $("#approval").removeAttr("disabled");
	    } else {
	      if( $("#progress").val() == 100 && $("#_progress").val() != 100)
	      {
	          $("#progress").val($("#_progress").val());
	      }
	      $("#approval").attr("disabled", "disabled");
	    }	    
	   e.preventDefault();
	});
	
	$("#progress").blur(function(e) {
	    var thisValue = $(this).val();
	    if(thisValue == 100)
	    {
	      $("#status").val(3);
	      $("#approval").removeAttr("disabled");
	    } else {
	       if( $("#status").val() == 3 && $("#_status").val() != 3)
	       {
	          $("#status").val( $("#_status").val() );
	       }
	      $("#approval").attr("disabled", "disabled");
	    }	    
	   e.preventDefault();
	});		
	

	
	if( $("#actionid").val() == undefined)
	{
		Action.getUsers();
		Action.getStatus();
	}
	
	$("#save_action").live( "click", function() {
		Action.save();
		return false;
	})
	
	$("#cancel").live( "click", function(){
		history.back();	
		return false;
	});	
	
	$("#delete_action").live( "click", function(){
		Action.deleteAction();
		return false;
	});
	
	$("#edit_action").click(function(){
		Action.edit();
		return false;
	});	
	
	$("#save_action_update").click(function(){
		Action.update();
		return false;
	});
	
	$(".upload_action").live("change", function(e){
	  Action.uploadAttachment(this.id);
	  e.preventDefault();
	});
	
    $(".remove_attach").live("click", function(e){
          var id       = this.id
          var ext      = $(this).attr('title');
          var type     = $(this).attr('ref');
          var filename = $(this).attr("file");
          $.post('controller.php?action=deleteActionAttachment', 
          {
              attachment : id,
              ext        : ext,
              id         : $("#action_id").val(),
              type       : type,
              filename   : filename
          }, function(response){
            if(response.error)
            {
                $("#result_message").css({padding:"5px", clear:"both"}).html(response.text)
            } else {
                $("#result_message").addClass('ui-state-ok').css({padding:"5px", clear:"both"}).html(response.text)
                $("#li_"+ext).fadeOut();
            }
          },'json');                   
          e.preventDefault();
    });	
    
    $("#show_query").live("click", function(e){
        Action.getActionQuery();    
        e.preventDefault();
    });
	
});


var Action 	= {
	
	getActionQuery  : function()
	{
	    var self = this;
        $("<div />",{id:"action_query"}).append($("<div />", {id:"loader"}))
        .dialog({
                autoOpen    : true,
                modal       : true,
                title       : "Query",
                position    : "left top",
                width       : "auto",
                buttons     : {
                                "Ok"    : function()
                                {
                                  $("#action_query").dialog("destroy").remove();
                                }
                } ,
                close       : function(event, ui)
                {
                    $("#action_query").dialog("destroy").remove();  
                },
                open        : function(event, ui)
                {
                    $("#loader").html("Loading query details ...");
	                $.get("controller.php?action=getActionQuery", {id:$("#action_id").val()}, function(responseData){
	                    $("#loader").html(responseData);
	                });                    
                } 
        })	        	 
	},
	
	getUsers		: function()
	{
		/*$.get("controller.php?action=getUsers", function( data ){
			$.each( data , function( index, val){
				$("#action_owner")
				.append($("<option />",{value:val.tkid, text:val.tkname+" "+val.tksurname}))								
			})
		},"json")	*/
	} ,
	
	getStatus		: function()
	{
		$.get("controller.php?action=getStatus", function( statuses ){
			$.each( statuses , function( index, val){
			$("#action_status")
			.append($("<option />",{value:val.id, text:val.name}))								
			})
		},"json")		
	} ,
	
	save			: function()
	{
		
		var rs_action 		= $("#action").val();
		var rs_action_owner = $("#action_owner :selected").val()		
		var rs_deliverable  = $("#deliverable").val()
		var rs_timescale    = $("#timescale").val()		
		var rs_deadline     = $("#deadline").val()
		var rs_progress     = $("#progress").val()
		var rs_status       = $("#action_status :selected").val()
		var rs_remindon     = $("#remindon").val()		
		
		var rs_attachements  	= [];
		$(".attachents").each( function( index, val){
			rs_attachements.push($(this).val());
		});

		if( rs_action == "" ) {
			jsDisplayResult("error", "error", "Please enter the action");				
			return false;
		} else if( rs_action_owner == "" ) {	
			jsDisplayResult("error", "error", "Please enter the action owner for this action");
			return false;				
		} else if( rs_deadline == "" ) {
			jsDisplayResult("error", "error", "Please enter the deadline for this action");	
			return false;					
		} else {
			jsDisplayResult("info", "info", "Saving action...  <img src='../images/loaderA32.gif' />" );
			$.post( "controller.php?action=newRiskAction" , 
			   {
					id				: $("#risk_id").val() ,
					r_action 		: rs_action ,
					action_owner    : rs_action_owner ,
					deliverable     : rs_deliverable ,
					timescale       : rs_timescale ,
					deadline     	: rs_deadline ,
					remindon     	: rs_remindon ,
					progress     	: "0" ,	
					status			: "1",
					attachment		: rs_attachements					
			   } , function( response ) {
				 if(response.saved ) {
					 $("#tr_nothing_yet").remove();
					 
					 $("#table_actions")
					  .append($("<tr />")
						.append($("<td />",{html:response.id}))
						.append($("<td />",{html:rs_action}))
						.append($("<td />",{html:rs_deliverable}))							
						.append($("<td />",{html:"New"}))
						.append($("<td />",{html:"0%"}))	
						.append($("<td />",{html:$("#action_owner :selected").text()}))								
					   ).before($("#extras"))
					  
					 
					$("#action").val("");
					$("#deliverable").val("")
					$("#timescale").val("")
					$("#deadline").val("")
					//$("#progress").val("")
					$("#remindon").val("")	
					$("#file_upload").html("")
					$("#loading").html("")
					$("#actions").action({risk_id:$("#risk_id").val(), editAction:true, location: "../actions/"});
					if( response.error )
					{
						jsDisplayResult("error", "error", response.text );
					} else {
						jsDisplayResult("ok", "ok", response.text );							
					}
				} else {
					jsDisplayResult("error", "error", response.text );
				}
			 } ,"json");
		}		
	} ,
	
	deleteAction 		: function()
	{				
		if(confirm("Are you sure you want to delete this action ")){	
			jsDisplayResult("info", "info", "Deleting . . .  <img src='../images/loaderA32.gif' />" );
			$.post("controller.php?action=deleteAction", { id : $("#actionid").val() }, function( ret ){
				if( ret == 1 ) {
					jsDisplayResult("error", "error", "Query action deleted");	
				} else if( ret == 0 ) {
					jsDisplayResult("info", "info", "No change was made");
				} else {
					jsDisplayResult("error", "error", "Error deleting the query action");	
				}													  
				});
			}
	} ,
	
	
	edit			: function()
	{
		var message 		= $("#edit_action_message").slideDown();
		var rs_action 		= $("#action").val();
		var rs_action_owner = $("#action_owner :selected").val();		
		var rs_deliverable  = $("#deliverable").val();
		var rs_timescale    = $("#timescale").val();		
		var rs_deadline     = $("#deadline").val();
		var rs_progress     = $("#progress").val();
		var rs_status       = $("#action_status :selected").val();
		var rs_remindon     = $("#remindon").val();		
		
		if( rs_action == "" ) {
			jsDisplayResult("error", "error", "Please enter the action");				
			return false;
		} else if( rs_action_owner == "" ) {	
			jsDisplayResult("error", "error", "Please enter the action owner for this action");
			return false;				
		} else {

			jsDisplayResult("info", "info", "Updating action...  <img src='../images/loaderA32.gif' />" );
			$.post( "controller.php?action=updateRiskAction" , 
			{
			   	id				: $("#actionid").val() ,
				r_action 		: rs_action ,
				action_owner    : rs_action_owner ,
				deliverable     : rs_deliverable ,
				timescale       : rs_timescale ,
				deadline     	: rs_deadline ,
				remindon     	: rs_remindon ,
				progress     	: rs_progress ,	
				status			: rs_status
		     }, function( response ) {
                if(response.error)
                { 
                    jsDisplayResult("error", "error", response.text);	
                } else {
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text);
                    } else {
                        jsDisplayResult("info", "info", response.text);
                    }
                }
			} ,"json");
		}		
	} , 
	
	update			: function()
	{
		var message 	= $("#update_message").slideDown("fast");
		var description = $("#description").val();
		var status		= $("#status :selected").val();
		var progress	= ((status =='3' || $("#status :selected").text()== "Addressed") ? "100" : $("#progress").val());
		var remindon	= $("#remindon").val();
		var attachment	= $("#update_attachment").val();
		var approve 	= ($("#approval").is(":checked") ? "on" : "" );

		if( description == "") {
			jsDisplayResult("error", "error", "Please enter the description for the update");
			return false;
		} else if( status == "" ) {
			jsDisplayResult("error", "error", "Please select the status for the update");
			return false;		
		} else if( progress == "" ) {
			jsDisplayResult("error", "error", "Please enter the progress for the update");
			return false;
		} else if( isNaN( parseInt(progress) ) ) {
			jsDisplayResult("error", "error", "Progress must be a number");	
			return false;							
		} else if(progress == "100" && status !== "3" ){
			jsDisplayResult("error", "error", "Progress cannot be 100% , if the status in not addressed");
			return false;	
		} else {
			if( $("#update_approval").is(":checked") ) {
				var message = $(".message").slideDown();
				jsDisplayResult("info", "info", "Sending request approval email ...  <img src='../images/loaderA32.gif' />");
				$.post("controller.php?action=sendRequestApprovalEmail", { id:$("#action_id").val()}, function( response ){
					if( response.error ) {
						jsDisplayResult("error", "error", response.text );
					} else {
						jsDisplayResult("ok", "ok", response.text );					
					}
				},"json");				
			}			
			jsDisplayResult("info", "info", "Saving action update ...  <img src='../images/loaderA32.gif' />");			
			$.post( "controller.php?action=newActionUpdate", 
				   {
				   	id 			: $("#action_id").val(),
				   	description	: description ,
					status		: status ,
					progress	: progress ,
					remindon 	: remindon,
					attachment	: attachment,
					approval	: approve
				  }, function( response ){
					if(response.error)
					{ 
						jsDisplayResult("error", "error", response.text);	
					} else {
					  if(response.updated)
					  {
					    jsDisplayResult("ok", "ok", response.text);
					  } else {
					    jsDisplayResult("info", "info", response.text);
					  }
					}				  
				}, "json")
		}	
	},
	
	uploadAttachment        : function(element_id)
	{ //alert(element_id);
        $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
	    $.ajaxFileUpload
	    (
		    {
			    url			  : 'controller.php?action=saveActionAttachment&element='+element_id,
			    secureuri	  : false,
			    fileElementId : element_id,
			    dataType	  : 'json',
			    success       : function (response, status)
			    {
				    $("#file_upload").html("");
				    if(response.error )
				    {
					    $("#file_upload").html(response.text )
				    } else {
				         if($("#result_message").length > 0)
				         {
				            $("#result_message").remove();
				         }
				    
                         $("#file_upload").html("");
                         if(response.error)
                         {
                           $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                         } else {
                           $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                           ).append($("<div />",{id:"files_uploaded"}))
                           if(!$.isEmptyObject(response.files))
                           {     
                              var list = [];  
                              list.push("<span>Files uploaded ..</span><br />");          
                              $.each(response.files, function(ref, file){
                              	list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                              	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                              	  list.push("<span style='margin-left:5px;'><a href=''controller.php?action=downloadFile&file="+file.key+"' file='"+file.name+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                              	list.push("</p>");
                              });
                              $("#files_uploaded").html(list.join(' '));
                              
                              $(".delete").click(function(e){
                                    var id   = this.id
                                    var ext  = $(this).attr('title');
                                    var name = $(this).attr('file');
                                    $.post('controller.php?action=removeActionFile', 
                                      {
                                       attachment : id,
                                       ext        : ext,
                                       name       : name,
                                       element    : element_id
                                      }, function(response){     
                                      if(response.error)
                                      {
                                        $("#result_message").html(response.text)
                                      } else {
                                         $("#result_message").addClass('ui-state-ok').html(response.text)
                                         $("#li_"+id).fadeOut();
                                      }
                                    },'json');                   
                                    e.preventDefault();
                              });
                              $("#"+element_id).val("");
                           } 
                         }  				

				    }
			    },
			    error: function (data, status, e)
			    {
				    $("#fileupload").html( "Ajax error -- "+e+", please try uploading again"); 
			    }
		    }
	    )
	    return false;	   
	}	
};

function newActionUploader()
{
    var query_id   = $("#risk_id").val(); 
    $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
	$.ajaxFileUpload
	(
		{
			url			  : 'controller.php?action=saveActionAttachment&query_id='+query_id,
			secureuri	  : false,
			fileElementId : 'new_action_attachment_'+query_id,
			dataType	  : 'json',
			success       : function (response, status)
			{
				$("#file_upload").html("");
				if(response.error )
				{
					$("#file_upload").html(response.text )
				} else {
                     $("#file_upload").html("");
                     if(response.error)
                     {
                       $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                     } else {
                       $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                       ).append($("<div />",{id:"files_uploaded"}))
                       if(!$.isEmptyObject(response.files))
                       {     
                          var list = [];  
                          list.push("<span>Files uploaded ..</span><br />");          
                          $.each(response.files, function(ref, file){
                          	list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                          	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                          	  list.push("<span style='margin-left:5px;'><a href=''controller.php?action=downloadFile&file="+file.key+"' file='"+file.name+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                          	list.push("</p>");
                            //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                          });
                          $("#files_uploaded").html(list.join(' '));
                          
                          $(".delete").click(function(e){
                            var id   = this.id
                            var ext  = $(this).attr('title');
                            var name = $(this).attr('file');
                            $.post('controller.php?action=removeActionFile', 
                              {
                               attachment : id,
                               ext        : ext,
                               action_id  : query_id,
                               name       : name
                              }, function(response){     
                              if(response.error)
                              {
                                $("#result_message").html(response.text)
                              } else {
                                 $("#result_message").addClass('ui-state-ok').html(response.text)
                                 $("#li_"+id).fadeOut();
                              }
                            },'json');                   
                            e.preventDefault();
                          });
                          $("#action_attachment").val("");
                       } 
                     }  				

					if( $("#delmessage").length != 0)
					{
						$("#delmessage").remove()
					} 
					$("#fileloading").append($("<div />",{id:"upmessage"}).addClass("ui-state-ok").css({"padding":"0.7em"})
					  .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"float":"left", "margin-right":"0.3em"}))
					  .append($("<span />",{html:data.file+" "+data.text}))		
					)						
					$("#fileloading").append($("<ul />",{id:"attachment_list"}))
                   if(!$.isEmptyObject(data.files))
                   {     
                      var list = [];            
                      $.each(response.files, function(ref, file){
                      	list.push("<p id='li_"+ref+"' style='padding:2px;' class='ui-state'>");
                      	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                      	  list.push("<span style='margin-left:5px;'><a href='../class/request.php?action=main.php?controller=action&action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                      	list.push("</p>");
                        //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                      });
                      $("#attachment_list").html(list.join(' '));
                      
                      $(".delete").click(function(e){
                        var id = this.id
                        var ext = $(this).attr('title');
                        $.post('controller.php?action=removeattachment', 
                          {
                           attachment : id,
                           ext        : ext,
                           action_id  : action_id
                          }, function(response){     
                          if(response.error)
                          {
                            $("#result_message").html(response.text)
                          } else {
                             $("#result_message").addClass('ui-state-ok').html(response.text)
                             $("#li_"+id).fadeOut();
                          }
                        },'json');                   
                        e.preventDefault();
                      });
                      $("#action_attachment").val("");
                   } 
				}
			},
			error: function (data, status, e)
			{
				$("#fileupload").html( "Ajax error -- "+e+", please try uploading again"); 
			}
		}
	)
	return false;
}


function updateUploader()
{
    var action_id   = $("#action_id").val(); 
    $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");

	$.ajaxFileUpload
	(
		{
			url			  : 'controller.php?action=updateAttachment&action_id='+action_id,
			secureuri	  : false,
			fileElementId : 'action_attachment_'+action_id,
			dataType	  : 'json',
			success       : function (response, status)
			{
				$("#file_upload").html("");
				if(response.error )
				{
					$("#file_upload").html(response.text )
				} else {
                     $("#file_upload").html("");
                     if(response.error)
                     {
                       $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                     } else {
                       $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                       ).append($("<div />",{id:"files_uploaded"}))
                       if(!$.isEmptyObject(response.files))
                       {     
                          var list = [];  
                          list.push("<span>Files uploaded ..</span><br />");          
                          $.each(response.files, function(ref, file){
                          	list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                          	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                          	  list.push("<span style='margin-left:5px;'><a href=''controller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                          	list.push("</p>");
                            //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                          });
                          $("#files_uploaded").html(list.join(' '));
                          
                          $(".delete").click(function(e){
                            var id = this.id
                            var ext = $(this).attr('title');
                            $.post('controller.php?action=removeAttachment', 
                              {
                               attachment : id,
                               ext        : ext,
                               action_id  : action_id
                              }, function(response){     
                              if(response.error)
                              {
                                $("#result_message").html(response.text)
                              } else {
                                 $("#result_message").addClass('ui-state-ok').html(response.text)
                                 $("#li_"+id).fadeOut();
                              }
                            },'json');                   
                            e.preventDefault();
                          });
                          $("#action_attachment").val("");
                       } 
                     }  				
				
				
				    
					if( $("#delmessage").length != 0)
					{
						$("#delmessage").remove()
					} 
					$("#fileloading").append($("<div />",{id:"upmessage"}).addClass("ui-state-ok").css({"padding":"0.7em"})
					  .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"float":"left", "margin-right":"0.3em"}))
					  .append($("<span />",{html:data.file+" "+data.text}))		
					)						
					$("#fileloading").append($("<ul />",{id:"attachment_list"}))
                   if(!$.isEmptyObject(data.files))
                   {     
                      var list = [];            
                      $.each(response.files, function(ref, file){
                      	list.push("<p id='li_"+ref+"' style='padding:2px;' class='ui-state'>");
                      	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                      	  list.push("<span style='margin-left:5px;'><a href='../class/request.php?action=main.php?controller=action&action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                      	list.push("</p>");
                        //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                      });
                      $("#attachment_list").html(list.join(' '));
                      
                      $(".delete").click(function(e){
                        var id = this.id
                        var ext = $(this).attr('title');
                        $.post('controller.php?action=removeattachment', 
                          {
                           attachment : id,
                           ext        : ext,
                           action_id  : action_id
                          }, function(response){     
                          if(response.error)
                          {
                            $("#result_message").html(response.text)
                          } else {
                             $("#result_message").addClass('ui-state-ok').html(response.text)
                             $("#li_"+id).fadeOut();
                          }
                        },'json');                   
                        e.preventDefault();
                      });
                      $("#action_attachment").val("");
                   } 
				}
			},
			error: function (data, status, e)
			{
				$("#fileupload").html( "Ajax error -- "+e+", please try uploading again"); 
			}
		}
	)
	return false;
}

function editActionUploader()
{
	var action_id   = $("#actionid").val(); 
    $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");

	$.ajaxFileUpload
	(
		{
			url			  : 'controller.php?action=editAttachment&action_id='+action_id,
			secureuri	  : false,
			fileElementId : 'edit_action_attachment_'+action_id,
			dataType	  : 'json',
			success       : function (response, status)
			{
				$("#file_upload").html("");
				if(response.error )
				{
					$("#file_upload").html(response.text )
				} else {
                     $("#file_upload").html("");
                     if(response.error)
                     {
                       $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                     } else {
                       $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                       ).append($("<div />",{id:"files_uploaded"}))
                       if(!$.isEmptyObject(response.files))
                       {     
                          var list = [];  
                          list.push("<span>Files uploaded ..</span><br />");          
                          $.each(response.files, function(ref, file){
                          	list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                          	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                          	  list.push("<span style='margin-left:5px;'><a href=''controller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                          	list.push("</p>");
                            //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                          });
                          $("#files_uploaded").html(list.join(' '));
                          
                          $(".delete").click(function(e){
                            var id = this.id
                            var ext = $(this).attr('title');
                            $.post('controller.php?action=removeAttachment', 
                              {
                               attachment : id,
                               ext        : ext,
                               action_id  : action_id
                              }, function(response){     
                              if(response.error)
                              {
                                $("#result_message").html(response.text)
                              } else {
                                 $("#result_message").addClass('ui-state-ok').html(response.text)
                                 $("#li_"+id).fadeOut();
                              }
                            },'json');                   
                            e.preventDefault();
                          });
                          $("#action_attachment").val("");
                       } 
                     }  				
				
				
				    
					if( $("#delmessage").length != 0)
					{
						$("#delmessage").remove()
					} 
					$("#fileloading").append($("<div />",{id:"upmessage"}).addClass("ui-state-ok").css({"padding":"0.7em"})
					  .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"float":"left", "margin-right":"0.3em"}))
					  .append($("<span />",{html:data.file+" "+data.text}))		
					)						
					$("#fileloading").append($("<ul />",{id:"attachment_list"}))
                   if(!$.isEmptyObject(data.files))
                   {     
                      var list = [];            
                      $.each(response.files, function(ref, file){
                      	list.push("<p id='li_"+ref+"' style='padding:2px;' class='ui-state'>");
                      	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                      	  list.push("<span style='margin-left:5px;'><a href='../class/request.php?action=main.php?controller=action&action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                      	list.push("</p>");
                        //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                      });
                      $("#attachment_list").html(list.join(' '));
                      
                      $(".delete").click(function(e){
                        var id = this.id
                        var ext = $(this).attr('title');
                        $.post('controller.php?action=removeattachment', 
                          {
                           attachment : id,
                           ext        : ext,
                           action_id  : action_id
                          }, function(response){     
                          if(response.error)
                          {
                            $("#result_message").html(response.text)
                          } else {
                             $("#result_message").addClass('ui-state-ok').html(response.text)
                             $("#li_"+id).fadeOut();
                          }
                        },'json');                   
                        e.preventDefault();
                      });
                      $("#action_attachment").val("");
                   } 
				}
			},
			error: function (data, status, e)
			{
				$("#fileupload").html( "Ajax error -- "+e+", please try uploading again"); 
			}
		}
	)
	return false;
}
