$(function(){
   
  var risk_id      = $("#risk_id").val(); 
  var more_details = $(".more_details");
  
  if($("#riskid").val() === undefined)
  {
     Query.getRiskType();  
     Query.getQueryOwners();
     Query.getRiskLevel();
     Query.getFinancialExposures();     
     Query.getQueryType();
     Query.getFinancialYear();
     Query.getCategories();
     Query.getQuerySource();
  }
  
  more_details.hide();
	
  $("#more_detail").click(function(e) {			 
	 more_details.toggle();
	 if($(this).val() == "Less Detail")
	 {
	   $(this).val("More Detail");
	 } else {
	   $(this).val("Less Detail");
	 }
	 e.preventDefault();
   }); 
   
   $(".datepicker").datepicker({
     showOn		 : "both",
     buttonImage 	 : "/library/jquery/css/calendar.gif",
     buttonImageOnly : true,
     changeMonth	 : true,
     changeYear	 : true,
     dateFormat      : "dd-M-yy",
     altField		 : "#startDate",
     altFormat		 : 'd_m_yy'
   });	 
     
   
   $("#add_another_recommendation").click( function(e){
	 $(this).parent().prepend($("<p />")
	    .append($("<textarea />",{name:"reco", cols:"30", rows:"5"}).addClass("recommend"))
	 )
	e.preventDefault()	  
   });	
   
   $("#add_another_clientresponse").click( function(e){
	 $(this).parent().prepend($("<p />")
	    .append($("<textarea />",{name:"client", cols:"30", rows:"5"}).addClass("clientresponse"))		
	 )
	 e.preventDefault();									  
   });	   

   $("#add_another_finding").click( function(e){
	 $(this).parent().prepend($("<p />")
	   .append($("<textarea />",{name:"find", cols:"30", rows:"5"}).addClass("find"))		
	 )
 	 e.preventDefault();									  
   });	
	
   $("#add_another_internalcontrol").click(function(e){
	  $(this).parent().prepend($("<p />")
	    .append($("<textarea />",{name:"find", cols:"30", rows:"5"}).addClass("internalcontrol"))		
	  )		
	  e.preventDefault();
    });
    
    $("#add_another_auditorconclusion").click(function(e){
	  $(this).parent().prepend($("<p />")
	      .append($("<textarea />",{name:"auditor_conclusion", cols:"30", rows:"5"}).addClass("auditorconclusion"))		
	  )				
	  e.preventDefault();
    });

    /*
    $("#query_type").live("change", function() {
	  $("#query_category").html("");
	  $("#query_category").append($("<option />",{value:"", text:"--query category--"}))									
	  Query.getCategories( $(this).val() );
    });     
    */
    $("#add_query").click(function(e) {
	  Query.save();
	  e.preventDefault();
    });    
    
    $("#edit_risk").click(function(e) {
	  Query.edit();
	  e.preventDefault();
    });

    $("#save_update").click(function(e) {
	  Query.update();
	  e.preventDefault();
    });    	     
    
    $("#delete_risk").click(function(e){
	  Query.deleteQuery();
	  e.preventDefault();
    });    
	
    $("#cancel").click(function(e){
	  history.back();							   
	  e.preventDefault();
    });	
    
    $(".upload_query").live("change", function(e){
        Query.uploadQueryAttachment(this.id);
        e.preventDefault();
    })
        
});

var Query         = {

	getRiskType				: function()
	{
	   $.getJSON("controller.php/?action=getActiveRiskType", function(risk_types) {
		  $.each(risk_types, function(index, val) {
		     $("#risk_type").append($("<option />",{text:val.name, value:val.id}));
		 })											 
	   });
	} , 
	
	getQuerySource          : function()
	{
	   $.getJSON("controller.php?action=getActiveQuerySource", function(risk_types) {
		  $.each(risk_types, function(index, val) {
		     $("#query_source").append($("<option />",{text:val.name, value:val.id}));
		 })											 
	   });   
	},
	
	getQueryOwners			: function()
	{
	   $.getJSON("controller.php?action=getDirectorate", function(direData){
		 $.each(direData ,function(index, directorate) {
			$("#directorate").append($("<option />",{text:directorate.dirtxt, value:directorate.subid}));
		 })
	   });
	} ,  
	
	getFinancialExposures	: function()
	{
	   $.getJSON("controller.php/?action=getFinancialExposure", function(financial_exposures) {
	    	  $.each(financial_exposures, function(index, val) {
	    	     if((val.status & 4) == 4)
	    	     {
	    	        $("#financial_exposure").append($("<option />",{text:val.name, value:val.id, selected:"selected"}));
	    	     } else {
	    	        $("#financial_exposure").append($("<option />",{text:val.name, value:val.id}));
	    	     }
		  })			
	   });
	} ,
	
	getRiskLevel			: function(id)
	{
	   $.getJSON("controller.php/?action=getRiskLevel", {id:id}, function(risk_levels) {
		$.each(risk_levels, function(index, val) {
		    if((val.status & 4) == 4)
		    {
		      $("#risk_level").append($("<option />",{text:val.name, value:val.id, selected:"selected"})); 
		    } else {
		      $("#risk_level").append($("<option />",{text:val.name, value:val.id}));
		    }
		})											 
	   });	
	} , 	 
	
	getQueryType			: function()
	{
	    $.getJSON("controller.php/?action=getQueryType", function(query_types) {
		   $.each(query_types, function(index, val) {
			 $("#query_type").append($("<option />",{text:val.name, value:val.id}));
		   })
	    });
	} ,
	
	getCategories			: function(id)
	{
	    $.getJSON("controller.php/?action=getCategory", function(categories) {
		  $.each(categories,function( index, val ) {
			 $("#query_category").append($("<option />",{text:val.name, value:val.id}));
		  })											 
	    });			
	}, 	
     
     getFinancialYear    : function()
     {
        $.getJSON("controller.php/?action=getFinancialYear",{status:1}, function(financialyears){
          $.each(financialyears, function(index, year){
              $("#financial_year").append($("<option />",{text:year.start_date+" - "+year.end_date, value:year.id}))
          });   
        }); 
     } ,	
	         
	save					: function()
	{
	    	//save new query
	    var message 				= $("#risk_message").slideDown("fast");
	    var r_type 					= $("#query_type :selected").val()
         var r_risk_type 		    = $("#risk_type :selected").val()
	    var r_category 				= $("#query_category :selected").val()
	    var r_description 			= $("#query_description").val()
	    var r_background			= $("#query_background").val();	
	    var r_financial_exposure    = $("#financial_exposure").val();
	    var r_monetary_implication	= $("#monetary_implication").val();
	    var r_detail 				= $("#risk_detail").val();
	    var r_risk_level		 	= $("#risk_level :selected").val();
	    var r_query_date		 	= $("#query_date").val();
	    var r_query_deadline_date	= $("#query_deadline_date").val();
	    var r_query_reference 		= $("#query_reference").val();
	    var r_directorate 			= $("#directorate :selected").val();
	    var r_financial_year        = $("#financial_year :selected").val();
		
	    $(".find").each( function( index, val){
	       if(index > 0 ) 
	       {
			r_finding += "__"+$(this).val();
		  } else {
			r_finding = $(this).val();	
		  }
	    });	
		
	    $(".recommend").each( function( index, val){
		  if(index > 0) 
		  {
			r_recommendation += "__"+$(this).val();
		  } else {
			r_recommendation = $(this).val();	
		  }
	    });

	    $(".clientresponse").each( function( index, val){
		  if(index > 0) 
		  {
			r_client_response += "__"+$(this).val();
		  } else {
			r_client_response = $(this).val();	
		  }
	    });		
		
	    $(".internalcontrol").each(function(index, val){
		   if(index > 0)
		   {
			 r_internalcontrol += "__"+$(this).val();
		   } else {
			 r_internalcontrol = $(this).val();	
		   }			
	    });
		
	    $(".auditorconclusion").each(function(index, val){
		  if(index > 0) 
		  {
			r_auditorconclusion += "__"+$(this).val();
		  } else {
			r_auditorconclusion = $(this).val();	
		  }						
	    });
		
	    if(r_financial_year === "") {
		  jsDisplayResult("error", "error", "Please select the financial year for this query");
		  return false;	    
	    } else if( r_query_reference == "" ) {
		  jsDisplayResult("error", "error", "Please enter the query reference for this query");			
		  return false;
	    } else if( r_description == "" ) {
		  jsDisplayResult("error", "error", "Please enter the description for this query");			
		  return false;
	    } else if( r_background == "" ) {
		  jsDisplayResult("error", "error", "Please enter the background for this query");			
	       return false;
	    } else if( r_query_date == "" ) {
		  jsDisplayResult("error", "error", "Please enter the query date for this query");			
		  return false;
	    } else if( r_query_deadline_date == "") {
		  jsDisplayResult("error", "error", "Please enter the query deadline date for this query");			
		  return false;				
	    } else  {
		  jsDisplayResult("info", "info", "Saving Query . . . <img src='../images/loaderA32.gif' >");
		  $.post( "controller.php?action=saveQuery",
		  {
               type 			 	       : r_type ,
               type_text			       : $("#query_type :selected").text(),
               category		 	           : r_category,
               query_source	               : $("#query_source :selected").val(),
               category_text		       : $("#query_category :selected").text(),				
               description 	 	           : r_description ,
               risk_level 	 	  	       : r_risk_level ,
               risk_type                   : r_risk_type,
               query_date 	 	  	       : r_query_date ,
               query_deadline_date	       : r_query_deadline_date,
               background		 	       : r_background ,				
               financial_exposure	       : r_financial_exposure ,	
               monetary_implication        : r_monetary_implication ,
               risk_detail		  	       : r_detail,				
               finding	  			       : r_finding , 
               recommendation		       : r_recommendation,					
               query_reference 	           : r_query_reference,
               user_responsible 	       : r_directorate ,
               client_response	  	       : r_client_response,
               auditor_conclusion	       : r_auditorconclusion,
               internal_control_deficiency : r_internalcontrol,
               financial_year              : r_financial_year,
               user_responsible_text       : $("#directorate :selected").text()		
		   }, function(response) {				  
               if(response.saved) 
               {
                    $("#riskid").val( response.id )
                    $("#idrisk").html( response.id )					
                    $("select#query_source option[value='']").attr("selected", "selected");
                    $("select#query_type option[value='']").attr("selected", "selected");
                    $("select#risk_type option[value='']").attr("selected", "selected");
                    $("select#query_category option[value='']").attr("selected", "selected");					
                    $("select#directorate option[value='']").attr("selected", "selected");					
                    $("#query_description").val("");
                    $("select#risk_level option[value='']").attr("selected", "selected");					
                    $("#query_date").val("");		
                    $("#query_deadline_date").val("");
                    $("#query_background").val("");
                    $("#financial_exposure").val("");
                    $("#monetary_implication").val("");
                    $("#risk_detail").val("");
                    $(".find").val("");
                    $(".recommend").val("");
                    $(".clientresponse").val("");
                    $(".auditorconclusion").val("");
                    $(".internalcontrol").val("");					
                    $("#query_reference").val("");	
                    $("#financial_year").val("");
                    $(".controls").val("");
                    $("#file_upload").html("");
				   if(response.error)
				   {
					jsDisplayResult("error", "error", response.text);
				   } else {
					 jsDisplayResult("ok", "ok", response.text);						
				   }
				} else {
console.log(response.post_info);
				   jsDisplayResult("error", "error", response.text);			
				}
		  }, "json");
	    }				  
	} ,     
	//edit query 
	edit			: function()
	{
		var message 				= $("#risk_message").slideDown();
		var r_type 					= $("#query_type :selected").val()
		var r_risk_type				= $("#risk_type :selected").val()
		var r_category 				= $("#query_category :selected").val()
		var r_description 			= $("#query_description").val()
		var r_background			= $("#query_background").val();	
		var r_financial_exposure    = $("#financial_exposure :selected").val();
		var r_monetary_implication	= $("#monetary_implication").val();
		var r_detail 				= $("#risk_detail").val();
		var r_query_reference 		= $("#query_reference").val();
		var r_directorate 			= $("#directorate :selected").val();
		var r_risk_level 			= $("#risk_level :selected").val()
		var r_query_date		 	= $("#query_date").val();
		var r_query_deadline_date	= $("#query_deadline_date").val();
		
		$(".find").each( function( index, val){
			if( index > 0 ) {
				r_finding += "__"+$(this).val();
			}else{
				r_finding = $(this).val();	
			}
		});

		$(".recommend").each( function( index, val){
			if( index > 0 ) {
				r_recommendation += "__"+$(this).val();
			}else{
				r_recommendation = $(this).val();	
			}
		});

		$(".clientresponse").each( function( index, val){
			if( index > 0 ) {
				r_client_response += "__"+$(this).val();
			}else{
				r_client_response = $(this).val();	
			}
		});	
		
		$(".internalcontrol").each(function(index, val){
			if( index > 0 ) {
				r_internalcontrol += "__"+$(this).val();
			} else {
				r_internalcontrol = $(this).val();	
			}			
		});
		
		$(".auditorconclusion").each(function(index, val){
			if( index > 0 ) {
				r_auditorconclusion += "__"+$(this).val();
			} else {
				r_auditorconclusion = $(this).val();	
			}						
		});		
        if( r_query_reference == "" ) {
			jsDisplayResult("error", "error", "Please enter the query reference for this query");
			return false;
		} else if( r_description == "" ) {
			jsDisplayResult("error", "error", "Please enter the description for this query");
			return false;
		} else if( r_background == "" ) {
			jsDisplayResult("error", "error", "Please enter the background for this query");
			return false;
		} else if( r_query_date == "" ) {
			jsDisplayResult("error", "error", "Please enter the query date for this query");			
			return false;
		} else if( r_query_deadline_date == "") {
			jsDisplayResult("error", "error", "Please enter the query deadline date for this query");			
			return false;				
		} else {
			jsDisplayResult("info", "info", "Updating query...  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=updateQuery",
			  {	id 					  : $("#riskid").val(),
				type 			 	  : parseInt(r_type),
				query_source	      : $("#query_source :selected").val(),
				type_text			  : $("#query_type :selected").text(),
                risk_type	 	      : r_risk_type ,
				category		 	  : r_category,
				category_text		  : $("#query_category :selected").text(),				
				description 	 	  : r_description ,
			    background		 	  : r_background ,				
				financial_exposure	  : parseInt(r_financial_exposure),	
				monetary_implication  : r_monetary_implication ,
				risk_detail		  	  : r_detail,				
				finding	  			  : r_finding, 
				recommendation		  : r_recommendation,					
				query_reference 	  :	r_query_reference,
				query_date 	 	  	  : r_query_date,
				query_deadline_date	  : r_query_deadline_date,
				user_responsible 	  : r_directorate,
				risk_level 	  		  : r_risk_level,				
				client_response	  	  : r_client_response,
				auditor_conclusion	  : r_auditorconclusion,
				internal_control_deficiency  : r_internalcontrol,				
				user_responsible_text : $("#directorate :selected").text()
			  } ,
			  function( response ) {
               if(response.error)
               {
                  jsDisplayResult("error", "error", response.text);
               } else {
                 if(response.updated)
                 {
                   jsDisplayResult("ok", "ok", response.text);
                 } else {
                   jsDisplayResult("info", "info", response.text);  
                 }
               }
			 } ,"json");
		}			
	} ,
	
	update			: function()
	{
		var message 				= $("#risk_message").slideDown("fast");
		var r_type 					= $("#risk_type :selected").val()		
		var r_financial_exposure    = $("#financial_exposure :selected").val();
		var r_monetary_implication	= $("#monetary_implication").val();
		var r_detail 				= $("#risk_detail").val();
		//var r_finding			 	= $("#finding").val();
		//var r_recommendation		= $("#recommendation").val();
		//var r_client_response		= $("#client_response").val();
		//var r_auditorconclusion		= $("#auditor_conclusion").val();
		//var r_internalcontrol		= $("#internal_control_deficiency").val();		
		var r_risk_level 			= $("#risk_level :selected").val()
		//var r_user_responsible 		= $("#userrespoid").val();
		var r_response		 		= $("#risk_response").val();	
		var r_status		 		= $("#risk_status :selected").val();

		$(".find").each( function( index, val){
			if( index > 0 ) {
				r_finding += "__"+$(this).val();
			}else{
				r_finding = $(this).val();	
			}
		});

		$(".recommend").each( function( index, val){
			if( index > 0 ) {
				r_recommendation += "__"+$(this).val();
			}else{
				r_recommendation = $(this).val();	
			}
		});

		$(".clientresponse").each( function( index, val){
			if( index > 0 ) {
				r_client_response += "__"+$(this).val();
			}else{
				r_client_response = $(this).val();	
			}
		});	
		
		$(".internalcontrol").each(function(index, val){
			if( index > 0 ) {
				r_internalcontrol += "__"+$(this).val();
			} else {
				r_internalcontrol = $(this).val();	
			}			
		});
		
		$(".auditorconclusion").each(function(index, val){
			if( index > 0 ) {
				r_auditorconclusion += "__"+$(this).val();
			} else {
				r_auditorconclusion = $(this).val();	
			}						
		});			
		
		if(r_response == "") {
			jsDisplayResult("error", "error", "Please enter the response");
			return false;
		}  else {
			jsDisplayResult("info", "info", "Saving query update...  <img src='../images/loaderA32.gif' />");
			$.post( "controller.php?action=newRiskUpdate",
			  {
				id					  : $("#riskid").val(),
				response 			  : r_response,
			    status		 	  	  : r_status ,
			    risk_type		 	  : r_type,
				financial_exposure	  : r_financial_exposure,	
				monetary_implication  : r_monetary_implication,
				risk_detail		  	  : r_detail,				
				finding	  			  : r_finding, 
				recommendation		  : r_recommendation,					
				risk_level 	  		  : r_risk_level,				
				client_response	  	  : r_client_response,
				auditor_conclusion	  : r_auditorconclusion,
				internal_control_deficiency  : r_internalcontrol	
			  } ,function( response ) {		  
                   if(response.error)
                   {
                      jsDisplayResult("error", "error", response.text );
                   } else {
                     if(response.updated)
                     {
                       jsDisplayResult("info", "info", response.text );
                     } else {
                       jsDisplayResult("ok", "ok", response.text );  
                     }
                   }
			  } , "json");
		}		
	} ,
	
	deleteQuery		: function()
	{

		var message 	= $("#risk_message").slideDown();	
		var userRespo 	=  $("#risk_user_responsible").val()
		if( confirm( "Are you sure you want to delete this query") ) {
			jsDisplayResult("info", "info", "Deleting query ...  <img src='../images/loaderA32.gif' />");
			$.post("controller.php?action=deleteRisk" ,
				{ 
					id 			: $("#riskid").val(),
					userRespo	: $("#risk_user_responsible :selected").val()
					}	 ,
				function( response ) {
                        if(response.error)
                        {
                           jsDisplayResult("error", "error", response.text );
                        } else {
                          if(response.updated)
                          {
                            jsDisplayResult("info", "info", response.text );
                          } else {
                            jsDisplayResult("ok", "ok", response.text );  
                          }
                        }
				},"json")
		}
		
	} , 
	
	uploadQueryAttachment       : function(element_id)
	{
        $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
        var query_id  = $("#riskid").val();
	    $.ajaxFileUpload
	    (
		    {
			    url			  : 'controller.php?action=saveQueryAttachment&element='+element_id,
			    secureuri	  : false,
			    fileElementId : element_id,
			    dataType	  : 'json',
			    success       : function (response, status)
			    {
				    $("#file_upload").html("");
				    if(response.error )
				    {
					    $("#file_upload").html(response.text )
				    } else {
                         $("#file_upload").html("");
                         if(response.error)
                         {
                           $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                         } else {
                            if($("#result_message").length > 0)
                            {
                                $("#result_message").remove();
                            }
                           $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                           ).append($("<div />",{id:"files_uploaded"}))
                           if(!$.isEmptyObject(response.files))
                           {     
                              var list = [];  
                              list.push("<span>Files uploaded ..</span><br />");          
                              $.each(response.files, function(ref, file){
                              	list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                              	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                              	  list.push("<span style='margin-left:5px;'><a href=''controller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                              	list.push("</p>");
                                //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                              });
                              $("#files_uploaded").html(list.join(' '));
                              
                              $(".delete").click(function(e){
                                var id   = this.id
                                var ext  = $(this).attr('title');
                                var name = $(this).attr('file');
                                $.post('controller.php?action=removeQueryFile', 
                                  {
                                    attachment : id,
                                    ext        : ext,
                                    name       : name,
                                    element    : element_id
                                  }, function(response){     
                                  if(response.error)
                                  {
                                    $("#result_message").html(response.text)
                                  } else {
                                     $("#result_message").addClass('ui-state-ok').html(response.text)
                                     $("#li_"+id).fadeOut();
                                  }
                                },'json');                   
                                e.preventDefault();
                              });
                              $("#action_attachment").val("");
                           } 
                         }  				
				    }
			    },
			    error: function (data, status, e)
			    {
				    $("#fileupload").html( "Ajax error -- "+e+", please try uploading again"); 
			    }
		    }
	    )
	    return false;
	}	
}


$(".removeattach").live("click", function(){
	var id   = this.id;
	var file = $(this).attr("title") 
	var upid = id;
	var indexof = id.indexOf(".");
	if( indexof > 0)
	{
		upid = id.substring( 0, indexof );
	}
	
	$.post("controller.php?action=RemoveQueryFile",{ id : id, file : file },  function( response ){
		if( $("#upmessage").length != 0)
		{
			$("#upmessage").remove()
		} 
		if( $("#delmessage").length > 0)
		{
			$("#delmessage").remove();
		}
		
		if( response.error )
		{
			$("#fileloading").prepend($("<div />", {id:"delmessage"}).addClass("ui-state-ok").addClass("ui-corner-all").css({"padding":"0.7em"})
			  .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"float":"left", "margin-right":"0.3em"}))
			  .append($("<span />",{html:response.text}))		
			)	
		} else {
			$("#uploaded_"+upid).fadeOut();			
			$("#fileloading").prepend($("<div />",{id:"delmessage"}).addClass("ui-state-ok").addClass("ui-corner-all").css({"padding":"0.7em"})
			  .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"float":"left", "margin-right":"0.3em"}))
			  .append($("<span />",{html:response.text}))		
			)			
		}		
	}, "json");
	return false;
});


$(".remove_attach").live("click", function(e){
      var id   = this.id
      var ext  = $(this).attr('title');
      var name = $(this).attr('file');
      $.post('controller.php?action=deleteQueryFile', { 
          attachment : id,
          ext        : ext,
          name       : name,
          query_id   : $("#riskid").val()
      }, function(response){     
            if(response.error)
            {
              $("#result_message").css({padding:"5px", clear:"both"}).html(response.text)
            } else {
               $("#result_message").addClass('ui-state-ok').css({padding:"5px", clear:"both"}).html(response.text)
               $("#li_"+ext).fadeOut();
            }
      },'json');                   
      e.preventDefault();	
});

function attachQuery()
{
    $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");

	$.ajaxFileUpload
	(
		{
			url			  : 'controller.php?action=saveAttachment',
			secureuri	  : false,
			fileElementId : 'new_qry_attachment',
			dataType	  : 'json',
			success       : function (response, status)
			{
				$("#file_upload").html("");
				if(response.error )
				{
					$("#file_upload").html(response.text )
				} else {
                     $("#file_upload").html("");
                     if(response.error)
                     {
                       $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                     } else {
                       $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                       ).append($("<div />",{id:"files_uploaded"}))
                       if(!$.isEmptyObject(response.files))
                       {     
                          var list = [];  
                          list.push("<span>Files uploaded ..</span><br />");          
                          $.each(response.files, function(ref, file){
                          	list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                          	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                          	  list.push("<span style='margin-left:5px;'><a href=''controller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                          	list.push("</p>");
                            //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                          });
                          $("#files_uploaded").html(list.join(' '));
                          
                          $(".delete").click(function(e){
                            var id = this.id
                            var ext = $(this).attr('title');
                            $.post('controller.php?action=removeQueryAttachment', 
                              {
                               attachment : id,
                               ext        : ext
                              }, function(response){     
                              if(response.error)
                              {
                                $("#result_message").html(response.text)
                              } else {
                                 $("#result_message").addClass('ui-state-ok').html(response.text)
                                 $("#li_"+id).fadeOut();
                              }
                            },'json');                   
                            e.preventDefault();
                          });
                          $("#action_attachment").val("");
                       } 
                     }  				
				
				
				    
					if( $("#delmessage").length != 0)
					{
						$("#delmessage").remove()
					} 
					$("#fileloading").append($("<div />",{id:"upmessage"}).addClass("ui-state-ok").css({"padding":"0.7em"})
					  .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"float":"left", "margin-right":"0.3em"}))
					  .append($("<span />",{html:data.file+" "+data.text}))		
					)						
					$("#fileloading").append($("<ul />",{id:"attachment_list"}))
                   if(!$.isEmptyObject(data.files))
                   {     
                      var list = [];            
                      $.each(response.files, function(ref, file){
                      	list.push("<p id='li_"+ref+"' style='padding:2px;' class='ui-state'>");
                      	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                      	  list.push("<span style='margin-left:5px;'><a href='../class/request.php?action=main.php?controller=action&action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                      	list.push("</p>");
                        //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                      });
                      $("#attachment_list").html(list.join(' '));
                      
                      $(".delete").click(function(e){
                        var id = this.id
                        var ext = $(this).attr('title');
                        $.post('controller.php?action=removeattachment', 
                          {
                           attachment : id,
                           ext        : ext,
                           action_id  : action_id
                          }, function(response){     
                          if(response.error)
                          {
                            $("#result_message").html(response.text)
                          } else {
                             $("#result_message").addClass('ui-state-ok').html(response.text)
                             $("#li_"+id).fadeOut();
                          }
                        },'json');                   
                        e.preventDefault();
                      });
                      $("#action_attachment").val("");
                   } 
				}
			},
			error: function (data, status, e)
			{
				$("#fileupload").html( "Ajax error -- "+e+", please try uploading again"); 
			}
		}
	)
	return false;
}


function updateQueryUploader()
{
    $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
    var query_id  = $("#riskid").val();
	$.ajaxFileUpload
	(
		{
			url			  : 'controller.php?action=updateQueryAttachment&query_id='+query_id,
			secureuri	  : false,
			fileElementId : 'update_qry_attachment_'+query_id,
			dataType	  : 'json',
			success       : function (response, status)
			{
				$("#file_upload").html("");
				if(response.error )
				{
			       $("#file_upload").html(response.text )
				} else {
                     $("#file_upload").html("");
                     if(response.error)
                     {
                       $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                     } else {
                       $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                       ).append($("<div />",{id:"files_uploaded"}))
                       if(!$.isEmptyObject(response.files))
                       {     
                          var list = [];  
                          list.push("<span>Files uploaded ..</span><br />");          
                          $.each(response.files, function(ref, file){
                          	list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                          	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                          	  list.push("<span style='margin-left:5px;'><a href=''controller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                          	list.push("</p>");
                            //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                          });
                          $("#files_uploaded").html(list.join(' '));
                          
                          $(".delete").click(function(e){
                            var id = this.id
                            var ext = $(this).attr('title');
                            $.post('controller.php?action=removeQueryAttachment', 
                              {
                               attachment : id,
                               ext        : ext
                              }, function(response){     
                              if(response.error)
                              {
                                $("#result_message").html(response.text)
                              } else {
                                 $("#result_message").addClass('ui-state-ok').html(response.text)
                                 $("#li_"+id).fadeOut();
                              }
                            },'json');                   
                            e.preventDefault();
                          });
                          $("#action_attachment").val("");
                       } 
                     }  				
					if( $("#delmessage").length != 0)
					{
						$("#delmessage").remove()
					} 
					$("#fileloading").append($("<div />",{id:"upmessage"}).addClass("ui-state-ok").css({"padding":"0.7em"})
					  .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"float":"left", "margin-right":"0.3em"}))
					  .append($("<span />",{html:data.file+" "+data.text}))		
					)						
					$("#fileloading").append($("<ul />",{id:"attachment_list"}))
                   if(!$.isEmptyObject(data.files))
                   {     
                      var list = [];            
                      $.each(response.files, function(ref, file){
                      	list.push("<p id='li_"+ref+"' style='padding:2px;' class='ui-state'>");
                      	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                      	  list.push("<span style='margin-left:5px;'><a href='../class/request.php?action=main.php?controller=action&action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                      	list.push("</p>");
                        //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                      });
                      $("#attachment_list").html(list.join(' '));
                      
                      $(".delete").click(function(e){
                        var id = this.id
                        var ext = $(this).attr('title');
                        $.post('controller.php?action=removeattachment', 
                          {
                           attachment : id,
                           ext        : ext,
                           action_id  : action_id
                          }, function(response){     
                          if(response.error)
                          {
                            $("#result_message").html(response.text)
                          } else {
                             $("#result_message").addClass('ui-state-ok').html(response.text)
                             $("#li_"+id).fadeOut();
                          }
                        },'json');                   
                        e.preventDefault();
                      });
                      $("#action_attachment").val("");
                   } 
				}
			},
			error: function (data, status, e)
			{
				$("#fileupload").html( "Ajax error -- "+e+", please try uploading again"); 
			}
		}
	)
	return false;
}
