// JavaScript Document
$.widget("ui.queryactivate", {
	
	options	: 	{
		tableId 		: "queryactivate_"+(Math.floor(Math.random(56) * 34)),
		url				: "controller.php?action=getActivationQueries",
		location		: "",
		start			: 0,
		limit			: 10,
		total			: 0,
		risk_id			: 0,
		current			: 1,
		isOwners		: [], 
		section         : "",
		viewAction      : ""
	} , 
	
	_init			: function()
	{
		this._getQueries();		
	} , 
	
	_create			: function()
	{
		var self = this;
		var html = [];
		html.push("<table width='100%' class='noborder'>");
		  html.push("<tr>");
		    html.push("<td width='50%' class='noborder'>");
		      html.push("<table width='100%' id='awaiting_"+self.options.tableId+"'>");
      		    html.push("<tr>")
      		    html.push("<td colspan='6'><h4>Queries Awaiting Activation</h4></td>");
      		    html.push("</tr>");
		      html.push("</table>");
		    html.push("</td>");
		    html.push("<td class='noborder'>");
		      html.push("<table width='100%' id='activated_"+self.options.tableId+"'>");
      		    html.push("<tr width='50%'>");
      		    html.push("<td colspan='5'><h4>Queries Activated</h4></td>");
      		    html.push("</tr>");      		    
		      html.push("</table>");
		    html.push("</td>");		    
		  html.push("</tr>");	  
		html.push("</table>");
		$(self.element).append(html.join(' '));		
	} , 
	
	_getQueries		: function()
	{
		var self = this;
	     $("body").append($("<div />",{id:"actionLoadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
	     );				
		$.getJSON(self.options.location+""+self.options.url, {
			start 	: self.options.start,
			limit 	: self.options.limit,
			id		: self.options.risk_id,
			section : self.options.section
		}, function(actionData){
		    $("#actionLoadingDiv").remove();
			$(".activated").remove();
			$(".awaiting").remove();
			self._displayHeaders(actionData.awaiting.headers);			
			self._displayAwaiting(actionData.awaiting.queries);
			self._displayActivated(actionData.activated.queries);
			/*
			self.options.total =  actionData.total
			self._displayPaging( actionData.total, actionData.columns );
			
			if( $.isEmptyObject(actionData.actions) )
			{
				$("#"+self.options.tableId).append($("<tr />")
				  .append($("<td />",{colspan:responseData.cols +1, html:"There are no actions for this query"}))		
				)	
			} else {
				self._display(actionData.actions, actionData.avgProgress, actionData.isOwner, actionData.actionStatus, actionData.columns);	
			}
			*/
		});
	} ,
	
	_displayAwaiting    : function(queries)
	{
	   var self = this;
	   var html = [];
	   if($.isEmptyObject(queries))
	   {
	     html.push("<tr class='awaiting'><td colspan='6'>There are no queries awaiting activation</td></tr>");
	   } else {
           $.each(queries, function(query_id, query){
              html.push("<tr class='awaiting'>");
                html.push("<td>"+query.query_item+"</td>");
                html.push("<td>"+query.query_reference+"</td>");
                html.push("<td>"+query.query_description+"</td>");
                html.push("<td>"+query.query_type+"</td>");
                html.push("<td>"+query.query_category+"</td>");
                html.push("<td>");
                 html.push("<input type='button' name='activate_"+query_id+"' id='activate_"+query_id+"' value='Activate' />");
                 $("#activate_"+query_id).live("click", function(e){
                    self._activateQuery(query_id);
                    e.preventDefault();
                 });
                html.push("</td>");
                
                
              html.push("</tr>");       
           });
       }
	   $("#awaiting_"+self.options.tableId).append(html.join(' ') );	
	   
	} ,
	
	_displayActivated			: function(queries)
	{  
	   var self = this;
	   var html = [];
	   if($.isEmptyObject(queries))
	   {
	     html.push("<tr class='activated'><td colspan='6'>There are no queries activated</td></tr>");
	   } else {	   
           $.each(queries, function(query_id, query){
              html.push("<tr class='activated'>");
                html.push("<td>"+query.query_item+"</td>");
                html.push("<td>"+query.query_reference+"</td>");
                html.push("<td>"+query.query_description+"</td>");
                html.push("<td>"+query.query_type+"</td>");
                html.push("<td>"+query.query_category+"</td>");
              html.push("</tr>");       
           });
       }
	   $("#activated_"+self.options.tableId).append(html.join(' ') );	
	} ,
	
	_activateQuery      : function(query_id)
	{
	    var self = this;
	    if($("#activate_dialog").length > 0)
	    {
	       $("#activate_dialog").remove();
	    }
	    var html = [];
	    html.push("<table>");
	      html.push("<tr>");
	        html.push("<th>Response</th>")
	        html.push("<td>");
	         html.push("<textarea id='response' name='response' cols='50' rows='8'></textarea>")
	        html.push("</td>");
	      html.push("</tr>");
	      html.push("<tr>");
	        html.push("<td colspan='2'>");
	         html.push("<p id='message'></p>")
	        html.push("</td>");
	      html.push("</tr>");
	    html.push("<table>");
	    
	    $("<div />",{id:"activate_dialog"}).append(html.join(' '))
	     .dialog({
	            autoOpen    : true,
	            modal       : true,
	            position    : "top",
	            title       : "Activate Query Q"+query_id,
	            width       : 'auto',
	            buttons     : {
	                            "Activate"      : function()
	                            {
	                                 $.post("controller.php?action=activateQuery", {
	                                    query_id : query_id,
	                                    response : $("#response").val()
	                                 }, function(response) {
	                                    if(response.error)
	                                    {
	                                      jsDisplayResult("error", "error", response.text);
	                                    } else {
	                                      jsDisplayResult("ok", "ok", response.text);
	                                      self._getQueries();
	                                    }
	                                 },"json");
	                                 $("#activate_dialog").dialog("destroy").remove();
	                            } ,
	                            
	                            "Cancel"        : function()
	                            {
	                               $("#activate_dialog").dialog("destroy").remove();
	                            }
	            },
	            close       : function(event, ui)
	            {
	               $("#activate_dialog").dialog("destroy").remove();
	            }, 
	            open        : function(event, ui)
	            {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
	            }
	     });
	    
	},
	
	_displayHeaders		: function(headers)
	{
		var self = this;
		var html = [];
		html.push("<tr class='activated'>");
        html.push("<th>"+headers.query_item+"</th>");	
        html.push("<th>"+headers.query_reference+"</th>");	
        html.push("<th>"+headers.query_description+"</th>");
        html.push("<th>"+headers.query_type+"</th>");	
        html.push("<th>"+headers.query_category+"</th>");
        html.push("<th id='remove'></th>");
        html.push("</tr>");
		$("#awaiting_"+self.options.tableId).append(html.join(' '));
		$("#activated_"+self.options.tableId).append(html.join(' ')).find("#remove").remove();;
	} ,
	
	_displayPaging		: function(total, cols)
	{
		var self  = this;	
		var html  = [];
		var pages = 0;
		if(total%self.options.limit > 0)
		{
		   pages = Math.ceil( total/self.options.limit )
		} else {
		   pages = Math.floor( total/self.options.limit )
		}
        html.push("<tr>");
          html.push("<td colspan='"+cols+"'>");
            html.push("<input type='button' id='action_first_"+self.option.risk_id+"' value=' |< ' />");
            html.push("<input type='button' id='action_previous_"+self.option.risk_id+"' value=' < ' />");
             html.push("<span>&nbsp;&nbsp;&nbsp;");
              html.push("Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ?  1 :pages));
             html.push("&nbsp;&nbsp;&nbsp;</span>");
            html.push("<input type='button' id='action_next_"+self.option.risk_id+"' value=' > ' />");
            html.push("<input type='button' id='action_last_"+self.option.risk_id+"' value=' >| ' />");
          html.push("</td>");
        if(self.options.editAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.updateAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.assuranceAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAssurance)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }    
        if(self.options.viewAction)
        {
           html.push("<td>&nbsp;&nbsp;</td>");
        }   	   
        html.push("</tr>");
        $("#"+self.options.tableId+"_"+self.options.risk_id).append(html.join(' '));                 
        if(self.options.current < 2)
        {
          $("#action_first_"+self.option.risk_id).attr('disabled', 'disabled');
          $("#action_previous_"+self.option.risk_id).attr('disabled', 'disabled');		     
        }
        if((self.options.current == pages || pages == 0))
        {
          $("#action_next_"+self.option.risk_id).attr('disabled', 'disabled');
          $("#action_last_"+self.option.risk_id).attr('disabled', 'disabled');		     
        }			 
		$("#action_next_"+self.option.risk_id).bind("click", function(){
			self._getNext( self );
		});
		$("#action_last_"+self.option.risk_id).bind("click",  function(){
			self._getLast( self );
		});
		$("#action_previous_"+self.option.risk_id).bind("click",  function(){
			self._getPrevious( self );
		});
		$("#action_first_"+self.option.risk_id).bind("click",  function(){
			self._getFirst( self );
		});			 
	} , 
		
		_getNext  			: function( $this ) {
			$this.options.current   = $this.options.current+1;
			$this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
			this._getActions();
		},	
		
		_getLast  			: function( $this ) {
			$this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
			$this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );			
			this._getActions();
		},
		
		_getPrevious    	: function( $this ) {
			$this.options.current   = parseFloat( $this.options.current ) - 1;
			$this.options.start 	= ($this.options.current-1)*$this.options.limit;		
			this._getActions();			
		},
		
		_getFirst  			: function( $this ) {
			$this.options.current   = 1;
			$this.options.start 	= 0;
			this._getActions();	
		}
	
	
});
