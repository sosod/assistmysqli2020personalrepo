<link rel="stylesheet" href="/assist.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/styles.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery/css/blue/jquery-ui.css" />
<?php
if("../../library/dbconnect/dbconnect.php"){
	require_once( "../../library/dbconnect/dbconnect.php" );
}
if( file_exists("../class/report.php")){
	require_once( "../class/report.php" );
}
if( file_exists("../class/naming.php")){
	require_once( "../class/naming.php" );
}
if($_GET['action'] == "generate" )
{
	$reportObj  = new Report();
	$quick_str  = $reportObj -> getQuickData($_GET['id']);
	$reportdata = array();
	if(!empty($quick_str))
	{
	  $reportdata  = unserialize(base64_decode($quick_str));
	}
     $report 	  = $reportObj -> generateReport($reportdata);
 	switch($reportdata['display'])
 	{
 		case "screen":
 			$reportObj -> displayHtmlReport($report);
 			break;
 		case "excell":
 			$reportObj -> displayCsvReport($report);
 			break;
 		case "pdf":
 			$reportObj -> displayHtmlReport($report);
 			break;
 		default:
 			$reportObj -> displayHtmlReport($report);
 	}
	exit(0);     
}
?>

