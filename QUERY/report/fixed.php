<?php
	$scripts = array( 'fixedreports.js', 'menu.js' );
	$styles = array( 'colorpicker.css' );
	$page_title = "Quick Report";
	require_once("../inc/header.php");
	require_once("../../library/class/assist_dbconn.php");
	require_once("../../library/class/assist_db.php");
	require_once("../../library/class/assist_helper.php");
	$i = 1;
	
	//get list of directorates in use
	$db = new ASSIST_DB("client");
	$dbref = $db->getDBRef();
	$sql = "SELECT DISTINCT d.dirid as id, d.dirtxt as text
		FROM ".$dbref."_query_register q
		LEFT OUTER JOIN ".$dbref."_dirsub s
		ON s.subid = q.sub_id AND s.active = 1
		LEFT OUTER JOIN ".$dbref."_dir d
		ON s.subdirid = d.dirid AND d.active = 1
		WHERE ".Risk::getStatusSQLForWhere("q")."
		ORDER BY d.dirsort"; 
	$dir = $db->mysql_fetch_all($sql);

	$sql = "SELECT DISTINCT a.action_owner as id, CONCAT(tk.tkname,' ',tk.tksurname) as text
		FROM ".$dbref."_actions a
		INNER JOIN ".$dbref."_query_register q
		ON a.risk_id = q.id AND a.active = 1
		LEFT OUTER JOIN assist_".$_SESSION['cc']."_timekeep tk
		ON a.action_owner = tk.tkid AND tk.tkstatus = 1
		WHERE ".Risk::getStatusSQLForWhere("q")."
		ORDER BY tk.tkname, tk.tksurname"; 
	$owner = $db->mysql_fetch_all($sql);
	
	
?>
<script src="/library/amcharts/javascript/amcharts.js" type="text/javascript"></script>
<script src="/library/amcharts/javascript/raphael.js" type="text/javascript"></script>
<form action="" method=post"">
<div>
	<div id="quick_report_message"></div>
    <table id="hideOnshow"><thead>
    	<tr>
        	<th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Report Format</th>
			<th>Filter</th>
            <th></th>
        </tr>
</thead>
<tbody>
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td>Query Register</td>
			<td>The Query Register details the current query as defined by the Query Owner(s)</td>
            <td>On Screen</td>            
			<td class=right></td>
            <td><input type="submit" id="show_riskregister" name="show_riskregister" value=" Generate "  /></td>
        </tr>
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td>Query Analysis Dashboard</td>
			<td>Overview of Queries by Classification and Status</td>
            <td>Bar & Pie Graph</td>            
            <td class=right></td>            
            <td><input type="button" value=" Generate " class=show_dashboard id=report_dashboard /></td>
        </tr>  
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td>Query Analysis Dashboard per Department</td>
			<td>Overview of Queries by Classification and Status per Department</td>
            <td>Bar & Pie Graph</td>            
            <td class=right><select id=filter_report_dashboard_dir><option value=ALL selected><?php echo $select_box_text ?></option>
			<?php
			foreach($dir as $d) {
				if(is_null($d['id'])) { echo "<option value=0>[Unspecified]</option>"; } else {
					echo "<option value=".$d['id'].">".ASSIST_HELPER::decode($d['text'])."</option>";
				}
			}
			?>
			</select></td>            
            <td><input type="button" value=" Generate " class=show_dashboard id=report_dashboard_dir /></td>
        </tr>  
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td>Action Analysis Dashboard</td>
			<td>Overview of Actions by Query Classification and Action Status</td>
            <td>Bar & Pie Graph</td>            
            <td class=right></td>            
            <td><input type="button" value=" Generate " class=show_dashboard id=report_dashboard_actions /></td>
        </tr>  
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td>Action Analysis Dashboard Per Person</td>
			<td>Overview of Actions by Query Classification and Action Status per Person</td>
            <td>Bar & Pie Graph</td>            
            <td class=right><select id=filter_report_dashboard_actions_person><option value=ALL selected><?php echo $select_box_text ?></option>
			<?php
			foreach($owner as $d) {
				if(is_null($d['id'])) { echo "<option value=0>[Unspecified]</option>"; } else {
					echo "<option value=".$d['id'].">".ASSIST_HELPER::decode($d['text'])."</option>";
				}
			}
			?>
			</select></td>            
            <td><input type="button" value=" Generate " class=show_dashboard id=report_dashboard_actions_person /></td>
        </tr>  
<!--        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td>Query Status</td>
			<td>View the number of queries in each query status</td>
            <td>Pie Graph</td>            
            <td class=right></td>            
            <td><input type="button" name="show_queryVsStatus" id="show_queryVsStatus" value=" Generate "  /></td>
        </tr> 
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td>Queries per Risk Type</td>
			<td>View the number of queries per risk type</td>
            <td>Bar Graph</td>            
            <td class=right></td>            
            <td><input type="button" id="show_queryVsRisk" value=" Generate "  /></td>
        </tr>
        <tr>
        	<td><?php echo $i; $i++; ?></td>
            <td>Queries per Department</td>
			<td>View the number of queries per Department</td>
            <td>Bar Graph</td>            
            <td class=right></td>            
            <td><input type="button" id="show_querisVsDepartment" value=" Generate "  /></td>
        </tr>  -->
</tbody>
</table>
</div>
</form>
<div id="chartdiv" style="width: 100%; height: 400px;"></div>