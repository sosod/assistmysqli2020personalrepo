<?php
$riskheadings = array();
$riskheadings['id'] = array(
	'display' => "Risk Item",
	'type' => "X",
	'color'=> "N",
	'field' => "id"
);
$nogroup = array("id","insertdate","impact_rating","likelihood_rating","inherent_risk_rating","inherent_risk_exposure","residual_risk_exposure","residual_risk","control_effectiveness_rating");
$nofilter = array("id","impact_rating","likelihood_rating","inherent_risk_rating","inherent_risk_exposure","residual_risk_exposure","residual_risk","control_effectiveness_rating","risk_update","type");
$nosort = $nofilter;
unset($nosort[0]);

$risk_header_ids = array(1,4,7,8,11,14,16,18,20,26,27,25,22,28,29,5);
$risk_head_settings = array(
	4 => array('name'=>"type",'color'=>"N",'type'=>"L",'table'=>"types",'table_field'=>"name"),
	7 => array('name'=>"category",'color'=>"N",'type'=>"L",'table'=>"categories",'table_field'=>"name"),
	8 => array('name'=>"description",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	11 => array('name'=>"background",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	14 => array('name'=>"impact",'color'=>"N",'type'=>"L",'table'=>"impact",'table_field'=>"assessment"),
	16 => array('name'=>"impact_rating",'color'=>"Y",'type'=>"T",'table'=>"",'table_field'=>""),
	18 => array('name'=>"likelihood",'color'=>"N",'type'=>"L",'table'=>"likelihood",'table_field'=>"assessment"),
	20 => array('name'=>"likelihood_rating",'color'=>"Y",'type'=>"T",'table'=>"",'table_field'=>""),
	26 => array('name'=>"inherent_risk_exposure",'color'=>"Y",'type'=>"L",'table'=>"inherent_exposure",'table_field'=>"magnitude"),
	27 => array('name'=>"inherent_risk_rating",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	25 => array('name'=>"current_controls",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	22 => array('name'=>"percieved_control_effective",'color'=>"Y",'type'=>"L",'table'=>"control_effectiveness",'table_field'=>"effectiveness"),
	28 => array('name'=>"residual_risk",'color'=>"N",'type'=>"T",'table'=>"",'table_field'=>""),
	29 => array('name'=>"residual_risk_exposure",'color'=>"Y",'type'=>"L",'table'=>"residual_exposure",'table_field'=>"magnitude")
//	, 5 => array('name'=>"risk_owner",'color'=>"N",'type'=>"L",'table'=>"",'table_field'=>"value")
);
$sql = "SELECT * FROM ".$dbref."_header_names WHERE active = 1 ORDER BY ordernumber";
include("../../inc_db_con.php");
while($row = mysql_fetch_array($rs)) {
	if(isset($risk_head_settings[$row['id']])) {
		$rhs = $risk_head_settings[$row['id']];
		$riskheadings[$rhs['name']] = array(
			'display' => $row['client_terminology'],
			'type' => $rhs['type'],
			'color' => $rhs['color'],
			'field' => $rhs['name'],
			'table' => $rhs['table'],
			'table_field' => $rhs['table_field']
		);
		if($row['name']=="percieved_control_effective") {
			$riskheadings['control_effectiveness_rating'] = array(
				'display' => "Control Rating",
				'type' => "T",
				'color' => "N",
				'field' => "control_effectiveness_rating",
				'table' => "",
				'table_field' => ""
			);
		}
	}
}
mysql_close($con);
$riskheadings['status'] = array(
	'display' => "Risk Status",
	'type' => "L",
	'color'=> "N",
	'field' => "status",
	'table' => "status",
	'table_field' => "client_terminology"
);
$risk_head_settings[99] = array('name'=>"status",'color'=>"N",'type'=>"L",'table'=>"status",'table_field'=>"client_terminology");
$riskheadings['insertdate'] = array(
	'display' => "Risk Creation Date",
	'type' => "D",
	'color'=> "N",
	'field' => "insertdate"
);
/*$riskheadings['risk_update'] = array(
	'display' => "Risk Update",
	'type' => "U",
	'color'=> "N",
	'field' => "risk_update"
);*/



$head = array_merge($riskheadings);
?>