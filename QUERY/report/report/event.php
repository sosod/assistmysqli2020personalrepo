<?php
	$scripts = array( 'edr.js','menu.js',  );
	$styles = array( 'colorpicker.css' );
	$page_title = "Event Driven Report";
	require_once("../inc/header.php");
?>
<div>
	<div id="edr_message"></div>
    <form method="post" id="edr-form" name="edr-form">
    <table border="1">
    	<tr>
        	<th>EDR#</th>
            <th>EDR Name</th>
            <th>EDR Description</th>
            <th>Action</th>
        </tr>
      	<tr>
        	<td colspan="4" align="left">
            	<input type="submit" name="new_edr" value="New" id="new_edr" />
            </td>
        </tr> 
    </table>
    </form>
    <div id="new_edr">
    <form method="post" id="edrnew-form" name="edrnew-form"> 
    	<table width="100%">
        	<tr>
            	<td>EDR #</td>
                <td><input type="text" name="edrnum" id="edrnum" /></td>
            </tr>
        	<tr>
            	<td>EDR Name</td>
                <td><input type="text" name="edrname" id="edrname" /></td>
            </tr>
        	<tr>
            	<td>EDR Description</td>
                <td><input type="text" name="edrdescription" id="edrdescription" /></td>
            </tr>                        
        	<tr>
            	<td>EDR Description</td>
                <td><input type="text" name="edrdescription" id="edrdescription" /></td>
            </tr>                        
        	<tr>
            	<td>EDR rule</td>
                <td>
                	<table width="100%">
                    	<tr>
                        	<td width="52%">Function to be applied</td>
                          <td width="48%"><input type="text" name="function" id="function" value="IF" /></td>
                      </tr>
                    	<tr>
                        	<td>On which data field will the report be genereated</td>
                            <td>
                            	<select id="datafield" name="datafield">
                                	<option><?php echo $select_box_text ?></option>
                                	<option>risk</option>
                                	<option>action</option>                                                                        
                                </select>
                            </td>
                        </tr>
                    	<tr>
                        	<td>Against which data field must the function be applied</td>
                            <td>
                            	<select id="datafieldfunction" name="datafieldfunction">
                                	<option></option>
                                	<option>Deadline date</option>                                    
                                </select>
                            </td>
                        </tr>           
                    	<tr>
                        	<td>Which of the following measuring criteria must generate the EDR</td>
                            <td>
                            	<select id="datafieldfunction" name="datafieldfunction">
                                	<option></option>
                                	<option>Deadline date</option>                                    
                                </select>
                            </td>
                        </tr>           
                    	<tr>
                        	<td>In which form must be the EDR be generated</td>
                            <td>
                            	<select id="edrform" name="edrform">
                                	<option></option>
                                	<option>Email</option>                                    
                                	<option>SMS</option>                                    
                                	<option>Pdf</option>                                                            
                                </select>
                            </td>
                        </tr>    
                    	<tr>
                        	<td>The EDR must be sent to ; </td>
                            <td>
                            	<select id="sentto" name="sentto">
                                	<option></option>
                                	<option>US</option>                                    
                                	<option>You</option>                                    
                                	<option>Me</option>                                                            
                                </select>
                            </td>
                        </tr>                 
                        <tr>
                        	<td>The narative to be included in the EDR is as follows</td>
                            <td>
							<textarea name="narrative" id="narrative"></textarea>
                            </td>
                        </tr>
                        <tr>
                        	<td>Which field needs to be included in the EDR?</td>
                            <td>
							<input type="text" id="fieldtoInclude" name="fieldtoInclude" />
                            </td>
                        </tr>                        
                        <tr>
                        	<td><em>Note : Save before sorting the output </em></td>
                            <td>
							<input type="submit" name="save_edr" id="save_edr" value="Save" />
							<input type="submit" name="reset_edr" id="reset_edr" value="Reset" />                            
                            </td>
                        </tr>                                                
                        <tr>
                        	<td></td>
                            <td>
							<input type="submit" name="sort_edr_output" id="sort_edr_output" value="Sort the EDR Report Output" />
                            </td>
                        </tr>                                                                        
                    </table>
                </td>
            </tr>                                                                       
        </table>
        </form>
    </div>
    
</div>