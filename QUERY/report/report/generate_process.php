<?php
require_once("../inc/inc_ignite.php");
//error_reporting(-1);
include("generate_headings.php");
//arrPrint($head);


function outputColor($color,$type) {
	if($type == "full") {
		return "<div style=\"background-color: #".$color."\">&nbsp;</div>";
	} else {
		return "<span style=\"background-color: #".$color."\">&nbsp;&nbsp;&nbsp;</span>&nbsp;";
	}
}

/*** GET LISTS ***/
$lists = array();
/*//RISK_OWNER
$sql = "SELECT tkid, CONCAT_WS(' ',tkname, tksurname) as value FROM assist_".$cmpcode."_timekeep WHERE tkid IN (SELECT DISTINCT risk_owner FROM ".$dbref."_risk_register WHERE active = true)";
include("generate_db.php");
while($row = mysql_fetch_array($rs)) {
	$lists['risk_owner'][$row['tkid']] = $row;
}
mysql_close($con);*/
foreach($risk_head_settings as $h) {
	if(($h['type']=="L" && $h['name']!="risk_owner") || $h['name'] == "residual_risk_exposure") {
		$hf = $h['name'];
		$table_field = $h['table_field'];
		$lists[$hf] = array();
		$sql = "SELECT * FROM ".$dbref."_".$h['table']." WHERE active = true ORDER BY ".$table_field;
		include("generate_db.php");
		while($row = mysql_fetch_array($rs)) {
			$l = array();
			$l['id'] = $row['id'];
			$l['color'] = isset($row['color']) ? $row['color'] : "ffffff";
			$l['value'] = $row[$table_field];
			if($hf=="inherent_risk_exposure" || $hf=="residual_risk_exposure") {
				$l['from'] = $row['rating_from'];
				$l['to'] = $row['rating_to'];
			}
			$lists[$hf][$row['id']] = $l;
		}
		mysql_close($con);
	}
}

//arrPrint($lists);

/*** VARIABLES ***/
$field = array();
$field['id'] = "Y";
$filter = array();
$filtertype = array();
$records = array();
$group = $_REQUEST['groupby'];
$sortarr = $_REQUEST['sort'];
$output = $_REQUEST['output'];
$rhead = strlen($_REQUEST['rhead'])>0 ? $_REQUEST['rhead'] : "Report";
$echocount = 0;
if(count($sortarr)==0) { $sort = array(); }
//GET FIELD HEADINGS
    foreach($head as $row)
    {
		$hf = $row['field'];
		if(isset($_REQUEST[$hf]) && $_REQUEST[$hf]=="Y") {
			$field[$hf] = "Y";
		}
		if(!in_array($hf,$nofilter)) {
			$filter[$hf] = $_REQUEST[$hf.'filter'];
			if($row['type']=="T" || $row['type']=="M") {
				$filtertype[$hf] = $_REQUEST[$hf.'filtertype'];
			}
			if(count($sortarr)==0 && !in_array($hf,$nosort)) {
				$sort[] = $hf;
			}
		}
    }
if(count($sortarr)==0) { $sortarr = $sort; $sort = null; }

//arrPrint($filter);


/*** GROUP BY ***/
$glist = array();
if(strlen($group)==0 || $group == "X") {
	$group = "X";
	$groupid = 0;
	$glist[] = array('id'=>0,'value'=>"");
} else {
	$groupid = $group;
	if($head[$group]['type']=="Y" || $head[$group]['type'] == "L") {
		$h = $head[$group];
		//$glist = $lists[$group];
		$glist = array();
		$sql = "SELECT * FROM ".$dbref."_".$h['table']." WHERE active = true ORDER BY ".$h['table_field'];
		include("generate_db.php");
		while($row = mysql_fetch_array($rs)) {
			$glist[$row['id']] = array('id'=>$row['id'],'value'=>$row[$h['table_field']]);
		}
		mysql_close($con);
	} else {
		$sql = "SELECT DISTINCT $groupid FROM ".$dbref."_risk_register";
		include("inc_db_con.php");
		while($row = mysql_fetch_assoc($rs)) {
			//if($group=="calldate") {
				//$gval = date("d M Y",$row[$groupid]);
			//} else {
				$gval = $row[$groupid];
			//}
			$gid = strFn("str_replace",strtolower(decode($gval))," ","");
			$glist[$gval] = array('id'=>$gid,'value'=>$gval);
		}
		mysql_close($con);
	}
}
//arrPrint($glist);



/**************** SET REPORT SQL ************/
//SELECT
$repsql[0] = "SELECT r.*";
//$repsql[1] = ", l.logdate, l.logdateact, l.logadmintext, l.logutype";
//FROM
$repsql[2] = " FROM ".$dbref."_risk_register r";
//$repsql[2].= " INNER JOIN ".$dbref."_log l ON l.logcallid = c.callid ";
foreach($riskheadings as $h) {
	$hf = $h['field'];
	if(!in_array($hf,$nofilter)) { 
		if($h['type']== "L" || $h['type'] == "Y") {
			switch($hf) {
				case "risk_owner":
					$repsql[2].= " INNER JOIN assist_".$cmpcode."_timekeep ".$hf." ON ";
					$repsql[2].= $hf.".tkid = r.".$h['field'];
					break;
				default:
					$repsql[2].= " INNER JOIN ".$dbref."_".$h['table']." ".$hf." ON ";
					$repsql[2].= $hf.".id = r.".$h['field'];
			}
		}
	}
}
//WHERE
$repsql[2].= " WHERE r.active = true ";
foreach($riskheadings as $h) {
	$where = "";
	$hf = $h['field'];
//echo "<P>".$hf;
	if(!in_array($hf,$nofilter)) {
	if(array_key_exists($hf,$lists)) {					//LISTS
		switch($hf) {
			case "risk_owner":
				if(count($filter[$hf])>1 && strtolower($filter[$hf][0])!="all" && strtolower($filter[$hf][0])!="x") {
					if(count($filter[$hf])!=count($lists[$hf])) {
						$where = "(";
						$where.= $hf.".tkid = '".implode("' OR ".$hf.".tkid = '",$filter[$hf]);
						$where.= "')";
					}
				} elseif(strtolower($filter[$hf][0])!="all" && strlen($filter[$hf][0])>0 && strtolower($filter[$hf][0])!="x") {
					$where = $hf.".tkid = '".$filter[$hf][0]."'";
				}
				break;
			default:
				if(count($filter[$hf])>1 && strtolower($filter[$hf][0])!="all" && strtolower($filter[$hf][0])!="x") {
					if(count($filter[$hf])!=count($lists[$hf])) {
						$where = "(";
						$where.= $hf.".id = ".implode(" OR ".$hf.".id = ",$filter[$hf]);
						$where.= ")";
					}
				} elseif(strtolower($filter[$hf][0])!="all" && checkIntRef($filter[$hf][0]) && strtolower($filter[$hf][0])!="x") {
					$where = $hf.".id = ".$filter[$hf][0];
				}
				break;
		}
	} elseif($hf=="insertdate") {
			$from = $filter[$hf][0];
			$to = $filter[$hf][1];
			if(strlen($from)>0 || strlen($to)>0) {
				if(strlen($from)>0 && strlen($to)>0) {
					$from = strFn("explode",$from," ","");
					$from = mktime(0,0,0,getMonth($from[1]),$from[0],$from[2]);
					$to = strFn("explode",$to," ","");
					$to = mktime(23,59,59,getMonth($to[1]),$to[0],$to[2]);
				} elseif(strlen($from)>0) {
					$from = strFn("explode",$from," ","");
					$from = mktime(0,0,0,getMonth($from[1]),$from[0],$from[2]);
					$to = mktime(23,59,59,getMonth($from[1]),$from[0],$from[2]);
				} elseif(strlen($to)>0) {
					$to = strFn("explode",$to," ","");
					$to = mktime(23,59,59,getMonth($to[1]),$to[0],$to[2]);
					$from = mktime(0,0,0,getMonth($to[1]),$to[0],$to[2]);
				}
				$from = date("Y-m-d H:i:s",$from);
				$to = date("Y-m-d H:i:s",$to);
				if(strlen($from)>0 && strlen($to)>0) {
					$where = "r.insertdate >= '$from' AND r.insertdate <= '$to'";
				}
			}
	} elseif(strlen($filter[$hf])>0) {							//TEXT FIELDS
			$filter[$hf] = trim($filter[$hf]);
                    switch($filtertype[$hf])
                    {
						case "X":
							break;
                        case "all":
                            $ft = array_unique(explode(" ",code($filter[$hf])));
							$where = "(r.".$hf." LIKE '%".strFn("implode",$ft,"%' AND r.$hf LIKE '%","")."%')";
							$where = str_replace("AND r.$hf LIKE '%%'","",$where);
                            break;
                        case "any":
                            $ft = array_unique(explode(" ",code($filter[$hf])));
							$where = "(r.".$hf." LIKE '%".strFn("implode",$ft,"%' OR r.$hf LIKE '%","")."%')";
							$where = str_replace("OR r.$hf LIKE '%%'","",$where);
                            break;
                        case "exact":
                            $where = "r.".$hf." LIKE '%".code($filter[$hf])."%'";
                            break;
                        default:
                            $where = "r.".$hf." LIKE '%".code($filter[$hf])."%'";
                            break;
                    }
	}
	if(strlen($where)>0) {
		$repsql[2].=" AND ".$where;
	}
	}//nofilter
}

//SQL ORDER
$repsql[3] = " ORDER BY ";
$r=0;
foreach($sortarr as $s) {
	$r++;
	if($r>1) { $repsql[3].=", "; }
	if($s=="risk_owner") {
		$repsql[3].= " ".$s.".tkname ASC, ".$s.".tksurname ASC";
	} elseif(isset($lists[$s]) ) {
		$repsql[3].= " ".$s.".".$head[$s]['table_field']." ASC";
	} else {
		$repsql[3].= " r.".$s." ASC";
	}
}
/************* END REPORT SQL ***************/

//GET records
$grecords = array();
$rec_id = "id";
$sql = implode(" ",$repsql);
//echo "<p>".$sql;
//echo "<P>GROUPID ".$groupid;
include("generate_db.php");
//echo "<P>MNR ".mysql_num_rows($rs);
	while($row = mysql_fetch_array($rs))
	{
		if(is_numeric($groupid)) { 
			$records[0][$row[$rec_id]] = $row;
			$grecords[$row[$rec_id]] = 0;
		} elseif($head[$group]['type']=="Y" || $head[$group]['type'] == "L") {
			$records[$row[$groupid]][$row[$rec_id]] = $row;
			$grecords[$row[$rec_id]] = $row[$groupid];
		} else {
			$gval = $row[$groupid];
			$gval = strFn("str_replace",strtolower(decode($gval))," ","");
			$records[$gval][$row[$rec_id]] = $row;
			$grecords[$row[$rec_id]] = $gval;
		}
	}
mysql_close($con);
	

//arrPrint($records);
//arrPrint($grecords);

	
/************** OUTPUT *********************/
	
//output format settings
$echo = "";
$cspan = 1 + count($field) - 4;
switch($output) 
{
case "csv":
	$gcell[1] = "\"\"\r\n\"";	//start of group
	$gcell[9] = "\"\r\n";	//end of group
	$cella[1] = "\""; //heading cell
	$cellz[1] = "\","; //heading cell
	$cella[2] = "\""; //normal cell
	$cellz[2] = "\","; //normal cell
	$cella[20] = "\""; //normal cell
	$cellz[20] = "\","; //normal cell
	$cella[21] = "\""; //normal cell
	$cellz[21] = "\","; //normal cell
	$rowa = "";
	$rowz = "\r\n";
	$pagea = "\"".$rhead."\"\r\n";
	$table[1] = "";
	$table[9] = "";
	$pagez = "\r\n\"Report generated on ".date("d F Y")." at ".date("H:i")."\"";
	$newline = chr(10);
	break;
case "excel":
	$gcell[1] = "<tr><td width=50></td></tr><tr><td nowrap class=title3 rowspan=1>";	//start of group
	$gcell[9] = "</td></tr>";	//end of group
	$cella[1] = "<td class=\"head\">"; //heading cell
	$cellz[1] = "</td>"; //heading cell
	$cella[2] = "<td class=call>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td class=call style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td class=call style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$rowa = chr(10)."<tr>";
	$rowz = "</tr>";
	$pagea = "<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">";
	$pagea.= "<head>";
	$pagea.= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\">";
	$pagea.= chr(10)."<!--[if gte mso 9]>";
	$pagea.= "<xml>";
	$pagea.= "<x:ExcelWorkbook>";
	$pagea.= "<x:ExcelWorksheets>";
	$pagea.= "<x:ExcelWorksheet>";
	$pagea.= "<x:Name>Complaints Report</x:Name>";
	$pagea.= "<x:WorksheetOptions>";
	$pagea.= "<x:Panes>";
	$pagea.= "</x:Panes>";
	$pagea.= "</x:WorksheetOptions>";
	$pagea.= "</x:ExcelWorksheet>";
	$pagea.= "</x:ExcelWorksheets>";
	$pagea.= "</x:ExcelWorkbook>";
	$pagea.= "</xml>";
	$pagea.= "<![endif]-->";
	$pagea.= chr(10)."<style>".chr(10)." td { font-style: Calibri; font-size:11pt; } .call { border-width: thin; border-color: #000000; border-style: solid; } .head { font-weight: bold; text-align: center; background-color: #EEEEEE; color: #000000; vertical-align:middle; font-style: Calibri;  border-width: thin; border-color: #000000; border-style: solid; } ".chr(10)." .title { font-size:20pt; font-style: Calibri; color:#000066; font-weight:bold; text-decoration:underline; text-align: center; } ".chr(10)." .title2 { font-size:16pt; font-style: Calibri; color:#000066; font-weight:bold; text-decoration:underline; text-align: center;} .title3 { font-size:14pt; font-style: Calibri; color:#000066; font-weight:bold;} .title4 { font-size:12pt; font-style: Calibri; color:#000000; font-weight:bold;}</style>";
	$pagea.= "</head>";
	$pagea.= "<body><table><tr><td class=title nowrap colspan=$cspan>".$cmpname."</td></tr><tr><td class=title2 nowrap colspan=$cspan>";
	$pagea.= $rhead;
	$pagea.="</td></tr>";
	$table[1] = "";
	$table[9] = "";
	$pagez = "<tr></tr><tr><td style=\"font-size:8pt;font-style:italic;\" nowrap>Report generated on ".date("d F Y")." at ".date("H:i").".</td></tr></table></body></html>";
	$newline = chr(10);
	break;
default:
	$gcell[1] = "<h3 class=fc>";	//start of group
	$gcell[9] = "</h3>";	//end of group
	$cella[1] = "<th>"; //heading cell
	$cellz[1] = "</th>"; //heading cell
	$cella[2] = "<td>";	//normal cell
	$cellz[2] = "</td>"; //normal cell
	$cella[20] = "<td style=\"text-align:center\">";	//centered normal cell
	$cellz[20] = "</td>"; //centered normal cell
	$cella[21] = "<td style=\"background-color: #eeeeee\">";	//ptd normal cell
	$cellz[21] = "</td>"; //ptd normal cell
	$rowa = "<tr>";
	$rowz = "</tr>";
	$pagea = "<html><head><meta http-equiv=\"Content-Language\" content=\"en-za\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\"><title>www.Ignite4u.co.za</title></head>";
	$pagea.= "<link rel=\"stylesheet\" href=\"/default.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"/styles/style_blue.css\" type=\"text/css\">";
	$pagea.= "<style type=text/css>table { border-width: 1px; border-style: solid; border-color: #ababab; }    table td { border: 1px solid #ababab; } .b-w { border: 1px solid #fff; }</style>";
	$pagea.= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 class=fc style=\"text-align: center;\">".$cmpname."</h1><h2 class=fc style=\"text-align: center;\">";
	$pagea.=$rhead;
	$pagea.= "</b></h2>";
	$table[1] = "<table cellpadding=3 cellspacing=0 width=100%>";
	$table[9] = "</table>";
	$pagez = "<p style=\"font-style: italic; font-size: 7pt;\">Report generated on ".date("d F Y")." at ".date("H:i")."</p></body></html>";
	$newline = "<br />";
	break;
}

//START PAGE
$echo = $pagea;
//GROUP LOOP
foreach($glist as $g) {
//	$groupresults = array();	//reset summary/group array
	$rec2 = $records[$g['id']];	//get calls for group
//print_r($call2);
	if(count($rec2)>0) {	//if there are calls for the group
		if($g['id']!=0) {	//display the group title
			$echo.= $table[9].$gcell[1].$g['value'].$gcell[9].$table[1];
		} else {
			$echo.= $table[1];
		}
		//TABLE HEADINGS
		$echo.= $rowa;
		//$echo.= $cella[1]."Ref".$cellz[1];
		foreach($head as $hrow) {
			if($field[$hrow['field']] == "Y") { 
				$echo.= $cella[1].decode($hrow['display']).$cellz[1]; 
			}
		}
		$echo.=$rowz;
		
	
		//TABLE ROWS
		foreach($rec2 as $rec)
		{
			$rid = $rec[$rec_id];
			$yn = "Y";
			if($yn == "Y") {
				$echo.= $rowa;
				//$echo.= $cella[2].$cid.$cellz[2];
				foreach($head as $hrow)
				{
					$hf = $hrow['field'];
					$ht = $hrow['type'];
					if($field[$hf] == "Y") { 
						$echo.= $cella[2];
						switch($ht) {
							case "D":
								$echo.= date("d M Y H:i",strtotime($rec[$hf]))." ";
//								$echo.= date("d M Y H:i",$rec[$hf])." ";
								break;
							case "Y":
							case "L":
								//$echo.=decode($lists[$hf][$call[$hf]]['value']);
								if($hf!="inherent_risk_exposure" && $hf!="residual_risk_exposure") {
									if($hrow['color']=="Y" && ($output != "excel" && $output!="csv")) {
										$echo.=outputColor($lists[$hf][$rec[$hf]]['color'],"sml");
									}
									$echo.=decode($lists[$hf][$rec[$hf]]['value']);
								} else {
									$color = "ffffff";
									foreach($lists[$hf] as $l) {
										if($rec[$hf]>=$l['from'] && $rec[$hf]<=$l['to']) {
											$color = $l['color'];
											break;
										}
									}
									$echo.= outputColor($color,"full");
								}
								break;
							default:
								switch($hf) {
									case "impact_rating":
									case "likelihood_rating":
										$hf2 = strFn("substr",$hf,0,stripos($hf,"_"));;
										if($output !="excel" && $output !="csv") {
											$echo.=outputColor($lists[$hf2][$rec[$hf]]['color'],"sml");
										}
									default:
										$echo.= strFn("str_replace",decode($rec[$hf]),chr(10),$newline)." ";
										break;
								}
						}
						$echo.= $cellz[2];
					}
				}	//foreach heading and logheading
				$echo.=$rowz;
			}	//yn = y
		}	//foreach line item
	} //end if group(kpis)>0
} //end foreach group


//END PAGE
$echo.= $table[9].$pagez;

//DISPLAY OUTPUT
switch($output)
{
	case "csv":
        //CHECK EXISTANCE OF STORAGE LOCATION
        $chkloc = "reports";
        checkFolder($chkloc);
		//WRITE DATA TO FILE
		$filename = "../../files/".$cmpcode."/".$chkloc."/".$modref."_".date("Ymd_Hi",$today).".csv";
        $newfilename = $modref."_report_".date("Ymd_Hi",$today).".csv";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        $content = 'text/plain';
		$_REQUEST['oldfile'] = $filename;
		$_REQUEST['newfile'] = $newfilename;
		$_REQUEST['content'] = $content;
		include("generate_dl.php");
		break;
	case "excel":
        //CHECK EXISTANCE OF STORAGE LOCATION
        $chkloc = "reports";
        checkFolder($chkloc);
		//WRITE DATA TO FILE
		$filename = "../../files/".$cmpcode."/".$chkloc."/".$modref."_".date("Ymd_Hi",$today).".xml";
        $newfilename = $modref."_report_".date("Ymd_Hi",$today).".xls";
        $file = fopen($filename,"w");
        fwrite($file,$echo."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        $content = 'application/ms-excel';
		$_REQUEST['oldfile'] = $filename;
		$_REQUEST['newfile'] = $newfilename;
		$_REQUEST['content'] = $content;
		include("generate_dl.php");
		break;
	default:
		echo $echo;
		break;
}
//echo $echo;
//phpinfo();
	
?>