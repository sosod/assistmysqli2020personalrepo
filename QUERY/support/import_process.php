<?php if($key=="LIST") { ?>
<style type=text/css>
table, table td {
	border: 1px solid #ffffff;
}
table td.value {
	width: 300px;
}
</style><?php 
}
//$me->arrPrint($_REQUEST);
//$me->arrPrint($_FILES);
//$me->arrPrint($_SESSION);

function dateComparison($date,$string) {
	return (
		   date("d-F-Y",$date)==$string 
		|| date("d-F-y",$date)==$string
		|| date("d F Y",$date)==$string
		|| date("d F y",$date)==$string
		|| date("d-M-Y",$date)==$string
		|| date("d-M-y",$date)==$string 
		|| date("d M Y",$date)==$string
		|| date("d M y",$date)==$string
	);
}

//Upload import file to files/CC/modref/import
$folder = $me->getModRef()."/import";
$me->checkFolder($folder);
$file_name = strtolower($key)."_".date("YmdHis").".csv";
$full_path = "../../files/".strtolower($me->getCmpCode())."/".$folder."/".$file_name;
move_uploaded_file($_FILES['import_file']['tmp_name'], $full_path);

//error_reporting(-1);

function saveSnapShot($buffer,$insert_data) {
//	global $key;
	global $me;
	global $file_name;

	
	$buffer = str_replace('src="/','src="http://assist.ignite4u.co.za/',$buffer);
	$buffer = str_replace('href="/','href="http://assist.ignite4u.co.za/',$buffer);
	$buffer = str_replace('url(/','url(http://assist.ignite4u.co.za/',$buffer);
	
	$buffer.= "<h2>Insert Data</h2>";
	foreach($insert_data as $index => $arr) {
		$buffer.="<h3>".$index."</h3>";
		foreach($arr as $i2=>$a) {
			$buffer.="<p class=b>".$i2;
			if(is_array($a)) {
				foreach($a as $i3 => $b) {
					$buffer." => Array (</p><p>".$i3." => ".$b."</p>";
				}
				$buffer.="<p>)</p>";
			} else {
				$buffer.=" => ".$a."</p>";
			}
		}
	}
	
	
//	$me = new ASSIST();
	$folder = $me->getModRef()."/import";
	$file_name = substr($file_name,0,-4)."_".date("YmdHis")."_IMPORT.html"; 
	$full_path = "../../files/".strtolower($me->getCmpCode())."/".$folder."/".$file_name;
	$file = fopen($full_path,"w");
	fwrite($file,$buffer."\n");
	fclose($file);
	
	//$buffer = "<P>File name: ".$file_name."<p>Path: ".$full_path.$buffer;
	//echo $buffer;
}

//ob_start("callback");

$fatal_error = false;
$error_count = 0;
$warning_count = 0;



//get template fields
$template_header = $template[0];
//get data from import file
$data = $qi->readImportTemplate($full_path);
//$me->arrPrint($data);
$document_header = $data[0];
unset($data[0]); unset($data[1]);




switch($key) {
case "LIST":
	$d = $data;
	$data = array();
	foreach($d as $row_index => $row) {
		foreach($row as $col_index => $value) {
			$data[$col_index][$row_index] = $value;
		}
	}

	$import_lists = $qi->getListsForImport();
	//$me->arrPrint($import_lists);
	
	$insert_data = array();
	//$location = array();
	foreach($template_header as $i => $fld) {
		if(in_array($fld,$import_lists)) {
			//$location[$fld] = $i;
			$import_count = 0;
			$insert_data[$fld] = array();
			$list_data = $qi->getListData($fld);
			echo "<h2>".$names[$fld]."</h2><table>";
			if(in_array($fld,array("query_owner"))) {
				$insert_data['dir'] = array();
				foreach($column as $row_index => $value) {
					$icon = "ok";
					$note = "&nbsp;";
					$dir = $me->code($data[$i-2][$row_index]);
					$sub = $me->code($data[$i-1][$row_index]);
					if(strlen(trim($dir))>0 && strlen(trim($sub))>0) {
						if(isset($list_data['values']['query_owner'][$me->stripStringForComparison($dir."_".$sub)])) {
							$warning_count++;
							$icon = "info";
							$note = "Warning: Duplicate value exists. <u>This item will NOT import.</u>";
						} else {
							$import_count++;
							if(!isset($list_data['values']['dir'][$me->stripStringForComparison($dir)])) {
								$sd = "X";
								$sd2 = $me->stripStringForComparison($dir);
								if(!in_array($dir,$insert_data['dir'])) {
									$insert_data['dir'][] = $dir;
								}
							} else {
								$sd = $list_data['values']['dir'][$me->stripStringForComparison($dir)];
								$sd2 = "";
							}
							$insert_data['query_owner'][] = array(
								'subtxt'=>$sub,
								'subdirid'=>$sd,
								'sd2'=>$sd2,
							);
						}
						echo "
						<tr>
									<td style='padding: 1px;'><div class='ui-state-".$icon." ui-corner-all' style='padding: 3px;'>".$me->getDisplayIcon($icon)."</div></td>
									<td class=value>".$dir." - ".$sub."</td>".
									($icon!="ok" ? "<td style='padding: 1px;'><div class='ui-state-".$icon." ui-corner-all' style='padding: 3px;'><div style='float:left;display:inline;margin-left: 3px;'>".$me->getDisplayIcon($icon)."</div><div style='display:inline;'>".$note."</div></div></td>" : "")."
						</tr>";
					}
				}
				echo "</table>";
				//$me->arrPrint($insert_data['dir']);
			} else {
				if(in_array($fld,array("financial_year"))) {
					echo "<tr><th></th><th>Start Date</th><th>End Date</th><th>".$names[$fld]."</th></tr>";
				}
				$column = $data[$i];
				foreach($column as $row_index => $value) {
					$icon = "ok";
					$note = "&nbsp;";
					if(in_array($fld,array("financial_year","query_owner")) || ( strlen($value)>0 && trim(strtolower($value))!="unspecified" && trim($value)!="-") ) {
						switch($fld) {
						case "financial_year":
							if(trim($value)!="-" && strlen(trim($value))>0) {
								$start_date = $data[$i-2][$row_index];
								$end_date = $data[$i-1][$row_index];
								$start = strtotime($start_date);
								$end = strtotime($end_date);
								if(isset($list_data['values'][$fld][$start."_".$end])) {
									$warning_count++;
									$icon = "info";
									$note = "Warning: Duplicate value exists (Ref: ".$list_data['values'][$fld][$start."_".$end]."). <u>This item will NOT import.</u>";
								} else {
									$import_count++;
									if(isset($list_data['values']['start'][$start])) {
										$icon = "info";
										$warning_count++;
										$note = "Warning: A ".$names[$fld]." already exists with a start date of ".$start_date." but with a different end date.  Please check your date.  This item will still import.";
									} elseif(isset($list_data['values']['end'][$end])) {
										$icon = "info";
										$warning_count++;
										$note.= (strlen($note)>0 && $note!="&nbsp;" ? "<br />" : "Warning: ")."A ".$names[$fld]." already exists with an end date of ".$end_date." but with a different start date.  Please check your date.  This item will still import.";
									} elseif(!dateComparison($start,$start_date)) {
										$icon = "info";
										$warning_count++;
										$note.= (strlen($note)>0 && $note!="&nbsp;" ? "<br />" : "Warning: ")."There appears to be an error comparing the original start date with the one that will be saved by the database.  Please verify that the correct date is displayed.  This item will still import.";
									} elseif(!dateComparison($end,$end_date)) {
										$icon = "info";
										$warning_count++;
										$note.= (strlen($note)>0 && $note!="&nbsp;" ? "<br />" : "Warning: ")."There appears to be an error comparing the original end date with the one that will be saved by the database.  Please verify that the correct date is displayed.  This item will still import.";
									} else {
										$icon = "ok";
										$insert_data[$fld][] = array(
											'start_date' => date("d-F-Y",$start),
											'end_date' => date("d-F-Y",$end),
											'last_day' => date("d-F-Y",$end),
										);
									}
								}
								echo "
								<tr>
									<td style='padding: 1px;'><div class='ui-state-".$icon." ui-corner-all' style='padding: 3px;'>".$me->getDisplayIcon($icon)."</div></td>
									<td>".date("d F Y",$start)."<br /><span style='font-style: italic; font-size: 75%'>(Original: ".$start_date.")</span></td>
									<td>".date("d F Y",$end)."<br /><span style='font-style: italic; font-size: 75%'>(Original: ".$end_date.")</span></td>
									<td>".date("d F Y",$start)." - ".date("d F Y",$end)."</td>".
									($icon!="ok" ? "<td style='padding: 1px;'><div class='ui-state-".$icon." ui-corner-all' style='padding: 3px;'><div style='float:left;display:inline;margin-left: 3px;'>".$me->getDisplayIcon($icon)."</div><div style='display:inline;'>".$note."</div></div></td>" : "")."

								</tr>";
							}
							break;
						case "query_owner":
							break;
						default:
							if(isset($list_data['values'][$me->stripStringForComparison($value)])) {
								$icon = "info";
								$note = "Warning: Duplicate value exists (Ref: ".$list_data['values'][$me->stripStringForComparison($value)]."). <u>This item will NOT import.</u>";
								$warning_count++;
							} else {
								$icon = "ok";
								$import_count++;
								$insert_data[$fld][] = $me->code($value);
							}
							echo "
							<tr>
								<td style='padding: 1px;'><div class='ui-state-".$icon." ui-corner-all' style='padding: 3px;'>".$me->getDisplayIcon($icon)."</div></td>
								<td class=value>".$value."</td>".
								($icon!="ok" ? "<td style='padding: 1px;'><div class='ui-state-".$icon." ui-corner-all' style='padding: 3px;'><div style='float:left;display:inline;margin-left: 3px;'>".$me->getDisplayIcon($icon)."</div><div style='display:inline;'>".$note."</div></div></td>" : "")."
							</tr>";
							break;	//end default
						} //end switch fld
					}
				}//end foreach column value
			}//endif
			echo "</table>";
			if($import_count==0) {
				echo "<p>There are no items to import.</p>";
			} else {
				echo "<p>".$import_count." item(s) will import when you click the Accept button.</p>";
				//$me->arrPrint($insert_data[$fld]);
				//$me->arrPrint($list_data);
			}
		}
	}
	
	
	/*foreach($import_lists as $l) {
		echo "<P>".$location[$l]."</p>";
	}*/
	
	echo "<hr />";
	if($warning_count>0) {
		echo "<P>There are ".$warning_count." non-fatal error(s).  If these are warnings for duplicate values but you can't see the existing values on the New > Query page, check the Setup > Defaults page and confirm that the values haven't been deactivated.  If they are Inactive then they cannot be re-created and they also cannot be used with New Queries whether they are created on New or via the Import function.</p>";
	}
	
	
	break; //end switch $key case LISTS

	
	
	
	
	
	
	
default:

//$me->arrPrint($data);
$insert_data = array();
$id_in_use = $qi->getIDsInUse($key);
$fields = $qi->getObjectFields($key);
$objects = $key=="ACTION" ? $qi->getObjects() : array();
$numbers = array("0","1","2","3","4","5","6","7","8","9");
	//display on screen
	echo "<table>";
	//for development purposes
/*	echo "<tr><th></th>";
		foreach($template[0] as $i => $f) {
			echo "<th>".($f)."</th>";
		}
	echo "</tr>";
	echo "<tr><th></th>";
		foreach($template[1] as $i => $f) {
			echo "<th>".($f)."</th>";
		}
	echo "</tr>";*/
	//template header
	echo "<tr><Th></th>";
		foreach($template_header as $i => $f) {
			echo "<th>".(isset($names[$f]) ? $names[$f] : $f)."</th>";
		}
	echo "</tr>";
	//document header
	echo "<tr><th></th>";
		foreach($template_header as $i => $f) {
			echo "<th>".$document_header[$i]."</th>";
		}
	//data for import
	foreach($data as $d_index => $d) {
		$db_data = array();
		$echo = "";
		$icon = "ok";
		//$note = array();
		$temp = array();
		foreach($template_header as $i => $f) {
			$my_icon = "ok";
			$val = $d[$i];
			$db_val = "";
			if($f=="Query ID") { 
				$f = $key=="ACTION" ? "risk_id" : "id"; 
			}
			//if($key=="ACTION" && $fields[$f]['type']=="IGNORE") {
			//} else {
				switch($fields[$f]['type']) {
					case "IGNORE":
						if($key=="ACTION") {
							if(isset($temp[$f]) && $my_icon=="ok") {
								$display_val = isset($temp[$f]) ? $temp[$f] : "";
								if($me->stripStringForComparison($display_val)!=$me->stripStringForComparison($val)) {
									$my_icon = "info";
									$icon = $icon=="ok" ? "info" : $icon;
									$warning_count++;
									$note = "Your ".$names[$f]." doesn't match the ".$names[$f]." associated with the indicated Query.<br />This won't affect the import but should be checked.";
								}
							}
						}
						break;
					case "OBJECT":
						$width="100px";
						for($i==0;$i<strlen($val);$i++) {						if(in_array(substr($val,$i,1),$numbers)) {							$db_val.=substr($val,$i,1);						}					}
						if(!isset($objects[$db_val])) {
							$icon = "error";
							$my_icon = "error";
							$fatal_error = true;
							$error_count++;
							$note = "Query could not be found.";
						} else {
							$temp['query_description'] = $objects[$db_val]['description'];
							$temp['query_reference'] = $objects[$db_val]['query_reference'];
						}
						$display_val = Risk::REFTAG.$db_val;
						break;
					case "REF":
						$width="100px";
						for($i==0;$i<strlen($val);$i++) {						if(in_array(substr($val,$i,1),$numbers)) {							$db_val.=substr($val,$i,1);						}					}
						if(in_array($db_val,$id_in_use)) {
							$icon = "error";
							$my_icon = "error";
							$fatal_error = true;
							$error_count++;
							$note = "Query ID is already in use.";
						} else {
							$id_in_use[] = $db_val;
						}
						$display_val = Risk::REFTAG.$db_val;
						break;
					case "DATE":
						$width="100px";
						if(strlen(trim($val))==0) { 
							$icon = "error";
							$my_icon = "error";
							$fatal_error = true;
							$error_count++;
							$note = "Required field.";
							$display_val = "";
						} else {
							$v = strtotime($val);
							$db_val = date("d-M-Y",$v);
							$display_val = date("d F Y",$v);
							if(!dateComparison($v,$val)) {
								$my_icon = "info";
								$icon = $icon=="ok" ? "info" : $icon;
								$warning_count++;
								$note = "Date verification failed. Please check the displayed date.";
							}
						}
						break;
					case "LIST":
						$width="200px";
						$list_data = $qi->getListData($f,true);
						if(strlen($val)==0 && $fields[$f]['required']==true) {
								$icon = "error";
								$my_icon = "error";
								$fatal_error = true;
								$error_count++;
								$note = "Required field";
								$display_val = "";
						} elseif($me->stripStringForComparison($val)=="unspecified" || (strlen($val)==0 && $fields[$f]['required']!=true)) {
							$db_val = 0;
							$display_val = "Unspecified";
						} elseif($f=="financial_year") {
							$v = strtotime($val);
							if(isset($list_data['values']['end'][$v])) {
								$db_val = $list_data['values']['end'][$v];
								$display_val = $list_data['rows'][$db_val]['value'];
							} else {
								$icon = "error";
								$my_icon = "error";
								$fatal_error = true;
								$error_count++;
								$note = "Unable to find the list item specified.";
								$display_val = $v." (".date("d F Y",$v).")";//"N/A";
							}
						} elseif($f=="query_owner") {
							$v = $me->stripStringForComparison($val);
							if(isset($list_data['values'][$f][$v])) {
								$db_val = $list_data['values'][$f][$v];
								$display_val = $list_data['rows']['dir'][$list_data['rows']['dirsub'][$db_val]['dirid']]['value']." - ".$list_data['rows']['dirsub'][$db_val]['value'];
							} else {
								$icon = "error";
								$my_icon = "error";
								$fatal_error = true;
								$error_count++;
								$note = "Unable to find the list item specified.";
								$display_val = "N/A";
							}
						} else {
							$v = $me->stripStringForComparison($val);
							if(isset($list_data['values'][$v])) {
								$db_val = $list_data['values'][$v];
								$display_val = stripslashes($list_data['rows'][$db_val]['value']);
							} else {
								$icon = "error";
								$my_icon = "error";
								$fatal_error = true;
								$error_count++;
								$note = "Unable to find the list item specified.";
								$display_val = "ERROR";
							}
						}
						break;
					case "TEXT":
					case "VC":
					default:
						$width="300px";
						if($fields[$f]['required'] && strlen(trim($val))==0) {
								$icon = "error";
								$my_icon = "error";
								$fatal_error = true;
								$error_count++;
								$note = "Required field.";
								$display_val = "";
						} else {
							$db_val = $me->code($val);
							$display_val = $db_val;
						}
						break;
				}
				if($fields[$f]['type']!="IGNORE") {
					$db_data[$f] = $db_val;
				}
			//}
			$echo.= "<td width=".$width.">".str_replace(chr(10),"<br />",$display_val)."<br /><span style='font-size: 75%' class=i>(Original: '".(strlen($val)>50?substr($val,0,45)."...":$val)."')</span>".(($my_icon!="ok") ? $me->getDisplayResult(array($my_icon,$note)) : "")."</td>";
		}
		echo "<tr><td style='padding: 1px;'><div class='ui-state-".$icon." ui-corner-all' style='padding: 3px;'>".$me->getDisplayIcon($icon)."</div></td>".$echo;
		//if(count($note)>0) {
			//echo "<td>".$me->getDisplayIcon($my_icon)." ".implode("<br />",$note)."</td>";
		//}
		echo "</tr>";
		if($my_icon=="ok") {
			$insert_data[] = $db_data;
		}
	}
	echo "</table>";
	echo "<hr />";
	if($warning_count>0) {
		echo "<P>There are ".$warning_count." non-fatal error(s).  </p>";
	}
	break;//end switch $key case default

}	//end switch $key







	if(!$fatal_error && $error_count==0) {
		echo "<form name=frm_accept method=post action=import_process_ajax.php>
		<input type=hidden name=data value='".base64_encode(serialize($insert_data))."' />
		<input type=hidden name=key value='$key' />
		<input type=hidden name=act value='ACCEPT' />
		
		<div style='width: 50%; text-align: center'><p><input type=button value=Accept class=isubmit /></p></div>
		</form>
		<script type=text/javascript>
		var insert_data = \"".base64_encode(serialize($insert_data))."\";
		$(function() {
			$('.isubmit').click(function() {
				//FOR DEVELOPMENT PURPOSES
				//alert(insert_data);
				//$('form[name=frm_accept]').submit();
				//LIVE
				var result = AssistHelper.doAjax('import_process_ajax.php',AssistForm.serialize($('form[name=frm_accept]')));
				$('<div />',{id:'dlg_process',title:'Processing...',html:'<p>Please be patient while your import is processed.</p><p class=center><img src=\'../../pics/ajax_loader_v2.gif\' /></p>'}).dialog({modal:true,closeOnEscape:false});
				$('.ui-dialog-titlebar').hide();
				if(result[0]=='ok') {
					document.location.href = 'index.php?key=".$key."&r[]='+result[0]+'&r[]='+result[1];
				} else {
					$('#dlg_process').dialog(\"close\");
					alert('The following error has occurred.\\n\\n'+result[1]+'\\n\\nPlease try again.');
				}
			});
			//FIXES PROBLEM OF GIF ANIMATION STOPPING ON ESCAPE KEY
			$(document).keydown(function(event){
				if (event.keyCode == 27) {
					event.preventDefault();
				}
			});			
		});
		</script>
		";
	} else {
		echo "<p>".$error_count." fatal error(s) exist and the import cannot be completed.  Please correct the errors (highlighted in red) and try again.</p>";
	}








//ob_end_flush();
saveSnapShot(ob_get_contents(),$insert_data);

?>