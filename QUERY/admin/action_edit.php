<?php
$scripts = array( 'jquery.ui.action.js','menu.js','actions.js'  );
$styles = array( 'colorpicker.css' );
$page_title = "Action Edit";;
require_once("../inc/header.php");
$risk 	  = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$query 	  = $risk -> getQueryDetail($_GET['id']);

$colObj   = new QueryColumns();
$rowNames = $colObj -> getHeaderList();
?>
<script language="javascript">
	$(function(){
		$("table#edit_action_table").find("th").css({"text-algn":"left"})
		$("#actions").action({risk_id:$("#risk_id").val(), editAction:true, page:"edit", section:"admin"});
	});
</script>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table class="noborder" width="100%">
  <tr>
    <td class="noborder" valign="top" width="50%">
			<table align="left"border="1" id="edit_action_table" width="100%"> 
			  <tr>
			  	<td class="noborder" colspan="2"><h4>Query Details</h4></td>
			  </tr>
       		    <tr>
                    <th><?php echo $rowNames['query_item']; ?>:</th>
                    <td>#<?php echo $query['query_item']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_reference']; ?>:</th>
                    <td><?php echo $query['query_reference']; ?></td>
                  </tr>      
                 <tr>
                    <th><?php echo $rowNames['query_source']; ?>:</th>
                    <td><?php echo $query['query_source']; ?></td>
                  </tr>                  
                  <tr>
                    <th><?php echo $rowNames['query_description']; ?>:</th>
                    <td><?php echo $query['description']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_background']; ?>:</th>
                    <td><?php echo $query['background']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_type']; ?>:</th>
                    <td> <?php echo $query['query_type']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_category']; ?>:</th>
                    <td><?php echo $query['query_category']; ?></td>
                  </tr>
	              <tr>
                    <th><?php echo $rowNames['financial_exposure']; ?>:</th>
                    <td><?php echo $query['financial_exposure']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['monetary_implication']; ?>:</th>
                    <td><?php echo $query['monetary_implication']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['risk_level']; ?>:</th>
                    <td><?php echo $query['risk_level']; ?></td>
                  </tr>      
                  <tr>
                    <th><?php echo $rowNames['risk_type']; ?>:</th>
                    <td><?php echo $query['risk_type']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['risk_detail']; ?>:</th>
                    <td><?php echo $query['risk_detail']; ?></td>
                  </tr>      
                  <tr>
                    <th><?php echo $rowNames['finding']; ?>:</th>
                    <td><?php echo $query['finding']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['internal_control_deficiency']; ?>:</th>
                    <td><?php echo $query['internal_control_deficiency']; ?></td>
                  </tr>        
                  <tr>
                    <th><?php echo $rowNames['recommendation']; ?>:</th>
                    <td><?php echo $query['recommendation']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['client_response']; ?>:</th>
                    <td><?php echo $query['client_response']; ?></td>
                  </tr>    
                  <tr>
                    <th><?php echo $rowNames['auditor_conclusion']; ?>:</th>
                    <td><?php echo $query['auditor_conclusion']; ?></td>
                  </tr> 
                 <tr>
                   <th><?php echo $rowNames['financial_year']; ?>:</th>
                   <td><?php echo $query['financial_year']; ?></td>
                 </tr>
                 <tr>
                    <th><?php echo $rowNames['query_owner']; ?>:</th>
                    <td><?php echo $query['query_owner']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_date']; ?>:</th>
                    <td><?php echo $query['query_date']; ?></td>
                  </tr>      
                  <tr>
                  	<th><?php echo $rowNames['query_deadline_date']; ?>:</th>
                  	<td><?php echo $query['query_deadline_date']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['attachements']; ?>:</th>
                    <td><?php Attachment::displayAttachmentOnly($query['attachment'], 'query');  ?></td>
                  </tr>  
	              <tr>
		            <td align="left" class="noborder">
		                <?php $me->displayGoBack("",""); ?>
		            </td>
		            <td align="right" class="noborder"> 
		            </td>
	               </tr>			             
			</table>
    </td>
    <td class="noborder" valign="top" width="50%">
       <div id="actions"></div>
    </td>
  </tr>
</table>
<input type="hidden" name="risk_id" id="risk_id" value="<?php echo $_GET['id']; ?>"  />
<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['tid']; ?>"  />
