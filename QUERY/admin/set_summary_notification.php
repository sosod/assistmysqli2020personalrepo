<?php
$scripts    = array('jquery.query.summarynotification.js','menu.js');
$styles     = array( 'colorpicker.css' );
$page_title = "My profile";
require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$("#summarynotification").summarynotification();
	});
</script>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table class="noborder">
   <tr>
    <td colspan="3" class="noborder"><div id="summarynotification"></div></td>
   </tr>
    <tr>
        <td align="left" class="noborder"><?php $me->displayGoBack("",""); ?></td>
        <td align="right" class="noborder"><?php $admire_helper->displayAuditLogLink("summary_notifications_logs", true); ?></td>
    </tr>
</table>

