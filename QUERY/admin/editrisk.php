<?php
$scripts = array( 'query.js', 'menu.js','ajaxfileupload.js', 'ignite.textcounter.js'  );
$styles = array( );
$page_title = "Edit Query";
require_once("../inc/header.php");
//require_once("controller.php");
$risk 		 = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$rsk 		 = $risk -> getARisk( $_REQUEST['id']);
$riskStatus  = $risk -> getRiskUpdate( $rsk['id'] );
$statusId 	 = ((empty($riskStatus) || !isset($riskStatus )) ? $rsk['status'] : $riskStatus['status'] );
$type 		 = new RiskType( "", "", "", "" );
$riskTypes	 = $type-> getActiveRiskType();	
$typeOb 	 = new QueryType( "", "", "", "" );
$types	 	 = $typeOb-> getActiveRiskType();	
$riskcat 	 = new RiskCategory( "", "", "", "" );
$categories  = $riskcat  -> getCategory() ;
$fin 		 = new FinancialExposure( "", "", "", "", "");
$finExposure = $fin -> getFinancialExposure() ;
$rkl 		 = new RiskLevel( "", "", "", "", "","");
$rLevel 	 = $rkl -> getLevel() ;
$uaccess 	 = new UserAccess( "", "", "", "", "", "", "", "", "" );
$users 		 =  $uaccess -> getUsers() ;
$getstat 	 = new RiskStatus( "", "", "" );
$statuses 	 = $getstat -> getStatus();
$likeRating  = array();
$impRating 	 = array();
$ctrRating 	 = "";
$dire 		 = new Directorate("", "", "", "");
$direct      = $dire -> getDirectorate();
$sourceObj     = new QuerySource();
$query_sources = $sourceObj -> getQuerySources();
$colObj          = new QueryColumns();
$rowNames        = $colObj -> getHeaderList();
$diradmin        = new Administrator($_SESSION['tid']);
$useraccess      = $diradmin -> getAdminAccess();
?>
<script>
$(function(){
	$("table#edit_risk_table").find("th").css({"text-align":"left"})
});
</script>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<div style="clear:both;">
        <form id="edit-risk-form" method="post" enctype="multipart/form-data">
        <table align="left" border="1" id="edit_risk_table">
       		 <tr>
                    <th><?php echo $rowNames['query_item']; ?>:</th>
                    <td>#<?php echo $rsk['id'] ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_reference']; ?>:</th>
                    <td><input type="text" id="query_reference" name="query_reference" value="<?php echo $rsk['query_reference'] ?>" /></td>
                  </tr>   
                  <tr>
                    <th><?php echo $rowNames['query_source']; ?>:</th>
                    <td>
                        <select id="query_source" name="query_source">
                            <option value=""><?php echo $select_box_text ?></option>
                            <?php
                             foreach($query_sources as $q_index => $q_source)
                             {
                            ?>
                              <option value="<?php echo $q_source['id']; ?>" <?php echo ($q_source['id'] == $rsk['query_source'] ? "selected='selected'" : "") ?>><?php echo $q_source['name']; ?></option>
                            <?php
                             }
                            ?>
                        </select>         
                    </td>
                  </tr>                                     
                  <tr>
                    <th><?php echo $rowNames['query_type']; ?>:</th>
                    <td>
                        <select id="query_type" name="query_type">
                            <option value=""><?php echo $select_box_text ?></option>
                            <?php
								foreach($types as $type ){
							?>
                            <option value="<?php echo $type['id'] ?>"
                            	<?php
									if( $type['id'] == $rsk['type'] ) 
									{
										?>
                                        selected="selected"
                                        <?php
									}
								?>
                            ><?php echo $type['name']; ?></option>
							<?php	
							}
							?>
                        </select>
                    </td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_category']; ?>:</th>
                    <td>
                        <select id="query_category" name="query_category">
                            <option value=""><?php echo $select_box_text ?></option>
                           <?php
								foreach($categories as $category ){
							?>
                            <option value="<?php echo $category['id']; ?>"
                            	<?php
									if( $category['id'] == $rsk['category'] ) 
									{
										?>
                                        selected="selected"
                                        <?php
									} ?>
									><?php echo $category['name']; ?></option>
							<?php	
							}
							?>
                        </select>    
                    </td>
                  </tr>
			      <tr>
			        <th><?php echo $rowNames['query_date']; ?>:</th>
			        <td><input type="text" name="query_date" id="query_date" class="datepicker" value="<?php echo $rsk['query_date']; ?>" readonly="readonly" /></td>
			      </tr>                   
                  <tr>
                    <th><?php echo $rowNames['query_description']; ?>:</th>
                    <td><textarea name="query_description" id="query_description" rows="7" cols="35"><?php echo $rsk['description']; ?></textarea></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_background']; ?>:</th>
                    <td><textarea name="query_background" id="query_background" rows="7" cols="35"><?php echo $rsk['background']; ?></textarea></td>
                  </tr>
     			<tr>
			        <th><?php echo $rowNames['query_owner']; ?>:</th>
			        <td>
			        <select id="directorate" name="directorate" class="responsibility">
			                <option value=""><?php echo $select_box_text ?></option>
			                <?php 
			                	foreach( $direct as $k => $dir ){
			                ?>
				                <option value="<?php echo $dir['subid']; ?>"
				                <?php 
				                if( $dir['subid'] == $rsk['sub_id']){
				                	?>
				                	selected="selected"
				                <?php 
			                	}
				                ?> 
				               ><?php echo $dir['dirtxt']; ?></option>
			                <?php 
			                	}
			                ?>
			         </select>        
			        </td>
       		   </tr>                  
                <tr>
                  	<th><?php echo $rowNames['query_deadline_date']; ?>:</th>
                  	<td><input type="text" name="query_deadline_date" id="query_deadline_date" class="datepicker" value="<?php echo $rsk['query_deadline_date']; ?>" readonly="readonly" /></td>
                </tr>                 
<tr class="more_details">
        <th><?php echo $rowNames['financial_exposure']; ?>:</th>
        <td>
        	<select id='financial_exposure' name='financial_exposure'>
            	<option value=""><?php echo $select_box_text ?></option>
                <?php
					foreach($finExposure as $key => $finExp){
				?>
                <option value='<?php echo $finExp['id']; ?>'
				<?php
                    if( $finExp['id'] == $rsk['financial_exposure'] ) 
                    {
                        ?>
                        selected="selected"
                        <?php
                    } ?>                
                ><?php echo $finExp['name']; ?></option>
                <?php
				}
				?>
        	</select>
        </td>
      </tr>
      <tr class="more_details">
        <th><?php echo $rowNames['monetary_implication']; ?>:</th>
        <td>
        	<textarea id='monetary_implication' name='monetary_implication' class="textcounter"><?php echo $rsk['monetary_implication'] ?></textarea>
        </td>
      </tr>
      <tr class="more_details">
        <th><?php echo $rowNames['risk_level']; ?>:</th>
        <td>
        <select id="risk_level" name="risk_level">
                <option value=""><?php echo $select_box_text ?></option>
                <?php
					foreach($rLevel as $key => $rskLevel){
				?>
                <option value="<?php echo $rskLevel['id']; ?>"
				<?php
                    if( $rskLevel['id'] == $rsk['risk_level'] ) 
                    {
                        ?>
                        selected="selected"
                        <?php
                    } ?>                    
                ><?php echo $rskLevel['name']; ?></option>
                <?php
				}
				?>               
         </select>            
        </td>
      </tr>      
      <tr class="more_details">
        <th><?php echo $rowNames['risk_type']; ?>:</th>
        <td>
        <select id="risk_type" name="risk_type">
                <option value=""><?php echo $select_box_text ?></option>
                <?php
					foreach($riskTypes as $key => $risk_type){
				?>
                <option value="<?php echo $risk_type['id']; ?>"
				<?php
                    if( $risk_type['id'] == $rsk['risk_type'] ) 
                    {
                        ?>
                        selected="selected"
                        <?php
                    } ?>                       
                ><?php echo $risk_type['name']; ?></option>
                <?php
				}
				?>                  
         </select>            
        </td>
      </tr>
      <tr class="more_details">
        <th><?php echo $rowNames['risk_detail']; ?>:</th>
        <td>       
        	<textarea rows="5" cols="35" id='risk_detail' name='risk_detail'><?php echo $rsk['risk_detail'] ?></textarea>
      </tr>      
      <tr class="more_details">
        <th><?php echo $rowNames['finding']; ?>:</th>
        <td>
        	 <?php 
        	 	$findings = split("__", $rsk['finding']);
        	 	if(!empty($findings)) {
        	 		foreach( $findings as $f => $fVal ) {
        	 ?>
        		<p><textarea class="find" id='finding' name='finding'><?php echo $fVal; ?></textarea></p>
        	<?php
        	 		} 
        	 	}
        	?>
            <input type="submit" name="add_another_finding" id="add_another_finding" value="Add Another"  /> 
        </td>
      </tr>
      <tr class="more_details">
        <th><?php echo $rowNames['internal_control_deficiency']; ?>:</th>
        <td>
			<?php 
         	$internalControl = split("__", $rsk['internal_control_deficiency']);
         	if(!empty($internalControl)) {
         		foreach( $internalControl as $f => $icVal ) {
         	?>
         		<p>
            		<textarea class="internalcontrol" id="internal_control_deficiency" name="internal_control_deficiency"><?php echo $icVal; ?></textarea>
            	</p>
        	<?php
         			} 
         		}
        	?> 
        	<input type="submit" name="add_another_internalcontrol" id="add_another_internalcontrol" value="Add Another"  />                 
        </td>
      </tr> 
      <tr class="more_details">
        <th><?php echo $rowNames['recommendation']; ?>:</th>
        <td>
        	<?php 
        		$recomendations = split("__", $rsk['recommendation']);
        		if(!empty($recomendations)){
        			foreach($recomendations as $r => $rVal){
        	?>
	            <p><textarea class="recommend" id='recommendation' name='recommendation'><?php echo $rVal; ?></textarea></p>
            <?php 
        			}
        		}
            ?>
            <input type="submit" name="add_another_recommendation" id="add_another_recommendation" value="Add Another"  />         
        </td>
      </tr>
      <tr class="more_details">
        <th><?php echo $rowNames['client_response']; ?>:</th>
        <td>
		        <?php 
		        	$cresponse = split("__", $rsk['client_response']);
		        	if( !empty($cresponse)){
		        		foreach($cresponse as $c => $cVal){
		        ?>
 	       			<p><textarea class="clientresponse" id='client_response' name='client_response'><?php echo $cVal; ?></textarea></p>
	        	<?php 
		        	}
	        	}
	        	?>
        	<input type="submit" name="add_another_clientresponse" id="add_another_clientresponse" value="Add Another"  />                 
        </td>
      </tr>  
      <tr class="more_details">
        <th><?php echo $rowNames['auditor_conclusion']; ?>:</th>
        <td>					        <?php 
	        	$aUresponse = split("__", $rsk['auditor_conclusion']);
	        	if( !empty($aUresponse)){
	        		foreach($aUresponse as $c => $aVal){
	        ?>
        			<p><textarea class="auditorconclusion" id="auditor_conclusion" name="auditor_conclusion"><?php echo $aVal; ?></textarea></p>
        	<?php 
	        	}
        	}
        	?>
        	<input type="submit" name="add_another_auditorconclusion" id="add_another_auditorconclusion" value="Add Another"  />                 
        </td>
      </tr> 
      <tr>
        	<th>Attachment:</th>
            <td>
                <input id="query_attachment_<?php echo $rsk['id']; ?>" name="query_attachment_<?php echo $rsk['id']; ?>" type="file" class="upload_query" />                            
                <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
                  <small>You can attach more than 1 file.</small>
                </p>
                <?php
                   Attachment::displayAttachmentList($rsk['attachment'], 'query');
                ?>
            </td>
   </tr>  
                  <tr>
					<td></td>
                  	<td align="right">
                    	<input type="submit" name="edit_risk" id="edit_risk" value="Save Changes " class="isubmit" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                         if(isset($useraccess['delete']))
                         {
                            if(isset($useraccess['delete'][$rsk['sub_id']]))
                            {
                         ?>
                            <input type="submit" name="delete_risk" id="delete_risk" value="Delete Query" class="idelete" /> 
                         <?php
                            }
                         }
                        ?>
                        
                        &nbsp;&nbsp;
                        <span style='float:right'><input type="submit" name="more_detail" id="more_detail" value="More Detail" class="iinform" /></span>
                        <input type="hidden" name="riskid" value="<?php echo $rsk['id']; ?>" id="riskid" /> 
                        <input type="hidden" name="risk_id" value="<?php echo $rsk['id']; ?>" id="risk_id" class="logid" /> 
                   </td>
                  </tr>
                  <tr>
                  	<td align="left" class="noborder">
                    	<?php $me->displayGoBack("",""); ?>
                    </td>
                  	<td align="right" class="noborder">
                    	<?php $admire_helper->displayAuditLogLink( "risk_edits" , false); ?>
                    </td>                    
                  </tr>
               </table>
           </form>
      </div>
     <div id="editLogs" style="clear:both;">
  </div>
