<?php
$scripts = array( 'query.js','menu.js', 'ajaxfileupload.js', 'ignite.textcounter.js' );
$styles = array( 'colorpicker.css' );
$page_title = "Update Risk";
require_once("../inc/header.php");

//require_once("controller.php");
$queryObj = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$query 	  = $queryObj -> getQueryDetail($_GET['id']);
$colObj   = new QueryColumns();
$rowNames = $colObj -> getHeaderList();
$type 		 = new RiskType( "", "", "", "" );
$types		 = $type-> getType();	
$riskcat 	 = new RiskCategory( "", "", "", "" );
$categories  = $riskcat  -> getCategory() ;
$fin 		 = new FinancialExposure( "", "", "", "", "");
$finExposure = $fin -> getFinancialExposure() ;
$rkl 		 = new RiskLevel( "", "", "", "", "","");
$rLevel 	 = $rkl -> getLevel() ;
$uaccess 	 = new UserAccess( "", "", "", "", "", "", "", "", "" );
$users 		 =  $uaccess -> getUserAccess() ;
$getstat 	 = new RiskStatus( "", "", "" );
$statuses 	 = $getstat -> getStatus();
$likeRating  = array();
$impRating 	 = array();
$ctrRating 	 = "";
?>
<script>
$(function(){
	$("table#updaterisk_table").find("th").css({"text-align":"left"})
});
</script>
<style>
	th{
		text-align:left;
	}
</style>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table class="noborder" width="100%">
    <tr>
    <td class="noborder" width="50%">
        <table align="left"border="1" id="updaterisk_table" width="100%"> 
        <tr>
            <td class="noborder" colspan="2"><h4>Query Details</h4></td>
        </tr>
       		    <tr>
                    <th><?php echo $rowNames['query_item']; ?>:</th>
                    <td>#<?php echo $query['query_item']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_reference']; ?>:</th>
                    <td><?php echo $query['query_reference']; ?></td>
                  </tr>      
                 <tr>
                    <th><?php echo $rowNames['query_source']; ?>:</th>
                    <td><?php echo $query['query_source']; ?></td>
                  </tr>                  
                  <tr>
                    <th><?php echo $rowNames['query_description']; ?>:</th>
                    <td><?php echo $query['description']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_background']; ?>:</th>
                    <td><?php echo $query['background']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_type']; ?>:</th>
                    <td> <?php echo $query['query_type']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_category']; ?>:</th>
                    <td><?php echo $query['query_category']; ?></td>
                  </tr>
	              <tr>
                    <th><?php echo $rowNames['financial_exposure']; ?>:</th>
                    <td><?php echo $query['financial_exposure']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['monetary_implication']; ?>:</th>
                    <td><?php echo $query['monetary_implication']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['risk_level']; ?>:</th>
                    <td><?php echo $query['risk_level']; ?></td>
                  </tr>      
                  <tr>
                    <th><?php echo $rowNames['risk_type']; ?>:</th>
                    <td><?php echo $query['risk_type']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['risk_detail']; ?>:</th>
                    <td><?php echo $query['risk_detail']; ?></td>
                  </tr>      
                  <tr>
                    <th><?php echo $rowNames['finding']; ?>:</th>
                    <td><?php echo $query['finding']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['internal_control_deficiency']; ?>:</th>
                    <td><?php echo $query['internal_control_deficiency']; ?></td>
                  </tr>        
                  <tr>
                    <th><?php echo $rowNames['recommendation']; ?>:</th>
                    <td><?php echo $query['recommendation']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['client_response']; ?>:</th>
                    <td><?php echo $query['client_response']; ?></td>
                  </tr>    
                  <tr>
                    <th><?php echo $rowNames['auditor_conclusion']; ?>:</th>
                    <td><?php echo $query['auditor_conclusion']; ?></td>
                  </tr> 
                 <tr>
                   <th><?php echo $rowNames['financial_year']; ?>:</th>
                   <td><?php echo $query['financial_year']; ?></td>
                 </tr>
                 <tr>
                    <th><?php echo $rowNames['query_owner']; ?>:</th>
                    <td><?php echo $query['query_owner']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_date']; ?>:</th>
                    <td><?php echo $query['query_date']; ?></td>
                  </tr>      
                  <tr>
                  	<th><?php echo $rowNames['query_deadline_date']; ?>:</th>
                  	<td><?php echo $query['query_deadline_date']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['attachements']; ?>:</th>
                    <td><?php Attachment::displayAttachmentOnly($query['attachment'], 'query');  ?></td>
                  </tr>
        <tr>
            <td align="left" class="noborder">
            <?php $me->displayGoBack("",""); ?>
            </td>
            <td align="right" class="noborder"> 
            <?php $admire_helper->displayAuditLogLink( "risk_update" , false); ?>
            </td>
        </tr>			             
        </table>
    </td>
    <td class="noborder" width="50%">
        <form id="update-risk-form" method="post" enctype="multipart/form-data">
            <table width="100%">
                <tr>
                    <td colspan="2" class="noborder"><h4>Query Update</h4></td>
                </tr>
                <tr>
                    <th style="text-align:left;"><?php echo $rowNames['financial_exposure']; ?>:</th>
                    <td>
                        <select id='financial_exposure' name='financial_exposure'>
                             <option value=""><?php echo $select_box_text ?></option>
                            <?php
                             foreach($finExposure as $key => $finExp)
                             {
                            ?>
                              <option value="<?php echo $finExp['id']; ?>" 
                                <?php echo ($finExp['id'] == $query['financialexposure'] ? "selected='selected'" : "");  ?>>
                                <?php echo $finExp['name']; ?>
                              </option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th style="text-align:left;"><?php echo $rowNames['monetary_implication']; ?>:</th>
                    <td>
                        <textarea id="monetary_implication" name="monetary_implication"><?php echo trim($query['monetary_implication']); ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th style="text-align:left;"><?php echo $rowNames['risk_level']; ?>:</th>
                    <td>
                        <select id="risk_level" name="risk_level">
                            <option value=""><?php echo $select_box_text ?></option>
                            <?php
                            foreach($rLevel as $key => $rskLevel)
                            {
                            ?>
                              <option value="<?php echo $rskLevel['id']; ?>"
                               <?php echo ($rskLevel['id'] == $query['risklevel'] ? "selected='selected'" : "");  ?>>
                               <?php echo $rskLevel['name']; ?>
                              </option>
                            <?php
                            }
                            ?>               
                        </select>            
                    </td>
                </tr>      
                <tr>
                    <th style="text-align:left;"><?php echo $rowNames['risk_type']; ?>:</th>
                    <td>
                        <select id="risk_type" name="risk_type">
                            <option value=""><?php echo $select_box_text ?></option>
                            <?php
                             foreach($types as $key => $type)
                             {
                            ?>
                            <option value="<?php echo $type['id']; ?>"
                               <?php echo ($type['id'] == $query['risktype'] ? "selected='selected'" : ""); ?>>
                               <?php echo $type['name']; ?>
                            </option>
                            <?php
                             }
                            ?>                  
                        </select>            
                    </td>
                </tr>
                <tr>
                    <th style="text-align:left;"><?php echo $rowNames['risk_detail']; ?>:</th>
                    <td>
                        <textarea rows="5" cols="35" id='risk_detail' name='risk_detail'><?php echo $query['risk_detail'] ?></textarea>
                    </td>
                </tr>      
                <tr>
                    <th style="text-align:left;"><?php echo $rowNames['finding']; ?>:</th>
                    <td>
                        <?php 
                        $findings = explode("__", $query['finding']);
                        if(!empty($findings)) 
                        {
                          foreach($findings as $f => $fVal) 
                          {
                        ?>
                          <p><textarea class="find" id='finding' name='finding'><?php echo $fVal; ?></textarea></p>
                        <?php
                          } 
                        }
                        ?>     
                        <input type="submit" name="add_another_finding" id="add_another_finding" value="Add Another"  />  
                    </td>
                </tr>
                <tr>
                    <th><?php echo $rowNames['internal_control_deficiency']; ?>:</th>
                    <td>
                        <?php 
                        $internalControl = explode("__", $query['internal_control_deficiency']);
                        if(!empty($internalControl))
                        {
                            foreach( $internalControl as $f => $icVal) 
                            {
                            ?>
                            <p>
                              <textarea class="internalcontrol" id="internal_control_deficiency" name="internal_control_deficiency"><?php echo $icVal; ?></textarea>
                            </p>
                            <?php
                            } 
                        }
                        ?> 
                        <input type="submit" name="add_another_internalcontrol" id="add_another_internalcontrol" value="Add Another" />
                    </td>
                </tr> 				      
                <tr>
                    <th style="text-align:left;"><?php echo $rowNames['recommendation']; ?>:</th>
                    <td>
                        <?php 
                        $recomendations = explode("__", $query['recommendation']);
                        if(!empty($recomendations))
                        {
                            foreach($recomendations as $r => $rVal)
                            {
                            ?>
                            <p>
                              <textarea class="recommend" id="recommendation" name="recommendation"><?php echo $rVal; ?></textarea>
                            </p>
                            <?php 
                            }
                        }
                        ?>				        
                        <input type="submit" name="add_another_recommendation" id="add_another_recommendation" value="Add Another" />
                    </td>
                </tr>
                <tr>
                    <th style="text-align:left;"><?php echo $rowNames['client_response']; ?>:</th>
                    <td>
                        <?php 
                        $cresponse = explode("__", $query['client_response']);
                        if( !empty($cresponse))
                        {
                            foreach($cresponse as $c => $cVal)
                            {
                            ?>
                            <p>
                              <textarea class="clientresponse" id="client_response" name="client_response"><?php echo $cVal; ?></textarea>
                            </p>
                            <?php 
                            }
                        }
                        ?>				         
                        <input type="submit" name="add_another_clientresponse" id="add_another_clientresponse" value="Add Another" />
                    </td>
                </tr>    
                <tr>
                    <th><?php echo $rowNames['auditor_conclusion']; ?>:</th>
                    <td>					        <?php 
                        $aUresponse = explode("__", $query['auditor_conclusion']);
                        if(!empty($aUresponse))
                        {
                            foreach($aUresponse as $c => $aVal)
                            {
                            ?>
                            <p>
                              <textarea class="auditorconclusion" id="auditor_conclusion" name="auditor_conclusion"><?php echo $aVal; ?></textarea>
                            </p>
                            <?php 
                            }
                        }
                        ?>
                        <input type="submit" name="add_another_auditorconclusion" id="add_another_auditorconclusion" value="Add Another"  />                 
                    </td>
                </tr>             
                <tr>
                <th style="text-align:left;">Response:</th>
                    <td>
                      <textarea name="risk_response" id="risk_response" cols="35" rows="7"></textarea>
                    </td>
                </tr>
                <tr>
                    <th style="text-align:left;">Status:</th>
                    <td>
                        <select id="risk_status" name="risk_status">
                            <option disabled=disabled value=""><?php echo $select_box_text ?></option>
                                <?php
                                foreach($statuses as $status)
                                {
                                ?>
                                <option value="<?php echo $status['id']; ?>"
                                <?php
                                if($status['id'] == $query['status']) 
                                {
                                ?>
                                    selected="selected"
                                <?php
                                } else {
                                    if($status['id'] == 1)
                                    {
                                    ?>
                                        selected="selected"
                                    <?php 
                                    }
                                } 
                                if($status['id'] == 1)
                                {
                                ?>
                                    disabled="disabled"
                                <?php 
                                }
                                ?>>
                                <?php echo $status['name']; ?>
                            </option>
                        <?php	
                        }
                        ?>                             
                        </select>      
                    </td>
                </tr>
                <tr>
                    <th>Remind On:</th>
                    <td>
                      <input type="text" name="remind_on" id="remind_on" class="datepicker" readonly="readonly" value="<?php echo (strtotime($query['remind_on']) > 0 ? date('d-M-Y', strtotime($query['remind_on'])) : ''); ?>" />
                    </td>
                </tr>                  
                <tr>
                <th style="text-align:left;"><?php echo $rowNames['attachements']; ?>:</th>
                    <td>
                        <input id="update_qry_attachment_<?php echo $query['query_item']; ?>" name="update_qry_attachment_<?php echo $query['query_item']; ?>" type="file" onChange="updateQueryUploader(this)" />                            
                        <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
                        <small>You can attach more than 1 file.</small>
                        </p>
                        <?php
                           Attachment::displayAttachmentList($query['attachment'], 'query');
                        ?>
                    </td>
                </tr> 
                <tr>
                    <th style="text-align:left;"></th>
                    <td align="right">
                        <input type="submit" name="save_update" id="save_update" value="Save" class="isubmit" />
                        &nbsp;&nbsp;&nbsp;
                        <input type="submit" name="cancel" id="cancel" value="Cancel" class="idelete" />                        
                        <input type="hidden" name="riskid" value="<?php echo $query['query_item']; ?>" id="riskid" />
                        <input type="hidden" name="risk_id" value="<?php echo $query['query_item']; ?>" id="risk_id" class="logid"/>
                        <input type="hidden" name="userrespoid" value="<?php //echo $query['risk_owner']; ?>" id="userrespoid" />  
                    </td>
                </tr>	
            </table>	
        </form>	
    </td>
 </tr>
</table>
