<?php
@session_start();
include_once("../inc/init.php");
$gHeaders = array();
$gActionStatus = array();


switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ) 
{
	case "updateQuery":
		$queryObj 		= new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$query_id       = $_POST['id'];
		$query  		= $queryObj -> getQueryDetail($query_id);			
		$attachements   = "";
		$changes 		= $queryObj -> getRiskChange($_POST, $query);
        $updates        = array();
        $update_data    = array();
        $msg            = "";
        if(isset($changes['changes']) && !empty($changes['changes']))
        {
            $msg                   = $changes['change_message'];
            $updates['changes']    = base64_encode(serialize($changes['changes']));
            $updates['risk_id']    = $query_id;
            $updates['insertuser'] = $_SESSION['tid'];
        }
        if(isset($changes['update_data']) && !empty($changes['update_data']))
        {
            $update_data = $changes['update_data'];
        }    
		$res                 = $queryObj -> editQuery($query_id, $update_data, $updates);
		if($res > 1) 
		{
			$response["id"] 	  = $query_id;
			$error			 	  = false;
			$text 				  = "Query id ".$_REQUEST['id']." has been successfully edited <br />";
			$user      		      = new UserAccess("", "", "", "", "", "", "", "", "" );
			$emailTo    	      = $user -> getRiskManagerEmails();
			$responsibleUserEmail = $user -> getUserResponsiblityEmail($_REQUEST['user_responsible'] );
			$queryOwner           = $user -> getUserResponsiblityEmail($query['insertuser']);
			$email 			      = new Email();
			$email -> userFrom    = $_SESSION['tkn'];
			$email -> subject     = "Edit Query";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message   = ""; 
			$message  .= "".$email -> userFrom." edited a query #".$query_id."\r\n\n";
			if(!empty($msg))
			{
				$message .= $msg;
			}
			$message .= " \n";
			if(!empty($att_change_str))
			{
			    $message .= $att_change_str;
			}
				//blank row above Please log on line
			$message .= "Please log onto Ignite Assist in order to view the edits.\n";
			$message  = nl2br($message);			
			$email -> body     = $message;

			if( $emailTo != "" )
			{
				$email -> to = $emailTo.","; 		
			}
			if( isset($responsibleUserEmail['tkemail']) && !empty($responsibleUserEmail['tkemail']) )
			{
				$email -> to .= $responsibleUserEmail['tkemail'].","; 		
			}			
			$email -> to   = rtrim( $email-> to, ",");	
			$email -> from = "no-reply@ignite4u.com";
			if( $email -> sendEmail() ) {
				$text .= " &nbsp;&nbsp;&nbsp;&nbsp; Notification email send successfully";
			} else {
				$error = true;
				$text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
			}
			$response = array("text" => $text, "error" => $error, "updated" => true );
		} else if($res == 0) {
			$response  = array("text" => "There was no change made to the query", "error"=> false, "updated" => false);
		}else {
			$response  = array("text" => "Error saving the query edit", "error" => true, "updated" => false);
		}
		 Attachment::clear('qryadded', 'query_'.$_REQUEST['id']);
		 echo json_encode( $response );		
		break;
    case "saveQueryAttachment":
        $key       = str_replace("_attachment", "", $_REQUEST['element']);  
        $attObj    = new Attachment($key, $_REQUEST['element']); 
        $results   = $attObj -> upload($key);
        echo json_encode($results);
        exit();
        break;
    case "removeQueryFile":
        $key      = str_replace("_attachment", "", $_REQUEST['element']); 
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment($key, $_REQUEST['element']);
        $response = $attObj -> removeFile($file, $key);
        echo json_encode($response);
        exit();	    
        break;
    case "deleteQueryAttachment":
       $file      = $_POST['attachment'];
       $attObj    = new Attachment($_POST['type']."_".$_POST['id'], $_POST['type']."_".$_POST['id']);
       $response  = $attObj -> deleteFile($file, $_POST['filename']);
       echo json_encode($response);
       exit();
    break;	
	case "editQueryAttachment":
        $attObj    = new Attachment('query_'.$_GET['query_id'], 'edit_qry_attachment_'.$_GET['query_id']); 
        $results   = $attObj -> upload('query_'.$_GET['query_id']);
        echo json_encode($results);
        exit();	
	break;
	case "updateQueryAttachment":
        $attObj    = new Attachment('query_'.$_GET['query_id'], 'update_qry_attachment_'.$_GET['query_id']); 
        $results   = $attObj -> upload('query_'.$_GET['query_id']);
        echo json_encode($results);
        exit();		
	break;
	case "deleteQueryFile":
	   $file      = $_REQUEST['attachment'];
	   $query_id  = $_REQUEST['query_id'];
	   $attObj    = new Attachment('query_'.$query_id, 'query_qry_attachment_'.$query_id);
	   $response  = $attObj -> deleteFile($file, $_REQUEST['name']);
       echo json_encode($response);
       exit();	
	   /*
	   $file      = $_REQUEST['attachment'];
	   $query_id  = $_REQUEST['query_id'];
	   $attObj    = new Attachment('query_edit_'.$query_id, 'edit_qry_attachment_'.$query_id);
	   $response  = $attObj -> deleteFile($file, $_REQUEST['name']);
       echo json_encode($response);
       exit();
       */
	break;
	case "removeQueryAttachment":
        $query_id = $_POST['query_id'];
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('query_'.$query_id, 'edit_qry_attachment_'.$query_id);
        $response = $attObj -> removeFile($file, 'query_'.$query_id);
        echo json_encode($response);
        exit();
		break;
	case "RemoveQueryFile":
		$filename = $_POST['id'];
		$file	  =  $_POST['file'];
		$att = new Attachments(array(), "query");
		$att->deletedFile( $filename, $file ); 	
		break;	
	case "deleteRisk":
		$risk = new Risk( "", "", "", "", "", "", "", "", "", "", "", "","");
		$id =  $risk -> deleteRisk( $_REQUEST['id'] );
		if( $id == 1 ) {
			$response["id"] 	= $id;
			$error 				= false;
			$text 				= " Query id ".$_REQUEST['id']." has been successfully deleted <br />";
			$user      		    = new UserAccess("", "", "", "", "", "", "", "", "" );
			$users 			    = $user -> getRiskManagerEmails();
			//$responsibleUserEmail 		  = $user -> getUserResponsiblityEmail( $_REQUEST['userRespo'] );
			$email 			    = new Email();
			$email -> userFrom  = $_SESSION['tkn'];
			$email -> subject   = "Query (Ref: ".$id.") has been deleted on Ignite Assist";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message   = ""; 
			$message .= "<i>".$email -> userFrom." has deleted a query .</i> \n";
			$message .= " \n";
				//blank row above Please log on line
			$message .= "Please log onto Ignite Assist in order to view the query.</i> \n";
			$message  = nl2br($message);			
			$email -> body     = $message;
			$emailTo 		   = "";
			if(!empty($users)){
				foreach($users as $userEmail ) {
					$emailTo = $userEmail['tkemail'].",";
				}
			}
			$emailTo = rtrim( $emailTo, ",");
			if( $emailTo != "" )
			{
				$email->to = $emailTo.","; 		
			}			
			$email->to = rtrim( $email-> to, ",");				
			$email -> from     = "info@ignite4u.com";
			if( $email -> sendEmail() ) {
				$text .= " &nbsp;&nbsp;&nbsp;&nbsp; Email send successfully";
			} else {
				$error = true;
				$text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
			}
			$response = array("error" => $error, "text" => $text, "deleted" => true );
		} else if( $id == 0) {
			$response = array("error" => false, "text" => "There was no change made to the query", "deleted" => true );
		}else {
			$response = array("error" => true, "text" => "Error deleting the query edit", "deleted" => false );
		}
		echo json_encode( $response );	
		break;	
		case "saveAttachement":
			$att = new Attachments($_FILES, "query_added");
			$att -> upload();
		break; 	
		case "removeFile":
			$att = new Attachments("", "query");
			$att -> remove( $_POST );
		break;	
    case "download":		
        header("Content-type:".$_GET['content']);
        header("Content-disposition: attachment; filename=".$_GET['new']);
        readfile("../../files/".$_GET['company']."/".$_SESSION['modref']."/".$_GET['folder']."/".$_GET['file']);
        exit();        
		break;			
	case "getNaming":
		$nm = new Naming();
		echo json_encode( $nm -> getNamingConversion() );	
		break;
	case "getRisk":
		$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo json_encode($risk -> getQueries($_GET));
		//echo json_encode( $risk -> getAllRisk( $_REQUEST['start'], $_REQUEST['limit'] ) );
		break;
	case "getActions":
		$risk = new RiskAction( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo json_encode($risk -> getQueryActions($_GET));
		//echo json_encode( $risk -> getAllRisk( $_REQUEST['start'], $_REQUEST['limit'] ) );
		break;	
	case "getActiveRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getActiveRiskType() );	
		break;
	case "totalRisk":
		$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo $risk -> totalRisk( "" );
		break;			
	case "getDefaults":
		$default = new Defaults("","");
		echo json_encode( $default -> getDefaults() );
		break;
	case "getDirectorate":
		$dir = new Directorate( "", "", "" ,"");
		echo json_encode( $dir -> getDirectorate() );
		break;
	case "getSubDirectorate":
		$dir = new SubDirectorate( "", "", "" ,"");
		echo json_encode( $dir -> getSubDirectorate() );
		break;
	case "getRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getType() );	
		break;		
	case "getTypeCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getTypeCategory( $_REQUEST['id']) );
		break;		
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getCategory() );
		break;
	case "getImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getImpact() );
		break;
	case "getActiveImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getActiveImpact() );
		break;				
	case "getResidualRisk":
		$res = new ResidualRisk( "", "", "", "", "");
		echo json_encode( $res -> getResidualRiskRanges() );
		break;				
	case "getImpactRating":
		$imp = new Impact( "", "", "", "", "");
		$imp -> getImpactRating( $_REQUEST['id']  );
		break;
	case "getInherentRiskRange":
		$ihr = new InherentRisk("", "", "", "", "");
		echo json_encode( $ihr -> getInherentRiskRange() );
		break;
	case "getLikelihoodRating":
		$lk = new Likelihood( "", "", "", "", "","");
		$lk -> getLikelihoodRating( $_REQUEST['id']  );
		break;	
	case "getLikelihood":
		$like = new Likelihood( "", "", "", "", "","");
		echo json_encode( $like -> getLikelihood() );
		break;
	case "getControl":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		echo json_encode( $ihr -> getControlEffectiveness());
		break;
	case "getControlRating":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		$ihr -> getControlRating( $_REQUEST['id'] );
		break;
	case "getUserAccess":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUserAccess() );
		break;	
	case "getUserAccessInfo":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUser( $_SESSION['tid'] ) );
		break;
	case "getUsers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUsers() );
		break;	
	case "getRiskStatus":
		$getstat = new RiskStatus( "", "", "" );
		echo json_encode( $getstat -> getStatus() );
		break;	
	case "deleteAction":
			$act = new RiskAction("", "", "", "", "", "", "","", "");
			$res = $act -> deleteRiskAction( $_REQUEST['id'] );
			$response = array();
			if( $res == 1){
				$response = array("text" => "Action deleted successfully", "error" => false);
			} else if( $res == 0){
				$response = array("text" => "Action deleted successfully", "error" => false);
			} else {
				$response = array("text" => "Error deleting the action", "error" => false);
			}
		break;	
	case "activateAction":
		$risk = new RiskAction( "" , "", "", "", "", "", "", "","");
		$risk -> activateRiskAction( $_REQUEST['id'] );
		break;
	case "newRiskUpdate":
		$queryObj 		= new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$query_id       = $_POST['id'];
		$query  		= $queryObj -> getQueryDetail($query_id);			
		$attachements   = "";
		$changes 		= $queryObj -> getRiskChange($_POST, $query);
        $updates        = array();
        $update_data    = array();
        $message        = "";
        $msg            = "";
        if(isset($changes['changes']) && !empty($changes['changes']))
        {
            $msg                   = $changes['change_message'];
            $updates['changes']    = base64_encode(serialize($changes['changes']));
            $updates['risk_id']    = $query_id;
            $updates['insertuser'] = $_SESSION['tid'];
        }
        if(isset($changes['update_data']) && !empty($changes['update_data']))
        {
            $update_data = $changes['update_data'];
        }     
     
		$id                 = $queryObj -> updateQuery($query_id, $update_data, $updates);

		if($id > 0) 
		{
			$response["id"]    = $id;
			$error 			   = false;
			$text 			   = "The Query update for query item ".$query_id." has been successfully update <br />";
			$user      		   = new UserAccess("", "", "", "", "", "", "", "", "" );
			$emailTo		   = $user -> getRiskManagerEmails();
			//$responsibleUserEmail 		  = $user -> getUserResponsiblityEmail( $_REQUEST['user_responsible'] );			
			$email 			   = new Email();
			$email -> userFrom = $_SESSION['tkn'];
			$email -> subject  = " Update Query";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message           = ""; 
			$message          .= $email -> userFrom." has Updated query #".$query_id."\n\n";
			$message          .= " \n";
			if(!empty($msg))
			{
				$message .= "The following updates were made \r\n\n";
				$message .= $msg;
			}
		    //blank row above Please log on line
			$message      .= "Please log onto Ignite Assist in order to View or Update the query and to assign any required ";
			$message      .= " actions to responsible users within ".$_SESSION['cn'];			
			$message       = nl2br($message);			
			$email -> body = $message;
			$email -> to   = rtrim($emailTo,",");
			$email -> from = "info@ignite4u.com";
			if($email -> sendEmail()) 
			{
			   $text .= " &nbsp;&nbsp;&nbsp;&nbsp; A notification e-mail has been sent";
			} else {
			   $error = true;
			   $text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
			}
			$response = array("error" => $error, "text" => $text, "updated" => true);
		} elseif($id == 0) {
		   $response  = array("error" => "No changes updated to the query", "error" => false, "updated" => false);
		} else {
		   $response  = array("error" => "Error saving the query update", "error" => true, "updated" => false);
		}
		Attachment::clear("qryadded", "query_".$_REQUEST['id'], 'query');
		Attachment::clear("qrydeleted", "query_".$_REQUEST['id'], 'query');
		echo json_encode( $response );
		break;
	case "getRiskAssurance":
		$rskass = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		$riskAssurance = $rskass -> getRisk( $_REQUEST['id'] );
		$displaying = array("query_item" 		=> "id",
							"query_type"		=> "type" ,
						    "query_category"	=> "category" ,
						    "query_description" => "description" ,
						    "query_background" 	=> "background",
						    "query_owner"    	=> "risk_owner"
							);
		$names 		  = new Naming();
		$header 	  = $names -> getHeaderNames();

		$queryAssurance = array();
		foreach( $header as $key => $value)
		{
			if( array_key_exists($value['name'], $displaying)){
				$queryAssurance[$value['client_terminology']] = $riskAssurance[$displaying[$value['name']]];
			} 
		}
		array_multisort($queryAssurance, SORT_NUMERIC);
		echo json_encode( $queryAssurance );
		break;	
	case "getQueryAssuranceStatus":
	  $obj = new QueryAssuranceStatus();
	  $statuses = $obj -> getAll();
	  echo json_encode($statuses);
    break;
	case "getQueryAssurances":
		$assuranceObj = new RiskAssurance($_GET['query_id'] , "", "", "", "", "");
		echo json_encode( $assuranceObj -> getQueryAssurance($_GET));
		break;
case "updateQueryAssurance":
	$assuranceObj = new RiskAssurance("", "","", "", "", "", "");
	$assurance    = $assuranceObj -> getAssurance($_POST['id']);
    $statusObj    = new QueryAssuranceStatus();
    $statuses     = $statusObj -> getAll();
    $currentstatus= "";
	$_changes     = array();
	$update_data  = array();
	foreach($assurance as $field => $value)
	{
	   if(isset($_POST[$field]))
	   {
	      if($_POST[$field] != $value)
	      {
             if($field == "status")
             {  
               $from = $to = "";
               if($_POST['status'] == 2)
               {
                  $_changes['status_'] = "Query Assurance #".$_POST['id']." has beed deleted"; 
               } elseif($_POST['status'] == 0) {
                  $_changes['status_'] = "Query Assurance #".$_POST['id']." has beed deactivated"; 
               }
               $update_data[$field] = $_POST[$field];
             } elseif($field == "signoff"){
                $from                = $statuses[$value]['client_terminology'];
                $to                  = $statuses[$_POST[$field]]['client_terminology'];
                $_changes[$field]    = array('from' => $from, 'to' => $to);
                $update_data[$field] = $_POST[$field];      
                $currentstatus       = $to;       
             } else {
                $from = $to = "";
                $_changes[$field]    = array('from' => $value, 'to' => $_POST[$field]);
                $update_data[$field] = $_POST[$field];
             }
	      }
	   }
	}
    $change_str = Attachment::processAttachmentChange($assurance['attachment'], "qassurance_".$_POST['id'], 'qassurance');
    if(isset($_SESSION['uploads']))
    {
       if(isset($_SESSION['uploads']['actionchanges']))
       {
          $_changes['attachment'] = $_SESSION['uploads']['actionchanges'];
          //$change_msg               .= $att_change_str;
       }
       if(isset($_SESSION['uploads']['attachments']))
       {
          if($assurance['attachment'] != $_SESSION['uploads']['attachments'])
          {
             $update_data['attachment'] = $_SESSION['uploads']['attachments'];
             $attachments               = $_SESSION['uploads']['attachments'];
          }
       }
    }
	$updates  = array();
	if(!empty($_changes))
	{
	   $changes['ref_']          = "Query assurance ref #".$_POST['id'];  
	   $changes                  = array_merge($changes, $_changes);
	   $changes['user']          = $_SESSION['tkn'];
	   if(!empty($currentstatus))
	   {
	      $changes['currentstatus'] = $currentstatus;
	   } else {
	      if(isset($statuses[$assurance['signoff']]))
	      {
	        $changes['currentstatus']  = $statuses[$assurance['signoff']]['client_terminology'];
	      } else {
	        $changes['currentstatus']  = " - ";
	      }
	   }
	   $updates['changes']       = base64_encode(serialize($changes));
	   $updates['insertuser']    = $_SESSION['tid'];
	}
	$res  = $assuranceObj -> updateAssurance($_POST['id'], $update_data, $updates);
	
	$response     = array();
	if($res > 0)
	{
	   $response  = array('text' => 'Query assurance updated successfully', 'error' => false, 'updated' => true);
	} else if($res == 0){
	   $response  = array('text' => 'No changes to query assurance', 'error' => false, 'updated' => false);
	} else {
	   $response  = array('text' => 'Error updating query assurance', 'error' => true);
	} 
    Attachment::clear('qryadded');
    Attachment::clear('qrydeleted');	
	echo json_encode($response);
	break;
	case "updateRiskAssurance":
		$assuranceObj = new RiskAssurance("" , "", "", "", "", "");
		$assurance    = $assuranceObj -> getAssurance( $_REQUEST['id'] );
        $att_change_str = Attachment::processAttachmentChange("", "qassurance_".$_POST['query_id'], 'qassurance');
		$attachment   = "";
		$attArr 	  = array(); 
		if(!empty($assurance['attachment']))
		{
			$attArr = unserialize(base64_decode($assurance['attachment']));
		} 
		if( isset($_SESSION['uploads']['assuranceattachmentdeleted']))
		{
			foreach( $_SESSION['uploads']['assuranceattachmentdeleted'] as $index => $file)
			{
				if( isset($attArr[$index])){
					unset($attArr[$index]);					
				}							
			}				
			unset( $_SESSION['uploads']['assuranceattachmentdeleted'] );
		}
		if( isset($_SESSION['uploads']['assuranceattachment']))
		{
			$attArr		= array_merge($attArr, $_SESSION['uploads']['assuranceattachment'] );
			unset($_SESSION['uploads']['assuranceattachment']);
		}
		if( !empty($attArr))
		{
			$attachment = base64_encode(serialize($attArr));
		}
		$rskass = new RiskAssurance($_REQUEST['risk_id'] , $_REQUEST['date_tested'], $_REQUEST['response'], $_REQUEST['signoff'], "", $attachment);
		$rskass -> updateRiskAssurance( $_REQUEST['id'] ) ;	
		break;
	case "saveQueryAssurance":
        $att_change_str = Attachment::processAttachmentChange("", "qassurance_".$_POST['query_id'], 'qassurance');
        $attachments    = "";
        if(isset($_SESSION['uploads']))
        {
           if(isset($_SESSION['uploads']['attachments']))
           {
              if(!empty($_SESSION['uploads']['attachments']))
              {
                //$update_data['attachment'] = $_SESSION['uploads']['attachments'];
                 $attachments               = $_SESSION['uploads']['attachments'];
              }
           }
        }
		$assObj = new RiskAssurance($_POST['query_id'] , $_POST['date_tested'], $_POST['response'], $_POST['signoff'], "", $attachments);
		echo json_encode($assObj -> saveQueryAssurance()) ;
        Attachment::clear('qryadded');
        Attachment::clear('qrydeleted');	
		break;
	case "uploadAttachment":
        $key       = str_replace("_attachment", "", $_REQUEST['element']);  
        $attObj    = new Attachment($key, $_REQUEST['element']); 
        $results   = $attObj -> upload($key);
        echo json_encode($results);
        exit();
		break;
	case "removeAttachment":
        $key      = str_replace("_attachment", "", $_REQUEST['element']); 
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment($key, $_REQUEST['element']);
        $response = $attObj -> removeFile($file, $key);
        echo json_encode($response);
        exit();	    
	    break;
    case "deleteAttachment":
       $file      = $_POST['attachment'];
       $attObj    = new Attachment($_POST['type']."_".$_POST['id'], $_POST['type']."_".$_POST['query_id']);
       $response  = $attObj -> deleteFile($file, $_POST['filename']);
       echo json_encode($response);
       exit();
    break;
	case "saveNotification":
		$usernot = new UserNotification();
		$usernot -> saveNotifications();
		break;
	case "updateNotification":
		$usernot = new UserNotification();
		echo json_encode( $usernot -> updateNotification(  $_POST['id'] ) );
		break;		
	case "getNotifications":
		$usernot = new UserNotification();
		echo json_encode( $usernot -> getNotifications() );
		break;
	case "deleteNot":
		$usernot = new UserNotification();
		echo json_encode( $usernot -> deleteNotification( $_REQUEST['id'] ) );
		break;		
	case "getRiskUpdates":
		$rskupdat 	  	= new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		$riskUpdateLog 	= $rskupdat -> getRiskUpdates( $_REQUEST['id'] );
		$changeLog		= array();
		$changeLog 	    = getRiskChanges( $riskUpdateLog, "updated" );		

		echo json_encode( $changeLog  );
		break;
	case "getApprovalData":
		$actionObj       = new RiskAction( "" , "", "", "", "", "", "", "","");
		$approvedActions = $actionObj -> getApprovedActions($_GET);
		$actionAwaiting  = $actionObj -> getActionToApprove($_GET);  
		$response        = array("awaiting" => $actionAwaiting, "approved" => $approvedActions);
		echo json_encode( $response );
		break;
    case "approveAction":
        $action_id      = $_POST['id'];
	    $actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
	    $action	        = $actObj -> getActionDetail($action_id) ;
	    $statusObj 		= new ActionStatus("","", "");
	    $statuses 		= $statusObj -> getStatuses();
	    $change_arr 	= array();
	    $change_msg	    = "";	
	    $update_data    = array();
	    $attStr 		= "";	
	
	    if(isset($_POST['response']) && !empty($_POST['response']))
	    {
	      $change_arr['response'] = $_POST['response'];
	      $change_msg             = " Added response ".$_POST['response'];
	    }
        $change_arr['approval_status']  = "Action has been approved.";  
        $change_msg                    .= "Action has been approved.\r\n\n";
        if(($action['action_status'] & RiskAction::AWAITING_APPROVAL) == RiskAction::AWAITING_APPROVAL)
        {
           $update_data['action_status'] = ($action['action_status'] - RiskAction::AWAITING_APPROVAL) +  RiskAction::ACTION_APPROVED;
        }
        $updates = array();
        if(!empty($change_arr))
        {
            if(!isset($change_arr['currentstatus']))
            {
               $change_arr['currentstatus'] = $statuses[$action['status']]['name'];
            }
            $change_arr['user']   = $_SESSION['tkn'];
            $updates['changes']   = base64_encode(serialize($change_arr));
            $updates['action_id'] = $action_id;
        }
	    $id                      = $actObj -> updateAction($action_id, $update_data, $updates);
	    $response                = array(); 
	    if($id > 0)
	    {
		    $response["id"] 	 = $id;
		    $error 				 = false;
		    $text 				 = "Action #".$action_id." has been successfully approved <br />";
		    $user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
		    $emailTo 		     = $user -> getRiskManagerEmails();		
		    $email 			     = new Email();
		
		    $email -> userFrom  = $_SESSION['tkn'];
		    $email -> subject   = " Action Approved ";
		    //Object is something like Risk or Action etc.  Object_id is the reference of that object
		    $message            = ""; 
		    //$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
		    $message           .= " \n";	//blank row above Please log on line
		    $message           .= $_SESSION['tkn']." approved action #".$action_id." related to query #".$action['risk_id'];
		    if(!empty($change_msg))
		    {
			    $message .= $change_msg;
		    }
		    if(!empty($att_change_str))
		    {
		        $message .= $att_change_str;
		    }			
		
		    $message .= "Please log onto Ignite Assist in order to View further information on the Query and or associated Actions \n";
		    $message           = nl2br($message);			
		    $email -> body     = $message;
		    $email_to          = "";
		    if(!empty($mailTo))
		    {
		       $email_to = $emailTo.",";
		    }
		    $email_to          = $action['tkemail'];
		    $email -> to 	  .= $email_to; 
		    $email -> from     = "no-reply@ingite4u.com";
		    if($email -> sendEmail()) 
		    {
		       $text .= " &nbsp;&nbsp;&nbsp;&nbsp; Email send successfully";
		    } else {
		       $error = true;
		       $text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
		    }
		    $response = array("text" => $text, "error" => $error );
	    } else {
		    $response = array("text" => "Error saving the action update", "error" => true, "updated" => false);
	    }
	echo json_encode($response);
	break;	
    case "declineAction":
        $action_id      = $_POST['id'];
	    $actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
	    $action	        = $actObj -> getActionDetail($action_id) ;
	    $statusObj 		= new ActionStatus("","", "");
	    $statuses 		= $statusObj -> getStatuses();
	    $change_arr 	= array();
	    $change_msg	    = "";	
	    $update_data    = array();
	    $attStr 		= "";	
	    if(isset($_POST['response']) && !empty($_POST['response']))
	    {
	      $change_arr['response'] = $_POST['response'];
	      $change_msg             = " Added response ".$_POST['response'];
	    }
        $change_arr['approval_status']  = "Action has been declined.";  
        $change_msg                    .= "Action has been declined.\r\n\n";
        if(($action['action_status'] & RiskAction::AWAITING_APPROVAL) == RiskAction::AWAITING_APPROVAL)
        {
           $update_data['action_status'] = $action['action_status'] - RiskAction::AWAITING_APPROVAL; 
        }
        $update_data['progress'] = 99;
        $update_data['status']   = 2; 
        $updates                 = array();
        if(!empty($change_arr))
        {
            if(!isset($change_arr['currentstatus']))
            {
               $change_arr['currentstatus'] = $statuses[$action['status']]['name'];
            }
            $change_arr['user']   = $_SESSION['tkn'];
            $updates['changes']   = base64_encode(serialize($change_arr));
            $updates['action_id'] = $action_id;
        }
	    $id                      = $actObj -> updateAction($action_id, $update_data, $updates);
	    $response  = array(); 
	    if($id > 0)
	    {
		    $response["id"] 	 = $id;
		    $error 				 = false;
		    $text 				 = "Action #".$action_id." has been successfully declined <br />";
		    $user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
		    $emailTo 		     = $user -> getRiskManagerEmails();		
		    $email 			     = new Email();
		
		    $email -> userFrom  = $_SESSION['tkn'];
		    $email -> subject   = " Action Declined ";
		    //Object is something like Risk or Action etc.  Object_id is the reference of that object
		    $message            = ""; 
		    //$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
		    $message           .= " \n";	//blank row above Please log on line
		    $message           .= $_SESSION['tkn']." declined action #".$action_id." related to query #".$action['risk_id'];
		    if(!empty($change_msg))
		    {
			    $message .= $change_msg;
		    }
		    if(!empty($att_change_str))
		    {
		        $message .= $att_change_str;
		    }			
		
		    $message .= "Please log onto Ignite Assist in order to View further information on the Query and or associated Actions \n";
		    $message           = nl2br($message);			
		    $email -> body     = $message;
		    $email_to          = "";
		    if(!empty($mailTo))
		    {
		       $email_to = $emailTo.",";
		    }
		    $email_to          = $action['tkemail'];
		    $email -> to 	  .= $email_to; 
		    $email -> from     = "no-reply@ingite4u.com";
		    if($email -> sendEmail()) 
		    {
		       $text .= " &nbsp;&nbsp;&nbsp;&nbsp; Email send successfully";
		    } else {
		       $error = true;
		       $text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
		    }
		    $response = array("text" => $text, "error" => $error );
	    } else {
		    $response = array("text" => "Error saving the action update", "error" => true, "updated" => false);
	    }
	    echo json_encode($response);
	break; 		
	case "unlockAction":
        $action_id      = $_POST['id'];
	    $actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
	    $action	        = $actObj -> getActionDetail($action_id) ;
	    $statusObj 		= new ActionStatus("","", "");
	    $statuses 		= $statusObj -> getStatuses();
	    $change_arr 	= array();
	    $change_msg	    = "";	
	    $update_data    = array();
	    $attStr 		= "";		
	    if(isset($_POST['response']) && !empty($_POST['response']))
	    {
	      $change_arr['response'] = $_POST['response'];
	      $change_msg             = " Added response ".$_POST['description'];
	    }
        $change_arr['approval_status']  = "Action has been unlocked.";  
        $change_msg                    .= "Action has been unlocked.\r\n\n";
        if(($action['action_status'] & RiskAction::ACTION_APPROVED) == RiskAction::ACTION_APPROVED)
        {
           $update_data['action_status'] = $action['action_status'] - RiskAction::ACTION_APPROVED; 
        }
        $update_data['progress'] = 99;
        $update_data['status']   = 2; 
        $updates                 = array();
        if(!empty($change_arr))
        {
            if(!isset($change_arr['currentstatus']))
            {
               $change_arr['currentstatus'] = $statuses[$action['status']]['name'];
            }
            $change_arr['user']   = $_SESSION['tkn'];
            $updates['changes']   = base64_encode(serialize($change_arr));
            $updates['action_id'] = $action_id;
        }
	    $res                      = $actObj -> updateAction($action_id, $update_data, $updates);
	    $response  = array(); 
		if($res > 0) 
		{
			$response = array( "error" => false , "text" => "Action #".$action_id." unlocked .."); 			
		} else if($res == 0) {
			$response = array("error" => true , "text" => "No change made trying to unlock the action"); 
		} else {
			$response = array("error" => true, "text" => "Error unlocking the action" ); 			
		}
		echo json_encode($response);
	break;	
	case "getActionActiviyLog":
	    $actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
	    $logs	        = $actObj -> getActionUpdateLog($_GET['id']);
	    echo $logs;
	break;		
	case "getRiskEdits":
		$risk 		  = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$riskEditLog  = $risk -> getRiskEdits( $_REQUEST['id'] );
		$changeLog 	  = array();		
		$changeLog 	  = getRiskChanges( $riskEditLog, "edited" );
	echo json_encode( $changeLog );
	break;	
	case "getManagerUsers":
	$user = new UserAccess( "", "", "", "", "", "", "", "", "");
	echo json_encode( $user -> getManagerUsers() );
	break;
	case "addAssuranceAttachment":
		$attObj = new Attachments($_FILES, "assuranceattachment");
		$attObj -> upload();			
	break;
	case "removeAssuranceFile":
		$attObj = new Attachments("", "assuranceattachment");
		$attObj -> deletedFile($_POST['index'], $_POST['file'] );
	break;
	case "editAssuranceAttachment":
		$attObj = new Attachments($_FILES, "assuranceattachment");
		$attObj -> upload();			
	break;
	case "deleteAssuranceFile":
		$ext = substr($_POST['file'], strpos($_POST['file'], ".") +1 );
		$attObj = new Attachments("", "assuranceattachment");
		$filename = $_POST['index'].".".$ext;
		$attObj -> remove($filename, $_POST['file'], "assuranceattachmentdeleted" );  
	break;
case "newActionUpdate":
    $action_id      = $_POST['id'];
	$actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
	$action	        = $actObj -> getActionDetail($action_id) ;
	$statusObj 		= new ActionStatus("","", "");
	$statuses 		= $statusObj -> getStatuses();
	$naming 	    = new Naming();
	$attachment     = array();
	$change_arr 	= array();
	$change_msg	    = "";	
	$update_data    = array();
	$attStr 		= "";	
    $att_change_str = Attachment::processAttachmentChange($action['attachement'], "action_".$action['id'], 'action');
	if($_POST['status'] !== $action['status'])
	{
	   $from                        = $statuses[$action['status']]['name'];
	   $to                          = $statuses[$_POST['status']]['name'];
	   $update_data['status']       = $_POST['status'];
	   $change_msg                  = "Action Status changed to ".$to." from ".$from."\r\n";
	   $change_arr['action_status'] = array('from' => $from, 'to' => $to);
	   $change_arr['currentstatus'] = $to;
	}
	
	if($_POST['progress'] != $action['progress'])
	{
	   $from                          = $action['progress'];
	   $to                            = $_POST['progress'];
	   $update_data['progress']       = $_POST['progress'];
	   $change_msg                    = "Action Progress changed to ".$to."% from ".$from."%\r\n";
	   $change_arr['action_progress'] = array('from' => $from."%", 'to' => $to."%");
	}
	
	if($_POST['remindon'] != $action['remindon'])
	{
	   $from                           = $action['remindon'];
	   $to                             = $_POST['remindon'];
	   $update_data['remindon']        = $_POST['remindon'];
	   $change_msg                     = "Action Remind On changed to ".$to." from ".$from."\r\n";
	   $change_arr['action_remind_on'] = array('from' => $from, 'to' => $to);  
	}
	if(isset($_POST['action_on']))
    {   
	    if($_POST['action_on'] != $action['action_on'])
	    {
           $from                  = (strtotime($action['action_on']) == 0 ? "-" : date("d-M-Y", strtotime($action['action_on'])) );
           $to                    = (strtotime($_POST['action_on']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['action_on'])) );
           if(strtotime($from) !== strtotime($to))
           {
             $change_arr['action_on']  = array('from' => $from, 'to' => $to);
             $change_msg              .= "Action on changed from ".$action['action_on']." to ".$_POST['action_on']."\r\n\n";
             $update_data['action_on'] = date("Y-m-d", strtotime($_POST['action_on']));
           }
	    }	
	}
	if(isset($_POST['description']) && !empty($_POST['description']))
	{
	  $change_arr['response'] = $_POST['description'];
	  $change_msg             = " Added response ".$_POST['description'];
	}
	
    if(isset($_POST['approval']) && $_POST['approval'] == 'on')
    {
       $change_arr['approval'] = "Requested sending of approval email.";  
       $change_msg            .= "Requested sending of approval email.\r\n\n";
    }	
    
    if($_POST['progress'] == 100 && $_POST['status'] == 3)
    {
        $change_arr['approval_status']  = "Awaiting approval status added.";  
        $change_msg                    .= "Awaiting approval status added.\r\n\n";
        if(($action['action_status'] & RiskAction::AWAITING_APPROVAL) != RiskAction::AWAITING_APPROVAL)
        {
           $update_data['action_status']   = $action['action_status'] + RiskAction::AWAITING_APPROVAL; 
        }
    }    
    
    if(isset($_SESSION['uploads']))
    {
       if(isset($_SESSION['uploads']['actionchanges']))
       {
          $change_arr['attachment'] = $_SESSION['uploads']['actionchanges'];
          $change_msg              .= $att_change_str;
       }
       if(isset($_SESSION['uploads']['attachments']))
       {
          if($action['attachement'] != $_SESSION['uploads']['attachments'])
          {
             $update_data['attachement'] = $_SESSION['uploads']['attachments'];
          }
       }
    }	

    $updates = array();
    if(!empty($change_arr))
    {
        if(!isset($change_arr['currentstatus']))
        {
           $change_arr['currentstatus'] = $statuses[$action['status']]['name'];
        }
        $change_arr['user']   = $_SESSION['tkn'];
        $updates['changes']   = base64_encode(serialize($change_arr));
        $updates['action_id'] = $action_id;
    }
	$id                      = $actObj -> updateAction($action_id, $update_data, $updates);
	$response  = array(); 
	if($id > 0)
	{
		$response["id"] 	 = $id;
		$error 				 = false;
		$text 				 = " New action update for action #".$action_id." has been successfully saved <br />";
		$user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
		$emailTo 		     = $user -> getRiskManagerEmails();		
		$email 			     = new Email();
		
		$email -> userFrom  = $_SESSION['tkn'];
		$email -> subject   = " Update Action ";
		//Object is something like Risk or Action etc.  Object_id is the reference of that object
		$message            = ""; 
		//$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
		$message           .= " \n";	//blank row above Please log on line
		$message           .= $_SESSION['tkn']." Updated action #".$action_id." related to query #".$action['risk_id'];
		if(!empty($change_msg))
		{
			$message .= $change_msg;
		}
		if(!empty($att_change_str))
		{
		    $message .= $att_change_str;
		}			
		
		$message .= "Please log onto Ignite Assist in order to View further information on the Query and or associated Actions \n";
		$message           = nl2br($message);			
		$email -> body     = $message;
		$email_to          = "";
		if(!empty($mailTo))
		{
		   $email_to = $emailTo.",";
		}
		$email_to          = $action['tkemail'];
		$email -> to 	  .= $email_to; 
		$email -> from     = "no-reply@ingite4u.com";
		if($email -> sendEmail()) 
		{
		   $text .= " &nbsp;&nbsp;&nbsp;&nbsp; Email send successfully";
		} else {
		   $error = true;
		   $text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
		}
		$response = array("text" => $text, "error" => $error );
	} else {
		$response = array("text" => "Error saving the action update", "error" => true, "updated" => false);
	}
	Attachment::clear('qryadded', 'action_'.$action['id']);
	echo json_encode($response);
	break;	
    case "editRiskAction":
    $action_id      = $_POST['id'];
	$actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
	$action	        = $actObj -> getActionDetail($action_id) ;
	$statusObj 		= new ActionStatus("","", "");
	$statuses 		= $statusObj -> getStatuses();
    $uaccess 	    = new UserAccess( "", "", "", "", "", "", "", "", "");
    $users 	        = $uaccess -> getUsersList();
	$attachment     = array();
	$change_arr 	= array();
	$change_msg	    = "";	
	$update_data    = array();
	$attStr 		= "";	

    $att_change_str = Attachment::processAttachmentChange($action['attachement'], "action_".$action['id'], 'action');
	if($_POST['deadline'] !== $action['deadline'])
	{
       $from                  = (strtotime($action['deadline']) == 0 ? "-" : date("d-M-Y", strtotime($action['deadline'])) );
       $to                    = (strtotime($_POST['deadline']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['deadline'])) );
       if(strtotime($from) !== strtotime($to))
       {
         $change_arr['deadline']  = array('from' => $from, 'to' => $to);
         $change_msg              .= "Action Deadline changed from ".$action['deadline']." to ".$_POST['deadline']."\r\n\n";
         $update_data['deadline'] = $_POST['deadline'];
       }
	}
	if($_POST['timescale'] !== $action['timescale'])
	{
	   $from                        = $action['timescale'];
	   $to                          = $_POST['timescale'];
	   $update_data['timescale']    = $_POST['timescale'];
	   $change_msg                  = "Action Timescale changed to ".$to." from ".$from."\r\n";
	   $change_arr['timescale']     = array('from' => $from, 'to' => $to);
	}
	if($_POST['r_action'] !== $action['action'])
	{
	   $from                        = $action['action'];
	   $to                          = $_POST['r_action'];
	   $update_data['action']       = $_POST['r_action'];
	   $change_msg                  = "Action Name changed to ".$to." from ".$from."\r\n";
	   $change_arr['action_name']   = array('from' => $from, 'to' => $to);
	}		
	if($_POST['action_owner'] != $action['owner'])
	{
	   $from                          = $users[$action['owner']]['user'];
	   $to                            = $users[$_POST['action_owner']]['user'];
	   $update_data['action_owner']   = $_POST['action_owner'];
	   $change_msg                    = "Action Owner changed to ".$to."% from ".$from."%\r\n";
	   $change_arr['action_owner']    = array('from' => $from, 'to' => $to);
	}
	if($_POST['remindon'] != $action['remindon'])
	{
       $from                  = (strtotime($action['remindon']) == 0 ? "-" : date("d-M-Y", strtotime($action['remindon'])) );
       $to                    = (strtotime($_POST['remindon']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['remindon'])) );
       if(strtotime($from) !== strtotime($to))
       {
         $change_arr['action_remind_on']  = array('from' => $from, 'to' => $to);
         $change_msg                     .= "Remind On changed to ".$to." from ".$from."\r\n\n";
         $update_data['remindon']         = $_POST['remindon'];
       }
	}
	if($_POST['deliverable'] != $action['deliverable'])
	{
       $from                             = $action['deliverable'];
       $to                               = $_POST['deliverable'];
       $change_arr['action_deliverable'] = array('from' => $from, 'to' => $to);
       $change_msg                      .= "Action Deliverable changed to ".$to." from ".$from."\r\n\n";
       $update_data['deliverable']       = $_POST['deliverable'];
	}	
	
    if(isset($_SESSION['uploads']))
    {
       if(isset($_SESSION['uploads']['actionchanges']))
       {
          $change_arr['attachment'] = $_SESSION['uploads']['actionchanges'];
          $change_msg              .= $att_change_str;
       }
       if(isset($_SESSION['uploads']['attachments']))
       {
          if($action['attachement'] != $_SESSION['uploads']['attachments'])
          {
             $update_data['attachement'] = $_SESSION['uploads']['attachments'];
          }
       }
    }
    $updates = array();
    if(!empty($change_arr))
    {
        if(!isset($change_arr['currentstatus']))
        {
           $change_arr['currentstatus'] = $statuses[$action['status']]['name'];
        }
        $change_arr['user']   = $_SESSION['tkn'];
        $updates['changes']   = base64_encode(serialize($change_arr));
        $updates['action_id'] = $action_id;
    }
	$id                      = $actObj -> editRiskAction($action_id, $update_data, $updates);
	$response  = array(); 
	if($id > 0)
	{
		$response["id"] 	 = $id;
		$error 				 = false;
		$text 				 = " Action #".$action_id." has been successfully saved <br />";
		$user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
		$emailTo 		     = $user -> getRiskManagerEmails();		
		$email 			     = new Email();
		
		$email -> userFrom  = $_SESSION['tkn'];
		$email -> subject   = " Edit Action ";
		//Object is something like Risk or Action etc.  Object_id is the reference of that object
		$message            = ""; 
		//$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
		$message           .= " \n";	//blank row above Please log on line
		$message           .= $_SESSION['tkn']." edited action #".$action_id." related to query #".$action['risk_id'];
		if(!empty($change_msg))
		{
			$message .= $change_msg;
		}
		if(!empty($att_change_str))
		{
		    $message .= $att_change_str;
		}			
		$message .= "Please log onto Ignite Assist in order to View further information on the Query and or associated Actions \n";
		$message           = nl2br($message);			
		$email -> body     = $message;
		$email_to          = "";
		if(!empty($mailTo))
		{
		   $email_to = $emailTo.",";
		}
		$email_to          = $action['tkemail'];
		$email -> to 	  .= $email_to; 
		$email -> from     = "no-reply@ingite4u.com";
		if($email -> sendEmail()) 
		{
		   $text .= " &nbsp;&nbsp;&nbsp;&nbsp; Email send successfully";
		} else {
		   $error = true;
		   $text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
		}
		$response = array("text" => $text, "error" => $error );
	} else {
		$response = array("text" => "Error saving the action edit", "error" => true, "updated" => false);
	}
    Attachment::clear("qryadded", 'action_'.$_REQUEST['id']);
    Attachment::clear("qrydeleted", 'action_'.$_REQUEST['id']);
	echo json_encode( $response );
	break;		
    case "saveActionAttachment":
        $key       = str_replace("_attachment", "", $_REQUEST['element']);  
        $attObj    = new Attachment($key, $_REQUEST['element']); 
        $results   = $attObj -> upload($key);
        echo json_encode($results);
        exit();
	    break;
    case "removeActionFile":
        $key      = str_replace("_attachment", "", $_REQUEST['element']); 
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment($key, $_REQUEST['element']);
        $response = $attObj -> removeFile($file, $key);
        echo json_encode($response);
        exit();	    
        break;
    case "deleteActionAttachment":
       $file      = $_POST['attachment'];
       $attObj    = new Attachment($_POST['type']."_".$_POST['id'], $_POST['type']."_".$_POST['id']);
       $response  = $attObj -> deleteFile($file, $_POST['filename']);
       echo json_encode($response);
       exit();
    break;	
    case "updateRiskAction":
        $action_id      = $_POST['id'];
	    $actObj	        = new RiskAction("", "", "","" , "", "", "", "", "");
	    $action	        = $actObj -> getActionDetail($action_id) ;
	    $statusObj 		= new ActionStatus("","", "");
	    $statuses 		= $statusObj -> getStatuses();
        $uaccess 	    = new UserAccess( "", "", "", "", "", "", "", "", "");
        $users 	        = $uaccess -> getUsersList();
	    $attachment     = array();
	    $change_arr 	= array();
	    $change_msg	    = "";	
	    $update_data    = array();
	    $attStr 		= "";	

        $att_change_str = Attachment::processAttachmentChange($action['attachement'], "action_".$action['id'], 'action');
	    if($_POST['deadline'] !== $action['deadline'])
	    {
           $from                  = (strtotime($action['deadline']) == 0 ? "-" : date("d-M-Y", strtotime($action['deadline'])) );
           $to                    = (strtotime($_POST['deadline']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['deadline'])) );
           if(strtotime($from) !== strtotime($to))
           {
             $change_arr['deadline']  = array('from' => $from, 'to' => $to);
             $change_msg              .= "Action Deadline changed from ".$action['deadline']." to ".$_POST['deadline']."\r\n\n";
             $update_data['deadline'] = $_POST['deadline'];
           }
	    }
	    if($_POST['timescale'] !== $action['timescale'])
	    {
	       $from                        = $action['timescale'];
	       $to                          = $_POST['timescale'];
	       $update_data['timescale']    = $_POST['timescale'];
	       $change_msg                  = "Action Timescale changed to ".$to." from ".$from."\r\n";
	       $change_arr['timescale']     = array('from' => $from, 'to' => $to);
	    }
	    if($_POST['r_action'] !== $action['action'])
	    {
	       $from                        = $action['action'];
	       $to                          = $_POST['r_action'];
	       if(trim($frfom) != trim($to))
	       {
	           $update_data['action']       = $_POST['r_action'];
	           $change_msg                  = "Action Name changed to ".$to." from ".$from."\r\n";
	           $change_arr['action_name']   = array('from' => $from, 'to' => $to);	        
	       }
	    }		
	    if($_POST['action_owner'] != $action['owner'])
	    {
	       $from                          = $users[$action['owner']]['user'];
	       $to                            = $users[$_POST['action_owner']]['user'];
	       $update_data['action_owner']   = $_POST['action_owner'];
	       $change_msg                    = "Action Owner changed to ".$to."% from ".$from."%\r\n";
	       $change_arr['action_owner']    = array('from' => $from, 'to' => $to);
	    }
	    if($_POST['remindon'] != $action['remindon'])
	    {
           $from                  = (strtotime($action['remindon']) == 0 ? "-" : date("d-M-Y", strtotime($action['remindon'])) );
           $to                    = (strtotime($_POST['remindon']) == 0 ? "-" : date("d-M-Y", strtotime($_POST['remindon'])) );
           if(strtotime($from) !== strtotime($to))
           {
             $change_arr['action_remind_on']  = array('from' => $from, 'to' => $to);
             $change_msg                     .= "Remind On changed to ".$to." from ".$from."\r\n\n";
             $update_data['remindon']         = $_POST['remindon'];
           }
	    }
	    if($_POST['deliverable'] != $action['deliverable'])
	    {
           $from                             = $action['deliverable'];
           $to                               = $_POST['deliverable'];
           $change_arr['action_deliverable'] = array('from' => $from, 'to' => $to);
           $change_msg                      .= "Action Deliverable changed to ".$to." from ".$from."\r\n\n";
           $update_data['deliverable']       = $_POST['deliverable'];
	    }	
	
        if(isset($_SESSION['uploads']))
        {
           if(isset($_SESSION['uploads']['actionchanges']))
           {
              $change_arr['attachment'] = $_SESSION['uploads']['actionchanges'];
              $change_msg              .= $att_change_str;
           }
           if(isset($_SESSION['uploads']['attachments']))
           {
              if($action['attachement'] != $_SESSION['uploads']['attachments'])
              {
                 $update_data['attachement'] = $_SESSION['uploads']['attachments'];
              }
           }
        }
        $updates = array();
        if(!empty($change_arr))
        {
            if(!isset($change_arr['currentstatus']))
            {
               $change_arr['currentstatus'] = $statuses[$action['status']]['name'];
            }
            $change_arr['user']   = $_SESSION['tkn'];
            $updates['changes']   = base64_encode(serialize($change_arr));
            $updates['action_id'] = $action_id;
        }
	    $res                      = $actObj -> editRiskAction($action_id, $update_data, $updates);
	    $response  = array(); 
	    if($res > 0)
	    {
		    $response["id"] 	 = $res;
		    $error 				 = false;
		    $text 				 = " Action #".$action_id." has been successfully saved <br />";
		    $user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
		    $emailTo 		     = $user -> getRiskManagerEmails();		
		    $email 			     = new Email();
		
		    $email -> userFrom  = $_SESSION['tkn'];
		    $email -> subject   = " Edit Action ";
		    //Object is something like Risk or Action etc.  Object_id is the reference of that object
		    $message            = ""; 
		    //$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
		    $message           .= " \n";	//blank row above Please log on line
		    $message           .= $_SESSION['tkn']." edited action #".$action_id." related to query #".$action['risk_id'];
		    if(!empty($change_msg))
		    {
			    $message .= $change_msg;
		    }
		    if(!empty($att_change_str))
		    {
		        $message .= $att_change_str;
		    }			
		    $message .= "Please log onto Ignite Assist in order to View further information on the Query and or associated Actions \n";
		    $message           = nl2br($message);			
		    $email -> body     = $message;
		    $email_to          = "";
		    if(!empty($mailTo))
		    {
		       $email_to = $emailTo.",";
		    }
		    $email_to          = $action['tkemail'];
		    $email -> to 	  .= $email_to; 
		    $email -> from     = "no-reply@ingite4u.com";
		    if($email -> sendEmail()) 
		    {
		       $text .= " &nbsp;&nbsp;&nbsp;&nbsp; Email send successfully";
		    } else {
		       $error = true;
		       $text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
		    }
		    $response = array("text" => $text, "error" => $error, "updated" => true );
	    } elseif($res == 0){
	       $response = array("text" => "No changes made to the action", "error" => false, "updated" => false);
	    } else {
		   $response = array("text" => "Error editing the action", "error" => true, "updated" => false);
	    }
        Attachment::clear("qryadded", 'action_'.$_REQUEST['id']);
        Attachment::clear("qrydeleted", 'action_'.$_REQUEST['id']);
	echo json_encode( $response );
	break;	
	case "newRiskAction":
        $data           = array();
        $att_change_str = Attachment::processAttachmentChange("", "action_".$_REQUEST['id'], 'action');
	    if(!empty($_SESSION['uploads']['attachments']))
	    {
	      $data['attachments'] = $_SESSION['uploads']['attachments'];		
	    }	
		$risk = new RiskAction( 
				$_REQUEST['id'] ,
				$_REQUEST['r_action'] ,
				$_REQUEST['action_owner'] ,
			    $_REQUEST['deliverable'] ,
		 		$_REQUEST['timescale'] ,
				$_REQUEST['deadline'] ,
				$_REQUEST['remindon'] ,
				$_REQUEST['progress'],
				$_REQUEST['status']
		);
		$id 	   = $risk -> saveRiskAction($data);
		$response  = array(); 
		if( $id > 0 ) {
			$response["id"] 			  = $id;
			$error						  = false;
			$text 						  = "New Action #".$id." has been successfully saved <br />";
			$user      		     		  = new UserAccess("", "", "", "", "", "", "", "", "" );
			$emailTo		     		  = $user -> getRiskManagerEmails();
			$responsibleUserEmail 		  = $user -> getUserResponsiblityEmail( $_REQUEST['action_owner'] );
			$email 			     		  = new Email();
			$email -> userFrom   		  = $_SESSION['tkn'];
			$risk 						  = new Risk("" ,"" , "", "", "", "", "", "", "", "", "", "", "");
			$riskInfo 					  = $risk -> getRisk($_REQUEST['id']);
			$email -> subject    		  = "New Action";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message  = "";			
			$message .= $email -> userFrom." created an Action relating to query #".$_REQUEST['id'];
			$message .= " : ".$riskInfo['description']." identified in the Query Register. The details of the action are as follows :";
			$message .= " \r\n\n";	//blank row above Please log on line
			$message .= "Action Number : ".$id."\r\n"; 
			$message .= "Action : ".$_REQUEST['r_action']."\r\n";
			$message .= "Deliverable : ".$_REQUEST['deliverable']."\r\n";
			$message .= "Action Owner : ".$responsibleUserEmail['tkname']." ".$responsibleUserEmail['tksurname']."\r\n";
			$message .= "Time Scale : ".$_REQUEST['timescale']."\r\n";
			$message .= "Deadline : ".$_REQUEST['deadline']."\r\n";
		    if(!empty($att_change_str))
		    {
		       $message .= $att_change_str;
		    }														
		    $message .= " \r\n\n";	//blank row above Please log on line										
			$message .= "<i>Please log onto Ignite Assist in order to View or Update the Action.</i> \n";
			$message  = nl2br($message);			
			$email -> body     = $message;
			if( $emailTo != "" )
			{
				$email->to = $emailTo.","; 		
			}
			
			if( isset($responsibleUserEmail['tkemail']) && !empty($responsibleUserEmail['tkemail']) )
			{
				$email->to .= $responsibleUserEmail['tkemail'].","; 		
			}			
			$email->to = rtrim( $email-> to, ",");			
			//$email -> to = ($emailTo == "" ? "" : $emailTo)."".($responsibleUserEmail['tkemail'] == "" ? "" : ",".$responsibleUserEmail['tkemail']);
			$email -> from     = "no-reply@ignite4u.com";
			if( $email -> sendEmail() ) {
				$error 	= false;
				$text  .= " &nbsp;&nbsp;&nbsp;&nbsp; Notification has been sent";
			} else {
				$error  = true;
				$text  .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
			}
			$response = array("error" => $error, "text" => $text, "saved" => true, "id" => $id);
		} else {
			$response = array("error" => true, "text" => "Error saving the action <br />".$id, "saved" => false);
		}
	    Attachment::clear("qryadded", 'action_new_'.$_REQUEST['id']);
	    Attachment::clear("qrydeleted", 'action_new_'.$_REQUEST['id']);
		if(isset($_SESSION['uploads']['action'])){
			unset($_SESSION['uploads']['action']);
		}		
		echo json_encode( $response );
		break;
	case "getActionQuery":
		$riskObj = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$query   = $riskObj -> getRiskTable($_GET['id'] );
		echo $query;
	break;
	case "saveReminder":
	   $reminderObj = new Reminder();
	   $res         = $reminderObj -> saveReminder();
	   if($res > 0)
	   {
	     $response = array('text' => 'Reminder saved successfully', 'error' => false);
	   } else {
	     $response = array('text' => 'Error saving reminder', 'error' => true);
	   }
	   echo json_encode($response);
	   exit();
	break;
	case "updateReminder":
	   $reminderObj = new Reminder();
	   $res         = $reminderObj -> updateReminder($_POST['id']);
	   if($res > 0)
	   {
	     $response = array('text' => 'Reminder updated successfully', 'error' => false, 'updated' => true);
	   } elseif($res == 0) {
	     $response = array('text' => 'No change made to the reminder', 'error' => false, 'updated' => false);
	   } else {
	     $response = array('text' => 'Error updating reminder', 'error' => true);
	   }
	   echo json_encode($response);
	   exit();
	break;
	case "saveSummaryNotification":
		$usernot  = new SummaryNotification();
		$res      = $usernot -> saveNotifications();
		$response = array();
		if($res > 0)
		{
		   $response = array('text' => 'Summary Notification setting saved', 'error' => false);
		} else {
		   $response = array('text' => 'Error saving the summary notification settings', 'error' => true); 
		}
		echo json_encode($response);
		exit();		
		break;
	case "updateSummaryNotification":
		$usernot  = new SummaryNotification();
		$res      = $usernot -> updateNotification($_POST['id']);
		$response = array();
		if($res > 0)
		{
		   $response = array('text' => 'Summary notifications successfully updated', 'error' => false, 'updated' => true);
		} elseif($res == 0) {
		   $response = array('text' => 'No change made to the summary notifications settings', 'error' => false, 'updated' => false);
		} else {
		  $response = array('text' => 'Error updating summary notifications', 'error' => true); 
		}
		echo json_encode($response);
		break;		
	case "getSummaryNotifications":
		$usernot  = new SummaryNotification();
		echo json_encode($usernot -> getNotifications());
		break;
	case "deleteSummaryNotification":
		$usernot = new SummaryNotification();
		$res      = $usernot -> deleteNotification($_POST['id']);
		$response = array();
		if($res > 0)
		{
		   $response = array('text' => 'Summary notifications successfully deleted', 'error' => false, 'updated' => true);
		} elseif($res == 0) {
		   $response = array('text' => 'No change made to the summary notifications settings', 'error' => false, 'updated' => false);
		} else {
		  $response = array('text' => 'Error deleting summary notifications', 'error' => true); 
		}
		echo json_encode($response);
		break;	
	case "getFinancialYear":
		$stat  = new FinancialYear("", "", "");
		$years = $stat -> getFinYear();
		echo json_encode($years);
	break;	
	case "getDirectorate":
		$dir = new Directorate( "", "", "" ,"");
		echo json_encode( $dir -> getDirectorate() );
	break;	
	case "getQueries":
		$risk 		  = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$queries  = $risk -> getActionQueries($_GET);
		echo json_encode($queries);
		exit();
	break;	
	default:
		//echo "It hasn't found the actions associated";
	break;

}

	function riskChange( $requestArr )
	{
		$risk 		    = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$riskInfo  		= $risk -> getRisk( $requestArr['id'] );

		$headers 		= new Naming();
		$currentStatus  = "";
		//setting the values , to ids so can compare the chnage on id, 
		$riskInfo['type'] 			= intval( $riskInfo['typeid'] );
		$riskInfo['status']			= intval( $riskInfo['statusId'] );	
		$riskInfo['category']		= intval( $riskInfo['categoryid'] );
		$riskInfo['user_responsible'] = $riskInfo['sub_id'];
		$requestArr['risk_type']    = intval( $requestArr['risk_type']);
		$requestArr['financial_exposure']    = intval( $requestArr['financial_exposure']);	
		$requestArr['risk_level']   = intval( $requestArr['risk_level']);				
		$changeMessage 			= "";
		$changeArr 	   			= array();
		$additionals = array("finding", "recommendation", "client_response", "auditor_conclusion", 
		                     "internal_control_deficiency");
		foreach( $requestArr as $key => $value) 
		{
			if( isset($riskInfo[$key]) || array_key_exists($key, $riskInfo))
			{
				if( trim($riskInfo[$key]) !== trim($value)){
					if( $key == "type")
					{
						$ty 		= new QueryType("", "", "" , "");
						$fromtype	= $ty -> getARiskType( $riskInfo[$key] );
						$from    	= $fromtype['name']; 
						$totype		= $ty -> getARiskType( $value );
						$to			= $totype['name'];		
						
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= $headers->setHeader($key)." changed to ".$to." from ".$from."\r\n\n";
					} else if( $key == "risk_type")	{
						$ty 		= new RiskType("", "", "" , "");
						$fromtype	= $ty -> getARiskType( $riskInfo[$key] );
						$from    	= $fromtype['name']; 
						$totype		= $ty -> getARiskType( $value );
						$to			= $totype['name'];
												
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= $headers->setHeader($key)." changed to ".$to." from ".$from."\r\n\n";
					} else if( $key == "risk_level") {
						$ty 		= new RiskLevel("", "", "" , "");
						$fromtype	= $ty -> getARiskLevel( $riskInfo[$key] );
						$from    	= $fromtype['name']; 
						$totype		= $ty -> getARiskLevel( $value );
						$to			= $totype['name'];		
						
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= $headers->setHeader($key)." changed to ".$to." from ".$from."\r\n\n";
					} else if( $key == "financial_exposure"){
						$fn 		= new FinancialExposure("", "", "", "" , "");
						$fromFin	= $fn -> getAFinancialExposure( $riskInfo[$key] );
						$from    	= $fromFin['name']; 
						$toFin		= $fn -> getAFinancialExposure( $value );
						$to			= $toFin['name'];		
						
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= $headers->setHeader($key)." changed to ".$to." from ".$from."\r\n\n";
					} else if( $key == "category"){
						$ty 		= new RiskCategory("", "", "" , "");
						$fromcat	= $ty -> getACategory( $riskInfo[$key] );
						$from 	    = $fromcat['name']; 
						$tocat		= $ty -> getACategory( $value );
						$to			= $tocat['name'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= $headers->setHeader($key)." changed to ".$to." from ".$from."\r\n\n";	
					} else if( $key == "likelihood"){
						$ty 		= new Likelihood("", "", "" , "", "", "");
						$fromlike	= $ty -> getALikelihood( $riskInfo[$key] );
						$from 	    = $fromlike['assessment']; 
						$tolike		= $ty -> getALikelihood( $value );
						$to			= $tolike['assessment'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= $headers->setHeader($key)." changed to ".$to." from ".$from."\r\n\n";
					} else if( $key == "effectiveness"){
						$ty 		= new ControlEffectiveness("", "", "" , "", "");
						$fromeffect	= $ty -> getAControlEffectiveness( $riskInfo[$key] );
						$from 	    = $fromeffect['effectiveness']; 
						$toeffect	= $ty -> getAControlEffectiveness( $value );
						$to			= $toeffect['effectiveness'];		
											
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= $headers->setHeader($key)." changed to ".$to." from ".$from."\r\n\n";	
					 } else if( $key == "status"){
						$ty 		= new RiskStatus("" , "", "");
						$fromstatus	= $ty -> getAStatus( $riskInfo[$key] );
						$from 	    = $fromstatus['name']; 
						$tostatus	= $ty -> getAStatus( $value );
						$to			= $tostatus['name'];		
						$currentStatus  = $to;					
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= $headers->setHeader($key)." changed to ".$to." from ".$from."\r\n\n";	
					} else if($key == "user_responsible"){
						$userObj = new Directorate("", "","", "");
						$toDept  = $userObj->getADirectorate( $value );
						$fromDept  = $userObj->getADirectorate( $riskInfo['sub_id'] );
						
						$to 	= $toDept['dirtxt'];
						$from 	= $fromDept['dirtxt'];
						$changeArr[$key] = array("from" => $from, "to" => $to);
						$changeMessage   .= $headers->setHeader("query_owner")." changed to ".$to." from ".$from."\r\n\n";	
					} else {
						if(in_array($key, $additionals))
						{
							$to 	         = str_replace("__", ", ", $value);
							$from 	         = str_replace("__", ", ", $riskInfo[$key]); 
							$_from           = ($from == "" ? "-" : $from);
							$changeArr[$key] = array("from" => $_from, "to" => $to);
							$changeMessage  .= $headers -> setHeader($key)." changed to ".$to." from ".($_from == "" ? "-" : $from)."\r\n\n";	
						} else {
							$_from           = ($riskInfo[$key] == "" ? "-" : $riskInfo[$key]);
							$changeArr[$key] = array("from" => $_from, "to" => $value);
							$changeMessage  .= $headers->setHeader($key)." changed to ".$value." from ".$_from."\r\n\n";
						}						
					}
				}
			} else if( $key == "response" ) {
						$changeArr[$key] = $value;
						$changeMessage  .= "Added Response ".$value."\r\n\n";
			}
		}
		$attArr	 = array();
		if( !empty($riskInfo['attachment']) )
		{
		   $attArr  = @unserialize(base64_decode($riskInfo['attachment']));
		   if(!is_array($attArr) || empty($attArr))
		   {
		     $attArr  = array();
		   }
		}
		//if there are any attachemetd deleted
		//check if there its in the saved attachments in the database 
		//if its in there , then unset it 
		if(isset($_SESSION['uploads']['editdeleted']))
		{
		   foreach( $_SESSION['uploads']['editdeleted']  as $key => $val)
		   {
			 if(array_key_exists($key, $attArr))
		      {
				$changeArr['attachment'][$key]  = "Deleted attachment ".$val;
				$changeMessage   				 .= "Deleted attachment ".$val;				
				unset($attArr[$key]);
			 } 				
		  }
		   unset( $_SESSION['uploads']['editdeleted'] );
		}
		//if there are any new atttachements added
		//if there is , then add them to the query array
		if(isset($_SESSION['uploads']['editedupload']))
		{
		   foreach( $_SESSION['uploads']['editedupload'] as $key => $val)
		   {
			if(!array_key_exists($key, $attArr))
			{
			   $changeArr['attachment'][$key]   = "Attachment added ".$val;
			   $changeMessage   		  		.= "Attachment added ".$val;
			   $attArr[$key] = $val;										
			}
		   } 
		   unset($_SESSION['uploads']['editedupload']);
		}	

		if(isset($_SESSION['uploads']['updatedupload']))
		{
		   foreach( $_SESSION['uploads']['updatedupload'] as $key => $val)
		   {
			 if(!array_key_exists($key, $attArr))
			 {
				$changeArr['attachment'][$key]   = "Attachment added ".$val;
				$changeMessage   		  		.= "Attachment added ".$val;
				$attArr[$key] = $val;										
			 }
		   } 
		   unset($_SESSION['uploads']['updatedupload']);
		}	
		if( $currentStatus == "")
		{
			$statObj  = new RiskStatus("" , "", "");
			$status	= $statObj->getAStatus( $riskInfo['status'] );
			$changeArr['currentstatus'] = $status['name'];
		} else {
			$changeArr['currentstatus'] = $currentStatus; 			
		}	 	
		$changeArr['user']  = $_SESSION['tkn'];
		return array(
					"changeArr" 		=> $changeArr,
				 	"changeMessage" 	=> $changeMessage,
					"responsiblePerson" => (isset($riskInfo['responsible_person']) ? $riskInfo['responsible_person'] : ""),
					"currentAttach"		=> $attArr
				);
	}
	
	function getRiskChanges( $riskLog, $context )
	{
		$changes 		= array();
		$changeLog		= array();
		$changeMessage  = "";
		foreach( $riskLog as $key => $log)
		{	
			$id				   		  = $log['query_item'];
			$changes	        	  = unserialize($log['changes']);
			
			$changeLog[$id]['date']   = $log['insertdate'];
			$changeLog[$id]['status'] = (isset($log['query_status']) ? $log['query_status'] : "");
			if( isset( $changes) && !empty($changes) ){
				$changeMessage  = "The query has been ".$context." by ".(isset($changes['user']) ? $changes['user'] : "")."<br /><br />";
				$changeMessage .= "The following changes were made : <br />";			
				foreach( $changes as $key => $change)
				{
					if( $key == "user")
					{ continue; } else if($key == "attachement"){
						foreach( $change as $key => $fileAction)
						{
							$changeMessage .= $fileAction."<br />";						
						}
					} else if($key == "response"){
					   $changeMessage .= "".$change." <br /> ";
					} else{
						 $changeMessage .= setHeader($key)." has changed to ".$change['to']." from ".$change['from']." <br /> ";
					}
				}
			}
		  $changeLog[$id]['changeMessage'] = $changeMessage;
		}		
		usort( $changeLog , "compare");
		return $changeLog;
	}
	
	function setHeader( $key )
	{
		$names 		  = new Naming();
		$header 	  = $names -> getHeaderNames();	
		$headerName   = ""; 
		foreach($header as $index => $name )
		{
			if( $name['name'] == $key )
			{
				$headerName = (isset($name['client_terminology']) ? $name['client_terminology'] : $name['ignite_terminology']);
			} else{
				$headerName = ucwords( str_replace("_", " ", $key ) );
			}
		}
		return $headerName;
	} 
	
	function compare($a ,$b)
	{
		return ( $a['date'] > $b['date'] ? -1 : 1 );
	}
	

	
?>
