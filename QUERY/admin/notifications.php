<?php
$scripts = array( 'menu.js', 'setup_logs.js'  );
$styles = array( 'colorpicker.css' );
$page_title = "View Risk";
require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$(".setup_defaults").click(function() {
			document.location.href = this.id+".php";
			return false;		  
		});
		$("table#link_to_page").find("th").css({"text-align":"left"})
	});
</script>
<table border="1" width="50%" cellpadding="0" cellspacing="0" id="link_to_page">
	<tr>
<!--    <tr>
    	<th>Automatic Reminders:</th>
        <td>
          <input type="button" name="set_reminders" id="set_reminders" value="Configure" class="setup_defaults"  />
        </td>
    </tr> -->
    <tr>
    	<th>Summary Notification:</th>
        <td>
          <input type="button" name="set_summary_notification" id="set_summary_notification" value="Configure" class="setup_defaults"  />
        </td>
    </tr>
</table>
