<?php
$scripts = array( 'jquery.ui.action.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Action Edit";
require_once("../inc/header.php");
$risk 		 = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$rsk 		 = $risk -> getRisk( $_GET['id']);

$naming 	= new Naming();
$rowNames   = $naming -> rowLabel();
function nameFields( $fieldId , $defaultName ){
	global $naming, $rowNames;
	if( isset( $rowNames[$fieldId] )) {
		echo $rowNames[$fieldId];
	}	else {
		echo $defaultName;
	}
}
?>
<script language="javascript">
	$(function(){
		$("#actions").action({risk_id:<?php echo $_GET['id']; ?>, viewAssurance:true});
	});
</script>
<table class="noborder" width="100%">
  <tr>
    <td class="noborder"  width="50%">
			<table align="left"border="1" id="edit_action_table"> 
				  <tr>
				  	<td class="noborder" colspan="2"><h4>Query Details</h4></td>
				  </tr>
				 <tr>
			            <th style="text-align:left;"><?php nameFields('query_item','Query Item'); ?>:</th>
			            <td>#<?php echo $rsk['id'] ?></td>
			      </tr>
			      <tr>
			            <th style="text-align:left;"><?php nameFields('query_type','Query Type'); ?>:</th>
			            <td>
			                <?php echo $rsk['type']; ?>
			            </td>
			     </tr>
	             <tr>
	                    <th style="text-align:left;"><?php nameFields('query_reference','Query Reference'); ?>:</th>
	                    <td><?php echo $rsk['query_reference'] ?></td>
	           	 </tr>             
		          <tr>
		            <th style="text-align:left;"><?php nameFields('query_category','Query Category'); ?>:</th>
		            <td>
		                <?php echo $rsk['category']; ?>    
		            </td>
		          </tr>
		          <tr>
		            <th style="text-align:left;"><?php nameFields('query_description','Query Description'); ?>:</th>
		            <td><?php echo $rsk['description'] ?></td>
		          </tr>
		          <tr>
		            <th style="text-align:left;"><?php nameFields('query_background','Background Of Query'); ?>:</th>
		            <td><?php echo $rsk['background'] ?></td>
		          </tr>                
		          <tr>
		            <th style="text-align:left;"><?php nameFields('query_owner','Responsibility of Query'); ?>:</th>
		            <td>
		                <?php echo $rsk['risk_owner']; ?>
		            </td>
		          </tr>
		          <tr>
		            <th style="text-align:left;"><?php nameFields('query_deadline_date','Query Deadline Date'); ?>:</th>
		            <td>
		                <?php echo $rsk['query_deadline_date']; ?>
		            </td>
		          </tr>  
		          <tr>
		            <th style="text-align:left;"><?php nameFields('query_date','Query Date'); ?>:</th>
		            <td>
		                <?php echo $rsk['query_date']; ?>
		            </td>
			    </tr> 
	          <tr>
		            <td align="left" class="noborder">
		                <?php $me->displayGoBack("",""); ?>
		            </td>
		            <td align="right" class="noborder"> 
		            </td>
	          </tr>			             
			</table>
    </td>
    <td class="noborder"  width="50%">
    	<div id="actions"></div>
    </td>
  </tr>
</table>
