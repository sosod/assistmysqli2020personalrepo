<?php 
$scripts = array( 'assurance_action.js', 'jquery.ui.actionassurance.js', 'menu.js','ajaxfileupload.js' );
$styles = array( );
$page_title = "Action Assurance";
require_once("../inc/header.php");
$risk 	  = new RiskAction( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$action   = $risk -> getActionDetail($_GET['id']);

$colObj   = new ActionColumns();
$columns  = $colObj -> getHeaderList();
?>
<script language="javascript">
	$(function(){
		$("#actionassurance").actionassurance({action_id:$("#action_id").val()});
	});
</script>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table class="noborder" width="100%" id="assurance_action_table" > 
	<tr>
		<td width="40%" valign="top" class="noborder">
			<table width="100%">
            		<tr>
            			<td colspan="2"><h4>Action Details</h4></td>
            		</tr>
            		<tr>
            			<th><?php echo (isset($columns['action_ref']) ? $columns['action_ref'] : "Ref"); ?>:</th>
            			<td><?php echo $_GET['id']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['query_action']; ?>:</th>
            			<td><?php echo $action['action']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['deliverable']; ?>:</th>
            			<td><?php echo $action['deliverable']; ?></td>
            		</tr> 
            		<tr>
            			<th><?php echo $columns['action_owner']; ?>:</th>
            			<td><?php echo $action['action_owner']; ?></td>
            		</tr>             		           		
            		<tr>
            			<th><?php echo $columns['action_status']; ?>:</th>
            			<td><?php echo $action['actionstatus']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['progress']; ?>:</th>
            			<td><?php echo ASSIST_HELPER::format_percent($action['progress']); ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['deadline']; ?>:</th>
            			<td><?php echo $action['deadline']; ?></td>
            		</tr>            		          
            		<tr>
            			<th><?php echo $columns['timescale']; ?>:</th>
            			<td><?php echo $action['timescale']; ?></td>
            		</tr> 
                    <tr>
                    <th>Attachment:</th>
                        <td>
                            <?php
                             Attachment::displayAttachmentOnly($action['attachement']);
                            ?>
                        </td>
                    </tr>
				<tr>
					<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
					<td class="noborder"><?php $admire_helper->displayAuditLogLink( "action_assurance_logs" , false); ?></td>
				</tr>   
			</table>	
		</td>
		<td width="60%" valign="top" class="noborder">
			<div width="100%" id="actionassurance"></div>
		</td>
	</tr>
</table>
<input type="hidden" id="action_id" name="action_id" value="<?php echo $_REQUEST['id']; ?>" />
