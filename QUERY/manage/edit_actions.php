<?php
$scripts = array( 'jquery.ui.action.js','menu.js','actions.js'  );
$styles = array( 'colorpicker.css' );
$page_title = "Action Edit";;
require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$("table#edit_action_table").find("th").css({"text-algn":"left"})
		$("#actions").action({editAction:true, page:"edit_actions", section:"manage"});
	});
</script>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<div id="actions"></div>
<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['tid']; ?>"  />
