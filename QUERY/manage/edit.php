<?php
$scripts = array(  'jquery.ui.query.js', 'menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Edit Risk";
require_once("../inc/header.php");


echo "<div style='width:250px;position:absolute;top:5px;right:5px'>";
ASSIST_HELPER::displayResult(array("info","Please note that Query details cannot be changed in Manage.  Manage only allows for additional Actions to be added.  To edit Query details please use the Admin section."));
echo "</div>";

?>
<script language="javascript">
	$(function(){
		$("#risk").query({editQuery:true, editActions:true, view:"viewMyn", page:"edit", section:"manage"});	
	});
</script>
<div id="risk"></div>
