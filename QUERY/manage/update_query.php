<?php $admire_helper->JSdisplayResultObj(""); ?>
<table id="risk_action_table" class="noborder" width="100%">
   <tr>
     <td width="50%" valign="top" class="noborder">
       <table class="noborder" width="100%">
    	    <tr>
    		 <td colspan="2"><h4>Query Details</h4></td>
    	    </tr>
          <tr>
            <th>Query Ref#:</th>
            <td><?php echo $_REQUEST['query_id']; ?></td>
          </tr>    	    
          <tr>
            <th><?php echo $headers['query_reference']; ?>:</th>
            <td><?php echo $query['reference']; ?></td>
          </tr>          
          <tr>
            <th><?php echo $headers['type']; ?>:</th>
            <td><?php echo $query['query_type']; ?></td>
          </tr>          
          <tr>
            <th><?php echo $headers['category']; ?>:</th>
            <td><?php echo $query['query_category']; ?></td>
          </tr>          
          <tr>
            <th><?php echo $headers['description']; ?>:</th>
            <td><?php echo $query['description']; ?></td>
          </tr>          
          <tr>
            <th><?php echo $headers['background']; ?>:</th>
            <td><?php echo $query['background']; ?></td>
          </tr>   
            <th><?php echo $headers['query_date']; ?>:</th>
            <td><?php echo $query['query_date']; ?></td>
          </tr>          
          <tr>
            <th><?php echo $headers['query_deadline_date']; ?>:</th>
            <td><?php echo $query['deadline_date']; ?></td>
          </tr>          
          <tr>
            <th><?php echo $headers['financial_exposure']; ?>:</th>
            <td><?php echo $query['query_financial_exposure']; ?></td>
          </tr>          
          <tr>
            <th><?php echo $headers['monetary_implication']; ?>:</th>
            <td><?php echo $query['monetary_implication']; ?></td>
          </tr>          
          <tr>
            <th><?php echo $headers['risk_level']; ?>:</th>
            <td><?php echo $query['query_risk_level']; ?></td>
          </tr>                    
          <tr>
            <th><?php echo $headers['risk_type']; ?>:</th>
            <td><?php echo $query['query_risk_type']; ?></td>
          </tr>     
          <tr>
            <th><?php echo $headers['risk_detail']; ?>:</th>
            <td><?php echo $query['risk_detail']; ?></td>
          </tr>                                                                               
	    <tr id="goback">
		  <td class="noborder" align="left" width="150">
			<?php $me->displayGoBack("",""); ?>
		  </td>
		  <td class="noborder"></td>
	    </tr>				
    	    <tr>
    	      <td colspan="2" class="noborder"><div id="actions"></div></td>
    	    </tr>
    	  </table>
     </td>
     <td valign="top" class="noborder" width="50%">
       <form id="add_action" name="add_action">
       <table align="left" id="addaction_table" width="100%">
      	<tr>
      	  <td colspan="2"><h4>Update Query</h4></td>
      	</tr>
          <tr>
            <th>Query Ref#:</th>
            <td><?php echo $_REQUEST['query_id']; ?></td>
          </tr>
          <tr>
            <th>Response:</th>
            <td><textarea name="response" id="response" cols="35" rows="7"></textarea></td>
          </tr>
          <tr>
            <th>Query Status:</th>
            <td>
                <select id="status" name="status">    
                   <option value=""><?php echo $select_box_text ?></option>
                   <?php 
                     foreach($statuses as $index => $status)
                     {
                   ?>
                     <option value="<?php echo $status['id']; ?>" <?php echo ($status['id'] == $query['status'] ? "selected='selected'" : ""); ?> <?php echo ($status['id'] == 1 ? "disabled='disabled'" : ""); ?>>
                        <?php echo $status['name']; ?>
                     </option>
                   <?php  
                     }
                   ?>                   
                </select>
            </td>
          </tr>
          <tr>
            <th>Remind On:</th>
            <td><input type="text" name="remind_on" id="remind_on" class="datepicker" readonly="readonly" value="<?php echo $query['remind_on']; ?>" /></td>
          </tr>
          <tr>
            <th>Attachment:</th>
            <td>
                <input id="update_qry_attachment_<?php echo $rsk['id']; ?>" name="update_qry_attachment_<?php echo $rsk['id']; ?>" type="file" onChange="editQueryUploader(this)" />                            
                <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
                <small>You can attach more than 1 file.</small>
                </p>
                <?php
                   Attachment::displayAttachmentList($rsk['attachment'], 'query');
                ?>
            </td>
          <tr>
          <tr>
            <th></th>
            <td>
              <input type="hidden" id="id" name="id" value="<?php echo $_REQUEST['query_id']; ?>" />
              <input type="submit" id="update_query" value="Update" name="update_query" class="isubmit" />
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="reset" id="cancel" value="Cancel" name="cancel" class="idelete" /> 
              <input type="hidden" name="risk_id" value="<?php echo $rsk['id']; ?>" id="risk_id" class="logid" /> 
              <!-- <button type="reset" value="Reset" class="idelete">Cancel</button>-->
            </td>
          </tr>
        </table>		
        </form>				       
      </td>
  </tr>
</table>
	
