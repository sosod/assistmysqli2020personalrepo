<?php
$scripts = array( 'jquery.ui.action.js', 'add_action.js','menu.js', 'ajaxfileupload.js');
$styles = array( 'colorpicker.css' );
$page_title = "Action Edit";
require_once("../inc/header.php");

$risk 	  = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$query 	  = $risk -> getQueryDetail($_REQUEST['id']);
$colObj   = new QueryColumns();
$rowNames = $colObj -> getHeaderList();
?>
<script>
$(function(){
		$("table#addaction_table").find("th").css({"text-align":"left"})
		$("#actions").action({risk_id:$("#risk_id").val(), editAction:true, location: "../actions/", section:"new"});	
});
</script>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table id="risk_action_table" class="noborder" width="100%">
	<tr>
    	<td width="50%" valign="top" class="noborder">
    		<table class="noborder" width="100%">
    			<tr>
    				<td colspan="2"><h4>Query Details</h4></td>
    			</tr>
       		    <tr>
                    <th><?php echo $rowNames['query_item']; ?>:</th>
                    <td>#<?php echo $query['query_item']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_reference']; ?>:</th>
                    <td><?php echo $query['query_reference']; ?></td>
                  </tr>  
                 <tr>
                    <th><?php echo $rowNames['query_source']; ?>:</th>
                    <td><?php echo $query['query_source']; ?></td>
                  </tr>                       
                  <tr>
                    <th><?php echo $rowNames['query_description']; ?>:</th>
                    <td><?php echo $query['description']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_background']; ?>:</th>
                    <td><?php echo $query['background']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_type']; ?>:</th>
                    <td> <?php echo $query['query_type']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_category']; ?>:</th>
                    <td><?php echo $query['query_category']; ?></td>
                  </tr>
	              <tr>
                    <th><?php echo $rowNames['financial_exposure']; ?>:</th>
                    <td><?php echo $query['financial_exposure']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['monetary_implication']; ?>:</th>
                    <td><?php echo $query['monetary_implication']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['risk_level']; ?>:</th>
                    <td><?php echo $query['risk_level']; ?></td>
                  </tr>      
                  <tr class="more_details">
                    <th><?php echo $rowNames['risk_type']; ?>:</th>
                    <td><?php echo $query['risk_type']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['risk_detail']; ?>:</th>
                    <td><?php echo $query['risk_detail']; ?></td>
                  </tr>      
                  <tr class="more_details">
                    <th><?php echo $rowNames['finding']; ?>:</th>
                    <td><?php echo $query['finding']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['internal_control_deficiency']; ?>:</th>
                    <td><?php echo $query['internal_control_deficiency']; ?></td>
                  </tr>        
                  <tr class="more_details">
                    <th><?php echo $rowNames['recommendation']; ?>:</th>
                    <td><?php echo $query['recommendation']; ?></td>
                  </tr>
                  <tr class="more_details">
                    <th><?php echo $rowNames['client_response']; ?>:</th>
                    <td><?php echo $query['client_response']; ?></td>
                  </tr>    
                  <tr class="more_details">
                    <th><?php echo $rowNames['auditor_conclusion']; ?>:</th>
                    <td><?php echo $query['auditor_conclusion']; ?></td>
                  </tr> 
                 <tr>
                   <th><?php echo $rowNames['financial_year']; ?>:</th>
                   <td><?php echo $query['financial_year']; ?></td>
                 </tr>
                 <tr>
                    <th><?php echo $rowNames['query_owner']; ?>:</th>
                    <td><?php echo $query['query_owner']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['query_date']; ?>:</th>
                    <td><?php echo $query['query_date']; ?></td>
                  </tr>      
                  <tr>
                  	<th><?php echo $rowNames['query_deadline_date']; ?>:</th>
                  	<td><?php echo $query['query_deadline_date']; ?></td>
                  </tr>
                  <tr>
                    <th><?php echo $rowNames['attachements']; ?>:</th>
                    <td><?php Attachment::displayAttachmentOnly($query['attachment'], 'query');  ?></td>
                  </tr> 
				<tr id="go_back">
					<td class="noborder" align="left" width="150">
						<?php $me->displayGoBack("",""); ?>
					</td>
					<td class="noborder"></td>
				</tr>				
    		</table>
        </td>
     <td valign="top" class="noborder" width="50%">
      <table align="left" id="addaction_table" width="100%">
      	  <tr>
      	  	<td colspan="2"><h4>Add New Action</h4></td>
      	  </tr>
          <tr>
            <th>Query Ref#:</th>
            <td><?php echo $_REQUEST['id']; ?></td>
          </tr>
          <tr>
            <th>Action:</th>
            <td><textarea name="action" id="action" cols="35" rows="7"></textarea></td>
          </tr>
          <tr>
            <th>Deliverable:</th>
            <td><textarea name="deliverable" id="deliverable" cols="35" rows="7"></textarea></td>
          </tr>
          <tr>
            <th>Action Owner:</th>
            <td>
                <select id="action_owner" name="action_owner">
                
                </select>
            </td>
          </tr>
          <tr>
            <th>Time Scale:</th>
            <td><input type="text" name="timescale" id="timescale" /></td>
          </tr>
          <tr>
            <th>Deadline:</th>
            <td><input type="text" name="deadline" id="deadline" class="datepicker" readonly="readonly"/></td>
          </tr>
          <tr>
            <th>Remind On:</th>
            <td><input type="text" name="remindon" id="remindon" class="datepicker" readonly="readonly" /></td>
          </tr>
          <tr>
            	<th>Attachment:</th>
                <td>
                    <input id="action_attachment_<?php echo $_REQUEST['id']; ?>" name="action_attachment_<?php echo $_REQUEST['id']; ?>" type="file" class=upload_action />
                    <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
                      <small>You can attach more than 1 file.</small>
                    </p>
                    <p id="file_upload">No files attached</p>
                </td>
          </tr>
          <tr>
            <th></th>
            <td>
            <input type="hidden" id="risk_id" name="risk_id" value="<?php echo $_REQUEST['id']; ?>" />
            <input type="submit" id="save_action" value="Save Action" name="save_action" class="isubmit" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="submit" id="cancel_action" value="Cancel Action" name="cancel_action" class="idelete" />
            </td>
          </tr>
        </table>						       
      </td>
  </tr>
  <tr>
    <td colspan="2" class="noborder"><div id="actions"></div></td>
  </tr>  
</table>
	
