<?php
$scripts = array( 'edit_profile.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "My profile";
require_once("../inc/header.php");
$pro = new UserNotification();
$profile = $pro -> getAProfile( $_GET['id'] );
?>
<div>
<table width="30%">
	<tr>
    	<th>Recieve:</th>
    	<td>
        <select name="recieve" id="recieve">
            <option value="">-select-</option>
            <option value="1" <?php if($profile['recieve_email'] == 1) {?> selected <?php } ?>>yes</option>
            <option value="0" <?php if($profile['recieve_email'] == 0) {?> selected <?php } ?>>no</option>                
        </select>        
        </td>        
    </tr>
	<tr>
    	<th>When:</th>
    	<td>
        <select name="recieve_when" id="recieve_when">
            <option value="">-select-</option>
            <option value="weekly" <?php if($profile['recieve_when'] == "weekly") {?> selected <?php } ?>  >Weekly</option>
            <option value="daily" <?php if($profile['recieve_when'] == "daily") {?> selected <?php } ?>>Daily</option>       
        </select>
        <select id="recieve_day" name="recieve_day" style="display:none">
            <option value=""><?php echo $select_box_text ?></option>
            <option value="mon">Monday</option>                
            <option value="tues">Tuesday</option>                
            <option value="wed">Wednesday</option>                
            <option value="thurs">Thursday</option>                
            <option value="fri">Friday</option>                
            <option value="sat">Saturday</option>                
            <option value="sun">Sunday</option>                
        </select>      
        </td>        
    </tr> 
	<tr>
    <th>When:</th>
    	<td>
<select name="recieve_what" id="recieve_what">
    <option value=""><?php echo $select_box_text ?></option>
    <option value="due_this_week"  <?php if($profile['recieve_what'] == "due_this_week") {?> selected <?php } ?> >Actions due this week</option>
    <option value="due_on_or_before_week" <?php if($profile['recieve_what'] == "due_on_or_before_week") {?> selected <?php } ?>>Actions due on or before this week</option>
    <option value="due_today" <?php if($profile['recieve_what'] == "due_today") {?> selected <?php } ?>>Actions due today</option>
    <option value="due_on_or_before_today" <?php if($profile['recieve_what'] == "due_on_or_before_today") {?> selected <?php } ?>>Actions due on or before today</option>
    <option value="all_incomplete_actions" <?php if($profile['recieve_what'] == "all_incomplete_actions") {?> selected <?php } ?>>All Incomplete actions</option>
</select>    
        </td>        
    </tr>        
     <tr>
     	<td></td>
        <td>
        <input type="submit" name="update" id="update" value="Update Profile" />
        <input type="submit" name="reset" id="reset" value="Reset" />
        <input type="hidden" name="notid" id="notid" value="<?php echo $_GET['id']; ?>" />        
        </td>
     </tr>    
</table>
</div>