<?php
$scripts    = array('menu.js', 'jquery.ui.query.js', 'jquery.ui.action.js');
$styles     = array();
$page_title = "View Risk";
require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$("#risk").query({viewQuery:"viewrisk", view:"viewAll", section:"manage", showActions:true});	
	});
</script>
<div id="risk"></div>
