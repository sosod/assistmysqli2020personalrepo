<?php
$scripts = array( 'jquery.ui.action.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Action update";
require_once("../inc/header.php");
$risk 	  = new RiskAction( "" , "", "", "", "", "", "", "", "", "", "", "", "");
$action   = $risk -> getActionDetail($_GET['id']);

//echo (17665 & 1024)."<br /><br />";
$action_logs = $risk -> getActionActivityLog($_GET['id']);

$colObj   = new ActionColumns();
$columns  = $colObj -> getHeaderList();

?>
<table class="noborder" width="100%">
  <tr>
    <td class="noborder" valign="top"  width="50%">
			<table align="left"border="1" id="edit_action_table"  width="100%"> 
            		<tr>
            			<td colspan="2"><h4>Action Details</h4></td>
            		</tr>
            		<tr>
            			<th><?php echo (isset($columns['action_ref']) ? $columns['action_ref'] : "Ref"); ?>:</th>
            			<td><?php echo $_GET['id']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['query_action']; ?>:</th>
            			<td><?php echo $action['action']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['deliverable']; ?>:</th>
            			<td><?php echo $action['deliverable']; ?></td>
            		</tr> 
            		<tr>
            			<th><?php echo $columns['action_owner']; ?>:</th>
            			<td><?php echo $action['action_owner']; ?></td>
            		</tr>             		           		
            		<tr>
            			<th><?php echo $columns['action_status']; ?>:</th>
            			<td><?php echo $action['actionstatus']; ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['progress']; ?>:</th>
            			<td><?php echo ASSIST_HELPER::format_percent($action['progress']); ?></td>
            		</tr>
            		<tr>
            			<th><?php echo $columns['deadline']; ?>:</th>
            			<td><?php echo $action['deadline']; ?></td>
            		</tr>            		          
            		<tr>
            			<th><?php echo $columns['timescale']; ?>:</th>
            			<td><?php echo $action['timescale']; ?></td>
            		</tr> 
                    <tr>
                    <th>Attachment:</th>
                        <td>
                            <?php
                             Attachment::displayAttachmentOnly($action['attachement']);
                            ?>
                        </td>
                    </tr>
				  <tr id="go_back">
					<td class="noborder" align="left" width="150">
						<?php $me->displayGoBack("",""); ?>
					</td>
					<td class="noborder">

					</td>
				  </tr>		             
			</table>
    </td>
    <td class="noborder" valign="top" width="50%">
	    <table width="100%">
	      <tr>
	        <th class="th2" colspan='4'>Activity Log</th>
	      </tr>
	      <tr>
	        <th>Date Logged</th>
	        <th>User</th>
	        <th>Change Log</th>
	        <th>Status</th>
	      </tr>
	      <?php
	        if(!empty($action_logs))
	        {
	           foreach($action_logs as $q_index => $log)
	           {
            ?>	            
	          <tr>
	            <td align="center"><?php echo $log['date_loged']; ?></td>
	            <td><?php echo $log['user']; ?></td>
	            <td><?php echo $log['changeMessage']; ?></td>
	            <td><?php echo $log['status']; ?></td>
	          </tr>	        
	        <?php   
	           }
	        } else {
	        ?>
	          <tr>
	            <td align="center" colspan="4">There are no logs yet</td>
	          </tr>	        	        
	        <?php
	        }
	      ?>
	    </table>        
    </td>
  </tr>
</table>
<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['tid']; ?>"  />
