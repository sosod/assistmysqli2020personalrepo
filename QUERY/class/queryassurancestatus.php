<?php
class QueryAssuranceStatus extends DBConnect
{
    
    function __construct()
    {
        parent::__construct();
    }    
    
    function getAll($options = array())
    {
        $results = $this -> get("SELECT * FROM #_query_assurance_status WHERE status & 2 <> 2");
        return $results;    
    }
    
    function getAssuranceStatus($option_sql = "")
    {
       $result = $this -> getRow("SELECT * FROM #_query_assurance_status WHERE 1 $option_sql");
       return $result;
    }
    
    function updateAssurance($id, $data)
    {
        $querystatus = $this -> getAssuranceStatus(" AND id = '".$id."' ");
        $_changes    = array();
        if(!empty($querystatus))
        {
            foreach($querystatus as $field => $val)
            {
               if(isset($data[$field]))
               {
                  if($data[$field] !== $val)
                  {
                    $_changes[$field] = array('from' => $val, 'to' => $data[$field]);
                  }
               }
            }            
        }
        if(!empty($_changes))
        {
           $changes['ref']        = "Query Assurance Status Ref #".$id;
           $changes['user']       = $_SESSION['tkn'];
           $changes               = array_merge($changes, $_changes);
           $updates['changes']    = base64_encode(serialize($changes));
           $updates['ref_id']     = $id;
           $updates['insertuser'] = $_SESSION['tid'];
           $res = $this -> insert("query_assurance_status_logs", $updates);
        }   
    
        $res = $this -> update("query_assurance_status", $data, "id =".$id);
        return $res;//$this -> affected_rows();
    }
    
    function save($data)
    {
       $data['insertuser'] = $_SESSION['tid'];
       return $this -> insert("query_assurance_status", $data);
       //return $this -> insertedId();
    }
    

}



