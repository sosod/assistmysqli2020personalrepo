<?php
/**
	* @package 	: Sub Directorate
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class SubDirectorate extends DBConnect
{
	protected $shortcode;
	
	protected $division;
	
	protected $user_responsible;
	
	protected $department;
	
	function __construct( $shortcode, $division, $user_responsible, $department )
	{
		$this -> shortcode 		  = trim($shortcode);
		$this -> division 	      = trim($division);
		$this -> user_responsible = trim($user_responsible);
		$this -> department  	  = trim($department);
				
		parent::__construct();
	}
	function getSubDirectorate()
	{
		$response = $this -> get( "SELECT SDR.id,SDR.active, SDR.shortcode,DR.department,TK.tkname, TK.tksurname,
								   TK.tkid, SDR.assign_actions
								   FROM ".$_SESSION['dbref']."_subdirectorate_structure  SDR
								   INNER JOIN ".$_SESSION['dbref']."_directorate_structure DR ON SDR.department = DR.id 
								   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON SDR.user_responsible = TK.tkid " );
		return $response;
	}
	
	function getASubDirectorate( $id ){
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_subdirectorate_structure WHERE id = $id" );
		return $response;
	}
	
	function saveSubDirectorate()
	{

		$insert_data = array( 
						"shortcode" 		=> $this -> shortcode,
						"division" 			=> $this -> division,
						"user_responsible"  => $this -> user_responsible,
						"department"		=> $this -> department,
						"insertuser"    	=> $_SESSION['tid'],									
						);
		$response = $this -> insert( "subdirectorate_structure" , $insert_data );
		echo $response;//$this -> insertedId();
	}
	
	function updateSubDirectorate( $id )
	{

		$insert_data = array( 
						"shortcode" 		=> $this -> shortcode,
						"division" 			=> $this -> division,
						"user_responsible"  => $this -> user_responsible,
						"department"		=> $this -> department,	
						"insertuser"    	=> $_SESSION['tid'],								
						);
		$response = $this -> update( "subdirectorate_structure" , $insert_data, "id=$id" );
		return $response;	
	}	
	
	function updateSubDirectorateStatus( $id, $status )
	{
		$updatedata = array(
						'active' 		=> $status,
						"insertuser"    => $_SESSION['tid'],						
		);
		$response = $this -> update( 'subdirectorate_structure', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateSubDirectorate( $id )
	{
		$update_data = array(
						'active' 		=> "1",
						"insertuser"    => $_SESSION['tid'],						
		);
		$response = $this -> update( 'subdirectorate_structure', $update_data, "id=$id" );
		echo $response;
	}
	//get the subdirectorate that the user is under and use it to get all the risk that fall under those sub directores 
	function getUserRiskDirectorate()
	{
		echo "SELECT ref FROM ".$_SESSION['dbref']."_dir_admins DA 
					 WHERE tkid = '".$_SESSION['tid']."' ";
		$response = $this ->get("SELECT ref FROM ".$_SESSION['dbref']."_dir_admins DA 
					 WHERE tkid = '".$_SESSION['tid']."'
					 ");
		return $response; 
	}
	
	
	function getDirectorateResponsible()
	{	
		$result = $this -> get( " SELECT s.subid , CONCAT_WS('-', d.dirtxt, s.subtxt) AS value
							      FROM ".$_SESSION['dbref']."_dir d
							   	  INNER JOIN ".$_SESSION['dbref']."_dirsub s ON d.dirid = s.subdirid
							       AND s.active = 1 
								   AND (s.subid 
									IN( SELECT ref FROM ".$_SESSION['dbref']."_dir_admins 
										WHERE active = 1 AND type = 'SUB'	
									  ) 
									   OR s.subid 
										IN( SELECT ref FROM ".$_SESSION['dbref']."_dir_admins WHERE active = 1 AND type = 'DIR' )
								   ) 
								 WHERE d.active = 1 ORDER BY d.dirsort, s.subsort
							   " );
		return $result;
	}	
	
}
?>