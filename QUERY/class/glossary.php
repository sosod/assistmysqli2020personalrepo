<?php
/**
	* @package  : Glossary
	* @author 	: admire<azinamo@gmail.com>
	 
**/
class Glossary extends DBConnect
{
	
	protected $id;
	protected $category;
	protected $terminology;
	protected $explanation;
	
	function __construct()
	{
		parent::__construct();
		$this -> category 		= (isset($_POST['category']) ? $_POST['category'] : "");
		$this -> terminology	= (isset($_POST['terminology']) ? $_POST['terminology'] : "");
		$this -> explanation 	= (isset($_POST['explanation']) ? $_POST['explanation'] : "");
	}
	
	function saveGlossary()
	{
		$insert_data = array( 
				"category" 		=> $this -> category,
				"terminology"   => $this -> terminology,
				"explanation"   => $this -> explanation,
                "insertuser"   => $this->helper->getUserID(),
				);
		$response = $this -> insert( "glossary" , $insert_data );
		echo $response;//$this -> insertedId();
	}
	
	function updateGlossary( $id )
	{
		$update_data = array( 
				"category" 		=> $this -> category,
				"terminology"   => $this -> terminology,
				"explanation"   => $this -> explanation,
				);
        $logsObj = new Logs();
        $data    = $this->getAGlossary( $id );
        $logsObj -> setParameters( $_POST, $data, "glossary");
		$response = $this -> update( "glossary" , $update_data, "id=$id" );
		echo $response;		
	}	
	
	function getGlossary()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_glossary WHERE active & 2 = 2 ORDER BY category" );
		return $response;
	}
	
	function getAGlossary( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_glossary WHERE id = '".$id."' " );
		return $response;
	}

	function deleteGlossary( $id )
	{
        $logsObj = new Logs();
        $data    = $this->getAGlossary( $id );
        $logsObj -> setParameters( array( "active" => 2 ), $data, "glossary");	
		$response = $this -> update("glossary", array( "active" => 2 ), "id=$id" );
		return $response;
	}
	
}
?>