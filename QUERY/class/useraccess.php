<?php
/**
	* @package 	: User Access
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class UserAccess extends DBConnect
{
	
	protected $user;
	
	protected $module_admin;
	
	protected $create_risks;
	
	protected $create_actions;
	
	protected $view_all;
	
	protected $edit_all;
	
	protected $can_import;
	
	protected $reports;
	
	protected $assurance;
	
	protected $setup;
	
	function __construct( $user="", $module_admin=0, $create_risks=0, $create_actions=0, $can_import=0,  $view_all=0, $edit_all=0, $reports=1, $assurance=0, $setup=0 )
	{
	
		$this -> user			= trim( $user );
		$this -> module_admin   = trim( $module_admin );
		$this -> create_risks   = trim( $create_risks );
		$this -> create_actions = trim( $create_actions );
		$this -> can_import     = trim( $can_import );
		$this -> view_all 	    = trim( $view_all );
		$this -> edit_all       = trim( $edit_all );
		$this -> reports        = trim( $reports );		
		$this -> assurance 		= trim( $assurance );
		$this -> setup 		    = trim( $setup );								
		parent::__construct();
	}
	
	function getDefaultUserAccess() {
		$array = array( 
						"module_admin"  => 0,
						"create_risks"  => 0,
						"create_actions"=> 0,
						"can_import"	=> 0,
						"view_all"      => 0,
						"edit_all" 		=> 0,
						"report"   		=> 1,
						"assurance"  	=> 0,
						"setup"  		=> 0,
					);
		return $array;
	}
	
	function saveUserAccess()
	{

		$insert_data = array( 
						"user" 			=> $this -> user,
						"module_admin"  => $this -> module_admin,
						"create_risks"  => $this -> create_risks,
						"create_actions"=> $this -> create_actions,
						"can_import"	=> $this -> can_import,
						"view_all"      => $this -> view_all,
						"edit_all" 		=> $this -> edit_all,
						"report"   		=> $this -> reports,
						"assurance"  	=> $this -> assurance,
						"setup"  		=> $this -> setup,
						"insertuser"    => $_SESSION['tid'],											
						);
		$response = $this -> insert( "useraccess" , $insert_data );
		$id = $response;//$this->insertedId();
		//$user_name = $this->getActionOwnerEmail($insert_data['user']);
		$old_user_data = $this->getUserAccessInfo($id);
		$changes = array('msg'=>"Added user ".$old_user_data['tkname']." ".$old_user_data['tksurname']);
		foreach($insert_data as $key => $new) {
			if($key!="insertuser") {
				$changes[$key] = array('to'=>($new==1?"Yes":"No"),'from'=>"-");
			}
		}
		$changes['user_record_id'] = $id;
		$changes['action'] = "NEW";
		$changes['insertusername'] = $_SESSION['tkn'];
		$changes['datetime'] = date("d-M-Y H:i:s");
		$insertdata = array(
			'setup_id'=>$id,
			'changes'=>base64_encode(serialize($changes)),
			'insertuser'=>$_SESSION['tid'],
		);
		$response = $this -> insert('useraccess_logs', $insertdata);
		echo $id;//$this -> insertedId();
	}
	
	function updateUserAccess()
	{
		$updatedata = array( 
						"module_admin"  => $this -> module_admin,
						"create_risks"  => $this -> create_risks,
						"create_actions"=> $this -> create_actions,
						"view_all"      => $this -> view_all,
						"edit_all" 		=> $this -> edit_all,
						"can_import"	=> $this -> can_import,
						"report"   		=> $this -> reports,
						"assurance"  	=> $this -> assurance,
						"setup"  		=> $this -> setup,
						"insertuser"    => $_SESSION['tid'],											
						);
		$old_user_data = $this->getUserAccessInfo($this->user);
		$changes = array('msg'=>"Edited user ".$old_user_data['tkname']." ".$old_user_data['tksurname']);
		foreach($updatedata as $key => $new) {
			if($key!="insertuser" && $new != $old_user_data[$key]) {
				$changes[$key] = array('to'=>($new==1?"Yes":"No"),'from'=>($old_user_data[$key]==1?"Yes":"No"));
			}
		}
		$changes['insertuser'] = $_SESSION['tid'];
		$changes['action'] = "EDIT";
		$changes['user_record_id']=$this->user;
		$changes['insertusername'] = $_SESSION['tkn'];
		$changes['datetime'] = date("d-M-Y H:i:s");
		$insertdata = array(
			'setup_id'=>$this->user,
			'changes'=>base64_encode(serialize($changes)),
			'insertuser'=>$_SESSION['tid'],
		);
		$response = $this -> insert('useraccess_logs', $insertdata);
		$response = $this -> update( "useraccess" , $updatedata, "id=".$this -> user );

		return $response;	
	}

	function getUser($id) {
		$response = $this -> getRow( "
									SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA 
								    INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON UA.user = TK.tkid
									WHERE UA.user = $id
								  ");
		return $response;
	}	
	
	function getUserAccessInfo( $id ) {
		/**
		 * Duplicated Risk changes to query
		 * AA-559 [Risk Assist - Cannot Edit Users under Setup]
		 * Changes by Sondelani Dumalisile (09 March 2021)
		 * comment : Special character's decoding function switched from the Old DBconnect file to latest ASSIST_MODULE_HELPER
		 */
		/*$response = $this -> getRow( "
									SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA 
								    INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON UA.user = TK.tkid
									WHERE UA.id = $id
								  ");
		return $response;*/
		$hlp = new ASSIST_MODULE_HELPER();
		$response = $hlp->mysql_fetch_one("SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA
								    INNER JOIN assist_".strtolower($_SESSION['cc'])."_timekeep TK ON UA.user = TK.tkid
									WHERE UA.id = $id");
		$res = array();
		foreach($response as $index => $user)
		{
			if($index == "tkname" || $index == "tksurname") {
				$res[$index] = $hlp->decode($user);
			}else{
				$res[$index] = $user;
			}
		}
		return $res;
	}

	function getMyUserAccess()	{	
		$response = $this -> getRow( "
									SELECT UA.* FROM  ".$_SESSION['dbref']."_useraccess UA 
									WHERE UA.user = '".$_SESSION['tid']."'
									ORDER BY UA.id DESC LIMIT 1
								  ");
		if(!(count($response)>0)) {
			$ua = $this->getDefaultUserAccess();
		}
		return $response;
	}

	
	function getUserAccess() {
		/**
		 * Duplicated Risk changes to query
		 * AA-559 [Risk Assist - Cannot Edit Users under Setup]
		 * Changes by Sondelani Dumalisile (09 March 2021)
		 * comment : Special character's decoding function switched from the Old DBconnect file to latest ASSIST_MODULE_HELPER
		 */
		/*$response = $this -> get( "SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA
								   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON UA.user = TK.tkid
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   WHERE TK.tkid <> 0000 AND TK.tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."'
								   ORDER BY TK.tkname, TK.tksurname" 
							, false);
		return $response;*/
		$hlp = new ASSIST_MODULE_HELPER();
		$sql = "SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_timekeep TK ON UA.user = TK.tkid
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid
								   WHERE  MU.usrmodref = '".strtoupper(strtoupper($_SESSION['modref']))."' AND TK.tkstatus = 1
								  ORDER BY TK.tkname, TK.tksurname ";
		$response = $hlp->mysql_fetch_all($sql);
		$list     = array();
		foreach($response as $index => $user)
		{
			foreach($user as $fld => $value) { $user[$fld] = $hlp->decode($value); }
			$list[$user['user']] = $user;
		}
		return $list;
	}
	
	
	function getAUser()
	{
		$response = $this -> get( "SELECT * FROM  ".$_SESSION['dbref']."_useraccess UA 
								   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON UA.user = TK.tkid
								   " );
		return $response;
	}
		
	function getUsers()
	{							  							    
		/*$response = $this -> get( "SELECT tkid, tkname, tksurname, tkstatus , tkemail,
		                           CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								   FROM assist_".$_SESSION['cc']."_timekeep TK 
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."'
								   ORDER BY TK.tkname, TK.tksurname" );
		return $response;*/
		$hlp = new ASSIST_MODULE_HELPER();
		$sql = "SELECT tkid, tkname, tksurname, tkstatus , tkemail,CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								   FROM assist_".strtolower($_SESSION['cc'])."_timekeep TK
								   INNER JOIN assist_".strtolower($_SESSION['cc'])."_menu_modules_users MU ON TK.tkid = MU.usrtkid
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".strtoupper(strtoupper($_SESSION['modref']))."'
								ORDER BY TK.tkname, TK.tksurname";
		$response = $hlp->mysql_fetch_all($sql);
		$data = array();
		foreach($response as $key => $item) {
			foreach($item as $fld => $value) { $item[$fld] = $hlp->decode($value); }
			$data[$key] = $item;
		}
		return $data;
	}

	function getUsersNotYetSetup() {
		$response = $this -> get( "SELECT tkid, tkname, tksurname, tkstatus , tkemail,
		                           CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								   FROM assist_".$_SESSION['cc']."_timekeep TK 
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."'
								   AND tkid NOT IN (SELECT user FROM ".$_SESSION['dbref']."_useraccess)
								   ORDER BY TK.tkname, TK.tksurname" );
		return $response;
	}
	
	function getUsersList()
	{
		$response = $this -> get( "SELECT tkid, tkname, tksurname, tkstatus , tkemail,
		                           CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								   FROM assist_".$_SESSION['cc']."_timekeep TK 
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."'
								   ORDER BY TK.tkname, TK.tksurname" );
		$results   = array();	
		foreach($response as $index => $res)
		{
		   $results[$res['tkid']] = $res;
		}
	  return $results;	    
	}
	
	function getRiskManager()
	{							  							    
		$response = $this -> get( "SELECT tkid, tkname, tksurname, tkstatus , tkemail, CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								   FROM assist_".$_SESSION['cc']."_timekeep TK 
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   INNER JOIN ".$_SESSION['dbref']."_useraccess UA ON UA.user = TK.tkid 
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."' AND UA.risk_manager = 1" );
		return $response;
	}

	function getARiskManager( $id )
	{
		$response  = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_useraccess WHERE user = '".$id."'" );
		return $response; 
	}

	function getRiskManagerEmails()
	{							  						    
		$response = $this -> get( "SELECT  tkemail
								   FROM assist_".$_SESSION['cc']."_timekeep TK 
								   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid 
								   INNER JOIN ".$_SESSION['dbref']."_useraccess UA ON UA.user = TK.tkid 
								   WHERE tkid <> 0000 AND tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."' AND UA.risk_manager = 1" );
		$emailTo   = "";
		if(!empty($response)){
			foreach($response as $userEmail ) {
				$emailTo = $userEmail['tkemail'].",";
			}
		}
		$emailTo = rtrim( $emailTo, ",");
		return $emailTo;
	}
	/**
		Fetched the email of the user who has been asssigned and action or risk
	**/
	function getUserResponsiblityEmail( $id ) 
	{
		$result  = $this -> getRow("SELECT tkemail, tkname, tksurname FROM assist_".$_SESSION['cc']."_timekeep 
		                             WHERE tkid = '".$id."'
		                           ");
		return $result;
	}
	
	function getUserEmailUnderDirectorate( $ref )
	{
		$response = $this->get("SELECT TK.tkemail 
								FROM assist_".$_SESSION['cc']."_timekeep TK 
								INNER JOIN  ".$_SESSION['dbref']."_dir_admins DA ON DA.tkid = TK.tkid 
								WHERE DA.ref = '".$ref."'
								"
							  );
							  
		return $response;
	}
	
	
	function setRiskManager( $id, $status )
	{
		$updatedata = array(
						'risk_manager'  => $status,
						"insertuser"    => $_SESSION['tid'],						
		);
		$response = $this -> update( 'useraccess', $updatedata, "user=$id" );
		return $response;
	}
	
	function isRiskManager()
	{
		$response = $this -> getRow("SELECT risk_manager 
									 FROM ".$_SESSION['dbref']."_useraccess 
									 WHERE user = '".$_SESSION['tid']."' "
									);
		if( $response['risk_manager'] == 1 ) 
		{
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function getUserDirectorates()
	{
		$results = $this -> get("SELECT ref FROM ".$_SESSION['dbref']."_dir_admins 
								 WHERE tkid = '".$_SESSION['tid']."' AND type = 'DIR' AND active = 1 
							    ");
		$riskRefs = array();
		foreach($results as $r_index => $userRef)
		{
		    $riskRefs[] = $userRef['ref']; 
		}
	   return $riskRefs;	
	}
	
	function getUserRefDirectorate()
	{
		$response = $this -> get("SELECT ref FROM ".$_SESSION['dbref']."_dir_admins 
								   WHERE tkid = '".$_SESSION['tid']."' AND type = 'DIR' AND active = 1 
								 ");
		$riskRefs = "";
		foreach($response as $row)
		{
			$riskRefs .= "'".$row['ref']."',";
		}
		return rtrim( ltrim( rtrim($riskRefs, ","), "'") , "'");
	}
	
	function getManagerUsers()
	{
		$response = $this -> get( "SELECT TK.tkname, TK.tksurname, UA.user
								   FROM  ".$_SESSION['dbref']."_useraccess UA 
								   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON UA.user = TK.tkid
								   WHERE UA.insertuser = '".$_SESSION['tid']."'
								   " 
								);
		return $response;
	}
	
}
?>
