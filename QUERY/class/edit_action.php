<?php
$scripts = array( 'edit_action.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Edit Action";
require_once("../inc/header.php");

$act 		= new RiskAction("", "", "", "", "", "", "","", "");
$action 	= $act -> getRiskAction( $_REQUEST['id'] );
$stat 		= new RiskStatus("", "", "");
$statuses 	= $stat -> getStatus();
$latestUp 	= $act -> getLatestActionUpdates( $_REQUEST['id'] );

$stastusId  = (empty($latestUp) ? $action['status'] : $latestUp['name']);
$uaccess 	= new UserAccess( "", "", "", "", "", "", "", "", "");
$users 		= $uaccess -> getUsers();
?>
<script>
$(function(){
	$("table#edit_action_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form method="post" name="edit-action-table" id="edit-action-table">
   <table align="left" id="edit_action_table" border="1">
   	<tr>
    	<th>Ref#:</th>
        <td>#<?php echo $_REQUEST['id']; ?></td>
    </tr>   	<tr>
    	<th>Action:</th>
        <td><textarea name="action" id="action" cols="35" rows="7"><?php echo $action['action']; ?></textarea></td>
    </tr>
   	<tr>
    	<th>Action Owner:</th>
        <td>
        	<select name="action_owner" id="action_owner">
            	<option><?php echo $select_box_text ?></option>
                <?php
                foreach($users as $user ){
				?>
               	<option value="<?php echo $user['tkid']; ?>" 
				<?php  if($user['tkid']==$action['action_owner']){ ?>
                selected="selected" <?php } ?>>
					<?php echo $user['tkname']." ".$user['tksurname']; ?>
                </option>
                <?php
				}
				?>
            </select>
        </td>
    </tr>
   	<!--<tr>
    	<th>Status:</th>
        <td>
        	<select name="action_status" id="action_status">
            	<option>--action status--</option>
                <?php
                foreach($statuses as $status ){
				?>
               	<option value="<?php echo $status['id']; ?>" <?php  if($status['name']==$stastusId){ ?>
                	selected="selected"
                 <?php } ?>>
					<?php echo $status['name']; ?>
                </option>
                <?php
				}
				?>
            </select>
        </td>
    </tr> -->
    <tr>
    	<th>Deliverable:</th>
        <td><textarea id="deliverable" name="deliverable" cols="35" rows="7"><?php echo $action['deliverable']; ?></textarea></td>
    </tr>
   	<tr>
    	<th>Time Scale:</th>
        <td><input type="text" id="timescale" name="timescale" value="<?php echo $action['timescale']; ?>" /></td>
    </tr>
   	<tr>
    	<th>Deadline:</th>
        <td><input type="text" id="deadline" name="deadline" value="<?php echo $action['deadline']; ?>" class="datepicker_"/></td>
    </tr>
    <tr>
    	<th>Remind On:</th>
        <td><input type="text" id="remindon" name="remindon" value="<?php echo $action['remindon']; ?>" class="datepicker_"/></td>
    </tr>
    <tr>
		<th></th>
    	<td>
        <input type="submit" name="edit_action" id="edit_action" value="Edit Action" class="isubmit"  />
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="submit" name="cancel_action" id="cancel_action" value="Cancel" class="idelete"  />
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;        
        <input type="submit" name="delete_action" id="delete_action" value="Delete" class="idelete"  />        
        <input type="hidden" name="actionid" id="actionid" value="<?php echo $_REQUEST['id'] ?>"  />
        </td>
    </tr>
   <tr>
   	<td  align="left" class="noborder">
    	<?php $me->displayGoBack("",""); ?>
    </td>
   	<td align="right" class="noborder">
   		<?php $admire_helper->displayAuditLogLink( "action_edit" , false); ?>
    </td>    
   </tr>
   </table>
</form>
<div id="actioneditlog" style="clear:both;"></div>
</div>
