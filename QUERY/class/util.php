<?php
class Util
{
     
     public static function toUserId($id)
     {
        return str_pad($id, 4, '0', STR_PAD_LEFT);
     }     
     
     public static function serializeEncode($data)
     {
        return base64_encode(serialize($data));
     }

     public static function serializeDecode($data)
     {
        return @unserialize(base64_decode($data));
     }

     public static function debug($data)
     {
        if(is_array($data))
        {
           print "<pre>";
               print_r($data);
           print "</pre>";
        } else {
           print "<pre>";
               var_dump($data);
           print "</pre>";     
        }
     }
     
     public static function isTextTooLong($text, $length = 10)
     {
        if(!empty($text))
        {
          $textArr  = explode(" ", $text);
          if(count($textArr) > $length)
          {
             return TRUE;
          } else {
             return FALSE;
          }
        } else {
          return FALSE;
        }
     }
     
     public static function cutString($text, $key, $length = 10)
     {
		$text = ASSIST_HELPER::code($text);
        $words = explode(" ", $text); 
        $newStr = "";
        for($i = 0; $i <= $length; $i++)
        {
          $newStr .= $words[$i]." "; 
        } 
		$newStr.="...";
        $value = "<span id='ref_".$key."' more='".$text."' less='".$newStr."'><span id=text_".$key.">".$newStr."</span><a href='#' id='view_".$key."' class='viewmore' style='color:orange; text-decoration: none;' title='".$text."'><img src='../../pics/plus.gif' style='margin-left: 3px;' /></a></span>";
         return $value;
     }
     
     public static function cutText($text, $key, $length = 10)
     {
        $words = explode(" ", $text); 
        $newStr = "";
        for($i = 0; $i <= $length; $i++)
        {
          $newStr .= $words[$i]." "; 
        } 
        $value = $newStr." . . . ";
        return $value;
     }  
}
?>
