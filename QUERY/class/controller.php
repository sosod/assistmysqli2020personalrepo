<?php
@session_start();
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../../".strtolower($classname).".php" ) ) {
		require_once( "../../".strtolower($classname).".php" );		
	}else if(file_exists("../../../../library/dbconnect/".strtolower($classname).".php")) {
		require_once( "../../../../library/dbconnect/".strtolower($classname).".php" );		
	} else {
		require_once( "../../library/dbconnect/".strtolower($classname).".php" );		
	}
}
switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ){
	
case "getActions":
	$naming = new Naming();
	$headers = $naming->getActionColums();
	$act = new RiskAction($_REQUEST['id'], "", "", "", "", "", "","", "");
	$actions = $act -> getRiskActions( $_GET['start'], $_GET['limit'] );
	$total 	 = $act -> _getTotalAction();
	//array( "data" => $response, "total" => $this->_getTotalAction() );

	$actionsArr = array();
	$headerArr  = array();
	$isOwners   = array();
	$totalProgress = 0;
	foreach( $actions as $key => $action)
	{
		$actionsArr[$action['id']]['reference'] = $action['id'];
		$headerArr['reference'] = "Reference #";		
		foreach($headers as $index => $head){
			if(array_key_exists($index, $action))
			{
				if($index == "progress"){
					$actionsArr[$action['id']][$index] = ($action[$index] == "" ? 0 : $action[$index])." % ";
					$headerArr[$index]		  = str_replace(" ", "<br />", $head);					
				} else if($index == "action_owner"){
					$actionsArr[$action['id']][$index] = $action[$index]."<em>(".$action['department'].")</em>";
					$headerArr[$index]		  = str_replace(" ", "<br />", $head);;					
				} else {
					$actionsArr[$action['id']][$index] = $action[$index];
					$headerArr[$index]		  = str_replace(" ", "<br />", $head);;
				}			
			}		
		}
		$totalProgress += $action['progress'];		
		if($action['owner'] == $_SESSION['tid'])
		{
			$isOwners[] =  $action['id'];
		}
	}	
	$avrProgess  = round( ($totalProgress/$total),2)." %";
	echo json_encode( array("headers" => $headerArr, "aveProgress" => $avrProgess, "actions" => $actionsArr, "total" => $total, "isOwner" => $isOwners ) );
	break;
case "getActionsToApprove":
	$act = new RiskAction($_REQUEST['id'], "", "", "", "", "", "","", "");
	echo json_encode( $act -> getActionToApprove() );
	break;
case "getRisk":
	$act = new Risk("", "", "", "", "", "", "","","", "", "", "", "" );
	echo json_encode( $act -> getRisk( $_REQUEST['id'] ) );
	break;	
case "getARisk":
	$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
	echo json_encode( $risk -> getRisk( $_REQUEST['id'] ) );
	break;	
case "getAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	echo json_encode( $act -> getRiskAction( $_REQUEST['id'] ) );
	break;
case "getAAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	echo json_encode( $act -> getAAction( $_REQUEST['id'] ) );
	break;	
case "getUserAccess":
	$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "");
	echo json_encode( $uaccess -> getUserAccess() );
	break;	
case "getUsers":
	$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
	echo json_encode( $uaccess -> getUsers() );
	break;
case "getStatus":
	$stat = new RiskStatus("", "", "");
	echo json_encode( $stat -> getStatus() );
	break;
case "newRiskAction":
	$act = new RiskAction( $_REQUEST['id'],
						   $_REQUEST["r_action"],
						   $_REQUEST["action_owner"],
						   $_REQUEST["deliverable"],
						   $_REQUEST["timescale"],
						   $_REQUEST["deadline"],
						   $_REQUEST["remindon"],
						   $_REQUEST['progress'],
						   $_REQUEST['status']
						  );
		$res 	   = $act -> saveRiskAction();
		$response  = array();
        if(is_numeric($res))
        {
                if( $res > 0 ) {
                    $response["id"] 			  = $res;
                    $response['success']["saved"] = "Action #".$res." has been successfully saved";
                    $user      		     		  = new UserAccess("", "", "", "", "", "", "", "", "" );
                    $users 			     		  = $user -> getRiskManagerEmails();
                    $responsibleUserEmail 		  = $user -> getUserResponsiblityEmail( $_REQUEST['action_owner'] );
                    $risk 						  = new Risk("" ,"" , "", "", "", "", "", "", "", "", "", "", "");
                    $riskInfo 					  = $risk -> getRisk($_REQUEST['id']);
                    $email 			     		  = new Email();
                    $email -> userFrom   		  = $_SESSION['tkn'];
                    $email -> subject    		  = "New Action";
                    //Object is something like Risk or Action etc.  Object_id is the reference of that object
                    $message  = "";
                    $message .= $email -> userFrom." created an Action relating to query #".$_REQUEST['id'];
                    $message .= " : ".$riskInfo['description']." identified in the Query Register. The details of the action are as follows :";
                    $message .= " \r\n\n";	//blank row above Please log on line
                    $message .= "Action Number : ".$res."\r\n";
                    $message .= "Action : ".$_REQUEST['r_action']."\r\n";
                    $message .= "Deliverable : ".$_REQUEST['deliverable']."\r\n";
                    $message .= "Action Owner : ".$responsibleUserEmail['tkname']." ".$responsibleUserEmail['tksurname']."\r\n";
                    $message .= "Time Scale : ".$_REQUEST['timescale']."\r\n";
                    $message .= "Deadline : ".$_REQUEST['deadline']."\r\n";
                    $message .= " \r\n\n";	//blank row above Please log on line
                    $message .= "<i>Please log onto Ignite Assist in order to View or Update the Action.</i> \n";
                    $message  = nl2br($message);
                    $email -> body     = $message;
                    $emailTo 		   = "";
                    foreach($users as $userEmail ) {
                        $emailTo = $userEmail['tkemail'].",";
                    }
                    $email -> to 	   = $emailTo.";".$responsibleUserEmail['tkemail'];
                    $email -> body     = $message;
                    $emailTo 		   = "";
                    if(!empty($user)){
                        foreach($users as $userEmail ) {
                            $emailTo = $userEmail['tkemail'].",";
                        }
                    }
                    $email -> to 	   = $responsibleUserEmail['tkemail'];
                    $email -> from     = "info@ingite4u.com";
                    if( $email -> sendEmail() ) {
                        $response['success']["emailsent"] = "and a notification has been sent";
                    } else {
                        $response['error']["emailsent"] = "There was an error sending the email";
                    }
                } else {
                    $response['error']["saved"] = "Error saving the action";
                }
        } else {
            $response = array("error" => true, "text" => $res );
        }
		if(isset($_SESSION['uploads']['action'])){
			unset($_SESSION['uploads']['action']);
		}	        
		echo json_encode( $response );
	break;
case "saveActionAttachement":
		$att = new Attachments($_FILES, "action");		
		$att -> upload();	
	break;
case "deleteAction":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	$act -> deleteRiskAction( $_REQUEST['id'] );
	break;
case "updateRiskAction":
	$act = new RiskAction("",
						  $_REQUEST["r_action"],
						  $_REQUEST["action_owner"],
						  $_REQUEST["deliverable"],
						  $_REQUEST["timescale"],
						  $_REQUEST["deadline"],
						  $_REQUEST["remindon"],
						  "",
						  (isset($_REQUEST['status']) ? $_REQUEST['status'] : "" ) 
						 );
	$action 	=  $act -> getAAction( $_REQUEST['id'] ) ;
	$action['remindon'] = (isset($action['updatedremindon']) ? $action['updatedremindon'] : $action['remindon']);
	$changesArr		= array();
	$changeMessage = "";
	//find the difference made to the action 
	foreach( $_REQUEST as $key => $value)
	{
		if( isset($action[$key]) ||  array_key_exists($key, $action))
		{
			if( $key == "action"){
				continue;
			} else if( $value !== $action[$key] ){
				$changeMessage    .= ucwords($key)." changed to ".$value." from ".$action[$key]." \r\n"; 
				$changesArr[$key]  = array("to" => $value, "from" => $action[$key]); 
			} 
		}
		if( $key == "r_action") {
			if( $value !== $action['action'] ){
				$changeMessage 		  .= "Action changed to ".$_REQUEST['r_action']." from ".$action['action']." \r\n"; 
				$changesArr['action']  = array( "to" => $_REQUEST['r_action'] ,"from" => $action['action']); 				
			}
		}
	}
	$changesArr['user'] = $_SESSION['tkn'];
	$res 				= $act -> updateRiskAction( $_REQUEST['id'] , serialize($changesArr) );
	$response 			= array();

	if(is_numeric($res)){
		if( $res > 0 ){
			$responeText 				  = "Action #".$_REQUEST['id']." has been successfully edited <br />";
			$user      		     		  = new UserAccess("", "", "", "", "", "", "", "", "" );
			$users 			     		  = $user -> getRiskManagerEmails();
			$email 			     		  = new Email();
			$email -> userFrom   		  = $_SESSION['tkn'];
			$email -> subject    		  = "Edit Action";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message   = ""; 
			//$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
			$message .= " \n";	//blank row above Please log on line
			
			$message .= $email -> userFrom." edited action #".$_REQUEST['id']." related to query #".$action['risk_id']."";
			$message .= " : ".$action['description']."\r\n\n";		
			if($changeMessage == ""){
				$message  .= "";
			} else {
				$message .= "The following changes were made : \r\n\n";
				$message .= $changeMessage;
			}
			$message .= "\r\n Please log onto Ignite Assist in order to View or Update the Action.\r\n";
			$message  = nl2br($message);		
	
			$email -> body     = $message;
			$emailTo 		   = "";
			if(!empty($users)){
				foreach($users as $userEmail ) {
					$emailTo = $userEmail['tkemail'].",";
				}
			}
			$email -> to 	   = ($emailTo == "" ? "" : rtrim($emailTo,","));
			$email -> from     = "info@ingite4u.com";
			if( $email -> sendEmail() ) {
				$response = array("error" => false, "text" => $responeText." &nbsp;&nbsp;&nbsp;&nbsp; Email send successfully \r\n" );
			} else {
				$response = array("error" => true , "text" => $responeText." &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email \r\n");
			} 	
		} else if( $res == 0){
			$response = array("text" => "No change was made to the action" , "error" => false);
		} else {
			$response = array("error" => true, "text" => "Error occurred updating the action" );
		} 
	} else {
		$response = array("error" => true, "text" => "Error occurred updating the action <br />".$res );		
	}
	echo json_encode( $response );
	break;
case "getEditLog":
	$act 	 		= new RiskAction("", "", "", "", "", "", "", "", "" );
	$editLog 		= $act -> getActionEditLog( $_REQUEST['id'] );
	$changes 		= array();
	$changeLog		= array();
	$changeMessage  = "";
	foreach( $editLog as $key => $log)
	{	
		$id				   		  = $log['id'];
		$changes	        	  = unserialize($log['changes']);
		$changeLog[$id]['date']   = $log['insertdate'];
		$changeLog[$id]['status'] = $log['status']; 
		if( isset( $changes) && !empty($changes) ){
			$changeMessage  = "The action has been edited by ".$changes['user']."<br /><br />";
			$changeMessage .= "The following changes were made : <br />";			
			foreach( $changes as $key => $change)
			{
				if( $key == "user")
				{ continue; } else{
				   $changeMessage .= ucwords($key)." has changed to ".$change['to']." from ".$change['from']." <br /> ";
				}
			}
		}
	  $changeLog[$id]['changeMessage'] = $changeMessage;
	  usort($changeLog, "compare");
	}
	echo json_encode( $changeLog );
	break;
case "getActionUpdates":
	$act 			= new RiskAction("", "", "","" , "", "", "", "", "");
	$updatesLog     = $act -> getActionUpdates( $_REQUEST['id'] ); 	
	$changes 		= array();
	$changeLog		= array();
	$changeMessage  = "";
	foreach( $updatesLog as $key => $log)
	{	
		$id				   		  = $log['id'];
		$changes	        	  = unserialize($log['changes']);
		$changeLog[$id]['date']   = $log['insertdate'];
		$changeLog[$id]['status'] = $log['status']; 
		if( isset( $changes) && !empty($changes) ){
			$changeMessage  = "The action has been updated by ".(isset($changes['user']) ? $changes['user'] : "")."<br /><br />";
			$changeMessage .= "The following changes were made : <br />";			
			foreach( $changes as $key => $change)
			{
				if( $key == "user")
				{ continue; } else if($key == "description"){
					$changeMessage 	.= $change."\r\n<br />";
				}else{
				   $changeMessage .= "\r\n".ucwords($key)." has changed to ".$change['to']." from ".$change['from']." <br /> ";
				}
			}
		}
	  $changeLog[$id]['changeMessage'] = $changeMessage;
	  usort($changeLog, "compare");
	}
	echo json_encode( $changeLog );
	break;	
case "newActionUpdate":
	$act 			= new RiskAction("", "", "","" , "", "", $_REQUEST["remindon"], $_REQUEST['progress'], $_REQUEST['status']);
	$action 		= $act -> getAAction( $_REQUEST['id'] ) ;
	$status 		= new ActionStatus("","", "");
	$naming 		= new Naming();
	$actionstatus	= $status->getAStatus( $_REQUEST['status'] ); 
	$changesArr 	= array();
	$changeMessage	= "";
	$requestArr 	= $_REQUEST;
	$action['progress']   = ((isset($action['updatedprogres']) ? $action['updatedprogres'] : $action['progress']) == "" ? 0 : (isset($action['updatedprogres']) ? $action['updatedprogres'] : $action['progress']));
	$requestArr['status'] = $actionstatus['name'];
	$action['status']	  = (isset($action['updatedstatus']) ? $action['updatedstatus'] : $action['status']);   
	foreach( $requestArr as $key => $value)
	{
		if( $key == "action"){
			continue;
		} else if( $key == "description"){
			$changeMessage   .= "Added update description ".$value."r\n" ;
			$changesArr[$key] = "Added update description ".$value."\r\n";
		} else if( isset($action[$key]) ||  array_key_exists($key, $action))
		{
			if( $value !== $action[$key] )
			{
				 $changeMessage  .= $naming -> setHeader($key)." changed to ".$value."".($key == "progress" ? "%" : "")." from ".$action[$key]."".($key == "progress" ? "%" : "")." \r\n"; 
				 $changesArr[$key]  = array("from" => $action[$key], "to" => $value); 
			} 
		}
	}
	$changesArr['user'] = $_SESSION['tkn'];	
	$id 	= $act -> newRiskActionUpdate( 
											$_REQUEST['id'] ,
											$_REQUEST['description'],
											$_REQUEST['approval'],
											$_REQUEST['attachment'],
											serialize($changesArr)
										);
	$response  = array(); 
		if( $id > 0 ) {
			$response["id"] 	 = $id;
			$error 				 = false;
			$text 				 = " New action update for action #".$_REQUEST['id']." has been successfully saved <br />";
			$user      		     = new UserAccess("", "", "", "", "", "", "", "", "" );
			$users 			     = $user -> getRiskManagerEmails();		
			$email 			     = new Email();
			
			$email -> userFrom  = $_SESSION['tkn'];
			$email -> subject   = " Update Action ";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message   = ""; 
			//$message .= "<i>".$email -> userFrom." has assigned  a new action to you.</i> \n";
			$message .= " \n";	//blank row above Please log on line
			$message .= $_SESSION['tkn']." Updated action #".$id." related to query #".$action['risk_id']." : ";
			$message .= " ".$action['description']."\r\n\n"; 
			if( $changeMessage == "")
			{
				$message  = "";
			} else{
				$message .= $changeMessage;
			}
			$message .= "Please log onto Ignite Assist in order to View further information on the Query and or associated Actions \n";
			$message  = nl2br($message);			
			$email -> body     = $message;
			$emailTo 		   = "";
			if(!empty($users)){
				foreach($users as $userEmail ) {
					$emailTo = $userEmail['tkemail'].",";
				}
			}
			$email -> to 	   = ($emailTo == "" ? "" : $emailTo);
			$emil -> to 	  .= $action['action_creator']; 
			$email -> from     = "info@ingite4u.com";
			if( $email -> sendEmail() ) {
				$text .= "Email send successfully";
			} else {
				$error = true;
				$text .= "There was an error sending the email";
			}
			$response = array("text" => $text, "error" => $error );
		} else {
			$response = array("text" => "Error saving the action update", "error" => true, "updated" => false);
		}
		echo json_encode( $response );
	break;
case "getActionAssurances":
	$action = new ActionAssurance($_REQUEST['id'], "", "", "", "", "", "");
	echo json_encode( $action -> getActionAssurances( $_REQUEST['id'], $_REQUEST['start'], $_REQUEST['limit'] ) );
	break;
case "getActionAssurance":
	$action = new RiskAction("", "", "", "", "", "", "","", "");
	$action -> saveActionAssurance($_REQUEST['id'], "", $_REQUEST['response'], $_REQUEST['signoff'], "", $_REQUEST['attachment'], "" ) ;
	break;
case "newAssuranceAction":
	$newAssurance = new ActionAssurance( $_REQUEST['id'], $_REQUEST['date_tested'], $_REQUEST['response'], $_REQUEST['signoff'], "", $_REQUEST['attachment'], "" );
	echo json_encode( $newAssurance -> saveActionAssurance() );
	break;
case "updateActionAssurance":
	$newAssurance = new ActionAssurance( $_REQUEST['action_id'], $_REQUEST['date_tested'], $_REQUEST['response'], $_REQUEST['signoff'], "", $_REQUEST['attachment'], "" );
	echo json_encode( $newAssurance -> updateActionAssurance( $_POST['id'] ) );
	break;
case "deleteActionAssurance":
	$newAssurance = new ActionAssurance( "", "", "", "", "", "", "" );
	echo json_encode( $newAssurance -> deleteActionAssurance( $_POST['id'] ) );	
	break;	
case "newApprovalAction":
		$act = new RiskAction("", "", "", "", "", "", "","", "");
		$act -> approveAction( 
								$_REQUEST['id'],
								$_REQUEST['response'],
								$_REQUEST['signoff'],
								($_REQUEST['notification'] == "on" ? "1" : "0"),
								$_REQUEST['attachment']
							);
	break;
case "sendRequestApprovalEmail":
			$response = array(); 
			$emailTo  = "";
			$act 	  = new RiskAction("", "", "", "", "", "", "","", "");
			$status   = $act -> getApprovalRequestStatus( $_REQUEST['id'] );
			if( ($status['action_status'] & RiskAction::REQUEST_APPROVAL) == RiskAction::REQUEST_APPROVAL  )
			{
				$response = array("error" => true, "text" => "A request for approval has already been sent");
			} else {
				$user      		     		  = new UserAccess("", "", "", "", "", "", "", "", "" );
				$users 			     		  = $user -> getRiskManagerEmails();
				$action						  = $act -> getAAction( $_REQUEST['id'] );
				$email 			     		  = new Email();
				$email -> userFrom   		  = $_SESSION['tkn'];
				$email -> subject    		  = "Action Approval Request";
				//Object is something like Risk or Action etc.  Object_id is the reference of that object
				$message   = ""; 
				$message .= $email->userFrom." has submitted a request for approval of an action #".$_REQUEST['id'];
				$message  .= " related to Query ".$action['risk_id']." : ".$action['description'];
				$message .= " \r\n\n";
					//blank row above Please log on line
				$message .= "Please log onto Ignite Assist to approve the action.\r\n\n";
				$message  = nl2br($message);		
				$email -> body     = $message;
				$emailTo 		   = "";
				if(!empty($users)){
					foreach($users as $userEmail ) {
						$emailTo .= $userEmail['tkemail'].",";
					}
				}
				$email -> to 	   = ($emailTo == "" ? "" : $emailTo)."".$action['action_creator'];
				$email -> from     = "info@ignite4u.com";
				if( $email -> sendEmail() ) {
					$id	= $act->setActionStatus( $_REQUEST['id'], $status['action_status'] + RiskAction::REQUEST_APPROVAL );					
					$response = array("text" => "Email send successfully", "error" => false);					
				} else {
					$response = array( "error" => true, "text" => "There was an error sending the request approval email" );
				}
			} 
		echo json_encode( $response );			
		break;	
case "searchActionToApprove":
	$act = new RiskAction("", "", "", "", "", "", "","", "");
	echo json_encode( $act -> searchActionToApprove( $_REQUEST['searchtext'] ) );
	break;
}

	function compare($a ,$b)
	{
		return ( $a['date'] > $b['date'] ? -1 : 1 );
	}

?>