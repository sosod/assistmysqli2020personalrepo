<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admire
 * Date: 6/21/11
 * Time: 2:31 AM
 * To change this template use File | Settings | File Templates.
 */
 
class Logs extends DBConnect{

    protected $tableName;

    protected $postArr;

    protected $objectInContext;

    protected $setupId;

    function __construct()
    {
        parent::__construct();
    }

    function setParameters($postArr, $context, $table )
    {
        $this->postArr          = $postArr;
        $this->objectInContext  = $context;
        $this->tableName        = $table;
        $keyPost                = key($_POST);
        $this->setupId          = (isset($postArr['id']) ? isset($postArr['id']) : $_POST[$keyPost]);
        $this->saveLog();
    }

    function viewLogs( $table_name )
    {
        $this->tableName = $table_name;
        $results = $this -> get("SELECT setup_id, changes, insertdate FROM ".$_SESSION['dbref']."_".$table_name." WHERE 1 ORDER BY insertdate DESC");
        $changes = array();
        foreach($results as $key => $updateArr){
            $changeMessage = $this->_extractChanges( $updateArr );
            $changes[$updateArr['insertdate']] = array(
                                                   "ref" 	 => $updateArr['setup_id'],
                                                   "date" 	 => $updateArr['insertdate'],
                                                   "changes" => $changeMessage
                                                 );
        }
        return $changes;
    }

    function saveLog()
    {
        $changesData = $this -> _processChanges();
        if(!empty($changesData['changes']))
        {   
            $_changes['changes']['ref_']    = "Ref #".(isset($_REQUEST['id']) ? $_REQUEST['id'] : $this -> setupId);
            $changesData['changes']['user'] = $_SESSION['tkn'];
            $changes              = array_merge($_changes, $changesData['changes']);
            $changes['changes']   = base64_encode(serialize($changes));
            $insertdata           = array("changes"    => $changes['changes'],
                                          "setup_id"   => $this -> setupId,
                                          "insertuser" => $_SESSION['tid']
                                        );                       
            return $this -> insert($this->tableName."_logs", $insertdata);
            //return $this->insertedId();
        }
    }

    function _processChanges()
    {
        $headers       = new Naming();
        $changes       = array();
        $changeMessage = $_SESSION['tkn'];
        $id            = "";
        if( !empty( $this->postArr['id']))
        {
        	$id = $this->postArr['id'];
        } else {
        	$id = $this->objectInContext['id'];
        }
        foreach($this->objectInContext as $key => $value)
        {
            if( isset($this->postArr[$key]) || array_key_exists($key, $this->postArr))
            {
                if($this->postArr[$key] != $value)
                {
                    if( $key == "status" || $key == "active")
                    {
                        $fromStatus  = $this-> _checkStatus( $value);
                        $toStatus    = $this-> _checkStatus( $this->postArr[$key] );
                        //$changeMessage = $headers ->setHeader($key)." changes to ".$toStatus." from ".$fromStatus."\r\n\n";
                        //$changes[$key] = array("from" => $fromStatus, "to" => $toStatus );
                        $changes[$key] =  " Has been ".$toStatus;
                    } else {
                        //$changeMessage = $headers ->setHeader($key)." changes to ".$this->postArr[$key]." from ".$value."\r\n\n";
                        $changes[$key] = array("from" => $value, "to" => $this->postArr[$key] );
                    }
                }
            }
        }
        return array("changes" => $changes, "message" => $changeMessage);
    }
	function _extractChanges( $updateArray )
	{
		$naming		= new Naming();
		$adds		= array("user", "date_completed", "saving", "attachments");
		$changeMessage = "";
		if( isset($updateArray['changes']) && !empty($updateArray['changes']) ){
			$changesArr = unserialize( $updateArray['changes'] );

			foreach( $changesArr  as $index => $val ){
				if( in_array($index,$adds) ){
					$changeMessage .= $val."<br />";
				} else if( $index == "remindon" ){
					if(is_array($val)){
						$changeMessage .=  $naming ->setHeader($index)." has changed to ".$val['to']." from ".$val['from']."<br />";
					} else {
						$changeMessage .=  $naming ->setHeader($index)." ".$val."<br />";
					}
				} else if( $index == "response" ){
					$changeMessage .= $val."<br />";
				}else{
					if( isset($val['to']) && isset($val['from'])){

						$changeMessage .= $naming ->setHeader($index)." has changed to ".$val['to']." from ".$val['from']."<br />";
					}
				}
			}
		}
		return $changeMessage;
	}

    function _checkStatus( $value )
    {
        $status = "";
        if( $value == 2){
            $status = " deleted";
        } else if($value == 1){
            $status = " activated";
        } else if($value == 0){
            $status = " deactivated";
        }
        return $status;
    }

}
