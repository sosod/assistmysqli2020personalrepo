<?php
/**
	* Inherent Risk
	* @author	: admire<azinamo@gmail.com>
	* @copyright: 2011 , Ignite Assist
**/
class InherentRisk extends DBConnect
{
	protected $rating_from;
	
	protected $rating_to;
	
	protected $magnitude;
	
	protected $response;
	
	protected $color;
		
	function __construct( $rating_from, $rating_to, $magnitude, $response,  $color )
	{
		$this -> rating_from = trim($rating_from);
		$this -> rating_to 	 = trim($rating_to);
		$this -> magnitude   = trim($magnitude);
		$this -> response    = trim($response);
		$this -> color 		 = trim($color);						
		parent::__construct();
	}
		
	function saveInherentRisk()
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"magnitude"   => $this -> magnitude,
						"response"    => $this -> response,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> insert( "inherent_exposure" , $insert_data );
		echo $response;//$this -> insertedId();
	}	
	
	function updateInherentRisk( $id )
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"magnitude"   => $this -> magnitude,
						"response"    => $this -> response,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> update( "inherent_exposure" , $insert_data , "id=$id");
		return $response;		
	}		
		
	function getInherentRisk()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_inherent_exposure " );
		echo json_encode( $response );
	}
	
	function getAInherentRisk( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_inherent_exposure WHERE id = $id" );
		return $response ;
	}

	function updateInherentRiskStatus( $id, $status )
	{
		$updatedata = array(
						'active' 		=> $status,
						"insertuser"    => $_SESSION['tid'],						
		);
		$response = $this -> update( 'inherent_exposure', $updatedata, "id=$id" );
		echo $response;
	}
	/**
		Fetches al the inherent risk and create the range group associating it with a color
		@return array
	**/
	function getInherentRiskRange()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_inherent_exposure " );
		$rangesColor = array();
		foreach($response as $key => $row ) 
		{
			$range = "";
			// create a string of the range 
			for( $i = $row['rating_from']; $i <= $row['rating_to'] ; $i++ ) 
			{
				$range .= $i.","; 
			}
			//create an array with the range and the associated color
			$rangesColor[$row['magnitude']] = array( "color" => $row['color'], "range" => rtrim($range,",") );	
		}
		return $rangesColor;
	}
	
	function getRiskInherentRange( $value )
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_inherent_exposure " );
		$rangesColor = "";
		foreach($response as $key => $resArray ) 
		{
			//$range = "";
			// create a string of the range 
			for( $i = $resArray['rating_from']; $i <= $resArray['rating_to'] ; $i++ ) 
			{
				if( $i == $value ) 
				{
					$rangesColor = $resArray['color'];	
				}
				
			}
			//create an array with the range and the associated color
		}
		return $rangesColor;
	}	
	
	function activateInherentRisk( $id )
	{
		$update_data = array(
					'active'		 => "1",
					"insertuser"     => $_SESSION['tid'],						
		);
		$response = $this -> update( 'inherent_exposure', $update_data, "id=$id" );
		echo $response;
	}
}
?>