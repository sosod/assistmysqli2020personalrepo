<?php
/**
	* @package 	: Directorate
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class Directorate extends DBConnect
{
	protected $shortcode;
	
	protected $dept_function;
	
	protected $user_responsible;
	
	protected $department;
	
	function __construct( $shortcode, $dept_function, $user_responsible, $department )
	{
		$this -> shortcode 		  = trim($shortcode);
		$this -> dept_function 	  = trim($dept_function);
		$this -> user_responsible = trim($user_responsible);
		$this -> department  	  = trim($department);
				
		parent::__construct();
	}
	function getDirectorate()
	{
		$defaults = new Defaults();
		$dirsub   = $defaults -> getDefaults();
		$toDir 	  = true;
		$toSub	  = false;
		foreach($dirsub as $index => $value )
		{
			if( $value['name'] == "risk_is_to_directorate" && $value['value'] == 1){
				$toDir = true;
			}
			
			if( $value['name'] == "risk_is_to_subdirectorate" && $value['value'] == 1){
				$toSub = true;
				$toDir = false;
			}			
		}
		if( $toDir ){
			$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_dir D
								   INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON  D.dirid = DS.subdirid
								   WHERE DS.subhead = 'Y' AND DS.active = 1 AND D.active = 1 " );
		} else if( $toSub ){
			/*$response = $this -> get(" SELECT s.subid , CONCAT_WS('-', d.dirtxt, s.subtxt) AS dirtxt
									  FROM ".$_SESSION['dbref']."_dir d
									  INNER JOIN ".$_SESSION['dbref']."_dirsub s ON d.dirid = s.subdirid
									   AND s.active = 1 
									   AND (s.subid 
										IN( SELECT ref FROM ".$_SESSION['dbref']."_dir_admins 
											WHERE active = 1 AND type = 'SUB'	
										  ) 
										   OR s.subid 
											IN( SELECT ref FROM ".$_SESSION['dbref']."_dir_admins WHERE active = 1 AND type = 'DIR' )
									   ) 
									 WHERE d.active = 1 ORDER BY d.dirsort, s.subsort
							   "  );*/
			$response  = $this -> get("SELECT 
									   subid,
									   CONCAT_WS(' - ', DR.dirtxt, DS.subtxt) AS dirtxt,
									   DR.active 
									   FROM ".$_SESSION['dbref']."_dir DR
									   INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DR.dirid = DS.subdirid
									   WHERE DR.active = 1 AND DS.active = 1
									   ORDER BY DR.dirsort, DS.subsort
 									");
		}
		

		$result = array_map("decodeDirectorate", $response);
		return $result;
	}
	
	function getAllDirectorates(){
		$response = $this -> get( "SELECT DS.id, DS.shortcode,DS.dept_function,DS.department ,UA.user, DS.assign_actions ,DS.active
								   FROM ".$_SESSION['dbref']."_directorate_structure DS WHERE 1
								" );
		return $response;
	}
	
	function getADirectorate( $id )
	{
		$defaults = new Defaults();
		$dirsub   = $defaults -> getDefaults();
		$toDir 	  = true;
		$toSub	  = false;
		foreach($dirsub as $index => $value )
		{
			if( $value['name'] == "risk_is_to_directorate" && $value['value'] == 1){
				$toDir = true;
			}
			
			if( $value['name'] == "risk_is_to_subdirectorate" && $value['value'] == 1){
				$toSub = true;
				$toDir = false;
			}			
		}

		if( $toDir ){
			$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_dir D
								   INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON  D.dirid = DS.subdirid
								   WHERE DS.subid = {$id}" );
		} else if( $toSub ){
			/*$response = $this -> get(" SELECT s.subid , CONCAT_WS('-', d.dirtxt, s.subtxt) AS dirtxt
									  FROM ".$_SESSION['dbref']."_dir d
									  INNER JOIN ".$_SESSION['dbref']."_dirsub s ON d.dirid = s.subdirid
									   AND s.active = 1 
									   AND (s.subid 
										IN( SELECT ref FROM ".$_SESSION['dbref']."_dir_admins 
											WHERE active = 1 AND type = 'SUB'	
										  ) 
										   OR s.subid 
											IN( SELECT ref FROM ".$_SESSION['dbref']."_dir_admins WHERE active = 1 AND type = 'DIR' )
									   ) 
									 WHERE d.active = 1 ORDER BY d.dirsort, s.subsort
							   "  );*/
			$response  = $this -> getRow("SELECT 
									   subid,
									   CONCAT_WS('-', DR.dirtxt, DS.subtxt) AS dirtxt,
									   DR.active 
									   FROM ".$_SESSION['dbref']."_dir DR
									   INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DR.dirid = DS.subdirid
									   WHERE subid = $id 
 									");
		}
		return $response;
	}
	
	function saveDirectorate()
	{

		$insert_data = array( 
						"shortcode" 		=> $this -> shortcode,
						"dept_function" 	=> $this -> dept_function,
						"user_responsible"  => $this -> user_responsible,
						"department"		=> $this -> department,			
						);
		$response = $this -> insert( "directorate_structure" , $insert_data );
		echo $response;//$this -> insertedId();
	}
	
	function updateDirectorate( $id )
	{
		$insert_data = array( 
						"shortcode" 		=> $this -> shortcode,
						"dept_function" 	=> $this -> dept_function,
						"user_responsible"  => $this -> user_responsible,
						"department"		=> $this -> department,			
						);
		$response = $this -> update( "directorate_structure" , $insert_data, "id=$id" );
		echo $response;	
	}
	
	
	function updateDirectorateStatus( $id, $status)
	{
		$updatedata = array(
						'active' => $status
		);
		$response = $this -> update( 'directorate_structure', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateDirectorate( $id )
	{
		$update_data = array(
						'active' => "1"
		);
		$response = $this -> update( 'directorate_structure', $update_data, "id=$id" );
		echo $response;
	}
	
	//NEEDED BY ADMINISTRATOR class
	function getSubDirectorates($dir_id) {
		$sql = "SELECT 
									   subid as id,
									   CONCAT_WS('-', DR.dirtxt, DS.subtxt) as name
									   FROM ".$_SESSION['dbref']."_dir DR
									   INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DR.dirid = DS.subdirid AND DS.active = 1
									   WHERE DR.dirid = ".$dir_id." AND DR.active = 1
 									";
			$response  = $this -> get($sql);
		return $response;
	}

	//NEEDED FOR REPORTING
	function getReportList() {
		$data = array();
		$response = $this->getDirectorate();
		foreach($response as $d) {
			$data[$d['subid']] = array(
				'id'	=> $d['subid'],
				'name'	=> $d['dirtxt'],
			);
		}
		return $data;
	}
	

}
function decodeDirectorate( $array )
{
	return array(
				 "dirtxt" => html_entity_decode($array['dirtxt'], ENT_QUOTES, "ISO-8859-1") ,
				 "subid"  => $array['subid'], 
				 "active" => $array['active']
				 );
}
?>