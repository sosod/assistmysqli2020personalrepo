<?php
/**
	This class sends and makes log of the emails send 
	@package	: Email 
	@author		: admire<azinamo@gmail.com>
	@copyright	: 
	@filename	: email.php
**/
class Email extends DBConnect
{
	/**
		Email id
		@int 
	**/
	protected $id;
	/**
		Type of email send
		@char
	**/
	protected $ref;
	/**
		Id reference of the email send
		@int
	**/
	protected $ref_id;
	/**
		From email address
		@char
	**/	
	var $from;
	/**
		To email address
		@char
	**/
	var $to;
	/**
		CC email address
		@char
	**/
	protected $cc;
	/**
		BCC addreess
		@char
	**/
	protected $bcc;
	/**
		Email body
		@char
	**/	
	var $body;
	/**
		Emial attachments
		@char
	**/
	protected $attach;
	/**
	Subject of the email	
	**/
	var $subject;
	var $userFrom;
	
	
	function __construct()
	{
		parent::__construct();
	}	
	
	function sendEmail( )
	{
		$message = "";
		//User details
		//$userFrom = $adduser['tkname']." ".$adduser['tksurname'];
		//$userFrom = "Admire";
		//$from     = "admire@trfficsynergy.com";//$adduser['tkemail'];
		//$to       = $recipient['tkemail'];
		//Email details
		//Object is something like Risk or Action etc.  Object_id is the reference of that object
		//$message .= "<i>".$userFrom." has assigned a new ".strtolower($object)." to you.</i> \n";
			//foreach field/heading {
		//$message .= "<b>".$field_heading.":</b> ".$field_value."\n";		
			// }
		///$message .= " \n";	//blank row above Please log on line
		///$message .= "<i>Please log onto Ignite Assist in order to update this ".strtolower($object).".</i> \n";
		///$message  = nl2br($message);
		//Send email
		$headers  = 'MIME-Version:1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=us-ascii' . "\r\n";
		$headers .= 'From: no-reply@ignite4u.co.za' . "\r\n";
		$headers .= 'Reply-to: '.$this -> userFrom.' <'.$this -> from.'>' . "\r\n";
		if(@mail($this -> to, $this -> subject, $this -> body,$headers)) 
		{
			return TRUE;
		} else {
			return FALSE;
		}
        return FALSE;
	}
	

}
?>
