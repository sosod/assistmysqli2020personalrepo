<?php
class Reminder extends DBConnect
{

	function __construct() 
	{
	    parent::__construct();				
	}

	function saveReminder()
	{
		$insert_data = array('query_days'  => $_POST['query_days'],
		                     'action_days' => $_POST['action_days'],
		                     'insertuser'  => $_SESSION['tid']
		                    );
		$response    = $this -> insert('reminders', $insert_data );
		return $response;//$this -> insertedId();
	}
	
	function getReminder($id)
	{
		$result = $this -> getRow("SELECT * FROM ".$_SESSION['dbref']."_reminders WHERE id = '".$id."' ");
		return $result;           
	}
		
	function getAReminder()
	{
		$results = $this -> getRow("SELECT * FROM ".$_SESSION['dbref']."_reminders WHERE 1");	
		return $results;
	}
			
		
	function updateReminder($id)
	{
	    $reminder   = $this -> getReminder($id);
	    $updatedata = array();
	    $changes    = array();
	    if(!empty($reminder))
	    {
	       if($reminder['query_days'] !== $_POST['query_days'])
	       {
	          $from                     = $reminder['query_days'];
	          $to                       = $_POST['query_days']; 
	          $updatedata['query_days'] = $to;
	          $changes['query_days']    = "Query reminder days before dealine changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
	       }
	       
	       if($reminder['action_days'] !== $_POST['action_days'])
	       {
	          $from                      = $reminder['action_days'];
	          $to                        = $_POST['action_days']; 
	          $updatedata['action_days'] = $to;
	          $changes['action_days']    = "Action reminder days before dealine changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
	       }      
	    }
	    $updates = array();
	    $res     = 0;
	    if(!empty($changes))
	    {
	       $changes['user']       = $_SESSION['tkn'];
	       $updates['changes']    = base64_encode(serialize($changes));
	       $updates['insertuser'] = $_SESSION['tid'];
	       $res                  += $this -> insert('reminders_logs', $updates);
	    }
	    if(!empty($updatedata))
	    {
	        $res         += $this -> update('reminders', $updatedata, "id=$id" );
	    }
		return $res;
	}


	function deleteReminder( $id )
	{
	    $updatedata         = array();
	    $changes            = array();
	    $notification       = $this -> getReminder($id);	    
	    if(isset($_POST['active']))
	    {
	        if($_POST['active'] != $notification['active'])
	        {
	            if($_POST['active'] == 0)
	            {
	                $changes['active_']    = "Reminder setting deleted \r\n\n";
	                $updatedata['active']  = $_POST['active'];
	            }
	        }	    
	    } 
	    $res = 0;
	    if(!empty($changes))
	    {
	       $updates['reminder_id']     = $id;
	       $changes['user']            = $_SESSION['tkn'];
	       $_changes['ref_']           = "Ref #".$id." has been updated";
	       $changes                    = array_merge($_changes, $changes);
	       $updates['insertuser']      = $_SESSION['tid'];
	       $updates['changes']         = base64_encode(serialize($changes));
	       $mar = $this -> insert('reminders_logs', $updates);
	       $res += $mar;//$this -> insertedId();
	    }
		$res = $this -> update('reminders', $updatedata, "id=$id" );
		return $res;
	}
	
}
?>
