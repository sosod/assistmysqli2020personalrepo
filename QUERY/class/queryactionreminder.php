<?php
class QueryActionReminder
{
   private $dbObj;  
   
   private $emailObj;
     
   function __construct($db)
   {
     $this -> dbObj    = $db;
   }     
   
   function sendReminders($users)
   {
        $actions      = $this -> getReminders();       
        $headers      = $this -> getActionHeaders(); 
        $statuses     = $this -> getStatuses();
        $pActions     = $this -> getMyProfileActions($users, $statuses);
        $userActions  = array();
        $reminderSent = 0; 
        $sendTo       = array();
        if(!empty($actions))
        {
          foreach($actions as $index => $val)
          {
             //if action owner exists , then send the email
             if(isset($users[$val['action_owner']]))
             {
                
                //$userActions[$val['action_status']]['actions'][$val['action_ref']] = $users[$val['action_owner']]['name'];
                $userActions[$val['action_owner']]['actions'][$val['action_ref']] = $val;
                $userActions[$val['action_owner']]['actions'][$val['action_ref']]['action_owner'] = $users[$val['action_owner']]['name'];
                if(isset($statuses[$val['status']]))
                {
                   $action_status = $statuses[$val['status']]['name'];
                } else {
                   $action_status = "New";
                }
                $userActions[$val['action_owner']]['actions'][$val['action_ref']]['action_status'] = $action_status;
                $userActions[$val['action_owner']]['user_data']                   = $users[$val['action_owner']];
             }
          }
        }
        $_userActions = array_merge($userActions, $pActions);  
        if(!empty($_userActions))
        {
             foreach($_userActions as $user_id => $data)
             {
                 $emailObj       = new ASSIST_EMAIL_SUMMARY($data['user_data']['email'], 'Query Assist: Reminder for Actions', '', '', '', 'Query Assist', 'Action Reminders', $headers, $data['actions'], "deadline");
                 if($emailObj -> sendEmail())
                 {
                     $reminderSent      += count($data['actions']);
                     $sendTo[$user_id] = " reminder email sent to ".$data['user_data']['email'];
                 }
             }           
        }
        $sendTo['totalSend'] = $reminderSent;      
        return $sendTo;
   }
   
   function getReminders()
   {
      $today                = date("d-M-Y"); 
      $reminder_setting_sql = $this -> getReminderSetting(); 
      $actions              = $this -> dbObj -> mysql_fetch_all("SELECT CONCAT('QA', A.id) AS action_ref, 
                                                                 A.action AS query_action, 
                                                                 A.deliverable, A.timescale, A.deadline, A.remindon,
                                                                 A.action_on, CONCAT(A.progress, '%') AS progress, 
                                                                 A.action_owner, A.status, A.action_status, A.attachement, 
                                                                 A.risk_id 
                                                                 FROM ".$this -> dbObj -> getDBRef()."_actions A
                                                                 WHERE STR_TO_DATE(A.remindon, '%d-%M-%Y') =  STR_TO_DATE('".$today."', '%d-%M-%Y')
                                                                 AND A.active = 1 AND A.status <> 3 $reminder_setting_sql ");        
     return $actions;  
   }
   
   function getMyProfileActions($users, $statuses)
   {
      $recieve_day = array(1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday');
      $userActions = array();
      foreach($users as $user_id => $userdata)
      {
        $usernotification = $this -> dbObj -> mysql_fetch_one("SELECT * FROM ".$this -> dbObj -> getDBRef()."_user_notifications
                                                               WHERE user_id = '".$user_id."'
                                                           ");
        $action_sql = " AND action_owner = '".$user_id."' ";                                                           
        if(!empty($usernotification))
        {
            //if the user is supposed to recieve the emails
           if(isset($usernotification['recieve_email']))
           {
              if($usernotification['recieve_when'] == "daily")
              {
                 $action_sql .= $this -> getRecieveWhat($usernotification['recieve_what']);
              } else if($usernotification['recieve_when'] == "weekly") {
                if(isset($recieve_day[$usernotification['recieve_day']]))
                {
                   $action_sql .= $this -> getRecieveWhat($usernotification['recieve_what']);
                }
              }
              $actions = $this -> dbObj -> mysql_fetch_all("SELECT CONCAT('QA', A.id) AS action_ref, 
                                                            A.action AS query_action,A.deliverable,  
                                                            A.timescale, A.deadline, A.remindon,A.action_on,
                                                            CONCAT(A.progress, '%') AS progress, A.action_owner, A.status, 
                                                            A.action_status, A.attachement, A.risk_id 
                                                            FROM ".$this -> dbObj -> getDBRef()."_actions A
                                                            WHERE A.active=1 $action_sql 
                                                         ");  
              if(!empty($actions))
              {
                 foreach($actions as $a_index => $action)
                 {
                    if(isset($users[$action['action_owner']]))
                    {
                      $action['action_owner']   = $users[$action['action_owner']]['name'];
                    } else {
                      $action['action_owner']   = $users[$user_id]['name'];
                    }
                    if(isset($statuses[$action['status']]))
                    {
                      $action['action_status']   = $statuses[$action['status']]['name'];
                    } else {
                      $action['action_status']   = "New";
                    }
                    $userActions[$user_id]['actions'][$action['action_ref']] = $action;
                    $userActions[$user_id]['user_data']                      = $userdata;
                 }
              }                                                               
           }                       
        }                                                                                                                                                                                   
      }
      return $userActions;
   }
   
   function getReminderSetting()
   {
      $reminder     = $this -> dbObj -> mysql_fetch_one("SELECT * FROM ".$this -> dbObj -> getDBRef()."_reminders WHERE status = 1");
      $reminder_sql = "";
      if(!empty($reminder))
      {
         $today         = date("d-M-Y");  
         $days          = date("Y-m-d", strtotime("+".$reminder['action_days']." days"));
         $reminder_sql .= " OR (STR_TO_DATE(A.deadline, '%d-%M-%Y') >=  STR_TO_DATE('".$today."', '%d-%M-%Y') 
                                 AND STR_TO_DATE(A.deadline, '%d-%M-%Y') <=  STR_TO_DATE('".$days."', '%d-%M-%Y')
                                )  ";        
      }
      return $reminder_sql;
   }
   
   function getRecieveWhat($recieve_what)
   {    
      $what_str  = "";
      $today     = date("d-M-Y");  
      $dayofweek = date('w');
      $action_in_week_sql = "";
      $todaydate = getdate();
      $weekday   = $todaydate['wday'];
      if(in_array($weekday, array(1, 2, 3, 4, 5)))
      {
        for($i = 1; $i <=5; $i++)
        {
           if($weekday == $i)
           {
              $day                 = date("Y-m-d");
              $action_in_week_sql .= " OR  STR_TO_DATE(A.deadline, '%d-%M-%Y') =  STR_TO_DATE('".$day."', '%d-%M-%Y')  ";
           } elseif($i > $weekday) {
              $days                = $i - $weekday;
              $day                 = date("Y-m-d", strtotime("+".$days." days"));
              $action_in_week_sql .= " OR  STR_TO_DATE(A.deadline, '%d-%M-%Y') =  STR_TO_DATE('".$day."', '%d-%M-%Y')  ";
              //$action_in_week_sql .= " OR A.deadline = ".date("Y-m-d", strtotime("+".$days." days"))." ";
           } elseif($i > $weekday) {
             $days                = $weekday - $i;
             $day                 = date("Y-m-d", strtotime("+".$days." days"));
             //$action_in_week_sql .= " OR A.deadline = ".date("Y-m-d", strtotime("+".$days." days"))." ";
             $action_in_week_sql .= " OR  STR_TO_DATE(A.deadline, '%d-%M-%Y') =  STR_TO_DATE('".$day."', '%d-%M-%Y')  ";
           }
        }
      }
      if($recieve_what == "all_incomplete_actions")
      {
         $what_str = " AND A.status <> 3 AND A.progress <> 100 ";
      } elseif($recieve_what == "due_this_week") {
         $what_str = $action_in_week_sql;
      } elseif($recieve_what == "overdue_or_due_this_week") {
         $what_str = " AND A.status <> 3 AND A.progress <> 100 ".$action_in_week_sql;
      } elseif($recieve_what == "due_today") {
         $what_str = " AND STR_TO_DATE(A.deadline, '%d-%M-%Y') =  STR_TO_DATE('".$today."', '%d-%M-%Y') ";
      } elseif($recieve_what == "overdue_or_due_today") {
         $what_str = " AND STR_TO_DATE(A.deadline, '%d-%M-%Y') <  STR_TO_DATE('".$today."', '%d-%M-%Y') 
                       AND A.status <> 3 AND A.progress <> 100
                     ";
      }
      return $what_str;
   }
   
   function getActionHeaders()
   {
  
     $headernames = $this -> dbObj -> mysql_fetch_all("SELECT * FROM ".$this -> dbObj -> getDBRef()."_header_names 
                                                        WHERE type <> 'query' ");
     $headers = array();
     foreach($headernames as $index => $header)
     {
        if( !in_array($header['name'], array('query_comments', 'action_comments', 'assurance', 'signoff')) )
        {
        $headers[$header['name']] = (!empty($header['client_terminology']) ? $header['client_terminology'] : $header['ignite_terminology']);
        }
     }
     return $headers;
   }
    
	function getStatuses()
	{
	   $response = $this -> dbObj -> mysql_fetch_all("SELECT * FROM ".$this -> dbObj -> getDBRef()."_action_status 
	                                                  WHERE status & 2 <> 2" );
       $results  = array();
       if(!empty($response))
       {
         foreach($response as $index => $status)
         {
            $results[$status['id']] = $status;
         }
       }
	   return $results;
   }    

 
}
?>
  
