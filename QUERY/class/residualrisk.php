<?php
/**
	* @package 	: Residual Risk
	* @author	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class ResidualRisk extends DBConnect
{

	protected $rating_from;
	
	protected $rating_to;
	
	protected $magnitude;
	
	protected $response;
	
	protected $color;

			
	function __construct( $rating_from, $rating_to, $magnitude, $response,  $color )
	{
		$this -> rating_from= trim($rating_from);
		$this -> rating_to 	= trim($rating_to);
		$this -> magnitude  = trim($magnitude);
		$this -> response  	= trim($response);
		$this -> color 		= trim($color);						
		parent::__construct();
	}
			
	function saveResidualRisk()
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"magnitude"   => $this -> magnitude,
						"response"    => $this -> response,
						"color"       => $this -> color,
						"insertuser"    => $_SESSION['tid'],						
						);
		$response = $this -> insert( "residual_exposure" , $insert_data );
		echo $response;//$this -> insertedId();
	}			
		
	function updateResidualRisk( $id )
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"magnitude"   => $this -> magnitude,
						"response"    => $this -> response,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> update( "residual_exposure" , $insert_data, "id=$id " );
		return $response;	
	}	
		
	function getResidualRisk()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_residual_exposure " );
		echo json_encode( $response );
	}
	/**
		Fetches all residual risk , and makes and array with the range group
		@return  array
	**/
	function getResidualRiskRanges()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_residual_exposure " );
		$rangesColor = array();
		foreach($response as $key => $resArray ) 
		{
			$range = "";
			// create a string of the range 
			for( $i = $resArray['rating_from']; $i <= $resArray['rating_to'] ; $i++ ) 
			{
				$range .= $i.","; 
			}
			//create an array with the range and the associated color
			$rangesColor[$resArray['magnitude']] = array( "color" => $resArray['color'], "range"=>rtrim($range,",") );	
		}
		return $rangesColor;
	}
	
	/**
		Fetches all residual risk , and makes and array with the range group
		@return  array
	**/
	function getRiskResidualRanges( $value )
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_residual_exposure " );
		$rangesColor = "";
		foreach($response as $key => $resArray ) 
		{
			//$range = "";
			// create a string of the range 
			for( $i = $resArray['rating_from']; $i <= $resArray['rating_to'] ; $i++ ) 
			{
				if( $i == $value )
				{
					$rangesColor = $resArray['color'];	
				}
				//$range .= $i.","; 
			}
			//create an array with the range and the associated color
			//$rangesColor[$resArray['color']] = array( "from" => $resArray['rating_from'], "to"=>$resArray['rating_to'] );	
		}
		return $rangesColor;
	}	
	
	function getAResidualRisk( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_residual_exposure WHERE id = $id" );
		return $response;
	}

	function updateResidualRiskStatus( $id, $status )
	{
		$updatedata = array(
						'active' 		=> $status,
						"insertuser"    => $_SESSION['tid'],						
		);
		$response = $this -> update( 'residual_exposure', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateResidualRisk( $id )
	{
		$update_data = array(
						'active' 		=> "1",
						"insertuser"    => $_SESSION['tid'],						
		);
		$response = $this -> update( 'residual_exposure', $update_data, "id=$id" );
		echo $response;
	}
}
?>