<?php
/**
	* @package 	: Action assurance
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 , Ignite Assist 
**/
class ActionAssurance extends DBConnect
{
	/**
		@var int
	**/
	protected $id;
	/**
		@var int
	**/
	protected $action_id;
	/**
		@var date
	**/
	protected $date_tested;
	/**
		@var char
	**/
	protected $response;
	/**
		@var int
	**/
	protected $signoff;
	/**
		@var char
	**/
	protected $assurance;
	/**
		@var int
	**/
	protected $attachment;
	/**
		@var char
	**/
	protected $active;
	
	function __construct($action_id, $date_tested, $response, $signoff, $assurance, $attachment, $active )
	{
		$this -> action_id 			= $action_id;
		$this -> date_tested 		= $date_tested;			
		$this -> active 			= $active;
		$this -> response 			= $response;
		$this -> signoff 			= $signoff;	
		$this->attachment 			= $attachment;		
		$this -> assurance			= $assurance;	
		parent::__construct();				
	}
	
	function saveActionAssurance()
	{
		$insert_data['action_id']   = $this -> action_id;
		$insert_data['date_tested'] = $this -> date_tested;			
		$insert_data['response']    = $this -> response;
		$insert_data['signoff'] 	= $this -> signoff;
		$insert_data['attachment']	= $this -> attachment;
		$insert_data['insertuser']  = $_SESSION['tid'];						
		$res                        = $this -> insert('action_assurance', $insert_data);
		if($res > 0)
		{
			return array("error" => false , "text" => "Action assurance saved  successfully");
		} else {
			return array("error" => true, "text" => "There was an error saving action assurance");
		}
	}
	
	function getActionAssurance()
	{
		$response = $this -> get("SELECT * FROM ".$_SESSION['dbref']."_action_assurance 
		                          WHERE action_id= ".$this -> action_id."  " );
		return $response;
	}
	
	function fetchAll($optionSql = "")
	{
	   $results = $this -> get(" SELECT A.id, CONCAT(TK.tkname,' ', TK.tksurname) AS user, A.response, DP.value AS department,
	                             A.date_tested, A.signoff, A.attachment, date_format(NOW(), '%d-%M-%Y %H:%i') AS datetime,
	                             AAS.client_terminology AS actionstatus
	                             FROM #_action_assurance A
	                             INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.insertuser
							     INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
							     LEFT JOIN #_action_assurance_status AAS ON AAS.id = A.signoff
							     WHERE 1 AND A.status & 2 <> 2 $optionSql
	                           ");
        return $results;	                           
	}
	
	
	function getActionAssurances($options = array())
	{
	    $sql = "";
	    if(isset($options['action_id']) && !empty($options['action_id']))
	    {
	       $sql  .= " AND A.action_id = '".$options['action_id']."' ";
	    }
	    $total            = $this -> getTotalActionAssurances($sql);
	    if(isset($options['limit']) && !empty($option['limit']))
	    {
	        $sql = " LIMIT ".$options['start'].",".$options['limit'];
	    }
        $results            = $this -> fetchAll($sql);
		$data               = array();
		$data['total']      = $total;
		//$data['assurances'] = $results;
        $data['assurances'] = array();
        if(!empty($results))
        {
          foreach($results as $assurance_id => $assurance)
          {
            foreach($assurance as $key => $val)
            {
               if($key == "attachment")
               {
                 $data['assurances'][$assurance_id][$key] = Attachment::displayAttachmentOnly($val, 'actionassurance', TRUE);
                 $data['assurances'][$assurance_id]['_attachment'] = Attachment::displayAttachmentList($val, 'actionassurance', TRUE);;
               } else {
                  $data['assurances'][$assurance_id][$key] = $val;
               }
            }
          }
        }		
		return $data;
	}
		
	function getTotalActionAssurances($optionSql = "")
	{
		$result = $this -> getRow("SELECT COUNT(*) AS total
	                               FROM #_action_assurance A
	                               INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.insertuser
							       INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
							       LEFT JOIN #_action_assurance_status AAS ON AAS.id = A.signoff
							       WHERE 1 AND A.status & 2 <> 2 $optionSql
								  ");
		return $result['total'];
	}
		
	function getAssurance($id)
	{
		$response = $this -> getRow("SELECT * FROM #_action_assurance WHERE id = $id");
		return $response;		
	} 
	
	function updateAssurance($id, $update_data, $insert_data)
	{
		$res  = 0; 
		
		if(!empty($update_data))
		{
		   $res += $this -> update('action_assurance', $update_data, "id = ".$id);
		}
		
		if(!empty($insert_data))
		{
		   $res += $this -> insert('action_assurance_logs', $insert_data);
		}
		return $res;
	}	

}
?>
