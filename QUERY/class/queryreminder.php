<?php
class QueryReminder 
{
  private $dbObj;
     
  private $users;
  
  private $legislations;
  
  
  private $legislationOwners = array();
     
  private $emailObj;

  function __construct($dbObj)
  {
     $this -> dbObj = $dbObj;
  }   
 
  function sendReminders($users)
  {
     $queries        = $this -> getReminders();      
     $querySend      = 0;  
     $queryReminders = array();
     $headers        = $this -> getQueryHeaders();          
     $user_access    = array();    
     $userQueries    = array(); 
     foreach($users as $user_id => $user)
     {
	     $diradminObj           = new Administrator($user_id);
	     $user_access[$user_id] = $diradminObj -> getAdminAccess();     
     }        
     if(!empty($queries))
     {
        foreach($queries as $queryId => $query)
        {
             if(!empty($user_access))
             {
                foreach($user_access as $user_id => $sub_ids)
                {
                  if(isset($sub_ids['update']))
                  {
                     if(isset($sub_ids['update'][$query['sub_id']]))
                     {
                       
                        $userQueries[$user_id]['queries'][$query['id']] = $query;
                        $userQueries[$user_id]['user_data']             = $users[$user_id];                    
                     }
                  }                  
                }
             }
        }
     }
     if(!empty($userQueries))
     {
         foreach($userQueries as $user_id => $data)
         {
             $emailObj       = new ASSIST_EMAIL_SUMMARY($data['user_data']['email'], 'Query Assist: Reminder for Query', '', '', '', 'Query Assist', 'Query Reminders', $headers, $data['queries'], "query_deadline_date");
             if($emailObj -> sendEmail())
             {
                 $querySend      += count($data['queries']);
                 $queryReminders[$user_id] = " reminder email sent to ".$data['user_data']['email'];
             }
         }           
     }
     $queryReminders['totalSend'] = $querySend;      
     return $queryReminders;  
  }
  
  function getReminders()
  {
     $today = date("Y-m-d");
     /*
     
     $results = $this -> dbObj -> mysql_fetch_all("SELECT Q.* 
                                                   FROM ".$this -> dbObj -> getDBRef()."_query_register Q
                                                   WHERE Q.remind_on = '".$today."' AND Q.status <> 3 
                                                 ");
     */
     $reminder_setting_sql = $this -> getReminderSetting(); 
     $results = $this -> dbObj -> mysql_fetch_all("SELECT DISTINCT(RR.id) AS query_item, RR.id, 
                                  RR.description AS query_description, 
                                  RR.background AS query_background,
							      RR.monetary_implication, RR.query_deadline_date, RR.risk_detail, RRT.name AS risk_type,
							      RR.finding, RR.recommendation, RR.client_response, RR.query_reference, RR.client_response,
							      RR.internal_control_deficiency, RR.auditor_conclusion, RR.query_date,
							      FE.name AS financial_exposure, RL.name AS risk_level, RC.name as query_category, 
							      RT.name as query_type, RT.shortcode as type_code, 
							      CONCAT(D.dirtxt, ' - ', DS.subtxt) AS query_owner, RR.sub_id, RS.name AS query_status,
							      RS.color AS query_status_color, CONCAT(FC.start_date, ' - ', FC.end_date) AS financial_year, 
							      RR.attachment, QS.name AS query_source
							      FROM ".$this -> dbObj -> getDBRef()."_query_register RR 
							      LEFT JOIN ".$this -> dbObj -> getDBRef()."_financial_years FC ON FC.id = RR.financial_year
							      LEFT JOIN ".$this -> dbObj -> getDBRef()."_categories RC ON RC.id = RR.category
							      LEFT JOIN ".$this -> dbObj -> getDBRef()."_types RT ON RT.id = RR.type
							      LEFT JOIN ".$this -> dbObj -> getDBRef()."_query_source QS ON QS.id = RR.query_source
							      LEFT JOIN ".$this -> dbObj -> getDBRef()."_risk_types RRT ON RRT.id = RR.risk_type
							      LEFT JOIN ".$this -> dbObj -> getDBRef()."_status RS ON RS.id = RR.status		
							      LEFT JOIN ".$this -> dbObj -> getDBRef()."_level RL ON RL.id = RR.risk_level	
							      LEFT JOIN ".$this -> dbObj -> getDBRef()."_financial_exposure FE ON FE.id = RR.financial_exposure
							      LEFT JOIN ".$this -> dbObj -> getDBRef()."_dir_admins DA ON DA.ref = RR.sub_id
							      LEFT JOIN ".$this -> dbObj -> getDBRef()."_dirsub DS ON DS.subid = RR.sub_id
							      LEFT JOIN ".$this -> dbObj -> getDBRef()."_dir D ON D.dirid = DS.subdirid
							      WHERE
							      ((RR.active & ".QUERY::ACTIVE.") = ".QUERY::ACTIVE.") AND 
							      ((RR.active & ".QUERY::ACTIVATED.") = ".QUERY::ACTIVATED.") AND 
							      ((RR.active & ".QUERY::DELETED.") <> ".QUERY::DELETED.") 
							      AND RR.remind_on = '".$today."' AND RR.status <> 3 
							      $reminder_setting_sql
							    ");                
							    
							                
     $queryList = array();
     foreach($results as $index => $query)
     {
         $queryList[$query['id']] = $query;
     }
     return $queryList;
  }

   function getQueryHeaders()
   {
    
     $headernames = $this -> dbObj -> mysql_fetch_all("SELECT * FROM ".$this -> dbObj -> getDBRef()."_header_names 
                                                        WHERE type = 'query' ");
     $headers = array();
     foreach($headernames as $index => $header)
     {
        $headers[$header['name']] = (!empty($header['client_terminology']) ? $header['client_terminology'] : $header['ignite_terminology']);
     }
     unset($headers['attachements']);
     unset($headers['query_progress']);
     return $headers;
      
   }
   
   function getReminderSetting()
   {
      $reminder     = $this -> dbObj -> mysql_fetch_one("SELECT * FROM ".$this -> dbObj -> getDBRef()."_reminders WHERE status = 1");
      $reminder_sql = "";
      if(!empty($reminder))
      {
         $today         = date("d-M-Y");  
         $days          = date("Y-m-d", strtotime("+".$reminder['query_days']." days"));
         $reminder_sql .= " OR (STR_TO_DATE(RR.query_deadline_date, '%d-%M-%Y') >=  STR_TO_DATE('".$today."', '%d-%M-%Y') 
                                 AND STR_TO_DATE(RR.query_deadline_date, '%d-%M-%Y') <=  STR_TO_DATE('".$days."', '%d-%M-%Y')
                                )  ";        
      }
      return $reminder_sql;
   }
     
      
}
?>
