<?php
/**
	* @package 	: Search
	* @author 	: admire<azinamo@gmail.com>
	* @copyrite : Ignite assit
**/
class Search extends DBConnect
{
	/**
		Search string
	**/
	protected $searchText;


	function __construct()
	{
		parent::__construct();
		$this -> searchText = (isset($_POST['searchtext']) ? $_POST['searchtext'] : "" );
	}


    function searchText()
    {
        $fields = $this -> _getFields();
        $sql = "";
        foreach( $fields as $table => $field)
        {
            foreach( $field as $i => $val){
                 $sql .= " OR ".$table.".".$val." = '".$this->searchText."' \r\n";
            }
        }
        $nm 			= new Naming();
        $headers 		= $nm -> getNamingConversion();
		$colored  		= array(
								"query_status_color"          => "query_status",
								"impact_rating" 			  => "IMrating",
								"likelihood_rating" 		  => "LKrating",
								);

		$response = $this -> get( "
							SELECT
							DISTINCT(QR.id) AS query_item,
							QR.description AS query_description,
							QR.background AS query_background,
							QR.monetary_implication,
							QR.risk_detail,
							RRT.name AS risk_type,
							QR.finding,
							QR.recommendation,
							QR.client_response ,
							QR.query_reference,
							QR.query_date,
							FE.name AS financial_exposure,
							RL.name AS risk_level,
							RC.name as query_category,
							RC.description as cat_descr,
							RT.name as query_type,
							RT.shortcode as type_code,
							D.dirtxt  AS query_owner,
							QR.sub_id,
							RS.name AS query_status,
							RS.color AS query_status_color
							FROM ".$_SESSION['dbref']."_query_register QR
							LEFT JOIN  ".$_SESSION['dbref']."_actions A ON A.risk_id = QR.id
							LEFT JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = QR.category
							LEFT JOIN ".$_SESSION['dbref']."_types RT ON RT.id = QR.type
							LEFT JOIN ".$_SESSION['dbref']."_risk_types RRT ON RRT.id = QR.risk_type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = QR.status
							LEFT JOIN ".$_SESSION['dbref']."_level RL ON RL.id = QR.risk_level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = QR.financial_exposure
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= QR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = QR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							WHERE QR.active = 1
							AND 
							( 
							  RC.name  LIKE '%".$this->searchText."%'
							  OR RT.name  LIKE '%".$this->searchText."%'
							  OR RRT.name  LIKE '%".$this->searchText."%'
							  OR RS.name LIKE '%".$this->searchText."%'
							  OR RL.name LIKE '%".$this->searchText."%'
							  OR FE.name LIKE '%".$this->searchText."%'
							  OR DS.subtxt LIKE '%".$this->searchText."%'
							  OR D.dirtxt LIKE '%".$this->searchText."%'
							  OR QR.description LIKE '%".$this->searchText."%'
							  OR QR.background LIKE '%".$this->searchText."%'
							  OR QR.monetary_implication LIKE '%".$this->searchText."%'
							  OR QR.risk_detail LIKE '%".$this->searchText."%'
							  OR QR.finding LIKE '%".$this->searchText."%'
							  OR QR.recommendation LIKE '%".$this->searchText."%'
							  OR QR.client_response LIKE '%".$this->searchText."%'
							  OR QR.query_reference LIKE '%".$this->searchText."%'
							  OR QR.query_date LIKE '%".$this->searchText."%'
							)
						");
		$riskArray = array();
		foreach( $response as $key => $risk )
		{
			//$updateArray 		= $this -> getRiskUpdate( $risk['query_item'] );
			foreach($headers as $field => $value)
			{
				if( isset( $risk[$field]) || array_key_exists($field, $risk))
				{
					$riskHeaders[$field]  					= $value;
					$riskArray[$risk['query_item']][$field] =  (in_array($field, array_keys($colored)) ? "<span style='width:100px; background-color:#".$risk[$field]."'>".(isset($risk[$colored[$field]]) ? $risk[$colored[$field]] : "")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>" : $risk[$field]);
				}
			}
		}
        return array("riskData" => $riskArray, "total" => count($riskArray) , "headers" => $riskHeaders);
    }

    function _getFields()
    { 
 	
        $financialExposureFields= $this->describe( "financial_exposure");
        $riskLevelFields        = $this->describe( "level");
        $statusFields           = $this->describe( "status");
        $riskTypeFields         = $this->describe( "risk_types");
        $typesFields            = $this->describe( "types");
        $categoryFields         = $this->describe( "categories");
        $queryFields            = $this->describe( "query_register");
        $actionFields           = $this->describe( "actions");
        
        $fields = array(
                        "QR"    => $queryFields,
                        "A"     => $actionFields,
                        "RC"    => $categoryFields,
                        "RT"    => $typesFields,
                        "RRT"   => $riskTypeFields,
                        "RS"    => $statusFields,
                        "RL"    => $riskLevelFields,
                        "FE"    => $financialExposureFields
                        );
        return $fields;
    }

    function _advancedFields()
    {
        $fields = $this -> _getFields();
        $tableFields = array();
        foreach( $fields['QR'] as $table => $val)
        {
                foreach( $_REQUEST['formdata'] as $key => $nameValue){
                    if($nameValue['name'] == $val)
                    {
                        if( trim($nameValue['value']) !== ""){
                            $tableFields['QR'][$val] = $nameValue['value'];
                       }
                    }
                }
        }
		//print "<pre>";
			//print_r($tableFields);
		//print "</pre>";
        return $tableFields;
    }


    function advancedSearch()
    {

    	$sql = "";
		foreach( $_REQUEST['formdata'] as $key => $keyValue){
			if($keyValue['value'] != ""){
				$sql .= "QR.".$keyValue['name']." = ".$keyValue['value']." AND ";			
			}
		}
		$sql = rtrim($sql , "AND ");
		if($sql == ""){
			$sql = "";
		} else {
			$sql = " AND ".$sql;			
		}
        $nm 			= new Naming();
        $headers 		= $nm -> getNamingConversion();
		$colored  		= array(
								"query_status_color"                => "query_status",
								"impact_rating" 			  => "IMrating",
								"likelihood_rating" 		  => "LKrating",
								);
		$response = $this -> get( "
							SELECT
							DISTINCT(QR.id) AS query_item,
							QR.description AS query_description,
							QR.background AS query_background,
							QR.monetary_implication,
							QR.risk_detail,
							RRT.name AS risk_type,
							QR.finding,
							QR.recommendation,
							QR.client_response ,
							QR.query_reference,
							QR.query_date,
							FE.name AS financial_exposure,
							RL.name AS risk_level,
							RC.name as query_category,
							RC.description as cat_descr,
							RT.name as query_type,
							RT.shortcode as type_code,
							D.dirtxt  AS query_owner,
							QR.sub_id,
							RS.name AS query_status,
							RS.color AS query_status_color
							FROM ".$_SESSION['dbref']."_query_register QR
							LEFT JOIN  ".$_SESSION['dbref']."_actions A ON A.risk_id = QR.id
							LEFT JOIN  ".$_SESSION['dbref']."_categories RC ON RC.id = QR.category
							LEFT JOIN ".$_SESSION['dbref']."_types RT ON RT.id = QR.type
							LEFT JOIN ".$_SESSION['dbref']."_risk_types RRT ON RRT.id = QR.risk_type
							LEFT JOIN ".$_SESSION['dbref']."_status RS ON RS.id = QR.status
							LEFT JOIN ".$_SESSION['dbref']."_level RL ON RL.id = QR.risk_level
							LEFT JOIN ".$_SESSION['dbref']."_financial_exposure FE ON FE.id = QR.financial_exposure
							LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref 	= QR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = QR.sub_id
							LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
							WHERE QR.active = 1
							$sql
							" );												
		$riskArray 	  = array();
		$riskHeaders  = array();
		foreach( $response as $key => $risk )
		{
			//$updateArray 		= $this -> getRiskUpdate( $risk['query_item'] );
			foreach($headers as $field => $value)
			{
				if( isset( $risk[$field]) || array_key_exists($field, $risk))
				{
					$riskHeaders[$field]  					= $value;
					$riskArray[$risk['query_item']][$field] =  (in_array($field, array_keys($colored)) ? "<span style='width:100px; background-color:#".$risk[$field]."'>".(isset($risk[$colored[$field]]) ? $risk[$colored[$field]] : "")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>" : $risk[$field]);
				}
			}
		}
        return array("riskData" => $riskArray, "total" => count($riskArray), "headers" => $riskHeaders);
    }
}
?>