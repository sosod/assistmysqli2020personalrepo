<?php
class ActionReminder
{
   private $dbObj;  
   
   private $emailObj;
     
   function __construct($db, $emailObj)
   {
     $this -> dbObj    = $db;
     $this -> emailObj = $emailObj;
   }     
   
   function sendReminders($users)
   {
        $actions = $this -> getReminders();  
        $headers = $this -> getActionHeaders(); 

        $statusObj = new ActionStatus();
        $statuses  = $statusObj -> getStatus();

        $reminderSent = 0; 
        $sendTo       = array();
        if(!empty($actions))
        {
          foreach($actions as $index => $val)
          {
             //if action owner exists , then send the email
             if(isset($users[$val['owner']]))
             {
                //set variables
                $emailStr = "";
                $emailStr .= $headers['id'].": ".$val['id']."\r\n\n";
                $emailStr .= $headers['action']." : ".$val['action']."\r\n\n";
                $emailStr .= $headers['deliverable']." : ".$val['deliverable']."\r\n\n";
                $emailStr .= $headers['progress']." : ".(int)$val['progress']."%"."\r\n\n";
                $emailStr .= $headers['status']." : ".(isset($statuses[$val['status']]) ? $statuses[$val['status']]['name'] :  "New")."\r\n\n";
                $emailStr .= $headers['deadline']." : ".$val['deadline']."\r\n\n";
                /* page through each action and add to table */
			
                $emailStr.="\r\nTo see all details please log onto Ignite Assist.";	

                $this ->  emailObj -> setRecipient("anesuzina@gmail.com");
                //$this ->  emailObj -> setRecipient($users[$val['owner']]['email']);					//REQUIRED
                $this ->  emailObj -> setSubject("Reminder for Query Action ".$val['id']);				//REQUIRED
                $this ->  emailObj -> setBody($emailStr);			//REQUIRED
                //send the email
                if($this ->  emailObj ->sendEmail())
                {
                    $reminderSent         += 1;
                    $sendTo[$reminderSent] = " reminder send to ".$users[$val['owner']]['email']." for action ".$val['id'];
                }             
             }
          }   
        }
        $sendTo['totalSend'] = $reminderSent;      
        return $sendTo;
   }
   
   function getReminders()
   { 
      $today = date("d-M-Y");        
      $actions = $this -> dbObj -> mysql_fetch_all("SELECT * FROM ".$this -> dbObj -> getDBRef()."_action 
                                                    WHERE STR_TO_DATE(remindon, '%d-%M-%Y') =  STR_TO_DATE('".$today."', '%d-%M-%Y')
                                                    AND active  <> 0 AND status <> 3 ");    
     return $actions;
     
   }
   
   function getActionHeaders()
   {
     $headernames = $this -> dbObj -> mysql_fetch_all("SELECT * FROM ".$this -> dbObj -> getDBRef()."_header_names 
                                                        WHERE type = 'action' ");
     $headers = array();
     foreach($headernames as $index => $header)
     {
        $headers[$header['name']] = (!empty($header['client_terminology']) ? $header['client_terminology'] : $header['ignite_terminology']);
     }
     return $headers;
   }
     
}
?>
  
