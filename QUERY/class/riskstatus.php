<?php
/**
	* Risk Statuses
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist , Risk Assiat Module

**/
class RiskStatus extends DBConnect
{

	protected $risk_status;
	
	protected $client_term;
	
	protected $color;

	function __construct( $risk_status, $client_term, $color)
	{
		$this -> risk_status = trim($risk_status);
		$this -> client_term = trim($client_term);
		$this -> color 		 = trim($color);
		parent::__construct();
	}
	
	function saveStatus()
	{
		$insert_data = array( 
						"name" 				 => $this -> risk_status,
						"client_terminology" => $this -> client_term,
						"color" 			 => $this -> color ,
						"insertuser" 		 => $_SESSION['tid'],						
						);
		$response = $this -> insert( "status" , $insert_data );
		$id = $response;//$this -> insertedId();
		return 	$id;	
	}
	
	function getStatus()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_status WHERE active & 2 <> 2" );
		return $response;		
	}

	function getAStatus( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_status WHERE id = '".$id."'" );
		return $response;		
	}

	function updateStatus( $id )
	{
		$insertdata = array(
						"name" 				 => $this -> risk_status,
						"client_terminology" => $this -> client_term,
						"color" 			 => $this -> color,
						"insertuser" 		 => $_SESSION['tid'],						
		);
		$logsObj = new Logs();
        $data    = $this->getAStatus( $id );
        $logsObj -> setParameters( $_POST, $data, "status");			
		$response = $this -> update( "status", $insertdata , "id=$id");
		return $response;
	}

	function changeStatus( $id , $status)
	{
		$updatedata = array(
						'active' 		 => $status,
						"insertuser" 	 => $_SESSION['tid'],						
						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
        $logsObj -> setParameters( $_POST, $data, "status");		
		$response = $this -> update( 'status', $updatedata, "id=$id" );
		return $response;
	}
	
	function activateStatus( $id )
	{
		$update_data = array(
						'active'			 => "1",
						"insertuser" 		 => $_SESSION['tid'],						
		);
        $logsObj = new Logs();
        $data    = $this->getAStatus( $id );
        $logsObj -> setParameters( $_POST, $data, "status");			
		$response = $this -> update( 'status', $update_data, "id=$id" );
		echo $response;
	}
	
	function getReportList() {
		return $this->getStatus();
	}
}
?>