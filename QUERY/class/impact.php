<?php
/**
	* @package 	: Imapct
	* @author	: admire<azinamo@gmail.com>
	* @copyright: 2011 , Ignite Assist
**/
class Impact extends DBConnect
{
		protected $rating_from;
		
		protected $rating_to;
		
		protected $assessment;
		
		protected $definition;
		
		protected $color;
		
	function __construct( $rating_from, $rating_to, $assessment, $definition,  $color )
	{
		$this -> rating_from = trim($rating_from);
		$this -> rating_to 	 = trim($rating_to);
		$this -> assessment  = trim($assessment);
		$this -> definition  = trim($definition);
		$this -> color 		 = trim($color);						
		parent::__construct();
	}
			
	function saveImpact()
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"assessment"  => $this -> assessment,
						"definition"  => $this -> definition,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> insert( "impact" , $insert_data );
		echo $response;//$this -> insertedId();
	}		
		
	function updateImpact( $id )
	{
		$insert_data = array( 
						"rating_from" => $this -> rating_from,
						"rating_to"   => $this -> rating_to,
						"assessment"  => $this -> assessment,
						"definition"  => $this -> definition,
						"color"       => $this -> color,
						"insertuser"  => $_SESSION['tid'],						
						);
		$response = $this -> update( "impact" , $insert_data, "id=$id" );
		return $response;		
	}	
			
	function getImpact()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_impact WHERE  1 " );
		return $response;
	}
	
	function getActiveImpact()
	{
		$response = $this -> get( "SELECT * FROM ".$_SESSION['dbref']."_impact WHERE active = 1 " );
		return $response;
	}
	
	
	function getAImpact( $id )
	{
		$response = $this -> getRow( "SELECT * FROM ".$_SESSION['dbref']."_impact WHERE id = '".$id."'" );
		return $response;
	}
	
	
	function getImpactRating( $id )
	{
		$response = $this -> get( "SELECT rating_from, rating_to FROM ".$_SESSION['dbref']."_impact WHERE id = '".$id."' " );
		$diff = $to = $from = 0;
		foreach($response as $row ) {
			$diff 	= $row['rating_to'] - $row['rating_from'];
			$to 	= $row['rating_to'];
			$from 	= $row['rating_from'];
		} 
		
		echo json_encode( array( "from" => $from, "to" => $to , "diff" => $diff) );
	}	

	function updateImpactStatus( $id , $status)
	{
		$updatedata = array(
						'active'	 =>  $status,
						"insertuser" => $_SESSION['tid'],						
		);
		$response = $this -> update( 'impact', $updatedata, "id=$id" );
		echo $response;
	}
	
	function activateImpact( $id )
	{
		$update_data = array(
						'active' 		=> "1",
						"insertuser"    => $_SESSION['tid'],						
		);
		$response = $this -> update( 'impact', $update_data, "id=$id" );
		echo $response;
	}
}
?>