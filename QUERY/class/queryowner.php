<?php
/**
	* Risk types for the risk module
	* @author 	: admire<azinamo@gmail.com>
	* @copyright: 2011 Ignite Assist
**/
class QueryOwner extends DBConnect {
	protected $name;
	protected $sort;
	protected $active;
	protected $dirid;
	
	const ACTIVE = 1;
	const DELETED = 2;
	const INACTIVE = 0;
	
	function __construct( $text,$dirid=0,$active=1,$sort=99)
	{
		$this->name = $text;
		$this->active = $active;
		$this->sort = $sort;
		$this->dirid = $dirid;
		parent::__construct();
	}
	
	function saveDir()
	{
		$insert_data = array( 
						"dirtxt"   => $this -> name,
						"active"		  => $this -> active,
						"dirsort" => $this -> sort,
						);
		$response = $this -> insert( "dir" , $insert_data );
		return $response;//$this -> insertedId();
	}

	function saveOwner()
	{
		$insert_data = array( 
						"subtxt"   => $this -> name,
						"active"		  => $this -> active,
						"subsort" => $this -> sort,
						"subdirid" => $this -> dirid,
						);
		$response = $this -> insert( "dirsub" , $insert_data );
		return $response;//$this -> insertedId();
	}
	
	
	
}
?>