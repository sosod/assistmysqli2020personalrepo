<?php
/**
	* @package 		:  Risk Assurance 
	* @author 		: admire<azinamo@gmail.com>
	* @copyright	: 2011 Ignite Assist	
**/
class RiskAssurance extends DBConnect
{
	/**
		@var int
	**/
	protected $id;
	/**
		@var int
	**/
	protected $risk_id;
	/**
		@var date
	**/
	protected $date_tested;
	/**
		@var char
	**/
	protected $response;
	/**
		@var int
	**/
	protected $signoff;
	/**
		@var char
	**/
	protected $assurance_provider;
	/**
		@var int
	**/
	protected $attachment;
	/**
		@var char
	**/
	protected $active;
	
	function __construct( $risk_id, $date_tested, $response, $signoff, $assurance_provider, $attachment )
	{
		$this -> risk_id 			= $risk_id;
		$this -> date_tested 		= $date_tested;			
		$this -> response 			= $response;
		$this -> signoff 			= $signoff;			
		$this -> assurance_provider = $assurance_provider;	
		$this->attachment 			= $attachment;
		parent::__construct();				
	}
		
	
	function getAssurance($id)
	{
		$response = $this -> getRow("SELECT * FROM #_assurance WHERE id = $id");
		return $response;		
	} 		
    function fetchAll($option_sql = "")
    {
		$results  = $this -> get("SELECT A.id, CONCAT(TK.tkname,' ', TK.tksurname) AS user, A.response,
								   DP.value AS department, A.date_tested, A.signoff, A.attachment, 
								   date_format(NOW(), '%d-%M-%Y %H:%i') AS datetime, AAS.client_terminology AS actionstatus
								   FROM ".$_SESSION['dbref']."_assurance A
								   INNER JOIN  assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.insertuser
								   INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
								   LEFT JOIN #_query_assurance_status AAS ON AAS.id = A.signoff
								   WHERE risk_id = '".$this -> risk_id."'
								   AND A.status & 2 <> 2 $option_sql
								   ");
        return $results;								   
    }		
    
	function getTotalQueryAssurances($optionSql = "")
	{
		$result = $this -> getRow("SELECT COUNT(*) AS total
	                               FROM #_assurance A
	                               INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = A.insertuser
							       INNER JOIN assist_".$_SESSION['cc']."_list_dept DP ON DP.id = TK.tkdept
							       LEFT JOIN #_query_assurance_status AAS ON AAS.id = A.signoff
							       WHERE 1 AND A.status & 2 <> 2 $optionSql
								  ");
		return $result['total'];
	}    
    
    function getQueryAssurance($options = array())
    {
       $sql = "";
       if(isset($options['limit']) && !empty($options['limit']))
       {
          $sql .= " LIMIT ".(int)$options['start'].",".$options['limit']; 
       }
	   $total            = $this -> getTotalQueryAssurances($sql);
	   if(isset($options['limit']) && !empty($option['limit']))
	   {
	     $sql = " LIMIT ".$options['start'].",".$options['limit'];
	   }
       $results            = $this -> fetchAll($sql);
	   $data               = array();
	   $data['total']      = $total;
	   $data['assurances'] = array();
	   if(!empty($results))
	   {
	      foreach($results as $assurance_id => $assurance)
	      {
	        foreach($assurance as $key => $val)
	        {
	           if($key == "attachment")
	           {
	             $data['assurances'][$assurance_id][$key] = Attachment::displayAttachmentOnly($val, 'qassurance', TRUE);
	             $data['assurances'][$assurance_id]['_attachment'] = Attachment::displayAttachmentList($val, 'qassurance', TRUE);;
	           } else {
	              $data['assurances'][$assurance_id][$key] = $val;
	           }
	        }
	      }
	   }
	   
	  return $data;
    }
		
	function saveQueryAssurance()
	{
		$insertdata['risk_id'] 			= $this -> risk_id;
		$insertdata['date_tested'] 		= $this -> date_tested;
		$insertdata['response']			= $this -> response;
		$insertdata['signoff']			= $this -> signoff;
		$insertdata['attachment']		= $this -> attachment;
		$insertdata['insertuser']  		= $_SESSION['tid'];
		$response  = $this -> insert('assurance', $insertdata);
		if($response > 0)
		{
		    $response = array("text" => "Query assurance saved", "error" => false);
		} else {
			$response = array("text" => "Error saving query assurance", "error" => true);
		}
		return $response;
	}
	
	function updateAssurance($id, $update_data, $insert_data)
	{
		$res  = 0; 
		
		if(!empty($update_data))
		{
		   $res += $this -> update('assurance', $update_data, "id = ".$id);
		}
		
		if(!empty($insert_data))
		{
		   $res += $this -> insert('assurance_logs', $insert_data);
		}
		return $res;
	}	

}
