<?php
class Attachments
{

	private $files = array();
	
	//whether we are uploading actions or queries
	private $uploadtype = "";
	
	private $key 		= ""; 
	
	private $uploadDir   = "";
	
	function __construct( $files, $uploadtype)
	{
		$this->files 		= $files;		
		if(is_array($this->files) && !empty($this->files)){
			$this->key 			= key($this->files);
		}
		$this->uploadtype   = $uploadtype;
	}

	function upload( $attachmentname = "" )
	{	
		$valid = $this->validFile();
		if($valid == "")
		{
			$attName = "";
			if( $attachmentname == "")
			{
				$attName = $this->uploadtype; 
			} else {
				$attName = $attachmentname;
			}	

			$this->_checkDir();
			$file 		 = $this->files[$this->key]['name'];
			$tmp 		 = $this->files[$this->key]['tmp_name'];
			$error 		 = $this->files[$this->key]['error'];
			$type 		 = $this->files[$this->key]['type'];
			$dir 		 = ""; 
			$extenstion  = substr($file, strpos($file, ".")+1); 
			$filename 	 = $attName."_".date("YmdHis")."_".$_SESSION['tid'].".".$extenstion;
			$destination = $this->uploadDir."/".$filename;
			$_SESSION['uploads'][$attName][$filename] = $file;

			
			if( !move_uploaded_file($tmp, $destination) ) {
				unset($_SESSION['uploads'][$attName][$filename]);
				echo json_encode( array("text" => "Error uploading the file", "error" => true ) );
			} else {
				$otherFiles = array();
				foreach( $_SESSION['uploads'][$attName] as $index => $file)
				{
					if( file_exists($this->uploadDir."/".$index))
					{
						$otherFiles[$index]  = $file;
					}	
				}
				
				echo json_encode( array("text"  => "Uploaded Successfull \r\n",
									    "error" => false,
										"file"  => $file,
										"other" => $otherFiles, //$_SESSION['uploads'][$attName],
										"total" => count( $otherFiles ) //$_SESSION['uploads'][$attName]
									  )
								); 
			}
		} else {
			echo json_encode( array("text" => $valid, "error" => true) );		
		}
	}
	
	
	function validFile()
	{
		$error = "";
		switch ($this->files[$this->key]['error'])
		{
			case UPLOAD_ERR_OK:
			break;
			case UPLOAD_ERR_INI_SIZE:
				$error = "Upload size exceed the maximum set";
			break;
			case UPLOAD_ERR_FORM_SIZE:
				$error = "Upload size exceed the maximum set in the form";
			break;
			case UPLOAD_ERR_PARTIAL:
				$error = "The uploaded file was only partial";
			break;
			case UPLOAD_ERR_NO_FILE:
				$error = "No file was uploaded";
			break;
			case UPLOAD_ERR_CANT_WRITE:
				$error = "Failed to write to disk ";
			break;
			default:
			break;
		}
		return $error;
	}
	
	function remove( $filename, $ogname, $attchmentname = "" )
	{		
		$this->_checkDir();
		$attName = "";
		if( $attchmentname == "")
		{
			$attName = $this->uploadtype; 
		} else {
			$attName = $attchmentname;
		}					
		$destination = $this->uploadDir;
		$oldname = $this->uploadDir."/".$filename;
		$newname = $this->uploadDir."/deleted/".$filename;		
		$response   = array(); 
		if( rename( $oldname, $newname) ) {
			$_SESSION['uploads'][$attName][$filename]  = $ogname;
			$response = array("error" => false, "text" => $ogname." removed");
		} else {
			$response = array("error" => true, "text" => "Error removing file . . . ");	
		}
		echo json_encode( $response );	
	}
	
	function deletedFile( $filename, $ogname )
	{
		$this->_checkDir();
		$oldname = $this->uploadDir."/".$filename;
		$newname = $this->uploadDir."/deleted/".$filename;
		$response = array(); 
		if( rename($oldname, $newname))
		{
			$response = array("text" => "File successfully removed", "error" => false );
			unset( $_SESSION['uploads'][$this->uploadtype][$filename] ); 
		} else {
			$response = array("text" => "Error removing file", "error" => true );
		} 
		echo json_encode( $response );	
	}
		
	//check if the directories to upload the files are available 
	function _checkDir()
	{
		if(!is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']))
		{
			if( mkdir( "../../files/".$_SESSION['cc']."/".$_SESSION['modref'] )){
				if( mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype))
				{			
					mkdir( "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype."/deleted" );
				}
			}	
		} else {
			if( !is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype)) {
				if( mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype))
				{			
					mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype."/deleted");
				}				
			}
		}				
		if( is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype)){
			$this->uploadDir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$this->uploadtype;
		}
	}
	
}
?>