<?php
//error_reporting(-1);
if( ! ini_get('date.timezone') )
{
   date_default_timezone_set('GMT');
} 
?>
<html>
<head>
<script type="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery.min.js"></script>
<script type="text/javascript" src="/library/jquery-ui-1.8.24/js/jquery-ui.min.js"></script>

<script type="text/javascript" src="/library/js/assiststring.js"></script>
<script type="text/javascript" src="/library/js/assisthelper.js"></script>
<script type="text/javascript" src="/library/js/assistform.js"></script>

<script type="text/javascript" src="/library/jquery/js/common.js"></script>
<link rel="stylesheet" href="/library/jquery-ui-1.8.24/css/jquery-ui.css" type="text/css">
<link rel="stylesheet" href="/assist.css" type="text/css">
<?php
//require_once ("../../inc_assist.php");
include_once("inc_ignite.php");
@session_start();
include_once("init.php");
$me = new ASSIST_MODULE_HELPER();
$db 	 = new DBConnect();
$default = new Defaults("","");
$admire_helper = new ADMIRE_ASSIST_HELPER();

$defaultScripts = array('json2.js');
$defaultStyles 	= array('styles.css');
$scripts = isset($scripts) ? $scripts : array();
$styles = isset($styles) ? $styles : array();
$scripts = array_merge($defaultScripts,$scripts);
$styles = array_merge($defaultStyles,$styles);
foreach( $scripts as $script){
	$script .= '?' . time();
?>
	<script type=text/javascript src="../js/<?php echo trim($script); ?>"></script>
<?php
} 
foreach( $styles as $sty) {
 ?>
 	<link rel="stylesheet" type="text/css" href="../css/<?php echo trim($sty); ?>" />
<?php
}
 ?>
<script type=text/javascript>
  $(document).ready(function(){
	$(".datepicker").datepicker({
		showOn		  : "both",
		buttonImage 	  : "/library/jquery/css/calendar.gif",
		buttonImageOnly  : true,
		changeMonth	  : true,
		changeYear	  : true,
		dateFormat 	  : "dd-M-yy",
		altField		  : "#startDate",
		altFormat		  : 'd_m_yy'
	});		

	$(".datepicker").datepicker({
		showOn		  : "both",
		buttonImage 	  : "/library/jquery/css/calendar.gif",
		buttonImageOnly  : true,
		changeMonth	  : true,
		changeYear	  : true,
		dateFormat 	  : "dd-M-yy",
		altField		  : "#startDate",
		altFormat		  : 'd_m_yy'
	});	
	
	$(".historydate").datepicker({	
		showOn			: "both",
		buttonImage 	     : "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		: true,
		changeYear		: true,
		dateFormat 		: "dd-M-yy",
		maxDate             : 0
	});	
	
	$(".futuredate").datepicker({	
		showOn			: "both",
		buttonImage 	     : "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		: true,
		changeYear		: true,
		dateFormat 		: "dd-M-yy",
		minDate             : +1
	});		
	
	$("#clear_remind").click(function(e){
		$("#remindon").val("");
		$("#reminder").val("");
		e.preventDefault();
	});		
		
    $("textarea").attr("cols", "80");
    $("textarea").attr("rows", "10");
	$("th").css({"text-align":"left"})
		
	var input = $("input")
	$.each( input, function(){
		if( $(this).hasClass("color") ){
			$(this).attr("size", "8")
		} else if( $(this).attr("size") == ""){
			$(this).attr("size", "30")
		} else  {
			$(this).attr("size", $(this).attr("size") )
		}
		//$("input").attr("size", "30");
	});	
	$(".viewmore").live("click",function(e) {
		//var id = $(this).prop("id");
		var $parentSpan       = $(this).parent();
		var $textSpan = $parentSpan.children("span:first-child");
		var $moreImg = $(this).children("img");
		if(AssistString.stripos($moreImg.attr("src"),"plus")>0) {
			$textSpan.html($parentSpan.attr("more"));
			$moreImg.attr("src","../../pics/minus.gif");
		} else {
			$textSpan.html($parentSpan.attr("less"));
			$moreImg.attr("src","../../pics/plus.gif");
		}
		//alert($textSpan.html()+" :: "+imgSrc);
		return false;
	});
    /*$(".viewmore").live("click", function(e){
       var id               = this.id;
       var parentSpan       = $(this).parent().attr("id");
       var longText         = $(this).attr("title");
       var shortText        = $("#"+parentSpan).text();
       var positionOfMore   = shortText.lastIndexOf("more..."); 
       shortText            = shortText.substr(0, positionOfMore);
       $("#"+parentSpan).html("");
       $("#"+parentSpan).html(longText+" <a href='#' class='less' id='less_"+id+"' style='color:orange;'>less...</a>");
         $("#less_"+id).bind("click", function(e){
           $("#"+parentSpan).html("");
           $("#"+parentSpan).html(shortText+" <a href='#' class='viewmore' id='"+id+"' title='"+longText+"' style='color:orange;'>more...</a>");
           e.preventDefault();
         })
       
       return false;
    });*/
});
</script>
</head>
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<div style="clear:both;" id=page_navigation_buttons>
<?php
	$requestUri  = preg_split('[\\/]', $_SERVER['REQUEST_URI'], -1, PREG_SPLIT_NO_EMPTY	);//explode("/",$_SERVER['REQUEST_URI']);
	$count 		 = count($requestUri);
	$pagename 	 = "";
	$basename    = ""; //foldername used to get the submenu 
    foreach( $requestUri as $key => $val)
	{
		if( $val == $_SESSION['modlocation'])
		{
			continue;
		} else {	
			//if the page has a php extenstion get the basename, 
			if( strpos($val,".") > 0){
				$pagename = substr($val, 0, strpos($val, "."));
				$basename = $requestUri[$count-2];
			} else {
				$basename = $val;
				$pagename = $val;
			}				
		}		
	}

    $menu 		= new Menu();
    $links  	= $menu -> getSubMenu( (($basename == "actions") ? "manage"  : $basename) );
	$user 		= new UserAccess( "", "", "", "", "", "", "", "", "");
	$userInfo 	= $user -> getUser( $_SESSION['tid']);
	$defObj     = new Defaults();
	$assuranceV = $defObj -> getADefault(6);
	
	//print_r($links);
	
if(!empty( $links )){

	$mainMenu = array();
	if(strpos($pagename, ".") > 0){ 
		$page_name = substr($pagename, 0, strpos($pagename, "."));
	} else {
		$page_name = $pagename;
	}

	
	//$page_name   = ($pagename == "manage" ? "view" ? $pagename);
	//(($page_name ==  strtolower(($link['client_name'] == "" ? $link['name'] : $link['client_name'])) )? "Y" : "");
	
	foreach( $links as $link ) {
		$active = "";
		if(strtolower($link['client_name']) == "defaults")
		{
			$user = array("risk_manager", "user_access", "useraccess");
			if(in_array($page_name, $user))
			{
				$page_name = "useraccess";
			} else {
				$page_name = "defaults"; 
			}
		}
		
		if( ($page_name == strtolower($link['client_name'])) ){
			$active = "Y";		
		} else if( $page_name == strtolower($link['folder'])) {
			$active = "Y";
		} else if($page_name == strtolower($link['name'])) {
			$active = "Y";		
		}		
		if( $link['folder'] == "assurance" ){
			if($assuranceV['value'] == 1 && $userInfo['assurance'] == 1){
			//echo $page_name." is active ".(($page_name ==  strtolower(($link['client_name'] == "" ? $link['name'] : $link['client_name'])) )? "Y" : "")."<br /><br />";
			$mainMenu[$link['id']] = array("id" => $link['id'], "link" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php",'active'=> $active,'name'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name']),'help'=>'') ;
			}
		} else {
			if($userInfo['risk_manager'] == 1) {
				$mainMenu[$link['id']] = array("id" => $link['id'], "link" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php",'active'=> $active,'name'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name']),'help'=>'') ;
			} else {
				if($userInfo['view_all'] == 1 && $link['folder'] == "view"){
					$mainMenu[$link['id']] = array("id" => $link['id'], "link" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php", 'active'=> $active,'name'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name']),'help'=>'') ;
				} else if($userInfo['edit_all'] == 1 && $link['folder'] == "edit"){
					$mainMenu[$link['id']] = array("id" => $link['id'], "link" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php", 'active'=> $active,'name'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name']),'help'=>'') ;
				} else if($userInfo['create_risks'] == 1 && $link['folder'] == "index"){
					$mainMenu[$link['id']] = array("id" => $link['id'], "link" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php", 'active'=> $active,'name'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name']),'help'=>'') ;
				} else if($userInfo['create_actions'] == 1 && $link['folder'] == "action"){
					$mainMenu[$link['id']] = array("id" => $link['id'], "link" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php", 'active'=> $active,'name'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name']),'help'=>'') ;
				} else {
					$mainMenu[$link['id']] = array("id" => $link['id'], "link" =>($basename == "actions" ? "../manage/".$link['folder'] : $link['folder']).".php", 'active'=> $active,'name'=> ($link['client_name'] == "" ? $link['name'] : $link['client_name']),'help'=>'') ;
				}
			}

		}
	}
	//echoNavigation(1,$mainMenu);
	echo $me->generateNavigationButtons($mainMenu, 1);// standardization [SD]
	$pages   = array();//array("view", "view_all", "edit", "edit_all", "update", "update_all", "action", "action_all");
	$columns = array("action_columns", "columns");
	
	if( in_array($pagename, $columns))
	{
		$active = "";
		if( $pagename == "columns"){
			$menuT[1] = array('id'=>"query_columns",'link'=>"columns.php",'active'=> "Y",'name'=>"Query Columns",'help'=>'');
			$menuT[2] = array('id'=>"action_columns",'link'=>"action_columns.php",'active'=> "",'name'=>"Action Columns",'help'=>'');
		} else {
			$menuT[1] = array('id'=>"query_columns",'link'=>"columns.php",'active'=> "",'name'=>"Query Columns",'help'=>'');
			$menuT[2] = array('id'=>"action_columns",'link'=>"action_columns.php",'active'=> "Y",'name'=>"Action Columns",'help'=>'');
		}		
		//echoNavigation(2,$menuT);
		echo $me->generateNavigationButtons($menuT, 2);// standardization [SD]
	} elseif(in_array($pagename, $pages)) {
	    $activeY = "";
	    $activeX = "";
	    if(substr($pagename, -3) == "all")
	    {
	       $activeY = "Y";
	    } else {
	       $activeX = "Y";
	    }
	    $page = str_replace("_all", "", $pagename);
        $menuT[2] = array('id'=>"display_mine",'link'=> $page.".php",'active'=> $activeX,'name'=>"Display Mine",'help'=>'');
        $menuT[1] = array('id'=>"display_all",'link'=> $page."_all.php",'active'=> $activeY,'name'=>"Display All",'help'=>'');
		//echoNavigation(2,$menuT);
		echo $me->generateNavigationButtons($menuT, 2);// standardization [SD]
	} elseif(in_array($pagename, array("edit", "edit_actions", "editrisk", "edit_action", "action_edit"))) {
	    $activeY = "";
	    $activeX = "";
	    if($pagename == "edit_actions" || $pagename == "edit_action" || $pagename == "action_edit")
	    {
	       $activeY = "Y";
	    } else {
	       $activeX = "Y";
	    }
        $menuT[2] = array('id'=>"edit_queries",'link'=> "edit.php",'active'=> $activeX,'name'=>"Query",'help'=>'');
        $menuT[1] = array('id'=>"edit_actions",'link'=> "edit_actions.php",'active'=> $activeY,'name'=>"Actions",'help'=>'');
		//echoNavigation(2,$menuT);
		echo $me->generateNavigationButtons($menuT, 2);// standardization [SD]
	} elseif(in_array($pagename, array("update", "update_actions", "updaterisk", "update_action", "action_update"))) {
	    $activeY = "";
	    $activeX = "";
	    if($pagename == "update_actions" || $pagename == "update_action")
	    {
	       $activeY = "Y";
	    } else {
	       $activeX = "Y";
	    }
        $menuT[2] = array('id'=>"update_queries",'link'=> "update.php",'active'=> $activeX,'name'=>"Query",'help'=>'');
        $menuT[1] = array('id'=>"update_actions",'link'=> "update_actions.php",'active'=> $activeY,'name'=>"Actions",'help'=>'');
		//echoNavigation(2,$menuT);
		echo $me->generateNavigationButtons($menuT, 2);// standardization [SD]
	}
	//print_r($menuT);
}

?>
</div>
<div style="clear:both;"><h1 style="text-align:left; clear:left;"><?php

        $allheadingnaming 	= new Naming();
        $allheadingrowNames   = $allheadingnaming -> allLabels();

        function replaceHeadingNameFields( $fieldId , $defaultName ){
            global $allheadingnaming, $allheadingrowNames;
            if( isset( $allheadingrowNames[$fieldId] )) {
                return $allheadingrowNames[$fieldId];
            }	else {
                return $defaultName;
            }
        }

		$friendlyHeads = array(
							"action"				=> "Action",
							"index"					=> "Query",
							"view" 					=> "View",
							"edit" 					=> "Edit",
							"update"				=> "Update",
							"approve"				=> "Approve Action",
							"myprofile"				=> "My Profile", 
							"assurance"				=> "Assurance",
							"editrisk"				=> "Edit Query" ,
							"action_edit" 			=> "Edit Actions",
							"edit_action" 			=> "Edit Action",
							"add_action" 			=> "Add Action",
							"updaterisk" 			=> "Update Query",
							"action_update" 		=> "Update Actions",
							"update_action" 		=> "Update Action",
							"riskapprove"			=> "Approve Action",
							"approve_action" 		=> "Approve Action",
							"action_approve" 		=> "Approve Query Actions",
							"riskassurance" 		=> "Query Assurance",
							"action_assurance" 		=> "Query Action Assurance",
							"assurance_action" 		=> "Action Assurance",
							"menu_heading" 			=> "Menus" ,
							"naming" 				=> "Field Names",
							"financial_year" 		=> replaceHeadingNameFields('financial_year','Financial Year'),
							"risk_status" 			=> replaceHeadingNameFields('query_status','Query Status'),
							"action_status" 		=> replaceHeadingNameFields('action_status','Action Status') ,
							"risk_categories"		=> replaceHeadingNameFields('query_category','Query Category'),
							"impact"				=> "Query Impact",
							"likelihood"			=> "Query Likelihood",
							"inherent_risk"			=> "Inherent Query Exposure",
							"control"				=> "Percieved Control Effectiveness",
							"residual"				=> "Residual Query Exposure",
							"directorate"			=> "Directorate Structure",
							"default_setting"		=> "Default Module Settings",
							"sub_directorate"		=> "Sub Directorate Structure",
							"useraccess" 			=> "User Access Settings",
							"risk_manager" 			=> "Query Manager",
							"user_access" 			=> "User Access Settings",		
							"notifications" 		=> "User Notifications",
							"defaults" 				=> "Module Setup",
							"default" 				=> "Module Setup",
							"edit_risk_status" 		=> "Edit " . replaceHeadingNameFields('query_status','Query Status'),
							"risk_status_status"	=> "Change Query-Status status",
							"edit_action_status"    => "Edit " . replaceHeadingNameFields('action_status','Action Status'),
							"change_action_status"  => "Change " . replaceHeadingNameFields('action_status','Action Status'),
							"edit_risk_type" 		=> "Edit " . replaceHeadingNameFields('risk_type','Risk Type'),
							"edit_risk_category" 	=> "Edit Query Category",
							"change_category_status"=> "Change status of category",
							"edit_impact" 			=> "Edit Impact",
							"edit_likelihood" 		=> "Edit Likelihood",
							"change_impact_status" 	=> "Change Risk Status",
							"change_control_status"	=> "Change Control Effectiveness Status",
							"edit_residual"			=> "Edit Residial Query",
							"change_residual_status"=> "Change Residual Status",
							"edit_directorate"		=> "Edit Directorate",
							"edit_subdirectorate"   => "Edit Sub Directorate",
							"edit_glossary"			=> "Edit Glossary",
							"edit_financial_year"	=> "Edit " . replaceHeadingNameFields('financial_year','Financial Year'),
							"edit_user"				=> "Edit User Settings",
							"udf" 					=> "User Defined Fields(UDF's)",
							"new_action"			=> "Add New Action",
							"advanced_search"		=> "Advanced Search",
							"columns"				=> "Columns to Display",
							"graph"					=> "Graph",
							"fixed"					=> "Fixed Reports",
							"inherentVsResidual" 	=> "Inherent Risk Versus Residual Risk",
							"graphPerType"	 		=> "Inherent Risk Versus Residual Risk Per Type",
							"graphPerCategory" 		=> "Inherent Risk Versus Residual Risk Per Category",
							"graphPerPerson"	 	=> "Inherent Risk Versus Residual Risk Per Person",
							"event"				 	=> "Event Driven Reports",
							"statistics" 			=> "Statistics",
							"generate"				=> "Generate Report",
							"quick"					=> "Quick Report",	
							"edit_risk_manager"	    => "Edit Query Manager",	
							"edit_control"			=> "Edit Control Effectiveness",
							"edit_inhrentrisk"		=> "Edit Inherent Query Exposure",																															
							"setup_dir"				=> "(Sub-)Directorates",
							"setup_dir_add"			=> "Directorates",
							"setup_dir_edit"		=> "Directorates",
							"setup_dir_order"		=> "Directorates",
							"setup_dir_sub_add"		=> "Sub-Directorates",
							"setup_dir_sub_order"	=> "Sub-Directorates",
							"setup_dir_sub_edit"	=> "Sub-Directorates",
							"setup_admin_dir_config"=> "Administrators",
							"financial_exposure"    => replaceHeadingNameFields('financial_exposure','Financial Exposure'),
							"risk_level"			=> replaceHeadingNameFields('risk_level','Risk Level'),
							"edit_risk_level"		=> "Edit " . replaceHeadingNameFields('risk_level','Risk Level'),
							"change_level_status"	=> "Change Level Status" ,
							"edit_financial_exposure" 	=> "Edit " . replaceHeadingNameFields('financial_exposure','Financial Exposure') ,
							"change_financial_exposure" => "Change " . replaceHeadingNameFields('financial_exposure','Financial Exposure'),
                            "change_type_status"   	=> "Change " . replaceHeadingNameFields('risk_type','Risk Type') . " Status",
							"change_like_status"	=> "Change Likelihood status", 
							"change_inherent_status" => "Change Inherent " . replaceHeadingNameFields('query_status','Query Status'),
							"glossary"				 => "Glossary",
							"change_risk_type_status"=> "Change " . replaceHeadingNameFields('risk_type','Risk Type') . " Status",
							"query_vs_department"	 => "Number of queries per department",
							"query_vs_risk_type"	 => "Number of queries per " . strtolower(replaceHeadingNameFields('risk_type','Risk Type')),
							"query_vs_status"		 => "Number of query in each status",
							"action_all"		     => "Action",
							"activate"               => "Activate" ,
							"support"					=> "Support",
							);
		//echo ($basename == "risk" ? "New" : ucwords($basename))." ".($currentpage=="" ? "" : " >>  ".(isset($friendlyHeads[$currentpage]) ? $friendlyHeads[$currentpage] : $currentpage));	
		//echo "<br />Here is it".(isset($friendlyHeads[$currentpage]) ? $friendlyHeads[$currentpage] : $currentpage);

		if($_SERVER['QUERY_STRING'] == "")
		{
			unset($_SESSION['secondMenu']);
			unset($_SESSION['firstMenu']);
			unset($_SESSION['thirdMenu']);
			
			unset($_SESSION['firstName']);
			unset($_SESSION['thirdName']);
			unset($_SESSION['secondName']);
			if($basename == "risk" || $basename == "query")
			{
				$_SESSION['firstName'] =  "New ".(isset($friendlyHeads[$pagename]) ? $friendlyHeads[$pagename] : ($pagename == "risk" ? "Query" : $pagename) ); //($basename == "risk" ? "New" : ucwords($basename))." ".($currentpage=="" ? "" : " >>  ".(isset($friendlyHeads[$currentpage]) ? $friendlyHeads[$currentpage] : $currentpage));//$this->niceName( $requestArr, $otherNames);			
			} else {
				$_SESSION['firstName'] =  (isset($friendlyHeads[$pagename]) ? $friendlyHeads[$pagename] : $pagename); //($basename == "risk" ? "New" : ucwords($basename))." ".($currentpage=="" ? "" : " >>  ".(isset($friendlyHeads[$currentpage]) ? $friendlyHeads[$currentpage] : $currentpage));//$this->niceName( $requestArr, $otherNames);			
			}
			
			$_SESSION['firstMenu'] = $_SERVER['REQUEST_URI'];
			
		} else if(strstr($_SERVER['QUERY_STRING'],"&")){
			$_SESSION['thirdMenu'] = $_SERVER['REQUEST_URI'];			
			$_SESSION['thirdName'] =  (isset($friendlyHeads[$pagename]) ? $friendlyHeads[$pagename] : $pagename); //($basename == "risk" ? "New" : ucwords($basename))." ".($currentpage=="" ? "" : " >>  ".(isset($friendlyHeads[$currentpage]) ? $friendlyHeads[$currentpage] : $currentpage));//$this->niceName( $requestArr, $otherNames);
		} else {
			unset($_SESSION['secondMenu']);
			unset($_SESSION['thirdName']);
			unset($_SESSION['thirdMenu']);
			
			$_SESSION['secondMenu'] = $_SERVER['REQUEST_URI'];	
			$_SESSION['secondName'] = (isset($friendlyHeads[$pagename]) ? $friendlyHeads[$pagename] : $pagename); //($basename == "risk" ? "New" : ucwords($basename))." ".($currentpage=="" ? "" : " >>  ".(isset($friendlyHeads[$currentpage]) ? $friendlyHeads[$currentpage] : $currentpage));//$this->niceName( $requestArr, $otherNames);		
		}
		echo "<a href='".$_SESSION['firstMenu']."' class='breadcrumbs'>".ucwords(str_replace("_", " ",$_SESSION['firstName']))."</a>".(isset($_SESSION['secondMenu']) ? "&nbsp;>>&nbsp;<a href='".$_SESSION['secondMenu']."' class='breadcrumbs'>".ucwords(str_replace("_"," ",$_SESSION['secondName']))."</a>" : "").(isset($_SESSION['thirdMenu']) ? "&nbsp;>>&nbsp;<a href='".$_SESSION['thirdMenu']."' class='breadcrumbs'>".ucwords(str_replace("_", " ", $_SESSION['thirdName']))."</a>" : "");		
		//echo "<br />Here is it".(isset($friendlyHeads[$currentpage]) ? $friendlyHeads[$currentpage] : $currentpage);


//		echo '<pre style="font-size: 18px">';
//		echo '<p>REQUEST URI</p>';
//		print_r($_SERVER['REQUEST_URI']);
//		echo '</pre>';

		$select_box_text = '--- SELECT ---';
	?>
</h1>
</div>
