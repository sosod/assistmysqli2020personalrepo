<?php
//error_reporting(-1);
$scripts = array();
$styles = array();
require_once("../inc/header.php");

require_once("../../module/autoloader.php");
$me = new ASSIST();
$db = new ASSIST_DB();
$qi = new QUERY_IMPORT();

function callback($buffer) {
	return $buffer;
}
 
ob_start("callback");

//$me->displayPageHeader();

//$menuObject = new Menu();
//$sub_menu = $menuObject->getSubMenu("risk");
//$me->arrPrint($sub_menu);

$process = array(
	'LIST'=>"Lists",
	'OBJECT'=>"Queries",
	'ACTION'=>"Actions",
	'ACTIVATE'=>"Activate",
);
$import_status = array(
	'LIST'=>true,
	'OBJECT'=>true,
	'ACTION'=>true,
);



if(isset($_REQUEST['key']) && strlen($_REQUEST['key'])>0) {
	$key = strtoupper($_REQUEST['key']);
	$subtitle = $process[$key];
	if(isset($_REQUEST['act'])) {
		switch($_REQUEST['act']) {
			case "IMPORT":
				$subtitle.=" >> Processing Import";
				break;
		}
	}
	echo "<h2>".$subtitle."</h2>";
} else {
	$subtitle = "";
	$key = "";
}

switch($key) {
case "LIST":	$warning = "WARNING!  This will delete ALL data already imported/loaded via Setup or Import and CANNOT be undone!";
case "OBJECT":	if(!isset($warning)) { $warning = "WARNING!  This will delete ALL data already imported/loaded via New or Import and CANNOT be undone!"; }
case "ACTION":	if(!isset($warning)) { $warning = "WARNING!  This will delete ALL data already imported/loaded via New or Import and CANNOT be undone!"; }
	require_once("import_page.php");
	break;
case "ACTIVATE":
	include("import_activate.php");
	break;
default:
	echo "
	<table class=form>";
	foreach($process as $key => $name) {
		if($key!="ACTIVATE") {
		echo "
		<tr>
			<th>".$name.":</th>
			<td>Import new ".$name."</td>
			<td class=center width=50px><input type=button value=Go id=$key  /></td>
		</tr>";
		}
	}
	/*echo "
		<tr>
			<th>Activate:</th>
			<td style='padding-right: 30px'>Click 'Activate' to confirm and activate all new ".$process['OBJECT']." and ".$process['ACTION'].".<br />They will not be available to users until this has been done.</td>
			<td><input type=button value=Activate id=activate /></td>
		</tr>
	</table>";*/
	echo "
		<tr>
			<th>Activate:</th>
			<td colspan=2>In order to make the newly imported Queries and Actions available to the users in Manage and Admin, you need to Activate them.<br />Go to New > Activate to review the list of new Queries and Activate where required.</td>
		</tr>
	</table>";

	?>
	<script type=text/javascript>
	$(function() {
		$("input:button").click(function() {
			var k = $(this).prop("id");
			document.location.href = 'import.php?key='+k;
		});
	});
	</script>


	<p>Reminder: <ol>
	<li>Before you start, make sure that the necessary Action Owners have been loaded as users.  To do this, login as the Administrator and follow the instructions in the Help manual regarding the creation of new users.</li>
	<li>Make sure that any users who need to update Queries have been given Administrator access to the relevant Sub-Directorate in Setup > Defaults > Sub-Directorate.</li>
	</ol></p>
<?php
	break;
}
//$echo = ob_get_contents();

ob_end_flush();
?>