<?php
$scripts = array('query.js','menu.js', 'ajaxfileupload.js');
$styles = array();
$page_title = "Add New Query";
require_once("../inc/header.php");
$naming 	= new Naming();
$rowNames   = $naming -> rowLabel();
function nameFields( $fieldId , $defaultName ){
	global $naming, $rowNames;
	if( isset( $rowNames[$fieldId] )) {
		echo $rowNames[$fieldId];
	}	else {
		echo $defaultName;
	}
}
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form id="new-risk-form" method="post" enctype="multipart/form-data">
<table border="0" id="risk_new" width="40%">
     <tr>
       <th><?php nameFields('financial_year','Financial Year'); ?>:</th>
       <td>
          <select id="financial_year" name="financial_year">
               <option value=""><?php echo $select_box_text ?></option>
           </select>
       </td>
     </tr> 
      <tr>
        <th><?php nameFields('query_reference','Query Reference'); ?>:</th>
        <td><input type="text" id="query_reference" name="query_reference" value="" /></td>
      </tr> 
      <tr>
        <th><?php nameFields('query_source','Query Source'); ?>:</th>
        <td>
            <select id="query_source" name="query_source">
                <option value=""><?php echo $select_box_text ?></option>
            </select>         
        </td>
      </tr>      
      <tr>
        <th><?php nameFields('query_type','Query Type'); ?>:</th>
        <td>
            <select id="query_type" name="query_type">
                <option value=""><?php echo $select_box_text ?></option>
            </select>
        </td>
      </tr>
      <tr>
        <th><?php nameFields('query_category','Query Category'); ?>:</th>
        <td>
            <select id="query_category" name="query_category">
                <option value=""><?php echo $select_box_text ?></option>
            </select>    
        </td>
      </tr>
      <tr>
        <th><?php nameFields('query_date','Query Raised Date'); ?>:</th>
        <td><input type="text" name="query_date" id="query_date" class="datepicker" readonly="readonly" /></td>
      </tr>      
      <tr>
        <th><?php nameFields('query_description','Query Description'); ?>:</th>
        <td><textarea name="query_description" id="query_description" cols="35" rows="7"></textarea></td>
      </tr>
      <tr>
        <th><?php nameFields('query_background','Background Of Query'); ?>:</th>
        <td><textarea name="query_background" id="query_background" cols="35" rows="7"></textarea></td>
      </tr>
    
      <?php 
        //if ($setups['risk_is_to_directorate'] == 1 ) { 
      ?>
     <tr>
        <th><?php nameFields('query_owner','Directorate responsible'); ?>:</th>
        <td>
        <select id="directorate" name="directorate" class="responsibility">
                <option value=""><?php echo $select_box_text ?></option>
         </select>        
        </td>
    <!--  </tr>
      <?php
      //}	else if ($setups['risk_is_to_subdirectorate'] == 1) {
      ?>
                     <tr>
        <th><?php nameFields('','Sub Directorate Responsible'); ?>:</th>
        <td>
        <select id="subdirectorate" name="subdirectorate" class="responsibility">
                <option value="">--sub directorate--</option>
         </select>  

        </td>
      </tr>
      <?php 
     // } else {
      ?>
      <tr>
        <th><?php nameFields('risk_owner','Responsibility of risk'); ?>:</th>
        <td>
        <select id="risk_user_responsible" name="risk_user_responsible" class="responsibility">
                <option value="">--user responsible--</option>
         </select>        
        </td>
      </tr> -->
      <?php
        //}
      ?>
      </tr>
      <tr>
      	<th><?php nameFields('query_deadline_date','Query Deadline Date'); ?>:</th>
      	<td><input type="text" name="query_deadline_date" id="query_deadline_date" class="datepicker" readonly="readonly" /></td>
      </tr>
      <tr>
        <th><?php nameFields('','Attach a document'); ?>:</th>
        <td>
        <tr>
           <th>Attachment:</th>
           <td>
                <input id="new_qry_attachment" name="new_qry_attachment" type="file" onChange="attachQuery(this)" />
                <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
                  <small>You can attach more than 1 file.</small>
                </p>
                <p id="file_upload"></p>
            </td>
        </tr>
        </td>
      </tr> 
	  <tr class="more_details">
        <th><?php nameFields('financial_exposure','Financial Exposure'); ?>:</th>
        <td>
        	<select id='financial_exposure' name='financial_exposure'>
            	<option value=""><?php echo $select_box_text ?></option>
        	</select>
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('monetary_implication','Monetary Implication'); ?>:</th>
        <td>
        	<textarea id='monetary_implication' name='monetary_implication' class="textcounter"></textarea>
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('risk_level','Risk Level'); ?>:</th>
        <td>
        <select id="risk_level" name="risk_level">
                <option value=""><?php echo $select_box_text ?></option>
         </select>            
        </td>
      </tr>      
      <tr class="more_details">
        <th><?php nameFields('risk_type','Risk Type'); ?>:</th>
        <td>
        <select id="risk_type" name="risk_type">
                <option value=""><?php echo $select_box_text ?></option>
         </select>            
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('risk_detail','Risk Detail'); ?>:</th>
        <td>
        	<textarea class="maxlimit" id='risk_detail' name='risk_detail'></textarea>
      </tr>      
      <tr class="more_details">
        <th><?php nameFields('finding','Add Finding'); ?>:</th>
        <td>
        	<p>
        		<textarea class="find" id='finding' name='finding'></textarea>
        	</p>       
            <input type="submit" name="add_another_finding" id="add_another_finding" value="Add Another"  /> 
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('internal_control_deficiency','Internal Control Deficiency'); ?>:</th>
        <td>
        	<p>
            	<textarea class="internalcontrol" id="internal_control_deficiency" name="internal_control_deficiency"></textarea>
            </p>
        	<input type="submit" name="add_another_internalcontrol" id="add_another_internalcontrol" value="Add Another"  />                 
        </td>
      </tr>        
      <tr class="more_details">
        <th><?php nameFields('recommendation','Add Recommendation'); ?>:</th>
        <td>
        	<p>
        		<textarea class="recommend" id='recommendation' name='recommendation'></textarea>
        	</p>
            <input type="submit" name="add_another_recommendation" id="add_another_recommendation" value="Add Another"  />         
        </td>
      </tr>
      <tr class="more_details">
        <th><?php nameFields('client_response','Add Client Response'); ?>:</th>
        <td>
        	<p>
            	<textarea class="clientresponse" id='client_response' name='client_response'></textarea>
            </p>
        	<input type="submit" name="add_another_clientresponse" id="add_another_clientresponse" value="Add Another"  />                 
        </td>
      </tr>    
      <tr class="more_details">
        <th><?php nameFields('auditor_conclusion','Auditor\'s Conclusion'); ?>:</th>
        <td>
        	<p>
            	<textarea class="auditorconclusion" id="auditor_conclusion" name='auditorconclusion'></textarea>
            </p>
        	<input type="submit" name="add_another_auditorconclusion" id="add_another_auditorconclusion" value="Add Another"  />                 
        </td>
      </tr>               
      <tr>
        <td>
            <!-- <?php if($setups['actions_aplied_to_risks'] == 1 ) { ?>
                <input type="submit" name="show_add_action" id="show_add_action" value="Add Action " />
            <?php } ?>  -->                      
       </td>
       <td>
		<span style='float:left'><input type="submit" name="add_query" id="add_query" value="Add Query " class="isubmit" /></span>
       	<span style='float:right'><input type="submit" name="more_detail" id="more_detail" value="More Detail" class="iinform" /></span>
       </td>
      </tr>
   </table>
</form>
</div>

