<?php 
require_once("../../module/autoloader.php");
//error_reporting(-1);
$me = new ASSIST();
$db = new ASSIST_DB();
$qi = new QUERY_IMPORT();
 
//$me->arrPrint($_REQUEST);

$data = $_REQUEST['data'];
$data = base64_decode($data);
$data = unserialize($data);
switch($_REQUEST['key']) {

case "LIST":
	foreach($data as $list => $rows) {
		if($list!="data") {
			if($list=="query_owner") {
				$rowsA = $data['dir'];
				unset($data['dir']);
				$compare = array();
			} else {
				$rowsA = array();
			}
			if(count($rows)>0) {
				//echo "<h2>".$list."</h2>";
				//$me->arrPrint($rowsA);
				//$me->arrPrint($rows);
				foreach($rowsA as $r) {
					$i = processListRow("dir",$r);
					$compare[$me->stripStringForComparison($r)] = $i;
				}
				foreach($rows as $r) {
					if($list=="query_owner") {
						if($r['subdirid']=="X") {
							$r['subdirid'] = $compare[$r['sd2']];
						}
					}
					processListRow($list,$r);
				}
			}
		}
	}

	$result = array("ok","Import finished!");


	break;



case "OBJECT":
	$obj = new Risk();
	foreach($data as $q) {
		$obj->saveImportedQuery($q);
	}
	$result = array("ok","Import completed successfully.");


	break;



case "ACTION":
	$obj = new RiskAction();
	foreach($data as $a) {
		$i = $obj->saveImportedAction($a);
		//echo "<br />".$i;
	}
	$result = array("ok","Import completed successfully.");
	break;
	
	


default:
	$result = array("error", "Sorry, don't know what you want me to do.");



}

echo json_encode($result);

function processListRow($list,$row) {
	if(!is_array($row)) {
		//process shortcode
		$x = explode(" ",$row);
		$sc = "";
		foreach($x as $z) {
			$sc.=substr($z,0,1);
		}
		$sc = strtoupper($sc);
	} else {
		$sc = "";
	}

$id = 0;

	switch($list) {
		case "financial_year":
			$obj = new FINANCIALYEAR($row['end_date'],$row['start_date'],$row['end_date']);
			$obj->saveFinYear(false);
			break;
		case "query_type":
			$obj = new QueryType($sc,$row,$row,0);
			$obj->saveType(false);
			break;
		case "query_category":
			$obj = new RiskCategory($row, $sc, $row, 0);
			$obj->saveCategory(false);
			break;
		case "dir":
			$obj = new QueryOwner($row);
			$id = $obj->saveDir();
			break;
		case "query_owner":
			$obj = new QueryOwner($row['subtxt'],$row['subdirid']);
			$obj->saveOwner();
			break;
		case "risk_type":
			$obj = new RiskType($sc,$row,$row,0);
			$obj->saveType(false);
			break;
		case "risk_level":
			$obj = new RiskLevel($sc,$row,$row,1);
			$obj->saveLevel(false);
			break;		
		case "query_source":
			$data = array(
				'name'=>$row,
				'description'=>$row,
				'status'=>1,
			);
			$obj = new QuerySource();
			$obj->saveQuerySource($data);
			break;
	}
	return $id;
}

?>