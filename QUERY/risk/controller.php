<?php
@session_start();
include_once("../inc/init.php");
switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ) {	
	case "saveQuery":
		$attachments    = "";
        $att_change_str = Attachment::processAttachmentChange("", "query_new", 'query');
	    if(!empty($_SESSION['uploads']['attachments']))
	    {
	      $attachments = $_SESSION['uploads']['attachments'];		
	    }	
		$risk = new QueryRisk( 
				$_POST['type'] ,
				$_POST['category'] ,
				$_POST['description'] ,
			    $_POST['background'] ,			
		 		(isset($_REQUEST['financial_exposure']) ? $_POST['financial_exposure'] : "" ),
				(isset($_REQUEST['monetary_implication']) ? $_POST['monetary_implication']  : "") ,
				(isset($_REQUEST['risk_detail']) ? $_POST['risk_detail'] : "") ,
				(isset($_REQUEST['finding']) ? $_POST['finding'] : ""),
				(isset($_REQUEST['recommendation']) ? $_POST['recommendation'] : "") ,
				(isset($_REQUEST['query_reference']) ? $_POST['query_reference'] : "") ,
				(isset($_REQUEST['client_response']) ? $_POST['client_response'] : ""),
				$_POST['user_responsible'],
				$attachments
		);
		$id       = $risk -> saveQuery($_POST);
		$response = array();
		if($id > 0 ) {
			
			$response["id"] 			  = $id;
			$error 						  = false; 
			$text 						  = "Query item #".$id." has been successfully saved <br />";
			$user      		     		  = new UserAccess("", "", "", "", "", "", "", "", "" );
			$managerEmailTo	     		  = $user -> getRiskManagerEmails();
			$directoryUser        		  = $user -> getUserEmailUnderDirectorate( $_REQUEST['user_responsible'] );
		
			$email 			     		  = new Email();
			$names 						  = new Naming();
			$header 					  = $names -> getHeaderNames();
			$email -> userFrom   		  = $_SESSION['tkn'];
			$email -> subject    		  = "New Query";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			//print "<pre>".print_r($header)."</pre>";
			$message   = ""; 
			$message .= $email -> userFrom." has created a New Query #".$id." \n\n";
			$subject = $message;
			$message .= "The details of the Query are as follows : \r\n";
			$message .= $names->nameLabel("query_item", "Query Item")." : ".$id."\r\n"; 
			$message .= $names->nameLabel("query_type", "Query Type")." : ".$_POST['type_text']."\r\n";
			$message .= $names->nameLabel("query_category", "Query Category")." : ".$_POST['category_text']."\r\n";
			$message .= $names->nameLabel("query_reference", "Query Reference")." : ".$_POST['query_reference']."\r\n";			
			$message .= $names->nameLabel("query_description", "Query Description")." : ".$_POST['description']."\r\n";
			$message .= $names->nameLabel("query_background", "Query Background")." : ".$_POST['background']."\r\n";
			$message .= $names->nameLabel("query_date", "Query Date")." : ".$_POST['query_date']."\r\n";
			$message .= $names->nameLabel("query_deadline_date", "Query Deadline Date")." : ".$_POST['query_deadline_date']."\r\n";			
			
			$message .= ($_POST['risk_type'] != "" ? $names->nameLabel("risk_type", "Risk Type")." : ".$_POST['risk_type']."\r\n" : "" );	
			
			$message .= ($_POST['risk_level'] != "" ? $names->nameLabel("risk_level", "Risk Level")." : ".$_POST['risk_level']."\r\n" : "" );		
						
			$message .= ($_POST['financial_exposure'] != "" ? $names->nameLabel("financial_exposure", "Financial Exposure")." : ".$_POST['financial_exposure']."\r\n" : "" );
			
			$message .= ($_POST['monetary_implication'] !="" ? $names->nameLabel("monetary_implication", "Monetary Implication")." : ".$_POST['monetary_implication']."\r\n" : "" );
			
			$message .= ($_POST['risk_detail'] != "" ? $names->nameLabel("risk_detail", "Risk Detail")." : ".$_POST['risk_detail']."\r\n" : "" );
			
			$message .= ($_POST['finding'] != "" ? $names->nameLabel("finding", "Finding")." : ".$_POST['finding']."\r\n" : "" );
			
			$message .= ($_POST['recommendation'] != "" ? $names->nameLabel("recommendation", "Recommendation")." : ".$_POST['recommendation']."\r\n" : "" );
			
			$message .= ($_POST['client_response'] != "" ? $names->nameLabel("client_response", "Client Response")." : ".$_POST['client_response']."\r\n" : "" );
			
			$message .= "Directorate Responsible : ".$_POST['user_responsible_text']."\r\n";
		    if(!empty($att_change_str))
		    {
		        $message .= $att_change_str;
		    }			
			$message .= " \r\n\n";	//blank row above Please log on line									
			$message .= "Please log onto Assist in order to View or Update the Query and to assign any required Action(s) to responsible users within ".$_SESSION['bpa_details']['cmpname']." \r\n";
			$message .= " \n";	//blank row above Please log on line
			$message  = nl2br($message);			
			$email -> body     = $message;
			$emailTo 		   = "";

			if(!empty( $directoryUser))
			{
				foreach($directoryUser as $userEmail)
				{
				  $emailTo = $userEmail['tkemail'].",";
				}
			}		
			$emailTo = rtrim($emailTo, ",");
			if( $emailTo != "" )
			{
				$email->to = $emailTo.","; 		
			}
			
			if( $managerEmailTo != "" )
			{
				$email->to .= $managerEmailTo.","; 		
			}			
			$email->to = rtrim( $email-> to, ",");
			/*
			 * 	echo "The new email to is ".$email->to."\r\n\n\n\n";
			echo "mangerEmailTo is ".$managerEmailTo."\r\n\n";
			echo "emailTo is ".$emailTo."\r\n\n";
			$email->to 	= ($emailTo == "" ? "" : rtrim($emailTo,","))."".($managerEmailTo == "" ? "" : $managerEmailTo);
			echo "The old email to is ".$email->to."\r\n\n";
			*/
			$email -> from     = "no-reply@assist.action4u.co.za";
			$e_mail = new ASSIST_EMAIL($email->to,$subject,$message,"TEXT");
			if($e_mail -> sendEmail())
			{
				$error = false;
				$text .= " &nbsp;&nbsp;&nbsp;&nbsp; Notification email has been sent  ";
			} else {
				$error = true; 
				$text  .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
			}
			$response = array("error" => $error , "text" => $text, "saved" => true, "id" => $id );
		} else {
			$response = array("error" => true , "text" => "Error saving the query [Error code: QRY121]", "saved" => false, "post_info" => $_POST);
		}
		Attachment::clear("", "query_new", 'query');
		if(isset($_SESSION['uploads']['query'])){
			unset($_SESSION['uploads']['query']);
		}
		echo json_encode( $response );
		break;
	case "updateQuery":
		$queryObj 		= new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$query_id       = $_POST['id'];
		$query  		= $queryObj -> getQueryDetail($query_id);
		$attachements   = "";
		$changes 		= $queryObj -> getRiskChange($_POST, $query);
		$updates        = array();
		$update_data    = array();
		$msg            = "";
		if(isset($changes['changes']) && !empty($changes['changes']))
		{
			$msg                   = $changes['change_message'];
			$updates['changes']    = base64_encode(serialize($changes['changes']));
			$updates['risk_id']    = $query_id;
			$updates['insertuser'] = $_SESSION['tid'];
		}
		if(isset($changes['update_data']) && !empty($changes['update_data']))
		{
			$update_data = $changes['update_data'];
		}
		$res                 = $queryObj -> editQuery($query_id, $update_data, $updates);
		if($res > 1)
		{
			$response["id"] 	  = $query_id;
			$error			 	  = false;
			$text 				  = "Query id ".$_REQUEST['id']." has been successfully edited <br />";
			$user      		      = new UserAccess("", "", "", "", "", "", "", "", "" );
			$emailTo    	      = $user -> getRiskManagerEmails();
			$responsibleUserEmail = $user -> getUserResponsiblityEmail($_REQUEST['user_responsible'] );
			$queryOwner           = $user -> getUserResponsiblityEmail($query['insertuser']);
			$email 			      = new Email();
			$email -> userFrom    = $_SESSION['tkn'];
			$email -> subject     = "Edit Query";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message   = "";
			$message  .= "".$email -> userFrom." edited a query #".$query_id."\r\n\n";
			if(!empty($msg))
			{
				$message .= $msg;
			}
			$message .= " \n";
			if(!empty($att_change_str))
			{
				$message .= $att_change_str;
			}
			//blank row above Please log on line
			$message .= "Please log onto Ignite Assist in order to view the edits.\n";
			$message  = nl2br($message);
			$email -> body     = $message;

			if( $emailTo != "" )
			{
				$email -> to = $emailTo.",";
			}
			if( isset($responsibleUserEmail['tkemail']) && !empty($responsibleUserEmail['tkemail']) )
			{
				$email -> to .= $responsibleUserEmail['tkemail'].",";
			}
			$email -> to   = rtrim( $email-> to, ",");
			$email -> from = "no-reply@ignite4u.com";
			if( $email -> sendEmail() ) {
				$text .= " &nbsp;&nbsp;&nbsp;&nbsp; Notification email send successfully";
			} else {
				$error = true;
				$text .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
			}
			$response = array("text" => $text, "error" => $error, "updated" => true );
		} else if($res == 0) {
			$response  = array("text" => "There was no change made to the query", "error"=> false, "updated" => false);
		}else {
			$response  = array("text" => "Error saving the query edit", "error" => true, "updated" => false);
		}
		Attachment::clear('qryadded', 'query_'.$_REQUEST['id']);
		echo json_encode( $response );
		break;
	case "newRiskAction":
        $data           = array();
		$data = array(
			'risk_id' 		=> $_REQUEST['id'],
			'query_action'	=> $_REQUEST['r_action'],
			'action_owner' 	=> $_REQUEST['action_owner'],
			'deliverable' 	=> $_REQUEST['deliverable'],
			'timescale' 	=> $_REQUEST['timescale'],
			'deadline' 		=> $_REQUEST['deadline'],
			'remindon' 		=> $_REQUEST['remindon'],
		);
        $att_change_str = Attachment::processAttachmentChange("", "action_new_".$_REQUEST['id'], 'action');
	    if(!empty($_SESSION['uploads']['attachments'])) {
	      $data['attachments'] = $_SESSION['uploads']['attachments'];		
	    }	

		//risk_id=0, $action="", $action_owner="", $deliverable="", $timescale="", $deadline="", $remindon="", $progress=0, $status=1]
		$risk = new RiskAction( 
				$_REQUEST['id'] ,
				$_REQUEST['r_action'] ,
				$_REQUEST['action_owner'] ,
			    $_REQUEST['deliverable'] ,
		 		$_REQUEST['timescale'] ,
				$_REQUEST['deadline'] ,
				$_REQUEST['remindon'] ,
				$_REQUEST['progress'],
				$_REQUEST['status']
		);
		$id 	   = $risk -> saveRiskAction($data);
		$response  = array(); 
		if( $id > 0 ) {
			$response["id"] 			  = $id;
			$error						  = false;
			$text 						  = "New Action #".$id." has been successfully saved <br />";
			$user      		     		  = new UserAccess("", "", "", "", "", "", "", "", "" );
			$emailTo		     		  = $user -> getRiskManagerEmails();
			$responsibleUserEmail 		  = $user -> getUserResponsiblityEmail( $_REQUEST['action_owner'] );
			$email 			     		  = new Email();
			$email -> userFrom   		  = $_SESSION['tkn'];
			$risk 						  = new Risk("" ,"" , "", "", "", "", "", "", "", "", "", "", "");
			$riskInfo 					  = $risk -> getRisk($_REQUEST['id']);
			$email -> subject    		  = "New Action";
			//Object is something like Risk or Action etc.  Object_id is the reference of that object
			$message  = "";			
			$message .= $email -> userFrom." created an Action relating to query #".$_REQUEST['id'];
			$message .= " : ".$riskInfo['description']." identified in the Query Register. The details of the action are as follows :";
			$message .= " \r\n\n";	//blank row above Please log on line
			$message .= "Action Number : ".$id."\r\n"; 
			$message .= "Action : ".$_REQUEST['r_action']."\r\n";
			$message .= "Deliverable : ".$_REQUEST['deliverable']."\r\n";
			$message .= "Action Owner : ".$responsibleUserEmail['tkname']." ".$responsibleUserEmail['tksurname']."\r\n";
			$message .= "Time Scale : ".$_REQUEST['timescale']."\r\n";
			$message .= "Deadline : ".$_REQUEST['deadline']."\r\n";
		    if(!empty($att_change_str))
		    {
		       $message .= $att_change_str;
		    }														
		    $message .= " \r\n\n";	//blank row above Please log on line										
			$message .= "<i>Please log onto Ignite Assist in order to View or Update the Action.</i> \n";
			$message  = nl2br($message);			
			$email -> body     = $message;
			if( $emailTo != "" )
			{
				$email->to = $emailTo.","; 		
			}
			
			if( isset($responsibleUserEmail['tkemail']) && !empty($responsibleUserEmail['tkemail']) )
			{
				$email->to .= $responsibleUserEmail['tkemail'].","; 		
			}			
			$email->to = rtrim( $email-> to, ",");			
			//$email -> to = ($emailTo == "" ? "" : $emailTo)."".($responsibleUserEmail['tkemail'] == "" ? "" : ",".$responsibleUserEmail['tkemail']);
			$email -> from     = "no-reply@ignite4u.com";
			if( $email -> sendEmail() ) {
				$error 	= false;
				$text  .= " &nbsp;&nbsp;&nbsp;&nbsp; Notification has been sent";
			} else {
				$error  = true;
				$text  .= " &nbsp;&nbsp;&nbsp;&nbsp; There was an error sending the email";
			}
			$response = array("error" => $error, "text" => $text, "saved" => true, "id" => $id);
		} else {
			$response = array("error" => true, "text" => "Error saving the action <br />".$id, "saved" => false, "whatv the data" => $_REQUEST);
		}
	    Attachment::clear("qryadded", 'action_new_'.$_REQUEST['id']);
	    Attachment::clear("qrydeleted", 'action_new_'.$_REQUEST['id']);
		if(isset($_SESSION['uploads']['action'])){
			unset($_SESSION['uploads']['action']);
		}		
		echo json_encode( $response );
		break;
	case "getFinancialExposure":
		$finExp = new FinancialExposure( "", "", "", "", "");
		echo json_encode( $finExp -> getFinancialExposure() );
		break;	
	case "getRiskLevel":
		$level = new Risklevel( "", "", "", "" );
		echo json_encode( $level-> getLevel() );	
		break;			
	case "removeQueryAttachment":
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('query_new', 'new_qry_attachment');
        $response = $attObj -> removeFile($file, 'query_new');
        echo json_encode($response);
        exit();	    
	    /*
		$filename = $_POST['id'];
		$file	  =  $_POST['file'];
		$att = new Attachments(array(), "query");
		$att->deletedFile( $filename, $file ); 	
		*/
		break;	
	case "saveAttachment":
        $attObj    = new Attachment('query_new', 'new_qry_attachment'); 
        $results   = $attObj -> upload('query_new');
        echo json_encode($results);
        exit();
	    /*
		$att = new Attachments($_FILES, "query");
		$att -> upload();	
		*/
		break;
    case "download":		
        header("Content-type:".$_GET['content']);
        header("Content-disposition: attachment; filename=".$_GET['new']);
        readfile("../../files/".$_GET['company']."/".$_SESSION['modref']."/".$_GET['folder']."/".$_GET['file']);
        exit();        
		break;
	case "saveActionAttachment":
        $attObj    = new Attachment('action_new_'.$_REQUEST['query_id'], 'new_action_attachment_'.$_REQUEST['query_id']); 
        $results   = $attObj -> upload('action_new_'.$_REQUEST['query_id']);
        echo json_encode($results);
        exit();			
		break;
	case "removeActionFile":
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('action_new_'.$_REQUEST['query_id'], 'new_action_attachment_'.$_REQUEST['query_id']);
        $response = $attObj -> removeFile($file, $_REQUEST['name']);
        echo json_encode($response);		
		break;
	case "updateRisk":
		$risk = new Risk( 	
				$_REQUEST['type'] ,
				$_REQUEST['category'] ,
				$_REQUEST['description'] ,
			    $_REQUEST['background'] ,
		 		$_REQUEST['impact'] ,
				$_REQUEST['impact_rating'] ,
				$_REQUEST['likelihood'] ,
				$_REQUEST['likelihood_rating'],
				$_REQUEST['currentcontrols'] ,
				$_REQUEST['percieved_control'] ,
				$_REQUEST['control_effectiveness'] ,
				$_REQUEST['user_responsible'],
				base64_encode(serialize($_REQUEST['attachment']))
				);
		$risk -> updateRisk( $_REQUEST['id'] , $_REQUEST['status'] );
	case "getRisk":
		$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo json_encode($risk -> getQueries($_GET));
		break;	
	case "getActivationQueries":
		$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo json_encode($risk -> getActivationQueries($_GET));
	    break;
	case "activateQuery":
		$risk     = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		$res      = $risk -> activateQuery($_POST);
		$response = array();
		if($res > 0)
		{
		  $response = array('text' => 'Query Q'.$_POST['query_id'].' successfully activated', 'error' => false, 'updated' => true);
		} elseif($res == 0){
		   $response = array('text' => 'No change made to query status', 'error' => false, 'updated' => true);
		} else {
		   $response = array('text' => 'No change made to query status', 'error' => false, 'updated' => true);
		}
		echo json_encode($response);
	break;	
	case "getARisk":
		$risk = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo json_encode( $risk -> getRisk( $_REQUEST['id'] ) );
		break;			
	case "getDefaults":
		$default = new Defaults("","");
		echo json_encode( $default -> getDefaults() );
		break;
	case "getDirectorate":
		$dir = new Directorate( "", "", "" ,"");
		echo json_encode( $dir -> getDirectorate() );
		break;
	case "getSubDirectorate":
		$dir = new SubDirectorate( "", "", "" ,"");
		//$ditc = $dir -> getDirectorateResponsible();
		echo json_encode( $dir -> getDirectorateResponsible() );
		break;
	case "getRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getType() );	
		break;
	case "getActiveRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getActiveRiskType() );	
		break;
    case "getQueryType":
		$type = new QueryType( "", "", "", "" );
		echo json_encode( $type-> getActiveRiskType() );
		break;
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getCategory() );
		break;
	case "getImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getImpact() );
		break;
	case "getActiveImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getActiveImpact() );
		break;		
	case "getImpactRating":
		$imp = new Impact( "", "", "", "", "");
		$imp -> getImpactRating( $_REQUEST['id']  );
		break;
	case "getLikelihoodRating":
		$lk = new Likelihood( "", "", "", "", "","");
		$lk -> getLikelihoodRating( $_REQUEST['id']  );
		break;	
	case "getLikelihood":
		$like = new Likelihood( "", "", "", "", "","");
		echo json_encode( $like -> getLikelihood() );
		break;
	case "getControl":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		echo json_encode( $ihr -> getControlEffectiveness());
		break;
	case "getControlRating":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		$ihr -> getControlRating( $_REQUEST['id'] );
		break;
	case "getUserAccess":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUserAccess() );
		break;	
	case "getUsers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUsers() );
		break;	
	case "getRiskStatus":
		$getstat = new RiskStatus( "", "", "" );
		echo json_encode( $getstat -> getStatus() );
		break;	
	case "deleteAction":
		$risk = new RiskAction( "" , "", "", "", "", "", "", "","");
		$risk -> deleteRiskAction( $_REQUEST['id'] );
		break;	
	case "activateAction":
		$risk = new RiskAction( "" , "", "", "", "", "", "", "","");
		$risk -> activateRiskAction( $_REQUEST['id'] );
	break;
	case "getRiskAssurance":
		$rskass = new Risk( "" , "", "", "", "", "", "", "", "", "", "", "","");
		echo json_encode( $rskass -> getRisk( $_REQUEST['id'] ) );
		//echo json_encode( $rskass -> getRisk( $_REQUEST['id'] ) );
		break;	
	case "getRiskAssurances":
		$rskass = new RiskAssurance($_REQUEST['id'] , "", "", "", "", "");
		echo json_encode( $rskass -> getRiskAssurance() );
		break;
	case "newRiskAssurance":
		$rskass = new RiskAssurance($_REQUEST['id'] , "", $_REQUEST['response'], $_REQUEST['signoff'], "", $_REQUEST['attachment']);
		$rskass -> saveRiskAssurance() ;
		break;
	case "saveNotification":
		$usernot = new UserNotification();
		$usernot -> saveNotifications();
	break;
	case "getNotifications":
		$usernot = new UserNotification();
		echo json_encode( $usernot -> getNotifications() );
	break;
	case "getStatus":
		$stat = new RiskStatus("", "", "");
		echo json_encode( $stat -> getStatus() );
	break;
	case "getFinancialYear":
		$stat  = new FinancialYear("", "", "");
		$years = $stat -> getFinYear();
		echo json_encode($years);
	break;		
	case "getActionQuery":
		$riskObj = new Risk( "", "", "", "", "", "", "", "", "", "", "", "", "");
		$query   = $riskObj -> getRiskTable($_GET['id'] );
		echo $query;
	break;	
	    case "getActions":
	    $act = new RiskAction($_REQUEST['id'], "", "", "", "", "", "","", "");
	    echo json_encode( $act -> getRiskActions( $_GET['start'], $_GET['limit']) );
	break;		
	case "getActiveQuerySource":
        $sourceObj = new QuerySource();
        echo json_encode($sourceObj -> getQuerySources(" AND status <> 0 "));
	break;
	default:
		echo "It hasn't found the actions associated";
	break;
	}
	

?>
