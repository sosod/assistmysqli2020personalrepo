<?php
$scripts = array( 'jquery.ui.query.js','menu.js',  );
$styles = array( 'colorpicker.css',);
$page_title = "Edit Query";
require_once("../inc/header.php");
?>
<script language="javascript">
    $(function(){
        $("#risk").query({editQuery:true, view:"viewAll", section:"new"});
    });
</script>
<div id="risk"></div>
