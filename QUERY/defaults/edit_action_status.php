<?php
$scripts = array( 'action_status.js','menu.js', 'jscolor.js', 'ignite.textcounter.js' );
$styles = array();
$page_title = "Edit Action Statuses";
require_once("../inc/header.php");
require_once("replaceNameFields.php");
$actionstatus 	= new ActionStatus( "", "", "" );
$action_status	= $actionstatus  -> getAStatus( $_REQUEST['id'] );	
?>
<script>
$(function(){
		$("table#editaction_status_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<form id="edit-form-action-status" name="edit-action-status">
<table border="1" id="editaction_status_table" cellpadding="5" cellspacing="0">
  <tr>
	    <th>Ref #:</th>
	    <td><?php echo $action_status['id']; ?></td>
   </tr>
   <tr>
    <th><?php replaceNameFields('action_status','Action Status'); ?>:</th>
    <td>
    	<textarea name="actionstatus" id="actionstatus" <?php if(($action_status['status'] & 5) == 5) { ?> disabled="disabled"<?php } ?> class="textcounter"><?php echo $action_status['name']; ?></textarea>
    </td>
   </tr>
  <tr>
   <th>Your Terminology:</th>
   <td>
   	  <textarea name="action_client_term" id="action_client_term" class="textcounter"><?php echo $action_status['client_terminology']; ?></textarea>
   </td>
  </tr>
  <tr> 
    <th>Color:</th>
    <td><input type="text" name="action_color" id="action_color"  class="color"  value="<?php echo ($action_status['color'] == "" ? "e2ddcf" : $action_status['color'] ); ?>"/></td>
   </tr>
  <tr>  
    <th></th>
    <td>
    	<input type="hidden" name="action_status_id" id="action_status_id" value="<?php echo $_REQUEST['id'] ?>" />
    	<input type="submit" name="edit_actionstatus" id="edit_actionstatus" value="Save Changes" />
        <input type="submit" name="cancel_actionstatus" id="cancel_actionstatus" value="Cancel" />
    </td>
  </tr>
 <tr>
    <td class="noborder" align="left"><?php $me->displayGoBack("/QUERY/defaults/action_status.php",""); ?></td>
    <td class="noborder"></td>
 </tr>
</table>
</form>
</div>
