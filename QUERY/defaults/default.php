<?php

header("Location: index.php");

$scripts = array( 'menu.js', 'setup_logs.js'  );
$styles = array( 'colorpicker.css' );
$page_title = "View Risk";
require_once("../inc/header.php");
?>
<script language="javascript">
	$(function(){
		$(".setup_defaults").click(function() {
			document.location.href = this.id+".php";
			return false;		  
		});
		$("table#link_to_page").find("th").css({"text-align":"left"})
	});
</script>
<table border="1" width="60%" cellpadding="5" cellspacing="0" id="link_to_page">
	<tr>
    <tr>
    	<th>Menu and Headings:</th>
        <td>Define the Menu headings to be displayed in the module</td>
        <td><input type="button" name="menu_heading" id="menu_heading" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Naming Convention:</th>
        <td>Define the Naming convention of the Query fields for the module </td>
        <td><input type="button" name="naming" id="naming" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Financial Years:</th>
        <td>Configure the Financial Years</td>
        <td><input type="button" name="financial_year" id="financial_year" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Query Status:</th>
        <td>Configure the Query Status</td>
        <td><input type="button" name="risk_status" id="risk_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Action Status:</th>
        <td>Configure the Action Status</td>
        <td><input type="button" name="action_status" id="action_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Query Types:</th>
        <td>Configure the Query Types</td>
        <td><input type="button" name="query_type" id="query_type" value="Configure" class="setup_defaults"  /></td>
    </tr>
     <tr>
    	<th>Query Categories:</th>
        <td>Configure the Query Categories</td>
        <td><input type="button" name="risk_categories" id="risk_categories" value="Configure" class="setup_defaults"  /></td>
    </tr>	
    <tr>
    	<th>Risk Types:</th>
        <td>Configure the Risk Types</td>
        <td><input type="button" name="risk_type" id="risk_type" value="Configure" class="setup_defaults"  /></td>
    </tr>
     <tr>
    	<th>Risk Level:</th>
        <td>Configure the Risk Level</td>
        <td><input type="button" name="risk_level" id="risk_level" value="Configure" class="setup_defaults"  /></td>
    </tr>
     <tr>
    	<th>Financial Exposure:</th>
        <td>Configure the Financial Exposure</td>
        <td>
         <input type="button" name="financial_exposure" id="financial_exposure" value="Configure" class="setup_defaults"  />
        </td>
    </tr>       
   <tr>
    	<th>Action Assurance Status:</th>
        <td>Define the Action Assurance Statuses</td>
        <td><input type="button" name="action_assurance" id="action_assurance" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Query Assurance Status:</th>
        <td>Define the Query Assurance Status</td>
        <td>
           <input type="button" name="query_assurance_status" id="query_assurance_status" value="Configure" class="setup_defaults"  />
        </td>
    </tr>    
    <!-- <tr>
    	<th>Inherent Query Exposure:</th>
        <td>Define the Inherent Query Exposure rating criteria</td>
        <td><input type="button" name="inherent_risk" id="inherent_risk" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Control Effectiveness:</th>
        <td>Define the Control Effectiveness rating criteria</td>
        <td><input type="button" name="control" id="control" value="Configure"  class="setup_defaults" /></td>
    </tr>
    <tr>
    	<th>Residual Query Exposure:</th>
        <td>Define the Residual Query Exposure rating criteria</td>
        <td><input type="button" name="residual" id="residual" value="Configure" class="setup_defaults"  /></td>
    </tr> -->
    <tr>
    	<th>(Sub-)Directorate Structure:</th>
        <td>Configure the (Sub-)Directorate Structure and responsibilities </td>
        <td><input type="button" name="setup_dir" id="setup_dir" value="Configure" class="setup_defaults"  /></td>
    </tr>	
<!--    <tr>
    	<th>Directorate Structure:</th>
        <td>Configure the Directorate Structure and responsibilities </td>
        <td><input type="button" name="directorate" id="directorate" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Sub-Directorate Structure:</th>
        <td>Configure the Sub-Directorate Structure and responsibilities</td>
        <td><input type="button" name="sub_directorate" id="sub_directorate" value="Configure" class="setup_defaults"  /></td>
    </tr> -->
    <tr>
<!--    	<th>Module Defaults:</th>
        <td>Configure the Module Defaults</td>
        <td><input type="button" name="default_setting" id="default_setting" value="Configure" class="setup_defaults"  /></td>
    </tr> -->
   <!-- <tr>
    	<th>User Defined Fields(UDF's):</th>
        <td>Configure Additional Fields</td>
        <td><input type="button" name="udf" id="udf" value="Configure"  class="setup_defaults" /></td>
    </tr>
    -->    
    <tr>
    	<th>Glossary:</th>
        <td>Configure the data to be shown on the Glossary</td>
        <td><input type="button" name="glossary" id="glossary" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>List Columns:</th>
        <td>Configure which columns display on the Manage pages</td>
        <td><input type="button" name="columns" id="columns" value="Configure" class="setup_defaults"  /></td>
    </tr>    
</table>
