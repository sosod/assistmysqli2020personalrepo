<?php
require_once ("../../module/autoloader.php");


$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../inc/header.php");
$dbref = strtolower($dbref);
$result = array();

$adb = new ASSIST_DB("client");
$helper = new ASSIST();
$cmpcode = $adb->getCmpCode();
$modref = strtoupper($adb->getModRef());
$dbref = $adb->getDBRef();

$sections = array(
	'update'=>array(
		'text'=>"Update",
		'can_function'=>"canUpdate",
		'default'=>"Y",
	),
	'edit'=>array(
		'text'=>"Edit",
		'can_function'=>"canEdit",
		'default'=>"N",
	),
	'approve'=>array(
		'text'=>"Approve",
		'can_function'=>"canApprove",
		'default'=>"N",
	),
	'delete'=>array(
		'text'=>"Delete",
		'can_function'=>"canDelete",
		'default'=>"N",
	),
	'add_action'=>array(
		'text'=>"Add Action",
		'can_function'=>"canAddAction",
		'default'=>"N",
	),
	'add_object'=>array(
		'text'=>"Add Query",
		'can_function'=>"canAddObject",
		'default'=>"N",
	),
);

//Get Active Users
$sql = "SELECT tkid as id, CONCAT_WS(' ',tkname,tksurname) as name 
		FROM assist_".$cmpcode."_timekeep 
		INNER JOIN assist_".$cmpcode."_menu_modules_users 
		ON tkid = usrtkid AND usrmodref = '$modref' 
		WHERE tkstatus = 1 ORDER BY tkname, tksurname";
$users = $adb->mysql_fetch_all($sql);


$del_style = "float:right;";
/*
$var = $_REQUEST;
if(isset($var['act'])) {
	switch($var['act']) {
		case "DEL":
			if(isset($var['a']) && checkIntRef($var['a'])) {
				$aid = $var['a'];
				$sql = "UPDATE ".$dbref."_dir_admins SET active = false WHERE id = $aid";
				include("inc_db_con.php");
					if(mysql_affected_rows() > 0) {
						$sql = "INSERT INTO assist_".$cmpcode."_log (date, tkid, ref, transaction,tsql) VALUES ($today,'$tkid','$modref','Deleted (sub-)Directorate administrator $aid','".code($sql)."')";
						include("inc_db_con.php");
						$result[0] = "check";
						$result[1] = "Administrator deleted.";
					} else {
						$result[0] = "error";
						$result[1] = "An error occurred.  No change was made.  Please try again.";
					}
			} else {
				$result[0] = "error";
				$result[1] = "An error occurred.  No change was made.  Please try again.";
			}
			break;
		case "ADD":
			$d = $var['d'];
			$usr = $var['atkid'];
			$r = $var['ref'];
			if(!isset($var['atkid']) || $var['atkid']=="X") {
				$result[0] = "error";
				$result[1] = "Please select the administrator's name from the User list.  No change was made.  Please try again.";
			} else {
				if($r=="DIR") {
					$ref = $d;
					$type = "DIR";
				} else {
					$r = explode("_",$r);
					$ref = $r[1];
					$type = "SUB";
				}
				if(!checkIntRef($ref)) {
					$result[0] = "error";
					$result[1] = "Please ensure that you select a valid Directorate / Sub-Directorate from the list available.  No change was made.  Please try again.";
				} else {
					$sql = "SELECT * FROM ".$dbref."_dir_admins WHERE ref = $ref AND type = '$type' AND tkid = '$usr' AND active = true";
					include("inc_db_con.php");
					$mnr = mysql_num_rows($rs);
					mysql_close($con);
					if($mnr > 0) {
						$result[0] = "info";
						$result[1] = "Administrator already exists.  No change was made.";
					} else {
						$sql = "INSERT INTO ".$dbref."_dir_admins (tkid,active,type,ref) VALUES ('$usr',true,'$type',$ref)";
						include("inc_db_con.php");
						if(mysql_affected_rows() > 0) {
							$sql = "INSERT INTO assist_".$cmpcode."_log (date, tkid, ref, transaction,tsql) VALUES ($today,'$tkid','$modref','Added (sub-)Directorate administrator ".mysql_insert_id()."','".code($sql)."')";
							include("inc_db_con.php");
							$result[0] = "check";
							$result[1] = "New administrator added.";
						} else {
							$result[0] = "error";
							$result[1] = "An error occurred.  No change was made.  Please try again.";
						}
					}
				}
			}
			break;
		default:
			break;
	}
}
*/

//Get details
$dirid = $_REQUEST['d'];
//Directorate
    $sql = "SELECT dirid as id, dirtxt as name FROM ".$dbref."_dir WHERE dirid = ".$dirid;
	$dir = $adb->mysql_fetch_one($sql);
//Sub Directorate
	$sql = "SELECT subid as id, subtxt as name FROM ".$dbref."_dirsub WHERE subdirid = ".$dirid." AND active = true ORDER BY subsort";
	$opt = $adb->mysql_fetch_all_by_id($sql,"id");
//get Administrators
	$admins = array(
		'DIR'=>array(),
		'SUB'=>array()
	);
	$sql = "SELECT a.id, a.tkid as user_id, a.type, a.ref, a.active, CONCAT(t.tkname,' ',t.tksurname) as name
			FROM ".$dbref."_dir_admins a
			INNER JOIN assist_".$cmpcode."_timekeep t
			ON t.tkid = a.tkid AND t.tkstatus = 1
			WHERE active & ".DirectorateAdministrator::ACTIVE." = ".DirectorateAdministrator::ACTIVE."
			AND (	(
				type = 'DIR' AND ref = $dirid
				".(count($opt)>0 ? ") OR  ( type = 'SUB' AND ref IN (".implode(",",array_keys($opt)).")" : "")."
			)	)
			ORDER BY t.tkname, t.tksurname";
	//$rs = $adb->db_query($sql);
    $rows = $adb->mysql_fetch_all($sql);
	//while($row = mysql_fetch_assoc($rs)) {
	foreach ($rows as $row){
		$admins[$row['type']][$row['ref']][$row['id']] = $row;
	}
	unset($rs);
	
$colspan = 8;
function drawHeading($title) {
	global $colspan, $sections;
	echo "	
		<tr>
			<th rowspan=2>$title</th>
			<th colspan=".$colspan.">Administrator</th>
		</tr>
		<tr>
			<th class=th2>User</th>";
		foreach($sections as $sec) {
			echo "<th class='access th2'>".$sec['text']."</th>";
		}
		echo "
			<th class='th2'></th>
		</tr>";
}	
	
function drawStatus($s) {
	return "<div style='width: 16px; margin: 0 auto' class='ui-widget ui-state-".($s===true ? "yes" : "fail")."'><span class='ui-icon ui-icon-".($s===true ? "check" : "closethick")."'>&nbsp;&nbsp;</span></div>";
}	
?>
<script type=text/javascript>
/*function delDir(i,tk,txt,d) {
    if(escape(i)==i && !isNaN(parseInt(i)))
    {
        if(confirm("Are you sure you want to delete administrator '"+tk+"' from "+txt+"?\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
        {
            document.location.href = "setup_admin_dir_config.php?act=DEL&a="+i+"&d="+d;
        }
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}*/
$(function() {
	$("table.list th").css({"text-align":"center","vertical-align":"middle"});
	$("table.form th").css({"text-align":"left","vertical-align":"top"});
	var maxWidth = 0;
	$("table tr:nth-child(2) th.access").each(function() {
		if($(this).width()>maxWidth) {
			maxWidth = $(this).width();
		}
	});
	$("table th.access, table td.center").width(maxWidth);
});
</script>
<style type=text/css>
/* Ignite Assist Styles */
.ui-state-ok, .ui-widget-content .ui-state-ok, .ui-widget-header .ui-state-ok  {border: 1px solid #009900; background: #efffef; color: #006600; }
.ui-state-ok a, .ui-widget-content .ui-state-ok a,.ui-widget-header .ui-state-ok a { color: #363636; }
.ui-state-yes, .ui-widget-content .ui-state-yes, .ui-widget-header .ui-state-yes  {border: 0px solid #009900; background: #ffffff; color: #006600; }
.ui-state-yes a, .ui-widget-content .ui-state-yes a,.ui-widget-header .ui-state-yes a { color: #363636; }
.ui-state-info, .ui-widget-content .ui-state-info, .ui-widget-header .ui-state-info  {border: 1px solid #fe9900; background: #fbf9ee ; color: #363636; }
.ui-state-info a, .ui-widget-content .ui-state-info a,.ui-widget-header .ui-state-info a { color: #363636; }
.ui-state-fail, .ui-widget-content .ui-state-fail, .ui-widget-header .ui-state-fail  {border: 0px solid #cc0001; background: #ffffff ; color: #cd0a0a; }
.ui-state-fail a, .ui-widget-content .ui-state-fail a,.ui-widget-header .ui-state-fail a { color: #363636; }
.ui-state-info .ui-icon {
	background-image: url(/library/jquery/css/images/ui-icons_fe9900_256x240.png);
}
.ui-state-fail .ui-icon, .ui-state-error .ui-icon {
	background-image: url(/library/jquery/css/images/ui-icons_cd0a0a_256x240.png);
}
.ui-state-ok .ui-icon, .ui-state-yes .ui-icon {
	background-image: url(/library/jquery/css/images/ui-icons_009900_256x240.png);
}
</style>
<?php
$me->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<p><input type=button value="Add New" id=btn_add /></p>
<table class=list>
	<?php
	echo drawHeading("Directorate");

	$x = array_key_exists($dirid,$admins['DIR']) ? $admins['DIR'][$dirid] : array();
	if(count($x)>0) {
		$a=0;
		echo "
		<tr>
			<td class='b td_dir' rowspan=".count($x).">".$dir['name']."</td>";
		foreach($x as $y) {
			echo "<td class=td_user>".$y['name']."</td>";
			$da = new DirectorateAdministrator($y);
			foreach($sections as $sec) {
				echo "<td class='center i'>".drawStatus(call_user_func(array($da, $sec['can_function'])))."</td>";
			}
			echo "
				<td><input type=button value=Edit id=".$y['id']." class=btn_edit /></td>
			";
			
			$a++;
			if($a<count($x)) {
				echo "</tr><tr>";
			}
		}
		echo "
		</tr>
		";
	} else {
		echo "
		<tr>
			<td class=b>".$dir['name']."</td>
			<td class=i colspan=".$colspan.">No Administrators</td>
		</tr>
		";
	}
	
	
	
	echo drawHeading("Sub-Directorate");
	foreach($opt as $id=>$s) {
		$x = isset($admins['SUB'][$id]) ? $admins['SUB'][$id] : array();
		if(count($x)>0) {
			$a=0;
			echo "
			<tr>
				<td class='b td_dir' rowspan=".count($x).">".$s['name']."</td>";
			foreach($x as $y) {
				echo "<td class=td_user>".$y['name']."</td>";
				$da = new DirectorateAdministrator($y);
				foreach($sections as $sec) {
					echo "<td class='center i'>".drawStatus(call_user_func(array($da, $sec['can_function'])))."</td>";
				}
				echo "
					<td><input type=button value=Edit id=".$y['id']." class=btn_edit /></td>
				";				
				$a++;
				if($a<count($x)) {
					echo "</tr><tr>";
				}
			}
			echo "
			</tr>
			";
		} else {
			echo "
			<tr>
				<td class=b>".$s['name']."</td>
				<td class=i colspan=".$colspan.">No Administrators</td>
			</tr>
			";
		}
		
	}
	
	
	?>
</table>

<?php
$me->displayGoBack("setup_dir.php");
?>
<div id=dlg_add title=Add>
<h2 id=h2_title>Add New</h2>
<form name=frm_add method=post action=setup_admin_dir_config.php >
<input type=hidden name=d value=<?php echo $dirid; ?> />
<input type=hidden name=act id=my_action value=ADD />
<input type=hidden name=i id=admin_id value=0 />
<table class=form>
	<tr>
		<th width=150>(Sub-)Directorate:</th>
		<td><select name=ref id=admin_ref>
			<option selected value=DIR><?php echo $dir['name']; ?></option>
			<?php foreach($opt as $key => $sub) {
				echo "<option value=\"".$key."\">".$sub['name']."</option>";
			} ?>
		</select></td>
	</tr>
	<tr>
		<th>User:</th>
		<td><select name=atkid id=admin_tkid>
			<option selected value=X><?php echo $select_box_text ?></option>
			<?php
			foreach($users as $row) {
				echo "<option value=".$row['id'].">".$row['name']."</option>";
			}
			?>
		</select></td>
	</tr>
	<?php
	foreach($sections as $i=>$s) {
		echo "<tr>
			<th>".$s['text'].":</th>
			<td><select name=$i id=admin_".$i.">
				<option ".($s['default']=="Y" ? "selected" : "")." value=Y>Yes</option>
				<option ".($s['default']!="Y" ? "selected" : "")." value=N>No</option>
			</select></td>
		</tr>";
	}
	?>
	<tr>
		<th>&nbsp;</th>
		<td class=right><input type=submit value=Save class=isubmit /> <input type=button value=Cancel class=btn_cancel /></td>
	</tr>
</table>
<p class=float><input type=button value='Delete Administrator' class=idelete /></p>
</form>
</div>
<script type=text/javascript>
$(function() {
	var dlgWidth=500;
	$("#dlg_add").dialog({
		modal: true,
		autoOpen: false,
		height: 400
	});
	$("#btn_add").click(function() { 
		$("#dlg_add").dialog("option","title","Add");
		$("#dlg_add #h2_title").text("Add New Administrator");
		$("#dlg_add #my_action").val("ADD");
		$("#dlg_add input:button.idelete").hide();
		$("#dlg_add input.isubmit").val("Save New Administrator");
		$("#dlg_add #admin_ref").val("DIR");
		$("#dlg_add #admin_tkid").val("X");
		<?php 
		foreach($sections as $key => $sec) {
			echo "
			$(\"#dlg_add #admin_".$key."\").val('".$sec['default']."');";
		} ?>
		openDialog(0);
	});
	$("#dlg_add .btn_cancel").click(function() { 
		$("form[name=frm_add").trigger("reset");
		$("#dlg_add").dialog("close"); 
	});
	$("input:button.btn_edit").click(function() { 
		$("#dlg_add").dialog("option","title","Edit");
		$("#dlg_add #h2_title").text("Edit Administrator");
		$("#dlg_add #my_action").val("EDIT");
		$("#dlg_add input.isubmit").val("Save Changes");
		$("#dlg_add input:button.idelete").show();
		var i = $(this).prop("id");
		$.ajax({
			type: "POST",
			url: "setup_admin_dir_ajax.php",
			data: "act=getEditData&i="+i,
			dataType: "json",
			success: function(data) {
				data['add_action'] = data['addaction'];
				data['add_object'] = data['addobject'];
				$("#dlg_add #admin_ref").val(data['ref']);
				$("#dlg_add #admin_tkid").val(data['tkid']);
				
				<?php 
				foreach($sections as $key => $sec) {
					echo "
					$(\"#dlg_add #admin_".$key."\").val(data['".$key."']);";
				} ?>
				
			}
		});
		openDialog(i);
	});
	$("#dlg_add form[name=frm_add]").submit(function(e) {
		var valid = true;
		$("#dlg_add form[name=frm_add] select").each(function() {
			$(this).removeClass("required");
			if($(this).val()=="X" || $(this).val()==0) {
				$(this).addClass("required");
				valid = false;
			}
		});
		if(valid) {
			$.ajax({
				type: "POST",
				url: "setup_admin_dir_ajax.php",
				data: $(e.target).serialize(),
				dataType: "json",
				error: function(dta) {
					//alert(jqXHR+":"+textStatus+":"+errorThrown);
                    console.log(dta,$(e.target).serialize());
				},
				success: function(data){
					url = "setup_admin_dir_config.php?d=<?php echo $dirid; ?>&r[]="+data[0]+"&r[]="+data[1];
					//alert(url);
					document.location.href = url;
				}
			});
		} else {
			alert("Please complete all fields.");
		}
		return false;
	});
	$("#dlg_add input:button.idelete").click(function() {
		if(confirm("Are you sure you wish to delete this administrator?")==true) {
			$("#dlg_add #my_action").val("DELETE");
			$("#dlg_add form[name=frm_add]").submit();
		}
	});
});
function openDialog(i) {
	$(function() {
		$("#dlg_add #admin_id").val(i);
		$("#dlg_add").dialog("open"); 
		var tblWidth=$("#dlg_add table").width();
		tblWidth += (100-(tblWidth%100));
		$("#dlg_add").dialog("option","width",tblWidth+50); 
	});
}
</script>
</body></html>
