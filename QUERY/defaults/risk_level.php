<?php
$scripts = array( 'risk_level.js','menu.js', 'setup_logs.js', 'ignite.textcounter.js' );
$styles = array();
$page_title = "Set Up Risk Level";
require_once("../inc/header.php");

require_once("replaceNameFields.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form id="risk-level-form" name="risk-level-form">
			<table border="1" id="risk_level_table">
			  <tr>
			    <th>Ref</th>
			    <th>Short Code</th>
			    <th><?php replaceNameFields('risk_level','Risk Level'); ?></th>
			    <th>Description</th>
			    <th></th>
			    <th>Status</th>
			    <th></th>    
			  </tr>
			  <tr>
			    <td>#</td>
			    <td>
			    	<input type="text" name="risklevel_shortcode" id="risklevel_shortcode" />
			    </td>
			    <td>
			      <textarea name="risklevel" id="risklevel" class="textcounter"></textarea>
			    </td>
			    <td>
			    	<textarea id="risklevel_description" name="risklevel_description" class="mainbox"></textarea>
			    </td>
			    <td><input type="submit" name="add_risklevel" id="add_risklevel" value="Add" /></td>
			    <td></td>
			    <td></td>
			  </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("index.php",""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink("level_logs", true) ?></td>
	</tr>
</table>
</div>

