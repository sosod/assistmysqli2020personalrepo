<?php
$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../inc/header.php");

$dbref = strtolower($dbref);

$var = $_REQUEST;
$act = isset($var['act']) ? $var['act'] :"";
if($act=="EDIT") {
	$dirid = $var['dirid'];
	if(!$me->checkIntRef($dirid)) {
		die("An error has occurred.  Please go back and try again.");
	} else {
		if(strlen($var['dir'])>0 && strlen($var['sub1'])>0) {
			$done = array('D'=>0,'S'=>0);
			//update dir
			$sql = "UPDATE ".$dbref."_dir SET dirtxt = '".$me->code($var['dir'])."' WHERE dirid = $dirid";
			//include("inc_db_con.php");
            $mar = $me->db_update($sql);
			if($mar > 0) { $done['D'] = 1; }
			//update primary sub
			$sql = "UPDATE ".$dbref."_dirsub SET subtxt = '".$me->code($var['sub1'])."' WHERE subhead = 'Y' AND subdirid = $dirid AND active = true";
			//include("inc_db_con.php");
            $mar = $me->db_update($sql);
			if($mar > 0) { $done['S'] = 1; }
			if($done['D']==true && $done['S']==true) {
				$result[0] = "check";
				$result[1] = "Directorate and Primary Sub-Directorate have been updated.";
			} elseif($done['D']==true) {
				$result[0] = "check";
				$result[1] = "Directorate has been updated.";
			} elseif($done['S']==true) {
				$result[0] = "check";
				$result[1] = "Primary Sub-Directorate has been updated.";
			} else {
				$result[0] = "info";
				$result[1] = "No changes where made.";
			}
		} else {
			$result[0] = "error";
			$result[1] = "Invalid Directorate or Primary Sub-Directorate.  No change was made.";
		}
	}
} elseif($act=="SUB_EDIT") {
	$subid = $var['subid'];
	if(!$me->checkIntRef($subid)) {
		die("An error has occurred.  Please go back and try again.");
	} else {
		$sql = "SELECT * FROM ".$dbref."_dirsub WHERE subid = $subid ";
		//include("inc_db_con.php");
			$sub = $me->mysql_fetch_one($sql);
		//mysql_close($con);
		$dirid = $sub['subdirid'];
		if(!$me->checkIntRef($dirid)) {
			die("An error has occurred.  Please go back and try again.");
		} else {
			$sql = "UPDATE ".$dbref."_dirsub SET subtxt = '".$me->code($var['sub'])."' WHERE subid = $subid";
			//include("inc_db_con.php");
            $mar = $me->db_update($sql);
			if($mar>0) {
				$result[0] = "check";
				$result[1] = "Sub-Directorate '".$var['sub']."' updated.";
			} else {
				$result[0] = "info";
				$result[1] = "No change was made.";
			}
		}
	}
} elseif($act=="SUB_DEL") {
	$subid = $var['s'];
	if(!$me->checkIntRef($subid)) {
		die("An error has occurred.  Please go back and try again.");
	} else {
		$sql = "SELECT * FROM ".$dbref."_dirsub WHERE subid = $subid ";
		//include("inc_db_con.php");
			$sub = $me->mysql_fetch_one($sql);
		//mysql_close($con);
		$dirid = $sub['subdirid'];
		if(!$me->checkIntRef($dirid)) {
			die("An error has occurred.  Please go back and try again.");
		} else {
			$sql = "UPDATE ".$dbref."_dirsub SET active = false WHERE subid = $subid";
			//include("inc_db_con.php");
            $mar = $me->db_update($sql);
			if($mar>0) {
				$result[0] = "check";
				$result[1] = "Sub-Directorate '".$sub['subtxt']."' deleted.";
			} else {
				$result[0] = "info";
				$result[1] = "No change was made.";
			}
		}
	}
} elseif($act=="SUB_ADD") {
	$dirid = $var['dirid'];
	if(!$me->checkIntRef($dirid)) {
		die("An error has occurred.  Please go back and try again.");
	} else {
		$sql = "SELECT max(subsort) as sub FROM ".$dbref."_dirsub WHERE subdirid = $dirid";
		//include("inc_db_con.php");
			$sub = $me->mysql_fetch_one($sql);
		//mysql_close($con);
		$subsort = (isset($sub['sub']) && is_numeric($sub['sub'])) ? $sub['sub'] + 1 : 2;
		$sql = "INSERT INTO ".$dbref."_dirsub SET subtxt = '".code($var['sub'])."', active = true, subdirid = $dirid, subhead = 'N', subsort = $subsort ";
		//include("inc_db_con.php");
        $mr = $me->db_insert($sql);
		if($mr>0) {
			$result[0] = "check";
			$result[1] = "Sub-Directorate '".$var['sub']."' created.";
		} else {
			$result[0] = "info";
			$result[1] = "No change was made.";
		}
	}
} elseif($act=="SUB_ORDER") {
	$dirid = $var['dirid'];
	$sort = $var['sort'];
	if(!$me->checkIntRef($dirid)) {
		die("An error has occurred.  Please go back and try again.");
	} else {
		$lqs[] = "UPDATE ".$dbref."_dirsub SET subsort = 1 WHERE subhead = 'Y' AND subdirid = $dirid";
		foreach($sort as $key => $s) {
			$lqs[] = "UPDATE ".$dbref."_dirsub SET subsort = ".($key+2)." WHERE subid = $s";
		}
		foreach($lqs as $sql) {
			//include("inc_db_con.php");
			$me->db_update($sql);
		}
		$result[0] = "check";
		$result[1] = "Sub-Directorates reordered.";
	}
} else {
	$dirid = isset($_REQUEST['d']) ? $_REQUEST['d'] :0;
	$result = isset($_REQUEST['r']) ? $_REQUEST['r'] : array();
}
if(!$me->checkIntRef($dirid)) {
	die("An error has occurred.  Please go back and try again.");
} else {
	$sql = "SELECT * FROM ".$dbref."_dir WHERE dirid = $dirid AND active = true";
	//include("inc_db_con.php");
		if($me->db_get_num_rows($sql)==0) {
			die("An error has occurred.  Please go back and try again.");
		} else {
			$dir = $me->mysql_fetch_one($sql);
		}
	//mysql_close($con);
	$sql = "SELECT * FROM ".$dbref."_dirsub WHERE subdirid = $dirid AND active = true ORDER BY  subsort ASC";
	//include("inc_db_con.php");
    $rows = $me->mysql_fetch_all($sql);
	$subhead = array();
	$sub = array();
	//while($row = mysql_fetch_assoc($rs)) {
    foreach ($rows as $row){
		if($row['subhead'] == "Y") {
			$subhead = $row;
		} else {
			$sub[] = $row;
		}
	}
	//mysql_close($con);
}

?>
<script type=text/javascript>
function Validate(me) {
	return true;
}

function dirDel(d) {
	if(isNaN(parseInt(d))) {
		alert("ERROR!\nAn error occurred.  Please refresh the page and try again.");
	} else {
		if(confirm("Are you sure that you wish to delete this Directorate?\n\nThis action will also delete all associated Sub-Directorates\nand make any associated risks inaccessible.")==true) {
			document.location.href = "setup_dir.php?act=DEL&dirid="+d;
		}
	}
}
</script>
<h3>Edit Directorate</h3>
<?php $me->displayResult($result); ?>
<form name=edit method=post action=setup_dir_edit.php onsubmit="return Validate(this);">
	<input type=hidden name=act value=EDIT />
	<input type=hidden name=dirid value=<?php echo($dirid); ?>>
	<table cellpadding=3 cellspacing=0 width=650>
		<tr>
			<th width=150>Name:</th>
			<td><input type=text size="50" name=dir maxlength=50 value="<?php echo ASSIST_HELPER::decode($dir['dirtxt']); ?>">&nbsp;</td>
			<td width=110><small>Max 50 characters</small></td>
		</tr>
		<tr>
			<th >Primary Sub-Directorate:</th>
			<td><input type=text size="50" name=sub1 maxlength=50 value="<?php echo ASSIST_HELPER::decode(isset($subhead['subtxt']) ? $subhead['subtxt'] : ""); ?>">&nbsp;</td>
			<td width=110>*required </td>
		</tr>
		<tr>
			<th valign=top >Sub-Directorates:</th>
			<td valign=top colspan=2><table class=noborder >
			<?php
				foreach($sub as $s) {
					echo "<tr><td class=noborder>".$s['subtxt']."</td><td class=\"noborder right\">&nbsp;<input type=button value=\" Edit \" onclick=\"document.location.href='setup_dir_sub_edit.php?s=".$s['subid']."';\" /></td></tr>";
				}
			?>
				<tr><td class=noborder colspan=2><input type=button value="Add New" onclick="document.location.href='setup_dir_sub_add.php?d=<?php echo $dirid; ?>';" /> <?php if(count($sub)>1) { ?><input type=button value="Display Order" onclick="document.location.href='setup_dir_sub_order.php?d=<?php echo $dirid; ?>';" /><?php } ?></td></tr>
			</table></td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top colspan="2"><input type=submit value="Save Changes" /> <input type=reset value="Reset" /> <span style="float: right;"><input type=button value=" Delete~" onclick="dirDel(<?php echo $dirid; ?>)" /></span></td>
		</tr>
	</table>
</form>
<p style="font-size: 7.5pt; margin-top: -15px;"><i>* This is an automatically generated Sub-Directorate that is required by the system and cannot be edited.<br />
~ This will delete both the Directorate and its associated Sub-Directorates.</i></p>
<?php
$urlback = "setup_dir.php";
$me->displayGoBack();
//include("inc_goback.php");
?>

