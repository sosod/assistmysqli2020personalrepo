<?php
$scripts = array( 'setup_menu.js','menu.js', 'setup_logs.js'  );
$styles = array( 'colorpicker.css' );
$page_title = "View Risk";
require_once("../inc/header.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<div id="container">
			<div id="setupmenu_message" ></div>
			</div>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("",""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink("menu_logs", true)?></td>
	</tr>
</table>
</div>
