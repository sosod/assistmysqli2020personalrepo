<?php
$scripts = array( 'action_status.js','menu.js','jscolor.js', 'setup_logs.js', 'ignite.textcounter.js' );
$styles = array();
$page_title = "Set Up Action Statuses";
require_once("../inc/header.php");

require_once("replaceNameFields.php");
?>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder">
			<table class="noborder">
			<tr>
				<td colspan="2" class="noborder">
					<form id="form-action-status" name="form-action-status">
					<table border="1" id="action_status_table">
					  <tr>
					    <th>Ref</th>
					    <th><?php replaceNameFields('action_status','Action Status'); ?></th>
					    <th>Your Terminology</th>
					    <th>Colour</th>
					    <th></th>
					    <th>Status</th>
					    <th></th>    
					  </tr>
					  <tr>
					    <td>#</td>
					    <td>
					    	<textarea name="actionstatus" id="actionstatus" class="textcounter"></textarea>
					    </td>
					    <td>
					    	<textarea name="action_client_term" id="action_client_term" class="textcounter"></textarea>
					    </td>
					    <td>
					    	<input type="text" name="action_color" id="action_color" class="color"  value="e2ddcf"/>
					    </td>
					    <td>
					    	<input type="submit" name="add_actionstatus" id="add_actionstatus" value="Add"/>
					    </td>
						<td></td>
					    <td>
					    </td>
					  </tr>
					</table>
					</form>		
				</td>		
			</tr>
			</table>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("index.php",""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink("action_status_logs", true); ?></td>
	</tr>
</table>



