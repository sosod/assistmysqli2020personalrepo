<?php
$scripts = array( 'edit_likelihood.js','menu.js', 'jscolor.js' );
$styles = array();
$page_title = "Edit Likehood";
require_once("../inc/header.php");
$liek 	= new Likelihood("", "", "", "", "","");
$likelihoods =  $liek -> getALikelihood( $_REQUEST['id'] ) ;
?>
<script>
$(function(){
		$("table#likelihood_table").find("th").css({"text-align":"left"})
});
</script>
<div>
<form id="edit-likelihood-form" name="edit-likelihood-form">
<div id="likelihood_message"></div>
<table border="1" id="likelihood_table">
  <tr>
  	
    <th>Ref:</th>
        <td>#<?php echo $_REQUEST['id']; ?></td>
    </tr>
    <tr>
    <th>Rating:</th>
    <td>
   		<input type="text" name="lkrating_from" id="lkrating_from" size="2" value="<?php echo $likelihoods['rating_from']; ?>" /> 
   	to
   		 <input type="text" name="lkrating_to" id="lkrating_to" size="2" value="<?php echo $likelihoods['rating_to']; ?>" />
    </td>
    </tr>
    <tr>
    <th>Assessment:</th>
    <td><input type="text" name="lkassessment" id="lkassessment" value="<?php echo $likelihoods['assessment']; ?>" /></td>
    </tr> 
    <tr>    
    <th>Probability:</th>
    <td><input type="text" name="lkprobability" id="lkprobability" value="<?php echo $likelihoods['probability']; ?>"/></td>
    </tr>
    <tr>    
    <th>Definition:</th>
    <td><textarea id="lkdefinition" name="lkdefinition"><?php echo $likelihoods['definition']; ?></textarea></td>
    </tr>
    <tr>
    <th>Colour:</th>
    <td><input type="text" name="lkcolor" id="llkcolor" class="color" value="<?php echo ($likelihoods['color'] == "" ? "e2ddcf" :$likelihoods['color'] ) ?>"/></td>
    </tr>
  <tr>
      <td>&nbsp;</td>
    <td>
    <input type="hidden" name="likelihood_id" id="likelihood_id" value="<?php echo $_REQUEST['id']; ?>" />
    <input type="submit" name="edit_likelihood" id="edit_likelihood" value="Save Changes" />
    <input type="submit" name="cancel_edit_likelihood" id="cancel_edit_likelihood" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td colspan="2" align="left">
    	<?php $me->displayGoBack("",""); ?>
    </td>
  </tr>
</table>
</form>
</div>
