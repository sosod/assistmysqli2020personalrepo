<?php
$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../inc/header.php");

$dbref = strtolower($dbref);

$var = $_REQUEST;

$id = $_REQUEST['d'];

if(!$me->checkIntRef($id)) {
	die("An error has occurred.  Please go back and try again.");
} else {
	$sql = "SELECT d.dirtxt FROM ".$dbref."_dir d WHERE d.dirid = $id";
	//include("inc_db_con.php");/

		if($me->db_get_num_rows($sql)==0) {
			die("An error has occurred.  Please go back and try again.");
		} else {
			$dir = $me->mysql_fetch_one($sql);
		}
	//mysql_close($con);
}

?>
<script type=text/javascript>
function Validate(me) {
	if(me.sub.value.length>0) {
		return true;
	} else {
		alert("Please enter a Sub-Directorate name.");
		return false;
	}
}
</script>
<h3>Add Sub-Directorate</h3>
<form name=edit method=post action=setup_dir_edit.php onsubmit="return Validate(this);">
	<input type=hidden name=act value=SUB_ADD />
	<input type=hidden name=dirid value=<?php echo($id); ?>>
	<table cellpadding=3 cellspacing=0 width=650>
		<tr>
			<th width=150>Directorate:</th>
			<td><?php echo ASSIST_HELPER::decode($dir['dirtxt']); ?>&nbsp;</td>
			<td width=110></td>
		</tr>
		<tr>
			<th>Sub-Directorate:</th>
			<td><input type=text size="50" name=sub maxlength=50 value="">&nbsp;</td>
			<td width=110>*required </td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top colspan="2"><input type=submit value=" Save " /> <input type=reset value="Reset" /></td>
		</tr>
	</table>
</form>
<?php
$urlback = "setup_dir_edit.php?d=".$id;
$me->displayGoBack();//include("inc_goback.php");
?>
