<?php
$scripts = array( 'edit_likelihood.js','menu.js', 'jscolor.js' );
$styles = array();
$page_title = "Edit Likehood";
require_once("../inc/header.php");
$liek 	= new Likelihood("", "", "", "", "","");
$likelihoods =  $liek -> getALikelihood( $_REQUEST['id'] ) ;
?>
<div>
<form id="edit-likelihood-form" name="edit-likelihood-form">
<div id="likelihood_message"></div>
<table border="1" id="likelihood_table">
  <tr>
  	
    <th>ID</th>
        <td>#<?php echo $_REQUEST['id']; ?></td>
    </tr>
    <tr>
    <th>Likelihood Status</th>
    <td>
   		<select name="like_status" id="like_status">
        	<option value="1" <?php if($likelihoods['active']==1) {?> selected="selected" <?php } ?>>Active</option>
            <option value="0" <?php if($likelihoods['active']==0) {?> selected="selected" <?php } ?>>InActive</option>
        </select>
    </td>
    </tr>
  <tr>
      <td>&nbsp;</td>
    <td>
    <input type="hidden" name="likelihood_id" id="likelihood_id" value="<?php echo $_REQUEST['id']; ?>" />
    <input type="submit" name="change_likelihood" id="change_likelihood" value="Edit" />
    <input type="submit" name="cancel_edit_likelihood" id="cancel_edit_likelihood" value="Cancel" />
    </td>
  </tr>
  <tr>
  	<td colspan="2" align="left">
    	<?php $me->displayGoBack("",""); ?>
    </td>
  </tr>
</table>
</form>
</div>
