<?php
$scripts = array( 'financialexposure.js','menu.js', 'jscolor.js', 'setup_logs.js', 'ignite.textcounter.js' );
$styles = array();
$page_title = "Set Up Risk Impacts";
require_once("../inc/header.php");

require_once("replaceNameFields.php");
?>
<div>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2"> 
				<form id="financial_exposure-form" name="financial_exposure-form">
				<table border="1" id="financial_exposure_table">
				  <tr>
				    <th>Ref</th>
				    <th>Rating</th>
				    <th><?php replaceNameFields('financial_exposure','Financial Exposure'); ?></th>
				    <th>Definition</th>
				    <th>Color </th>
				    <th></th>
				    <th>Status</th>    
				    <th></th>
				  </tr>
				  <tr>
				    <td>#</td>
				    <td>
				   		<input type="text" name="from" id="from" size="5" /> 
				   	to
				   		 <input type="text" name="to" id="to" size="5" />
				    </td>
				    <td>
				    	<textarea name="name" id="name" class="textcounter"></textarea>
				    </td>
				    <td>
				    	<textarea id="definition" name="definition" class="mainbox"></textarea>
				    </td>
				    <td>
				    	<input type="text" name="color" id="color" class="color" value="e2ddcf" />
				    </td>
				    <td><input type="submit" name="add" id="add" value="Add" /></td>
				    <td></td>
				    <td></td>
				  </tr>
				</table>
				</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("index.php",""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink("financial_exposure_logs", true)?></td>
	</tr>
</table>

</div>


