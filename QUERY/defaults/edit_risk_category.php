<?php
$scripts = array( 'risk_category.js','menu.js', 'ignite.textcounter.js' );
$styles = array('jPicker-1.1.6.min.css' );
$page_title = "Edit Risk Status";
require_once("../inc/header.php");
require_once("replaceNameFields.php");
$typ 		= new QueryType( "", "", "", "");
$types 		= $typ -> getType() ;	
$riskcat 	= new RiskCategory( "", "", "", "" );
$category 	= $riskcat  -> getACategory( $_REQUEST['id']);
?>
<script>
$(function(){
	$("table#edit_riskcategory_table").find("th").css({"text-align":"left"});
});
</script>
    <div>
    <?php $admire_helper->JSdisplayResultObj(""); ?>
    <form id="edit-risk-category-form" name="edit-risk-category-form" method="post">
    <table border="1" id="edit_riskcategory_table">
    <tr>
    	 <th>Ref #:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
      <tr>
        <th>Short Code:</th>
        <td><input type="text" name="category_shortcode" id="category_shortcode" value="<?php echo htmlentities($category['shortcode'] ); ?>" /></td>
      </tr>
      <tr>
        <th><?php replaceNameFields('query_category','Query Category'); ?>:</th>
        <td>
        	<textarea name="risk_category" id="risk_category" class="textcounter"><?php echo htmlentities( $category['name'] ); ?></textarea>
   	    </td>
      </tr>
      <tr>
      	<th>Description of <?php replaceNameFields('query_category','Query Category'); ?>:</th>
        <td>
        	<textarea class="mainbox" name="category_description" id="category_description"><?php echo htmlentities( $category['description'] ); ?></textarea>
        </td>
      </tr>
      <!--
      <tr>
      	<th>Query Type:</th>
        <td>
            <select name="category_risktype" id="category_risktype">
           	 <option value="">--Select query type--</option>
             <?php foreach($types as $type) {
			 ?>
               <option value="<?php echo $type['id']; ?>"
               	<?php echo (trim($category['type_id'])==trim($type['id']) ? "selected" : "")?>
               ><?php echo $type['name']; ?></option>
             <?php
	 		}  ?>
            </select>
       </td>
      </tr> 
      -->
      <tr>     
      	<th></th>
        <td>
        <input type="hidden" name="risk_category_id" id="risk_category_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="edit_riskcategory" id="edit_riskcategory" value="Save Changes" class="isubmit" />
        <input type="submit" name="cancel_riskcategory" id="cancel_riskcategory" value="Cancel" class="idelete" />
        </td>
      </tr>
      <tr>
        <td class="noborder" align="left"><?php $me->displayGoBack("/QUERY/defaults/risk_categories.php",""); ?></td>
        <td class="noborder"></td>
      </tr>
    </table>
    </form>
    </div>
