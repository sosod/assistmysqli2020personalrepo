<?php
$scripts = array( 'menu.js', 'setup_logs.js'  );
$styles = array( 'colorpicker.css' );
$page_title = "View Risk";
require_once("../inc/header.php");
require_once("replaceNameFields.php");
?>
<script language="javascript">
	$(function(){
		$(".setup_defaults").click(function() {
			document.location.href = this.id+".php";
			return false;		  
		});
		$("table#link_to_page").find("th").css({"text-align":"left"})
	});
</script>
<table border="1" width="60%" cellpadding="5" cellspacing="0" id="link_to_page">
	<tr>
    <tr>
    	<th>Menu and Headings:</th>
        <td>Define the Menu headings to be displayed in the module</td>
        <td><input type="button" name="menu_heading" id="menu_heading" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Query Headings:</th>
        <td>Define the Naming convention of the Query fields for the module </td>
        <td><input type="button" name="naming" id="naming" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Action Headings:</th>
        <td>Define the Naming convention of the Action fields for the module </td>
        <td><input type="button" name="naming_action" id="naming_action" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th><?php replaceNameFields('financial_year','Financial Year'); ?>:</th>
        <td>Configure the <?php replaceNameFields('financial_year','Financial Year'); ?> list items.</td>
        <td><input type="button" name="financial_year" id="financial_year" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th><?php replaceNameFields('query_status','Query Status'); ?>:</th>
        <td>Configure the <?php replaceNameFields('query_status','Query Status'); ?> list items.</td>
        <td><input type="button" name="risk_status" id="risk_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th><?php replaceNameFields('action_status','Action Status'); ?>:</th>
        <td>Configure the <?php replaceNameFields('action_status','Action Status'); ?> list items.</td>
        <td><input type="button" name="action_status" id="action_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th><?php replaceNameFields('query_type','Query Type'); ?>:</th>
        <td>Configure the <?php replaceNameFields('query_type','Query Type'); ?> list items.</td>
        <td><input type="button" name="query_type" id="query_type" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th><?php replaceNameFields('query_source','Query Source'); ?>:</th>
        <td>Configure the <?php replaceNameFields('query_source','Query Source'); ?> list items.</td>
        <td><input type="button" name="query_source" id="query_source" value="Configure" class="setup_defaults"  /></td>
    </tr>
     <tr>
    	<th><?php replaceNameFields('query_category','Query Category'); ?>:</th>
        <td>Configure the <?php replaceNameFields('query_category','Query Category'); ?> list items.</td>
        <td><input type="button" name="risk_categories" id="risk_categories" value="Configure" class="setup_defaults"  /></td>
    </tr>	
    <tr>
    	<th><?php replaceNameFields('risk_type','Risk Type'); ?>:</th>
        <td>Configure the <?php replaceNameFields('risk_type','Risk Type'); ?> list items.</td>
        <td><input type="button" name="risk_type" id="risk_type" value="Configure" class="setup_defaults"  /></td>
    </tr>
     <tr>
    	<th><?php replaceNameFields('risk_level','Risk Level'); ?>:</th>
        <td>Configure the <?php replaceNameFields('risk_level','Risk Level'); ?> list items.</td>
        <td><input type="button" name="risk_level" id="risk_level" value="Configure" class="setup_defaults"  /></td>
    </tr>
     <tr>
    	<th><?php replaceNameFields('financial_exposure','Financial Exposure'); ?>:</th>
        <td>Configure the <?php replaceNameFields('financial_exposure','Financial Exposure'); ?> list items.</td>
        <td><input type="button" name="financial_exposure" id="financial_exposure" value="Configure" class="setup_defaults"  /></td>
    </tr>  
   <tr>
    	<th>Action Assurance Status:</th>
        <td>Define the Action Assurance Statuses</td>
        <td><input type="button" name="action_assurance_status" id="action_assurance_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Query Assurance Status:</th>
        <td>Define the Query Assurance Status</td>
        <td>
           <input type="button" name="query_assurance_status" id="query_assurance_status" value="Configure" class="setup_defaults"  />
        </td>
    </tr>         
   <!-- <tr>
    	<th>Impact:</th>
        <td>Define the impact assesment and rating criteria </td>
        <td><input type="button" name="impact" id="impact" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Likelihood:</th>
        <td>Define the Query Likelihood assessment and rating criteria</td>
        <td><input type="button" name="likelihood" id="likelihood" value="Configure" class="setup_defaults"  /></td>
    </tr>    
    <tr>
    	<th>Inherent Query Exposure:</th>
        <td>Define the Inherent Query Exposure rating criteria</td>
        <td><input type="button" name="inherent_risk" id="inherent_risk" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Control Effectiveness:</th>
        <td>Define the Control Effectiveness rating criteria</td>
        <td><input type="button" name="control" id="control" value="Configure"  class="setup_defaults" /></td>
    </tr>
    <tr>
    	<th>Residual Query Exposure:</th>
        <td>Define the Residual Query Exposure rating criteria</td>
        <td><input type="button" name="residual" id="residual" value="Configure" class="setup_defaults"  /></td>
    </tr> -->
    <tr>
    	<th>(Sub-)Directorate Structure:</th>
        <td>Configure the (Sub-)Directorate Structure and responsibilities </td>
        <td><input type="button" name="setup_dir" id="setup_dir" value="Configure" class="setup_defaults"  /></td>
    </tr>	
<!--    <tr>
    	<th>Directorate Structure:</th>
        <td>Configure the Directorate Structure and responsibilities </td>
        <td><input type="button" name="directorate" id="directorate" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Sub-Directorate Structure:</th>
        <td>Configure the Sub-Directorate Structure and responsibilities</td>
        <td><input type="button" name="sub_directorate" id="sub_directorate" value="Configure" class="setup_defaults"  /></td>
    </tr> -->
<!--    <tr>
    	<th>Module Defaults:</th>
        <td>Configure the Module Defaults</td>
        <td><input type="button" name="default_setting" id="default_setting" value="Configure" class="setup_defaults"  /></td>
    </tr> -->
   <!-- <tr>
    	<th>User Defined Fields(UDF's):</th>
        <td>Configure Additional Fields</td>
        <td><input type="button" name="udf" id="udf" value="Configure"  class="setup_defaults" /></td>
    </tr>
    -->    
    <tr>
    	<th>Glossary:</th>
        <td>Configure the data to be shown on the Glossary</td>
        <td><input type="button" name="glossary" id="glossary" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>List Columns:</th>
        <td>Configure which columns display on the Manage pages</td>
        <td><input type="button" name="columns" id="columns" value="Configure" class="setup_defaults"  /></td>
    </tr>    
    <tr>
    	<th>Data Report:</th>
        <td>View the data usage for this module</td>
        <td><input type="button" name="datareport" id="datareport" value="View" class="setup_defaults"  /></td>
    </tr>   
	
</table>
