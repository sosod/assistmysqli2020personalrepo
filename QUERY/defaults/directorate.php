<?php
$scripts = array( 'directorate.js','menu.js');
$styles = array( );
$page_title = "Set Up Risk directorate structure";
require_once("../inc/header.php");
?>
<div>
<form method="post" id="directorate-form" name="directorate-form">
<div id="directorate_message"></div>
<table border="1" id="directorate_table">
  <tr>
    <th>ID</th>
    <th>Short Code</th>
    <th>Department</th>
    <th>Department Function</th>    
    <th>Responsible for risk</th>
    <th></th>    
    <th>Status</th> 
    <th></th>       
  </tr>
  <tr>
    <td>#</td>
    <td><input type="text" name="directorate_shortcode" id="directorate_shortcode"  /></td>
    <td><input type="text" name="dr_department" id="dr_department" /></td>
    <td><textarea name="dept_function" id="dept_function"></textarea></td>
    <td>
    	<select id="dr_users_respo" name="dr_users_respo">
        	<option value=""><?php echo $select_box_text ?></option>
        </select>
    </td>
    <td><input type="submit" name="add_directorate" id="add_directorate" value="Add" /></td>
    <td></td>
	<td></td>        
  </tr>
</table>
</form>
</div>
