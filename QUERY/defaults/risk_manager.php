<?php
$scripts = array( 'risk_manager.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "Risk Manager";
require_once("../inc/header.php");
?>
<?php $admire_helper->JSdisplayResultObj(""); ?>
<table class="noborder" width="60%">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post" name="risk-manager-form" id="risk-manager-form"> 
			<div id="risk_manager_message"></div>
			<table border="1" id="risk_manager_table" width="100%">
			  <tr>
			    <th>Ref</th>
			    <th>User</th>
			    <th>Action</th>
			  </tr>
			  <tr>
			    <td>#</td>
			    <td>
			    	<select id="risk_manager_users" name="risk_manager_users">
			        	<option value=""><?php echo $select_box_text ?></option>
			        </select>
			    </td>
			    <td>
			    	<input type="submit" name="save" id="save" value="Set as query manager" />
			    </td>
			  </tr>
			<!--  <tr>
			  	<td colspan="3" align="left"><a href="javascript:history.back()">Go Back</a></td>
			  </tr>-->
			</table>
			</form>
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php $me->displayGoBack("",""); ?></td>
		<td class="noborder"></td>
	</tr>
</table>



