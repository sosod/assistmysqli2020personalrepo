<?php
$scripts = array( 'edit_subdir.js','menu.js');
$styles = array( );
$page_title = "Edit Sub Directorate";
require_once("../inc/header.php");
$uaccess 		= new UserAccess( "", "", "", "", "", "", "", "", "" );
$users 			= $uaccess -> getUsers() ;
$dir 			= new Directorate( "", "", "" ,"");
$directorates	= $dir -> getDirectorate();
$subdir 		= new SubDirectorate( "", "", "" ,"");
$subdirectorate = $subdir -> getASubDirectorate( $_REQUEST['id'] );
?>
<script>
$(function(){
		$("table#edit_subdirectorate_table").find("th").css({"text-align":"left"})
});
</script>
    <div>
    <form id="edit-subdirectorate-form" name="edit-subdirectorate-form" method="post">
    <div id="subdirectorate_message"></div>
    <table border="1" id="edit_subdirectorate_table">
    <tr>
    	 <th>Ref:</th>
    	 <td><?php echo $_REQUEST['id']; ?></td>
    </tr>
          <tr>
      	<th>Short Code:</th>
        <td>
       <textarea name="sub_shortcode" id="sub_shortcode"><?php echo $subdirectorate['shortcode']; ?></textarea>
        </td>
      </tr>
      <tr>
        <th>Department:</th>
        <td>
            <select name="subdir_dpt" id="subdir_dpt">
           	 <option value=""><?php echo $select_box_text ?></option>
             <?php foreach($directorates  as $directorate) {
			 ?>
               <option value="<?php echo $directorate['id']; ?>"
               	<?php echo (trim($directorate['id'])==trim($subdirectorate['department']) ? "selected" : "")?>
               ><?php echo $directorate['department']; ?></option>
             <?php
			 }  ?>
            </select>
        </td>
      </tr>
      <tr>
        <th>Division:</th>
        <td><input type="text" name="subdir_division" id="subdir_division" value="<?php echo $subdirectorate['department']; ?>" /></td>
      </tr>

      <tr>
      <tr>
      	<th>Responsible for risk:</th>
        <td>
            <select name="subdir_user" id="subdir_user">
           	 <option value=""><?php echo $select_box_text ?></option>
             <?php foreach($users  as $user) {
			 ?>
               <option value="<?php echo $user['tkid']; ?>"
               	<?php echo (trim($subdirectorate['user_responsible'])==trim($user['tkid']) ? "selected" : "")?>
               ><?php echo $user['tkname']." ".$user['tksurname']; ?></option>
             <?php
			 }  ?>
            </select>
       </td>
      </tr>
      <tr>
      <tr>     
      	<th></th>
        <td>
        <input type="hidden" name="subdir_id" id="subdir_id" value="<?php echo $_REQUEST['id']?>" />
        <input type="submit" name="edit_subdir" id="edit_subdir" value="Save Changes" />
        <input type="submit" name="cancel_subdir" id="cancel_subdir" value="Cancel" />
        </td>
      </tr>
      <tr>
      	<td align="left" colspan="2">
        	<?php $me->displayGoBack("",""); ?>
        </td>
        </tr>
    </table>
    </form>
    </div>
