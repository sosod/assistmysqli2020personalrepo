<?php
@session_start();
include_once("../inc/init.php");

	switch( ( isset($_REQUEST['action']) ?  $_REQUEST['action'] : "" ) ){
	case "financial_year":
		$fnyr = new FinancialYear( $_REQUEST['last_day'], $_REQUEST['start_date'], $_REQUEST['end_date'] );
		echo json_encode( $fnyr -> saveFinYear() );
		break;
	case "getFinYear":
		$fnyr = new FinancialYear( "", "", "" );
		echo json_encode( $fnyr -> getFinYear() );
		break;
	case "editFinancialYear":
		$fnyr = new FinancialYear( $_REQUEST['last_day'], $_REQUEST['start_date'], $_REQUEST['end_date'] );
		echo json_encode( $fnyr -> updateFinacialYear( $_REQUEST['id']) );
		break;		
	case "delFinYear":
		$fnyr = new FinancialYear( "", "", "" );
		echo json_encode($fnyr -> deleteFinacialYear( $_REQUEST['id']));
		break;
	case "menu":
		$menu = new Menu();
		$menu -> updateMenuName( $_REQUEST['name'] , $_REQUEST['value'] );
		break;
	case "getMenu":
		$menu = new Menu();
		echo json_encode( $menu -> getMenu() );
		break;
	case "getAllMenu":
		$menu = new Menu();
		echo json_encode( $menu -> getAllMenu() );		
		break;
	case "getNaming":
		$nm = new Naming();
		echo json_encode( $nm -> getNaming() );
 		break;
	case "getActionHeaderNamesForSetup":
		$nm = new Naming();
		echo json_encode( $nm -> getActionHeaderNamesForSetup() );
 		break;
	case "getHeaderNames":
		$nm = new Naming();
		echo json_encode( $nm -> getHeaderNames() );
 		break;		
	case "updateHeaderNames":
		$nm = new Naming();
		echo json_encode( $nm -> updateHeaderNames( $_REQUEST['fieldname'], $_REQUEST['value'] ) );
		break;
	case "getFieldNames":
		$nm = new Naming();
		echo json_encode( $nm -> getFieldNames() );	
		break;
	case "naming":
		$nm = new Naming();
		$nm -> saveNaming( $_REQUEST['fieldname'], $_REQUEST['value'] ,1);
 		break;
	case "riskstatus":
		$stat = new RiskStatus( $_REQUEST['name'], $_REQUEST['client_terminology'], $_REQUEST['color']);
		$res 	 = $stat -> saveStatus();
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Query status saved", "error" => false, "id" => $res);
		} else {
			$response = array("text" => "Error saving the query status", "error" => true);
		}
		echo json_encode( $response );
		break;
	case "getRiskstatus":
		$getstat = new RiskStatus( "", "", "" );
		echo json_encode( $getstat -> getStatus() );		
		break;
	case "editRiskStatus":
		$getstat = new RiskStatus( $_REQUEST['name'], $_REQUEST['client_terminology'], $_REQUEST['color']);
		echo json_encode( $getstat -> updateStatus( $_REQUEST['id']) );	
		break;
	case "updaterisk":
		$up = new RiskStatus( $_REQUEST['name'], $_REQUEST['client_terminology'], $_REQUEST['color']);
		$up-> updateStatus( $_REQUEST['id'] );		
		break;
	case "changestatus":
		$deact = new RiskStatus( "", "", "");
		$res = $deact -> changeStatus( $_REQUEST['id'], $_REQUEST['active'] );
		$response = array();
		if( $res == 1)
		{
			$response = array("text" => "Query status successfully updated ", "error" => false );				
		} else if( $res == 0) {
			$response = array("text" => "No change was made to the query status ", "error" => false );			
		} else {
			$response = array("text" => "Error updating the query status", "error" => true );
		}	
		echo json_encode( $response );	
		break;
	case "deleteStatus":
		$act = new RiskStatus( "", "", "");
		$response = $act -> changeStatus($_REQUEST, 2);
		break;
	case "activate":
		$act = new RiskStatus( "", "", "");
		$act -> activateStatus( $_REQUEST['id'] );		
		break;
	case "getActionStatus":
		$actionstatus = new ActionStatus( "", "", "" );
		$actionstatus  -> getStatus();	
		break;
	case "updateActionStatus":
		$up = new ActionStatus( $_REQUEST['name'], $_REQUEST['client_term'], $_REQUEST['color']);
		echo json_encode( $up-> updateStatus( $_REQUEST['id'] ) );		
		break;
	case "changeActionstatus":
		$deactionstatus = new ActionStatus( "", "", "");
		$deactionstatus -> changeActionStatus( $_REQUEST['id'], $_REQUEST['status'] );		
		break;
	case "activateactionstatus":
		$actactionstatus = new ActionStatus( "", "", "");
		$actactionstatus -> activateStatus( $_REQUEST['id'] );		
		break;
	case "newActionStatus":
		$statObj = new ActionStatus( $_REQUEST['name'], $_REQUEST['client_term'], $_REQUEST['color']);
		$res 	 = $statObj -> saveStatus();
		$response = array();
		if( $res > 0)
		{
			$response = array("text" => "Query status saved", "error" => false, "id" => $res);
		} else {
			$response = array("text" => "Error saving the query status", "error" => true);
		}
		echo json_encode( $response );
		break;
	case "getUsedTypes":
		$type = new QueryType( "", "", "", "" );
		echo json_encode( $type-> getUsedTypes() );	 
		break;		
	case "getQueryType":
		$type = new QueryType( "", "", "", "" );
		echo json_encode( $type-> getType() );	 
		break;
	case "updateQueryType":
		$type = new QueryType( $_REQUEST['shortcode'], $_REQUEST['name'], $_REQUEST['description'], "" );
		echo json_encode( $type-> updateType( $_REQUEST['id']) ) ;	
		break;		
	case "changeQueryStatus":
		$type = new QueryType( "", "", "", "" );
		$type-> changeTypeStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "QueryActivate":
		$type = new QueryType( "", "", "", "" );
		$type-> activateType( $_REQUEST['id'] );	
		break;
	case "newQueryType":
		$type = new QueryType( $_REQUEST['shortcode'], $_REQUEST['name'], $_REQUEST['description'], "" );
		$type-> saveType( );	
		break;
	case "getRiskTypes":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getUsedTypes() );
		break;
	case "getRiskType":
		$type = new RiskType( "", "", "", "" );
		echo json_encode( $type-> getType() );
		break;
	case "updateRiskType":
		$type = new RiskType( $_REQUEST['shortcode'], $_REQUEST['name'], $_REQUEST['description'], "" );
		echo json_encode( $type-> updateType( $_REQUEST['id']) ) ;
		break;
	case "changetypestatus":
		$type = new RiskType( "", "", "", "" );
		$type-> changeTypeStatus( $_REQUEST['id'], $_REQUEST['active'] );
		break;
	case "typeactivate":
		$type = new RiskType( "", "", "", "" );
		$type-> activateType( $_REQUEST['id'] );
		break;
	case "newRiskType":
		$type = new RiskType( $_REQUEST['shortcode'], $_REQUEST['name'], $_REQUEST['description'], "" );
		$type-> saveType( );
		break;
	case "getCategory":
		$riskcat = new RiskCategory( "", "", "", "" );
		echo json_encode( $riskcat  -> getCategory() );
		break;
	case "updateCategory":
		$riskcat = new RiskCategory( $_REQUEST['category'], $_REQUEST['shortcode'], $_REQUEST['description'], $_REQUEST['type_id']);
		echo json_encode( $riskcat  -> updateCategory( $_REQUEST['id']) );
		break;
	case "newCategory":
		$riskcat = new RiskCategory( $_REQUEST['category'], $_REQUEST['shortcode'], $_REQUEST['description'], $_REQUEST['type_id'] );
		$riskcat  -> saveCategory();
		break;
	case "catdeactivate":
		$type = new RiskCategory( "", "", "", "" );
		$type-> deactivateCategory( $_REQUEST['id'], $_REQUEST['active'] );	
		break;
	case "catactivate":
		$type = new RiskCategory( "", "", "", "" );
		$type-> activateCategory( $_REQUEST['id'] );	
		break;
	case "getImpact":
		$imp = new Impact( "", "", "", "", "");
		echo json_encode( $imp -> getImpact() );
		break;
	case "changeImpstatus":
		$impact = new Impact( "", "", "", "", "");
		$impact -> updateImpactStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "impactactivate":
		$impact = new Impact( "", "", "", "", "");
		$impact -> activateImpact( $_REQUEST['id'] );	
		break;
	case "newImpact":
		$impact = new Impact( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['assmnt'], $_REQUEST['dfn'], $_REQUEST['clr']);
		$impact -> saveImpact();	
		break;
	case "updateImpact":
		$impact = new Impact( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['assmnt'], $_REQUEST['dfn'], $_REQUEST['clr']);
		echo json_encode( $impact -> updateImpact( $_REQUEST['id'] ) );	
		break;
	case "getLikelihood":
		$like = new Likelihood( "", "", "", "", "","");
		echo json_encode( $like -> getLikelihood() );
		break;
	case "changeLikestat":
		$like = new Likelihood( "", "", "", "", "", "");
		$like -> updateLikelihoodStatus( $_REQUEST['id'] , $_REQUEST['status'] );	
		break;
	case "likeactivate":
		$like = new Likelihood( "", "", "", "", "", "");
		$like -> activateLikelihood( $_REQUEST['id'] );	
		break;
	case "newLikelihood":
		$like = new Likelihood( 
						$_REQUEST['from'], $_REQUEST['to'], $_REQUEST['assmnt'], $_REQUEST['dfn'], $_REQUEST['prob'], $_REQUEST['clr']
					);
		$like -> saveLikelihood();	
		break;
	case "updateLikelihood":
		$like = new Likelihood( 
						$_REQUEST['from'],
						$_REQUEST['to'],
						$_REQUEST['assmnt'],
						$_REQUEST['dfn'],
						$_REQUEST['prob'],
						$_REQUEST['clr']
					);
		echo json_encode( $like -> updateLikelihood( $_REQUEST['id']) );	
		break;		
	case "getInherentRisk":
		$ihr= new InherentRisk( "", "", "", "", "");
		$ihr -> getInherentRisk();
		break;
	case "changeInherent":
		$ihr = new InherentRisk( "", "", "", "", "");
		$ihr -> updateInherentRiskStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "ihractactivate":
		$ihr = new InherentRisk( "", "", "", "", "");
		$ihr -> activateInherentRisk( $_REQUEST['id'] );	
		break;
	case "newInherentRisk":
		$ihr = new InherentRisk( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['magnitude'], $_REQUEST['response'], $_REQUEST['clr']);
		$ihr -> saveInherentRisk();	
		break;
	case "updateInherentRisk":
		$ihr = new InherentRisk( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['magnitude'], $_REQUEST['response'], $_REQUEST['clr']);
		echo $ihr -> updateInherentRisk( $_REQUEST['id'] );	
		break;

	case "getControl":
		$ihr= new ControlEffectiveness( "", "", "", "", "");
		echo json_encode( $ihr -> getControlEffectiveness() );
		break;
	case "changeControl":
		$ihr = new ControlEffectiveness( "", "", "", "", "");
		$ihr -> updateControlEffectivenessStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "ctrlactivate":
		$ihr = new ControlEffectiveness( "", "", "", "", "");
		$ihr -> activateControlEffectiveness( $_REQUEST['id'] );	
		break;
	case "newControl":
		$ihr = new ControlEffectiveness( $_REQUEST['ctrl_shortcode'], $_REQUEST['ctrl'], $_REQUEST['qual'], $_REQUEST['rating'], $_REQUEST['clr']);
		$ihr -> saveControlEffectiveness();	
		break;
	case "updateControl":
		$ihr = new ControlEffectiveness( $_REQUEST['ctrl_shortcode'], $_REQUEST['ctrl'], $_REQUEST['qual'], $_REQUEST['rating'], $_REQUEST['clr']);
		echo $ihr -> updateControlEffectiveness( $_REQUEST['id'] );	
		break;
	case "getResidualRisk":
		$resrisk = new ResidualRisk( "", "", "", "", "");
		$resrisk -> getResidualRisk();
		break;
	case "changeResidualStatus":
		$resrisk = new ResidualRisk( "", "", "", "", "");
		$resrisk -> updateResidualRiskStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "residualactivate":
		$resrisk = new ResidualRisk( "", "", "", "", "");
		$resrisk -> activateResidualRisk( $_REQUEST['id'] );	
		break;
	case "newResidualRisk":
		$resrisk = new ResidualRisk( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['magn'], $_REQUEST['resp'], $_REQUEST['clr']);
		$resrisk -> saveResidualRisk();	
		break;
	case "updateResidualRisk":
		$resrisk = new ResidualRisk( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['magn'], $_REQUEST['resp'], $_REQUEST['clr']);
		echo $resrisk -> updateResidualRisk( $_REQUEST['id'] );	
		break;
	case "newUserAccess":
		$uaccess = new UserAccess( $_REQUEST['user'], $_REQUEST['modadmin'], $_REQUEST['cr_risk'], isset($_REQUEST['cr_actions']) ? $_REQUEST['cr_actions'] : 0, $_REQUEST['c_import'],
		 $_REQUEST['v_all'], $_REQUEST['e_all'], $_REQUEST['reports'], $_REQUEST['assurance'], $_REQUEST['setup'] );
		$uaccess -> saveUserAccess();
		break;
	case "getUsers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUsers() );
		break;
	case "getUsersForSetup":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUsersNotYetSetup() );
		break;
	case "updateUserAccess":
		$uaccess = new UserAccess( $_REQUEST['user'], $_REQUEST['modadmin'], $_REQUEST['cr_risk'],  isset($_REQUEST['cr_actions']) ?$_REQUEST['cr_actions']:0,  $_REQUEST['c_import'],
		 $_REQUEST['v_all'], $_REQUEST['e_all'], $_REQUEST['reports'], $_REQUEST['assurance'], $_REQUEST['setup'] );
		echo json_encode( $uaccess ->updateUserAccess() );
		break;		
	case "getUserAccess":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getUserAccess() );
		break;
	case "setRiskManager":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		$uaccess -> setRiskManager( $_REQUEST['id'], $_REQUEST['status']);
		break;
	case "getRiskManagers":
		$uaccess = new UserAccess( "", "", "", "", "", "", "", "", "" );
		echo json_encode( $uaccess -> getRiskManager() );		
		break;	
	case "newDirectorate":
		$dir = new Directorate( $_REQUEST['shortcode'], $_REQUEST['fxn'], $_REQUEST['user'] ,$_REQUEST['dpt']);
		$dir -> saveDirectorate();
		break;
	case "updateDirectorate":
		$dir = new Directorate( $_REQUEST['shortcode'], $_REQUEST['fxn'], $_REQUEST['user'] ,$_REQUEST['dpt']);
		echo $dir -> updateDirectorate( $_REQUEST['id'] );
		break;
	case "getDirectorate":
		$dir = new Directorate( "", "", "" ,"");
		echo json_encode( $dir -> getDirectorate() );
		break;
	case "changeDireStatus":
		$dir = new Directorate( "", "", "" ,"");
		$dir -> updateDirectorateStatus( $_REQUEST['id'], $_REQUEST['status'] );
		break;
	case "dire_activate":
		$dir = new Directorate( "", "", "" ,"");
		$dir -> activateDirectorate( $_REQUEST['id'] );
		break;
	case "newSubDirectorate":
		$dir = new SubDirectorate( $_REQUEST['shortcode'], $_REQUEST['fxn'], $_REQUEST['user'] ,$_REQUEST['dpt']);
		$dir -> saveSubDirectorate();
		break;
	case "updateSubDirectorate":
		$dir = new SubDirectorate( $_REQUEST['shortcode'], $_REQUEST['fxn'], $_REQUEST['user'] ,$_REQUEST['dpt']);
		echo $dir -> updateSubDirectorate( $_REQUEST['id'] );
		break;
	case "getSubDirectorate":
		$dir = new SubDirectorate( "", "", "" ,"");
		echo json_encode( $dir -> getSubDirectorate() );
		break;
	case "changeSubDirStatus":
		$dir = new SubDirectorate( "", "", "" ,"");
		$dir -> updateSubDirectorateStatus( $_REQUEST['id'], $_REQUEST['status'] );
		break;
	case "updateDefaults":
		$def = new Defaults();
		$def -> updateDefaults( $_REQUEST['name'], ($_REQUEST['value'] == "yes" ? "1" : "0") );
		break;
	case "subdir_activate":
		$dir = new SubDirectorate( "", "", "" ,"");
		$dir -> activateSubDirectorate( $_REQUEST['id'] );
		break;
	case "saveGlossary":
		$glosssary = new Glossary();
		$glosssary -> saveGlossary();
		break;
	case "getGlossary":
		$glosssary = new Glossary();
		echo json_encode( $glosssary -> getGlossary() ); 
		break;
	case "getAGlossary":
		$glosssary = new Glossary();
		echo json_encode( $glosssary -> getAGlossary() ); 
		break;	
	case "updateGlossary":
		$glosssary = new Glossary();
		echo $glosssary -> updateGlossary( $_REQUEST['id']); 
		break;	
	case "deleteGlossary":
		$glosssary = new Glossary();
		echo $glosssary -> deleteGlossary( $_REQUEST['id'] ); 
		break;	
	case "getColumns":
		$cols = new Columns();
		echo json_encode( array( "active" => $cols -> getActiveColumns() , "inactive"=>  $cols -> getInActiveColumns() ) ); 
		break;	
	case "saveColumnOrder":
		$cols = new Columns();
		parse_str( $_REQUEST['active'], $active ); 
		parse_str( $_REQUEST['inactive'], $inactive ); 						
		$cols -> saveColumnsOrder( $active['column'], $inactive['column']);
		break;	
	case "newUDF":
		$udf 	  = new UDF();
		$response = array();
		if( $udf -> saveUDF() > 0 ){
			$response = array( "error" => false, "text" => "UDF saved succesifully" );
		} else {
			$response = array( "error" => true, "text" => "An error occured saving the UDF" );			
		}
		echo json_encode( $response );
		break;
	case "getUDF":
		$udf 	  = new UDF();
		echo json_encode( $udf -> getUDF() );		
		break;		
	case "deleteUDF":
		$udf 	  = new UDF();
		$response = array();
		if( $udf -> deleteUDF( $_REQUEST['id'] ) == 1 ) {
			$response = array("error" => false, "text" => "UDF deleted");
		} else if( $udf -> deleteUDF( $_REQUEST['id'] ) == 0 ){
			$response = array("error" => true, "text" => "No changes were made ");
		} else {
			$response = array("error" => true, "text" => "An error occured deleting the data");
		}	
		echo json_encode( $response );			
		break;	
	case "getDefaults":
		$def = new Defaults();
		echo json_encode( $def -> getDefaults() );
		break;
	case "getRiskLevels":
		$level = new RiskLevel( "", "", "", "" );
		echo json_encode( $level->getLevel() );	 
		break;
	case "updateRiskLevel":
		$level = new RiskLevel( $_REQUEST['shortcode'], $_REQUEST['name'], $_REQUEST['description'], $_REQUEST['status'] );
		echo json_encode( $level-> updateLevel( $_REQUEST['id']) ) ;	
		break;		
	case "changelevelstatus":
		$level = new RiskLevel( "", "", "", "" );
		$level-> changeLevelStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "deActivateLevel":
		$level = new RiskLevel( "", "", "", "" );
		$level-> changeLevelStatus( $_REQUEST['id'], $_POST['status'] );	
		break;		
	case "typeactivate":
		$level = new RiskLevel( "", "", "", "" );
		$level-> activateLevel( $_REQUEST['id'] );	
		break;
	case "saveRiskLevel":
		$level = new RiskLevel( $_REQUEST['shortcode'], $_REQUEST['name'], $_REQUEST['description'], $_REQUEST['status'] );
		$level-> saveLevel( );	
		break;
	case "getFinancialExposure":
		$finExp = new FinancialExposure( "", "", "", "", "");
		$res = $finExp -> getFinancialExposure();
		echo json_encode($res);
		break;
	case "changeFinancialExposureStatus":
		$finExp = new FinancialExposure( "", "", "", "", "");
		$finExp -> updateFinancialExposureStatus( $_REQUEST['id'], $_REQUEST['status'] );	
		break;
	case "FinancialExposureActivate":
		$finExp = new FinancialExposure( "", "", "", "", "");
		$finExp -> activateFinancialExposure( $_REQUEST['id'] );	
		break;
	case "newFinancialExposure":
		$finExp = new FinancialExposure( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['name'], $_REQUEST['definition'], $_REQUEST['color']);
		$finExp -> saveFinancialExposure();	
		break;
	case "updateFinancialExposure":
		$finExp = new FinancialExposure( $_REQUEST['from'], $_REQUEST['to'], $_REQUEST['name'], $_REQUEST['definition'], $_REQUEST['color']);
		echo json_encode( $finExp -> updateFinancialExposure( $_REQUEST['id'] ) );	
		break;
    case "view_log":
        $logObj  = new Logs();
        $logs    = $logObj -> viewLogs( $_POST['table_name'] );
        echo json_encode($logs);
        break;
    case "getActionColumns":
    	$actionObj = new ActionColumns();
    	$columns   = $actionObj -> getNaming();
    	echo json_encode( $columns );
    	break;
    case "getQueryColumns":
    	$qObj    = new QueryColumns();
    	$columns = $qObj -> getNaming();
    	echo json_encode($columns);
    	break;
    case "updateColumns":
    	$qObj = new QueryColumns();
    	$positions = array();
    	$manage    = array();
    	$new	   = array();

    	foreach( $_POST['data'] as $index => $cols )
    	{
    		if( $cols['name'] == "position")
    		{
    			array_push($positions, $cols['value']);     			
    		}
    		
    		if($cols['name']  == "manage")
    		{
    			array_push($manage, $cols['value'] );    			
    		}

    		if( $cols['name'] == "new")
    		{
    			array_push($new, $cols['value']);    			
    		}		
    	}
    	$res = "";
    	foreach($positions as $pos => $id)
    	{
    		$updateArr = array();
    		if( in_array($id, $manage))
    		{
    			$updateArr = array("ordernumber" => $pos, "active" => 4 );    			
    		} else {
    			$updateArr = array("ordernumber" => $pos, "active" => 0 );
    		}
    		
    		if( in_array($id, $new))
    		{
    			$updateArr = array("ordernumber" => $pos , "active" => $updateArr['active'] + 8 );
    		} else {
    			$updateArr = array("ordernumber" => $pos , "active" => $updateArr['active']  );
    		}
    		$res += $qObj -> update("header_names", $updateArr, "id={$id}"); 
    	}
    	$response = array();
    	if( $res > 0)
    	{
    		$response = array("text" => "Query columns have successfully been updated", "error" => false);
    	} else {
    		$response = array("text" => "Error updating query columns", "error" => true);    		
    	}
    	echo json_encode( $response ); 
    	break;	
    case "getAllactionassurancestatus":
      $actionAssuranceObj = new ActionAssuranceStatus();
      $assurances         = $actionAssuranceObj -> getAll();
      echo json_encode($assurances);
    break;
    case "getAllqueryassurancestatus":
      $actionAssuranceObj = new QueryAssuranceStatus();
      $assurances         = $actionAssuranceObj -> getAll();
      echo json_encode($assurances);
    break;    
    case "saveactionassurancestatus":
      $actionAssuranceObj = new ActionAssuranceStatus();
      $res                = $actionAssuranceObj -> save($_POST);
      $response           = array();
      if($res > 0)
      {
        $response = array('text' => 'Action assurance status successfully saved', 'error' => false);
      } else {
        $response = array('text' => 'Error saving the action assurance status', 'error' => true);
      }
      echo json_encode($response);     
    break;
    case "savequeryassurancestatus":
      $actionAssuranceObj = new QueryAssuranceStatus();
      $res                = $actionAssuranceObj -> save($_POST);
      $response           = array();
      if($res > 0)
      {
        $response = array('text' => 'Query assurance status successfully saved', 'error' => false);
      } else {
        $response = array('text' => 'Error saving the query assurance status', 'error' => true);
      }
      echo json_encode($response);     
    break;  
    case "updateactionassurancestatus":
      $actionAssuranceObj = new ActionAssuranceStatus();
      $res                = $actionAssuranceObj -> updateAssurance($_POST['id'], $_POST);
      $response           = array(); 
      if($res > 0)
      {
        $response = array('text' => 'Query assurance status successfully saved', 'error' => false, 'updated' => true);
      } elseif($res == 0) {
        $response = array('text' => 'No change made to action assurance status', 'error' => false, 'updated' => false);
      } else {
        $response = array('text' => 'Error saving the query assurance status', 'error' => true);
      }
      echo json_encode($response);  
    break;  
    case "updatequeryassurancestatus":
      $actionAssuranceObj = new QueryAssuranceStatus();
      $res                = $actionAssuranceObj -> updateAssurance($_POST['id'], $_POST);
      $response           = array();
      if($res > 0)
      {
        $response = array('text' => 'Query assurance status successfully updated', 'error' => false, 'updated' => true);
      } elseif($res == 0) {
        $response = array('text' => 'No change made to the query assurance status', 'error' => false, 'updated' => false);
      } else {
        $response = array('text' => 'Error updating the query assurance status', 'error' => true);
      }
      echo json_encode($response);  
    break;
    case "getQuerySources":
      $sourceObj = new QuerySource();
      echo json_encode($sourceObj -> getQuerySources());
    break;  
    case "saveQuerySource":
      $sourceObj = new QuerySource();
      $res       = $sourceObj -> saveQuerySource($_POST);
      $response  = array();
      if($res > 0)
      {
        $response = array('text' => 'Query source successfully saved', 'error' => false);
      } else {
        $response = array('text' => 'Error saving the query source', 'error' => true);
      }
      echo json_encode($response);
      exit();
    break;
    case "updateQuerySource":
       $sourceObj    = new QuerySource();
       $query_source = $sourceObj -> getQuerySource($_POST['id']);
       $update_data  = array();
       $updates      = array();
       $changes      = array();
       $change_arr   = array();
       if(!empty($query_source))
       {
          foreach($query_source as $q_index => $q_source)
          {
             if(isset($_POST[$q_index]))
             {
                if($_POST[$q_index] !== $q_source)
                {
                   if($q_index == "status")
                   {
                      if($_POST[$q_index] == 0)
                      {
                        $change_arr['status_'] = "Query Source has been deactivated";
                      } elseif($_POST[$q_index] == 1){
                        $change_arr['status_'] = "Query Source has been activated";
                      } elseif($_POST[$q_index] == 2){
                         $change_arr['status_'] = "Query Source has been deleted";
                      }
                      $update_data[$q_index] = $_POST[$q_index];
                   } else {
                      $change_arr[$q_index]  = array('from' => $q_source, 'to' => $_POST[$q_index]);
                      $update_data[$q_index] = $_POST[$q_index]; 
                   }
                }
             }
          }
       }
       if(!empty($change_arr))
       {
          $changes['ref_']       = "Ref #".$_POST['id']." has been changed";
          $changes['user']       = $_SESSION['tkn'];
          $changes               = array_merge($changes, $change_arr);
          $updates['changes']    = base64_encode(serialize($changes));
          $updates['setup_id']   = $_POST['id'];
          $updates['insertuser'] = $_SESSION['tid'];
       }
       $res      = $sourceObj -> updateQuerySource($_POST['id'], $update_data, $updates);
       $response = array();
       if($res > 0)
       {
         $response = array('text' => 'Query Source successfully updated', 'error' => false, 'updated' => true);
       } elseif($res == 0){
         $response = array('text' => 'No change made to query source ', 'error' => false, 'updated' => false);
       } else {
         $response = array('text' => 'Error updating query source ', 'error' => true, 'updated' => false);
       }
       echo json_encode($response);
       exit();
    break;
    default:
		require_once("/views/".$_REQUEST['a'].".php");
		break;
	}
?>
