<?php //error_reporting(-1);
$scripts = array( 'useraccess.js','menu.js',  );
$styles = array( 'colorpicker.css' );
$page_title = "User Access";
require_once("../inc/header.php");
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());
?>
<div>
<form method="post" id="useraccess-form" name="useraccess-form">
<div id="useraccess_message" class="message"></div>
<table border="1" id="useraccess_table">
  <tr>
    <th>Ref</th>
    <th>User</th>
    <th>Module<br />Admin</th>
    <th>Create<br />Queries</th>
    <th>Import<br />Queries</th>
    <!-- <th>Create Actions</th> -->
    <th>Update<br />All</th>
    <th>Edit All</th>
    <th>Reports</th>
    <th>Assurance</th>
    <th>Setup</th>
    <th>Action</th>
  </tr>
  <tr>
    <td>#</td>
    <td>
		<select name="userselect" id="userselect">
        	<option value=""><?php $select_box_text = isset($select_box_text) ? $select_box_text : ""; echo $select_box_text; ?></option>
        </select>
    </td>
    <td>
    	<select id="module_admin" name="module_admin">
            <option value="1">Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>
    <td>
    	 <select id="create_risks">
            <option value="1">Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>
    <td>
    	 <select id="can_import">
            <option value="1">Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>
    <!-- <td>
    	 <select id="create_actions" name="create_actions">
        	<option>--create actions--</option>
            <option value="1" selected="selected">Yes</option>
            <option value="0">No</option>            
        </select>
    </td> -->    
    <td>
    	 <select id="view_all" name="view_all">
            <option value="1" >Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>
    <td>
    	<select id="edit_all" name="edit_all">
            <option value="1">Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>
    <td>
    	 <select id="reports" name="reports">
            <option value="1" selected="selected">Yes</option>
            <option value="0">No</option>            
        </select>
    </td>
    <td>
    	 <select id="assurance" name="assurance">
            <option value="1">Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>    
    <td>
    	 <select id="usersetup" name="usersetup">
            <option value="1">Yes</option>
            <option value="0" selected="selected">No</option>            
        </select>
    </td>
    <td><input type="submit" value="Add" id="setup_access" name="setup_access" /></td>
  </tr>
</table>
<div>
	<table class="noborder" id="user_log">
	<tr>
		<td class="noborder"><?php $me->displayGoBack("", ""); ?></td>
		<td class="noborder"><?php $admire_helper->displayAuditLogLink( "useraccess_logs", true); ?></td>
	</tr>
	</table>
</div>
</form>
</div>
<script type=text/javascript>
$(function() {
	var tr = 0;
	$("#useraccess_table tr").each(function() {
		$(this).children().css("text-align","center");
	});
	var u_access_width = $("#useraccess_table tr").width();
	$("#user_log").width(u_access_width);
	//$("#useraccess_table tr:first").next().children().css("text-align","center");
	//$("#useraccess_table tr:first").next().children().css("text-align","center");
});
</script>