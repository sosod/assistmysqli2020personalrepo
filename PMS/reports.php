<?php
include("inc_ignite.php");
include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Reports" => null ) ); ?>

<table cellspacing="0" class="info">
  <tr>
    <th style="width:120px;">Fishbowl Report</th>
    <td style="width:350px">View the employee KPA evaluations</td>
    <td class="action"><? ViewHelper::button( "reports_fishbowl.php", "View" ); ?></td>
  </tr>
  <tr>
    <th style="width:120px;">Bell curve graphs</th>
    <td style="width:350px">View the KPA Bell Curve graphs for departments and job levels</td>
    <td class="action"><? ViewHelper::button( "reports_bellcurve.php", "View" ); ?></td>
  </tr>
  <tr>
    <th style="width:120px;">Career Development</th>
    <td style="width:350px">View the employee learning activities</td>
    <td class="action"><? ViewHelper::button( "reports_learning.php", "View" ); ?></td>
  </tr>
</table>
    

<?php
include("inc_footer.php");
?>