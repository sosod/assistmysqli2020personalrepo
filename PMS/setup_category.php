<?
include("inc_ignite.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $category = new Category();
  $category->init( $_REQUEST , true );
  
  if( $category->getWeights() && $category->getTotalKPAWeight() != 100 )
  {
    $errors = array();
    $errors[] = "The KPA weights need to add up to 100%."; 
  }
  else
  {
    $category->save();
  
    ViewHelper::redirect( "setup_categoryinfo.php?id=" . $category->getId() );  
  }
}
else
{
  $category = Category::load( $_REQUEST["id"] );
}
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Categories" => "setup_categories.php", $category->getName() => null ) ); ?>

<? ViewHelper::validation( "frmCategory" ); ?>

<? ViewHelper::errors( $errors ); ?>

<form id="frmCategory" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Name</th>
      <td><input type="text" name="name" value="<?= $category->getName() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Evaluation frequency</th>
      <td><?= ViewHelper::select( "period_frequency", ViewHelper::getPeriodFrequencySelectList(), $category->getPeriodFrequency(), true, false ) ?></td>
    </tr>
    <tr>
      <th>Use weights?</th>
      <td><?= ViewHelper::select( "weights", ViewHelper::getYesNoSelectList(), $category->getWeights(), true, false ) ?></td>
    </tr>
    <tr>
      <th>Min KPAs</th>
      <td><?= ViewHelper::select( "min_kpas", ViewHelper::getNumericSelectList( 1, 10 ), $category->getMinKPAs(), true, false ) ?></td>
    </tr>
    <tr>
      <th>Max KPAs</th>
      <td><?= ViewHelper::select( "max_kpas", ViewHelper::getNumericSelectList( 1, 30 ), $category->getMaxKPAs(), true, false ) ?></td>
    </tr>
    <tr>
      <th>Organisational KPA weight</th>
      <td><?= ViewHelper::select( "org_kpas_weight", ViewHelper::getNumericSelectList( 0, 100 ), $category->getOrganisationKPAWeight(), true, false ) ?>%</td>
    </tr>
    <tr>
      <th>Learning KPA weight</th>
      <td><?= ViewHelper::select( "learn_kpas_weight", ViewHelper::getNumericSelectList( 0, 100 ), $category->getLearningKPAWeight(), true, false ) ?>%</td>
    </tr>
    <tr>
      <th>Job KPA weight</th>
      <td><?= ViewHelper::select( "job_kpas_weight", ViewHelper::getNumericSelectList( 0, 100 ), $category->getJobKPAWeight(), true, false ) ?>%</td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $category->getId() ?>" />
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"setup_categories.php" ) ); ?>

<? include("inc_footer.php"); ?>