<?
if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $learningActivity = new LearningActivity();
  $learningActivity->init( $_REQUEST , true );
  $learningActivity->save();
  
  ViewHelper::redirect( "{$section}.php#learningactivities" );
}
else
{
  $learningActivity = LearningActivity::load( $_REQUEST["id"] );
}
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( ucwords( $section ) => "{$section}.php", $selectedUser->getFullName() => null, "Learning Activity" => null ) ); ?>

<? include("{$section}_inc_summary.php"); ?>

<? ViewHelper::validation( "frmLearningActivity" ); ?>

<h2>Learning Activity</h2>
<form id="frmLearningActivity" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Name</th>
      <td><input type="text" name="name" value="<?= $learningActivity->getName() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Priority</th>
      <td><?= ViewHelper::select( "priority", LearningActivity::prioritySelectList(), $learningActivity->getPriority(), true ) ?></td>
    </tr>
    <tr>
      <th>Name of training provider</th>
      <td><input type="text" name="provider" value="<?= $learningActivity->getProvider() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Suggested training intervention</th>
      <td><textarea name="intervention" cols="40" rows="4" class="required"><?= $learningActivity->getIntervention() ?></textarea></td>
    </tr>
    <tr>
      <th>Mode of delivery</th>
      <td><textarea name="delivery_mode" cols="40" rows="4" class="required"><?= $learningActivity->getDeliveryMode() ?></textarea></td>
    </tr>
    <tr>
      <th>Suggested Time frames</th>
      <td><textarea name="time_frames" cols="40" rows="4" class="required"><?= $learningActivity->getTimeFrames() ?></textarea></td>
    </tr>
    <tr>
      <th>Outcomes expected</th>
      <td><textarea name="outcome" cols="40" rows="4" class="required"><?= $learningActivity->getOutcome() ?></textarea></td>
    </tr>
    <tr>
      <th>Work opportunity</th>
      <td><textarea name="work_opportunity" cols="40" rows="4" class="required"><?= $learningActivity->getWorkOpportunity() ?></textarea></td>
    </tr>
    <tr>
      <th>Support person</th>
      <td><?= ViewHelper::select( "support_person_id", User::selectList(), $learningActivity->getSupportPersonId(), true ) ?></td>
    </tr>
    <tr>
      <th>Status</th>
      <td><?= ViewHelper::select( "status", LearningActivity::statusSelectList(), $learningActivity->getStatus(), true, false ) ?></td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $learningActivity->getId() ?>" />
  <input type="hidden" name="user_id" value="<?= $selectedUser->getId() ?>" />
  <? if( $learningActivity->isTransient() ) { ?>
  <input type="hidden" name="created_by_id" value="<?= getLoggedInUserId() ?>" />  
  <? } ?>
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"{$section}.php#learningactivities" ) ); ?>

<? include("inc_footer.php"); ?>