<?
include("inc_ignite.php");
include("activation_logic.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $jobFunction = new JobFunction();
  $jobFunction->init( $_REQUEST , true );
  $jobFunction->save();
  
  ViewHelper::redirect( "activation.php#jobfunctions" );
}
else
{
  $jobFunction = JobFunction::load( $_REQUEST["id"] );
}
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Activation" => "activation.php", $selectedUser->getFullName() => null, "Job function" => null ) ); ?>

<? include("activation_inc_summary.php"); ?>

<? ViewHelper::validation( "frmJobFunction" ); ?>

<h2>Job function</h2>
<form id="frmJobFunction" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Name</th>
      <td><input type="text" name="name" value="<?= $jobFunction->getName() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Description</th>
      <td><textarea name="description" cols="40" rows="6" class="required"><?= $jobFunction->getDescription() ?></textarea></td>
    </tr>
    <tr>
      <th>Status:</th>
      <td><?= ViewHelper::select( "status", ViewHelper::statusSelectList(), $jobFunction->getStatus(), true, false ) ?></td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $jobFunction->getId() ?>" />
  <input type="hidden" name="user_id" value="<?= $selectedUser->getId() ?>" />
  <? if( $jobFunction->isTransient() ) { ?>
  <input type="hidden" name="created_by_id" value="<?= getLoggedInUserId() ?>" />  
  <? } ?>
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"activation.php#jobfunctions" ) ); ?>

<? include("inc_footer.php"); ?>