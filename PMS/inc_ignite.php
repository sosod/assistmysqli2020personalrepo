<?php
//session_start();

function __autoload($class_name)
{
  if( $class_name == "Date" || $class_name == "Date_Span" )
  {
      require_once 'Date.php';
      require_once 'Date/Calc.php';
      require_once 'Date/Span.php';
  }
  else
  {
    require_once "classes/" . $class_name . '.class.php';
  }
}

require_once '../inc_session.php';
$logfile = "../logs/error_".$cmpcode.".txt";

include("inc_PMS_util.php");
include("inc_PMS_db.php");

/* Make a connection so that mysql_real_escape_string works */
getConnection();

/* LOAD THE LOGGED IN USER'S SETUP */
$userSetup = UserSetup::load( getLoggedInUserId() );
?>