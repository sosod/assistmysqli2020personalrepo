<?php
include("inc_ignite.php");
include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Setup" => null ) ); ?>

<h2>Module configuration</h2>
<table cellspacing="0" class="info">
  <tr>
    <th style="width:120px;">Categories</th>
    <td style="width:350px">Define the user categories</td>
    <td class="action"><? ViewHelper::button( "setup_categories.php", "Configure" ); ?></td>
  </tr>
  <tr>
    <th style="width:120px;">Employees</th>
    <td style="width:350px">Define the employee's access and other module variables</td>
    <td class="action"><? ViewHelper::button( "setup_users.php", "Configure" ); ?></td>
  </tr>
  <tr>
    <th style="width:120px;">Evaluation Years</th>
    <td style="width:350px">Define the evaluation years and their organisational goals</td>
    <td class="action"><? ViewHelper::button( "setup_evalyears.php", "Configure" ); ?></td>
  </tr>
  <tr>
    <th style="width:120px;">Core Competencies</th>
    <td style="width:350px">Configure the core competencies</td>
    <td class="action"><? ViewHelper::button( "setup_competencies.php", "Configure" ); ?></td>
  </tr>
</table>
    

<?php
include("inc_footer.php");
?>