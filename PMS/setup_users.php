<?
include("inc_ignite.php");
include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Employees" => null ) ); ?>

<table class="list" cellspacing="0">
  <tr>
    <th style="width:70%;">Employee</th>
    <th style="width:30%;">Actions</th>        
  </tr>
  <?
  foreach( User::records(false,"tkname,tksurname") as $obj )
  {
  ?>
  <tr>
    <td><?= $obj->getFullName() ?></td>
    <td>
      <? ViewHelper::button( "setup_user.php?user_id={$obj->getId()}", "Configure" ); ?>
      <? ViewHelper::button( "setup_userinfo.php?user_id={$obj->getId()}", "View" ); ?>
    </td>		
  </tr>
  <?
  }
  ?>
</table>

<? ViewHelper::actions( array( "Back"=>"setup.php" ) ); ?>

<?php
include("inc_footer.php");
?>