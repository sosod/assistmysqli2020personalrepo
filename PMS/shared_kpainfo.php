<?
$kpa = KPA::load( $_REQUEST["id"] );
  
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( ucwords( $section ) => "{$section}.php", $selectedUser->getFullName() => null, KPA::displayType( $kpa->getType() ) . " KPA" => null ) ); ?>

<? include("{$section}_inc_summary.php"); ?>

<h2><?= KPA::displayType( $kpa->getType() ) ?> KPA</h2>
<table class="info" cellspacing="0">
  <tr>
    <th>Type</th>
    <td><?= KPA::displayType( $kpa->getType() ) ?></td>
  </tr>
  <? if( $kpa->getType() == "org" ){ ?>
  <tr>
    <th>Organisational Goal:</th>
    <td><?= $kpa->getOrgGoal()->getName() ?></td>
  </tr>
  <? }else if( $kpa->getType() == "job" ){ ?>
  <tr>
    <th>Job function:</th>
    <td><?= $kpa->getJobFunction()->getName() ?></td>
  </tr>
  <? } ?>
  <tr>
    <th>Name</th>
    <td><?= $kpa->getName() ?></td>
  </tr>
  <tr>
    <th>Objective</th>
    <td><?= $kpa->getObjective() ?></td>
  </tr>
  <tr>
    <th>Measurement</th>
    <td><?= $kpa->getMeasurement() ?></td>
  </tr>
  <tr>
    <th>Baseline</th>
    <td><?= $kpa->getBaseline() ?></td>
  </tr>
  <tr>
    <th>Target unit</th>
    <td><?= $kpa->getTargetUnit() ?></td>
  </tr>
  
  <? 
    $periods = $selectedUser->getCategory()->getPeriodFrequency();
  
    for( $i = 1; $i <= $periods; $i++ ) 
    {
      $kpaTarget = $kpa->getTarget( $i );  
  ?>
  <tr>
    <th>Target for period <?= $i ?></th>
    <td><?= $kpaTarget->getTarget() ?></td>
  </tr>
  <? } ?>
  <? if( $selectedUser->getCategory()->getWeights() ){ ?>
  <tr>
    <th>Weight</th>
    <td><?= $kpa->getWeight() ?>%</td>
  </tr>
  <? } ?>
  <tr>
    <th>Competency</th>
    <td><?= $kpa->getCompetency()->getName() ?></td>
  </tr>
  <tr>
    <th>Proficiency Level</th>
    <td><?= ViewHelper::displayProficiencyLevel( $kpa->getProficiencyLevel() ) ?></td>
  </tr>
  <tr>
    <th>Deadline</th>
    <td><?= $kpa->getDeadline() ?></td>
  </tr>
  <tr>
    <th>Comments</th>
    <td><?= $kpa->getComments() ?></td>
  </tr>
  <tr>
    <th>Created by:</th>
    <td><?= $kpa->getCreatedBy()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Created on:</th>
    <td><?= $kpa->getCreatedOn() ?></td>
  </tr>
</table>

<? if( $section == ViewHelper::$SECTION_EVALUATION ){ ?>
<h2>Evaluation</h2>
<table class="info" cellspacing="0">
  <tr>
    <th>Evaluation period</th>
    <td><?= $selectedPeriod->getDisplayName() ?></td>
  </tr>
  <tr>
    <th>Employee rating</th>
    <td><?= ViewHelper::ifNull( $kpa->getEvaluation( $selectedPeriod->getPeriod() )->getUserRating(), "Not yet" ) ?></td>
  </tr>
  <tr>
    <th>Employee rating comment</th>
    <td><?= $kpa->getEvaluation( $selectedPeriod->getPeriod() )->getUserRatingComment() ?></td>
  </tr>
  <tr>
    <th>Manager rating</th>
    <td><?= ViewHelper::ifNull( $kpa->getEvaluation( $selectedPeriod->getPeriod() )->getManagerRating(), "Not yet" ) ?></td>
  </tr>
  <tr>
    <th>Manager rating comment</th>
    <td><?= $kpa->getEvaluation( $selectedPeriod->getPeriod() )->getManagerRatingComment() ?></td>
  </tr>
  <tr>
    <th>Joint rating</th>
    <td><?= ViewHelper::ifNull( $kpa->getEvaluation( $selectedPeriod->getPeriod() )->getJointRating(), "Not yet" ) ?></td>
  </tr>
  <tr>
    <th>Joint rating comment</th>
    <td><?= $kpa->getEvaluation( $selectedPeriod->getPeriod() )->getJointRatingComment() ?></td>
  </tr>
</table>
<? } ?>

<div class="actions">
  <? ViewHelper::button( "{$section}.php#{$kpa->getType()}kpas", "Back" ); ?>
</div>
<? include("inc_footer.php"); ?>