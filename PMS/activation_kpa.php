<?
include("inc_ignite.php");
include("activation_logic.php");

$type = $_REQUEST["type"];

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $kpa = new KPA();
  $kpa->init( $_REQUEST, true );
  
  $targets = $_REQUEST["targets"];
  
  /* START VALIDATION */
  
  //Deadline may not be after eval year end
  $deadlineDate = new Date( $_REQUEST["deadline"] );
  $evalYearEndDate = new Date( $selectedYear->getEnd() );
  if( $deadlineDate->after( $evalYearEndDate ) )
  {
    $errors[] = "Deadline may not be after the evaluation year's end which is " . $selectedYear->getEnd();
  }  
  
  //Deadline may not be before last target's end date
  $lastPeriod = -1;
  
  foreach( $targets as $period => $target )
  {
    if( exists( $target) )
    {
      $lastPeriod = $period;
    }  
  }
  
  if( $lastPeriod == -1 )
  {
    $errors[] = "Please specify at least 1 period target";
  }
  else
  {
    $lastPeriodStartDate = DateHelper::getPeriodStartDate( $selectedYear->getStart(), $selectedUser->getCategory()->getPeriodFrequency(), $lastPeriod );
    
    if( $deadlineDate->before( $lastPeriodStartDate ) )
    {
      $errors[] = "The deadline can not be before the last target's period start date of " . $lastPeriodStartDate->format3( "Y-m-d" );
    }
  }
  
  /* END VALIDATION */  

  if( count( $errors) == 0 )
  {
    $kpa->save();
    
    Base::baseDelete( "DELETE FROM assist_" . getCompanyCode() . "_" . KPATarget::$TABLE_NAME . " WHERE kpa_id = {$kpa->getId()}" , "Existing KPA targets before saving new ones" );
    
    foreach( $targets as $period => $target )
    {
      $kpaTarget = $kpa->getTarget( $period );
      $kpaTarget->setKpaId( $kpa->getId() );
      $kpaTarget->setPeriod( $period );
      $kpaTarget->setTarget( $target );
      $kpaTarget->save();
    }
    
    ViewHelper::redirect( "activation.php#{$type}kpas" );    
  }
}
else
{
  $kpa = KPA::load( $_REQUEST["id"] );
  
  if( $kpa->isTransient() == false )
  {
    $type = $kpa->getType();
  }
}
?>

<? include("inc_header.php"); ?>

<? if( $type == "org" ){ ?>
<script type="text/javascript">
  var orgGoals = new Array();
  <? foreach( OrgGoal::records( "eval_year_id = {$selectedYear->getId()}" ) as $orgGoal ){ ?>
  orgGoals['<?= $orgGoal->getId() ?>'] = <?= $orgGoal->toJsonString() ?>;
  <? } ?>
  
  $(document).ready(function(){
    $("#orgGoalId").change(function(){
      var values = orgGoals[ $(this).val() ];
      
      $("textarea[name='objective']").val( values.objective );
      $("textarea[name='measurement']").val( values.measurement );      
      $("input[name='baseline']").val( values.baseline ).attr("readonly",true);
      $("input[name='target_unit']").val( values.targetUnit ).attr("readonly",true);
      $("input[name='deadline']").val( values.deadline );
      
    });
  });
</script>
<? } ?>

<? ViewHelper::breadcrumbs( array( "Activation" => "activation.php", $selectedUser->getFullName() => null, KPA::displayType( $type ) . " KPA" => null ) ); ?>

<? include("activation_inc_summary.php"); ?>

<? ViewHelper::validation( "frmKPA", $errors ); ?>

<h2><?= KPA::displayType( $type ) ?> KPA</h2>
<form id="frmKPA" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Type</th>
      <td><?= KPA::displayType( $type ) ?><input type="hidden" name="type" value="<?= $type ?>" /></td>
    </tr>
    <? if( $type == "org" ){ ?>
    <tr>
      <th>Organisational Goal:</th>
      <td><?= ViewHelper::select( "org_goal_id", OrgGoal::selectList(), $kpa->getOrgGoalId(), true, "Select from list ...", "orgGoalId" ) ?></td>
    </tr>
    <? }else if( $type == "job" ){ ?>
    <tr>
      <th>Job function:</th>
      <td><?= ViewHelper::select( "job_function_id", JobFunction::selectList( "user_id = '{$selectedUser->getId()}' AND status = 'active'" ), $kpa->getJobFunctionId(), true ) ?></td>
    </tr>
    <? } ?>
    <tr>
      <th>Name</th>
      <td><input type="text" name="name" value="<?= $kpa->getName() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Objective</th>
      <td><textarea name="objective" cols="40" rows="6" class="required"><?= $kpa->getObjective() ?></textarea></td>
    </tr>
    <tr>
      <th>Measurement</th>
      <td><textarea name="measurement" cols="40" rows="6" class="required"><?= $kpa->getMeasurement() ?></textarea></td>
    </tr>
    <tr>
      <th>Baseline</th>
      <td><input type="text" name="baseline" value="<?= $kpa->getBaseline() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Target unit</th>
      <td><input type="text" name="target_unit" value="<?= $kpa->getTargetUnit() ?>" maxlength="255" class="required"/></td>
    </tr>
    
    <? 
      $periods = $selectedUser->getCategory()->getPeriodFrequency();
    
      for( $i = 1; $i <= $periods; $i++ ) 
      {
        $kpaTarget = $kpa->getTarget( $i );  
        
        $value = $kpaTarget->getTarget();
        if( exists( $value ) == false )
        {
          $value = $targets[$i];
        } 
    ?>
    <tr>
      <th>Target for period <?= $i ?></th>
      <td><input type="text" name="targets[<?= $i ?>]" value="<?= $value ?>" maxlength="255"/></td>
    </tr>
    <? } ?>
    <? if( $selectedUser->getCategory()->getWeights() ){ ?>
    <tr>
      <th>Weight</th>
      <td><?= ViewHelper::select( "weight", ViewHelper::getNumericSelectList( 0, 100 ), $kpa->getWeight(), true ) ?>%</td>
    </tr>
    <? } ?>
    <tr>
      <th>Core Competency</th>
      <td><?= ViewHelper::select( "competency_id", Competency::selectList(), $kpa->getCompetencyId(), true ) ?></td>
    </tr>
    <tr>
      <th>Proficiency Level</th>
      <td><?= ViewHelper::select( "proficiency_level", ViewHelper::proficiencyLevelSelectList(), $kpa->getProficiencyLevel(), true ) ?></td>
    </tr>
    <tr>
      <th>Deadline</th>
      <td><input type="text" name="deadline" value="<?= $kpa->getDeadline() ?>" readonly="readonly" class="required dateISO date-input"/></td>
    </tr>
    <tr>
      <th>Comments</th>
      <td><textarea name="comments" cols="40" rows="6"><?= $kpa->getComments() ?></textarea></td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $kpa->getId() ?>" />
  <input type="hidden" name="user_id" value="<?= $selectedUser->getId() ?>" />
  <input type="hidden" name="eval_year_id" value="<?= $selectedYear->getId() ?>" />  
  <? if( $selectedUser->getCategory()->getWeights() == false ){ ?>
  <input type="hidden" name="weight" value="0" />  
  <? } ?>
  <? if( $kpa->isTransient() ) { ?>
  <input type="hidden" name="created_by_id" value="<?= getLoggedInUserId() ?>" />  
  <? } ?>
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"activation.php#{$type}kpas" ) ); ?>

<? include("inc_footer.php"); ?>