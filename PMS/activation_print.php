<?php
include("inc_ignite.php");
include("activation_logic.php");

if( isset( $selectedUser ) == false || isset( $selectedYear ) == false )
{
  redirect( "activation.php" );
}

$qualifications = Qualification::records( "user_id = '{$selectedUser->getId()}' AND status = 'active'", "created_on ASC" );
$jobFunctions = JobFunction::records( "user_id = '{$selectedUser->getId()}' AND status = 'active'", "created_on ASC" );
$learningActivities = LearningActivity::records( "user_id = '{$selectedUser->getId()}' AND status = 'active'", "created_on ASC" );
$comments = Comment::records( $activationLoadWhere . " AND period is NULL", "created_on ASC" );  

$kpas = array();
foreach( KPA::$TYPES as $type => $desc ) 
{
  $kpas[ $type ] = KPA::records( $activationLoadWhere . " AND type = '{$type}' ", "created_on ASC" );
}

require ('lib/fpdf/IgnitePDF.class.php');

$pdf = new IgnitePDF('L', 'pt', 'A4');
$pdf->AddPage();
$pdf->H1( "Activation" );


/* PERSONAL INFORMATION */
$pdf->H2('Personal Information');

$widths = array( 200, 580 );
$rows = array( array( "Name", $selectedUser->getFullName() ), 
               array( "Gender", $selectedUser->getGenderDisplay() ),
               array( "Race", $selectedUser->getRace() ), 
               array( "Department", $selectedUser->getDepartment() ), 
               array( "Section", $selectedUser->getSection() ),
               array( "Job title", $selectedUser->getJobTitle() ), 
               array( "Job level", $selectedUser->getJobLevel() ), 
               array( "Job number", $selectedUser->getJobNumber() ),
               array( "Manager", $selectedUser->getManager() ),
               array( "Performance year", $selectedYear->getDisplayName() ), 
               array( "Date", date("Y-m-d H:i:s") )
              );

$pdf->infoTable( $rows, $widths );

$pdf->AddPage();

/* Qualifications */
$pdf->H2( 'Qualifications' );
$headers = array( "Type", "Name", "Institution", "Accredited", "Comments" );
$widths = array( 80, 160, 160, 80, 300 );
$rows = array();

foreach( $qualifications as $qualification )
{
  $rows[] = array( Qualification::displayType( $qualification->getType() ), 
                   $qualification->getName(),
                   $qualification->getInstitution(),                    
                   ViewHelper::boolYesNo( $qualification->getAccredited() ), 
                   $qualification->getComments() );
}

$pdf->listTable( $headers, $rows, $widths );

/* JOB FUNCTIONS */
$pdf->H2('Job functions');
$headers = array( "Name", "Description" );
$widths = array( 200, 580 );
$rows = array();

foreach( $jobFunctions as $jobFunction )
{
  $rows[] = array( $jobFunction->getName(), $jobFunction->getDescription() );
}

$pdf->listTable( $headers, $rows, $widths );

$pdf->AddPage();

/* KPAS */
$pdf->H2('Key Performance Areas');

foreach( $kpas as $type => $kpas ) 
{
  $pdf->H3( KPA::displayType( $type ) . " KPAs" );
  
  foreach( $kpas as $kpa )
  {
    $pdf->H4( $kpa->getName() );
    
    $headers = array();
  
    if( $type == "org" )
    {
      $headers[] = "Org Goal";
    }
    else if( $type == "job" )
    {
      $headers[] = "Job Function";
    }
    
    $headers[] = "Objective";
    $headers[] = "Measurement";
    $headers[] = "Baseline";
    $headers[] = "Target Unit";
    $headers[] = "Competency";
    $headers[] = "Proficiency";
    $headers[] = "Deadline";
    
    if( $selectedUser->getCategory()->getWeights() )
    {
      $headers[] = "Weight";
    }
    
    $headers[] = "Comments";
    
    if( $selectedUser->getSetup()->getCategory()->getWeights() )
    {
      if( $type == "org" || $type == "job" )
      {
        $widths = array( 87, 87, 87, 87, 87, 87, 52, 50, 39, 117 ); //columns = 10
      }
      else
      {
        $widths = array( 106, 104, 104, 104, 104, 52, 50, 39, 117 );  //columns = 9    
      }
    }
    else
    {
      if( $type == "org" || $type == "job" )
      {
        $widths = array( 91, 91, 92, 92, 92, 92, 50, 50, 130 ); //columns = 9     
      }
      else
      {
        $widths = array( 110, 110, 110, 110, 110, 50, 50, 130 ); //columns = 8     
      }
    }
    
    $rows = array();
    $data = array();
    
    if( $type == "org" )
    {
      $data[] = $kpa->getOrgGoal()->getName();
    }
    else if( $type == "job" )
    {
      $data[] = $kpa->getJobFunction()->getName();
    }
    
    $data[] = $kpa->getObjective();
    $data[] = $kpa->getMeasurement();
    $data[] = $kpa->getBaseline();
    $data[] = $kpa->getTargetUnit();
    $data[] = $kpa->getCompetency()->getName();
    $data[] = ViewHelper::displayProficiencyLevel( $kpa->getProficiencyLevel() );
    $data[] = $kpa->getDeadline();  

    if( $selectedUser->getSetup()->getCategory()->getWeights() )
    {
      $data[] = $kpa->getWeight();
    }
    
    $data[] = $kpa->getComments();    
    
    $rows[] = $data;
  
    $pdf->listTable( $headers, $rows, $widths );
      
    $widths = array( 100, 680 );
    
    $periods = $selectedUser->getSetup()->getCategory()->getPeriodFrequency();
  
    $targets = null;
  
    for( $i = 1; $i <= $periods; $i++ ) 
    {
      $kpaTarget = $kpa->getTarget( $i );  
      
      if( exists( $kpaTarget->getTarget() ) )
      {
        if( exists( $targets) )
        {
          $targets .= ", ";  
        }
        
        $targets .= "Period " . $i . ": " . $kpaTarget->getTarget();
      }
    }
    
    $rows = array( array( "Targets", $targets ) ) ;
    $pdf->infoTable( $rows, $widths );
  }
}

$pdf->AddPage();

/* LEARNING ACTIVITIES */
$pdf->H2('Learning Activities');
$headers = array ("Priority", "Name", "Provider", "Intervention", "Outcome", "Delivery Mode", "Time frames", "Work opportunity", "Support person");
$widths = array ( 60, 90, 90, 90, 90, 90, 90, 90, 90 );

$rows = array ();

foreach ( $learningActivities as $learningActivity )
{
  $rows[] = array ( LearningActivity::displayPriority( $learningActivity->getPriority() ), 
                    $learningActivity->getName(), 
                    $learningActivity->getProvider(),
                    $learningActivity->getIntervention(), 
                    $learningActivity->getOutcome(), 
                    $learningActivity->getDeliveryMode(), 
                    $learningActivity->getTimeFrames(),                                         
                    $learningActivity->getWorkOpportunity(), 
                    $learningActivity->getSupportPerson()->getFullName()
                  );
}

$pdf->listTable($headers, $rows, $widths);

/* COMMENTS */
$pdf->H2('General Comments');
$headers = array( "Created on", "Created by", "Message" );
$widths = array( 156, 156, 468 );
$rows = array();

foreach( $comments as $comment )
{
  $rows[] = array( $comment->getCreatedOn(), $comment->getCreatedBy()->getFullName(), $comment->getMessage() );
}

$pdf->listTable( $headers, $rows, $widths );
$pdf->Ln(50);

$pdf->SetFont('Arial', '', 9);
$pdf->Cell(300, 21, "Signed and accepted by the Employee", 'T', 1);
$pdf->Ln(20);
$pdf->Cell(300, 21, "Date", 'T', 1);
$pdf->Ln(50);
$pdf->Cell(300, 21, "Signed and accepted by the Manager", 'T', 1);
$pdf->Ln(20);
$pdf->Cell(300, 21, "Date", 'T', 1);

$pdf->Output('activation_'.str_replace(" ", "_", strtolower($selectedUser->getFullName())).'.pdf', 'I');
?>