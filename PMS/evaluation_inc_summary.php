<h2>Evaluation Summary</h2>
<table cellspacing="0" class="info">
  <tr>
    <th width="15%">Full name</th>
    <td width="35%"><?= $selectedUser->getFullName() ?></th>
    <th width="15%">Manager</th>
    <td width="35%"><?= $selectedUser->getManager() ?></th>    
  </tr>
  <tr>
    <th>Year</th> 
    <td><?= $selectedYear->getShortDisplayName() ?></td>   
    <th>Period</th> 
    <td><?= $selectedPeriod->getDisplayName() ?></td>
  </tr>
</table>