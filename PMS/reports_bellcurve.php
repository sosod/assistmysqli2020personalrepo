<?php
include("inc_ignite.php");

$showReport = false;

$selectedYear = new EvalYear();
$selectedCategory = new Category();
$selectedPeriod = null;

if( $_REQUEST["action"] == ViewHelper::$ACTION_SELECT_YEAR_PERIOD )
{
  $selectedYear = EvalYear::load( $_REQUEST["eval_year_id"] );
  $selectedCategory = Category::load( $_REQUEST["category_id"] );  
  $selectedPeriod = $_REQUEST["period"];    
  
  if( $selectedPeriod == "" )
  {
    $selectedPeriod = null;
  }
  
  if( $selectedYear->isTransient() == false && $selectedCategory->isTransient() == false && isset( $selectedPeriod ) )
  {
    $showReport = true;
    
    $departmentId = $_REQUEST["departmentId"];
    $jobLevel = $_REQUEST["jobLevel"];  
  }
}
?>
<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Reports" => "reports.php", "Bell Curve Graphs" => null ) ); ?>
    
<form action="reports_bellcurve.php" id="frmUserSelect" method="get" >
  <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
    <tr>
      <td valign="middle">
        <strong>Category:</strong> 
        <?= ViewHelper::select( "category_id", Category::selectList(), $selectedCategory->getId(), true ) ?>
        <strong>Evaluation year:</strong> 
        <?= ViewHelper::select( "eval_year_id", EvalYear::selectList(), $selectedYear->getId(), true ) ?>
      </td>
      <td valign="middle"><input type="submit" value="Select" /></td>
    </tr>
  </table>
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SELECT_YEAR_PERIOD ?>" />  
</form> 
<?
if( $selectedYear->isTransient() == false && $selectedCategory->isTransient() == false )
{
?>
<br/>   
<form action="reports_bellcurve.php" id="frmPeriodSelect" method="get" >
  <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
    <tr>
      <td>
        <strong>Evaluation period:</strong> 
        <?
          $periodSelectList = array();
          for( $i = 1; $i <= $selectedCategory->getPeriodFrequency(); $i++ )
          {
            if( DateHelper::isFuturePeriod( $selectedYear->getStart(), $selectedCategory->getPeriodFrequency(), $i ) == false )
            {
              $periodSelectList[] = array( $i, DateHelper::getPeriodDisplay( $selectedYear->getStart(), $selectedCategory->getPeriodFrequency(), $i ) );
            }
          }
          
          ViewHelper::select( "period", $periodSelectList, $selectedPeriod, true );
        ?>
      </td>
      <td valign="middle"><input type="submit" value="Select" /></td>
    </tr>
  </table>
  <input type="hidden" name="eval_year_id" value="<?= $selectedYear->getId() ?>" />
  <input type="hidden" name="category_id" value="<?= $selectedCategory->getId() ?>" />
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SELECT_YEAR_PERIOD ?>" />
</form>  
<?    
}
?>
<hr/>

<?
  if( $showReport )
  {
?>
    
<h5>Report Filter:</h5>
    
<form action="reports_bellcurve.php" id="frmFilter" method="get" >
  <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
    <tr>
      <td valign="middle">
        <label>Department</label>
        <?= ViewHelper::select( "departmentId", ReportHelper::getDepartmentSelectList(), $departmentId, false, "All" ) ?>
        <label>Job level</label>
        <?= ViewHelper::select( "jobLevel", ViewHelper::getJobLevelSelectList(), $jobLevel, false, "All" ) ?>
      </td>
      <td valign="middle"><input type="submit" value="Select" /></td>
    </tr>
  </table>
  <input type="hidden" name="eval_year_id" value="<?= $selectedYear->getId() ?>" />
  <input type="hidden" name="category_id" value="<?= $selectedCategory->getId() ?>" />
  <input type="hidden" name="period" value="<?= $selectedPeriod ?>" />    
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SELECT_YEAR_PERIOD ?>" />  
</form>  
    
<script type="text/javascript" src="amline/swfobject.js"></script>

<? 
  $departments = ReportHelper::getDepartmentList( $departmentId );
    
  foreach( $departments as $department )
  { 
?>
<div id="department-graph">
  <h5><?= $department["value"] ?> department</h5>
  
  <div id="graph_<?=$department["id"]?>">
		<strong>You need to upgrade your Flash Player</strong>
	</div>

	<script type="text/javascript">
		// <![CDATA[		
		var so = new SWFObject("amline/amline.swf", "amline", "520", "400", "8", "#FFFFFF");
		so.addVariable("path", "amline/");
		so.addVariable("settings_file", encodeURIComponent("charts/bell_curve_settings.xml?<?php echo mktime();?>")); // you can set two or more different settings files here (separated by commas)
		so.addVariable("data_file", encodeURIComponent("reports_bellcurve_data.php?<?php echo mktime();?>&eval_year_id=<?=$selectedYear->getId()?>&category_id=<?=$selectedCategory->getId()?>&period=<?= $selectedPeriod ?>&departmentId=<?=$department["id"]?>&jobLevel=<?=$jobLevel?>"));
    so.addVariable("loading_settings", "LOADING SETTINGS");
    so.addVariable("loading_data", "LOADING DATA");
    so.addVariable("preloader_color", "#666666");
		so.write("graph_<?=$department["id"]?>");
		// ]]>
	</script>
</div>
<? 
  } 
}
else
{          
?>
<p class="instruction">Please select an evaluation year, category and period above to continue.</p> 
<?
  }
?>
  
<?php
include("inc_footer.php");
?>