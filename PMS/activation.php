<?php
include("inc_ignite.php");
include("activation_logic.php");
include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Activation" => "activation.php", $selectedUser->getFullName() => null ) ); ?>

<form id="frmUserYearSelect" method="get">
  <table class="info full" cellspacing="0" style="margin-bottom:0;">
    <tr>
      <td valign="middle">
        <strong>Employee:</strong> 
        <? 
          if( count( getLoggedInUser()->getManagedUsers() ) > 0 )
          {
            echo ViewHelper::select( "selected_user_id", getLoggedInUser()->getManagedUsersSelectList(), $selectedUser->getId(), true );
          }
          else
          {
            echo $selectedUser->getFullName();
          }  
        ?>
        <strong>Evaluation year:</strong> 
        <?= ViewHelper::select( "selected_year_id", EvalYear::selectList(), $selectedYear->getId(), true ) ?>
        <input type="submit" value="Select" />
      </td>
    </tr>
  </table>
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SELECT_USER_YEAR ?>" />
</form>  

<? if( isset( $selectedUser ) && $selectedUser->isTransient() == false && 
       isset( $selectedYear ) && $selectedYear->isTransient() == false && 
       isset( $selectedActivationStatus ) && $selectedActivationStatus->isTransient() == false ) 
   {
?>         

<?
  /* ********************************************************************************************** */
  /* ***************************************** VALIDATION ***************************************** */
  /* ********************************************************************************************** */
  $qualifications = Qualification::records( "user_id = '{$selectedUser->getId()}' AND status = 'active'", "created_on ASC" );
  $jobFunctions = JobFunction::records( "user_id = '{$selectedUser->getId()}' AND status = 'active'", "created_on ASC" );
  $learningActivities = LearningActivity::records( "user_id = '{$selectedUser->getId()}' AND status = 'active'", "created_on ASC" );
    
  $totalKPAs = 0;
  
  //Check KPA type total weights
  foreach( KPA::$TYPES as $type => $desc ) 
  {
    $kpas[ $type ] = KPA::records( $activationLoadWhere . " AND type = '{$type}' ", "created_on ASC" );
    $kpaTotalWeight[ $type ] = KPA::totalWeight( $kpas[ $type ] );
    
    if( $kpaTotalWeight[ $type ] != $selectedUser->getCategory()->getKPAWeight( $type ) )
    {
      $errors[] = "{$desc} KPA weight is not equal to the required " . $selectedUser->getSetup()->getCategory()->getKPAWeight( $type ) . "%";
    }
    
    $totalKPAs += count( $kpas[ $type ] );
  }
  
  //Check min KPAs
  if( $totalKPAs < $selectedUser->getSetup()->getCategory()->getMinKPAs() )
  {
    $errors[] = "Min number of KPAs is " . $selectedUser->getSetup()->getCategory()->getMinKPAs() . ". Currently there are " . $totalKPAs;  
  }
  
  //Check max KPAs
  if( $totalKPAs > $selectedUser->getSetup()->getCategory()->getMaxKPAs() )
  {
    $errors[] = "Max number of KPAs is " . $selectedUser->getSetup()->getCategory()->getMaxKPAs() . ". Currently there are " . $totalKPAs;  
  }
  
  //Check minimum requirements
  if( count( $qualifications ) == 0 )
  {
    $errors[] = "At least 1 qualification is required";
  }
  
  if( count( $jobFunctions ) == 0 )
  {
    $errors[] = "At least 1 job function is required";
  }
  
  if( count( $errors ) == 0 )
  {
    if( $selectedActivationStatus->getRequestApprovalOn() == null )
    {
      $messages[] = "Activation ready to request approval";  
    }
    else if( $selectedActivationStatus->getManagerApprovalOn() == null )
    {
      $messages[] = "Activation requires manager approval";  
    }
    else if( $selectedActivationStatus->getUserApprovalOn() == null )
    {
      $messages[] = "Activation requires employee approval";  
    }
  }
?>

<h2>Activation Summary <? ViewHelper::button( "activation_print.php", "Print Activation" ); ?></h2>
<table cellspacing="0" class="info full">
  <tr>
    <th>Period frequency:</th>
    <td><?= ViewHelper::displayPeriodFrequency( $selectedUser->getSetup()->getCategory()->getPeriodFrequency() ) ?></td>        
    <th>Min/Max KPAs:</th>
    <td>Min <?= $selectedUser->getSetup()->getCategory()->getMinKPAs() ?> | Max <?= $selectedUser->getSetup()->getCategory()->getMaxKPAs() ?></td>        
    <th>KPA Weights:</th>
    <td>
    <? if( $selectedUser->getCategory()->getWeights() ){ ?>    
    Org <?= $selectedUser->getSetup()->getCategory()->getOrganisationKPAWeight() ?>% |
    Job <?= $selectedUser->getSetup()->getCategory()->getJobKPAWeight() ?>% |
    Learning <?= $selectedUser->getCategory()->getLearningKPAWeight() ?>%
    <? }else{ ?>
    N/A
    <? } ?>
    </td>        
  </tr>
  <tr>
    <th>Request approval on:</th>
    <? if( $selectedActivationStatus->getRequestApprovalOn() == null ) { ?>
    <td class="red-text">
      Not yet
      <? if( count( $errors ) == 0 && getLoggedInUserId() == $selectedUser->getId() ) { ?>
      <? ViewHelper::button( "activation.php?action=" . ViewHelper::$ACTION_REQUEST_APPROVAL, "Request Approval" ); ?>
      <? } ?>
    <? }else{ ?>
    <td class="green-text">
    <?= $selectedActivationStatus->getRequestApprovalOn() ?>
    <? } ?>
    </td>        
    <th>Manager approval on:</th>
    <? if( $selectedActivationStatus->getManagerApprovalOn() == null ){ ?>
    <td class="red-text">
      Not yet
      <? if( count( $errors ) == 0 && $selectedActivationStatus->getRequestApprovalOn() != null && $selectedUser->getManagerId() == getLoggedInUserId() ) { ?>
      <? ViewHelper::button( "activation.php?action=" . ViewHelper::$ACTION_MANAGER_APPROVAL, "Manager Approval" ); ?>
      <? } ?>       
    <? }else{ ?>
    <td class="green-text"> 
      <?= $selectedActivationStatus->getManagerApprovalOn() ?> 
    <? } ?>
    </td>
    <th>Employee approval on:</th>
    <? if( $selectedActivationStatus->getUserApprovalOn() == null ) { ?>
    <td class="red-text">
      Not yet
      <? if( count( $errors ) == 0 && 
             $selectedActivationStatus->getManagerApprovalOn() != null &&
             getLoggedInUserId() == $selectedUser->getId() ) { ?>
      <? ViewHelper::button( "activation.php?action=" . ViewHelper::$ACTION_USER_APPROVAL, "Employee Approval" ); ?>
      <? } ?>
    <? }else{ ?>
    <td class="green-text">
    <?= $selectedActivationStatus->getUserApprovalOn() ?>
    <? } ?>
    </td>
  </tr>
</table>
<? ViewHelper::errors( $errors ); ?>
<? ViewHelper::messages( $messages ); ?>

<div id="tabs">
  <ul>
    <li><a href="#general">General</a></li>  
    <li><a href="#qualifications">Qualifications</a></li>  
    <li><a href="#jobfunctions">Job functions</a></li>  
    <li><a href="#orgkpas">Organisational KPAs</a></li>
    <li><a href="#jobkpas">Job KPAs</a></li>
    <li><a href="#learningkpas">Learning KPAs</a></li>
    <li><a href="#learningactivities">Learning Activities</a></li>    
    <li><a href="#comments">Comments</a></li>                
  </ul>
  <div id="general">
    <table class="info full" cellspacing="0">
      <tr>
        <th>Gender:</th>
        <td><?= $selectedUser->getGenderDisplay() ?></td>
        <th>Race:</th>
        <td><?= $selectedUser->getRace() ?></td>
      </tr>
      <tr>
        <th>Department:</th>
        <td><?= $selectedUser->getDepartment() ?></td>
        <th>Section:</th>
        <td><?= $selectedUser->getSetup()->getSection() ?></td>
      </tr>
      <tr>
        <th>Job title:</th>
        <td><?= $selectedUser->getJobTitle() ?></td>
        <th>Job level:</th>
        <td><?= ViewHelper::displayJobLevel( $selectedUser->getSetup()->getJobLevel() ) ?></td>
      </tr>
      <tr>
        <th>Job number</th>
        <td><?= $selectedUser->getSetup()->getJobNumber() ?></td>
        <th>Category</th>
        <td><?= $selectedUser->getSetup()->getCategory()->getName() ?></td>
      </tr>
    </table>
  </div>
  <div id="qualifications">
    
    <? ViewHelper::actions( array( "New"=>"activation_qualification.php" ) ); ?>
    
    <table class="list full" cellspacing="0">
      <tr>
        <th style="width:10%;">Type</th>
        <th style="width:30%;">Name</th>
        <th style="width:30%;">Institution</th>        
        <th style="width:10%;">Accredited</th>
        <th style="width:20%;">Actions</th>                
      </tr>
      <?
        if( count( $qualifications ) )
        { 
          foreach( $qualifications as $obj ) { ?>
      <tr>
        <td><?= Qualification::displayType( $obj->getType() ) ?></td>
        <td><?= $obj->getName() ?></td>
        <td><?= $obj->getInstitution() ?></td>        
        <td><?= ViewHelper::boolYesNo( $obj->getAccredited() ) ?></td>
        <td>
          <? ViewHelper::button( "activation_qualification.php?id={$obj->getId()}", "Edit" ); ?>
          <? ViewHelper::button( "activation_qualificationinfo.php?id={$obj->getId()}", "View" ); ?>
        </td>
      </tr>
      <?  
          } 
        }
        else
        {
      ?>
      <tr>
        <td colspan="5">No qualifications</td>
      </tr>
      <?    
        }
      ?>
    </table>
    
  </div>
  <div id="jobfunctions">
    
    <? ViewHelper::actions( array( "New"=>"activation_jobfunction.php" ) ); ?>
    
    <table class="list full" cellspacing="0">
      <tr>
        <th style="width:20%;">Name</th>
        <th style="width:60%;">Description</th>        
        <th style="width:20%;">Actions</th>                
      </tr>
      <?
        if( count( $jobFunctions ) )
        { 
          foreach( $jobFunctions as $obj ) { ?>
      <tr>
        <td><?= $obj->getName() ?></td>
        <td><?= $obj->getDescription() ?></td>
        <td>
          <? ViewHelper::button( "activation_jobfunction.php?id={$obj->getId()}", "Edit" ); ?>
          <? ViewHelper::button( "activation_jobfunctioninfo.php?id={$obj->getId()}", "View" ); ?>
        </td>
      </tr>
      <?  
          } 
        }
        else
        {
      ?>
      <tr>
        <td colspan="3">No job functions</td>
      </tr>
      <?    
        }
      ?>
    </table>
    
  </div>  
  
  <? foreach( KPA::$TYPES as $type => $desc ) { ?>
  <div id="<?= $type ?>kpas">
    <? ViewHelper::actions( array( "New"=>"activation_kpa.php?type={$type}" ), $isMutable ); ?>
    
    <table class="list full" cellspacing="0">
      <tr>
        <th style="width:20%;">Name</th>
        <th style="width:25%;">Objective</th>
        <th style="width:25%;">Measurement</th>
        <? if( $selectedUser->getSetup()->getCategory()->getWeights() ){ ?>
        <th style="width:10%;">Weight</th>                
        <? } ?>
        <th style="width:20%;">Actions</th>        
      </tr>
      <?
        if( count( $kpas[$type] ) )
        { 
          foreach( $kpas[$type] as $obj ) { ?>
      <tr>
        <td><?= $obj->getName() ?></td>
        <td><?= $obj->getObjective() ?></td>
        <td><?= $obj->getMeasurement() ?></td>
        <? if( $selectedUser->getSetup()->getCategory()->getWeights() ){ ?>
        <td><?= $obj->getWeight() ?>%</td>        
        <? } ?>
        <td>
          <? ViewHelper::button( "activation_kpa.php?id={$obj->getId()}", "Edit", $isMutable ); ?>
          <? ViewHelper::button( "activation_kpa.php?action=delete&id={$obj->getId()}", "Delete", $isMutable ); ?>
          <? ViewHelper::button( "activation_kpainfo.php?id={$obj->getId()}", "View" ); ?>
        </td>
      </tr>
      <?  
          }
          
          if( $selectedUser->getSetup()->getCategory()->getWeights() ){
      ?>
      <tr>
        <td colspan="3">&nbsp;</td>
        <td class="<?= ( $kpaTotalWeight[$type] != $selectedUser->getSetup()->getCategory()->getKPAWeight( $type) ) ? "red" : "green" ?>"><?= $kpaTotalWeight[$type] ?>% [<?= $selectedUser->getSetup()->getCategory()->getKPAWeight( $type ) ?>%]</td>
        <td>&nbsp;</td>
      </tr>
      <?
          }
        }
        else
        {
      ?>
      <tr>
        <td colspan="5">No <?= $desc ?> KPAs defined</td>
      </tr>
      <?    
        }
      ?>
    </table>
  </div>
  <? } ?>
  
  <div id="learningactivities">
    
    <? ViewHelper::actions( array( "New"=>"activation_learningactivity.php" ) ); ?>
    
    <table class="list full" cellspacing="0">
      <tr>
        <th style="width:35%;">Name</th>
        <th style="width:30%;">Provider</th>                
        <th style="width:15%;">Priority</th>        
        <th style="width:20%;">Actions</th>                
      </tr>
      <?
        if( count( $learningActivities ) )
        { 
          foreach( $learningActivities as $obj ) { ?>
      <tr>
        <td><?= $obj->getName() ?></td>
        <td><?= $obj->getProvider() ?></td>
        <td><?= LearningActivity::displayPriority( $obj->getPriority() ) ?></td>
        <td>
          <? ViewHelper::button( "activation_learningactivity.php?id={$obj->getId()}", "Edit" ); ?>
          <? ViewHelper::button( "activation_learningactivityinfo.php?id={$obj->getId()}", "View" ); ?>
        </td>
      </tr>
      <?  
          } 
        }
        else
        {
      ?>
      <tr>
        <td colspan="3">No learning activities</td>
      </tr>
      <?    
        }
      ?>
    </table>
    
  </div>  
  
  <div id="comments">
    
    <? ViewHelper::actions( array( "New"=>"activation_comment.php" ), $isMutable ); ?>
    
    <table class="list full" cellspacing="0">
      <tr>
        <th style="width:60%;">Comment</th>
        <th style="width:20%;">Created by</th>
        <th style="width:20%;">Created on</th>        
      </tr>
      <?
        $comments = Comment::records( $activationLoadWhere . " AND period is NULL", "created_on ASC" );

        if( count( $comments ) )
        { 
          foreach( $comments as $obj ) { ?>
      <tr>
        <td><?= $obj->getMessage() ?></td>
        <td><?= $obj->getCreatedBy()->getFullName() ?></td>
        <td><?= $obj->getCreatedOn() ?></td>
      </tr>
      <?  
          } 
        }
        else
        {
      ?>
      <tr>
        <td colspan="3">No comments</td>
      </tr>
      <?    
        }
      ?>
    </table>  
    
  </div>
</div>      

<?
  }
  else
  {
?>
  <h2 class="error-messages-header">Please select an employee and evaluation year to continue.</h2>
<?    
  }
?>
<?php
include("inc_footer.php");
?>