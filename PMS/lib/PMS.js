function redirect( url )
{
  window.location.href = url;	
}

function confirmDelete( url )
{
	if( confirm("Are you sure you want to delete this record? This action is permanent and cannot be undone.") )
	{
	  redirect( url );
	}
	
	return false;
}
  
$(document).ready(function(){
  $(".date-input").datepicker({ dateFormat: 'yy-mm-dd', changeMonth:true, changeYear:true });  
  $('#tabs').tabs({ cookie: { expires: 30 } });
  $("table.list tr").not(".heading,.info").mouseover(function(){$(this).addClass("over");}).mouseout(function(){$(this).removeClass("over");});
  $("table.list tr:even").not(".heading, .info").addClass("alt");
  $("table.info tr:even").addClass("alt");  
  $("table.form tr:even").addClass("alt");  
  $("table.form tr:last").addClass("actions");            
  
  $(".confirm-delete").click( function(){
  	return confirm("Are you sure you want to delete this record? This action is permanent and cannot be undone.");
  });            
});  