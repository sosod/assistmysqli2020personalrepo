<?php
include("inc_ignite.php");
include("evaluation_logic.php");
include("inc_header.php");
?>

<? ViewHelper::breadcrumbs( array( "Evaluation" => "evaluation.php", $selectedUser->getFullName() => null ) ); ?>

<form id="frmUserYearPeriodSelect" method="get">
  <table class="info full" cellspacing="0" style="margin-bottom:0;">
    <tr>
      <td valign="middle">
        <strong>Employee:</strong>
        <?
          if( count( getLoggedInUser()->getManagedUsers() ) > 0 )
          {
            echo ViewHelper::select( "selected_user_id", getLoggedInUser()->getManagedUsersSelectList(), $selectedUser->getId(), true );
          }
          else
          {
            echo $selectedUser->getFullName();
          }
        ?>
        <strong>Evaluation year:</strong>
        <?= ViewHelper::select( "selected_year_id", EvalYear::selectList(), $selectedYear->getId(), true ) ?>

        <? if( isset( $selectedYear ) && $selectedYear->isTransient() == false ){ ?>
        <strong>Evaluation period:</strong>
        <?
          $periodSelectList = array();
          for( $i = 1; $i <= $selectedUser->getCategory()->getPeriodFrequency(); $i++ )
          {
            if( DateHelper::isFuturePeriod( $selectedYear->getStart(), $selectedUser->getCategory()->getPeriodFrequency(), $i ) == false )
            {
              $periodSelectList[] = array( $i, DateHelper::getPeriodDisplay( $selectedYear->getStart(), $selectedUser->getCategory()->getPeriodFrequency(), $i ) );
            }
          }
        ?>
        <?= ViewHelper::select( "selected_period", $periodSelectList, $selectedPeriod->getPeriod(), true ) ?>
        <? } ?>
        <input type="submit" value="Select" />
      </td>
    </tr>
  </table>
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SELECT_USER_YEAR_PERIOD ?>" />
</form>

<? if( isset( $selectedUser ) && $selectedUser->isTransient() == false &&
       isset( $selectedYear ) && $selectedYear->isTransient() == false &&
       isset( $selectedPeriod ) && $selectedPeriod->isTransient() == false &&
       isset( $selectedEvaluationStatus ) && $selectedEvaluationStatus->isTransient() == false )
   {
?>

<?
  /* ********************************************************************************************** */
  /* ***************************************** VALIDATION ***************************************** */
  /* ********************************************************************************************** */

  $allEvaluated = true;
  //Get all KPAs
  foreach( KPA::$TYPES as $type => $desc )
  {
    $kpas[ $type ] = KPA::kpasWithTarget( $type, $selectedUser->getId(), $selectedYear->getId(), $selectedPeriod->getPeriod() );

    //Load KPA evaluation object
    foreach( $kpas[ $type ] as $kpa )
    {
      $evaluation = $kpa->getEvaluation( $selectedPeriod->getPeriod() );

      if( isset( $evaluation ) == false )
      {
        $allEvaluated = false;
      }
      else
      {
        if( $selectedEvaluationStatus->getUserCompleted() == null )
        {
          if( $evaluation->getUserRating() == null )
          {
            $allEvaluated = false;
          }
        }
        else if( $selectedEvaluationStatus->getManagerCompleted() == null )
        {
          if( $evaluation->getManagerRating() == null )
          {
            $allEvaluated = false;
          }
        }
        else if( $selectedEvaluationStatus->getJointCompleted() == null )
        {
          if( $evaluation->getJointRating() == null )
          {
            $allEvaluated = false;
          }
        }
      }
    }
  }

  if( $allEvaluated == false )
  {
    $errors[] = "All KPAs must be evaluated before you can continue";
  }

?>

<h2>Evaluation Summary <? ViewHelper::button( "evaluation_print.php", "Print Evaluation" ); ?></h2>
<table cellspacing="0" class="info full">
  <tr>
    <th>Employee completed:</th>
    <? if( $selectedEvaluationStatus->getUserCompleted() == null ) { ?>
    <td class="red-text">
      Not yet
    <? }else{ ?>
    <td class="green-text">
    <?= $selectedEvaluationStatus->getUserCompleted() ?><br/>
    <?= $selectedEvaluationStatus->getUserCompletedComment() ?>
    <? } ?>
    </td>
    <th>Manager completed:</th>
    <? if( $selectedEvaluationStatus->getManagerCompleted() == null ) { ?>
    <td class="red-text">
      Not yet
    <? }else{ ?>
    <td class="green-text">
    <?= $selectedEvaluationStatus->getManagerCompleted() ?><br/>
    <?= $selectedEvaluationStatus->getManagerCompletedComment() ?>
    <? } ?>
    </td>
    <th>Joint completed:</th>
    <? if( $selectedEvaluationStatus->getJointCompleted() == null ) { ?>
    <td class="red-text">
      Not yet
    <? }else{ ?>
    <td class="green-text">
    <?= $selectedEvaluationStatus->getJointCompleted() ?><br/>
    <?= $selectedEvaluationStatus->getJointCompletedComment() ?>
    <? } ?>
    </td>
  </tr>
  <tr>
    <th>Joint approved:</th>
    <? if( $selectedEvaluationStatus->getJointApproved() == null ) { ?>
    <td class="red-text">
      Not yet
    <? }else{ ?>
    <td class="green-text">
    <?= $selectedEvaluationStatus->getJointApproved() ?><br/>
    <?= $selectedEvaluationStatus->getJointApprovedComment() ?>
    <? } ?>
    </td>
    <th>Fishbowl completed:</th>
    <? if( $selectedEvaluationStatus->getFishbowlCompleted() == null ) { ?>
    <td class="red-text">
      Not yet
    <? }else{ ?>
    <td class="green-text">
    <?= $selectedEvaluationStatus->getFishbowlCompleted() ?><br/>
    <?= $selectedEvaluationStatus->getFishbowlCompletedComment() ?>
    <? } ?>
    </td>
    <th>Fishbowl approved:</th>
    <? if( $selectedEvaluationStatus->getFishbowlApproved() == null ) { ?>
    <td class="red-text">
      Not yet
    <? }else{ ?>
    <td class="green-text">
    <?= $selectedEvaluationStatus->getFishbowlApproved() ?><br/>
    <?= $selectedEvaluationStatus->getFishbowlApprovedComment() ?>
    <? } ?>
    </td>
  </tr>
  <? if( $selectedEvaluationStatus->getJointApproved() != null ) { ?>
  <tr>
    <th>Average Rating</td>
    <td><?= $selectedEvaluationStatus->getAverageRating() ?></td>
    <th>Fishbowl Rating</td>
    <td colspan="3"><?= $selectedEvaluationStatus->getFishbowlRating() ?></td>
  </tr>
  <? } ?>
</table>

<? ViewHelper::errors( $errors ); ?>
<? ViewHelper::messages( $messages ); ?>

<?
  if( count( $errors ) == 0 && $showStatusForm )
  {
?>
<h2><?= $heading ?></h2>
<form id="frmEvalStatusChange" method="post" >
  <table class="form" cellspacing="0">
    <tr>
      <th>Comment</th>
      <td><input type="text" name="status_comment" maxlength="255" size="60" value="" /></td>
    </tr>
    <?
    if( $statusAction == ViewHelper::$ACTION_JOINT_EVALUATION_APPROVED )
    {
      $kpaList = array();
      //Get all KPAs
      foreach( KPA::$TYPES as $type => $desc )
      {
        $kpaList = array_merge( $kpaList, $kpas[ $type ] );
      }

      $averageRating = KPAHelper::calculateAverageRating( $selectedUser, $selectedPeriod->getPeriod(), $kpaList );
    ?>
    <tr>
      <th>Average rating</th>
      <td><?= $averageRating ?></td>
    </tr>
    <input type="hidden" name="average_rating" value="<?= $averageRating ?>" />
    <?
    }
    elseif( $statusAction == ViewHelper::$ACTION_FISHBOWL_EVALUATION_COMPLETE )
    {
    ?>
    <tr>
      <th>Fishbowl rating</th>
      <td><?= ViewHelper::select( "fishbowl_rating", KPAEvaluation::ratingSelectList(), $selectedEvaluationStatus->getAverageRating(), true ) ?></td>
    </tr>
    <? } ?>
    <tr>
      <td></td><td><input type="submit" value="<?= $button ?>" /></td>
    </tr>
  </table>
  <input type="hidden" name="action" value="<?= $statusAction ?>" />

</form>
<?
  }
?>

<div id="tabs">
  <ul>
    <li><a href="#orgkpas">Organisational KPAs</a></li>
    <li><a href="#jobkpas">Job KPAs</a></li>
    <li><a href="#learningkpas">Learning KPAs</a></li>
    <li><a href="#learningactivities">Learning Activities</a></li>
    <li><a href="#comments">Comments</a></li>
  </ul>

  <? foreach( KPA::$TYPES as $type => $desc ) { ?>
  <div id="<?= $type ?>kpas">

    <table class="list full" cellspacing="0">
      <tr>
        <th style="width:20%;">Name</th>
        <th style="width:20%;">Employee rating</th>
        <th style="width:20%;">Manager rating</th>
        <th style="width:20%;">Joint rating</th>
        <th style="width:20%;">Actions</th>
      </tr>
      <?
        if( count( $kpas[$type] ) )
        {
          foreach( $kpas[$type] as $obj ) { ?>
      <tr>
        <td><?= $obj->getName() ?></td>
        <td><?= ViewHelper::ifNull( KPAEvaluation::displayRating( $obj->getEvaluation( $selectedPeriod->getPeriod() )->getUserRating() ), "No rating" ) ?></td>
        <td><?= ViewHelper::ifNull( KPAEvaluation::displayRating( $obj->getEvaluation( $selectedPeriod->getPeriod() )->getManagerRating() ), "No rating" ) ?></td>
        <td><?= ViewHelper::ifNull( KPAEvaluation::displayRating( $obj->getEvaluation( $selectedPeriod->getPeriod() )->getJointRating() ), "No rating" ) ?></td>
        <td>
          <? ViewHelper::button( "evaluation_kpa.php?kpa_id={$obj->getId()}", "Evaluate", $isMutable ); ?>
          <? ViewHelper::button( "evaluation_kpainfo.php?id={$obj->getId()}", "View" ); ?>
        </td>
      </tr>
      <?
          }
        }
        else
        {
      ?>
      <tr>
        <td colspan="5">No <?= $desc ?> KPAs defined</td>
      </tr>
      <?
        }
      ?>
    </table>
  </div>
  <? } ?>

  <div id="learningactivities">
    <? ViewHelper::actions( array( "New"=>"evaluation_learningactivity.php" ) ); ?>

    <table class="list full" cellspacing="0">
      <tr>
        <th style="width:35%;">Name</th>
        <th style="width:30%;">Provider</th>
        <th style="width:15%;">Priority</th>
        <th style="width:20%;">Actions</th>
      </tr>
      <?
        $learningActivities = LearningActivity::records( "user_id = '{$selectedUser->getId()}' AND status = 'active'", "created_on ASC" );

        if( count( $learningActivities ) )
        {
          foreach( $learningActivities as $obj ) { ?>
      <tr>
        <td><?= $obj->getName() ?></td>
        <td><?= $obj->getProvider() ?></td>
        <td><?= LearningActivity::displayPriority( $obj->getPriority() ) ?></td>
        <td>
          <? ViewHelper::button( "evaluation_learningactivity.php?id={$obj->getId()}", "Edit" ); ?>
          <? ViewHelper::button( "evaluation_learningactivityinfo.php?id={$obj->getId()}", "View" ); ?>
        </td>
      </tr>
      <?
          }
        }
        else
        {
      ?>
      <tr>
        <td colspan="3">No learning activities</td>
      </tr>
      <?
        }
      ?>
    </table>
  </div>

  <div id="comments">
    <? ViewHelper::actions( array( "New"=>"evaluation_comment.php" ), $isMutable ); ?>

    <table class="list full" cellspacing="0">
      <tr>
        <th style="width:60%;">Comment</th>
        <th style="width:20%;">Created by</th>
        <th style="width:20%;">Created on</th>
      </tr>
      <?
        $comments = Comment::records( $evaluationLoadWhere, "created_on ASC" );

        if( count( $comments ) )
        {
          foreach( $comments as $obj ) { ?>
      <tr>
        <td><?= $obj->getMessage() ?></td>
        <td><?= $obj->getCreatedBy()->getFullName() ?></td>
        <td><?= $obj->getCreatedOn() ?></td>
      </tr>
      <?
          }
        }
        else
        {
      ?>
      <tr>
        <td colspan="3">No comments</td>
      </tr>
      <?
        }
      ?>
    </table>
  </div>
</div>

<?
  }
  else
  {
?>
  <h2 class="error-messages-header">Please select an employee, evaluation year and period to continue.</h2>
<?
  }
?>
<?php
include("inc_footer.php");
?>