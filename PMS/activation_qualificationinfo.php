<?
include("inc_ignite.php");
include("activation_logic.php");

$qualification = Qualification::load( $_REQUEST["id"] );
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Activation" => "activation.php", $selectedUser->getFullName() => null, "Qualification" => null ) ); ?>

<? include("activation_inc_summary.php"); ?>

<h2>Qualification</h2>
<table class="info" cellspacing="0">
  <tr>
    <th>Type</th>
    <td><?= Qualification::displayType( $qualification->getType() ) ?></td>
  </tr>
  <tr>
    <th>Name</th>
    <td><?= $qualification->getName() ?></td>
  </tr>
  <tr>
    <th>Institution</th>
    <td><?= $qualification->getInstitution() ?></td>
  </tr>
  <tr>
    <th>Accredited</th>
    <td><?= ViewHelper::boolYesNo( $qualification->getAccredited() ) ?></td>
  </tr>
  <tr>
    <th>Comments</th>
    <td><?= $qualification->getComments() ?></td>
  </tr>
  <tr>
    <th>Status:</th>
    <td><?= ViewHelper::displayStatus( $qualification->getStatus() ) ?></td>
  </tr>
  <tr>
    <th>Created by:</th>
    <td><?= $qualification->getCreatedBy()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Created on:</th>
    <td><?= $qualification->getCreatedOn() ?></td>
  </tr>
</table>

<? ViewHelper::actions( array( "Back"=>"activation.php#qualifications" ) ); ?>

<? include("inc_footer.php"); ?>