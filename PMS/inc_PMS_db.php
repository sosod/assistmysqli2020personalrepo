<?php
$CONST_TABLE_PREFIX = "assist_${cmpcode}_pms";

$loggedInUser = null;
$conn = null;

/******************************************************************************/
/****************************** HELPER FUNCTIONS ******************************/ 
/******************************************************************************/
function setMessage( $msg )
{
  $_SESSION["msg"] = $msg;
}

function nullIfEmpty( $value )
{
  if( isset( $value ) && strlen( trim( $value ) ) != 0 )
  {
    return $value;   
  }  
  
  return "NULL";
}

function getLoggedInUser()
{ 
  global $loggedInUser;

  if( $loggedInUser == null )
  {
    $loggedInUser = User::load( getLoggedInUserId() );
  }
  
  return $loggedInUser;  
}

/******************************************************************************/
/******************************* CORE FUNCTIONS *******************************/ 
/******************************************************************************/
function getConnection()
{
  global $conn;
  
  if( isset( $conn ) == false )
  {
    global $cmpcode;
    global $tkid;
    global $tkname;      
    
    $conn = mysql_connect("localhost","ignittps_ignite4","ign92054u");
    
    if(!$conn)
    {
    	$sqlerror = "<p>&nbsp;</p><p><b>---ERROR DETAILS BEGIN---</b></p><ul><li>Database error on sql: ".mysql_error()."</li><li>".$sql."</li><li>".$db."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
    	$fileloc = $_SERVER['PHP_SELF'];
      $to = "assist@ignite4u.co.za";
      $from = "assist@ignite4u.co.za";
      $subject = "Ignite Assist CON Error";
      $message = "<font face=arial size=2><p><b>Company:</b> ".$cmpcode."<br><b>TKID:</b> ".$tkid."<br><b>TK Name:</b> ".$tkname."</p>";
      $message .= "<p><b>File:</b> ".$fileloc."</p>";
    	$message .= $sqlerror;
      $message .= "</center></font>";
      $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
      mail($to,$subject,$message,$header);
    	die("<h1>Error</h1><p>Unfortunately Ignite Assist has encountered an error.<br>An email has been sent to Ignite Advisory Services at <a href=mailto:assist@ignite4u.co.za>assist@ignite4u.co.za</a> with the error details.<br>Thank you.</p><p>");
    }
    
    $db = "ignittps_i".strtolower($cmpcode);
    mysql_select_db($db,$conn);
  }
  
  return $conn;
}

function getResultSet( $sql )
{
  global $cmpcode;
  global $tkid;
  global $tkname;    
  
  $con = getConnection();
  
  $rs = mysql_query($sql,$con);

  if(!$rs)
  {
  	$sqlerror = "<p>&nbsp;</p><p><b>---ERROR DETAILS BEGIN---</b></p><ul><li>Database error on sql: ".mysql_error()."</li><li>".$sql."</li><li>".$db."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
  	$fileloc = $_SERVER['PHP_SELF'];
    $to = "assist@ignite4u.co.za";
    $from = "assist@ignite4u.co.za";
    $subject = "Ignite Assist SQL50 Error";
    $message = "<font face=arial size=2><p><b>Company:</b> ".$cmpcode."<br><b>TKID:</b> ".$tkid."<br><b>TK Name:</b> ".$tkname."</p>";
    $message .= "<p><b>File:</b> ".$fileloc."</p>";
  	$message .= $sqlerror;
    $message .= "</center></font>";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail($to,$subject,$message,$header);
  	die("<h1>Error</h1><p>Unfortunately Ignite Assist has encountered an error.<br>An email has been sent to Ignite Advisory Services at <a href=mailto:assist@ignite4u.co.za>assist@ignite4u.co.za</a> with the error details.<br>Thank you.</p><p>");
  }  
  
  return $rs;
}

function updateRecord( $sql )
{
  return getResultSet( $sql );
}

function insertRecord( $sql )
{
  getResultSet( $sql );
  
  return mysql_insert_id();
}

function getRecord( $rs )
{
  return stripslashes_deep( mysql_fetch_array($rs) );
}

function stripslashes_deep($value)
{
  $value = is_array($value) ? array_map('stripslashes_deep', $value) : stripslashes($value);

  return $value;
}

function logTransaction( $message, $sql, $old = "" )
{
  debug("[TRANSACTION] " . $message . " | " . $sql );
  
  global $cmpcode;
  global $tkid;
  global $tkname;    
  global $today;
  
  $ref = "PMS";
  $sql = str_replace("'","|",$sql);
  $message = str_replace("'","&#39",$message);
  $old = str_replace("'","&#39",$old);
  
  $sqlLog = "INSERT INTO assist_".$cmpcode."_log SET date = '".$today."', tkid = '".$tkid."', ref = '".$ref."', transaction = '".$message."', tsql = '".$sql."', told = '".$old."'";
  
  insertRecord( $sqlLog );
}

?>