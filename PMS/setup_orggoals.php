<?
include("inc_ignite.php");

$evalYear = EvalYear::load( $_REQUEST["eval_year_id"] );

include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Eval years" => "setup_evalyears.php", $evalYear->getShortDisplayName() => null, "Goals" => null ) ); ?>

<table class="list" cellspacing="0">
  <tr>
    <th style="width:70%;">Name</th>
    <th style="width:30%;">Actions</th>        
  </tr>
  <?
  foreach( OrgGoal::records( "eval_year_id = {$evalYear->getId()}" ) as $obj )
  {
  ?>
  <tr>
    <td><?= $obj->getName() ?></td>
    <td>
      <? ViewHelper::button( "setup_orggoal.php?id={$obj->getId()}", "Configure" ); ?>
      <? ViewHelper::button( "setup_orggoalinfo.php?id={$obj->getId()}", "View" ); ?>
      <? debug( "setup_orggoal.php?action=" . ViewHelper::$ACTION_DELETE . "&id={$obj->getId()}&eval_year_id={$evalYear->getId()}" );
         ViewHelper::button( "setup_orggoal.php?action=" . ViewHelper::$ACTION_DELETE . "&id={$obj->getId()}&eval_year_id={$evalYear->getId()}", "Delete" ); ?>      
    </td>		
  </tr>
  <?
  }
  ?>
</table>

<? ViewHelper::actions( array( "Back"=>"setup_evalyears.php", "New"=>"setup_orggoal.php?eval_year_id=" . $evalYear->getId() ) ); ?>

<?php
include("inc_footer.php");
?>