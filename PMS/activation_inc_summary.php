<h2>Activation Summary</h2>
<table cellspacing="0" class="info">
  <tr>
    <th width="15%">Full name</th>
    <td width="35%"><?= $selectedUser->getFullName() ?></th>
    <th width="15%">Manager</th>
    <td width="35%"><?= $selectedUser->getManager() ?></td>
  </tr>
  <tr>
    <th>Evaluation year</th> 
    <td><?= $selectedYear->getDisplayName() ?></td>
    <th>Evaluation periods</th> 
    <td><?= $selectedUser->getCategory()->getPeriodFrequency() ?></td>
  </tr>
</table>