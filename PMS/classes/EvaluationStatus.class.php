<?php
  class EvaluationStatus extends Base 
  {
    public static $TABLE_NAME = "pms_evaluationstatus";
    public static $DISPLAY_NAME = "Evaluation status";  
    
    private $userId;
    private $evalYearId;
    private $period;
    private $userCompleted;
    private $userCompletedComment;
    private $managerCompleted;
    private $managerCompletedComment;
    private $jointCompleted;
    private $jointCompletedComment;
    private $jointApproved;
    private $jointApprovedComment;
    private $fishbowlCompleted;
    private $fishbowlCompletedComment;
    private $fishbowlApproved;
    private $fishbowlApprovedComment;
    private $averageRating; //set upon joint approval as the avg score of all kpas with weights factored in if applicable
    private $fishbowlRating;
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */ 
    private $user;
    private $evalYear;
                
    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new EvaluationStatus();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new EvaluationStatus();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new EvaluationStatus();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setUserId( self::initValue( $values["user_id"], $escape ) );
      $this->setEvalYearId( self::initValue( $values["eval_year_id"], $escape ) );
      $this->setPeriod( self::initValue( $values["period"], $escape ) );      
      $this->setUserCompleted( self::initValue( $values["user_completed"], $escape ) );
      $this->setUserCompletedComment( self::initValue( $values["user_completed_comment"], $escape ) );
      $this->setManagerCompleted( self::initValue( $values["manager_completed"], $escape ) );
      $this->setManagerCompletedComment( self::initValue( $values["manager_completed_comment"], $escape ) );
      $this->setJointCompleted( self::initValue( $values["joint_completed"], $escape ) );
      $this->setJointCompletedComment( self::initValue( $values["joint_completed_comment"], $escape ) );
      $this->setJointApproved( self::initValue( $values["joint_approved"], $escape ) );
      $this->setJointApprovedComment( self::initValue( $values["joint_approved_comment"], $escape ) );
      $this->setFishbowlCompleted( self::initValue( $values["fishbowl_completed"], $escape ) );
      $this->setFishbowlCompletedComment( self::initValue( $values["fishbowl_completed_comment"], $escape ) );
      $this->setFishbowlApproved( self::initValue( $values["fishbowl_approved"], $escape ) );
      $this->setFishbowlApprovedComment( self::initValue( $values["fishbowl_approved_comment"], $escape ) );
      $this->setAverageRating( self::initValue( $values["average_rating"], $escape ) );
      $this->setFishbowlRating( self::initValue( $values["fishbowl_rating"], $escape ) );
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( user_id, " .
               "  eval_year_id, " . 
               "  period, " . 
               "  user_completed, " . 
               "  user_completed_comment, " . 
               "  manager_completed, " . 
               "  manager_completed_comment, " .
               "  joint_completed, " . 
               "  joint_completed_comment, " .
               "  joint_approved, " . 
               "  joint_approved_comment, " .
               "  fishbowl_completed, " . 
               "  fishbowl_completed_comment, " .
               "  fishbowl_approved, " . 
               "  fishbowl_approved_comment, " .
               "  average_rating, " .
               "  fishbowl_rating ) " .
               "VALUES " .
               "( '{$this->userId}', " . 
               "   {$this->evalYearId}, " . 
               "   {$this->period}, " . 
               "  " . DBHelper::sqlNULL( $this->userCompleted ) . ", " .   
               "  " . DBHelper::sqlNULL( $this->userCompletedComment ) . ", " .  
               "  " . DBHelper::sqlNULL( $this->managerCompleted ) . ", " .   
               "  " . DBHelper::sqlNULL( $this->managerCompletedComment ) . ", " .
               "  " . DBHelper::sqlNULL( $this->jointCompleted ) . ", " .   
               "  " . DBHelper::sqlNULL( $this->jointCompletedComment ) . ", " .
               "  " . DBHelper::sqlNULL( $this->jointApproved ) . ", " .   
               "  " . DBHelper::sqlNULL( $this->jointApprovedComment ) . ", " .
               "  " . DBHelper::sqlNULL( $this->fishbowlCompleted ) . ", " .   
               "  " . DBHelper::sqlNULL( $this->fishbowlCompletedComment ) . ", " .
               "  " . DBHelper::sqlNULL( $this->fishbowlApproved ) . ", " .   
               "  " . DBHelper::sqlNULL( $this->fishbowlApprovedComment ) . ", " .
               "  " . DBHelper::sqlNULL( $this->averageRating ) . ", " .               
               "  " . DBHelper::sqlNULL( $this->fishbowlRating ) . " )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET user_id = '{$this->userId}', " .
               "     eval_year_id = {$this->evalYearId}, " .
               "     period = {$this->period}, " .               
               "     user_completed = " . DBHelper::sqlNULL( $this->userCompleted ) . ", " .
               "     user_completed_comment = " . DBHelper::sqlNULL( $this->userCompletedComment, true ) . ", " .
               "     manager_completed = " . DBHelper::sqlNULL( $this->managerCompleted ) . ", " .
               "     manager_completed_comment = " . DBHelper::sqlNULL( $this->managerCompletedComment, true ) . ", " .
               "     joint_completed = " . DBHelper::sqlNULL( $this->jointCompleted ) . ", " .
               "     joint_completed_comment = " . DBHelper::sqlNULL( $this->jointCompletedComment, true ) . ", " .
               "     joint_approved = " . DBHelper::sqlNULL( $this->jointApproved ) . ", " .
               "     joint_approved_comment = " . DBHelper::sqlNULL( $this->jointApprovedComment, true ) . ", " .
               "     fishbowl_completed = " . DBHelper::sqlNULL( $this->fishbowlCompleted ) . ", " .
               "     fishbowl_completed_comment = " . DBHelper::sqlNULL( $this->fishbowlCompletedComment, true ) . ", " .
               "     fishbowl_approved = " . DBHelper::sqlNULL( $this->fishbowlApproved ) . ", " .
               "     fishbowl_approved_comment = " . DBHelper::sqlNULL( $this->fishbowlCompletedComment, true ) . ", " .
               "     average_rating = " . DBHelper::sqlNULL( $this->averageRating ) . ", " .               
               "     fishbowl_rating = " . DBHelper::sqlNULL( $this->fishbowlRating ) .  
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function setUserId($userId)
    {
      $this->userId = $userId;
    }
    
    public function getUserId()
    {
      return $this->userId;
    }

    public function getEvalYearId()
    {
      return $this->evalYearId;
    }
    
    public function setEvalYearId($evalYearId)
    {
      $this->evalYearId = $evalYearId;
    }

    public function getPeriod()
    {
      return $this->period;
    }
    
    public function setPeriod($period)
    {
      $this->period = $period;
    }

    public function getFishbowlApproved()
    {
      return $this->fishbowlApproved;
    }
    
    public function setFishbowlApproved($fishbowlApproved)
    {
      $this->fishbowlApproved = $fishbowlApproved;
    }
    
    public function getFishbowlApprovedComment()
    {
      return $this->fishbowlApprovedComment;
    }
    
    public function setFishbowlApprovedComment($fishbowlApprovedComment)
    {
      $this->fishbowlApprovedComment = $fishbowlApprovedComment;
    }
    
    public function getFishbowlCompleted()
    {
      return $this->fishbowlCompleted;
    }
    
    public function setFishbowlCompleted($fishbowlCompleted)
    {
      $this->fishbowlCompleted = $fishbowlCompleted;
    }
    
    public function getFishbowlCompletedComment()
    {
      return $this->fishbowlCompletedComment;
    }
    
    public function setFishbowlCompletedComment($fishbowlCompletedComment)
    {
      $this->fishbowlCompletedComment = $fishbowlCompletedComment;
    }
    
    public function getFishbowlRating()
    {
      return $this->fishbowlRating;
    }
    
    public function setFishbowlRating($fishbowlRating)
    {
      $this->fishbowlRating = $fishbowlRating;
    }
    
    public function getAverageRating()
    {
      return $this->averageRating;
    }
    
    public function setAverageRating($averageRating)
    {
      $this->averageRating = $averageRating;
    }
    
    public function getJointApproved()
    {
      return $this->jointApproved;
    }
    
    public function setJointApproved($jointApproved)
    {
      $this->jointApproved = $jointApproved;
    }
    
    public function getJointApprovedComment()
    {
      return $this->jointApprovedComment;
    }
    
    public function setJointApprovedComment($jointApprovedComment)
    {
      $this->jointApprovedComment = $jointApprovedComment;
    }
    
    public function getJointCompleted()
    {
      return $this->jointCompleted;
    }
    
    public function setJointCompleted($jointCompleted)
    {
      $this->jointCompleted = $jointCompleted;
    }
    
    public function getJointCompletedComment()
    {
      return $this->jointCompletedComment;
    }
    
    public function setJointCompletedComment($jointCompletedComment)
    {
      $this->jointCompletedComment = $jointCompletedComment;
    }
    
    public function getManagerCompleted()
    {
      return $this->managerCompleted;
    }
    
    public function setManagerCompleted($managerCompleted)
    {
      $this->managerCompleted = $managerCompleted;
    }
    
    public function getManagerCompletedComment()
    {
      return $this->managerCompletedComment;
    }
    
    public function setManagerCompletedComment($managerCompletedComment)
    {
      $this->managerCompletedComment = $managerCompletedComment;
    }
    
    public function getUserCompleted()
    {
      return $this->userCompleted;
    }
    
    public function setUserCompleted($userCompleted)
    {
      $this->userCompleted = $userCompleted;
    }
    
    public function getUserCompletedComment()
    {
      return $this->userCompletedComment;
    }
    
    public function setUserCompletedComment($userCompletedComment)
    {
      $this->userCompletedComment = $userCompletedComment;
    }
        
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getUser() 
    {
      if( isset( $this->user ) == false )
      {
        $this->user = User::load( $this->userId );
      }
      
      return $this->user; 
    }
    
    public function getEvalYear() 
    {
      if( isset( $this->evalYear ) == false )
      {
        $this->evalYear = EvalYear::load( $this->evalYearId );
      }
      
      return $this->evalYear; 
    }
  }
?>
