<?php
  class EvalYear extends Base
  {
    public static $TABLE_NAME = "pms_evalyear";
    public static $DISPLAY_NAME = "Eval year";

    private $start;
    private $end;

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    private $periods;

    /* --------------------------------------------------------------------------------------- */
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function __construct()
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    public static function load( $id )
    {
      $obj = new EvalYear();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }

    public static function loadWhere( $condition )
    {
      $obj = new EvalYear();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }

    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";

		  $rs = getResultSet( $sql );

		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new EvalYear();
        $obj->init( $record );

		    $records[] = $obj;
		  }

		  return $records;
    }

    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setStart( self::initValue( $values["start"], $escape ) );
      $this->setEnd( self::initValue( $values["end"], $escape ) );
    }

    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( start, end ) " .
               "VALUES " .
               "( '{$this->start}', '{$this->end}' )";

        $this->insert( $sql );
      }
      else
      {
        $sql = " SET start = '{$this->start}', " .
               "     end = '{$this->end}' " .
               " WHERE id = " . $this->getId();

        $this->update( $sql );
      }
    }

    /* --------------------------------------------------------------------------------------- */
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    public static function selectList()
    {
      $list = array();

      foreach( self::records() as $record )
      {
        $list[] = array( $record->getId(), $record->getShortDisplayName() );
      }

      return $list;
    }

    public function getDisplayName()
    {
      if( $this->isTransient() )
      {
        return "New";
      }

      $tmpStart = strtotime( $this->start );
      $tmpEnd = strtotime( $this->end );

      return date( "j F Y", $tmpStart )  . " to " . date( "j F Y", $tmpEnd );

      return $this->start . " to " . $this->end;
    }

    public function getShortDisplayName()
    {
      if( $this->isTransient() )
      {
        return "New";
      }

      $tmpStart = strtotime( $this->start );
      $tmpEnd = strtotime( $this->end );

      return date( "j M y", $tmpStart )  . " to " . date( "j M y", $tmpEnd );
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    public function getEnd()
    {
      return $this->end;
    }

    public function setEnd($end)
    {
      $this->end = $end;
    }

    public function getStart()
    {
      return $this->start;
    }

    public function setStart($start)
    {
      $this->start = $start;
    }

    public function getPeriods()
    {
      if( isset( $this->periods ) == false )
      {
        $this->periods = EvalPeriod::records( "eval_year_id = {$this->getId()}", "start ASC" );
      }

      return $this->periods;
    }
}
?>
