<?php
  class OrgGoal extends Base
  {
    public static $TABLE_NAME = "pms_orggoal";
    public static $DISPLAY_NAME = "Organisational Goal";  

    private $evalYearId;    
    private $name;
    private $objective;
    private $measurement;        
    private $baseline;
    private $targetUnit;
    private $deadline;            
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */ 
    private $evalYear;
    
    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new OrgGoal();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new OrgGoal();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new OrgGoal();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setName( self::initValue( $values["name"], $escape ) );
      $this->setEvalYearId( self::initValue( $values["eval_year_id"], $escape ) ); 
      $this->setObjective( self::initValue( $values["objective"], $escape ) );
      $this->setMeasurement( self::initValue( $values["measurement"], $escape ) );
      $this->setBaseline( self::initValue( $values["baseline"], $escape ) );      
      $this->setTargetUnit( self::initValue( $values["target_unit"], $escape ) );  
      $this->setDeadline( self::initValue( $values["deadline"], $escape ) );                        
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( eval_year_id, name, objective, measurement, baseline, target_unit, deadline ) " .
               "VALUES " .
               "( {$this->evalYearId}, '{$this->name}', '{$this->objective}', '{$this->measurement}', '{$this->baseline}', '{$this->targetUnit}', '{$this->deadline}' )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET eval_year_id = {$this->evalYearId}, " .
               "     name = '{$this->name}', " .
               "     objective = '{$this->objective}', " .
               "     measurement = '{$this->measurement}', " .
               "     baseline = '{$this->baseline}', " .
               "     target_unit = '{$this->targetUnit}', " .
               "     deadline = '{$this->deadline}' " .
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 
    
    /* --------------------------------------------------------------------------------------- */   
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */      
    public static function selectList()
    {
      $list = array();  
  
      foreach( self::records() as $record )
      {
        $list[] = array( $record->getId(), $record->getName() );    
      }
      
      return $list;      
    } 
    
    public function getDisplayName()
    {
      if( $this->isTransient() )
      {
        return "New";
      }
      
      return $this->name;
    }
    
    public function toJsonString()
    {
      $replace = array('\\'=>'\\\\',"'"=>"\\'",'"'=>'\\"',"\r"=>'\\r',"\n"=>'\\n','</'=>'<\/');
      
      $json  = "{";
      $json .= "evalYearId:{$this->evalYearId},";
      $json .= "name:'" . strtr( $this->name, $replace ) . "',";
      $json .= "objective:'" . strtr( $this->objective, $replace ) . "',";
      $json .= "measurement:'" . strtr( $this->measurement, $replace ) . "',";
      $json .= "baseline:'" . strtr( $this->baseline, $replace ) . "',";
      $json .= "targetUnit:'" . strtr( $this->targetUnit, $replace ) . "',";
      $json .= "deadline:'" . strtr( $this->deadline, $replace ) . "'";
      $json .= "}";
      
      return $json;
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getName()
    {
      return $this->name;
    }

    public function setName($name)
    {
      $this->name = $name;
    }
    
    public function getBaseline()
    {
      return $this->baseline;
    }
    
    public function setBaseline($baseline)
    {
      $this->baseline = $baseline;
    }
    
    public function getDeadline()
    {
      return $this->deadline;
    }
    
    public function setDeadline($deadline)
    {
      $this->deadline = $deadline;
    }
    
    public function getEvalYearId()
    {
      return $this->evalYearId;
    }
    
    public function setEvalYearId($evalYearId)
    {
      $this->evalYearId = $evalYearId;
    }
    
    public function getMeasurement()
    {
      return $this->measurement;
    }
    
    public function setMeasurement($measurement)
    {
      $this->measurement = $measurement;
    }
    
    public function getObjective()
    {
      return $this->objective;
    }
    
    public function setObjective($objective)
    {
      $this->objective = $objective;
    }
    
    public function getTargetUnit()
    {
      return $this->targetUnit;
    }

    public function setTargetUnit($targetUnit)
    {
      $this->targetUnit = $targetUnit;
    }

    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getEvalYear() 
    {
      if( isset( $this->evalYear ) == false )
      {
        $this->evalYear = EvalYear::load( $this->evalYearId );
      }
      
      return $this->evalYear; 
    }
}
?>
