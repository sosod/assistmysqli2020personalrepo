<?php
  class Competency extends Base 
  {
    public static $TABLE_NAME = "pms_competency";
    public static $DISPLAY_NAME = "Core Managerial Competency";  
    
    private $name;
    private $definition;
    private $status;

    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new Competency();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new Competency();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new Competency();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setName( self::initValue( $values["name"], $escape ) );
      $this->setDefinition( self::initValue( $values["definition"], $escape ) );
      $this->setStatus( self::initValue( $values["status"], $escape ) );
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( name, definition, status ) " .
               "VALUES " .
               "( '{$this->name}', '{$this->definition}', '{$this->status}' )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET status = '{$this->status}' " .
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 
    
    /* --------------------------------------------------------------------------------------- */   
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */      
    public static function selectList( $activeOnly = true )
    {
      $list = array();  
  
      if( $activeOnly )
      {
        $records = self::records( "status = 'active'" );  
      }
      else
      {
        $records = self::records();  
      }
  
      foreach( $records as $record )
      {
        $list[] = array( $record->getId(), $record->getName() );    
      }
      
      return $list;      
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getStatus()
    {
      return $this->status;
    }

    public function setStatus($status)
    {
      $this->status = $status;
    }

    public function getDefinition()
    {
      return $this->definition;
    }

    public function setDefinition($definition)
    {
      $this->definition = $definition;
    }

    public function getName()
    {
      return $this->name;
    }

    public function setName($name)
    {
      $this->name = $name;
    }
        
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
}
?>
