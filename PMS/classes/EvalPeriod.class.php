<?php
  class EvalPeriod extends Base 
  {
    public static $TABLE_NAME = "pms_evalperiod";
    public static $DISPLAY_NAME = "Eval period";  

    private $evalYearId;        
    private $start;
    private $end;

    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new EvalPeriod();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new EvalPeriod();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new EvalPeriod();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setEvalYearId( self::initValue( $values["eval_year_id"], $escape ) );      
      $this->setStart( self::initValue( $values["start"], $escape ) );
      $this->setEnd( self::initValue( $values["end"], $escape ) );
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( eval_year_id, start, end ) " .
               "VALUES " .
               "( {$this->evalYearId}, '{$this->start}', '{$this->end}' )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET start = '{$this->start}', " .
               "     end = {$this->end}, " .
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 

    /* --------------------------------------------------------------------------------------- */   
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */      
    public static function selectList()
    {
      $list = array();  
  
      foreach( self::records() as $record )
      {
        $list[] = array( $record->getId(), $record->getDisplayName() );    
      }
      
      return $list;      
    } 
    
    public function getDisplayName()
    {
      if( $this->isTransient() )
      {
        return "New";
      }
      
      return $this->start . " - " . $this->end;
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getEvalYearId()
    {
      return $this->evalYearId;
    }
    
    public function setEvalYearId($evalYearId)
    {
      $this->evalYearId = $evalYearId;
    }
    
    public function getEnd()
    {
      return $this->end;
    }
    
    public function setEnd($end)
    {
      $this->end = $end;
    }
    
    public function getStart()
    {
      return $this->start;
    }
    
    public function setStart($start)
    {
      $this->start = $start;
    }
}
?>
