<?php
  class ActivationStatus extends Base 
  {
    public static $TABLE_NAME = "pms_activationstatus";
    public static $DISPLAY_NAME = "Activation status";  
    
    private $userId;
    private $evalYearId;
    private $requestApprovalOn;
    private $managerApprovalOn;
    private $userApprovalOn;

    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */ 
    private $user;
    private $evalYear;
                
    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new ActivationStatus();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new ActivationStatus();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new ActivationStatus();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setUserId( self::initValue( $values["user_id"], $escape ) );
      $this->setEvalYearId( self::initValue( $values["eval_year_id"], $escape ) );
      $this->setRequestApprovalOn( self::initValue( $values["request_approval_on"], $escape ) );
      $this->setManagerApprovalOn( self::initValue( $values["manager_approval_on"], $escape ) );
      $this->setUserApprovalOn( self::initValue( $values["user_approval_on"], $escape ) );
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( user_id, eval_year_id, request_approval_on, manager_approval_on, user_approval_on ) " .
               "VALUES " .
               "( '{$this->userId}', {$this->evalYearId}, " . DBHelper::sqlNULL( $this->requestApprovalOn ) . ", " . DBHelper::sqlNULL( $this->managerApprovalOn ) . ", " . DBHelper::sqlNULL( $this->userApprovalOn ) . " )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET user_id = '{$this->userId}', " .
               "     eval_year_id = {$this->evalYearId}, " .
               "     request_approval_on = " . DBHelper::sqlNULL( $this->requestApprovalOn ) . ", " .
               "     manager_approval_on = " . DBHelper::sqlNULL( $this->managerApprovalOn ) . ", " .
               "     user_approval_on = " . DBHelper::sqlNULL( $this->userApprovalOn ) . " " . 
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function setUserId($userId)
    {
      $this->userId = $userId;
    }
    
    public function getUserId()
    {
      return $this->userId;
    }

    public function getEvalYearId()
    {
      return $this->evalYearId;
    }
    
    public function setEvalYearId($evalYearId)
    {
      $this->evalYearId = $evalYearId;
    }
    
    public function getManagerApprovalOn()
    {
      return $this->managerApprovalOn;
    }
    
    public function setManagerApprovalOn($managerApprovalOn)
    {
      $this->managerApprovalOn = $managerApprovalOn;
    }
    
    public function getRequestApprovalOn()
    {
      return $this->requestApprovalOn;
    }
    
    public function setRequestApprovalOn($requestApprovalOn)
    {
      $this->requestApprovalOn = $requestApprovalOn;
    }
    
    public function getUserApprovalOn()
    {
      return $this->userApprovalOn;
    }
    
    public function setUserApprovalOn($userApprovalOn)
    {
      $this->userApprovalOn = $userApprovalOn;
    }
        
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getUser() 
    {
      if( isset( $this->user ) == false )
      {
        $this->user = User::load( $this->userId );
      }
      
      return $this->user; 
    }
    
    public function getEvalYear() 
    {
      if( isset( $this->evalYear ) == false )
      {
        $this->evalYear = EvalYear::load( $this->evalYearId );
      }
      
      return $this->evalYear; 
    }
  }
?>
