<?php
  class DBHelper
  {
  	/******************************************************************************/
		/******************************* LIST FUNCTIONS *******************************/ 
		/******************************************************************************/
		public static function getListRecords( $sql )
		{
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while($record = getRecord($rs) )
		  {
		    $records[] = $record;
		  }
		  
		  return $records;
		}
		
		public static function getList( $table, $restrict = false, $order_by = "id ASC" )
		{
		  global $CONST_TABLE_PREFIX;
		    
		  $sql = "SELECT * FROM ${CONST_TABLE_PREFIX}_${table} " . ( $restrict ? "WHERE " . $restrict : "" ) . " ORDER BY $order_by";  
		
		  return getListRecords( $sql );
		}	
    
    public static function sqlNULL( $value, $escape = false )
    {
      if( isset( $value ) == false || $value == "" )
      {
        return "NULL";
      }
      else
      {
        if( is_string( $value ) )
        {
          if( $escape )
          {
            return "'" . mysql_real_escape_string( $value ) . "'";    
          }
          else
          {
            return "'" . $value . "'";    
          }          
        }
        else
        {
          return $value;
        }
        
      }
    }
  }
?>