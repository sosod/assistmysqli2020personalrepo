<?php
  class UserSetup extends Base
  {
    public static $TABLE_NAME = "pms_usersetup";
    public static $DISPLAY_NAME = "User setup";  
    
    private $userId;
    private $categoryId;
    private $section;
    private $jobLevel;
    private $jobNumber;
    private $hrManager;

    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */ 
    private $user;
    private $category;
                
    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new UserSetup();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new UserSetup();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new UserSetup();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setUserId( self::initValue( $values["user_id"], $escape ) );
      $this->setCategoryId( self::initValue( $values["category_id"], $escape ) );
      $this->setSection( self::initValue( $values["section"], $escape ) );               
      $this->setJobLevel( self::initValue( $values["job_level"], $escape ) );
      $this->setJobNumber( self::initValue( $values["job_number"], $escape ) );  
      $this->setHRManager( self::initValue( $values["hr_manager"], $escape ) );      
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( user_id, category_id, section, job_level, hr_manager, job_number ) " .
               "VALUES " .
               "( '{$this->userId}', {$this->categoryId}, '{$this->section}', {$this->jobLevel}, {$this->hrManager}, '{$this->jobNumber}' )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET user_id = '{$this->userId}', " .
               "     category_id = {$this->categoryId}, " .
               "     section = '{$this->section}', " .
               "     job_level = {$this->jobLevel}, " .
               "     hr_manager = {$this->hrManager}, " . 
               "     job_number = '{$this->jobNumber}' " . 
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function setUserId($userId)
    {
      $this->userId = $userId;
    }
    
    public function getUserId()
    {
      return $this->userId;
    }
    
    public function setCategoryId($categoryId)
    {
      $this->categoryId = $categoryId;
    }
    
    public function getCategoryId()
    {
      return $this->categoryId;
    }
    
    public function setSection($section)
    {
      $this->section = $section;
    }
    
    public function getSection()
    {
      return $this->section;
    }
    
    public function setJobLevel($jobLevel)
    {
      $this->jobLevel = $jobLevel;
    }
    
    public function getJobLevel()
    {
      return $this->jobLevel;
    }
    
    public function setHRManager($hrManager)
    {
      $this->hrManager = $hrManager;
    }
    
    public function getHRManager()
    {
      return $this->hrManager;
    }
    
    public function setJobNumber($jobNumber)
    {
      $this->jobNumber = $jobNumber;
    }
    
    public function getJobNumber()
    {
      return $this->jobNumber;
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getUser() 
    {
      if( isset( $this->user ) == false )
      {
        $this->user = User::load( $this->userId );
      }
      
      return $this->user; 
    }
    
    public function getCategory() 
    {
      if( isset( $this->category ) == false )
      {
        $this->category = Category::load( $this->categoryId );
      }
      
      return $this->category; 
    }
  }
?>
