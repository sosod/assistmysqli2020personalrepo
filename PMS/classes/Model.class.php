<?php

interface Model
{
  public static function load( $id );
  public static function loadWhere( $condition );
  public static function records( $condition = false, $orderBy = "id" );
  public function init( $values, $escape = false );  
  public function save();
}

?>