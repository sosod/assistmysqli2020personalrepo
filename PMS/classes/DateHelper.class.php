<?php
  class DateHelper
  {
    public static function getPeriodStartDate( $evalYearStartDate, $periodFrequency, $period )
    {
      $months = ( $period - 1 ) * ( 12 / $periodFrequency );
      
      $periodStartDate = new Date( $evalYearStartDate );
      $periodStartDate->addMonths( $months );

      return $periodStartDate;
    }
    
    public static function getPeriodEndDate( $evalYearStartDate, $periodFrequency, $period )
    {
      $months = ( $period - 1 ) * ( 12 / $periodFrequency ) + ( ( 12 / $periodFrequency ) - 1 );
      
      $periodEndDate = new Date( $evalYearStartDate );
      $periodEndDate->addMonths( $months );
      
      $dateCalc = new Date_Calc();
      $lastDay = $dateCalc->getLastDayOfMonth( $periodEndDate->getMonth(), $periodEndDate->getYear() );
      
      $periodEndDate->setDay( $lastDay, true );

      return $periodEndDate;
    }
    
    public static function getPeriodDisplay( $evalYearStartDate, $periodFrequency, $period )
    {
      $start = self::getPeriodStartDate( $evalYearStartDate, $periodFrequency, $period );
      $end = self::getPeriodEndDate( $evalYearStartDate, $periodFrequency, $period );
            
      return $start->format3( "j M y" ) . " to " . $end->format3( "j M y" );
    }
    
    public static function isFuturePeriod( $evalYearStartDate, $periodFrequency, $period )
    {
      $start = self::getPeriodStartDate( $evalYearStartDate, $periodFrequency, $period );
      
      $currentDate = new Date();
      
      if( $currentDate->before( $start ) )
      {
        return true;
      }
      
      return false;
    }
  }
?>