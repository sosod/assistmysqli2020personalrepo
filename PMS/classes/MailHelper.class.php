<?php
  class MailHelper
  {
    public static function processTemplate( $fileName, $replacements ) 
    {
      $template = '';
      
      if( $fp = fopen( $fileName, 'rb' ) )
      {
        while( !feof( $fp ) )
        {
          $template .= fread( $fp, 1024 );
        }
        
        fclose( $fp );
        
        foreach( $replacements as $variable => $text )
        {
          $template = str_replace( $variable, $text, $template );
        } 
        
        return $template;
      }

      return false;
    }
    
    public static function sendMail( $toAddress, $toName, $subject, $body, $from = "assist@ignite4u.co.za", $replyTo = "assist@ignite4u.co.za" )
    {
      debug( "sendMail called with: " );
      debug( "  + toAddress = " . $toAddress );      
      debug( "  + toName = " . $toName );      
      debug( "  + subject = " . $subject );
      debug( "  + body = " . $body );                        
      
      $headers = "From: {$from}\r\nReply-To: {$replyTo}\r\n";  
      
      mail( $toAddress, $subject, $body, $headers );
    }
  
  }
?>