<?php
  class Category extends Base 
  {
    public static $TABLE_NAME = "pms_category";
    public static $DISPLAY_NAME = "Category";  
    
    private $name;
    private $periodFrequency;
    private $weights;
    private $minKPAs;
    private $maxKPAs;
    private $organisationKPAWeight;
    private $learningKPAWeight;
    private $jobKPAWeight;

    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new Category();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new Category();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new Category();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setName( self::initValue( $values["name"], $escape ) );
      $this->setPeriodFrequency( self::initValue( $values["period_frequency"], $escape ) );
      $this->setWeights( self::initValue( $values["weights"], $escape ) );
      $this->setMinKPAs( self::initValue( $values["min_kpas"], $escape ) );      
      $this->setMaxKPAs( self::initValue( $values["max_kpas"], $escape ) );      
      $this->setOrganisationKPAWeight( self::initValue( $values["org_kpas_weight"], $escape ) );
      $this->setLearningKPAWeight( self::initValue( $values["learn_kpas_weight"], $escape ) );
      $this->setJobKPAWeight( self::initValue( $values["job_kpas_weight"], $escape ) );                        
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( name, period_frequency, weights, min_kpas, max_kpas, org_kpas_weight, learn_kpas_weight, job_kpas_weight ) " .
               "VALUES " .
               "( '{$this->name}', {$this->periodFrequency}, {$this->weights}, {$this->min_kpas}, {$this->max_kpas}, {$this->org_kpas_weight}, {$this->learn_kpas_weight}, {$this->job_kpas_weight} )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET name = '{$this->name}', " .
               "     period_frequency = {$this->periodFrequency}, " .
               "     weights = {$this->weights}, " .
               "     min_kpas = {$this->minKPAs}, " .
               "     max_kpas = {$this->maxKPAs}, " .               
               "     org_kpas_weight = {$this->organisationKPAWeight}, " .
               "     learn_kpas_weight = {$this->learningKPAWeight}, " .
               "     job_kpas_weight = {$this->jobKPAWeight} " .
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    }

    /* --------------------------------------------------------------------------------------- */   
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */      
    public static function selectList()
    {
      $list = array();  
  
      foreach( self::records() as $record )
      {
        $list[] = array( $record->getId(), $record->getName() );    
      }
      
      return $list;      
    } 
    
    public function getTotalKPAWeight()
    {
      return $this->organisationKPAWeight + $this->learningKPAWeight + $this->jobKPAWeight;
    }

    public function getKPAWeight( $type )
    {
      if( $type == "org" )
      {
        return $this->organisationKPAWeight;  
      }
      else if( $type == "job" )
      {
        return $this->jobKPAWeight;  
      }
      else if( $type == "learning" )
      {
        return $this->learningKPAWeight;  
      }
    }

    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getName()
    {
      return $this->name;
    }
    
    public function setName($name)
    {
      $this->name = $name;
    }

    public function getPeriodFrequency()
    {
      return $this->periodFrequency;
    }
    
    public function setPeriodFrequency($periodFrequency)
    {
      $this->periodFrequency = $periodFrequency;
    }
    
    public function getWeights()
    {
      return $this->weights;
    }
    
    public function setWeights($weights)
    {
      $this->weights = $weights;
    }    
    
    public function getJobKPAWeight()
    {
      return $this->jobKPAWeight;
    }
    
    public function setJobKPAWeight($jobKPAWeight)
    {
      $this->jobKPAWeight = $jobKPAWeight;
    }
    
    public function getLearningKPAWeight()
    {
      return $this->learningKPAWeight;
    }
    
    public function setLearningKPAWeight($learningKPAWeight)
    {
      $this->learningKPAWeight = $learningKPAWeight;
    }
    
    public function getMaxKPAs()
    {
      return $this->maxKPAs;
    }
    
    public function setMaxKPAs($maxKPAs)
    {
      $this->maxKPAs = $maxKPAs;
    }

    public function getMinKPAs()
    {
      return $this->minKPAs;
    }

    public function setMinKPAs($minKPAs)
    {
      $this->minKPAs = $minKPAs;
    }
    
    public function getOrganisationKPAWeight()
    {
      return $this->organisationKPAWeight;
    }
    
    public function setOrganisationKPAWeight($organisationKPAWeight)
    {
      $this->organisationKPAWeight = $organisationKPAWeight;
    }
}
?>
