<?php
  class User extends Base
  {
    public static $TABLE_NAME = "timekeep";
    public static $DISPLAY_NAME = "User";  
        
    private $title; //tktitle
    private $firstName; //tkname
    private $lastName; //tksurname
    private $email; //tkemail  
    private $department;
    private $section;
    private $jobTitle;
    private $jobLevel;
    private $jobNumber;    
    private $managerId;
    private $manager;   
    private $managerEmail;    
    private $gender;
    private $race;
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */ 
    private $setup;        
    private $managedUsers;
    
    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */ 
    public static function load( $id )
    {
      $obj = new User();

      $sql = " SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . 
             " WHERE tkid = '{$id}' ";
      
      $obj->init( getRecord( mysql_query( $sql, getConnection() ) ) );
      
      return $obj;
    }
    
    public static function loadExtended( $id )
    {
      $obj = new User();

      $sql  = "SELECT u.*, us.section, us.job_level, us.job_number, d.value AS department, j.value AS job_title, " .
              " (SELECT CONCAT( um.tkname, ' ', um.tksurname ) FROM assist_".getCompanyCode()."_" . self::$TABLE_NAME . " um WHERE um.tkid = u.tkmanager ) AS manager, " .
              " (SELECT um.tkemail FROM assist_".getCompanyCode()."_" . self::$TABLE_NAME . " um WHERE um.tkid = u.tkmanager ) AS manager_email, " .
              " (SELECT udfvvalue FROM assist_".getCompanyCode()."_udfvalue WHERE udfvindex = 4 AND udfvcode = u.tkrace ) AS race " .
              " FROM assist_".getCompanyCode()."_" . self::$TABLE_NAME . " u, " .
              " assist_".getCompanyCode()."_pms_usersetup us, " .
              " assist_".getCompanyCode()."_list_dept d, " .
              " assist_".getCompanyCode()."_list_jobtitle j " .
              " WHERE tkid = '" . $id . "' " .
              " AND us.user_id = u.tkid " .
              " AND d.id = u.tkdept " .
              " AND j.id = u.tkdesig ";   
                
      $obj->init( getRecord( mysql_query( $sql, getConnection() ) ), true );
      
      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "tkid" )
    {
      $sql = "SELECT u.* FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " u, " .
             "assist_".getCompanyCode()."_menu_modules_users mu " . 
             "WHERE u.tkid = mu.usrtkid AND mu.usrmodref = 'PMS' " .
             ( $condition ? "AND " . $condition : "" ) . 
             " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new User();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $extended = false )
    {
      $this->setId( $values["tkid"] );
      $this->setTitle( $values["tktitle"] );
      $this->setFirstName( $values["tkname"] );
      $this->setLastName( $values["tksurname"] );
      $this->setEmail( $values["tkemail"] );      
      $this->setManagerId( $values["tkmanager"] );
      $this->setGender( $values["tkgender"] );
      
      if( $extended )
      {
        $this->setDepartment( $values["department"] );
        $this->setSection( $values["section"] );
        $this->setJobTitle( $values["job_title"] );
        $this->setJobLevel( $values["job_level"] );                 
        $this->setJobNumber( $values["job_number"] );                                        
        $this->setManager( $values["manager"] );
        $this->setManagerEmail( $values["manager_email"] );        
        $this->setRace( $values["race"] );          
      }
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */  
    public function setTitle($title)
    {
      $this->title = $title;
    }
    
    public function getTitle()
    {
      return $this->title;
    }
    
    public function setFirstName($firstName)
    {
      $this->firstName = $firstName;
    }
    
    public function getFirstName()
    {
      return $this->firstName;
    }
    
    public function setLastName($lastName)
    {
      $this->lastName = $lastName;
    }
    
    public function getLastName()
    {
      return $this->lastName;
    }
    
    public function setEmail($email)
    {
      $this->email = $email;
    }
    
    public function getEmail()
    {
      return $this->email;
    }
    
    public function setDepartment($department)
    {
      $this->department = $department;
    }
    
    public function getDepartment()
    {
      return $this->department;
    }
    
    public function setSection($section)
    {
      $this->section = $section;
    }
    
    public function getSection()
    {
      return $this->section;
    }
    
    public function setJobTitle($jobTitle)
    {
      $this->jobTitle = $jobTitle;
    }
    
    public function getJobTitle()
    {
      return $this->jobTitle;
    }
    
    public function setJobLevel($jobLevel)
    {
      $this->jobLevel = $jobLevel;
    }
    
    public function getJobLevel()
    {
      return $this->jobLevel;
    }
    
    public function setJobNumber($jobNumber)
    {
      $this->jobNumber = $jobNumber;
    }
    
    public function getJobNumber()
    {
      return $this->jobNumber;
    }
    
    public function setManagerId($managerId)
    {
      $this->managerId = $managerId;
    }
    
    public function getManagerId()
    {
      return $this->managerId;
    }
    
    public function setManager($manager)
    {
      $this->manager = $manager;
    }
    
    public function getManager()
    {
      return $this->manager;
    }
    
    public function setManagerEmail($managerEmail)
    {
      $this->managerEmail = $managerEmail;
    }
    
    public function getManagerEmail()
    {
      return $this->managerEmail;
    }
    
    public function setGender($gender)
    {
      $this->gender = $gender;
    }
    
    public function getGender()
    {
      return $this->gender;
    }
    
    public function setRace($race)
    {
      $this->race = $race;
    }
    
    public function getRace()
    {
      return $this->race;
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getSetup() 
    {
      if( isset( $this->setup ) == false )
      {
        $this->setup = UserSetup::loadWhere( "user_id = '{$this->getId()}'" );
      }
      
      return $this->setup; 
    }
    
    public function getCategory() 
    {
      return $this->getSetup()->getCategory(); 
    }
    
    public function getManagedUsers() 
    {
      if( isset( $this->managedUsers ) == false )
      {
        $this->managedUsers = User::records( "( tkmanager = '{$this->getId()}' OR tkid = '{$this->getId()}' )" );
      }
      
      return $this->managedUsers; 
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* ---------------------------------- HELPER FUNCTIONS ----------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    public static function selectList()
    {
      $list = array();  
  
      foreach( self::records() as $record )
      {
        $list[] = array( $record->getId(), $record->getFullName() );    
      }
      
      return $list;      
    }

    public function getFullName()
    {
      return $this->firstName . " " . $this->lastName;
    } 
    
    public function getGenderDisplay()
    {
      return $this->gender == "M" ? "Male" : "Female";
    }  
    
    public function getManagedUsersSelectList()
    {
      $list = array();  
  
      foreach( $this->getManagedUsers() as $record )
      {
        $list[] = array( $record->getId(), $record->getFullName() );    
      }
      
      return $list; 
    }
    
    public static function getManagerSelectList() 
    {
      $list = array();  
      
      $sql = "SELECT u.tkid, u.tkname, u.tksurname FROM assist_".getCompanyCode()."_timekeep u WHERE u.tkid in (SELECT um.tkmanager from assist_".getCompanyCode()."_timekeep um, assist_".getCompanyCode()."_menu_modules_users mu WHERE um.tkid = mu.usrtkid AND mu.usrmodref = 'PMS' GROUP BY um.tkid) ";
      
		  $rs = getResultSet( $sql );
		  
		  while( $record = getRecord( $rs ) )
		  {
		    $list[] = array( $record["tkid"], $record["tkname"] . " " . $record["tksurname"] );    
		  }
      
      return $list; 
    }
  }   
?>
