<?php
  class ViewHelper
  {
    public static $SECTION_ACTIVATION = "activation";
    public static $SECTION_EVALUATION = "evaluation";
    public static $SECTION_REPORTS = "reports";
    public static $ACTION_SAVE = "save";
    public static $ACTION_DELETE = "delete";
    public static $ACTION_SELECT_USER = "select_user";
    public static $ACTION_SELECT_USER_YEAR = "select_user_year";
    public static $ACTION_SELECT_USER_YEAR_PERIOD = "select_user_year_period";
    public static $ACTION_SELECT_YEAR_PERIOD = "select_year_period";
    public static $ACTION_SELECT_YEAR = "select_year";
    public static $ACTION_REQUEST_APPROVAL = "request_approval";
    public static $ACTION_MANAGER_APPROVAL = "manager_approval";
    public static $ACTION_USER_APPROVAL = "user_approval";
    public static $ACTION_USER_EVALUATION_COMPLETE = "user_evaluation_complete";
    public static $ACTION_MANAGER_EVALUATION_COMPLETE = "manager_evaluation_complete";
    public static $ACTION_JOINT_EVALUATION_COMPLETE = "joint_evaluation_complete";
    public static $ACTION_JOINT_EVALUATION_APPROVED = "joint_evaluation_approved";
    public static $ACTION_FISHBOWL_EVALUATION_COMPLETE = "fishbowl_evaluation_complete";
    public static $ACTION_FISHBOWL_EVALUATION_APPROVED = "fishbowl_evaluation_approved";

    private static $PERIOD_FREQUENCIES = array( "12"=>"Monthly",
                                                "6"=>"Bi monthly",
                                                "4"=>"Quarterly",
                                                "3"=>"Four monthly",
                                                "2"=>"Bi annually",
                                                "1"=>"Annually" );

    private static $JOB_LEVELS = array( "1"=>"01 - General Worker - Junior Level",
                                  		  "2"=>"02 - General Worker - Middle Level",
                                  		  "3"=>"03 - General Worker - Senior Level",
                                        "4"=>"04 - Overseer - Junior Level",
                                  		  "5"=>"05 - Overseer - Middle Level",
                                  		  "6"=>"06 - Overseer - Senior Level",
                                        "7"=>"07 - Supervisor - Junior Level",
                                  		  "8"=>"08 - Supervisor - Middle Level",
                                  		  "9"=>"09 - Supervisor - Senior Level",
                                        "10"=>"10 - Junior Management - Junior Level",
                                  		  "11"=>"11 - Junior Management - Middle Level",
                                  		  "12"=>"12 - Junior Management - Senior Level",
                                        "13"=>"13 - Middle Management - Junior Level",
                                  		  "14"=>"14 - Middle Management - Middle Level",
                                  		  "15"=>"15 - Middle Management - Senior Level",
                                        "16"=>"16 - Senior Management - Junior Level",
                                  		  "17"=>"17 - Senior Management - Middle Level",
                                  		  "18"=>"18 - Senior Management - Senior Level",
                                        "19"=>"19 - Executive Management - Junior Level",
                                  		  "20"=>"20 - Executive Management - Middle Level",
                                  		  "21"=>"21 - Executive Management - Senior Level" );

    public static $STATUSES = array( "active" => "Active",
                                     "inactive" => "Inactive" );

    public static $PROFICIENCY_LEVELS = array( "basic" => "Basic",
                                               "competent" => "Competent",
                                               "advanced" => "Advanced",
                                               "expert" => "Expert" );

  	/******************************************************************************/
		/******************************* LIST FUNCTIONS *******************************/
		/******************************************************************************/
		public static function getYesNoSelectList()
		{
		  $list = array();

		  $list[] = array( "0", "No" );
		  $list[] = array( "1", "Yes" );

		  return $list;
		}

		public static function getNumericSelectList( $low=0, $high=100 )
		{
		  $list = array();

		  for( $i = $low; $i <= $high; $i++ )
		  {
		    $list[] = array( "$i", "$i" );
		  }

		  return $list;
		}

    public static function getPeriodFrequencySelectList()
		{
		  $list = array();

      foreach( self::$PERIOD_FREQUENCIES as $periods => $desc )
      {
        $list[] = array( $periods, $desc );
      }

		  return $list;
		}

    public static function displayPeriodFrequency( $frequency )
    {
      return self::$PERIOD_FREQUENCIES[ $frequency ];
    }

    public static function getJobLevelSelectList()
		{
		  $list = array();

		  foreach( self::$JOB_LEVELS as $level => $desc )
      {
        $list[] = array( $level, $desc );
      }

		  return $list;
		}

    public static function statusSelectList()
    {
      $list = array();

      foreach( self::$STATUSES as $status => $desc )
      {
        $list[] = array( $status, $desc );
      }

      return $list;
    }

    public static function proficiencyLevelSelectList()
    {
      $list = array();

      foreach( self::$PROFICIENCY_LEVELS as $level => $desc )
      {
        $list[] = array( $level, $desc );
      }

      return $list;
    }

		/******************************************************************************/
		/**************************** INTERNAL FUNCTIONS ******************************/
		/******************************************************************************/

    public static function arrayContains( $array, $value )
    {
      foreach( $array as $element )
      {
        if( $element == $value )
        {
          return true;
        }
      }

      return false;
    }

    public static function arrayStartWithContains( $array, $value )
    {
      foreach( $array as $element )
      {
        if( strpos( $element, $value ) === 0 )
        {
          return true;
        }
      }

      return false;
    }


    /******************************************************************************/
		/******************************* FLOW FUNCTIONS *******************************/
		/******************************************************************************/
    public static function redirect( $url )
    {
      header( "Location: $url" );
      exit();
    }

		/******************************************************************************/
		/****************************** MACRO FUNCTIONS *******************************/
		/******************************************************************************/

		public static function breadcrumbs( $breadcrumbs )
		{
      $header = "<h1 id=\"breadcrumbs\">";

      $cnt = 0;
      foreach( $breadcrumbs as $crumb => $url )
      {
        if( $cnt != 0 )
        {
          $header .= " &raquo; ";
        }

        if( exists( $url ) )
        {
          $header .= "<a href=\"{$url}\">{$crumb}</a>";
        }
        else
        {
          $header .= $crumb;
        }

        $cnt++;
      }

      $header .= "</h1>";

      echo $header;
		}

    public static function actions( $buttons, $isActive = true )
		{
		  $actions = "<div class=\"actions\">";

      foreach( $buttons as $name=>$url )
      {
        $actions .= "<input type=\"button\" value=\"{$name}\" onclick=\"redirect('{$url}');\" " . ($isActive == false ? "disabled=\"disabled\"" : "") . "/> ";
      }

      $actions .= "</div>";

      echo $actions;
    }

    public static function validation( $formName, $errors = false )
		{
      echo "<script type=\"text/javascript\">$(document).ready(function(){ $(\"#{$formName}\").validate(); }); </script>";

      if( $errors )
      {
        self::errors( $errors );
      }
    }

    public static function button( $url, $value, $isActive = true )
		{
		  if( $value == "Delete" )
      {
        echo "<input type=\"button\" onclick=\"confirmDelete('" . $url . "');\" value=\"" . $value ."\" " . ( $isActive == false ? "disabled=\"disabled\"" : "") . " />";
      }
      else
      {
        echo "<input type=\"button\" onclick=\"redirect('" . $url . "');\" value=\"" . $value ."\" " . ( $isActive == false ? "disabled=\"disabled\"" : "") . " />";
      }
		}

    public static function select( $name, $options, $value, $required, $emptyOption = "Select from list ...", $id = false, $style = false )
    {
      echo "<select " . ($id ? "id=\"$id\" " : "") . "name=\"" . $name . "\" " . (($required) ? "class=\"required\"" : "") . (($style) ? "style=\"$style\"" : "") . " >";

      if( $emptyOption )
      {
        echo "<option value=\"\">$emptyOption</option>";
      }

      foreach ($options as $option)
      {
        echo "<option value=\"" . htmlentities($option[0]) . "\" " . (($option[0] == $value) ? "selected=\"selected\"" : "") . ">" . $option[1] . "</option>";
      }

      echo "</select>";
    }

    public static function multiselect( $name, $options, $required, $values )
    {
      echo "<select name=\"" . $name . "\" " . (($required) ? "class=\"required\"" : "") . " multiple=\"multiple\" style=\"width:200px;\" >";

      foreach ($options as $option)
      {
        echo "<option value=\"" . htmlentities($option[0]) . "\" " . ( self::arrayContains( $values, $option[0] )  ? "selected=\"selected\"" : "") . ">" . $option[1] . "</option>";
      }

      echo "</select>";
    }

    public static function errors( $errors )
    {
      if( count( $errors ) > 0 )
      {
        echo "<h2 class=\"error-messages-header\">Please correct the following errors:</h2>";
        echo "<ul class=\"error-messages\">";

        foreach( $errors as $error )
        {
          echo "<li>$error</li>";
        }

        echo "</ul>";
      }
    }

    public static function messages( $messages )
    {
      if( count( $messages ) > 0 )
      {
        echo "<ul class=\"messages\">";

        foreach( $messages as $message )
        {
          echo "<li>$message</li>";
        }

        echo "</ul>";
      }
    }

    /******************************************************************************/
		/****************************** HELPER FUNCTIONS ******************************/
		/******************************************************************************/
    public static function boolYesNo( $boolValue )
    {
      return $boolValue ? "Yes" : "No";
    }

    public static function ifNull( $value, $default )
    {
      return $value != null ? $value : $default;
    }

    public static function displayStatus( $status )
    {
      return self::$STATUSES[ $status ];
    }

    public static function displayProficiencyLevel( $level )
    {
      return self::$PROFICIENCY_LEVELS[ $level ];
    }

    public static function displayJobLevel( $level )
    {
      return self::$JOB_LEVELS[ $level ];
    }
  }
?>