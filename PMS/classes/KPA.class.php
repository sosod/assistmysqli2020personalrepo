<?php
  class KPA extends Base 
  {
    public static $TABLE_NAME = "pms_kpa";
    public static $DISPLAY_NAME = "KPA";  
    
    public static $TYPES = array( "org" => "Organisational",
                                  "job" => "Job", 
                                  "learning" => "Learning" );
    
    private $userId;
    private $evalYearId;
    private $type;
    private $orgGoalId;
    private $jobFunctionId;    
    private $name;
    private $objective;
    private $measurement;
    private $baseline;
    private $targetUnit;
    private $weight;
    private $comments;
    private $deadline;
    private $competencyId;
    private $proficiencyLevel;
    private $createdById;
    private $createdOn;

    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */ 
    private $user;
    private $evalYear;
    private $createdBy;
    private $orgGoal;
    private $jobFunction;
    private $competency;    
    private $targets;
    private $evaluation;        
                
    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new KPA();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new KPA();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new KPA();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public static function kpasWithTarget( $type, $userId, $evalYearId, $period )
    {
      $sql = "SELECT kpa.* FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " kpa, " .
             "              assist_" . getCompanyCode() . "_" . KPATarget::$TABLE_NAME . " kpatarget " . 
             "WHERE kpa.id = kpatarget.kpa_id " .
             "AND kpatarget.period = " . $period . " " .
             "AND kpatarget.target != '' " .
             "AND kpa.type = '" . $type . "' " .
             "AND kpa.user_id = '" . $userId . "' " .             
             "AND kpa.eval_year_id = " . $evalYearId . " " .             
             "ORDER BY kpa.created_on ASC ";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new KPA();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setUserId( self::initValue( $values["user_id"], $escape ) );
      $this->setEvalYearId( self::initValue( $values["eval_year_id"], $escape ) );      
      $this->setType( self::initValue( $values["type"], $escape ) );
      $this->setOrgGoalId( self::initValue( $values["org_goal_id"], $escape ) );
      $this->setJobFunctionId( self::initValue( $values["job_function_id"], $escape ) );            
      $this->setName( self::initValue( $values["name"], $escape ) );
      $this->setObjective( self::initValue( $values["objective"], $escape ) );
      $this->setMeasurement( self::initValue( $values["measurement"], $escape ) );
      $this->setBaseline( self::initValue( $values["baseline"], $escape ) );      
      $this->setTargetUnit( self::initValue( $values["target_unit"], $escape ) );      
      $this->setWeight( self::initValue( $values["weight"], $escape ) );      
      $this->setComments( self::initValue( $values["comments"], $escape ) ); 
      $this->setDeadline( self::initValue( $values["deadline"], $escape ) );            
      $this->setCompetencyId( self::initValue( $values["competency_id"], $escape ) );            
      $this->setProficiencyLevel( self::initValue( $values["proficiency_level"], $escape ) );            
      $this->setCreatedById( self::initValue( $values["created_by_id"], $escape ) );
      $this->setCreatedOn( self::initValue( $values["created_on"], $escape ) );
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( user_id, eval_year_id, type, org_goal_id, job_function_id, name, objective, measurement, baseline, target_unit, weight, comments, deadline, competency_id, proficiency_level, created_by_id, created_on ) " .
               "VALUES " .
               "( '{$this->userId}', {$this->evalYearId}, '{$this->type}', " . DBHelper::sqlNULL( $this->orgGoalId ) . ", " . DBHelper::sqlNULL( $this->jobFunctionId ) . ", '{$this->name}', '{$this->objective}', '{$this->measurement}', '{$this->baseline}', '{$this->targetUnit}', {$this->weight}, '{$this->comments}', '{$this->deadline}', {$this->competencyId}, '{$this->proficiencyLevel}', '{$this->createdById}', CURRENT_TIMESTAMP )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET user_id = '{$this->userId}', " .
               "     eval_year_id = '{$this->evalYearId}', " .
               "     type = '{$this->type}', " .
               "     org_goal_id = " . DBHelper::sqlNULL( $this->orgGoalId ) . ", " .               
               "     job_function_id = " . DBHelper::sqlNULL( $this->jobFunctionId ) . ", " .                              
               "     name = '{$this->name}', " .
               "     objective = '{$this->objective}', " .
               "     measurement = '{$this->measurement}', " .
               "     baseline = '{$this->baseline}', " .
               "     target_unit = '{$this->targetUnit}', " .
               "     weight = {$this->weight}, " .
               "     comments = '{$this->comments}', " .               
               "     deadline = '{$this->deadline}', " .
               "     competency_id = '{$this->competencyId}', " .
               "     proficiency_level = '{$this->proficiencyLevel}' " .                              
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 
    
    /* --------------------------------------------------------------------------------------- */   
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */      
    public static function typeSelectList()
    {
      $list = array();  
  
      foreach( self::$TYPES as $type => $desc )
      {
        $list[] = array( $type, $desc );    
      }
      
      return $list;      
    }
    
    public static function displayType( $type )
    {
      return self::$TYPES[ $type ];
    }
    
    public static function totalWeight( $kpas )
    {
      $totalWeight = 0;
      
      if( isset( $kpas) && count( $kpas ) > 0 )
      {
        foreach( $kpas as $kpa )
        {
          $totalWeight += $kpa->getWeight();
        }
      }
      
      return $totalWeight;
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function setUserId($userId)
    {
      $this->userId = $userId;
    }
    
    public function getUserId()
    {
      return $this->userId;
    }

    public function getBaseline()
    {
      return $this->baseline;
    }
    
    public function setBaseline($baseline)
    {
      $this->baseline = $baseline;
    }
    
    public function getComments()
    {
      return $this->comments;
    }
    
    public function setComments($comments)
    {
      $this->comments = $comments;
    }
    
    public function getCreatedById()
    {
      return $this->createdById;
    }
    
    public function setCreatedById($createdById)
    {
      $this->createdById = $createdById;
    }
    
    public function getCreatedOn()
    {
      return $this->createdOn;
    }
    
    public function setCreatedOn($createdOn)
    {
      $this->createdOn = $createdOn;
    }
    
    public function getEvalYearId()
    {
      return $this->evalYearId;
    }
    
    public function setEvalYearId($evalYearId)
    {
      $this->evalYearId = $evalYearId;
    }
    
    public function getMeasurement()
    {
      return $this->measurement;
    }
    
    public function setMeasurement($measurement)
    {
      $this->measurement = $measurement;
    }
    
    public function getName()
    {
      return $this->name;
    }
    
    public function setName($name)
    {
      $this->name = $name;
    }
    
    public function getObjective()
    {
      return $this->objective;
    }
    
    public function setObjective($objective)
    {
      $this->objective = $objective;
    }
    
    public function getTargetUnit()
    {
      return $this->targetUnit;
    }
    
    public function setTargetUnit($targetUnit)
    {
      $this->targetUnit = $targetUnit;
    }
    
    public function getType()
    {
      return $this->type;
    }
    
    public function setType($type)
    {
      $this->type = $type;
    }
    
    public function getWeight()
    {
      return $this->weight;
    }
    
    public function setWeight($weight)
    {
      $this->weight = $weight;
    }

    public function getJobFunctionId()
    {
      return $this->jobFunctionId;
    }
    
    public function setJobFunctionId($jobFunctionId)
    {
      $this->jobFunctionId = $jobFunctionId;
    }
    
    public function getOrgGoalId()
    {
      return $this->orgGoalId;
    }
    
    public function setOrgGoalId($orgGoalId)
    {
      $this->orgGoalId = $orgGoalId;
    }

    public function getDeadline()
    {
      return $this->deadline;
    }
    
    public function setDeadline($deadline)
    {
      $this->deadline = $deadline;
    }

    public function getCompetencyId()
    {
      return $this->competencyId;
    }
    
    public function setCompetencyId($competencyId)
    {
      $this->competencyId = $competencyId;
    }
    
    public function getProficiencyLevel()
    {
      return $this->proficiencyLevel;
    }
    
    public function setProficiencyLevel($proficiencyLevel)
    {
      $this->proficiencyLevel = $proficiencyLevel;
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getUser() 
    {
      if( isset( $this->user ) == false )
      {
        $this->user = User::load( $this->userId );
      }
      
      return $this->user; 
    }
    
    public function getEvalYear() 
    {
      if( isset( $this->evalYear ) == false )
      {
        $this->evalYear = EvalYear::load( $this->evalYearId );
      }
      
      return $this->evalYear; 
    }
    
    public function getCreatedBy() 
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = User::load( $this->createdById );
      }
      
      return $this->createdBy; 
    }
    
    public function getOrgGoal() 
    {
      if( isset( $this->orgGoal ) == false )
      {
        $this->orgGoal = OrgGoal::load( $this->orgGoalId );
      }
      
      return $this->orgGoal; 
    }
    
    public function getJobFunction() 
    {
      if( isset( $this->jobFunction ) == false )
      {
        $this->jobFunction = JobFunction::load( $this->jobFunctionId );
      }
      
      return $this->jobFunction; 
    }
    
    public function getCompetency() 
    {
      if( isset( $this->competency ) == false )
      {
        $this->competency = Competency::load( $this->competencyId );
      }
      
      return $this->competency; 
    }
    
    public function getTargets() 
    {
      if( isset( $this->targets ) == false && $this->isTransient() == false )
      {
        $this->targets = KPATarget::records( " kpa_id = {$this->getId()}", "period ASC" );
      }
      
      return $this->targets; 
    }
    
    public function getTarget( $period )
    {
      $targets = $this->getTargets();
      
      if( isset( $targets ) )
      {
        foreach( $targets as $target )
        {
          if( $target->getPeriod() == $period )
          {
            return $target;
          }
        }  
      }
      
      return new KPATarget();
    }
    
    public function getEvaluation( $period ) 
    {
      if( isset( $this->evaluation ) == false || $this->evaluation->getPeriod() != $period )
      {
        $this->evaluation = KPAEvaluation::loadWhere( "kpa_id = " . $this->getId() . " AND period = " . $period );
      }
      
      return $this->evaluation; 
    }
  }
?>
