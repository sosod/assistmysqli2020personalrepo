<?php
  class KPATarget extends Base
  {
    public static $TABLE_NAME = "pms_kpatarget";
    public static $DISPLAY_NAME = "KPA Target";  
    
    private $kpaId;
    private $period;
    private $target;

    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new KPATarget();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new KPATarget();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new KPATarget();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setKPAId( self::initValue( $values["kpa_id"], $escape ) );
      $this->setPeriod( self::initValue( $values["period"], $escape ) );
      $this->setTarget( self::initValue( $values["target"], $escape ) );
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( kpa_id, period, target ) " .
               "VALUES " .
               "( {$this->kpaId}, {$this->period}, '{$this->target}' )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET kpa_id = '{$this->kpaId}', " .
               "     period = {$this->period}, " .
               "     target = '{$this->target}' " .
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 
    
    /* --------------------------------------------------------------------------------------- */   
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */      
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getKpaId()
    {
      return $this->kpaId;
    }
    
    public function setKpaId($kpaId)
    {
      $this->kpaId = $kpaId;
    }
    
    public function getPeriod()
    {
      return $this->period;
    }
    
    public function setPeriod($period)
    {
      $this->period = $period;
    }
    
    public function getTarget()
    {
      return $this->target;
    }
    
    public function setTarget($target)
    {
      $this->target = $target;
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
}
?>
