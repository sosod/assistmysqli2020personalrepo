<?php
class Base
{
  private $id;
  private $tableName;
  private $displayName;
  
  function __construct( $tableName, $displayName ) 
  {
    $this->tableName = $tableName;
    $this->displayName = $displayName;
  }
  
  /* HELPER FUNCTIONS */
  public function isTransient()
  {
    return exists( $this->id ) == false;
  }
 
  public function getObjectData( $id )
  {
    if( exists( $id ) )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_{$this->tableName} where id={$id}";
    
      return getRecord( getResultSet( $sql ) );
    }
    
    return null;
  }
  
  public function getObjectDataWhere( $condition )
  {
    $sql = "SELECT * FROM assist_" . getCompanyCode() . "_{$this->tableName} where {$condition}";
    
    return getRecord( getResultSet( $sql ) );
  }
  
  public function insert( $sql )
  {
    $sql = "INSERT INTO assist_" . getCompanyCode() . "_{$this->tableName} " . $sql;
    
    debug( $sql );
    
    mysql_query( $sql, getConnection() );
  
    $this->id = mysql_insert_id();  
      
    logTransaction( "INSERT: {$this->displayName}", $sql );
  }
  
  public function update( $sql )
  {
    $sql = "UPDATE assist_" . getCompanyCode() . "_{$this->tableName} " . $sql;
    
    debug( $sql );
    
    mysql_query( $sql, getConnection() ); 
    
    logTransaction( "UPDATE: {$this->displayName}", $sql );
  }
  
  public function delete( $where )
  {
    debug( "where = " . $where );

    $sql = "DELETE FROM assist_" . getCompanyCode() . "_{$this->tableName} WHERE " . $where;

    debug( "sql = " . $sql );
    
    self::baseDelete( $sql, $this->displayName );     
  }
  
  public static function baseDelete( $fullSQL, $identifier )
  {
    mysql_query( $fullSQL, getConnection() ); 
    
    logTransaction( "Delete: " . $identifier, $fullSQL );
  }
  
  public static function initValue( $value, $escape = false )
  {
    return $escape == false ? $value : mysql_real_escape_string( $value );
  }

  /* GETTERS AND SETTERS */   
  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }
}
?>
