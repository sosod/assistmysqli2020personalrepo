<?php
  class LearningActivity extends Base
  {
    public static $TABLE_NAME = "pms_learningactivity";
    public static $DISPLAY_NAME = "Learning Activity";  
    
    public static $PRIORITIES = array( "high" => "High",
                                       "normal" => "Normal", 
                                       "low" => "Low" ); 
                                 
    public static $STATUSES = array( "active" => "Active",
                                     "completed" => "Completed" );                              
    
    private $userId;
    private $name;
    private $priority;
    private $provider;
    private $intervention;
    private $outcome;
    private $deliveryMode;
    private $timeFrames;
    private $workOpportunity;
    private $supportPersonId;
    private $status;    
    private $createdById;
    private $createdOn;    
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */ 
    private $user;
    private $supportPerson;
    private $createdBy;    

    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new LearningActivity();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new LearningActivity();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new LearningActivity();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setUserId( self::initValue( $values["user_id"], $escape ) );
      $this->setName( self::initValue( $values["name"], $escape ) );
      $this->setPriority( self::initValue( $values["priority"], $escape ) );
      $this->setProvider( self::initValue( $values["provider"], $escape ) );
      $this->setIntervention( self::initValue( $values["intervention"], $escape ) );
      $this->setOutcome( self::initValue( $values["outcome"], $escape ) );
      $this->setDeliveryMode( self::initValue( $values["delivery_mode"], $escape ) );
      $this->setTimeFrames( self::initValue( $values["time_frames"], $escape ) );
      $this->setWorkOpportunity( self::initValue( $values["work_opportunity"], $escape ) );
      $this->setSupportPersonId( self::initValue( $values["support_person_id"], $escape ) );
      $this->setStatus( self::initValue( $values["status"], $escape ) );
      $this->setCreatedById( self::initValue( $values["created_by_id"], $escape ) );
      $this->setCreatedOn( self::initValue( $values["created_on"], $escape ) );
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( user_id, name, priority, provider, intervention, outcome, delivery_mode, time_frames, work_opportunity, support_person_id, status, created_by_id, created_on ) " .
               "VALUES " .
               "( '{$this->userId}', '{$this->name}', '{$this->priority}', '{$this->provider}', '{$this->intervention}', '{$this->outcome}', '{$this->deliveryMode}', '{$this->timeFrames}', '{$this->workOpportunity}', '{$this->supportPersonId}', '{$this->status}', '{$this->createdById}', CURRENT_TIMESTAMP )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET name = '{$this->name}', " .
               "     priority = '{$this->priority}', " .
               "     provider = '{$this->provider}', " .
               "     intervention = '{$this->intervention}', " .
               "     outcome = '{$this->outcome}', " .
               "     delivery_mode = '{$this->deliveryMode}', " .
               "     time_frames = '{$this->timeFrames}', " .
               "     work_opportunity = '{$this->workOpportunity}', " .
               "     support_person_id = '{$this->supportPersonId}', " .
               "     status = '{$this->status}' " .
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    }

    /* --------------------------------------------------------------------------------------- */   
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */      
    public static function selectList()
    {
      $list = array();  
  
      foreach( self::records() as $record )
      {
        $list[] = array( $record->getId(), $record->getName() );    
      }
      
      return $list;      
    }
    
    public static function statusSelectList()
    {
      $list = array();  
  
      foreach( self::$STATUSES as $status => $desc )
      {
        $list[] = array( $status, $desc );    
      }
      
      return $list;      
    } 
    
    public static function prioritySelectList()
    {
      $list = array();  
  
      foreach( self::$PRIORITIES as $priority => $desc )
      {
        $list[] = array( $priority, $desc );    
      }
      
      return $list;      
    } 
    
    public static function displayPriority( $priority )
    {
      return self::$PRIORITIES[ $priority ];
    }
    
    public static function displayStatus( $status )
    {
      return self::$STATUSES[ $status ];
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getName()
    {
      return $this->name;
    }
    
    public function getCreatedById()
    {
      return $this->createdById;
    }

    public function setCreatedById($createdById)
    {
      $this->createdById = $createdById;
    }

    public function getCreatedOn()
    {
      return $this->createdOn;
    }

    public function setCreatedOn($createdOn)
    {
      $this->createdOn = $createdOn;
    }

    public function getDeliveryMode()
    {
      return $this->deliveryMode;
    }

    public function setDeliveryMode($deliveryMode)
    {
      $this->deliveryMode = $deliveryMode;
    }

    public function getIntervention()
    {
      return $this->intervention;
    }

    public function setIntervention($intervention)
    {
      $this->intervention = $intervention;
    }

    public function setName($name)
    {
      $this->name = $name;
    }

    public function getOutcome()
    {
      return $this->outcome;
    }

    public function setOutcome($outcome)
    {
      $this->outcome = $outcome;
    }

    public function getPriority()
    {
      return $this->priority;
    }

    public function setPriority($priority)
    {
      $this->priority = $priority;
    }

    public function getProvider()
    {
      return $this->provider;
    }

    public function setProvider($provider)
    {
      $this->provider = $provider;
    }

    public function getStatus()
    {
      return $this->status;
    }

    public function setStatus($status)
    {
      $this->status = $status;
    }

    public function getSupportPersonId()
    {
      return $this->supportPersonId;
    }

    public function setSupportPersonId($supportPersonId)
    {
      $this->supportPersonId = $supportPersonId;
    }

    public function getTimeFrames()
    {
      return $this->timeFrames;
    }

    public function setTimeFrames($timeFrames)
    {
      $this->timeFrames = $timeFrames;
    }

    public function getUserId()
    {
      return $this->userId;
    }

    public function setUserId($userId)
    {
      $this->userId = $userId;
    }

    public function getWorkOpportunity()
    {
      return $this->workOpportunity;
    }

    public function setWorkOpportunity($workOpportunity)
    {
      $this->workOpportunity = $workOpportunity;
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getUser() 
    {
      if( isset( $this->user ) == false )
      {
        $this->user = User::load( $this->userId );
      }
      
      return $this->user; 
    }
    
    public function getSupportPerson() 
    {
      if( isset( $this->supportPerson ) == false )
      {
        $this->supportPerson = User::load( $this->supportPersonId );
      }
      
      return $this->supportPerson; 
    }
    
    public function getCreatedBy() 
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = User::load( $this->createdById );
      }
      
      return $this->createdBy; 
    }
}
?>
