<?php
  class KPAEvaluation extends Base 
  {
    public static $TABLE_NAME = "pms_kpaevaluation";
    public static $DISPLAY_NAME = "KPA Evaluation";  
    
    public static $RATINGS = array( "1" => "1 | Does not meet job requirements", 
                                    "2" => "2 | Meets some of the job requirements", 
                                    "3" => "3 | Meets most of the job requirements", 
                                    "4" => "4 | Meets all the job requirements", 
                                    "5" => "5 | Meets and occasionally exceeds all the job requirements", 
                                    "6" => "6 | Meets and frequently exceeds all the job requirements", 
                                    "7" => "7 | Performs significantly and consistently above standard job requirements" );  
    
    private $kpaId;
    private $period;
    private $userRating;
    private $userRatingComment;
    private $managerRating;
    private $managerRatingComment;
    private $jointRating;
    private $jointRatingComment;

    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new KPAEvaluation();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new KPAEvaluation();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new KPAEvaluation();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setKPAId( self::initValue( $values["kpa_id"], $escape ) );
      $this->setPeriod( self::initValue( $values["period"], $escape ) );
      $this->setUserRating( self::initValue( $values["user_rating"], $escape ) );
      $this->setUserRatingComment( self::initValue( $values["user_rating_comment"], $escape ) );
      $this->setManagerRating( self::initValue( $values["manager_rating"], $escape ) );
      $this->setManagerRatingComment( self::initValue( $values["manager_rating_comment"], $escape ) );
      $this->setJointRating( self::initValue( $values["joint_rating"], $escape ) );
      $this->setJointRatingComment( self::initValue( $values["joint_rating_comment"], $escape ) );
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( kpa_id, period, user_rating, user_rating_comment, manager_rating, manager_rating_comment, joint_rating, joint_rating_comment ) " .
               "VALUES " .
               "( {$this->kpaId}, " .
               "  {$this->period}, " .               
               "  " . DBHelper::sqlNULL( $this->userRating ) . ", " .                  
               "  " . DBHelper::sqlNULL( $this->userRatingComment ) . ", " .
               "  " . DBHelper::sqlNULL( $this->managerRating ) . ", " .                  
               "  " . DBHelper::sqlNULL( $this->managerRatingComment ) . ", " .
               "  " . DBHelper::sqlNULL( $this->jointRating ) . ", " .                  
               "  " . DBHelper::sqlNULL( $this->jointRatingComment ) . " )";
               
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET kpa_id = '{$this->kpaId}', " .
               "     period = {$this->period}, " .
               "     user_rating = " . DBHelper::sqlNULL( $this->userRating ) . ", " .
               "     user_rating_comment = " . DBHelper::sqlNULL( $this->userRatingComment ) . ", " .                  
               "     manager_rating = " . DBHelper::sqlNULL( $this->managerRating ) . ", " .
               "     manager_rating_comment = " . DBHelper::sqlNULL( $this->managerRatingComment ) . ", " .                  
               "     joint_rating = " . DBHelper::sqlNULL( $this->jointRating ) . ", " .
               "     joint_rating_comment = " . DBHelper::sqlNULL( $this->jointRatingComment ) . 
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 
    
    /* --------------------------------------------------------------------------------------- */   
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */      
    public static function ratingSelectList()
    {
      $list = array();  
  
      foreach( self::$RATINGS as $rating => $desc )
      {
        $list[] = array( $rating, $desc );    
      }
      
      return $list;      
    }
    
    public static function displayRating( $rating )
    {
      return self::$RATINGS[ $rating ];
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getKpaId()
    {
      return $this->kpaId;
    }
    
    public function setKpaId($kpaId)
    {
      $this->kpaId = $kpaId;
    }
    
    public function getPeriod()
    {
      return $this->period;
    }
    
    public function setPeriod($period)
    {
      $this->period = $period;
    }

    public function getJointRating()
    {
      return $this->jointRating;
    }
    
    public function setJointRating($jointRating)
    {
      $this->jointRating = $jointRating;
    }
    
    public function getJointRatingComment()
    {
      return $this->jointRatingComment;
    }
    
    public function setJointRatingComment($jointRatingComment)
    {
      $this->jointRatingComment = $jointRatingComment;
    }
    
    public function getManagerRating()
    {
      return $this->managerRating;
    }
    
    public function setManagerRating($managerRating)
    {
      $this->managerRating = $managerRating;
    }
    
    public function getManagerRatingComment()
    {
      return $this->managerRatingComment;
    }
    
    public function setManagerRatingComment($managerRatingComment)
    {
      $this->managerRatingComment = $managerRatingComment;
    }
    
    public function getUserRating()
    {
      return $this->userRating;
    }
    
    public function setUserRating($userRating)
    {
      $this->userRating = $userRating;
    }
    
    public function getUserRatingComment()
    {
      return $this->userRatingComment;
    }
    
    public function setUserRatingComment($userRatingComment)
    {
      $this->userRatingComment = $userRatingComment;
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
}
?>
