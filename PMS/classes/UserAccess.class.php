<?php
  class UserAccess extends Base
  {
    public static $TABLE_NAME = "pms_useraccess";
    public static $DISPLAY_NAME = "User access";    
    public static $ACCESS_SECTIONS = array( "activation" => "Activation",
                                            "evaluation" => "Evaluation",
                                            "reports" => "Reporting",
                                            "setup" => "Setup" );
                                            
    public static $AVAILABLE_ACCESS_SECTIONS = array( "reports" => "Reporting",
                                                      "setup" => "Setup" );                                            
    
    private $userId;
    private $sections;
    
    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function loadWhere( $condition, $orderBy = "id" )
    {
      $obj = new UserAccess();
      
      $records = DBHelper::getListRecords( "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}" );
      
      $userId = null;
      $sections = array();
      
      foreach( $records as $record )
      {
        $userId = $record["user_id"];
        $sections[] = $record["section"];
      }
      
      $obj->setUserId( $userId );
      $obj->setSections( $sections );      

      return $obj;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setUserId( self::initValue( $values["user_id"], $escape ) );
      $this->setSections( $values["sections"] );               
    }
    
    public function save()
    {
      $this->delete( "user_id = '{$this->userId}'" );        

      foreach( $this->sections as $section )
      {
        $sql = "( user_id, section ) " .
               "VALUES " .
               "( '{$this->userId}', '{$section}' )";
        
        $this->insert( $sql );       
      }  
    } 
    
    public static function load( $id )
    {
      throw Exception( "Illegal Access to method not implemented" );
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      throw Exception( "Illegal Access to method not implemented" );
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */      
    public static function selectList()
    {
      $list = array();  
  
      foreach( self::$AVAILABLE_ACCESS_SECTIONS as $section => $desc )
      {
        $list[] = array( $section, $desc );    
      }
      
      return $list;      
    } 
    
    public static function display( $section )
    {
      return self::$ACCESS_SECTIONS[ $section ];
    }
        
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function setUserId($userId)
    {
      $this->userId = $userId;
    }
    
    public function getUserId()
    {
      return $this->userId;
    }
    
    public function setSections($sections)
    {
      $this->sections = $sections;
    }
    
    public function getSections()
    {
      return $this->sections;
    }
  }
?>
