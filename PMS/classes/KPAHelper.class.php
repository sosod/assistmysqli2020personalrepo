<?php
  class KPAHelper
  {
    public static function calculateAverageRating( $user, $period, $kpas )
    {
      $useWeights = $user->getCategory()->getWeights();
      
      $totalScore = 0;
      $avgScore = 0;
      $totalKPAs = count( $kpas );

      foreach( $kpas as $kpa )
      {
        $kpaEvaluation = $kpa->getEvaluation( $period ); 
        
        if( $useWeights )
        {
          $avgScore += ( $kpaEvaluation->getJointRating() * ( $kpa->getWeight() / 100 ) );
        }
        else
        {
          $totalScore += $kpaEvaluation->getJointRating();
        }
      }
      
      if( $useWeights == false )
      {
        $avgScore = $totalScore / $totalKPAs;
      }
      
      return round( $avgScore, 0 );
    }
  }
?>