<?php
  class Qualification extends Base
  {
    public static $TABLE_NAME = "pms_qualification";
    public static $DISPLAY_NAME = "Qualification";  
    
    public static $TYPES = array( "formal" => "Formal",
                                  "informal" => "Informal" );                             
                                  
    private $userId;
    private $status;
    private $type;
    private $name;
    private $institution;
    private $accredited;   
    private $comments;        
    private $createdById;
    private $createdOn;

    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */ 
    private $user;
    private $evalYear;
    private $createdBy;
                
    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new Qualification();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new Qualification();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new Qualification();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setUserId( self::initValue( $values["user_id"], $escape ) );
      $this->setStatus( self::initValue( $values["status"], $escape ) );
      $this->setType( self::initValue( $values["type"], $escape ) );
      $this->setName( self::initValue( $values["name"], $escape ) );      
      $this->setInstitution( self::initValue( $values["institution"], $escape ) );      
      $this->setAccredited( self::initValue( $values["accredited"], $escape ) );      
      $this->setComments( self::initValue( $values["comments"], $escape ) );            
      $this->setCreatedById( self::initValue( $values["created_by_id"], $escape ) );
      $this->setCreatedOn( self::initValue( $values["created_on"], $escape ) );
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( user_id, status, type, name, institution, accredited, comments, created_by_id, created_on ) " .
               "VALUES " .
               "( '{$this->userId}', '{$this->status}', '{$this->type}', '{$this->name}', '{$this->institution}', {$this->accredited}, '{$this->comments}', '{$this->createdById}', CURRENT_TIMESTAMP )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET user_id = '{$this->userId}', " .
               "     status = '{$this->status}', " .
               "     type = '{$this->type}', " .
               "     name = '{$this->name}', " .
               "     institution = '{$this->institution}', " .
               "     accredited = {$this->accredited}, " .
               "     comments = '{$this->comments}' " .               
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 
    
    /* --------------------------------------------------------------------------------------- */   
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */      
    public static function typeSelectList()
    {
      $list = array();  
  
      foreach( self::$TYPES as $type => $desc )
      {
        $list[] = array( $type, $desc );    
      }
      
      return $list;      
    }
    
    public static function displayType( $type )
    {
      return self::$TYPES[ $type ];
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function setUserId($userId)
    {
      $this->userId = $userId;
    }
    
    public function getUserId()
    {
      return $this->userId;
    }

    public function getStatus()
    {
      return $this->status;
    }
    
    public function setStatus($status)
    {
      $this->status = $status;
    }

    public function getCreatedById()
    {
      return $this->createdById;
    }
    
    public function setCreatedById($createdById)
    {
      $this->createdById = $createdById;
    }
    
    public function getCreatedOn()
    {
      return $this->createdOn;
    }
    
    public function setCreatedOn($createdOn)
    {
      $this->createdOn = $createdOn;
    }

    public function getName()
    {
      return $this->name;
    }
    
    public function setName($name)
    {
      $this->name = $name;
    }

    public function getAccredited()
    {
      return $this->accredited;
    }

    public function setAccredited($accredited)
    {
      $this->accredited = $accredited;
    }

    public function getComments()
    {
      return $this->comments;
    }

    public function setComments($comments)
    {
      $this->comments = $comments;
    }

    public function getInstitution()
    {
      return $this->institution;
    }

    public function setInstitution($institution)
    {
      $this->institution = $institution;
    }

    public function getType()
    {
      return $this->type;
    }

    public function setType($type)
    {
      $this->type = $type;
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getUser() 
    {
      if( isset( $this->user ) == false )
      {
        $this->user = User::load( $this->userId );
      }
      
      return $this->user; 
    }
    
    public function getEvalYear() 
    {
      if( isset( $this->evalYear ) == false )
      {
        $this->evalYear = EvalYear::load( $this->evalYearId );
      }
      
      return $this->evalYear; 
    }
    
    public function getCreatedBy() 
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = User::load( $this->createdById );
      }
      
      return $this->createdBy; 
    }
  }
?>
