<?php
class ReportHelper
{
  public static function getFishbowlOrderBySelectList()
  {
    $list = array();  
    
    $list[] = array( "tkname", "User" );
    $list[] = array( "job_level", "Job level" );  
    $list[] = array( "department", "Department" );    
    $list[] = array( "section", "Section" );
    $list[] = array( "tkmanager", "Manager" );        
    
    return $list;
  }
  
  public static function getLearningOrderBySelectList()
  {
    $list = array();  
    
    $list[] = array( "tkname", "User" );
    $list[] = array( "job_level", "Job level" );  
    $list[] = array( "department", "Department" );    
    $list[] = array( "section", "Section" );
    $list[] = array( "tkmanager", "Manager" );    
    $list[] = array( "priority", "Priority" );              
    $list[] = array( "status", "Status" );        
    
    return $list;
  }
  
  public static function getOrderByDirectionSelectList()
  {
    $list = array();  
    
    $list[] = array( "ASC", "Ascending" );
    $list[] = array( "DESC", "Descending" );  
    
    return $list;
  }  
  
  public static function getSectionSelectList()
  {
    $list = array();  
    
    foreach( DBHelper::getListRecords( "SELECT section FROM assist_" . getCompanyCode() . "_pms_usersetup GROUP BY section" ) as $record )
    {
      $list[] = array( $record["section"], $record["section"] );    
    }
    
    return $list;
  }
  
  public static function getActivitySelectList( $category )
  {
    $list = array();  
    
    foreach( DBHelper::getListRecords( "SELECT l.name FROM assist_" . getCompanyCode() . "_pms_learningactivity l, assist_".getCompanyCode()."_pms_usersetup us WHERE us.user_id = l.user_id AND us.category_id = " . $category->getId() . " GROUP BY name" ) as $record )
    {
      $list[] = array( $record["name"], $record["name"] );    
    }
    
    return $list;
  }
  
  public static function getDepartmentList( $departmentId )
  {
    return DBHelper::getListRecords( "SELECT * FROM assist_" . getCompanyCode() . "_list_dept WHERE yn = 'Y' " . ( $departmentId ? " AND id = " . $departmentId : "" ) ); 
  }
  
  public static function getDepartmentSelectList()
  {
    $list = array();  
    
    foreach( DBHelper::getListRecords( "SELECT * FROM assist_" . getCompanyCode() . "_list_dept WHERE yn = 'Y'" ) as $record )
    {
      $list[] = array( $record["id"], $record["value"] );    
    }
    
    return $list;
  }
  
  public static function getUserSelectList( $category ) 
  {
    $list = array();  
    
    $sql = "SELECT u.tkid, u.tkname, u.tksurname FROM assist_".getCompanyCode()."_timekeep u, assist_".getCompanyCode()."_pms_usersetup us " .
           "WHERE u.tkid = us.user_id AND us.category_id = " . $category->getId() .
           " ORDER BY u.tkname";
    
	  $rs = getResultSet( $sql );
	  
	  while( $record = getRecord( $rs ) )
	  {
	    $list[] = array( $record["tkid"], $record["tkname"] . " " . $record["tksurname"] );    
	  }
    
    return $list; 
  }
}

?>