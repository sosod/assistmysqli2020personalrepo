<?php
  class JobFunction extends Base 
  {
    public static $TABLE_NAME = "pms_jobfunction";
    public static $DISPLAY_NAME = "Job function";  
    
    private $userId;
    private $status;
    private $name;
    private $description;    
    private $createdById;
    private $createdOn;

    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */ 
    private $user;
    private $evalYear;
    private $createdBy;
                
    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new JobFunction();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new JobFunction();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new JobFunction();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setUserId( self::initValue( $values["user_id"], $escape ) );
      $this->setStatus( self::initValue( $values["status"], $escape ) );
      $this->setName( self::initValue( $values["name"], $escape ) );
      $this->setDescription( self::initValue( $values["description"], $escape ) );      
      $this->setCreatedById( self::initValue( $values["created_by_id"], $escape ) );
      $this->setCreatedOn( self::initValue( $values["created_on"], $escape ) );
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( user_id, status, name, description, created_by_id, created_on ) " .
               "VALUES " .
               "( '{$this->userId}', '{$this->status}', '{$this->name}', '{$this->description}', '{$this->createdById}', CURRENT_TIMESTAMP )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET user_id = '{$this->userId}', " .
               "     status = '{$this->status}', " .
               "     name = '{$this->name}', " .
               "     description = '{$this->description}' " .               
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 
    
    /* --------------------------------------------------------------------------------------- */   
    /* -------------------------------- VIEW HELPER FUNCTIONS -------------------------------- */
    /* --------------------------------------------------------------------------------------- */      
    public static function selectList( $condition = false )
    {
      $list = array();  
  
      if( $condition )
      {
        $records = self::records( $condition );  
      }
      else
      {
        $records = self::records();  
      }
  
      foreach( $records as $record )
      {
        $list[] = array( $record->getId(), $record->getName() );    
      }
      
      return $list;      
    }
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function setUserId($userId)
    {
      $this->userId = $userId;
    }
    
    public function getUserId()
    {
      return $this->userId;
    }

    public function getStatus()
    {
      return $this->status;
    }
    
    public function setStatus($status)
    {
      $this->status = $status;
    }

    public function getCreatedById()
    {
      return $this->createdById;
    }
    
    public function setCreatedById($createdById)
    {
      $this->createdById = $createdById;
    }
    
    public function getCreatedOn()
    {
      return $this->createdOn;
    }
    
    public function setCreatedOn($createdOn)
    {
      $this->createdOn = $createdOn;
    }

    public function getDescription()
    {
      return $this->description;
    }
    
    public function setDescription($description)
    {
      $this->description = $description;
    }
    
    public function getName()
    {
      return $this->name;
    }
    
    public function setName($name)
    {
      $this->name = $name;
    }

    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getUser() 
    {
      if( isset( $this->user ) == false )
      {
        $this->user = User::load( $this->userId );
      }
      
      return $this->user; 
    }
    
    public function getEvalYear() 
    {
      if( isset( $this->evalYear ) == false )
      {
        $this->evalYear = EvalYear::load( $this->evalYearId );
      }
      
      return $this->evalYear; 
    }
    
    public function getCreatedBy() 
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = User::load( $this->createdById );
      }
      
      return $this->createdBy; 
    }
  }
?>
