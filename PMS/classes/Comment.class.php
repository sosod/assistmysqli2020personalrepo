<?php
  class Comment extends Base 
  {
    public static $TABLE_NAME = "pms_comment";
    public static $DISPLAY_NAME = "Comment";  
    
    private $userId;
    private $evalYearId;
    private $period;    
    private $message;
    private $createdById;
    private $createdOn;

    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */ 
    private $user;
    private $evalYear;
    private $createdBy;
                
    /* --------------------------------------------------------------------------------------- */   
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */             
    function __construct() 
    {
      parent::__construct( self::$TABLE_NAME, self::$DISPLAY_NAME );
    }            
                
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public static function load( $id )
    {
      $obj = new Comment();
      $obj->init( $obj->getObjectData( $id ) );

      return $obj;
    }
    
    public static function loadWhere( $condition )
    {
      $obj = new Comment();
      $obj->init( $obj->getObjectDataWhere( $condition ) );

      return $obj;
    }
    
    public static function records( $condition = false, $orderBy = "id" )
    {
      $sql = "SELECT * FROM assist_" . getCompanyCode() . "_" . self::$TABLE_NAME . " " . ( $condition ? "WHERE " . $condition : "" ) . " ORDER BY {$orderBy}";  
      
		  $rs = getResultSet( $sql );
		  
		  $records = array();
		  while( $record = getRecord( $rs ) )
		  {
		    $obj = new Comment();
        $obj->init( $record );
        
		    $records[] = $obj;
		  }
		  
		  return $records;
    }
    
    public function init( $values, $escape = false )
    {
      $this->setId( self::initValue( $values["id"], $escape ) );
      $this->setUserId( self::initValue( $values["user_id"], $escape ) );
      $this->setEvalYearId( self::initValue( $values["eval_year_id"], $escape ) );
      $this->setPeriod( self::initValue( $values["period"], $escape ) );      
      $this->setMessage( self::initValue( $values["message"], $escape ) );
      $this->setCreatedById( self::initValue( $values["created_by_id"], $escape ) );
      $this->setCreatedOn( self::initValue( $values["created_on"], $escape ) );
    }
    
    public function save()
    {
      if( $this->isTransient() )
      {
        $sql = "( user_id, eval_year_id, period, message, created_by_id, created_on ) " .
               "VALUES " .
               "( '{$this->userId}', {$this->evalYearId}, " . DBHelper::sqlNULL( $this->period ) . ", '{$this->message}', '{$this->createdById}', CURRENT_TIMESTAMP )";
        
        $this->insert( $sql );       
      }
      else
      {
        $sql = " SET user_id = '{$this->userId}', " .
               "     eval_year_id = {$this->evalYearId}, " .
               "     period = " . DBHelper::sqlNULL( $this->period ) . ", " .               
               "     message = '{$this->message}' " .
               " WHERE id = " . $this->getId();
               
        $this->update( $sql );   
      }
    } 
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function setUserId($userId)
    {
      $this->userId = $userId;
    }
    
    public function getUserId()
    {
      return $this->userId;
    }

    public function getEvalYearId()
    {
      return $this->evalYearId;
    }
    
    public function setEvalYearId($evalYearId)
    {
      $this->evalYearId = $evalYearId;
    }
    
    public function setPeriod($period)
    {
      $this->period = $period;
    }
    
    public function getPeriod()
    {
      return $this->period;
    }

    public function getCreatedById()
    {
      return $this->createdById;
    }
    
    public function setCreatedById($createdById)
    {
      $this->createdById = $createdById;
    }
    
    public function getCreatedOn()
    {
      return $this->createdOn;
    }
    
    public function setCreatedOn($createdOn)
    {
      $this->createdOn = $createdOn;
    }
    
    public function getMessage()
    {
      return $this->message;
    }
    
    public function setMessage($message)
    {
      $this->message = $message;
    }
    
    /* --------------------------------------------------------------------------------------- */   
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */   
    public function getUser() 
    {
      if( isset( $this->user ) == false )
      {
        $this->user = User::load( $this->userId );
      }
      
      return $this->user; 
    }
    
    public function getEvalYear() 
    {
      if( isset( $this->evalYear ) == false )
      {
        $this->evalYear = EvalYear::load( $this->evalYearId );
      }
      
      return $this->evalYear; 
    }
    
    public function getCreatedBy() 
    {
      if( isset( $this->createdBy ) == false )
      {
        $this->createdBy = User::load( $this->createdById );
      }
      
      return $this->createdBy; 
    }
  }
?>
