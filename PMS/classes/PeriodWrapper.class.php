<?php

class PeriodWrapper
{
  private $period;
  private $start;
  private $end;
  
  public function isTransient()
  {
    return isset( $this->period ) == false;
  }
  
  public function getPeriod()
  {
    return $this->period;
  }

  public function setPeriod($period)
  {
    $this->period = $period;
  }

  public function getEnd()
  {
    return $this->end;
  }

  public function setEnd($end)
  {
    $this->end = $end;
  }

  public function getStart()
  {
    return $this->start;
  }

  public function setStart($start)
  {
    $this->start = $start;
  }
  
  public function getDisplayName()
  {
    return $this->start->format3( "j M y" ) . " to " . $this->end->format3( "j M y" );
  }

}

?>