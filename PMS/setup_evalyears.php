<?
include("inc_ignite.php");
include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Eval years" => null ) ); ?>

<table class="list" cellspacing="0">
  <tr>
    <th style="width:70%;">Evaluation year</th>
    <th style="width:30%;">Actions</th>        
  </tr>
  <?
  
  $records = EvalYear::records();
  
  if( count( $records ) )
  {
    foreach( EvalYear::records() as $obj )
    {
  ?>
  <tr>
    <td><?= $obj->getDisplayName() ?></td>
    <td>
      <? ViewHelper::button( "setup_evalyear.php?id={$obj->getId()}", "Configure" ); ?>
      <? ViewHelper::button( "setup_orggoals.php?eval_year_id={$obj->getId()}", "Goals" ); ?>
      <? ViewHelper::button( "setup_evalyearinfo.php?id={$obj->getId()}", "View" ); ?>
    </td>		
  </tr>
  <?
    }
  }
  else
  {
  ?>
  <tr><td colspan="2">No evaluation years defined</td></tr>
  <?  
  }
  ?>
</table>

<? ViewHelper::actions( array( "Back"=>"setup.php", "New"=>"setup_evalyear.php" ) ); ?>

<?php
include("inc_footer.php");
?>