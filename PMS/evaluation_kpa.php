<?
include("inc_ignite.php");
include("evaluation_logic.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $kpaEvaluation = new KPAEvaluation();
  $kpaEvaluation->init( $_REQUEST , true );
  $kpaEvaluation->save();
  
  ViewHelper::redirect( "evaluation.php" );
}

$kpa = KPA::load( $_REQUEST["kpa_id"] );
$kpaEvaluation = $kpa->getEvaluation( $selectedPeriod->getPeriod() );

if( $selectedEvaluationStatus->getUserCompleted() == null )
{
  $rating = $kpaEvaluation->getUserRating();
  $rating_comment = $kpaEvaluation->getUserRatingComment();  
  $rating_name = "user_rating";  
  $rating_display_name = "Employee Rating";
}
else if( $selectedEvaluationStatus->getManagerCompleted() == null )
{
  $rating = $kpaEvaluation->getManagerRating();
  $rating_comment = $kpaEvaluation->getManagerRatingComment();  
  $rating_name = "manager_rating";  
  $rating_display_name = "Manager Rating";
}
else if( $selectedEvaluationStatus->getJointCompleted() == null )
{
  $rating = $kpaEvaluation->getJointRating();
  $rating_comment = $kpaEvaluation->getJointRatingComment();    
  $rating_name = "joint_rating";  
  $rating_display_name = "Joint Rating";
}

?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Evaluation" => "evaluation.php", $selectedUser->getFullName() => null, "KPA Evaluation" => null ) ); ?>

<? include("evaluation_inc_summary.php"); ?>

<? ViewHelper::validation( "frmKPA" ); ?>

<h2>KPA Evaluation</h2>

<form id="frmKPA" method="post" >  
  <table class="form" cellspacing="0">
    
    <? if( $selectedEvaluationStatus->getUserCompleted() == null ){ ?>
    
    <tr>
      <th>Employee Rating</th>
      <td><?= ViewHelper::select( "user_rating", KPAEvaluation::ratingSelectList(), $kpaEvaluation->getUserRating(), true ) ?></td>
    </tr>
    <tr>
      <th>Employee Rating Comment</th>
      <td><input type="text" name="user_rating_comment" maxlength="255" size="60" value="<?= $kpaEvaluation->getUserRatingComment() ?>" /></td>
    </tr>
    
    <? }else if( $selectedEvaluationStatus->getManagerCompleted() == null ){ ?>
    
    <tr>
      <th>Manager Rating</th>
      <td><?= ViewHelper::select( "manager_rating", KPAEvaluation::ratingSelectList(), $kpaEvaluation->getManagerRating(), true ) ?></td>
    </tr>
    <tr>
      <th>Manager Rating Comment</th>
      <td><input type="text" name="manager_rating_comment" maxlength="255" size="60" value="<?= $kpaEvaluation->getManagerRatingComment() ?>" /></td>
    </tr>
    <input type="hidden" name="user_rating" value="<?= $kpaEvaluation->getUserRating() ?>" />
    <textarea name="user_rating_comment" style="display:none;"><?= $kpaEvaluation->getUserRatingComment() ?></textarea>
      
    <? }else if( $selectedEvaluationStatus->getJointCompleted() == null ){ ?>
    
    <tr>
      <th>Joint Rating</th>
      <td><?= ViewHelper::select( "joint_rating", KPAEvaluation::ratingSelectList(), $kpaEvaluation->getJointRating(), true ) ?></td>
    </tr>
    <tr>
      <th>Joint Rating Comment</th>
      <td><input type="text" name="joint_rating_comment" maxlength="255" size="60" value="<?= $kpaEvaluation->getJointRatingComment() ?>" /></td>
    </tr>
    <input type="hidden" name="user_rating" value="<?= $kpaEvaluation->getUserRating() ?>" />
    <textarea name="user_rating_comment" style="display:none;"><?= $kpaEvaluation->getUserRatingComment() ?></textarea>
    <input type="hidden" name="manager_rating" value="<?= $kpaEvaluation->getManagerRating() ?>" />
    <textarea name="manager_rating_comment" style="display:none;"><?= $kpaEvaluation->getManagerRatingComment() ?></textarea>
    
    <? } ?>
    
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $kpaEvaluation->getId() ?>" />
  <input type="hidden" name="kpa_id" value="<?= $kpa->getId() ?>" />
  <input type="hidden" name="user_id" value="<?= $selectedUser->getId() ?>" />
  <input type="hidden" name="eval_year_id" value="<?= $selectedYear->getId() ?>" />
  <input type="hidden" name="period" value="<?= $selectedPeriod->getPeriod() ?>" />  
  <input type="hidden" name="created_by_id" value="<?= getLoggedInUserId() ?>" />
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>


<h2>KPA Information</h2>
<table class="info" cellspacing="0">
  <tr>
    <th>Type</th>
    <td><?= KPA::displayType( $kpa->getType() ) ?></td>
  </tr>
  <? if( $kpa->getType() == "org" ){ ?>
  <tr>
    <th>Organisational Goal:</th>
    <td><?= $kpa->getOrgGoal()->getName() ?></td>
  </tr>
  <? }else if( $kpa->getType() == "job" ){ ?>
  <tr>
    <th>Job function:</th>
    <td><?= $kpa->getJobFunction()->getName() ?></td>
  </tr>
  <? } ?>
  <tr>
    <th>Name</th>
    <td><?= $kpa->getName() ?></td>
  </tr>
  <tr>
    <th>Objective</th>
    <td><?= $kpa->getObjective() ?></td>
  </tr>
  <tr>
    <th>Measurement</th>
    <td><?= $kpa->getMeasurement() ?></td>
  </tr>
  <tr>
    <th>Baseline</th>
    <td><?= $kpa->getBaseline() ?></td>
  </tr>
  <tr>
    <th>Target unit</th>
    <td><?= $kpa->getTargetUnit() ?></td>
  </tr>
  
  <? 
    $periods = $selectedUser->getCategory()->getPeriodFrequency();
  
    for( $i = 1; $i <= $periods; $i++ ) 
    {
      $kpaTarget = $kpa->getTarget( $i );  
  ?>
  <tr>
    <th>Target for period <?= $i ?></th>
    <td><?= $kpaTarget->getTarget() ?></td>
  </tr>
  <? } ?>
  <? if( $selectedUser->getCategory()->getWeights() ){ ?>
  <tr>
    <th>Weight</th>
    <td><?= $kpa->getWeight() ?>%</td>
  </tr>
  <? } ?>
  <tr>
    <th>Competency</th>
    <td><?= $kpa->getCompetency()->getName() ?></td>
  </tr>
  <tr>
    <th>Proficiency Level</th>
    <td><?= ViewHelper::displayProficiencyLevel( $kpa->getProficiencyLevel() ) ?></td>
  </tr>
  <tr>
    <th>Deadline</th>
    <td><?= $kpa->getDeadline() ?></td>
  </tr>
  <tr>
    <th>Comments</th>
    <td><?= $kpa->getComments() ?></td>
  </tr>
  <tr>
    <th>Created by:</th>
    <td><?= $kpa->getCreatedBy()->getFullName() ?> on <?= $kpa->getCreatedOn() ?></td>
  </tr>
</table>

<h2>Evaluation Ratings</h2>
<table class="info" cellspacing="0">
  <tr>
    <th>Employee rating</th>
    <td><?= ViewHelper::ifNull( $kpaEvaluation->getUserRating(), "No rating" ) ?> | <?= ViewHelper::ifNull( $kpaEvaluation->getUserRatingComment(), "No comment" ) ?></td>
  </tr>
  <tr>
    <th>Manager rating</th>
    <td><?= ViewHelper::ifNull( $kpaEvaluation->getManagerRating(), "No rating" ) ?> | <?= ViewHelper::ifNull( $kpaEvaluation->getManagerRatingComment(), "No comment" ) ?></td>
  </tr>
  <tr>
    <th>Joint rating</th>
    <td><?= ViewHelper::ifNull( $kpaEvaluation->getJointRating(), "No rating" ) ?> | <?= ViewHelper::ifNull( $kpaEvaluation->getJointRatingComment(), "No comment" ) ?></td>
  </tr>
</table>

<? ViewHelper::actions( array( "Back"=>"evaluation.php#{$kpa->getType()}kpas" ) ); ?>

<? include("inc_footer.php"); ?>