<?
include("inc_ignite.php");
include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Core Competencies" => null ) ); ?>

<table class="list" cellspacing="0">
  <tr>
    <th style="width:30%;">Name</th>
    <th style="width:50%;">Definition</th>
    <th style="width:10%;">Status</th>
    <th style="width:10%;">Actions</th>        
  </tr>
  <?
  foreach( Competency::records() as $obj )
  {
  ?>
  <tr>
    <td><?= $obj->getName() ?></td>
    <td><?= $obj->getDefinition() ?></td>    
    <td><?= ViewHelper::displayStatus( $obj->getStatus() ) ?></td>        
    <td>
      <? ViewHelper::button( "setup_competency.php?id={$obj->getId()}", "Configure" ); ?>
    </td>		
  </tr>
  <?
  }
  ?>
</table>

<? ViewHelper::actions( array( "Back"=>"setup.php" ) ); ?>

<?php
include("inc_footer.php");
?>