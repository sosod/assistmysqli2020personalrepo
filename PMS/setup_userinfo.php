<?
include("inc_ignite.php");
include("inc_header.php");

$userSetup = UserSetup::loadWhere( "user_id = '" . $_REQUEST["user_id"] . "'" );
$userAccess = UserAccess::loadWhere( "user_id = '" . $_REQUEST["user_id"] . "'" );

if( $userSetup->isTransient() )
{
  $userSetup->setUserId( $_REQUEST["user_id"] );
}
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Employees" => "setup_users.php", $userSetup->getUser()->getFullName() => null ) ); ?>

<h2>Employee setup</h2>
<table class="info" cellspacing="0">
  <tr>
    <th>Name</th>
    <td><?= $userSetup->getUser()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Category</th>
    <td><?= $userSetup->getCategory()->getName() ?></td>
  </tr>
  <tr>
    <th>Is HR manager?</th>
    <td><?= ViewHelper::boolYesNo( $userSetup->getHRManager() ) ?></td>
  </tr>
  <tr>
    <th>Section</th>
    <td><?= $userSetup->getSection() ?></td>
  </tr>
  <tr>
    <th>Job level</th>
    <td><?= $userSetup->getJobLevel() ?></td>
  </tr>
  <tr>
    <th>Job number</th>
    <td><?= $userSetup->getJobNumber() ?></td>
  </tr>
  <tr>
    <th>Access sections</th>
    <td>
      <? foreach( $userAccess->getSections() as $section ) { ?>
      <?= UserAccess::display( $section ) ?><br/>
      <? } ?>
    </td>
  </tr>
</table>

<? ViewHelper::actions( array( "Back"=>"setup_users.php", "Configure"=>"setup_user.php?user_id={$userSetup->getUserId()}" ) ); ?>

<?php
include("inc_footer.php");
?>