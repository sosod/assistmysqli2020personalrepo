<?
include("inc_ignite.php");
include("inc_header.php");
?>
    
<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Categories" => null ) ); ?>

<table class="list" cellspacing="0">
  <tr>
    <th style="width:70%;">Category</th>
    <th style="width:30%;">Actions</th>        
  </tr>
  <?
  foreach( Category::records() as $obj )
  {
  ?>
  <tr>
    <td><?= $obj->getName() ?></td>
    <td>
      <? ViewHelper::button( "setup_category.php?id={$obj->getId()}", "Configure" ); ?>
      <? ViewHelper::button( "setup_categoryinfo.php?id={$obj->getId()}", "View" ); ?>
    </td>		
  </tr>
  <?
  }
  ?>
</table>

<? ViewHelper::actions( array( "Back"=>"setup.php" ) ); ?>

<?php
include("inc_footer.php");
?>