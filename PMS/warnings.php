<?php
include("inc_ignite.php");

$CONST_WARNING_TYPES = array( 1 => "Your user setup has not been configured yet. Please contact your supervisor.",
                              2 => "The selected user's setup has not been configured yet." );

$type = $_REQUEST["type"];

/* Clear stored session objects */
$_SESSION["sess_{$tkid}_eval_selected_period"] = null;
$_SESSION["sess_{$tkid}_eval_selected_year"] = null;
$_SESSION["sess_{$tkid}_eval_selected_user"] = null;
$_SESSION["sess_{$tkid}_act_selected_year"] = null;
$_SESSION["sess_{$tkid}_act_selected_user"] = null;

include("inc_header.php");
?>

<? ViewHelper::breadcrumbs( array( "System Warning" => null ) ); ?>

<h2>Warning</h2>
<p><?= $CONST_WARNING_TYPES[$type] ?></p>

<?php
include("inc_footer.php");
?>