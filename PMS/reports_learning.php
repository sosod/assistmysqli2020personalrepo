<?php
include("inc_ignite.php");

$showReport = false;

$selectedCategory = new Category();

if( $_REQUEST["action"] == ViewHelper::$ACTION_SELECT_YEAR )
{
  $selectedCategory = Category::load( $_REQUEST["category_id"] );

  if( $selectedCategory->isTransient() == false )
  {
    $showReport = true;

    $orderBy = $_REQUEST["orderBy"];
    if( exists( $orderBy ) == false )
    {
      $orderBy = "tkname";
    }
    $orderByDir = $_REQUEST["orderByDir"];
    if( exists( $orderByDir ) == false )
    {
      $orderByDir = "ASC";
    }

    $userId = $_REQUEST["userId"];
    $activity = $_REQUEST["activity"];
    $departmentId = $_REQUEST["departmentId"];
    $section = $_REQUEST["section"];
    $supportPersonId = $_REQUEST["supportPersonId"];
    $jobLevel = $_REQUEST["jobLevel"];
    $status = $_REQUEST["status"];


    /* BUILD UP THE SQL QUERY */
    $sql = " SELECT l.*, " .
           "        u.*, " .
           "        us.section, " .
           "        us.job_level, " .
           "        d.value AS department, " .
           "        j.value AS job_title, " .
           "        (SELECT CONCAT( um.tkname, ' ', um.tksurname ) FROM assist_".getCompanyCode()."_timekeep um WHERE um.tkid = u.tkmanager ) AS manager, " .
           "        (SELECT CONCAT( sp.tkname, ' ', sp.tksurname ) FROM assist_".getCompanyCode()."_timekeep sp WHERE sp.tkid = l.support_person_id ) AS support_person " .
           " FROM assist_".getCompanyCode()."_pms_learningactivity l, " .
           "      assist_".getCompanyCode()."_timekeep u, " .
           "      assist_".getCompanyCode()."_pms_usersetup us, " .
           "      assist_".getCompanyCode()."_list_dept d, " .
           "      assist_".getCompanyCode()."_list_jobtitle j " .
           " WHERE l.user_id = u.tkid " .
           " AND us.user_id = l.user_id " .
           " AND d.id = u.tkdept " .
           " AND j.id = u.tkdesig " .
           " AND us.category_id = " . $selectedCategory->getId();

    if( exists( $userId ) )
    {
      $sql .= " AND l.user_id = '" . $userId . "'";
    }

    if( exists( $activity ) )
    {
      $sql .= " AND l.name = '" . $activity . "'";
    }

    if( exists( $departmentId ) )
    {
      $sql .= " AND u.tkdept = " . $departmentId;
    }

    if( exists( $section ) )
    {
      $sql .= " AND us.section = '" . $section . "' ";
    }

    if( exists( $supportPersonId ) )
    {
      $sql .= " AND l.support_person_id = '" . $supportPersonId . "'";
    }

    if( exists( $jobLevel ) )
    {
      $sql .= " AND us.job_level = " . $jobLevel;
    }

    if( exists( $status ) )
    {
      $sql .= " AND l.status = '" . $status . "'";
    }

    $sql .= " ORDER BY " . $orderBy . " " . $orderByDir;

    debug( $sql );
  }
}

?>
<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Reports" => "reports.php", "Learning Activities" => null ) ); ?>

<form action="reports_learning.php" id="frmUserSelect" method="get" >
  <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
    <tr>
      <td valign="middle">
        <strong>Category:</strong>
        <?= ViewHelper::select( "category_id", Category::selectList(), $selectedCategory->getId(), true ) ?>
      </td>
      <td valign="middle"><input type="submit" value="Select" /></td>
    </tr>
  </table>
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SELECT_YEAR ?>" />
</form>
<hr/>
<?
  if( $showReport )
  {
?>

<h5>Report Filter:</h5>

<form action="reports_learning.php" id="frmFilter" method="get" >
  <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
    <tr>
      <td valign="middle">
        <label>User</label>
        <?= ViewHelper::select( "userId", ReportHelper::getUserSelectList( $selectedCategory ), $userId, false, "All" ) ?>
        <label>Activity</label>
        <?= ViewHelper::select( "activity", ReportHelper::getActivitySelectList( $selectedCategory ), $activity, false, "All" ) ?>
      </td>
    </tr>
    <tr>
      <td valign="middle">
        <label>Department</label>
        <?= ViewHelper::select( "departmentId", ReportHelper::getDepartmentSelectList(), $departmentId, false, "All" ) ?>
        <label>Section</label>
        <?= ViewHelper::select( "section",  ReportHelper::getSectionSelectList(), $section, false, "All" ) ?>
        <label>Job level</label>
        <?= ViewHelper::select( "jobLevel", ViewHelper::getJobLevelSelectList(), $jobLevel, false, "All" ) ?>
      </td>
    </tr>
    <tr>
      <td valign="middle">
        <label>Support Person</label>
        <?= ViewHelper::select( "supportPersonId", User::selectList(), $supportPersonId, false, "All" ) ?>

        <label>Status</label>
        <?= ViewHelper::select( "status", LearningActivity::statusSelectList(), $status, false, "All" ) ?>
      </td>
    </tr>
    <tr>
      <td valign="middle">
        <label>Order by</label>
        <?= ViewHelper::select( "orderBy", ReportHelper::getLearningOrderBySelectList(), $orderBy, false, false  ) ?>
        <?= ViewHelper::select( "orderByDir", ReportHelper::getOrderByDirectionSelectList(), $orderByDir, false, false ) ?>
        <input type="submit" value="Select" />
      </td>
    </tr>
  </table>
  <input type="hidden" name="category_id" value="<?= $selectedCategory->getId() ?>" />
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SELECT_YEAR ?>" />
</form>

<table class="list full" cellspacing="0" style="margin-top:15px;">
  <tr>
    <th width="10%">User</th>
    <th width="5%">Job level</th>
    <th width="10%">Department</th>
    <th width="10%">Section</th>
    <th width="5%">Priority</th>
    <th width="20%">Activity</th>
    <th width="20%">Training</th>
    <th width="10%">Support Person</th>
    <th width="5%">Status</th>
    <th width="5%">Actions</th>
  </tr>
  <?
    $data = DBHelper::getListRecords( $sql );

    if( count($data) > 0 )
    {
      foreach( $data as $record )
      {
  ?>
  <tr>
    <td><?= $record["tkname"] ?> <?= $record["tksurname"] ?></td>
    <td align="right"><?= $record["job_level"] ?></td>
    <td><?= $record["department"] ?></td>
    <td><?= $record["section"] ?></td>
    <td align="right"><?= $record["priority"] ?></td>
    <td><?= $record["name"] ?></td>
    <td><?= $record["intervention"] ?></td>
    <td><?= $record["support_person"] ?></td>
    <td><?= LearningActivity::displayStatus( $record["status"] ) ?></td>
    <td><input type="button" value="View" onclick="redirect('reports_learning_info.php?id=<?= $record['id'] ?>');"/></td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="10">No record match the selected filter options.</td></tr>
  <?
    }
  ?>
</table>
<?
  }
  else
  {
?>
<p class="instruction">Please select a category and evaluation year above to continue.</p>
<?
  }
?>
<?php
include("inc_footer.php");
?>