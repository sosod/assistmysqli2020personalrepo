<?
function debug($msg)
{
  /*$fd = fopen("C:\www\hosts\assist.ignite4u.co.za\logs\PMS.log", "a");
  $str = "[" . date("Y/m/d h:i:s", mktime()) . "] " . $msg;
  fwrite($fd, $str . "\n");
  fclose($fd);*/
}

/*********** VIEW HELPER FUNCTIONS **********/
function getCompanyCode()
{
  global $cmpcode;

  return $cmpcode;
}

function getCompanyName()
{
  global $cmpname;

  return $cmpname;
}

function getLoggedInUserId()
{
  global $tkid;

  return $tkid;
}

function exists( $value )
{
  if( isset( $value ) )
  {
    $value = trim( $value );

    if( strlen( $value ) != 0 )
    {
      return true;
    }
  }

  return false;
}

function redirect( $url )
{
  header("Location: $url");
  exit();
}

function getParam( $name )
{
  return $_REQUEST[ $name ];
}

function getSQLParam( $name )
{
  return mysql_real_escape_string( getParam( $name ) );
}

function boolValue( $value )
{
  return $value ? "Yes" : "No";
}

function printMessages( $messages )
{
  if( count($messages) > 0 )
  {
    echo "<ul class=\"error-messages\">";

    foreach( $messages as $message )
    {
      echo "<li>$message</li>";
    }

    echo "</ul>";
  }
}
?>