<?
include("inc_ignite.php");
$orgGoal = OrgGoal::load( $_REQUEST["id"] );
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Eval years" => "setup_evalyears.php", $orgGoal->getEvalYear()->getShortDisplayName() => null, "Goals" => "setup_orggoals.php?eval_year_id={$orgGoal->getEvalYearId()}", "Goal" => null ) ); ?>

<table class="info" cellspacing="0">
  <tr>
    <th>Evaluation year</th>
    <td><?= $orgGoal->getEvalYear()->getDisplayName() ?></td>
  </tr>
  <tr>
    <th>Name</th>
    <td><?= $orgGoal->getName() ?></td>
  </tr>
  <tr>
    <th>Objective</th>
    <td><?= $orgGoal->getObjective() ?></td>
  </tr>
  <tr>
    <th>Measurement</th>
    <td><?= $orgGoal->getMeasurement() ?></td>
  </tr>
  <tr>
    <th>Baseline</th>
    <td><?= $orgGoal->getBaseline() ?></td>
  </tr>
  <tr>
    <th>Target unit</th>
    <td><?= $orgGoal->getTargetUnit() ?></td>
  </tr>
  <tr>
    <th>Deadline</th>
    <td><?= $orgGoal->getDeadline() ?></td>
  </tr>
</table>

<? ViewHelper::actions( array( "Back"=>"setup_orggoals.php?eval_year_id=" . $orgGoal->getEvalYearId() ) ); ?>

<? include("inc_footer.php"); ?>