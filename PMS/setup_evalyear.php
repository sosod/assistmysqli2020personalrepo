<?
include("inc_ignite.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $evalYear = new EvalYear();
  $evalYear->init( $_REQUEST , true );
  
  $dateSpan = new Date_Span();
  $dateSpan->setFromDateDiff( new Date( $evalYear->getStart() ), new Date( $evalYear->getEnd() ) );
  $days = $dateSpan->toDays();
  
  if( $days != 364 && $days != 365 )
  {
    $errors = array();
    $errors[] = "The specified start and end date does not equate to a full year."; 
  }
  else
  {
    $evalYear->save();
  
    ViewHelper::redirect( "setup_evalyearinfo.php?id=" . $evalYear->getId() );  
  }
}
else
{
  $evalYear = EvalYear::load( $_REQUEST["id"] );  
}
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Evaluation years" => "setup_evalyears.php", $evalYear->getDisplayName() => null ) ); ?>

<? ViewHelper::validation( "frmEvalYear" ); ?>

<? ViewHelper::errors( $errors ); ?>

<form id="frmEvalYear" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Start</th>
      <td><input type="text" name="start" value="<?= $evalYear->getStart() ?>" readonly="readonly" class="required dateISO date-input"/></td>
    </tr>
    <tr>
      <th>End</th>
      <td><input type="text" name="end" value="<?= $evalYear->getEnd() ?>" readonly="readonly" class="required dateISO date-input"/></td>
    </tr>
    <? /* if( $evalYear->isTransient() ) { 
    <tr>
      <th>Number of evaluation periods</th>
      <td><?= ViewHelper::select( "num_of_periods", ViewHelper::getNumberOfPeriodsSelectList(), $_REQUEST["num_of_periods"], true )</td>
    </tr>
     } */?>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $evalYear->getId() ?>" />
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"setup_evalyears.php" ) ); ?>

<? include("inc_footer.php"); ?>