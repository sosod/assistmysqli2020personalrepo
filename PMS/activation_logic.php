<?php

  $action = $_REQUEST["action"];

	/******************************************************************************/
	/************************** SESSION SELECTED USER *****************************/
	/******************************************************************************/
  $selectedUserId = $_SESSION["sess_{$tkid}_act_selected_user"];

  if( exists($selectedUserId) )
  {
    $selectedUser = User::loadExtended( $selectedUserId );
  }

  if( isset( $selectedUser ) == false || $selectedUser->isTransient() || $action == ViewHelper::$ACTION_SELECT_USER_YEAR )
  {
    $selected_user_id = $_REQUEST[ "selected_user_id" ];

    if( isset( $selected_user_id ) )
    {
      $selectedUser = User::loadExtended( $selected_user_id );
    }
    else
    {
      $selectedUser = User::loadExtended( getLoggedInUserId() );
    }

    //load the lazy members
    $selectedUser->getSetup();
    $selectedUser->getSetup()->getCategory();

    $_SESSION["sess_{$tkid}_act_selected_user"] = $selectedUser->getId();
  }

  if( $selectedUser->getSetup()->isTransient() )
  {
    ViewHelper::redirect( "warnings.php?type=2" );
  }

  /******************************************************************************/
	/************************** SESSION SELECTED YEAR *****************************/
	/******************************************************************************/
  $selectedYearId = $_SESSION["sess_{$tkid}_act_selected_year"];

  if( exists($selectedYearId) )
  {
    $selectedYear = EvalYear::load( $selectedYearId );
  }

  if( isset( $selectedYear ) == false || $selectedYear->isTransient() || $action == ViewHelper::$ACTION_SELECT_USER_YEAR )
  {
    $selected_year_id = $_REQUEST[ "selected_year_id" ];

    if( isset( $selected_year_id ) )
    {
      $selectedYear = EvalYear::load( $selected_year_id );
    }
    else
    {
      $selectedYear = EvalYear::loadWhere( "start <= '" . date( "Y-m-d" ) . "' AND end >= '" . date( "Y-m-d" ) . "'" );
    }

    $_SESSION["sess_{$tkid}_act_selected_year"] = $selectedYear->getId();
  }

  /******************************************************************************/
	/************************* ACTIVATION VIEW HELPER *****************************/
	/******************************************************************************/
  $activationLoadWhere = "user_id = '{$selectedUser->getId()}' AND eval_year_id = {$selectedYear->getId()}";

  /******************************************************************************/
	/************************ SESSION ACTIVATION STATUS ***************************/
	/******************************************************************************/
  $selectedActivationStatusId = $_SESSION["sess_{$tkid}_selected_activation_status"];

  if( exists($selectedActivationStatusId) )
  {
    $selectedActivationStatus = ActivationStatus::load( $selectedActivationStatusId );
  }

  if( ( isset( $selectedActivationStatus ) == false || $action == ViewHelper::$ACTION_SELECT_USER_YEAR ) &&
      ( isset( $selectedYear) && $selectedYear->isTransient() == false &&
        isset( $selectedUser) && $selectedUser->isTransient() == false ) )
  {
    $selectedActivationStatus = ActivationStatus::loadWhere( $activationLoadWhere );

    if( $selectedActivationStatus->isTransient() )
    {
      $selectedActivationStatus->setUserId( $selectedUser->getId() );
      $selectedActivationStatus->setEvalYearId( $selectedYear->getId() );
      $selectedActivationStatus->save();
    }

    $_SESSION["sess_{$tkid}_selected_activation_status"] = $selectedActivationStatus->getId();
  }

  /******************************************************************************/
	/************************ ACTIVATION STATUS UPDATE ****************************/
	/******************************************************************************/
  if( $action == ViewHelper::$ACTION_REQUEST_APPROVAL )
  {
    $selectedActivationStatus->setRequestApprovalOn( date( "Y-m-d H:i:s" ) );
    $selectedActivationStatus->save();

    $_SESSION["sess_{$tkid}_selected_activation_status"] = $selectedActivationStatus->getId();

    /* send email to the user's manager */
    $template = "template/act_step1.html";
    $replacements = array( "##employee##"=>$selectedUser->getFullName(), "##evalYear##"=>$selectedYear->getDisplayName() );
    $body = MailHelper::processTemplate( $template, $replacements );
    $subject = "Activation requires manager approval | Performance Assist";
    $toAddress = $selectedUser->getManagerEmail();
    $toName = $selectedUser->getManager();

    //MailHelper::sendMail( $toAddress, $toName, $subject, $body );
    $from = "assist@ignite4u.co.za";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail( $toAddress, $subject, $body, $header );
  }
  else if( $action == ViewHelper::$ACTION_MANAGER_APPROVAL )
  {
    $selectedActivationStatus->setManagerApprovalOn( date( "Y-m-d H:i:s" ) );
    $selectedActivationStatus->save();

    $_SESSION["sess_{$tkid}_selected_activation_status"] = $selectedActivationStatus->getId();

    /* send email to the user */
    $template = "template/act_step2.html";
    $replacements = array( "##employee##"=>$selectedUser->getFullName(), "##evalYear##"=>$selectedYear->getDisplayName() );
    $body = MailHelper::processTemplate( $template, $replacements );
    $subject = "Activation requires employee approval | Performance Assist";
    $toAddress = $selectedUser->getEmail();
    $toName = $selectedUser->getFullName();

    //MailHelper::sendMail( $toAddress, $toName, $subject, $body );
    $from = "assist@ignite4u.co.za";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail( $toAddress, $subject, $body, $header );
  }
  else if( $action == ViewHelper::$ACTION_USER_APPROVAL )
  {
    $selectedActivationStatus->setUserApprovalOn( date( "Y-m-d H:i:s" ) );
    $selectedActivationStatus->save();

    $_SESSION["sess_{$tkid}_selected_activation_status"] = $selectedActivationStatus->getId();

    /* send email to the user's manager */
    $template = "template/act_step3.html";
    $replacements = array( "##employee##"=>$selectedUser->getFullName(), "##evalYear##"=>$selectedYear->getDisplayName() );
    $body = MailHelper::processTemplate( $template, $replacements );
    $subject = "Activation requires manager approval | Performance Assist";
    $toAddress = $selectedUser->getManagerEmail();
    $toName = $selectedUser->getManager();

    //MailHelper::sendMail( $toAddress, $toName, $subject, $body );
    $from = "assist@ignite4u.co.za";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail( $toAddress, $subject, $body, $header );
  }

  /******************************************************************************/
	/***************************** ACCESS LOGIC ***********************************/
	/******************************************************************************/
  $isMutable = false;

  if( isset( $selectedActivationStatus)  )
  {
    if( $selectedActivationStatus->getRequestApprovalOn() == null )
    {
      $isMutable = true;
    }
    else
    {
      if( $selectedUser->getManagerId() == getLoggedInUserId() )
      {
        $isMutable = true;
      }
    }
  }
?>