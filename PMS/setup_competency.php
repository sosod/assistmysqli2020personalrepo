<?
include("inc_ignite.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $competency = new Competency();
  $competency->init( $_REQUEST , true );
  $competency->save();

  ViewHelper::redirect( "setup_competencies.php" );  
}
else
{
  $competency = Competency::load( $_REQUEST["id"] );
}
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Core Competencies" => "setup_competencies.php", $competency->getName() => null ) ); ?>

<? ViewHelper::validation( "frmCompetency" ); ?>

<? ViewHelper::errors( $errors ); ?>

<form id="frmCompetency" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Name</th>
      <td><?= $competency->getName() ?></td>
    </tr>
    <tr>
      <th>Definition</th>
      <td><?= $competency->getDefinition() ?></td>
    </tr>
    <tr>
      <th>Status:</th>
      <td><?= ViewHelper::select( "status", ViewHelper::statusSelectList(), $competency->getStatus(), true, false ) ?></td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $competency->getId() ?>" />
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"setup_competencies.php" ) ); ?>

<? include("inc_footer.php"); ?>