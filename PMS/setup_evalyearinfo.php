<?
include("inc_ignite.php");
include("inc_header.php");

$evalYear = EvalYear::load( $_REQUEST["id"] );
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Evaluation years" => "setup_evalyears.php", $evalYear->getDisplayName() => null ) ); ?>

<h2>Evaluation year</h2>
<table class="info" cellspacing="0">
  <tr>
    <th>Start</th>
    <td><?= $evalYear->getStart() ?></td>
  </tr>
  <tr>
    <th>End</th>
    <td><?= $evalYear->getEnd() ?></td>
  </tr>
</table>

<? ViewHelper::actions( array( "Back"=>"setup_evalyears.php", "Configure"=>"setup_evalyear.php?id={$evalYear->getId()}" ) ); ?>

<? /*
<h2>Evaluation periods</h2>
<table class="list" cellspacing="0">
  <tr>
    <th>Start</th>
    <th>End</td>
  </tr>
  <?
  foreach( $evalYear->getPeriods() as $obj )
  {
  ? >
  <tr>
    <td>< ?= $obj->getStart() ? ></td>
    <td>< ?= $obj->getEnd() ? ></td>    
  </tr>
  <? } ? >
</table>

< ? ViewHelper::actions( array( "Back"=>"setup_evalyears.php" ) ); ? >

*/ ?>

<?php
include("inc_footer.php");
?>