<?
include("inc_ignite.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $userSetup = new UserSetup();
  $userSetup->init( $_REQUEST , true );
  $userSetup->save();
  
  $userAccess = new UserAccess();
  $userAccess->init( $_REQUEST , true );
  $userAccess->save();
  
  ViewHelper::redirect( "setup_userinfo.php?user_id=" . $_REQUEST["user_id"] );
}

$userSetup = UserSetup::loadWhere( "user_id = '" . $_REQUEST["user_id"] . "'" );
$userAccess = UserAccess::loadWhere( "user_id = '" . $_REQUEST["user_id"] . "'" );

if( $userSetup->isTransient() )
{
  $userSetup->setUserId( $_REQUEST["user_id"] );
}
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Employees" => "setup_users.php", $userSetup->getUser()->getFullName() => null ) ); ?>

<? ViewHelper::validation( "frmUserSetup" ); ?>

<form id="frmUserSetup" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Name</th>
      <td><?= $userSetup->getUser()->getFullName() ?></td>
    </tr>
    <tr>
      <th>Category</th>
      <td><?= ViewHelper::select( "category_id", Category::selectList(), $userSetup->getCategoryId(), true, false ) ?></td>
    </tr>
    <tr>
      <th>Is HR manager?</th>
      <td><?= ViewHelper::select( "hr_manager", ViewHelper::getYesNoSelectList(), $userSetup->getHRManager(), true, false ) ?></td>
    </tr>
    <tr>
      <th>Section</th>
      <td><input type="text" name="section" value="<?= $userSetup->getSection() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Job level</th>
      <td><?= ViewHelper::select( "job_level", ViewHelper::getJobLevelSelectList(), $userSetup->getJobLevel(), true, false ) ?></td>
    </tr>
    <tr>
      <th>Job number</th>
      <td><input type="text" name="job_number" value="<?= $userSetup->getJobNumber() ?>" maxlength="20" class="required"/></td>
    </tr>
    <tr>
      <th>Access sections</th>
      <td>
        <input type="checkbox" name="sections[]" value="activation" checked="checked" readonly="readonly" />Activation<br/>
        <input type="checkbox" name="sections[]" value="evaluation" checked="checked" readonly="readonly" />Evaluation<br/> 
        <? foreach( UserAccess::$AVAILABLE_ACCESS_SECTIONS as $section => $desc ) { ?>
        <input type="checkbox" name="sections[]" value="<?= $section ?>" <?= ViewHelper::arrayContains( $userAccess->getSections(), $section ) ? "checked=\"checked\"" : "" ?> /><?= $desc ?><br/>
        <? } ?>
      </td>  
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $userSetup->getId() ?>" />
  <input type="hidden" name="user_id" value="<?= $userSetup->getUserId() ?>" />      
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"setup_users.php" ) ); ?>

<? include("inc_footer.php"); ?>