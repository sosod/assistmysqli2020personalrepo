<?
include("inc_ignite.php");

debug("in orggoal with action: " . $_REQUEST["action"] );
debug("in orggoal with id: " . $_REQUEST["id"] );
debug("in orggoal with eval year id: " . $_REQUEST["eval_year_id"] );

$evalYear = EvalYear::load( $_REQUEST["eval_year_id"] );

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $orgGoal = new OrgGoal();
  $orgGoal->init( $_REQUEST , true );
  
  $deadline = new Date( $orgGoal->getDeadline() );
  $endEvalYear = new Date( $evalYear->getEnd() );  
  
  if( $endEvalYear->before( $deadline ) )
  {
    $errors = array();
    $errors[] = "Deadline may not be after the evaluation year's end date which is " . $evalYear->getEnd();
  }
  else
  {
    $orgGoal->save();
    ViewHelper::redirect( "setup_orggoals.php?eval_year_id={$evalYear->getId()}" );    
  }
}
else if( $_REQUEST["action"] == ViewHelper::$ACTION_DELETE )
{
  debug("in delete action");
  
  $orgGoal = new OrgGoal();
  $orgGoal->delete( "id = " . $_REQUEST["id"] );
  
  debug("done with delete action");
  
  ViewHelper::redirect( "setup_orggoals.php?eval_year_id=" . $_REQUEST["eval_year_id"] );    
}
else
{
  $orgGoal = OrgGoal::load( $_REQUEST["id"] );
  
  if( $evalYear->isTransient() )
  {
    $evalYear = $orgGoal->getEvalYear();
  }
}
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Eval years" => "setup_evalyears.php", $evalYear->getShortDisplayName() => null, "Goals" => "setup_orggoals.php?eval_year_id={$evalYear->getId()}", "Goal" => null ) ); ?>

<? ViewHelper::validation( "frmOrgGoal" ); ?>

<? ViewHelper::errors( $errors ); ?>

<form id="frmOrgGoal" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Evaluation year</th>
      <td><?= $evalYear->getDisplayName() ?></td>
    </tr>
    <tr>
      <th>Name</th>
      <td><input type="text" name="name" value="<?= $orgGoal->getName() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Objective</th>
      <td><textarea name="objective" cols="40" rows="6" class="required"><?= $orgGoal->getObjective() ?></textarea></td>
    </tr>
    <tr>
      <th>Measurement</th>
      <td><textarea name="measurement" cols="40" rows="6" class="required"><?= $orgGoal->getMeasurement() ?></textarea></td>
    </tr>
    <tr>
      <th>Baseline</th>
      <td><input type="text" name="baseline" value="<?= $orgGoal->getBaseline() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Target unit</th>
      <td><input type="text" name="target_unit" value="<?= $orgGoal->getTargetUnit() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Deadline</th>
      <td><input type="text" name="deadline" value="<?= $orgGoal->getDeadline() ?>" readonly="readonly" class="required dateISO date-input"/></td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $orgGoal->getId() ?>" />
  <input type="hidden" name="eval_year_id" value="<?= $evalYear->getId() ?>" />  
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"setup_orggoals.php?eval_year_id={$evalYear->getId()}" ) ); ?>

<? include("inc_footer.php"); ?>