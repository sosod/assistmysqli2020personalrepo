<?
$comment = new Comment();

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $comment->init( $_REQUEST , true );
  $comment->save();
  
  ViewHelper::redirect( "{$section}.php#comments" );
}
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( ucwords( $section ) => "{$section}.php", $selectedUser->getFullName() => null, "Comment" => null ) ); ?>

<? include("{$section}_inc_summary.php"); ?>

<? ViewHelper::validation( "frmComment" ); ?>

<h2>Comment</h2>
<form id="frmComment" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Comment</th>
      <td><textarea name="message" cols="40" rows="6" class="required"><?= $comment->getMessage() ?></textarea></td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $comment->getId() ?>" />
  <input type="hidden" name="user_id" value="<?= $selectedUser->getId() ?>" />
  <input type="hidden" name="eval_year_id" value="<?= $selectedYear->getId() ?>" />
  <? if( $section == ViewHelper::$SECTION_EVALUATION ){ ?>    
  <input type="hidden" name="period" value="<?= $selectedPeriod->getPeriod() ?>" />      
  <? } ?>
  <input type="hidden" name="created_by_id" value="<?= getLoggedInUserId() ?>" />
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"{$section}.php#comments" ) ); ?>

<? include("inc_footer.php"); ?>