<?php

  $action = $_REQUEST["action"];

	/******************************************************************************/
	/************************** SESSION SELECTED USER *****************************/
	/******************************************************************************/
  $selectedUserId = $_SESSION["sess_{$tkid}_eval_selected_user"];

  if( exists($selectedUserId) )
  {
    $selectedUser = User::loadExtended( $selectedUserId );
  }

  if( isset( $selectedUser ) == false || $selectedUser->isTransient() || $action == ViewHelper::$ACTION_SELECT_USER_YEAR_PERIOD )
  {
    $selected_user_id = $_REQUEST[ "selected_user_id" ];

    if( isset( $selected_user_id ) )
    {
      $selectedUser = User::loadExtended( $selected_user_id );
    }
    else
    {
      $selectedUser = User::loadExtended( getLoggedInUserId() );
    }

    //load the lazy members
    $selectedUser->getSetup();
    $selectedUser->getSetup()->getCategory();

    $_SESSION["sess_{$tkid}_eval_selected_user"] = $selectedUser->getId();
  }

  if( $selectedUser->getSetup()->isTransient() )
  {
    ViewHelper::redirect( "warnings.php?type=2" );
  }

  /******************************************************************************/
	/************************** SESSION SELECTED YEAR *****************************/
	/******************************************************************************/
  $selectedYearId = $_SESSION["sess_{$tkid}_eval_selected_year"];

  if( exists($selectedYearId) )
  {
    $selectedYear = EvalYear::load( $selectedYearId );
  }

  if( isset( $selectedYear ) == false || $selectedYear->isTransient() || $action == ViewHelper::$ACTION_SELECT_USER_YEAR_PERIOD )
  {
    $selected_year_id = $_REQUEST[ "selected_year_id" ];

    if( isset( $selected_year_id ) )
    {
      $selectedYear = EvalYear::load( $selected_year_id );
    }
    else
    {
      $selectedYear = EvalYear::loadWhere( "start <= '" . date( "Y-m-d" ) . "' AND end >= '" . date( "Y-m-d" ) . "'" );
    }

    $_SESSION["sess_{$tkid}_eval_selected_year"] = $selectedYear->getId();
  }

  /******************************************************************************/
	/************************* SESSION SELECTED PERIOD ****************************/
	/******************************************************************************/
  $sess_selected_period = $_SESSION["sess_{$tkid}_eval_selected_period"];

  if( exists( $sess_selected_period))
  {
     $selectedPeriod = new PeriodWrapper();
     $selectedPeriod->setPeriod( $sess_selected_period );
     $selectedPeriod->setStart( DateHelper::getPeriodStartDate( $selectedYear->getStart(), $selectedUser->getCategory()->getPeriodFrequency(), $sess_selected_period) );
     $selectedPeriod->setEnd( DateHelper::getPeriodEndDate( $selectedYear->getStart(), $selectedUser->getCategory()->getPeriodFrequency(), $sess_selected_period) );
  }

  if( isset( $selectedYear ) && $selectedYear->isTransient() == false )
  {
    if( isset( $selectedPeriod ) == false || $selectedPeriod->isTransient() || $action == ViewHelper::$ACTION_SELECT_USER_YEAR_PERIOD )
    {
      $selected_period = $_REQUEST["selected_period"];

      if( isset( $selected_period ) == false )
      {
        $selected_period = 1;
      }

      $selectedPeriod = new PeriodWrapper();
      $selectedPeriod->setPeriod( $selected_period );
      $selectedPeriod->setStart( DateHelper::getPeriodStartDate( $selectedYear->getStart(), $selectedUser->getCategory()->getPeriodFrequency(), $selected_period) );
      $selectedPeriod->setEnd( DateHelper::getPeriodEndDate( $selectedYear->getStart(), $selectedUser->getCategory()->getPeriodFrequency(), $selected_period) );

      $_SESSION["sess_{$tkid}_eval_selected_period"] = $selected_period;
    }
  }

  if( isset( $selectedYear ) && $selectedYear->isTransient() == false &&
      isset( $selectedUser ) && $selectedUser->isTransient() == false &&
      isset( $selectedPeriod ) && $selectedPeriod->isTransient() == false )
  {
    /******************************************************************************/
  	/************************* EVALUATION VIEW HELPER *****************************/
  	/******************************************************************************/
    $evaluationLoadWhere = "user_id = '{$selectedUser->getId()}' AND eval_year_id = {$selectedYear->getId()} AND period = {$selectedPeriod->getPeriod()}";

    /******************************************************************************/
  	/************************ SESSION EVALUATION STATUS ***************************/
  	/******************************************************************************/
    $selectedEvaluationStatusId = $_SESSION["sess_{$tkid}_selected_evaluation_status"];

    if( exists($selectedEvaluationStatusId) )
    {
      $selectedEvaluationStatus = EvaluationStatus::load($selectedEvaluationStatusId);
    }

    if( ( isset( $selectedEvaluationStatus ) == false || $action == ViewHelper::$ACTION_SELECT_USER_YEAR_PERIOD ) &&
        ( isset( $selectedYear) && $selectedYear->isTransient() == false &&
          isset( $selectedUser) && $selectedUser->isTransient() == false &&
          isset( $selectedPeriod ) && $selectedPeriod->isTransient() == false ) )  {

      $selectedEvaluationStatus = EvaluationStatus::loadWhere( $evaluationLoadWhere );

      if( $selectedEvaluationStatus->isTransient() )
      {
        $selectedEvaluationStatus->setUserId( $selectedUser->getId() );
        $selectedEvaluationStatus->setEvalYearId( $selectedYear->getId() );
        $selectedEvaluationStatus->setPeriod( $selectedPeriod->getPeriod() );
        $selectedEvaluationStatus->save();
      }

      $_SESSION["sess_{$tkid}_selected_evaluation_status"] = $selectedEvaluationStatus->getId();
    }
  }

  /******************************************************************************/
	/************************ EVALUATION STATUS UPDATE ****************************/
	/******************************************************************************/

  if( $action == ViewHelper::$ACTION_USER_EVALUATION_COMPLETE )
  {
    $selectedEvaluationStatus->setUserCompleted( date( "Y-m-d H:i:s" ) );
    $selectedEvaluationStatus->setUserCompletedComment( mysql_real_escape_string( $_REQUEST["status_comment"] ) );
    $selectedEvaluationStatus->save();

    $_SESSION["sess_{$tkid}_selected_evaluation_status"] = $selectedEvaluationStatus->getId();

    /* send email to the user's manager */
    $template = "template/eval_employee_completed.html";
    $replacements = array( "##employee##"=>$selectedUser->getFullName(), "##evalYear##"=>$selectedYear->getDisplayName(), "##period##"=>$selectedPeriod->getDisplayName() );
    $body = MailHelper::processTemplate( $template, $replacements );
    $subject = "Employee Evaluation Completed | Performance Assist ";
    $toAddress = $selectedUser->getManagerEmail();
    $toName = $selectedUser->getManager();

    //MailHelper::sendMail( $toAddress, $toName, $subject, $body );
    $from = "assist@ignite4u.co.za";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail( $toAddress, $subject, $body, $header );
  }
  else if( $action == ViewHelper::$ACTION_MANAGER_EVALUATION_COMPLETE )
  {
    $selectedEvaluationStatus->setManagerCompleted( date( "Y-m-d H:i:s" ) );
    $selectedEvaluationStatus->setManagerCompletedComment( mysql_real_escape_string( $_REQUEST["status_comment"] ) );
    $selectedEvaluationStatus->save();

    $_SESSION["sess_{$tkid}_selected_evaluation_status"] = $selectedEvaluationStatus->getId();

    /* send email to the manager */
    $template = "template/eval_manager_completed.html";
    $replacements = array( "##employee##"=>$selectedUser->getFullName(), "##evalYear##"=>$selectedYear->getDisplayName(), "##period##"=>$selectedPeriod->getDisplayName() );
    $body = MailHelper::processTemplate( $template, $replacements );
    $subject = "Manager Evaluation Completed | Performance Assist | Ignite Assist";
    $toAddress = $selectedUser->getManagerEmail();
    $toName = $selectedUser->getManager();

    //MailHelper::sendMail( $toAddress, $toName, $subject, $body );
    $from = "assist@ignite4u.co.za";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail( $toAddress, $subject, $body, $header );
  }
  else if( $action == ViewHelper::$ACTION_JOINT_EVALUATION_COMPLETE )
  {
    $selectedEvaluationStatus->setJointCompleted( date( "Y-m-d H:i:s" ) );
    $selectedEvaluationStatus->setJointCompletedComment( mysql_real_escape_string( $_REQUEST["status_comment"] ) );
    $selectedEvaluationStatus->save();

    $_SESSION["sess_{$tkid}_selected_evaluation_status"] = $selectedEvaluationStatus->getId();

    /* send email to the employee */
    $template = "template/eval_joint_completed.html";
    $replacements = array( "##employee##"=>$selectedUser->getFullName(), "##evalYear##"=>$selectedYear->getDisplayName(), "##period##"=>$selectedPeriod->getDisplayName() );
    $body = MailHelper::processTemplate( $template, $replacements );
    $subject = "Joint Evaluation Completed | Performance Assist | Ignite Assist";
    $toAddress = $selectedUser->getEmail();
    $toName = $selectedUser->getFullName();

    //MailHelper::sendMail( $toAddress, $toName, $subject, $body );
    $from = "assist@ignite4u.co.za";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail( $toAddress, $subject, $body, $header );
  }
  else if( $action == ViewHelper::$ACTION_JOINT_EVALUATION_APPROVED )
  {
    $selectedEvaluationStatus->setJointApproved( date( "Y-m-d H:i:s" ) );
    $selectedEvaluationStatus->setJointApprovedComment( mysql_real_escape_string( $_REQUEST["status_comment"] ) );
    $selectedEvaluationStatus->setAverageRating( mysql_real_escape_string( $_REQUEST["average_rating"] ) );
    $selectedEvaluationStatus->save();

    $_SESSION["sess_{$tkid}_selected_evaluation_status"] = $selectedEvaluationStatus->getId();

    /* send email to the manager */
    $template = "template/eval_joint_approved.html";
    $replacements = array( "##employee##"=>$selectedUser->getFullName(), "##evalYear##"=>$selectedYear->getDisplayName(), "##period##"=>$selectedPeriod->getDisplayName() );
    $body = MailHelper::processTemplate( $template, $replacements );
    $subject = "Joint Evaluation Approved | Performance Assist | Ignite Assist";
    $toAddress = $selectedUser->getManagerEmail();
    $toName = $selectedUser->getManager();

    //MailHelper::sendMail( $toAddress, $toName, $subject, $body );
    $from = "assist@ignite4u.co.za";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail( $toAddress, $subject, $body, $header );
  }
  else if( $action == ViewHelper::$ACTION_FISHBOWL_EVALUATION_COMPLETE )
  {
    $selectedEvaluationStatus->setFishbowlCompleted( date( "Y-m-d H:i:s" ) );
    $selectedEvaluationStatus->setFishbowlCompletedComment( mysql_real_escape_string( $_REQUEST["status_comment"] ) );
    $selectedEvaluationStatus->setFishbowlRating( mysql_real_escape_string( $_REQUEST["fishbowl_rating"] ) );
    $selectedEvaluationStatus->save();

    $_SESSION["sess_{$tkid}_selected_evaluation_status"] = $selectedEvaluationStatus->getId();

    /* send email to the employee */
    $template = "template/eval_fishbowl_completed.html";
    $replacements = array( "##employee##"=>$selectedUser->getFullName(), "##evalYear##"=>$selectedYear->getDisplayName(), "##period##"=>$selectedPeriod->getDisplayName() );
    $body = MailHelper::processTemplate( $template, $replacements );
    $subject = "Fishbowl Evaluation Completed | Performance Assist | Ignite Assist";
    $toAddress = $selectedUser->getEmail();
    $toName = $selectedUser->getFullName();

    //MailHelper::sendMail( $toAddress, $toName, $subject, $body );
    $from = "assist@ignite4u.co.za";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail( $toAddress, $subject, $body, $header );
  }
  else if( $action == ViewHelper::$ACTION_FISHBOWL_EVALUATION_APPROVED )
  {
    $selectedEvaluationStatus->setFishbowlApproved( date( "Y-m-d H:i:s" ) );
    $selectedEvaluationStatus->setFishbowlApprovedComment( mysql_real_escape_string( $_REQUEST["status_comment"] ) );
    $selectedEvaluationStatus->save();

    $_SESSION["sess_{$tkid}_selected_evaluation_status"] = $selectedEvaluationStatus->getId();

    /* send email to the manager */
    $template = "template/eval_fishbowl_approved.html";
    $replacements = array( "##employee##"=>$selectedUser->getFullName(), "##evalYear##"=>$selectedYear->getDisplayName(), "##period##"=>$selectedPeriod->getDisplayName() );
    $body = MailHelper::processTemplate( $template, $replacements );
    $subject = "Fishbowl Evaluation Approved | Performance Assist | Ignite Assist";
    $toAddress = $selectedUser->getManagerEmail();
    $toName = $selectedUser->getManager();

    //MailHelper::sendMail( $toAddress, $toName, $subject, $body );
    $from = "assist@ignite4u.co.za";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail( $toAddress, $subject, $body, $header );
  }

  /******************************************************************************/
	/***************************** ACCESS LOGIC ***********************************/
	/******************************************************************************/
  $isMutable = false;
  $showStatusForm = false;

  if( isset( $selectedEvaluationStatus ) )
  {
    if( $selectedEvaluationStatus->getUserCompleted() == null )
    {
      if( $selectedUser->getId() == getLoggedInUserId() )
      {
        $isMutable = true;
        $showStatusForm = true;
        $heading = "Complete Employee Evaluation";
        $button = "Mark As Completed";
        $statusAction = ViewHelper::$ACTION_USER_EVALUATION_COMPLETE;
      }
      else
      {
        $errors[] = "Employee evaluation must be completed by the employee";
      }
    }
    else if( $selectedEvaluationStatus->getManagerCompleted() == null )
    {
      if( $selectedUser->getManagerId() == getLoggedInUserId() )
      {
        $isMutable = true;
        $showStatusForm = true;
        $heading = "Complete Manager Evaluation";
        $button = "Mark As Completed";
        $statusAction = ViewHelper::$ACTION_MANAGER_EVALUATION_COMPLETE;
      }
      else
      {
        $errors[] = "Manager evaluation must be completed by the employee's manager";
      }
    }
    else if( $selectedEvaluationStatus->getJointCompleted() == null )
    {
      if( $selectedUser->getManagerId() == getLoggedInUserId() )
      {
        $isMutable = true;
        $showStatusForm = true;
        $heading = "Complete Joint Evaluation";
        $button = "Mark As Completed";
        $statusAction = ViewHelper::$ACTION_JOINT_EVALUATION_COMPLETE;
      }
      else
      {
        $errors[] = "Joint evaluation must be completed by the employee's manager";
      }
    }
    else if( $selectedEvaluationStatus->getJointApproved() == null )
    {
      if( $selectedUser->getId() == getLoggedInUserId() )
      {
        $isMutable = false;
        $showStatusForm = true;
        $heading = "Joint Evaluation Approval";
        $button = "Mark As Approved";
        $statusAction = ViewHelper::$ACTION_JOINT_EVALUATION_APPROVED;
        $show = true;
      }
      else
      {
        if( $selectedUser->getManagerId() == getLoggedInUserId() )
        {
          $showStatusForm = false;
          $isMutable = true;
        }

        $errors[] = "Joint evaluation must be approved by the employee";
      }
    }
    else if( $selectedEvaluationStatus->getFishbowlCompleted() == null )
    {
      if( getLoggedInUser()->getSetup()->getHRManager() )
      {
        $isMutable = false;
        $showStatusForm = true;
        $heading = "Complete Fishbowl Evaluation";
        $button = "Mark As Completed";
        $statusAction = ViewHelper::$ACTION_FISHBOWL_EVALUATION_COMPLETE;
        $show = true;
      }
      else
      {
        $errors[] = "Fishbowl evaluation must be completed by a HR manager";
      }
    }
    else if( $selectedEvaluationStatus->getFishbowlApproved() == null )
    {
      if( $selectedUser->getId() == getLoggedInUserId() )
      {
        $isMutable = false;
        $showStatusForm = true;
        $heading = "Fishbowl Evaluation Approval";
        $button = "Mark As Approved";
        $statusAction = ViewHelper::$ACTION_FISHBOWL_EVALUATION_APPROVED;
        $show = true;
      }
      else
      {
        $errors[] = "Fishbowl evaluation must be approved by the employee";
      }
    }
  }

?>