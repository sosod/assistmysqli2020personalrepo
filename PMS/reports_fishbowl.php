<?php
include("inc_ignite.php");

$showReport = false;

$selectedYear = new EvalYear();
$selectedCategory = new Category();
$selectedPeriod = null;

if( $_REQUEST["action"] == ViewHelper::$ACTION_SELECT_YEAR_PERIOD )
{
  $selectedYear = EvalYear::load( $_REQUEST["eval_year_id"] );
  $selectedCategory = Category::load( $_REQUEST["category_id"] );  
  $selectedPeriod = $_REQUEST["period"];    
  
  if( $selectedPeriod == "" )
  {
    $selectedPeriod = null;
  }
  
  if( $selectedYear->isTransient() == false && $selectedCategory->isTransient() == false && isset( $selectedPeriod ) )
  {
    $showReport = true;
    
    $orderBy = $_REQUEST["orderBy"];
    if( exists( $orderBy ) == false )
    {
      $orderBy = "tkname";
    }  
    
    $orderByDir = $_REQUEST["orderByDir"];
    if( exists( $orderByDir ) == false )
    {
      $orderByDir = "ASC";
    }
    
    $departmentId = $_REQUEST["departmentId"];
    $section = $_REQUEST["section"];
    $managerId = $_REQUEST["managerId"];
    $jobLevel = $_REQUEST["jobLevel"];  
    
    
    /* BUILD UP THE SQL QUERY */
    $sql = " SELECT u.*, " .
           " us.section, " .
           " us.job_level, " .
           " d.value AS department, ".
           " j.value AS job_title, " .
           " (SELECT CONCAT( um.tkname, ' ', um.tksurname ) FROM assist_".getCompanyCode()."_timekeep um WHERE um.tkid = u.tkmanager ) AS manager, " .
           " (SELECT es.average_rating FROM assist_".getCompanyCode()."_pms_evaluationstatus es WHERE es.user_id = u.tkid AND es.eval_year_id = ".$selectedYear->getId()." AND es.period = ".$selectedPeriod." ) AS avg_rating,  " .
           " (SELECT es.fishbowl_rating FROM assist_".getCompanyCode()."_pms_evaluationstatus es WHERE es.user_id = u.tkid AND es.eval_year_id = ".$selectedYear->getId()." AND es.period = ".$selectedPeriod." ) AS fishbowl_rating " .
           " FROM assist_".getCompanyCode()."_timekeep u, " .
           " assist_".getCompanyCode()."_pms_usersetup us, " .
           " assist_".getCompanyCode()."_list_dept d, " .
           " assist_".getCompanyCode()."_list_jobtitle j " .
           " WHERE us.user_id = u.tkid " .
           " AND us.category_id =  " . $selectedCategory->getId() .
           " AND d.id = u.tkdept " .
           " AND j.id = u.tkdesig ";   
           
    if( exists( $departmentId ) )
    {
      $sql .= " AND u.tkdept = " . $departmentId; 
    }
    
    if( exists( $section ) )
    {
      $sql .= " AND us.section = '" . $section . "' "; 
    }         
    
    if( exists( $managerId ) )
    {
      $sql .= " AND u.tkmanager = '" . $managerId . "'"; 
    }
    
    if( exists( $jobLevel ) )
    {
      $sql .= " AND us.job_level = " . $jobLevel; 
    }
    
    $sql .= " ORDER BY " . $orderBy . " " . $orderByDir;
    
    debug( $sql );
  }
}
?>
<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Reports" => "reports.php", "Fishbowl" => null ) ); ?>

<form action="reports_fishbowl.php" id="frmUserSelect" method="get" >
  <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
    <tr>
      <td valign="middle">
        <strong>Category:</strong> 
        <?= ViewHelper::select( "category_id", Category::selectList(), $selectedCategory->getId(), true ) ?>
        <strong>Evaluation year:</strong> 
        <?= ViewHelper::select( "eval_year_id", EvalYear::selectList(), $selectedYear->getId(), true ) ?>
      </td>
      <td valign="middle"><input type="submit" value="Select" /></td>
    </tr>
  </table>
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SELECT_YEAR_PERIOD ?>" />  
</form> 
<?
if( $selectedYear->isTransient() == false && $selectedCategory->isTransient() == false )
{
?>
<br/>   
<form action="reports_fishbowl.php" id="frmPeriodSelect" method="get" >
  <table class="form autosize" cellspacing="0" style="margin-bottom:0;">
    <tr>
      <td>
        <strong>Evaluation period:</strong> 
        <?
          $periodSelectList = array();
          for( $i = 1; $i <= $selectedCategory->getPeriodFrequency(); $i++ )
          {
            if( DateHelper::isFuturePeriod( $selectedYear->getStart(), $selectedCategory->getPeriodFrequency(), $i ) == false )
            {
              $periodSelectList[] = array( $i, DateHelper::getPeriodDisplay( $selectedYear->getStart(), $selectedCategory->getPeriodFrequency(), $i ) );
            }
          }
          
          ViewHelper::select( "period", $periodSelectList, $selectedPeriod, true );
        ?>
      </td>
      <td valign="middle"><input type="submit" value="Select" /></td>
    </tr>
  </table>
  <input type="hidden" name="eval_year_id" value="<?= $selectedYear->getId() ?>" />
  <input type="hidden" name="category_id" value="<?= $selectedCategory->getId() ?>" />
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SELECT_YEAR_PERIOD ?>" />
</form>  
<?    
}
?>
<hr/>
    
<?
  if( $showReport )
  {
?>
    
<h5>Report Filter:</h5>
    
<form action="reports_fishbowl.php" id="frmFilter" method="get" class="filter" >
  <div>
    <label>Department</label>
    <?= ViewHelper::select( "departmentId", ReportHelper::getDepartmentSelectList(), $departmentId, false, "All" ) ?>
    <label>Section</label>
    <?= ViewHelper::select( "section",  ReportHelper::getSectionSelectList(), $section, false, "All" ) ?>
  </div>
  <div> 
    <label>Manager</label>
    <?= ViewHelper::select( "managerId", User::getManagerSelectList(), $managerId, false, "All" ) ?>
    <label>Job level</label>
    <?= ViewHelper::select( "jobLevel", ViewHelper::getJobLevelSelectList(), $jobLevel, false, "All" ) ?>
  </div>  
  <div>    
    <label>Order by</label>
    <?= ViewHelper::select( "orderBy", ReportHelper::getFishbowlOrderBySelectList(), $orderBy, false, false ) ?>
    <?= ViewHelper::select( "orderByDir", ReportHelper::getOrderByDirectionSelectList(), $orderByDir, false, false ) ?>
    <input type="submit" value="Select" />
  </div>
  <input type="hidden" name="eval_year_id" value="<?= $selectedYear->getId() ?>" />
  <input type="hidden" name="category_id" value="<?= $selectedCategory->getId() ?>" />
  <input type="hidden" name="period" value="<?= $selectedPeriod ?>" />    
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SELECT_YEAR_PERIOD ?>" />  
</form>  
    
<table class="list full" cellspacing="0" style="margin-top:15px;">
  <tr>
    <th width="20%">User</th>
    <th width="12%">Job level</th>
    <th width="16%">Department</th>
    <th width="16%">Section</th>
    <th width="16%">Manager</th>
    <th width="10%">Avg Rating</th>
    <th width="10%">Fishbowl Rating</th>
  </tr>
  <?
    $data = DBHelper::getListRecords( $sql );
    
    if( count($data) > 0 )
    {
      foreach( $data as $record ) 
      {
  ?>
  <tr>
    <td><?= $record["tkname"] ?> <?= $record["tksurname"] ?></td>
    <td align="right"><?= $record["job_level"] ?></td>
    <td><?= $record["department"] ?></td>
    <td><?= $record["section"] ?></td>
    <td><?= $record["tkmanager"] == "S" ? "Self" : $record["manager"] ?></td>
    <td align="right"><?= exists($record["avg_rating"]) ? $record["avg_rating"] : "?" ?></td>
    <td align="right"><?= exists($record["fishbowl_rating"]) ? $record["fishbowl_rating"] : "?" ?></td>
  </tr>
  <?
      }
    }
    else
    {
  ?>
  <tr><td colspan="8">No record match the selected filter options.</td></tr>
  <?        
    }  
  ?>  
</table>    
<?
  }
  else
  {
?>
<p class="instruction">Please select an evaluation year, category and period above to continue.</p> 
<?
  }
?>
  
<?php
include("inc_footer.php");
?>