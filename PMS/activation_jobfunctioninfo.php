<?
include("inc_ignite.php");
include("activation_logic.php");

$jobFunction = JobFunction::load( $_REQUEST["id"] );
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Activation" => "activation.php", $selectedUser->getFullName() => null, "Job function" => null ) ); ?>

<? include("activation_inc_summary.php"); ?>

<h2>Job Function</h2>
<table class="info" cellspacing="0">
  <tr>
    <th>Name</th>
    <td><?= $jobFunction->getName() ?></td>
  </tr>
  <tr>
    <th>Description</th>
    <td><?= $jobFunction->getDescription() ?></td>
  </tr>
  <tr>
    <th>Status:</th>
    <td><?= ViewHelper::displayStatus( $jobFunction->getStatus() ) ?></td>
  </tr>
  <tr>
    <th>Created by:</th>
    <td><?= $jobFunction->getCreatedBy()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Created on:</th>
    <td><?= $jobFunction->getCreatedOn() ?></td>
  </tr>
</table>

<? ViewHelper::actions( array( "Back"=>"activation.php#jobfunctions" ) ); ?>

<? include("inc_footer.php"); ?>