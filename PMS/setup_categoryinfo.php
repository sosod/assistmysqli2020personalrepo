<?
include("inc_ignite.php");
include("inc_header.php");

$category = Category::load( $_REQUEST["id"] );
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Setup" => "setup.php", "Categories" => "setup_categories.php", $category->getName() => null ) ); ?>

<h2>Category</h2>
<table class="info" cellspacing="0">
  <tr>
    <th>Name</th>
    <td><?= $category->getName() ?></td>
  </tr>
  <tr>
    <th>Evaluation frequency</th>
    <td><?= ViewHelper::displayPeriodFrequency( $category->getPeriodFrequency() ) ?></td>
  </tr>
  <tr>
    <th>Use weights?</th>
    <td><?= ViewHelper::boolYesNo( $category->getWeights() ) ?></td>
  </tr>
  <tr>
    <th>Min KPAs</th>
    <td><?= $category->getMinKPAs() ?></td>
  </tr>
  <tr>
    <th>Max KPAs</th>
    <td><?= $category->getMaxKPAs() ?></td>
  </tr>
  <tr>
    <th>Organisation KPA weight</th>
    <td><?= $category->getOrganisationKPAWeight() ?>%</td>
  </tr>
  <tr>
    <th>Learning KPA weight</th>
    <td><?= $category->getLearningKPAWeight() ?>%</td>
  </tr>
  <tr>
    <th>Job KPA weight</th>
    <td><?= $category->getJobKPAWeight() ?>%</td>
  </tr>
</table>

<? ViewHelper::actions( array( "Back"=>"setup_categories.php", "Configure"=>"setup_category.php?id={$category->getId()}" ) ); ?>

<?php
include("inc_footer.php");
?>