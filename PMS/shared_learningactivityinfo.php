<?
$learningActivity = LearningActivity::load( $_REQUEST["id"] );
?>

<? include("inc_header.php"); ?>

<?php if( $section == ViewHelper::$SECTION_REPORTS ){ ?>

<? ViewHelper::breadcrumbs( array( ucwords( $section ) => "{$section}.php", "Learning Activities" => "reports_learning.php", "Learning Activity" => null ) ); ?>

<?php } else { ?>

<? ViewHelper::breadcrumbs( array( ucwords( $section ) => "{$section}.php", $selectedUser->getFullName() => null, "Learning Activity" => null ) ); ?>

<? include("{$section}_inc_summary.php"); ?>

<?php } ?>

<h2>Learning Activity</h2>
<table class="info" cellspacing="0">
  <tr>
    <th>Name</th>
    <td><?= $learningActivity->getName() ?></td>
  </tr>
  <tr>
    <th>Priority</th>
    <td><?= LearningActivity::displayPriority( $learningActivity->getPriority() ) ?></td>
  </tr>
  <tr>
    <th>Name of training provider</th>
    <td><?= $learningActivity->getProvider() ?></td>
  </tr>
  <tr>
    <th>Suggested training intervention</th>
    <td><?= $learningActivity->getIntervention() ?></td>
  </tr>
  <tr>
    <th>Mode of delivery</th>
    <td><?= $learningActivity->getDeliveryMode() ?></td>
  </tr>
  <tr>
    <th>Suggested Time frames</th>
    <td><?= $learningActivity->getTimeFrames() ?></td>
  </tr>
  <tr>
    <th>Outcomes expected</th>
    <td><?= $learningActivity->getOutcome() ?></td>
  </tr>
  <tr>
    <th>Work opportunity</th>
    <td><?= $learningActivity->getWorkOpportunity() ?></td>
  </tr>
  <tr>
    <th>Support Person</th>
    <td><?= $learningActivity->getSupportPerson()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Status</th>
    <td><?= LearningActivity::displayStatus( $learningActivity->getStatus() ) ?></td>
  </tr>
  <tr>
    <th>Created by:</th>
    <td><?= $learningActivity->getCreatedBy()->getFullName() ?></td>
  </tr>
  <tr>
    <th>Created on:</th>
    <td><?= $learningActivity->getCreatedOn() ?></td>
  </tr>
</table>

<? ViewHelper::actions( array( "Back"=>"{$section}.php#learningactivities" ) ); ?>

<? include("inc_footer.php"); ?>