<?
include("inc_ignite.php");
include("activation_logic.php");

if( $_REQUEST["action"] == ViewHelper::$ACTION_SAVE )
{
  $qualification = new Qualification();
  $qualification->init( $_REQUEST , true );
  $qualification->save();
  
  ViewHelper::redirect( "activation.php#qualifications" );
}
else
{
  $qualification = Qualification::load( $_REQUEST["id"] );
}
?>

<? include("inc_header.php"); ?>

<? ViewHelper::breadcrumbs( array( "Activation" => "activation.php", $selectedUser->getFullName() => null, "Qualification" => null ) ); ?>

<? include("activation_inc_summary.php"); ?>

<? ViewHelper::validation( "frmQualification" ); ?>

<h2>Qualification</h2>
<form id="frmQualification" method="post" >  
  <table class="form" cellspacing="0">
    <tr>
      <th>Type:</th>
      <td><?= ViewHelper::select( "type", Qualification::typeSelectList(), $qualification->getType(), true ) ?></td>
    </tr>
    <tr>
      <th>Name</th>
      <td><input type="text" name="name" value="<?= $qualification->getName() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Institution</th>
      <td><input type="text" name="institution" value="<?= $qualification->getInstitution() ?>" maxlength="255" class="required"/></td>
    </tr>
    <tr>
      <th>Accredited:</th>
      <td><?= ViewHelper::select( "accredited", ViewHelper::getYesNoSelectList(), $qualification->getAccredited(), true ) ?></td>
    </tr>
    <tr>
      <th>Comments</th>
      <td><textarea name="comments" cols="40" rows="6"><?= $qualification->getComments() ?></textarea></td>
    </tr>
    <tr>
      <th>Status:</th>
      <td><?= ViewHelper::select( "status", ViewHelper::statusSelectList(), $qualification->getStatus(), true, false ) ?></td>
    </tr>
    <tr>
      <td></td><td><input type="submit" value="Save" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?= $qualification->getId() ?>" />
  <input type="hidden" name="user_id" value="<?= $selectedUser->getId() ?>" />
  <? if( $qualification->isTransient() ) { ?>
  <input type="hidden" name="created_by_id" value="<?= getLoggedInUserId() ?>" />  
  <? } ?>
  <input type="hidden" name="action" value="<?= ViewHelper::$ACTION_SAVE ?>" />
</form>

<? ViewHelper::actions( array( "Back"=>"activation.php#qualifications" ) ); ?>

<? include("inc_footer.php"); ?>