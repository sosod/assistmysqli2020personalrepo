<?php
include("inc_ignite.php");
include("inc_errorlog.php");
?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/assist.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($moduletitle); ?>: Update</b></h1>
<?php

if($_FILES["minfile"]["error"] > 0) //IF ERROR WITH UPLOAD FILE
{
    if($_FILES["minfile"]["error"]==2)
    {
        echo("<h2>Error</h2><p>Error: ".$_FILES["minfile"]["error"]." - File size exceeds max file size limit.</p>");
    }
    else
    {
        echo("<h2>Error</h2><p>Error: ".$_FILES["minfile"]["error"]."</p>");
    }
}
else    //IF ERROR WITH UPLOAD FILE
{
    //GET FILE TYPE
    //$ftype = $_FILES["minfile"]["type"];
    //if($ftype != "application/pdf") //IF FILE TYPE IS NOT PDF
    //{
    //    echo("<h2>Error</h2><p>Invalid file.  Only PDF documents may be uploaded.</p>");
    //}
    //else    //IF FILE TYPE IS NOT PDF
    //{
        //GET FORM DATA
        $mincateid = $_POST["mincateid"];
//        $mday = $_POST["mday"];
//        $mmon = $_POST["mmon"];
//        $myear = $_POST["myear"];
        $mindt = $_POST['mindt'];
        $mintoc = $_POST["mintoc"];
//        $mintoc = str_replace("'","&#39",$mintoc);
        $mintoc = htmlentities($mintoc,ENT_QUOTES,"ISO-8859-1");
        //SET MINUTES DATE
        $mind = explode("-",$mindt);
        $mindate = mktime(0,0,0,$mind[1],$mind[0],$mind[2]);
//        $mdate = getdate(mktime(0,0,0,$mmon,$mday,$myear));
//        $mindate = $mdate[0];
        //UPDATE CONTENT TABLE
        $sql = "INSERT INTO assist_".$cmpcode."_".$moduledb."_content SET ";
        $sql .= "docdate = '".$mindate."', ";
        $sql .= "doctoc = '".$mintoc."', ";
        $sql .= "docfilename = '', ";
        $sql .= "doccateid = '".$mincateid."', ";
        $sql .= "docyn = 'N', ";
        $sql .= "docadduser = '".$tkid."', ";
        $sql .= "docadddate = '".$today."'";
        //db_insert($sql)
        $minid = db_insert($sql);
            //UPDATE TRANSACTION LOG
            $tsql = $sql;
            //$tref = "MN";
            $trans = "Added new record - ".$minid;
            include("inc_transaction_log.php");
/*        //GET CONTENT ID FOR NEW FILENAME
        $sql = "SELECT docid FROM assist_".$cmpcode."_".$moduledb."_content WHERE ";
        $sql .= "docdate = '".$mindate."' AND ";
        $sql .= "doctoc = '".$mintoc."' AND ";
        $sql .= "docfilename = '' AND ";
        $sql .= "doccateid = '".$mincateid."' AND ";
        $sql .= "docyn = 'N' AND ";
        $sql .= "docadduser = '".$tkid."' AND ";
        $sql .= "docadddate = '".$today."'";
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $minid = $row['docid'];
        mysql_close();
*/        //SET FILENAME
    set_time_limit(180);
    if(strlen($minid) > 0 && is_numeric($minid) && $minid > 0)
    {
        $mf = $_FILES["minfile"]["name"];
        $mf2 = explode(".",$mf);
        $mf3 = count($mf2);
        $minfiletype = $mf2[$mf3-1];
        $minfilename = "reporting_".date("Ymd",$mindate)."_".$minid.".".$minfiletype;
		checkFolder($tref);
        $minfileloc = "../files/".$cmpcode."/".$tref."/".$minfilename;
        //UPLOAD UPLOADED FILE
    set_time_limit(180);
        copy($_FILES["minfile"]["tmp_name"], $minfileloc);
    set_time_limit(180);
            //UPDATE TRANSACTION LOG
            $tsql = "";
            //$tref = "MN";
            $trans = "Uploaded file for record ".$minid." - new file name is ".$minfilename.".";
            include("inc_transaction_log.php");
        //UPDATE CONTENT RECORD WITH FILENAME AND SET YN = Y
        $sql = "UPDATE assist_".$cmpcode."_".$moduledb."_content SET ";
        $sql .= "docfilename = '".$minfilename."', ";
        $sql .= "docyn = 'Y' WHERE docid = ".$minid;
        include("inc_db_con.php");
            //UPDATE TRANSACTION LOG
            $tsql = $sql;
            //$tref = "MN";
            $trans = "Updated record ".$minid.".";
            include("inc_transaction_log.php");
        $minresult = "Y";
    //}   //IF FILE TYPE IS NOT PDF
    }
    else    //if minid valid
    {
        $minresult = "N";
        echo("<h2>Error</h2><p>There was an error while trying to upload the file.  Please go back and try again.</p>");
        include("inc_goback_history.php");
    }
}   //IF ERROR WITH UPLOAD FILE
echo("<form name=mr><input type=hidden name=r value=".$minresult."><input type=hidden name=c value=".$mincateid."></form>");
?>
<script language=JavaScript>
var mr = document.mr.r.value;
if(mr == "Y")
{
    var c = document.mr.c.value;
    document.location.href = "update.php?c="+c;
}
</script>
</body>

</html>
