<?php
    include("inc_ignite.php");

$tid = $_POST['tkid'];
$cateid = $_POST['cateid'];

if(count($cateid)>0)    //IF CATEGORY DATA FROM THE FORM IS VALID
{
    if(strlen($tid) > 0)    //IF THE TK WAS SPECIFIED ON THE FORM
    {
        foreach($cateid as $cid)    //LOOP TROUGH EACH OF THE CATEGORIES SELECTED
        {
            //ADD USER VIEW ACCESS
            $sql = "INSERT INTO assist_".$cmpcode."_".$moduledb."_list_users SET tkid = '".$tid."', cateid = ".$cid.", yn = 'Y'";
            include("inc_db_con.php");
                //CREATE TRANSACTION LOG
                $tsql = $sql;
                //$tref = "MN";
                $trans = "Added user access to category ".$cid;
                include("inc_transaction_log.php");
        }   //END LOOP
    }   //END IF TK SUBMITTED
}
else    //IF CATEGORY DATA WAS SUBMITTED
{
    if(strlen($tid) > 0)    //IF TK WAS SUBMITTED AND NO CATEGORY DATA WAS RECEIVED DISPLAY ERROR MESSAGE
    {
        $result = "<p><b>ERROR:</b> No categories were selected.  The user was not added.</p>";
    }   //END IF TK SUBMITTED
}   //END IF CATEGORY DATA SUBMITTED


    include("inc_admin.php");
    //GET ADMINISTRATOR DETAILS PER CATEGORIES
    $sql = "SELECT count(cateid) as cc, catematkid FROM assist_".$cmpcode."_".$moduledb."_categories WHERE cateyn = 'Y' group by catematkid";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $tkcate[$row['catematkid']] = $row['cc'];
        }
    mysql_close($con);
    
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var tkid = me.tkid.value;
    if(tkid == "X")
    {
        alert("Please select a user.");
    }
    else
    {
        return true;
    }
    return false;
}
function delUser(tid,tkn) {
    while(tkn.indexOf("_")>0)
    {
        tkn = tkn.replace("_"," ");
    }
    if(confirm("Are you sure you wish to remove all access to Reports Assist for "+tkn+"?")==true)
    {
        document.location.href = "setup_users_delete.php?t="+tid;
    }
}

function editUser(tid,tkn) {
    document.location.href = "setup_users_edit.php?t="+tid;
}
</script>

<link rel="stylesheet" href="/assist.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($moduletitle); ?>: Setup - Users</b></h1>
<?php echo($result); ?>
<table border=1 cellpadding=3 cellspacing=0 width=550>
    <tr>
        <td class=tdheader>User</td>
        <td class=tdheader>View Categories</td>
        <td class=tdheader>Category<br>Admin?</td>
        <td class=tdheader width=50>&nbsp;</td>
    </tr>
<?php
$sql = "SELECT DISTINCT t.tkid, t.tkname, t.tksurname, c.catetitle FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_".$moduledb."_list_users u, assist_".$cmpcode."_".$moduledb."_categories c ";
$sql.= "WHERE t.tkid = u.tkid AND u.cateid = c.cateid AND u.yn = 'Y' AND c.cateyn = 'Y' AND t.tkstatus = 1 ORDER BY t.tkname, t.tksurname, c.catetitle";
include("inc_db_con.php");
$r = mysql_num_rows($rs);
if($r > 0)
{
    $tk2 = "0000";
    while($row = mysql_fetch_array($rs))
    {
        $tk1 = $row['tkid'];
        if($tk1 != $tk2)
        {
            if($tk2 != "0000")
            {
?>
        </ul></td>
    <?php
    if(strlen($tkcate[$tk2]) == 0)
    {
        ?>
        <td class=tdgeneral valign=top>No</td>
        <td class=tdgeneral valign=top align=center><?php echo("<input type=button value=Edit onclick=\"editUser('t".$tk2."','".$tkn2."')\"> <input type=button value=Del onclick=\"delUser('t".$tk1."','".$tkn2."')\">");?></td>
        <?php
    }
    else
    {
        ?>
        <td class=tdgeneral valign=top>Yes*</td>
        <td class=tdgeneral valign=top align=center><?php echo("<input type=button value=Edit onclick=\"editUser('t".$tk2."','".$tkn2."')\">");?></td>
        <?php
    }
    ?>
    </tr>
    <?php
            }
    ?>
    <tr>
        <td class=tdgeneral valign=top><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
        <td class=tdgeneral><ul>
<?php
        }
        echo("<li>".$row['catetitle']."</li>");
        $tk2 = $tk1;
        $tkn = $row['tkname']." ".$row['tksurname'];
        $tkn2 = str_replace(" ","_",$tkn);
    }?>
        </ul></td>
    <?php
    if(strlen($tkcate[$tk2]) == 0)
    {
        ?>
        <td class=tdgeneral valign=top>No</td>
        <td class=tdgeneral valign=top align=center><?php echo("<input type=button value=Edit onclick=\"editUser('t".$tk2."','".$tkn2."')\"> <input type=button value=Del onclick=\"delUser('t".$tk1."','".$tkn2."')\">");?></td>
        <?php
    }
    else
    {
        ?>
        <td class=tdgeneral valign=top>Yes*</td>
        <td class=tdgeneral valign=top align=center><?php echo("<input type=button value=Edit onclick=\"editUser('t".$tk2."','".$tkn2."')\">");?></td>
        <?php
    }
    ?>
    </tr>
    <?php
}
mysql_close($con);

$sql = "SELECT t.tkid, t.tkname, t.tksurname ";
$sql.= "FROM assist_".$cmpcode."_menu_modules_users m, assist_".$cmpcode."_timekeep t ";
$sql.= "WHERE m.usrmodref = '".$tref."' ";
$sql.= "AND m.usrtkid = t.tkid ";
$sql.= "AND t.tkid NOT IN (SELECT tkid FROM assist_".$cmpcode."_".$moduledb."_list_users WHERE yn = 'Y') ";
$sql.= "ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
    $mu = mysql_num_rows($rs);
    if($mu > 0)
    {
        $u = 0;
        while($row = mysql_fetch_array($rs))
        {
            $user[$u] = $row;
            $u++;
        }
    }
mysql_close($con);
if($mu > 0)
{
    $sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories WHERE cateyn = 'Y' ORDER BY catetitle";
    include("inc_db_con.php");
        $c = mysql_num_rows($rs);
        if($c > 0)
        {
            $a = 0;
            while($row = mysql_fetch_array($rs))
            {
                $cate[$a] = $row;
                $a++;
            }
        }
    mysql_close($con);
    if($c > 0)
    {
?>
<form name=adduser action=setup_users.php method=post onsubmit="return Validate(this);">
    <tr>
        <td class=tdgeneral valign=top><select name=tkid><option select value=X>--- SELECT ---</option>
        <?php
            foreach($user as $row)
            {
                echo("<option value=\"".$row['tkid']."\">".$row['tkname']." ".$row['tksurname']."</option>");
            }
        ?></select>
        </td>
        <td class=tdgeneral colspan=2><select name=cateid[] size=<?php echo($c); ?> multiple>
        <?php
            foreach($cate as $row)
            {
                echo("<option value=".$row['cateid'].">".$row['catetitle']."</option>");
            }
        ?>
        </select><Br><i>To select multiple categories,<br>hold down the CTRL key while selecting.</i></td>
        <td class=tdgeneral valign=top align=center><input type=submit value=Add></td>
    </tr>
</form>
<?php
    }
}
?>
</table>
<p>* Please note that users who are Category Administrators can not<Br>be deleted until the categories are assigned to a different user.
<p>
<?php
$urlback = "setup.php";
include("inc_goback.php");


$helpfile = "../help/".$tref."_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}



?>
</body>

</html>
