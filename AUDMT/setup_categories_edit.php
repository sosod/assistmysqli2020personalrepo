<?php
    include("inc_ignite.php");

$cateid = $_GET['c'];
$catetitle = $_GET["catetitle"];
$catematkid = $_GET["catematkid"];

if(strlen($catetitle) > 0 && strlen($catematkid) > 0)  //IF DATA HAS BEEN SUBMITTED VIA THE FORM
{
    //CREATE NEW CATEGORY RECORD
    $sql = "UPDATE assist_".$cmpcode."_".$moduledb."_categories SET ";
    $sql.= "catetitle = '".$catetitle."', ";
    $sql.= "catematkid = '".$catematkid."', ";
    $sql.= "cateyn = 'Y' ";
    $sql.= "WHERE cateid = ".$cateid;
    include("inc_db_con.php");
        //LOG THE NEW CATEGORY
        $tsql = $sql;
        //$tref = "MN";
        $trans = "Updated category: ".$catetitle;
        include("inc_transaction_log.php");
    $sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_list_users WHERE cateid = ".$cateid." AND tkid = '".$catematkid."' AND yn = 'Y'";
    include("inc_db_con.php");
        $u = mysql_num_rows($rs);
    mysql_close($con);
    if($u == 0)
    {
        $sql = "INSERT INTO assist_".$cmpcode."_".$moduledb."_list_users SET tkid = '".$tkid."', cateid = ".$cateid.", yn = 'Y'";
        include("inc_db_con.php");
            //LOG THE NEW CATEGORY
            $tsql = $sql;
            //$tref = "MN";
            $trans = "Added user view access to updated category ".$cateid." due to change in administrator.";
            include("inc_transaction_log.php");
    }
    $editresult = "Y";
}
else
{
    $editresult = "N";
}

    include("inc_admin.php");
    
    
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {

    var ct = me.catetitle.value;
    var ma = me.catematkid.value;
    
    if(ct.length == 0)
    {
        alert("Please enter a category title.");
    }
    else
    {
        if(ma == "X")
        {
            alert("Please select the administrator for this category.");
        }
        else
        {
            return true;
        }
    }
    return false;
}
</script>

<link rel="stylesheet" href="/assist.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b><?php echo($moduletitle); ?>: Setup - Categories</b></h1>
<form name=editcate action=setup_categories_edit.php method=get onsubmit="return Validate(this);">
<input type=hidden name=c value=<?php echo($cateid); ?>>
<input type=hidden name=r value=<?php echo($editresult); ?>>
<table border=1 cellpadding=3 cellspacing=0 width=550>
    <tr>
        <td class=tdheader>Ref</td>
        <td class=tdheader>Category</td>
        <td class=tdheader>Admin User</td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$moduledb."_categories WHERE cateid = ".$cateid." ORDER BY catetitle";
include("inc_db_con.php");
$r = mysql_num_rows($rs);
if($r > 0)
{
    $ccount = 0;
    while($row = mysql_fetch_array($rs))
    {
        $caterow[$ccount] = $row;
        $ccount++;
    }
}
mysql_close($con);

$ccount = 0;
if($r > 0)
{
    foreach($caterow as $cate)
    {
?>
    <tr>
        <td class=tdgeneral valign=top align=center><?php echo($cate['cateid']); ?></td>
        <td class=tdgeneral><input type=text name=catetitle size=30 value="<?php echo($cate['catetitle']); ?>"></td>
        <td class=tdgeneral><select name=catematkid><?php
                $sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users u WHERE t.tkstatus = 1 AND t.tkid <> '0000' AND u.usrtkid = t.tkid AND u.usrmodref = '".$tref."' ORDER BY t.tkname, t.tksurname";
                include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['tkid'];
                    $tk = $row['tkname']." ".$row['tksurname'];
                    if($id == $cate['catematkid'])
                    {
                        echo("<option selected value=".$id.">".$tk."</option>");
                    }
                    else
                    {
                        echo("<option value=".$id.">".$tk."</option>");
                    }
                }
                mysql_close();

/*                $sql = "SELECT count(t.tkid) as cc, t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_".$moduledb."_list_users u WHERE t.tkid = u.tkid AND u.yn = 'Y' AND t.tkstatus = 1 GROUP BY t.tkid, t.tkname, t.tksurname ORDER BY tkname, tksurname";
                include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        if($row['tkid'] == $cate['catematkid'])
                        {
                            echo("<option selected value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
                        }
                        else
                        {
                            echo("<option value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
                        }
                    }
                mysql_close();
  */          ?></td>
    </tr>
    <tr>
        <td class=tdgeneral colspan=3><input type=submit value="Save changes"></td>
    </tr>
<?php
    }
}
?>
</table>
</form>

<?php
$urlback = "setup_categories.php";
include("inc_goback.php");

$helpfile = "../help/".$tref."_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
<script language=JavaScript>
    var dr = document.editcate.r.value;
    if(dr == "Y")
    {
        document.location.href = "setup_categories.php";
    }
</script>
</body>

</html>
