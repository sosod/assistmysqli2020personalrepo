<script type="text/javascript">
  $(function()
  {
    $("#setup-nav").buttonset();
    $("#setup-nav input[type='radio']").click( function()
    {
      redirect( $(this).val() );
    });
  });
</script>
<div id="setup-nav" class="subNav"><input type="radio" id="nav1" name="radio" value="setup_users.php" <?php if( $section == 1 ){ ?>checked="checked"<?php } ?> /><label for="nav1">&nbsp;&nbsp;Users&nbsp;&nbsp;</label><input type="radio" id="nav2" name="radio" value="setup_risks.php" <?php if( $section == 2 ){ ?>checked="checked"<?php } ?> /><label for="nav2">&nbsp;&nbsp;Risks&nbsp;&nbsp;</label></div>