<?php
  include("inc_ignite.php");
  include("inc_header.php");
?>
<h1>Reports &amp; Graphs</h1>

<table cellspacing="1" cellpadding="0" class="default zebra">
  <thead>
    <tr>
      <th>&nbsp;</th>
      <th>Name</th>
      <th>Description</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><img src="images/icon-pdf.gif" alt="PDF Report" /></td>
      <td>Query Report</td>
      <td>Download all the query data in A3 PDF format.</td>
      <td><?= View::link_button( "reports_queries_pdf.php", "View" ) ?></td>
    </tr>
    <tr>
      <td><img src="images/icon-report.png" alt="Report" /></td>
      <td>Query Data</td>
      <td>Search for and download the query data in PDF format.</td>
      <td><?= View::link_button( "reports_queries_all.php", "View" ) ?></td>
    </tr>
    <tr>
      <td><img src="images/icon-report.png" alt="Report" /></td>
      <td>Queries Overdue</td>
      <td>View the queries that have not been completed by the deadline.</td>
      <td><?= View::link_button( "reports_queries_overdue.php", "View" ) ?></td>
    </tr>
    <tr>
      <td><img src="images/icon-report.png" alt="Report" /></td>
      <td>Actions Overdue</td>
      <td>View the actions that have not been completed by the deadline.</td>
      <td><?= View::link_button( "reports_actions_overdue.php", "View" ) ?></td>
    </tr>
    <!-- <tr>
      <td><img src="images/icon-report.png" alt="Report" /></td>
      <td>Response Time</td>
      <td>View the response time within a specified timeframe.</td>
      <td><?= View::link_button( "reports_queries_response.php", "View" ) ?></td>
    </tr> -->
    <tr>
      <td><img src="images/icon-graph.png" alt="Chart" /></td>
      <td>Query Status</td>
      <td>View the number of queries in each status.</td>
      <td><?= View::link_button( "reports_queries_status.php", "View" ) ?></td>
    </tr>
    <tr>
      <td><img src="images/icon-graph.png" alt="Chart" /></td>
      <td>Queries Per Risk</td>
      <td>View the number of queries per risk.</td>
      <td><?= View::link_button( "reports_queries_per_risk.php", "View" ) ?></td>
    </tr>
    <tr>
      <td><img src="images/icon-graph.png" alt="Chart" /></td>
      <td>Queries Per Department</td>
      <td>View the number of queries per department.</td>
      <td><?= View::link_button( "reports_queries_per_department.php", "View" ) ?></td>
    </tr>
  </tbody>
</table>

<?php
  include("inc_footer.php");
?>