<?php
  include("inc_ignite.php");

  $department_id = $_POST["department_id"];
  $start_date = $_POST["start_date"];
  $end_date = $_POST["end_date"];

  include("inc_header.php");
?>
<script type="text/javascript" src="lib/swfobject.js"></script>
<h1>Reports &amp; Graphs &raquo; Queries per risk</h1>

<form action="reports_queries_per_risk.php" method="post" class="filter">
  <fieldset>
    <?php View::label( "fldDepartment", "Department" ) ?><?php View::select( "fldDepartment", "department_id", $department_id, View::get_department_select_list(), "All" ) ?>
    <?php View::label( "fldStartDate", "Start Date" ) ?><?php View::text( "fldStartDate", "start_date", ( exists( $start_date ) ? $start_date : "" ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input" ) ) ?>
    <?php View::label( "fldEndDate", "End Date" ) ?><?php View::text( "fldEndDate", "end_date", ( exists( $end_date ) ? $end_date : "" ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input" ) ) ?>
    <?php View::hidden( "action", View::$ACTION_FILTER ) ?>
    <?php View::submit( "Apply filter", true ) ?>
  </fieldset>
</form>

<div id="flashcontent">
  <strong>You need to upgrade your Flash Player</strong>
</div>

<script type="text/javascript">
  var so = new SWFObject("charts/amcolumn/amcolumn.swf", "ampcolumn", "950", "500", "8", "#FFFFFF");
  so.addVariable("path", "charts/amcolumn/");
  so.addVariable("chart_id", "amcolumn"); // if you have more then one chart in one page, set different chart_id for each chart
  so.addVariable("settings_file", encodeURIComponent("charts/amcolumn/amcolumn_settings.xml?v=1.00") );
  so.addVariable("data_file", encodeURIComponent("reports_queries_per_risk_data.xml.php?department_id=<?= $department_id ?>&start_date=<?= $start_date ?>&end_date=<?= $end_date ?>"));
  so.addVariable("preloader_color", "#999999");
  so.addParam("wmode", "opaque");
  so.write("flashcontent");
</script>

<?php
  include("inc_footer.php");
?>