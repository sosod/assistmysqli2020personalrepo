<?php
  include("inc_ignite.php");

  $risk = new Risk();
  $risks = $risk->get_records();

  $department_id = $_GET["department_id"];
  $start_date = $_GET["start_date"];
  $end_date = $_GET["end_date"];

  $sql = "SELECT q.risk_id AS risk, COUNT(*) AS cnt FROM assist_" . get_company_code() . "_aqa_assignment a
          JOIN assist_" . get_company_code() . "_aqa_query q on q.id = a.query_id ";
  $has_where = false;

  if( exists( $department_id ) )
  {
    $sql .= ( $has_where ? "AND" : "WHERE" ) . " a.department_id = " . $department_id . " ";
    $has_where = true;
  }

  if( exists( $start_date ) )
  {
    $sql .= ( $has_where ? "AND" : "WHERE" ) . " a.created_on > '" . $start_date . " 00:00:00' ";
    $has_where = true;
  }

  if( exists( $end_date ) )
  {
    $sql .= ( $has_where ? "AND" : "WHERE" ) . " a.created_on < '" . $end_date . " 23:59:59' ";
    $has_where = true;
  }

  $xml  = "<?xml version='1.0' encoding='UTF-8'?><chart><series>";

  $xml .= "<value xid='-1'>No Risk</value>";

  foreach( $risks as $risk )
  {
    $xml .= "<value xid='" . $risk->id . "'>" . $risk->name . "</value>";
  }

  $xml .= "</series><graphs>";

  $statusses = array( "New"=>"New", "Responded"=>"Responded", "In-progress"=>"In-progress", "Completed"=>"Completed", "Closed"=>"Closed" );

  foreach( $statusses as $status )
  {
    $tmp_sql = $sql;
    $tmp_sql .= ( $has_where ? "AND" : "WHERE" ) . " a.status = '" . $status . "' ";
    $tmp_sql .= " GROUP BY q.risk_id";

    $records = query_records( $tmp_sql );

    $xml .= "<graph gid='" . $status . "' title='" . $status . "'>";

    foreach( $records as $record )
    {
      $risk_id = ( $record["risk"] == null ? "-1" : $record["risk"] );

      $xml .= "<value xid='" . $risk_id . "'>" . $record["cnt"] . "</value>";
    }

    $xml .= "</graph>";
  }

  $xml .= "</graphs></chart>";

  echo $xml;
?>
