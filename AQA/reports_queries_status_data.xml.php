<?php
  include("inc_ignite.php");

  $department_id = $_GET["department_id"];
  $start_date = $_GET["start_date"];
  $end_date = $_GET["end_date"];

  $sql = "SELECT a.status, COUNT(*) AS cnt FROM assist_" . get_company_code() . "_aqa_assignment a ";
  $has_where = false;

  if( exists( $department_id ) )
  {
    $sql .= ( $has_where ? "AND" : "WHERE" ) . " a.department_id = " . $department_id . " ";
    $has_where = true;
  }

  if( exists( $start_date ) )
  {
    $sql .= ( $has_where ? "AND" : "WHERE" ) . " a.created_on > '" . $start_date . " 00:00:00' ";
    $has_where = true;
  }

  if( exists( $end_date ) )
  {
    $sql .= ( $has_where ? "AND" : "WHERE" ) . " a.created_on < '" . $end_date . " 23:59:59' ";
    $has_where = true;
  }

  $sql .= " GROUP BY status";

  $records = query_records( $sql );

  $xml  = "<?xml version='1.0' encoding='UTF-8'?><pie>";

  $total = 0;

  foreach( $records as $data )
  {
    $xml .= "<slice title='" . $data["status"] . "'>" . $data["cnt"] . "</slice>";
    $total += (int)$data["cnt"];
  }

  $xml .= "
           <labels>
             <label lid='0'>
               <y>10</y>
               <align>center</align>
               <text_size>12</text_size>
               <text>
               <![CDATA[<b>Total number of queries: " . $total . "</b>]]>
               </text>
             </label>
           </labels></pie>";

  echo $xml;
?>