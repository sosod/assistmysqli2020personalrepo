<?php
  include("inc_ignite.php");

  if( get_logged_in_user()->has_access( "query" ) == false )
  {
    View::redirect( "access_denied.php" );
  }

  $section = $_REQUEST["section"];

  if( $_POST["action"] == View::$ACTION_SAVE )
  {
    $assignment = new Assignment( $_POST["id"] );
    $assignment->init( $_POST );

    if( $assignment->validate() )
    {
      if( $assignment->save() )
      {
        if( exists( $_POST["current_assigned_to_id"] ) &&
            $_POST["current_assigned_to_id"] != $assignment->assigned_to_id )
        {
          EmailHelper::send_query_assignment( $assignment );
        }

        View::set_session_message( "assignment", "Assignment successfully saved." );
        View::redirect( "assignment_info.php?id=" . $assignment->id . "&section=" . $section );
      }
      else
      {
        View::show_db_error( $assignment->get_sql() );
      }
    }
  }
  else
  {
    $assignment = new Assignment( $_GET["id"] );
  }
?>

<?
  include("inc_header.php");
  include("inc_nav_query.php");
?>

<script type="text/javascript">
  var user_dept_json = <?php View::user_dept_json() ?>;
</script>
<script type="text/javascript" src="lib/assignment.js"></script>

<h1><?= View::get_section_header( $section ) ?> &raquo; Edit Assignment</h1>

<div class="actions">
  <?= View::back_button( $section, "info", $assignment->id ) ?>
</div>

<form action="" method="post" class="default">
  <div>
    <?php View::label( "fldUser", "User", true ) ?>
    <?php View::text( "fldUser", "user", $assignment->get_assigned_to()->get_full_name(), array( "class"=>"autocomplete", "size"=>"25" ) ) ?>
    <span id="fldUserStatus" class="user-status-inline <?= exists( $assignment->assigned_to_id ) ? "ui-button-icon-primary ui-icon ui-icon-check" : "" ?>"></span>
    <?php View::hidden( "assigned_to_id", $assignment->assigned_to_id, array( "id"=>"fldUserId" ) ) ?>
    <?php View::field_error( $assignment->get_field_error( "assigned_to_id" ) ) ?>
  </div>
  <div>
    <?php View::label( "fldDepartment", "Department", true ) ?>
    <?php View::text( "fldDepartment", "department", $assignment->get_department()->value, array( "readonly"=>"readonly" ) ) ?>
    <?php View::hidden( "department_id", $assignment->department_id, array( "id"=>"fldDepartmentId" ) ) ?>
    <?php View::field_error( $assignment->get_field_error( "department_id" ) ) ?>
  </div>
  <div>
    <?php View::label( "fldDeadline", "Deadline", true ) ?>
    <?php View::text( "fldDeadline", "deadline", ( exists( $assignment->deadline ) ? $assignment->deadline : View::get_current_date() ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input", "readonly"=>"readonly" ) ) ?>
    <?php View::field_error( $assignment->get_field_error( "deadline" ) ) ?>
  </div>
  <?php View::hidden( "action", View::$ACTION_SAVE ) ?>
  <?php View::hidden( "id", $assignment->id ) ?>
  <?php View::hidden( "current_assigned_to_id", $assignment->assigned_to_id ) ?>
  <?php View::hidden( "section", $section ) ?>
  <?php View::submit( "Save" ) ?><?php View::section_cancel( $section, "info", $assignment->id ) ?>
</form>

<?php
  include("inc_footer.php");
?>