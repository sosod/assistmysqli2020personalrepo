<?php
  include("inc_ignite.php");
  include("inc_header.php");
?>
<div class="ui-state-error ui-corner-all" style="padding:0 0.7em;margin:2em;">
  <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> <strong>Access Denied</strong></p>
  <p>You do not have access to the requested area.</p>
</div>
<?php
  include("inc_footer.php");
?>