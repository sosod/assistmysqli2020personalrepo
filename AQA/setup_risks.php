<?php
  include("inc_ignite.php");

  if( get_logged_in_user()->has_access( "setup" ) == false )
  {
    View::redirect( "access_denied.php" );
  }

  include("inc_header.php");
?>
<?php
  $section = 2;
  include("inc_nav_setup.php");
?>

<h1>Setup &raquo; Risks</h1>

<?php View::get_session_message( "risk" ) ?>

<div class="actions">
  <?= View::link_button( "setup_risk.php", "New risk" ) ?>
</div>

<table cellspacing="1" cellpadding="0" class="default zebra">
  <thead>
    <tr>
      <th>Risk</th>
      <th>Status</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $risk = new Risk();
  $records = $risk->get_records();

  if( count( $records ) > 0 )
  {
    foreach( $records as $obj )
    {
  ?>
    <tr>
      <td><?= $obj->name ?></td>
      <td><?= $obj->status ?></td>
      <td><?= View::link_button( "setup_risk.php?id=" . $obj->id, "Edit" ) ?></td>
    </tr>
  <?php
    }
  }
  else
  {
  ?>
    <tr><td colspan="3">No records found</td></tr>
  <?php
  }
  ?>
  </tbody>
</table>

<?php
  include("inc_footer.php");
?>