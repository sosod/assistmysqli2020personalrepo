<?php
include("inc_ignite.php");

$action_id = $_REQUEST["action_id"];
$action = new Action( $action_id );
$action_updates = $action->get_updates();

?>
<div id="infoWindow">
  <h2>Action Information</h2>
  <table cellspacing="1" cellpadding="0" class="info">
    <tr>
      <th>Action #</th>
      <td><?= $action->number ?></td>
    </tr>
    <tr>
      <th>Instruction</th>
      <td><?= $action->instruction ?></td>
    </tr>
    <tr>
      <th>Assigned To</th>
      <td><?= $action->get_assigned_to()->get_full_name() ?></td>
    </tr>
    <tr>
      <th>Deadline</th>
      <td><?= $action->deadline ?></td>
    </tr>
    <tr>
      <th>Remind On</th>
      <td><?= $action->remind_on ?></td>
    </tr>
    <tr>
      <th>Progress</th>
      <td><?= $action->progress ?>%</td>
    </tr>
    <tr>
      <th>Status</th>
      <td><?= $action->status ?> <em>(<?= exists( $action->last_updated_on ) ? $action->last_updated_on : $action->created_on ?>)</em></td>
    </tr>
  </table>

  <h2>Updates</h2>
  <table cellspacing="1" cellpadding="0" class="default zebra">
    <thead>
      <tr>
        <th>Updated on</th>
        <th>Description</th>
        <th>Progress</th>
        <th>Attachment</th>
      </tr>
    </thead>
    <tbody>
    <?php
    if( count( $action_updates ) > 0 )
    {
      foreach( $action_updates as $obj )
      {
    ?>
      <tr>
        <td><?= $obj->created_on ?></td>
        <td><?= $obj->description ?></td>
        <td><?= $obj->progress ?>%</td>
        <?php if( exists( $obj->attachment ) ){ ?>
        <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $obj->attachment ?>">Download attachment</a></td>
        <?php }else{ ?>
        <td></td>
        <?php } ?>
      </tr>
    <?php
      }
    }
    else
    {
    ?>
      <tr><td colspan="4">No updates have been made to this action.</td></tr>
    <?php
    }
    ?>
    </tbody>
  </table>
</div>