<?php
function __autoload($class_name)
{
  require_once "classes/" . $class_name . ".class.php";
}

require_once '../inc_session.php';
$logfile = "../logs/error_".$cmpcode.".txt";

include("inc_aqa_util.php");
include("inc_aqa_db.php");

/* LOAD THE LOGGED IN USER'S SETUP */
if( get_logged_in_user()->is_admin_user() == false )
{
  if( $skip_user_setup_check == false && ( exists( get_logged_in_user()->get_setup() ) == false || get_logged_in_user()->get_setup()->is_transient() ) )
  {
    View::redirect( "error.php?code=1" );
  }
}
?>