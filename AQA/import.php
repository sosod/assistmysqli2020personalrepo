<?php
  $skip_user_setup_check = true;
  include("inc_ignite.php");

  $tmp_conn = mysql_connect( "localhost","ignittps_ignite4","ign92054u" );
  mysql_select_db( "ignittps_imos0001", $tmp_conn );

  $sql = "select * from assist_mos0001_audit_action";

  $rs = mysql_query( $sql, $tmp_conn );

  echo "START<br/>";

  while( $record = mysql_fetch_array( $rs, MYSQL_ASSOC ) )
  {
    $query = new Query();
    $query->audit_type = "Internal";
    $query->audit_ref = $record["actref"];
    $query->audit_date = date("Y-m-d", $record["actactiondate"]);
    $query->assignment_information = $record["acttext3"];
    $query->audit_finding = $record["acttext1"];
    $query->audit_recommendation = $record["acttext2"];
    $query->created_by_id = $record["actadduser"];
    $query->created_on = date("Y-m-d", $record["actadddate"]);

    if( $query->insert() == false )
    {
      View::show_db_error( $query->get_sql() );
      exit;
    }

    $assignment = new Assignment();
    $assignment->query_id = $query->id;
    $assignment->number = 1;
    $assignment->assigned_to_id = $record["actassignedto"];
    $assignment->department_id = get_department_id( $tmp_conn, $assignment->assigned_to_id );
    $assignment->deadline = date("Y-m-d", $record["actdeadline"]);
    $assignment->status = convert_status( $record["actstatusid"] );

    $last_log = get_last_log( $tmp_conn, $record["actid"] );

    if( $assignment->status == "Completed" )
    {
      $assignment->response = $last_log["logupdate"];
      $assignment->responded_on = date("Y-m-d", $last_log["logdate"] );
      $assignment->completed_on = date("Y-m-d", $last_log["logdate"] );
    }

    $assignment->created_by_id = $query->created_by_id;
    $assignment->created_on = $query->created_on;

    if( $assignment->insert() == false )
    {
      View::show_db_error( $assignment->get_sql() );
      exit;
    }

    $sql_logs = "select * from assist_mos0001_audit_log where logactid = " . $record["actid"] . " order by logdate asc";
    $rs_logs = mysql_query( $sql_logs, $tmp_conn );

    $num_of_logs = mysql_num_rows( $rs_logs );

    if( $num_of_logs > 1 )
    {
      $action = new Action();
      $action->assignment_id = $assignment->id;
      $action->number = 1;
      $action->instruction = $query->audit_recommendation;
      $action->assigned_to_id = $assignment->assigned_to_id;
      $action->deadline = $assignment->deadline;
      $action->remind_on = $action->deadline;
      $action->status = convert_status( $record["actstatusid"] );
      $action->progress = $last_log["logstate"];

      if( $action->status == "Completed" )
      {
        $action->completed_on = date("Y-m-d", $last_log["logdate"] );
      }

      $action->created_by_id = $assignment->created_by_id;
      $action->created_on = $assignment->created_on;

      if( $action->insert() == false )
      {
        View::show_db_error( $action->get_sql() );
        exit;
      }

      $cnt = 0;
      while( $record = mysql_fetch_array( $rs_logs, MYSQL_ASSOC ) )
      {
        if( $cnt != 0 )
        {
          $action_update = new ActionUpdate();
          $action_update->action_id = $action->id;
          $action_update->progress = clean_up_progress( $record["logstate"] );
          $action_update->description = $record["logupdate"];
          $action_update->created_by_id = $record["logupdatetkid"];
          $action_update->created_on = date("Y-m-d", $record["logdate"]);

          if( $action_update->insert() == false )
          {
            View::show_db_error( $action_update->get_sql() );
            exit;
          }
        }

        $cnt++;
      }
    }
  }

  echo "FINISHED";

  function get_department_id( $tmp_conn, $user_id )
  {
    $sql = "select deptid from assist_mos0001_audit_list_users where tkid = '" . $user_id . "'";
    $rs = mysql_query( $sql, $tmp_conn );
    $record = mysql_fetch_array( $rs, MYSQL_ASSOC );

    return $record["deptid"];
  }

  function convert_status( $status_id )
  {
    if( $status_id == "NW" )
    {
      return "New";
    }
    else if( $status_id == "IP" )
    {
      return "In-progress";
    }
    else if( $status_id == "CL" )
    {
      return "Completed";
    }
  }

  function get_last_log( $tmp_conn, $action_id )
  {
    $sql = "select * from assist_mos0001_audit_log where logactid = '" . $action_id . "' order by logdate desc limit 1";

    $rs = mysql_query( $sql, $tmp_conn );
    $record = mysql_fetch_array( $rs, MYSQL_ASSOC );

    return $record;
  }

  function clean_up_progress( $progress )
  {
    if( $progress < 0 )
    {
      return 0;
    }

    return $progress;
  }

?>