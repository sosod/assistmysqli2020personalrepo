<?
function debug( $msg )
{
  /*$fd = fopen( "C:\Users\Home\Dev\WWW\assist.ignite4u.co.za\logs\AQA.log", "a" );
  $str = "[".date( "Y/m/d h:i:s", mktime() )."] ".$msg;
  fwrite( $fd, $str."\n" );
  fclose( $fd );*/
}

/*********** VIEW HELPER FUNCTIONS **********/
function exists( $value )
{
  if( isset( $value ) )
  {
    if( is_string( $value ) )
    {
      $value = trim( $value );

      if( strlen( $value ) != 0 )
      {
        return true;
      }
    }
    else
    {
      return true;
    }
  }

  return false;
}

function get_param( $name )
{
  return $_REQUEST[ $name ];
}

function get_sql_param( $name, $default = false )
{
	if( isset( $_REQUEST[ $name ] ) == false )
	{
		if( $default !== false )
		{
		  return $default;
		}
		else
		{
			return null;
		}
	}

  return mysql_real_escape_string( $_REQUEST[ $name ] );
}

function object_to_array( $var )
{
  $result = array();
  $references = array();

  // loop over elements/properties
  foreach( $var as $key => $value )
  {
    // recursively convert objects
    if( is_object( $value ) || is_array( $value ) )
    {
      // but prevent cycles
      if( !in_array( $value, $references ) )
      {
        $result[$key] = object_to_array( $value );
        $references[] = $value;
      }
    }
    else
    {
      // simple values are untouched
      $result[$key] = $value;
    }
  }

  return $result;
}
?>