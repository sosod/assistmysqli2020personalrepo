<?php
  include("inc_ignite.php");
?>
<?php
  $pager = unserialize( $_SESSION["aqa_reports_queries_all_pager"] );

  if( isset( $pager ) == false || $pager == false )
  {
    $pager = new PagerBean();
  }

  if( exists( $_REQUEST["page"] ) )
  {
    $pager->page = $_REQUEST["page"];
  }

  $_SESSION["aqa_reports_queries_all_pager"] = serialize( $pager );

  $filter = unserialize( $_SESSION["aqa_reports_queries_all_filter"] );

  if( isset( $filter ) == false || $filter == false )
  {
    $filter = new QueryFilterBean();
  }

  if( $_POST["action"] == View::$ACTION_FILTER )
  {
    $filter->init( $_POST );

    if( isset( $_POST["pdf"] ) )
    {
      PDFGenerator::generate_queries_all_report( $filter, $_POST["query_detail"] );
      exit;
    }
    else if( isset( $_POST["csv"] ) )
    {
      $file = CSVGenerator::generate_queries_all_report( $filter );

      View::CSV_Redirect( $file );

      exit;
    }
  }

  $_SESSION["aqa_reports_queries_all_filter"] = serialize( $filter );

  include("inc_header.php");
?>

<h1>Reports &amp; Graphs &raquo; Query Data</h1>

<form action="reports_queries_all.php" method="post" class="filter">
  <fieldset>
    <?php View::label( "fldSearch", "Search" ) ?><?php View::text( "fldSearch", "search", $filter->search ) ?>
    <?php View::label( "fldAuditType", "Audit Type" ) ?><?php View::select( "fldAuditType", "audit_type", $filter->audit_type, View::get_audit_type_select_list(), "All" ) ?>
    <?php View::label( "fldAuditDate", "Audit Date" ) ?><?php View::text( "fldAuditDate", "audit_date", ( exists( $filter->audit_date ) ? $filter->audit_date : "" ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input" ) ) ?>
    <?php View::label( "fldFinancialYear", "Financial Year" ) ?><?php View::select( "fldFinancialYear", "financial_year", $filter->financial_year, View::get_financial_year_select_list(), "All" ) ?>
    <?php View::label( "fldRiskLevel", "Risk Level" ) ?><?php View::select( "fldRiskLevel", "risk_level", $filter->risk_level, View::get_risk_level_select_list(), "All" ) ?>
  </fieldset>
    <fieldset>
        <?php View::label( "fldRisk", "Risk" ) ?><?php View::select( "fldRisk", "risk_id", $filter->risk_id, View::get_risk_select_list(), "All" ) ?>
    </fieldset>
  <fieldset>
    <?php View::label( "fldUser", "Assigned To" ) ?><?php View::select( "fldUser", "user_id", $filter->user_id, View::get_user_select_list(), "All" ) ?>
    <?php View::label( "fldDepartment", "Department" ) ?><?php View::select( "fldDepartment", "department_id", $filter->department_id, View::get_department_select_list(), "All" ) ?>
    <?php View::label( "fldDeadline", "Deadline" ) ?><?php View::text( "fldDeadline", "deadline", ( exists( $filter->deadline ) ? $filter->deadline : "" ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input" ) ) ?>
    <?php View::label( "fldStatus", "Status" ) ?><?php View::select( "fldStatus", "status", $filter->status, View::get_assignment_status_select_list(), "All" ) ?>
    <?php View::label( "fldAssignmentInformation", "Subject" ) ?><?php View::text( "fldAssignmentInformation", "assignment_information", $filter->assignment_information ) ?>
    <?php View::hidden( "action", View::$ACTION_FILTER ) ?>
    <?php View::hidden( "page", "1" ) ?>
  </fieldset>
  <fieldset>
    <?php View::label( "fldStartDate", "Start Date" ) ?><?php View::text( "fldStartDate", "start_date",  $filter->start_date  , array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input" ) ) ?>
    <?php View::label( "fldEndDate", "End Date" ) ?><?php View::text( "fldEndDate", "end_date", $filter->end_date, array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input" ) ) ?>
    <?php View::label( "fldpreviouslyqueried", "Previously Queried", false, true ) ?><?php View::boolean_checkbox( "fldpreviouslyqueried", "previously_queried", 0 ) ?>
    <?php View::submit( "Apply filter", true ) ?>
  </fieldset>
  <fieldset>
    <?php View::submit( "Create CSV", true, "csv" ) ?>
    <?php View::submit( "Create PDF", true, "pdf" ) ?>
    <?php View::label( "fldReportHeader", "Report Header" ) ?><?php View::Text( "fldReportHeader", "report_header", $filter->report_header ) ?>
    <?php View::boolean_checkbox( "fldQueryDetail", "query_detail", 0 ) ?>
    <?php View::label( "fldQueryDetail", "Include query detail in PDF report", false, true ) ?>
  </fieldset>
</form>

<table cellspacing="1" cellpadding="0" class="default zebra">
  <thead>
    <tr>
      <th>Audit Ref</th>
      <th>Audit Type</th>
      <th>Subject</th>
      <th>Audit Date</th>
      <th>Previously Queried</th>
      <th>Risk Level</th>
      <th>Risk</th>
      <th>Assignment #</th>
      <th>Assigned To</th>
      <th>Deadline</th>
      <th>Status</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $records = QueryHelper::get_query_data( $filter, $pager );

  if( count( $records ) > 0 )
  {
    foreach( $records as $row )
    {
  ?>
    <tr>
      <td><?= $row["audit_ref"] ?></td>
      <td><?= $row["audit_type"] ?></td>
      <td><?= $row["assignment_information"] ?></td>
      <td><?= $row["audit_date"] ?></td>
      <td><?= View::display_bool($row["previously_queried"]) ?></td>
      <td><?= $row["risk_level"] ?></td>
      <td><?= $row["risk"] ?></td>
      <td><?= $row["number"] ?></td>
      <td><?= $row["user"] ?></td>
      <td><?= $row["deadline"] ?></td>
      <td><?= $row["status"] ?></td>
      <td><a href="ajax_assignment_info.php?assignment_id=<?= $row["id"] ?>" rel="facebox"><img src="images/icon-view.png" alt="View" title="View"/></a></td>
    </tr>
  <?php
    }
  }
  else
  {
  ?>
    <tr><td colspan="9">There are no queries matching your criteria.</td></tr>
  <?php
  }
  ?>
  </tbody>
</table>

<?php
if( count( $records ) > 0 )
{
?>
<div class="pager">
  <span class="overview">Page <?= $pager->page ?> of <?= $pager->total_pages ?></span>

  <? if( $pager->bottom_range > 1 ){?>
  <span class="extend">...</span>
  <? } ?>

  <? if( $pager->page > 1 ){?>
  <a class="page prev" href="?page=<?= $pager->page-1 > 0 ? $pager->page-1 : 1 ?>">&laquo;</a>
  <? } ?>

  <? for( $i = $pager->bottom_range; $i < $pager->top_range+1; $i++ ){ ?>
    <? if( $pager->page == $i ){ ?>
    <span class="page current"><?= $i ?></span>
    <? }else{ ?>
    <a class="page" href="?page=<?= $i ?>"><?= $i ?></a>
    <? } ?>
  <? } ?>

  <? if( $pager->top_range < $pager->total_pages ){?>
  <span class="extend">...</span>
  <? } ?>

  <? if( $pager->page < $pager->total_pages ){?>
  <a class="page next" href="?page=<?= $pager->page+1 ?>">&raquo;</a>
  <? } ?>
</div>
<?php
}
?>

<?php
  include("inc_footer.php");
?>