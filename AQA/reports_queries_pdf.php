<?php
  include("inc_ignite.php");
?>
<?php
  $filter = unserialize( $_SESSION["aqa_reports_query_filter"] );

  if( isset( $filter ) == false || $filter == false )
  {
    $filter = new QueryFilterBean();
  }

  if( $_POST["action"] == View::$ACTION_FILTER )
  {
    $filter->init( $_POST );
    PDFGenerator::generate_query_report( $filter, $_POST["assignment_data"], $_POST["action_data"], $_POST["action_update_data"] );
    exit;
  }

  $_SESSION["aqa_reports_query_filter"] = serialize( $filter );

  include("inc_header.php");
?>

<h1>Reports &amp; Graphs &raquo; Query Report</h1>

<form action="reports_queries_pdf.php" method="post" class="filter">
  <fieldset>
    <?php View::label( "fldAuditType", "Audit Type" ) ?><?php View::select( "fldAuditType", "audit_type", $filter->audit_type, View::get_audit_type_select_list(), "All" ) ?>
    <?php View::label( "fldFinancialYear", "Financial Year" ) ?><?php View::select( "fldFinancialYear", "financial_year", $filter->financial_year, View::get_financial_year_select_list(), "All" ) ?>
    <?php View::label( "fldRiskLevel", "Risk Level" ) ?><?php View::select( "fldRiskLevel", "risk_level", $filter->risk_level, View::get_risk_level_select_list(), "All" ) ?>
    <?php View::label( "fldRisk", "Risk" ) ?><?php View::select( "fldRisk", "risk_id", $filter->risk_id, View::get_risk_select_list(), "All" ) ?>
    <?php View::label( "fldpreviouslyqueried", "Previously Queried", false, true ) ?><?php View::boolean_checkbox( "fldpreviouslyqueried", "previously_queried", 0 ) ?>
  </fieldset>
  <fieldset>
    <?php View::label( "fldUser", "Assigned To" ) ?><?php View::select( "fldUser", "user_id", $filter->user_id, View::get_user_select_list(), "All" ) ?>
    <?php View::label( "fldDepartment", "Department" ) ?><?php View::select( "fldDepartment", "department_id", $filter->department_id, View::get_department_select_list(), "All" ) ?>
    <?php View::label( "fldStatus", "Status" ) ?><?php View::select( "fldStatus", "status", $filter->status, View::get_assignment_status_select_list(), "All" ) ?>
    <?php View::label( "fldAssignmentInformation", "Subject" ) ?><?php View::Text( "fldAssignmentInformation", "assignment_information", $filter->assignment_information ) ?>
    <?php View::hidden( "action", View::$ACTION_FILTER ) ?>
    <?php View::hidden( "page", "1" ) ?>
  </fieldset>
  <fieldset>
    <?php View::boolean_checkbox( "fldAssignmentData", "assignment_data", 0 ) ?>
    <?php View::label( "fldAssignmentData", "Include assignment data", false, true ) ?>
    <br/>
    <?php View::boolean_checkbox( "fldActionData", "action_data", 0 ) ?>
    <?php View::label( "fldActionData", "Include action data", false, true ) ?>
    <br/>
    <?php View::boolean_checkbox( "fldActionUpdateData", "action_update_data", 0 ) ?>
    <?php View::label( "fldActionUpdateData", "Include action update data", false, true ) ?>
  </fieldset>
  <fieldset>
    <?php View::submit( "Create Report", true ) ?>
      <?php View::label( "fldReportHeader", "Report Header" ) ?><?php View::Text( "fldReportHeader", "report_header", $filter->report_header ) ?>
  </fieldset>
</form>

<?php
  include("inc_footer.php");
?>