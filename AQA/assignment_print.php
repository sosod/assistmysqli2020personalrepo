<?php
  include("inc_ignite.php");

  $assignment_id = $_GET["assignment_id"];

  if( isset( $assignment_id ) )
  {
    $assignment = new Assignment( $assignment_id );

    PDFGenerator::generate_assignment_printout( $assignment );
  }
?>