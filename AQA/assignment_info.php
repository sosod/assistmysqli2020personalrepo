<?php
  include("inc_ignite.php");

  $section = exists( $_REQUEST["section"] ) ? $_REQUEST["section"] : -1;

  if( $_POST["action"] == View::$ACTION_SAVE )
  {
    $sign_off = $_POST["sign_off"];
    $completed = $_POST["completed"];
    $complete_only = $_POST["complete_only"];
    $rejected = false;

    $assignment = new Assignment( $_POST["id"] );
    
    if( $sign_off )
    {
      $assignment->bln_follow_up = $_POST["bln_follow_up"];
      $assignment->follow_up = $_POST["follow_up"];
      $assignment->effective_controls = $_POST["effective_controls"];
      $assignment->closed_status = $_POST["closed_status"];
      $assignment->reject_reason = $_POST["reject_reason"];
      $assignment->risk_reference_number = $_POST["risk_reference_number"];
      $assignment->risk_status = $_POST["risk_status"];
      $assignment->further_actions = $_POST["further_actions"];
    }
    else if( $complete_only == false )
    {
      $assignment->response = $_POST["response"];
      $assignment->is_policy_sop = $_POST["is_policy_sop"];
      $assignment->sop_assistance = $_POST["sop_assistance"];
    } else if( $complete_only ) {
      $assignment->completed_on = $_POST["completed_on"];
    }

    if( $assignment->validate( true, $sign_off ) )
    {
      if( $sign_off )
      {
        $assignment->closed_by_id = get_logged_in_user_id();
        $assignment->closed_on = DBHelper::get_now();

        //if query is rejected it needs to go back to the user to deal with it
        if( $assignment->closed_status == "Rejected"  ){
          $rejected = true;
          $assignment->status = "In-progress";
        } else {
          $assignment->status = "Closed";
        }

      }
      else if( $complete_only )
      {
        $assignment->status = "Completed";        
      }
      else
      {
        $assignment->responded_on = DBHelper::get_now();

        if( $completed )
        {
          $assignment->status = "Completed";
          $assignment->completed_on = $_POST["completed_on"];
        }
        else
        {
          if( $assignment->is_new() )
          {
            $assignment->status = "Responded";
          }
        }
      }

      if( $assignment->save($rejected) )
      {
        if( $sign_off )
        {
          if( $assignment->closed_status == "Rejected"  ){
            EmailHelper::send_query_rejected( $assignment );
            View::set_session_message( "assignment", "Query rejected and sent back to user for further action." );
          } else {
            EmailHelper::send_query_signed_off( $assignment );
            View::set_session_message( "assignment", "Sign-off successfully saved." );
          }
        }
        else if( $complete_only )
        {
          View::set_session_message( "assignment", "Query successfully marked as completed." );
        }
        else
        {
          View::set_session_message( "assignment", "Response successfully saved." );
        }

        View::redirect( "assignment_info.php?id=" . $assignment->id . "&section=" . $section );
      }
      else
      {
        View::show_db_error( $assignment->get_sql() );
      }
    }
  }
  else
  {
    $assignment = new Assignment( $_GET["id"] );
  }

  $actions = $assignment->get_actions();

  include("inc_header.php");
  include("inc_nav_query.php");
?>

<script type="text/javascript">
    var numAttachments = 0;

    <? if(exists($assignment->counter)) { ?>
    numAttachments = <?= $assignment->counter ?>;
    <?}?>


</script>
<script type="text/javascript" src="lib/assignment_info.js"></script>

<h1><?= View::get_section_header( $section ) ?> &raquo; Assignment</h1>

<?php View::get_session_message( "assignment" ) ?>
<?php View::get_session_message( "action" ) ?>




<div class="actions">
  <?= View::back_button( $section ) ?> |
  <?= View::link_button( "assignment_print.php?assignment_id=" . $assignment->id . "&section=" . $section, "Print" ) ?> |
  <?php if( get_logged_in_user()->is_super_user() ){ ?>
  <?= View::link_button( "query_delete.php?id=" . $assignment->get_query()->id . "&section=" . $section, "Delete query", array( "rel"=>"confirm-delete" ) ) ?>
  <?php } ?>
</div>

<div class="columnWrapper">
  <div class="column">
    <h2>Assignment Information</h2>

    <?php if( $assignment->is_new() &&
              ( $assignment->get_query()->created_by_id == get_logged_in_user_id() ||
                get_logged_in_user()->is_super_user() ) ){ ?>
    <div class="actions narrow">
      <?= View::link_button( "assignment.php?id=" . $assignment->id . "&section=" . $section, "Edit assignment" ) ?>
    </div>
    <?php } ?>

    <table cellspacing="1" cellpadding="0" class="info">
      <tr>
        <th>Assignment #</th>
        <td><?= $assignment->number ?></td>
      </tr>
      <tr>
        <th>Assigned To</th>
        <td><?= $assignment->get_assigned_to()->get_full_name() ?></td>
      </tr>
      <tr>
        <th>Department</th>
        <td><?= $assignment->get_department()->value ?></td>
      </tr>
      <tr>
        <th>Deadline</th>
        <td><?= $assignment->deadline ?></td>
      </tr>
      <tr>
        <th>Status</th>
        <?php if($assignment->status == "Completed") { ?>
        <td><?= $assignment->status ?> <em>(<?= substr( $assignment->completed_on, 0, 10 ) ?>)</em></td>
        <?php } else if($assignment->status == "Closed") { ?>
        <td><?= $assignment->status ?> <em>(<?= substr( $assignment->closed_on, 0, 10 ) ?>)</em></td>
        <?php } else { ?>
        <td><?= $assignment->status ?> <em>(<?= exists( $assignment->last_updated_on ) ? substr( $assignment->last_updated_on, 0, 10 ) : substr( $assignment->created_on, 0, 10) ?>)</em></td>
        <? } ?>
      </tr>
    </table>

    <h2>Query Information</h2>

    <?php if( $assignment->get_query()->created_by_id == get_logged_in_user_id() ||
              get_logged_in_user()->is_super_user() ){ ?>
    <div class="actions narrow">
      <?= View::link_button( "query.php?id=" . $assignment->get_query()->id . "&section=" . $section . "&assignment_id=" . $assignment->id, "Edit query" ) ?>
    </div>
    <?php } ?>

    <table cellspacing="1" cellpadding="0" class="info">
      <tr>
        <th>Audit Type</th>
        <td><?= $assignment->get_query()->audit_type ?></td>
      </tr>
      <tr>
        <th>Audit Ref</th>
        <td><?= $assignment->get_query()->audit_ref ?></td>
      </tr>
      <tr>
        <th>Audit Date</th>
        <td><?= $assignment->get_query()->audit_date ?></td>
      </tr>
      <tr>
      <th>Financial Year</th>
      <td><?= $assignment->get_query()->financial_year ?></td>
      </tr>
      <tr>
        <th>Subject / Assignment Information</th>
        <td><?= $assignment->get_query()->assignment_information ?></td>
      </tr>
      <tr>
        <th>Audit Finding</th>
        <td><?= $assignment->get_query()->audit_finding ?></td>
      </tr>
      <tr>
        <th>Audit Recommendation</th>
        <td><?= $assignment->get_query()->audit_recommendation ?></td>
      </tr>
      <tr>
        <th>Audit Document</th>
        <?php if( exists( $assignment->get_query()->audit_doc ) ){ ?>
        <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->get_query()->audit_doc ?>">Download audit document</a></td>
        <?php }else{ ?>
        <td></td>
        <?php } ?>
      </tr>
      <tr>
        <th>Previously Queried </th>
        <td ><?= View::display_bool( $assignment->get_query()->previously_queried ) ?></td>
      </tr>
      <tr>
        <th>Risk Level</th>
        <td><?= $assignment->get_query()->risk_level ?></td>
      </tr>
      <tr>
        <th>Risk</th>
        <td><?= $assignment->get_query()->get_risk()->name ?></td>
      </tr>
      <tr>
        <th>Risk Detail</th>
        <td><?= $assignment->get_query()->risk_detail ?></td>
      </tr>
    </table>
  </div>
  <div class="column">
    <h2>Assignment Response</h2>

    <?php if( ($assignment->has_responded() && $assignment->is_rejected() == false ) || ( $assignment->is_new() == false && $assignment->is_inprogress() == false )){ ?>
    <table cellspacing="1" cellpadding="0" class="info">
      <tr>
        <th>Response</th>
        <td><?= $assignment->response ?></td>
      </tr>
      <tr>
          <th>Is there a Policy or Standard Operating Procedure(SOP) in place?</th>
          <td><?= $assignment->is_policy_sop ?></td>
      </tr>
      <?php if( $assignment->is_policy_sop == "No" ){ ?>
        <tr>
            <th>Do you need assistance with developing a Policy or Standard Operating Procedure (SOP)?</th>
            <td><?= View::display_bool($assignment->sop_assistance) ?></td>
        </tr>
    <?php } ?>
        <tr>
        <th>SOP/Policy</th>
      <?php if( exists( $assignment->SOP_Policy_attachment ) ){ ?>
        <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->SOP_Policy_attachment ?>">Download SOP/Policy attachment</a></td>
      <?php }else{ ?>
      <td></td>
      <?php } ?>
    </tr>
      <tr>
        <th>Attachment 1</th>
        <?php if( exists( $assignment->response_attachment ) ){ ?>
        <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->response_attachment ?>">Download attachment</a></td>
        <?php }else{ ?>
        <td></td>
        <?php } ?>
      </tr>
      <tr>
        <th>Attachment 2</th>
        <?php if( exists( $assignment->response_attachment_2 ) ){ ?>
        <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->response_attachment_2 ?>">Download attachment</a></td>
        <?php }else{ ?>
        <td></td>
        <?php } ?>
      </tr>
      <tr>
        <th>Attachment 3</th>
        <?php if( exists( $assignment->response_attachment_3 ) ){ ?>
        <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->response_attachment_3 ?>">Download attachment</a></td>
        <?php }else{ ?>
        <td></td>
        <?php } ?>
      </tr>
      <tr>
        <th>Attachment 4</th>
        <?php if( exists( $assignment->response_attachment_4 ) ){ ?>
        <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->response_attachment_4 ?>">Download attachment</a></td>
        <?php }else{ ?>
        <td></td>
        <?php } ?>
      </tr>
      <tr>
        <th>Attachment 5</th>
        <?php if( exists( $assignment->response_attachment_5 ) ){ ?>
        <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->response_attachment_5 ?>">Download attachment</a></td>
        <?php }else{ ?>
        <td></td>
        <?php } ?>
      </tr>
      <tr>
        <th>Responded On</th>
        <td><?= $assignment->responded_on ?></td>
      </tr>
      <tr>
        <th>Print</th>
        <td><?= View::link_button( "assignment_response_print.php?assignment_id=" . $assignment->id . "&section=" . $section, "Print response" ) ?></td>
      </tr>
    </table>
<?php 
//Code added by JC on 20 May 2018 at request of IAS Helpdesk request IGN0001/R835
if( $assignment->is_responded() && count($assignment->get_open_actions()) == 0 ){
//if( $assignment->is_responded() && count( $actions ) == 0 ){ 
?>
    <form action="" method="post" class="default actions" style="margin-top:10px;">

      <?php View::label( "fldCompletedOn", "Completed On", true ) ?>
      <?php View::text( "fldCompletedOn", "completed_on", View::get_current_date(), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input" ) ) ?>
      <?php View::hidden( "action", View::$ACTION_SAVE ) ?>
      <?php View::hidden( "id", $assignment->id ) ?>
      <?php View::hidden( "section", $section ) ?>
      <?php View::hidden( "complete_only", "1" ) ?>
      <?php View::submit( "Mark query as completed", true ) ?>
    </form>
    <?php } ?>

    <?php }else if( $assignment->assigned_to_id == get_logged_in_user_id() || get_logged_in_user()->is_super_user() ){ ?>

    <form action="" method="post" enctype="multipart/form-data" class="default">
      <div>
        <?php View::label( "fldResponse", "Response", true ) ?>
        <?php View::textarea( "fldResponse", "response", $assignment->response, array( "cols"=>"40", "rows"=>"4" ) ) ?>
        <?php View::field_error( $assignment->get_field_error( "response" ) ) ?>
      </div>
      <div>
         <?php View::label( null, "Is there a Policy or Standard Operating Procedure(SOP) in place?",false, true ) ?>
      </div>
      <div>
        <?php View::radio( "fldIsPolicySOP_Yes", "is_policy_sop", "Yes", "Yes", $assignment->is_policy_sop ) ?>
        <?php View::radio( "fldIsPolicySOP_No", "is_policy_sop", "No", "No", $assignment->is_policy_sop ) ?>
        <?php View::field_error( $assignment->get_field_error( "is_policy_sop" ) ) ?>
      </div>
      <div id="wrapSOPAssistance" <?= $assignment->is_policy_sop != "No" ? "style=\"display:none\"" : "" ?>>
        <?php View::boolean_checkbox( "fldSOPAssistance", "sop_assistance", $completed ) ?>
        <?php View::label( "fldSOPAssistance", "Do you need assistance with developing a Policy or Standard Operating Procedure (SOP)?", false, true ) ?>
        <?php View::field_error( $assignment->get_field_error( "sop_assistance" ) ) ?>   
      </div>
      <div id="wrapSOPPolicyFile" <?= $assignment->is_policy_sop != "No" ? "style=\"display:none\"" : "" ?>>
        <?php View::label( "fldSOP_Policy_File", "SOP/Policy" ) ?>
        <?php View::file( "fldSOP_Policy_File", "SOP_Policy_attachment_file", $assignment->SOP_Policy_attachment ) ?>
        <?php View::field_error( $assignment->get_field_error( "SOP_Policy_attachment" ) ) ?>
      </div>
      <div id="wrapesponseFile" class="assignment" <?= no ? "" : "style=\"display:none;\"" ?>>
        <?php View::label( "fldResponseFile", "Attachment 1" ) ?>
        <?php View::file( "fldResponseFile", "response_attachment_file", $assignment->response_attachment ) ?>
        <?php View::button("Remove File", array( "id"=>"RemoveAttachment", "class"=>"ui-button" ) )?>
        <?php View::field_error( $assignment->get_field_error( "response_attachment" ) ) ?>
      </div>
        <div id="wrapesponseFile_2" class="assignment" <?= no ? "" : "style=\"display:none;\"" ?>>
        <?php View::label( "fldResponseFile_2", "Attachment 2" ) ?>
        <?php View::file( "fldResponseFile_2", "response_attachment_file_2", $assignment->response_attachment_2 ) ?>
        <?php View::button( "Remove File", array( "id"=>"RemoveAttachment2", "class"=>"ui-button" ) )?>
        <?php View::field_error( $assignment->get_field_error( "response_attachment_2" ) ) ?>
      </div>
        <div id="wrapesponseFile_3" class="assignment" <?= no   ? "" : "style=\"display:none;\"" ?>>
        <?php View::label( "fldResponseFile_3", "Attachment 3" ) ?>
        <?php View::file( "fldResponseFile_3", "response_attachment_file_3", $assignment->response_attachment_3 ) ?>
        <?php View::button( "Remove File", array( "id"=>"RemoveAttachment3", "class"=>"ui-button" ) )?>
        <?php View::field_error( $assignment->get_field_error( "response_attachment_3" ) ) ?>
      </div>
        <div id="wrapesponseFile_4" class="assignment" <?= no   ? "" : "style=\"display:none;\"" ?>>
        <?php View::label( "fldResponseFile_4", "Attachment 4" ) ?>
        <?php View::file( "fldResponseFile_4", "response_attachment_file_4", $assignment->response_attachment_4 ) ?>
        <?php View::button( "Remove File", array( "id"=>"RemoveAttachment4", "class"=>"ui-button" ) )?>
        <?php View::field_error( $assignment->get_field_error( "response_attachment_4" ) ) ?>
      </div>
        <div id="wrapesponseFile_5" class="assignment" <?= no   ? "" : "style=\"display:none;\"" ?>>
        <?php View::label( "fldResponseFile_5", "Attachment 5" ) ?>
        <?php View::file( "fldResponseFile_5", "response_attachment_file_5", $assignment->response_attachment_5 ) ?>
        <?php View::button( "Remove File", array( "id"=>"RemoveAttachment5", "class"=>"ui-button" ) )?>
        <?php View::field_error( $assignment->get_field_error( "response_attachment_5" ) ) ?>
      </div>
      <?php if( count( $actions ) == 0 ){ ?>
      <div class="indent">
      <?php View::boolean_checkbox( "fldCompleted", "completed", $completed ) ?>
      <?php View::label( "fldCompleted", "Mark this query as completed?", false, true ) ?>
      <?php View::text( "fldCompletedOn", "completed_on", View::get_current_date(), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input" ) ) ?>
      </div>
      <?php } ?>
      <?php View::hidden( "action", View::$ACTION_SAVE ) ?>
      <?php View::hidden( "id", $assignment->id ) ?>
      <?php View::hidden( "section", $section ) ?>
      <?php View::submit() ?>
    </form>
    <?php }else{ ?>
    <p><?= $assignment->get_assigned_to()->get_full_name() ?> has not responded yet.</p>
    <?php } ?>

    <h2>Actions</h2>
    <?php if( $assignment->is_completed() == false && $assignment->is_closed() == false && ( $assignment->assigned_to_id == get_logged_in_user_id() || get_logged_in_user()->is_super_user() ) ){ ?>
    <div class="actions">
      <?= View::link_button( "action.php?assignment_id=" . $assignment->id . "&section=" . $section, "New action" ) ?>
    </div>
    <?php } ?>

    <table cellspacing="1" cellpadding="0" class="default zebra">
      <thead>
        <tr>
          <th>Number</th>
          <th>Assigned To</th>
          <th>Deadline</th>
          <th>Progress</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
      <?php
      if( count( $actions ) > 0 )
      {
        foreach( $actions as $obj )
        {
      ?>
        <tr>
          <td><?= $obj->number ?></td>
          <td><?= $obj->get_assigned_to()->get_full_name() ?></td>
          <td><?= $obj->deadline ?></td>
          <td><?= $obj->progress ?>%</td>
          <td><?= $obj->status ?></td>
          <td>
            <?= View::link_button( "action_info.php?id=" . $obj->id . "&section=" . $section, "View" ) ?>
            <?php if( get_logged_in_user()->is_super_user() ){ ?>
            <?= View::link_button( "action_delete.php?id=" . $obj->id . "&section=" . $section . "&assignment_id=" . $assignment->id, "Delete", array( "rel"=>"confirm-delete" ) ) ?>
            <?php } ?>
          </td>
        </tr>
      <?php
        }
      }
      else
      {
      ?>
        <tr><td colspan="6">No actions have been created for this query</td></tr>
      <?php
      }
      ?>
      </tbody>
    </table>
    
    <?php if( $assignment->is_completed() && get_logged_in_user()->has_access( "signoff_query" ) ){ ?>
     <form action="" method="post" class="default">
        <h2>Internal Audit Response</h2>
        <div>
            <?php View::label("fldBlnFollowUp", "Follow-up", true) ?>
            <?php View::radio( "fldBlnFollowUp_yes", "bln_follow_up", "1", "Yes", $assignment->bln_follow_up ) ?>
            <?php View::radio( "fldBlnFollowUp_no", "bln_follow_up", "0", "No", $assignment->bln_follow_up ) ?>
            <?php /*View::boolean_checkbox("fldBlnFollowUp", "bln_follow_up", $assignment->bln_follow_up) */?>
            <?php /*View::field_hint( "fldBlnFollowUp", "Yes/No" ) */?>
            <?php View::field_error( $assignment->get_field_error( "bln_follow_up" ) ) ?>
        </div>
        <div>
           <?php View::label( "fldFollowUp", "IA Follow-up Procedure" ) ?>
           <?php View::textarea( "fldFollowUp", "follow_up", $assignment->follow_up , array( "cols"=>"40", "rows"=>"4")) ?>
           <?php View::field_error( $assignment->get_field_error( "follow_up" ) ) ?>
       </div>
        <div>
            <?php View::label("fldEffectiveControls", "Are Effective Controls in place", true) ?>
            <?php View::radio( "fldEffectiveControls_yes", "effective_controls", "1", "Yes", $assignment->effective_controls ) ?>
            <?php View::radio( "fldEffectiveControls_no", "effective_controls", "0", "No", $assignment->effective_controls ) ?>
            <?php View::field_error( $assignment->get_field_error( "effective_controls" ) ) ?>
        </div>
        <h2>Sign-off Query</h2>
      <div>
        <?php View::label( null, "Sign-off Status", true ) ?>
        <?php View::radio( "fldClosedStatus_accept", "closed_status", "Accepted", "Accept", $assignment->closed_status ) ?>
        <?php View::radio( "fldClosedStatus_reject", "closed_status", "Rejected", "Reject", $assignment->closed_status ) ?>
        <?php View::field_error( $assignment->get_field_error( "closed_status" ) ) ?>
      </div>
      <div id="wrapRejectReason" <?= $assignment->closed_status != "Rejected" ? "style=\"display:none\"" : "" ?>>
        <?php View::label( "fldRejectReason", "Reject Reason", true ) ?>
        <?php View::textarea( "fldRejectReason", "reject_reason", $assignment->reject_reason, array( "cols"=>"40", "rows"=>"4" ) ) ?>
        <?php View::field_error( $assignment->get_field_error( "reject_reason" ) ) ?>
      </div>
      <div>
        <?php View::label( "fldRiskReferenceNumber", "Risk Reference Number" ) ?>
        <?php  View::text( "fldRiskReferenceNumber", "risk_reference_number", $assignment->risk_reference_number ) ?>
      </div>
      <div>
        <?php View::label( null, "Risk Status", true ) ?>
        <?php View::radio( "fldRiskStatus_addresses", "risk_status", "Addressed", "Addressed", $assignment->risk_status ) ?>
        <?php View::radio( "fldRiskStatus_unaddresses", "risk_status", "Unaddressed", "Unaddressed", $assignment->risk_status ) ?>
        <?php View::field_error( $assignment->get_field_error( "risk_status" ) ) ?>
      </div>
      <div>
        <?php View::label( "fldFurtherActions", "Further Actions" ) ?>
        <?php View::textarea( "fldFurtherActions", "further_actions", $assignment->further_actions, array( "cols"=>"40", "rows"=>"4" ) ) ?>
        <?php View::field_error( $assignment->get_field_error( "further_actions" ) ) ?>
      </div>
      <?php View::hidden( "action", View::$ACTION_SAVE ) ?>
      <?php View::hidden( "sign_off", "1" ) ?>
      <?php View::hidden( "id", $assignment->id ) ?>
      <?php View::hidden( "section", $section ) ?>
      <?php View::submit() ?>
    </form>

    <?php } ?>

    <?php if( $assignment->is_closed() || ($assignment->is_rejected() && !$assignment->is_completed() ) ){ ?>
    <h2>Internal Audit Response</h2>
    <table cellspacing="1" cellpadding="0" class="info">
      <tr>
        <th>Follow-up</th>
        <td><?= View::display_bool($assignment->bln_follow_up) ?></td>
      </tr>
        <tr>
        <th>IA Follow-up Procedure</th>
        <td><?= $assignment->follow_up ?></td>
      </tr>
      <tr>
        <th>Are Effective Controls in place</th>
        <td><?= View::display_bool($assignment->effective_controls) ?></td>
      </tr>
      </table>
      <h2>Sign-off Query</h2>
    <table cellspacing="1" cellpadding="0" class="info">
      <tr>
        <th>Sign-off Status</th>
        <td><?= $assignment->closed_status ?></td>
      </tr>
      <?php if( $assignment->closed_status == "Rejected" ){ ?>
      <tr>
        <th>Reject Reason</th>
        <td><?= $assignment->reject_reason ?></td>
      </tr>
      <?php } ?>
      <tr>
          <th>Risk Reference Number</th>
          <td><?= $assignment->risk_reference_number ?></td>
      </tr>
      <tr>
        <th>Risk Status</th>
        <td><?= $assignment->risk_status ?></td>
      </tr>
      <tr>
        <th>Further Actions</th>
        <td><?= $assignment->further_actions ?></td>
      </tr>
    
    <?php } ?>
  </div>
</div>

<?php
  include("inc_footer.php");
?>