<?php
  include("inc_ignite.php");

  if( get_logged_in_user()->is_super_user() == false )
  {
    View::redirect( "access_denied.php" );
  }

  $section = exists( $_REQUEST["section"] ) ? $_REQUEST["section"] : 5;
  $id = $_GET["id"];

  $query = new Query();
  $query->id = $id;
  if( $query->delete() )
  {
    View::redirect( View::get_section_url( $section ) );
  }
  else
  {
    View::show_db_error( $query->get_sql() );
  }
?>