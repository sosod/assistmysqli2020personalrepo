<?php
  include("inc_ignite.php");

  if( get_logged_in_user()->has_access( "setup" ) == false )
  {
    View::redirect( "access_denied.php" );
  }

  if( $_POST["action"] == View::$ACTION_SAVE )
  {
    $usersetup = new UserSetup();
    $usersetup->init( $_POST );

    if( $usersetup->validate() )
    {
      if( $usersetup->save() )
      {
        View::set_session_message( "usersetup", "User setup successfully saved." );
        View::redirect( "setup_users.php" );
      }
      else
      {
        View::show_db_error( $usersetup->get_sql() );
      }
    }
  }
  else
  {
    $usersetup = new UserSetup( $_GET["id"] );
  }
?>

<?
  include("inc_header.php");
?>
<?php
  $section = 1;
  include("inc_nav_setup.php");
?>

<h1>Setup &raquo; Users &raquo; User</h1>

<div class="actions">
  <?php View::link_button( "setup_users.php", "Back" ) ?>
</div>

<form action="" method="post" class="default">
  <div>
    <?php View::label( "fldAdministrator", "Administrator" ) ?>
    <?php View::boolean_checkbox( "fldAdministrator", "module_admin", $usersetup->module_admin ) ?>
    <?php View::field_hint( "fldAdministrator", "Allows access to the setup section of the module" ) ?>
    <?php View::field_error( $usersetup->get_field_error( "module_admin" ) ) ?>
  </div>
  <div>
    <?php View::label( "fldCreateQuery", "Create Queries" ) ?>
    <?php View::boolean_checkbox( "fldCreateQuery", "create_query", $usersetup->create_query ) ?>
    <?php View::field_hint( "fldCreateQuery", "Allows the user to create new queries" ) ?>
    <?php View::field_error( $usersetup->get_field_error( "create_query" ) ) ?>
  </div>
  <div>
    <?php View::label( "fldSignOffQuery", "Sign-off Queries" ) ?>
    <?php View::boolean_checkbox( "fldSignOffQuery", "signoff_query", $usersetup->signoff_query ) ?>
    <?php View::field_hint( "fldSignOffQuery", "Allows the user to sign-off queries" ) ?>
    <?php View::field_error( $usersetup->get_field_error( "signoff_query" ) ) ?>
  </div>
  <div>
    <?php View::label( "fldDeptNotify", "Dept Notifications" ) ?>
    <?php View::boolean_checkbox( "fldDeptNotify", "dept_notify", $usersetup->dept_notify ) ?>
    <?php View::field_hint( "fldDeptNotify", "Receive notifications for queries limited to the user's own department" ) ?>
    <?php View::field_error( $usersetup->get_field_error( "dept_notify" ) ) ?>
  </div>
  <div>
    <?php View::label( "fldAllNotify", "All Notifications" ) ?>
    <?php View::boolean_checkbox( "fldAllNotify", "all_notify", $usersetup->all_notify ) ?>
    <?php View::field_hint( "fldAllNotify", "Receive notifications for all queries in the system" ) ?>
    <?php View::field_error( $usersetup->get_field_error( "all_notify" ) ) ?>
  </div>
  <?php View::hidden( "action", View::$ACTION_SAVE ) ?>
  <?php View::hidden( "user_id", $usersetup->user_id ) ?>
  <?php View::hidden( "id", $usersetup->id ) ?>
  <?php View::submit() ?><?php View::cancel( "setup_users.php" ) ?>
</form>

<?php
  include("inc_footer.php");
?>