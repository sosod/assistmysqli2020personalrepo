<?php
  include("inc_ignite.php");

  if( get_logged_in_user()->has_access( "setup" ) == false )
  {
    View::redirect( "access_denied.php" );
  }

  include("inc_header.php");

  /* create a default user setup record for any user that does not already have one */
  $users = query_records( "SELECT t.tkid FROM assist_" . get_company_code() . "_timekeep t
                          LEFT JOIN assist_" . get_company_code() . "_aqa_usersetup u ON u.user_id = t.tkid
                          JOIN assist_" . get_company_code() . "_menu_modules_users m ON t.tkid = m.usrtkid
                          WHERE m.usrmodref = 'AQA'
                          AND u.id IS NULL " );

  foreach( $users as $user )
  {
    $user_setup = new UserSetup();
    $user_setup->user_id = $user["tkid"];
    $user_setup->create_query = 0;
    $user_setup->signoff_query = 0;
    $user_setup->module_admin = 0;
    $user_setup->dept_notify = 0;
    $user_setup->all_notify = 0;
    $user_setup->save();
  }
?>
<?php
  $section = 1;
  include("inc_nav_setup.php");
?>

<h1>Setup &raquo; Users</h1>

<?php View::get_session_message( "usersetup" ) ?>

<table cellspacing="1" cellpadding="0" class="default zebra">
  <thead>
    <tr>
      <th>User</th>
      <th>Administrator</th>
      <th>Create Queries</th>
      <th>Sign-off Queries</th>
      <th>Department Notifications</th>
      <th>All Notifications</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $user_setup = new UserSetup();
  $records = $user_setup->get_records();

  if( count( $records ) > 0 )
  {
    foreach( $records as $obj )
    {
  ?>
    <tr>
      <td><?= $obj->get_user()->get_full_name() ?></td>
      <td><?= View::display_bool( $obj->module_admin ) ?></td>
      <td><?= View::display_bool( $obj->create_query ) ?></td>
      <td><?= View::display_bool( $obj->signoff_query ) ?></td>
      <td><?= View::display_bool( $obj->dept_notify ) ?></td>
      <td><?= View::display_bool( $obj->all_notify ) ?></td>
      <td><?= View::link_button( "setup_user.php?id=" . $obj->id, "Edit" ) ?></td>
    </tr>
  <?php
    }
  }
  else
  {
  ?>
    <tr><td colspan="7">No records found</td></tr>
  <?php
  }
  ?>
  </tbody>
</table>

<?php
  include("inc_footer.php");
?>