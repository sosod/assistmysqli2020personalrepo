<?php
  include("inc_ignite.php");

  $section = exists( $_REQUEST["section"] ) ? $_REQUEST["section"] : -1;

  if( $_POST["action"] == View::$ACTION_SAVE )
  {
    $completed = $_POST["completed"];
    $action = new Action( $_POST["action_id"] );
    $action_update = new ActionUpdate();

    $action_update->init( $_POST );

    if( $completed )
    {
      $action_update->progress = 100;
    }

    if( $action_update->validate() )
    {
      if( $action_update->save() )
      {
        $action->progress = $action_update->progress;

        if( $action->progress == 100 )
        {
          $action->status = "Completed";
          $action->completed_on = DBHelper::get_now();
        }
        else
        {
          $action->status = "In-progress";
        }

        if( $action->save() )
        {
          if( $action->status == "Completed" )
          {
            EmailHelper::send_action_completed( $action );
          }

          View::set_session_message( "action", "Action update successfully saved." );
          View::redirect( "action_info.php?id=" . $action->id . "&section=" . $section );
        }
        else
        {
          View::show_db_error( $action->get_sql() );
        }
      }
      else
      {
        View::show_db_error( $action->get_sql() );
      }
    }
  }
  else
  {
    $action = new Action( $_GET["id"] );
    $action_update = new ActionUpdate();
  }

  $action_updates = $action->get_updates();

  include("inc_header.php");
  include("inc_nav_query.php");

?>

<script type="text/javascript" src="lib/action_update.js"></script>

<h1><?= View::get_section_header( $section ) ?> &raquo; Assignment &raquo; Action</h1>

<?php View::get_session_message( "action" ) ?>

<div class="actions">
  <?= View::back_button( $section ) ?>

  <?php if( get_logged_in_user()->is_super_user() ){ ?>
  <?= View::link_button( "action_delete.php?id=" . $action->id . "&section=" . $section, "Delete", array( "rel"=>"confirm-delete" ) ) ?>
  <?php } ?>

</div>

<div class="columnWrapper">
  <div class="column">
    <h2>Action Information</h2>

    <?php if( $action->is_completed() == false &&
              ( $action->get_assignment()->created_by_id == get_logged_in_user_id() ||
                get_logged_in_user()->is_super_user() ) ){ ?>
    <div class="actions narrow">
      <?= View::link_button( "action.php?id=" . $action->id . "&section=" . $section, "Edit action" ) ?>
    </div>
    <?php } ?>

    <table cellspacing="1" cellpadding="0" class="info">
      <tr>
        <th>Action #</th>
        <td><?= $action->number ?></td>
      </tr>
      <tr>
        <th>Instruction</th>
        <td><?= $action->instruction ?></td>
      </tr>
      <tr>
        <th>Assigned To</th>
        <td><?= $action->get_assigned_to()->get_full_name() ?></td>
      </tr>
      <tr>
        <th>Deadline</th>
        <td><?= $action->deadline ?></td>
      </tr>
      <tr>
        <th>Remind On</th>
        <td><?= $action->remind_on ?></td>
      </tr>
      <tr>
        <th>Progress</th>
        <td><?= $action->progress ?>%</td>
      </tr>
      <tr>
        <th>Status</th>
        <td><?= $action->status ?> <em>(<?= exists( $action->last_updated_on ) ? $action->last_updated_on : $action->created_on ?>)</em></td>
      </tr>
    </table>

  </div>
  <div class="column">

    <h2>Updates</h2>

    <table cellspacing="1" cellpadding="0" class="default zebra">
      <thead>
        <tr>
          <th>Updated on</th>
          <th>Description</th>
          <th>Progress</th>
          <th>Attachment</th>
        </tr>
      </thead>
      <tbody>
      <?php
      if( count( $action_updates ) > 0 )
      {
        foreach( $action_updates as $obj )
        {
      ?>
        <tr>
          <td><?= $obj->created_on ?></td>
          <td><?= $obj->description ?></td>
          <td><?= $obj->progress ?>%</td>
          <?php if( exists( $obj->attachment ) ){ ?>
          <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $obj->attachment ?>">Download attachment</a></td>
          <?php }else{ ?>
          <td></td>
          <?php } ?>
        </tr>
      <?php
        }
      }
      else
      {
      ?>
        <tr><td colspan="4">No updates have been made to this action.</td></tr>
      <?php
      }
      ?>
      </tbody>
    </table>

    <?php if( $action->is_completed() == false &&
              ( $action->assigned_to_id == get_logged_in_user_id() ||
                get_logged_in_user()->is_super_user() ) ){ ?>
    <h2>New Update</h2>
    <form action="" method="post" enctype="multipart/form-data" class="default">
      <div>
        <?php View::label( "fldDescription", "Description", true ) ?>
        <?php View::textarea( "fldDescription", "description", $action_update->description, array( "cols"=>"40", "rows"=>"4" ) ) ?>
        <?php View::field_error( $action_update->get_field_error( "description" ) ) ?>
      </div>
      <div>
        <?php View::label( "fldProgress", "Progress", true ) ?>
        <?php View::select( "fldProgress", "progress", $action_update->progress, View::get_numeric_select_list( $action->progress, 100 ), false, $completed ? array( "disabled" => "disabled" ) : array() ) ?>%
        <?php View::field_error( $action_update->get_field_error( "progress" ) ) ?>
      </div>
      <div>
        <?php View::label( "fldAttachmentFile", "Attachment" ) ?>
        <?php View::file( "fldAttachmentFile", "attachment_file", $action_update->attachment ) ?>
        <?php View::field_error( $action_update->get_field_error( "attachment" ) ) ?>
      </div>
      <div class="indent">
      <?php View::boolean_checkbox( "fldCompleted", "completed", $completed ) ?>
      <?php View::label( "fldCompleted", "Mark this action as completed?", false, true ) ?>
      </div>
      <?php View::hidden( "action", View::$ACTION_SAVE ) ?>
      <?php View::hidden( "action_id", $action->id ) ?>
      <?php View::hidden( "section", $section ) ?>
      <?php View::submit() ?>
    </form>
    <?php } ?>
  </div>
</div>

<?php
  include("inc_footer.php");
?>