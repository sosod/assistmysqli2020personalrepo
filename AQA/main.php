<?php
  include("inc_ignite.php");

  if( get_logged_in_user()->is_admin_user() )
  {
    View::redirect( "setup_users.php" );
  }

  View::redirect( "queries_open.php" );
?>