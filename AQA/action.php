<?php
  include("inc_ignite.php");

  $section = $_REQUEST["section"];

  if( $_POST["action"] == View::$ACTION_SAVE )
  {
    $action = new Action( $_POST["id"] );
    $action->init( $_POST );

    if( $action->validate() )
    {
      if( $action->save() )
      {
        if( exists( $_POST["current_assigned_to_id"] ) &&
            $_POST["current_assigned_to_id"] != $action->assigned_to_id )
        {
          EmailHelper::send_action_assignment( $action );
        }

        View::set_session_message( "action", "Action successfully saved." );
        View::redirect( "assignment_info.php?id=" . $action->assignment_id . "&section=" . $section );
      }
      else
      {
        View::show_db_error( $action->get_sql() );
      }
    }
  }
  else
  {
    $action = new Action( $_GET["id"] );

    if( $action->is_transient() )
    {
      $action->assignment_id = $_GET["assignment_id"];
    }
  }
?>

<?
  include("inc_header.php");
  include("inc_nav_query.php");
?>

<script type="text/javascript">
  var user_dept_json = <?php View::user_dept_json( $action->get_assignment()->department_id ) ?>;
</script>
<script type="text/javascript" src="lib/action.js"></script>

<h1><?= View::get_section_header( $section ) ?> &raquo; Assignment &raquo; Action</h1>

<div class="actions">
  <?= View::back_button( $section, "info", ( $section == 2 ? $action->id : $action->assignment_id ) ) ?>
</div>

<form action="" method="post" class="default">
  <div>
    <?php View::label( "fldInstruction", "Instruction", true ) ?>
    <?php View::textarea( "fldInstruction", "instruction", $action->instruction, array( "cols"=>"40", "rows"=>"4" ) ) ?>
    <?php View::field_error( $action->get_field_error( "instruction" ) ) ?>
  </div>
  <div>
    <?php View::label( "fldUser", "User", true ) ?>
    <?php View::text( "fldUser", "user", $action->get_assigned_to()->get_full_name(), array( "class"=>"autocomplete", "size"=>"25" ) ) ?>
    <span id="fldUserStatus" class="user-status-inline <?= exists( $action->assigned_to_id ) ? "ui-button-icon-primary ui-icon ui-icon-check" : "" ?>"></span>
    <?php View::hidden( "assigned_to_id", $action->assigned_to_id, array( "id"=>"fldUserId" ) ) ?>
    <?php View::field_error( $action->get_field_error( "assigned_to_id" ) ) ?>
  </div>
  <div>
    <?php View::label( "fldDeadline", "Deadline", true ) ?>
    <?php View::text( "fldDeadline", "deadline", ( exists( $action->deadline ) ? $action->deadline : View::get_current_date() ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input", "readonly"=>"readonly" ) ) ?>
    <?php View::field_error( $action->get_field_error( "deadline" ) ) ?>
  </div>
  <div>
    <?php View::label( "fldRemindOn", "Remind On", true ) ?>
    <?php View::text( "fldRemindOn", "remind_on", ( exists( $action->remind_on ) ? $action->remind_on : View::get_current_date() ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input", "readonly"=>"readonly" ) ) ?>
    <?php View::field_error( $action->get_field_error( "remind_on" ) ) ?>
  </div>
  <div>
    <?php View::label( "fldKPINumber", "KPI Number" ) ?>
    <?php View::text( "fldKPINumber", "kpi_number", $action->kpi_number, array( "maxlength"=>"255", "size"=>"10" ) ) ?>
    <?php View::field_error( $action->get_field_error( "kpi_number" ) ) ?>
  </div>
  <?php View::hidden( "action", View::$ACTION_SAVE ) ?>
  <?php View::hidden( "id", $action->id ) ?>
  <?php View::hidden( "assignment_id", $action->assignment_id ) ?>
  <?php View::hidden( "current_assigned_to_id", $action->assigned_to_id ) ?>
  <?php View::hidden( "section", $section ) ?>
  <?php View::submit() ?><?php View::section_cancel( $section, "info", ( $section == 2 ? $action->id : $action->assignment_id ) ) ?>
</form>

<?php
  include("inc_footer.php");
?>