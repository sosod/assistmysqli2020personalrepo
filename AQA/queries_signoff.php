<?php
  include("inc_ignite.php");

  if( get_logged_in_user()->has_access( "query_signoff" ) == false )
  {
    View::redirect( "access_denied.php" );
  }

  include("inc_header.php");

  $section = 3;
  include("inc_nav_query.php");
?>

<h1><?= View::get_section_header( $section ) ?></h1>

<?php View::get_session_message( "query" ) ?>

<table cellspacing="1" cellpadding="0" class="default zebra">
  <thead>
    <tr>
      <th>Audit Ref</th>
      <th>Audit Type</th>
      <th>Subject</th>
      <th>Audit Date</th>
      <th>Risk Level</th>
      <th>Risk</th>
      <th>Assignment #</th>
      <th>Assigned To</th>
      <th>Deadline</th>
      <th>Status</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $records = QueryHelper::get_signoff_query_data();

  if( count( $records ) > 0 )
  {
    foreach( $records as $row )
    {
  ?>
    <tr>
      <td><?= $row["audit_ref"] ?></td>
      <td><?= $row["audit_type"] ?></td>
      <td><?= $row["assignment_information"] ?></td>
      <td><?= $row["audit_date"] ?></td>
      <td><?= $row["risk_level"] ?></td>
      <td><?= $row["risk"] ?></td>
      <td><?= $row["number"] ?></td>
      <td><?= $row["user"] ?></td>
      <td><?= $row["deadline"] ?></td>
      <td><?= $row["status"] ?></td>
      <td>
        <a href="ajax_assignment_info.php?assignment_id=<?= $row["id"] ?>" rel="facebox"><img src="images/icon-view.png" alt="View" title="View"/></a>
        <?= View::link_button( "assignment_info.php?id=" . $row["id"] . "&section=" . $section, "View" ) ?>
      </td>
    </tr>
  <?php
    }
  }
  else
  {
  ?>
    <tr><td colspan="11">There are currently no queries that are ready for sign-off.</td></tr>
  <?php
  }
  ?>
  </tbody>
</table>

<?php
  include("inc_footer.php");
?>