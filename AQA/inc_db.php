<?php
$CONST_TABLE_PREFIX = "assist_${cmpcode}_aqa";

$logged_in_user = null;
$conn = null;

/******************************************************************************/
/****************************** HELPER FUNCTIONS ******************************/
/******************************************************************************/
function set_session_message( $msg )
{
  $_SESSION["msg"] = $msg;
}

function get_logged_in_user_id()
{
  global $tkid;

  return $tkid;
}

function get_company_code()
{
  global $cmpcode;

  return $cmpcode;
}

function get_logged_in_user()
{
  global $logged_in_user;

  if( $logged_in_user == null )
  {
    $logged_in_user = User::load( get_logged_in_user_id() );
  }

  return $logged_in_user;
}

/******************************************************************************/
/******************************* CORE FUNCTIONS *******************************/
/******************************************************************************/
function get_connection( $force = false )
{
  global $conn;

  if( isset( $conn ) == false || !$conn || $force == true )
  {
    global $cmpcode;
    global $tkid;
    global $tkname;

    $conn = mysql_connect("localhost","ignittps_ignite4","ign92054u");

    if(!$conn)
    {
    	$sqlerror = "<p>&nbsp;</p><p><b>---ERROR DETAILS BEGIN---</b></p><ul><li>Database error on sql: ".mysql_error()."</li><li>".$sql."</li><li>".$db."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
    	$fileloc = $_SERVER['PHP_SELF'];
      $to = "assist@ignite4u.co.za";
      $from = "assist@ignite4u.co.za";
      $subject = "Ignite Assist CON Error";
      $message = "<font face=arial size=2><p><b>Company:</b> ".$cmpcode."<br><b>TKID:</b> ".$tkid."<br><b>TK Name:</b> ".$tkname."</p>";
      $message .= "<p><b>File:</b> ".$fileloc."</p>";
    	$message .= $sqlerror;
      $message .= "</center></font>";
      $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
      mail($to,$subject,$message,$header);
    	die("<h1>Error</h1><p>Unfortunately Ignite Assist has encountered an error.<br>An email has been sent to Ignite Advisory Services at <a href=mailto:assist@ignite4u.co.za>assist@ignite4u.co.za</a> with the error details.<br>Thank you.</p><p>");
    }

    $db = "ignittps_i".strtolower($cmpcode);
    mysql_select_db($db,$conn);
  }

  return $conn;
}

function get_resultset( $sql )
{
  global $cmpcode;
  global $tkid;
  global $tkname;

  $conn = get_connection();
  $rs = mysql_query($sql,$conn);

  if(!$rs)
  {
  	$sqlerror = "<p>&nbsp;</p><p><b>---ERROR DETAILS BEGIN---</b></p><ul><li>Database error on sql: ".mysql_error()."</li><li>".$sql."</li><li>".$db."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
  	$fileloc = $_SERVER['PHP_SELF'];
    $to = "assist@ignite4u.co.za";
    $from = "assist@ignite4u.co.za";
    $subject = "Ignite Assist SQL50 Error";
    $message = "<font face=arial size=2><p><b>Company:</b> ".$cmpcode."<br><b>TKID:</b> ".$tkid."<br><b>TK Name:</b> ".$tkname."</p>";
    $message .= "<p><b>File:</b> ".$fileloc."</p>";
  	$message .= $sqlerror;
    $message .= "</center></font>";
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail($to,$subject,$message,$header);
  	die("<h1>Error</h1><p>Unfortunately Ignite Assist has encountered an error.<br>An email has been sent to Ignite Advisory Services at <a href=mailto:assist@ignite4u.co.za>assist@ignite4u.co.za</a> with the error details.<br>Thank you.</p><p>");
  }

  return $rs;
}

function get_record( $rs )
{
  return stripslashes_deep( mysql_fetch_array( $rs, MYSQL_ASSOC ) );
}

function query_count( $sql )
{
  $rs = get_resultset( $sql );
  $record = get_record( $rs );
  return (int)$record["cnt"];
}

function query_records( $sql )
{
  $rs = get_resultset($sql);

	$records = array();

	while( $record = get_record($rs) )
	{
		$records[] = $record;
	}

  return $records;
}

function execute_update( $sql )
{
  if( mysql_query( $sql, get_connection() ) )
  {
    return true;
  }

  return false;
}

function execute_insert( $sql )
{
  if( mysql_query( $sql, get_connection() ) )
  {
    return mysql_insert_id( get_connection() );
  }

  return false;
}

function stripslashes_deep( $value )
{
  $value = is_array( $value ) ? array_map( 'stripslashes_deep', $value ) : stripslashes( $value );

  return $value;
}

function log_transaction( $message, $sql, $old = "" )
{
  global $cmpcode;
  global $tkid;
  global $tkname;
  global $today;

  $ref = "AQA";
  $sql = str_replace("'","|",$sql);
  $message = str_replace("'","&#39",$message);
  $old = str_replace("'","&#39",$old);

  $sqlLog = "INSERT INTO assist_".$cmpcode."_log SET date = '".$today."', tkid = '".$tkid."', ref = '".$ref."', transaction = '".$message."', tsql = '".$sql."', told = '".$old."'";

	get_resultset( $sqlLog );
}

function quote( $value )
{
  if( isset( $value ) )
  {
    if( is_int( $value ) )
    {
      return $value;
    }
    elseif( is_float( $value ) )
    {
      return sprintf( '%F', $value );
    }

    return "'".addcslashes( $value, "\000\n\r\\'\"\032" )."'";
  }
  else
  {
    $value = 'NULL';
  }

  return $value;
}
?>