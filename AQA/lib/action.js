$.ui.autocomplete.prototype._renderItem = function( ul, item )
{
  var re = new RegExp( "^" + this.term );
  var t = item.label.replace( re, "<span class='ui-autocomplete-match'>" + this.term + "</span>" );
  return $( "<li></li>" ).data( "item.autocomplete", item )
                         .append( "<a>" + t + "<br><em>" + item.department + "</em></a>" )
                         .appendTo( ul );
};

$( document ).ready( function()
{
  $("input.autocomplete").autocomplete(
  {
    source: function( request, response ) 
    {
      response( user_dept_json );
    },
    focus: function( event, ui )
    {
      $(this).val( ui.item.label );
      return false;
    },
    select: function( event, ui )
    {
      $this.val( ui.item.label );
      $("#fldUserId" ).val( ui.item.value );
      $("#fldUserStatus" ).addClass( "ui-button-icon-primary" ).addClass( "ui-icon" ).addClass( "ui-icon-check" );
      
      return false;
    }
  });
});