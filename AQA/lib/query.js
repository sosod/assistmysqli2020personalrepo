var selected_users = [];

$.ui.autocomplete.prototype._renderItem = function( ul, item )
{
  var re = new RegExp( "^" + this.term );
  var t = item.label.replace( re, "<span class='ui-autocomplete-match'>" + this.term + "</span>" );
  return $( "<li></li>" ).data( "item.autocomplete", item )
                         .append( "<a>" + t + "<br><em>" + item.department + "</em></a>" )
                         .appendTo( ul );
};

$( document ).ready( function()
{
  $("input.autocomplete").autocomplete(
  {
    source: function( request, response ) 
    {
      var valid_options = [];
      
      $.each( user_dept_json, function( i, value )
      {
        if( $.inArray( value.value, selected_users ) == -1 )
        {
          valid_options.push( value );  
        }
      });
      
      response( valid_options );
    },
    focus: function( event, ui )
    {
      $(this).val( ui.item.label );
      return false;
    },
    select: function( event, ui )
    {
      $this = $(this);
      
      $this.val( ui.item.label );
      $("#" + $(this).attr("id") + "Id" ).val( ui.item.value );
      $("#" + $(this).attr("id") + "Dept" ).val( ui.item.department );
      $("#" + $(this).attr("id") + "DeptId" ).val( ui.item.departmentId );
      $("#" + $(this).attr("id") + "Status" ).addClass( "ui-button-icon-primary" ).addClass( "ui-icon" ).addClass( "ui-icon-check" );
      
      selected_users.push( ui.item.value );
      
      return false;
    }
  });
  
  $("#trigAddUser").click( function()
  {
    if( numAssignedUsers < 5 )
    {
      numAssignedUsers++;
      
      $("#rowUser" + numAssignedUsers ).fadeIn(); 
    }
  });
  
  $("#trigRemoveUser").click( function()
  {
    if( numAssignedUsers > 1 )
    {
      $row = $("#rowUser" + numAssignedUsers );
      $row.fadeOut( "fast" ).find( "input" ).not(".date-input").val("");
      $row.find(".user-status").removeClass( "ui-icon" );
      
      numAssignedUsers--;
    }
  });
  
  $("#fldRisk").change( function()
  {
    $this = $(this);
    $riskName = $("#fldRiskName");
    
    if( $this.val() == "-1" )
    {
      $riskName.fadeIn();
    }
    else
    {
      $riskName.hide();
    }
  });
});