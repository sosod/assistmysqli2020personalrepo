function redirect( url )
{
  window.location.href = url;	
}

function confirm_delete( url )
{
	if( confirm( "Are you sure you want to delete this record? This action is permanent and cannot be undone." ) )
	{
		redirect( url );
	}
	
	return false;
}
  
$(document).ready(function()
{
    $("table.zebra tr").mouseover(function(){$(this).addClass("over");}).mouseout(function(){$(this).removeClass("over");});
    $("table.zebra tr:even").addClass("alt");
    $(".confirm-delete").click( function()
    {
      return confirm_delete( this.href );
    });
    
    $("a[rel='confirm-delete']").click( function()
    {
      return confirm_delete( this.href );
    });
    
    $("a.linkButton").button();
    
    $( "button.submit" ).each( function()
    {
      $this = $(this);
      
      if( $this.val() == "Save" )
      {
        $this.button({
          icons: { primary: 'ui-icon-disk' }
        }); 
      }
      else
      {
        $this.button();
      }
    });
    
    $( "button#trigAddUser" ).button(
  	{
  	  icons: { primary: 'ui-icon-circle-plus' }
  	});
    
    $( "button#trigRemoveUser" ).button(
  	{
  	  icons: { primary: 'ui-icon-circle-minus' }
  	});
        
    $( "button#trigAddAttachment" ).button(
    {
      icons: { primary: 'ui-icon-circle-plus' }
    });

   
    
    $( "a.button" ).button();
    
    $(".date-input").datepicker({ dateFormat: 'yy-mm-dd', changeMonth:true, changeYear:true, yearRange: '-100:+10' });
    
    $(document).bind('keydown', 'alt+shift+z', function()
    {
      $("#user_setup_info").toggle();
    });
    
    $('a[rel*=facebox]').facebox();
});  