<?php
require('lib/fpdf/fpdf.php');

class IgnitePDF extends FPDF
{
  function Header()
  {
      //Select Arial bold 15
      $this->SetFont('Arial','',6);
      $this->SetTextColor(0,0,0);
      $this->Cell(0,10,'Private and Confidential',0,0,'C');
      //Line break
      $this->Ln(20);
  }

  function Footer()
  {
    //Go to 1.5 cm from bottom
    $this->SetY(-15);
    //Select Arial italic 8
    $this->SetFont('Arial','',6);
    $this->SetTextColor(0,0,0);
    //Print centered page number
    $this->Cell(0,10,'Page '.$this->PageNo() . ' - Powered by Ignite Assist - http://www.ignite4u.co.za',0,0,'C');
  }

  function printRow($data, $widths, $isTextList=false, $align='C', $fill = false )
  {
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
    {
      $nb=max($nb,$this->NbLines($widths[$i],$data[$i]));
    }
    $h=12*$nb;

    //Issue a page break first if needed
    $this->CheckPageBreak($h);

    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
      $w=$widths[$i];

      //Save the current position
      $x=$this->GetX();
      $y=$this->GetY();

      //Draw the border
      if( $isTextList == false )
      {
        $this->Rect($x,$y,$w,$h, $fill ? "DF" : "D");
      }

      //Print the text
      $align = $isTextList ? 'L' : $align;

      $this->MultiCell($w,12,$data[$i],0,$align );

      //Put the position to the right of the cell
      $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
  }

  function printInfoRow( $data, $name_width, $value_width )
  {
    //Calculate the height of the row
    $nb=0;

    foreach( $data as $name => $value )
    {
      $nb = max( $nb, $this->NbLines( $value_width, $value ) );
    }

    $h=15*$nb;

    //Issue a page break first if needed
    $this->CheckPageBreak( $h );

    //Draw the cells of the row

    foreach( $data as $name => $value )
    {
      //Save the current position
      $x=$this->GetX();
      $y=$this->GetY();


      $this->SetFont('Arial','B',7);
      $this->SetFillColor(155,155,155);
      $this->SetTextColor(255,255,255);

      $this->Rect( $x, $y, $name_width, $h, "DF" );
      $this->MultiCell( $name_width, 15, $name, 0, "C" );

      $this->SetXY( $x+$name_width, $y );

      $x=$this->GetX();
      $y=$this->GetY();

      $this->SetFont('Arial','',7);
      $this->SetTextColor(0,0,0);

      $this->Rect( $x, $y, $value_width, $h, "D" );
      $this->MultiCell( $value_width, 15, $value, 0, "L" );

      $this->SetXY( $x+$value_width, $y );
    }

    $this->Ln($h);
  }


  function CheckPageBreak($h)
  {
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
    {
      $this->AddPage($this->CurOrientation);
    }
  }

  function NbLines($w,$txt)
  {
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
    {
      $w=$this->w-$this->rMargin-$this->x;
    }

    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);

    if($nb>0 and $s[$nb-1]=="\n")
    {
      $nb--;
    }

    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;

    while($i<$nb)
    {
      $c=$s[$i];
      if($c=="\n")
      {
        $i++;
        $sep=-1;
        $j=$i;
        $l=0;
        $nl++;
        continue;
      }

      if($c==' ')
      {
        $sep=$i;
      }

      $l+=$cw[$c];
      if($l>$wmax)
      {
        if($sep==-1)
        {
          if($i==$j)
          {
            $i++;
          }
        }
        else
        {
          $i=$sep+1;
        }

        $sep=-1;
        $j=$i;
        $l=0;
        $nl++;
      }
      else
      {
        $i++;
      }
    }

    return $nl;
  }

  function listTable( $headers, $rows, $widths, $align = 'C', $indent = -1 )
  {
    if( $indent != -1 )
    {
      $this->SetX( $indent );
    }

    $this->SetFont('Arial','B',7);
    $this->SetFillColor(155,155,155);
    $this->SetTextColor(255,255,255);
    //Headers
    $this->printRow( $headers, $widths, false, $align, true );

    if( $indent != -1 )
    {
      $this->SetX( $indent );
    }

    $this->SetFont('Arial','',7);
    $this->SetTextColor(0,0,0);

    foreach( $rows as $row )
    {
      $this->printRow( $row, $widths, false, $align );
      
      if( $indent != -1 )
      {
        $this->SetX( $indent );
      }
    }

    $this->Cell( array_sum($widths), 10, '', 'T' );

    $this->Ln();
    
    //$this->SetX( 0 );
  }

  function infoTable( $rows, $widths )
  {
    for( $i = 0; $i < count( $rows ); $i++ )
    {
      $this->SetFont('Arial','B',7);
      $this->SetFillColor(155,155,155);
      $this->SetTextColor(255,255,255);
      $this->Cell( $widths[0], 15, $rows[ $i ][0], 1, 0, 'C', true );

      $this->SetFont('Arial','',7);
      $this->SetTextColor(0,0,0);
      $this->Cell( $widths[1], 15, $rows[ $i ][1], 1, 0 );
      $this->Ln();
    }

    //Closure line
    $this->Cell( array_sum($widths), 10, '', 'T' );
    $this->Ln();
  }

  function summaryTable( $data, $width = 780 )
  {
    $name_width = $width / 6;
    $value_width = $name_width * 2;

    $tmp_data = array();

    $cnt = 0;
    foreach( $data as $name => $value )
    {
      $tmp_data[ $name ] = $value;

      if( $cnt % 2 == 1 )
      {
        $this->printInfoRow( $tmp_data, $name_width, $value_width );
        $tmp_data = array();
      }

      $cnt++;
    }

    if( count( $tmp_data ) > 0 )
    {
      $this->printInfoRow( $tmp_data, $name_width, $value_width );
    }

    $this->Ln();
  }

  function textList( $rows )
  {
    $widths_3col = array(20,30,450);
    $widths_4col = array(20,30,40,440);

    $this->SetFont('Arial','',8);
    $this->SetTextColor(0,0,0);

    foreach( $rows as $row )
    {
      $widths = $widths_3col;

      if( count( $row ) == 4 )
      {
        $widths = $widths_4col;
      }

      $this->printRow( $row, $widths, true );
    }

    //$this->Cell( array_sum($widths), 10, '', 'T' );
    $this->Ln();
  }

  function Heading( $text, $fontSize, $textAlign, $fontStyle, $ln = true )
  {
    $this->SetFont('Arial',$fontStyle,$fontSize);
    $this->SetTextColor(0,0,0);
    $this->MultiCell(0,$fontSize+6,$text,0,$textAlign);

    if( $ln )
    {
      $this->Ln(10);
    }
  }

  function H1( $text, $align = 'L', $fontStyle = 'B' )
  {
    $this->Heading( $text, 18, $align, $fontStyle );
  }

  function H2( $text, $align = 'L', $fontStyle = 'B' )
  {
    $this->Heading( $text, 14, $align, $fontStyle );
  }

  function H3( $text, $align = 'L', $fontStyle = 'B' )
  {
    $this->Heading( $text, 12, $align, $fontStyle );
  }
  
  function H4( $text, $align = 'L', $fontStyle = 'B' )
  {
    $this->Heading( $text, 10, $align, $fontStyle, false );
  }

	function TableCaption( $text, $headSpace = true, $align = 'L', $fontStyle = 'B', $indent = -1 )
  {
  	if( $headSpace )
		{
			$this->Ln(10);
		}
    
    if( $indent != -1 )
    {
      $this->SetX( $indent );
    }

    $this->SetFont('Arial',$fontStyle,12);
    $this->SetTextColor(0,0,0);
    $this->MultiCell(0,12,$text,0,$align);
    $this->Ln(5);
  }

  function TextLine( $text, $indent = 0, $fontSize = 8, $textAlign = "L", $fontStyle="" )
  {
    $this->SetX( $indent );
    $this->SetFont('Arial',$fontStyle,$fontSize);
    $this->MultiCell(0,$fontSize+6,$text,0,$textAlign);
  }
}
?>
