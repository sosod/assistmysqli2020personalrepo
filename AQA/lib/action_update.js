$( document ).ready( function()
{
  $("#fldCompleted").click( function() 
  {
    if( $(this).is(":checked") )
    {
      $("#fldProgress").val( "100" ).attr( "disabled", true ); 
    }
    else
    {
      $("#fldProgress").attr( "disabled", false );
    }
  });
});