<script type="text/javascript">
  $(function()
  {
    $("#query-nav").buttonset();
    $("#query-nav input[type='radio']").click( function()
    {
      redirect( $(this).val() );
    });
  });
</script>
<div id="query-nav" class="subNav"><input type="radio" id="nav1" name="radio" value="queries_open.php" <?php if( $section == 1 ){ ?>checked="checked"<?php } ?> /><label for="nav1">&nbsp;&nbsp;My Open Queries&nbsp;&nbsp;</label><input type="radio" id="nav2" name="radio" value="actions_open.php" <?php if( $section == 2 ){ ?>checked="checked"<?php } ?> /><label for="nav2">&nbsp;&nbsp;My Open Actions&nbsp;&nbsp;</label><?php if( get_logged_in_user()->has_access( "signoff_query" ) ) { ?><input type="radio" id="nav3" name="radio" value="queries_signoff.php" <?php if( $section == 3 ){ ?>checked="checked"<?php } ?> /><label for="nav3">&nbsp;&nbsp;Queries to sign-off&nbsp;&nbsp;</label><?php } ?><input type="radio" id="nav4" name="radio" value="queries_all.php" <?php if( $section == 4 ){ ?>checked="checked"<?php } ?> /><label for="nav4">&nbsp;&nbsp;Browse All Queries&nbsp;&nbsp;</label><?php if( get_logged_in_user()->has_access( "query" ) ) { ?><input type="radio" id="nav5" name="radio" value="query.php" <?php if( $section == 5 ){ ?>checked="checked"<?php } ?> /><label for="nav5">&nbsp;&nbsp;New Query&nbsp;&nbsp;</label><?php } ?></div>