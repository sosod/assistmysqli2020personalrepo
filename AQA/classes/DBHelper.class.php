<?php
  class DBHelper
  {
    public static function get_now()
    {
      return date( "Y-m-d H:i:s" );
    }

    public static function get_current_date()
    {
      return date( "Y-m-d" );
    }
  }
?>