<?php
  class View
  {
  	public static $ACTION_SAVE = "save";
  	public static $ACTION_FILTER = "filter";
    public static $ACTION_DELETE = "delete";

  	/******************************************************************************/
		/******************************* LIST FUNCTIONS *******************************/
		/******************************************************************************/
		public static function get_bool_select_list()
    {
      return array( "0"=>"No", "1"=>"Yes" );
    }

		public static function get_numeric_select_list( $low=0, $high=100 )
    {
      $list = array();

      for( $i = $low; $i <= $high; $i++ )
      {
        $list[ (string)$i ] = (string)$i;
      }

      return $list;
    }

    public static function get_user_select_list()
    {
      $list = array();

      $user = new User();

      foreach( $user->get_records( false, "tkname asc") as $record )
      {
        $list[ (string)$record->id ] = $record->get_full_name();
      }

      return $list;
    }

		public static function get_department_select_list()
    {
      $list = array();

      $department = new Department();

      foreach( $department->get_records( "yn = 'Y'", "value asc") as $record )
      {
        $list[ (string)$record->id ] = $record->value;
      }

      return $list;
    }
    
    function get_financial_year_select_list()
    {
      return array("2010-2011"=>"2010-2011", "2011-2012"=>"2011-2012", "2012-2013"=>"2012-2013", "2013-2014"=>"2013-2014", "2014-2015"=>"2014-2015" , "2015-2016"=>"2015-2016" , "2016-2017"=>"2016-2017" , "2017-2018"=>"2017-2018" , "2018-2019"=>"2018-2019" , "2019-2020"=>"2019-2020"  );
    }

    function get_audit_type_select_list()
    {
      return array( "Internal"=>"Internal", "External"=>"External" );
    }

    function get_risk_level_select_list()
    {
      return array( "Low"=>"Low", "Medium"=>"Medium", "High"=>"High", "Severe"=>"Severe" );
    }

    function get_risk_status_select_list()
    {
      return array( "Active"=>"Active", "Inactive"=>"Inactive" );
    }

    function get_assignment_status_select_list()
    {
      return array( "New"=>"New", "Responded"=>"Responded", "In-progress"=>"In-progress", "Completed"=>"Completed", "Closed"=>"Closed" );
    }

    function get_action_status_select_list()
    {
      return array( "New"=>"New", "In-progress"=>"In-progress", "Completed"=>"Completed" );
    }

    public static function get_risk_select_list( $other = false )
    {
      $list = array();

      $risk = new Risk();

      foreach( $risk->get_records( "status = 'Active'", "name asc") as $record )
      {
        $list[ (string)$record->id ] = $record->name;
      }

      if( $other )
      {
        $list[ "-1" ] = "Other ...";
      }

      return $list;
    }

    /******************************************************************************/
		/******************************* FLOW FUNCTIONS *******************************/
		/******************************************************************************/
    public static function redirect( $url )
    {
      header( "Location: $url" );
      exit();
    }
    
    public static function CSV_Redirect($filename)
    {
		$usr_filename = end(explode("/",$filename));
        header("Content-type: text/csv");
        header("Content-disposition: attachment;filename=$usr_filename");
        readfile($filename);
    }

    /******************************************************************************/
    /******************************* INTERNAL FUNCTIONS ***************************/
    /******************************************************************************/
    private static function html_params( $params )
    {
      $html_params = "";

      if( is_array( $params ) && count( $params ) > 0 )
      {
        foreach( $params as $name => $value )
        {
          $html_params .= $name . "=\"" . $value . "\" ";
        }
      }

      return $html_params;
    }

		/******************************************************************************/
		/******************************* MACRO FUNCTIONS ******************************/
		/******************************************************************************/
    public static function label( $for, $value, $required = false, $inline = false )
    {
      echo "<label for=\"" . $for . "\"" . ( $inline ? "class=\"inline\"" : "" ) . ">" . $value . ( $inline == false ? ": " : "" ) . ( $required ? "<span class=\"required\">*</span>" : "" ) . "</label>";
    }

    public static function placeholder( $id, $value, $params = array() )
    {
      echo "<span id=\"" . $id . "\" class=\"placeholder\"" . self::html_params( $params ) . ">" . $value . "</span>";
    }

    public static function field_error( $message, $inline = true )
    {
      if( exists( $message ) )
      {
        echo "<p class=\"fieldError" . ( $inline ? "Inline" : "" ) . "\">" . $message . "</p>";
      }
    }

    public static function field_hint( $id, $message )
    {
      if( exists( $message ) )
      {
        echo "<span class=\"fieldHint\">" . $message . "</span>";
      }
    }

    public static function hidden( $name, $value, $params = array() )
    {
      echo "<input type=\"hidden\" name=\"" . $name . "\" value=\"" . $value . "\" " . self::html_params( $params ) . "/>";
    }

    public static function submit( $value = "Save", $inline = false, $name = "submit" )
    {
      //echo "<input type=\"submit\" value=\"" . $value. "\" class=\"button\" />";
      echo "<button type=\"submit\" name=\"" . $name . "\" class=\"submit" . ( $inline ? " inline" : "" ) . "\">" . $value. "</button>";
    }

    public static function section_cancel( $section, $type = "list", $id = "", $params = array() )
    {
      $href = self::get_section_url( $section, $type, $id );

      echo "<span class=\"cancel\">or <a href=\"" . $href . "\">Cancel</a>";
    }

    public static function cancel( $href, $value = "Cancel" )
    {
      echo "<span class=\"cancel\">or <a href=\"" . $href . "\">" . $value. "</a>";
    }

    public static function text( $id, $name, $value, $params = array() )
    {
      echo "<input type=\"text\" id=\"" . $id . "\" name=\"" . $name . "\" value=\"" . $value . "\" " . self::html_params( $params ) . "/>";
    }

    public static function radio( $id, $name, $option, $label, $value, $params = array() )
    {
      echo "<input type=\"radio\" id=\"" . $id . "\" name=\"" . $name . "\" value=\"" . $option . "\" " . self::html_params( $params ) . ( $value == $option ? "checked=\"checked\" " : "" ) . "/>";
      echo self::label( $id, $label, false, true );
    }

    public static function boolean_checkbox( $id, $name, $value, $params = array() )
    {
      echo "<input type=\"checkbox\" id=\"" . $id . "\" name=\"" . $name . "\" value=\"1\" " . self::html_params( $params ) . ( $value ? "checked=\"checked\" " : "" ) . "/>";
    }

    public static function textarea( $id, $name, $value, $params = array() )
    {
      echo "<textarea id=\"" . $id . "\" name=\"" . $name . "\" " . self::html_params( $params ) . ">" . $value . "</textarea>";
    }

    public static function file( $id, $name, $value, $params = array() )
    {
      echo "<input type=\"file\" id=\"" . $id . "\" name=\"" . $name . "\" value=\"" . $value . "\" " . self::html_params( $params ) . "/>";
    }

    public static function select( $id, $name, $value, $options, $empty_option = false, $params = array() )
    {
      echo "<select id=\"" . $id . "\" name=\"" . $name . "\" " . self::html_params( $params ) . ">";

      if( $empty_option !== false )
      {
        echo "<option value=\"\">$empty_option</option>";
      }

      foreach( $options as $option_value => $option_name )
      {
        echo "<option value=\"" . htmlentities( $option_value ) . "\" " . ( $option_value == $value ? "selected=\"selected\"" : "" ) . ">" . $option_name . "</option>";
      }

      echo "</select>";
    }

    public static function link_button( $href, $label, $params = array() )
    {
      echo "<a href=\"" . $href . "\" class=\"linkButton\" " . self::html_params( $params ) . ">" . $label. "</a>";
    }

    public static function button( $value, $params = array() )
    {
      echo "<button type=\"button\" " . self::html_params( $params ) . ">" . $value. "</button>";
    }

    public static function back_button( $section, $type = "list", $id = "", $params = array() )
    {
      $href = self::get_section_url( $section, $type, $id );

      echo "<a href=\"" . $href . "\" class=\"linkButton\" " . self::html_params( $params ) . ">Back</a>";
    }

    /******************************************************************************/
		/****************************** HELPER FUNCTIONS ******************************/
		/******************************************************************************/
    public static function display_bool( $value )
    {
      return $value ? "Yes" : "No";
    }

    public static function if_null( $value, $default )
    {
      return $value != null ? $value : $default;
    }

    public static function get_current_date()
    {
      return date('Y-m-d');
    }

		/******************************************************************************/
    /******************************* COMMS FUNCTIONS ******************************/
    /******************************************************************************/
		public static function set_session_message( $id, $message )
    {
      $_SESSION["aqa_message_" . $id] = $message;
    }

		public static function get_session_message( $id )
    {
    	$message = $_SESSION["aqa_message_" . $id];
			$_SESSION["aqa_message_" . $id] = null;

			if( isset( $message ) )
			{
			  $session_message = "<div id=\"session-message\" class=\"ui-widget\">
        <div style=\"padding: 0pt 0.7em;\" class=\"ui-state-active ui-corner-all\">
          <p style=\"margin: 5px 0;\"><span style=\"float: left; margin:-0.1em 0.3em 0 0;\" class=\"ui-icon ui-icon-check\"></span>
          <strong>Success:</strong> " . $message . "</p>
        </div>
      </div>";

				echo $session_message;
			}
    }

    /******************************************************************************/
    /******************************* DEBUG FUNCTIONS ******************************/
    /******************************************************************************/
    public static function show_db_error( $sql )
    {
      $error_message = "<div id=\"db-error\" class=\"ui-widget\">
        <div class=\"ui-state-error ui-corner-all\" style=\"padding: 0 .7em;\">
          <p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>
          <strong>DATABASE ERROR #" . mysql_errno( get_connection() ) . ":</strong> " . mysql_error( get_connection() ). "</p>
          <p>" . $sql . "</p>
        </div>
      </div>";

      echo $error_message;
    }

    public static function debug_user_setup( $user_setup )
    {
      if( exists( $user_setup ) && $user_setup->is_persisted() )
      {
        $user_setup_info = "<div id=\"user_setup_info\" class=\"ui-widget\" style=\"display:none\">
                            <div class=\"ui-state-error ui-corner-all\" style=\"padding: 0 .7em;\">
                            USER SETUP:" . $user_setup->to_json() . "</div></div>";
      }

      echo $user_setup_info;
    }

    /******************************************************************************/
    /******************************* JSON FUNCTIONS *******************************/
    /******************************************************************************/
    public static function user_dept_json( $department_id = false )
    {
      $sql = "SELECT u.tkid AS value, CONCAT(u.tkname, ' ', u.tksurname) AS label, d.id as departmentId, d.value AS department
              FROM assist_" . get_company_code() . "_timekeep u
              JOIN assist_" . get_company_code() . "_menu_modules_users m ON m.usrtkid = u.tkid
              LEFT JOIN assist_" . get_company_code() . "_list_dept d ON u.tkdept = d.id
              WHERE m.usrmodref = 'AQA' ";

      if( $department_id != false )
      {
        $sql .= "AND d.id = " . $department_id;
      }

      $users = query_records( $sql );

      echo json_encode( $users );
    }

    public static function get_section_header( $section )
    {
      if( $section == 1 )
      {
        return "My Open Queries";
      }
      else if( $section == 2 )
      {
        return "My Open Actions";
      }
      else if( $section == 3 )
      {
        return "Queries to sign-off";
      }
      else if( $section == 4 )
      {
        return "Browser All Queries";
      }
      else if( $section == 5 )
      {
        return "New Query";
      }
    }

    public static function get_section_url( $section, $type = "list", $id = "" )
    {
      if( $section == 1 )
      {
        if( $type == "list" )
        {
          return "queries_open.php";
        }
        else if( $type == "info" )
        {
          return "assignment_info.php?id=" . $id . "&section=" . $section;
        }
      }
      else if( $section == 2 )
      {
        if( $type == "list" )
        {
          return "actions_open.php";
        }
        else if( $type == "info" )
        {
          return "action_info.php?id=" . $id . "&section=" . $section;
        }
      }
      else if( $section == 3 )
      {
        if( $type == "list" )
        {
          return "queries_signoff.php";
        }
        else if( $type == "info" )
        {
          return "assignment_info.php?id=" . $id . "&section=" . $section;
        }
      }
      else if( $section == 4 )
      {
        if( $type == "list" )
        {
          return "queries_all.php";
        }
        else if( $type == "info" )
        {
          return "assignment_info.php?id=" . $id . "&section=" . $section;
        }
      }
      else if( $section == 5 )
      {
        if( $type == "list" )
        {
          return "queries_open.php";
        }
      }
    }
  }
?>