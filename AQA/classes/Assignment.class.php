<?php
  class Assignment extends Base
  {
      var $counter = 0;
    /* --------------------------------------------------------------------------------------- */
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function __construct( $id = null )
	  {
            
	    $table = "aqa_assignment";
	    $fields = array( "query_id", "number", "assigned_to_id", "department_id", "deadline", "response", "is_policy_sop", "sop_assistance", "SOP_Policy_attachment", 
                             "response_attachment", "response_attachment_2", "response_attachment_3", "response_attachment_4", "response_attachment_5", "responded_on",
	                     "status", "completed_on", "closed_status", "closed_by_id", "closed_on", "reject_reason", "risk_reference_number", "risk_status",
	                     "further_actions", "bln_follow_up", "follow_up", "effective_controls", "created_by_id", "created_on", "last_updated_by_id", "last_updated_on");

	    parent::__construct( $table, $fields, $id );
	  }

	  /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- SAVE ------------------------------------------ */
    /* --------------------------------------------------------------------------------------- */
    function save($rejected=false)
    {
      if( $this->is_persisted() )
      {
          if( exists( $this->SOP_Policy_attachment ) )
        {
          $new_attachment = $this->upload_sop_policy_attachment();

          if( exists( $new_attachment ) )
          {
            $this->SOP_Policy_attachment = $new_attachment;
          }
        }
        else
        {
          $this->SOP_Policy_attachment = $this->upload_sop_policy_attachment();
        }
        if( exists( $this->response_attachment ) )
        {
          $new_attachment = $this->upload_response_attachment();

          if( exists( $new_attachment ) )
          {
            $this->response_attachment = $new_attachment;
          }
        }
        else
        {
          $this->response_attachment = $this->upload_response_attachment();
        }
        
        //        Add a second attachment
        if( exists( $this->response_attachment_2 ) )
        {
          $new_attachment = $this->upload_response_attachment_2();

          if( exists( $new_attachment ) )
          {
            $this->response_attachment_2 = $new_attachment;
          }
        }
        else
        {
          $this->response_attachment_2 = $this->upload_response_attachment_2();
        }
        
        //        Add a third attachment
        if( exists( $this->response_attachment_3 ) )
        {
          $new_attachment = $this->upload_response_attachment_3();

          if( exists( $new_attachment ) )
          {
            $this->response_attachment_3 = $new_attachment;
          }
        }
        else
        {
          $this->response_attachment_3 = $this->upload_response_attachment_3();
        }
        
        //        Add a fourth attachment
        if( exists( $this->response_attachment_4 ) )
        {
          $new_attachment = $this->upload_response_attachment_4();

          if( exists( $new_attachment ) )
          {
            $this->response_attachment_4 = $new_attachment;
          }
        }
        else
        {
          $this->response_attachment_4 = $this->upload_response_attachment_4();
        }
        
        //        Add a fifth attachment
        if( exists( $this->response_attachment_5 ) )
        {
          $new_attachment = $this->upload_response_attachment_5();

          if( exists( $new_attachment ) )
          {
            $this->response_attachment_5 = $new_attachment;
          }
        }
        else
        {
          $this->response_attachment_5 = $this->upload_response_attachment_5();
        }

        $this->last_updated_by_id = get_logged_in_user_id();
        $this->last_updated_on = date( "Y-m-d H:i:s" );

        if( $this->is_inprogress() && !$rejected )
        {
          $this->close_if_ready( false );
        }

        return $this->update();
      }
      else
      {
        $this->created_by_id = get_logged_in_user_id();
        $this->created_on = date( "Y-m-d H:i:s" );

        return $this->insert();
      }
    }
    
     public function pluscounter()
     {
         if ($this->counter < 5)
         {
             $this->counter++;
         }
//         return $this->counter;
     }

    

    private function upload_response_attachment()
    {
      if( exists( $_FILES['response_attachment_file'] ) )
      {
        $upload_dir = "/AQA/assignment/";

        AQAUtils::create_directory_if_not_exists( $upload_dir );

        $target = "../files/" . get_company_code() . $upload_dir . basename( $_FILES['response_attachment_file']['name'] );

        if( move_uploaded_file( $_FILES['response_attachment_file']['tmp_name'], $target ) )
        {
          return $target;
        }
      }
      
      return null;
    }
    
    private function upload_response_attachment_2()
    {
      if( exists( $_FILES['response_attachment_file_2'] ) )
      {
        $upload_dir = "/AQA/assignment/";

        AQAUtils::create_directory_if_not_exists( $upload_dir );

        $target = "../files/" . get_company_code() . $upload_dir . basename( $_FILES['response_attachment_file_2']['name'] );

        if( move_uploaded_file( $_FILES['response_attachment_file_2']['tmp_name'], $target ) )
        {
          return $target;
        }
      }
      
      return null;
    }
    
    private function upload_response_attachment_3()
    {
      if( exists( $_FILES['response_attachment_file_3'] ) )
      {
        $upload_dir = "/AQA/assignment/";

        AQAUtils::create_directory_if_not_exists( $upload_dir );

        $target = "../files/" . get_company_code() . $upload_dir . basename( $_FILES['response_attachment_file_3']['name'] );

        if( move_uploaded_file( $_FILES['response_attachment_file_3']['tmp_name'], $target ) )
        {
          return $target;
        }
      }
      
      return null;
    }
    
    private function upload_response_attachment_4()
    {
      if( exists( $_FILES['response_attachment_file_4'] ) )
      {
        $upload_dir = "/AQA/assignment/";

        AQAUtils::create_directory_if_not_exists( $upload_dir );

        $target = "../files/" . get_company_code() . $upload_dir . basename( $_FILES['response_attachment_file_4']['name'] );

        if( move_uploaded_file( $_FILES['response_attachment_file_4']['tmp_name'], $target ) )
        {
          return $target;
        }
      }
      
      return null;
    }
    
    private function upload_response_attachment_5()
    {
      if( exists( $_FILES['response_attachment_file_5'] ) )
      {
        $upload_dir = "/AQA/assignment/";

        AQAUtils::create_directory_if_not_exists( $upload_dir );

        $target = "../files/" . get_company_code() . $upload_dir . basename( $_FILES['response_attachment_file_5']['name'] );

        if( move_uploaded_file( $_FILES['response_attachment_file_5']['tmp_name'], $target ) )
        {
          return $target;
        }
      }
      
      return null;
    }
    
    private function upload_sop_policy_attachment ()
    {
        if( exists( $_FILES['SOP_Policy_attachment_file'] ) )
      {
        $upload_dir = "/AQA/assignment/";

        AQAUtils::create_directory_if_not_exists( $upload_dir );

        $target = "../files/" . get_company_code() . $upload_dir . basename( $_FILES['SOP_Policy_attachment_file']['name'] );

        if( move_uploaded_file( $_FILES['SOP_Policy_attachment_file']['tmp_name'], $target ) )
        {
          return $target;
        }
      }
      
      return null;
      }

    /*function close_if_ready( $save = true )
    {
      $open_actions = $this->get_open_actions();

      if( count( $open_actions ) === 0 && exists( $this->response ) )
      {
        $this->status = "Completed";
        $this->completed_on = DBHelper::get_now();

        if( $save )
        {
          $this->save();
        }

        EmailHelper::send_query_completed( $this );
      }
    }*/

    function mark_as_inprogress()
    {
      $this->status = "In-progress";
      $this->save();
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- DELETE ---------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function delete()
    {
      $this->delete_actions();

      return parent::delete();
    }

    function delete_actions()
    {
      $actions = $this->get_actions();

      if( count( $actions ) > 0 )
      {
        foreach( $actions as $action )
        {
          $action->delete();
        }
      }
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- VALIDATION ------------------------------------ */
    /* --------------------------------------------------------------------------------------- */
    function validate( $response = false, $sign_off = false )
    {
      $validator = new Validator();

      if( $response )
      {
        if( $validator->validate_required( $this->response, "Response" ) == false )
        {
          $this->set_field_error( "response", $validator->get_error_message() );
        }
      }

      if( $sign_off )
      {
        if( $validator->validate_required( $this->bln_follow_up, "Follow-up" ) == false )
        {
          $this->set_field_error( "bln_follow_up", $validator->get_error_message() );
        }

        if( $this->bln_follow_up ){
          if( $validator->validate_required( $this->follow_up, "Follow-up procedure" ) == false )
          {
            $this->set_field_error( "follow_up", $validator->get_error_message() );
          }
        }

        if( $validator->validate_required( $this->effective_controls, "Effective controls" ) == false )
        {
          $this->set_field_error( "effective_controls", $validator->get_error_message() );
        }

        if( $validator->validate_required( $this->closed_status, "Sign-off status" ) == false )
        {
          $this->set_field_error( "closed_status", $validator->get_error_message() );
        }
        else if( $this->closed_status == "Rejected" && $validator->validate_required( $this->reject_reason, "Reject reason" ) == false  )
        {
          $this->set_field_error( "reject_reason", $validator->get_error_message() );
        }

        if( $validator->validate_required( $this->risk_status, "Risk status" ) == false )
        {
          $this->set_field_error( "risk_status", $validator->get_error_message() );
        }
      }

      if( $validator->validate_required( $this->assigned_to_id, "User" ) == false )
      {
        $this->set_field_error( "assigned_to_id", $validator->get_error_message() );
      }

      if( $validator->validate_required( $this->department_id, "Department" ) == false )
      {
        $this->set_field_error( "department_id", $validator->get_error_message() );
      }

      if( $validator->validate_date( $this->deadline, "Deadline" ) == false )
      {
        $this->set_field_error( "deadline", $validator->get_error_message() );
      }

      return $this->has_field_error() === false;
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- HELPERS --------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function is_new()
    {
      return $this->status == "New";
    }

    function has_responded()
    {
      return $this->is_responded() || exists( $this->responded_on );
    }

    function is_responded()
    {
      return $this->status == "Responded";
    }

    function is_inprogress()
    {
      return $this->status == "In-progress";
    }

    function is_completed()
    {
      return $this->status == "Completed";
    }

    function is_closed()
    {
      return $this->status == "Closed";
    }

    function is_rejected()
    {
      return $this->closed_status == "Rejected";
    }

		/* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- LAZY LOADERS ---------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function get_query()
    {
      if( isset( $this->query ) == false )
      {
        $this->query = new Query( $this->query_id );
      }

      return $this->query;
    }

	  function get_assigned_to()
    {
    	if( isset( $this->assigned_to ) == false )
			{
        $this->assigned_to = User::load( $this->assigned_to_id );
			}

			return $this->assigned_to;
    }

    function get_department()
    {
      if( isset( $this->department ) == false )
      {
        $this->department = new Department( $this->department_id );
      }

      return $this->department;
    }

    function get_closed_by()
    {
      if( isset( $this->closed_by ) == false )
      {
        $this->closed_by = User::load( $this->closed_by_id );
      }

      return $this->closed_by;
    }

    function get_created_by()
    {
      if( isset( $this->created_by ) == false )
      {
        $this->created_by = User::load( $this->created_by_id );
      }

      return $this->created_by;
    }

    function get_last_updated_by()
    {
      if( isset( $this->last_updated_by ) == false )
      {
        $this->last_updated_by = User::load( $this->last_updated_by_id );
      }

      return $this->last_updated_by;
    }

    function get_actions( $status = false )
    {
      $action = new Action();

      if( $status != false )
      {
        return $action->get_records( "assignment_id = " . $this->id . " AND status = " . quote( $status ), "number asc" );
      }
      else
      {
        return $action->get_records( "assignment_id = " . $this->id, "number asc" );
      }
    }

    function get_open_actions()
    {
      $action = new Action();

      return $action->get_records( "assignment_id = " . $this->id . " AND status != 'Completed'", "number asc" );
    }
  }
?>
