<?php
class CSVGenerator
{
  public static function generate_queries_all_report( $filter )
  {
    $records = QueryHelper::get_query_data( $filter );

    $file_name = "../files/" . get_company_code() . "/query_data.csv";
    $fp = fopen( $file_name, w );

    $row = array(
      "Audit Ref",
      "Audit Type",
      "Audit Date",
      "Audit Finding",
      "Audit Recommendation",
      "Risk Level",
      "Risk",
      "Risk Detail",
      "Assignment #",
      "Assigned To",
      "Department",
      "Deadline",
      "Status",
      "Assignment Response",
      "Responded On",
      "Follow-up",
      "IA Follow-up Procedure",
      "Sign-off status",
      "Risk Status",
      "Further Actions"
    );

    fputcsv( $fp, $row );

    foreach( $records as $record )
    {
      $assignment = new Assignment( $record["id"] );

      $row = array(
        $assignment->get_query()->audit_ref,
        $assignment->get_query()->audit_type,
        $assignment->get_query()->audit_date,
        $assignment->get_query()->financial_year,
        $assignment->get_query()->assignment_information,  
        $assignment->get_query()->audit_finding,
        $assignment->get_query()->audit_recommendation,
        $assignment->get_query()->previously_queried,
        $assignment->get_query()->risk_level,
        $assignment->get_query()->get_risk()->name,
        $assignment->get_query()->risk_detail,
        $assignment->number,
        $assignment->get_assigned_to()->get_full_name(),
        $assignment->get_department()->value,
        $assignment->deadline,
        $assignment->status,
        $assignment->response,
        $assignment->is_policy_sop,
        $assignment->sop_assistance,
        $assignment->responded_on,
        $assignment->bln_follow_up,
        $assignment->follow_up,
        $assignment->effective_controls,
        $assignment->closed_status,
        $assignment->risk_status,
        $assignment->further_actions
      );

      fputcsv( $fp, $row );
    }

    fclose( $fp );

    return $file_name;
  }

}
?>