<?php
  class User
  {
    public static $TABLE_NAME = "timekeep";
    public static $DISPLAY_NAME = "User";

		public $id; //tkid
    public $title; //tktitle
    public $firstName; //tkname
    public $lastName; //tksurname
    public $email; //tkemail
    public $department;
    public $section;
    public $jobTitle;
    public $jobLevel;
    public $jobNumber;
    public $managerId;
    public $manager;
    public $managerEmail;
    public $gender;
    public $race;
    private $username;

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------- LAZY LOAD VARIABLES --------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    public $setup;

    /* --------------------------------------------------------------------------------------- */
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function __construct()
    {
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------- INTERFACE FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    public static function load( $id )
    {
      $obj = new User();

      if( $id == "0000" )
      {
        $obj->id = "0000";
        $obj->username = "admin";
        $obj->firstName = "Ignite Assist Administrator";
      }
      else
      {
        $sql = " SELECT * FROM assist_" . get_company_code() . "_" . self::$TABLE_NAME .
               " WHERE tkid = '{$id}' ";

        $obj->init( get_record( mysql_query( $sql, get_connection() ) ) );
      }

      return $obj;
    }

    public static function load_extended( $id )
    {
      $obj = new User();

      $sql  = "SELECT u.*, us.section, us.job_level, us.job_number, d.value AS department, j.value AS job_title, " .
              " (SELECT CONCAT( um.tkname, ' ', um.tksurname ) FROM assist_".get_company_code()."_" . self::$TABLE_NAME . " um WHERE um.tkid = u.tkmanager ) AS manager, " .
              " (SELECT um.tkemail FROM assist_".get_company_code()."_" . self::$TABLE_NAME . " um WHERE um.tkid = u.tkmanager ) AS manager_email, " .
              " (SELECT udfvvalue FROM assist_".get_company_code()."_udfvalue WHERE udfvindex = 4 AND udfvcode = u.tkrace ) AS race " .
              " FROM assist_".get_company_code()."_" . self::$TABLE_NAME . " u, " .
              " assist_".get_company_code()."_pp_user_setup us, " .
              " assist_".get_company_code()."_list_dept d, " .
              " assist_".get_company_code()."_list_jobtitle j " .
              " WHERE tkid = '" . $id . "' " .
              " AND us.user_id = u.tkid " .
              " AND d.id = u.tkdept " .
              " AND j.id = u.tkdesig ";

      $obj->init( get_record( mysql_query( $sql, get_connection() ) ), true );

      return $obj;
    }

    public static function get_records( $condition = false, $orderBy = "tkid" )
    {
      $sql = "SELECT u.* FROM assist_" . get_company_code() . "_" . self::$TABLE_NAME . " u, " .
             "assist_".get_company_code()."_menu_modules_users mu " .
             "WHERE u.tkid = mu.usrtkid AND mu.usrmodref = 'AQA' " .
             ( $condition ? "AND " . $condition : "" ) .
             " ORDER BY {$orderBy}";

		  $rs = get_resultset( $sql );

		  $records = array();
		  while( $record = get_record( $rs ) )
		  {
		    $obj = new User();
        $obj->init( $record );

		    $records[] = $obj;
		  }

		  return $records;
    }

    public function init( $values, $extended = false )
    {
      $this->setId( $values["tkid"] );
      $this->setTitle( $values["tktitle"] );
      $this->setFirstName( $values["tkname"] );
      $this->setLastName( $values["tksurname"] );
      $this->setEmail( $values["tkemail"] );
      $this->setManagerId( $values["tkmanager"] );
      $this->setGender( $values["tkgender"] );
      $this->setUsername( $values["tkuser"] );

      if( $extended )
      {
        $this->setDepartment( $values["department"] );
        $this->setSection( $values["section"] );
        $this->setJobTitle( $values["job_title"] );
        $this->setJobLevel( $values["job_level"] );
        $this->setJobNumber( $values["job_number"] );
        $this->setManager( $values["manager"] );
        $this->setManagerEmail( $values["manager_email"] );
        $this->setRace( $values["race"] );
      }
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------- GETTERS AND SETTERS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    public function setId($id)
    {
      $this->id = $id;
    }

    public function getId()
    {
      return $this->id;
    }

		public function setTitle($title)
    {
      $this->title = $title;
    }

    public function getTitle()
    {
      return $this->title;
    }

    public function setFirstName($firstName)
    {
      $this->firstName = $firstName;
    }

    public function getFirstName()
    {
      return $this->firstName;
    }

    public function setLastName($lastName)
    {
      $this->lastName = $lastName;
    }

    public function getLastName()
    {
      return $this->lastName;
    }

    public function setEmail($email)
    {
      $this->email = $email;
    }

    public function getEmail()
    {
      return $this->email;
    }

    public function setDepartment($department)
    {
      $this->department = $department;
    }

    public function getDepartment()
    {
      return $this->department;
    }

    public function setSection($section)
    {
      $this->section = $section;
    }

    public function getSection()
    {
      return $this->section;
    }

    public function setJobTitle($jobTitle)
    {
      $this->jobTitle = $jobTitle;
    }

    public function getJobTitle()
    {
      return $this->jobTitle;
    }

    public function setJobLevel($jobLevel)
    {
      $this->jobLevel = $jobLevel;
    }

    public function getJobLevel()
    {
      return $this->jobLevel;
    }

    public function setJobNumber($jobNumber)
    {
      $this->jobNumber = $jobNumber;
    }

    public function getJobNumber()
    {
      return $this->jobNumber;
    }

    public function setManagerId($managerId)
    {
      $this->managerId = $managerId;
    }

    public function getManagerId()
    {
      return $this->managerId;
    }

    public function setManager($manager)
    {
      $this->manager = $manager;
    }

    public function getManager()
    {
      return $this->manager;
    }

    public function setManagerEmail($managerEmail)
    {
      $this->managerEmail = $managerEmail;
    }

    public function getManagerEmail()
    {
      return $this->managerEmail;
    }

    public function setGender($gender)
    {
      $this->gender = $gender;
    }

    public function getGender()
    {
      return $this->gender;
    }

    public function setRace($race)
    {
      $this->race = $race;
    }

    public function getRace()
    {
      return $this->race;
    }

    public function setUsername( $username ) {
      $this->username = $username;
    }

    public function getUsername() {
      return $this->username;
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------- LAZY LOAD FUNCTIONS --------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    public function get_setup()
    {
      if( isset( $this->setup ) == false && $this->id != "0000" )
      {
        $this->setup = new UserSetup();
        $this->setup->load_from_user_id( $this->id );
        $this->setup->user = $this;
      }

      return $this->setup;
    }

    /* --------------------------------------------------------------------------------------- */
    /* ---------------------------------- HELPER FUNCTIONS ----------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    public static function select_list()
    {
      $list = array();

      foreach( self::records() as $record )
      {
        $list[] = array( $record->getId(), $record->getFullName() );
      }

      return $list;
    }

    public function get_full_name( $decode = false )
    {
      if( exists( $this->firstName ) && exists( $this->lastName ) )
      {
        return $decode ? html_entity_decode( $this->firstName . " " . $this->lastName, ENT_QUOTES, "ISO-8859-1" ) : $this->firstName . " " . $this->lastName;
      }

      return "";
    }

    public function get_gender_display()
    {
      return $this->gender == "M" ? "Male" : "Female";
    }

    public function is_super_user()
    {
      return $this->is_support_user() || $this->is_admin_user() || $this->get_setup()->module_admin;
    }

    public function is_support_user()
    {
      return $_SESSION["is_support"] == 1;
      /*return $this->username == "support";*/
    }

    public function is_admin_user()
    {
      return $this->id == "0000";
    }

    public function has_access( $access )
    {
      if( $this->is_super_user() )
      {
        return true;
      }

      if( $access == "query" && $this->get_setup()->create_query )
      {
        return true;
      }

      if( $access == "signoff_query" && $this->get_setup()->signoff_query )
      {
        return true;
      }

      if( $access == "setup" && $this->get_setup()->module_admin )
      {
        return true;
      }

      return false;
    }

  }
?>
