<?php
  class UserSetup extends Base
  {
    /* --------------------------------------------------------------------------------------- */
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function __construct( $id = null )
	  {
	    $table = "aqa_usersetup";
	    $fields = array( "user_id", "module_admin", "create_query", "signoff_query", "dept_notify", "all_notify" );

	    parent::__construct( $table, $fields, $id );
	  }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- VALIDATION ------------------------------------ */
    /* --------------------------------------------------------------------------------------- */
    function validate()
    {
      $validator = new Validator();

      return $this->has_field_error() === false;
    }

		/* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- HELPERS --------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function load_from_user_id( $id )
    {
      $this->load_where( "user_id = " . $this->quote( $id ) );
    }

    function init( $data )
    {
      parent::init( $data );
      $this->cleanup_boolean_data();
    }

    function cleanup_boolean_data()
    {
      if( $this->module_admin == null )
      {
        $this->module_admin = 0;
      }

      if( $this->create_query == null )
      {
        $this->create_query = 0;
      }

      if( $this->signoff_query == null )
      {
        $this->signoff_query = 0;
      }

      if( $this->dept_notify == null )
      {
        $this->dept_notify = 0;
      }

      if( $this->all_notify == null )
      {
        $this->all_notify = 0;
      }
    }

    function to_json()
    {
      return json_encode( array( "id"=>$this->id,
                                 "user_id"=>$this->user_id,
                                 "module_admin"=>$this->module_admin,
                                 "create_query"=>$this->create_query,
                                 "signoff_query"=>$this->signoff_query,
                                 "dept_notify"=>$this->dept_notify,
                                 "all_notify"=>$this->all_notify ) );
    }

		/* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- LAZY LOADERS ---------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function get_user()
    {
    	if( isset( $this->user ) == false )
			{
        $this->user = User::load( $this->user_id );
			}

			return $this->user;
    }
  }
?>
