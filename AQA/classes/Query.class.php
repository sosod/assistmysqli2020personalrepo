<?php
  class Query extends Base
  {
    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- CONSTRUCTOR ----------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function __construct( $id = null )
	  {
	    $table = "aqa_query";
	    $fields = array( "audit_type", "audit_ref", "audit_date", "financial_year", "assignment_information", "audit_finding", "audit_recommendation", "audit_doc",
	                     "previously_queried","risk_level", "risk_id", "risk_detail", "created_by_id", "created_on", "last_updated_by_id", "last_updated_on" );

	    parent::__construct( $table, $fields, $id );
	  }

	  /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- SAVE ------------------------------------------ */
    /* --------------------------------------------------------------------------------------- */
    function save()
    {
      if( $this->is_persisted() )
      {
        if( exists( $this->audit_doc ) )
        {
          $new_doc = $this->upload_audit_doc();

          if( exists( $new_doc ) )
          {
            $this->audit_doc = $new_doc;
          }
        }
        else
        {
          $this->audit_doc = $this->upload_audit_doc();
        }

        $this->last_updated_by_id = get_logged_in_user_id();
        $this->last_updated_on = DBHelper::get_now();

        return $this->update();
      }
      else
      {
        //upload attachment if exists and set path onto query
        $this->audit_doc = $this->upload_audit_doc();
        $this->created_by_id = get_logged_in_user_id();
        $this->created_on = DBHelper::get_now();

        $result = $this->insert();

        if( $result )
        {
          //create the assignment records
          $this->create_assignment_records();

          return true;
        }

        return false;
      }
    }

    private function upload_audit_doc()
    {
      if( exists( $_FILES['audit_doc_file'] ) )
      {
        $upload_dir = "/AQA/query/";

        AQAUtils::create_directory_if_not_exists( $upload_dir );

        $target = "../files/" . get_company_code() . $upload_dir . basename( $_FILES['audit_doc_file']['name'] );

        if( move_uploaded_file( $_FILES['audit_doc_file']['tmp_name'], $target ) )
        {
          return $target;
        }
      }
      
      return null;
    }

    private function create_assignment_records()
    {
      $number = 0;

      if( exists( $this->user1_id ) )
      {
        debug("saving user 1");
        $number++;

         $assignment = new Assignment();
        $assignment->query_id = $this->id;
        $assignment->number = $number;
        $assignment->assigned_to_id = $this->user1_id;
        $assignment->department_id = $this->user1_department_id;
        $assignment->deadline = $this->user1_deadline;
        $assignment->status = 'New';
        
//        $assignment = new Assignment();
//        $assignment->query_id = $this->id;
//        $assignment->number = $number;
//        $assignment->assigned_to_id = $this->user1_id;
//        $assignment->department_id = $this->user1_department_id;
//        $assignment->deadline = $this->user1_deadline;
//        $assignment->status = 'New';

        $result = $assignment->save();

        debug("user 1 result = " . $result );

        if( $result == false )
        {
          View::show_db_error( $assignment->get_sql() );
        }
        else
        {
          //send new query notification
          $assignment->query = $this;
          EmailHelper::send_query_assignment( $assignment );
        }
      }

      if( exists( $this->user2_id ) )
      {
        $number++;

        $assignment = new Assignment();
        $assignment->query_id = $this->id;
        $assignment->number = $number;
        $assignment->assigned_to_id = $this->user2_id;
        $assignment->department_id = $this->user2_department_id;
        $assignment->deadline = $this->user2_deadline;
        $assignment->status = 'New';

        $result = $assignment->save();

        debug("user 2 result = " . $result );

        if( $result == false )
        {
          View::show_db_error( $assignment->get_sql() );
        }
        else
        {
          //send new query notification
          $assignment->query = $this;
          EmailHelper::send_query_assignment( $assignment );
        }
      }

      if( exists( $this->user3_id ) )
      {
        $number++;

        $assignment = new Assignment();
        $assignment->query_id = $this->id;
        $assignment->number = $number;
        $assignment->assigned_to_id = $this->user3_id;
        $assignment->department_id = $this->user3_department_id;
        $assignment->deadline = $this->user3_deadline;
        $assignment->status = 'New';

        $result = $assignment->save();

        debug("user 3 result = " . $result );

        if( $result == false )
        {
          View::show_db_error( $assignment->get_sql() );
        }
        else
        {
          //send new query notification
          $assignment->query = $this;
          EmailHelper::send_query_assignment( $assignment );
        }
      }

      if( exists( $this->user4_id ) )
      {
        $number++;

        $assignment = new Assignment();
        $assignment->query_id = $this->id;
        $assignment->number = $number;
        $assignment->assigned_to_id = $this->user4_id;
        $assignment->department_id = $this->user4_department_id;
        $assignment->deadline = $this->user4_deadline;
        $assignment->status = 'New';

        $result = $assignment->save();

        debug("user 4 result = " . $result );

        if( $result == false )
        {
          View::show_db_error( $assignment->get_sql() );
        }
        else
        {
          //send new query notification
          $assignment->query = $this;
          EmailHelper::send_query_assignment( $assignment );
        }
      }

      if( exists( $this->user5_id ) )
      {
        $number++;

        $assignment = new Assignment();
        $assignment->query_id = $this->id;
        $assignment->number = $number;
        $assignment->assigned_to_id = $this->user5_id;
        $assignment->department_id = $this->user5_department_id;
        $assignment->deadline = $this->user5_deadline;
        $assignment->status = 'New';

        $result = $assignment->save();

        debug("user 5 result = " . $result );

        if( $result == false )
        {
          View::show_db_error( $assignment->get_sql() );
        }
        else
        {
          //send new query notification
          $assignment->query = $this;
          EmailHelper::send_query_assignment( $assignment );
        }
      }
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- DELETE ---------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function delete()
    {
      $this->delete_assignments();

      return parent::delete();
    }

    function delete_assignments()
    {
      $assignments = $this->get_assignments();

      if( count( $assignments ) > 0 )
      {
        foreach( $assignments as $assignment )
        {
          $assignment->delete();
        }
      }
    }

	  /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- VALIDATION ------------------------------------ */
    /* --------------------------------------------------------------------------------------- */
    function validate()
    {
      $validator = new Validator();

      if( $validator->validate_required( $this->audit_type, "Audit Type" ) == false )
      {
        $this->set_field_error( "audit_type", $validator->get_error_message() );
      }

      if( $validator->validate_required( $this->audit_ref, "Audit Ref" ) == false )
      {
        $this->set_field_error( "audit_ref", $validator->get_error_message() );
      }

      if( $validator->validate_required( $this->assignment_information, "Subject / Assignment Information" ) == false )
      {
        $this->set_field_error( "assignment_information", $validator->get_error_message() );
      }
      
      if( $validator->validate_required( $this->audit_date, "Audit Date" ) == false )
      {
        $this->set_field_error( "audit_date", $validator->get_error_message() );
      }
      
      if( $validator->validate_required( $this->financial_year, "Financial Year" ) == false )
      {
        $this->set_field_error( "financial_year", $validator->get_error_message() );
      }

      if( $validator->validate_required( $this->audit_finding, "Audit Finding" ) == false )
      {
        $this->set_field_error( "audit_finding", $validator->get_error_message() );
      }

      if( $validator->validate_required( $this->audit_recommendation, "Audit Recommendation" ) == false )
      {
        $this->set_field_error( "audit_recommendation", $validator->get_error_message() );
      }
      
//      if( $validator->validate_required( $this->previously_queried, "Previously Queried" ) == false )
//      {
//        $this->set_field_error( "previously_queried", $validator->get_error_message() );
//      }

      //if any of the risk values are specified, then all need to be filled in
      if( exists( $this->risk_level ) || exists( $this->risk_id ) || exists( $this->risk_detail ) )
      {
        if( $validator->validate_required( $this->risk_level, "Risk Level" ) == false )
        {
          $this->set_field_error( "risk_level", $validator->get_error_message() );
        }

        if( $validator->validate_required( $this->risk_id, "Risk" ) == false )
        {
          $this->set_field_error( "risk_id", $validator->get_error_message() );
        }

        if( $validator->validate_required( $this->risk_detail, "Risk Detail" ) == false )
        {
          $this->set_field_error( "risk_detail", $validator->get_error_message() );
        }
      }

      /* VALIDATE ASSIGNMENT USERS */
      if( $this->is_transient() )
      {
        if( $validator->validate_required( $this->user1_id, "User 1" ) == false )
        {
          $this->set_field_error( "user1_id", $validator->get_error_message() );
        }
        else if( $validator->validate_date( $this->user1_deadline, "User 1 deadline" ) == false )
        {
          debug( "setting deadline msg: " . $validator->get_error_message() );
          $this->set_field_error( "user1_deadline", $validator->get_error_message() );
        }

        if( exists( $this->user2_id ) )
        {
          if( $validator->validate_date( $this->user2_deadline, "User 2 deadline" ) == false )
          {
            $this->set_field_error( "user2_deadline", $validator->get_error_message() );
          }
        }

        if( exists( $this->user3_id ) )
        {
          if( $validator->validate_date( $this->user3_deadline, "User 3 deadline" ) == false )
          {
            $this->set_field_error( "user3_deadline", $validator->get_error_message() );
          }
        }

        if( exists( $this->user4_id ) )
        {
          if( $validator->validate_date( $this->user4_deadline, "User 4 deadline" ) == false )
          {
            $this->set_field_error( "user4_deadline", $validator->get_error_message() );
          }
        }

        if( exists( $this->user5_id ) )
        {
          if( $validator->validate_date( $this->user5_deadline, "User 5 deadline" ) == false )
          {
            $this->set_field_error( "user5_deadline", $validator->get_error_message() );
          }
        }
      }

      return $this->has_field_error() === false;
    }

		/* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- HELPERS --------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function set_assignment_data( $data )
    {
      $this->user1 = $data["user1"];
      $this->user1_id = $data["user1_id"];
      $this->user1_department = $data["user1_department"];
      $this->user1_department_id = $data["user1_department_id"];
      $this->user1_deadline = $data["user1_deadline"];

      $this->user2 = $data["user2"];
      $this->user2_id = $data["user2_id"];
      $this->user2_department = $data["user2_department"];
      $this->user2_department_id = $data["user2_department_id"];
      $this->user2_deadline = $data["user2_deadline"];

      $this->user3 = $data["user3"];
      $this->user3_id = $data["user3_id"];
      $this->user3_department = $data["user3_department"];
      $this->user3_department_id = $data["user3_department_id"];
      $this->user3_deadline = $data["user3_deadline"];

      $this->user4 = $data["user4"];
      $this->user4_id = $data["user4_id"];
      $this->user4_department = $data["user4_department"];
      $this->user4_department_id = $data["user4_department_id"];
      $this->user4_deadline = $data["user4_deadline"];

      $this->user5 = $data["user5"];
      $this->user5_id = $data["user5_id"];
      $this->user5_department = $data["user5_department"];
      $this->user5_department_id = $data["user5_department_id"];
      $this->user5_deadline = $data["user5_deadline"];
    }

    function get_num_of_assigned_users()
    {
      $cnt = 0;

      if( exists( $this->user1_id ) )
      {
        $cnt++;
      }

      if( exists( $this->user2_id ) )
      {
        $cnt++;
      }

      if( exists( $this->user3_id ) )
      {
        $cnt++;
      }

      if( exists( $this->user4_id ) )
      {
        $cnt++;
      }

      if( exists( $this->user5_id ) )
      {
        $cnt++;
      }

      return $cnt;
    }

		/* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- LAZY LOADERS ---------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function get_risk()
    {
      if( isset( $this->risk ) == false )
      {
        $this->risk = new Risk( $this->risk_id );
      }

      return $this->risk;
    }

    function get_created_by()
    {
      if( isset( $this->created_by ) == false )
      {
        $this->created_by = User::load( $this->created_by_id );
      }

      return $this->created_by;
    }

    function get_last_updated_by()
    {
      if( isset( $this->last_updated_by ) == false )
      {
        $this->last_updated_by = User::load( $this->last_updated_by_id );
      }

      return $this->last_updated_by;
    }

    function get_assignments()
    {
      $assignment = new Assignment();

      return $assignment->get_records( "query_id = " . $this->id );
    }
  }
?>
