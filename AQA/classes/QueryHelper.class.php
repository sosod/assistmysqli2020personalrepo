<?php
  class QueryHelper
  {
    /******************************************************************************/
    /******************************* LIST DATA FUNCTIONS **************************/
    /******************************************************************************/
    public static function get_user_assignment_data()
    {
      $sql = "SELECT q.audit_ref, q.audit_type, q.audit_date, assignment_information, a.number, a.deadline, a.status, a.id
              FROM assist_" . get_company_code() . "_aqa_assignment a
              JOIN assist_" . get_company_code() . "_aqa_query q ON q.id = a.query_id
              WHERE a.status not in ('Closed','Completed')
              AND a.assigned_to_id = '" . get_logged_in_user_id() . "'
              ORDER BY a.status, a.deadline desc";

      return query_records( $sql );
    }
    
    public static function get_queries( $filter )
    {
      $sql_select = "SELECT q.* ";
      $sql_from = "FROM assist_" . get_company_code() . "_aqa_query q
                   JOIN assist_" . get_company_code() . "_aqa_assignment a ON q.id = a.query_id ";

      $sql_where = "";

      $has_where = false;

      if( exists( $filter->audit_type ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " q.audit_type = " . quote( $filter->audit_type ) . " ";
        $has_where = true;
      }
      
      if( exists( $filter->financial_year ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " q.financial_year = " . quote( $filter->financial_year ) . " ";
        $has_where = true;
      }

      if( exists( $filter->risk_level ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " q.risk_level = " . quote( $filter->risk_level ) . " ";
        $has_where = true;
      }

      if( exists( $filter->risk_id ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " q.risk_id = " . quote( $filter->risk_id ) . " ";
        $has_where = true;
      }

      if( exists( $filter->user_id ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " a.assigned_to_id = " . quote( $filter->user_id ) . " ";
        $has_where = true;
      }

      if( exists( $filter->department_id ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " a.department_id = " . quote( $filter->department_id ) . " ";
        $has_where = true;
      }

      if( exists( $filter->status ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " a.status = " . quote( $filter->status ) . " ";
        $has_where = true;
      }
      
      if ( exists( $filter->assignment_information ) )
      {
          $sql_where .= ( $has_where ? "And" : "Where" ) . " q.assignment_information like  " . quote( "%" . $filter->assignment_information . "%" ) . " ";
          $has_where = true;
      }
      
      if ( exists( $filter->previously_queried ) )
      {
          $sql_where .= ( $has_where ? "And" : "Where" ) . " q.previously_queried =  " .  $filter->previously_queried    . " ";
          $has_where = true;
      }

      $sql_order = "ORDER BY q.created_on";
      
      
      $rs = get_resultset( $sql_select . $sql_from . $sql_where . $sql_order );
      
      $records = array();

      while( $record = get_record( $rs ) )
      {
        $query = new Query();
        $query->init( $record );
    
        $records[] = $query;
      }

      return $records;
    }

    public static function get_query_data( $filter, &$pager = null )
    {
      $sql_select = "SELECT q.audit_ref, q.audit_type, q.audit_date, q.financial_year, q.assignment_information, q.previously_queried, a.number, a.deadline, a.status, a.id, q.risk_level, r.name AS risk, CONCAT(u.tkname, ' ', u.tksurname) AS user ";
      $sql_count = "SELECT count(*) AS cnt ";
      $sql_from = "FROM assist_" . get_company_code() . "_aqa_assignment a
                   JOIN assist_" . get_company_code() . "_aqa_query q ON q.id = a.query_id
                   LEFT JOIN assist_" . get_company_code() . "_timekeep u ON u.tkid = a.assigned_to_id
                   LEFT JOIN assist_" . get_company_code() . "_aqa_risk r ON r.id = q.risk_id ";

      $sql_where = "";

      $has_where = false;

      if( exists( $filter->search ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " ( q.audit_ref like " . quote( "%" . $filter->search . "%" ) . " OR " .
                                                         "   q.audit_finding like " . quote( "%" . $filter->search . "%" ) . " OR " .
                                                         "   q.assignment_information like " . quote( "%" . $filter->search . "%" ) . " OR " .
                                                         "   q.audit_recommendation like " . quote( "%" . $filter->search . "%" ) . " OR " .
                                                         "   q.risk_detail like " . quote( "%" . $filter->search . "%" ) . " OR " .
                                                         "   a.response like " . quote( "%" . $filter->search . "%" ) . " OR " .
                                                         "   a.is_policy_sop like " . quote("%" . $filter->search . "%") . "or" .
                                                         "   a.reject_reason like " . quote( "%" . $filter->search . "%" ) . " OR " .
                                                         "   a.further_actions like " . quote( "%" . $filter->search . "%" ) . " )";
        $has_where = true;
      }

      if( exists( $filter->audit_type ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " q.audit_type = " . quote( $filter->audit_type ) . " ";
        $has_where = true;
      }

      if( exists( $filter->audit_date ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " q.audit_date = " . quote( $filter->audit_date ) . " ";
        $has_where = true;
      }
      
      if( exists( $filter->financial_year ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " q.financial_year = " . quote( $filter->financial_year ) . " ";
        $has_where = true;
      }

      if( exists( $filter->risk_level ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " q.risk_level = " . quote( $filter->risk_level ) . " ";
        $has_where = true;
      }

      if( exists( $filter->risk_id ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " q.risk_id = " . quote( $filter->risk_id ) . " ";
        $has_where = true;
      }

      if( exists( $filter->user_id ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " a.assigned_to_id = " . quote( $filter->user_id ) . " ";
        $has_where = true;
      }

      if( exists( $filter->department_id ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " a.department_id = " . quote( $filter->department_id ) . " ";
        $has_where = true;
      }

      if( exists( $filter->deadline ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " a.deadline = " . quote( $filter->deadline ) . " ";
        $has_where = true;
      }

      if( exists( $filter->status ) )
      {
        $sql_where .= ( $has_where ? "AND" : "WHERE" ) . " a.status = " . quote( $filter->status ) . " ";
        $has_where = true;
      }
      
      if ( exists( $filter->assignment_information ) )
      {
          $sql_where .= ( $has_where ? "And" : "Where" ) . " q.assignment_information like  " . quote( "%" . $filter->assignment_information . "%" ) . " ";
          $has_where = true;
      }
      
      if ( exists( $filter->previously_queried ) )
      {
          $sql_where .= ( $has_where ? "And" : "Where" ) . " q.previously_queried =  " .  $filter->previously_queried    . " ";
          $has_where = true;
      }
      
      if ( exists ($filter->start_date))
      {
          $sql_where .= ( $has_where ? "And" : "Where" ) . " q.audit_date >=  " . quote( $filter->start_date ) . " ";
          $has_where = true;
      }
      
      if ( exists ($filter->end_date))
      {
          $sql_where .= ( $has_where ? "And" : "Where" ) . " q.audit_date <=  " . quote( $filter->end_date ) . " ";
          $has_where = true;
      }

      $sql_order = "ORDER BY a.status, a.deadline desc ";

      if( isset( $pager ) )
      {
        $sql_limit = "LIMIT " . $pager->get_start() . "," . $pager->records_per_page;

        $pager->total_records = query_count( $sql_count . $sql_from . $sql_where );

        $pager->calculate_values();
      }

      return query_records(  $sql_select . $sql_from . $sql_where . $sql_order . $sql_limit );
    }

    public static function get_signoff_query_data()
    {
      $sql = "SELECT q.audit_ref, q.audit_type, q.audit_date, q.assignment_information, a.number, a.deadline, a.status, a.id, q.risk_level, r.name AS risk, CONCAT(u.tkname, ' ', u.tksurname) AS user
              FROM assist_" . get_company_code() . "_aqa_assignment a
              JOIN assist_" . get_company_code() . "_aqa_query q ON q.id = a.query_id
              LEFT JOIN assist_" . get_company_code() . "_timekeep u ON u.tkid = a.assigned_to_id
              LEFT JOIN assist_" . get_company_code() . "_aqa_risk r ON r.id = q.risk_id
              WHERE a.status = 'Completed'";

      return query_records( $sql );
    }

    public static function get_query_overdue_data( $filter, &$pager = null )
    {
      $sql_select = "SELECT q.audit_ref, q.audit_type, q.audit_date, financial_year, q.assignment_information, q.previously_queried, a.number, a.deadline, a.completed_on, a.status, a.id, q.risk_level, CONCAT(u.tkname, ' ', u.tksurname) AS user, IF( a.completed_on IS NULL, TO_DAYS(CURDATE()) - TO_DAYS(a.deadline), TO_DAYS(a.completed_on) - TO_DAYS(a.deadline) ) AS days_overdue ";
      $sql_count = "SELECT count(*) AS cnt ";
      $sql_from = "FROM assist_" . get_company_code() . "_aqa_assignment a
                   JOIN assist_" . get_company_code() . "_aqa_query q ON q.id = a.query_id
                   LEFT JOIN assist_" . get_company_code() . "_timekeep u ON u.tkid = a.assigned_to_id ";

      $sql_where = "WHERE ( a.deadline < DATE( a.completed_on ) || ( a.status != 'Completed' AND a.status != 'Closed' AND a.deadline < CURDATE() ) ) ";

      if( exists( $filter->search ) )
      {
        $sql_where .= "AND ( q.audit_ref like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   q.audit_finding like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   q.audit_recommendation like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   q.risk_detail like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   a.response like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   a.is_policy_sop like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   a.reject_reason like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   a.further_actions like " . quote( "%" . $filter->search . "%" ) . " )";
      }

      if( exists( $filter->audit_type ) )
      {
        $sql_where .= "AND q.audit_type = " . quote( $filter->audit_type ) . " ";
      }

      if( exists( $filter->audit_date ) )
      {
        $sql_where .= "AND q.audit_date = " . quote( $filter->audit_date ) . " ";
      }
      
      if( exists( $filter->financial_year ) )
      {
        $sql_where .= "AND q.financial_year = " . quote( $filter->financial_year ) . " ";
      }

      if( exists( $filter->risk_level ) )
      {
        $sql_where .= "AND q.risk_level = " . quote( $filter->risk_level ) . " ";
      }

      if( exists( $filter->risk_id ) )
      {
        $sql_where .= "AND q.risk_id = " . quote( $filter->risk_id ) . " ";
      }

      if( exists( $filter->user_id ) )
      {
        $sql_where .= "AND a.assigned_to_id = " . quote( $filter->user_id ) . " ";
      }

      if( exists( $filter->department_id ) )
      {
        $sql_where .= "AND a.department_id = " . quote( $filter->department_id ) . " ";
      }

      if( exists( $filter->deadline ) )
      {
        $sql_where .= "AND a.deadline = " . quote( $filter->deadline ) . " ";
      }

      if( exists( $filter->status ) )
      {
        $sql_where .= "AND a.status = " . quote( $filter->status ) . " ";
      }

      if( exists($filter->assignment_information))
      {
          $sql_where .= "AND q.assignment_information like " . quote("%" . $filter->assignment_information . "%" ) . "";
      }
      
      if ( exists( $filter->previously_queried ) )
      {
          $sql_where .=  "And  q.previously_queried =  " .  $filter->previously_queried    . " ";
      }
      
      if ( exists($filter->start_date) )
      {
          $sql_where .=  "AND q.audit_date >=  " . quote( $filter->start_date ) . " ";
      }
      
      if ( exists($filter->end_date) )
      {
          $sql_where .= "AND q.audit_date <=  " . quote( $filter->end_date ) . " ";
      }
      
      $sql_order = "ORDER BY a.status, a.deadline desc ";

      if( isset( $pager ) )
      {
        $sql_limit = "LIMIT " . $pager->get_start() . "," . $pager->records_per_page;

        $pager->total_records = query_count( $sql_count . $sql_from . $sql_where );

        $pager->calculate_values();
      }

      return query_records(  $sql_select . $sql_from . $sql_where . $sql_order . $sql_limit );
    }

    public static function get_action_overdue_data( $filter, &$pager = null )
    {
      $sql_select = "SELECT q.audit_ref, q.financial_year, q.assignment_information, q.previously_queried, a.number AS assignment_number, ac.number AS action_number, ac.instruction, ac.deadline, ac.completed_on, ac.status, ac.progress, ac.id, CONCAT(u.tkname, ' ', u.tksurname) AS user, IF( ac.completed_on IS NULL, TO_DAYS(CURDATE()) - TO_DAYS(ac.deadline), TO_DAYS(ac.completed_on) - TO_DAYS(ac.deadline) ) AS days_overdue ";
      $sql_count = "SELECT count(*) AS cnt ";
      $sql_from = "FROM assist_" . get_company_code() . "_aqa_action ac
                   JOIN assist_" . get_company_code() . "_aqa_assignment a ON a.id = ac.assignment_id
                   JOIN assist_" . get_company_code() . "_aqa_query q ON q.id = a.query_id
                   LEFT JOIN assist_" . get_company_code() . "_timekeep u ON u.tkid = ac.assigned_to_id ";

      $sql_where = "WHERE ( ac.deadline < DATE( ac.completed_on ) || ( ac.status != 'Completed' AND ac.deadline < CURDATE() ) ) ";

      if( exists( $filter->search ) )
      {
        $sql_where .= "AND ( ac.instruction like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   q.audit_ref like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   q.assignment_information like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   q.audit_finding like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   q.audit_recommendation like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   q.risk_detail like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   a.response like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   a.is_policy_sop like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   a.reject_reason like " . quote( "%" . $filter->search . "%" ) . " OR " .
                      "   a.further_actions like " . quote( "%" . $filter->search . "%" ) . " )";
      }

      if( exists( $filter->audit_type ) )
      {
        $sql_where .= "AND q.audit_type = " . quote( $filter->audit_type ) . " ";
      }

      if( exists( $filter->audit_date ) )
      {
        $sql_where .= "AND q.audit_date = " . quote( $filter->audit_date ) . " ";
      }

      if( exists( $filter->financial_year ) )
      {
        $sql_where .= "AND q.financial_year = " . quote( $filter->financial_year ) . " ";
      }
      
      if( exists( $filter->risk_level ) )
      {
        $sql_where .= "AND q.risk_level = " . quote( $filter->risk_level ) . " ";
      }

      if( exists( $filter->risk_id ) )
      {
        $sql_where .= "AND q.risk_id = " . quote( $filter->risk_id ) . " ";
      }

      if( exists( $filter->user_id ) )
      {
        $sql_where .= "AND a.assigned_to_id = " . quote( $filter->user_id ) . " ";
      }

      if( exists( $filter->department_id ) )
      {
        $sql_where .= "AND a.department_id = " . quote( $filter->department_id ) . " ";
      }

      if( exists( $filter->deadline ) )
      {
        $sql_where .= "AND a.deadline = " . quote( $filter->deadline ) . " ";
      }

      if( exists( $filter->status ) )
      {
        $sql_where .= "AND a.status = " . quote( $filter->status ) . " ";
      }

      if( exists($filter->assignment_information) )
      {
          $sql_where .= "AND q.assignment_information like " . quote( "%" . $filter->assignment_information . "%" ) . " ";
      }
      
      if ( exists( $filter->previously_queried ) )
      {
          $sql_where .=  "And  q.previously_queried =  " .  $filter->previously_queried    . " ";
      }
      
      if ( exists($filter->start_date) )
      {
          $sql_where .=  "AND q.audit_date >=  " . quote( $filter->start_date ) . " ";
      }
      
      if ( exists($filter->end_date) )
      {
          $sql_where .= "AND q.audit_date <=  " . quote( $filter->end_date ) . " ";
      }
      
      if( exists( $filter->action_user_id ) )
      {
        $sql_where .= "AND ac.assigned_to_id = " . quote( $filter->action_user_id ) . " ";
      }

      if( exists( $filter->action_status ) )
      {
        $sql_where .= "AND ac.status = " . quote( $filter->action_status ) . " ";
      }

      $sql_order = "ORDER BY ac.status, ac.deadline desc ";

      if( isset( $pager ) )
      {
        $sql_limit = "LIMIT " . $pager->get_start() . "," . $pager->records_per_page;

        $pager->total_records = query_count( $sql_count . $sql_from . $sql_where );

        $pager->calculate_values();
      }

      return query_records(  $sql_select . $sql_from . $sql_where . $sql_order . $sql_limit );
    }
  }
?>