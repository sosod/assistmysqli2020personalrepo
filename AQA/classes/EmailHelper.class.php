<?php
class EmailHelper
{
  private static $from_email = "Ignite Assist <no-reply@ignite4u.co.za>";
  private static $reply_to_email = "Ignite Advisory Services <helpdesk@igniteconsult.co.za>";
  

  public static function send_query_assignment( $assignment, $template = "templates/query_assignment.tpl", $subject = "Query Assigned" )
  {
    $data = self::get_assignment_data_model( $assignment );
    $body = self::process_template( $template, $data );

    $recipients = array( $assignment->get_assigned_to()->email );
    $recipients = array_merge( $recipients, array_diff( self::get_shadow_recipients( $assignment->department_id ), $recipients ) );

    self::send( $recipients, $body, $subject );
  }

  public static function send_query_completed( $assignment, $template = "templates/query_completed.tpl", $subject = "Query Completed" )
  {
    $data = self::get_assignment_data_model( $assignment );
    $body = self::process_template( $template, $data );

    $recipients = array( $assignment->get_created_by()->email );
    $recipients = array_merge( $recipients, array_diff( self::get_shadow_recipients( $assignment->department_id ), $recipients ) );
    $recipients = array_merge( $recipients, array_diff( self::get_signoff_recipients(), $recipients ) );

    self::send( $recipients, $body, $subject );
  }

  public static function send_query_signed_off( $assignment, $template = "templates/query_signed_off.tpl", $subject = "Query Signed-off" )
  {
    $data = self::get_assignment_data_model( $assignment );
    $body = self::process_template( $template, $data );

    $recipients = array( $assignment->get_created_by()->email );
    $recipients = array_merge( $recipients, array_diff( self::get_shadow_recipients( $assignment->department_id ), $recipients ) );
    $recipients = array_merge( $recipients, array_diff( self::get_signoff_recipients(), $recipients ) );

    self::send( $recipients, $body, $subject );
  }

    public static function send_query_rejected( $assignment, $template = "templates/query_rejected.tpl", $subject = "Query Rejected" )
    {
        $data = self::get_assignment_data_model( $assignment );
        $body = self::process_template( $template, $data );

        $recipients = array( $assignment->get_assigned_to()->email );
        $recipients = array_merge( $recipients, array_diff( self::get_shadow_recipients( $assignment->department_id ), $recipients ) );
        $recipients = array_merge( $recipients, array_diff( self::get_signoff_recipients(), $recipients ) );

        self::send( $recipients, $body, $subject );
    }

  public static function send_action_assignment( $action, $template = "templates/action_assignment.tpl", $subject = "Action Assigned" )
  {
    $data = self::get_action_data_model( $action );
    $body = self::process_template( $template, $data );

    $recipients = array( $action->get_assigned_to()->email );
    $recipients = array_merge( $recipients, array_diff( array( $action->get_assignment()->get_assigned_to()->email ), $recipients ) );
    $recipients = array_merge( $recipients, array_diff( self::get_shadow_recipients( $action->get_assignment()->department_id ), $recipients ) );

    self::send( $recipients, $body, $subject );
  }

  public static function send_action_completed( $action, $template = "templates/action_completed.tpl", $subject = "Action Completed" )
  {
    $data = self::get_action_data_model( $action );
    $body = self::process_template( $template, $data );

    $recipients = array( $action->get_assigned_to()->email );
    $recipients = array_merge( $recipients, array_diff( array( $action->get_assignment()->get_assigned_to()->email ), $recipients ) );
    $recipients = array_merge( $recipients, array_diff( self::get_shadow_recipients( $action->get_assignment()->department_id ), $recipients ) );

    self::send( $recipients, $body, $subject );
  }

  public static function send_action_reminder( $raw_data, $template = "templates/action_reminder.tpl", $subject = "Action Reminder" )
  {
    $data = array( "action_number" => $raw_data["number"],
                   "action_instruction" => $raw_data["instruction"],
                   "action_assigned_to" => $raw_data["assigned_to"],
                   "action_deadline" => $raw_data["deadline"],
                   "action_remind_on" => $raw_data["remind_on"],
                   "action_kpi_number" => $raw_data["kpi_number"],
                   "action_progress" => $raw_data["progress"],
                   "action_status" => $raw_data["status"]
                 );

    $body = self::process_template( $template, $data );

    $recipients = array( $raw_data["email"] );

    self::send( $recipients, $body, $subject );
  }

  private static function send( $recipients, $body, $subject )
  {
    $headers = "From: " . self::$from_email . "\r\nReply-To: " . self::$reply_to_email . " . \r\n";

    $cnt = 0;

    foreach( $recipients as $email )
    {
      $result = mail( $email, $subject . " | Audit Query | Ignite Assist", $body, $headers );
      if( $result)
      {
        $cnt++;
      }
    }

    return $cnt;
  }

  private static function process_template( $template, $data )
  {
    $template_data = '';

    if( $fp = fopen( $template, 'rb' ) )
    {
      while( !feof( $fp ) )
      {
        $template_data .= fread( $fp, 1024 );
      }

      fclose( $fp );
    }
    else
    {
      return false;
    }

    foreach( $data as $variable => $value )
    {
      $template_data = str_replace( "##" . $variable . "##", $value, $template_data );
    }

    return $template_data;
  }

  private static function get_shadow_recipients( $department )
  {
    $sql = "SELECT t.tkemail AS email
            FROM assist_" . get_company_code() . "_aqa_usersetup us
            JOIN assist_" . get_company_code() . "_timekeep t ON t.tkid = us.user_id
            WHERE ( us.all_notify = 1 OR ( us.dept_notify = 1 AND t.tkdept = " . $department . " ) )";

    $records = query_records( $sql );

    $recipients = array();

    foreach( $records as $record )
    {
      $recipients[] = $record["email"];
    }

    return $recipients;
  }

  private static function get_signoff_recipients()
  {
    $sql = "SELECT t.tkemail AS email
            FROM assist_" . get_company_code() . "_aqa_usersetup us
            JOIN assist_" . get_company_code() . "_timekeep t ON t.tkid = us.user_id
            WHERE us.signoff_query = 1 ";

    $records = query_records( $sql );

    $recipients = array();

    foreach( $records as $record )
    {
      $recipients[] = $record["email"];
    }

    return $recipients;
  }

  private static function get_assignment_data_model( $assignment )
  {
    return array( "audit_type" => $assignment->get_query()->audit_type,
                  "audit_ref" => $assignment->get_query()->audit_ref,
                  "audit_date" => $assignment->get_query()->audit_date,
                  "financial_year"=> $assignment->get_query()->financial_year,
                  "assignment_information" => $assignment->get_query()->assignment_information,
                  "audit_finding" => $assignment->get_query()->audit_finding,
                  "audit_recommendation" => $assignment->get_query()->audit_recommendation,
                  "audit_document" => $assignment->get_query()->audit_document,
                  "previously_queried" => $assignment->get_query()->previously_queried,
                  "risk_level" => $assignment->get_query()->risk_level,
                  "risk" => $assignment->get_query()->get_risk()->name,
                  "risk_detail" => $assignment->get_query()->risk_detail,
                  "assigned_to" => $assignment->get_assigned_to()->get_full_name(),
                  "department" => $assignment->get_department()->value,
                  "deadline" => $assignment->deadline,
                  "bln_follow_up" => $assignment->bln_follow_up,
                  "follow_up" => $assignment->follow_up,
                  "effective_controls" => $assignment->effective_controls,
                  "assignment_response" => $assignment->response,
                  "is_policy_sop" => $assignment->is_policy_sop,
                  "sop_assistance" => $assignment->sop_assistance,
                  "assignment_responded_on" => $assignment->responded_on,
                  "assignment_completed_on" => $assignment->completed_on,
                  "assignment_closed_on" => $assignment->closed_on,
                  "assignment_closed_status" => $assignment->closed_status,
                  "assignment_closed_by" => $assignment->get_closed_by()->get_full_name(),
                  "assignment_reject_reason" => $assignment->reject_reason,
                  "risk_reference_number" => $assignment->risk_reference_number,
                  "assignment_risk_status" => $assignment->risk_status,
                  "assignment_further_actions" => $assignment->further_actions,
                  "assignment_bln_follow_up" => ($assignment->bln_follow_up ? "Yes" : "No"),
                  "assignment_follow_up" => $assignment->follow_up,
                  "effective_controls" => ($assignment->effective_controls ? "Yes" : "No")
                );
  }

  private static function get_action_data_model( $action )
  {
    return array( "audit_type" => $action->get_assignment()->get_query()->audit_type,
                  "audit_ref" => $action->get_assignment()->get_query()->audit_ref,
                  "audit_date" => $action->get_assignment()->get_query()->audit_date,
                  "financial_year"=> $action->get_assignment()->get_query()->financial_year,
                  "assignment_information" => $action->get_assignment()->get_query()->assignment_information,
                  "audit_finding" => $action->get_assignment()->get_query()->audit_finding,
                  "audit_recommendation" => $action->get_assignment()->get_query()->audit_recommendation,
                  "audit_document" => $action->get_assignment()->get_query()->audit_document,
                  "previously_queried" => $action->get_assignment()->get_query()->previously_queried,
                  "risk_level" => $action->get_assignment()->get_query()->risk_level,
                  "risk" => $action->get_assignment()->get_query()->get_risk()->name,
                  "risk_detail" => $action->get_assignment()->get_query()->risk_detail,
                  "assigned_to" => $action->get_assignment()->get_assigned_to()->get_full_name(),
                  "department" => $action->get_assignment()->get_department()->value,
                  "deadline" => $action->get_assignment()->deadline,
                  "action_number" => $action->number,
                  "action_instruction" => $action->instruction,
                  "action_assigned_to" => $action->get_assigned_to()->get_full_name(),
                  "action_deadline" => $action->deadline,
                  "action_remind_on" => $action->remind_on,
                  "action_kpi_number" => $action->kpi_number,
                  "action_completed_on" => $action->completed_on
                );
  }
}
?>