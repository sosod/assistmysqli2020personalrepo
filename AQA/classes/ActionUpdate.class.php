<?php
  class ActionUpdate extends Base
  {
    /* --------------------------------------------------------------------------------------- */
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function __construct( $id = null )
	  {
	    $table = "aqa_action_update";
	    $fields = array( "action_id", "progress", "description", "attachment", "created_by_id", "created_on" );

	    parent::__construct( $table, $fields, $id );
	  }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- SAVE ------------------------------------------ */
    /* --------------------------------------------------------------------------------------- */
    function save()
    {
      if( isset( $this->id ) && $this->id > 0 )
      {
        if( exists( $this->attachment ) )
        {
          $new_attachment = $this->upload_attachment();

          if( exists( $new_attachment ) )
          {
            $this->attachment = $new_attachment;
          }
        }
        else
        {
          $this->attachment = $this->upload_attachment();
        }

        return $this->update();
      }
      else
      {
        $this->attachment = $this->upload_attachment();
        $this->created_by_id = get_logged_in_user_id();
        $this->created_on = date( "Y-m-d H:i:s" );

        return $this->insert();
      }
    }

    private function upload_attachment()
    {
      if( exists( $_FILES['attachment_file'] ) )
      {
        $upload_dir = "/AQA/action/";

        AQAUtils::create_directory_if_not_exists( $upload_dir );

        $target = "../files/" . get_company_code() . $upload_dir . basename( $_FILES['attachment_file']['name'] );

        if( move_uploaded_file( $_FILES['attachment_file']['tmp_name'], $target ) )
        {
          return $target;
        }
      }

      return null;
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- DELETE ---------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function delete()
    {
      return parent::delete();
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- VALIDATION ------------------------------------ */
    /* --------------------------------------------------------------------------------------- */
    function validate()
    {
      $validator = new Validator();

      if( $validator->validate_required( $this->action_id, "Action" ) == false )
      {
        $this->set_field_error( "action_id", $validator->get_error_message() );
      }

      if( $validator->validate_required( $this->description, "Description" ) == false )
      {
        $this->set_field_error( "description", $validator->get_error_message() );
      }

      if( $validator->validate_required( $this->progress, "Progress" ) == false )
      {
        $this->set_field_error( "progress", $validator->get_error_message() );
      }

      return $this->has_field_error() === false;
    }

		/* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- HELPERS --------------------------------------- */
    /* --------------------------------------------------------------------------------------- */

		/* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- LAZY LOADERS ---------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function get_action()
    {
    	if( isset( $this->action ) == false )
			{
        $this->action = new Action( $this->action_id );
			}

			return $this->action;
    }

    function get_created_by()
    {
      if( isset( $this->created_by ) == false )
      {
        $this->created_by = User::load( $this->created_by_id );
      }

      return $this->created_by;
    }
  }
?>
