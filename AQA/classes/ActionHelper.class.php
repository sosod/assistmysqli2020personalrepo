<?php
  class ActionHelper
  {
    /******************************************************************************/
    /******************************* LIST DATA FUNCTIONS **************************/
    /******************************************************************************/
    public static function get_user_action_data()
    {
      $sql = "SELECT q.audit_ref, q.audit_type, q.audit_date, q.assignment_information, q.previously_queried, ac.number, ac.instruction, ac.deadline, ac.status, ac.progress, ac.id
              FROM assist_" . get_company_code() . "_aqa_action ac
              JOIN assist_" . get_company_code() . "_aqa_assignment a ON a.id = ac.assignment_id
              JOIN assist_" . get_company_code() . "_aqa_query q ON q.id = a.query_id
              WHERE ac.status != 'Completed'
              AND ac.assigned_to_id = '" . get_logged_in_user_id() . "'
              ORDER BY ac.status, ac.deadline desc";

      return query_records( $sql );
    }
  }
?>