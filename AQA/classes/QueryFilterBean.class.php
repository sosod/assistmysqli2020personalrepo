<?php
class QueryFilterBean
{
  public $search;
  public $audit_type;
  public $audit_date;
  public $financial_year;

  public $risk_level;
  public $risk_id;

  public $user_id;
  public $department_id;
  public $deadline;
  public $status;

  public $action_user_id;
  public $action_status;
  Public $assignment_information;
  
  public $assignment_data;
  public $action_data;
  public $action_update_data;
  Public $start_date;
  Public $end_date;
  Public $previously_queried;
  Public $report_header;

  /* --------------------------------------------------------------------------------------- */
  /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
  /* --------------------------------------------------------------------------------------- */
  function __construct()
  {
  }

  function init( $data )
  {
    $this->search = $data["search"];
    $this->audit_type = $data["audit_type"];
    $this->audit_date = $data["audit_date"];
    $this->financial_year = $data["financial_year"];
    $this->risk_level = $data["risk_level"];
    $this->risk_id = $data["risk_id"];
    $this->user_id = $data["user_id"];
    $this->department_id = $data["department_id"];
    $this->deadline = $data["deadline"];
    $this->status = $data["status"];
    $this->assignment_information = $data["assignment_information"];
    $this->previously_queried = $data["previously_queried"];

    $this->action_user_id = $data["action_user_id"];
    $this->action_status = $data["action_status"];
    
    $this->assignment_data = $data["assignment_data"];
    $this->action_data = $data["action_data"];
    $this->action_update_data = $data["action_update_data"];
    $this->start_date = $data["start_date"];
    $this->end_date = $data["end_date"];
    $this->report_header = $data["report_header"];
  }

  function get_risk()
  {
    if( isset( $this->risk ) == false )
    {
      $this->risk = new Risk( $this->risk_id );
    }

    return $this->risk;
  }

  function get_user()
  {
    if( isset( $this->user ) == false )
    {
      $this->user = User::load( $this->user_id );
    }

    return $this->user;
  }

  function get_department()
  {
    if( isset( $this->department ) == false )
    {
      $this->department = new Department( $this->department_id );
    }

    return $this->department;
  }

  function get_action_user()
  {
    if( isset( $this->action_user ) == false )
    {
      $this->action_user = User::load( $this->action_user_id );
    }

    return $this->action_user;
  }
}
?>