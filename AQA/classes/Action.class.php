<?php
  class Action extends Base
  {
    /* --------------------------------------------------------------------------------------- */
    /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function __construct( $id = null )
	  {
	    $table = "aqa_action";
	    $fields = array( "assignment_id", "number", "instruction", "assigned_to_id", "deadline",
	                     "remind_on", "kpi_number", "status", "progress", "completed_on",
	                     "created_by_id", "created_on", "last_updated_by_id", "last_updated_on" );

	    parent::__construct( $table, $fields, $id );
	  }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- SAVE ------------------------------------------ */
    /* --------------------------------------------------------------------------------------- */
    function save()
    {
      if( isset( $this->id ) && $this->id > 0 )
      {
        $this->last_updated_by_id = get_logged_in_user_id();
        $this->last_updated_on = date( "Y-m-d H:i:s" );

        $update_result = $this->update();

        if( $this->status == "Completed" )
        {
          $this->get_assignment()->close_if_ready();
        }

        return $update_result;
      }
      else
      {
        $this->number = $this->get_next_number();
        $this->status = "New";
        $this->progress = 0;
        $this->created_by_id = get_logged_in_user_id();
        $this->created_on = date( "Y-m-d H:i:s" );

        $insert_result = $this->insert();

        if( $insert_result )
        {
          EmailHelper::send_action_assignment( $this );

          $this->get_assignment()->mark_as_inprogress();
        }

        return $insert_result;
      }
    }

    private function get_next_number()
    {
      $sql = "SELECT COUNT(*) AS cnt FROM assist_" . get_company_code() . "_aqa_action a WHERE a.assignment_id = " . quote( $this->assignment_id );

      return query_count( $sql ) + 1;
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- DELETE ---------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function delete()
    {
      $this->delete_updates();

      return parent::delete();
    }

    function delete_updates()
    {
      $updates = $this->get_updates();

      if( count( $updates ) > 0 )
      {
        foreach( $updates as $update )
        {
          $update->delete();
        }
      }
    }

    /* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- VALIDATION ------------------------------------ */
    /* --------------------------------------------------------------------------------------- */
    function validate()
    {
      $validator = new Validator();

      if( $validator->validate_required( $this->assignment_id, "Assignment" ) == false )
      {
        $this->set_field_error( "assignment_id", $validator->get_error_message() );
      }

      if( $validator->validate_required( $this->instruction, "Instruction" ) == false )
      {
        $this->set_field_error( "instruction", $validator->get_error_message() );
      }

      if( $validator->validate_required( $this->assigned_to_id, "User" ) == false )
      {
        $this->set_field_error( "assigned_to_id", $validator->get_error_message() );
      }

      if( $validator->validate_date( $this->deadline, "Deadline" ) == false )
      {
        $this->set_field_error( "deadline", $validator->get_error_message() );
      }

      if( $validator->validate_date( $this->remind_on, "Remind on" ) == false )
      {
        $this->set_field_error( "remind_on", $validator->get_error_message() );
      }

      if( exists( $this->kpi_number ) )
      {
        //TODO validate KPI Number on SDBIP
      }

      return $this->has_field_error() === false;
    }

		/* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- HELPERS --------------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function is_new()
    {
      return $this->status == "New";
    }

    function is_inprogress()
    {
      return $this->status == "In-progress";
    }

    function is_completed()
    {
      return $this->status == "Completed";
    }
		/* --------------------------------------------------------------------------------------- */
    /* --------------------------------------- LAZY LOADERS ---------------------------------- */
    /* --------------------------------------------------------------------------------------- */
    function get_assignment()
    {
      if( isset( $this->assignment ) == false )
      {
        $this->assignment = new Assignment( $this->assignment_id );
      }

      return $this->assignment;
    }

    function get_assigned_to()
    {
      if( isset( $this->assigned_to ) == false )
      {
        $this->assigned_to = User::load( $this->assigned_to_id );
      }

      return $this->assigned_to;
    }

    function get_created_by()
    {
      if( isset( $this->created_by ) == false )
      {
        $this->created_by = User::load( $this->created_by_id );
      }

      return $this->created_by;
    }

    function get_last_updated_by()
    {
      if( isset( $this->last_updated_by ) == false )
      {
        $this->last_updated_by = User::load( $this->last_updated_by_id );
      }

      return $this->last_updated_by;
    }

    function get_updates()
    {
      $action_update = new ActionUpdate();

      return $action_update->get_records( "action_id = " . $this->id, "created_on desc" );
    }
  }
?>
