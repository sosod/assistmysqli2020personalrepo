<?php
  class AQAUtils
  {
    public static function create_directory_if_not_exists( $path )
    {
      $path = "files/" . get_company_code() . $path;
      
      $chk = explode( "/", $path );
      $chkloc = "..";
      foreach( $chk as $c ) 
      {
        $chkloc .= "/" . $c;
        if( !is_dir( $chkloc ) ) 
        {
          mkdir( $chkloc ); 
        }
      }
    } 
  }
?>