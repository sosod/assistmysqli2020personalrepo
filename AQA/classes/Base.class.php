<?php
class Base
{
  private $table;
  private $fields;
  private $validation_errors = array();
  private $error;
  private $sql;

  function __construct( $table, $fields, $id = null )
  {
    $this->table = $table;
    $this->fields = $fields;

		if( exists( $id ) )
		{
			$this->load( $id );
		}
  }

  function load( $id )
  {
    $this->sql = 'SELECT id, '.join( ",\n", $this->fields ).'
            FROM `assist_'.get_company_code().'_'.$this->table.'`
            WHERE id = '.$id;

    $this->init( get_record( get_resultset( $this->sql ) ) );
  }

	function load_where( $where )
  {
    $this->id = $id;

    $this->sql = 'SELECT id, '.join( ",\n", $this->fields ).'
            FROM `assist_'.get_company_code().'_'.$this->table.'`
            WHERE '.$where;

    $this->init( get_record( get_resultset( $this->sql ) ) );
  }

	function init( $data )
	{
    $this->id = $data['id'];

		foreach ( $this->fields as $field )
    {
      if( exists( $data[$field] ) )
      {
        $this->$field = $data[$field];
      }
    }
	}

  function save()
  {
    if( isset( $this->id ) && $this->id > 0 )
    {
      return $this->update();
    }
    else
    {
      return $this->insert();
    }
  }

  function update()
  {
    $this->sql = 'UPDATE `assist_'.get_company_code().'_'.$this->table.'`
            SET '.join( ",\n", $this->get_update_values() ).'
            WHERE id = '.$this->quote( $this->id );

	  if( execute_update( $this->sql ) === true )
	  {
	    log_transaction( "UPDATE: " . get_class( $this ), $this->sql );

	    return true;
	  }
	  else
	  {
	    return false;
	  }
  }

  function insert()
  {
    $this->sql = 'INSERT INTO `assist_'.get_company_code().'_'.$this->table.'`
            ('.join( ",\n", $this->fields ).')
            VALUES
						('.join( ",\n", $this->get_insert_values() ).')';

    $this->id = execute_insert( $this->sql );


    if( $this->id === false )
    {
      return false;
    }
    else
    {
      log_transaction( "INSERT: " . get_class($this), $this->sql );
      return true;
    }
  }

  function delete()
  {
    $this->sql = 'DELETE FROM `assist_'.get_company_code().'_'.$this->table.'`
            WHERE id = '.$this->quote( $this->id );

    if( execute_update( $this->sql ) === true )
    {
      log_transaction( "DELETE: " . get_class($this), $this->sql );

      return true;
    }
    else
    {
      return false;
    }
  }

	function get_records( $where = false, $order = "id asc" )
  {
  	$this->sql = 'SELECT id, '.join( ",\n", $this->fields ).'
            FROM `assist_'.get_company_code().'_'.$this->table.'`';

		if( $where )
		{
			$this->sql .= ' WHERE ' . $where;
		}

		$this->sql .= ' ORDER BY ' . $order;

    $rs = get_resultset( $this->sql );

    $records = array();

		$class = get_class($this);

    while( $record = get_record( $rs ) )
    {
      $obj = new $class;
      $obj->init( $record );

      $records[] = $obj;
    }

    return $records;
  }

  function get_insert_values()
  {
    $out = array();

    foreach ( $this->fields as $field )
    {
      $out[] = $this->quote( $this->$field );
    }

    return $out;
  }

  function get_update_values()
  {
    $out = array();

    foreach ( $this->fields as $field )
    {
      $out[] = $field.' = '.$this->quote( $this->$field );
    }

    return $out;
  }

  function quote( $value )
  {
    if( isset( $value ) )
    {
      if( is_int( $value ) )
      {
        return $value;
      }
      elseif( is_float( $value ) )
      {
        return sprintf( '%F', $value );
      }
			elseif( is_string( $value ) && strlen( $value ) == 0 )
			{
				return 'NULL';
			}

      return "'".addcslashes( $value, "\000\n\r\\'\"\032" )."'";
    }
    else
	  {
	    $value = 'NULL';
	  }

	  return $value;
  }

  function is_transient()
  {
    return isset( $this->id ) == false || $this->id == 0;
  }

  function is_persisted()
  {
    return $this->is_transient() == false;
  }

	function get_display_name()
  {
  	if( $this->is_transient() )
		{
      return "New";
		}
		else
	  {
	  	return $this->name;
		}
  }

  function set_field_error( $field, $message )
  {
    $this->validation_errors[ $field ] = $message;
  }

  function get_field_error( $field )
  {
    return $this->validation_errors[ $field ];
  }

  function has_field_error()
  {
    return count( $this->validation_errors ) > 0;
  }

  function get_sql()
  {
    return $this->sql;
  }

  function set_error( $message )
  {
    $this->error = $message;
  }

  function get_error()
  {
    return $this->error;
  }

	/*function to_string( $debug = true )
	{
	  foreach ( $this->fields as $field )
    {
    	if( $debug )
	    {
	      debug( $field.' = '.$this->$field );
	    }
	    else
	    {
	      echo $field.' = '.$this->$field;
	    }
		}
	}*/
}
?>
