<?php
class Validator
{
  private $error_message;

  function __construct()
  {

  }

  public function validate_required( $value, $identifier )
  {
    if( exists( $value ) === false )
    {
      $this->error_message = $identifier . " is required";

      return false;
    }

    return true;
  }

  public function validate_date( $value, $identifier )
  {
    if( $this->validate_required( $value, $identifier ) )
    {
      debug( "date b4: " . $value );

      $timestamp = strtotime( $value );

      debug( "timestamp: " . $timestamp );

      if( is_numeric( $timestamp ) )
      {
        debug( "is numeric" );
        $month = date( 'm', $timestamp );
        $day   = date( 'd', $timestamp );
        $year  = date( 'Y', $timestamp );

        debug( "month: " . $month );
        debug( "day: " . $day );
        debug( "year: " . $year );

        if( checkdate( $month, $day, $year ) )
        {
          debug( "checkdate is true" );

          return true;
        }
      }

      $this->error_message = $identifier . " is not a valid date.";

      return false;
    }

    return false;
  }

  public function get_error_message()
  {
    return $this->error_message;
  }
}