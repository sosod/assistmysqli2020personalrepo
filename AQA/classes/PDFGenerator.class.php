<?php
require('lib/fpdf/IgnitePDF.class.php');

class PDFGenerator
{
  public static function generate_query_report( $filter, $assignment_data = false, $action_data = false, $action_update_data = false )
  {
    $pdf=new IgnitePDF('L','pt','A3'); //width 1130
    $pdf->AddPage();
    
    $headers = array( "Audit Ref", "Audit Type", "SubjeSct", "Audit Date", "Audit Finding", "Audit Recommendation", "Risk Level", "Risk", "Risk Detail", "Created By" );
    $widths = array( 90, 50, 50, 100, 200, 285, 50, 90, 150, 80 );
    
    $query_headers = array( "Audit Type", "Subject", "Audit Date", "Audit Finding", "Audit Recommendation", "Risk Level", "Risk", "Risk Detail", "Created By" );
    $query_widths = array( 50, 50, 50, 250, 315, 50, 90, 180, 80 );
    
    $assignment_headers = array( "Number", "Assigned To", "Department", "Deadline", "Responded On", "Response", "Status", "Completed On", "Signed-off On", "Sign-off Status", "Reject Reason", "Risk Status", "Further Actions", "IA Follow-up Procedure" );
    $assignment_widths = array( 35, 80, 80, 50, 50, 221, 50, 50, 50, 50, 120, 50, 122, 122 );
    
    $action_headers = array( "Number", "Assigned To", "Instruction", "Deadline", "Remind On", "KPI Number", "Status", "Progress", "Completed On", "Last Updated On" );
    $action_widths = array( 35, 80, 665, 50, 50, 50, 50, 50, 50, 50 );
    
    $action_update_headers = array( "Updated On", "Progress", "Description" );
    $action_update_widths = array( 80, 50, 1000 );
    
    $query_records = QueryHelper::get_queries( $filter );

    if( $filter->assignment_data == true )
    {
      foreach( $query_records as $query )
      {
        $query_rows = array();
        $query_rows[] = array(
          $query->audit_type,
          $query->assignment_information,
          $query->audit_date,
          $query->audit_finding,
          $query->audit_recommendation,
          //$query->previously_queried,
          $query->risk_level,
          $query->get_risk()->name,
          $query->risk_detail,
          $query->get_created_by()->get_full_name()
        );
        
        $pdf->H1( 'Query: ' . $query->audit_ref );
        $pdf->listTable( $query_headers, $query_rows, $query_widths, 'L' );
        
        
        $assignment_rows = array();
        $assignment_records = $query->get_assignments(); 
        
        if( $filter->action_data == true )
        {
          if( count( $assignment_records ) > 0 )
          {
            foreach( $assignment_records as $assignment )
            {
              $assignment_rows = array();            

              $assignment_rows[] = array(
                $assignment->number,
                $assignment->get_assigned_to()->get_full_name(),
                $assignment->get_department()->value,
                $assignment->deadline,
                substr( $assignment->responded_on, 0, 10 ),
                $assignment->response,
                $assignment->status,
                substr( $assignment->completed_on, 0, 10 ),
                substr( $assignment->closed_on, 0, 10 ),
                $assignment->closed_status,
                $assignment->reject_reason,
                $assignment->risk_status,
                $assignment->further_actions,                
                $assignment->follow_up
              );
              
              $pdf->TableCaption( 'Assignment ' . $assignment->number . ': ' . $assignment->get_assigned_to()->get_full_name() );
              $pdf->listTable( $assignment_headers, $assignment_rows, $assignment_widths, 'L' );              
              
              $action_rows = array();
              $action_records = $assignment->get_actions();
              
              if( $filter->action_update_data == true )
              {
                if( count( $action_records ) > 0 )
                {
                  foreach( $action_records as $action )
                  {
                    $action_rows = array();
                    
                    $action_rows[] = array(
                      $action->number,
                      $action->get_assigned_to()->get_full_name(),
                      $action->instruction,
                      $action->deadline,
                      substr( $action->remind_on, 0, 10 ),
                      $action->kpi_number,
                      $action->status,
                      $action->progress,
                      substr( $action->completed_on, 0, 10 ),
                      substr( $assignment->last_updated_on, 0, 10 )
                    );
                    
                    $pdf->TableCaption( "Assignment " . $assignment->number . " Action", true, 'L', 'B' );
                    $pdf->listTable( $action_headers, $action_rows, $action_widths, 'L' );
                    
                    $action_update_rows = array();
                    $action_update_records = $action->get_updates();
                    
                    if( count( $action_update_records ) > 0 )
                    {
                      
                      $pdf->TableCaption( "Action " . $action->number . " Updates", true, 'L', 'B' );
                      
                      foreach( $action_update_records as $action_update )
                      {
                        $action_update_rows[] = array(
                          substr( $action_update->created_on, 0, 10 ),
                          $action_update->progress,
                          $action_update->description
                        );
                      }
                  
                      $pdf->listTable( $action_update_headers, $action_update_rows, $action_update_widths, 'L' );
                    }
                    else
                    {
                      $pdf->H4( "No updates for action " . $action->number ); 
                    }
                  }
                }
                else
                {
                  $pdf->H4( "No actions for assignment " . $assignment->number );
                }
              }
              else
              {
                if( count( $action_records ) > 0 )
                {
                  $pdf->TableCaption( "Assignment Actions", true, 'L', 'B' );
              
                  foreach( $action_records as $action )
                  {
                    $action_rows[] = array(
                      $action->number,
                      $action->get_assigned_to()->get_full_name(),
                      $action->instruction,
                      $action->deadline,
                      substr( $action->remind_on, 0, 10 ),
                      $action->kpi_number,
                      $action->status,
                      $action->progress,
                      substr( $action->completed_on, 0, 10 ),
                      substr( $assignment->last_updated_on, 0, 10 )
                    );
                  }
                }
                else
                {
                  $pdf->H4( "No actions for assignment " . $assignment->number );
                }
            
                $pdf->listTable( $action_headers, $action_rows, $action_widths, 'L' );
              }
            }
          }
          else
          {
            $pdf->H4( "No assignments" );
          }
        }
        else
        {
          if( count( $assignment_records ) > 0 )
          {
            $pdf->TableCaption( "Query Assignments", true, 'L', 'B' );
            
            foreach( $assignment_records as $assignment )
            {
              $assignment_rows[] = array(
                $assignment->number,
                $assignment->get_assigned_to()->get_full_name(),
                $assignment->get_department()->value,
                $assignment->deadline,
                substr( $assignment->responded_on, 0, 10 ),
                $assignment->response,                
                $assignment->status,
                substr( $assignment->completed_on, 0, 10 ),
                substr( $assignment->closed_on, 0, 10 ),
                $assignment->closed_status,
                $assignment->reject_reason,
                $assignment->risk_reference_number,
                $assignment->risk_status,
                $assignment->further_actions,
                $assignment->bln_follow_up,
                $assignment->follow_up,
                $assignment->effective_controls
              );
            }
        
            $pdf->listTable( $assignment_headers, $assignment_rows, $assignment_widths, 'L' );
          }
          else
          {
            $pdf->H4( "No assignments" );
          }
        }

        $pdf->AddPage();
      }
    }
    else
    {
//      $pdf->H1( 'Queries' );
        
      $pdf->H1( $filter->report_header );
      $rows = array();
      
      foreach( $query_records as $query )
      {
        $rows[] = array(
          $query->audit_ref,
          $query->audit_type,
          $query->assignment_information,
          $query->audit_date,
          $query->audit_finding,
          $query->audit_recommendation,
          //$query->previously_queried,
          $query->risk_level,
          $query->get_risk()->name,
          $query->risk_detail,
          $query->get_created_by()->get_full_name()
        );
      }
  
      $pdf->listTable( $headers, $rows, $widths, 'L' );
    }
    
    /*
    if( $query_detail )
    {
      self::add_query_info( $pdf, $records );
    }*/

    $pdf->Output( 'query_report.pdf','I');
  }
  
  public static function generate_queries_all_report( $filter, $query_detail = false )
  {
    $pdf=new IgnitePDF('L','pt','A4');
    $pdf->AddPage();
//    $pdf->H1( "Query Data Report" );
    $pdf->H1( $filter->report_header );

    self::add_filter_info( $pdf, $filter );

    $pdf->TableCaption( "Queries" );

    $headers = array( "Audit Ref", "Audit Type", "Subject", "Audit Date", "Risk Level", "Risk", "Assignment #", "Assigned To", "Deadline", "Status" );
    $widths = array( 75, 75, 75, 75, 75, 75, 75, 75, 86, 60 );
    $rows = array();

    $records = QueryHelper::get_query_data( $filter );

    foreach( $records as $record )
    {
      $rows[] = array(
        $record["audit_ref"],
        $record["audit_type"],
        $record["assignment_information"],
        $record["audit_date"],
        //$record["previously_queried"],
        $record["risk_level"],
        $record["risk"],
        $record["number"],
        $record["user"],
        $record["deadline"],
        $record["status"]
      );
    }

    $pdf->listTable( $headers, $rows, $widths );

    if( $query_detail )
    {
      self::add_query_info( $pdf, $records );
    }

    $pdf->Output( 'queries_data.pdf','I');
  }

  public static function generate_queries_overdue_report( $filter, $query_detail = false )
  {
    $pdf=new IgnitePDF('L','pt','A4');
    $pdf->AddPage();
//    $pdf->H1( "Queries Overdue Report" );
    $pdf->H1( $filter->report_header);

    self::add_filter_info( $pdf, $filter );

    $pdf->TableCaption( "Queries" );

    $headers = array( "Audit Ref", "Audit Type", "Subject", "Audit Date", "Assignment #", "Assigned To", "Deadline", "Completed On", "Days Overdue", "Status" );
    $widths = array( 75, 75, 75, 75, 75, 75, 75, 75, 75, 86 );
    $rows = array();

    $records = QueryHelper::get_query_overdue_data( $filter );

    foreach( $records as $record )
    {
      $rows[] = array(
        $record["audit_ref"],
        $record["audit_type"],
        $record["assignment_information"],
        $record["audit_date"],
        $record["number"],
        $record["user"],
        $record["deadline"],
        $record["completed_on"],
        $record["days_overdue"],
        $record["status"]
      );
    }

    $pdf->listTable( $headers, $rows, $widths );

    if( $query_detail )
    {
      self::add_query_info( $pdf, $records );
    }

    $pdf->Output( 'queries_overdue.pdf','I');
  }

  public static function generate_actions_overdue_report( $filter, $query_detail = false )
  {
    $pdf=new IgnitePDF('L','pt','A4');
    $pdf->AddPage();
//    $pdf->H1( "Actions Overdue Report" );
    $pdf->H1( $filter->report_header);

    self::add_filter_info( $pdf, $filter, true );

    $pdf->TableCaption( "Actions" );

    $headers = array( "Audit Ref", "Assignment #", "SubjeSct", "Action #", "Assigned To", "Instruction", "Deadline", "Completed On", "Days Overdue", "Progress", "Status" );
    $widths = array( 70, 70, 70, 70, 70, 70, 70, 70, 70, 70, 70 );
    $rows = array();

    $records = QueryHelper::get_action_overdue_data( $filter );

    foreach( $records as $record )
    {
      $rows[] = array(
        $record["audit_ref"],
        $record["assignment_number"],
        $record["assignment_information"],
        $record["action_number"],
        $record["user"],
        $record["instruction"],
        $record["deadline"],
        $record["completed_on"],
        $record["days_overdue"],
        $record["progress"] . "%",
        $record["status"]
      );
    }

    $pdf->listTable( $headers, $rows, $widths );
    $pdf->Output( 'actions_overdue.pdf','I');
  }

  public static function generate_assignment_printout( $assignment )
  {
    $pdf=new IgnitePDF('P','pt','A4');
    $pdf->AddPage();
    $pdf->H1( "Audit Query: " . $assignment->get_query()->audit_ref . "-" . $assignment->number );

    $pdf->SetTextColor( 0, 0, 0 );


    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Assigned To:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 180, 12, $assignment->get_assigned_to()->get_full_name( true ), 0, 0, 'L' );

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Department:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 80, 12, $assignment->get_department()->value, 0, 1, 'L' );

    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Assigned On:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 180, 12, $assignment->created_on, 0, 0, 'L' );

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Status:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 80, 12, $assignment->status, 0, 1, 'L' );

    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Deadline:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 180, 12, $assignment->deadline, 0, 0, 'L' );

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Status date:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 80, 12, $assignment->last_updated_on, 0, 1, 'L' );

    $pdf->Ln( 15 );

    $pdf->H2( "Query Details" );

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Audit type:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 180, 12, $assignment->get_query()->audit_type, 0, 0, 'L' );

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Audit ref:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 80, 12, $assignment->get_query()->audit_ref, 0, 1, 'L' );

    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Audit date:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 180, 12, $assignment->get_query()->audit_date, 0, 1, 'L' );

    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Audit finding:', 0, 1, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->MultiCell( 0, 12, $assignment->get_query()->audit_finding, 0, 'L' );

    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Audit recommendation:', 0, 1, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->MultiCell( 0, 12, $assignment->get_query()->audit_recommendation, 0, 'L' );

    if( isset( $assignment->get_query()->risk_id ) )
    {
      $pdf->Ln( 15 );

      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 60, 12, 'Risk level:', 0, 0, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->Cell( 180, 12, $assignment->get_query()->risk_level, 0, 0, 'L' );

      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 60, 12, 'Risk:', 0, 0, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->Cell( 80, 12, $assignment->get_query()->get_risk()->name, 0, 1, 'L' );

      $pdf->Ln(5);

      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 60, 12, 'Risk detail:', 0, 1, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->MultiCell( 0, 12, $assignment->get_query()->risk_detail, 0, 'L' );
    }

    $pdf->Ln( 15 );

    $pdf->H2( "Management Response" );

    if( isset( $assignment->responded_on ) )
    {
      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 60, 12, 'Responded on:', 0, 0, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->Cell( 180, 12, $assignment->responded_on, 0, 1, 'L' );

      $pdf->Ln(5);

      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 60, 12, 'Response:', 0, 1, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->MultiCell( 0, 12, $assignment->response, 0, 'L' );
    }
    else
    {
      $pdf->SetFont( 'Arial', 'I', 8 );
      $pdf->Cell( 60, 12, 'No response submitted yet.', 0, 1, 'L' );
    }

    $pdf->Ln( 15 );


    $pdf->H2( "Actions" );

    $actions = $assignment->get_actions();

    if( count( $actions ) > 0 )
    {
      $headers = array( "Number", "Assigned To", "Instruction", "Deadline", "Progress", "Status" );
      $widths = array( 50, 130, 200, 60, 40, 60 );
      $rows = array();

      foreach( $actions as $action )
      {
        $rows[] = array(
          $action->number,
          $action->get_assigned_to()->get_full_name( true ),
          $action->instruction,
          $action->deadline,
          $action->progress . "%",
          $action->status
        );
      }

      $pdf->listTable( $headers, $rows, $widths );
    }
    else
    {
      $pdf->SetFont( 'Arial', 'I', 8 );
      $pdf->Cell( 60, 12, 'No actions defined.', 0, 1, 'L' );
    }


    $pdf->Ln( 15 );

    if( $assignment->is_closed() )
    {
      $pdf->H2( "Sign-off summary" );

      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 120, 12, 'IA Follow-up Procedure:', 0, 0, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->Cell( 180, 12, $assignment->follow_up, 0, 1, 'L' );

      $pdf->Ln(5);

      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 120, 12, 'Status:', 0, 0, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->Cell( 180, 12, $assignment->closed_status, 0, 1, 'L' );

      $pdf->Ln(5);

      if( $assignment->closed_status == "Rejected" )
      {
        $pdf->SetFont( 'Arial', 'B', 8 );
        $pdf->Cell( 120, 12, 'Reject reason:', 0, 0, 'L' );
        $pdf->SetFont( 'Arial', '', 8 );
        $pdf->Cell( 180, 12, $assignment->reject_reason, 0, 1, 'L' );

        $pdf->Ln(5);
      }
      
      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 120, 12, 'Risk Reference Number:', 0, 0, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->Cell( 180, 12, $assignment->risk_reference_number, 0, 1, 'L' );

      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 120, 12, 'Risk status:', 0, 0, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->Cell( 180, 12, $assignment->risk_status, 0, 1, 'L' );

      $pdf->Ln(5);

      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 120, 12, 'Further actions:', 0, 1, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->MultiCell( 0, 12, $assignment->further_actions, 0, 'L' );
    }

    $pdf->Output( 'audit_query_' . $assignment->get_query()->audit_ref . '_' . $assignment->number . '.pdf','I');
  }

  public static function generate_assignment_response_printout( $assignment, $company )
  {
    $pdf=new IgnitePDF('P','pt','A4');
    $pdf->AddPage();
    $pdf->H1( $company, 'C' );
    $pdf->H2( "Response to " . $assignment->get_query()->audit_type . " Audit Query", 'C' );

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Audit ref:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 80, 12, $assignment->get_query()->audit_ref, 0, 1, 'L' );

    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Audit date:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 180, 12, $assignment->get_query()->audit_date, 0, 1, 'L' );

    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Audit finding:', 0, 1, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->MultiCell( 0, 12, $assignment->get_query()->audit_finding, 0, 'L' );

    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Audit recommendation:', 0, 1, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->MultiCell( 0, 12, $assignment->get_query()->audit_recommendation, 0, 'L' );

    if( exists( $assignment->get_query()->risk_id ) )
    {
      $pdf->Ln(5);

      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 60, 12, 'Risk level:', 0, 0, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->Cell( 180, 12, $assignment->get_query()->risk_level, 0, 1, 'L' );

      $pdf->Ln(5);

      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 60, 12, 'Risk:', 0, 0, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->Cell( 80, 12, $assignment->get_query()->get_risk()->name, 0, 1, 'L' );

      $pdf->Ln(5);

      $pdf->SetFont( 'Arial', 'B', 8 );
      $pdf->Cell( 60, 12, 'Risk detail:', 0, 1, 'L' );
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->MultiCell( 0, 12, $assignment->get_query()->risk_detail, 0, 'L' );
    }

    $pdf->Ln(5);

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Management response:', 0, 1, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->MultiCell( 0, 12, $assignment->response, 0, 'L' );

    $actions = $assignment->get_actions();

    if( count( $actions ) > 0 )
    {
      $pdf->Ln( 10 );
      $pdf->H4( "Actions" );

      $headers = array( "Number", "Assigned To", "Instruction", "Deadline", "Progress", "Status" );
      $widths = array( 50, 130, 200, 60, 40, 60 );
      $rows = array();

      foreach( $actions as $action )
      {
        $rows[] = array(
          $action->number,
          $action->get_assigned_to()->get_full_name( true ),
          $action->instruction,
          $action->deadline,
          $action->progress . "%",
          $action->status
        );
      }

      $pdf->listTable( $headers, $rows, $widths );
    }

    $pdf->Ln( 10 );

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Signed:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 180, 12, $assignment->get_assigned_to()->get_full_name( true ), 0, 1, 'L' );

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->Cell( 60, 12, 'Date:', 0, 0, 'L' );
    $pdf->SetFont( 'Arial', '', 8 );
    $pdf->Cell( 180, 12, $assignment->responded_on, 0, 1, 'L' );

    $pdf->Output( 'audit_query_response_' . $assignment->get_query()->audit_ref . '_' . $assignment->number . '.pdf','I');
  }

  private static function add_filter_info( &$pdf, $filter, $action = false )
  {
    $pdf->TableCaption( "Report Filter Criteria" );

    $filterData = array(
      "Search" => $filter->search,
      "Audit Type" => exists( $filter->audit_type ) ? $filter->audit_type : "All",
      "Audit Date" => exists( $filter->audit_date ) ? $filter->audit_date : "All",
      "previously_queried"  => exists( $filter->previously_queried ) ? $filter->previously_queried : "All",
      "Risk Level" => exists( $filter->risk_level ) ? $filter->risk_level : "All",
      "Risk" => exists( $filter->risk_id ) ? $filter->get_risk()->name : "All",
      "Assigned To" => exists( $filter->user_id ) ? $filter->get_user()->get_full_name( true ) : "All",
      "Department" => exists( $filter->department_id ) ? $filter->get_department()->value : "All",
      "Deadline" => exists( $filter->deadline ) ? $filter->deadline : "All",
      "Status" => exists( $filter->status ) ? $filter->status : "All",
      "Assignment Information" => $filter->assignment_information,
      "Start Date" => $filter->start_date,
      "End Date" => $filter->end_date  
            
    );

    if( $action )
    {
      $filterData["Action Assigned To"] = exists( $filter->action_user_id ) ? $filter->get_action_user()->get_full_name( true ) : "All";
      $filterData["Action Status"] = exists( $filter->action_status ) ? $filter->action_status : "All";
    }

    $pdf->summaryTable( $filterData );
  }

  private static function add_query_info( &$pdf, $records )
  {
    foreach( $records as $record )
    {
      $pdf->AddPage('P');

      $assignment = new Assignment( $record["id"] );

      $pdf->h1( "Query: " . $assignment->get_query()->audit_ref . "-" . $assignment->number );

      $pdf->TableCaption( "Query Information" );

      $data = array(
        "Audit Type" => $assignment->get_query()->audit_type,
        "Audit Ref" => $assignment->get_query()->audit_ref,
        "Audit Date" => $assignment->get_query()->audit_date,
        "Audit Finding" => $assignment->get_query()->audit_finding,
        "Audit Recommendation" => $assignment->get_query()->audit_recommendation,
        "Risk Level" => $assignment->get_query()->risk_level,
        "Risk" => $assignment->get_query()->get_risk()->name,
        "Risk Detail" => $assignment->get_query()->risk_detail,
        "Created By" => $assignment->get_query()->get_created_by()->get_full_name( true ),
        "Created On" => $assignment->get_query()->created_on
      );

      $pdf->summaryTable( $data, 550 );

      $pdf->TableCaption( "Assignment Information" );

      $data = array(
        "Assignment #" => $assignment->number,
        "Assigned On" => $assignment->created_on,
        "Assigned To" => $assignment->get_assigned_to()->get_full_name( true ),
        "Department" => $assignment->get_department()->value,
        "Deadline" => $assignment->deadline,
        "Status" => $assignment->status
      );

      $pdf->summaryTable( $data, 550 );

      if( exists( $assignment->responded_on ) )
      {
        $pdf->TableCaption( "Assignment Response" );

        $data = array(
          "Response" => $assignment->response,
          "Responded On" => $assignment->responded_on,
        );

        $pdf->summaryTable( $data, 550 );
      }

      if( $assignment->status == 'Closed' )
      {
        $pdf->TableCaption( "Assignment Sign-off" );

        $data = array(
          "IA Follow-up Procedure" => $assignment->follow_up,
          "Sign-off Status" => $assignment->closed_status,
          "Reject Reason" => $assignment->reject_reason,
          "risk_reference_number" => $assignment->risk_reference_number,
          "Risk Status" => $assignment->risk_status,
          "Further Actions" => $assignment->further_actions
        );

        $pdf->summaryTable( $data, 550 );
      }

      $records = $assignment->get_actions();

      if( count( $records ) > 0 )
      {
        $pdf->TableCaption( "Actions" );

        $headers = array( "Number", "Assigned To", "Deadline", "Progress", "Status" );
        $widths = array( 60, 160, 110, 110, 110 );
        $rows = array();

        foreach( $records as $record )
        {
          $rows[] = array(
            $record->number,
            $record->get_assigned_to()->get_full_name( true ),
            $record->deadline,
            $record->progress,
            $record->status
          );
        }

        $pdf->listTable( $headers, $rows, $widths );
      }
    }
  }

}
?>