<?php
class PagerBean
{
  public $records_per_page = 10;
  public $page = 1;
  public $total_records = 0;
  public $start;
  public $total_pages;
  public $bottom_range;
  public $top_range;

  /* --------------------------------------------------------------------------------------- */
  /* ------------------------------------- CONSTRUCTOR ------------------------------------- */
  /* --------------------------------------------------------------------------------------- */
  function __construct()
  {
  }

  function get_start()
  {
    if( $this->page - 1 == 0 )
    {
      return 0;
    }
    else
    {
      return ( $this->page - 1 ) * $this->records_per_page;
    }
  }

  function calculate_values()
  {
    //calculate total pages
    $this->total_pages = floor( $this->total_records / $this->records_per_page );

    if( $this->total_records % $this->records_per_page > 0 )
    {
      $this->total_pages++;
    }

    if( $this->total_pages == 0 )
    {
      $this->total_pages = 1;
    }

    //calculate bottom range
    $this->bottom_range = $this->page - 4;

    if( $this->bottom_range < 1 )
    {
      $this->bottom_range = 1;
    }

    //calculate top range
    $this->top_range = $this->bottom_range + 9;
    if( $this->top_range > $this->total_pages )
    {
      $this->top_range = $this->total_pages;
    }
  }
}
?>