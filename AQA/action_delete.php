<?php
  include("inc_ignite.php");

  if( get_logged_in_user()->is_super_user() == false )
  {
    View::redirect( "access_denied.php" );
  }

  $section = exists( $_GET["section"] ) ? $_GET["section"] : 5;
  $assignment_id =  $_GET["assignment_id"];
  $id = $_GET["id"];

  $action = new Action();
  $action->id = $id;

  if( $action->delete() )
  {
    if( exists( $assignment_id ) )
    {
      View::redirect( View::get_section_url( $section, "info", $assignment_id ) );
    }
    else
    {
      View::redirect( View::get_section_url( $section ) );
    }
  }
  else
  {
    View::show_db_error( $action->get_sql() );
  }
?>