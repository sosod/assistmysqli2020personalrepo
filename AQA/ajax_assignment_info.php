<?php
include("inc_ignite.php");

$assignment_id = $_REQUEST["assignment_id"];
$assignment = new Assignment( $assignment_id );

?>
<div id="infoWindow">
  <h2>Assignment Information</h2>
  <table cellspacing="1" cellpadding="0" class="info">
    <tr>
      <th>Assignment #</th>
      <td><?= $assignment->number ?></td>
    </tr>
    <tr>
      <th>Assigned To</th>
      <td><?= $assignment->get_assigned_to()->get_full_name() ?></td>
    </tr>
    <tr>
      <th>Department</th>
      <td><?= $assignment->get_department()->value ?></td>
    </tr>
    <tr>
      <th>Deadline</th>
      <td><?= $assignment->deadline ?></td>
    </tr>
    <tr>
      <th>Status</th>
      <td><?= $assignment->status ?> <em>(<?= exists( $assignment->last_updated_on ) ? $assignment->last_updated_on : $assignment->created_on ?>)</em></td>
    </tr>
  </table>

  <h2>Query Information</h2>
  <table cellspacing="1" cellpadding="0" class="info">
    <tr>
      <th>Audit Type</th>
      <td><?= $assignment->get_query()->audit_type ?></td>
    </tr>
    <tr>
      <th>Audit Ref</th>
      <td><?= $assignment->get_query()->audit_ref ?></td>
    </tr>
    <tr>
      <th>Audit Date</th>
      <td><?= $assignment->get_query()->audit_date ?></td>
    </tr>
    <tr>
      <th>Financial Year</th>
      <td><?= $assignment->get_query()->financial_year ?></td>
    </tr>
    <tr>
      <th>Subject / Assignment Information</th>
      <td><?= $assignment->get_query()->assignment_information ?></td>
    </tr>
    <tr>
      <th>Audit Finding</th>
      <td><?= $assignment->get_query()->audit_finding ?></td>
    </tr>
    <tr>
      <th>Audit Recommendation</th>
      <td><?= $assignment->get_query()->audit_recommendation ?></td>
    </tr>
    <tr>
      <th>Audit Document</th>
      <?php if( exists( $assignment->get_query()->audit_doc ) ){ ?>
      <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->get_query()->audit_doc ?>">Download audit document</a></td>
      <?php }else{ ?>
      <td></td>
      <?php } ?>
    </tr>
    <tr>
        <th>Previously Queried </th>
        <td><?= $assignment->get_query()->previously_queried  ?></td>
    </tr>
    <tr>
      <th>Risk Level</th>
      <td><?= $assignment->get_query()->risk_level ?></td>
    </tr>
    <tr>
      <th>Risk</th>
      <td><?= $assignment->get_query()->get_risk()->name ?></td>
    </tr>
    <tr>
      <th>Risk Detail</th>
      <td><?= $assignment->get_query()->risk_detail ?></td>
    </tr>
  </table>

  <h2>Assignment Response</h2>
  <?php if( $assignment->has_responded() ||
                ( $assignment->is_new() == false &&
                  $assignment->is_inprogress() == false ) ){ ?>
  <table cellspacing="1" cellpadding="0" class="info">
    <tr>
      <th>Response</th>
      <td><?= $assignment->response ?></td>
    </tr>
    <tr>
        <th>Is there a Policy or Standard Operating Procedure(SOP) in place?</th>
        <td><?= $assignment->is_policy_sop ?></td>
    </tr>
    <?php if( $assignment->is_policy_sop == "No" ){ ?>
    <tr>
        <th>Do you need assistance with developing a Policy or Standard Operating Procedure (SOP)?</th>
        <td><?= View::display_bool($assignment->sop_assistance) ?></td>
    </tr>
    <?php } ?>
    <tr>
        <th>SOP/Policy</th>
      <?php if( exists( $assignment->SOP_Policy_attachment ) ){ ?>
        <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->SOP_Policy_attachment ?>">Download attachment</a></td>
      <?php }else{ ?>
      <td></td>
      <?php } ?>
    </tr>
    <tr>
      <th>Attachment</th>
      <?php if( exists( $assignment->response_attachment ) ){ ?>
      <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->response_attachment ?>">Download attachment</a></td>
      <?php }else{ ?>
      <td></td>
      <?php } ?>
    </tr>
    <tr>
      <th>Attachment 2</th>
      <?php if( exists( $assignment->response_attachment_2 ) ){ ?>
      <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->response_attachment_2 ?>">Download attachment</a></td>
      <?php }else{ ?>
      <td></td>
      <?php } ?>
    </tr>
    <tr>
      <th>Attachment 3</th>
      <?php if( exists( $assignment->response_attachment_3 ) ){ ?>
      <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->response_attachment_3 ?>">Download attachment</a></td>
      <?php }else{ ?>
      <td></td>
      <?php } ?>
    </tr>
    <tr>
      <th>Attachment 4</th>
      <?php if( exists( $assignment->response_attachment_4 ) ){ ?>
      <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->response_attachment_4 ?>">Download attachment</a></td>
      <?php }else{ ?>
      <td></td>
      <?php } ?>
    </tr>
    <tr>
      <th>Attachment 5</th>
      <?php if( exists( $assignment->response_attachment_5 ) ){ ?>
      <td><img src="images/icon-download.png" alt="Download" /><a target="_blank" href="<?= $assignment->response_attachment_5 ?>">Download attachment</a></td>
      <?php }else{ ?>
      <td></td>
      <?php } ?>
    </tr>
    <tr>
      <th>Responded On</th>
      <td><?= $assignment->responded_on ?></td>
    </tr>
    <tr>
      <th>Print</th>
      <td><?= View::link_button( "assignment_response_print.php?assignment_id=" . $assignment->id . "&section=" . $section, "Print response" ) ?></td>
    </tr>
  </table>
  <?php }else{ ?>
  <p><?= $assignment->get_assigned_to()->get_full_name() ?> has not responded yet.</p>
  <?php } ?>

  <?php if( $assignment->is_closed() ){ ?>
  <h2>Sign-off Information</h2>
  <table cellspacing="1" cellpadding="0" class="info">
      <tr>
      <th>Follow-up</th>
      <td><?= $assignment->bln_follow_up ?></td>
    </tr>
    <tr>
    <tr>
      <th>IA Follow-up Procedure</th>
      <td><?= $assignment->follow_up ?></td>
    </tr>
    <tr>
        <th>Effective Controls</th>
        <td><?= $assignment->effective_controls ?></td>
    </tr>
    <tr>
      <th>Sign-off Status</th>
      <td><?= $assignment->closed_status ?></td>
    </tr>
    <?php if( $assignment->closed_status == "Rejected" ){ ?>
    <tr>
      <th>Reject Reason</th>
      <td><?= $assignment->reject_reason ?></td>
    </tr>
    <?php } ?>
    <tr>
        <th>Risk Reference Number</th>
        <td><?= $assignment->risk_reference_number ?></td>
    </tr>
    <tr>
      <th>Risk Status</th>
      <td><?= $assignment->risk_status ?></td>
    </tr>
    <tr>
      <th>Further Actions</th>
      <td><?= $assignment->further_actions ?></td>
    </tr>
  </table>
  <?php } ?>

  <?php $actions = $assignment->get_actions(); ?>
  <h2>Actions</h2>
  <table cellspacing="1" cellpadding="0" class="default zebra">
    <thead>
      <tr>
        <th>Number</th>
        <th>Assigned To</th>
        <th>Deadline</th>
        <th>Progress</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
    <?php
    if( count( $actions ) > 0 )
    {
      foreach( $actions as $obj )
      {
    ?>
      <tr>
        <td><?= $obj->number ?></td>
        <td><?= $obj->get_assigned_to()->get_full_name() ?></td>
        <td><?= $obj->deadline ?></td>
        <td><?= $obj->progress ?>%</td>
        <td><?= $obj->status ?></td>
      </tr>
    <?php
      }
    }
    else
    {
    ?>
      <tr><td colspan="5">No actions have been created for this query</td></tr>
    <?php
    }
    ?>
    </tbody>
  </table>
</div>