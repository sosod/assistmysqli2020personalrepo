<?php
  include("inc_ignite.php");

  if( get_logged_in_user()->has_access( "setup" ) == false )
  {
    View::redirect( "access_denied.php" );
  }

  if( $_POST["action"] == View::$ACTION_SAVE )
  {
    $risk = new Risk();
    $risk->init( $_POST );

    if( $risk->validate() )
    {
      if( $risk->save() )
      {
        View::set_session_message( "risk", "Risk successfully saved." );
        View::redirect( "setup_risks.php" );
      }
      else
      {
        View::show_db_error( $risk->get_sql() );
      }
    }
  }
  else
  {
    $risk = new Risk( $_GET["id"] );
  }
?>

<?
  include("inc_header.php");
?>
<?php
  $section = 2;
  include("inc_nav_setup.php");
?>

<h1>Setup &raquo; Risks &raquo; Risk</h1>

<div class="actions">
  <?php View::link_button( "setup_risks.php", "Back" ) ?>
</div>

<form action="" method="post" class="default">
  <div>
    <?php View::label( "fldName", "Name", true ) ?>
    <?php View::text( "fldName", "name", $risk->name, array( "maxlength"=>"255", "size"=>"30" ) ) ?>
    <?php View::field_error( $risk->get_field_error( "name" ) ) ?>
  </div>
  <div>
    <?php View::label( "fldStatus", "Status", true ) ?>
    <?php View::select( "fldStatus", "status", $risk->status, View::get_risk_status_select_list() ) ?>
    <?php View::field_error( $risk->get_field_error( "status" ) ) ?>
  </div>
  <?php View::hidden( "action", View::$ACTION_SAVE ) ?>
  <?php View::hidden( "id", $risk->id ) ?>
  <?php View::submit() ?><?php View::cancel( "setup_risks.php" ) ?>
</form>

<?php
  include("inc_footer.php");
?>