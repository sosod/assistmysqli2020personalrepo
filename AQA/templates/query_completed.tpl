A query has been completed and is ready to be signed-off:

---------------------------------------
Query Response
---------------------------------------
Response: ##assignment_response##
Responded On: ##assignment_responded_on##

---------------------------------------
Query Assignment
---------------------------------------
Assigned To: ##assigned_to##
Department: ##department##
Deadline: ##deadline##
Completed On: ##assignment_completed_on##

---------------------------------------
Query Information
---------------------------------------
Audit Type: ##audit_type##
Audit Ref: ##audit_ref##
Audit Date: ##audit_date##
Subject / Assignment Information: ##assignment_information##
Audit Finding: ##audit_finding##
Audit Recommendation: ##audit_recommendation##
Risk Level: ##risk_level##
Risk: ##risk##
Risk Detail: ##risk_detail##