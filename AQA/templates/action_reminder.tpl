The following action requires your attention:

---------------------------------------
Action Assignment:
---------------------------------------
Action Number: ##action_number##
Action Instruction: ##action_instruction##
Assigned To: ##action_assigned_to##
Deadline: ##action_deadline##
Remind On: ##action_remind_on##
KPI Number: ##action_kpi_number##
Progress: ##action_progress##%
Status: ##action_status##