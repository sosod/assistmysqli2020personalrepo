An action has been assigned with the following details:

---------------------------------------
Action Assignment:
---------------------------------------
Action Number: ##action_number##
Action Instruction: ##action_instruction##
Assigned To: ##action_assigned_to##
Deadline: ##action_deadline##
Remind On: ##action_remind_on##
KPI Number: ##action_kpi_number##

---------------------------------------
Query Assignment
---------------------------------------
Assigned To: ##assigned_to##
Department: ##department##
Deadline: ##deadline##

---------------------------------------
Query Information
---------------------------------------
Audit Type: ##audit_type##
Audit Ref: ##audit_ref##
Audit Date: ##audit_date##
Subject / Assignment Information: ##assignment_information##
Audit Finding: ##audit_finding##
Audit Recommendation: ##audit_recommendation##
Risk Level: ##risk_level##
Risk: ##risk##
Risk Detail: ##risk_detail##