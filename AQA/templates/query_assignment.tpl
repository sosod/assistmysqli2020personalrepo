A query has been assigned with the following details:

---------------------------------------
Query Assignment
---------------------------------------
Assigned To: ##assigned_to##
Department: ##department##
Deadline: ##deadline##

---------------------------------------
Query Information
---------------------------------------
Audit Type: ##audit_type##
Audit Ref: ##audit_ref##
Audit Date: ##audit_date##
Subject / Assignment Information: ##assignment_information##
Audit Finding: ##audit_finding##
Audit Recommendation: ##audit_recommendation##
Risk Level: ##risk_level##
Risk: ##risk##
Risk Detail: ##risk_detail##