A query has been signed off:

---------------------------------------
Query Sign-off
---------------------------------------
Signed-off By: ##assignment_closed_by##
Sign-off Status: ##assignment_closed_status##
Sign-off Reason: ##assignment_reject_reason##
Risk Status: ##assignment_risk_status##
Further Actions: ##assignment_further_actions##

---------------------------------------
Query Response
---------------------------------------
Response: ##assignment_response##
Responded On: ##assignment_responded_on##

---------------------------------------
Query Assignment
---------------------------------------
Assigned To: ##assigned_to##
Department: ##department##
Deadline: ##deadline##
Completed On: ##assignment_completed_on##
Signed-off On: ##assignment_closed_on##

---------------------------------------
Query Information
---------------------------------------
Audit Type: ##audit_type##
Audit Ref: ##audit_ref##
Audit Date: ##audit_date##
Subject / Assignment Information: ##assignment_information##
Audit Finding: ##audit_finding##
Audit Recommendation: ##audit_recommendation##
Risk Level: ##risk_level##
Risk: ##risk##
Risk Detail: ##risk_detail##