<?php
  $skip_user_setup_check = true;
  include("inc_ignite.php");
  include("inc_header.php");

  if( $_GET["code"] == "1" )
  {
    $message = "Your user setup has not been created for this module. Please contact your administrator.";
  }

?>
<div class="ui-state-error ui-corner-all" style="padding:0 0.7em;margin:2em;">
  <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> <strong>An error has occured</strong></p>
  <p><?= $message ?></p>
</div>
<?php
  include("inc_footer.php");
?>