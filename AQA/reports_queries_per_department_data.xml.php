<?php
  include("inc_ignite.php");

  $department = new Department();
  $departments = $department->get_records();

  $department_id = $_GET["department_id"];
  $start_date = $_GET["start_date"];
  $end_date = $_GET["end_date"];
  $audit_type = $_GET["audit_type"];

  $sql = "SELECT a.department_id, COUNT(*) AS cnt FROM assist_" . get_company_code() . "_aqa_assignment a join assist_" . get_company_code() . "_aqa_query q on q.id = a.query_id ";
  $has_where = false;

  if( exists( $start_date ) )
  {
    $sql .= ( $has_where ? "AND" : "WHERE" ) . " a.created_on > '" . $start_date . " 00:00:00' ";
    $has_where = true;
  }

  if( exists( $end_date ) )
  {
    $sql .= ( $has_where ? "AND" : "WHERE" ) . " a.created_on < '" . $end_date . " 23:59:59' ";
    $has_where = true;
  }
  
  if( exists( $audit_type ) )
  {
    $sql .= ( $has_where ? "AND" : "WHERE" ) . " q.audit_type = '" . $audit_type . "' ";
    $has_where = true;
  }

  $xml  = "<?xml version='1.0' encoding='UTF-8'?><chart>";

  $xml .= "<graphs>";

  $statusses = array( "New"=>"New", "Responded"=>"Responded", "In-progress"=>"In-progress", "Completed"=>"Completed", "Closed"=>"Closed" );

  foreach( $statusses as $status )
  {
    $tmp_sql = $sql;
    $tmp_sql .= ( $has_where ? "AND" : "WHERE" ) . " a.status = '" . $status . "' ";
    $tmp_sql .= " GROUP BY a.department_id";

    $records = query_records( $tmp_sql );

    $xml .= "<graph gid='" . $status . "' title='" . $status . "'>";

    foreach( $records as $record )
    {
      $xml .= "<value xid='" . $record["department_id"] . "'>" . $record["cnt"] . "</value>";
    }

    $xml .= "</graph>";
  }

  $xml .= "</graphs>";
  $xml .= "<series>";

  $tmp_sql = $sql;
  $tmp_sql .= " GROUP BY a.department_id";

  $records = query_records( $tmp_sql );

  foreach( $departments as $department )
  {
    $count = "";

    foreach( $records as $record )
    {
      if( $record["department_id"] == $department->id )
      {
        $count = $record["cnt"];
        break;
      }
    }

    $xml .= "<value xid='" . $department->id . "'>" . $department->value . ( $count > 0 ? "\n(" . $count . ")" : "" ) .  "</value>";
  }

  $xml .= "</series>";
  $xml .= "</chart>";

  echo $xml;
?>
