<?php
  include("inc_ignite.php");

  if( get_logged_in_user()->has_access( "query" ) == false )
  {
    View::redirect( "access_denied.php" );
  }

  $section = exists( $_REQUEST["section"] ) ? $_REQUEST["section"] : 5;
  $assignment_id = $_GET["assignment_id"];

  if( $_POST["action"] == View::$ACTION_SAVE )
  {
    $query = new Query( $_POST["id"] );
    $query->init( $_POST );

    if( $query->is_transient() )
    {
      $query->set_assignment_data( $_POST );
    }

    /* check if a custom risk was specified */
    if( exists( $_POST["risk_name"] ) )
    {
      $risk = new Risk();
      $risk->name = $_POST["risk_name"];
      $risk->status = "Active";
      $risk->save();

      $query->risk_id = $risk->id;
    }

    if( $query->validate() )
    {
      if( $query->save() )
      {
        View::set_session_message( "query", "Query successfully saved." );

        if( $section == 5 )
        {
          View::redirect( "queries_open.php" );
        }
        else
        {
          View::redirect( "assignment_info.php?id=" . $assignment_id . "&section=" . $section );
        }
      }
      else
      {
        View::show_db_error( $query->get_sql() );
      }
    }
  }
  else
  {
    $query = new Query( $_GET["id"] );
  }
?>

<?
  include("inc_header.php");
  include("inc_nav_query.php");
?>

<script type="text/javascript">
  var user_dept_json = <?php View::user_dept_json() ?>;
  var numAssignedUsers = <?= $query->get_num_of_assigned_users() > 0 ? $query->get_num_of_assigned_users() : 1 ?>;
</script>
<script type="text/javascript" src="lib/query.js"></script>

<h1><?= View::get_section_header( $section ) ?><?php if( $section != 5 ){ ?> &raquo; Edit Query<?php } ?></h1>

<div class="actions">
  <?= $section == 5 ? View::back_button( $section ) : View::back_button( $section, "info", $assignment_id ) ?>
</div>

<form action="" method="post" enctype="multipart/form-data" class="default">
  <?php View::hidden( "action", View::$ACTION_SAVE ) ?>
  <?php View::hidden( "id", $query->id ) ?>
  <?php View::hidden( "section", $section ) ?>
  <fieldset class="column">
    <h2 class="ui-corner-all">Query information</h2>
    <div>
      <?php View::label( null, "Audit Type", true ) ?>
      <?php View::radio( "fldAuditType_internal", "audit_type", "Internal", "Internal", $query->audit_type ) ?>
      <?php View::radio( "fldAuditType_external", "audit_type", "External", "External", $query->audit_type ) ?>
      <?php View::field_error( $query->get_field_error( "audit_type" ) ) ?>
    </div>
    <div>
      <?php View::label( "fldAuditRef", "Audit Ref", true ) ?>
      <?php View::text( "fldAuditRef", "audit_ref", $query->audit_ref, array( "maxlength"=>"20", "size"=>"20" ) ) ?>
      <?php View::field_error( $query->get_field_error( "audit_ref" ) ) ?>
    </div>
    <div>
      <?php View::label( "fldAuditDate", "Audit Date", true ) ?>
      <?php View::text( "fldAuditDate", "audit_date", ( exists( $query->audit_date ) ? $query->audit_date : View::get_current_date() ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input" ) ) ?>
      <?php View::field_error( $query->get_field_error( "audit_date" ) ) ?>
    </div>
    <div>
      <?php View::label( "fldFinancialYear", "Financial Year" ) ?>
      <?php View::select( "fldFinancialYear", "financial_year", $query->financial_year, View::get_financial_year_select_list(), "None" ) ?>
      <?php View::field_error( $query->get_field_error( "financial_year" ) ) ?>
    </div>
    <div>
      <?php View::label( "fldAssignmentInformation", "Subject / Assignment Information", true ) ?>
      <?php View::text( "fldAssignmentInformation", "assignment_information", $query->assignment_information, array( "maxlength"=>"200", "size"=>"20" ) ) ?>
      <?php View::field_error( $query->get_field_error( "assignment_information" ) ) ?>
    </div>
    <div>
      <?php View::label( "fldAuditFinding", "Audit Finding", true ) ?>
      <?php View::textarea( "fldAuditFinding", "audit_finding", $query->audit_finding, array( "cols"=>"40", "rows"=>"4" ) ) ?>
      <?php View::field_error( $query->get_field_error( "audit_finding" ) ) ?>
    </div>
    <div>
      <?php View::label( "fldAuditRecommendation", "Audit Recommendation", true ) ?>
      <?php View::textarea( "fldAuditRecommendation", "audit_recommendation", $query->audit_recommendation, array( "cols"=>"40", "rows"=>"4" ) ) ?>
      <?php View::field_error( $query->get_field_error( "audit_recommendation" ) ) ?>
    </div>
    <div>
      <?php View::label( "fldAuditDoc", "Audit Document" ) ?>
      <?php View::file( "fldAuditDoc", "audit_doc_file", $query->audit_doc ) ?>
      <?php View::field_error( $query->get_field_error( "audit_doc" ) ) ?>
    </div>
    <div>
        <?php View::label("fldPreviouslyQueried", "Previously Queried") ?>
        <?php View::boolean_checkbox("fldPreviouslyQueried", "previously_queried", $query->previously_queried) ?>
        <?php View::field_error( $query->get_field_error( "previously_queried" ) ) ?>
    </div>
    <div>
      <?php View::label( "fldRiskLevel", "Risk Level" ) ?>
      <?php View::select( "fldRiskLevel", "risk_level", $query->risk_level, View::get_risk_level_select_list(), "None" ) ?>
      <?php View::field_error( $query->get_field_error( "risk_level" ) ) ?>
    </div>
    <div>
      <?php View::label( "fldRisk", "Risk" ) ?>
      <?php View::select( "fldRisk", "risk_id", $query->risk_id, View::get_risk_select_list( true ), "None" ) ?>
      <?php View::text( "fldRiskName", "risk_name", $risk_name, array( "maxlength"=>"255", "size"=>"25", "style"=>( $query->risk_id == -1 ? "" : "display:none" ) ) ) ?>
      <?php View::field_error( $query->get_field_error( "risk_id" ) ) ?>
    </div>
    <div>
      <?php View::label( "fldRiskDetail", "Risk Detail" ) ?>
      <?php View::textarea( "fldRiskDetail", "risk_detail", $query->risk_detail, array( "cols"=>"40", "rows"=>"4" ) ) ?>
      <?php View::field_error( $query->get_field_error( "risk_detail" ) ) ?>
    </div>
  <?php if( $query->is_persisted() ){ ?>
    <?php View::submit( "Update" ) ?><?php $section == 5 ? View::section_cancel( $section ) : View::section_cancel( $section, "info", $assignment_id )  ?>
  </fieldset>
  <?php }else{?>
  </fieldset>
  <fieldset class="column ui-corner-all">
    <h2 class="ui-corner-all">Assign to</h2>
    <div>
      <?php View::button( "Add User", array( "id"=>"trigAddUser", "class"=>"ui-button" ) )?>
      <?php View::button( "Remove User", array( "id"=>"trigRemoveUser", "class"=>"ui-button" ) )?>
    </div>

    <?php View::field_error( $query->get_field_error( "user1_id" ), false ) ?>
    <?php View::field_error( $query->get_field_error( "user1_deadline" ), false ) ?>
    <?php View::field_error( $query->get_field_error( "user2_deadline" ), false ) ?>
    <?php View::field_error( $query->get_field_error( "user3_deadline" ), false ) ?>
    <?php View::field_error( $query->get_field_error( "user4_deadline" ), false ) ?>
    <?php View::field_error( $query->get_field_error( "user5_deadline" ), false ) ?>

    <table cellspacing="0" cellpadding="0" class="form">
      <thead>
        <tr>
          <th style="width:5%">#</th>
          <th style="width:40%">User</th>
          <th style="width:30%">Department</th>
          <th style="width:25%">Deadline</th>
        </tr>
      </thead>
      <tbody>
        <tr id="rowUser1" class="assignment">
          <td>1</td>
          <td>
            <?php View::text( "fldUser1", "user1", $query->user1, array( "class"=>"autocomplete", "size"=>"25" ) ) ?>
            <span id="fldUser1Status" class="user-status <?= exists( $query->user1_id ) ? "ui-button-icon-primary ui-icon ui-icon-check" : "" ?>"></span>
            <?php View::hidden( "user1_id", $query->user1_id, array( "id"=>"fldUser1Id" ) ) ?>
          </td>
          <td>
            <?php View::text( "fldUser1Dept", "user1_department", $query->user1_department, array( "readonly"=>"readonly" ) ) ?>
            <?php View::hidden( "user1_department_id", $query->user1_department_id, array( "id"=>"fldUser1DeptId" ) ) ?>
          </td>
          <td><?php View::text( "fldUser1Date", "user1_deadline", ( exists( $query->user1_deadline ) ? $query->user1_deadline : View::get_current_date() ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input", "readonly"=>"readonly" ) ) ?></td>
        </tr>
        <tr id="rowUser2" class="assignment alt" <?= exists( $query->user2_id ) ? "" : "style=\"display:none;\"" ?>>
          <td>2</td>
          <td>
            <?php View::text( "fldUser2", "user2", $query->user2, array( "class"=>"autocomplete", "size"=>"25" ) ) ?>
            <span id="fldUser2Status" class="user-status <?= exists( $query->user2_id ) ? "ui-button-icon-primary ui-icon ui-icon-check" : "" ?>"></span>
            <?php View::hidden( "user2_id", $query->user2_id, array( "id"=>"fldUser2Id" ) ) ?>
          </td>
          <td>
            <?php View::text( "fldUser2Dept", "user2_department", $query->user2_department, array( "readonly"=>"readonly" ) ) ?>
            <?php View::hidden( "user2_department_id", $query->user2_department_id, array( "id"=>"fldUser2DeptId" ) ) ?>
          </td>
          <td><?php View::text( "fldUser2Date", "user2_deadline", ( exists( $query->user2_deadline ) ? $query->user2_deadline : View::get_current_date() ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input", "readonly"=>"readonly" ) ) ?></td>
        </tr>
        <tr id="rowUser3" class="assignment" <?= exists( $query->user3_id ) ? "" : "style=\"display:none;\"" ?>>
          <td>3</td>
          <td>
            <?php View::text( "fldUser3", "user3", $query->user3, array( "class"=>"autocomplete", "size"=>"25" ) ) ?>
            <span id="fldUser3Status" class="user-status <?= exists( $query->user3_id ) ? "ui-button-icon-primary ui-icon ui-icon-check" : "" ?>"></span>
            <?php View::hidden( "user3_id", $query->user3_id, array( "id"=>"fldUser3Id" ) ) ?>
          </td>
          <td>
            <?php View::text( "fldUser3Dept", "user3_department", $query->user3_department, array( "readonly"=>"readonly" ) ) ?>
            <?php View::hidden( "user3_department_id", $query->user3_department_id, array( "id"=>"fldUser3DeptId" ) ) ?>
          </td>
          <td><?php View::text( "fldUser3Date", "user3_deadline", ( exists( $query->user3_deadline ) ? $query->user3_deadline : View::get_current_date() ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input", "readonly"=>"readonly" ) ) ?></td>
        </tr>
        <tr id="rowUser4" class="assignment alt" <?= exists( $query->user4_id ) ? "" : "style=\"display:none;\"" ?>>
          <td>4</td>
          <td>
            <?php View::text( "fldUser4", "user4", $query->user4, array( "class"=>"autocomplete", "size"=>"25" ) ) ?>
            <span id="fldUser4Status" class="user-status <?= exists( $query->user4_id ) ? "ui-button-icon-primary ui-icon ui-icon-check" : "" ?>"></span>
            <?php View::hidden( "user4_id", $query->user4_id, array( "id"=>"fldUser4Id" ) ) ?>
          </td>
          <td>
            <?php View::text( "fldUser4Dept", "user4_department", $query->user4_department, array( "readonly"=>"readonly" ) ) ?>
            <?php View::hidden( "user4_department_id", $query->user4_department_id, array( "id"=>"fldUser4DeptId" ) ) ?>
          </td>
          <td><?php View::text( "fldUser4Date", "user4_deadline", ( exists( $query->user4_deadline ) ? $query->user4_deadline : View::get_current_date() ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input", "readonly"=>"readonly" ) ) ?></td>
        </tr>
        <tr id="rowUser5" class="assignment" <?= exists( $query->user5_id ) ? "" : "style=\"display:none;\"" ?>>
          <td>5</td>
          <td>
            <?php View::text( "fldUser5", "user5", $query->user5, array( "class"=>"autocomplete", "size"=>"25" ) ) ?>
            <span id="fldUser5Status" class="user-status <?= exists( $query->user5_id ) ? "ui-button-icon-primary ui-icon ui-icon-check" : "" ?>"></span>
            <?php View::hidden( "user5_id", $query->user5_id, array( "id"=>"fldUser5Id" ) ) ?>
          </td>
          <td>
            <?php View::text( "fldUser5Dept", "user5_department", $query->user5_department, array( "readonly"=>"readonly" ) ) ?>
            <?php View::hidden( "user5_department_id", $query->user5_department_id, array( "id"=>"fldUser5DeptId" ) ) ?>
          </td>
          <td><?php View::text( "fldUser5Date", "user5_deadline", ( exists( $query->user5_deadline ) ? $query->user5_deadline : View::get_current_date() ), array( "maxlength"=>"10", "size"=>"10", "class"=>"date-input", "readonly"=>"readonly" ) ) ?></td>
        </tr>

      </tbody>
    </table>
  </fieldset>
  <?php View::submit( "Save", true ) ?><?php $section == 5 ? View::section_cancel( $section ) : View::section_cancel( $section, "info", $assignment_id )  ?>
  <?php } ?>
</form>

<?php
  include("inc_footer.php");
?>