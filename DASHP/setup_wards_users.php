<?php
include("inc_head_setup.php");
include("inc/setup.php");
$row = array();
$ref = $_GET['i'];
?>
<style type=text/css>
.tdmain {
    border-right: 0px;
    line-height: 12pt;
}
.tdbut {
    border-left: 0px;
}
</style>
<script type=text/javascript>
function Validate(me) {
    var nt = me.w.value;
    var ot = me.w2.value;
    
    if(nt.length>0 && nt != ot)
    {
        return true;
    }
    else
    {
        alert("Please enter a new value that is different from the original value.");
        return false;
    }
}
function editWard(i) {
    if(i>0 && !isNaN(parseInt(i)))
    {
        if(confirm("Are you sure you wish to delete this <?php echo(strtolower(strFn("substr",$setup[3][0],0,-1))); ?>?")==true)
        {
            document.location.href = "setup_wards.php?a=wards&act=del&w="+i;
        }
    }
    else
    {
        alert("An error has occured.\nPlease reload the page and try again.");
    }
}
function delUser(i,t) {
    if(i>0 && !isNaN(parseInt(i)))
    {
        if(confirm("Are you sure you wish to delete "+t+" as a user from this <?php echo(strtolower(strFn("substr",$setup[3][0],0,-1))); ?>?")==true)
        {
            document.location.href = "setup_wards_users.php?a=wards&act=delu&w="+i+"&i=<?php echo($ref); ?>";
        }
    }
    else
    {
        alert("An error has occured.\nPlease reload the page and try again.");
    }
}
</script>
<h1><b><?php echo($modtitle); ?>: Setup - <?php echo($setup[3][0]); ?> ~ Edit</b></h1>
<?php displayResult($result); ?>
<?php

if(strlen($ref)>0 && is_numeric($ref))
{
    $sql = "SELECT * FROM ".$dbref."_list_wards WHERE id = $ref AND yn = 'Y'";
    include("inc_db_con.php");
    $mnr = mysql_num_rows($rs);
    $detail = mysql_fetch_array($rs);
    mysql_close($con);
}
else
{
    $mnr = 0;
}
if($mnr == 0)
{
    echo("<h2>ERROR</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
?>
<form name=ward method=get action=setup_wards_edit.php onsubmit="return Validate(this)" language=jscript>
<input type=hidden name=a value=wards><input type=hidden name=act value=addu>
<table cellpadding=3 cellspacing=0 width=600>
    <tr height=27>
        <td width=150 class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff;">Reference:</th>
        <td width=450 ><?php echo($ref); ?><input type=hidden name=i value=<?php echo($ref); ?>></td>
    </tr>
    <tr height=27>
        <td class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff; border-top: 1px solid #ffffff;"><?php strFn("substr",$setup[3][0],0,-1); ?> Name:</th>
        <td><?php echo($detail['value']); ?></td>
    </tr>
<?php
//    $sql2 = "SELECT a.id, t.tkname, t.tksurname FROM ".$dbref."_list_admins a, assist_".$cmpcode."_timekeep t WHERE a.yn = 'Y' AND a.type = 'W' AND a.tkid = t.tkid AND t.tkstatus = 1 AND ref = ".$ref;
//    $sql2.= " ORDER BY t.tkname, t.tksurname";
    $sql2 = userList("W",$ref,"L");
//	$sql2 = userList("",0,"A");
//	echo($sql2);
    include("inc_db_con2.php");
        if(mysql_num_rows($rs2)==0)
        {
            $usrs[0][1] = "No Administrators";
        }
        else
        {
            $usrs = array();
            while($row2 = mysql_fetch_array($rs2))
            {
                $usr[0] = $row2['id'];
                $usr[1] = $row2['tkname']." ".$row2['tksurname'];
                $usrs[] = $usr;
            }
        }
     mysql_close($con2);

?>
    <tr height=27>
        <td class=tdheader valign=top style="text-align: left; padding-top: 5px; border-bottom-color: #ffffff; border-top: 1px solid #ffffff;">Current Admins:</th>
        <?php if($usrs[0][1] == "No Administrators") { ?>
        <td>No Administrators</td>
        <?php } else { ?>
        <td><table cellpadding=1 cellspacing=0 style="border: 0px;" width=300>
        <?php foreach($usrs as $us) { ?>
        <?php include("inc_tr.php"); ?>
            <td width=271 style="border: 1px solid #ffffff; border-right: 0px;"><?php echo($us[1]); ?></td>
            <td width=29 align=center style="border: 1px solid #ffffff; border-left: 0px;"><input type=button value=Del onclick="delUser(<?php echo($us[0]); ?>,<?php echo("'".$us[1]."'"); ?>)"></td>
        </tr>
        <?php } ?>
        </table></td>
        <?php } ?>
    </tr>
<?php for($u=1;$u<6;$u++) {
?>
    <tr height=27>
        <td class=tdheader style="text-align: left; border-bottom: 1px solid #ffffff; border-top: 1px solid #ffffff;">New Admin:</th>
        <td><select name=ad[]><option selected value=X>--- SELECT ---</option><?php
//        $sql = "SELECT t.tkid, t.tkname, t.tksurname FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users u ";
//        $sql.= "WHERE t.tkid = u.usrtkid AND u.usrmodref = '$modref'";
//        $sql.= " AND t.tkid NOT IN (SELECT tkid FROM ".$dbref."_list_admins WHERE type = 'W' AND ref = $ref AND yn = 'Y')";
    	$sql = userList("W",$ref,"A");

        include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                echo("<option value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
            }
        mysql_close($con);
        ?></select></td>
    </tr>
<?php } ?>
    <tr height=27>
        <td colspan=2><input type=submit value="Add users"> <input type=reset> <input type=button value=Cancel onclick="document.location.href = 'setup_wards_edit.php?i=<?php echo($ref); ?>';"></td>
    </tr>
</table>
</form>
<?php
}
$urlback = "setup_wards_edit.php?i=$ref";
include("inc_goback.php");
?>
</body>

</html>
