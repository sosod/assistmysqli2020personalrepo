<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminEdit.php");
include("inc/AdminEditProgress.php");


?>

<h1><?php echo($modtitle); ?>: Admin - Edit a Dashboard</h1>
<?php displayResult($result); ?>
<?php

$ref = $variables['ref'];
if(strlen($ref)==0) { $ref = $result[2]; }
if(!checkIntRef($ref))
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);
    echo("<h2>Congratulations</h2>");
    echo("<p>You have successfully edited Dashboard $ref: ".$dash['dashname'].".</p>");
    $urlback = "admin.php";
    include("inc_goback.php");
} //endif ref error ?>
<P>&nbsp;</p>
</body>
</html>
