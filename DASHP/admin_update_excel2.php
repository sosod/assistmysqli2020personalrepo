<?php
include("inc_head_export.php");
include("inc/Admin.php");
$frequency = getCaptureFreq();
$time = getTime($variables['timeid']);
$timeid = $variables['timeid'];
$timedate = $variables['timedate'];
//print_r($setup);
//$variables = $_REQUEST;
$ref = $variables['ref'];
if(checkIntRef($ref)) {
    $dash = getDashboard($ref);
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$dash['dashowner']."'";
    include("inc_db_con.php");
        $dashowner = mysql_fetch_array($rs);
    mysql_close($con);
    $records = getRecords($ref);
    $fields = getFields($ref);
    $vars = getVariablesData($ref);
    $types = getTypes();
    $datatime = array($timeid,$timeid);
    $data = getDataMonth($ref,$datatime,"",$timedate);
} else {
    die("<h2>Error</h2><p>An error has occured.  Please go back and try again.</p>");
}
//print_r($data);
$thead = "";
if($dash['dashfrequency']>3) {
    switch($dash['dashfrequency'])
    {
        case 4:
            $thead.="Month ending ";
            break;
        case 5:
            $thead.="2-Months ending ";
            break;
        case 6:
            $thead.="Quarter ending ";
            break;
        case 7:
            $thead.="4-Months ending ";
            break;
        case 8:
            $thead.="6-Months ending ";
            break;
        case 9:
            $thead.="Year ending ";
            break;
    }
    $thead.= date("d F Y",$time[$timeid]['eval']);
    $timedate = date("d-m-Y",$time[$timeid]['eval']);
} else {
    $td = strFn("explode",$timedate,"-","");
    $thead = "Day of ".date("d F Y",mktime(12,0,0,$td[1],$td[0],$td[2]));
    //$thead = "IN PROGRESS";
}

$fdata = "";
$fdata.= "\"Dashboard Details\"";
$fdata.= "\r\n";
$fdata.= "\"Reference:\",\"$ref\"";
$fdata.= "\r\n";
$fdata.= "\"Dashboard Name:\",\"".decode($dash['dashname'])."\"";
$fdata.= "\r\n";
$fdata.= "\"Dashboard Owner:\",\"".decode($dashowner['tkname']." ".$dashowner['tksurname'])."\"";
$fdata.= "\r\n";
$fdata.= "\"Capture Frequency:\",\"".YesNo($dash['dashsdbip'])."\"";
$fdata.= "\r\n";
//$fdata.= "\r\n";

$frame = array();
$frameid = strFn("explode",$variables['frame'],"_","");
if(checkIntRef($frameid[1]))
{
    switch($frameid[0])
    {
        case "D":
            $frame['dir'] = getSection("D",$ref,$frameid[1]);
            $frame['sub'] = getSections("S",$ref);
            $frame['towns'] = getSections("T",$ref);
            $frame['wards'] = getSections("W",$ref);
            break;
        case "S":
//            $frame['dir'] = getSection("D",$ref,$frameid[1]);
            $frame['sub'] = getSection("S",$ref,$frameid[1]);
            foreach($frame['sub'] as $sb) { $dirid = $sb[0]['subdirid']; }
            $frame['dir'] = getSection("D",$ref,$dirid);
            $frame['towns'] = getSections("T",$ref);
            $frame['wards'] = getSections("W",$ref);
            break;
        case "T":
            $frame['dir'] = getSections("D",$ref);
            $frame['sub'] = getSections("S",$ref);
            $frame['towns'] = getSection("T",$ref,$frameid[1]);
            break;
        case "W":
            $frame['dir'] = getSections("D",$ref);
            $frame['sub'] = getSections("S",$ref);
            $frame['wards'] = getSection("W",$ref,$frameid[1]);
            break;
        default:
            die("An error has occurred.  Please go back and try again.");
            break;
    }
}
else
{
    die("An error has occurred.  Please go back and try again.");
}

//echo("<P>FRAME-");
//print_r($frame);
    $values = array();
//echo("<P>VAR-"); print_r($vars);
    $i = 1;

        $sections = array(
            "dir"=>$dash['dashdir'],
            "sub"=>$dash['dashsub'],
            "towns"=>$dash['dashtowns'],
            "wards"=>$dash['dashwards']
        );

            $monthcols = count($fields);

        if($sections['towns']<0) { unset($sections['towns']); }
        if($sections['wards']<0) { unset($sections['wards']); }
        $l = 0;
        $m = 2;
        for($m2=1;$m2<=$m;$m2++)
        {
            $l = $l + ($monthcols * $m);
        }
        $m--;
//echo("<P>SECTIONS-"); print_r($sections);
$fdata.= "\"$thead\"";
$fdata.= "\r\n";
$fdata.= "\"\",\"\",\"\",\"\"";
foreach($fields as $fld)
{
    if($fld['fldfn']!="Y") {
        $fdata.= ",\"".decode($fld['fldtxt'])."\"";
    }
}
$fdata.= "\r\n";

        foreach($frame['dir'] as $dir)
        {
            $did = $dir['id'];
            $txt = decode($dir['txt']);
            $subs = $frame['sub'][$did];
                foreach($subs as $sub)
                {
                    $subid = $sub['id'];
                    $txt2 = decode($sub['txt']);
                    $extras = array(1=>"wards",2=>"towns");
                    $preview = 0;
                    foreach($extras as $ex)
                    {
                        if($ex == "wards") { $type = "W"; } else { $type = "T"; }
                        if($sections[$ex]>0)
                        {
                            $secs = $frame[$ex];
                            foreach($secs as $sec)
                            {
                                $sid = $sec['id'];
                                $sdfid = $sec['dfid'];
                                $stxt = decode($sec['txt']);
                                    foreach($records as $rec)
                                    {
                                        $recid = $rec['recid'];
                                        $fn = $rec['recfn'];
                                        $rtxt = decode($rec['rectxt']);
                                        if($fn != "Y" && $fn!="H") {
                                            $fdata.= "\"$txt\",\"$txt2\",\"$stxt\",\"$rtxt\"";
                                            foreach($fields as $fld)
                                            {
                                                if($fld['fldfn']!="Y") {
                                                    $fldid = $fld['fldid'];
                                                    $var = $vars[$recid][$fldid];
                                                    $val = $data[$timeid][$did][$subid][$sdfid][$recid][$fldid]['value'];
                                                switch($var['vartype'])
                                                {
                                                    case "R":
                                                        $fdata.=",\"".$val."\"";
                                                        break;
                                                    case "NUM":
                                                        $fdata.=",\"".$val."\"";
                                                        break;
                                                    case "PERC":
                                                        $fdata.=",\"".$val."\"";
                                                        break;
                                                    case "DT":
                                                            if(strlen($val)>0) {
                                                                $val = strFn("explode",$val,",","");
                                                                $val = date("d-m-Y",$val[1]);
                                                            } else {
                                                                $val = "";
                                                            }
                                                        $fdata.=",\"".$val."\"";
                                                        break;
                                                    case "LT":
                                                        if(strlen($val)>1) {
                                                            $val = strFn("explode",$val,",","");
                                                            if(is_numeric($val[1]) && strlen($val[1])>0) {
                                                                $sql3 = "SELECT value FROM ".$dbref."_list_dropdowns_values WHERE id = ".$val[1];
                                                                include("inc_db_con3.php");
                                                                    $row3 = mysql_fetch_array($rs3);
                                                                    $fdata.=",\"".decode($row3['value'])."\"";
                                                                mysql_close($con3);
                                                            } else {
                                                                $fdata.=",\"\"";
                                                            }
                                                        } else {
                                                            $fdata.=",\"\"";
                                                        }
                                                        break;
                                                    default:
                                                        $fdata.=",\"".strFn("str_replace",decode($val),"<br>",chr(10))."\"";
                                                        break;
                                                }
                                                }
                                            }
                                            $fdata.= "\r\n";
                                        }
                                    }
                            }
                        }
                    }
                }
        }
//        print_r($data);
        //WRITE DATA TO FILE
        $filename = "../files/".$cmpcode."/dashboard_".$ref."_".date("Ymd_Hi",$today).".csv";
        $newfilename = "dashboard_update_".$ref."_".date("dFY_Hi",$today).".csv";
        $file = fopen($filename,"w");
        fwrite($file,$fdata."\n");
        fclose($file);
        //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$newfilename.'"');
        readfile($filename);

?>
