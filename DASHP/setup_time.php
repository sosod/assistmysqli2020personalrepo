<?php
include("inc_head_setup.php");
include("inc/setup.php");
?>
<style type=text/css>
.tdmain {
    border-right: 0px;
    line-height: 12pt;
}
.tdbut {
    border-left: 0px;
}
</style>
<script type=text/javascript>
function editWard(i) {
    if(i>0 && !isNaN(parseInt(i)))
    {
        document.location.href = "setup_towns_edit.php?i="+i;
    }
    else
    {
        alert(i.length+"-"+isNaN(parseInt(i))+"An error has occured.\nPlease reload the page and try again.");
    }
}
function Validate(me) {
    if(me.w.value.length>0)
        return true;
    else
        return false;
}
</script>
<h1><b><?php echo($modtitle); ?>: Setup - Time Periods</b></h1>
<?php displayResult($result); ?>
<table cellpadding=3 cellspacing=0 width=570 style="margin-top: 30px;">
    <tr height=30>
        <th class=lft width=110>Update</th>
        <td>Update a time period.*</td>
        <td align=center><input type=button value=Update onclick="setupGoTo('time_list');"></td>
    </tr>
    <tr height=30>
        <th class=lft>Setup</th>
        <td>Setup automatic reminder emails and closure* of time periods.</td>
        <td align=center><input type=button value=" Setup " onclick="setupGoTo('time_setup');"></td>
    </tr>
</table>
<p style="font-size: 7.5pt; margin-top: 5px;"><i>* Note: Once a time period is closed you will need to contact Ignite Advisory Services to reopen it.</i></p>
<?php
$urlback = "setup.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
