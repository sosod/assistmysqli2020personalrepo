<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminUpdate.php");

//print_r($variables);
?>
<h1><?php echo($modtitle); ?>: Admin - Update a Dashboard</h1>
<?php displayResult($result); ?>


<?php
if($variables['dash']=="Y") {
    $urlback = "admin_dash_view.php?ref=".$variables['ref'];
} else {
    $urlback = "admin_update.php";
}
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
