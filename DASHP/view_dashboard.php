<?php
include("inc_head.php");
include("inc/Admin.php");
//$variables = $_REQUEST;

$frequency = getCaptureFreq();
$time = getTime();
$timeid = $variables['timeid'];
$timedate = $variables['timedate'];
$datatime = array($timeid,$timeid);
$timeend = $time[$timeid]['eval'];
$timeenddays = date("d",$timeend);

$ref = $variables['ref'];
if(checkIntRef($ref)) {
    $dash = getDashboard($ref);
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$dash['dashowner']."'";
    include("inc_db_con.php");
        $dashowner = mysql_fetch_array($rs);
    mysql_close($con);
    $records = getRecords($ref);
    $fields = getFields($ref);
    $vars = getVariables($ref);
    $types = getTypes();
    $data = getDataMonth($ref,$datatime,"",$timedate);
} else {
    die("<h2>Error</h2><p>An error has occured.  Please go back and try again.</p>");
}

$thead = "";
if($dash['dashfrequency']>3) {
    switch($dash['dashfrequency'])
    {
        case 4:
            $thead.="Month ending ";
            break;
        case 5:
            $thead.="2-Months ending ";
            break;
        case 6:
            $thead.="Quarter ending ";
            break;
        case 7:
            $thead.="4-Months ending ";
            break;
        case 8:
            $thead.="6-Months ending ";
            break;
        case 9:
            $thead.="Year ending ";
            break;
    }
    $thead.= date("d F Y",$time[$timeid]['eval']);
} else {
//    $thead = "IN PROGRESS";
    $td = strFn("explode",$timedate,"-","");
    $thead = date("d F Y",mktime(12,0,0,$td[1],$td[0],$td[2]));

}
?>
<h1><?php echo($modtitle); ?>: View</h1>
<h2>Dashboard Details</h2>
<form id=frm action="admin_update_draw3.php" method=post>
<input type=hidden name=ref value="<?php echo($ref);?>">
<input type=hidden name=timeid value="<?php echo($timeid);?>">
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td class="tdheaderl b-bottom-w" width=200>Reference:</td>
		<td width=400><?php echo($ref); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Name:</td>
		<td><?php echo($dash['dashname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Dashboard Owner:</td>
		<td><?php echo($dashowner['tkname']." ".$dashowner['tksurname']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Capture Frequency:</td>
		<td><?php echo($frequency[$dash['dashfrequency']]['value']); ?></td>
	</tr>
	<tr>
		<td class="tdheaderl b-bottom-w b-top-w">Link to SDBIP?:</td>
		<td><?php echo(YesNo($dash['dashsdbip'])); ?></td>
	</tr>
</table>
<p>&nbsp;</p>
<div align=center>
<?php
            $frame['dir'] = getSections("D",$ref);
            $frame['sub'] = getSections("S",$ref);
            $frame['towns'] = getSections("T",$ref);
            $frame['wards'] = getSections("W",$ref);

/*echo("<P>DATA-");
print_r($data);
echo("<P>RECORDS-");
print_r($records);
echo("<P>FIELDS-");
print_r($fields);
echo("<P>VARS-");
print_r($vars);
echo("<P>FRAME-");
print_r($frame);*/
    $values = array();
//echo("<P>VAR-"); print_r($vars);
    $i = 1;

        $sections = array(
            "dir"=>$dash['dashdir'],
            "sub"=>$dash['dashsub'],
            "towns"=>$dash['dashtowns'],
            "wards"=>$dash['dashwards']
        );

            $monthcols = count($fields);

        if($sections['towns']<0) { unset($sections['towns']); }
        if($sections['wards']<0) { unset($sections['wards']); }
        $l = 0;
        $m = 2;
        for($m2=1;$m2<=$m;$m2++)
        {
            $l = $l + ($monthcols * $m);
        }
        $m--;
//echo("<P>SECTIONS-"); print_r($sections);
        echo("<table cellpadding=3 cellspacing=0>");
        echo("<tr>");
        echo("    <td rowspan=2>&nbsp;</td>");
        echo("    <td class=tdheader colspan=$monthcols > $thead </td>");
        echo("</tr>");
        echo("<tr>");
            $mx = echoFieldDash($ref,$m,$fields);
        echo("</tr>");
        foreach($frame['dir'] as $dir)
        {
            $lev = 1;
            $did = $dir['id'];
            $txt = $dir['txt'];
            $subs = $frame['sub'][$did];
                echo("<tr>");
                    echo("<td class=level".$lev." colspan=$l >$txt</td>");
                echo("</tr>");
                foreach($subs as $sub)
                {
                    $lev = 2;
                    $subid = $sub['id'];
                    $txt2 = $sub['txt'];
                    echo(chr(10)."<tr>");
                        echo("<td class=level".$lev." colspan=$l >$txt2</td>");
                    echo("</tr>");
                    $lev = 3;
                    $extras = array(1=>"wards",2=>"towns");
                    $preview = 0;
                    foreach($extras as $ex)
                    {
                        if($ex == "wards") { $type = "W"; } else { $type = "T"; }
                        if($sections[$ex]>0)
                        {
                            $preview++;
                            $secs = $frame[$ex];
                            $s = 1;
                            foreach($secs as $sec)
                            {
                                $sid = $sec['id'];
                                $sdfid = $sec['dfid'];
                                $stxt = $sec['txt'];
                                echo(chr(10)."<tr>");
                                    echo("<td class=level".$lev." colspan=$l >$stxt</td>");
                                echo("</tr>");
                                $s++;
                                    foreach($records as $rec)
                                    {
                                        $recid = $rec['recid'];
                                        $fn = $rec['recfn'];
//                                        if($fn == "Y") { $class = "total"; $varclass = "vartotal"; } else { $class = "record"; $varclass = "varrecord"; }
                                        switch($fn) {
                                            case "H":
                                                $class = "heading"; $varclass = "varheading";
                                                break;
                                            case "Y":
                                                $class = "total"; $varclass = "vartotal";
                                                break;
                                            default:
                                                $class = "record"; $varclass = "varrecord";
                                                break;
                                        }

                                        echo("<tr height=25>");
                                        echo(chr(10)."<td class=".$class." width=100>".$rec['rectxt']."</td>");
                                        foreach($fields as $fld)
                                        {
                                            $fldid = $fld['fldid'];
                                            $ffn = $fld['fldfn'];
                                            if($ffn == "Y" && $fn != "H") { $class = "total"; $varclass = "vartotal"; }
                                            if($fn == "Y" && $ffn == "Y") { $varclass.= " style=\"text-decoration: underline;\""; }
                                            $fldid = $fld['fldid'];
                                            $var = $vars[$recid][$fldid];
            if($var['vartype']=="NUM" || $var['vartype']=="PERC" || $var['vartype']=="R") {
                $tdstyle = " style=\"text-align: right;\"";
            } else {
                $tdstyle = "";
            }
                                            echo(chr(10)."<td class=$varclass valign=middle $tdstyle >");  //print_r($var);
                                        if($fn=="H") {
                                            echo("&nbsp;");
                                        } else {
                                            $vartype = $var['vartype'];
                                            $val = $data[$timeid][$did][$subid][$sdfid][$recid][$fldid]['value'];
//                                            $vt =  $data[$timeid][$did][$subid][$sdfid][$recid][$fldid]['vt'];
                                            if(strlen($val)>0) {
                                                switch($vartype)
                                                {
                                                    case "BLK":
                                                        echo("&nbsp;");
                                                        break;
                                                    case "NUM":
                                                        echo(number_format($val,2)." ".$var['varunit']);
                                                        break;
                                                    case "PERC":
                                                        echo(number_format($val,2)." ".$var['varunit']);
                                                        break;
                                                    case "R":
                                                        echo("R ".number_format($val,2));
                                                        break;
                                                    case "DT":
                                                        if(strlen($val)>0)
                                                        {
                                                            $vl = strFn("explode",$val,",","");
                                                            $vc = 0;
                                                            foreach($vl as $v)
                                                            {
                                                                if(is_numeric($v) && strlen($v)>0)
                                                                {
                                                                    if($vc>0) { echo("<br>"); }
                                                                    echo(date("d M Y",$v));
                                                                    $vc++;
                                                                }
                                                            }
                                                        }
//                                                        echo($val);
                                                        //echo(date("d M Y",$val));
                                                        break;
                                                    case "LT":
                                                        if(strlen($val)>0)
                                                        {
                                                            $vl = strFn("explode",$val,",","");
                                                            $vc = 0;
                                                            foreach($vl as $v)
                                                            {
                                                                if(is_numeric($v) && strlen($v)>0)
                                                                {
                                                                    if($vc>0) { echo("<br>"); }
                                                                    $sql3 = "SELECT value FROM ".$dbref."_list_dropdowns_values WHERE id = ".$v;
                                                                    include("inc_db_con3.php");
                                                                        $row3 = mysql_fetch_array($rs3);
                                                                        echo($row3['value']);
                                                                    mysql_close($con3);
                                                                    $vc++;
                                                                }
                                                            }
                                                        }
/*                                                        if(is_numeric($val) && strlen($val)>0) {
                                                            $sql3 = "SELECT value FROM ".$dbref."_list_dropdowns_values WHERE id = ".$val;
                                                            include("inc_db_con3.php");
                                                                $row3 = mysql_fetch_array($rs3);
                                                                echo($row3['value']);
                                                            mysql_close($con3);
                                                        } else {
                                                            echo(" ");
                                                        }*/
//                                                        echo($val);
                                                        break;
                                                    default:
                                                        echo($val);
                                                        break;
                                                }
                                            }
                                        }
                                            echo("</td>");
                                        }
                                        echo("</tr>");
                                    }
                            }
                        }
                    }
                    $lev = 2;
                    echo("<tr>");
                        echo("<td class=level".$lev."total colspan=1 align=right >Sub-Total for<br>$txt2:</td>");
                        foreach($fields as $fld)
                        {
                            echo("<td class=level".$lev."total >"); 
                            if($fld['fldfn']=="Y") {
                                $subvalues[$fld['fldid']][$subid] = subTotalDisplayValue($fld['fldid'],$vars['SUBF'][$fld['fldid']],$subvalues,"SUBF",$subid);
                            } else {
                                $subvalues[$fld['fldid']][$subid] = subTotalDisplayValue($fld['fldid'],$vars['SUB'][$fld['fldid']],$data[$timeid][$did][$subid],"SUB",$subid);
                            }
                            echo("</td>");
                        }
                    echo("</tr>");
                }
                $lev = 1;
                echo(chr(10));
                echo("<tr>");
                    echo("<td class=level".$lev."total colspan=1 align=right >Total for<br>$txt:</td>");
                                foreach($fields as $fld)
                                {
                                    echo("<td class=level".$lev."total >");
                                        if($fld['fldfn']=="Y")
                                        {
                                            totalDisplayValue($fld['fldid'],$vars['DIRF'][$fld['fldid']],$subvalues);
                                        }
                                        else
                                        {
                                            totalDisplayValue($fld['fldid'],$vars['DIR'][$fld['fldid']],$subvalues);
                                        }
                                    echo("</td>");
                                }
                echo("</tr>");
                $subvalues = array();
        }
        echo("</table>");

?>
<p>&nbsp;</p>
</div>
</form>
<?php
$urlback = "view.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
