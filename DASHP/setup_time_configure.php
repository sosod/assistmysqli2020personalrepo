<?php
include("inc_head_setup.php");
include("inc/setup.php");
?>
		<script type="text/javascript">
			$(function(){

                //Emails
                $('.date-input').datepicker({
                    showOn: 'both',
                    buttonImage: 'lib/images/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-m-yy',
                    changeMonth:true,
                    changeYear:true
                });

			});

function closeTP(t,txt) {
    if(escape(t)==t && !isNaN(parseInt(t)))
    {
        if(confirm("Are you sure you wish to close the time period: "+txt+"?\nTo reopen the time period, you will need to contact Ignite Advisory Services.\n\nIf yes, please click 'OK', else click 'Cancel'.")==true)
        {
            document.location.href = "setup_time_configure_close.php?t="+t;
        }
    }
    else
    {
        alert("An error has occurred.  Please reload the page and try again.");
    }
}

		</script>
<h1><b><?php echo($modtitle); ?>: Setup - Time Periods ~ Update</b></h1>
<?php displayResult($result); ?>
<?php
$tid = $_GET['t'];

    $sql = "SELECT * FROM ".$dbref."_list_time WHERE id = ".$tid;
    include("inc_db_con.php");
        $trow = mysql_fetch_array($rs);
    mysql_close();
    $tval = date("d F Y", $trow['sval'])." - ".date("d F Y", $trow['eval']);

?>
<h2><?php echo(date("d F Y",$trow['sval'])); ?> - <?php echo(date("d F Y",$trow['eval'])); ?></h2>
<form name=edit method=get action=setup_time_list.php>
<input type=hidden name=a value=time><input type=hidden name=act value=save>
<input type=hidden name=t value=<?php echo($tid); ?>>
<table cellpadding=3 cellspacing=0 width=570>
    <tr>
		<td class=tdheaderl style="border-bottom: 1px solid #ffffff;" width=120>Auto Reminder:</td>
		<td class=tdgeneral valign=top><input type=text name=arem class="date-input" readonly="readonly" value="<?php if($trow['arem']>0) { echo(date("d-m-Y",$trow['arem'])); } ?>"><input type=hidden id=actualDate2></td>
	</tr>
    <tr>
		<td class=tdheaderl style="border-bottom: 1px solid #ffffff;" width=120>Auto Close:</td>
		<td class=tdgeneral valign=top><input type=text name=aclose class="date-input" readonly="readonly" value="<?php if($trow['aclose']>0) { echo(date("d-m-Y",$trow['aclose'])); } ?>"></td>
	</tr>
    <tr>
		<td class=tdheaderl width=120>&nbsp;</td>
		<td class=tdgeneral valign=top><input type=submit value=" Save Changes "> <?php if($trow['eval']<$today) { ?><input type=button value=" Close " onclick="closeTP(<?php echo($trow['id']); ?>,'<?php echo($tval); ?>')"><?php } ?></td>
    </tr>
</table>
</form>
<p style="font-size: 7.5pt; margin-top: 5px;"><i> Note:<br>
<span style="font-size: 6.5pt;">+</span> To cancel an Auto Closure or Auto Reminder, set the date in the past.<br>
<span style="font-size: 6.5pt;">+</span> Time periods may only be closed once the end date has passed.<br>
<span style="font-size: 6.5pt;">+</span> Once a time period is closed you will need to contact Ignite Advisory Services to reopen it.<br>
<span style="font-size: 6.5pt;">+</span> All automatic closures and reminder emails occurr at 01:00 on the date selected.</i></p>
<?php
$urlback = "setup_time_list.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
