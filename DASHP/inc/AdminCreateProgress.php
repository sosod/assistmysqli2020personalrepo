<?php
$p1 = 0;  $p2 = 1;
$process[$p1][$p2] = array(    "id"=>$p2,    "txt"=>"Setup Dashboard"    ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "txt"=>"Setup Records",      "redo"=>"2-1"    ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "txt"=>"Setup Fields",       "redo"=>"3-1"    ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "txt"=>"Setup Variables",    "redo"=>"4-1"    ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "txt"=>"Setup SDBIP Link",   "redo"=>"5-1"    ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "txt"=>"Finalise"     );
$p1 = 1; $p2 = 1;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>$p1."-".($p2+1),    "txt"=>"Setup Dashboard"         ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>$p1."-".($p2+1),    "txt"=>"Setup Directorates"      ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>$p1."-".($p2+1),    "txt"=>"Setup Sub-Directorates"  ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>$p1."-".($p2+1),    "txt"=>"Setup ".$setup[3][0]     ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>$p1."-".($p2+1),    "txt"=>"Setup Towns"             ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>"2-1",              "txt"=>"Preview"                 );
$p1 = 2; $p2 = 1;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>"2-1",              "txt"=>"Setup Records"               ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>$p1."-".($p2+1),    "txt"=>"Records - Pre-defined Lists" ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>$p1."-".($p2+1),    "txt"=>"Records - Manual"            ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>"3-1",              "txt"=>"Preview"                     );
$p1 = 3; $p2 = 1;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>"3-1",              "txt"=>"Setup Fields"               ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>$p1."-".($p2+1),    "txt"=>"Fields - Pre-defined Lists" ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>$p1."-".($p2+1),    "txt"=>"Fields - Manual"            ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>"4-1",              "txt"=>"Preview"                    );
$p1 = 4; $p2 = 1;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>"4-2",              "txt"=>"Variables"           ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>"5-1",              "txt"=>"Preview"             );
$p1 = 5; $p2 = 1;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>"5-2",              "txt"=>"SDBIP Link"           ); $p2++;
$process[$p1][$p2] = array(    "id"=>$p2,    "next"=>"6-1",              "txt"=>"Preview"                    );
$p1 = 6; $p2 = 1;
$process[$p1][$p2] = array(    "id"=>$p2,    "txt"=>"Finalise"           );




//DISPLAY PROCESS
function displayProgress($title,$step,$total) {
global $process;
global $ref;

    echo("<table cellpadding=10 cellspacing=0 class=noborder width=600>");
    echo("    <tr>");
    echo("        <td class=noborder width=300 valign=top>");
    if(count($step)>0)
    {
        echo("            <h3 style=\"margin-bottom: 5px;\"> $title </h3>");
        echo("            <table cellpadding=2 cellspacing=0 class=noborder>");
        foreach($step as $prog) {
            echo("                <tr>");
            echo("                    <td class=noborder>");
                switch($prog['status'])
                {
                    case "C":
                        echo("<span class=\"ui-icon ui-icon-arrowthick-1-e\" style=\"float: left; margin-right: 0em;\">");
                        break;
                    case "N":
                        echo("&nbsp;");
                        break;
                    case "NA":
                        echo("<span class=\"ui-icon ui-icon-circle-minus\" style=\"float: left; margin-right: 0em;\">");
                        break;
                    case "Y":
                        echo("<span class=\"ui-icon ui-icon-circle-check\" style=\"float: left; margin-right: 0em;\">");
                        break;
                    default:
                        break;
                }
            echo("                    </td>");
            echo("                    <td class=noborder>".$prog['id'].".</td>");
            echo("                    <td class=noborder>".$prog['txt']."</td>");
            echo("                </tr>");
        }
        echo("            </table>");
    }
    echo("        </td>");
    echo("        <td class=noborder width=300 valign=top style=\"text-align:right;\">");
    echo("            <h3 style=\"margin-bottom: 5px;\"> Total Process </h3>");
    echo("            <table cellpadding=2 cellspacing=0 class=noborder>");
    foreach($total as $prog) {
        $redo = $process[0][$prog['id']]['redo'];
        if(strlen($redo) == 3 && $prog['status'] == "Y") { $url = "<a href=admin_create$redo.php?ref=$ref>"; } else { $url = ""; }
        echo("                <tr>");
        echo("                    <td class=noborder style=\"text-align:right;\">".$url.$prog['txt']."</a></td>");
        echo("                    <td class=noborder>".$prog['id'].".</td>");
        echo("                    <td class=noborder>");
            switch($prog['status'])
            {
                case "C":
                    echo("<span class=\"ui-icon ui-icon-arrowthick-1-w\" style=\"float: left; margin-right: 0em;\">");
                    break;
                case "N":
                    echo("&nbsp;");
                    break;
                case "NA":
                    echo("<span class=\"ui-icon ui-icon-circle-minus\" style=\"float: left; margin-right: 0em;\">");
                    break;
                case "Y":
                    echo("<span class=\"ui-icon ui-icon-circle-check\" style=\"float: left; margin-right: 0em;\">");
                    break;
                default:
                    break;
            }
        echo("                    </td>");
        echo("                </tr>");
    }
    echo("            </table>");
    echo("        </td>");
    echo("    </tr>");
    echo("</table>");
}

//TOTAL PROCESS
function setProgress($p) {
    global $process;
    $proc = $process[$p];
    $progress = array(); //Y=Done, C=Current, N=Not done, NA=Not applicable
    $pc = count($proc);
    for($pid=1;$pid<=count($proc);$pid++)
    {
        $txt = $proc[$pid]['txt'];
        switch($p)
        {
            case 0:
                $progress[] = array("id"=>$pid,"txt"=>$txt,"status"=>getProgress($pid));
                break;
            case 1:
                $progress[] = array("id"=>$pid,"txt"=>$txt,"status"=>getProgress1($pid));
                break;
            case 2:
                $progress[] = array("id"=>$pid,"txt"=>$txt,"status"=>getProgress2($pid));
                break;
            case 3:
                $progress[] = array("id"=>$pid,"txt"=>$txt,"status"=>getProgress2($pid));
                break;
            case 4:
                $progress[] = array("id"=>$pid,"txt"=>$txt,"status"=>getProgress2($pid));
                break;
            case 5:
                $progress[] = array("id"=>$pid,"txt"=>$txt,"status"=>getProgress2($pid));
                break;
        }
    }
    return $progress;
}

function getProgress($pid) {
    global $dash;
    global $step;
    $pid = $pid * 1;

    if(strlen($step)==0)
    {
        if($pid == 1)
        {
            $s = "C";
        }
        else
        {
            if($pid==5)
            {
                if($dash['dashsdbip']=="N")
                {
                    $s = "NA";
                }
                else
                {
                    $s = "N";
                }
            }
            else
            {
                $s = "N";
            }
        }
    }
    else
    {
        if($step == "Y")
        {
            $s = "Y";
        }
        else
        {
            $stp = strFn("explode",$step,"-","");
            $stp[0] = $stp[0]*1;
                    if($stp[0]==$pid)
                    {
                        $s = "C";
                    }
                    else
                    {
                        if($pid!=5)
                        {
                            if($stp[0]<$pid)
                            {
                                $s = "N";
                            }
                            else
                            {
                                $s = "Y";
                            }
                        }
                        else
                        {
                            if($dash['dashsdbip']== "N")
                            {
                                $s = "NA";
                            }
                            else
                            {
                                if($stp[0]<$pid)
                                {
                                    $s = "N";
                                }
                                else
                                {
                                    $s = "Y";
                                }
                            }
                        }
                    }
        }
    }
    return $s;
}




//STEP 1
function setProgress1() {
    global $setup;

        $progress = array(); //Y=Done, C=Current, N=Not done, NA=Not applicable
        $pid = 1;
        $progress[] = array("id"=>1,"txt"=>"Setup Dashboard","status"=>getProgress1($pid));
        $pid++;
        $progress[] = array("id"=>2,"txt"=>"Setup Directorates","status"=>getProgress1($pid));
        $pid++;
        $progress[] = array("id"=>3,"txt"=>"Setup Sub-Directorates","status"=>getProgress1($pid));
        $pid++;
        $progress[] = array("id"=>4,"txt"=>"Setup ".$setup[3][0],"status"=>getProgress1($pid));
        $pid++;
        $progress[] = array("id"=>5,"txt"=>"Setup Towns","status"=>getProgress1($pid));
        $pid++;
        $progress[] = array("id"=>6,"txt"=>"Preview & Move onto Step 2","status"=>getProgress1($pid));
    return $progress;
}

function getProgress1($pid) {
/*      PID
        1 = Setup Dashboard
        2 = Setup Directorates
        3 = Setup Sub-Directorates
        4 = Setup Wards
        5 = Setup Towns
        6 = Preview & Move onto Step 2
        STATUS
        Y = Done
        C = Current
        N = Not Done
        NA = Not Applicable
*/
    global $dash;
    global $page;

    $referer = strFn("explode",$_SERVER['PHP_SELF'],"/","");
    $refmax = count($referer) - 1;
    if($referer[$refmax]=="admin_create1-1.php" && $pid == 1)
    {
        $s = "C";
    }
    else
    {
    $pid = $pid * 1;
    $step = ($page['step'])*1;
    $type = array(
        2=>"dir",
        3=>"sub",
        4=>"wards",
        5=>"towns"
    );
    $status = strFn("explode",$dash['dashstatus'],"-","");

    switch($pid)
    {
        case 1:
            if($step==$pid) {
                $s = "C";
            } else {
                $s = "Y";
            }
            break;
        case 6:
            if($step==$pid) {
                $s = "C";
            } else {
                $s = "N";
            }
            break;
        default:
            $t = $type[$pid];
            $f = $dash['dash'.$t];
            $f = $f*1;
            if($f<0) {
                $s = "NA";
            } else {
                if($page['step']==$pid) {
                    $s = "C";
                } else {
                    if($page['step']>$pid) {
                        $s = "Y";
                    } else {
                        $s = "N";
                    }
                }
            }
            break;
    }
    }
    return $s;
}





//STEP 2
function getProgress2($pid) {
/*      STATUS
        Y = Done
        C = Current
        N = Not Done
        NA = Not Applicable
*/
    global $dash;
    global $page;

    $pid = $pid * 1;
    $step = ($page['step'])*1;

    switch($pid)
    {
        case 1:
            if($step==$pid) {
                $s = "C";
            } else {
                $s = "Y";
            }
            break;
        case 6:
            if($step==$pid) {
                $s = "C";
            } else {
                $s = "N";
            }
            break;
        default:
                if($page['step']==$pid) {
                    $s = "C";
                } else {
                    if($page['step']>$pid) {
                        $s = "Y";
                    } else {
                        $s = "N";
                    }
                }
            break;
    }
    return $s;
}
?>
