<?php
$step = $variables['step'];
$stepact = $variables['stepact'];
if($stepact=="save")
{
    switch($step)
    {
        case "3-1":
            saveStep31();
            break;
        case "3-2":
            saveStep32();
            break;
        case "3-3":
            $nextaction = $variables['nextaction'];
            if($nextaction!="edit") {
                $result = saveStep33();
            } else {
                $recid = $variables['recid'];
            }
            break;
        default:
            $result[0] = "error";
            $result[1] = "Nothing to do";
            break;
    }
}



function saveStep31() {
    global $dbref;
    global $cmpcode;
    global $variables;
    
    $ref = $variables['ref'];
    $next = $variables['next'];
        changeStatus($ref,"P_2-4","P_3-1"); //dashid,statold,statnew
}

function saveStep32() {
    global $cmpcode;
    global $dbref;
    global $variables;
    global $tkid;
    $ref = $variables['ref'];
    $action = $variables['nextaction'];
                        $res[0] = "error";
                        $res[1] = "An error occurred. Please try again.";
                        $res[2] = $ref;
    if(checkIntRef($ref))
    {
        $recsort = 0;
        $ddid = $variables['ddid'];
        foreach($ddid as $d)
        {
            if(checkIntRef($d))
            {
                $recsort++;
                $sql = "INSERT INTO ".$dbref."_dashboard_fields (flddashid,fldtxt,fldfn,fldyn,fldsort,fldtarget) ";
                $sql.= "SELECT $ref, value, 'N', 'Y', $recsort, 'N' FROM ".$dbref."_list_dropdowns_values WHERE id = $d ";
                include("inc_db_con.php");
            }
        }
        changeStatus($ref,"P_3-1","P_3-2"); //dashid,statold,statnew
        $res[0] = "check";
        $res[1] = "Fields saved.";
        $res[2] = $ref;
    }
    else
    {
        $res[0] = "error";
        $res[1] = "An error has occurred.";
        $res[2] = $ref;
    }
    return $res;
}

function saveStep33() {
    global $cmpcode;
    global $dbref;
    global $variables;
    global $tkid;
    $ref = $variables['ref'];
    $action = $variables['nextaction'];
                        $res[0] = "error";
                        $res[1] = "An error occurred. Please try again.";
                        $res[2] = $ref;

    if(checkIntRef($ref))
    {
        $dash = getDashboard($ref);
        if($dash['dashstatus']!="P_3-3") {
            switch($action)
            {
                case "delete":
                    $recid = $variables['recid'];
                    if(checkIntRef($recid))
                    {
                        $sql = "UPDATE ".$dbref."_dashboard_fields SET fldyn = 'N' WHERE fldid = $recid AND flddashid = $ref";
                        include("inc_db_con.php");
                        $mar = mysql_affected_rows($con);
                        if($mar>0)
                        {
                            $res[0] = "check";
                            $res[1] = "Field deleted subcessfully.";
                            $res[2] = $ref;
                        }
                        else
                        {
                            $res[0] = "error";
                            $res[1] = "An error occurred. Please try again.";
                            $res[2] = $ref;
                        }
                    } else {
                            $res[0] = "error";
                            $res[1] = "An error occurred. Please try again.";
                            $res[2] = $ref;
                    }
                    break;
                case "edit":
                    break;
                case "editsave":
                    $recid = $variables['recid'];
                    if(checkIntRef($recid))
                    {
                        $recn = $variables['rn'];
                        $recf = $variables['fn'];
                        $rect = $variables['rt'];
                        $sql = "UPDATE ".$dbref."_dashboard_fields SET fldtxt = '$recn', fldfn = '$recf', fldtarget = '$rect' WHERE fldid = $recid AND flddashid = $ref ";
                        include("inc_db_con.php");
                        $mar = mysql_affected_rows($con);
                        if($mar>0)
                        {
                            $res[0] = "check";
                            $res[1] = "Field $recid updated.";
                            $res[2] = $ref;
                        }
                        else
                        {
                            $res[0] = "error";
                            $res[1] = "An error occurred while attempting to update field $recid.  No change was made.";
                            $res[2] = $ref;
                        }
                    } else {
                            $res[0] = "error";
                            $res[1] = "An error occurred. Please try again.";
                            $res[2] = $ref;
                    }
                    break;
                default:
                    $rn = $variables['rn'];
                    $rf = $variables['fn'];
                    $rt = $variables['rt'];
                    $recsort = $variables['recsort'];
                    $res[0] = "check";
                    $res[1] = "Fields saved.";
                    $res[2] = $ref;
                    for($r=0;$r<count($rn);$r++)
                    {
                        $rectxt = $rn[$r];
                        $recfn = $rf[$r];
                        $rect = $rt[$r];
                        if(strlen($rectxt)>0)
                        {
                            if(strlen($recfn)==0) { $recfn = "N"; }
                            if(strlen($rect)==0) { $rect = "N"; }
                            $rectxt = code($rectxt);
                            $sql = "INSERT INTO ".$dbref."_dashboard_fields (flddashid,fldtxt,fldfn,fldyn,fldsort,fldtarget) VALUES ";
                            $sql.= "($ref,'$rectxt','$recfn','Y',$recsort,'$rect')";
                            include("inc_db_con.php");
                            $recsort++;
                        }
                    }
                    break;
            }
        } else {
            $res = array();
        }
    }
    else
    {
        $res[0] = "error";
        $res[1] = "An error has occurred.";
        $res[2] = $ref;
    }

    if($action=="preview")
    {
        $statold = $variables['oldstep'];
        changeStatus($ref,$statold,"P_3-3"); //dashid,statold,statnew
    }
    
    return $res;
}
?>
