<?php
$step = $variables['step'];
$stepact = $variables['stepact'];
if($stepact=="save")
{
    switch($step)
    {
        case "1-1":
            $vars = array();
            $fields = array("ref","dashname","dashfrequency","dashsdbip","f_wards","f_towns");
            foreach($fields as $flds)
            {
                $vars[$flds] = $variables[$flds];
            }
            $result = saveStep11($vars);
            break;
        case "1-2":
            $ref = $variables['ref'];
            $result = saveStep1234($ref,$step,"D");
            break;
        case "1-3":
            $ref = $variables['ref'];
            $result = saveStep1234($ref,$step,"S");
            break;
        case "1-4":
            $ref = $variables['ref'];
            $result = saveStep1234($ref,$step,"W");
            break;
        case "1-5":
            $ref = $variables['ref'];
            $result = saveStep1234($ref,$step,"T");
            break;
        case "1-6":
            break;
    }
}



function saveStep11($vars) {
    global $dbref;
    global $cmpcode;
    global $tkid;

        $ref = $vars['ref'];
        $dashname = code($vars['dashname']);
        $dashfreq = $vars['dashfrequency'];
        $dashsdbip = $vars['dashsdbip'];
        $frm = strFn("explode",$vars['frames'],";","");
        $frames = array("dir"=>1,"sub"=>2,"wards"=>-1,"towns"=>-1);
        $f = 3;
        if($vars['f_towns']=="Y") { $frames['towns']=$f; $f++; }
        if($vars['f_wards']=="Y") { $frames['wards']=$f; $f++; }


        $dtchk = getdate();
        $dtchk = $dtchk[0] - 3600;
    $datecheck = date("Y-m-d H:i:s",$dtchk);
    if(!checkIntRef($ref)) {
        echoError();
    }
    else
    {
        $dash = getDashboard($ref);
            $sql = "UPDATE ".$dbref."_dashboard ";
            $sql.= "SET dashname = '$dashname', dashsdbip = '$dashsdbip'";
            $sql.= ", dashstatus = 'E_1-1'";
            $sql.= ", dashdir = ".$frames['dir']." , dashsub = ".$frames['sub']." , dashwards = ".$frames['wards']." , dashtowns = ".$frames['towns'];
            $sql.= " WHERE dashid = $ref ";
        include("inc_db_con.php");
            $mar = mysql_affected_rows($con);
            if($mar>0)
            {
                //CHECK TOWNS
                if($dash['dashtowns'] != $frames['towns'] && $frames['towns']<0)
                {
                    $sql = "UPDATE ".$dbref."_dashboard_frame SET dfyn = 'N' WHERE dfdashid = $ref AND dftype = 'T'";
                    include("inc_db_con.php");
                }
                //CHECK WARDS
                if($dash['dashwards'] != $frames['wards'] && $frames['wards']<0)
                {
                    $sql = "UPDATE ".$dbref."_dashboard_frame SET dfyn = 'N' WHERE dfdashid = $ref AND dftype = 'W'";
                    include("inc_db_con.php");
                }
                //INSERT STATUS
                $sql = "INSERT INTO ".$dbref."_dashboard_status (statdashid,statmoddate,statold,statnew) ";
                $sql.= "VALUES ($ref,now(),'".$variables['oldstep']."','E_1-1')";
                include("inc_db_con.php");
                $res[0] = "check";
                $res[1] = "Step 1.1 updated for dashboard $ref.";
                $res[2] = $ref;
            }
            else
            {
                $res[0] = "error";
                $res[1] = "No change was made.";
                $res[2] = $ref;
            }
    }
    return $res;
}


function saveStep1234($ref,$step,$type) {
    global $variables;
    global $cmpcode;
    global $dbref;
    $res = array();
    $res[0] = "error";
    $res[1] = "Nothing done";
    $step = strFn("explode",$step,"-","");
    $step = $step[1]*1;
    $items = array();
    if(checkIntRef($ref))
    {
        $dash = getDashboard($ref);
        $sections = getSections($type,$ref);
        switch($type)
        {
            case "D":
                $items = getDirectorates();
                $idfld = "dirid";
                break;
            case "S":
                $items = getSubDirectorates();
                $idfld = "subid";
                break;
            case "T":
                $items = getTowns();
                $idfld = "id";
                break;
            case "W":
                $items = getWards();
                $idfld = "id";
                break;
            default:
                echoError();
                break;
        }

/*        if(count($dash)>0)
        {
            $ids = $variables['id'];
            $err = "N";
            foreach($ids as $id)
            {
                if($err == "N")
                {
                    $yn = $variables['yn_'.$id];
                    $pa = $variables['pa_'.$id];
                    $pa = strFn("explode",$pa,"_","");
                    $sa = $variables['sa_'.$id];
                    $sa = strFn("explode",$sa,"_","");
                    if($yn == "Y")
                    {
                        $sql = "INSERT INTO ".$dbref."_dashboard_frame (dfdashid,dfforeignid,dfyn,dftype) ";
                        $sql.= "VALUES ($ref,$id,'Y','$type')";
                        include("inc_db_con.php");
                            $fid = mysql_insert_id($con);

                        if(checkIntRef($fid))
                        {
                            if(count($pa)>1) {
                                //create pa record
                                if($pa[0]=="u") {
                                    $patkid = $pa[1];
                                } else {
                                    $sql = "SELECT tkid FROM ".$dbref."_list_admins WHERE id = ".$pa[1];
                                    include("inc_db_con.php");
                                        $parow = mysql_fetch_array($rs);
                                        $patkid = $parow['tkid'];
                                    mysql_close($con);
                                }
                                $sql = "SELECT daid FROM ".$dbref."_dashboard_admins WHERE datype = '$type' AND daframeid = $fid AND datkid = '$patkid' AND dayn = 'Y' AND daprimary = 'Y' ";
                                include("inc_db_con.php");
                                    $pada = mysql_num_rows($rs);
                                mysql_close($con);
                                if(strlen($patkid)>3 && $pada == 0) {
                                    $sql = "INSERT INTO ".$dbref."_dashboard_admins (datype,daframeid,datkid,dayn,daprimary) ";
                                    $sql.= "VALUES ('$type',$fid,'$patkid','Y','Y')";
                                    include("inc_db_con.php");
                                }
                            }
                            if(count($sa)>1) {
                                //create sa record
                                if($sa[0]=="u") {
                                    $satkid = $sa[1];
                                } else {
                                    $sql = "SELECT tkid FROM ".$dbref."_list_admins WHERE id = ".$sa[1];
                                    include("inc_db_con.php");
                                        $sarow = mysql_fetch_array($rs);
                                        $satkid = $sarow['tkid'];
                                    mysql_close($con);
                                }
                                $sql = "SELECT daid FROM ".$dbref."_dashboard_admins WHERE datype = '$type' AND daframeid = $fid AND datkid = '$satkid' AND dayn = 'Y' AND daprimary = 'Y' ";
                                include("inc_db_con.php");
                                    $sada = mysql_num_rows($rs);
                                mysql_close($con);
                                if(strlen($satkid)>3 && $sada == 0) {
                                    $sql = "INSERT INTO ".$dbref."_dashboard_admins (datype,daframeid,datkid,dayn,daprimary) ";
                                    $sql.= "VALUES ('$type',$fid,'$satkid','Y','N')";
                                    include("inc_db_con.php");
                                }
                            }
                        }
                        else
                        {
                            $err = "Y";
                            $res[0] = "error";
                            $res[1] = "An error has occurred.  Please go back and try again.";
                            $res[2] = $ref;
                        } //checkintref(fid)
                    } //yn = y
                } //err = n
            } //foreach id
            //insert new status record
            $sql = "INSERT INTO ".$dbref."_dashboard_status (statdashid,statmoddate,statold,statnew) ";
            $sql.= "VALUES ($ref,now(),'P_1-".($step-1)."','P_1-".$step."')";
            include("inc_db_con.php");
            //update dashstatus
            $sql = "UPDATE ".$dbref."_dashboard SET dashstatus = 'P_1-".$step."' WHERE dashid = $ref";
            include("inc_db_con.php");
                $res[0] = "check";
                $res[1] = "Step 1.".$step." completed.";
                $res[2] = $ref;
        }
        else
        {
            $res[0] = "error";
            $res[1] = "An error has occurred.  Please go back and try again.";
            $res[2] = $ref;
        }*/
    }
    else
    {
            $res[0] = "error";
            $res[1] = "An error has occurred.  Please go back and try again.";
            $res[2] = 0;
    }
    return $res;
}

?>
