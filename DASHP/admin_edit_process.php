<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminEdit.php");
include("inc/AdminEditProgress.php");


?>

<h1><?php echo($modtitle); ?>: Admin - Edit a Dashboard</h1>
<?php displayResult($result); ?>
<?php

$ref = $variables['ref'];
if(strlen($ref)==0) { $ref = $result[2]; }
if(strlen($ref)==0 || $ref == 0)
{
    echo("<h2>Error!</h2><p>An error has occurred.  Please go back and try again.</p>");
}
else
{
    $dash = getDashboard($ref);
    $progress = setProgress();

    displayProgress("","",$progress);
} //endif ref error ?>
<P>&nbsp;</p>
</body>
</html>
