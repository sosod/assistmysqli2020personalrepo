<?php
include("inc_head.php");
include("inc/Admin.php");
include("inc/AdminCreateProgress.php");


$variables = $_REQUEST;
$ref = $variables['ref'];
if(checkIntRef($ref)) { $dash = getDashboard($ref); } else { $dash = array(); }

?>
<script type=text/javascript>
function Validate(me) {
    var valid8 = false;
    var errno = 0;
    if(me.dashname.value.length == 0)
        {        me.dashname.className = 'required';        errno++;    }
    else
        {        me.dashname.className = 'blank';    }
    if(me.dashfrequency.value.length == 0 || me.dashfrequency.value == "X")
        {        me.dashfrequency.className = 'required';        errno++;    }
    else
        {        me.dashfrequency.className = 'blank';    }
    if(errno>0)
        {        alert("Please complete all fields.");        return false;    }
    else
        {        return true;    }
    return false;
}
</script>
<h1><?php echo($modtitle); ?>: Admin - Create a New Dashboard</h1>
<?php displayResult($result); ?>
<h2>Step 1.1: Setup Dashboard</h2>
<form id=frm onsubmit="return Validate(this);" action="admin_create1-2.php" method=post>
<input type=hidden name=step value="1-1"><input type=hidden name=stepact value=save><input type=hidden name=ref value="<?php echo($ref);?>">
<table cellpadding=3 cellspacing=0 width=600>
	<tr>
		<td class=tdheaderl style="border-bottom: 1px solid #ffffff;" width=200>Name of Dashboard:</td>
		<td width=400><input type="text" size="50" value="<?php echo($dash['dashname']); ?>" name=dashname maxlength=150></td>
	</tr>
	<tr>
		<td class=tdheaderl style="border-bottom: 1px solid #ffffff; border-top: 1px solid #ffffff;">Capture Frequency:</td>
		<td><select name=dashfrequency>
            <option <?php if(strlen($dash['dashfrequency'])==0) { echo("selected"); } ?> value="X">--- SELECT ---</option>
            <?php
            $sql = "SELECT * FROM ".$dbref."_list_std_values WHERE listid = 1 AND yn = 'Y' ORDER BY sort";
            include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['id'];
                    $val = $row['value'];
                    echo("<option ");
                    if($dash['dashfrequency']==$id) { echo("selected"); }
                    echo(" value=$id > $val </option>");
                }
            mysql_close($con);
            ?>
        </select></td>
	</tr>
	<tr>
		<td class=tdheaderl style="border-bottom: 1px solid #ffffff; border-top: 1px solid #ffffff;">Link to SDBIP?:</td>
		<td><select name=dashsdbip><?php if(strlen($dash['dashsdbip'])==0) { $yn = "Y"; } else { $yn = $dash['dashsdbip']; } echo(selectYesNo($yn)); ?></select></td>
	</tr>
    <tr height=27>
		<td rowspan=4 valign=top class=tdheaderl style="border-top: 1px solid #ffffff;">Dashboard Framework:</td>
		<td style="padding-left: 27px">1. Directorates</td>
    </tr>
    <tr height=27>
		<td style="padding-left: 27px">2. Sub-Directorates</td>
    </tr>
    <tr height=27>
		<td><input type=checkbox <?php if($dash['dashtowns']>0) { echo("checked"); } ?> name=f_towns value=Y> 3. Towns</td>
    </tr>
    <tr height=27>
		<td><input type=checkbox <?php if($dash['dashwards']>0) { echo("checked"); } ?> name=f_wards value=Y> 4. <?php echo($setup[3][0]); ?> </td>
    </tr>
</table>
<table width=600 cellpadding=3 cellspacing=0 style="margin-top: 20px;">
	<tr>
		<td style="text-align:center;"><input type="submit" value="Next -->"></td>
	</tr>
</table></div>
</form>
<?php
$urlback = "admin.php";
include("inc_goback.php");

        $stepprogress = setProgress(1);
        $totalprogress = setProgress(0);
        displayProgress("Step 1 Process",$stepprogress,$totalprogress);

?>
</body>
</html>
