<?php



/**************
 * CODE TO MEASURE PAGE LOAD TIME 
 * ******************
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

function markTime($s) {
	global $start;
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo '<p>'.$s.' => '.$total_time.' seconds.';
	
}
*/

//error_reporting(-1);

require_once ("../module/autoloader.php");
//marktime("autoloader");

/*********
 * Get file name to process breadcrumb path within module.
 */
$self = $_SERVER['PHP_SELF'];
$self = explode("/",$self);
$self = explode(".",end($self));
$page = $self[0];
if(isset($my_page)) {
	$page.="_".$my_page;
}
$navigation_path = explode("_",$page); //ASSIST_HELPER::arrPrint($navigation_path);
//marktime("file name");
/********
 * Check for redirects
 * Note: Admin redirect is determined within the admin.php file as it requires a user access validation check
 */
$redirect = array(
    'manage'			=> "manage_view",
    'report'			=> "report_fixed",
    'admin'				=> "admin_competency",
    'setup'				=> "setup_defaults"
);
if(isset($redirect[$page])) {
    header("Location:".$redirect[$page].".php");
}
//marktime("redirect");
/************
 * Process Navigation buttons and Breadcrumbs
 */
$menuObject = new JAL2_MENU();
$menu = $menuObject->getPageMenu($page); 
 
//ASSIST_HELPER::arrPrint($menu);
 
if($navigation_path[0]=="new" && $navigation_path[1]=="create" && $menu['buttons'][1][0]['link']!="new_create.php") {
	header("Location:".$menu['buttons'][1][0]['link']);
}
 //marktime("menu");
  
/***********
 * Start general header
 */

$headingObject = new JAL2_HEADINGS();
$displayObject = new JAL2_DISPLAY();
$nameObject = new JAL2_NAMES();
$helper = new JAL2();

$s = array("/library/js/assist.ui.paging.js","/library/jquery-plugins/jscolor/jscolor.js","/".$helper->getModLocation()."/common/jal2helper.js","/".$helper->getModLocation()."/common/jal2.js","/".$helper->getModLocation()."/common/hoverIntent.js");
if(isset($scripts)) {
	$scripts = array_merge(
		$s,
		$scripts
	);
} else {
	$scripts = $s;
}

ASSIST_HELPER::echoPageHeader("1.10.0",$scripts);

//$helper->arrPrint($_REQUEST);

//$contract_object_name = $helper->getContractObjectName();
//$deliverable_object_name = $helper->getDeliverableObjectName();
//$action_object_name = $helper->getActionObjectName();
//$finance_object_name = $helper->getObjectName("FINANCE");
//$template_object_name = $helper->getObjectName("TEMPLATE");

$js = ""; //to catch any js put out by the various display classes

//marktime("general");

/*********
 * Module-wide Javascript
 */
    
?>
<script type="text/javascript" >
$(document).ready(function() {

	//$("table.tbl-container, table.tbl-container td:first").addClass("noborder");
	//$("table.tbl-container tr").find("td:first").addClass("noborder");
	//$("table.tbl-subcontainer").parent("td").addClass("noborder");
	//$("table.sub_table").addClass("noborder").find("td").addClass("noborder");
	$("table.tbl-container").addClass("noborder");
	$("table.tbl-container:not(.not-max)").css("width","100%");
	$("table.tbl-container td").addClass("noborder").find("table:not(.tbl-container) td").removeClass("noborder");
	$("table.th2").find("th").addClass("th2");
	$("table tr.th2").find("th").addClass("th2");
	$("table.tbl_audit_log").css("width","100%").find("td").removeClass("noborder");
	//$("h2").css("margin-top","0px");
	$("table.tbl-subcontainer, table.tbl-subcontainer td").addClass("noborder");

$("select").css("padding","1px");


	//FINANCE specific formatting
	$("table.finance").addClass("form");
	$("table.finance th.DEFAULT").css({"background-color":"#FFFFFF","color":"#000000","border":"1px solid #ababab"});
	
	$("table.finance th.SUB_TOTAL").addClass("th2");
	$("table.finance td.SUB_TOTAL").css({"font-weight":"bold"});
	
	$("table.finance th.TOTAL").parent().addClass("total");
	//$("table.finance td.TOTAL").addClass("th2");
	
	$("table.finance-list tr:gt(0)").find("td:gt(0)").addClass("right");

	$("button.abutton").children(".ui-button-text").css({"padding-top":"0px","padding-bottom":"0px"});



	$("table tr.tr_final td").css("color","#009900");
	//TREE VIEW SETTINGS
	$("table.tbl-tree tr:eq(2)").find("td:first").prop("width","500px");

});

</script>
<style type="text/css">
	.noborder { border: 1px dashed #ffffff; }
	table.tbl_audit_log { width: 100%; }
	table.attach { border: 0px dashed #009900; }
	table.attach td { 
		border: 0px dashed #009900;
		vertical-align: middle;
		padding: 1px; 
	}
	.button_class, .ui-widget.button_class {
		font-size: 75%;
		padding: 1px;
	}
	tr.total th {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
		border: 1px solid #ababab;
	}
	tr.total td {
		background-color: #dedede;
		font-weight: bold;
		color: #000099;
	}
	.abutton {
		font-size: 90%;
		padding: 0px;
		font-weight: bold;
	}
	
	table.tbl-tree {
		border: 0px solid #fe9900;
	}
	table.tbl-tree th {
		border: 0px solid #fe9900;
		color: #000099;
		background-color: #ffffff;
		font-size: 150%;
		line-height: 155%;
		font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;
	}
	table.tbl-tree td {
		border: 0px solid #fe9900;
	}
	table.tbl-tree td.cate-pos, table.tbl-tree tr.cate-pos td {
		font-weight: bold;
		text-decoration: none;
		font-style: none;
		font-size: 115%;
		background-color: #9494d4;
		color: #000000;
	}
	table.tbl-tree td.td-line-break, table.tbl-tree tr.td-line-break td {
		font-weight: normal;
		text-decoration: none;
		font-style: none;
		font-size: 0.5em;
		line-height: 0.5em;
		padding: 1px;
		background-color: #FFFFFF;
	}

	table.tbl-tree td.grand-parent, table.tbl-tree tr.grand-parent td {
		font-weight: bold;
		text-decoration: none;
		font-style: none;
		font-size: 125%;
	}
	table.tbl-tree td.parent, table.tbl-tree tr.parent td {
		font-weight: bold;
		text-decoration: none;
		font-style: none;
	}
	table.tbl-tree td.sub-parent, table.tbl-tree tr.sub-parent td {
		font-weight: normal;
		text-decoration: none;
		font-style: italic;
	}
	table.tbl-tree td.child, table.tbl-tree tr.child td {
		font-weight: normal;
		text-decoration: none;
		font-style: none;
	}
	.white-border {
		border-color: #FFFFFF;
	}
	
	table.tbl-tree tr.grand-parent td.td-button, table.tbl-tree tr.grand-parent td.count {
		font-weight: normal;
		text-decoration: none;
		font-style: none;
	}
	
	button:disabled {
		cursor: not-allowed; 
	}
	
	.disabled-button {
		color: #777777;
		border: 1px solid #777777;
		background-color: #efefef;
	}
 
.ui-button-state-ok, .ui-widget-content .ui-button-state-ok, .ui-widget-header .ui-button-state-ok  {
	border: 0px solid #ffffff; 
	background: url(); 
	color: #009933;
	cursor: pointer; 
}
.ui-button-state-ok a, .ui-widget-content .ui-button-state-ok a,.ui-widget-header .ui-button-state-ok a { 
	color: #363636; 
	padding: 0px;
	cursor: pointer; 
}

.ui-button-state-info, .ui-widget-content .ui-button-state-info, .ui-widget-header .ui-button-state-info  {border: 0px solid #ffffff; background: url(); color: #fe9900; }
.ui-button-state-info a, .ui-widget-content .ui-button-state-info a,.ui-widget-header .ui-button-state-info a { color: #fe9900; }

.ui-button-state-default, .ui-widget-content .ui-button-state-default, .ui-widget-header .ui-button-state-default  {border: 1px solid #888888; background: url() ; color: #888888; }
.ui-ui-button-state-default a, .ui-widget-content .ui-button-state-default a,.ui-widget-header .ui-button-state-default a { color: #363636; }

.ui-state-error-button, .ui-widget-content .ui-state-error-button, .ui-widget-header .ui-state-error-button  {border: 0px solid #cc0001; background: url() ; color: #cd0a0a; }
.ui-state-error-button a, .ui-widget-content .ui-state-error-button a,.ui-widget-header .ui-state-error-button a { color: #363636; }


.ui-state-icon-button, .ui-widget-content .ui-state-icon-button, .ui-widget-header .ui-state-icon-button  {
	border: 0px solid #ffffff; 
	background: url(); 
	color: #363636;
	padding: 0px;
	margin: 0px;  
}
.ui-state-icon-button a, .ui-widget-content .ui-state-icon-button a,.ui-widget-header .ui-state-icon-button a { color: #363636; }

.ui-state-focus-icon-button {
	border: 0px solid #ffffff; 
	background: #ffffff; 
	color: #FE9900;
	padding: 0px; 
	margin: 0px;
}

.ui-button-state-error .ui-icon {
	background-image: url(/library/images/ui-icons_cc0001_256x240.png);
}
.ui-button-state-ok .ui-icon {
	background-image: url(/library/images/ui-icons_009900_256x240.png);
}
.ui-button-state-blue .ui-icon {
	background-image: url(/library/images/ui-icons_000099_256x240.png);
}
.ui-button-state-black .ui-icon {
	background-image: url(/library/images/ui-icons_222222_256x240.png);
}
.ui-button-state-info .ui-icon {
	background-image: url(/library/images/ui-icons_fe9900_256x240.png);
}

.ui-button-state-default .ui-icon {
	background-image: url(/library/images/ui-icons_888888_256x240.png);
}


.ui-button-state-green, .ui-widget-content .ui-button-state-green, .ui-widget-header .ui-button-state-green  {
	border: 1px solid #009933; 
	background: url(); 
	color: #009933;
	cursor: pointer; 
}
.ui-button-state-green .ui-icon {
	background-image: url(/library/images/ui-icons_009900_256x240.png);
}


.ui-button-state-red, .ui-widget-content .ui-button-state-red, .ui-widget-header .ui-button-state-red  {
	border: 1px solid #cc0001; 
	background: url(); 
	color: #cc0001;
	cursor: pointer; 
}
.ui-button-state-red .ui-icon {
	background-image: url(/library/images/ui-icons_cc0001_256x240.png);
}	
</style>
<?php
//marktime("module wide");
/**********
 * Navigation buttons
 */
if(!isset($no_page_heading) || !$no_page_heading) {
	$active_button_label = $menuObject->drawPageTop($menu,(isset($display_navigation_buttons) ? $display_navigation_buttons : true));
}
?>