<?php
require_once("inc_header.php");

echo ASSIST_HELPER::getFloatingDisplay(array("info","Any blank headings will be replaced with the default terminology."));
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());



$menu_names = $menuObject->getRenameableMenuFromDB();

?>
<table class='tbl-container not-max'><tr><td>
<form name=frm_menu_names>
	<input type=hidden name=section value='menu_names' />
<table id=tbl_menu_names>
	<tr>
		<th>Ref</th>
		<th>Breadcrumbs</th>
		<th>Default Terminology</th>
		<th>Your Terminology</th>
		<th></th>
	</tr>
		<?php
		//ASSIST_HELPER::arrPrint($menu_names);
		$breadcrumbs = array('contract'=>$helper->getContractObjectName());
		foreach($menu_names as $contents_menu) {
			$cobject = $contents_menu['row'];
			$child_objects = $contents_menu['child'];
			$ff = $displayObject->createFormField("MEDVC",array('id'=>$cobject['section'],'name'=>$cobject['section'],'orig'=>$cobject['client'],'class'=>"valid8me"),$cobject['client']);
			echo "
			<tr id=tr_".$cobject['id']." class=contents>
				<td>".$cobject['id']."</td>
				<td></td>
				<td>".$cobject['mdefault']."</td>
				<td id=td_".$cobject['id'].">".$ff['display']."</td>
				<td><input type=button value=Save class=btn_save ref='".$cobject['section']."' /></td>
			</tr>";
			if(count($child_objects)>0) {
				foreach($child_objects as $first_menu) {
					$first_child_objects = $first_menu['child'];
					if(isset($first_menu['row'])){
						$fobject = $first_menu['row'];
						$fff = $displayObject->createFormField("MEDVC",array('id'=>$fobject['section'],'name'=>$fobject['section'],'orig'=>$fobject['client'],'class'=>"valid8me"),$fobject['client']);
						echo "
						<tr id=tr_".$fobject['id']." ".(count($first_child_objects)>0?"class=has_child":"").">
							<td>".$fobject['id']."</td>
							<td>".$cobject['client']." >> ".$fobject['client']."</td>
							<td>".$fobject['mdefault']."</td>
							<td id=td_".$fobject['id'].">".$fff['display']."</td>
							<td><input type=button value=Save class=btn_save ref='".$fobject['section']."' /></td>
						</tr>";
					} else {
						$fobject = array('client'=>$helper->getContractObjectName());
					}
					if(count($first_child_objects)>0) {
						foreach($first_child_objects as $object) {
							$sff = $displayObject->createFormField("MEDVC",array('id'=>$object['section'],'name'=>$object['section'],'orig'=>$object['client'],'class'=>"valid8me"),$object['client']);							
							echo "
							<tr id=tr_".$object['id'].">
								<td>".$object['id']."</td>
								<td>".$cobject['client']." >> ".$fobject['client']." >> ".$object['client']."</td>
								<td>".$object['mdefault']."</td>
								<td id=td_".$object['id'].">".$sff['display']."</td>
								<td><input type=button value=Save class=btn_save ref='".$object['section']."' /></td>
							</tr>";
						}
					}
				}
			}
		}
		/*foreach($menu_names as $object ){
			$ff = $displayObject->createFormField("MEDVC",array('id'=>$object['section'],'name'=>$object['section'],'orig'=>$object['client'],'class'=>"valid8me"),$object['client']);
			$l = explode("_",$object['section']);  //unset($l[count($l)-1]);
			$bc = array();
			foreach($l as $b){
				if(isset($breadcrumbs[$b])) {
					$bc[] = $breadcrumbs[$b]; 
				} else {
					$breadcrumbs[$b] = $object['client'];
				}
			}
			echo "
			<tr id=tr_".$object['id'].">
				<td>".$object['id']."</td>
				<td>".(count($bc)>0?implode(" >> ",$bc)." >> ":"").$object['client']."</td>
				<td>".$object['mdefault']."</td>
				<td id=td_".$object['id'].">".$ff['display']."</td>
				<td><input type=button value=Save /></td>
			</tr>";
		} //print_r($breadcrumbs);*/
		?>
	<tr>
		<td colspan=3></td>
		<td colspan=1 class=center><input type=button value="Save All" class=btn_save_all /></td>
		<td colspan=1></td>
	</tr>	
</table>
</form>
	</td></tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"setup",array('section'=>"MENU"),"menu"); ?></td>
	</tr>
</table>

<script type="text/javascript">
$(function() {
	<?php echo $js; ?>
	
	//alert($("#tbl_object_names").css("width"));
	
	$("#tbl_menu_names tr.contents td").css("background-color","#e6e6f5");
	//$("#tbl_menu_names tr.has_child td").css("background-color","#e6e6f5");
	
	$("input:text.valid8me").keyup(function() {
		if($(this).val()!=$(this).attr('orig')) {
			$(this).addClass("orange-border");
		} else {
			$(this).removeClass("orange-border");
		}
		var pi = $(this).parents("table").prop("id");
		var c = $("#"+pi+" .orange-border").length;
		if(c==0) {
			$("#"+pi+" tr:last input:button").val("No Changes to Save");
			$("#"+pi+" tr:last input:button").prop("disabled",true);
		} else { 
			$("#"+pi+" tr:last input:button").prop("disabled",false);
			var v = (c>1 ? "Save All "+c+" Changes" : "Save The "+c+" Change");
			$("#"+pi+" tr:last input:button").val(v);
		}
	});
	
	$(".btn_save_all, .btn_save").click(function() {
		var pi = $(this).parents("table").prop("id");
		if(pi=="tbl_object_names") {
			$form = $("form[name=frm_object_names]");
		} else {
			$form = $("form[name=frm_menu_names]");
		}
		if($(this).hasClass("btn_save")) {
			var dta = "section="+$form.find("input:hidden[name=section]").val();
			var r = $(this).attr("ref");
			var v = $("#"+r).val();
			dta+="&"+r+"="+v;
		} else {
			var dta = AssistForm.serialize($form);
		}
		//alert(dta);
		AssistHelper.processing();
		var result = AssistHelper.doAjax("inc_controller.php?action=Menu.Edit",dta);
		if(result[0]=="ok" && !$(this).hasClass("btn_save")) {
			document.location.href = 'setup_defaults_menu.php?r[]='+result[0]+'&r[]='+result[1];
		} else {
			AssistHelper.finishedProcessing(result[0],result[1]);
		}
	});
	
	$("#tbl_menu_names").find("input:text.valid8me:first").each(function() {
		$(this).trigger("keyup");
	});
	
});
</script>