<?php
/* requires & uses $tdb as object of ASSIST_DB class => created in main/tables.php */
/** creates 2 arrays for the frontpage display: $head & $actions **/

$headers     = array("name", "owner", "outcome", "progress", "status");
$sql         = "SELECT name, client_terminology, ignite_terminology FROM ".$dbref."_naming WHERE 1";
$result      = $tdb -> db_query($sql);
$head        = array();
$head['ref']      = array('text'=>"Ref", 'long'=>false, 'deadline'=>false, 'type'=>"S");
$head['deadline'] = array('text'=>"Deadline", 'long'=>false, 'deadline'=>true, 'type'=>"S");
while($row = mysql_fetch_array($result))
{
    if(in_array($row['name'], $headers)) 
    {
		$head[$row['name']] = array(
		     'text'         => ($row['client_terminology'] ? $row['client_terminology'] : $row['ignite_terminology']),
			'ref'          => ($row['client_terminology'] ? $row['client_terminology'] : $row['ignite_terminology']),
			'long'         => false,
			'deadline'     => false,
			'type'         => "S",
		);	
		
    }
}
unset($result);
$head['link'] = array('text'=>"", 'long'=>false, 'deadline'=>false,'type'=>"S");



	$sql = "SELECT RA.id ,
			RA.name,
			RA.outcome,
			RA.deadline,
			RA.progress,
			RA.status AS statusid,
			RAS.name as status,
			CONCAT(TK.tkname,' ',TK.tksurname) AS owner,
			RA.kpiid
			FROM ".$dbref."_action RA 
			LEFT JOIN ".$dbref."_list_actionstatus RAS ON RA.status = RAS.id
			LEFT JOIN assist_".$cmpcode."_timekeep TK ON  TK.tkid  = RA.owner 
			INNER JOIN ".$dbref."_kpi K ON K.id = RA.kpiid 
			INNER JOIN  ".$dbref."_kpa KA ON KA.id = K.kpaid 
			INNER JOIN  ".$dbref."_user_component UC ON UC.componentid = KA.componentid 
			WHERE STR_TO_DATE(RA.deadline,'%d-%M-%Y') <= ADDDATE(now(), ".$next_due.")  	
			AND UC.status & 8 = 8 AND UC.userid = '".$tkid."' 
			AND RA.owner = '".$tkid."' AND RA.actionstatus & 2 <> 2 AND RA.status <> 3
			ORDER BY ";

			switch($action_profile['field3']) 
			{
				case "dead_desc":	$sql.= " RA.deadline DESC "; break;
				default:
				case "dead_asc":	$sql.= " RA.deadline ASC "; break;
			}
			$sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
            
	$tasks	= $tdb->mysql_fetch_all_fld($sql,"id");
	$actions = array();
	$today = mktime(0,0,0,date("m"),date("d"),date("Y"));	
	foreach($tasks as $id => $task) {
	    $deadline = "";
	    $stattusid = $task['statusid'];		
	    if($task['statusid'] !== "3")
	    {
			$deaddiff = $today - strtotime($task['deadline']);
			$diffdays = floor($deaddiff/(3600*24));
			if($deaddiff < 0) 
			{
				$diffdays*=-1;
				$days = $diffdays > 1 ? "days" : "day";
				$deadline =  "<span>Due in $diffdays $days</span>";
			} elseif($deaddiff==0) {
				$deadline =  "<span class=today><b>Due today</b></span>";
			} else {
				$days = $diffdays > 1 ? "days" : "day";
				$deadline = "<span class=overdue><b>$diffdays $days</b> overdue</span>";
			}			
			$actions[$id] = array(
				'ref'          =>$id,
				'deadline'     =>$deadline,
				'name'         =>$task['name'],
				'owner'        =>$task['owner'],
				'outcome'      =>$task['outcome'],
				'progress'     =>($task['progress'] == "" ? "0%" : $task['progress']."%"),
				'status'       =>($task['status'] == "" ? "New" : $task['status'] ),
				'link'         =>"main.php?controller=action&action=updateaction&id=".$task['id']."&kpiid=".$task['kpiid']."&parent_id=2&folder=manage",
			);
			
		}
	}
?>
