$(function(){

	$("#addfrequency").hide();
	
	Evaluationfrequencies.getEvaluationyears();
	Evaluationfrequencies.getUsers();
	var section = $("#section").val()	
	

	$("#users").change(function(){
		var userid = $(this).val();
		Evaluationfrequencies.getEvaluationPeriods(userid);
		return false;
	});	
	
	
	$("#evaluationyear").change(function(){
		var evaluationyear = $(this).val();
		$("#evaluationyearid").val( evaluationyear )
		if( evaluationyear == ""  || evaluationyear == undefined)
		{
			$("#addfrequency").fadeOut();
		} else {
			if( section != "admin") { 
				$("#addfrequency").fadeIn();
			}
		}
		Evaluationfrequencies.get( evaluationyear );
		return false;
	});
	
	$("#addnew").click(function(){
		
		if($("#addnewdialog").length > 0)
		{
		     $("#addnewdialog").remove();
		}
		
		$("<div />",{id:"addnewdialog"})
		 .append($("<table />")
		   .append($("<tr />")
			  .append($("<th />",{html:"Evaluation Frequency Name:"}))
			  .append($("<td />")
				 .append($("<input />",{type:"text", name:"name", id:"name", value:""}))	  
			  )
		   )
		   .append($("<tr />")
			 .append($("<th />",{html:"Number of Evaluation Periods:"}))
			 .append($("<td />")
			   .append($("<select />",{id:"period", name:"period"})
				 .append($("<option />",{text:"--period--", value:""}))	   
			   )		 
			 )
		   )
		   .append($("<tr />")
			 .append($("<td />",{colspan:"2"})
				 .append($("<p />")
				   .append($("<span />"))
				   .append($("<span />",{id:"message"}))
				 )	 
			  )
		   )
		 )
		 .dialog({
			 		autoOpen	: true, 
			 		title 		: "Add Evaluation Frequencies",
			 		modal		: true, 
			 		width		: "500px",
			 		position	: "top",
			 		buttons		: {
		 							"Save"		: function()
		 							{
			 							if( $("#name").val() == "")
			 							{
			 								$("#message").html("Please enter the name of evaluation frequency")
			 								return false;
			 							} else if( $("#period :selected").val() == ""){
			 								$("#message").html("Please select the period for this evaluation period");
			 								return false;
			 							} else {
			 								Evaluationfrequencies.save();
			 							}
		 							} , 
		 							"Cancel"	: function()
		 							{
		 								$(this).dialog("destroy");
		 							}
		 						}, 
		 			open		: function( event, ui)
		 			{
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		 			}
			 
		 });
		
		for( var i=1; i<=24; i++ )
		{
			$("#period").append($("<option />",{text:i, value:i}))			
		}		
		return false;
	})
	
});

var Evaluationfrequencies		= {
		
	getUsers                 : function()
	{
		$.post("main.php?controller=user&action=getAll", { status : 1}, function( responseData ){
			$.each( responseData, function( index, user) {
				$("#users").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname }))					
			});								
		},"json");
	} ,	
		
	getEvaluationyears		: function()
	{
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
			$.each( responseData, function( index, evalYear) {
				$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))
			});								
		},"json");
	} , 
						
	get			: function( evaluationyear, section )
	{
		$(".freq").remove();
		$.getJSON("main.php?controller=evaluationfrequencies&action=getAll",{
			evaluationyear		: evaluationyear,
			section				: section
		}, function( responseData ){
			if( $.isEmptyObject(responseData) )
			{
				$("#table_evaluationfreq").append($("<tr />").addClass("freq")
				   .append($("<td />",{colspan:"5", html:"There are no evaluation frequencies set for this evaluation year"}))
				)
			} else {
				$.each( responseData, function( index, data){
					Evaluationfrequencies.display( data, section )				
				});					
			}					
		});
	}  , 
	
	save		: function()
	{
		var evalYear = $("#evaluationyearid").val()
		$.post("main.php?controller=evaluationfrequencies&action=saveEvalFreq",{
			name 	: $("#name").val(),
			period	: $("#period :selected").val(),
			evaluationyear : evalYear
		}, function( response ) {
			if( response.error )
			{
				$("#message").html(response.text);			
			} else {
				Evaluationfrequencies.get( evalYear );
				jsDisplayResult("ok", "ok", response.text );
				$("#addnewdialog").dialog("destroy");
				$("#addnewdialog").remove();			
			}
		},"json")	
	} , 
	
	updateFreq		: function( index )
	{
		
		$.post("main.php?controller=evaluationfrequencies&action=updateFreq", {
			id		: index,
			name	: $("#name").val(),
			period	: $("#period :selected").val()
		}, function( response ) {
			if( response.error )
			{
				$("#message").html(response.text)			
			} else {
				Evaluationfrequencies.get( $("#evaluationyearid").val() )	
			       if(response.updated)
			       {
				     jsDisplayResult("info", "info", response.text );
			       } else {
				     jsDisplayResult("ok", "ok", response.text );					
			       } 	
				$("#editdialog").dialog("destroy");
				$("#editdialog").remove();			
			}
		},"json");
	} , 
	
	display		: function( data)
	{
		var section = $("#section").val();
		$("#table_evaluationfreq")
		  .append($("<tr />").addClass("freq")
			 .append($("<td />",{html:data.id}))
			 .append($("<td />",{html:data.name}))
			 .append($("<td />",{html:data.period}))
			 .append($("<td />",{html:((data.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>") }).css({display:(section == "admin" ? "none" : "table-cell")}))
			 .append($("<td />",{html:((data.status & 8) == 8 ? "<b>Closed</b>" : "<b>Opened</b>") }).css({display:(section == "admin" ? "table-cell" : "none")}))
			 .append($("<td />").css({display:(section == "admin" ? "table-cell" : "none")})
			   .append($("<input />",{type:"button", id:"openclose_"+data.id, name:"openclose_"+data.id, value:((data.status & 8) == 8 ? "Open" : "Close") }))		 
			 )			 
			 .append($("<td />").css({display:(section == "admin" ? "none" : "table-cell")})
			   .append($("<input />",{type:"button", id:"edit_"+data.id, name:"edit_"+data.id, value:"Edit"}))		 
			 )
		  )
		
		  $("#openclose_"+data.id).click(function(){
			
			  if( $("#openclosedialog_"+data.id).length > 0)
			  {
				  $("#openclosedialog_"+data.id).remove();				  
			  }
			  
			  $("<div />",{id:"openclosedialog_"+data.id, html:"You are about to "+((data.status & 8) == 8 ? "open" : "close")+" this evaluation period "})
			   .dialog({
				   		autoOpen	: true,
				   		modal		: true,
				   		title 		: "Open Close",
				   		position	: "top",
				   		width		: "500px",
				   		buttons		: {
				   						"Open"		: function()
				   						{
				   							Evaluationfrequencies.open( data.id );				   
				   						} , 
				   						
				   						"Close"		: function()
				   						{
				   							Evaluationfrequencies.close( data.id);				   							
				   						} , 
				   						
				   						"Cancel"	: function()
				   						{
				   							$(this).dialog("destroy")				   							
				   						}
			   			} , 
			   			
			   			open		: function( event , ui)
			   			{
			 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
			 				var openButton = buttons[0];
			 				var closeButton = buttons[1];
			 				$(openButton).css({"color":"#090", display:((data.status & 8) == 8 ? "inline" : "none")});;
			 				$(closeButton).css({"color":"#090", display:((data.status & 8) == 8 ? "none" : "inline")});
			   			}
			   });
			return false;  
		  })
		  
		  $("#edit_"+data.id).live("click", function(){
			    if( $("#editdialog").length > 0)
			    {
			    	$("#editdialog").remove();
			    }
			  
				$("<div />",{id:"editdialog"})
				 .append($("<table />")
				   .append($("<tr />")
					  .append($("<th />",{html:"Evaluation Frequency Name:"}))
					  .append($("<td />")
						 .append($("<input />",{type:"text", name:"name", id:"name", value:data.name}))	  
					  )
				   )
				   .append($("<tr />")
					 .append($("<th />",{html:"Number of Evaluation Periods:"}))
					 .append($("<td />")
					   .append($("<select />",{id:"period", name:"period"})
						 .append($("<option />",{text:"--period--", value:""}))	   
					   )		 
					 )
				   )
				   .append($("<tr />")
					 .append($("<td />",{colspan:"2"})
						 .append($("<p />")
						   .append($("<span />"))
						   .append($("<span />",{id:"message"}))
						 )	 
					  )
				   )
				 )
				 .dialog({
					 		autoOpen	: true, 
					 		title 		: "Edit Evaluation Frequencies",
					 		modal		: true, 
					 		width		: "500px",
					 		position 	: "top",
					 		buttons		: {
				 							"Update"		: function()
				 							{
					 							if( $("#name").val() == "")
					 							{
					 								$("#message").html("Please enter the name of evaluation frequency")
					 								return false;
					 							} else if( $("#period :selected").val() == ""){
					 								$("#message").html("Please select the period for this evaluation period");
					 								return false;
					 							} else {
					 								Evaluationfrequencies.updateFreq( data.id );
					 							}
				 							} , 
				 							"Cancel"	: function()
				 							{
				 								$(this).dialog("destroy");
				 							}, 
				 							
				 							"Delete"	: function()
				 							{
				 								Evaluationfrequencies.deleteFreq( data.id, 2 );						
				 							} , 
				 							
				 							"Deactivate"  : function()
				 							{
				 								Evaluationfrequencies.updateStatus( data.id, 0 );				 								
				 							} , 
				 							
				 							"Activate"		: function()
				 							{
				 								Evaluationfrequencies.updateStatus( data.id, 1 );				 								
				 							} 
				 							
				 						}, 
				 			open		: function( event, ui)
				 			{
				 				var dialog 			 = $(event.target).parents(".ui-dialog.ui-widget");
				 				var buttons 	 	 = dialog.find(".ui-dialog-buttonpane").find("button");	
				 				var saveButton 		 = buttons[0];
				 				var deleteButton 	 = buttons[2]
				 				var deactivateButton = buttons[3];
				 				var activateButton   = buttons[4];
				 				$(saveButton).css({"color":"#090"});
				 				$(deleteButton).css({"color":"red", display:(data.used ? "none" : "inline")});
				 				$(deactivateButton).css({"color":"red", display:(data.used ? ((data.status & 1) == 1 ? "inline" : "none") : "none")});
				 				$(activateButton).css({"color":"#090", display:(data.used ? ((data.status & 1) == 1 ? "none" : "inline") : "none")});
				                    for( var i=1; i<=24; i++ )
				                    {
				                       if(data.period == i)
				                       {
				                         $("#period").append($("<option />",{text:i, value:i, selected:"selected"}))
				                       } else {
				                         $("#period").append($("<option />",{text:i, value:i}))
				                       }
					
				                    }				 				     
				 			}
					 
				 });

			 return false; 
		  });
		  
	} , 
	
	deleteFreq				: function( index)
	{
		
		if( confirm("Are you sure you want to delete this evaluation frequency"))
		{	
			Evaluationfrequencies.updateStatus( index, 2 );
		}		
	} , 
	
	updateStatus			: function( index, status )
	{
		$.post("main.php?controller=evaluationfrequencies&action=updateFreq", {
			id 		: index, 
			status  : status 
		}, function( response ){
			if( response.error )
			{
				$("#message").html(response.text)			
			} else {
				Evaluationfrequencies.get( $("#evaluationyear").val() );
			       if(response.updated)
			       {
				     jsDisplayResult("info", "info", response.text );
			       } else {
				     jsDisplayResult("ok", "ok", response.text );					
			       } 	
				$("#editdialog").dialog("destroy");
				$("#editdialog").remove();			
			}			
		},"json");
	} , 
	
	open			: function( index )
	{
		$.post("main.php?controller=evaluationfrequencies&action=open", {
			id 		: index 
		}, function( response ){
			if( response.error )
			{
				$("#message").html(response.text)			
			} else {
				Evaluationfrequencies.get( $("#evaluationyear").val() );
			       if(response.updated)
			       {
				     jsDisplayResult("info", "info", response.text );
			       } else {
				     jsDisplayResult("ok", "ok", response.text );					
			       } 	
				$("#openclosedialog_"+index).dialog("destroy");
				$("#openclosedialog_"+index).remove();			
			}			
		},"json");
	} , 
	
	close			: function( index )
	{
		$.post("main.php?controller=evaluationfrequencies&action=close", {
			id 		: index 
		}, function( response ){
			if( response.error )
			{
				$("#message").html(response.text)			
			} else {
				Evaluationfrequencies.get( $("#evaluationyear").val() );
			       if(response.updated)
			       {
				     jsDisplayResult("info", "info", response.text );
			       } else {
				     jsDisplayResult("ok", "ok", response.text );					
			       } 	
				$("#openclosedialog_"+index).dialog("destroy");
				$("#openclosedialog_"+index).remove();			
			}			
		},"json");
	} ,
	
	getEvaluationPeriods           : function(user)
	{
		$.post("main.php?controller=evaluationfrequencies&action=getAll", {
			id 		: user 
		}, function( response ){
			if( response.error )
			{
				$("#message").html(response.text)			
			} else {
				//Evaluationfrequencies.get( $("#evaluationyear").val() );		
			}			
		},"json");
	}
		
};
