$(function(){
	
    if( $("#id").val() == undefined)
    {	
	    Kpi.getNationalKpa();
	    Kpi.getIdpObjectives();
	    Kpi.getWeighting();
	}
	
	$("#kpiactions").change(function(){
	     $("body").data("module", "");
	     $("body").data("type", "");
	     $("body").data("subtype", "");
	    if($(this).val() == "N"){
	        $("<div />",{id:"notemapping"})
            .append($("<form />", {id:"mapping_form", id:"mapping_form"})
              .append($("<table />",{id:"modulemapping", width:"100%"})
                .append($("<tr />")
                  .append($("<th />",{html:"Select the module"}))
                  .append($("<td />")
                    .append($("<select />",{id:"module", name:"module"}).addClass("module")
                      .append($("<option />",{value:"", text:"--please select--"}))
                    )
                  )
                )
              )             
            )
	        .dialog({
	            autoOpen    : true,
	            modal       : true,
	            title       : "KPI Mapping",
	            position    : "top",
	            width       : 700,
	            buttons     : {
	                            "Ok"    : function()
	                            {
	                                var data = $("body").data();
	                                var mapStr  = "";
	                                if(data.hasOwnProperty("module"))
	                                {
	                                   if(data["module"] !== "")
	                                   {
	                                     mapStr  += data["module"];
	                                   }
	                                }
	                                if(data.hasOwnProperty("type"))
	                                {
	                                   if(data["type"] !== "")
	                                   {
	                                     mapStr  += " >> "+data["type"];
	                                   }
	                                }
	                                if(data.hasOwnProperty("subtype"))
	                                {
	                                   if(data["subtype"] !== "")
	                                   {
	                                     mapStr  += " >> "+data["subtype"];
	                                   }
	                                }	                                
	                                $("#maplist").html("<i>Linked to</i> "+mapStr);         
	                                $('body').data('mappingdata', $("#mapping_form").serialize());
	                                $("#notemapping").dialog("destroy");
	                                $("#notemapping").remove();	     
	                                         
	                            } ,
	                            "Cancel"    : function()
	                            {
	                                $("#notemapping").dialog("destroy");
	                                $("#notemapping").remove();	                            	                                
	                            }
	            },
	            close       : function()
	            {
                    $("#notemapping").dialog("destroy");
                    $("#notemapping").remove();
	            }, 
	            open        : function(event, ui)
	            {
	 			var dialog = $(event.target).parents(".ui-dialog.ui-widget");
	 			var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
	 			var saveButton = buttons[0];
	 			$(saveButton).css({"color":"#090"});
	 					                
	                $.ajaxSetup({async:false});
	                Kpi.getModules(); 
	            }
	        })	    
	        
	        $("#module").live("change", function(){
	          $(".moduletype").remove(); 
	          $(".subtype").remove();
	          var moduleref = $(this).val()
	          $("body").data("module", $("#module :selected").text());
	          Kpi.getModuleTypes(moduleref);
	          return false;
	        });
	        
	        $("#rsk").live("change", function(){
	          $(".subtype").remove();
	          var refid = $(this).val()	          
	          Kpi.getSubTypes(refid, "risk");
	          return false;
	        });
	        
	        $("#qry").live("change", function(){
	          $(".subtype").remove();
	          var refid = $(this).val()	          	          
	          Kpi.getSubTypes(refid, "qry");
	          return false;
	        });	        
	        	        
	    }
	    return false;
	});
	
	$("#save").click(function(){
		Kpi.save();
		return false;
	});
	
	$("#edit").click(function(){
	   Kpi.edit();
	   return false;
	});
	
	$("#update").click(function(){
	   Kpi.update();
	   return false;
	});
		
});	


var Kpi 		= {
		
	save			: function()
	{
		jsDisplayResult("info", "info", "saving kpi . . . <img src='public/images/loaderA32.gif' />");
		$.post("main.php?controller=kpi&action=saveKpi",{
			data 		: $("#addkpiform").serialize() , 
			kpaid	 	: $("#kpaid").val()	,
			modulemapping : $("body").data("mappingdata")
		}, function( response ) {
			if( response.error )
			{
				jsDisplayResult("error", "error", response.text );
			} else {
				jsDisplayResult("ok", "ok", response.text );
			}			
		},"json");	
	},
	 
	update			: function()
	{
		if($("#response").val() == "")
		{
		     jsDisplayResult("error", "error", "Please enter the response message");
		     $("#response").focus();
		     return false;
		} else if($("#kpistatus :selected").val() == ""){
		     jsDisplayResult("error", "error", "Please select the kpi status ");
		     $("#kpastatus").focus();
		     return false;		
		} else {	
		     jsDisplayResult("info", "info", "Updating kpi . . . <img src='public/images/loaderA32.gif' />");
		     $.post("main.php?controller=kpi&action=update",{
			     response	: $("#response").val(), 
			     status    : $("#kpistatus :selected").val(),
			     id	     : $("#kpiid").val(),
			     remindon  : $("#remindon").val()	
		     }, function( response ) {
			     if( response.error )
			     {
				     jsDisplayResult("error", "error", response.text );
			     } else {
				     jsDisplayResult("ok", "ok", response.text );
			     }			
		     },"json");	
	     }
	}, 
	
	edit			: function()
	{
		jsDisplayResult("info", "info", "Updating kpi . . . <img src='public/images/loaderA32.gif' />");
		$.post("main.php?controller=kpi&action=editKpi",{
			data 		: $("#editkpiform").serialize() , 
			kpiid	 	: $("#id").val()	
		}, function( response ) {
			if( response.error )
			{
				jsDisplayResult("error", "error", response.text );
			} else {
				jsDisplayResult("ok", "ok", response.text );
			}			
		},"json");	
	}, 
	
	
	getNationalKpa			: function()
	{
		$.getJSON("main.php?controller=nationalkpa&action=getAll",{
			evaluationyear 	: $("#evaluationyear").val()			
		}, function( responseData ){
			$.each( responseData, function( index, nationalkpa){
			   $("#nationalkpa").append($("<option />",{value:nationalkpa.id, text:nationalkpa.name}))				
			});						
		});
	} , 
	
	getIdpObjectives		: function()
	{
		$.getJSON("main.php?controller=nationalobjectives&action=getAll",{
			evaluationyear 	: $("#evaluationyear").val()			
		} ,function( responseData ){
			$.each( responseData, function( index, nationalobjectives){
				$("#idpobjectives").append($("<option />",{value:nationalobjectives.id, text:nationalobjectives.name}))				
			});						
		});	
	} , 
	
	getWeighting			: function()
	{
		$.getJSON("main.php?controller=weighting&action=getAll", {
			data	: { status : 1}
		}, function( weightings ){
			$.each( weightings, function(index, weighting ){
				$("#weighting").append($("<option />",{text:weighting.value+"("+weighting.weight+")", value:weighting.id}))
			});				
		});
	}  ,
     getModules              : function()
	{
	    $.getJSON("main.php?controller=kpimapping&action=getModules", function(modules){
	      if(!$.isEmptyObject(modules))
	      {
	        $.each(modules, function(index, module){
                $("#module").append($("<option />",{value:module, text:index}))	        
	        });
	        
	      }
	    }); 
	} , 
	
	getModuleTypes      : function(moduleref)
	{
	    $.getJSON("main.php?controller=kpimapping&action=getModuleTypes",{module:moduleref},function(typeResponses){
	      if(!$.isEmptyObject(typeResponses))
	      {
	        $("#"+moduleref).remove();
	        $("#modulemapping").append($("<tr />").addClass("moduletype")
	          .append($("<th />",{html:typeResponses.title}))
	          .append($("<td />")
	           .append($("<select />",{id:moduleref, name:"module_"+moduleref}).addClass("mainmodule").change(function(e){
	               $("body").data("type", $("#"+moduleref+" :selected").text());
	               e.preventDefault();
	             })
	             .append($("<option />",{value:"", text:"--please select--"}))
	           )
	          )
	        )
             if(!$.isEmptyObject(typeResponses.types)){
                  $.each(typeResponses.types, function(index, val){
                    $("#"+moduleref).append($("<option />",{value:val.id, text:val.name}));
                  });
             }
	      }  	    
	      $("#"+moduleref).append($("<option />",{value:"all", text:"All"}));
	    });
	} ,
	
	getSubTypes         : function(refid, type)
	{
	    $.getJSON("main.php?controller=kpimapping&action=getSubModuleTypes",{refid:refid,type:type},function(catResponses){
	      if(!$.isEmptyObject(catResponses))
	      {
	        $("#sub_"+refid).remove();
	        $("#modulemapping").append($("<tr />").addClass("subtype")
	          .append($("<th />",{html:catResponses.title}))
	          .append($("<td />")
	           .append($("<select />",{id:"sub_"+refid, name:"subtype_"+refid}).addClass("submoduleType").change(function(e){
                    $("body").data("subtype", $("#sub_"+refid+" :selected").text());
                    e.preventDefault();
	              })
	             .append($("<option />",{value:"", text:"--please select--"}))
	           )
	          )
	        )
             if(!$.isEmptyObject(catResponses.categories)){
                  $.each(catResponses.categories, function(index, val){
                    $("#sub_"+refid).append($("<option />",{value:val.id, text:val.name}));
                  });
             }             
	      }
	        if(refid == "all")
	        {
	          $("#sub_"+refid).append($("<option />",{value:"all", text:"All", disabled:"disabled", selected:"selected"}));  	    
	        } else {
	          $("#sub_"+refid).append($("<option />",{value:"all", text:"All"}));
	        }	      
	    });  
	}	
		
};
