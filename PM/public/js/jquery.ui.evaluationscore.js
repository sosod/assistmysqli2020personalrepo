$.widget("ui.evaluationscore", {

    options         : {
     evaluationyear      : 0,
     evaluationperiod    : 0,
     manager             : 0,
     department          : 0,    
     section             : 0,
     orderby             : 0,
     order               : 0,
     autoLoad            : false
    } ,
    
    _init           : function()
    {
       if(this.options.autoLoad)
       {
          this._get();
       }
    } ,
    
    _create         : function()
    {
       var self = this;
       $(self.element).append($("<table />",{width:"100%"}).addClass("noborder")
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />")
               .append($("<tr />")
                 .append($("<th />",{html:"Evaluation year:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationyear", id:"evaluationyear"}))
                 )
                 .append($("<th />",{html:"Evaluation period:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationperiod", id:"evaluationperiod"}))             
                 )            
               )
               .append($("<tr />").addClass("filters").css({display:"none"})
                 .append($("<th />",{html:"Manager:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"manager", id:"manager"}))
                 )
               )
               .append($("<tr />").addClass("filters").css({display:"none"})
                 .append($("<th />",{html:"Department:"}))
                 .append($("<td />",{colspan:3})
                   .append($("<select />",{name:"department", id:"department"}))
                 )
               )  
               .append($("<tr />").addClass("filters").css({display:"none"})
                 .append($("<th />",{html:"Section:"}))
                 .append($("<td />",{colspan:3})
                   .append($("<select />",{name:"section", id:"section"}))             
                 )            
               )  
               .append($("<tr />").addClass("filters").css({display:"none"})
                 .append($("<th />",{html:"Order By:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"orderby", id:"orderby"}))
                 )
                 .append($("<td />",{colspan:2})
                   .append($("<select />",{name:"order", id:"order"}))             
                 )            
               )                                
             )             
           )           
         )
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />",{width:"100%", id:"evaluation_score_table"}))
           )
         )
       )
       
       self._loadEvaluationYears();
       $("#evaluationyear").on('change', function(){
          self.options.evaluationyear = $(this).val();
          self._loadEvaluationPeriods($(this).val());       
       });
       
       $("#evaluationperiod").on('change', function(e){
         $(".filters").show();
         self.options.evaluationperiod = $(this).val();
         self._loadUsers();
         self._get();
         e.preventDefault();
       });
       
       $("#manager").on('change', function(e){
         self.options.manager = $(this).val();
         self._get();
         e.preventDefault();
       })

    } ,

	_loadEvaluationYears		: function()
	{
	     $("#evaluationyear").append($("<option />",{value:"", text:"--select evaluation year--"}))
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
		   $.each( responseData, function( index, evalYear) {
			 $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))
		   });								
		},"json");
	} ,
	
	_loadEvaluationPeriods      : function(evaluationyear)
	{
	    $("#evaluationperiod").empty();
	    $("#evaluationperiod").append($("<option />",{value:"", text:"--select evaluation period--"}))
	    $.post("main.php?controller=evaluationperiod&action=get_all", {evaluationyear : evaluationyear},
		 function( responseData ){
		  var period_int = 1;
		  $.each(responseData['periods'][1], function(index, period) {
		    text_str = period.start_date+" -- "+period.end_date+" ("+period.name+" "+period_int+" )";
		    period_int++;
		    $("#evaluationperiod").append($("<option />",{value:period.id, text:text_str}))
		  });								
	    },"json");
	} ,
	
	_loadUsers            : function(userid)
	{
	    $(".manager").empty();
	     $("#manager").append($("<option />",{value:"all", text:"All"}))
		$.post("main.php?controller=user&action=getAll",function(users){	
	        if(!$.isEmptyObject(users))
	        {
	           $.each(users, function(index, user){
	              $("#manager").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}))
	          });
	        }
		},"json"); 
		
	} ,
	
    _get            : function()
    {
       var self = this;
	  $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading ... <img src='public/images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"300px", left:"300px", border:"0px solid #FFFFF", padding:"5px"})
	  )         
       $.getJSON("main.php?controller=evaluation&action=get_evaluation_score_report", {
            evaluationyear     : self.options.evaluationyear,
            evaluationperiod   : self.options.evaluationperiod,
            manager            : self.options.manager,
            department         : self.options.department,
            orderby            : self.options.orderby,
            order              : self.options.order
       }, function(evaluationStatusReport) {
          $("#evaluation_score_table").html("")
          self._displayHeaders();
          self._display(evaluationStatusReport);
          $("#loadingDiv").remove();
       }); 
    },
    
    
    _display          : function(report)
    {
      var self = this;
      var html = [];
      var len  = report.length;
      for(var i = 0; i < len; i++)
      {
          html.push("<tr>");
           html.push("<td>"+report[i]['user']+"</th>");
           html.push("<td>"+report[i]['manager']+"</th>");               
           html.push("<td>"+(report[i]['selfeval'] === undefined ? "Not Yet Done" : report[i]['selfeval'])+"</td>");
           html.push("<td>"+(report[i]['managereval'] === undefined ? "Not Yet Done" : report[i]['managereval'])+"</td>");
           html.push("<td>"+(report[i]['jointeval'] === undefined ? "Not Yet Done" : report[i]['jointeval'])+"</td>");
           html.push("<td>"+(report[i]['revieweval'] === undefined ? "Not Yet Done" : report[i]['revieweval'])+"</td>");
          html.push("</tr>");          
      }
      $("#evaluation_score_table").append("<tbody>"+html.join('')+"</tbody>");
    } ,
    
    _displayHeaders    : function()
    {
       var html = [];
       html.push("<thead><tr>");
        html.push("<th>User</th>");
        html.push("<th>Manager</th>");
        html.push("<th>Self Evaluation</th>");
        html.push("<th>Manager Evaluation</th>");
        html.push("<th>Joint Evaluation</th>");
        html.push("<th>Evaluation Review</th>");
       html.push("</tr></thead>");
       $("#evaluation_score_table").append(html.join(''));
    }



});
