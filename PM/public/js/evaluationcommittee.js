$(function(){
	
	Evaluationcommittee.getEvaluationyears();
	
	$("#evaluationyear").change(function(){
	    
	    $("#addevaluationcommittee").show();
	
	    $("#evaluationyearid").val( $(this).val() );
    	     Evaluationcommittee.get( $(this).val() );
	    return false;
	});
    
    $("#addnew").click(function(){
        if($("#addnewevaluationcommittee").length > 0)
        {
            $("#addnewevaluationcommittee").remove();
        }
        
        $("<div />",{id:"addnewevaluationcommittee"})
        .append($("<table />")
          .append($("<tr />")
            .append($("<th />",{html:"Evaluation Committee Title:", valign:"top"}))
            .append($("<td />")
              .append($("<input />",{type:"text", name:"title", id:"title", value:"", size:50}))
            )
          )        
          .append($("<tr />")
            .append($("<th />",{html:"User:", valign:"top"}))
            .append($("<td />")
              .append($("<br />")) 
              .append($("<select />",{id:"users", name:"users", multiple:"multiple"}))
            )
          )
          .append($("<tr />")
            .append($("<th />",{html:"Additional User:", valign:"top"}))
            .append($("<td />")
               .append($("<table />")
                 .append($("<tr />")
                   .append($("<td />",{html:"<b>First Name</b>"}).addClass("th2"))
                   .append($("<td />",{html:"<b>Last Name</b>"}).addClass("th2"))
                 )
                 .append($("<tr />")
                   .append($("<td />")
                      .append($("<input />",{id:"firstname_1", name:"firstname_1", value:""}).addClass("additionaluser"))
                   )
                   .append($("<td />")
                      .append($("<input />",{id:"lastname_1", name:"lastname_1", value:""}).addClass("additionaluser"))
                   )
                 )  
                 .append($("<tr />",{id:"tr_additionusers"})
                    .append($("<td />",{colspan:"2"})
                      .append($("<input />",{type:"button", name:"add_user", id:"add_user", value:"Add Another"})
                        .click(function(){
                            var kname = $(".additionaluser").last().attr("name");
                            var _id = parseInt(kname.substr(9)) + 1; 
                            $(this).before($("<tr />",{id:"trinput_"+_id})
                               .append($("<td />")
                                  .append($("<input />",{id:"firstname_"+_id, name:"firstname_"+_id, value:""}).addClass("additionaluser"))
                               )
                               .append($("<td />")
                                  .append($("<input />",{id:"lastname_"+_id, name:"lastname_"+_id, value:""}).addClass("additionaluser"))
                               )    
                               .append($("<td />")
                                  .append($("<a />",{id:"remove_"+_id, text:"Remove", href:"#"}))
                               )                                                            
                            ) 
                            
                            $("#remove_"+_id).live("click", function(){
                                $("#trinput_"+_id).remove();
                                return false;
                            });
                            return false;
                        })
                      )
                    )
                 )               
               )
            )
          )          
         .append($("<tr />")
            .append($("<td />",{colspan:"2", id:"message"}))
         )                    
        ).dialog({
                 autoOpen   : true,
                 modal      : true,
                 position   : "top",
                 title      : "Add Evaluation Committee",
                 width      : "600px",
                 buttons    : {
                                "Save"      : function()
                                {
                                    var otherusers = [];
                                    var nameRef = [];                                            
                                    $(".additionaluser").each(function(aIndex, aVal){
                                        var textname = $(aVal).attr("name");
                                        var dashIndex = textname.indexOf("_");
                                        var refId = textname.substr((dashIndex)+1)
                                        nameRef[refId] = refId;
                                    });
                                    
                                    $.each(nameRef, function(i, k){
                                        if(k != undefined)
                                        {
                                          otherusers.push( {firstname:$("#firstname_"+i).val(), lastname:$("#lastname_"+i).val() } ); 
                                        }
                                    });
                                            
                                    if($("#title").val() == ""){
                                        $("#message").html("please enter the title of the committee . . ");
                                        return false;
                                    } else if($("#users").val() == null || $("#users").val() == undefined){
                                        if(otherusers == "")
                                        {
                                            $("#message").html("please enter or select the committee users . . ");
                                            return false;
                                        }
                                    } else {
                                        $("#message").addClass("ui-state-info").html("saving . . . <img src='public/images/loaderA32.gif' />")
                                        $.post("main.php?controller=evaluationcommittee&action=saveCommittee",{
                                            data : {
                                                    title : $("#title").val(),
                                                    user  : $("#users").val(),
                                                    usertext : otherusers,
                                                    evaluationyear  : $("#evaluationyear").val()
                                            }
                                        }, function(response){
				                            if( response.error )
				                            {  
					                            $("#message").html(  response.text );					
				                            } else {
					                            Evaluationcommittee.get( $("#evaluationyear").val() );
					                            jsDisplayResult("ok", "ok", response.text );
					                            $("#addnewevaluationcommittee").dialog("destroy");
					                            $("#addnewevaluationcommittee").remove();
				                            }		                                                                                
                                        },"json")
                                    }   
                                }, 
                                "Cancel"    : function()
                                {
                                    $("#addnewevaluationcommittee").dialog("destroy");
                                    $("#addnewevaluationcommittee").remove();                                
                                }
                 } ,
                 open       : function()
                 {
                    Evaluationcommittee.getUsers();  
                 }, 
                 close     : function()
                 {
                    $("#addnewevaluationcommittee").dialog("destroy");
                    $("#addnewevaluationcommittee").remove();
                 }       
        })
        
        return false;
    });
    
});


var Evaluationcommittee	= {

	    getEvaluationyears		: function()
	    {
		    $.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
			    $.each( responseData, function( index, evalYear) {
				    $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))
			    });								
		    },"json");
	    } , 
	
		get 			: function(evaluationyear)
		{
		    $(".evaluationcommittee").remove();
			$.getJSON("main.php?controller=evaluationcommittee&action=get_all",{
			    evaluationyear  : evaluationyear
			} , function( responseData ) {
			    if($.isEmptyObject(responseData))
			    {
		            $("#table_evaluationcommittee").append($("<tr />").addClass("evaluationcommittee")
		               .append($("<td />",{colspan:"5", html:"There are no evaluation committees for this evaluation year"}))
		            )
			    } else {
				    $.each( responseData, function( index, committee){
					    Evaluationcommittee.display( committee );					
				    });		
				}		
			});
		} , 
		
		save			: function()
		{
			$.post("main.php?controller=evaluationcommittee&action=saveCommittee", {
				data : {}
			}, function( response ) {
	            if(response.error)
	            {
		          jsDisplayResult("error", "error", response.text );
	            } else {
		          jsDisplayResult("ok", "ok", response.text );					
	            } 					
			},"json")
			
		} , 
		
		update			: function( id )
		{
			$.post("main.php?controller=evaluationcommittee&action=updateCommittee", {
				data : {}, 
				id	 : id
			}, function( response ) {
				if( response.error ){
					jsDisplayResult("error", "error", response.text );
				} else {
		            if(response.updated)
		            {
			          jsDisplayResult("info", "info", response.text );
		            } else {
			          jsDisplayResult("ok", "ok", response.text );					
		            } 		
				}				
			},"json")
		} , 
		
		updateStatus		: function( id, status)
		{
			$.post("main.php?controller=evaluationcommittee&action=updateCommittee", {
				status : status, 
				id	 : id
			}, function( response ) {
				if( response.error ){
					$("#message").html( response.text )
				} else {
		            if(response.updated)
		            {
			          jsDisplayResult("info", "info", response.text );
		            } else {
			          jsDisplayResult("ok", "ok", response.text );					
		            } 					     
				    Evaluationcommittee.get( $("#evaluationyearid").val() );
                    $("#editevaluationcommittee_"+id).dialog("destroy");
                    $("#editevaluationcommittee_"+id).remove();					
				}				
			},"json")
		} , 
		
		deleteCommittee		: function( id )
		{
			if( confirm("Are you sure you want to delete this evaluation committtee"))
			{
				Evaluationcommittee.updateStatus(id, 2);				
			}
		} , 
		
		getUsers				: function(users)
		{
			$.getJSON("main.php?controller=user&action=getAll", function(response){
                if(!$.isEmptyObject(response))
                {
                    $.each(response, function(index, val){
                        $("#users").append($("<option />",{text:val.tkname+" "+val.tksurname, value:val.tkid}))
                    });       
                    if(!$.isEmptyObject(users))
                    {             
                        $.each(users,function(i, value){
                            $("#users option[value='"+value+"']").attr("selected", "selected");
                        });
                    }
                }
			});
			
		} , 
		
		display             : function( evaluationcommittee )
		{
		    $("#table_evaluationcommittee").append($("<tr />").addClass("evaluationcommittee")
		       .append($("<td />",{html:evaluationcommittee.ref}))
		       .append($("<td />",{html:evaluationcommittee.title}))
		       .append($("<td />",{html:evaluationcommittee.users+" "+evaluationcommittee.other_users}))
		       .append($("<td />",{html:((evaluationcommittee.status & 1) == 1 ? "<b>Active</b>" : "<b>InActive</b>")}))
		       .append($("<td />")
		          .append($("<input />",{type:"button", id:"edit_"+evaluationcommittee.ref, name:"edit_"+evaluationcommittee.ref, value:"Edit"}))
		       )
		    )
		    
		    $("#edit_"+evaluationcommittee.ref).live("click", function(){
                if($("#editevaluationcommittee_"+evaluationcommittee.ref).length > 0)
                {
                    $("#editevaluationcommittee_"+evaluationcommittee.ref).remove();
                }
                
                $("<div />",{id:"editevaluationcommittee_"+evaluationcommittee.ref})
                .append($("<table />")
                  .append($("<tr />")
                    .append($("<th />",{html:"Evaluation Committee Title:", valign:"top"}))
                    .append($("<td />")
                      .append($("<input />",{type:"text", name:"title", id:"title", value:evaluationcommittee.title, size:50}))
                    )
                  )
                  .append($("<tr />")
                    .append($("<th />",{html:"User:", valign:"top"}))
                    .append($("<td />")
                      .append($("<br />")) 
                      .append($("<select />",{id:"users", name:"users", multiple:"multiple"}))
                    )
                  )
                  .append($("<tr />")
                    .append($("<th />",{html:"Additional User:", valign:"top"}))
                    .append($("<td />")
                       .append($("<table />")
                         .append($("<tr />")
                           .append($("<td />",{html:"<b>First Name</b>"}).addClass("th2"))
                           .append($("<td />",{html:"<b>Last Name</b>"}).addClass("th2"))
                         )  
                         .append($("<tr />",{id:"tr_additionusers"})
                            .append($("<td />",{colspan:"2"})
                              .append($("<input />",{type:"button", name:"add_user", id:"add_user", value:"Add Another"})
                                .click(function(){
                                    var kname = $(".additionaluser").last().attr("name");
                                    var _id = parseInt(kname.substr(9)) + 1; 
                                    $(this).before($("<tr />")
                                       .append($("<td />")
                                          .append($("<input />",{id:"firstname_"+_id, name:"firstname_"+_id, value:""}).addClass("additionaluser"))
                                       )
                                       .append($("<td />")
                                          .append($("<input />",{id:"lastname_"+_id, name:"lastname_"+_id, value:""}).addClass("additionaluser"))
                                       )                                                            
                                    ) 
                                    return false;
                                })
                              )
                            )
                         )               
                       )             
                    )
                  )          
                  
                 .append($("<tr />")
                    .append($("<td />",{colspan:"2", id:"message"}))
                 )                    
                ).dialog({
                         autoOpen   : true,
                         modal      : true,
                         position   : "top",
                         title      : "Edit Evaluation Committee",
                         width      : "600px",
                         buttons    : {
                                        "Save Changes"      : function()
                                        {
                                            var otherusers = [];
                                            var nameRef = [];                                            
                                            $(".additionaluser").each(function(aIndex, aVal){
                                                var textname = $(aVal).attr("name");
                                                var dashIndex = textname.indexOf("_");
                                                var refId = textname.substr((dashIndex)+1)
                                                nameRef[refId] = refId;
                                            });
                                            
                                            $.each(nameRef, function(i, k){
                                                if(k != undefined)
                                                {
                                                  otherusers.push( {firstname:$("#firstname_"+i).val(), lastname:$("#lastname_"+i).val() } ); 
                                                }
                                            });
                                            
                                            if($("#title").val() == ""){
                                                $("#message").html("please enter the title of the committee . . ");
                                                return false;
                                            } else if($("#users").val() == null || $("#users").val() == undefined){
                                                if(otherusers == "")
                                                {
                                                    $("#message").html("please enter or select the committee users . . ");
                                                    return false;
                                                }
                                            } else {
                                                $("#message").addClass("ui-state-info").html("updating . . . <img src='public/images/loaderA32.gif' />");
                                                $.post("main.php?controller=evaluationcommittee&action=edit_committee",{
                                                    data : {
                                                            title : $("#title").val(),
                                                            user  : $("#users").val(),
                                                            usertext : otherusers,
                                                            evaluationyear  : $("#evaluationyear").val(), 
                                                            id              : evaluationcommittee.ref
                                                    }
                                                }, function(response){
				                                    if( response.error )
				                                    {  
					                                    $("#message").html(  response.text );					
				                                    } else {
					                                    Evaluationcommittee.get( $("#evaluationyear").val() );
		                                                    if(response.updated)
		                                                    {
			                                                  jsDisplayResult("info", "info", response.text );
		                                                    } else {
			                                                  jsDisplayResult("ok", "ok", response.text );					
		                                                    } 		
					                                    $("#editevaluationcommittee_"+evaluationcommittee.ref).dialog("destroy");
					                                    $("#editevaluationcommittee_"+evaluationcommittee.ref).remove();
				                                    }		                                                                                
                                                },"json")
                                            
                                            }   
                                        },                                              
                                        "Cancel"    : function()
                                        {
		                                    $("#editevaluationcommittee_"+evaluationcommittee.ref).dialog("destroy");
		                                    $("#editevaluationcommittee_"+evaluationcommittee.ref).remove();
                                        }, 
                                        "Activate"    : function()
                                        {
                                            Evaluationcommittee.updateStatus(evaluationcommittee.ref, 1);
                                        },
                                        "Deactivate"    : function()
                                        {
	                                        Evaluationcommittee.updateStatus(evaluationcommittee.ref, 0);
                                        }, 
                                        "Delete"    : function()
                                        {
		                                    Evaluationcommittee.updateStatus(evaluationcommittee.ref, 2);
                                        } 
                         } ,
                         open       : function(event, ui)
                         {
			 				var displayDelete = false;
			 				var displayDeactivate = false;
			 				var displayActivate = false; 
			 				//if the status is for the default , then dont display delete
			 				if( (evaluationcommittee.status & 1) == 1)
			 				{
			 					displayDeactivate = true;
			 					displayDelete     = true;
			 				} else {
			 				    displayDelete     = true;
			 					displayActivate = true;
			 				}
			 				/*
			 				if( status.used == false)
			 				{
			 					displayDelete 	  = true;
			 					displayDeactivate = false;
			 				}
			 				
			 				if( (evaluationcommittee.status & 4) == 4 )
			 				{
			 					displayDeactivate = false;
			 					displayDelete = false;
			 					displayActivate = false;
			 				}
			 				*/
			 				
			 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
			 				var saveButton = buttons[0];
			 				var deleteButton = buttons[4]
			 				var deactivateButton = buttons[3];
			 				var activateButton   = buttons[2];
			 				$(saveButton).css({"color":"#090"});
			 				$(deleteButton).css({"color":"red", display:(displayDelete ? "inline" : "none")});
			 				$(deactivateButton).css({"color":"red", display:(displayDeactivate ? "inline" : "none")});
			 				$(activateButton).css({"color":"#090", display:(displayActivate ? "inline" : "none")});   
    
    
                            Evaluationcommittee.getUsers(evaluationcommittee.user_ids);  
                            $.each(evaluationcommittee.user_texts,function(i, val){
                                var _id = parseInt(i) + 1;
                                $("#tr_additionusers").before($("<tr />",{id:"trinput_"+_id})
                                   .append($("<td />")
                                     .append($("<input />",{id:"firstname_"+_id, name:"firstname_"+_id, value:val.firstname}).addClass("additionaluser"))
                                   )
                                   .append($("<td />")
                                     .append($("<input />",{id:"lastname_"+_id, name:"lastname_"+_id, value:val.lastname}).addClass("additionaluser"))
                                   )
                                   .append($("<td />")
                                     .append($("<a />",{href:"#", id:"remove_"+_id, text:"Remove"}))
                                   )                                   
                                )                            
                                
                                $("#remove_"+_id).live("click", function(){
                                    $("#trinput_"+_id).remove();
                                    return false;
                                });
                            })
                            
                         }, 
                         close     : function()
                         {
                            $("#editevaluationcommittee_"+evaluationcommittee.ref).dialog("destroy");
                            $("#editevaluationcommittee_"+evaluationcommittee.ref).remove();
                         }       
                })
                
                return false;
		    });
		    
		}
		 
		
		
};	
