$(function(){

   CreationStatus.getEvaluationyears();
   CreationStatus.getUser();
   
   var user           = $("#user");
   var evaluationyear = $("#evaluationyear")
   
   user.change(function(){
     if($(this).val() !== "")
     {
        if(evaluationyear.val() !== "")
        {
          CreationStatus.createReport($(this).val(), evaluationyear.val());
        }
     }
   });
   
   evaluationyear.change(function(){
     if($(this).val() !== "")
     {
        if(user.val() !== "")
        {
          CreationStatus.createReport(user.val(), $(this).val());
        }
     }
   });   
   

});

var CreationStatus  = {

	getEvaluationyears		: function()
	{
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
			$.each( responseData, function( index, evalYear) {
				$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))					
			});								
		},"json");
	} ,
	
	getUser            : function(userid)
	{
	    $(".users").remove();
		$.post("main.php?controller=user&action=getAll",function(users){	
	        if(!$.isEmptyObject(users))
	        {
	            $.each(users, function(index, user){
                    if(user.tkid == userid)
                    {
	                 $("#user").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname, selected:"selected"})) 
	               } else {
	                 $("#user").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}))    
	               }
	            });
	        }
		},"json"); 
		
	} ,
	
	createReport             : function(user, evaluationyear)
	{
	   var self = this;
	   $('.creation_statuses').remove();
	   $.post("main.php?controller=usercomponent&action=getCreationStatus", 
	   {user:user, evaluationyear:evaluationyear},function( creationStatuses ) {
	     if(!$.isEmptyObject(creationStatuses))
	     {
	       $.each(creationStatuses, function(index, userCreationStatus){
	          if(userCreationStatus.hasOwnProperty('status'))
	          {
                  self._displayEmptyComponent(userCreationStatus)	          
	          } else {
                  self._displayComponents(userCreationStatus);
	          }
	       });
	     } else {
	        $("#table_header").after($("<tr />").addClass('creation_statuses')
	          .append($("<td />",{colspan:"5", html:"There no sub-ordinates setup for the selected criteria"}))
	        )
	     }
	   },"json"); 
	     
	} ,
	
	_displayComponents   : function(userComponentStatus)
	{
	    var self = this;     
	    $("#table_header").after($("<tr />").addClass('creation_statuses')
	      .append($("<td />",{html:userComponentStatus.user}))
	      .append($("<td />",{html:userComponentStatus.manager}))
	      .append($("<td />",{html:userComponentStatus.created}))
	      .append($("<td />",{html:userComponentStatus.confirmed}))
	      .append($("<td />",{html:userComponentStatus.activated}))
	    )
	},
	
	_displayEmptyComponent   : function(userComponentStatus)
	{
	    var self = this;     
	    $("#table_header").after($("<tr />").addClass('creation_statuses')
	      .append($("<td />",{html:userComponentStatus.user}))
	      .append($("<td />",{html:userComponentStatus.manager}))
	      .append($("<td />",{colspan:"3", html:userComponentStatus.status}).css({"color":"#000000"}))
	    )
	}
};
