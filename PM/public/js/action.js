$(function(){

    if($("#id").val() == undefined){	
	    Action.getCompetency();
	    Action.getProficiency();
	    Action.getWeighting();
	}

	$("#edit").click(function(e){
		Action.edit();
		e.preventDefault();
	});	
	
	$("#update").click(function(e){
		Action.update();
		e.preventDefault();
	});	
		
	
	$("#save").click(function(e){
		Action.save();
		e.preventDefault();
	});
	
	$("#actionstatus").change(function(e) {
	    var thisValue = $(this).val();
	    if(thisValue == 3)
	    {
	      $("#progress").val(100);
	      $("#requestapproval").removeAttr("disabled");
	    } else {
	      if( $("#progress").val() == 100 && $("#currentprogress").val() != 100)
	      {
	          $("#progress").val($("#currentprogress").val());
	      }
	      $("#requestapproval").attr("disabled", "disabled");
	    }	    
	   e.preventDefault();
	});
	
	$("#progress").blur(function(e) {
	    var thisValue = $(this).val();
	    if(thisValue == 100)
	    {
	      $("#actionstatus").val(3);
	      $("#requestapproval").removeAttr("disabled");
	    } else {
	       if( $("#actionstatus").val() == 3 && $("#action_status").val() != 3)
	       {
	          $("#actionstatus").val( $("#action_status").val() );
	       }
	      $("#requestapproval").attr("disabled", "disabled");
	    }	    
	   e.preventDefault();
	});	
	
	$(".remove_attach").click(function(e){
          var id  = this.id
          var ext = $(this).attr('title');
          $.post('main.php?controller=action&action=delete_attachment', 
            { attachment : id,
              ext        : ext,
              action_id  : $("#actionid").val()
             }, function(response){     
            if(response.error)
            {
              $("#result_message").html(response.text)
            } else {
               $("#result_message").addClass('ui-state-ok').html(response.text)
               $("#li_"+ext).fadeOut();
            }
          },'json');                   
          e.preventDefault();
	});

	
});

var Action 		= {
		
	save		: function()
	{
		jsDisplayResult("info", "info", "saving action . . . <img src='public/images/loaderA32.gif' /> " );
		$.post("main.php?controller=action&action=saveAction",{
			data 	: $("#addactionform").serialize(),
			kpiId	: $("#kpiid").val()		
		}, function( response ){
			if( response.error )
			{
				jsDisplayResult("error", "error", response.text );
			} else {
				jsDisplayResult("ok", "ok", response.text );
			}	
		},"json");	
	}, 
	
	getCompetency		: function()
	{
		$.getJSON("main.php?controller=competency&action=getAll", {
			status : {  status : 1 }				
		}, function( competencies ) {
			$.each( competencies, function( index, competency){
				$("#competency").append($("<option />",{text:competency.value, value:competency.id}))
			});				
		});
	} , 
	
	getProficiency		: function()
	{
		$.getJSON("main.php?controller=proficiency&action=getAll",{
			data : { status : 1 }
		}, function( proficiencies ){
			$.each( proficiencies, function(index, proficiency){
				$("#proficiency").append($("<option />",{text:proficiency.value, value:proficiency.id}))					
			});
		})			
	} , 
	
	getWeighting			: function()
	{
		$.getJSON("main.php?controller=weighting&action=getAll", {
			data	: { status : 1}
		}, function( weightings ){
			$.each( weightings, function(index, weighting ){
				$("#weighting").append($("<option />",{text:weighting.value+"("+weighting.weight+")", value:weighting.id}))
			});				
		})
		
	} , 
	
	edit            : function()
	{
		jsDisplayResult("info", "info", "updating action . . .  <img src='public/images/loaderA32.gif' /> " );
		$.post("main.php?controller=action&action=editAction",{
			data 	 : $("#editactionform").serialize(),
			actionid   : $("#id").val()		
		}, function( response ){
			if( response.error )
			{
				jsDisplayResult("error", "error", response.text );
			} else {
				jsDisplayResult("ok", "ok", response.text );
			}	
		},"json");	
	} , 
	
	update            : function()
	{
		if($("#response").val() == "")
		{
		     jsDisplayResult("error", "error", "Please enter the response message");
		     $("#response").focus();
		     return false;
		} else if($("#actionstatus :selected").val() == ""){
		     jsDisplayResult("error", "error", "Please select the action status ");
		     $("#actionstatus").focus();
		     return false;		
		} else if($("#progress").val() == ""){
		     jsDisplayResult("error", "error", "Please enter the action progress");
		     $("#progress").focus();
		     return false;		
		} else if($("#progress").val() > 100 || $("#progress").val() < 0 ){
		     jsDisplayResult("error", "error", "Please enter valid action progress between 1 and 100");
		     $("#progress").focus();
		     return false;		
		} else if(isNaN($("#progress").val())){
		     jsDisplayResult("error", "error", "Please enter a valid action progress");
		     $("#progress").focus();
		     return false;		
		} else {		
		     jsDisplayResult("info", "info", "updating action . . .  <img src='public/images/loaderA32.gif' /> " );
		     $.post("main.php?controller=action&action=update",{
			     response 	 : $("#response").val(),
			     progress   : $("#progress").val(),
			     status     : $("#actionstatus :selected").val(),
			     actionon   : $("#actionon").val(),
			     id         : $("#actionid").val(),
			     remindon   : $("#remindon").val(), 
			     requestapproval : $("#requestapproval").val()		
		     }, function( response ){
			     if( response.error )
			     {
				     jsDisplayResult("error", "error", response.text );
			     } else {
				     jsDisplayResult("ok", "ok", response.text );
			     }	
		     },"json");
		}	
	}	
};


function uploadActionAttachment(target)
{
   var action_id = $("#actionid").val();
   $("#uploaded_image").empty();
   $("#file_upload").html("uploading  ... <img  src='public/images/loaderA32.gif' />");
   $.ajaxFileUpload({
        url           : "main.php?controller=action&action=upload_attachment&action_id="+action_id,              
        dataType      : "json",
        secureuri     : true,
        fileElementId : "action_attachment_"+action_id,
        success       : function(response, status)
        {
             $("#file_upload").html("");
             if(response.error)
             {
               $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
             } else {
               $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
               ).append($("<div />",{id:"files_uploaded"}))
               if(!$.isEmptyObject(response.files))
               {     
                  var list = [];            
                  $.each(response.files, function(ref, file){
                  	list.push("<p id='li_"+ref+"' style='padding:2px;' class='ui-state'>");
                  	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                  	  list.push("<span style='margin-left:5px;'><a href='../class/request.php?action=main.php?controller=action&action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                  	list.push("</p>");
                    //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                  });
                  $("#files_uploaded").html(list.join(' '));
                  
                  $(".delete").click(function(e){
                    var id = this.id
                    var ext = $(this).attr('title');
                    $.post('main.php?controller=action&action=removeattachment', 
                      {
                       attachment : id,
                       ext        : ext,
                       action_id  : action_id
                      }, function(response){     
                      if(response.error)
                      {
                        $("#result_message").html(response.text)
                      } else {
                         $("#result_message").addClass('ui-state-ok').html(response.text)
                         $("#li_"+id).fadeOut();
                      }
                    },'json');                   
                    e.preventDefault();
                  });
                  $("#action_attachment").val("");
               } 
             }            
        } ,
        error         : function(data, status, e)
        {
          $("#file_upload").html( "Ajax error -- "+e+", please try uploading again");             
        }
    });    
}
