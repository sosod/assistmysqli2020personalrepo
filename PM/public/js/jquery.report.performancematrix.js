$.widget("ui.performancematrix", {

    options         : {
     evaluationyear      : 0,
     evaluationperiod    : 0,
     directorate         : 0,
     subdirectorate      : 0,    
     employee            : 0,
     orderby             : 0,
     order               : 0,
     autoLoad            : false
    } ,
    
    _init           : function()
    {
       if(this.options.autoLoad)
       {
          this._get();
       }
    } ,
    
    _create         : function()
    {
       var self = this;
       $(self.element).append($("<table />",{width:"100%"}).addClass("noborder")
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />",{width:"50%"})
               .append($("<tr />")
                 .append($("<th />",{html:"Evaluation year:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationyear", id:"evaluationyear"}))
                 )         
               )
               .append($("<tr />").addClass("filters").css({"display":"none"})
                 .append($("<th />",{html:"Employee:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"employee", id:"employee"}))
                 )
               )  
               .append($("<tr />",{id:"message"})
                 .append($("<td />",{colspan:"2"})
                   .append($("<p />",{html:"Please select the evaluation year"})
                      .addClass("ui-state-info")
                      .css({padding:"5px"})
                    )
                 )
               )                                
             )             
           )           
         )
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />",{width:"100%", id:"performance_matrix_table"}))
           )
         )
       )
       
       self._loadEvaluationYears();
       
       $("#evaluationyear").on('change', function(e){
          $(".filters").show();
          $("#message").hide()
          self._loadUsers();
          self.options.evaluationyear = $(this).val();
          self._get();
          e.preventDefault();
       });
       
       $("#employee").on('change', function(e){
         self.options.employee = $(this).val();
         self._get();
         e.preventDefault();
       })

    } ,

	_loadEvaluationYears		: function()
	{
	     $("#evaluationyear").append($("<option />",{value:"", text:"--select evaluation year--"}))
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
		   $.each( responseData, function( index, evalYear) {
			 $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))
		   });								
		},"json");
	} ,
	
	_loadEvaluationPeriods      : function(evaluationyear)
	{
	    $("#evaluationperiod").empty();
	    $("#evaluationperiod").append($("<option />",{value:"", text:"--select evaluation period--"}))
	    $.post("main.php?controller=evaluationperiod&action=get_all", {evaluationyear : evaluationyear},
		 function( responseData ){
		  var period_int = 1;
		  $.each(responseData['periods'][1], function(index, period) {
		    text_str = period.start_date+" -- "+period.end_date+" ("+period.name+" "+period_int+" )";
		    period_int++;
		    $("#evaluationperiod").append($("<option />",{value:period.id, text:text_str}))
		  });								
	    },"json");
	} ,
	
	_loadUsers            : function(departmentid)
	{
	     var self = this;
	     self.options.employee = "";
	     $("#employee").empty();
	     $("#employee").append($("<option />",{value:"", text:"All"}))
		$.post("main.php?controller=user&action=getAll",
		{
		   departmentid : departmentid
		}, function(users) {	
	        if(!$.isEmptyObject(users))
	        {
	           $.each(users, function(index, user){
	              $("#employee").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}))
	          });
	        }
		},"json"); 
	} ,
	
    _get            : function()
    {
       var self = this;
	  $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading ... please wait <img src='public/images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"300px", left:"300px", border:"0px solid #FFFFF", padding:"5px"})
	  )         
       $.getJSON("main.php?controller=usercomponent&action=get_confirmed_matrix_report", {
            evaluationyear     : self.options.evaluationyear,
            evaluationperiod   : self.options.evaluationperiod,
            directorate        : self.options.directorate,
            employee           : self.options.employee
       }, function(matrixReport) {
          $("#performance_matrix_table").html("")
          if($.isEmptyObject(matrixReport))
          {
             $("#performance_matrix_table").append("<tbody><tr><td colspan='6'>No records match the selected filters</td></tr></tbody>");
          } else {
            self._display(matrixReport);
          }
          $("#loadingDiv").remove();
       }); 
    },
    
    
    _display          : function(reportData)
    {
      var self = this;
      var html = [];
       html.push("<thead>");
       html.push("<tr>");
        html.push("<td colspan='19'><h2>Confirmed Matrix Report for "+reportData['year']['start']+" - "+reportData['year']['end']+" as at "+reportData['report_date']+"</h2></td>");
       html.push("</tr>");                    
       html.push("</thead>");      
       var report = reportData['usermatrix'];
       var len    = report.length;
      for(var i = 0; i < len; i++)
      {
         var _self_total    = 0;
         var _manager_total = 0;
         var _joint_total   = 0;
         var _review_total  = 0;
          html.push("<tr>");
           html.push("<th colspan='2'>Emaployee</th>");
           html.push("<td colspan='2'>"+report[i]['user']['user']+"</td>");
           html.push("<th colspan='2'>Manager</th>");
           html.push("<td colspan='2'>"+report[i]['user']['manager']+"</td>");
           html.push("<th colspan='2'>Evaluation Year</th>");
           html.push("<td colspan='2'>"+report[i]['user']['year']+"</td>");
           html.push("<th colspan='2'>Job Level</th>");
           html.push("<td colspan='2'>"+report[i]['user']['joblevel']+"</td>");        
           html.push("<th colspan='2'>Perfomance Category</th>");
           html.push("<td colspan='2'>"+report[i]['user']['category']+"</td>");           
          html.push("</tr>");
          if(report[i].hasOwnProperty('component'))
          {
                html.push("<tr>");
                  html.push("<td colspan='2'></td>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Reference</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Name</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Objective</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Outcome</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Measurement</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Proof Of Evidence</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Baseline</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Targer Unit</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>National KPA</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>IDP Objective</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Performance Standards</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Targer Per Period</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Weighting</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Program Driver</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Deadline</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Progress</b></th>");
                  html.push("<th style='background-color:#EEF; color:#000;'><b>Status</b></th>");
                html.push("</tr>");                 
              $.each(report[i]['component'], function(componentid, component){

                    html.push("<tr>");
                      html.push("<td colspan='19'><h3><b>"+component['componentname']+"</b> <em>"+component['weight']+"</em>%</b></h3></td>");
                    html.push("</tr>");    
                    if(component.hasOwnProperty('kpa'))
                    {
                        html.push("<tr>");
                          html.push("<td colspan='19' style='background-color:#EEF; color:#000;'><b>KPA</td>");
                        html.push("</tr>"); 
                        if(!$.isEmptyObject(component['kpa']['data']))
                        {
                            $.each(component['kpa']['data'], function(kpa_index, kpa){
                                html.push("<tr>");
                                  html.push("<th colspan='2'></th>");
                                  html.push("<td>"+kpa['id']+"</td>");
                                  html.push("<td>"+kpa['name']+"</td>");
                                  html.push("<td>"+kpa['objective']+"</td>");
                                  html.push("<td>"+kpa['outcome']+"</td>");
                                  html.push("<td>"+kpa['measurement']+"</td>");
                                  html.push("<td>"+kpa['proof_of_evidence']+"</td>");
                                  html.push("<td>"+kpa['baseline']+"</td>");
                                  html.push("<td>"+kpa['targetunit']+"</td>");
                                  html.push("<td>"+kpa['nationalkpa']+"</td>");
                                  html.push("<td>"+kpa['idpobjectives']+"</td>");
                                  html.push("<td>"+kpa['performance_standard']+"</td>");
                                  html.push("<td>"+kpa['target_period']+"</td>");
                                  html.push("<td>"+kpa['weighting']+"</td>");
                                  html.push("<td>"+kpa['owner']+"</td>");
                                  html.push("<td>"+kpa['deadline']+"</td>");
                                  html.push("<td>"+kpa['progress']+"</td>");
                                  html.push("<td>"+kpa['status']+"</td>");                   
                                html.push("</tr>"); 
                            });                         
                        }   
                    }                  
                    if(component.hasOwnProperty('kpi'))
                    {
                        html.push("<tr>");
                          html.push("<td colspan='19' style='background-color:#EEF; color:#000;'><b>KPI</td>");
                        html.push("</tr>"); 
                        if(!$.isEmptyObject(component['kpi']['data']))
                        {
                            $.each(component['kpi']['data'], function(kpi_index, kpi){
                                html.push("<tr>");
                                  html.push("<th colspan='2'></th>");
                                  html.push("<td>"+kpi['id']+"</td>");
                                  html.push("<td>"+kpi['name']+"</td>");
                                  html.push("<td>"+kpi['objective']+"</td>");
                                  html.push("<td>"+kpi['outcome']+"</td>");
                                  html.push("<td>"+kpi['measurement']+"</td>");
                                  html.push("<td>"+kpi['proof_of_evidence']+"</td>");
                                  html.push("<td>"+kpi['baseline']+"</td>");
                                  html.push("<td>"+kpi['targetunit']+"</td>");
                                  html.push("<td>"+kpi['nationalkpa']+"</td>");
                                  html.push("<td>"+kpi['idpobjectives']+"</td>");
                                  html.push("<td>"+kpi['performance_standard']+"</td>");
                                  html.push("<td>"+kpi['target_period']+"</td>");
                                  html.push("<td>"+kpi['weighting']+"</td>");
                                  html.push("<td>"+kpi['owner']+"</td>");
                                  html.push("<td>"+kpi['deadline']+"</td>");
                                  html.push("<td>"+kpi['progress']+"</td>");
                                  html.push("<td>"+kpi['status']+"</td>");                   
                                html.push("</tr>"); 
                            });                            
                        }   
                    }                                      
                    if(component.hasOwnProperty('actions'))
                    {
                        html.push("<tr>");
                          html.push("<td colspan='19' style='background-color:#EEF; color:#000;'><b>Actions</td>");
                        html.push("</tr>");                      
                        if(!$.isEmptyObject(component['actions']['data']))
                        {
                            $.each(component['actions']['data'], function(action_index, action){ 
                                html.push("<tr>");
                                  html.push("<th colspan='2'></th>");
                                  html.push("<td>"+action['id']+"</td>");
                                  html.push("<td>"+action['name']+"</td>");
                                  html.push("<td>"+action['objective']+"</td>");
                                  html.push("<td>"+action['outcome']+"</td>");
                                  html.push("<td>"+action['measurement']+"</td>");
                                  html.push("<td>"+action['proof_of_evidence']+"</td>");
                                  html.push("<td>"+action['baseline']+"</td>");
                                  html.push("<td>"+action['targetunit']+"</td>");
                                  html.push("<td>"+action['nationalkpa']+"</td>");
                                  html.push("<td>"+action['idpobjectives']+"</td>");
                                  html.push("<td>"+action['performance_standard']+"</td>");
                                  html.push("<td>"+action['target_period']+"</td>");
                                  html.push("<td>"+action['weighting']+"</td>");
                                  html.push("<td>"+action['owner']+"</td>");
                                  html.push("<td>"+action['deadline']+"</td>");
                                  html.push("<td>"+action['progress']+"</td>");
                                  html.push("<td>"+action['status']+"</td>");                   
                                html.push("</tr>"); 
                            });                             
                        }   
                        if(component.hasOwnProperty('module_actions'))
                        {
                            if(!$.isEmptyObject(component['module_actions']['data']))
                            {
                                $.each(component['module_actions']['data'], function(action_index, action){ 
                                    html.push("<tr>");
                                      html.push("<th colspan='2'></th>");
                                      html.push("<td>"+action['id']+"</td>");
                                      html.push("<td>"+action['name']+"</td>");
                                      html.push("<td>"+action['objective']+"</td>");
                                      html.push("<td>"+action['outcome']+"</td>");
                                      html.push("<td>"+action['measurement']+"</td>");
                                      html.push("<td>"+action['proof_of_evidence']+"</td>");
                                      html.push("<td>"+action['baseline']+"</td>");
                                      html.push("<td>"+action['targetunit']+"</td>");
                                      html.push("<td>"+action['nationalkpa']+"</td>");
                                      html.push("<td>"+action['idpobjectives']+"</td>");
                                      html.push("<td>"+action['performance_standard']+"</td>");
                                      html.push("<td>"+action['target_period']+"</td>");
                                      html.push("<td>"+action['weighting']+"</td>");
                                      html.push("<td>"+action['owner']+"</td>");
                                      html.push("<td>"+action['deadline']+"</td>");
                                      html.push("<td>"+action['progress']+"</td>");
                                      html.push("<td>"+action['status']+"</td>");                   
                                    html.push("</tr>"); 
                                });                             
                            }                            
                        }
                        
                    }                  

              });             
          } else {
            html.push("<tr>");
              html.push("<td colspan='19'>There are no components setup for this user for the selected criteria</td>");      
            html.push("</tr>");                
          }
          html.push("<tr>");
            html.push("<td colspan='19'>&nbsp;&nbsp;&nbsp;</td>");      
          html.push("</tr>"); 
      }
      $("#performance_matrix_table").append("<tbody>"+html.join('')+"</tbody>").find('th').css({"text-align":"left"});
    }


});
