$(function(){
	
	$("#evaluationyear").change(function(){
		var evaluationyear = $(this).val();
		$("#detailed").show();
		$(".evaluationyear").fadeIn();
		$("#evaluationyearid").val( evaluationyear );
		$("#evaluationYear").html("<em>"+$("#evaluationyear :selected").attr('title')+"</em>" );
		//$("#goalsdiv").goals({url:"kpa?action=getAll",  : evaluationyear,  goaltype:1});
		$("#goalsdiv").kpa({evaluationyear:evaluationyear, componentid:3, addKpa:true, editKpa:true, addKpi:true, addAction:true, section:"admin", page:"admin"})
		return false;
	});
	
	var evaluationyear = $("#evaluationyearid").val();
	if( evaluationyear !== "")
	{	
	   $("#goalsdiv").kpa({evaluationyear:evaluationyear, componentid:3, addKpa:true, editKpa:true, addKpi:true, addAction:true, section:"admin", page:"admin"});	
	}
	
	 OrganisationalSetup.getEvaluationyears( evaluationyear );
	 
});

var OrganisationalSetup = {
		
		showOrganisational		: function()
		{
			$("#evaluationYear").html("<em>"+$("#evaluationyear :selected").attr('title')+"</em>" );
			$("#selectevaluationyear").dialog("destroy");
			$("#selectevaluationyear").remove();
			$("#goalsdiv").goals({url:"kpa?action=getAll", goaltype:3, addKpa:true, editKpa : true,  addKpi:true, addAction:true});			
		}  ,
		
		getEvaluationyears		: function( evalyear )
		{
			$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
				$.each( responseData, function( index, evalYear) {
					if( evalyear == evalYear.id )
					{
						$("#evaluationYear").fadeIn().html("Evaluation Year : "+evalYear.start+" -- "+evalYear.end );
					}
					if(evalyear == evalYear.id)
					{
                              $("#evaluationyear").append($("<option />",{value    : evalYear.id, 
                                                                          text     : evalYear.start+" -- "+evalYear.end, 
                                                                          selected : "selected",
                                                                          title    : "<b>From<\/b> "+evalYear.start+" <b>to<\/b> "+evalYear.end
                                                                         })
                                                           )										     
					} else {
                              $("#evaluationyear").append($("<option />",{value    : evalYear.id, 
                                                                          text     : evalYear.start+" -- "+evalYear.end,
                                                                          title    : "<b>From<\/b> "+evalYear.start+" <b>to<\/b> "+evalYear.end
                                                                         })
                                                           )										     					
					}						
				});								
			},"json");
		} 

};
