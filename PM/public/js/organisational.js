$(function(){
	
	$("#evaluationyear").change(function(){
	     
	     if($("#organisationldialog").length > 0)
	     {
	          $("#organisationldialog").dialog("destroy");
	          $("#organisationldialog").remove();
	     }
	     
		var evaluationyear = $(this).val();
		$("#detailed").show();
		$(".evaluationyear").fadeIn();
		$("#evaluationyearid").val( evaluationyear );
		$("#evaluationYear").html("<em>"+$("#evaluationyear :selected").attr('title')+"</em>" );	     
	     
	     $("<div />",{id:"organisationldialog", html:"Does KPAs come from this module or imported from SDBIP"}).dialog({
	                    autoOpen  : true,
	                    title     : "Add/Import Organisational KPA, KPI and Actions",
	                    position  : "top",
	                    width     : 350,
	                    height    : 150,
	                    buttons   : {
	                                 "Import from SDBIP"       : function()
	                                 {
	                                        $("#organisationldialog").dialog("destroy");
	                                        $("#organisationldialog").remove();
	                                        $(".importsdbip").show();
	                                        $(".createnew").hide();	                                  
	                                 } , 
	                                 "Create New"        : function()
	                                 {
	                                   $(".createnew").show();
	                                   $(".importsdbip").hide();
		                              //$("#goalsdiv").goals({url:"kpa?action=getAll",  : evaluationyear,  goaltype:1});
		                              $("#goalsdiv").kpa({evaluationyear:evaluationyear, componentid:1, addKpa:true, editKpa:true, addKpi:true, addAction:true, section:"admin", page:"admin"})	               
	                                        $("#organisationldialog").dialog("destroy");
	                                        $("#organisationldialog").remove();		                                                
	                                 }
	                    }	     
	     })
		return false;
	});
	
	var evaluationyear = $("#evaluationyearid").val();
	if( evaluationyear !== "")
	{	
	     $("#goalsdiv").kpa({evaluationyear:evaluationyear, componentid:1, addKpa:true, editKpa:true,  addKpi:true, addAction:true, section:"admin", page:"admin"})	
	}
	
	 OrganisationalSetup.getEvaluationyears( evaluationyear );
	 
});

var OrganisationalSetup = {
		
		showOrganisational		: function()
		{
			$("#evaluationYear").html("<em>"+$("#evaluationyear :selected").attr('title')+"</em>" );
			$("#selectevaluationyear").dialog("destroy");
			$("#selectevaluationyear").remove();
			$("#goalsdiv").goals({url:"kpa?action=getAll", goaltype:1, addKpa:true, editKpa : true,  addKpi:true, addAction:true});			
		}  ,
		
		getEvaluationyears		: function( evalyear )
		{
			$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
				$.each( responseData, function( index, evalYear) {
					if( evalyear == evalYear.id )
					{
						$("#evaluationYear").fadeIn().html("Evaluation Year : "+evalYear.start+" -- "+evalYear.end );
					}
					if(evalyear == evalYear.id)
					{
                              $("#evaluationyear").append($("<option />",{value    : evalYear.id, 
                                                                          text     : evalYear.start+" -- "+evalYear.end, 
                                                                          selected : "selected",
                                                                          title    : "<b>From<\/b> "+evalYear.start+" <b>to<\/b> "+evalYear.end
                                                                         })
                                                           )										     
					} else {
                              $("#evaluationyear").append($("<option />",{value    : evalYear.id, 
                                                                          text     : evalYear.start+" -- "+evalYear.end,
                                                                          title    : "<b>From<\/b> "+evalYear.start+" <b>to<\/b> "+evalYear.end
                                                                         })
                                                           )										     					
					}			
				});								
			},"json");
		} 

};
