$.widget("ui.bellcurve", {

    options         : {
     evaluationyear      : 0,
     evaluationperiod    : 0,
     directorate         : 0,
     subdirectorate      : 0,    
     joblevel            : 0,
     orderby             : 0,
     order               : 0,
     evaluationlevel     : "",
     autoLoad            : false
    } ,
    
    _init           : function()
    {
       if(this.options.autoLoad)
       {
          this._get();
       }
    } ,
    
    _create         : function()
    {
       var self = this;
       $(self.element).append($("<table />",{width:"100%"}).addClass("noborder")
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />")
               .append($("<tr />")
                 .append($("<th />",{html:"Evaluation year:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationyear", id:"evaluationyear"}))
                 )
                 .append($("<th />",{html:"Evaluation period:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationperiod", id:"evaluationperiod"}))             
                 )            
               )
               .append($("<tr />").addClass("filters").css({"display":"none"})
                 .append($("<th />",{html:"Evaluation Level:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"evaluationlevel", id:"evaluationlevel"})
                      .append($("<option />",{value:"", text:"All"}))
                      .append($("<option />",{value:1, text:"Self Evaluation"}))
                      .append($("<option />",{value:2, text:"Manager Evaluation"}))
                      .append($("<option />",{value:3, text:"Joint Evaluation"}))
                      .append($("<option />",{value:4, text:"Review Evaluation"}))
                   )
                 )
               )
               .append($("<tr />").addClass("filters").css({"display":"none"})
                 .append($("<th />",{html:"Department:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"department", id:"department"}))
                 )
               )
               .append($("<tr />").addClass("filters").css({"display":"none"})
                 .append($("<th />",{html:"Job Level:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"joblevel", id:"joblevel"}))
                 )
               )  
               .append($("<tr />",{id:"message"})
                 .append($("<td />",{colspan:"4"})
                   .append($("<p />",{html:"Please select the evaluation year and evaluation period"})
                      .addClass("ui-state-info")
                      .css({padding:"5px"})
                    )
                 )
               )                             
             )             
           )           
         )
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />",{width:"100%", id:"bellcurve_table"}))
           )
         )
       )
       if(self.options.evaluationperiod != "")
       {
          $(".filters").show();
          $("#message").hide();
       }
       
       if(self.options.evaluationlevel !== "")
       {
          $("#evaluationlevel").val(self.options.evaluationlevel);
       }
       
       self._loadEvaluationYears();
       self._loadDirectorates();
       self._loadJobLevels();
       if(self.options.evaluationyear != "")
       {
          self._loadEvaluationPeriods(self.options.evaluationyear);               
       }
       
       $("#evaluationlevel").on('change', function(){
         self.options.evaluationlevel = $(this).val();
         self._get();
       })
       $("#department").on('change', function(){
         self.options.directorate = $(this).val();
         self._get();
       })
       
       $("#evaluationyear").on('change', function(){
          self.options.evaluationyear = $(this).val();
          self._loadEvaluationPeriods($(this).val());       
       });
       
       $("#evaluationperiod").on('change', function(e){
         $(".filters").show();
         $("#message").remove();
         self.options.evaluationperiod = $(this).val();
         self._get();
         e.preventDefault();
       });
       
       $("#joblevel").on('change', function(e){
         self.options.joblevel = $(this).val();
         self._get();
         e.preventDefault();
       })

    } ,

	_loadEvaluationYears		: function()
	{
	    var self = this;
	    $("#evaluationyear").append($("<option />",{value:"", text:"--select evaluation year--"}))
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
		   $.each( responseData, function( index, evalYear) {
		    if(self.options.evaluationyear == evalYear.id)
		    {
		        $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end, selected:"selected"}))
		    } else {
		        $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))
		    }
		   });								
		},"json");
	} ,
	
	_loadEvaluationPeriods      : function(evaluationyear)
	{
	    var self = this;
	    $("#evaluationperiod").empty();
	    $("#evaluationperiod").append($("<option />",{value:"", text:"--select evaluation period--"}))
	    $.post("main.php?controller=evaluationperiod&action=get_all", {evaluationyear : evaluationyear},
		 function( responseData ){
		  var period_int = 1;
		  $.each(responseData['periods'][1], function(index, period) {
		    text_str = period.start_date+" -- "+period.end_date+" ("+period.name+" "+period_int+" )";
		    period_int++;
		    if(self.options.evaluationperiod == period.id)
		    {
		        $("#evaluationperiod").append($("<option />",{value:period.id, text:text_str, selected:"selected"}))
		    } else {
		        $("#evaluationperiod").append($("<option />",{value:period.id, text:text_str}))
		    }
		  });								
	    },"json");
	} ,
	
	_loadJobLevels            : function()
	{
	    var self = this;
	    $("#joblevel").empty();
	    $("#joblevel").append($("<option />",{value:"", text:"--select job level--"}))
         $.getJSON("main.php?controller=joblevel&action=get_all", function(joblevels){
	        if(!$.isEmptyObject(joblevels))
	        {
	           $.each(joblevels, function(index, joblevel){
		        if(self.options.joblevel == joblevel.id)
		        {
		           $("#joblevel").append($("<option />",{value:joblevel.id, text:joblevel.name, selected:"selected"}))
		        } else {
		           $("#joblevel").append($("<option />",{value:joblevel.id, text:joblevel.name}))
		        }	           	              
	          });
	        }
         });
	} ,
	
	_loadDirectorates       : function()
	{
	    var self = this;
	    $("#department").empty();
	    $("#department").append($("<option />",{value:"", text:"All"}))
	    $.post("main.php?controller=directorate&action=get_all_departments",function(directorates){	
	        if(!$.isEmptyObject(directorates))
	        {
	           $.each(directorates, function(index, directorate){
		          if(self.options.directorate == directorate.id)
		          {
		             $("#department").append($("<option />",{value:directorate.id, text:directorate.name,  selected:"selected"}))
		          } else {
		             $("#department").append($("<option />",{value:directorate.id, text:directorate.name}))
		          }	     	              
	          });
	        }
	    },"json");
	} ,
	
    _get            : function()
    {
        var self = this;
        document.location.href = "main.php?controller=report&action=bell_curve&evaluationyear="+self.options.evaluationyear+"&evaluationperiod="+self.options.evaluationperiod+"&directorate="+self.options.directorate+"&joblevel="+self.options.joblevel+"&evaluationlevel="+self.options.evaluationlevel
    }


});
