$(function(){
    JobLevel.getEvaluationyears();
    
    $("#evaluationyear").change(function(){
        var evaluationyear = $(this).val()
        JobLevel.getCategories(evaluationyear);
        JobLevel.get(evaluationyear);    
        return false;
    })
});

var JobLevel = {

	getEvaluationyears		: function()
	{
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
			$.each( responseData, function( index, evalYear) {
				$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))					
			});								
		},"json");
	} ,    
    
    get         : function(evaluationyear)
    {
	   $(".joblevel").remove();
        $.getJSON("main.php?controller=joblevel&action=getAll", {evaluationyear:evaluationyear}, function(joblevels){
            $.each(joblevels.joblevels, function(orgposition, joblevel){
                JobLevel.display(joblevel, evaluationyear);           
            });
            $(".categories").empty();
            JobLevel.attachCategories(joblevels.usedcategories);      
            $("#joblevel_table").append($("<tr />").addClass('joblevel')
               .append($("<td />",{colspan:"4"}).css({"text-align":"right"})
                 .append($("<input />",{type:"button", name:"assign_all", id:"assign_all", value:"Assign All"})
                   .click(function(e){
                       JobLevel.assignAll(evaluationyear);
                       e.preventDefault()                    
                   })
                 )
               )
             )    
        });
    } ,
    
    display         : function(joblevel, evaluationyear)
    {
        $("#joblevel_table").append($("<tr />").addClass("joblevel")
           .append($("<td />",{html:joblevel.joblevel}))
           .append($("<td />",{html:joblevel.orgposition+"("+joblevel.orglevel+") - "+joblevel.levelname}))
           .append($("<td />")
             .append($("<select />",{id:"category_"+joblevel.joblevel, name:"category_"+joblevel.joblevel}).addClass("categories"))
           )
           .append($("<td />")
             .append($("<input />",{type:"button", name:"save_"+joblevel.levelid, id:"save_"+joblevel.levelid, value:"Assign"})
              .css({"display" : (joblevel.used ? "none" : "block")})
              .click(function(){
                    jsDisplayResult("info", "info", "Saving  ...  <img src='public/images/loaderA32.gif' />");
                    $.post("main.php?controller=joblevel&action=saveCategoryJobLevel",{
                        evaluationyear  : evaluationyear, 
                        joblevel        : joblevel.levelid,
                        orglevel        : joblevel.orglevel,
                        category_id     : $("#category_"+joblevel.joblevel+" :selected").val()
                    }, function(response) {
                        if(response.error)
                        {
                            jsDisplayResult("error", "error", response.text);
                        } else {
                            jsDisplayResult("ok", "ok", response.text);
                            JobLevel.get(evaluationyear);
                        }
                    },"json");            
                    return false;
              })
             )
             .append($("<input />",{type:"button", name:"update_"+joblevel.levelid, id:"update_"+joblevel.levelid, value:"Assign"})
               .css({"display" : (joblevel.used ? "block" : "none")})
               .click(function(){
                    jsDisplayResult("info", "info", "Updating  ...  <img src='public/images/loaderA32.gif' />");
                    $.post("main.php?controller=joblevel&action=updatingCategoryJobLevel",{
                        evaluationyear  : evaluationyear, 
                        joblevel        : joblevel.joblevel,
                        orglevel        : joblevel.orglevel,
                        category_id     : $("#category_"+joblevel.joblevel+" :selected").val()
                    }, function(response) {
                        if(response.error)
                        {
                            jsDisplayResult("error", "error", response.text);
                        } else {
				       if(response.updated)
				       {
					     jsDisplayResult("info", "info", response.text );
				       } else {
					     jsDisplayResult("ok", "ok", response.text );					
				       } 	
                           JobLevel.get(evaluationyear);
                        }
                    },"json");            
                    return false;
               })
             )
           )
        ).find("tbody>tr").filter(":odd").addClass("tdhover")
        
    } ,
    
    getCategories       : function(evaluationyear, usedcategories)
    {
        $(".categories").empty();
       
		$.getJSON("main.php?controller=performancecategories&action=getAll",{
			evaluationyear : evaluationyear				
		}, function( responseData ){
		    $("body").data("categories", responseData);
		});
    } , 
    
    attachCategories    : function(usedcategories)
    {
        var categories = $("body").data("categories");
        $(".categories").append($("<option />",{text:"--please select--", value:""}))
		$.each( categories, function( index, category ){	
	        $(".categories").append($("<option />",{value:category.id, text:category.name}));					
	        $.each(usedcategories, function(joblevel, catLevel) {
	            $("#category_"+joblevel+" option[value="+catLevel.category+"]").attr("selected", "selected");
	        });		        
		});			
    } ,
    
    assignAll            : function(evaluationyear)
    {
       jsDisplayResult("info", "info", "saving  . . .  <img src='public/images/loaderA32.gif' />");
       $.post("main.php?controller=joblevel&action=save_all",{
          data     : $("#joblevel_form").serialize()
        }, function(response){
              if(response.error)
              {
                  jsDisplayResult("error", "error", response.text);
              } else {
		       if(response.updated)
		       {
			     jsDisplayResult("info", "info", response.text );
		       } else {
			     jsDisplayResult("ok", "ok", response.text );					
		       } 	
                  JobLevel.get(evaluationyear);
              }
       },"json");
    }



};
