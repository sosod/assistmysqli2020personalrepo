$(function(){

   EvaluationStatus.getEvaluationyears();
   EvaluationStatus.getUser();
   
   var user           = $("#user");
   var evaluationyear = $("#evaluationyear")
   
   user.change(function(){
     if($(this).val() !== "")
     {
        if(evaluationyear.val() !== "")
        {
          EvaluationStatus.evaluationStatusReport($(this).val(), evaluationyear.val());
        }
     }
   });
   
   evaluationyear.change(function(){
     if($(this).val() !== "")
     {
        if(user.val() !== "")
        {
          EvaluationStatus.evaluationStatusReport(user.val(), $(this).val());
        }
     }
   });   
   

});

var EvaluationStatus  = {

	getEvaluationyears		: function()
	{
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
			$.each( responseData, function( index, evalYear) {
				$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))					
			});								
		},"json");
	} ,
	
	getUser            : function(userid)
	{
	    $(".users").remove();
		$.post("main.php?controller=user&action=getAll",function(users){	
	        if(!$.isEmptyObject(users))
	        {
	            $.each(users, function(index, user){
                    if(user.tkid == userid)
                    {
	                 $("#user").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname, selected:"selected"})) 
	               } else {
	                 $("#user").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}))    
	               }
	            });
	        }
		},"json"); 
		
	} ,
	
	evaluationStatusReport             : function(user, evaluationyear)
	{
	   var self = this;
	   $('.evaluation_scores').remove();
	   $.post("main.php?controller=evaluation&action=get_evaluation_scores_report", 
	   {user:user, evaluationyear:evaluationyear},function(evaluationStatuses) {
	     if(!$.isEmptyObject(evaluationStatuses))
	     {
	       $.each(evaluationStatuses, function(index, evaluationStatus){
	          if(evaluationStatus.hasOwnProperty('status'))
	          {
                  self._displayEmptyComponent(evaluationStatus)	          
	          } else {
                  self._displayComponents(evaluationStatus);
	          }
	       });
	     } else {
	        $("#table_header").after($("<tr />").addClass('evaluation_scores')
	          .append($("<td />",{colspan:"6", html:"There no sub-ordinates setup for the selected criteria"}))
	        )
	     }
	   },"json"); 
	     
	} ,
	
	_displayComponents   : function(evaluationStatus)
	{
	    var self = this;     
	    $("#table_header").after($("<tr />").addClass('evaluation_scores')
	      .append($("<td />",{html:evaluationStatus.user}))
	      .append($("<td />",{html:evaluationStatus.manager}))
	      .append($("<td />",{html:evaluationStatus.selfeval}))
	      .append($("<td />",{html:evaluationStatus.managereval}))
	      .append($("<td />",{html:evaluationStatus.jointeval}))
	      .append($("<td />",{html:evaluationStatus.revieweval}))
	    )
	},
	
	_displayEmptyComponent   : function(userComponentStatus)
	{
	    var self = this;     
	    $("#table_header").after($("<tr />").addClass('evaluation_scores')
	      .append($("<td />",{html:userComponentStatus.user}))
	      .append($("<td />",{html:userComponentStatus.manager}))
	      .append($("<td />",{colspan:"4", html:userComponentStatus.status}).css({"color":"#000000"}))
	    )
	}
};
