$(function(){
	
	$("#natObj").hide();
	Nationalobjectives.getEvaluationyears(); 
	
	$("#evaluationyear").change(function(){
		var evaluationyear = $(this).val();
		$("#evaluationyearid").val( evaluationyear );
		
		if( evaluationyear == undefined || evaluationyear == "")
		{
			$("#natObj").fadeOut();			
		} else {
			$("#natObj").fadeIn();
		}		
		Nationalobjectives.get( evaluationyear );
		return false;
	});
	
	$("#addnew").click(function(){
		
		if( $("#addnewdialog").length > 0)
		{
			$("#addnewdialog").remove();			
		}
		
		$("<div />",{id:"addnewdialog"})
		  .append($("<table />")
			 .append($("<tr />")
			   .append($("<th />",{html:"Short Code"}))
			   .append($("<td />")
				  .append($("<input />",{type:"text", name:"shortcode", id:"shortcode", value:""}))	   
			    )
			 )
			 .append($("<tr />")
			   .append($("<th />",{html:"National KPA"}))
			   .append($("<td />")
				  .append($("<input />",{type:"text", name:"name", id:"name", value:""}))	   
			    )
			 )
			 .append($("<tr />")
			   .append($("<th />",{html:"National KPA Description"}))
			   .append($("<td />")
				  .append($("<textarea />",{type:"text", name:"description", id:"description", value:""}))	   
			    )
			 )
			 .append($("<tr />")
			   .append($("<td />",{colspan:2})
				 .append($("<p />")
				   .append($("<span />"))
				   .append($("<span />",{id:"message"}))
				 )	   
			   )		 
			 )
		  ).dialog({
			  		autoOpen		: true, 
			  		modal			: true, 
			  		title 			: "Add New National Objectives",
			  		width			: "500px",
			  		position		: "top",
			  		buttons			: {
			  							"Save"		: function()
			  							{
			  								if( $("#shortcode").val() == ""){
			  									$("#message").html("Please enter the short code");
			  									return false;
			  								} else if( $("#name").val() == ""){
			  									$("#message").html("Please enter the national kpa name")
			  									return false;
			  								} else if( $("#description").val() == ""){
			  									$("#message").html("Please enter the national kpa description");
			  									return false;
			  								} else {
			  									Nationalobjectives.save();			  									
			  								}
			  								
			  							} , 
			  							"Cancel"		: function()
			  							{
			  								$(this).dialog("destroy");			  								
			  							}			  
		  			} , 
		  			
		  			open			: function( event , ui)
		  			{
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});			
		  			}
		  });
		
		return false;
	});
	
});

var Nationalobjectives 		= {
		
		get				: function( evaluationyear )
		{
			$(".national").remove();
			$.getJSON("main.php?controller=nationalobjectives&action=getAll",{
				evaluationyear 	: 	evaluationyear			
			},function( responseData ){
				if( $.isEmptyObject(responseData))
				{
					$("#table_nationalobjectives").append($("<tr />").addClass("national")
					  .append($("<td />",{html:"There are no national objectives setup for this financial year",colspan:"6"}))		
					)
				} else {
					$.each( responseData, function( index, nationalobjectives){
						Nationalobjectives.display(nationalobjectives);				
					});						
				}					
			});
	
		} ,
		
		getEvaluationyears		: function( )
		{
			$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
				$.each( responseData, function( index, evalYear){
					$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end}))					
				});								
			},"json");
		} ,
		
		save			: function()
		{
			var evalYear  = $("#evaluationyearid").val();
			$.post("main.php?controller=nationalobjectives&action=saveNationalObj", {
				data 	: {
							shortcode 	: $("#shortcode").val(),
							name		: $("#name").val(),
							description	: $("#description").val(),
							evaluationyear	: evalYear
					}				
			},function( response ){
				if( response.error )
				{
					$("#message").html( response.text )
				} else {
					Nationalobjectives.get( evalYear );
					jsDisplayResult("ok", "ok", response.text);
					$("#addnewdialog").dialog("destroy");
					$("#addnewdialog").remove();
				}				
			},"json")
			
		} , 
		
		deleteKpa		: function( id )
		{
			if( confirm("Are you sure you want to delete this national kpa"))
			{
				Nationalobjectives.updateStatus( id, 2);				
			}
			
		} ,
		
		updateObjectives			: function( id )
		{
			$.post("main.php?controller=nationalobjectives&action=updateNationalObj", {
				data 	: {
					shortcode 	: $("#shortcode").val(),
					name		: $("#name").val(),
					description	: $("#description").val()
				} , 
				id		: id 
			}, function( response ){
				if( response.error )
				{
					$("#message").html( response.text )
				} else {
					Nationalobjectives.get( $("#evaluationyearid").val() );
		                 if(response.updated)
		                 {
			               jsDisplayResult("info", "info", response.text );
		                 } else {
			               jsDisplayResult("ok", "ok", response.text );					
		                 } 	
					$("#editobjectivedialog").dialog("destroy");
					$("#editobjectivedialog").remove();
				}	
			},"json");
			
		} , 
		
		updateStatus	: function( id, status  )
		{
			$.post("main.php?controller=nationalobjectives&action=updateNationalObj",{
				data	: { status : status } , 
				id 		: id
			}, function( response ) {
				if( response.error )
				{
					$("#message").html( response.text )
				} else {
					Nationalobjectives.get( $("#evaluationyearid").val() );
		                 if(response.updated)
		                 {
			               jsDisplayResult("info", "info", response.text );
		                 } else {
			               jsDisplayResult("ok", "ok", response.text );					
		                 } 	
					$("#editobjectivedialog").dialog("destroy");
					$("#editobjectivedialog").remove();
				}	
			},"json");
			
		} , 
		
		display				: function( nationalobjectives )
		{
			$("#table_nationalobjectives").append($("<tr />").addClass("national")
			  .append($("<td />",{html:nationalobjectives.id}))
			  .append($("<td />",{html:nationalobjectives.shortcode}))
			  .append($("<td />",{html:nationalobjectives.name}))
			  .append($("<td />",{html:nationalobjectives.description}))
			  .append($("<td />",{html:((nationalobjectives.status & 1) == 1 ? "Active" : "Inactive")}))
			  .append($("<td />")
				.append($("<input />",{type:"button", name:"edit_"+nationalobjectives.id, id:"edit_"+nationalobjectives.id, value:"Edit"}))	  
			  )
			)
			
			$("#edit_"+nationalobjectives.id).live("click", function(){
				if( $("#editobjectivedialog").length > 0)
				{				
					$("#editobjectivedialog").remove()
				}
				$("<div />",{id:"editobjectivedialog"})
				 .append($("<table />")
					 .append($("<tr />")
					   .append($("<th />",{html:"Short Code"}))
					   .append($("<td />")
						  .append($("<input />",{type:"text", name:"shortcode", id:"shortcode", value: nationalobjectives.shortcode}))	   
					    )
					 )
					 .append($("<tr />")
					   .append($("<th />",{html:"National KPA"}))
					   .append($("<td />")
						  .append($("<input />",{type:"text", name:"name", id:"name", value:nationalobjectives.name}))	   
					    )
					 )
					 .append($("<tr />")
					   .append($("<th />",{html:"National KPA Description"}))
					   .append($("<td />")
						  .append($("<textarea />",{type:"text", name:"description", id:"description", value:nationalobjectives.description}))	   
					    )
					 )
					 .append($("<tr />")
					   .append($("<td />",{colspan:2})
						 .append($("<p />")
						   .append($("<span />"))
						   .append($("<span />",{id:"message"}))
						 )	   
					   )		 
					 )	 
				 ).dialog({
					 		autoOpen	: true,
					 		modal		: true,
					 		position    : "top",
					 		title		: "Edit national objective #"+nationalobjectives.id,
					 		buttons		: {
					 						"Save Changes"		: function()
					 						{
												if( $("#shortcode").val() == ""){
													$("#message").html("Please enter the short code");
													return false;
												} else if( $("#name").val() == ""){
													$("#message").html("Please enter the national kpa name")
													return false;
												} else if( $("#description").val() == ""){
													$("#message").html("Please enter the national kpa description");
													return false;
												} else {
													Nationalobjectives.updateObjectives( nationalobjectives.id  );			  									
												}					 						
											} , 
					 																		
					 						"Cancel"			: function()
					 						{
					 							$(this).dialog("destroy");					 							
					 						} , 
					 						
					 						"Delete"			: function()
					 						{
					 							Nationalobjectives.deleteKpa( nationalobjectives.id );
					 						} , 
					 						
					 						"Deactivate"		: function()
					 						{
					 							Nationalobjectives.updateStatus( nationalobjectives.id, 0 );					 							
					 						} ,
					 						
					 						"Activate"			: function()
					 						{
					 							Nationalobjectives.updateStatus( nationalobjectives.id, 1 );					 							
					 						}					 
				 						  } , 
				 						  
				 			open		: function( event, ui)
				 			{
						 				var displayDelete = false;
						 				var displayDeactivate = false;
						 				var displayActivate = false; 
						 				//if the status is for the default , then dont display delete
						 				if( (nationalobjectives.status & 1) == 1)
						 				{
						 					displayDeactivate = true;
						 				} else {
						 					displayActivate = true;
						 				}
						 				
						 				if( nationalobjectives.used == false)
						 				{
						 					displayDelete 	  = true;
						 					displayDeactivate = false;
						 					displayActivate = false;
						 				}				 				
						 				
						 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
						 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
						 				var saveButton = buttons[0];
						 				var deleteButton = buttons[2]
						 				var deactivateButton = buttons[3];
						 				var activateButton   = buttons[4];
						 				$(saveButton).css({"color":"#090"});
						 				$(deleteButton).css({"color":"red", display:(displayDelete ? "inline" : "none")});
						 				$(deactivateButton).css({"color":"red", display:(displayDeactivate ? "inline" : "none")});
						 				$(activateButton).css({"color":"#090", display:(displayActivate ? "inline" : "none")}); 
				 			}
				 });
					
				return false;
			});
			
		}
		
		
	
};
