$(function(){
	
	if($("#kpaid").val() == undefined)
	{
	    Kpa.getNationalKpa();
	    Kpa.getIdpObjectives();
	    Kpa.getUsers();
	    Kpa.getWeighting();
	}
	
	$("#updatekpa").click(function(){
	    Kpa.update();
        return false;	
	});
	
	$("#update").click(function(){
	    Kpa.edit();
        return false;	
	});
	
	$("#save").click(function(){
		Kpa.save();		
		return false;
	});
	
	
});

var Kpa 		= {
		
	save			: function()
	{
		var section = "";
		if( $("#section").val() !== undefined && $("#section").val() !== "")
		{
			section = $("#section").val();			
		}
		jsDisplayResult("info", "info", "Saving kpa  . . . <img src='public/images/loaderA32.gif' /> " );
		$.post("main.php?controller=kpa&action=saveKpa",{
			data 		 : $("#addkpaform").serialize() ,
			section		 : section,
			componentid	 : $("#goaltype").val()			
		}, function( response ) {
			if( response.error )
			{
			   jsDisplayResult("error", "error", response.text );
			} else {
			   jsDisplayResult("ok", "ok", response.text );
			}			
		},"json");	
	},
	
	getWeighting			: function()
	{
		$.getJSON("main.php?controller=weighting&action=getAll", {
			data	: { status : 1}
		}, function( weightings ){
			$.each( weightings, function(index, weighting ){
				$("#weighting").append($("<option />",{text:weighting.value+"("+weighting.weight+")", value:weighting.id}))
			});				
		});
	} ,
	
	getNationalKpa			: function()
	{
		$.getJSON("main.php?controller=nationalkpa&action=getAll",{
			evaluationyear 	: $("#evaluationyear").val()			
		}, function( responseData ){
			$.each( responseData, function( index, nationalkpa){
			   $("#nationalkpa").append($("<option />",{value:nationalkpa.id, text:nationalkpa.name}))				
			});						
		});
	} , 
	
	getIdpObjectives		: function()
	{
		$.getJSON("main.php?controller=nationalobjectives&action=getAll",{
			evaluationyear 	: $("#evaluationyear").val()			
		} ,function( responseData ){
			$.each( responseData, function( index, nationalobjectives){
				$("#idpobjectives").append($("<option />",{value:nationalobjectives.id, text:nationalobjectives.name}))				
			});						
		});	
	} , 	
	
	getUsers                : function()
	{
		$.getJSON("main.php?controller=user&action=getAll",{
			evaluationyear 	: $("#evaluationyear").val()			
		} ,function( responseData ){
			$.each( responseData, function( index, user){
				$("#owner").append($("<option />",{value:user.tkid, text:user.username}))				
			});						
		});	
	},
	
	edit			: function()
	{
		jsDisplayResult("info", "info", "Updating kpa  . . . <img src='public/images/loaderA32.gif' /> " );
		$.post("main.php?controller=kpa&action=editKpaData",{
			data 		 : $("#editkpaform").serialize() ,
			id              : $("#kpaid").val(),
			componentid	 : $("#goaltype").val()			
		}, function( response ) {
			if( response.error )
			{
				jsDisplayResult("error", "error", response.text );
			} else {
				jsDisplayResult("ok", "ok", response.text );
			}			
		},"json");	
	} , 
	
	update			: function()
	{
		if($("#response").val() == "")
		{
		     jsDisplayResult("error", "error", "Please enter the response message");
		     $("#response").focus();
		     return false;
		} else if($("#kpastatus :selected").val() == ""){
		     jsDisplayResult("error", "error", "Please select the kpa status ");
		     $("#kpastatus").focus();
		     return false;		
		} else {
               jsDisplayResult("info", "info", "Updating kpa  . . . <img src='public/images/loaderA32.gif' /> " );		
		     $.post("main.php?controller=kpa&action=update",{
			     response 	     : $("#response").val() ,
			     status         : $("#kpastatus :selected").val(),
			     id	          : $("#kpaid").val(),
			     remindon       : $("#remindon").val()			
		     }, function( response ) {
			     if( response.error )
			     {
				     jsDisplayResult("error", "error", response.text );
			     } else {
				     jsDisplayResult("ok", "ok", response.text );
			     }			
		     },"json");
		}
	}  
	
};
