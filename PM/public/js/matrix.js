$(function(){
	
    $("table").find("th").css({"text-align":"left"})
    	    
    $("select.evaluationfrequency").change(function(e){
        $("select.evaluationfrequency").val($(this).val());     
    });
    	    
	//var categoryId = $("#categorytid").val();
	
	$(".savematrix").click(function(e){	  
         var categoryId = ($(this).attr("name")).substr(12);
   	    Matrix.updateMatrix(categoryId);
	    e.preventDefault();
	});
	
	$(".save").click(function(e){
         var categoryId = ($(this).attr("name")).substr(5);
   	    Matrix.save(categoryId);	
	    e.preventDefault();
	});

     $(".weighting").bind("change", function(e) {
        var totalWeight = 0;   
        var selectedValue = $(this).val();  
        var selectedCategoryClass = ($(this).attr("class")).split(" ")[0];
        var categoryId = selectedCategoryClass.substr(16);
        //console.log( selectedCategoryClass+"  and category id is now "+categoryId );     
        //console.log("This is coming here now . . . ");
        $("."+selectedCategoryClass).each(function(){
          var thisId = this.id;
          var idKey  = thisId.substr(10);
          if($("#usecat_"+idKey).is(":checked"))
          {
             totalWeight = totalWeight + parseInt($(this).val());
          }
        })
        //console.log("Total weight for this category should be "+totalWeight);
        var remaining = 100 - parseInt(totalWeight); 
        //reload the weighting range if the total wight is not 100%
        if( totalWeight < 100)
        {
             $("."+selectedCategoryClass).each(function(){
               var thisId = this.id;
               var idKey  = thisId.substr(10);
               var thisValue = parseInt($(this).val());
               if($("#usecat_"+idKey).is(":checked"))
               {    
                  $(this).empty();  
                  var upperLimit = parseInt(remaining) + thisValue;  
                  for(var i = 1; i <= upperLimit; i++)
                  {
                    $(this).append($("<option />",{text:i, value:i, selected:(thisValue === i ? "selected" : "")}))
                  }
               } else {
                  $(this).empty(); 
                  for(var x = 1; x <= remaining; x++)
                  {
                    $(this).append($("<option />",{text:x, value:x}))
                  }               
               }
             })
             $("#total_"+categoryId).html(totalWeight+"%");
        } else if( totalWeight === 100 ) {
          $("#total_"+categoryId).html(totalWeight+"%").removeClass("ui-state-error").addClass("ui-state-ok");
          //$(".weighting").empty(); 
             $("."+selectedCategoryClass).each(function(){
               var thisId = this.id;
               var idKey  = thisId.substr(10);
               var thisValue = parseInt($(this).val());
               if($("#usecat_"+idKey).is(":checked"))
               {    
                  $(this).empty();  
                  //var upperLimit = parseInt(remaining) + thisValue;  
                  for(var i = 1; i <= 100; i++)
                  {
                    $(this).append($("<option />",{text:i, value:i, selected:(thisValue === i ? "selected" : "")}))
                  }
               } else {
                  $(this).empty(); 
                  $(this).append($("<option />",{text:x, value:x}))
               }
             })
        } else {
          $("#total_"+categoryId).html(totalWeight+"%");
        }
        e.preventDefault();
     });

	/*$("#savechanges_"+compId).live("click",function(){
		Matrix.updateMatrix( compId );
		return false;
	});		
	
	
	

	$(".configurejoblevels_"+compId).live("click", function(){
		var catId = $(this).attr("id");
		Matrix.displayJobLevels( catId , compId);
		return false;
	});
	
	$(".usecategory_"+compId).change(function() {
		var catId = $(this).val();		
		//alert("Configure the jobs levels for this category . . .")
		$("#joblevels_"+catId).attr("disabled", "");	
		return false;
	});
	
	/*$(".componentweight_"+compId).change(function(){
	    var total = 0;	
        /*$(".componentweight_"+compId).each(function(){
            total = parseInt($(this).val()) + parseInt(total);	    
	    });
        if(total < 100)
        {
            $("#totalweighting").html(total+"%").addClass("ui-state").addClass("ui-state-info").css({"padding":"5px"})
        } else {
            $("#totalweighting").html(total+"%").addClass("ui-state").addClass("ui-state-ok").css({"padding":"5px"})
        }      
        
	    return false;
	});	*/
})

var Matrix 		= {
	
	save		: function(categoryId)
	{
		jsDisplayResult("info", "info", "saving performance matrix . . .  <img src='public/images/loaderA32.gif' />");
		$.post("main.php?controller=performancematrix&action=save_matrix", {
			data 		: $("#performancematrixform_"+categoryId).serialize(),
			evaluationyear	: $("#evaluationyear").val(),
			categoryid 	: categoryId
		}, function( response ) {
			if( response.error )
			{
				jsDisplayResult("error", "error", response.text);
			} else {
				jsDisplayResult("ok", "ok", response.text);
			}		
		},"json");
	}  , 
	
	updateMatrix		: function(categoryId)
	{
		jsDisplayResult("info", "info", "updating performance matrix . . .  <img src='public/images/loaderA32.gif' />");
		$.post("main.php?controller=performancematrix&action=update_matrix", {
			data 		: $("#performancematrixform_"+categoryId).serialize(),
			evaluationyear	: $("#evaluationyear").val(),
			categoryid 	: categoryId, 
			id			: $("#matrixid_"+categoryId).val() 	
		}, function( response ) {
			if(response.error)
			{
			  jsDisplayResult("error", "error", response.text);
			} else {
			  if(response.updated)
			  {
			    jsDisplayResult("info", "info", response.text);
			  } else {
			    jsDisplayResult("ok", "ok", response.text);
			  }
			}		
		},"json");
	} ,
	
	displayJobLevels			: function( catID, compId)
	{
	
	    if($("#jobleveldialog_"+catID).length > 0)
	    {
	        $("#jobleveldialog_"+catID).remove();
	    }
	
		$("<div />",{id:"jobleveldialog_"+catID})
		 .append($("<table />")
		   .append($("<tr />")
			  .append($("<th />",{html:"Job Levels"}))
			  .append($("<td />")
				 .append($("<select />",{id:"cat_joblevel", name:"cat_joblevel", multiple:"multiple"}).css({height:"250px"}))
			  )
		    )
		   .append($("<tr />")
			 .append($("<td />",{colspan:"2"})
			   .append($("<p />")
				  .append($("<span />"))
				  .append($("<span />",{id:"message"}))
			    )		 
			 )	   
		   )
		 )
		 .dialog({
			 		autoOpen		: true, 
			 		modal			: true, 
			 		title 			: "Assign Job Levels", 
			 		width			: "500px",
			 		position		: "top",	
			 		height          : 400,
			 		buttons			: {
			 							"Assign"	: function()
			 							{
			 								if( $("#cat_joblevel").val() == null )
			 								{
			 									$("#message").html("Please assign at least one job level");
			 								} else {
			 									Matrix.assign( catID,compId );	
			 								}			
			 							} , 
			 							"Cancel"		: function()
			 							{
			 								//$(this).dialog("destroy");	
			 								$("#jobleveldialog_"+catID).dialog("destroy");
			 								$("#jobleveldialog_"+catID).remove();			 								
			 							}
		 			} ,
		 			open            : function(event, ui)
		 			{
		                $.getJSON("main.php?controller=joblevel&action=getAll", 
		                {
			                data : { status : 1 }
		                }, function( responseData ) {
			                $.each( responseData , function( index, joblevel){
				                if( joblevel.used ==  false)
				                {
					                $("#cat_joblevel").append($("<option />",{text:(joblevel.title == "" ? "" : joblevel.title+" - ")+joblevel.joblevel, value:joblevel.id}))
				                } else {
				                    $("#cat_joblevel").append($("<option />",{text:(joblevel.title == "" ? "" : joblevel.title+" - ")+joblevel.joblevel, value:joblevel.id, disabled:"disabled"}))
				                } 			
			                })				
		                });	 			    
		 			} ,
		 			close           : function(event, ui)
		 			{
				        $("#jobleveldialog_"+catID).dialog("destroy");
				        $("#jobleveldialog_"+catID).remove();
		 			}
		 			
		 });
	} , 
	
	assign						: function( catID , compId)
	{			
		$.post("main.php?controller=performancecategories&action=assignJobLevels",{
			joblevels 	: $("#cat_joblevel").val(), 
			catId 		: catID,
			componentid : compId,
			evaluation_year : $("#evaluationyear").val()
		}, function( response ){
			if( response.error )
			{
				$("#message").html( response.text );
			} else {
				var jobLevels = $("#cat_joblevel").val()
				if(jobLevels != null)
				{
					var levellst = "<ul id='"+catID+"_levellist'>";
					$.each( jobLevels, function( i, val){
						levellst += "<li>"+$("select#cat_joblevel option[value="+val+"]").text()+"</li>";
					});
					levellst += "</ul>";
					var catId = catID.replace("joblevels_", "");
					$("#td_"+catId).html( levellst );
				}
				$("#"+catID).attr("disabled", "disabled");
				jsDisplayResult("ok", "ok", response.text);
				$("#jobleveldialog_"+catID).dialog("destroy");
				$("#jobleveldialog_"+catID).remove();
			}	
		},"json");
		
	} 
	
}
