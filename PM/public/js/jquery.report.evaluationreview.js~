$.widget("ui.evaluationreview", {

    options         : {
     evaluationyear      : 0,
     evaluationperiod    : 0,
     department         : 0,
     subdirectorate      : 0,    
     employee            : 0,
     orderby             : 0,
     order               : 0,
     autoLoad            : false
    } ,
    
    _init           : function()
    {
       if(this.options.autoLoad)
       {
          this._get();
       }
    } ,
    
    _create         : function()
    {
       var self = this;
       $(self.element).append($("<table />",{width:"100%"}).addClass("noborder")
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />")
               .append($("<tr />")
                 .append($("<th />",{html:"Evaluation year:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationyear", id:"evaluationyear"}))
                 )
                 .append($("<th />",{html:"Evaluation period:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationperiod", id:"evaluationperiod"}))             
                 )            
               )
               .append($("<tr />").addClass("filters").css({"display":"none"})
                 .append($("<th />",{html:"Department:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"department", id:"department"}))
                 )
               )
               .append($("<tr />").addClass("filters").css({"display":"none"})
                 .append($("<th />",{html:"Employee:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"employee", id:"employee"}))
                 )
               )  
               .append($("<tr />",{id:"message"})
                 .append($("<td />",{colspan:"4"})
                   .append($("<p />",{html:"Please select the evaluation year and evaluation period"})
                      .addClass("ui-state-info")
                      .css({padding:"5px"})
                    )
                 )
               )                                 
             )             
           )           
         )
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />",{width:"100%", id:"evaluation_matrix_table"}))
           )
         )
       )
       
       self._loadEvaluationYears();
       self._loadDirectorates();
       
       $("#department").on('change', function(){
         self._loadUsers($(this).val());
         self.options.department = $(this).val();
         self._get();
       })
       
       $("#evaluationyear").on('change', function(){
          self.options.evaluationyear = $(this).val();
          self._loadEvaluationPeriods($(this).val());       
       });
       
       $("#evaluationperiod").on('change', function(e){
         $(".filters").show();
         $("#message").hide();
         self.options.evaluationperiod = $(this).val();
         self._loadUsers();
         self._get();
         e.preventDefault();
       });
       
       $("#employee").on('change', function(e){
         self.options.employee = $(this).val();
         self._get();
         e.preventDefault();
       })

    } ,

	_loadEvaluationYears		: function()
	{
	     $("#evaluationyear").append($("<option />",{value:"", text:"--select evaluation year--"}))
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
		   $.each( responseData, function( index, evalYear) {
			 $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))
		   });								
		},"json");
	} ,
	
	_loadEvaluationPeriods      : function(evaluationyear)
	{
	    $("#evaluationperiod").empty();
	    $("#evaluationperiod").append($("<option />",{value:"", text:"--select evaluation period--"}))
	    $.post("main.php?controller=evaluationperiod&action=get_all", {evaluationyear : evaluationyear},
		 function( responseData ){
		  var period_int = 1;
		  $.each(responseData['periods'][1], function(index, period) {
		    text_str = period.start_date+" -- "+period.end_date+" ("+period.name+" "+period_int+" )";
		    period_int++;
		    $("#evaluationperiod").append($("<option />",{value:period.id, text:text_str}))
		  });								
	    },"json");
	} ,
	
	_loadUsers            : function(departmentid)
	{
	     var self = this;
	     self.options.employee = "";
	     $("#employee").empty();
	     $("#employee").append($("<option />",{value:"", text:"All"}))
		$.post("main.php?controller=user&action=getAll",
		{
		   departmentid : departmentid
		}, function(users) {	
	        if(!$.isEmptyObject(users))
	        {
	           $.each(users, function(index, user){
	              $("#employee").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}))
	          });
	        }
		},"json"); 
		
	} ,
	
	_loadDirectorates       : function()
	{
	    $("#department").empty();
	    $("#department").append($("<option />",{value:"", text:"All"}))
	    $.post("main.php?controller=directorate&action=get_all_departments",function(directorates){	
	        if(!$.isEmptyObject(directorates))
	        {
	           $.each(directorates, function(index, directorate){
	              $("#department").append($("<option />",{value:directorate.id, text:directorate.name}))
	          });
	        }
	    },"json");
	} ,
	
	_loadSubDirectorates     : function(directorate)
	{
	    $("#subdirectorate").empty();
	     $("#subdirectorate").append($("<option />",{value:"", text:"All"}))
		$.post("main.php?controller=directorate&action=get_all_subdirectorate",
		  {directorate:directorate}, function(subdirectorates){	
	        if(!$.isEmptyObject(subdirectorates))
	        {
	           $.each(subdirectorates, function(index, subdirectorate){
	              $("#subdirectorate").append($("<option />",{value:subdirectorate.id, text:subdirectorate.name}))
	          });
	        }
		},"json");
	},
	
    _get            : function()
    {
       var self = this;
	  $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading ... <img src='public/images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"300px", left:"300px", border:"0px solid #FFFFF", padding:"5px"})
	  )         
       $.getJSON("main.php?controller=performancematrix&action=get_matrix_report", {
            evaluationyear     : self.options.evaluationyear,
            evaluationperiod   : self.options.evaluationperiod,
            department         : self.options.department,
            employee           : self.options.employee
       }, function(matrixReport) {
          $("#evaluation_matrix_table").html("")
          if($.isEmptyObject(matrixReport))
          {
             $("#evaluation_matrix_table").append("<tbody><tr><td colspan='6'>No records match the selected filters</td></tr></tbody>");
          } else {
            self._display(matrixReport);
          }
          $("#loadingDiv").remove();
       }); 
    },
    
    
    _display          : function(reportData)
    {
      var self = this;
      var html = [];
       html.push("<thead>");
       html.push("<tr>");
        html.push("<td colspan='16'><h2>Evaluation Review Report as at "+reportData['report_date']+"</h2></td>");
       html.push("</tr>");                    
          html.push("<tr>");
           html.push("<th>Employee</th>");
           html.push("<th>Manager</th>");
           html.push("<th>Self Evaluation</th>");
           html.push("<th>Manager Evaluation</th>");
           html.push("<th>Joint Evaluation</th>");
           html.push("<th>Review Evaluation</th>");
          html.push("</tr>");     
       html.push("</thead>");      
       var report = reportData['usermatrix'];
       var len    = report.length;
      for(var i = 0; i < len; i++)
      {
          if(report[i].hasOwnProperty('component'))
          {
             html.push("<tr>");
               html.push("<td>"+report[i]['user']['user']+"</td>");
               html.push("<td>"+report[i]['user']['manager']+"</td>");
               html.push("<td style='text-align:right;'><b>"+report[i]['self']+"</b></td>");
               html.push("<td style='text-align:right;'><b>"+report[i]['manager']+"</b></td>");
               html.push("<td style='text-align:right;'><b>"+report[i]['joint']+"</b></td>");
               html.push("<td style='text-align:right;'><b>"+report[i]['review']+"</b></td>");
             html.push("</tr>");   
          } else {
             html.push("<tr>");
               html.push("<td>"+report[i]['user']['user']+"</td>");
               html.push("<td>"+report[i]['user']['manager']+"</td>");
               html.push("<td style='text-align:right;'><b>0</b></td>");
               html.push("<td style='text-align:right;'><b>0</b></td>");
               html.push("<td style='text-align:right;'><b>0</b></td>");
               html.push("<td style='text-align:right;'><b>0</b></td>");
             html.push("</tr>");               
          }
      }
      $("#evaluation_matrix_table").append("<tbody>"+html.join('')+"</tbody>");
    }


});
