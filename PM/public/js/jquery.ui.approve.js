$.widget("ui.approve", {

     options   : {
          user      : "",     
          tableRef  : "",
          headers   : {}
     } ,
     
     _init     : function()
     {
        this._get();
     } ,
     
     _create    : function()
     {
        var self = this;
        $(self.element).append($("<table />",{width:"100%"})
               .append($("<tr />")
                 .append(($("<td />")
                   .append($("<h4 />",{html:"Actions Awaiting Approval"}))
                 )
               )
               .append(($("<td />")
                   .append($("<h4 />",{html:"Actions Approved"}))
                 )
               )          
             )
             .append($("<tr />")
               .append(($("<td />", {width:"50%"})
                   .append($("<table />",{id:"table_awaiting_approval", width:"100%"}))
                 )
               )
               .append(($("<td />", {width:"50%"})
                   .append($("<table />",{id:"table_approved", width:"100%"}))
                 )
               )
             )    
        )            
     } ,
     
     _get       : function()
     {
       var self = this;
       $.getJSON("main.php?controller=action&action=approvalActions", {
          user : self.options.user
       }, function(actions){
       
          $("#table_awaiting_approval").html("");
          $("#table_approved").html("");
          self.options.headers = actions.headers;
          self._headers("table_awaiting_approval", true);
          if(!$.isEmptyObject(actions.awaiting))
          {

            if($.isEmptyObject(actions.awaiting.data))
            {
               $("#table_awaiting_approval").append($("<tr />")
                 .append($("<td />",{colspan:"7", html:"There are no actions awaiting approval"}))
               );
            } else {
              self._display(actions.awaiting.data, "table_awaiting_approval");
            }
          }
          self._headers("table_approved", false);          
          if(!$.isEmptyObject(actions.approved))
          {
             //console.log( actions.approved )
            if($.isEmptyObject(actions.approved.data))
            {
               $("#table_approved").append($("<tr />")
                 .append($("<td />",{colspan:"6", html:"There are no actions approved"}))
               );
            } else {
              self._display(actions.approved.data, "table_approved");
            }             
          }
         
       
       })
     } ,
     
     _display    : function(actions, table)
     {
       var self = this;
       $.each(actions, function(index, action){
         var tr = $("<tr />");
         $.each(action, function(key, val){
           tr.append($("<td />",{html:val}))          
         });
          tr.append($("<td />").css({display:(table == "table_approved" ? "none" : "table-cell")})
              .append($("<input />",{type:"button", name:"approve_"+index, id:"approve_"+index, value:"Approve"})
                .click(function(e){
                  self._approveAction(index);
                  e.preventDefault();
                })
              )
          )          
         $("#"+table).append(tr);
       })   
     } ,
     
     _headers     : function(table, extra)
     {
        var self = this;
        var tr = $("<tr />");
        $.each(self.options.headers, function(index, head){
          tr.append($("<th />",{html:head}))
        });
         tr.append($("<th />",{html:"&nbsp;"}).css({display:(extra ? "table-cell" : "none")}))
        
        $("#"+table).append(tr);
     } ,
     
     _approveAction        : function(actionId)
     {
        var self = this;
        if($("#approve_dialog").length > 0)
        {
          $("#approve_dialog").remove();
        }
        
        $("<div />",{id:"approve_dialog"}).append($("<table />")
          .append($("<tr />")
            .append($("<th />",{html:"Response:"}))
            .append($("<td />")
              .append($("<textarea />",{id:"response", name:"response", cols:"50", rows:"8"}))
            )
          )
          .append($("<tr />")
            .append($("<th />",{html:"Sign Off:"}))
            .append($("<td />")
              .append($("<select />",{id:"signoff", name:"signoff"})
                .append($("<option />",{value:"0", text:"No"}))
                .append($("<option />",{value:"1", text:"Yes"}))
              )
            )
          ) 
          .append($("<tr />")
            .append($("<td />",{colspan:"2"})
              .append($("<p />",{id:"message"}))
            )
          )                    
        ).dialog({
             modal    : true,
             position : "top",
             width    : 500,
             autoOpen : true,
             title    : "Approve/Decline",
             buttons  : {
                          "Approve"     : function()
                          {
                             $("#message").addClass("ui-state-info").css({"padding":"5px"}).html("approving action ... ");
                             $.getJSON("main.php?controller=action&action=approve",{
                              id        : actionId, 
                              response  : $("#response").val(),
                              sign_off  : $("#signoff :selected").val()
                             }, function(response){
                                 if(response.error)
                                 {						 										
                                   $("#message").addClass("ui-state-error").html(response.text);
                                 } else {
                                    jsDisplayResult("ok", "ok", response.text);
                                    $("#approve_dialog").dialog("destroy");
                                    $("#approve_dialog").remove();                                  
                                    self._get();   
                                 }	                                  
                             });
                          } ,
                          
                          "Cancel"       : function()
                          {
                            $("#approve_dialog").dialog("destroy");
                            $("#approve_dialog").remove(); 
                          } ,
                          
                          "Decline"     : function()
                          {
                             if($("#response").val() == "")
                             {
                                $("#message").addClass("ui-state-error").css({"padding":"5px"}).html("Please enter the response");
                             } else {                            
                                 $("#message").addClass("ui-state-info").css({"padding":"5px"}).html("declining action ... ");
                                 $.post("main.php?controller=action&action=decline",{
                                      id        : actionId, 
                                      response  : $("#response").val(),
                                      sign_off  : $("#signoff :selected").val()
                                 }, function(response){
                                     if(response.error)
                                     {						 										
                                       $("#message").addClass("ui-state-error").css({"padding":"5px"}).hmtl(response.text);
                                     } else {
                                        $("#approve_dialog").dialog("destroy");
                                        $("#approve_dialog").remove();                                   
                                        self._get();
                                        jsDisplayResult("ok", "ok", response.text);                                        
                                     }	                                  
                                 },"json");  
                             }
                          }
             } ,
             close    : function(event, ui)
             {
                $("#approve_dialog").dialog("destroy");
                $("#approve_dialog").remove(); 
             } ,
             open     : function(event, ui)
             { 
                var dialog        = $(event.target).parents(".ui-dialog.ui-widget");
                var buttons       = dialog.find(".ui-dialog-buttonpane").find("button");	
                var approveButton = buttons[0];
                var declineButton = buttons[2];
                $(approveButton).css({"color":"#090"});
                $(declineButton).css({"color":"red"});
                $(this).css("height", "auto")
             }
        })
     }



});
