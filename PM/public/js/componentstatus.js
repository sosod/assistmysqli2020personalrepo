$(function(){
	
	ComponentStatus.get();
	
	$("#addnew").click(function(){
		
		if( $("#addnewdialog").length > 0)
		{
			$("#addnewdialog").remove();
		}
		
		$("<div />",{id:"addnewdialog"})
		 .append($("<table />")
		   .append($("<tr />")
			  .append($("<th />",{html:"Status Name:"}))
			  .append($("<td />")
				  .append($("<input />",{type:"text", name:"name", id:"name", value:""}))	  
			  )
		   )
		   .append($("<tr />")
			  .append($("<th />",{html:"Client Terminology:"}))
			  .append($("<td />")
				 .append($("<input />",{type:"text", name:"client_terminology", id:"client_terminology", value:""}))	  
			  )
		   )
		   .append($("<tr />")
			 .append($("<th />",{html:"Colour"}))
			 .append($("<td />")
			   .append($("<input />",{type:"text", id:"color", name:"color", value:""}))		 
			 )
		   )
		   .append($("<tr />")
			 .append($("<td />",{colspan:"2"})
			   .append($("<p />")
				  .append($("<span />"))
				  .append($("<span />",{id:"message"}))
			   )		 
			 )	   
		   )
		 )
		 .dialog({
			 		autoOpen		: true, 
			 		modal			: true, 
			 		width			: "500px",
			 		position		: "top",
			 		title			: "Add New Component Status",
			 		buttons			: {
			 							"Save"		: function()
			 							{
			 								if( $("#name").val() == "")
			 								{
			 									$("#message").html("Please enter the component name status");
			 									return false;
			 								} else if( $("#client_terminology").val() == "") {
			 									$("#message").html("Please enter the client terminology");
			 									return false;
			 								} else {
			 									ComponentStatus.save();			 									
			 								}
			 							} , 
			 							
			 							"Cancel"		: function()
			 							{
			 								$(this).dialog("destroy");			 								
			 							}
		 			} , 
		 			open			: function(event, ui)
		 			{
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		 			}
			 
		 });
		
		 var myPicker = new jscolor.color(document.getElementById('color'), {})
		 myPicker.fromString('99FF33')  // now you can access API via 'myPicker' variable
		
	});
	
})

var ComponentStatus		= {
	
	get			: function()
	{
		$(".compstatus").remove()
		$.getJSON("main.php?controller=componentstatus&action=getAll", function( responseData ){
			$.each( responseData, function( index, status){
				ComponentStatus.display( status );				
			});			
		})
	} , 
	
	save		: function()
	{
		$.post("main.php?controller=componentstatus&action=saveStatus",{
			data	: {
						name	: $("#name").val(),
						client_terminology : $("#client_terminology").val(),
						color	: $("#color").val()			
			}			
		}, function( response ){
			if( response.error )
			{
				$("#message").html( response.text )
			} else {
				ComponentStatus.get();
				jsDisplayResult("ok", "ok", response.text );
				$("#addnewdialog").dialog("destroy");
				$("#addnewdialog").remove();	
			}		
		},"json");		
	} , 
	
	
	display		: function( status )
	{
		$("#table_componentstatus").append($("<tr />").addClass("compstatus")
			.append($("<td />",{html:status.id}))
		   .append($("<td />",{html:status.name}))
		   .append($("<td />",{html:status.client_terminology}))
		   .append($("<td />")
			  .append($("<span />",{html:status.color}).css({"background-color":"#"+status.color, padding:"5px"}))	   
		   )
		   .append($("<td />",{html:((status.status & 1) == 1 ? "Active" : "Inactive")}))
		   .append($("<td />")
			 .append($("<input />",{type:"button", name:"edit_"+status.id, id:"edit_"+status.id, value:"Edit"}))	   
		   )
		)
		
		$("#edit_"+status.id).live("click", function(){
			
				if( $("#editdialog").length > 0)
				{
					$("#editdialog").remove();
				}
				
				$("<div />",{id:"editdialog"})
				 .append($("<table />")
				   .append($("<tr />")
					  .append($("<th />",{html:"Status Name:"}))
					  .append($("<td />")
						  .append($("<input />",{type:"text", name:"name", id:"name", value:status.name, disabled:"disabled"}))	  
					  )
				   )
				   .append($("<tr />")
					  .append($("<th />",{html:"Client Terminology:"}))
					  .append($("<td />")
						 .append($("<input />",{type:"text", name:"client_terminology", id:"client_terminology", value:status.client_terminology}))	  
					  )
				   )
				   .append($("<tr />")
					 .append($("<th />",{html:"Colour"}))
					 .append($("<td />")
					   .append($("<input />",{type:"text", id:"color", name:"color", value:status.color}))		 
					 )
				   )
				   .append($("<tr />")
					 .append($("<td />",{colspan:"2"})
					   .append($("<p />")
						  .append($("<span />"))
						  .append($("<span />",{id:"message"}))
					   )		 
					 )	   
				   )
				 )
				 .dialog({
					 		autoOpen		: true, 
					 		modal			: true, 
					 		width			: "500px",
					 		position		: "top",
					 		title			: "Update Component Status",
					 		buttons			: {
					 							"Update"	: function()
					 							{
					 								if( $("#name").val() == "")
					 								{
					 									$("#message").html("Please enter the component name status");
					 									return false;
					 								} else if( $("#client_terminology").val() == "") {
					 									$("#message").html("Please enter the client terminology");
					 									return false;
					 								} else {
					 									ComponentStatus.update( status.id );			 									
					 								}
					 							} , 
					 							
					 							"Cancel"		: function()
					 							{
					 								$(this).dialog("destroy");			 								
					 							} , 
					 							
					 							"Delete"		: function()
					 							{
					 								ComponentStatus.deleteStatus( status.id );					 								
					 							} ,
					 							
					 							"Deactivate"	: function()
					 							{
					 								ComponentStatus.updateStatus(status.id, 0);					 								
					 							} , 
					 							
					 							"Activate"		: function()
					 							{
					 								ComponentStatus.updateStatus( status.id, 1);					 										
					 							}
				 			} , 
				 			open			: function(event, ui)
				 			{
				 				var displayDelete = false;
				 				var displayDeactivate = false;
				 				var displayActivate = false; 
				 				//if the status is for the default , then dont display delete
				 				if( (status.status & 1) == 1)
				 				{
				 					displayDeactivate = true;
				 				} else {
				 					displayActivate = true;
				 				}
				 				
				 				if( status.used == false)
				 				{
				 					displayDelete 	  = true;
				 					displayDeactivate = false;
				 				}
				 				
				 				if( (status.status & 4) == 4 )
				 				{
				 					displayDeactivate = false;
				 					displayDelete = false;
				 					displayActivate = false;
				 				}
				 				
				 				
				 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
				 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
				 				var saveButton = buttons[0];
				 				var deleteButton = buttons[2]
				 				var deactivateButton = buttons[3];
				 				var activateButton   = buttons[4];
				 				$(saveButton).css({"color":"#090"});
				 				$(deleteButton).css({"color":"red", display:(displayDelete ? "inline" : "none")});
				 				$(deactivateButton).css({"color":"red", display:(displayDeactivate ? "inline" : "none")});
				 				$(activateButton).css({"color":"#090", display:(displayActivate ? "inline" : "none")});
				 			}
					 
				 });
				
				 var myPicker = new jscolor.color(document.getElementById('color'), {})
				 myPicker.fromString( status.color )  // now you can access API via 'myPicker' variable
				
			return false;
		});
		
	} , 
	
	update			: function( id )
	{
		$.post("main.php?controller=componentstatus&action=updateStatus",{
			data	: {
						name	: $("#name").val(),
						client_terminology : $("#client_terminology").val(),
						color	: $("#color").val()			
			} ,
			id 		: id
		}, function( response ){
			if( response.error )
			{
				$("#message").html( response.text )
			} else {
			  ComponentStatus.get();
		       if(response.updated)
		       {
			     jsDisplayResult("info", "info", response.text );
		       } else {
			     jsDisplayResult("ok", "ok", response.text );					
		       } 	
			  $("#editdialog").dialog("destroy");
			  $("#editdialog").remove();	
			}		
		},"json");
	} , 
	
	updateStatus	: function( id, status)
	{
		$.post("main.php?controller=componentstatus&action=updateStatus",{
			data	: {
						status	: status
			}, 
			id		: id
		}, function( response ){
			if( response.error )
			{
				$("#message").html( response.text )
			} else {				
				ComponentStatus.get();
		       if(response.updated)
		       {
			     jsDisplayResult("info", "info", response.text );
		       } else {
			     jsDisplayResult("ok", "ok", response.text );					
		       } 	
				$("#editdialog").dialog("destroy");
				$("#editdialog").remove();	
			}		
		},"json");
	} , 
	
	deleteStatus	: function( id )
	{
		if( confirm("Are you sure you want to delete this status") )
		{
			ComponentStatus.updateStatus(id, 2);			
		}
		
	}
	
	
};
