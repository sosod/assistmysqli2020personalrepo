$.widget("ui.evaluation_period", {

     options        : function()
     {
     
     
     } ,
     
     __init         : function()
     {
         this._get();    
              
     } ,
     
     __create       : function()
     {
        var self = this;
        $(self.element).append($("<table />")
          .append($("<tr />")
            .append($("<th />",{html:"Ref"}))
            .append($("<th />",{html:"Frequency Name"}))
            .append($("<th />",{html:"Period Start Date"}))
            .append($("<th />",{html:"Period End Date"}))
            .append($("<th />",{html:"Period Open From"}))
            .append($("<th />",{html:"Period Open To"}))
            .append($("<th />",{html:"Status"}))
            .append($("<th />",{html:"&nbsp;"}))
          )
        )
     } ,
     
     _get           : function()
     {
        var self = this;
        $.getJSON("main.php?controller=evaluationperiodcontroller&action=get_all", function(evaluationPeriods) {
          console.log( evaluationPeriods );
        });
     } ,
     
     _display         : function(evaluationPeriods)
     {
          
     
     
     } ,
     
     _save_changes       : function()
     {
     
     
     } 

});
