$.widget("ui.evaluationperiod", {

    options         : {
     evaluationyear      : 0,
     evaluationperiod    : 0,
     directorate         : 0,
     subdirectorate      : 0,    
     user                : 0,
     orderby             : 0,
     order               : 0,
     autoLoad            : false
    } ,
    
    _init           : function()
    {
       if(this.options.autoLoad)
       {
          this._get();
       }
    } ,
    
    _create         : function()
    {
       var self = this;
       $(self.element).append($("<table />",{width:"100%"}).addClass("noborder")
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />")
               .append($("<tr />")
                 .append($("<th />",{html:"Evaluation year:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationyear", id:"evaluationyear"}))
                 )
                 .append($("<th />",{html:"Evaluation period:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationperiod", id:"evaluationperiod"}))             
                 )            
               )
               .append($("<tr />")
                 .append($("<th />",{html:"User:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"user", id:"user"}))
                 )
               )                                
             )             
           )           
         )
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />",{id:"evaluation_period_table"}))
           )
         )
       )
       
       self._loadEvaluationYears();
       self._loadUsers();
       
       $("#evaluationyear").on('change', function(){
          self.options.evaluationyear = $(this).val();
          self._loadEvaluationPeriods($(this).val());      
       });
       
       $("#evaluationperiod").on('change', function(e){
         self.options.evaluationperiod = $(this).val();
         e.preventDefault();
       });
       
       $("#user").on('change', function(e){
         self.options.user = $(this).val();
         self._get();
         e.preventDefault();
       })

    } ,

	_loadEvaluationYears		: function()
	{
	     $("#evaluationyear").append($("<option />",{value:"", text:"--select evaluation year--"}))
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
		   $.each( responseData, function( index, evalYear) {
			 $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))
		   });								
		},"json");
	} ,
	
	_loadEvaluationPeriods      : function(evaluationyear)
	{
	    $("#evaluationperiod").empty();
	    $("#evaluationperiod").append($("<option />",{value:"", text:"--select evaluation period--"}))
	    $.post("main.php?controller=evaluationperiod&action=get_all", {evaluationyear : evaluationyear},
		 function( responseData ){
		  var period_int = 1;
		  $.each(responseData['periods'][1], function(index, period) {
		    text_str = period.start_date+" -- "+period.end_date+" ("+period.name+" "+period_int+" )";
		    period_int++;
		    $("#evaluationperiod").append($("<option />",{value:period.id, text:text_str}))
		  });								
	    },"json");
	    
	} ,
	
	_loadUsers            : function(departmentid)
	{
	    var self = this;
	    self.options.employee = "";
	    $("#user").empty();
	    $("#user").append($("<option />",{value:"", text:"--select user--"}))
		$.post("main.php?controller=user&action=getAll", function(users) {	
	        if(!$.isEmptyObject(users))
	        {
	           $.each(users, function(index, user){
	              $("#user").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}))
	          });
	        }
		},"json"); 
		
	} ,
		
    _get            : function()
    {
       var self = this;
	  $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading ... <img src='public/images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"300px", left:"300px", border:"0px solid #FFFFF", padding:"5px"})
	  )         
       $.getJSON("main.php?controller=evaluationperiod&action=get_user_evaluation_period", {
            evaluationyear     : self.options.evaluationyear,
            evaluationperiod   : self.options.evaluationperiod,
            employee           : self.options.user
       }, function(evaluationPeriod) {
          $("#evaluation_period_table").html("");
          if($.isEmptyObject(evaluationPeriod))
          {
             $("#evaluation_period_table").append("<tbody><tr><td colspan='6'>No records match the selected filters</td></tr></tbody>");
          } else {
            self._display(evaluationPeriod);
          }
          $("#loadingDiv").remove();
       }); 
    },
    
    
    _display          : function(evaluationPeriod)
    {
      var self = this;
      var html = [];
       html.push("<thead>");
       html.push("<tr>");
        html.push("<th>Ref</td>");
        html.push("<th>Start and End Date Of the Evaluation Period</th>");
        html.push("<th>Open From</th>");
        html.push("<th>Open To</th>");
        html.push("<th></th>");
        html.push("<th></th>");
       html.push("</tr>");                    
       html.push("</thead>");
       html.push("<tbody>");
       html.push("<tr>");
        html.push("<td>"+evaluationPeriod['evaluation_period_id']+"</td>");        
        html.push("<td>"+evaluationPeriod['start']+" -to- "+evaluationPeriod['end']+"</td>");
        html.push("<td><input type='text' name='open_from' id='open_from' value='"+evaluationPeriod['open']+"' class='datepicker' readonly='readonly' /></td>");
        html.push("<td><input type='text' name='open_to' id='open_to' value='"+evaluationPeriod['close']+"' class='datepicker' readonly='readonly' /></td>");
        if(evaluationPeriod.comment == undefined)
        {
           html.push("<td></td>");
        } else {
           html.push("<td>"+evaluationPeriod.comment+"</td>");        
        }        
        html.push("<td><input type='button' name='save' id='save' value='Save Changes' /></td>");
       html.push("</tr>");                    
       html.push("</tbody>");       
      $("#evaluation_period_table").append(html.join(''));
	    $(".datepicker").datepicker({
		    showOn		  : "both",
		    buttonImage 	  : "/library/jquery/css/calendar.gif",
		    buttonImageOnly  : true,
		    changeMonth	  : true,
		    changeYear	  : true,
		    dateFormat 	  : "dd-M-yy",
		    altField		  : "#startDate",
		    altFormat		  : 'd_m_yy'
	    });	      
        
        $("#save").on('click', function(e){
           $("<div />",{id:"evaluationperiod_dialog"}).append($("<table />", {width:"100%"})
             .append($("<tr />")
               .append($("<th />",{html:"Comment"}))
               .append($("<td />")
                  .append($("<textarea />",{name:"comment", id:"comment", cols:50, rows:8}))
               )
             )
             .append($("<tr />")
               .append($("<td />", {colspan:2})
                 .append($("<p />",{id:"message"}).css({"padding":"5px"}))
               )
             )
           ).dialog({
                     autoOpen   : true,
                     title      : "Employee Specific Evaluation period",
                     modal      : true,
                     position   : "top",
                     width      : 500,
                     buttons    : {
                                    "Save   " : function()
                                    {
	                                    $("#message").addClass("ui-state-info")
	                                                 .html("updating ...<img src='public/images/loaderA32.gif' />" );
	                                    $.post("main.php?controller=evaluationperiod&action=update_user_specific", {
	                                      open_from       : $("#open_from").val(),
	                                      open_to         : $("#open_to").val(),
	                                      comment         : $("#comment").val(),
	                                      evaluationyear  : self.options.evaluationyear,
	                                      evaluation_period_id : self.options.evaluationperiod,
	                                      user_id         : $("#user :selected").val(),
	                                      savenew         : evaluationPeriod.new_period
	                                    }, function(response) {
		                                   if(response.error)
		                                   {
			                                 $("#message").removeClass("ui-state-info").addClass("ui-state-error")
			                                              .html( response.text )
		                                   } else {
		                                      $("#evaluationperiod_dialog").remove();
			                                  jsDisplayResult("ok", "ok", response.text );	
			                                  //$(this).dialog().dialog('destroy');
			                                  self._get();
		                                   }			
	                                    },"json"); 
                                    } ,
                                    
                                    "Cancel"   : function()
                                    {
                                        $(this).dialog('destroy');
                                    } 
                     } ,
		 			 open			: function(event, ui)
		 			 {
		 				var dialog     = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons    = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		 			 } ,
		 			 close          : function(event, ui)
		 			 {
		 			    $("#evaluationperiod_dialog").remove();
		 			 }
           })
        });
          
     } ,
     
     _save_evaluation_period       : function()
     {
        
     }


});
