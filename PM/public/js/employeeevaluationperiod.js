$(function(){

    EmployeeSpecific.getEvaluationyears();
    EmployeeSpecific.getUser();
     
    var evaluationyear = "";
    var user           = user;
	$("#evaluationyear").change(function(e){
		var evaluationyear = $(this).val();
		$("#evaluationyearid").val( evaluationyear )
		if(evaluationyear == ""  || evaluationyear == undefined)
		{
		     $(".cre").show();
			$("#employeespecific").fadeOut();
		} else {
		     $(".cre").hide();
			$("#employeespecific").fadeIn();
		}
		EmployeeSpecific.set('evaluationyear', evaluationyear);
		EmployeeSpecific.getEmployeeSpecifics();
		e.preventDefault();
	});
	
	$("#user").change(function(e){
	  var user = $(this).val();
	  EmployeeSpecific.set('user', user);
	  EmployeeSpecific.getEmployeeSpecifics();
	  e.preventDefault();
	});
	
	$("#save").click(function(e){
	  EmployeeSpecific.save();
	  e.preventDefault();
	});
});


var EmployeeSpecific = {
     evaluationyear       : "",
     user                 : "",
     set                  : function(key, value)
     {
        var self = this;
        self[key] = value;
     } ,
     
     get                 : function(key)
     {    
          var self = this;
          return self[key];
     } ,  
          
	getEvaluationyears		: function()
	{
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
		  $.each(responseData, function( index, evalYear) {
			$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))	
		  });								
		},"json");
	} ,
	
	getUser            : function(userid)
	{
	    $(".users").remove();
		$.post("main.php?controller=user&action=getAll",function(users){	
	        if(!$.isEmptyObject(users))
	        {
	           $.each(users, function(index, user){
                 if(user.tkid == userid)
                 {
	                $("#user").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname, selected:"selected"}))
	             } else {
	                $("#user").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}))    
	             }
	            });
	        }
		},"json"); 
	} , 	
	
	getEmployeeSpecifics                      : function()
	{
	  var self = this;
	  $(".employeespecific").remove();
	  $.getJSON("main.php?controller=evaluationperiod&action=getUserAll", {evaluationyear:self.evaluationyear, user:self.user},
	   function(response) {
	      if(response.using_user)
	      {
	        self._displayEdit(response, self);
	      } else {
	        self._displayNew(response, self);
	      }
	  });
	} ,

     _displayNew             : function(period, data)
	{
	   $("#table_employeespecific").append($("<tr />").addClass("employeespecific")
	     .append($("<td />",{html:period.id}))
	     .append($("<td />")
	       .append($("<input />",{type:"text", name:"start_date", id:"start_date", value:period.start_date, readonly:"readonly"}).addClass("datepicker")) 
	     )
	     .append($("<td />")
	       .append($("<input />",{type:"text", name:"end_date", id:"end_date", value:period.end_date, readonly:"readonly"}).addClass("datepicker"))
	     )	
	     .append($("<td />",{html:period.comment}))		          
	     .append($("<td />")
	       .append($("<input />",{type:"button", name:"edit_period", id:"edit_period", value:"Save"})
	         .click(function(e){
	            EmployeeSpecific.save(period, data);
	            e.preventDefault();
	         })
	       )
	       .append($("<input />",{type:"hidden", name:"period_id", id:"period_id", value:period.id}))
	     )	     	     
	   )
	     $(".datepicker").datepicker({
		     showOn			: "both",
		     buttonImage 	: "/library/jquery/css/calendar.gif",
		     buttonImageOnly	: true,
		     changeMonth		: true,
		     changeYear		: true,
		     dateFormat 		: "dd-M-yy",
		     altField		: "#startDate",
		     altFormat		: 'd_m_yy'
	     });	
	} ,
	
     _displayEdit             : function(period, data)
	{
	   $("#table_employeespecific").append($("<tr />").addClass("employeespecific")
	     .append($("<td />",{html:period.id}))
	     .append($("<td />")
	       .append($("<input />",{type:"text", name:"start_date", id:"start_date", value:period.start_date, readonly:"readonly"}).addClass("datepicker")) 
	     )
	     .append($("<td />")
	       .append($("<input />",{type:"text", name:"end_date", id:"end_date", value:period.end_date, readonly:"readonly"}).addClass("datepicker"))
	     )	
	     .append($("<td />",{html:period.comment}))		          
	     .append($("<td />")
	       .append($("<input />",{type:"button", name:"edit_period", id:"edit_period", value:"Save Changes"})
	         .click(function(e){
	            EmployeeSpecific.update(period, data);
	            e.preventDefault();
	         })
	       )
	       .append($("<input />",{type:"hidden", name:"period_id", id:"period_id", value:period.id}))
	     )	     	     
	   )
	     $(".datepicker").datepicker({
		     showOn			: "both",
		     buttonImage 	: "/library/jquery/css/calendar.gif",
		     buttonImageOnly	: true,
		     changeMonth		: true,
		     changeYear		: true,
		     dateFormat 		: "dd-M-yy",
		     altField		: "#startDate",
		     altFormat		: 'd_m_yy'
	     });	
	} ,
	
	update                   : function(period, data)
	{
	   var self = this;
	    if($("#updatecreationperiod_dialog").length > 0)
	    {
	      $("#updatecreationperiod_dialog").remove();
	    }
	    $("<div />",{id:"updatecreationperiod_dialog"}).append($("<table />")
	      .append($("<tr />")
	        .append($("<td />")
	          .append($("<textarea />",{cols:"60", rows:"8", id:"comment", name:"comment", text:period.comment}))
	        )
	      )
	      .append($("<tr />")
	        .append($("<td />")
	          .append($("<span />",{id:"message"}))
	        )
	      )	      
	    ).dialog({
	          autoOpen  : true,
	          modal     : true,
	          title     : "Enter Comment",
	          width     : 500,
	          position  : "top",
	          buttons   : {
	                         "Save Changes" : function()
	                         {
	                            $("#message").addClass("ui-state-info").html("updating ..." );
	                            $.post("main.php?controller=creationperiod&action=updateUserSpecific", {
	                              start_date : $("#start_date").val(),
	                              end_date   : $("#end_date").val(),
	                              comment    : $("#comment").val(),
	                              evaluationyear   : data.evaluationyear,
	                              creation_period  : period.id,
	                              user_id          : $("#user :selected").val(),
	                              id               : period.id
	                            }, function(response) {
		                           if(response.error)
		                           {
			                         jsDisplayResult("error", "error", response.text );
		                           } else {
		                              $("#updatecreationperiod_dialog").remove();
			                         jsDisplayResult("ok", "ok", response.text );	
		                           }			
	                            },"json"); 
	                         } ,
	                         "Cancel"       : function()
	                         {
	                          $("#updatecreationperiod_dialog").remove();
	                         }
	          } ,
	          close     : function(event, ui)
	          {
	             $("#updatecreationperiod_dialog").remove();
	          } ,
	          open      : function(event, ui)
	          {

	          }
	    });
	} ,
	
	
	save                   : function(period, data)
	{
	   var self = this;
	    if($("#creationperiod_dialog").length > 0)
	    {
	      $("#creationperiod_dialog").remove();
	    }
	    $("<div />",{id:"creationperiod_dialog"}).append($("<table />")
	      .append($("<tr />")
	        .append($("<td />")
	          .append($("<textarea />",{cols:"60", rows:"8", id:"comment", name:"comment", text:""}))
	        )
	      )
	      .append($("<tr />")
	        .append($("<td />")
	          .append($("<span />",{id:"save_message"}).css({"padding":"5px"}))
	        )
	      )	      
	    ).dialog({
	          autoOpen  : true,
	          modal     : true,
	          title     : "Enter Comment",
	          width     : 500,
	          position  : "top",	          
	          buttons   : {
	                         "Save" : function()
	                         {
	                            $("#save_message").addClass("ui-state-info").html("updating ..." );
	                            $.post("main.php?controller=creationperiod&action=saveUserSpecific", {
	                              start_date : $("#start_date").val(),
	                              end_date   : $("#end_date").val(),
	                              comment    : $("#comment").val(),
	                              evaluationyear   : data.evaluationyear,
	                              creation_period  : period.id,
	                              user_id          : $("#user :selected").val()
	                            }, function(response) {
		                           if(response.error)
		                           {
			                         jsDisplayResult("error", "error", response.text );
		                           } else {
		                              $("#creationperiod_dialog").remove();
			                         jsDisplayResult("ok", "ok", response.text );	
		                           }			
	                            },"json");
	                         } ,
	                         "Cancel"       : function()
	                         {
	                          $("#creationperiod_dialog").remove();
	                         }
	          } ,
	          close     : function(event, ui)
	          {
	             $("#creationperiod_dialog").remove();
	          } ,
	          open      : function(event, ui)
	          {

	          }
	    });
	} 		
};
