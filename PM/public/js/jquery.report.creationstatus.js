$.widget("ui.creationstatus", {

    options         : {
     evaluationyear      : 0,
     evaluationperiod    : 0,
     manager             : 0,
     department          : 0,    
     section             : 0,
     orderby             : 0,
     order               : 0,
     autoLoad            : false
    } ,
    
    _init           : function()
    {
       if(this.options.autoLoad)
       {
          this._get();
       }
    } ,
    
    _create         : function()
    {
       var self = this;
       $(self.element).append($("<table />",{width:"100%"}).addClass("noborder")
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />")
               .append($("<tr />")
                 .append($("<th />",{html:"Evaluation year:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationyear", id:"evaluationyear"}))
                 )           
               )
               .append($("<tr />").addClass("filters").css({display:"none"})
                 .append($("<th />",{html:"Manager:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"manager", id:"manager"}))
                 )
               )                         
             )             
           )           
         )
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />",{width:"100%", id:"creation_status_table"}))
           )
         )
       )
       
       self._loadEvaluationYears();
       $("#evaluationyear").on('change', function(e){
          $(".filters").show();
          self.options.evaluationyear = $(this).val();
          self._loadUsers();
          self._get();
          e.preventDefault();     
       });
       
       $("#manager").on('change', function(e){
         self.options.manager = $(this).val();
         self._get();
         e.preventDefault();
       })

    } ,

	_loadEvaluationYears		: function()
	{
	    $("#evaluationyear").append($("<option />",{value:"", text:"--select evaluation year--"}))
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function(responseData){
		   $.each(responseData, function( index, evalYear) {
			 $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))
		   });								
		},"json");
	} ,
	
	_loadUsers            : function(userid)
	{
	    $(".manager").empty();
	     $("#manager").append($("<option />",{value:"", text:"All"}))
		$.post("main.php?controller=user&action=getAll",function(users){	
	        if(!$.isEmptyObject(users))
	        {
	           $.each(users, function(index, user){
	              $("#manager").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}))
	          });
	        }
		},"json"); 
		
	} ,
	
    _get            : function()
    {
       var self = this;
	  $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading ... <img src='public/images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"300px", left:"300px", border:"0px solid #FFFFF", padding:"5px"})
	  )         
       $.getJSON("main.php?controller=usercomponent&action=get_creation_statuses_report", {
            evaluationyear     : self.options.evaluationyear,
            manager            : self.options.manager
       }, function(creationStatusReport) {
          //console.log("Creation status report");
          //console.log( creationStatusReport );
       
          $("#creation_status_table").html("")
          self._displayHeaders(creationStatusReport);
          if($.isEmptyObject(creationStatusReport))
          {
             $("#creation_status_table").append("<tbody><tr><td colspan='6'>No records match the selected filters</td></tr></tbody>");
          } else {
            self._display(creationStatusReport['data']);
          }
          $("#loadingDiv").remove();
       }); 
    },
    
    
    _display          : function(report)
    {
      var self = this;
      var html = [];
      var len  = report.length;
      for(var i = 0; i < len; i++)
      {
          html.push("<tr>");
           html.push("<td>"+report[i]['user']+"</th>");
           html.push("<td>"+report[i]['manager']+"</th>");               
           html.push("<td>"+(report[i]['created'] === undefined ? "Not Yet Done" : report[i]['created'])+"</td>");
           html.push("<td>"+(report[i]['confirmed'] === undefined ? "Not Yet Done" : report[i]['confirmed'])+"</td>");
           html.push("<td>"+(report[i]['activated'] === undefined ? "Not Yet Done" : report[i]['activated'])+"</td>");
          html.push("</tr>");          
      }
      $("#creation_status_table").append("<tbody>"+html.join('')+"</tbody>");
    } ,
    
    _displayHeaders    : function(report)
    {
       var html = [];
       html.push("<thead>");
       html.push("<tr>");
       html.push("<td colspan='5'><h2>Performance Matrix Creation Status Report for "+report['year']['start']+" - "+report['year']['end']+" as at "+report['report_date']+"<h2></td>");
       html.push("</tr>");
       html.push("<tr>");
        html.push("<th>Employee Name</th>");
        html.push("<th>Manager Name</th>");
        html.push("<th>Created On</th>");
        html.push("<th>Confirmation Date</th>");
        html.push("<th>Activated Date</th>");
       html.push("</tr></thead>");
       $("#creation_status_table").append(html.join(''));
    }



});
