$.widget("ui.evaluationperiod", {

     options        :{
     
     
     } ,
     
     _init         : function()
     {
         this._get();    
     } ,
     
     _create       : function()
     {
        var self = this;
        $(self.element).append($("<table />",{id:"openclose_table"})
          .append($("<tr />")
            .append($("<th />",{html:"Ref"}))
            .append($("<th />",{html:"Frequency Name"}))
            .append($("<th />",{html:"Period Start Date"}))
            .append($("<th />",{html:"Period End Date"}))
            .append($("<th />",{html:"Period Open From"}))
            .append($("<th />",{html:"Period Open To"}))
            .append($("<th />",{html:"Status"}))
            .append($("<th />",{html:"&nbsp;"}))
          )
        )
     } ,
     
     _get           : function()
     {
        var self = this;
        $(".openclose").remove();
        $.getJSON("main.php?controller=evaluationperiod&action=get_all", {
          evaluationyear : $("#evaluationyearid").val()          
        }, function(evaluationPeriods) {
          self._display(evaluationPeriods.frequencies, evaluationPeriods.periods);
        });
     } ,
     
     _display         : function(frequencies, periods)
     {
        var self = this;
        $.each(frequencies, function(freq_id, freq){
          $("#openclose_table").append($("<tr />").addClass('openclose')
            .append($("<td />",{html:freq_id}))
            .append($("<td />",{html:"<b>"+freq['name']+"</b>"}))
            .append($("<td />",{colspan:'6'}))
          )
          if(periods.hasOwnProperty(freq_id))
          {
            $.each(periods[freq_id], function(period_id, period){
               $("#openclose_table").append($("<tr />").addClass('openclose')
                 .append($("<td />",{html:"", colspan:'2'}))
                 .append($("<td />",{html:period.start_date}))
                 .append($("<td />",{html:period.end_date}))
                 .append($("<td />")
                   .append($("<input />",{type:"text", name:"from_"+period_id, id:"from_"+period_id, value:period.open_from})
                     .addClass("datepicker") 
                   )
                 )
                 .append($("<td />")
                    .append($("<input />",{type:"text", name:"to_"+period_id, id:"to_"+period_id, value:period.open_to})
                      .addClass("datepicker")
                    ) 
                 )
                 .append($("<td />",{html:((period.status & 4) == 4 ? "Opened" : "Closed")}))
                 .append($("<td />")
                    .append($("<input />",{type:"button", name:"save_"+period_id, id:"save_"+period_id, value:"Save Changes"})
                       .on('click', function(e){
                         self._save_changes(period_id);
                         e.preventDefault();
                       })
                    ) 
                 )
               )            
            })              
          }
                  
        });
	     $(".datepicker").datepicker({
		     showOn		 : "both",
		     buttonImage 	 : "/library/jquery/css/calendar.gif",
		     buttonImageOnly : true,
		     changeMonth	 : true,
		     changeYear	 : true,
		     dateFormat 	 : "dd-M-yy",
		     altField		 : "#startDate",
		     altFormat		 : 'd_m_yy'
	     });	     
     } ,
     
     _save_changes       : function(period_id)
     {
        var self = this;
        jsDisplayResult("info", "info", "updating . . .");
        $.post("main.php?controller=evaluationperiod&action=open_evaluation_period", {
              open_from  : $("#from_"+period_id).val(),
              open_to    : $("#to_"+period_id).val(),
              id         : period_id
        }, function(response) {
            if(response.error)
            {
               jsDisplayResult("error", "error", response.text);	
            } else {
               if(response.updated)
               {
                  jsDisplayResult("info", "info", response.text );	
               } else {
                  jsDisplayResult("ok", "ok", response.text );	     
                  self._get();
               }
            }	
        },"json")          
     } 

});
