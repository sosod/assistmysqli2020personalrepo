$.widget("assurance.action",{

    options     : {
        start           : 0,
        limit           : 10,
        current         : 1,
        total           : 0,
        autoLoad        : true,
        assurance       : true,
        actionid        : 0, 
        tableRef        : "table_assurance"
    } ,
    
    _init        : function()
    {
        if(this.options.autoLoad)
        {
            this._get();
        }
    
    } ,
    
    _create       : function()
    {
        var self = this;
        $(self.element).append($("<table />",{width:"100%", id:self.options.tableRef}))
    } ,
    
    _get          : function()
    {
        var self = this;
        $.getJSON("main.php?controller=actionassurance&action=get_action_assurance",{
            start       : self.options.start,
            limit       : self.options.limit,
            kpaid       : self.options.kpaid
        },function(kpaData){
            $("#"+self.options.tableRef).html("");
            self._headers();
            self._display(kpaData);
        });
    } , 
    
    _display    : function(kpaData)
    {
        var self = this;
        var html = []
        if($.isEmptyObject(kpaData))
        {        
            html.push("<tr>");
              html.push("<td colspan='7'>There are no Action assurance</td>");
            html.push("</tr>");
        } else {
           console.log( kpaData );
           $.each(kpaData, function(index, kpa){
                html.push("<tr>");
                  html.push("<td>"+kpa.id+"</td>");
                  html.push("<td>"+kpa.system_date+"</td>");
                  html.push("<td>"+kpa.date_tested+"</td>");
                  html.push("<td>"+kpa.response+"</td>");
                  html.push("<td>"+(kpa.sign_off == 1 ? "Yes" : "No")+"</td>");
                  html.push("<td>"+kpa.assurance_by+"</td>");
                  html.push("<td><input type='button' name='edit_"+kpa.id+"' id='edit_"+kpa.id+"' value='Edit' />");
                  html.push("<br /><input type='button' name='delete_"+kpa.id+"' id='delete_"+kpa.id+"' value='Delete' /></td>");
                html.push("</tr>");
                $("#edit_"+kpa.id).live("click", function(e){
                    self._editKpaAssurance(kpa, kpa.id);
                   e.preventDefault();
                });
                
                $("#delete_"+kpa.id).live("click", function(e){
                    if(confirm("Are you sure you want to delete this Action assurance"))
                    {
                       self._deleteKpaAssurance(kpa.id);
                    }
                   e.preventDefault();
                });                
                                
           });
        }
        html.push("<tr>");
          html.push("<td colspan='7'><input type='button' name='add' id='add' value='Add New' /></td>");
        html.push("</tr>");
        $("#add").live("click", function(e){
            self._addKpaAssurance();
           e.preventDefault();
        });

       $("#"+self.options.tableRef).append(html.join(' '));
    } ,
    
    _headers     : function()
    {
        var self = this;
        self._paging();
        var html = [];
        html.push("<tr>");
          html.push("<th>Reference</th>");
          html.push("<th>System Date and time</th>");
          html.push("<th>Date Tested</th>");
          html.push("<th>Response</th>");
          html.push("<th>Sign Off</th>");
          html.push("<th>Assurance By</th>");
          html.push("<th></th>");
        html.push("</tr>");
        $("#"+self.options.tableRef).append(html.join(' '));
    } ,
    
    _paging     : function()
    {
		var self = this;
		var pages;
		if(self.options.total%self.options.limit > 0){
			pages   = Math.ceil(self.options.total/self.options.limit); 				
		} else {
			pages   = Math.floor(self.options.total/self.options.limit); 				
		}
		if( self.options.setupLegislation )
		{
			columns = 8;
		}		
		var html = [];
		html.push("<tr>")
		 html.push("<td colspan='2'>");
		 html.push("<input type='button' name='first' id='first' value='|<' />");
		 html.push("<input type='button' name='previous' id='previous' value='<' />");
		 html.push("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		 html.push("<input type='button' name='next' id='next' value='>' />");
		 html.push("<input type='button' name='last' id='last' value='>|' />");
		 html.push("</td>");
 		 html.push("<td colspan='6'></td>");
		html.push("</tr>");
		$("#"+self.options.tableRef).append( html.join(' ') );

		  if(self.options.current < 2)
		  {
                $("#first").attr('disabled', 'disabled');
                $("#previous").attr('disabled', 'disabled');		     
		  }
		  if((self.options.current == pages || pages == 0))
		  {
                $("#next").attr('disabled', 'disabled');
                $("#last").attr('disabled', 'disabled');		     
		  }		  
		$("#next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
		});
    } ,
    
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getLegislation();
	},	
	_getLast  			: function( rk ) {
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getLegislation();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getLegislation();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getLegislation();				
	}   ,  
    
    _addKpaAssurance        : function()
    {
        var self = this;
        if($("#add_assurance_dialog").length > 0)
        {
          $("#add_assurance_dialog").remove();
        }
        
        $("<div />",{id:"add_assurance_dialog"}).append($("<table />")
            .append($("<tr />")
              .append($("<th />",{html:"Response"}))
              .append($("<td />")
                .append($("<textarea />",{cols:"80", rows:"7", name:"response", id:"response"}))
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Sign Off"}))
              .append($("<td />")
                 .append($("<select />",{name:"signoff", id:"signoff"})
                   .append($("<option />",{value:"1", text:"Yes"}))
                   .append($("<option />",{value:"0", text:"No"}))
                 )                
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Date Tested"}))
              .append($("<td />")
                .append($("<input />",{type:"text", name:"date_tested", id:"date_tested", readonly:"readonly"})
                  .addClass("datepicker")
                )
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Attachment"}))
              .append($("<td />")
                .append($("<input />",{type:"file", name:"attach_kpa", id:"attach_kpa"}))
              )
            )  
            .append($("<tr />")
              .append($("<td />",{colspan:"2"})
                .append($("<p />",{id:"message"}))
              )
            )                                    
        ).dialog({
                    autoOpen    : true,
                    modal       : true,
                    title       : "Add Action Assurance",
                    position    : "top",
                    width       : 'auto',
                    buttons     : {
                                       " Save "     : function()
                                       {
                                           if($("#response").val() == "")
                                           {
                                             $("#message").css({"padding":"5px"}).addClass("ui-state-error").html("Please enter the reponse");
                                           } else {
                                             $("#message").css({"padding":"5px"}).addClass("ui-state-info").html("saving....")
                                             $.post("main.php?controller=actionassurance&action=save", {
                                                response    : $("#response").val(),
                                                sign_off    : $("#signoff").val(),
                                                date_tested : $("#date_tested").val(),
                                                kpi_id      : self.options.kpiid                                           
                                             }, function(response){
                                                if(response.error)
                                                {
                                                  $("#message").css({"padding":"5px"}).addClass("ui-state-error").html(response.text);
                                                } else {
                                                  $("#message").html(response.text);
                                                  $("#add_assurance_dialog").remove();
                                                  self._get();
                                                }                                             
                                             },"json")
                                           }
                                            
                                       } ,
                                       
                                       " Cancel "   : function()
                                       {
                                          $("#add_assurance_dialog").remove();                                    
                                       }
                    } ,
                    close       : function(event, ui)
                    {
                        $("#add_assurance_dialog").remove();
                    } ,
                    open        : function(event, ui)
                    {
		 				var dialog     = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons    = dialog.find(".ui-dialog-buttonpane").find("button");			 				
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		 				
	                    $(".datepicker").datepicker({
		                    showOn		    : "both",
		                    buttonImage 	: "/library/jquery/css/calendar.gif",
		                    buttonImageOnly : true,
		                    changeMonth	    : true,
		                    changeYear	    : true,
		                    dateFormat 	    : "dd-M-yy",
		                    altField		: "#startDate",
		                    altFormat		: 'd_m_yy'
	                    });			 				
                    }
                    
        }); 
    } ,
    
    _editKpaAssurance       : function(kpa, id)
    {
        var self = this;
        if($("#assurance_dialog_"+id).length > 0)
        {
          $("#assurance_dialog_"+id).remove();
        }
        
        $("<div />",{id:"assurance_dialog_"+id}).append($("<table />")
            .append($("<tr />")
              .append($("<th />",{html:"Response"}))
              .append($("<td />")
                .append($("<textarea />",{cols:"80", rows:"7", name:"response", id:"response", text:kpa.response}))
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Sign Off"}))
              .append($("<td />")
                 .append($("<select />",{name:"signoff", id:"signoff"})
                   .append($("<option />",{value:"1", text:"Yes"}))
                   .append($("<option />",{value:"0", text:"No"}))
                 )                
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Date Tested"}))
              .append($("<td />")
                .append($("<input />",{type:"text", name:"date_tested", id:"date_tested", readonly:"readonly", value:kpa.date_tested})
                  .addClass("datepicker")
                )
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Attachment"}))
              .append($("<td />")
                .append($("<input />",{type:"file", name:"attach_kpa", id:"attach_kpa"}))
              )
            )  
            .append($("<tr />")
              .append($("<td />",{colspan:"2"})
                .append($("<p />",{id:"message"}))
              )
            )                                    
        ).dialog({
                    autoOpen    : true,
                    modal       : true,
                    title       : "Edit Action Assurance",
                    position    : "top",
                    width       : 'auto',
                    buttons     : {
                                       " Save Changes "     : function()
                                       {
                                           if($("#response").val() == "")
                                           {
                                             $("#message").css({"padding":"5px"}).addClass("ui-state-error").html("Please enter the reponse");
                                           } else {
                                             $("#message").css({"padding":"5px"}).addClass("ui-state-info").html("updating....")
                                             $.post("main.php?controller=actionassurance&action=update", {
                                                response    : $("#response").val(),
                                                sign_off    : $("#signoff").val(),
                                                date_tested : $("#date_tested").val(),
                                                id          : id                                           
                                             }, function(response){
                                                if(response.error)
                                                {
                                                  $("#message").css({"padding":"5px"}).addClass("ui-state-error").html(response.text);
                                                } else {
                                                  if(response.updated)
                                                  {
                                                    jsDisplayResult("info", "info", response.text );	                
                                                  } else {
                                                    jsDisplayResult("ok", "ok", response.text );	                
                                                  }
                                                  $("#assurance_dialog_"+id).remove();
                                                  self._get();
                                                }                                             
                                             },"json")
                                           }
                                            
                                       } ,
                                       
                                       " Cancel "   : function()
                                       {
                                          $("#assurance_dialog_"+id).remove();                                    
                                       }
                    } ,
                    close       : function(event, ui)
                    {
                        $("#assurance_dialog_"+id).remove();
                    } ,
                    open        : function(event, ui)
                    {
		 				var dialog     = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons    = dialog.find(".ui-dialog-buttonpane").find("button");			 				
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		 				$("#signoff").val(kpa.sign_off);
		 				
	                    $(".datepicker").datepicker({
		                    showOn		    : "both",
		                    buttonImage 	: "/library/jquery/css/calendar.gif",
		                    buttonImageOnly : true,
		                    changeMonth	    : true,
		                    changeYear	    : true,
		                    dateFormat 	    : "dd-M-yy",
		                    altField		: "#startDate",
		                    altFormat		: 'd_m_yy'
	                    });			 				
                    }
                    
        }); 
    }, 
    
    _deleteKpaAssurance             : function(id)
    {
        var self = this;
         $("#message").css({"padding":"5px"}).addClass("ui-state-info").html("updating....")
         $.post("main.php?controller=actionassurance&action=update", {
            status      : 2,
            id          : id                                           
         }, function(response){
            if(response.error)
            {
              jsDisplayResult("error", "error", response.text );	
            } else {
              if(response.updated)
              {
                jsDisplayResult("info", "info", response.text );	                
              } else {
                jsDisplayResult("ok", "ok", response.text );	                
              }
              self._get();
            }                                             
         },"json")
    } 
    


});
