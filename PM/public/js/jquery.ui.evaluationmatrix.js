$.widget("ui.evaluationmatrix", {

    options         : {
     evaluationyear      : 0,
     evaluationperiod    : 0,
     directorate         : 0,
     subdirectorate      : 0,    
     employee            : 0,
     orderby             : 0,
     order               : 0,
     autoLoad            : false
    } ,
    
    _init           : function()
    {
       if(this.options.autoLoad)
       {
          this._get();
       }
    } ,
    
    _create         : function()
    {
       var self = this;
       $(self.element).append($("<table />",{width:"100%"}).addClass("noborder")
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />")
               .append($("<tr />")
                 .append($("<th />",{html:"Evaluation year:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationyear", id:"evaluationyear"}))
                 )
                 .append($("<th />",{html:"Evaluation period:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationperiod", id:"evaluationperiod"}))             
                 )            
               )
               .append($("<tr />")
                 .append($("<th />",{html:"Directorate:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"directorate", id:"directorate"}))
                 )
               )
               .append($("<tr />")
                 .append($("<th />",{html:"Employee:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"employee", id:"employee"}))
                 )
               )                                
             )             
           )           
         )
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />",{width:"100%", id:"evaluation_matrix_table"}))
           )
         )
       )
       
       self._loadEvaluationYears();
       self._loadDirectorates();
       
       $("#directorate").on('change', function(){
         self._loadUsers($(this).val());
         self.options.directorate = $(this).val();
         self._get();
       })
       
       $("#evaluationyear").on('change', function(){
          self.options.evaluationyear = $(this).val();
          self._loadEvaluationPeriods($(this).val());       
       });
       
       $("#evaluationperiod").on('change', function(e){
         $(".filters").show();
         self.options.evaluationperiod = $(this).val();
         self._loadUsers();
         self._get();
         e.preventDefault();
       });
       
       $("#employee").on('change', function(e){
         self.options.employee = $(this).val();
         self._get();
         e.preventDefault();
       })

    } ,

	_loadEvaluationYears		: function()
	{
	     $("#evaluationyear").append($("<option />",{value:"", text:"--select evaluation year--"}))
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
		   $.each( responseData, function( index, evalYear) {
			 $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))
		   });								
		},"json");
	} ,
	
	_loadEvaluationPeriods      : function(evaluationyear)
	{
	    $("#evaluationperiod").empty();
	    $("#evaluationperiod").append($("<option />",{value:"", text:"--select evaluation period--"}))
	    $.post("main.php?controller=evaluationperiod&action=get_all", {evaluationyear : evaluationyear},
		 function( responseData ){
		  var period_int = 1;
		  $.each(responseData['periods'][1], function(index, period) {
		    text_str = period.start_date+" -- "+period.end_date+" ("+period.name+" "+period_int+" )";
		    period_int++;
		    $("#evaluationperiod").append($("<option />",{value:period.id, text:text_str}))
		  });								
	    },"json");
	} ,
	
	_loadUsers            : function(departmentid)
	{
	     var self = this;
	     self.options.employee = "";
	     $("#employee").empty();
	     $("#employee").append($("<option />",{value:"", text:"All"}))
		$.post("main.php?controller=user&action=getAll",
		{
		   departmentid : departmentid
		}, function(users) {	
	        if(!$.isEmptyObject(users))
	        {
	           $.each(users, function(index, user){
	              $("#employee").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}))
	          });
	        }
		},"json"); 
		
	} ,
	
	_loadDirectorates       : function()
	{
	    $("#directorate").empty();
	    $("#directorate").append($("<option />",{value:"", text:"All"}))
	    $.post("main.php?controller=directorate&action=get_all_departments",function(directorates){	
	        if(!$.isEmptyObject(directorates))
	        {
	           $.each(directorates, function(index, directorate){
	              $("#directorate").append($("<option />",{value:directorate.id, text:directorate.name}))
	          });
	        }
	    },"json");
	} ,
	
	_loadSubDirectorates     : function(directorate)
	{
	    $("#subdirectorate").empty();
	     $("#subdirectorate").append($("<option />",{value:"", text:"All"}))
		$.post("main.php?controller=directorate&action=get_all_subdirectorate",
		  {directorate:directorate}, function(subdirectorates){	
	        if(!$.isEmptyObject(subdirectorates))
	        {
	           $.each(subdirectorates, function(index, subdirectorate){
	              $("#subdirectorate").append($("<option />",{value:subdirectorate.id, text:subdirectorate.name}))
	          });
	        }
		},"json");
	},
	
    _get            : function()
    {
       var self = this;
	  $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading ... <img src='public/images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"300px", left:"300px", border:"0px solid #FFFFF", padding:"5px"})
	  )         
       $.getJSON("main.php?controller=performancematrix&action=get_matrix_report", {
            evaluationyear     : self.options.evaluationyear,
            evaluationperiod   : self.options.evaluationperiod,
            directorate        : self.options.directorate,
            employee           : self.options.employee
       }, function(matrixReport) {
          $("#evaluation_matrix_table").html("")
          if($.isEmptyObject(matrixReport))
          {
             $("#evaluation_matrix_table").append("<tbody><tr><td colspan='6'>No records match the selected filters</td></tr></tbody>");
          } else {
            self._display(matrixReport);
          }
          $("#loadingDiv").remove();
       }); 
    },
    
    
    _display          : function(reportData)
    {
      var self = this;
      var html = [];
       html.push("<thead>");
       html.push("<tr>");
        html.push("<td colspan='16'><h2>Evaluation Matrix Report as at "+reportData['report_date']+"</h2></td>");
       html.push("</tr>");                    
       html.push("</thead>");      
       var report = reportData['usermatrix'];
       var len    = report.length;
      for(var i = 0; i < len; i++)
      {
         var _self_total    = 0;
         var _manager_total = 0;
         var _joint_total   = 0;
         var _review_total  = 0;
          html.push("<tr>");
           html.push("<th colspan='2'>Employee</th>");
           html.push("<td colspan='2'>"+report[i]['user']['user']+"</td>");
           html.push("<th colspan='2'>Manager</th>");
           html.push("<td colspan='2'>"+report[i]['user']['manager']+"</td>");
           html.push("<th colspan='2'>Evaluation Year</th>");
           html.push("<td colspan='2'>"+report[i]['user']['year']+"</td>");
           html.push("<th colspan='2'>Evaluation On</th>");
           html.push("<td colspan='2'></td>");        
          html.push("</tr>");
          html.push("<tr>");
           html.push("<th colspan='2'>Job Level</th>");
           html.push("<td colspan='2'>"+report[i]['user']['joblevel']+"</td>");
           html.push("<th colspan='2'>Perfomance Category</th>");
           html.push("<td colspan='2'>"+report[i]['user']['category']+"</td>");
           html.push("<th colspan='2'>Evaluation Period</th>");
           html.push("<td colspan='2'>"+report[i]['user']['period']+"</td>");
           html.push("<th colspan='2'>Status</th>");
           html.push("<td colspan='2'></td>");        
          html.push("</tr>"); 
          if(report[i].hasOwnProperty('component'))
          {
              $.each(report[i]['component'], function(componentid, component){
                 var _self_average    = 0;
                 var _manager_average = 0;
                 var _joint_average   = 0;
                 var _review_average  = 0;
                 var count            = 0;
                 var evals = {
                                self_rating     : 0,
                                self_score      : 0,
                                _self_score     : 0,
                                _self_rating    : 0,
                                manager_rating  : 0,
                                manager_score   : 0,
                                _manager_score  : 0,
                                _manager_rating : 0,
                                joint_rating    : 0,
                                joint_score     : 0,
                                _joint_score    : 0,
                                _joint_rating   : 0,
                                review_rating   : 0,
                                review_score    : 0,
                                _review_rating  : 0,
                                _review_score   : 0
                             }               
                 if(component.hasOwnProperty('data'))
                 {
                    html.push("<tr>");
                      html.push("<td colspan='4'><b>"+component['componentname']+"</b> <em>"+component['weight']+"</em>%</b></td>");
                      html.push("<td colspan='3'><b>Self Evaluation</b></td>");
                      html.push("<td colspan='3'><b>Manager Evaluation</b></td>");
                      html.push("<td colspan='3'><b>Joint Evaluation</b></td>");
                      html.push("<td colspan='3'><b>Evaluation Review</b></td>");
                    html.push("</tr>");                      
                    html.push("<tr style='background-color:#EEF;'>");
                      html.push("<td>"+component['type']+"</td>");
                      html.push("<td>Deadline Date</td>");  
                      html.push("<td>Status</td>");
                      html.push("<td>Weight</td>");
                      html.push("<td>Score</td>");
                      html.push("<td>W.Score</td>");
                      html.push("<td>Comments</td>");
                      html.push("<td>Score</td>");
                      html.push("<td>W.Score</td>");
                      html.push("<td>Comments</td>");
                      html.push("<td>Score</td>");
                      html.push("<td>W.Score</td>");
                      html.push("<td>Comments</td>");                      
                      html.push("<td>Score</td>");
                      html.push("<td>W.Score</td>");
                      html.push("<td>Comments</td>");                      
                    html.push("</tr>");               
                    if(!$.isEmptyObject(component['data']))
                    {
                      var weighting    = 0
                      $.each(component['data'], function(datakey, data){
                         count++;
                         //if(evals.hasOwnPropert())
                         $.each(evals, function(key, val){
                            if(data.hasOwnProperty(key) && (data[key] !== undefined))
                            {
                               evals[key]      = data[key];
                               evals['_'+key] += parseFloat(data[key]);
                            } 
                         });
                         weighting    += parseFloat(data['weight']);
                         html.push("<tr>");
                           html.push("<td>"+data['name']+"</td>");
                           html.push("<td>"+data['deadline']+"</td>");  
                           html.push("<td>"+data['status']+"</td>");
                           html.push("<td style='text-align:right;'>"+data['weight']+"</td>");
                           html.push("<td style='text-align:right;'>"+evals['self_rating']+"</td>");
                           html.push("<td style='text-align:right;'>"+evals['self_score']+"</td>");
                           html.push("<td>"+(data['self_comment'] === undefined ? "" : data['self_comment'])+"</td>");
                           html.push("<td style='text-align:right;'>"+evals['manager_rating']+"</td>");
                           html.push("<td style='text-align:right;'>"+evals['manager_score']+"</td>");
                           html.push("<td>"+(data['manager_comment'] === undefined ? "" :data['manager_comment'])+"</td>");
                           html.push("<td style='text-align:right;'>"+evals['joint_rating']+"</td>");
                           html.push("<td style='text-align:right;'>"+evals['joint_score']+"</td>");
                           html.push("<td>"+(data['joint_comment'] === undefined ? "" : data['joint_comment'])+"</td>");
                           html.push("<td style='text-align:right;'>"+evals['review_rating']+"</td>");
                           html.push("<td style='text-align:right;'>"+evals['review_score']+"</td>");
                           html.push("<td>"+(data['review_comment'] === undefined ? "" : data['review_comment'])+"</td>");
                         html.push("</tr>"); 
                      });                                                             
                    }
                         if(count != 0)
                         {
                             _self_average    = ((evals['_self_score']/count)).toFixed(2);
                             _self_total     += parseFloat(_self_average * (component['weight']/100));
                             _manager_average = ((evals['_manager_score']/count)).toFixed(2);
                             _manager_total  += parseFloat(_manager_average * (component['weight']/100));
                             _joint_average   = ((evals['_joint_score']/count)).toFixed(2);
                             _joint_total    += parseFloat(_joint_average * (component['weight']/100));
                             _review_average  = ((evals['_review_score']/count)).toFixed(2);
                             _review_total   += parseFloat(_review_average * (component['weight']/100));
                         }
                         html.push("<tr>");
                           html.push("<td colspan='3'><b>Total Score</b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['totalweighting'] == undefined ? "" : component['totalweighting'])+"</b></td>");
                           html.push("<td style='text-align:right;'><b>"+evals['_self_rating']+"</b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['self_total'] == undefined ? "" : component['self_total'])+"</b></td>");
                           html.push("<td><b></b></td>");
                           html.push("<td style='text-align:right;'><b>"+evals['_manager_rating']+"</b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['manager_total'] == undefined ? "" : component['manager_total'])+"</b></td>");
                           html.push("<td><b></b></td>");
                           html.push("<td style='text-align:right;'><b>"+evals['_joint_rating']+"</b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['joint_total'] == undefined ? "" : component['joint_total'])+"</b></td>");
                           html.push("<td><b></b></td>");
                           html.push("<td style='text-align:right;'><b>"+evals['_review_rating']+"</b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['review_total'] == undefined ? "" : component['review_total'])+"</b></td>");
                           html.push("<td><b></b></td>");
                         html.push("</tr>");      
                         html.push("<tr>");
                           html.push("<td colspan='4'><b>Average Score</b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['selfrating'] == undefined ? "" : component['selfrating'])+"</b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['self_avg'] == undefined ? "" : component['self_avg'])+"</b></td>");
                           html.push("<td><b></b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['managerrating'] == undefined ? "" : component['managerrating'])+"</b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['manager_avg'] == undefined ? "" : component['manager_avg'])+"</b></td>");
                           html.push("<td><b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['jointrating'] == undefined ? "" : component['jointrating'])+"</b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['joint_avg'] == undefined ? "" : component['joint_avg'])+"</b></td>");
                           html.push("<td><b></b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['reviewrating'] == undefined ? "" : component['reviewrating'])+"</b></td>");
                           html.push("<td style='text-align:right;'><b>"+(component['review_avg'] == undefined ? "" : component['review_avg'])+"</b></td>");
                           html.push("<td></td>");
                         html.push("</tr>");    
                    html.push("<tr>");
                    html.push("<td colspan='16'>&nbsp;&nbsp;&nbsp;</td>");      
                    html.push("</tr>");                
                 }
              });
             html.push("<tr>");
               html.push("<td colspan='5'></td>");
               html.push("<td style='color:red; background-color:orange; text-align:right;'><b>"+report[i]['self']+"</b></td>");
               html.push("<td colspan='2'></td>");
               html.push("<td style='color:red; background-color:orange; text-align:right;'><b>"+report[i]['manager']+"</b></td>");
               html.push("<td colspan='2'></td>");
               html.push("<td style='color:red; background-color:orange; text-align:right;'><b>"+report[i]['joint']+"</b></td>");
               html.push("<td colspan='2'></td>");
               html.push("<td style='color:red; background-color:orange; text-align:right;'><b>"+report[i]['review']+"</b></td>");
               html.push("<td></td>");
             html.push("</tr>");   
             html.push("<tr>");
              html.push("<td colspan='16'>&nbsp;&nbsp;&nbsp;</td>");      
             html.push("</tr>");               
          } else {
            html.push("<tr>");
              html.push("<td colspan='16'>There are no components setup for this user for the selected criteria</td>");      
            html.push("</tr>");                
          }
          html.push("<tr>");
            html.push("<td colspan='16'>&nbsp;&nbsp;&nbsp;</td>");      
          html.push("</tr>"); 
      }
      $("#evaluation_matrix_table").append("<tbody>"+html.join('')+"</tbody>");
    }


});
