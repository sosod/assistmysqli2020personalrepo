$.widget("ui.evaluation", {

     options        : {
          start               : 0,
          limit               : 250,
          total               : 0,
          current             : 1,
          url                 : "main.php?controller=usercomponent&action=getUserAll",
          user                : "",
          action              : "matrixcomponents",
          evaluation_period   : {},
          userdata            : {}, 
          currentComponent    : 0, 
          componentid         : "",
          allowNotification   : true
     } ,
     
     _init           : function()
     {
        this._get();
     } ,
     
     _create         : function()
     {
          var self = this;
          //$(self.element).append($("<table />"))
     } ,
     
     _get              : function()
     {
        var self = this;
	    $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading components... <img src='public/images/loaderA32.gif' />"})
		   .css({position:"absolute", "z-index":"9999", top:"300px", left:"300px", border:"0px solid #FFFFF", padding:"5px"})
		)         
         $.getJSON(self.options.url, {
            user     : self.options.user,
            section  : $("#section").val(),
            page     : $("#page").val() 
         } , function(componentsData) {
            $(self.element).html("");
            if(componentsData.hasOwnProperty('matrix_update'))
            {
               if(!$.isEmptyObject(componentsData.matrix_update))
               {
                  self._showUpdateMessage(componentsData);
               }
            }
            if(componentsData.hasOwnProperty('evaluation_period'))
            {
               self.options.evaluation_period = componentsData['evaluation_period'];
            }
            
            if(!$.isEmptyObject(componentsData.userdata))
            {
               $("#userdetails").html("");
               self.options.userdata = componentsData.userdata;
               self._displayHeader(componentsData.userdata, componentsData.subordinates);
            }               
            if(componentsData.error)
            {
                $(self.element).html(componentsData.text).addClass("ui-state-highlight").css({padding:"5px"})
                //$("#activatecomponent").attr("disabled", "disabled");
                $("#div_activatecomponent").hide();
                $("#div_confirmcomponent").hide();
            } else {
               if(!$.isEmptyObject(componentsData.components))
               {   
                   //enable confirmation of completed evaluations
                   if((componentsData.previous_evaluation == componentsData.next) 
                    && !(componentsData.is_confirmed))
                   {
                      if(componentsData.can_evaluate)
                      {
                         $("#div_confirmcomponent").show();
                         $("#confirmEvaluate").val( "Complete "+componentsData.previous_evaluation.replace('_', ' ')+" evaluation" );
                         $("#confirmEvaluate").live('click', function(event){
                            var str = componentsData.transition_message;  
                            self._notifyEvaluationChange(str, componentsData.previous_evaluation );
                            self.options.allowNotification  = false;
                            event.preventDefault();
                         });                       
                      } else {
                        $("#div_confirmcomponent").hide();      
                      }
                   } else {
                      $("#div_confirmcomponent").hide();
                   }
               
               
                  if(componentsData.hasOwnProperty("transition_message"))
                  {
                     if(componentsData.transition_message != "" && self.options.allowNotification == true)
                     {
                        self._notifyEvaluationChange(componentsData.transition_message, componentsData.previous_evaluation );
                     } else {
                        self._displayComponents(componentsData.components, componentsData.totalWeight, componentsData.userdata, componentsData.next);                        
                     }               
                  } else {
                      self._displayComponents(componentsData.components, componentsData.totalWeight, componentsData.userdata, componentsData.next);                    
                  }
                  $("#div_activatecomponent").show();
               } else {
                  $(self.element).html( componentsData.error );
                  //$("#activatecomponent").attr("disabled", "disabled");
                  $("#div_activatecomponent").hide();
               }            
             }
         });    
         
         self._displayFooter();                           
         $("#activatecomponent").live('click', function(event){
           self._activateUserComponents();
           event.preventDefault();
         });          

     } ,
     
     _displayComponents            : function(components, totalWeight, user, next)
     {    
        var self              = this;
        var compenetSecWeight = 0;
        $(self.element).removeClass("ui-state").removeClass("ui-state-highlight");
        var li                = $("<ul />",{id:"tabslist"})
        var componentIndex    = 0;
        $.each(components, function(index, component){
            if(self.options.componentid != "")
            {
                if(self.options.componentid == index)
                {
                    self.options.currentComponent = componentIndex;
                }
            }
            compenetSecWeight = parseInt(component.weight) + parseInt(compenetSecWeight);
            hrefStr = "main.php?controller=component&action="+self.options.action+"&next_eval="+next+"&id="+index+"&user="+user.tkid+"&selecteduser="+self.options.user+"&componentindex="+componentIndex
            li.append($("<li />")
               .append($("<a />",{href:hrefStr, html:"<span>"+component.name+" (<b>"+component.percentage+"</b>)</span>"}))
            )       
            componentIndex++;
        });

        if(totalWeight < 100)
        {
	         li.append($("<li />")
	           .append($("<span />",{html:"Sum of components not equalt to 100%"}).addClass("ui-state-error").css({padding:"5px", "margin-top":"0px", position:"absolute"}))
	         )								      
        }  
        if((compenetSecWeight < 100))
        {                
          $("#activatecomponent").attr("disabled", "disabled");
        }
        
        $(self.element).append(li).tabs({ajaxOptions: { async: false },
                                          spinner : "Loading ... ",
                                          select  : function(event, ui) {
                                               //show spinner
                                              // $("#tabs-loading-message").show();   
                                              $(".ui-tabs").css({"padding":"0px"});
                                              $(".ui-tabs-panel").css({"padding":"0px"});
                                              self.options.currentComponent = ui.index
                                              //$('ui.panel').blockUI();
                                          },
                                          load: function() {
                                              $(".ui-tabs").css({"padding":"0px"});
                                              $(".ui-tabs-panel").css({"padding":"0px"}); 
                                             //console.log("completed loading ... ");
                                             //  hide spinner
                                             //$("#tabs-loading-message").hide();               
                                            //$('ui.panel').unblockUI();
                                          }                 
                                        }).tabs({selected:self.options.currentComponent})                                        
     } ,
     
     _displayHeader      : function(userdata, subordinates)
     {
        var self = this;
        var html = [];
        html.push("<table width='100%'>");
          html.push("<thead>");
          html.push("<tr>");
           html.push("<th>Select User : </th>");
           html.push("<td>");
             html.push("<select id='user' name='user'><select>");
           html.push("</td>");
           html.push("<th>Manager : </th>");
           html.push("<td>"+userdata.managername+"</td>"); 
           html.push("<th>Evaluation Year : </th>");
           html.push("<td>"+userdata.start+" to "+userdata.end+"</td>");           
          html.push("</tr>");
          html.push("<tr>");
           html.push("<th>Job Level : </th>");
           html.push("<td>"+userdata.joblevel+"("+userdata.joblevelid+")"+"</td>");
           html.push("<th>Performance Category : </th>");
           html.push("<td>"+userdata.performancecategory+"</td>");     
           html.push("<th>Evaluation Period : </th>");
           html.push("<td><span id='evaluation_period'></span></td>");                      
          html.push("</tr>");          
          html.push("</thead>");
        html.push("</table>");
        $("#userdetails").append(html.join(' ')).find('th').css({"text-align":"left"});

        if(!$.isEmptyObject(self.options.evaluation_period))
        {
          if(self.options.evaluation_period['start_date'] == undefined || self.options.evaluation_period['end_date'] == undefined)
          {
            $("#evaluation_period").html("<b>N/A</b>")
          } else {
             $("#evaluation_period").html("<span>"+self.options.evaluation_period['start_date']+" to "+self.options.evaluation_period['end_date']+" </span>")            
           }
        } else {
          $("#evaluation_period").html("<b>Closed</b>")
        }
        if(!$.isEmptyObject(subordinates))
        {
          $.each(subordinates, function(sIndex, user){
              if(sIndex === self.options.user)
              {
                $("#user").append($("<option />",{text:user, value:sIndex, selected:"selected"}))
              } else if(sIndex === userdata.tkid) {
                $("#user").append($("<option />",{text:user, value:sIndex, selected:"selected"}))
              } else {
                $("#user").append($("<option />",{text:user, value:sIndex}))
              }
          });
        }
        
        $("select#user").live("change", function(e){
            if($("#tabslist").length > 0)
            {
               $("#tabslist").remove();
            }
            self.options.currentComponent = 0;
            self.options.user             = $(this).val();
            self._get();
            e.preventDefault()      
        });
     } ,
     
     _displayFooter                : function()
     {
        var self = this;
        /*$("<input />",{type:"button", name:"download", id:"download", value:"Download PDF"}).appendTo($(self.element))
        $(self.element).append($("<input />",{type:"button", name:"download", id:"download", value:"Download PDF"}).click(function(e){
               document.location.href = "";
               e.preventDefault();
          })
        )*/
     } ,
    
     _showUpdateMessage             : function(componentData)
     { 
       var self = this;
       if($("#matrix_updatemessage").length > 0)
       {
           $("#matrix_updatemessage").remove();
       } 
       
       $("<div />",{id:"matrix_updatemessage"}).append($("<p />").addClass("ui-state-info").css({padding:"5px"})
          .append($("<span />"))
          .append($("<span />",{html:"The perfomance matrix for the category <b>"+componentData['userdata']['performancecategory']+"</b> for the evaluation year <b>"+componentData['userdata']['start']+" "+componentData['userdata']['end']+"</b> has been updated since the time you created the matrix.<br /> Click update to update your matrix and cancel to continue with this matrix"}))
        ).dialog({
          autoOpen  : true,
          modal     : true,
          position  : "top",
          width     : 300,
          title     : "Perfomanace Matrix Update",
          buttons   : {
                       "Update "   : function()
                       {
                         $.post("main.php?controller=usercomponent&action=updatePerfomanceMatrix", {
                             userdata   : componentData.userdata,
                             components : componentData.matrix_update
                         },function(response){
                            $("#matrix_updatemessage").remove(); 
                            document.location.href = "main.php?controller=component&action=newcomponent&parent_id=1&folder=new";
                         },"json")
                       } ,
                       
                       "Cancel"    : function()
                       {
                         $("#matrix_updatemessage").remove(); 
                       }
          } ,
          open      : function(event, ui)
          {
              $("#matrix_updatemessage").append($("<ul />",{id:"update_list"}))
              $.each(componentData.matrix_update, function(index, comp){
                $("#update_list").append($("<li />",{html:comp.name+" component was activated"}))
              });
          } ,
          close     : function(event, ui)
          {
             $(this).dialog("destroy");
             $("#matrix_updatemessage").remove();
          }
          
       })
       
    
     },
    
     _activateUserComponents       : function()
     {
          var self = this;
           if($("#activatedialog").length > 0)
           {      
              $("#activatedialog").remove();
           }
	     /*var responseUser; 
	     $.ajaxSetup({async:false});
	     $.getJSON("main.php?controller=user&action=getUser", function(response){  
	          responseUser = response;
	     });*/		
           $("<div />", {id:"activatedialog", html:"I, "+self.options.userdata.tkmanager+", manager of "+self.options.userdata.user+" hereby:"})
            .append($("<ul />")
              .append($("<li />",{html:"Declare that "+self.options.userdata.user+" has agreed to this Performance Matrix; and"}))
              .append($("<li />",{html:"Declare that I have the required authority to activate this Performance Matrix."}))
            )
            .append($("<div />",{id:"message"}))
            .dialog({
                     autoOpen  : true,
                     modal     : true,
                     position  : "top",
                     width     : 350,
                     height    : 200,
                     title     : "Activation of Performance Matrix",
                     buttons   : {
                                   "Activate"     : function()
                                   {
                                       $("#message").addClass('ui-state-info').html("activating ...<img src='public/images/loaderA32.gif' /> ");
                                         $.post("main.php?controller=usercomponent&action=activateComponent", {
                                             user           : self.options.user
                                         }, function(response) {
			                             if( response.error )
			                             {
				                           jsDisplayResult("error", "error", response.text );
	                                       } else {
                                               $("#activatedialog").dialog("destroy");
                                               $("#activatedialog").remove(); 
                                               jsDisplayResult("ok", "ok", response.text );
                                               self._get();
			                             }	
                                         },"json");            
                                   } ,
                                   "Cancel"       : function()
                                   {
                                      $("#activatedialog").dialog("destroy");
                                      $("#activatedialog").remove();
                                   }
                     } , 
                     close      : function()
                     {
                         $("#activatedialog").dialog("destroy");    
                         $("#activatedialog").remove();                     
                     } ,
                     open       : function(event, ui)
                     {
	 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
	 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
	 				var saveButton = buttons[0];
	 				$(saveButton).css({"color":"#090"});                    
                     }
            });
     } ,
     
    _notifyEvaluationChange           : function(transition_message, previous_evaluation)
    {
       var self = this;
       if($("#evaluation_change_dialog").length > 0)
       {
          $("#evaluation_change_dialog").remove();
          $(".ui-dialog").remove();
       }
       
       $("<div />",{id:"evaluation_change_dialog"}).append($("<table />")
          .append($("<tr />")
            .append($("<td />",{colspan:"2"})
              .append($("<p />").addClass("ui-state-info").css({"padding":"5px"})
                 .append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))
                 .append($("<span />",{html:transition_message}).css({"padding":"3px", "margin-left":"0.3em"})) 
              )
            )
          )
          .append($("<tr />")
            .append($("<th />",{html:"Overall Comment:"}))
            .append($("<td />")
              .append($("<textarea />", {rows:7, cols:50, name:"overall_comment", id:"overall_comment"}))
            )
          )
       )
       .append($("<tr />")
          .append($("<td />",{colspan:"2"})
            .append($("<p />",{id:"msg"}).css({padding:"5px"}))
          )
       )
       .dialog({
          autoOpen  : true,
          modal     : true,
          position  : "top",
          width     : 500,
          title     : " Evaluation Stage Complete",
          buttons   : {
                         "Ok"    : function()
                         {
                            if($("#overall_comment").val() == "")
                            {
                              $("#msg").css({"padding":"5px"}).addClass("ui-state-error").html("Please enter the comment");
                              $("#overall_comment").focus();
                            } else {                          
                              $.post("main.php?controller=usercomponent&action=notify_manager",{
                                  kpa_change     : transition_message,
                                  prev_eval      : previous_evaluation, 
                                  user           : self.options.user,
                                  user_selected  : $("#user").val(),
                                  comment        : $("#overall_comment").val()     
                              },function(response) {
                                 if(response.error)
                                 {
                                    $("#msg").css({"padding":"5px"}).addClass("ui-state-error").html(response.text);
                                 } else {
                                   $("#evaluation_change_dialog").remove();
                                   $("#evaluation_change_dialog").dialog("destroy");
                                   $(".ui-dialog").remove();
                                   $(".ui-widget-overlay").remove();
                                   jsDisplayResult("ok", "ok", "Evaluation successfully updated..." );
                                   self.options.allowNotification = false;
                                   self._get();
                                 }	                                    
                              },"json");
                            }
                         } ,
                         
                         "Cancel"           : function()
                         {
                            $("#evaluation_change_dialog").remove();
                            $("#evaluation_change_dialog").dialog("destroy");
                            $(".ui-dialog").remove();
                            $(".ui-widget-overlay").remove();       
                            self.options.allowNotification = false; 
                            self.options.userNext = previous_evaluation;
                            self._get();                 
                         }
          } ,
          close     : function(event, ui)
          {
              $("#evaluation_change_dialog").dialog("destroy");
              $("#evaluation_change_dialog").remove();
              $(".ui-dialog").remove();
              $(".ui-widget-overlay").remove();
          } ,
          open      : function(event, ui)
          {
		     var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		     var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
	         $(buttons[0]).css({"color":"#090"});
	         $(this).css({height:'auto'});
          }
       })
    
    }          

});
