$(function(){

     $("#selectuser").change(function(e){
       Newmatrix.displayComponents($(this).val());
       e.preventDefault();
     });


     $("#activatecomponent").click(function(){
           if($("#activatedialog").length > 0)
           {      
              $("#activatedialog").remove();
           }
	     var responseUser; 
	     $.ajaxSetup({async:false});
	     $.getJSON("main.php?controller=user&action=getUser", function(response){  
	          responseUser = response;
	     });		
           $("<div />", {id:"activatedialog", html:"I, Manager Name + Surname, manager of "+responseUser.tkname+" "+responseUser.tksurname+" hereby:"})
            .append($("<ul />")
              .append($("<li />",{html:"Declare that "+responseUser.tkname+" "+responseUser.tksurname+" has agreed to this Performance Matrix; and"}))
              .append($("<li />",{html:"Declare that I have the required authority to activate this Performance Matrix."}))
            )
            .dialog({
                     autoOpen  : true,
                     modal     : true,
                     position  : "top",
                     width     : 350,
                     height    : 200,
                     title     : "Activation of Performance Matrix",
                     buttons   : {
                                   "Activate"     : function()
                                   {
                                         $.post("main.php?controller=component&action=activateComponent", function(response) {
			                             if( response.error )
			                             {
				                             jsDisplayResult("error", "error", response.text );
			                             } else {
                                                $("#activatedialog").dialog("destroy");
                                                $("#activatedialog").remove();                      
				                             jsDisplayResult("ok", "ok", response.text );
			                             }	
                                         },"json");                                        
                                   } ,
                                   "Cancel"       : function()
                                   {
                                      $("#activatedialog").dialog("destroy");
                                      $("#activatedialog").remove();
                                   }
                     } , 
                     close      : function()
                     {
                         $("#activatedialog").dialog("destroy");    
                         $("#activatedialog").remove();                     
                     } ,
                     open       : function(event, ui)
                     {
	 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
	 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
	 				var saveButton = buttons[0];
	 				$(saveButton).css({"color":"#090"});                    
                     }
            });
          return false;
     });
	/*$("#evaluationyear").change(function(){
		var evalYear = $(this).val(); 
		$("#detailed").show();
		$("#evalyeartext").html( $("#evaluationyear :selected").text() )
		$("#tabs").html("");
          Newmatrix.displayComponents(evalYear, $("#page").val() );
		return false;
	});*/
	Newmatrix.displayComponents();
	/*var evaluationyear = $("#evaluationyearid").val();
	if( evaluationyear != "")
	{		
	     Newmatrix.displayComponents(evaluationyear);   
	}
	Newmatrix.getEvaluationyears();
	*/
});

var Newmatrix  = {
		
		getUser                  : function( componentId )
		{

		     return responseUser;
		} ,
		
		getEvaluationyears		: function()
		{
			$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
				$.each( responseData, function( index, evalYear) {
					$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))					
				});								
			},"json");
		} , 
		
		showComponents		: function()
		{
			$("#evaluationYear").html("Evaluation Year : "+$("#evaluationyear :selected").text() );
			$("#selectevaluationyear").dialog("destroy");
			$("#selectevaluationyear").remove();
			$("#goalsdiv").performancematrix({addKpa:true, editKpa : true,  addKpi:true, addAction:true});			
		}  , 
		
		displayComponents   : function(user)
		{
		    $("#tabs").html("");
		    $.getJSON("main.php?controller=component&action=getPerformanceComponents",{
			    page       : $("#page").val(),
			    section    : $("#section").val(),
			    user       : user
		    }, function(componentData) {	
		          if(componentData.error)
		          {
			         $("#tabs").append($("<p />").addClass("ui-state-highlight").css({"padding":"5px"})
				              .append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))
				              .append($("<span />",{html:componentData.text}).css({"margin-left":"3px"}))
				         )
		          } else {
		    
			         if($.isEmptyObject(componentData.components))
			         {
				         $("#tabs").append($("<p />").addClass("ui-state-highlight").css({"padding":"5px"})
				           .append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))
				           .append($("<span />",{html:"There are no components setup for the selected evaluation year"}).css({"margin-left":"3px"}))
				         )
				         $("#activatecomponent").hide();
			         } else {
			             $("#tabs").removeClass("ui-state").removeClass("ui-state-highlight");
				         var li = $("<ul />",{id:"tablist"})
				         $.each(componentData.components, function( index, component){
					         li.append($("<li />")
					           .append($("<a />",{href:"main.php?controller=component&action=matrixcomponents&id="+component.id+"&user="+user, html:component.name+" (<b>"+component.percentage+"</b>)"}))
					         )				
				         });
				         if(componentData.totalWeight < 100)
				         {
					         li.append($("<li />")
					           .append($("<span />",{html:"Sum of components not equalt to 100%"}).addClass("ui-state-error").css({padding:"5px", "margin-top":"0px", position:"absolute"}))
					         )								      
         				         $("#activatecomponent").attr("disabled", "disabled");
				         }
				         $("#tabs").append(li).tabs({ajaxOptions: { async: false }});				
			         }	
			    }	
		    });			
		} 		
		
		
};
