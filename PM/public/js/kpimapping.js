$(function(){

	KpiMapping.getEvaluationyears();
    KpiMapping.getUsers();

	$("#users").change(function(){
		var user = $(this).val();
		KpiMapping.display(user)
		return false;
	});
	
});

var KpiMapping = {

		getEvaluationyears		: function()
		{
			$.post("evaluationyears?action=getAll", { status : 1}, function( responseData ){
				$.each( responseData, function( index, evalYear) {
					$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))					
				});								
			},"json");
		} , 
		
		getUsers				: function(users)
		{
			$.getJSON("user?action=getAll", function(response){
                if(!$.isEmptyObject(response))
                {
                    $.each(response, function(index, val){
                        $("#users").append($("<option />",{text:val.tkname+" "+val.tksurname, value:val.tkid}))
                    });       
                    if(!$.isEmptyObject(users))
                    {             
                        $.each(users,function(i, value){
                            $("#users option[value='"+value+"']").attr("selected", "selected");
                        });
                    }
                }
			});
			
		} ,
		
		display         : function(user)
		{
		    $(".mapping").remove()
		    $("#modulestable").remove();
		    $("#tablemapping").append($("<td />",{colspan:4, valign:"top"}).addClass("mapping")
		        .append($("<table />",{id:"modulestable", width:"100%"}))
		    )
		    $.get("kpimapping?action=getModules", {
		        user : user
		    },function(mapping){
		        $("#modulestable").html(mapping)
		        
		        $(".assign").live("click", function(){
		            var selectRef = (this.id).replace("assign", "kpi");
		            var selectedVal =  $("select#"+selectRef+" :selected").val();
                    if( selectedVal == "")
                    {
                        jsDisplayResult("error", "error", "Kpi cannot be empty");
                        return false;
                    } else {
                        jsDisplayResult("info", "info", "assigning kpi mapping  . . . <img src='../resources/images/loaderA32.gif' /> " );
                        $.post("kpimapping?action=saveMapping",{
                           user      : user,
                           modref_id : this.id,
                           kpi       : selectedVal,		                    
                           module    : this.id
                        }, function( response ){
	                        if( response.error )
	                        {
		                        jsDisplayResult("error", "error", response.text );
	                        } else {
		                        jsDisplayResult("ok", "ok", response.text );
		                        KpiMapping.display(user)
	                        }	
                        },"json"); 
                   }
		            return false;
		        });
		        
                $(".update").live("click", function(){
                    
		            var selectRef = (this.id).replace("update", "kpi");
		            var selectedVal =  $("select#"+selectRef+" :selected").val();                         
                    if( selectedVal == "")
                    {
                        jsDisplayResult("error", "error", "Kpi cannot be empty");
                        return false;
                    } else {
                        jsDisplayResult("info", "info", "updating kpi mapping  . . . <img src='../resources/images/loaderA32.gif' /> " );
                        $.post("kpimapping?action=updateMapping",{
                           user      : user,
                           modref_id : this.id,
                           kpi       : selectedVal,		                    
                           module    : this.id
                        }, function( response ){
	                        if( response.error )
	                        {
		                        jsDisplayResult("error", "error", response.text );
	                        } else {
		                        jsDisplayResult("ok", "ok", response.text );
		                        KpiMapping.display(user)
	                        }	
                        },"json"); 
                   }
                });			        
	            /*	    
		        $.each(mapping.modules, function( index, module){
		        
		            $("#modulestable").append($("<tr />")
		              .append($("<td />")
		                .append($("<span />").addClass("ui-icon").addClass("ui-icon-minus").css({"float":"left"}))
                        .append($("<span />",{html:"<b>"+index+"<b>"}))
		              )
		              .append($("<td />").addClass("gray"))
	                  .append($("<td />",{html:"<b>KPI<p/>"}).addClass("gray"))
  		              .append($("<td />").addClass("gray"))
		            )
                    $.each( module, function(id, val){
		                $("#modulestable").append($("<tr />")
		                  .append($("<td />").addClass("noborder"))
		                  .append($("<td />")
		                    .append($("<span />").addClass("ui-icon").addClass("ui-icon-carat-1-sw").css({"float":"left"}))
                            .append($("<span />",{html:val.name}))
		                  )
                          .append($("<td />")
                            .append($("<select />",{id:index+"_kpi_"+val.id, name:index+"_kpi_"+val.id}).addClass("kpilist")
                              .append($("<option />",{text:"--please select--", value:""}))                
                            )
                          ) 
                          .append($("<td />")
                            .append($("<input />",{type:"button", id:index+"_assign_"+val.id, name:index+"_assign_"+val.id, value:"Assign"})
                                .addClass("gray")
                                .css({"display":(val.used  ? "none" : "block")})
                            )
                            .append($("<input />",{type:"button", id:index+"_update_"+val.id, name:index+"_update_"+val.id, value:"Update"})
                                .addClass("gray")
                                .css({"display":(val.used ? "block" : "none")})
                            )
                          )                           
		                )

                        if( mapping.modRefUsed[index] !== undefined)
                        {
                            if(mapping.modRefUsed[index][id] !== undefined)
                            {  
                               $("select#"+index+"_kpi_"+id+" option[value='27']").attr("selected", "selected") 
                                console.log( index+"_kpi_"+id+" value --- "+mapping.modRefUsed[index][id]);
                            }                     
                        }


		                $("#"+index+"_update_"+val.id).live("click", function(){
		                    if( $("#"+index+"_kpi_"+val.id+" :selected").val() == "")
		                    {
		                        jsDisplayResult("error", "error", "Kpi cannot be empty");
		                        return false;
		                    } else {
		                        jsDisplayResult("info", "info", "updating kpi mapping  . . . <img src='../resources/images/loaderA32.gif' /> " );
		                        $.post("kpimapping?action=updateMapping",{
		                           user      : user,
		                           modref_id : val.id,
		                           kpi       : $("#"+index+"_kpi_"+val.id+" :selected").val(),		                    
		                           module    : index
		                        }, function( response ){
			                        if( response.error )
			                        {
				                        jsDisplayResult("error", "error", response.text );
			                        } else {
				                        jsDisplayResult("ok", "ok", response.text );
				                        KpiMapping.display(user)
			                        }	
		                        },"json"); 
		                   }
		                });		                
		                
		                $("#"+index+"_assign_"+val.id).live("click", function(){
		                    if( $("#"+index+"_kpi_"+val.id+" :selected").val() == "")
		                    {
		                        jsDisplayResult("error", "error", "Kpi cannot be empty");
		                        return false;
		                    } else {
		                        jsDisplayResult("info", "info", "assigning kpi mapping  . . . <img src='../resources/images/loaderA32.gif' /> " );
		                        $.post("kpimapping?action=saveMapping",{
		                           user      : user,
		                           modref_id : val.id,
		                           kpi       : $("#"+index+"_kpi_"+val.id+" :selected").val(),		                    
		                           module    : index
		                        }, function( response ){
			                        if( response.error )
			                        {
				                        jsDisplayResult("error", "error", response.text );
			                        } else {
				                        jsDisplayResult("ok", "ok", response.text );
				                        KpiMapping.display(user)
			                        }	
		                        },"json"); 
		                        return false;
		                   }
		                });
		                
                    })
		        });
		        */
		        
		    },"html");	
            /*
		    $.getJSON("kpimapping?action=getKpi",{
		        status  : 4
		    }, function(kpis){
		        $.each(kpis.kpi, function( index, kpi){
		            $(".kpilist").append($("<option />",{value:index, text:kpi.name}))
		        });
		        		        
		    });	*/	    	    
		}		
		


};
