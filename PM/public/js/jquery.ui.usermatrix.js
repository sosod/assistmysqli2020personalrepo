$.widget("ui.evaluation", {
     
     options        : {
        user        : "",
        page        : "",
        section     : "",
        url         : "main.php?controller=usercomponent&action=getUserAll" 
     } ,
     
     _init          : function()
     {
       this._get();        
     } ,
     
     _create        : function()
     {
        var el = $(this.element)
        el.append($("<table />",{width:"100%"}).addClass("noborder")
           .append($("<tr />")
             .append($("<td />",{id:"userdetails"}).addClass("noborder"))
           )
           .append($("<tr />")
              .append($("<td />").addClass("noborder")
                .append($("<div />",{id:"usercomponents"}))
              )
           )
           .append($("<tr />")
             .append($("<td />",{id:"actions"}).addClass("noborder"))
           )
        )
     } ,
     
     _get           : function()
     {
       var self = this;
	  $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='public/images/loaderA32.gif' />"})
	   .css({position:"absolute", "z-index":"9999", top:"300px", left:"300px", border:"0px solid #FFFFF", padding:"5px"})
	  )
	  
	  $.getJSON(self.options.url, {
	      user       : self.options.user,
	      page       : self.option.page,
	      section    : self.options.section
	  },function(components) {
	     $("#loadingDiv").remove();
          if(!$.isEmptyObject(components.userdata))
          {
             $("#userdetails").html("");
             self._displayUserDetails(components.userdata, components.subordinates);
          } 
          if(components.error)
          {
                $("#usercomponents").html(components.text).addClass("ui-state-highlight").css({padding:"5px"})
                $("#activatecomponent").attr("disabled", "disabled");
          } else {
            if(!$.isEmptyObject(components.components))
            {
               $("#usercomponents").html("");
               $("#actions").html("")
               self._displayComponents(components.components, components.totalWeight, components.userdata  )
               self._displayFooter();  
            } else {
               $("#usercomponents").html( components.error );
                $("#activatecomponent").attr("disabled", "disabled");
            }          
          }          

	  });   
     } ,
     
     _displayUserDetails : function(userdata, subordinates)
     {
        var self = this;
        $("#userdetails").append($("<table />",{width:"100%"})
          .append($("<tr />")
             .append($("<th />",{html:"Select User :"}).css({"text-align":"center"}))
             .append($("<td />")
                .append($("<select />",{id:'user', name:'user'}))
             )
            .append($("<th />",{html:"Manager :"}).css({"text-align":"center"}))
            .append($("<td />",{html:userdata.managername}))
            .append($("<th />",{html:"Evaluation Year :"}).css({"text-align":"center"}))
            .append($("<td />",{html:userdata.start+" to "+userdata.end}))               
          )
          .append($("<tr />")
           .append($("<th />",{html:"Job Level :"}).css({"text-align":"center"}))
           .append($("<td />",{html:userdata.joblevel+"("+userdata.joblevelid+")"}))
           .append($("<th />",{html:"Evaluation Status :"}).css({"text-align":"center"}))
           .append($("<td />",{html:""}))
           .append($("<th />",{html:"Performance Category :"}).css({"text-align":"center"}))
           .append($("<td />",{html:userdata.performancecategory}))          
          )                  
        )
        
        if(!$.isEmptyObject(subordinates))
        {
          $.each(subordinates, function(sIndex, user){
             if(sIndex === userdata.tkid)
             {
               $("#user").append($("<option />",{text:user, value:sIndex, selected:"selected"}))
             } else {
               $("#user").append($("<option />",{text:user, value:sIndex}))
             }
          });
        }
        
        $("select#user").live("change", function(e){
            self.options.user = $(this).val();
            self._get();
            e.preventDefault()      
        });
     } ,
     
     _displayComponents            : function(components, totalWeight, user)
     {
        var self = this;
        $("#usercomponents").removeClass("ui-state").removeClass("ui-state-highlight");
        var li = $("<ul />",{id:"tabslist"})
        $.each(components, function(index, component){
            li.append($("<li />").append($("<a />",{href:"main.php?controller=component&action="+self.options.action+"&id="+index+"&user="+user.tkid, 
                                    html:"<span>"+component.name+" (<b>"+component.percentage+"</b>)</span>"}))
               )       
        });
         if(totalWeight < 100)
         {
	         li.append($("<li />")
	           .append($("<span />",{html:"Sum of components not equalt to 100%"}).addClass("ui-state-error").css({padding:"5px", "margin-top":"0px", position:"absolute"}))
	         )								      
	         $("#activatecomponent").attr("disabled", "disabled");
         }        
         $("#usercomponents").append(li).tabs("destroy").tabs({ ajaxOptions : { async: false }, spinner:"Loading..." });	
     } ,
     
     _displayFooter      : function()
     {
        var self = this;
        $("#actions").html("")  
        /*$("#actions").append($("<input />",{type:"button", name:"download", id:"download", value:"Download as PDF"}).click(function(e){
               document.location.href = "main.php?controller=usercomponent&action=download&user"+self.options.user+"&section="+self.options.section+"&page="+self.options.page;
               e.preventDefault();
          })
        )*/
        $("#actions").append($("<input />",{type:"hidden", name:"section", id:"section", value:self.options.section}))
        $("#actions").append($("<input />",{type:"hidden", name:"page", id:"page", value:self.options.page}))
     } ,
     
     destroy        : function()
     {
         var self = this;
         $(self.element).html("");
         $.Widget.prototye.destroy.apply(this, arguments);
     }
})
