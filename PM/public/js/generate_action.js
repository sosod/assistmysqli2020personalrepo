$(function(){

	
	$("#sortable").sortable().disableSelection(); 
     Report.getEvaluationyears();
	Report.getNationalKpa();
	Report.getIdpObjectives();
	Report.getWeighting();
	Report.getUsers();
	
	if($("#quickid").val() === undefined)
	{
	     $(".paction").attr("checked","checked"); 
	} else {
	     $.getJSON("main.php?controller=action&action=Report.getAQuickReport", {quickid : $("#quickid").val()}, function(response){
		     $("#report_description").text( response.description )
		     $("#report_name").val( response.name )
		     $("#report_title").val( response.data.report_title )
		     //check the legislatin fields that have been selected
		     $(".paction").each(function(){
			   var id = $(this).attr("id")
			   $.each(response.data.legislations, function(key  ,val){
				if("paction_"+key === id)
				{ 
				   $("#"+id).attr("checked", "checked");					
				}
			   });
		     });
		
		     $.each(response.data.value, function( index, val){
			    if(index == "paction_date_date")
			    {
				  $("#from_paction_date").val( val.from )
				  $("#to_paction_date").val( val.to )
			    } else {				
				  $("#"+index).val(val);
				  $("#"+index).text(val);
				  $("#"+index+" option[value='"+val+"']").attr("selected", "selected")								
			    }
		     });
		     $.each(response.data['match'], function(index , key){
			     $("#match_"+index+" option[value='"+key+"']").attr("selected", "selected")
		     });		
		
		     $("#group option[value='"+response.data.group_by+"']").attr("selected", "selected")
		
		     $.each(response['sort'], function(index , val){
			     $("#sortable").append($("<li />").addClass("ui-state-default")
							     .append($("<span />",{id:"__"+index}).addClass("ui-icon").addClass("ui-icon-arrowthick-2-n-s").addClass("sort"))
							     .append($("<input />",{type:"hidden", name:"sort[]", value:"__"+index}))
							     .append( val )
			     );			
		     });
		
	     }); 
	}

	
	$("#check_paction").click(function(){
		$(".paction").each(function(){
		   if( !$(this).is(":checked"))
		   {
			 $(this).attr("checked", "checked");
		   } 			
		})
		return false;
	});
	
	$("#invert_action").click(function(){
		$(".paction").each(function(){
		  if( $(this).is(":checked") )
		  {
			$(this).attr("checked", "")
		  } else {
			$(this).attr("checked", "checked")
		  }
		})
	   return false;
	})
});

Report         = {

	getEvaluationyears		: function()
	{
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
			$.each( responseData, function( index, evalYear) {
				$("#evaluation_year").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))					
			});								
		},"json");
	},
	
	getNationalKpa			: function()
	{
		$.getJSON("main.php?controller=nationalkpa&action=getAll",{
			evaluationyear 	: $("#evaluationyear").val()			
		}, function( responseData ){
			$.each( responseData, function( index, nationalkpa){
			   $("#nationalkpa").append($("<option />",{value:nationalkpa.id, text:nationalkpa.name}))				
			});						
		});
	} , 
	
	getIdpObjectives		: function()
	{
		$.getJSON("main.php?controller=nationalobjectives&action=getAll",{
			evaluationyear 	: $("#evaluationyear").val()			
		} ,function( responseData ){
			$.each( responseData, function( index, nationalobjectives){
				$("#idpobjectives").append($("<option />",{value:nationalobjectives.id, text:nationalobjectives.name}))				
			});						
		});	
	},
	
	getWeighting			: function()
	{
		$.getJSON("main.php?controller=weighting&action=getAll", {
			data	: { status : 1}
		}, function( weightings ){
			$.each( weightings, function(index, weighting ){
				$("#weighting").append($("<option />",{text:weighting.value+"("+weighting.weight+")", value:weighting.id}))
			});				
		});
	} ,

	getUsers                : function()
	{
		$.getJSON("main.php?controller=user&action=getAll",{
			evaluationyear 	: $("#evaluationyear").val()			
		} ,function( responseData ){
			$.each( responseData, function( index, user){
				$("#owner").append($("<option />",{value:user.tkid, text:user.username}))				
			});						
		});	
	}			

}

