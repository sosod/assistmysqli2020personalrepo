var Default  = Default || {};
var Default      = (function(){

     
     return {
     
          size           : function(obj)
          {
             var count = 0, key;
             if(!$.isEmptyObject(obj))
             {
               for(key in obj)
               {
                  if(obj.hasOwnProperty(key))
                  {
                    count++;
                  }
               }
             }
             return count;          
          } ,
          
          inObject       : function(value, object)
          {
              if(object.hasOwnProperty(value))
              {
                return true;
              }
             return false;
          } ,
          
          _inArray       : function(val, array)
          {
             for(var i = 0; i <=array.length; i++)
             {
               if(array[i] == val)
               {
                  return i;
               }
             }
            return -1;
          }
     
     }



})();
