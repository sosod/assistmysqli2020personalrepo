$(function(){

     Fixed.getEvaluationyears();
     Fixed.getUser();

});


var Fixed = {
     evaluationyear       : "",
     user                 : "",
     set                  : function(key, value)
     {
        var self = this;
        self[key] = value;
     } ,
     
     get                 : function(key)
     {    
          var self = this;
          return self[key];
     } ,  
          
	getEvaluationyears		: function()
	{
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
			$.each( responseData, function( index, evalYear) {
				$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))					
			});								
		},"json");
	} ,
	
	getUser            : function(userid)
	{
	    $(".users").remove();
		$.post("main.php?controller=user&action=getAll",function(users){	
	        if(!$.isEmptyObject(users))
	        {
	            $.each(users, function(index, user){
                    if(user.tkid == userid)
                    {
	                 $("#user").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname, selected:"selected"})) 
	               } else {
	                 $("#user").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}))    
	               }
	            });
	        }
		},"json"); 
		
	} 
};
