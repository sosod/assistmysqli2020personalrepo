$(function(){
	
	$("#addratingscale").hide();
	
	$("#evaluationyear").change(function() {
		var evaluationyear = $(this).val();
		$("#evaluationyearid").val( evaluationyear );
		if( evaluationyear == undefined || evaluationyear == ""){
			$("#addratingscale").fadeOut()
		} else {
			$("#addratingscale").fadeIn()
		}
		RatingScale.get( evaluationyear );	
		return false;
	});
	
	RatingScale.getEvaluationyears();

	
	$("#addnew").click(function(){
		
		if( $("#addnewdialog").length > 0)
		{
			$("#addnewdialog").remove();			
		}
		
		$("<div />",{id:"addnewdialog"})
		 .append($("<table />")
			 .append($("<tr />")
			   .append($("<th />",{html:"Numerical Rating:"}))
			   .append($("<td />")
				 .append($("<input />",{type:"text", name:"to", id:"to", value:""}))	   
			   )		 
			 )
			 .append($("<tr />")
			   .append($("<th />",{html:"Description:"}))
			   .append($("<td />")
				  .append($("<textarea />",{name:"description", id:"description"}))	   
			   )
			 )
			 .append($("<tr />")
			   .append($("<th />",{html:"Definition"}))
			   .append($("<td />")
				  .append($("<textarea />",{id:"definition", name:"description"}))	   
			   )
			 )
			 .append($("<tr />")
			   .append($("<th />",{html:"Colour Assigned to Rating Score:"}))
			   .append($("<td />")
				  .append($("<input />",{type:"text", name:"color", id:"color", value:"e2ddcf"}).addClass("color"))	   
			   )
			 )
			 .append($("<tr />")
			   .append($("<td />",{colspan:"2"})
				  .append($("<p />",{id:"message"}))	   
			    )
			 )
		  )
		 .dialog({
	 		autoOpen		: true, 
	 		modal			: true, 
	 		title			: "Add New Rating Scale", 
	 		width			: "600px", 
	 		position		: "top",
	 		buttons			: {
	 							"Save"		: function()
	 							{
                                             if( $("#to").val() == "") {
	 									$("#message").html("Please enter the numerical for this rating scale ");
	 									return false;
	 								} else if( isNaN( $("#to").val() )){
	 									$("#message").html("Please enter a valid numerical rating  number")
	 									return false;
	 								} else if( $("#description").val() == "") {
	 									$("#message").html("Please enter the description for this rating scale");
	 									return false;
	 								} else if( $("#definition").val() == ""){
	 									$("#message").html("Please enter the definition for this rating scale");
	 									return false;
	 								} else {
	 									RatingScale.save();			 									
	 								}
	 
	 							}  , 	
	 							
	 							"Cancel"		: function()
	 							{
	 								$(this),dialog("destroy");		 								
	 							}
              } , 
              
              open				: function( event, ui)
              {
	 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
	 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
	 				var saveButton = buttons[0];
	 				$(saveButton).css({"color":"#090"});	            	  
              }
		 })
		 
		 var myPicker = new jscolor.color(document.getElementById('color'), {})
		 myPicker.fromString('99FF33')  // now you can access API via 'myPicker' variable
		 		 
		return false;
	});
	
});


var RatingScale			= {
		
	getEvaluationyears		: function()
	{
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
			$.each( responseData, function( index, evalYear) {
				$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))					
			});								
		},"json");
	} , 
	
	get			: function( evaluationyear )
	{
		$(".rating").remove();
		$.getJSON("main.php?controller=ratingscale&action=getAll", {
			evaluationyear 	: evaluationyear
		},function( responseData ){
			if( $.isEmptyObject(responseData) )
			{
				$("#table_ratingscale").append($("<tr />").addClass("rating")
				  .append($("<td />",{colspan:"7", html:"There are no rating scale setup for this evaluation year"}))		
				)
			} else {
				$.each( responseData, function( index, rating){
					RatingScale.display( rating )				
				});	
			}
		});
	} , 
	
	save		: function()
	{
		var evaluationYear = $("#evaluationyearid").val()
		$.post("main.php?controller=ratingscale&action=saveRatingScale", {
			data : {
					ratingto		     : $("#to").val(),
					description		: $("#description").val(),
					definition		: $("#definition").val(), 
					evaluationyear	     : evaluationYear,
					color			: $("#color").val()
			}			
		}, function( response ){
			if( response.error )
			{
				$("#message").html( response.text )
			} else {				
				RatingScale.get( evaluationYear );
				jsDisplayResult("ok", "ok", response.text );
				$("#addnewdialog").dialog("destroy");
				$("#addnewdialog").remove();	
			}
		},"json")
		
	} , 
		
	display		: function( rating )
	{
		$("#table_ratingscale").append($("<tr />").addClass("rating")
		  .append($("<td />",{html:rating.id}))		
		  .append($("<td />",{html:rating.ratingto}))
		  .append($("<td />",{html:rating.description}))
		  .append($("<td />",{html:rating.definition}))
		  .append($("<td />")
			 .append($("<span />",{html:rating.color}).css({"background-color":"#"+rating.color, padding:"5px"}))	  
		  )
		  .append($("<td />",{html:((rating.status & 1) == 1 ? "Active" : "Inacitve")}))
		  .append($("<td />")
			 .append($("<input />",{type:"button", name:"edit_"+rating.id, id:"edit_"+rating.id, value:"Edit"}))	  
		  )
		)
		
		$("#edit_"+rating.id).live("click", function(){
			
			if( $("#editdialog").length > 0 )
			{
				$("#editdialog").remove();				
			}
			
			$("<div />",{id:"editdialog"})
			 .append($("<table />")
				 .append($("<tr />")
				   .append($("<th />",{html:"Numerical Rating:"}))
				   .append($("<td />")
					 .append($("<input />",{type:"text", name:"to", id:"to", value:rating.ratingto}))	   
				   )		 
				 )
				 .append($("<tr />")
				   .append($("<th />",{html:"Description:"}))
				   .append($("<td />")
					  .append($("<textarea />",{name:"description", id:"description", text:rating.description}))	   
				   )
				 )
				 .append($("<tr />")
				   .append($("<th />",{html:"Definition"}))
				   .append($("<td />")
					  .append($("<textarea />",{id:"definition", name:"definition", text:rating.definition}))	   
				   )
				 )
				 .append($("<tr />")
				   .append($("<th />",{html:"Colour Assigned to Rating Score:"}))
				   .append($("<td />")
					  .append($("<input />",{type:"text", name:"color", id:"color", value:rating.color}).addClass("color"))	   
				   )
				 )				 
				 .append($("<tr />")
				   .append($("<td />",{colspan:"2"})
					  .append($("<p />",{id:"message"}))	   
				    )
				 )
			  )
			 .dialog({
		 		autoOpen		: true, 
		 		modal			: true, 
		 		title			: "Edit Rating Scale", 
		 		width			: "600px", 
		 		position		     : "top",
		 		buttons			: {
		 							"Update"		: function()
		 							{
                                                 if( $("#to").val() == "") {
		 									$("#message").html("Please enter the numerical rating for this rating scale ");
		 									return false;
		 								} else if( isNaN( $("#to").val() )){
		 									$("#message").html("Please enter a valid numerical rating ")
		 									return false;
		 								} else if( $("#description").val() == ""){
		 									$("#message").html("Please enter the description for this rating scale");
		 									return false;
		 								} else if( $("#definition").val() == ""){
		 									$("#message").html("Please enter the definition for this rating scale");
		 									return false;
		 								} else {
		 									RatingScale.updateRating( rating.id );			 									
		 								}
		 
		 							}  , 	
		 							
		 							"Cancel"		: function()
		 							{
		 								$(this).dialog("destroy");
		 								$("#editdialog").remove();			 								
		 							} , 
		 							
		 							"Delete"		 : function()
		 							{
		 								RatingScale.deleteRating( rating.id )				 								
		 							} , 
		 							
		 							"Deactivate"		: function()
		 							{
		 								RatingScale.updateStatus( rating.id , 0)				 								
		 							} , 
		 							
		 							"Activate"				: function()
		 							{
		 								RatingScale.updateStatus( rating.id, 1 );				 								
		 							}
	              } , 
	              
	              open				: function( event, ui)
	              {
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
		 				var saveButton = buttons[0];
		 				var deleteButton = buttons[2]
		 				var deactivateButton = buttons[3];
		 				var activateButton   = buttons[4];
		 				$(saveButton).css({"color":"#090"});
		 				$(deleteButton).css({"color":"red", display:(rating.used ? "none" : "inline")});
		 				$(deactivateButton).css({"color":"red", display:(rating.used ? ((rating.status & 1) == 1 ? "inline" : "none") : "none")});
		 				$(activateButton).css({"color":"#090", display:(rating.used ? ((rating.status & 1) == 1 ? "none" : "inline") : "none")});  		            	  
	              }
			 })
			 var myPicker = new jscolor.color(document.getElementById('color'), {})
			 myPicker.fromString( rating.color )  // now you can access API via 'myPicker' variable			 
			 			 
			return false;
		});
		
	} , 	
	
	deleteRating				: function( id )
	{
		if( confirm("Are you sure you want to delete this rating scale"))
		{	
			RatingScale.updateStatus( index, 2);
		}
	} , 
	
	updateRating				: function( id )
	{
		$.post("main.php?controller=ratingscale&action=updateRatingScale",{
			id 		: id, 
			data : {
				ratingto		: $("#to").val(),
				description		: $("#description").val(),
				definition		: $("#definition").val(), 
				evaluationyear	: $("#evaluationyear :selected").val(),
				color			: $("#color").val()
			}	
		},function( response ){
			if( response.error )
			{
				$("#message").html( response.text )
			} else {				
				RatingScale.get( $("#evaluationyearid").val() );
			       if(response.updated)
			       {
				     jsDisplayResult("info", "info", response.text );
			       } else {
				     jsDisplayResult("ok", "ok", response.text );					
			       } 	
				$("#editdialog").dialog("destroy");
				$("#editdialog").remove();	
			}
		},"json")
		
	} , 
	
	updateStatus				: function(id, status )
	{
		$.post("main.php?controller=ratingscale&action=updateRatingScale", {
			id 		: id, 
			data	: { status : status }
		}, function( response ){
			if( response.error )
			{
				$("#message").html( response.text )
			} else {
				RatingScale.get( $("#evaluationyearid").val() );
			       if(response.updated)
			       {
				     jsDisplayResult("info", "info", response.text );
			       } else {
				     jsDisplayResult("ok", "ok", response.text );					
			       } 	
				$("#editdialog").dialog("destroy");
				$("#editdialog").remove();	
			}
		},"json");
		
	}
		
		
};
