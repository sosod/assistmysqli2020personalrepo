$(function(){
	Menu.get();
	
});

var Menu 		= {
		
     get			: function()
     {
          $.getJSON("main.php?controller=menu&action=getMenu", function( response ) {
               $.each( response, function( index, menu) {
	               $("#menutable").append($("<tr />",{id:"tr_"+menu.id})
		               .append($("<td />",{html:menu.id}))
		               .append($("<td />",{html:menu.ignite_terminology}))
		               .append($("<td />")
		                 .append($("<input />",{type:"text", name:"menuitem_"+menu.id, id:"menuitem_"+menu.id, value:menu.client_terminology,size:"40"}))
		               )
		               .append($("<td />")
		                 .append($("<input />",{type:"button", name:"update_"+menu.id, id:"update_"+menu.id, value:"Update"}))		
		               )
	               )			
	
	               $("#tr_"+menu.id).hover(function(){
		               $(this).addClass("gray");						
	               }, function(){
		               $(this).removeClass("gray");						
	               });
	
	
	               $("#update_"+menu.id).on("click", function() {
		               jsDisplayResult("info", "info", "Updating  ...  <img src='public/images/loaderA32.gif' />");
		               $.post("main.php?controller=menu&action=updateMenu", {
			               client_terminology : $("#menuitem_"+menu.id).val(),
			               id	 			   : menu.id
		               }, function( response ) {
			               if( response.error ){
				               jsDisplayResult("error", "error", response.text);
			               } else {
				            if(response.updated)
				            {
				               jsDisplayResult("info", "info", response.text );
				            } else {
				               jsDisplayResult("ok", "ok", response.text );
				            }
			               }						
		               },"json");
		               return false;
	               });
               });
          });
	} , 
	
	update		: function()
	{
	}		
	
	
		
}; 	
