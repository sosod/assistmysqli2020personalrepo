$(function(){

	Evaluationyears.get();
	
	$(".date").datepicker({
		showOn			: "both",
		buttonImage 	: "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		: true,
		changeYear		: true,
		dateFormat 		: "dd-M-yy",
		altField		: "#startDate",
		altFormat		: 'd_m_yy'
	});
		
	$("#savechanges").click(function(){
		Evaluationyears.updateEvaluationYearPeriods( $("#evaluationyearid").val() );
		return false;
	});
	
	$("#add").click(function(){
		
		if($("#addnewdialog").length > 0)
		{
			$("#addnewdialog").remove();
		}

		$("<div />",{id:"addnewdialog"})
		  .append($("<table />",{width:"100%"})
			 .append($("<tr />")
			   .append($("<th />",{html:"Last Day of Evaluation Year"}))
			   .append($("<th />",{html:"Start Date:"}))
			   .append($("<th />",{html:"End Date:"}))
			 )
			 .append($("<tr />")
			   .append($("<td />")
			      .append($("<span />",{html:"<b>Year Ending</b>:"}))
				  .append($("<input />",{id:"lastday", name:"lastday", value:""}).addClass("datepicker"))	   
			   )			   
			   .append($("<td />")
				 .append($("<input />",{id:"start", id:"start", value:""}).addClass("datepicker"))	   
			   )
			   .append($("<td />")
				 .append($("<input />",{id:"end", name:"end", value:""}).addClass("datepicker"))   
			   )
			 )
			 .append($("<tr />")
			   .append($("<td />",{colspan:"3"})
				   .append($("<span />",{id:"message"}).css({"padding":"5px"}))
			   )		 
			 )
		  )
		  .dialog({
			  		title 	: "New Evaluation Year", 
			  		autoOpen: true, 
			  		modal 	: true, 
			  		width	: "600px",
			  		position: "top",			  		
			  		buttons : {
			  					"Save"		: function()
			  					{
			  						if( $("#lastday").val() == "")
			  						{
										$("#message").addClass("ui-state-error").html("Please enter the last day of the financial year");
			  							return false;
			  						} else if( $("#start").val() == "") {
			  							$("#message").addClass("ui-state-error").html("Please enter the start day of the financial year");			 
			  							return false;
			  						} else if( $("#end").val() == "") {
			  							$("#message").addClass("ui-state-error").html("Please enter the end day of the financial year");			 
			  							return false;
			  						} else {
			  							Evaluationyears.save();
			  						}
			  					} ,
			  					
			  					"Cancel"	: function()
			  					{			  						
			  						$(this).dialog("destroy");
			  					}
		  			} , 
		  			
		  			open		: function( event, ui)
		  			{
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		  			}
		  });
		
		$(".datepicker").datepicker({
			showOn			: "both",
			buttonImage 	: "/library/jquery/css/calendar.gif",
			buttonImageOnly	: true,
			changeMonth		: true,
			changeYear		: true,
			dateFormat 		: "dd-M-yy",
			altField		: "#startDate",
			altFormat		: 'd_m_yy'
		}).attr("readonly", "readonly");				
		return false;
	});

});

var Evaluationyears    = {
		
		get			: function()
		{
			$(".evalyear").remove()
			$.getJSON("main.php?controller=evaluationyears&action=getAll",{
				section : $("#section").val()
			}, function( responseData ){
				$.each( responseData , function( index, evaluationyear){
					Evaluationyears.display( evaluationyear )					
				});								
			});
		} , 
		
		save		: function()
		{
			$(".dialogmessage").show();
			$("#message").html("Saving evaluation years . . .");
			$.post("main.php?controller=evaluationyears&action=saveEvaluationYears", {
				lastday	: $("#lastday").val(), 
				end		: $("#end").val(),
				start	: $("#start").val()
			}, function( response ){
				if( response.error )
				{  
					$("#message").addClass("ui-state-error").html( response.text );					
				} else {
					Evaluationyears.get();
					jsDisplayResult("ok", "ok", response.text );
					$("#addnewdialog").dialog("destroy");
					$("#addnewdialog").remove();
				}				
			},"json");
		} , 
		
		display		: function( data )
		{
			var self    = this;
			var section = $("#section").val();
			$("#table_evaluationyears").append($("<tr />",{id:"tr_"+data.id}).addClass("evalyear")
			    .append($("<td />",{html:data.id}))
			    .append($("<td />",{html:data.lastday}))
			    .append($("<td />",{html:data.start}))
			    .append($("<td />",{html:data.end}))
			    .append($("<td />",{html:((data.status & 8) == 8 ? "<b>Closed</b>" : "<b>Opened</b>")}).css({"display" : (section == "admin" ?  "table-cell" : "none")}))
			    .append($("<td />").css({"display" : (section == "admin" ?  "none" : "table-cell")})
			      .append($("<input />",{type:"button", name:"edit_"+data.id, id:"edit_"+data.id, value:" Edit "})
 					.click(function(){
 						self._editEvaluationYear(data);
 					})
			      )		
			    )
			    .append($("<td />").css({"display" : (section == "admin" ?  "none" : "table-cell")})
			      .append($("<input />",{type:"button", name:"configure_"+data.id, id:"configure_"+data.id, value:" Configure Evaluation Periods "}))		
			    )
			    .append($("<td />").css({"display" : (section == "admin" ?  "table-cell" : "none")})
			      .append($("<input />",{type:"button", name:"close_"+data.id, id:"close_"+data.id, value:((data.status & 8) == 8 ? "Open" : "Close")}))
			    )			    
			)
				
			$("#configure_"+data.id).live("click", function(){
				dataText = JSON.stringify( data );
				document.location.href = "main.php?controller=evaluationyears&action=editevaluationyears&id="+data.id+"&parent=4&folder=setup";
				//Evaluationyears.update( data )				
				return false;
			});
			
			$("#close_"+data.id).click(function(){
				$("<div />",{html:"You are "+((data.status & 8) == 8 ? "opening" : "closing")+" this evaluation year", id:"closeevalyeardialog_"+data.id})
				  .dialog({
				  		autoOpen	: true,
				  		modal		: true,
				  		title		: "Closing evaluation year",
				  		buttons		: {
				  							"Close"		: function()
				  							{
				  					
				  								Evaluationyears.closeEvaluationYear( data.id )
				  							} ,  
				  							
				  							"Open"		: function()
				  							{
				  								Evaluationyears.openEvaluationYear( data.id )					  								
				  							} ,
				  							
				  							"Cancel"	: function()
				  							{
				  								$(this).dialog("destroy");					  								
				  							}
			  						} , 
			  			"open"		: function( event , ui)
			  			{
  			 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
  			 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
  			 				var closeButton = buttons[0];
  			 				var openButton 	= buttons[1];
  			 				$(closeButton).css({"color":"#090", display:((data.status & 8 ) != 8  ? "inline" : "none")});
  			 				$(openButton).css({"color":"#090", display:((data.status & 8) == 8 ? "inline" : "none")});
			  			}
				  })
				return false;
			});
			
			
		} , 
		
		_editEvaluationYear		: function( data )
		{
			if($("#editdialog").length > 0)
			{
				$("#editdialog").remove();
			}

			$("<div />",{id:"editdialog"}) 
			  .append($("<form />",{id:"editevaluationyears_form"})
				  .append($("<table />",{width:"100%"})
					 .append($("<tr />")
					   .append($("<th />",{html:"Last Day of financial year"}))
					   .append($("<th />",{html:"Start Date"}))
					   .append($("<th />",{html:"End Date"}))
					 )
					 .append($("<tr />")
					   .append($("<td />")
						 .append($("<span />",{html:"<b>Year Ending</b>:"}))
						 .append($("<input />",{id:"lastday", name:"lastday", value:data.lastday}).addClass("datepicker"))	   
					   )						 
					   .append($("<td />")
						 .append($("<input />",{id:"start", id:"start", value:data.start}).addClass("datepicker"))	   
					   )
					   .append($("<td />")
						 .append($("<input />",{id:"end", name:"end", value:data.end}).addClass("datepicker"))	   
					   )				   
					 )
					 .append($("<tr />")
					   .append($("<td />",{colspan:"3"})
						  .append($("<span />",{id:"message"}).css({"padding":"5px"}))	   
					   )		 
					 )
				  )
			  )
			  .dialog({
				  		title 		: "Edit Evaluation Year", 
				  		autoOpen	: true, 
				  		modal 		: true, 
				  		width		: "700px",
				  		position	: "top",
				  		buttons 	: {
					  					"Save Changes"		: function()
					  					{
					  						if( $("#lastday").val() == "")
					  						{
												$("#message").addClass("ui-state-error").html("Please enter the last day of the financial year");
					  							return false;
					  						} else if( $("#start").val() == "") {
					  							$("#message").addClass("ui-state-error").html("Please enter the start day of the financial year");			 
					  							return false;
					  						} else if( $("#end").val() == "") {
					  							$("#message").addClass("ui-state-error").html("Please enter the end day of the financial year");			 
					  							return false;
					  						} else {
					  							Evaluationyears.updateEvaluationYears( data.id );
					  						}
					  					} ,
					  					
					  					"Cancel"	: function()
					  					{			  						
					  						$(this).dialog("destroy");
					  					} ,				  					
					  					
					  					"Delete"	: function()
					  					{
					  						Evaluationyears.deleteEvaluationYears( data.id );
					  					} , 
					  					
					  					"Deactivate"	: function()
					  					{
					  						Evaluationyears.updateStatus( data.id, 0);				  						
					  					} , 
					  					
					  					"Activate"		: function()
					  					{
					  						Evaluationyears.updateStatus( data.id, 1);				  						
					  					} 
			  			} , 
			  			open			: function( event, ui)
			  			{
			 				var dialog 			 = $(event.target).parents(".ui-dialog.ui-widget");
			 				var buttons 		 = dialog.find(".ui-dialog-buttonpane").find("button");	
			 				var saveButton 		 = buttons[0];
			 				var deleteButton 	 = buttons[2]
			 				var deactivateButton = buttons[3];
			 				var activateButton   = buttons[4];
			 				$(saveButton).css({"color":"#090"});
			 				$(deleteButton).css({"color":"red", display:(data.used ? "none" : "inline")});
			 				$(deactivateButton).css({"color":"red", display:(data.used ? ((data.status & 1) == 1 ? "inline" : "none") : "none")});
			 				$(activateButton).css({"color":"#090", display:(data.used ? ((data.status & 1) == 1 ? "none" : "inline") : "none")});
			  			} ,
			  			close 			: function()
			  			{
			  				$(this).dialog('destroy');
			  			}
			  });	

				$(".datepicker").datepicker({
					showOn			: "both",
					buttonImage 	: "/library/jquery/css/calendar.gif",
					buttonImageOnly	: true,
					changeMonth		: true,
					changeYear		: true,
					dateFormat 		: "dd-M-yy",
					altField		: "#startDate",
					altFormat		: 'd_m_yy'
				}).attr("readonly", "readonly");				
		} ,  
		
		updateChanges			: function( index )
		{
			jsDisplayResult("info", "info", "Saving evalution years . . . <img src='public/images/loaderA32.gif' /> " );
			$.post("main.php?controller=evaluationyears&action=updateChanges",{
				data	: $("#evaluationfrequencies_form").serializeArray(),
				id		: index
			}, function( response ) {
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text );
				} else {
				   if(response.updated)
				   {
					jsDisplayResult("info", "info", response.text );
				   } else {
					jsDisplayResult("ok", "ok", response.text );					
				   }		
			        Evaluationyears.get(); 					     
				   $("#configureevaluationyears").dialog("destroy");
				}				
			},"json");
			
		} , 
		
		updateEvaluationYears			: function( index )
		{
			$("#message").addClass("ui-state-info").html("Updating evalution years . . . <img src='public/images/loaderA32.gif' /> " );
			$.post("main.php?controller=evaluationyears&action=updateEvaluationYear",{
				id	    : index, 
				lastday	: $("#lastday").val(), 
				end		: $("#end").val(),
				start	: $("#start").val()
			}, function( response ) {
				if( response.error )
				{
				   $("#message").removeClass("ui-state-info").addClass("ui-state-error").html(response.text);
				} else {
				   if(response.updated)
				   {
					  jsDisplayResult("info", "info", response.text );
				   } else {
					  jsDisplayResult("ok", "ok", response.text );					
				      Evaluationyears.get(); 	
				   }		
				   $("#editdialog").remove();	     
				}
			},"json");
		} , 

		updateEvaluationYearPeriods			: function( index )
		{
			jsDisplayResult("info", "info", "Updating ... <img src='public/images/loaderA32.gif' /> " );
			$.post("main.php?controller=evaluationyears&action=updateEvaluationYearPeriods",{
				id	    : index, 
				data	: $("#editevaluationyears_form").serializeArray(), 
			}, function( response ) {
				if( response.error )
				{
				   jsDisplayResult("error", "error", response.text );
				} else {
				   if(response.updated)
				   {
					  jsDisplayResult("info", "info", response.text );
				   } else {
					  jsDisplayResult("ok", "ok", response.text );					
				   }		    
				}
			},"json");
		} , 		
		
		closeEvaluationYear			: function( index )
		{
			jsDisplayResult("info", "info", "Closing evalution years . . . <img src='public/images/loaderA32.gif' /> " );
			$.post("main.php?controller=evaluationyears&action=close",{
				id	    : index 
			}, function( response ) {
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text );
					$("#closeevalyeardialog_"+index).dialog("destroy");
				} else {
					Evaluationyears.get();
				     if(response.updated)
				     {
					     jsDisplayResult("info", "info", response.text );
				     } else {
					     jsDisplayResult("ok", "ok", response.text );					
				     }	
					$("#closeevalyeardialog_"+index).dialog("destroy");
					$("#closeevalyeardialog_"+index).remove();
				}
			},"json");
		} , 
		
		openEvaluationYear			: function( index )
		{
			jsDisplayResult("info", "info", "Opening evalution years . . . <img src='public/images/loaderA32.gif' /> " );
			$.post("main.php?controller=evaluationyears&action=open",{
				id	    : index 
			}, function( response ) {
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text );
					$("#closeevalyeardialog_"+index).dialog("destroy");
				} else {
					Evaluationyears.get();
				     if(response.updated)
				     {
					     jsDisplayResult("info", "info", response.text );
				     } else {
					     jsDisplayResult("ok", "ok", response.text );					
				     }	
					$("#closeevalyeardialog_"+index).dialog("destroy");
					$("#closeevalyeardialog_"+index).remove();
				}
			},"json");
		} , 
		
		
};
