$.widget("ui.componentmatrix",{
	
	options			: {
			componentId 	: "",
			evaluationyear	: "",
			tableId 		: "table_componentmatrix",
			start			: 0,
			limit			: 10,
			current			: 1,
			total			: 0, 
			allowedColumns	: [], 
			categories		: []
	} , 
	
	_init			: function()
	{
		this._getDetailedComponent();		
	} , 
	
	_create			: function()
	{

	} , 
	
	_getDetailedComponent		: function()
	{
		var self = this;
		$.getJSON("main.php?controller=component&action=getComponents", {
			componentId 	: self.options.componentId, 
			evaluationyear	: self.options.evaluationyear, 
			section        : 'setup'
		}, function( componentData ) {

			$(self.element).append($("<form />",{id:"performancematrix", name:"performancematrix"})
			  .append($("<table />",{id:self.options.tableId}))		
			)
			
			self.options.categories = componentData.categories;
			
			self._displayHeaders(componentData.headers, componentData.categories, componentData.component );
			/*self._display( componentData.kpaData, componentData.kpi );			
			*/
			self._display(componentData.kpa, componentData.kpi, componentData.actions, componentData.configs );			
		});
		
	} , 
	
	_display		: function( kpas , kpis, actions, config)
	{
		var self = this;
		$.each(kpas, function(kpaId, kpa){
			var kpatr = $("<tr />",{id:"kpa_"+kpaId});
			kpatr.append($("<td />")
			  .append($("<a />",{href:"#", id:"viewkpi_"+kpaId})
				.append($("<span />").addClass("ui-icon").addClass("ui-icon-plus"))	  
			  )		
			)
			kpatr.append($("<td />"))
			kpatr.append($("<td />"))		
		     $.each(kpa, function(kpaKey, kpaval){
		         if($.inArray(kpaKey, self.options.allowedColumns) > -1)
		         {
		           kpatr.append($("<td />",{html:kpaval}))
		         }
		     }); 
		     
		     if(!$.isEmptyObject(self.options.categories))
			{
				$.each( self.options.categories, function( i, cat) {
					kpatr.append($("<td />")
					  .append($("<select />",{id:cat.id+"_kpa_"+kpaId, name:cat.id+"_kpa_"+kpaId})
						.append($("<option />",{value:"1", text:"Yes"}))
						.append($("<option />",{value:"0", text:"No"}))							  
					  )		
					)				
				});	
			} 
		    
		    $("#"+self.options.tableId).append(kpatr);
		    //display the kpis
		    if(!$.isEmptyObject(kpis[kpaId]))
		    {
		        $.each(kpis[kpaId], function(kpiId, kpi){
		          var kpitr = $("<tr />", {id:"kpi_"+kpiId}).css({"background-color":"#EEEEEE"});
			     kpitr.append($("<td />"))
			     kpitr.append($("<td />")
			       .append($("<a />",{href:"#", id:"viewkpi_"+kpiId})
				     .append($("<span />").addClass("ui-icon").addClass("ui-icon-plus"))	  
			       )		
			     )			     
			     kpitr.append($("<td />"))				    
			     $.each(kpi, function(kpiKey, kpival){
		              if($.inArray(kpiKey, self.options.allowedColumns) > -1)
		              {			          
			            kpitr.append($("<td />",{html:kpival}));			     
			         }
			     });
		          if(!$.isEmptyObject(self.options.categories))
			     {
				     $.each( self.options.categories, function( i, cat) {
					     kpitr.append($("<td />")
					       .append($("<select />",{id:cat.id+"_kpi_"+kpiId, name:cat.id+"_kpi_"+kpiId})
						     .append($("<option />",{value:"1", text:"Yes"}))
						     .append($("<option />",{value:"0", text:"No"}))							  
					       )		
					     )					
				     });	
			     }
     		     $("#"+self.options.tableId).append(kpitr); 	//append the kpis to the table
     		     
     		     //check if there are actions , if so display actions
                    if(actions[kpaId] != undefined){
                         
          		     if(!$.isEmptyObject(actions[kpaId][kpiId]))
          		     {
          		         $.each(actions[kpaId][kpiId], function(actionId, action){
               		          var actionTr = $("<tr />",{id:"action_"+actionId}).css({"background-color":"#EEEFFF"});
			                    actionTr.append($("<td />"))
			                    actionTr.append($("<td />"))			          
			                    actionTr.append($("<td />")
			                      .append($("<a />",{href:"#", id:"viewkpi_"+kpiId})
				                    .append($("<span />").addClass("ui-icon").addClass("ui-icon-plus"))	  
			                      )		
			                    )			     
               		          $.each(action ,function(actionKey, actionval){
               		               if($.inArray(actionKey, self.options.allowedColumns) > -1)
               		               {
               		                    actionTr.append($("<td />",{html:actionval}));
               		               }          		          
               		          });          		          
		                         if(!$.isEmptyObject(self.options.categories))
			                    {
				                    $.each(self.options.categories, function( i, cat) {
					                    actionTr.append($("<td />")
					                      .append($("<select />",{id:cat.id+"_action_"+actionId, name:cat.id+"_action_"+actionId})
						                    .append($("<option />",{value:"1", text:"Yes"}))
						                    .append($("<option />",{value:"0", text:"No"}))							  
					                      )		
					                    )					     					                    					
				                    });	
			                    }          		            
               		         $("#"+self.options.tableId).append(actionTr);     
          		         });     		     
          		     }     
     		     }		     		     
		        });
		    } 		
		});
		
		
		if(!$.isEmptyObject(config))
		{
		     $.each(config['kpa'], function(kpa_id, categories){
                  $.each(categories, function( catId, kpaCatVal){
                     $("#"+catId+"_kpa_"+kpa_id+" option[value='"+kpaCatVal+"']").attr("selected", "selected")
                  })
		     });
		     
		     $.each(config['kpi'], function(kpi_id, categories){
                  $.each(categories, function( catId, kpiCatVal){
                     $("#"+catId+"_kpi_"+kpi_id+" option[value='"+kpiCatVal+"']").attr("selected", "selected")
                  })
		     });		     
		     
		     $.each(config['action'], function(action_id, categories){
                  $.each(categories, function( catId, actionCatVal){
                     $("#"+catId+"_action_"+action_id+" option[value='"+actionCatVal+"']").attr("selected", "selected")
                  });
		     });		     		     
		}
		
		
		
		$(self.element).append($("<table />").addClass("noborder")
		  .append($("<tr />")
			.append($("<td />").addClass("noborder")
			  .append($("<input />",{type:"button", name:"configure", id:"configure", value:"Save Changes"}))		
			)	  
		  )		
		)
		
		$("#configure").live("click", function(){
			jsDisplayResult("info", "info", "Updating performance categories . . .  <img src='public/images/loaderA32.gif' />");
			$.post("main.php?controller=component&action=updateComponentCategories",{
				data 	: $("#performancematrix").serialize(),
				id 		: self.options.componentId
			}, function( response ){
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else {
					jsDisplayResult("ok", "ok", response.text);
				}							
			},"json");
			return false;
		});
		
	}  ,
	
	_displayHeaders		: function( headers, categories , component)
	{
		var self = this;
		var trH = $("<tr />").append($("<td />",{colspan:5, html:"<b>"+component.name+"</b>"}))

		var tr 	= $("<tr />") 
        tr.append($("<td />",{html:"<b><small>K</small><br /><small>P</small><br /><small>A</small><br /></b>"}).css({width:"1em", color:"blue"}))
        tr.append($("<td />",{html:"<small><b>K<br />P<br />I</b></small>"}).css({color:"blue"})) 
        tr.append($("<td />",{html:"<small><b>A<br />C<br />T<br />I<br />O<br />N</b></small>"}).css({color:"blue"})) 
		$.each( headers, function( key , head) {
			if( $.inArray(key , self.options.allowedColumns ) > -1)
			{
				tr.append($("<th />",{html:head}));					
			}						
		});
          var cols = Default.size(categories);
		tr.append($("<th />",{colspan:cols, html:"Performance Categories"}))
		$.each( categories, function( i , cat){
			trH.append($("<td />",{html:"<b>"+cat.name+"</b>"}))			
		});
		
		$("#"+self.options.tableId).append( tr )
		$("#"+self.options.tableId).append( trH )

	}
	
	
	
})
