$(function(){

     CreationPeriod.getEvaluationyears();
	
	$("#creationperiod").hide();
	
	$("#evaluationyear").change(function(){
		var evaluationyear = $(this).val();
		$("#evaluationyearid").val( evaluationyear )
		if(evaluationyear == ""  || evaluationyear == undefined)
		{
		     $(".cre").show();
			$("#creationperiod").fadeOut();
		} else {
		     $(".cre").hide();
			$("#creationperiod").fadeIn();
		}
		CreationPeriod.get( evaluationyear );
		return false;
	});
	
	$("#save").click(function(e){
	  CreationPeriod.save();
	  e.preventDefault();
	});
	

});


var CreationPeriod = {

	getEvaluationyears		: function()
	{
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
			$.each( responseData, function( index, evalYear) {
				$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))					
			});								
		},"json");
	} , 
	
	save                     : function()
	{
	   jsDisplayResult("info", "info", "saving . . . " );
	   $.post("main.php?controller=creationperiod&action=save", {
	     start_date : $("#start_date").val(),
	     end_date   : $("#end_date").val(),
	     evaluationyear : $("#evaluationyearid").val()
	   }, function(response) {
		  if(response.error)
		  {
			jsDisplayResult("error", "error", response.text );
		  } else {
			jsDisplayResult("ok", "ok", response.text );	
		  }			
	   },"json");
	} ,
	
	update                   : function()
	{
	   jsDisplayResult("info", "info", "updating ..." );
	   $.post("main.php?controller=creationperiod&action=update", {
	     start_date : $("#start_date").val(),
	     end_date   : $("#end_date").val(),
	     id         : $("#period_id").val()
	   }, function(response) {
		  if(response.error)
		  {
			jsDisplayResult("error", "error", response.text );
		  } else {
			jsDisplayResult("ok", "ok", response.text );	
		  }			
	   },"json");
	} ,
	
	get                      : function(evaluationyear)
	{
	  var self = this;
	  $(".creationperiod").remove();
	  $.getJSON("main.php?controller=creationperiod&action=getAll", {evaluationyear : evaluationyear}, function(response){
	      if(!$.isEmptyObject(response))
	      {
              self._displayEdit(response);
	      } else {
	         self._displayNew();
	      }
	  });
	
	} ,

	_displayEdit             : function(period)
	{
	   $("#table_creationperiod").append($("<tr />").addClass("creationperiod")
	     .append($("<td />",{html:period.id}))
	     .append($("<td />")
	       .append($("<input />",{type:"text", name:"start_date", id:"start_date", value:period.start_date, readonly:"readonly"}).addClass("datepicker")) 
	     )
	     .append($("<td />")
	       .append($("<input />",{type:"text", name:"end_date", id:"end_date", value:period.end_date, readonly:"readonly"}).addClass("datepicker"))
	     )	     
	     .append($("<td />")
	       .append($("<input />",{type:"button", name:"edit_period", id:"edit_period", value:"Save Changes"})
	         .click(function(e){
	            CreationPeriod.update();
	            e.preventDefault();
	         })
	       )
	       .append($("<input />",{type:"hidden", name:"period_id", id:"period_id", value:period.id}))
	     )	     	     
	   )
	     $(".datepicker").datepicker({
		     showOn			: "both",
		     buttonImage 	: "/library/jquery/css/calendar.gif",
		     buttonImageOnly	: true,
		     changeMonth		: true,
		     changeYear		: true,
		     dateFormat 		: "dd-M-yy",
		     altField		: "#startDate",
		     altFormat		: 'd_m_yy'
	     });	
	} ,
	
	_displayNew              : function()
	{
	   $("#table_creationperiod").append($("<tr />").addClass("creationperiod")
	     .append($("<td />")
	       .append($("<input />",{type:"text", name:"start_date", id:"start_date", value:"", readonly:"readonly"}).addClass("datepicker")) 
	     )
	     .append($("<td />")
	       .append($("<input />",{type:"text", name:"end_date", id:"end_date", value:"", readonly:"readonly"}).addClass("datepicker"))
	     )	     
	     .append($("<td />")
	       .append($("<input />",{type:"button", name:"save_period", id:"save_period", value:"Save"})
	         .click(function(e){
	           CreationPeriod.save();
	           e.preventDefault();
	         })
	       )
	     )	     	     
	   )     
	     $(".datepicker").datepicker({
		     showOn			: "both",
		     buttonImage 	: "/library/jquery/css/calendar.gif",
		     buttonImageOnly	: true,
		     changeMonth		: true,
		     changeYear		: true,
		     dateFormat 		: "dd-M-yy",
		     altField		: "#startDate",
		     altFormat		: 'd_m_yy'
	     });		   
	}

};
