$(function(){
    
    //jQuery.ajaxSetup({async:false});
    //User.getUserSettings();
    //var userObject =  $('body').data('usersetting');
    var userid = $("#user_id").val()
    User.loadDefaultEvaluationYear(userid);
    $("#configure").click(function(){
        User.configureUserEvaluationYear(userid); 
        return false;  
    });   
  
  if($("#id").val() == undefined)
  {   
    User.get(userid);   
  }
  
     $("#adminuser").live('change', function(e){
        var userid = $(this).val();
        User.loadDefaultEvaluationYear(userid);
        e.preventDefault();
     });
     
  $("#setup_access").click(function(){
       if($("#userselect :selected").val() == "")
       {
          jsDisplayResult("error", "error", "Please select the user " );
          return false
       } else {
           jsDisplayResult("info", "info", "Saving user settings  . . . <img src='public/images/loaderA32.gif' /> " );
           $.post("main.php?controller=user&action=saveUserAccess", {
             usersetup : $("#useraccess-form").serialize()
           }, function(response) {
                 if( response.error )
                 {
                     jsDisplayResult("error", "error", response.text );
                 } else {    
                     User.get();   
                     jsDisplayResult("ok", "ok", response.text );
                 }                                 
           },"json");
      }
    return false;
  });   
     
  $("#save_changes").click(function(){
      jsDisplayResult("info", "info", "updating user settings . . . <img src='public/images/loaderA32.gif' /> " );
      $.post("main.php?controller=user&action=saveUserAccess", {
        usersetup : $("#useraccess-form").serialize()
      }, function(response) {
            if( response.error )
            {
               jsDisplayResult("error", "error", response.text );			
            } else {     
               if(response.updated)
               {
                 jsDisplayResult("info", "info", response.text);                 
               } else {
                 jsDisplayResult("ok", "ok", response.text);
               }
            }                                 
      },"json");
    return false;
  });   
     
});

var  User      = {
		getUserSettings		: function()
		{
			$.post("main.php?controller=user&action=getSettings",function(responseData){	
		        if(!$.isEmptyObject(responseData))
		        {
		          $('body').data("usersetting", responseData);
		          $("#default_year").html("<b>From </b><em>"+responseData.start+"</em> <b> To </b> <em>"+responseData.end+"</em>").addClass("gray");
		        }
			},"json");
		}, 
		get            : function(userid)
		{
		    $(".users").remove();
		    $("#userselect").empty();
		    $("#userselect").append($("<option />",{text:"--please select--", value:""}));
			$.post("main.php?controller=user&action=getAll",function(users){	
		        if(!$.isEmptyObject(users))
		        {
		            $.each(users, function(index, user){
		                //user has been setup
		                if(user.status != 0)
		                {
		                   User.display( user );
		                   $("#userselect").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname, "disabled":"disabled"}));
		                } else {
		                   $("#userselect").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}));
		                }
		                if(user.tkid == userid)
		                {
		                 $("#adminuser").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname, selected:"selected"}));
		                } else {
		                  $("#adminuser").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname}));
		                }
		                
		            });
		        }
			},"json"); 
			
		} , 
		
		display     : function(user)
		{
		    $("#useraccess_table").append($("<tr />",{id:"tr_"+user.tkid}).addClass("users")
		      .bind("mouseenter mouseleave", function(){
		        //$(this).toggleClass("tdhover");
		      })
		      .append($("<td />",{html:user.tkid}))
		      .append($("<td />",{html:user.username}))
		      .append($("<td />",{html:((user.status & 1) == 1 ? "Yes" : "No")}))
		      .append($("<td />",{html:((user.status & 4) == 4 ? "Yes" : "No")}))
		      .append($("<td />",{html:((user.status & 1) == 1 ? "Yes" : "No")}))
		      .append($("<td />",{html:((user.status & 4) == 4 ? "Yes" : "No")}))
		      .append($("<td />",{html:((user.status & 4) == 4 ? "Yes" : "No")}))		      
		      .append($("<td />",{html:((user.status & 8) == 8 ? "Yes" : "No")}))
		      .append($("<td />",{html:((user.status & 32) == 32 ? "Yes" : "No")}))
		      .append($("<td />",{html:((user.status & 64) == 64 ? "Yes" : "No")}))
		      .append($("<td />",{html:((user.status & 16) == 16 ? "Yes" : "No")}))
		      .append($("<td />",{html:((user.status & 128) == 128 ? "Yes" : "No")}))
		      .append($("<td />",{html:((user.status & 256) == 256 ? "Yes" : "No")}))
		      .append($("<td />",{html:((user.status & 512) == 512 ? "Yes" : "No")}))
		      .append($("<td />",{html:((user.status & 1024) == 1024 ? "Yes" : "No")}))
		      .append($("<td />")
		        .append($("<input />",{type:"button", name:"edit_user_"+user.tkid, id:"edit_user_"+user.tkid, value:"Edit Settings"}))
		        .append($("<input />",{type:"button", name:"delete_user_"+user.tkid, id:"delete_user_"+user.tkid, value:"Delete"}))
		      )		    
		    ).find("tbody>tr").filter(":even").addClass("tdhover")
		    
		    $("#edit_user_"+user.tkid).live("click", function(){
		        document.location.href = "main.php?controller=user&action=edituser&userid="+user.tkid+"&parent=4&folder=setup";		    
		        return false;
		    });
		    
		    $("#delete_user_"+user.tkid).live("click", function(){
		        if( confirm("Are you sure you want to delete this user settings"))
		        {
                      jsDisplayResult("info", "info", "updating user settings . . . <img src='../resources/images/loaderA32.gif' /> " );
                      $.post("main.php?controller=user&action=deleteUser", {
                        userid : user.tkid
                      }, function(response) {
                            if( response.error )
                            {
                                 jsDisplayResult("error", "error", response.text );			
                            } else {    
                                $("#tr_"+user.tkid).fadeOut();
                                jsDisplayResult("ok", "ok", response.text );
                            }                                 
                      },"json");
                    return false;
		        }
		        return false;
		    });

		} ,
		
		configureUserEvaluationYear    : function(user, hasEvaluationYear)
		{
		     var self = this;
               if($("#configure_evaluationyear").length > 0)
               {
                 $("#configure_evaluationyear").remove();
               }

               $("<div />",{id:"configure_evaluationyear"})
               .append($("<form />",{id:"setup_evalutionyear"})
                  .append($("<table />",{id:"table_evaluationyear", width:"100%"}))     
               )
               .dialog({
                        autoOpen  : true,
                        modal     : true,
                        position  : "top",
                        title     : "Setup Evaluation Year",
                        buttons   : {
                                      "Save Changes"  : function()
                                      {
                                         var checkedVal = $("#setup_evalutionyear input[name='setdafault']:checked").val();
                                         if( checkedVal == undefined)
                                         {
                                          $("#message").html("Please select the default evaluation year");
                                          return false;
                                         } else {
                                            $("#message").addClass('ui-state-info').html("updating user settings . . . <img src='../resources/images/loaderA32.gif' /> " );
                                            $.post("main.php?controller=user&action=updateUser",{
                                              default_year  : $("#setup_evalutionyear input[name='setdafault']:checked").val(),
                                              user_id       : user
                                            }, function(response) {
	                                           if(response.error )
	                                           {
		                                           $("#message").html(response.text)			
	                                           } else {    
		                                          if(response.updated)
		                                          {
		                                            jsDisplayResult("info", "info", response.text );
		                                          } else {
		                                             jsDisplayResult("ok", "ok", response.text );  
		                                          } 		
                                                    self.loadDefaultEvaluationYear(user);
                                                    $("#configure_evaluationyear").dialog("destroy");
                                                    $("#configure_evaluationyear").remove();	
	                                           }                              
                                            },"json");
                                         } 
                                      },
                                      "Cancel"        : function()
                                      {
                                        $("#configure_evaluationyear").dialog("destroy");
                                        $("#configure_evaluationyear").remove();
                                      } 
                        },
                        open     : function(event, ui)
                        {
           				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
           				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
           				var saveButton = buttons[0];
           				$(saveButton).css({"color":"#090"});
	                       var defaultYear = "";
	                       if(hasEvaluationYear)
	                       {
	                           var userObject =  $('body').data('usersetting');
	                           var defaultYear =  userObject['default_year'];
	                           $("#set_"+defaultYear).attr("checked", "checked");
	                       }
	                       $.post("main.php?controller=evaluationyears&action=getAll", {status:1}, function(responseData){
	                           $("#table_evaluationyear").append($("<tr />")
	                             .append($("<th />",{html:"From"}))
	                             .append($("<th />",{html:"To"}))
	                             .append($("<th />",{html:"Set As Default"}))
	                           )				        
	                           if($.isEmptyObject(responseData))
	                           {
	                               $("#table_evaluationyear").append($("<tr />")
	                                 .append($("<td />",{colspan:3, html:"There are no evaluation years set yet"}))
	                               )
	                           } else {
		                           $.each(responseData, function( index, evalYear) {
		                               $("#table_evaluationyear").append($("<tr />")
		                                 .append($("<td />",{html:evalYear.start}))
		                                 .append($("<td />",{html:evalYear.end}))
		                                 .append($("<td />")
		                                   .append($("<input />",{type:"radio", name:"setdafault", id:"set_"+evalYear.id, value:evalYear.id})
		                                     .attr("checked", (evalYear.id == defaultYear ? "checked" :""))
		                                   )
		                                 )
		                               )
		                           });	
	                           }		
                               $("#table_evaluationyear").append($("<tr />")
                                 .append($("<td />",{colspan:3})
                                   .append($("<span />",{id:"message"}))
                                 )
                               )	
                               $("#table_evaluationyear tr:even").css({"background-color":"#F7F7F7"});
                               				
	                       },"json");
                        },
                        close    : function()
                        {
                            $("#configure_evaluationyear").dialog("destroy");
                            $("#configure_evaluationyear").remove();              
                        }     
               });   
		} ,
		
          loadDefaultEvaluationYear     : function(userid)
          {
             var self = this;
		     $.getJSON("main.php?controller=user&action=getUser",{user:userid}, function(userdata){
		        if($.isEmptyObject(userdata))
		        {
		           self._createDefaultYear(userid);
		        } else {
		           userdata.userid = userid;
		           self._updateDefaultYear(userdata);
		        }
		     });              
          },
          
          _createDefaultYear            : function(userid)
          {
             $(".user_default").remove();
             $("#table_user_default_year").append($("<tr />").addClass("user_default")
               .append($("<th />",{html:"Default Evaluation Year"}))
               .append($("<td />",{html:"User default evaluation year is not yet set"}))
               .append($("<td />")
                  .append($("<input />",{type:"button", name:"configure", id:"configure", value:"Configure"})
                     .click(function(e){
                         User.configureUserEvaluationYear(userid, false);
                         e.preventDefault()
                     })                  
                  )
               )
             )  
             
          }, 
          
          _updateDefaultYear            : function(user)
          {
             $(".user_default").remove();
             $("body").data("usersetting", user);
             $("#table_user_default_year").append($("<tr />").addClass("user_default")
               .append($("<th />",{html:"Default Evaluation Year"}))
               .append($("<td />",{html:user.evaluationyear}))
               .append($("<td />")
                  .append($("<input />",{type:"button", name:"configure", id:"configure", value:"Configure"})
                     .click(function(e){
                         User.configureUserEvaluationYear(user.userid, true);
                         e.preventDefault()
                     })
                  )
               )
             )  
          }
          
}
