    $.widget("ui.kpa",{
    
    options     : {
        start               : 0,
        limit               : 10,
        current             : 1,
        total               : 0,
        componentid         : 0,
        evaluationyear      : 0, 
        section             : "",
        page                : "",
        user                : "",
        url                 : "main.php?controller=kpa&action=getAll",
        btnDisplayOptions   : {},
        evaluationSetting   : {}, 
        evaluations         : {},
        statusOfEvaluations : {},
        openKpi             : false,
        openAction          : false,
        kpaNextEvaluation   : {},
        userdata            : {},
        userlist            : {},
        userlogged          : {},
        jointByManager      : false,
        inCommittee         : false,
        userNext            : ""               
    } , 
    
    _init      : function()
    { 
        this._get();
    } , 
    
    _create     : function()
    {
        
    }, 
    
    _get        : function()
    {
        var self = this;
        self.options.user = $("#user").val();
		$.getJSON(self.options.url, {
			evaluationyear 	: self.options.evaluationyear,
			componentid		: self.options.componentid, 
			section             : self.options.section,
			user                : self.options.user, 
			page                : self.options.page
		}, function(kpaData) {
		       $(self.element).html("");	
		       $(".kpatable").remove();
               self.options.userdata     = kpaData.userdata
               self.options.userlogged   = kpaData.userlogged
               self.options.inCommittee  = kpaData.in_committee;
               if(kpaData.hasOwnProperty('kpa_change'))
               {
                  if(!$.isEmptyObject(kpaData.kpa_change))
                  {
                    self._notifyChange(kpaData); 
                  }
               } 	
               if(kpaData.hasOwnProperty('kpi_change'))
               {
                  if($.isEmptyObject(kpaData.kpa_change))
                  {
                    self._notifyChange(kpaData); 
                  }
               }
               	               
               if(kpaData.hasOwnProperty('settings'))
               {
                  if(kpaData['settings'].hasOwnProperty('evaluations'))
                  {
                    self.options.evaluations = kpaData.evaluations;    
                  }
                  if(kpaData['settings'].hasOwnProperty('next_evaluation'))
                  {
                    self.options.kpaNextEvaluation = kpaData['settings']['next_evaluation'];
                  }                   
                  self.options.evaluationSetting = kpaData.settings;     
               }
		      $(self.element).html($("<table />",{id:"kpatable_"+self.options.componentid}).addClass("noborder").addClass("kpatable"))
	          if(self._isEvaluationAtKpa() && self.options.page == "evaluate")
	          {
	             var kpa_str = "";
	             
	             if(kpaData.hasOwnProperty('kpa_evaluations'))
	             {
	                if(kpaData.kpa_evaluations.total === undefined)
	                {
	                   kpa_str = "There are no KPA available for evaluation in this evaluation period";
	                } else {
	                   var kpa_eval = (kpaData.kpa_evaluations.total_kpa - kpaData.kpa_evaluations.total);
	                   kpa_str = "<b>Status : </b>"+kpaData.kpa_evaluations.next+" - "+kpa_eval+"/"+kpaData.kpa_evaluations.total_kpa+" KPA evaluated. [ "+kpaData.kpa_evaluations.progress+"]";
	                }	               
	             } else {
                    kpa_str = "There are no KPA available for evaluation in this evaluation period";              
	             }	          
	               
	             $("#kpatable_"+self.options.componentid).append($("<tr />")
	               .append($("<td />",{colspan:(kpaData.cols+3)}).addClass("noborder")
                       .append($("<span />",{id:"action_status", html:kpa_str}).addClass("ui-state-info").css({padding:"5px"}))
	               )
	             )
	          }
	          if(self._isEvaluationAtKpi() && self.options.page == "evaluate")
	          {
	             var kpi_str = "";
	             if(kpaData.kpi_evaluations.total === undefined)
	             {
	               kpi_str = "There are no KPI available for evaluation in this evaluation period";
	             } else {
	               var kpi_eval = (kpaData.kpi_evaluations.total_kpi - kpaData.kpi_evaluations.total);
	               kpi_str = "<b>Status : </b>"+kpaData.kpi_evaluations.next+" - "+kpi_eval+"/"+kpaData.kpi_evaluations.total_kpi+" KPI evaluated. [ "+kpaData.kpi_evaluations.progress+"]";
	             }	          
	               
	             $("#kpatable_"+self.options.componentid).append($("<tr />")
	               .append($("<td />",{colspan:(kpaData.cols + 3)}).addClass("noborder")
                       .append($("<span />",{id:"action_status", html:kpi_str}).addClass("ui-state-info").css({padding:"5px"}))
	               )
	             )
	          }
	          if(self._isEvaluationAtAction() && self.options.page == "evaluate")
	          {    
	             var action_str = "";
	             if(kpaData.action_evaluations.total === undefined)
	             {
	               action_str = "There are no actions available for evaluation in this evaluation period";
	             } else {
	               var action_eval = (kpaData.action_evaluations.total_actions - kpaData.action_evaluations.total);
	               action_str = "<b>Status : </b>"+kpaData.action_evaluations.next+" - "+action_eval+"/"+kpaData.action_evaluations.total_actions+" Actions evaluated. [ "+kpaData.action_evaluations.progress+"]";
	             }
	             $("#kpatable_"+self.options.componentid).append($("<tr />")
	               .append($("<td />",{colspan:(kpaData.cols+3)}).addClass("noborder")
	                 .append($("<span />",{id:"action_status", html:action_str}).addClass("ui-state-info").css({padding:"5px"}))
	               )
	             )
	          }	          	          
	          
	          var canConfirm = false;
               if(kpaData.hasOwnProperty('hasActions'))
               {
                  if(kpaData.hasActions)
                  {
                      if((self.options.userdata['managerid'] == self.options.userlogged['tkid']) 
                     || (self.options.userlogged['managerid'] == "S"))
                     {
                       canConfirm = true;
                     }
                  }
               }
                if((self.options.userdata['managerid'] == self.options.userlogged['tkid']) || 
                   (self.options.userlogged['managerid'] == "S")) {              
                    
               } else {
                  $("#activatecomponent").attr("disabled", "disabled");
               }
               
	          $("#kpatable_"+self.options.componentid)
	           .append($("<tr />")
	              .append($("<td />",{colspan:(parseInt(kpaData.cols)+4)}).addClass("noborder")
	                 .append($("<input />",{type:"button", name:"add_kpa_"+self.options.componentid, id:"add_kpa_"+self.options.componentid, value:"Add KPA"}).css({"display":(self.options.page == "new" ? "block" : (self.options.page == "admin" ? "block" : "none"))}))
	                 .append($("<input />",{type:"button", 
	                                        name:"confirm_kpa_"+self.options.componentid,
	                                        id:(canConfirm ? "confirm_kpa_"+self.options.componentid : "confirm"), 
	                                        value:"Confirm",
	                                        disabled : (canConfirm ? "" : "disabled")}).css({"display":(self.options.page == "confirm" ? "block" : "none")})
	                    .click(function(e){
	                          self._confirmComponent();
	                          e.preventDefault();
	                    })
	                 )
	                 /*.append($("<input />",{type:"button", name:"activate_kpa_"+self.options.componentid, id:"activate_kpa_"+self.options.componentid, value:"Activate"}).css({"display":(self.options.page == "activate" ? "block" : "none")}))*/
	              )
	           )
	         //)
	         
	         $("#add_kpa_"+self.options.componentid).live("click", function(e){
	               if(self.options.section == "admin")
	               {
	                 document.location.href = "main.php?controller=kpa&action=admin_addkpa&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&section="+self.options.section+"&folder="+self.options.section+"&user="+self.options.user;
	               } else {
	                 document.location.href = "main.php?controller=kpa&action=addkpa&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&section="+self.options.section+"&folder="+self.options.section+"&user="+self.options.user;
	               }
	            e.preventDefault();
	         });
	         
	         $("#activate_kpa_"+self.options.componentid).live("click", function(e){
                self._activateComponent();
	           e.preventDefault();
	         });			    
		    
		    if($.isEmptyObject(kpaData.data))
		    {		   
		        $("#kpatable_"+self.options.componentid).append($("<tr />")
		          .append($("<td />",{colspan:(parseInt(kpaData.cols)+4)}).addClass("noborder")
		            .append($("<p />")
		              .addClass("ui-state")
		              .addClass("ui-state-highlight")
		              .css({"padding":"5px"})
		              .append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))
		              .append($("<span />",{html:"There are no KPAs associated with this component"}).css({"margin-left":"3px"}))
		            )
		          )
		        ) 
			} else {
		         //$(self.element).append($("<table />",{id:"kpatable_"+self.options.componentid}).addClass("noborder")
     	        self._displayHeaders(kpaData.headers);
			   self._display(kpaData.data, kpaData.cols, kpaData.kpi_data, kpaData.settings);
			}
		});    
    } , 
    
    _displayHeaders     : function(headers)
    {
        var self = this;
        var tr = $("<tr />")
        tr.append($("<td />",{html:"<b><small>K<br />P<br />A<br />s</small></b>"}).css({width:"1em", color:"blue"}))
        tr.append($("<td />",{html:"<small><b>K<br />P<br />I</b><br />s</small>"}).css({color:"blue"})) 
        tr.append($("<td />",{html:"<small><b>A<br />C<br />T<br />I<br />O<br />N</b><br />s</small>"}).css({color:"blue"})) 
        $.each(headers, function(index, val){
            tr.append($("<th />",{html:val}))        
        });
        var displayOptions = self._getDisplayStatus();
        tr.append($("<th />",{html:"&nbsp;"}).css({"display":(displayOptions['displayTd'] ? "table-cell" : "none")}))
        tr.append($("<th />",{html:"Employee Rating"}).css({"display":(displayOptions['selfEvaluationRating'] ? "table-cell" : "none")}))
        tr.append($("<th />",{html:"Employee Comment"}).css({"display":(displayOptions['selfEvaluationComment'] ? "table-cell" : "none")}))
        tr.append($("<th />",{html:"Manager Rating"}).css({"display":(displayOptions['managerEvaluationRating'] ? "table-cell" : "none")}))
        tr.append($("<th />",{html:"Manager Comment"}).css({"display":(displayOptions['managerEvaluationComment'] ? "table-cell" : "none")}))
        tr.append($("<th />",{html:"Joint Evaluation"}).css({"display":(displayOptions['jointEvaluationRating'] ? "table-cell" : "none")}))
        tr.append($("<th />",{html:"Joint Comment"}).css({"display":(displayOptions['jointEvaluationComment'] ? "table-cell" : "none")}))  
        tr.append($("<th />",{html:"Evaluation Review Rating"}).css({"display":(displayOptions['evaluationReviewRating'] ? "table-cell" : "none")}))
        tr.append($("<th />",{html:"Evaluation Review Comment"}).css({"display":(displayOptions['evaluationReviewComment'] ? "table-cell" : "none")})) 
        $("#kpatable_"+self.options.componentid).append(tr);
    },
    
    _display    : function(kpas, cols, kpa_data, settings)
    {
      var self = this;
      var displayOptions = self._getDisplayStatus();     
      self._openEvaluationLevel();
      $.each(kpas, function(index, kpa){
        var tr = $("<tr />",{id:"tr_"+index}).addClass("kpa");
        tr.append($("<td />")
           .append($("<a />",{href:"#", id:"viewkpi_"+index}).css({"border":"1px solid #000;"})
             .append($("<span />",{id:"viewkpiicon_"+index}).addClass("ui-icon").addClass("ui-icon-"+(self.options.openKpi ? "minus" : "plus")))
           )
        )
        tr.append($("<td />",{html:"&nbsp;"}));
        tr.append($("<td />",{html:"&nbsp;"}));
        $.each(kpa, function(key, val){
            tr.append($("<td />",{html:val}));
        });
        var displayEvaluationButton = self._canEvaluateKPA(index);
        tr.append($("<td />").css({"display":(displayOptions['displayTd'] ? "table-cell" : "none")})
          .append($("<input />",{type:"button", name:self.options.componentid+"add_kpi_"+index, id:self.options.componentid+"add_kpi_"+index, value:"Add KPI"}).css({"display":(displayOptions['displayAddBtn']  ? "block" : "none")}))
          .append($("<input />",{type:"button", name:self.options.componentid+"edit_kpa_"+index, id:self.options.componentid+"edit_kpa_"+index, value:"Edit KPA"}).css({"display":(displayOptions['displayEditBtn'] ? "block" : "none")}))
          .append($("<input />",{type:"button", name:self.options.componentid+"_update_kpa_"+index, id:self.options.componentid+"_update_kpa_"+index, value:"Update KPA"}).css({"display":(displayOptions['displayUpdateBtn'] ? "block" : "none")}))    
          .append($("<input />",{type:"button", name:self.options.componentid+"_evaluate_kpa_"+index, id:self.options.componentid+"_evaluate_kpa_"+index, value:"Evaluate KPA"}).css({"display":(displayEvaluationButton ? "block" : "none")}))               
          .append($("<span />",{html:"<b></b>"})
            .css({"padding":"5px", "display":(displayOptions['displayEvaluateBtn'] ? "none" : "block")})
          )
          .append($("<span />").css({display:(displayOptions['displayEvaluateBtn'] ? "block" : "none")})
             .append($("<span />")
             .addClass(settings['evaluationstatuses'][index]['self']['status']).css({"float":"left"}))
             .append($("<span />",{html:"Self Evaluation"})
               .css({"margin-left":"3px", "color":(self._isNextEvaluation(index, 'self') ? "" : "#AAAAAA")})
             )  
          )
          .append($("<span />").css({display:(displayOptions['displayEvaluateBtn'] ? "block" : "none")})
             .append($("<span />")
             .addClass(settings['evaluationstatuses'][index]['manager']['status']).css({"float":"left"}))
             .append($("<span />",{html:"Manager Evaluation"})
              .css({"margin-left":"3px", "color":(self._isNextEvaluation(index, 'manager') ? "" : "#AAAAAA")})
             )  
          )
          .append($("<span />").css({display:(displayOptions['displayEvaluateBtn'] ? "block" : "none")})
             .append($("<span />")
             .addClass(settings['evaluationstatuses'][index]['joint']['status']).css({"float":"left"}))
             .append($("<span />",{html:"Joint Evaluation"})
               .css({"margin-left":"3px", "color":(self._isNextEvaluation(index, 'joint') ? "" : "#AAAAAA")})
             )  
          )
          .append($("<span />").css({display:(displayOptions['displayEvaluateBtn'] ? "block" : "none")})
             .append($("<span />")
             .addClass(settings['evaluationstatuses'][index]['review']['status']).css({"float":"left"}))
             .append($("<span />",{html:"Evaluation Review"})
               .css({"margin-left":"3px", "color":(self._isNextEvaluation(index, 'review') ? "" : "#AAAAAA")})
             )  
          )          
          .append($("<input />",{type:"button", name:self.options.componentid+"_approve_kpa_"+index, id:self.options.componentid+"_approve_kpa_"+index, value:"Approve KPA"}).css({"display":(displayOptions['displayApproveBtn'] ? "block" : "none")}))
          .append($("<input />",{type:"button", name:self.options.componentid+"_assurance_kpa_"+index, id:self.options.componentid+"_assurance_kpa_"+index, value:"Assurance KPA"}).css({"display":(displayOptions['displayAssuranceBtn'] ? "block" : "none")}))   
          .append($("<input />",{type:"button", name:self.options.componentid+"delete_kpa_"+index, id:self.options.componentid+"delete_kpa_"+index, value:"Delete KPA"}).css({"display":(displayOptions['displayDeleteBtn'] ? "block" : "none")}))                 
        )
        var dispalyEvaluationOptions = {};
        if(self.options.page == "evaluate")
        {
            if(self._isEvaluationAtKpa())
            {
               //dispalyEvaluationOptions = self._getDisplayEvalutionOptions(index);
               tr.append($("<td />",{html:self._kpaEvaluation(index, "self", "rating")}).css({"display":(displayOptions['selfEvaluationRating'] ? "table-cell" : "none")})
               )
               tr.append($("<td />",{html:self._kpaEvaluation(index, "self", "comment")}).css({"display":(displayOptions['selfEvaluationComment'] ? "table-cell" : "none")}))
               tr.append($("<td />",{html:self._kpaEvaluation(index, "manager", "rating")}).css({"display":(displayOptions['managerEvaluationRating'] ? "table-cell" : "none")}))
               tr.append($("<td />",{html:self._kpaEvaluation(index, "manager", "comment")}).css({"display":(displayOptions['managerEvaluationComment'] ? "table-cell" : "none")}))
               tr.append($("<td />",{html:self._kpaEvaluation(index, "joint", "rating")}).css({"display":(displayOptions['jointEvaluationRating'] ? "table-cell" : "none")}))
               tr.append($("<td />",{html:self._kpaEvaluation(index, "joint", "comment")}).css({"display":(displayOptions['jointEvaluationComment'] ? "table-cell" : "none")}))  
               tr.append($("<td />",{html:self._kpaEvaluation(index, "review", "rating")}).css({"display":(displayOptions['evaluationReviewRating'] ? "table-cell" : "none")}))
               tr.append($("<td />",{html:self._kpaEvaluation(index, "review", "comment")}).css({"display":(displayOptions['evaluationReviewComment'] ? "table-cell" : "none")}))         
            } else {
              tr.append($("<td />",{html:"<b>"+displayOptions['evaluationText']+"</b>", colspan:"8"})
                .css({"color":"#AAAAAA"})
               )     
            }           
         } 
           $("#"+self.options.componentid+"delete_kpa_"+index).live("click", function(e){
               jsDisplayResult("info", "info", "deleting kpa . . . <img src='public/images/loaderA32.gif' /> " );  
               if(confirm("Are you sure you want to delete this KPA"))
               {
	               $.post("main.php?controller=kpa&action=updateStatus",{
		               kpastatus	: 2,
		               id		: index
	               }, function( response ){
		               if( response.error )
		               {
			               jsDisplayResult("error", "error", response.text );
		               } else {			
		                if(response.updated)
		                {
                             $("#tr_"+index).remove();		
                             jsDisplayResult("info", "info", response.text);
		                } else {
		                   $("#tr_"+index).remove();		
			              jsDisplayResult("ok", "ok", response.text);
			              self._get();		               
		                }
		              }		
	               },"json");                    
               }           
               e.preventDefault();
           });
                   
		$("#"+self.options.componentid+"add_kpi_"+index).live("click", function(e){
               document.location.href = "main.php?controller=kpi&action=addkpi&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section+"&user="+self.options.user;	
			e.preventDefault();
		});

		$("#"+self.options.componentid+"edit_kpa_"+index).live("click", function(e){
		     
			if( self.options.section == "setup")
			{
			     document.location.href = "main.php?controller=kpa&action=setup_editkpa&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section;
			} else {
                  document.location.href = "main.php?controller=kpa&action=editkpa&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section;			
			}		     
			
			e.preventDefault();
		});
		
		$("#"+self.options.componentid+"_update_kpa_"+index).live("click", function(e){   
              document.location.href = "main.php?controller=kpa&action=updatekpa&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section;
		   e.preventDefault();
		});		
		
		$("#"+self.options.componentid+"_evaluate_kpa_"+index).live("click", function(e){   
		    self._evaluateKpa(self.options.componentid, index); 
		
              /*document.location.href = "main.php?controller=kpa&action=evaluatekpa&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val();*/
		   e.preventDefault();
		});				
		
		$("#"+self.options.componentid+"_approve_kpa_"+index).live("click", function(e){   
              document.location.href = "main.php?controller=kpa&action=approvekpa&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section;
		   e.preventDefault();
		});				
				
		$("#"+self.options.componentid+"_assurance_kpa_"+index).live("click", function(e){   
              document.location.href = "main.php?controller=kpa&action=assurancekpa&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section;
		   e.preventDefault();
		});				
		        
        $("#kpatable_"+self.options.componentid).append(tr);
    
        var actionUrl = "";
        var kpiUrl    = "";
        if(self.options.page == 'evaluate')
        {
          actionUrl = "main.php?controller=action&action=getActionEvaluationData";
          kpiUrl    = "main.php?controller=kpi&action=getKpiEvaluationData";
        }
        if($.isEmptyObject(kpa_data[index]['ids']))
        {
           $("#viewkpi_"+index).attr("title", "0 kpi"); 
           $("#kpatable_"+self.options.componentid).append($("<tr />",{id:index+"_childtr_#"}));
        } else {
            $.each(kpa_data[index]['ids'], function(kpiId, kpistatus){  
                $("#kpatable_"+self.options.componentid).append($("<tr />",{id:index+"_childtr_"+kpiId}).css({"display":(self.options.openKpi ? "table-row" : "none")}).addClass("childfor_"+index));
                if(!$.isEmptyObject(kpa_data[index]['actionInfo'][kpiId]))
                {
                    $.each(kpa_data[index]['actionInfo'][kpiId], function(actionId, actionStatus){
                        $("#kpatable_"+self.options.componentid).append($("<tr />",{id:index+"_child_"+kpiId+"_action_"+actionId}).css({"display":(self.options.openAction ? "table-row" : "none")}).addClass("child_action_for_"+kpiId));       
                    })
                } else {
                    $("#viewaction_"+kpiId).attr("title", "0 actions");
                }
                $("#"+index+"_childtr_"+kpiId).action({kpaid:index, kpiid:kpiId, componentid:self.options.componentid, evaluationyear:self.options.evaluationyear, section:self.options.section, page:self.options.page, displayOptions:displayOptions, url:actionUrl, evaluationSettings:self.options.evaluationSetting, user:self.options.user, evaluations:self.options.evaluations, openAction:self.options.openAction, kpistatus:kpistatus, userNext:self.options.userNext});
            });        
            $("#tr_"+index).kpi({kpaid:index, componentid:self.options.componentid, evaluationyear:self.options.evaluationyear, section:self.options.section, page:self.options.page, displayOptions:displayOptions, url:kpiUrl, evaluationSettings:self.options.evaluationSetting, user:self.options.user, evaluations:self.options.evaluations, openAction:self.options.openAction, userNext:self.options.userNext});
        }
        
        $("#viewkpi_"+index).live("click", function(){
            if($(".childfor_"+index).is(":hidden"))
            {
               $("#viewkpiicon_"+index).removeClass("ui-icon-plus").addClass("ui-icon-minus");
            } else {
                $("#viewkpiicon_"+index).removeClass("ui-icon-minus").addClass("ui-icon-plus");
            }
            $(".childfor_"+index).toggle();
            //$.getScript("../resources/js/jquery.ui.kpi.js", function(){
               //$("#"+index+"_childtr_").kpi({kpaid:index, componentid:self.options.componentid, evaluationyear:self.options.evaluationyear})
            //});
            return false;
        });    
      });       
        
		 $("#kpatable_"+self.options.componentid).append($("<tr />").css({display:(displayOptions['displayConfigureBtn']  ? "table-row" : "none")})
		   .append($("<td />",{colspan:(parseInt(cols)+4)}).css({"text-align":"right"}).addClass("noborder")
			  .append($("<input />",{type:"button", name:"configure_"+self.options.componentid , id:"configure_"+self.options.componentid, value:"Configure"}))
		   )		
		)
		$("#configure_"+self.options.componentid).live("click", function(){
			document.location.href = "main.php?controller=kpa&action=configure&type="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section;
			return false;
		});     
    },
    
    _notifyChange           : function(kpaData)
    {
       var self = this;
       if($("#kpa_change").length > 0)
       {
          $("#kpa_change").remove();
       }
       $("<div />",{id:"kpa_change"}).append($("<p />").addClass("ui-state-info")
         .append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))
         .append($("<span />",{html:"There has been an update to the template data since the initial setup for <b>"+kpaData['userdata']['tkname']+" "+kpaData['userdata']['tksurname']+"</b> for the component <b>"+kpaData['componentname']+"</b> for the evaluation year <b>"+kpaData['userdata']['start']+" --  "+kpaData['userdata']['end']+"</b>."}).css({"padding":"3px", "margin-left":"0.3em"}))
       ).dialog({
          autoOpen  : true,
          modal     : true,
          position  : "top",
          width     : 300,
          title     : "Template data change",
          buttons   : {
                         "Update Changes"    : function()
                         {
                            $.post("main.php?controller=kpa&action=update_template_change",{
                              kpa_change     : kpaData.kpa_change,
                              kpi_change     : kpaData.kpi_change,
                              componentid    : self.options.componentid,
                              userdata       : kpaData['userdata'],
                              evaluationyear : self.options.evaluationyear
                            },function(response) {
                              $("#kpa_change").remove();
                              document.location.href = "main.php?controller=component&action=newcomponent&parent_id=1&folder=new";
                            },"json");
                         },
                         "Cancel "           : function()
                         {
                            $("#kpa_change").remove();
                         }
          } ,
          close     : function(event, ui)
          {
             $("#kpa_change").remove();
          } ,
          open      : function(event, ui)
          {
             $("#kpa_change").append($("<ul />",{id:"kpa_added_list"}))
             if(kpaData['kpa_change'].hasOwnProperty('added'))
             {
               $.each(kpaData['kpa_change']['added'], function(index, kpa){
                  $("#kpa_added_list").append($("<li />",{html:"Kpa <b>"+kpa.name+"</b> was added "}))
               })
             } 
		   var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		   var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
	        $(buttons[0]).css({"color":"#090"});
          }
       })
    
    } ,
    
    _confirmComponent       : function()
    {
        var self = this;
        if($("#confirmdialog_"+self.options.componentid).length > 0)
        {
          $("#confirmdialog_"+self.options.componentid).remove();
        }
	     var responseUser; 
	     $.ajaxSetup({async:false});
	     $.getJSON("main.php?controller=user&action=getUser",{user:self.options.user}, function(response){  
	          responseUser = response;
	     });
        
        $("<div />",{id:"confirmdialog_"+self.options.componentid, html:"I, "+responseUser.manager+", manager of "+responseUser.user+" hereby:"})
        .append($("<ul />")
          .append($("<li />",{html:"Confirm that I have reviewed all the components of the Performance Matrix of "+responseUser.user+" for the evaluation year: "+responseUser.evaluationyear+";"}))
          .append($("<li />",{html:"Declare that "+responseUser.user+" has reviewed and has agreed to this Performance Matrix; and"}))
          .append($("<li />",{html:"Declare that I have the required authority to confirm this Performance Matrix."}))
        )
        .append($("<div />",{id:"message"}).css({"padding":"5px"}))
        .dialog({
                autoOpen    : true, 
                modal       : true,
                position    : "top",
                title       : "Confirmation of Performance Matrix",
                buttons     : {
                                "Confirm"   : function()
                                {
                                    $("#message").addClass("ui-state-info").html("confirming component ... <img src='public/images/loaderA32.gif' />");
                                    $.post("main.php?controller=usercomponent&action=confrimComponent", {
                                        componentid : self.options.componentid,
                                        user        : $("#user").val()
                                    }, function(response) {
			                        if(response.error )
			                        {
				                     $("#message").addClass("ui-state-erorr").html(response.text);
				                     //jsDisplayResult("error", "error",  );
			                        } else {
			                          /*$("#confirm_kpa_"+self.options.componentid).attr("disabled", "disabled");
                                         $("#confirmdialog_"+self.options.componentid).dialog("destroy");
                                         $("#confirmdialog_"+self.options.componentid).remove(); 
				                     */
				                     jsDisplayResult("ok", "ok", response.text);
				                     document.location.href = "main.php?controller=component&action=confirm&parent=1&folder=new";
			                        }	
                                    },"json");
                                }, 
                                "Cancel"    : function()
                                {
                                    $("#confirmdialog_"+self.options.componentid).dialog("destroy");
                                    $("#confirmdialog_"+self.options.componentid).remove();                                 
                                }
                } ,
                "close"     : function(event, ui)
                {
                    $("#confirmdialog_"+self.options.componentid).dialog("destroy");
                    $("#confirmdialog_"+self.options.componentid).remove(); 
                },
                "open"      : function(event, ui)
                {
 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
 				var saveButton = buttons[0];
 				$(saveButton).css({"color":"#090"}); 
                }                 
        });
    } ,
    
    _activateComponent      : function()
    {
        var self = this;
        if($("#activatedialog_"+self.options.componentid).length>0)
        {
          $("#activatedialog_"+self.options.componentid).remove();
        }
        $("<div />",{id:"activatedialog_"+self.options.componentid, html:"Activate Component"})
        .dialog({
                autoOpen    : true, 
                modal       : true,
                position    : "top",
                title       : "Activate Component",
                buttons     : {
                                "Activate"   : function()
                                {
                                   /*
                                    $.post("main.php?controller=usercomponent&action=activateComponent", {
                                        componentid : self.options.componentid,
                                        user        : self.options.user
                                    }, function(response) {
				                   if( response.error )
				                   {
				                      jsDisplayResult("error", "error", response.text );
				                    } else {
                                          $("#activatedialog_"+self.options.componentid).dialog("destroy");
                                          $("#activatedialog_"+self.options.componentid).remove(); 
				                      jsDisplayResult("ok", "ok", response.text );
				                    }	
                                    },"json");
                                    */
                                }, 
                                "Cancel"    : function()
                                {
                                    $("#activatedialog_"+self.options.componentid).dialog("destroy");
                                    $("#activatedialog_"+self.options.componentid).remove();                                 
                                }
                } ,
                "close"     : function(event, ui)
                {
                    $("#activatedialog_"+self.options.componentid).dialog("destroy");
                    $("#activatedialog_"+self.options.componentid).remove(); 
                },
                "open"      : function(event, ui)
                {
                
                }                 
        });    
    } , 
    
    
    _evaluateKpa              : function(componentid, kpaid)
    {
       var self = this;
       if($("#assessment_"+componentid+"_"+kpaid).length)
       {
          $("#assessment_"+componentid+"_"+kpaid).remove();       
       }
       
       $("<div />",{id:"assessment_"+componentid+"_"+kpaid})
         .append($("<table />",{width:"100%"})
           .append($("<tr />")
             .append($("<th />",{html:"Rating :"}))
             .append($("<td />")
               .append($("<select />",{name:"kparating", id:"kparating"})
                 .append($("<option />",{text:"--please select--", value:""}))
               )
             )
           )
           .append($("<tr />")
             .append($("<th />",{html:"Comment :"}))
             .append($("<td />")
               .append($("<textarea />",{name:"kpacomment", id:"kpacomment", cols:"50", rows:"7"}))
             )
           )  
           .append($("<tr />")
             .append($("<th />",{html:"Recommendation :"}))
             .append($("<td />")
               .append($("<textarea />",{name:"kparecommendation", id:"kparecommendation", cols:"50", rows:"7"}))
             )
           )                      
           .append($("<tr />")
             .append($("<td />",{colspan:"2"})
               .append($("<span />",{id:"message"}))
             )
           )
         ).dialog({
             autoOpen   : true,
             title      : "Kpa Evaluation",
             modal      : true,
             position   : "top",
             width      : 500,
             buttons    : {
                            "Save"      : function()
                            {
                               if($("#kparating").val() == "")
                               {
                                   $("#message").addClass("ui-state-error").css({padding:"5px"}).html("Please select the rating")
                                   return false;
                               } else if($("#kpacomment").val() == ""){
                                   $("#message").addClass("ui-state-error").css({padding:"5px"}).html("Please enter the comment");
                                   return false;
                               } else {
                                    $("#message").addClass("ui-state-info").css({padding:"5px"}).html("saving evaluation information . . . <img src='public/images/loaderA32.gif' /> " );
                                    $.post("main.php?controller=kpa&action=save_kpa_evaluation",{
                                       rating          : $("#kparating").val(),
                                       comment         : $("#kpacomment").val(),
                                       recommendation  : $("#kparecommendation").val(),
                                       type            : "kpa",
                                       reference       : kpaid,
                                       componentid     : componentid,
                                       user            : self.options.user,
                                       next_evaluation : self.options.kpaNextEvaluation[kpaid]
                                   }, function(response) {
                                        if(response.error)
                                        {
                                           $("#message").addClass("ui-state-error").css({padding:'5px'}).html(response.text)
                                        } else {
                                           jsDisplayResult("ok", "ok", response.text);
                                           $("#assessment_"+componentid+"_"+kpaid).remove();
                                           $("#assessment_"+componentid+"_"+kpaid).dialog("remove");  
                                           self._get();                                              
                                        }
                                    },"json")     
                               }
                            } ,
                            
                            "Cancel"    : function()
                            {
                               $("#assessment_"+componentid+"_"+kpaid).remove();
                               $("#assessment_"+componentid+"_"+kpaid).dialog("remove");     
                            }
             } ,
             close       : function(event, ui)
             {
                $("#assessment_"+componentid+"_"+kpaid).remove();
                $("#assessment_"+componentid+"_"+kpaid).dialog("remove");
             } ,
             open        : function(event, ui)
             {
			 var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			 var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
			 var saveButton = buttons[0];
			 $(saveButton).css({"color":"#090"});               
                //load
                var ratingScales = $('body').data('ratingScales');
                if($.isEmptyObject(ratingScales))
                {
                     $.getJSON("main.php?controller=ratingscale&action=getAll",{status:1} ,function(ratingScales){
                        if(!$.isEmptyObject(ratingScales))
                        {
                           $('body').data('ratingScales', ratingScales);
                           $("#kparating").empty();
                           $("#kparating").append($("<option />",{text:"--please select--", value:""}));
                           $.each(ratingScales, function(index, ratingScale){
                              $("#kparating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));
                           });
                        }
                     });
                } else {
                      $("#kparating").empty();
                      $("#kparating").append($("<option />",{text:"--please select--", value:""}));
                      $.each(ratingScales, function(index, ratingScale){
                         $("#kparating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));
                      });                    
                }
             }
         })    
    } ,
    
    _getDisplayStatus         : function() 
    {
        var self  = this;
        var  btnDisplayOptions = {}
        btnDisplayOptions['displayTd']        = true;
        btnDisplayOptions['displayAddBtn']    = true;
        btnDisplayOptions['displayEditBtn']   = false;
        btnDisplayOptions['displayUpdateBtn']    = false;
        btnDisplayOptions['displayEvaluateBtn']  = false;
        btnDisplayOptions['displayKPIEvaluateBtn'] = false;
        btnDisplayOptions['displayActionEvaluateBtn'] = false;
        btnDisplayOptions['displayApproveBtn']   = false;
        btnDisplayOptions['displayAssuranceBtn'] = false;
        btnDisplayOptions['displayDeleteBtn']    = true;
        btnDisplayOptions['displayConfigureBtn']    = true;
        btnDisplayOptions['evaluationText']    =  "";
        btnDisplayOptions['kpiEvaluationText']    =  "";
        btnDisplayOptions['actionEvaluationText']    =  "";
        btnDisplayOptions['selfEvaluationRating']    = false
        btnDisplayOptions['selfEvaluationComment']   = false;
        btnDisplayOptions['managerEvaluationRating'] = false;
        btnDisplayOptions['managerEvaluationComment']= false;
        btnDisplayOptions['jointEvaluationRating']   = false;
        btnDisplayOptions['jointEvaluationComment']  = false; 
        btnDisplayOptions['evaluationReviewRating']   = false;
        btnDisplayOptions['evaluationReviewComment']  = false;         
        
        if(self.options.page == "activate")
        {
           btnDisplayOptions['displayTd'] = false;
        } else if(self.options.page == "view"){
           btnDisplayOptions['displayTd'] = false;
        }        
        if(self.options.section == 'manage')
        {
          btnDisplayOptions['displayAddBtn'] = false;
        }
        
        if(self.options.section == 'new')
        {
          btnDisplayOptions['displayEditBtn'] = true;
        }
        if(self.options.section == 'admin')
        {
          btnDisplayOptions['displayEditBtn'] = true;
        }
        if(self.options.page == 'edit')
        {
          btnDisplayOptions['displayEditBtn'] = true;
        }
        if(self.options.page == 'update')
        {
          btnDisplayOptions['displayUpdateBtn'] = true;
          btnDisplayOptions['displayDeleteBtn']    = false;
        }
        if(self.options.page == 'evaluate')
        {
          btnDisplayOptions['selfEvaluationRating']    = true;
          btnDisplayOptions['selfEvaluationComment']   = true;
          btnDisplayOptions['managerEvaluationRating'] = true;
          btnDisplayOptions['managerEvaluationComment']= true;
          btnDisplayOptions['jointEvaluationRating']   = true;
          btnDisplayOptions['jointEvaluationComment']  = true;    
          btnDisplayOptions['evaluationReviewRating']   = true;
          btnDisplayOptions['evaluationReviewComment']  = true;              
                
          //check if evealuation can be done at KPA level
          if(!$.isEmptyObject(self.options.evaluationSetting))
          {
             if(self.options.evaluationSetting['evaluate_on'] == "kpa")
             {
                btnDisplayOptions['displayEvaluateBtn'] = true;     
             } else {
               btnDisplayOptions['displayEvaluateBtn'] = false;
               btnDisplayOptions['evaluationText'] = "No evaluation at KPA Level";
             }
             
             if(self.options.evaluationSetting['evaluate_on'] == "kpi")
             {
                btnDisplayOptions['displayKPIEvaluateBtn'] = true;     
             } else {
               btnDisplayOptions['displayKPIEvaluateBtn'] = false;
               btnDisplayOptions['kpiEvaluationText'] = "No evaluation at KPI Level";
             }
             
             
             if(self.options.evaluationSetting['evaluate_on'] == "action")
             {
                btnDisplayOptions['displayActionEvaluateBtn'] = true;     
             } else {
               btnDisplayOptions['displayActionEvaluateBtn'] = false;
               btnDisplayOptions['actionEvaluationText'] = "No evaluation at action Level";
             }
             
             
             
          } else {
             btnDisplayOptions['displayEvaluateBtn'] = false;
             btnDisplayOptions['evaluationText'] = "No evaluation at KPA Level";
          }
          btnDisplayOptions['displayDeleteBtn']    = false;
        }        
        if(self.options.page == 'approve')
        {
          btnDisplayOptions['displayApproveBtn'] = true;
          btnDisplayOptions['displayDeleteBtn']    = false;
        }                
        if(self.options.page == 'assurance')
        {
          btnDisplayOptions['displayAssuranceBtn'] = true;
          btnDisplayOptions['displayDeleteBtn']    = false;
        }                
	   if( self.options.section != "admin")
	   {
	     btnDisplayOptions['displayConfigureBtn'] = false;			
	   }                
        return btnDisplayOptions;        
    } ,
    
    _kpaEvaluation         : function(kpaId, evaluationType, type)
    {
          var self  = this;
          var evaluationSettings = self.options.evaluationSetting;
          var evaluations        = self.options.evaluations;
          var textOptions = {}
          textOptions['selfevaluation']    = false
          textOptions['managerevaluation'] = false;
          textOptions['jointevaluation']   = false;
          textOptions['evaluationreview']   = false;
          if(evaluationSettings['evaluate_on'] == "kpa")
          {
             if(evaluationSettings['evaluations'].hasOwnProperty(kpaId))
             {                    
               if(evaluationSettings['evaluations'][kpaId].hasOwnProperty(evaluationType))
               {
                  if(evaluationSettings['evaluations'][kpaId][evaluationType].hasOwnProperty(type))
                  {                 
                    return evaluationSettings['evaluations'][kpaId][evaluationType][type];
                  }
               }
             }
          }
    } ,

    _isNextEvaluation           : function(index, evaluationType)
    {
      var self = this;
       if(!$.isEmptyObject(self.options.kpaNextEvaluation))
       {
          if(self.options.kpaNextEvaluation[index] === evaluationType)
          {
            return true;
          } else {
            return false;
          }
       }
       return false;
    } , 
     
   _openEvaluationLevel         : function()
   {
     var self = this;
     if(self.options.page === "evaluate")
     {
        if(self.options.evaluationSetting['evaluate_on'] == "kpi")
        {
          self.options.openKpi = true;
        } else if(self.options.evaluationSetting['evaluate_on'] === "action") {
          self.options.openKpi = true;
          self.options.openAction = true;
        }
     }
   }, 
    
    _isEvaluationAtKpa          : function()
    {
       var self = this;
       if(self.options.evaluationSetting['evaluate_on'] == "kpa")
       {
          return true;
       }     
       return false;
    },
    
    _isEvaluationAtKpi          : function()
    {
       var self = this;
       if(self.options.evaluationSetting['evaluate_on'] == "kpi")
       {
          return true;
       }     
       return false;
    },
    
    _isEvaluationAtAction          : function()
    {
       var self = this;
       if(self.options.evaluationSetting['evaluate_on'] == "action")
       {
          return true;
       }     
       return false;
    },        
    
  _canEvaluateKPA                        : function(kpaId)
    {
      var self = this;
      if(self._isEvaluationAtKpa() && self.options.page == "evaluate")
      {
         if(!$.isEmptyObject(self.options.evaluationSetting))
         {
            if(self.options.evaluationSetting['next_evaluation'][kpaId] === "")
            {
              return false;
            } else {

               if(self.options.evaluationSetting['next_evaluation'][kpaId] == "manager")
               {
                  if(self.options.userNext == "manager")
                  {
                     //the manager evaluation part is only done by the manager of the user logged, 
                     //so the user selected(userdata)'s manager should be the userlogged(userlogged) or when the manager 
                     //is the manager of theirselves S
                     if((self.options.userdata['managerid'] == self.options.userlogged['tkid']) 
                     || (self.options.userlogged['managerid'] == "S"))
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                    return false;
                  }
               }
               
               if(self.options.evaluationSetting['next_evaluation'][kpaId] == "self")
               {
                  if(self.options.userNext == "self")
                  {
                    //self evaluation is done by the userlogged in and is the user selected    
                     if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                     return false;
                  }
               }  
               
               if(self.options.evaluationSetting['next_evaluation'][kpaId] == "joint")
               {
                  if(self.options.userNext == "joint")
                  {
                      //joint evaluation is done either by the userlogged or can be done by the manage of the user selected
                     if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
                     {
                        return true;
                     } else if((self.options.userdata['managerid'] == self.options.userlogged['tkid'])){
                        self.options.jointByManager = true;
                        return true;
                     } else {
                        return false;
                     }
                  } else {
                    return false;
                  }
                  /*if((self.options.userdata['tkid'] == self.options.userlogged['tkid']) 
                     || (self.options.userdata['managerid'] == self.options.userlogged['tkid'])
                   
                  )
                  {
                     return true;
                  } else {
                     return false;
                  }*/
               }                 
               if(self.options.evaluationSetting['next_evaluation'][kpaId] == "review")
               {
                  if(self.options.userNext == "review")
                  {
                     //evaluation review is done by any of the users who are in evaluation committee
                     if(self.options.inCommittee == true)
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                    return false;
                  }
               }                   
            }
            return true;
         }   
      } else {
         return false;
      }
    }    
        
});
