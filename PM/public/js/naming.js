$(function(){
	
	Naming.get();
	
});

var Naming		= {

     get			: function()
     {	
     $.post("main.php?controller=naming&action=getAll", function( namingData ){
	     $.each( namingData, function( index, naming){
		     $("#tablenaming").append($("<tr />",{id:"tr_"+naming.id})
			     .append($("<td />",{html:naming.id}))	
		         .append($("<td />",{html:naming.ignite_terminology}))
		         .append($("<td />")
		           .append($("<input />",{type:"text", name:"naming_"+naming.id, id:"naming_"+naming.id, value:naming.client_terminology, size:"40"}))		
		         )
		         .append($("<td />")
		         	.append($("<input />",{type:"button" ,name:"update_"+naming.id, id:"update_"+naming.id, value:"Update"}))	
		         )
		     )
		
		     $("#tr_"+naming.id).hover(function(){
			     $(this).addClass("gray");						
		     }, function(){
			     $(this).removeClass("gray");						
		     });					
		
		     $("#update_"+naming.id).on("click", function(){
			     jsDisplayResult("info", "info", "Updating  ...  <img src='public/images/loaderA32.gif' />");
			     $.post("main.php?controller=naming&action=updateNaming",{
				     client_terminology :  $("#naming_"+naming.id).val(),
				     id	 : naming.id
			     }, function( response ){
				     if(response.error){
				       jsDisplayResult("error", "error", response.text);
				     } else {
				       if(response.updated)
				       {
				          jsDisplayResult("info", "info", response.text );
				       } else {
				          jsDisplayResult("ok", "ok", response.text );
				       }
				     }							
			     },"json");						
			     return false;
		     });
	     });				
     },"json");
     }
}
