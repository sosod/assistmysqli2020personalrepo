$.widget("assurance.kpi",{

    options     : {
        start           : 0,
        limit           : 10,
        current         : 1,
        total           : 0,
        autoLoad        : true,
        assurance       : true,
        kpiid           : 0, 
        tableRef        : "table_assurance"
    } ,
    
    _init        : function()
    {
        if(this.options.autoLoad)
        {
            this._get();
        }
    
    } ,
    
    _create       : function()
    {
        var self = this;
        $(self.element).append($("<table />",{width:"100%", id:self.options.tableRef}))
    } ,
    
    _get          : function()
    {
        var self = this;
        $.getJSON("main.php?controller=kpiassurance&action=get_kpi_assurance",{
            start       : self.options.start,
            limit       : self.options.limit,
            kpaid       : self.options.kpaid
        },function(kpaData){
            $("#"+self.options.tableRef).html("");
            self._headers();
            self._display(kpaData);
        });
    } , 
    
    _display    : function(kpiData)
    {
        var self = this;
        var html = []
        if($.isEmptyObject(kpiData))
        {        
            html.push("<tr>");
              html.push("<td colspan='8'>There are no KPI assurance</td>");
            html.push("</tr>");
        } else {
           $.each(kpiData, function(index, kpi){
                html.push("<tr>");
                  html.push("<td>"+kpi.id+"</td>");
                  html.push("<td>"+kpi.system_date+"</td>");
                  html.push("<td>"+kpi.date_tested+"</td>");
                  html.push("<td>"+kpi.response+"</td>");
                  html.push("<td>"+(kpi.sign_off == 1 ? "Yes" : "No")+"</td>");
                  html.push("<td>"+kpi.assurance_by+"</td>");
                  html.push("<td>"+kpi.attachment+"</td>");
                  html.push("<td><input type='button' name='edit_"+kpi.id+"' id='edit_"+kpi.id+"' value='Edit' />");
                  html.push("<br /><input type='button' name='delete_"+kpi.id+"' id='delete_"+kpi.id+"' value='Delete' /></td>");
                html.push("</tr>");
                $("#edit_"+kpi.id).live("click", function(e){
                    self._editKpaAssurance(kpi, kpi.id);
                   e.preventDefault();
                });
                
                $("#delete_"+kpi.id).live("click", function(e){
                    if(confirm("Are you sure you want to delete this KPI assurance"))
                    {
                       self._deleteKpaAssurance(kpi.id);
                    }
                   e.preventDefault();
                });                
                                
           });
        }
        html.push("<tr>");
          html.push("<td colspan='8'><input type='button' name='add' id='add' value='Add New' /></td>");
        html.push("</tr>");
        $("#add").live("click", function(e){
            self._addKpaAssurance();
           e.preventDefault();
        });

       $("#"+self.options.tableRef).append(html.join(' ')).find('.remove_attach').remove();
       $("#"+self.options.tableRef).find("#result_message").remove();
       
       
	   $(".remove_attach").live("click", function(e){
	        if(confirm("Are you sure you want to delete this kpa assurance attachment"))
	        {
              var id  = this.id
              var ext = $(this).attr('title');
              $.post('main.php?controller=kpiassurance&action=delete_attachment', 
                { attachment : id,
                  ext        : ext,
                  id         : self.options.kpiid
                 }, function(response){     
			        if(response.error )
			        {
				       jsDisplayResult("error", "error", response.text );
			        } else {
				       jsDisplayResult("ok", "ok", response.text );
				       $("#li_"+ext).fadeOut();
				       $(".class_"+ext).fadeOut();
			        }	                
              },'json');                   
              e.preventDefault();
	        }
	    });        
    } ,
    
    _headers     : function()
    {
        var self = this;
        self._paging();
        var html = [];
        html.push("<tr>");
          html.push("<th>Reference</th>");
          html.push("<th>System Date and time</th>");
          html.push("<th>Date Tested</th>");
          html.push("<th>Response</th>");
          html.push("<th>Sign Off</th>");
          html.push("<th>Assurance By</th>");
          html.push("<th>Attachment</th>");
          html.push("<th></th>");
        html.push("</tr>");
        $("#"+self.options.tableRef).append(html.join(' '));
    } ,
    
    _paging     : function()
    {
		var self = this;
		var pages;
		if(self.options.total%self.options.limit > 0){
			pages   = Math.ceil(self.options.total/self.options.limit); 				
		} else {
			pages   = Math.floor(self.options.total/self.options.limit); 				
		}
		if( self.options.setupLegislation )
		{
			columns = 8;
		}		
		var html = [];
		html.push("<tr>")
		 html.push("<td colspan='4' class='noborder'>");
		 html.push("<input type='button' name='first' id='first' value='|<' />");
		 html.push("<input type='button' name='previous' id='previous' value='<' />");
		 html.push("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		 html.push("<input type='button' name='next' id='next' value='>' />");
		 html.push("<input type='button' name='last' id='last' value='>|' />");
		 html.push("</td>");
 		 html.push("<td colspan='4' class='noborder'></td>");
		html.push("</tr>");
		$("#"+self.options.tableRef).append( html.join(' ') );

		  if(self.options.current < 2)
		  {
                $("#first").attr('disabled', 'disabled');
                $("#previous").attr('disabled', 'disabled');		     
		  }
		  if((self.options.current == pages || pages == 0))
		  {
                $("#next").attr('disabled', 'disabled');
                $("#last").attr('disabled', 'disabled');		     
		  }		  
		$("#next").bind("click", function(evt){
			self._getNext( self );
		});
		$("#last").bind("click",  function(evt){
			self._getLast( self );
		});
		$("#previous").bind("click",  function(evt){
			self._getPrevious( self );
		});
		$("#first").bind("click",  function(evt){
			self._getFirst( self );
		});
    } ,
    
	_getNext  			: function( rk ) {
		rk.options.current = rk.options.current+1;
		rk.options.start = parseFloat( rk.options.current - 1) * parseFloat( rk.options.limit );
		this._getLegislation();
	},	
	_getLast  			: function( rk ) {
		rk.options.current =  Math.ceil( parseFloat( rk.options.total )/ parseFloat( rk.options.limit ));
		rk.options.start   = parseFloat(rk.options.current-1) * parseFloat( rk.options.limit );			
		this._getLegislation();
	},	
	_getPrevious    	: function( rk ) {
		rk.options.current  = parseFloat( rk.options.current ) - 1;
		rk.options.start 	= (rk.options.current-1)*rk.options.limit;
		this._getLegislation();			
	},	
	_getFirst  			: function( rk ) {
		rk.options.current  = 1;
		rk.options.start 	= 0;
		this._getLegislation();				
	}   ,  
    
    _addKpaAssurance        : function()
    {
        var self = this;
        if($("#add_assurance_dialog").length > 0)
        {
          $("#add_assurance_dialog").remove();
        }
        
        $("<div />",{id:"add_assurance_dialog"}).append($("<table />")
            .append($("<tr />")
              .append($("<th />",{html:"Response"}))
              .append($("<td />")
                .append($("<textarea />",{cols:"80", rows:"7", name:"response", id:"response"}))
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Sign Off"}))
              .append($("<td />")
                 .append($("<select />",{name:"signoff", id:"signoff"})
                   .append($("<option />",{value:"1", text:"Yes"}))
                   .append($("<option />",{value:"0", text:"No"}))
                 )                
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Date Tested"}))
              .append($("<td />")
                .append($("<input />",{type:"text", name:"date_tested", id:"date_tested", readonly:"readonly"})
                  .addClass("datepicker")
                )
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Attachment"}))
              .append($("<td />")
                .append($("<div />")
                  .append($("<input />",{type:"file", name:"attach_kpi_"+self.options.kpiid, id:"attach_kpi_"+self.options.kpiid}))
                  .append($("<span />",{html:"You can attach more than 1 file"})
                     .addClass("ui-state").addClass("ui-state-info").css({"padding":"5px", "width":"150px"})
                   )
                )
                .append($("<div />",{id:"file_uploaded"}))
              )
            )  
            .append($("<tr />")
              .append($("<td />",{colspan:"2"})
                .append($("<p />",{id:"message"}))
              )
            )                                    
        ).dialog({
                    autoOpen    : true,
                    modal       : true,
                    title       : "Add KPI Assurance",
                    position    : "top",
                    width       : 'auto',
                    buttons     : {
                                       " Save "     : function()
                                       {
                                           if($("#response").val() == "")
                                           {
                                             $("#message").css({"padding":"5px"}).addClass("ui-state-error").html("Please enter the reponse");
                                           } else {
                                             $("#message").css({"padding":"5px"}).addClass("ui-state-info").html("saving....")
                                             $.post("main.php?controller=kpiassurance&action=save", {
                                                response    : $("#response").val(),
                                                sign_off    : $("#signoff").val(),
                                                date_tested : $("#date_tested").val(),
                                                kpi_id      : self.options.kpiid                                           
                                             }, function(response){
                                                if(response.error)
                                                {
                                                  $("#message").css({"padding":"5px"}).addClass("ui-state-error").html(response.text);
                                                } else {
                                                  $("#message").html(response.text);
                                                  jsDisplayResult("ok", "ok", response.text );	
                                                  $("#add_assurance_dialog").remove();
                                                  self._get();
                                                }                                             
                                             },"json")
                                           }
                                            
                                       } ,
                                       
                                       " Cancel "   : function()
                                       {
                                          $("#add_assurance_dialog").remove();                                    
                                       }
                    } ,
                    close       : function(event, ui)
                    {
                        $("#add_assurance_dialog").remove();
                    } ,
                    open        : function(event, ui)
                    {
		 				var dialog     = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons    = dialog.find(".ui-dialog-buttonpane").find("button");			 				
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		 				
	                    $(".datepicker").datepicker({
		                    showOn		    : "both",
		                    buttonImage 	: "/library/jquery/css/calendar.gif",
		                    buttonImageOnly : true,
		                    changeMonth	    : true,
		                    changeYear	    : true,
		                    dateFormat 	    : "dd-M-yy",
		                    altField		: "#startDate",
		                    altFormat		: 'd_m_yy'
	                    });			
	                   $("#attach_kpi_"+self.options.kpiid).live("change", function(e){
	                      self._uploadFile(this);
	                      e.preventDefault();
	                   }); 		                    	                    
	                     				
                    }
                    
        }); 
    } ,
    
    _editKpaAssurance       : function(kpi, id)
    {
        var self = this;
        if($("#assurance_dialog_"+id).length > 0)
        {
          $("#assurance_dialog_"+id).remove();
        }
        
        $("<div />",{id:"assurance_dialog_"+id}).append($("<table />")
            .append($("<tr />")
              .append($("<th />",{html:"Response"}))
              .append($("<td />")
                .append($("<textarea />",{cols:"80", rows:"7", name:"response", id:"response", text:kpi.response}))
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Sign Off"}))
              .append($("<td />")
                 .append($("<select />",{name:"signoff", id:"signoff"})
                   .append($("<option />",{value:"1", text:"Yes"}))
                   .append($("<option />",{value:"0", text:"No"}))
                 )                
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Date Tested"}))
              .append($("<td />")
                .append($("<input />",{type:"text", name:"date_tested", id:"date_tested", readonly:"readonly", value:kpi.date_tested})
                  .addClass("datepicker")
                )
              )
            )
            .append($("<tr />")
              .append($("<th />",{html:"Attachment"}))
              .append($("<td />")
                .append($("<div />")
                  .append($("<input />",{type:"file", name:"attach_kpi_"+self.options.kpiid, id:"attach_kpi_"+self.options.kpiid}))
                   .append($("<span />",{html:"You can attach more than 1 file"})
                     .addClass("ui-state").addClass("ui-state-info").css({"padding":"5px", "width":"150px"})
                   )
                )
                .append($("<div />",{id:"file_uploaded"}))
                .append($("<div />",{id:"__files", html:kpi.attachment}))
              )
            )  
            .append($("<tr />")
              .append($("<td />",{colspan:"2"})
                .append($("<p />",{id:"message"}))
              )
            )                                    
        ).dialog({
                    autoOpen    : true,
                    modal       : true,
                    title       : "Edit KPI Assurance",
                    position    : "top",
                    width       : 'auto',
                    buttons     : {
                                       " Save Changes "     : function()
                                       {
                                           if($("#response").val() == "")
                                           {
                                             $("#message").css({"padding":"5px"}).addClass("ui-state-error").html("Please enter the reponse");
                                           } else {
                                             $("#message").css({"padding":"5px"}).addClass("ui-state-info").html("updating....")
                                             $.post("main.php?controller=kpiassurance&action=update", {
                                                response    : $("#response").val(),
                                                sign_off    : $("#signoff").val(),
                                                date_tested : $("#date_tested").val(),
                                                id          : id                                           
                                             }, function(response){
                                                if(response.error)
                                                {
                                                  $("#message").css({"padding":"5px"}).addClass("ui-state-error").html(response.text);
                                                } else {
                                                  if(response.updated)
                                                  {
                                                    jsDisplayResult("info", "info", response.text );	                
                                                  } else {
                                                    jsDisplayResult("ok", "ok", response.text );	                
                                                  }
                                                  $("#assurance_dialog_"+id).remove();
                                                  self._get();
                                                }                                             
                                             },"json")
                                           }
                                            
                                       } ,
                                       
                                       " Cancel "   : function()
                                       {
                                          $("#assurance_dialog_"+id).remove();                                    
                                       }
                    } ,
                    close       : function(event, ui)
                    {
                        $("#assurance_dialog_"+id).remove();
                    } ,
                    open        : function(event, ui)
                    {
		 				var dialog     = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons    = dialog.find(".ui-dialog-buttonpane").find("button");			 				
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		 				$("#signoff").val(kpi.sign_off);
		 				
	                    $(".datepicker").datepicker({
		                    showOn		    : "both",
		                    buttonImage 	: "/library/jquery/css/calendar.gif",
		                    buttonImageOnly : true,
		                    changeMonth	    : true,
		                    changeYear	    : true,
		                    dateFormat 	    : "dd-M-yy",
		                    altField		: "#startDate",
		                    altFormat		: 'd_m_yy'
	                    });
	                    	                    
	                   $("#attach_kpi_"+self.options.kpiid).live("change", function(e){
	                      self._uploadFile(this);
	                      e.preventDefault();
	                   }); 		                    
	                    			 				
                    }
                    
        }); 
    }, 
    
    _deleteKpaAssurance             : function(id)
    {
        var self = this;
         $("#message").css({"padding":"5px"}).addClass("ui-state-info").html("updating....")
         $.post("main.php?controller=kpiassurance&action=update", {
            status      : 2,
            id          : id                                           
         }, function(response){
            if(response.error)
            {
              jsDisplayResult("error", "error", response.text );	
            } else {
              if(response.updated)
              {
                jsDisplayResult("info", "info", response.text );	                
              } else {
                jsDisplayResult("ok", "ok", response.text );	                
              }
              self._get();
            }                                             
         },"json")
    }, 
    
    _uploadFile                     : function(file)
    {
       var self = this;
       $("#uploaded_image").empty();
       $("#file_uploaded").html("uploading  ... <img  src='public/images/loaderA32.gif' />");
       $.ajaxFileUpload({
            url           : "main.php?controller=kpiassurance&action=upload_attachment&id="+self.options.kpiid,
            dataType      : "json",
            secureuri     : true,
            fileElementId : "attach_kpi_"+self.options.kpiid,
            success       : function(response, status)
            {
                 $("#file_uploaded").html("");
                 if(response.error)
                 {
                   $("#file_uploaded").removeClass('ui-state-ok').addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                 } else {
                   $("#file_uploaded").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).removeClass('ui-state-error').addClass('ui-state-ok')
                   ).append($("<div />",{id:"files_uploaded"}))
                   if(!$.isEmptyObject(response.files))
                   {     
                      var list = [];            
                      $.each(response.files, function(ref, file){
                      	list.push("<p id='li_"+ref+"' style='padding:2px;' class='ui-state'>");
                      	  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                      	  list.push("<span style='margin-left:5px;'><a href='../class/request.php?action=main.php?controller=kpiassurance&action=download_file&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                      	list.push("</p>");
                        //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                      });
                      //console.log( list );
                      $("#files_uploaded").html(list.join(' '));
                      
                      $(".delete").click(function(e){
                        var id = this.id
                        var ext = $(this).attr('title');
                        $.post('main.php?controller=kpiassurance&action=remove_attachment', 
                          {
                           attachment : id,
                           ext        : ext,
                           action_id  : self.options.kpiid,
                          }, function(response){     
                          if(response.error)
                          {
                            $("#result_message").html(response.text)
                          } else {
                             $("#result_message").addClass('ui-state-ok').html(response.text)
                             $("#li_"+id).fadeOut();
                          }
                        },'json');                   
                        e.preventDefault();
                      });
                     $("#attach_kpi_"+self.options.kpiid).val("");
                   } 
                 }            
            } ,
            error         : function(data, status, e)
            {
              $("#file_upload").html( "Ajax error -- "+e+", please try uploading again");             
            }
        });    
    } 
        
    


});
