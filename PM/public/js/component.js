$(function(){
	
	Performancecomponents.getEvaluationyears();
	$("#addcomponent").hide();	
	
	$("#evaluationyear").change(function(){
		var evaluationyear = $(this).val();
		if(evaluationyear !== "")
		{
		     $("#evaluationyearid").val( evaluationyear );
		     $("#addcomponent").fadeIn();
		} else {
		    $("#addcomponent").fadeOut();
		}
	     Performancecomponents.get( evaluationyear );
		return false;
	})

	
	$("#addnew").click(function(){
		
		
		if( $("#editnewdialog").length > 0 )
		{
			$("#editnewdialog").remove();
		}		
		
		$("<div />",{id:"addnewdialog"})
		 .append($("<table />")
		   .append($("<tr />")
			 .append($("<th />",{html:"Component Code:"}))
			 .append($("<td />")
			    .append($("<input />",{type:"text", name:"shortcode", id:"shortcode", value:""}))		 
			 )
		   )
		   .append($("<tr />")
			  .append($("<th />",{html:"Performance Management Component Name:"}))
			  .append($("<td />")
				 .append($("<textarea />",{name:"name", id:"name"}))	  
			  )
		   )
		   .append($("<tr />")
			 .append($("<th />",{html:"Perfomance Management Component Description:"}))
			 .append($("<td />")
			   .append($("<textarea />",{id:"description", name:"description"}))		 
			 )
		   )
		   .append($("<tr />")
			 .append($("<td />",{colspan:"2"})
			   .append($("<p />")
				 .append($("<span />"))
				 .append($("<span />",{id:"message"}))
			   )		 
			 )	   
		   )
		 )
		 .dialog({
			 		autoOpen	: true, 
			 		modal		: true, 
			 		width		: "600px", 
			 		position	: "top",
			 		title		: "Add New Performance Management Component",
			 		buttons		: {
			 						"Save"		: function()
			 						{
			 							if( $("#evaluation_year :selected").val() == "") {
			 								$("#message").html("Please select the evaluation year");
			 								return false;
			 							} else if( $("#shortcode").val() == "")	{
			 								$("#message").html("Please enter the shortcode")
			 								return false;
			 							} else if( $("#name").val() == "") {
			 								$("#message").html("Please enter the name of the component")
			 								return false;
			 							} else if( $("#description").val() == "") {
			 								$("#message").html("Please enter the description of the component");
			 								return false;
			 							} else {
			 								Performancecomponents.save();
			 							}
			 						} , 
			 						
			 						"Cancel"	: function()
			 						{			 							
			 							$(this).dialog("destroy");
			 						}			 			 
		 			} , 
		 			open 		: function( event, ui)
		 			{
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		 				
		 			}
			 	
		 });
			$.getJSON("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ) {				
				$.each( responseData, function( index, ev_year) {
					$("#evaluation_year").append($("<option />",{text:ev_year.start+" - "+ev_year.end, value:ev_year.id}))
				})
			});
		
		return false;
	});
	
});

var Performancecomponents  = {
		
		getEvaluationyears		: function()
		{
			$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
				$.each( responseData, function( index, evalYear) {
					$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))					
				});								
			},"json");
		} , 		
		
		get			: function( evaluationyear )
		{
			$(".comp").remove()
			$.getJSON("main.php?controller=component&action=getAll",{
				evaluationyear	: evaluationyear				
			}, function(responseData) {
				if($.isEmptyObject(responseData))
				{
					$("#table_performancecomponents").append($("<tr />").addClass("comp")
					  .append($("<td />",{colspan:"6", html:"There are no performance componets in this evaluation year"}))		
					)
				} else {
					$.each(responseData, function( index, component){
						Performancecomponents.display( component );									
					});					
				}	
			});
	
		} , 
		
		
		save		: function()
		{
			var evalYear = $("#evaluationyearid").val();
			$.post("main.php?controller=component&action=saveComponent",{
				data :	{	name 			: $("#name").val(),
							shortcode		: $("#shortcode").val(),
							description		: $("#description").val(), 
							evaluationyear	: evalYear  
			  }
			}, function( response ) {
				if( response.error )
				{
					$("#message").html( response.text )
				} else {
					Performancecomponents.get( evalYear);
					jsDisplayResult("ok", "ok", response.text );
					$("#addnewdialog").dialog("destroy");
					$("#addnewdialog").remove();	
				}				
			},"json");
		} , 
		
		
		display		: function( data )
		{
			$("#table_performancecomponents").append($("<tr />").addClass("comp")
			  .append($("<td />",{html:data.id}))
			  .append($("<td />",{html:data.shortcode}))
			  .append($("<td />",{html:data.name}))
			  .append($("<td />",{html:data.description}))
			  .append($("<td />",{html:((data.status & 1) == 1 ? "Active" : "Inacitve")}))
			  .append($("<td />")
				 .append($("<input />",{type:"button", name:"edit_"+data.id, id:"edit_"+data.id, value:"Edit"}))	  
			  )
			)
			
			$("#edit_"+data.id).live("click", function(){
				
				if( $("#editnewdialog").length > 0 )
				{
					$("#editnewdialog").remove();
				}
				
				$("<div />",{id:"editnewdialog"})
				 .append($("<table />")
				   .append($("<tr />")
					 .append($("<th />",{html:"Component Code:"}))
					 .append($("<td />")
					    .append($("<input />",{type:"text", name:"shortcode", id:"shortcode", value:data.shortcode}))		 
					 )
				   )
				   .append($("<tr />")
					  .append($("<th />",{html:"Performance Management Component Name:"}))
					  .append($("<td />")
						 .append($("<textarea />",{name:"name", id:"name", text:data.name}))	  
					  )
				   )
				   .append($("<tr />")
					 .append($("<th />",{html:"Perfomance Management Component Description:"}))
					 .append($("<td />")
					   .append($("<textarea />",{id:"description", name:"description", text:data.description}))		 
					 )
				   )
				   .append($("<tr />")
					 .append($("<td />",{colspan:2})
					   .append($("<p />")
						 .append($("<span />"))
						 .append($("<span />",{id:"message"}))
					   )		 
					 )	   
				   )
				 )
				 .dialog({	
					 		autoOpen	: true, 
					 		modal		: true, 
					 		width		: "600px", 
					 		position	: "top",
					 		title		: "Edit Performance Management Component",
					 		buttons		: {
					 						"Update"		: function()
					 						{
					 							if( $("#evaluation_year :selected").val() == "") {
					 								$("#message").html("Please select the evaluation year");
					 								return false;
					 							} else if( $("#shortcode").val() == "")	{
					 								$("#message").html("Please enter the shortcode")
					 								return false;
					 							} else if( $("#name").val() == "") {
					 								$("#message").html("Please enter the name of the component")
					 								return false;
					 							} else if( $("#description").val() == "") {
					 								$("#message").html("Please enter the description of the component");
					 								return false;
					 							} else {
					 								Performancecomponents.updateComponent( data.id );
					 							}
					 						} , 
					 						
					 						"Cancel"	: function()
					 						{			 							
					 							$(this).dialog("destroy");
					 						} , 
					 						
					 						"Delete"		: function()
					 						{
					 							Performancecomponents.deleteComponent( data.id );					 							
					 						} , 
					 						
					 						"Deactivate"	: function()
					 						{
					 							Performancecomponents.updateStatus( data.id, 0);					 							
					 						} , 
					 						
					 						"Activate"		: function()
					 						{
					 							Performancecomponents.updateStatus( data.id, 1);
					 						}
				 			} , 
				 			open 		: function( event, ui)
				 			{
				 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
				 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
			 				
				 				var saveButton = buttons[0];
				 				var deleteButton = buttons[2]
				 				var deactivateButton = buttons[3];
				 				var activateButton   = buttons[4];
				 				$(saveButton).css({"color":"#090"});
				 				$(deleteButton).css({"color":"red", display:((data.status & 4) == 4 ? "none" : "inline")});
				 				$(deleteButton).css({display:(data.used ? "none" : "inline")});
				 				$(deactivateButton).css({"color":"red", display:(data.used ? ((data.status & 1) == 1 ? "inline" : "none") : "none")});
				 				$(activateButton).css({"color":"#090", display:(data.used ? ((data.status & 1) == 1 ? "none" : "inline") : "none")});	
				 			}
					 	
				 });
				$.getJSON("main.php?controller=component&action==getAll", { status : 1}, function( responseData ) {				
					$.each( responseData, function( index, ev_year) {
						$("#evaluation_year").append($("<option />",{text:ev_year.start+" - "+ev_year.end, value:ev_year.id, selected:(data.evaluationyear == ev_year.id ? "selected" : "")}))
					})
				});
				return false;
			})
			
		} , 
		
		deleteComponent			: function( id )
		{
			if( confirm("Are you sure you want to delete this component") )
			{
				Performancecomponents.updateStatus( id, 2);
			}
		} , 
		
		updateStatus			: function( id, status)
		{
			$.post("main.php?controller=component&action=updateComponent", {
				id 		: id,
				data 	: { status : status }
			}, function( response ){
				if( response.error )
				{
					$("#message").html( response.text )
				} else {
					Performancecomponents.get( $("#evaluationyearid").val() );
				       if(response.updated)
				       {
					     jsDisplayResult("info", "info", response.text );
				       } else {
					     jsDisplayResult("ok", "ok", response.text );					
				       } 	
					$("#editnewdialog").dialog("destroy");
					$("#editnewdialog").remove();	
				} 				
			},"json");			
		} , 
		
		updateComponent			: function( id )
		{
			$.post("main.php?controller=component&action=updateComponent",{
				data :	{	name 			: $("#name").val(),
							shortcode		: $("#shortcode").val(),
							description		: $("#description").val() 
			  } , 
			  id	: id
			}, function( response ) {
				if( response.error )
				{
					$("#message").html( response.text )
				} else {
					Performancecomponents.get( $("#evaluationyearid").val() );
				       if(response.updated)
				       {
					     jsDisplayResult("info", "info", response.text );
				       } else {
					     jsDisplayResult("ok", "ok", response.text );					
				       } 	
					$("#editnewdialog").dialog("destroy");
					$("#editnewdialog").remove();	
				}				
			},"json");
		}
		
		
		
};
