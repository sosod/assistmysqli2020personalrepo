$.widget("ui.action", {
    
   options        : {
       start             : 0,
       limit             : 0,
       componentid       : 0,
       kpiid             : 0,
       kpaid             : 0,
       section           : "",
       page              : "",
       displayOptions    : {}, 
       url               : "",
       actionSettings    : {},
       user              : "" ,
       evaluations       : {},
       autoLoad          : true, 
       nextEvaluation    : {},
       statuses          : {}, 
       kpistatus         : {},
       userdata          : {},
       userlist          : {},
       userlogged        : {},
       jointByManager    : false,
       inCommittee       : false,
       userNext          : "",
       actionNextEvaluation : {}
    } ,
    
    _init           : function()
    {
      if(this.options.autoLoad)
      {
        this._get();
      }
    } ,
    
    _create         : function()
    {
        var self = this;
    } ,
    
    _get            : function()
    {
		var self = this;
		var actionUrl = (self.options.url == "" ? "main.php?controller=action&action=getAll" : self.options.url);
		$.getJSON(actionUrl,{
			kpiid 	     : self.options.kpiid,
			componentid  : self.options.componentid,
			section	     : self.options.section,
			user         : self.options.user,
			kpistatus    : self.options.kpistatus,
		     page        : self.options.page
		}, function(actionsData) {
               self.options.userdata     = actionsData.userdata
               self.options.userlogged   = actionsData.userlogged
               self.options.inCommittee = actionsData.in_committee;		
               if(actionsData.hasOwnProperty('settings'))
               {
                  self.options.actionSettings = actionsData['settings'];
                  if(actionsData['settings'].hasOwnProperty('next_evaluation'))
                  {
                    self.options.actionNextEvaluation = actionsData['settings']['next_evaluation'];
                  }
                  if(actionsData['settings'].hasOwnProperty('evaluations'))
                  {
                    self.options.evaluations = actionsData['settings'].evaluations;    
                  }                     
                      
               }
               if(actionsData.hasOwnProperty('statuses'))
               {
                  self.options.statuses = actionsData.statuses;
               }
			self._display(actionsData.data, actionsData.parentkpi);			
		});
    } ,
    
    _display         : function(actions, kpiid)
    {
       var self           = this;
       var actionSettings = self.options.actionSettings;
       $.each(actions, function(index, action){
         var html = []
         html.push("<td>&nbsp;&nbsp;</td>");
         html.push("<td>&nbsp;&nbsp;</td>");
           html.push("<td>");
             html.push("<span class='ui-icon ui-icon-carat-1-sw'>&nbsp;&nbsp;</span>");
           html.push("</td>");
         $.each( action, function(key, val){
            html.push("<td style='color:#555555;'>"+val+"</td>");
         })
         var displayOptions           = self.options.displayOptions;
         var displayActionEvaluateBtn = self._canEvaluate(index);
         displayEditActionEvaluateBtn = self._canEditEvaluate(index);
         var canUpdate                = true;
         var canEdit                  = true;
         var canDelete                = true;
         var canAssurance             = true;
         var canUpdate                = true;
         var canUpdate                = true;
         var updateText               = "Update Action";
         if(self.options.statuses.hasOwnProperty(index))
         {
           if(((self.options.statuses[index] & 32) == 32))
           {
              canUpdate  = false;
              updateText = "Awaiting Approval";
           }
           if(((self.options.statuses[index] & 64) == 64))
           {
              canUpdate   = false
              updateText  = "Approved";
           }
           if((self.options.statuses[index] & 128) == 128)
           {
              canEdit      = false;
              canUpdate    = false;
              canAssurance = false;
              canDelete    = false; 
              if(self.options.page != "evaluate")
              {                  
                displayOptions['displayActionEvaluateBtn'] = false;
                displayActionEvaluateBtn                   = false;
              }
           }
           
         }
         if(displayOptions['displayTd'])
         {
            html.push("<td>");
             if(displayOptions['displayEditBtn'])
             {
                if(canEdit)
                {
                   html.push("<input type='button' name='"+self.options.kpiid+"_edit_action_"+index+"' id='"+self.options.kpiid+"_edit_action_"+index+"' value='Add Action' />");
                   $("#"+self.options.kpiid+"_edit_action_"+index).live("click", function(){
                        if(self.options.section == "setup")
                        {
                            document.location.href = "main.php?controller=action&action=setup_editaction&id="+index+"&kpiid="+self.options.kpiid+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section
                        } else {
                           document.location.href = "main.php?controller=action&action=edit_action&id="+index+"&kpiid="+self.options.kpiid+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section
                        }
                        return false;
                   });                   
                   
                   html.push("<input type='button' name='"+self.options.kpiid+"_delete_action_"+index+"' id='"+self.options.kpiid+"_delete_action_"+index+"' value='Delete Action' />");
                   $("#"+self.options.kpiid+"_delete_action_"+index).live("click", function(){
                       jsDisplayResult("info", "info", "deleting action . . . <img src='public/images/loaderA32.gif' /> " );               
                       if(confirm("Are you sure you want to delete this action"))
                       {
		                  $.post("main.php?controller=action&action=updateStatus",{
			                  actionstatus	: 2,
			                  id		    : index
		                  }, function( response ){
			                if( response.error )
			                {
				                jsDisplayResult("error", "error", response.text );
			                } else {		     
		                        if(response.updated)
		                        {
                                     $("#"+self.options.kpaid+"_child_"+self.options.kpiid+"_action_"+index).remove();		
                                     jsDisplayResult("info", "info", response.text);
		                        } else {
		                           $("#"+self.options.kpaid+"_child_"+self.options.kpiid+"_action_"+index).remove();		
			                      jsDisplayResult("ok", "ok", response.text);
			                      self._get();		               
		                        }			               	
			                }		
		                  },"json");                    
                       }           
                       return false;
                   });                    
                } else {
                   html.push("<input type='button' name='edit_action__"+index+"' id='edit_action__"+index+"' value='Add Action' disabled='disabled' />");
                   html.push("<input type='button' name='delete_action_"+index+"' id='delete_action_"+index+"' disabled='disabled' value='Delete Action' />");                   
                   
                }
             }

             if(displayOptions['displayUpdateBtn'])
             {
                if(canUpdate)
                {
                   html.push("<input type='button' name='update_action_"+index+"' id='update_action_"+index+"' value='Evaluate Action' />");
                   $("#update_action_"+index).live("click", function(){
                       document.location.href = "main.php?controller=action&action=updateaction&id="+index+"&kpiid="+self.options.kpiid+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section
                    return false;
                   });                     
                } else {
                    html.push("<input type='button' name='update_action__"+index+"' id='update_action__"+index+"' value='Evaluate Action' disabled='disabled' />");                    
                }
             }
             if(displayActionEvaluateBtn)
             {
                html.push("<input type='button' name='"+self.options.kpiid+"_evaluate_action_"+index+"' id='"+self.options.kpiid+"_evaluate_action_"+index+"' value='Evaluate Action' />");                 
                $("#"+self.options.kpiid+"_evaluate_action_"+index).live("click", function(e){
                   self._evaluateAction(index, self.options.kpiid);
                   e.preventDefault();                                
                });                 
             }
             
             if(displayEditActionEvaluateBtn)
             {
                html.push("<input type='button' name='"+self.options.kpiid+"_edit_evaluate_action_"+index+"' id='"+self.options.kpiid+"_edit_evaluate_action_"+index+"' value='Edit Action Evaluation' />");                 
                $("#"+self.options.kpiid+"_edit_evaluate_action_"+index).live("click", function(e){
                   self._editEvaluateAction(index, kpiid);
                   e.preventDefault();                                
                });                 
             }             
             
             
             if(displayOptions['displayActionEvaluateBtn'])
             {
                html.push("<br />\r\n\n\n\n");   
                html.push("<span style='padding:5px;'></span>");
                html.push("<span style='float:left;' class='"+actionSettings['evaluationstatuses'][index]['self']['status']+"'></span>");
                html.push("<span style='color:"+(self._isNextActionEvaluation(index, "self") ? "#000000" : "#AAAAAA")+"'>");
                    html.push("Self Evaluation");
                html.push("</span>");
                
                html.push("<br />\r\n\n\n\n");
                  html.push("<span style='float:left;' class='"+actionSettings['evaluationstatuses'][index]['manager']['status']+"'></span>");
                html.push("<span style='color:"+(self._isNextActionEvaluation(index, "manager") ? "" : "#AAAAAA")+"'>");
                  html.push("Manager Evaluation");
                html.push("</span>");
                html.push("<br />\r\n\n\n\n");
                html.push("<span style='float:left;' class='"+actionSettings['evaluationstatuses'][index]['joint']['status']+"'></span>");
                html.push("<span style='color:"+(self._isNextActionEvaluation(index, "joint") ? "" : "#AAAAAA")+"'>");
                  html.push("Joint Evaluation");
                html.push("</span>");
                html.push("<br />\r\n\n\n\n");
                  html.push("<span style='float:left;' class='"+actionSettings['evaluationstatuses'][index]['review']['status']+"'></span>");
                html.push("<span style='color:"+(self._isNextActionEvaluation(index, "review") ? "" : "#AAAAAA")+"'>");                  
                  html.push("Evaluation Review");
                html.push("</span>");                       
                html.push("<span style='padding:5px;'></span>");                    
             }
             if(displayOptions['displayAssuranceBtn'])
             {
                html.push("<input type='button' name='"+self.options.kpiid+"_assurance_action_"+index+"' id='"+self.options.kpiid+"_assurance_action_"+index+"' value='Assurance Action' />");                 

               $("#"+self.options.kpiid+"_assurance_action_"+index).live("click", function(){
                   document.location.href = "main.php?controller=action&action=assuranceaction&id="+index+"&kpiid="+self.options.kpiid+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section
                return false;
               });         
             }
             
            html.push("</td>");
         }        
         if(self._isEvaluationAtAction() && displayOptions['displayActionEvaluateBtn'])
         {           
                var evaluations = self.options.evaluations;          
                 if(displayOptions['selfEvaluationRating'])
                 {
                    html.push("<td>"+self._evals(index, 'self', 'rating')+"</td>");
                 }
                 if(displayOptions['selfEvaluationComment'])
                 {
                    html.push("<td>"+self._evals(index, 'self', 'comment')+"</td>");
                 }
                 if(displayOptions['managerEvaluationRating'])
                 {
                    html.push("<td>"+self._evals(index, 'manager', 'rating')+"</td>");
                 }      
                 if(displayOptions['managerEvaluationComment'])
                 {
                    html.push("<td>"+self._evals(index, 'manager', 'comment')+"</td>");
                 } 
                 if(displayOptions['jointEvaluationRating'])
                 {
                    html.push("<td>"+self._evals(index, 'joint', 'rating')+"</td>");
                 }      
                 if(displayOptions['jointEvaluationComment'])
                 {
                    html.push("<td>"+self._evals(index, 'joint', 'comment')+"</td>");
                 }   
                 if(displayOptions['evaluationReviewRating'])
                 {
                    html.push("<td>"+self._evals(index, 'review', 'rating')+"</td>");
                 }      
                 if(displayOptions['evaluationReviewComment'])
                 {
                    html.push("<td>"+self._evals(index, 'review', 'comment')+"</td>");             
                 }                 
          } else {
             if(self.options.page == "evaluate")
             {
                html.push("<td colspan='8' style='color:#AAAAAA'>");
                 html.push("<b>No evaluation at action</b>");
                html.push("</td>");                
             }   
          }
          $("#"+self.options.kpaid+"_child_"+self.options.kpiid+"_action_"+index).append( html.join(' ') );
       });     
    } ,
    
    _evaluateAction           : function(actionId, kpiid)
    {
      var self = this;
      if($("#actionevaluation_"+actionId).length > 0)
      {
         $("#actionevaluation_"+actionId).remove();
      }    
      
      $("<div />",{id:"actionevaluation_"+actionId}).append($("<table />")
        .append($("<tr />")
           .append($("<th />",{html:"Rating :"}))
           .append($("<td />")
              .append($("<select />",{id:"actionrating", name:"actionrating"}))
           )
        )
        .append($("<tr />")
           .append($("<th />",{html:"Comment :"}))
           .append($("<td />")
              .append($("<textarea />",{id:"actioncomment", name:"actioncomment", rows:"7", cols:"50"}))
           )
        )
        .append($("<tr />")
           .append($("<th />",{html:"Recommendation :"}))
           .append($("<td />")
              .append($("<textarea />",{id:"actionrecommendation", name:"actionrecommendation", rows:"7", cols:"50"}))
           )
        ) 
        .append($("<tr />")
           .append($("<td />",{colspan:2})
              .append($("<p />").css({padding:"5px"})
                .append($("<span />",{id:"message_"+actionId}))
              )
           )
        )         
      ).dialog({
          autoOpen    : true,
          title       : "Action Evaluation",
          position    : "top",
          width       : 500,
          modal       : true,
          buttons     : {
                           "Save"       : function()
                           {                       
                              if($("#actionrating").val() == "")
                              {
                                $("#message_"+actionId).addClass("ui-state-error").css({padding:'5px'}).html("Please select the rating");
                                return false;
                              } else if($("#actioncomment").val() == "") {
                                $("#message_"+actionId).addClass("ui-state-error").css({padding:'5px'}).html("Please enter the comment for this evaluation");
                                return false
                              } else {
                                  if(self.options.jointByManager)
                                  {
                                     self._showConfirmJointEvaluationDialog(actionId);  
                                  } else {
                                     self._saveActionEvaluation(actionId, kpiid);
                                  }                                     
                              }
                           } ,
                           "Cancel"     : function()
                           {
                              $("#actionevaluation_"+actionId).dialog("destroy");
                              $("#actionevaluation_"+actionId).remove();
                           }
                        },
          open        : function(event, ui)
          {
			 var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			 var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
			 var saveButton = buttons[0];
			 $(saveButton).css({"color":"#090"});  
			          
                var ratingScales = $('body').data('ratingScales');
                if($.isEmptyObject(ratingScales))
                {
                     $.getJSON("main.php?controller=ratingscale&action=getAll",{status:1} ,function(ratingScales){
                        if(!$.isEmptyObject(ratingScales))
                        {
                           $('body').data('ratingScales', ratingScales);
                           $("#actionrating").empty();
                           $("#actionrating").append($("<option />",{text:"--please select--", value:""}));
                           $.each(ratingScales, function(index, ratingScale){
                              $("#actionrating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));
                           });
                        }
                     });
                } else {
                      $("#actionrating").empty();
                      $("#actionrating").append($("<option />",{text:"--please select--", value:""}));
                      $.each(ratingScales, function(index, ratingScale) {
                         $("#actionrating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));
                      });                    
                } 			      
          } ,
          close       : function(event, ui)
          {
             $("#actionevaluation_"+actionId).dialog("destroy");
             $("#actionevaluation_"+actionId).remove();          
          }   
      })
    } ,
    
    _editEvaluateAction           : function(actionId, kpiid)
    {
      var self = this;
      if($("#editactionevaluation_"+actionId).length > 0)
      {
         $("#editactionevaluation_"+actionId).remove();
      }    
      var evaluations   = self.options.evaluations[actionId][self.options.userNext];
      console.log("Evaluations ---- ");
      console.log( evaluations );
      
      $("<div />",{id:"editactionevaluation_"+actionId}).append($("<table />")
        .append($("<tr />")
           .append($("<th />",{html:"Rating :"}))
           .append($("<td />")
              .append($("<select />",{id:"actionrating", name:"actionrating"}))
           )
        )
        .append($("<tr />")
           .append($("<th />",{html:"Comment :"}))
           .append($("<td />")
              .append($("<textarea />",{id:"actioncomment", name:"actioncomment", rows:"7", cols:"50", text:evaluations['comment']}))
           )
        )
        .append($("<tr />")
           .append($("<th />",{html:"Recommendation :"}))
           .append($("<td />")
              .append($("<textarea />",{id:"actionrecommendation", name:"actionrecommendation", rows:"7", cols:"50", text:evaluations['recommendation']}))
           )
        ) 
        .append($("<tr />")
           .append($("<td />",{colspan:2})
              .append($("<p />").css({padding:"5px"})
                .append($("<span />",{id:"message_"+actionId}))
              )
           )
        )         
      ).dialog({
          autoOpen    : true,
          title       : "Edit Action Evaluation",
          position    : "top",
          width       : 500,
          modal       : true,
          buttons     : {
                           "Save Changes"       : function()
                           {                       
                              if($("#actionrating").val() == "")
                              {
                                $("#message_"+actionId).addClass("ui-state-error").css({padding:'5px'}).html("Please select the rating");
                                return false;
                              } else if($("#actioncomment").val() == "") {
                                $("#message_"+actionId).addClass("ui-state-error").css({padding:'5px'}).html("Please enter the comment for this evaluation");
                                return false
                              } else {
                                  if(self.options.jointByManager)
                                  {
                                     self._showConfirmJointEvaluationDialog(actionId);  
                                  } else {
                                     self._updateActionEvaluation(actionId, kpiid);
                                  }                                     
                              }
                           } ,
                           "Cancel"     : function()
                           {
                              $("#actionevaluation_"+actionId).dialog("destroy");
                              $("#actionevaluation_"+actionId).remove();
                           }
                        },
          open        : function(event, ui)
          {
			 var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			 var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
			 var saveButton = buttons[0];
			 $(saveButton).css({"color":"#090"});  
			          
                var ratingScales = $('body').data('ratingScales');
                if($.isEmptyObject(ratingScales))
                {
                     $.getJSON("main.php?controller=ratingscale&action=getAll",{status:1} ,function(ratingScales){
                        if(!$.isEmptyObject(ratingScales))
                        {
                           $('body').data('ratingScales', ratingScales);
                           $("#actionrating").empty();
                           $("#actionrating").append($("<option />",{text:"--please select--", value:""}));
                           $.each(ratingScales, function(index, ratingScale){
                             if(evaluations['ratingid'] == ratingScale.id)
                             {
                                $("#actionrating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id, selected:"selected"}));                               
                             } else { 
                                $("#actionrating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id})); 
                             }
                           });
                        }
                     });
                } else {
                      $("#actionrating").empty();
                      $("#actionrating").append($("<option />",{text:"--please select--", value:""}));
                      $.each(ratingScales, function(index, ratingScale) {
                         if(evaluations['ratingid'] == ratingScale.id)
                         {
                            $("#actionrating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id, selected:"selected"}));                               
                         } else { 
                            $("#actionrating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id})); 
                         }
                      });                    
                } 			      
          } ,
          close       : function(event, ui)
          {
               $("#actionevaluation_"+actionId).dialog("destroy");
               $("#actionevaluation_"+actionId).remove();          
          }   
      })
    } ,    
    
    _showConfirmJointEvaluationDialog   : function(actionId)
    {
        var self = this;
        if($("#confirmjointdialog").length > 0)
        {      
          $("#confirmjointdialog").remove();
        }
        var html = [];
        if(self.options.userlogged['tkid'] == self.options.userdata['tkid'])
        {
           html.push("<div>")
            html.push("I, "+self.options.userdata['user']+", reporting to "+self.options.userdata['managername']+" hereby:");
            html.push("<ul>");
             html.push("<li>");
               html.push("Confirm that I met with "+self.options.userdata['managername']+" to jointly evaluate my performance on this component of the Performance Matrix for the evaluation period "+self.options.userlogged['evaluationperiod']+" for the evaluation year "+self.options.userdata['start']+" to "+self.options.userdata['end']+";");
             html.push("</li>");
             html.push("<li>");
               html.push("Declare that we agreed on this joint rating;");
             html.push("</li>");             
             html.push("<li>");
               html.push("Declare that I have the required authority to confirm this joint evaluation rating.");
             html.push("</li>");            
            html.push("</ul>");
           html.push("</div>");
        } else {
           html.push("<div>")
            html.push("I, "+self.options.userdata['managername']+", manager of "+self.options.userdata['user']+" hereby:");
            html.push("<ul>");
             html.push("<li>");
               html.push("Confirm that I met with "+self.options.userdata['user']+" to jointly evalute the performance on this component of the Performance Matrix for the evaluation period "+self.options.userlogged['evaluationperiod']+" for the evaluation year "+self.options.userdata['start']+" "+self.options.userdata['end']+";");
             html.push("</li>");
             html.push("<li>");
               html.push("Declare that we agreed on this joint rating;");
             html.push("</li>");             
             html.push("<li>");
               html.push("Declare that I have the required authority to confirm this joint evaluation rating.");
             html.push("</li>");            
            html.push("</ul>");
           html.push("</div>");
        }
        $("<div />", {id:"confirmjointdialog"}).append( html.join(' ') )
        .append($("<div />",{id:"message"}))
        .dialog({
                 autoOpen  : true,
                 modal     : true,
                 position  : "top",
                 width     : 350,
                 title     : "Joint Evaluation of Performance Matrix",
                 buttons   : {
                               "Ok"     : function()
                               {
                                   self._saveActionEvaluation(actionId);
                                   $("#confirmjointdialog").dialog("destroy");
                                   $("#confirmjointdialog").remove();   
                               } ,
                               "Cancel"       : function()
                               {
                                  $("#confirmjointdialog").dialog("destroy");
                                  $("#confirmjointdialog").remove();
                               }
                 } , 
                 close      : function(event, ui)
                 {
                     $("#confirmjointdialog").dialog("destroy");    
                     $("#confirmjointdialog").remove();                     
                 } ,
                 open       : function(event, ui)
                 {
		            var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		            var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		            var saveButton = buttons[0];
		            $(saveButton).css({"color":"#090"});
		            $(this).css({height:'auto'})                    
                 }
        });
    } ,
    
    _saveActionEvaluation           : function(actionId, kpiid)
    {
        var self = this;
         
        $("#message_"+actionId).addClass("ui-state-info").css({padding:"5px"}).html("saving ... <img src='public/images/loaderA32.gif' />")
       $.post("main.php?controller=action&action=save_evaluation",{
            rating          : $("#actionrating").val(),
            comment         : $("#actioncomment").val(),
            recommendation  : $("#actionrecommendation").val(),
            componentid     : self.options.componentid,
            reference       : actionId,
            user            : self.options.user,
            evaluationyear  : self.options.evaluationyear,
            next_evaluation : self.options.actionSettings['next_evaluation'][actionId] ,
            kpi_id          : self.options.kpiid,
            kpiid           : kpiid
       }, function(response){
          if(response.error)
          {
            $("#message_"+actionId).html(response.text);
          } else {                                 
            document.location.href = "main.php?controller=component&action=evaluate&parent=2&folder=manage&user="+self.options.user+"&component="+self.options.componentid;
            $("#actionevaluation_"+actionId).dialog("destroy");
            $("#actionevaluation_"+actionId).remove();  
            jsDisplayResult("ok", "ok", response.text);                                             
          }
       },"json");  
    } ,
    
    _updateActionEvaluation           : function(actionId, kpiid, id)
    {
        var self = this;
         
        $("#message_"+actionId).addClass("ui-state-info").css({padding:"5px"}).html("updating ... <img src='public/images/loaderA32.gif' />")
       $.post("main.php?controller=action&action=save_evaluation",{
            rating          : $("#actionrating").val(),
            comment         : $("#actioncomment").val(),
            recommendation  : $("#actionrecommendation").val(),
            componentid     : self.options.componentid,
            reference       : actionId,
            user            : self.options.user,
            evaluationyear  : self.options.evaluationyear,
            next_evaluation : self.options.userNext ,
            kpi_id          : self.options.kpiid,
            kpiid           : kpiid,
            id              : id
       }, function(response){
          if(response.error)
          {
            $("#message_"+actionId).html(response.text);
          } else {                                 
            document.location.href = "main.php?controller=component&action=evaluate&parent=2&folder=manage&user="+self.options.user+"&component="+self.options.componentid;
            $("#actionevaluation_"+actionId).dialog("destroy");
            $("#actionevaluation_"+actionId).remove();  
            jsDisplayResult("ok", "ok", response.text);                                             
          }
       },"json");  
    } ,
    
    _isEvaluationAtAction          : function()
    {
       var self = this;
       if(self.options.actionSettings['evaluate_on'] === "action")
       {
           return true;
       } else {
          return false;
       }                   
       return false;
    },
    
    _checkEvaluated           : function(actionId, evaluationType)
    {
      var self = this;
      if(self.options.actionSettings.hasOwnProperty(actionId))
      {
         if(self.options.actionSettings[actionId].hasOwnProperty(evaluationType))
         {
            return true;
         }
      } 
      return false;
    } ,
    
    _isNextActionEvaluation        : function(index, evaluationType)
    {
       var self = this;
       if(!$.isEmptyObject(self.options.actionNextEvaluation))
       {
          if(self.options.actionNextEvaluation[index] === evaluationType)
          {
             return true;
          } else {
             return false;
          }
       }
       return false;
    } ,
    
    _evals        : function(actionId, type, evaluationType)
    {
      var self        = this; 
      var evaluations = self.options.actionSettings['evaluations']
      if(evaluations[actionId].hasOwnProperty(type))
      {
        if(evaluations[actionId][type][evaluationType] == 0 || evaluations[actionId][type][evaluationType] == "")
        {
          return "";
        } else {
          return (evaluations[actionId][type][evaluationType] == null ? "" : evaluations[actionId][type][evaluationType]);
        }
      } else {
         return "";
      }                       
    } ,
    
    _canEvaluate                        : function(actionId)
    {
      var self = this;
      var nextEvaluation = self.options.actionSettings['next_evaluation']
      if(self._isEvaluationAtAction())
      {
          if(nextEvaluation[actionId] == "")
          {
            return false;
          } else {
               if(nextEvaluation[actionId] == "manager")
               {
                  if(self.options.userNext == "manager")
                  {
                     //the manager evaluation part is only done by the manager of the user logged, 
                     //so the user selected(userdata)'s manager should be the userlogged(userlogged) or when the manager 
                     //is the manager of theirselves S
                     if((self.options.userdata['managerid'] == self.options.userlogged['tkid']) 
                     || (self.options.userlogged['managerid'] == "S"))
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                    return false;
                  }
               }

               if(nextEvaluation[actionId] == "self")
               {
                  if(self.options.userNext == "self")
                  {
                    //self evaluation is done by the userlogged in and is the user selected    
                     if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                     return false;
                  }
               }  
               
               if(nextEvaluation[actionId] == "joint")
               {
                  if(self.options.userNext == "joint")
                  {
                      //joint evaluation is done either by the userlogged or can be done by the manage of the user selected
                     if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
                     {
                        self.options.jointByManager = true;
                        return true;
                     } else if((self.options.userdata['managerid'] == self.options.userlogged['tkid'])){
                        self.options.jointByManager = true;
                        return true;
                     } else {
                        return false;
                     }
                  } else {
                    return false;
                  }
                  /*if((self.options.userdata['tkid'] == self.options.userlogged['tkid']) 
                     || (self.options.userdata['managerid'] == self.options.userlogged['tkid'])
                   
                  )
                  {
                     return true;
                  } else {
                     return false;
                  }*/
               }   
               if(nextEvaluation[actionId] == "review")
               {
                  if(self.options.userNext == "review")
                  {
                     //evaluation review is done by any of the users who are in evaluation committee
                     if(self.options.inCommittee == true)
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                    return false;
                  }
               }                
            return true;
          }
      } else {
          return false;
      }
    } ,
    
    _canEditEvaluate                 : function(actionId)
    {
       var self = this;
       if(self._isEvaluationAtAction() && self.options.page == "evaluate")
       {

          if(self.options.userNext == "self")
          {
            if(!self._canEvaluate(actionId))
            {
               if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
               {
                 return true;
               } else {
                 return false;
               }              
            }
          } 
          if(self.options.userNext == "manager")
          {
            if(!self._canEvaluate(actionId))
            //if(self._isSelfEvaluated(kpaId))
            {
               //the manager evaluation part is only done by the manager of the user logged, 
               //so the user selected(userdata)'s manager should be the userlogged(userlogged) or when the manager 
               //is the manager of theirselves S
               if((self.options.userdata['managerid'] == self.options.userlogged['tkid']) 
               || (self.options.userlogged['managerid'] == "S"))
               {
                  return true;
               } else {
                  return false;
               }               
            }
          }
          
          if(self.options.userNext == "joint")
          {
            if(!self._canEvaluate(actionId))
            {
                //joint evaluation is done either by the userlogged or can be done by the manage of the user selected
                if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
                {
                  return true;
                } else if((self.options.userdata['managerid'] == self.options.userlogged['tkid'])){
                  return true;
                } else {
                  return false;
                }               
            }            
          }
          
          if(self.options.userNext == "review")
          {
            if(!self._canEvaluate(actionId))
            {
               //evaluation review is done by any of the users who are in evaluation committee
               if(self.options.inCommittee == true)
               {
                 return true;
               } else {
                 return false;
               }              
            }                              
          }                
       }
       return false;         
        
    }
    
});
