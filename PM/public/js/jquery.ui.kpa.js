    $.widget("ui.kpa",{
    
    options     : {
        start               : 0,
        limit               : 10,
        current             : 1,
        total               : 0,
        componentid         : 0,
        evaluationyear      : 0, 
        section             : "",
        page                : "",
        user                : "",
        url                 : "main.php?controller=kpa&action=getAll",
        btnDisplayOptions   : {},
        evaluationSetting   : {}, 
        evaluations         : {},
        statusOfEvaluations : {},
        openKpi             : false,
        openAction          : false,
        kpaNextEvaluation   : {},
        userdata            : {},
        userlist            : {},
        userlogged          : {},
        jointByManager      : false,
        inCommittee         : false,
        userNext            : ""               
    } , 
    
    _init      : function()
    { 
        this._get();
    } , 
    
    _create     : function()
    {

    }, 
    
    _get        : function()
    {
        var self = this;
        self.options.user = $("#user").val();
		$.getJSON(self.options.url, {
			evaluationyear 	: self.options.evaluationyear,
			componentid		: self.options.componentid, 
			section         : self.options.section,
			user            : self.options.user, 
			page            : self.options.page
		}, function(kpaData) {
	       $(self.element).html("");	
	       $(".kpatable").remove();
           self.options.userdata     = kpaData.userdata
           self.options.userlogged   = kpaData.userlogged
           self.options.inCommittee  = kpaData.in_committee;
           if(kpaData.hasOwnProperty('kpa_change'))
           {
              if(!$.isEmptyObject(kpaData.kpa_change))
              {
                self._notifyChange(kpaData); 
              }
           } 
           if(kpaData.hasOwnProperty('kpi_change'))
           {
              if($.isEmptyObject(kpaData.kpa_change))
              {
                self._notifyChange(kpaData); 
              }
           }
           if(kpaData.hasOwnProperty('settings'))
           {
              if(kpaData['settings'].hasOwnProperty('evaluations'))
              {
                self.options.evaluations = kpaData['settings'].evaluations;    
              }
              if(kpaData['settings'].hasOwnProperty('next_evaluation'))
              {
                self.options.kpaNextEvaluation = kpaData['settings']['next_evaluation'];
              }                   
              self.options.evaluationSetting = kpaData.settings;     
           }           

	       var evaluation_complete = false;
	       if(kpaData.hasOwnProperty('evaluation_complete'))
	       {
             if(kpaData.evaluation_complete)
             {
               evaluation_complete = true;
             } 		        
	       }		  
            var html = [];
            html.push("<table class='kpatable noborder' id='kpatable_"+self.options.componentid+"'>"); 
	        if(self._isEvaluationAtKpa() && self.options.page == "evaluate")
	        {
	           var kpa_str = ""; 
	           if(kpaData.hasOwnProperty('kpa_evaluations'))
	           {
	              if(evaluation_complete)
	              {
	                 kpa_str = "Evaluation Complete";
	              } else {
                     if(kpaData.kpa_evaluations.total === undefined  || kpaData.kpa_evaluations.total == null)
                     {
                        kpa_str = "There are no KPA available for evaluation in this evaluation period";
                     } else  {
                           //var kpa_eval = (kpaData.kpa_evaluations.total_kpa - kpaData.kpa_evaluations.total);
                       var kpa_eval = kpaData.kpa_evaluations.done;
                       kpa_str = "<b>Status : </b>"+kpaData.kpa_evaluations.next+" - "+kpa_eval+"/"+kpaData.kpa_evaluations.total_kpa+" KPA evaluated. [ "+kpaData.kpa_evaluations.progress+"]";
                     }    
                  }
	           } else {
                  kpa_str = "There are no KPA available for evaluation in this evaluation period";              
	           }	            
	             html.push("<tr>") ;
	               html.push("<td colspan='"+(kpaData.cols+3)+"' class='noborder'>");
	                html.push("<span id='action_status' class='ui-state-info' style='padding:5px;'>"+kpa_str+"</span>")
	               html.push("</td>");
	             html.push("</tr>");
	        }
            if(self._isEvaluationAtKpi() && self.options.page == "evaluate")
            {
                var kpi_str = "";
                if(evaluation_complete)
                {
                  kpi_str = "Evaluation Complete";
                } else {
                  if(kpaData.kpi_evaluations.total === undefined  || kpaData.kpi_evaluations.total == null)
                  {
                   kpi_str = "There are no KPI available for evaluation in this evaluation period";
                  } else {
                   var kpi_eval = kpaData.kpi_evaluations.done;
                   kpi_str = "<b>Status : </b>"+kpaData.kpi_evaluations.next+" - "+kpi_eval+"/"+kpaData.kpi_evaluations.total_kpi+" KPI evaluated. [ "+kpaData.kpi_evaluations.progress+"]";
                  }	          
               }
	             html.push("<tr>") ;
	               html.push("<td colspan='"+(kpaData.cols+3)+"' class='noborder'>");
	                html.push("<span id='action_status' class='ui-state-info' style='padding:5px;'>"+kpi_str+"</span>")
	               html.push("</td>");
	             html.push("</tr>");
            }
            if(self._isEvaluationAtAction() && self.options.page == "evaluate")
            {    
                var action_str = "";
                if(evaluation_complete)
                {
                   action_str = "Evaluation Complete";
                } else {	             
                 if(kpaData.action_evaluations.total === undefined || kpaData.action_evaluations.total == null)
                 {
                   action_str = "There are no actions available for evaluation in this evaluation period";
                 } else {
                   var action_eval = kpaData.action_evaluations.done;
                   action_str = "<b>Status : </b>"+kpaData.action_evaluations.next+" - "+action_eval+"/"+kpaData.action_evaluations.total_actions+" Actions evaluated. [ "+kpaData.action_evaluations.progress+"]";
                 }                    
                }
	             html.push("<tr>") ;
	               html.push("<td colspan='"+(kpaData.cols+3)+"' class='noborder'>");
	                html.push("<span id='action_status' class='ui-state-info' style='padding:5px;'>"+action_str+"</span>")
	               html.push("</td>");
	             html.push("</tr>");
            }	 
            var canConfirm = false;
            if(kpaData.hasOwnProperty('hasActions'))
            {
              if(kpaData.hasActions)
              {
                  if((self.options.userdata['managerid'] == self.options.userlogged['tkid']) 
                 || (self.options.userlogged['managerid'] == "S"))
                 {
                   canConfirm = true;
                 }
              }
            }
            //manager can only do the confirmations
            if((self.options.userdata['managerid'] == self.options.userlogged['tkid']) || 
               (self.options.userlogged['managerid'] == "S")) {              
                $("#activatecomponent").removeAttr("disabled");
            } else {
                $("#activatecomponent").attr("disabled", "disabled");
            }            
            if(self.options.page == "new" || self.options.page == "admin")
            {
               html.push("<tr>") ;
                 html.push("<td colspan='"+(kpaData.cols+4)+"' class='noborder'>");
                  html.push("<input type='button' id='add_kpa_"+self.options.componentid+"' name='add_kpa_"+self.options.componentid+"' value='Add KPA' class='isubmit' />");
                 html.push("</td>");
               html.push("</tr>"); 
               
	         $("#add_kpa_"+self.options.componentid).live("click", function(e){
                    if(self.options.section == "admin")
                    {
                     document.location.href = "main.php?controller=kpa&action=admin_addkpa&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&section="+self.options.section+"&folder="+self.options.section+"&user="+self.options.user;
                    } else {
                     document.location.href = "main.php?controller=kpa&action=addkpa&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&section="+self.options.section+"&folder="+self.options.section+"&user="+self.options.user;
                    }
                    e.preventDefault();
	           });
            }
            if(canConfirm && self.options.page == "confirm")
            {
               html.push("<tr>") ;
                 html.push("<td colspan='"+(kpaData.cols+4)+"' class='noborder'>");
                  html.push("<input type='button' id='confirm_kpa_"+self.options.componentid+"' name='confirm_kpa_"+self.options.componentid+"' value='Confirm' class='isubmit' />");
                 html.push("</td>");
               html.push("</tr>"); 
               
               $("#confirm_kpa_"+self.options.componentid).live("click", function(e){
                  self._confirmComponent();
                  e.preventDefault();
               });
            }
		    if($.isEmptyObject(kpaData.data))
		    {		   
               html.push("<tr>") ;
                 html.push("<td colspan='"+(kpaData.cols+4)+"' class='noborder'>");
                  html.push("<p class='ui-state ui-state-info' style='padding:5px;'>");
                    html.push("<span class='ui-icon ui-state-info' style='float:left;'></span>");
                    html.push("<span style='margin:3px;'>There are no KPAs associated with this component</span>");
                  html.push("</p>");
                 html.push("</td>");
               html.push("</tr>"); 
		       html.push("</table>");
		       $(self.element).html(html.join(' '));               
			} else {
		       html.push("</table>");
		       $(self.element).html(html.join(' '));     	       
     	       self._displayHeaders(kpaData.headers);
			   self._display(kpaData.data, kpaData.cols, kpaData.kpi_data, kpaData.settings);
			}
	         $("#activate_kpa_"+self.options.componentid).live("click", function(e){
                self._activateComponent();
	           e.preventDefault();
	         });
		                 	
		});    
    } , 
    
    _displayHeaders     : function(headers)
    {
        var html = [];       
        var self = this;
        html.push("<tr>");
         html.push("<td style='width:1em; color:blue;'><b><small>K<br />P<br />A<br />s</small></b></td>");
         html.push("<td style='width:1em; color:blue;'><small><b>K<br />P<br />I</b><br />s</small></td>");
         html.push("<td style='width:1em; color:blue;'><small><b>A<br />C<br />T<br />I<br />O<br />N</b><br />s</small></td>");
         $.each(headers, function(index, val){
             html.push("<th>"+val+"</th>");
         });         
         var displayOptions = self._getDisplayStatus();
         if(displayOptions['displayTd'])
         {
           html.push("<th>&nbsp;&nbsp;&nbsp;</th>");
         }
         if(displayOptions['selfEvaluationRating'])
         {
           html.push("<th>Employee Rating</th>");
         }
         if(displayOptions['selfEvaluationComment'])
         {
           html.push("<th>Employee Comment</th>");
         }
         if(displayOptions['managerEvaluationRating'])
         {
           html.push("<th>Manager Rating</th>");
         }
         if(displayOptions['managerEvaluationComment'])
         {
           html.push("<th>Manager Comment</th>");
         }
         if(displayOptions['jointEvaluationRating'])
         {
           html.push("<th>Joint Evaluation</th>");         
         }
         if(displayOptions['jointEvaluationComment'])
         {
           html.push("<th>Joint Comment</th>");         
         }
         if(displayOptions['evaluationReviewRating'])
         {
           html.push("<th>Evaluation Review Rating</th>");         
         }
         if(displayOptions['evaluationReviewComment'])
         {
           html.push("<th>Evaluation Review Comment</th>");         
         }
        $("#kpatable_"+self.options.componentid).append(html.join(' '));
    },
    
    _display    : function(kpas, cols, kpa_data, settings)
    {
       var self           = this;
       var displayOptions = self._getDisplayStatus();     
       var html           = [];
       self._openEvaluationLevel();
       $.each(kpas, function(index, kpa){
          html.push("<tr id='tr_"+index+"' class='kpa'>")       
             html.push("<td>");
              html.push("<a href='#' id='viewkpi_"+index+"'>");
               html.push("<span id='viewkpiicon_"+index+"' class='ui-icon ui-icon-"+(self.options.openKpi ? 'minus' : 'plus')+"'>");
               html.push("</span>");
              html.push("</a>");
             html.push("</td>");
             html.push("<td>&nbsp</td>");
             html.push("<td>&nbsp</td>");
            $.each(kpa, function(key, val){
               html.push("<td>"+val+"</td>");
            });

          var displayEvaluationButton     = self._canEvaluateKPA(index);
          var displayEvaluationEditButton = self._canEditEvaluation(index);
          if(displayOptions['displayTd'])
          {
                 html.push("<td>");
                 if(displayOptions['displayAddBtn'])
                 {
                    html.push("<input type='button' name='"+self.options.componentid+"add_kpi_"+index+"' id='"+self.options.componentid+"add_kpi_"+index+"' value='Add KPI' />");    
                    
                   
		            $("#"+self.options.componentid+"add_kpi_"+index).live("click", function(e){
                           document.location.href = "main.php?controller=kpi&action=addkpi&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section+"&user="+self.options.user;	
			            e.preventDefault();
		            });                                
                 }
                 if(displayOptions['displayEditBtn'])
                 {
                    html.push("<input type='button' name='"+self.options.componentid+"edit_kpa_"+index+"' id='"+self.options.componentid+"edit_kpa_"+index+"' value='Edit KPA' />"); 
                    
		            $("#"+self.options.componentid+"edit_kpa_"+index).live("click", function(e){
		                 
			            if( self.options.section == "setup")
			            {
			                 document.location.href = "main.php?controller=kpa&action=setup_editkpa&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section;
			            } else {
                              document.location.href = "main.php?controller=kpa&action=editkpa&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section;			
			            }		     
			
			            e.preventDefault();
		            });               
                       html.push("<br />");
                       html.push("<input type='button' name='"+self.options.componentid+"_delete_kpa_"+index+"' id='"+self.options.componentid+"_delete_kpa_"+index+"' value='Delete KPA' />");                
                        
                        $("#"+self.options.componentid+"_delete_kpa_"+index).live("click", function(e){ 
                           if(confirm("Are you sure you want to delete this KPA"))
                           {
                              jsDisplayResult("info", "info", "deleting kpa . . . <img src='public/images/loaderA32.gif' /> " ); 
                              $.post("main.php?controller=kpa&action=updateStatus",{
                                   kpastatus	: 2,
                                   id		: index
                              }, function(response){
                                   if(response.error)
                                   {
                                       jsDisplayResult("error", "error", response.text );
                                   } else {			
                                    if(response.updated)
                                    {
                                       $("#tr_"+index).remove();		
                                       jsDisplayResult("info", "info", response.text);
                                    } else {
                                       $("#tr_"+index).remove();		
                                       jsDisplayResult("ok", "ok", response.text);
                                       self._get();		               
                                    }
                                  }		
                              },"json");                    
                           }           
                           e.preventDefault();
                        });                    
                 }
                 if(displayOptions['displayUpdateBtn'])
                 {
                    html.push("<input type='button' name='"+self.options.componentid+"_update_kpa_"+index+"' id='"+self.options.componentid+"_update_kpa_"+index+"' value='Update KPA' />"); 
                    
		
		            $("#"+self.options.componentid+"_update_kpa_"+index).live("click", function(e){   
                          document.location.href = "main.php?controller=kpa&action=updatekpa&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section;
		               e.preventDefault();
		            });	                    
                                 
                 }
                  if(displayOptions['displayAssuranceBtn'])
                  {
                     html.push("<input type='button' name='"+self.options.componentid+"_assurance_kpa_"+index+"' id='"+self.options.componentid+"_assurance_kpa_"+index+"' value='Assurance KPA' />");
                    
		            $("#"+self.options.componentid+"_assurance_kpa_"+index).live("click", function(e){   
                          document.location.href = "main.php?controller=kpa&action=assurancekpa&kpaid="+index+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section;
		               e.preventDefault();
		            });	                
                    
                  }  
                 if(displayEvaluationButton)
                 {
                    html.push("<input type='button' name='"+self.options.componentid+"_evaluate_kpa_"+index+"' id='"+self.options.componentid+"_evaluate_kpa_"+index+"' value='Evaluate KPA' />");   
                    
		            $("#"+self.options.componentid+"_evaluate_kpa_"+index).live("click", function(e){   
		                self._evaluateKpa(self.options.componentid, index); 
		                e.preventDefault();
		            });		                                 
                 }
                 if(displayEvaluationEditButton)
                 {
                    html.push("<input type='button' name='"+self.options.componentid+"_evaluate_kpa_"+index+"' id='"+self.options.componentid+"_evaluate_kpa_"+index+"' value='Edit KPA Evaluation' />");   
                    
		            $("#"+self.options.componentid+"_evaluate_kpa_"+index).live("click", function(e){   
		                self._editKpaEvaluate(self.options.componentid, index); 
		                e.preventDefault();
		            });		                                                     
                 }
                 
                 if(displayOptions['displayEvaluateBtn'])
                 {
                    html.push("<br />\r\n\n\n\n");        
                    html.push("<span style='padding:5px;'>");
                      html.push("<span style='float:left;' class='"+settings['evaluationstatuses'][index]['self']['status']+"' ></span>");
                      html.push("<span style='color:"+(self._isNextEvaluation(index, 'self') ? '' : '#AAAAAA')+"'>Self Evaluation</span>");
                    html.push("</span>");
                    html.push("<br />\r\n\n\n\n");                
                    html.push("<span style='padding:5px;'>");
                      html.push("<span style='float:left;' class='"+settings['evaluationstatuses'][index]['manager']['status']+"' ></span>");
                      html.push("<span style='color:"+(self._isNextEvaluation(index, 'manager') ? '' : '#AAAAAA')+"'>Manager Evaluation</span>");
                    html.push("</span>");
                    html.push("<br />\r\n\n\n\n");                
                    html.push("<span style='padding:5px;'>");
                      html.push("<span style='float:left;' class='"+settings['evaluationstatuses'][index]['joint']['status']+"' ></span>"); 
                      html.push("<span style='color:"+(self._isNextEvaluation(index, 'joint') ? '' : '#AAAAAA')+"'>Joint Evaluation</span>");
                    html.push("</span>");
                    html.push("<br />\r\n\n\n\n");                
                    html.push("<span style='padding:5px;'>");
                      html.push("<span style='float:left;' class='"+settings['evaluationstatuses'][index]['review']['status']+"' ></span>");
                      html.push("<span style='color:"+(self._isNextEvaluation(index, 'review') ? '' : '#AAAAAA')+"'>Evaluation Review</span>");              
                    html.push("</span>");
                 }       
                 html.push("</td>");
              }

              var dispalyEvaluationOptions = {};
              if(self.options.page == "evaluate")
              {
                 if(self._isEvaluationAtKpa())
                 {
                      if(displayOptions['selfEvaluationRating'])
                      {
                        html.push("<td>");
                           html.push(self._kpaEvaluation(index, "self", "rating"));                
                        html.push("</td>");
                      }      
                      
                      if(displayOptions['selfEvaluationComment'])
                      {
                        html.push("<td>");
                           html.push(self._kpaEvaluation(index, "self", "comment"));                
                        html.push("</td>");
                      }       
                      
                      if(displayOptions['managerEvaluationRating'])
                      {
                        html.push("<td>");
                           html.push(self._kpaEvaluation(index, "manager", "rating"));                
                        html.push("</td>");
                      }       
                      
                      if(displayOptions['managerEvaluationComment'])
                      {
                        html.push("<td>");
                           html.push(self._kpaEvaluation(index, "manager", "comment"));                
                        html.push("</td>");
                      }  
                      
                      if(displayOptions['jointEvaluationRating'])
                      {
                        html.push("<td>");
                           html.push(self._kpaEvaluation(index, "joint", "rating"));                
                        html.push("</td>");
                      }       
                      
                      if(displayOptions['jointEvaluationComment'])
                      {
                        html.push("<td>");
                           html.push(self._kpaEvaluation(index, "joint", "comment"));                
                        html.push("</td>");
                      } 
                      
                      if(displayOptions['evaluationReviewRating'])
                      {
                        html.push("<td>");
                           html.push(self._kpaEvaluation(index, "review", "rating"));                
                        html.push("</td>");
                      }       
                      
                      if(displayOptions['evaluationReviewComment'])
                      {
                        html.push("<td>");
                           html.push(self._kpaEvaluation(index, "review", "comment"));                
                        html.push("</td>");
                      }                                                                                                        
                } else {
                  html.push("<td colspan='8'>");
                    html.push("<span style='color:#AAAAAA;'><b>"+displayOptions['evaluationText']+"</b></span>");
                  html.push("</td>");                
                }
              }
           html.push("</tr>");
         
           if($.isEmptyObject(kpa_data[index]['ids']))
           {
               $("#viewkpi_"+index).attr("title", "0 kpi"); 
               $("#kpatable_"+self.options.componentid).append($("<tr />",{id:index+"_childtr_#"}));
               html.push("<tr id='"+index+"_childtr_#'  style='display:none;'></tr>");
           } else {
                $.each(kpa_data[index]['ids'], function(kpiId, kpistatus) { 
                  if(self.options.openKpi)
                  {
                    html.push("<tr id='"+index+"_childtr_"+kpiId+"' class='childfor_"+index+" kpi'></tr>");
                  } else {
                    html.push("<tr id='"+index+"_childtr_"+kpiId+"' class='childfor_"+index+" action' style='display:none;'></tr>");
                  }
                  if(!$.isEmptyObject(kpa_data[index]['actionInfo'][kpiId]))
                  {
                     if(!$.isEmptyObject(kpa_data[index]['actionInfo'][kpiId]['ids']))
                     {
                         $.each(kpa_data[index]['actionInfo'][kpiId]['ids'], function(actionId, actionStatus){
                           if(self.options.openAction)
                           {
                              html.push("<tr id='"+index+"_child_"+kpiId+"_action_"+actionId+"' class='child_action_for_"+kpiId+" action'></tr>");
                           } else {
                             html.push("<tr id='"+index+"_child_"+kpiId+"_action_"+actionId+"' class='child_action_for_"+kpiId+" action' style='display:none;'></tr>");
                           }      
                        });                        
                     }
                  } else {
                      $("#viewaction_"+kpiId).attr("title", "0 actions");
                  }
                });        
           }
            
            $("#viewkpi_"+index).live("click", function(){
                if($(".childfor_"+index).is(":hidden"))
                {
                   $("#viewkpiicon_"+index).removeClass("ui-icon-plus").addClass("ui-icon-minus");
                } else {
                   $("#viewkpiicon_"+index).removeClass("ui-icon-minus").addClass("ui-icon-plus");
                }
                $(".childfor_"+index).toggle();
                return false;
            });            
            
       });
       $("#kpatable_"+self.options.componentid).append( html.join('') );
       var actionUrl = "";
       var kpiUrl    = "";
       if(self.options.page == 'evaluate')
       {
          actionUrl = "main.php?controller=action&action=getActionEvaluationData";
          kpiUrl    = "main.php?controller=kpi&action=getKpiEvaluationData";
       }
       if(!$.isEmptyObject(kpa_data))
       {
          $.each(kpa_data, function(kpaId, kpaKPI){
             if(!$.isEmptyObject(kpaKPI['ids']))
             {
                $.each(kpaKPI['ids'], function(kpiId, kpistatus) { 
                    $("#"+kpaId+"_childtr_"+kpiId).action({kpaid:kpaId, kpiid:kpiId, componentid:self.options.componentid, evaluationyear:self.options.evaluationyear, section:self.options.section, page:self.options.page, displayOptions:displayOptions, evaluationSettings:self.options.evaluationSetting, user:self.options.user, evaluations:self.options.evaluations, openAction:self.options.openAction, kpistatus:kpistatus, userNext:self.options.userNext, url:actionUrl});
                    });        
                $("#tr_"+kpaId).kpi({kpaid:kpaId, componentid:self.options.componentid, evaluationyear:self.options.evaluationyear, section:self.options.section, page:self.options.page, displayOptions:displayOptions, url:kpiUrl, evaluationSettings:self.options.evaluationSetting, user:self.options.user, evaluations:self.options.evaluations, openAction:self.options.openAction, userNext:self.options.userNext});
             }                 
          });            
       }
       
    },
    
    _notifyChange           : function(kpaData)
    {
       var self = this;
       if($("#kpa_change").length > 0)
       {
          $("#kpa_change").remove();
       }
       $("<div />",{id:"kpa_change"}).append($("<p />").addClass("ui-state-info")
         .append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))
         .append($("<span />",{html:"There has been an update to the template data since the initial setup for <b>"+kpaData['userdata']['tkname']+" "+kpaData['userdata']['tksurname']+"</b> for the component <b>"+kpaData['componentname']+"</b> for the evaluation year <b>"+kpaData['userdata']['start']+" --  "+kpaData['userdata']['end']+"</b>."}).css({"padding":"3px", "margin-left":"0.3em"}))
       ).dialog({
          autoOpen  : true,
          modal     : true,
          position  : "top",
          width     : 300,
          title     : "Template data change",
          buttons   : {
                         "Update Changes"    : function()
                         {
                            $.post("main.php?controller=kpa&action=update_template_change",{
                              kpa_change     : kpaData.kpa_change,
                              kpi_change     : kpaData.kpi_change,
                              componentid    : self.options.componentid,
                              userdata       : kpaData['userdata'],
                              evaluationyear : self.options.evaluationyear
                            },function(response) {
                              $("#kpa_change").remove();
                              document.location.href = "main.php?controller=component&action=newcomponent&parent_id=1&folder=new";
                            },"json");
                         },
                         "Cancel "           : function()
                         {
                            $("#kpa_change").remove();
                         }
          } ,
          close     : function(event, ui)
          {
             $("#kpa_change").remove();
          } ,
          open      : function(event, ui)
          {
             $("#kpa_change").append($("<ul />",{id:"kpa_added_list"}))
             if(kpaData['kpa_change'].hasOwnProperty('added'))
             {
               $.each(kpaData['kpa_change']['added'], function(index, kpa){
                  $("#kpa_added_list").append($("<li />",{html:"Kpa <b>"+kpa.name+"</b> was added "}))
               })
             } 
		   var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		   var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
	        $(buttons[0]).css({"color":"#090"});
          }
       })
    
    } ,
    
    _confirmComponent       : function()
    {
        var self = this;
        if($("#confirmdialog_"+self.options.componentid).length > 0)
        {
          $("#confirmdialog_"+self.options.componentid).remove();
        }
	     var responseUser; 
	     $.ajaxSetup({async:false});
	     $.getJSON("main.php?controller=user&action=getUser",{user:self.options.user}, function(response){  
	          responseUser = response;
	     });
        
        $("<div />",{id:"confirmdialog_"+self.options.componentid, html:"I, "+responseUser.manager+", manager of "+responseUser.user+" hereby:"})
        .append($("<ul />")
          .append($("<li />",{html:"Confirm that I have reviewed all the components of the Performance Matrix of "+responseUser.user+" for the evaluation year: "+responseUser.evaluationyear+";"}))
          .append($("<li />",{html:"Declare that "+responseUser.user+" has reviewed and has agreed to this Performance Matrix; and"}))
          .append($("<li />",{html:"Declare that I have the required authority to confirm this Performance Matrix."}))
        )
        .append($("<div />",{id:"message"}).css({"padding":"5px"}))
        .dialog({
                autoOpen    : true, 
                modal       : true,
                position    : "top",
                title       : "Confirmation of Performance Matrix",
                buttons     : {
                                "Confirm"   : function()
                                {
                                    $("#message").addClass("ui-state-info").html("confirming component ... <img src='public/images/loaderA32.gif' />");
                                    $.post("main.php?controller=usercomponent&action=confrimComponent", {
                                        componentid : self.options.componentid,
                                        user        : $("#user").val()
                                    }, function(response) {
			                        if(response.error )
			                        {
				                     $("#message").addClass("ui-state-erorr").html(response.text);
				                     //jsDisplayResult("error", "error",  );
			                        } else {
			                          /*$("#confirm_kpa_"+self.options.componentid).attr("disabled", "disabled");
                                         $("#confirmdialog_"+self.options.componentid).dialog("destroy");
                                         $("#confirmdialog_"+self.options.componentid).remove(); 
				                     */
				                     jsDisplayResult("ok", "ok", response.text);
		document.location.href = "main.php?controller=component&action=confirm&parent=1&folder=new&user="+$("#user").val();
			                        }	
                                    },"json");
                                }, 
                                "Cancel"    : function()
                                {
                                    $("#confirmdialog_"+self.options.componentid).dialog("destroy");
                                    $("#confirmdialog_"+self.options.componentid).remove();                                 
                                }
                } ,
                "close"     : function(event, ui)
                {
                    $("#confirmdialog_"+self.options.componentid).dialog("destroy");
                    $("#confirmdialog_"+self.options.componentid).remove(); 
                },
                "open"      : function(event, ui)
                {
 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
 				var saveButton = buttons[0];
 				$(saveButton).css({"color":"#090"}); 
                }                 
        });
    } ,
    
    _activateComponent      : function()
    {
        var self = this;
        if($("#activatedialog_"+self.options.componentid).length>0)
        {
          $("#activatedialog_"+self.options.componentid).remove();
        }
        $("<div />",{id:"activatedialog_"+self.options.componentid, html:"Activate Component"})
        .dialog({
                autoOpen    : true, 
                modal       : true,
                position    : "top",
                title       : "Activate Component",
                buttons     : {
                                "Activate"   : function()
                                {
                                   /*
                                    $.post("main.php?controller=usercomponent&action=activateComponent", {
                                        componentid : self.options.componentid,
                                        user        : self.options.user
                                    }, function(response) {
				                   if( response.error )
				                   {
				                      jsDisplayResult("error", "error", response.text );
				                    } else {
                                          $("#activatedialog_"+self.options.componentid).dialog("destroy");
                                          $("#activatedialog_"+self.options.componentid).remove(); 
				                      jsDisplayResult("ok", "ok", response.text );
				                    }	
                                    },"json");
                                    */
                                }, 
                                "Cancel"    : function()
                                {
                                    $("#activatedialog_"+self.options.componentid).dialog("destroy");
                                    $("#activatedialog_"+self.options.componentid).remove();                                 
                                }
                } ,
                "close"     : function(event, ui)
                {
                    $("#activatedialog_"+self.options.componentid).dialog("destroy");
                    $("#activatedialog_"+self.options.componentid).remove(); 
                },
                "open"      : function(event, ui)
                {
                
                }                 
        });    
    } , 
    
    
    _evaluateKpa              : function(componentid, kpaid)
    {
       var self = this;
       if($("#assessment_"+componentid+"_"+kpaid).length)
       {
          $("#assessment_"+componentid+"_"+kpaid).remove();       
       }
       
       $("<div />",{id:"assessment_"+componentid+"_"+kpaid})
         .append($("<table />",{width:"100%"})
           .append($("<tr />")
             .append($("<th />",{html:"Rating :"}))
             .append($("<td />")
               .append($("<select />",{name:"kparating", id:"kparating"})
                 .append($("<option />",{text:"--please select--", value:""}))
               )
             )
           )
           .append($("<tr />")
             .append($("<th />",{html:"Comment :"}))
             .append($("<td />")
               .append($("<textarea />",{name:"kpacomment", id:"kpacomment", cols:"50", rows:"7"}))
             )
           )  
           .append($("<tr />")
             .append($("<th />",{html:"Recommendation :"}))
             .append($("<td />")
               .append($("<textarea />",{name:"kparecommendation", id:"kparecommendation", cols:"50", rows:"7"}))
             )
           )                      
           .append($("<tr />")
             .append($("<td />",{colspan:"2"})
               .append($("<span />",{id:"message"}))
             )
           )
         ).dialog({
             autoOpen   : true,
             title      : "KPA Evaluation",
             modal      : true,
             position   : "top",
             width      : 500,
             buttons    : {
                            "Save"      : function()
                            {                            
                               if($("#kparating").val() == "")
                               {
                                   $("#message").addClass("ui-state-error").css({padding:"5px"}).html("Please select the rating")
                                   return false;
                               } else if($("#kpacomment").val() == ""){
                                   $("#message").addClass("ui-state-error").css({padding:"5px"}).html("Please enter the comment");
                                   return false;
                               } else {
                                  if(self.options.jointByManager)
                                  {
                                     self._showConfirmJointEvaluationDialog(kpaid, componentid);
                                  } else {
                                     self._saveKpaEvaluation(kpaid, componentid);
                                  }                                 
                               }
                            } ,
                            
                            "Cancel"    : function()
                            {
                               $("#assessment_"+componentid+"_"+kpaid).remove();
                               $("#assessment_"+componentid+"_"+kpaid).dialog("remove");     
                            }
             } ,
             close       : function(event, ui)
             {
                $("#assessment_"+componentid+"_"+kpaid).remove();
                $("#assessment_"+componentid+"_"+kpaid).dialog("remove");
             } ,
             open        : function(event, ui)
             {
			     var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			     var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
			     var saveButton = buttons[0];
			     $(saveButton).css({"color":"#090"});               
                    //load
                    var ratingScales = $('body').data('ratingScales');
                    if($.isEmptyObject(ratingScales))
                    {
                         $.getJSON("main.php?controller=ratingscale&action=getAll",{status:1} ,function(ratingScales){
                            if(!$.isEmptyObject(ratingScales))
                            {
                               $('body').data('ratingScales', ratingScales);
                               $("#kparating").empty();
                               $("#kparating").append($("<option />",{text:"--please select--", value:""}));
                               $.each(ratingScales, function(index, ratingScale){
                                  $("#kparating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));
                               });
                            }
                         });
                    } else {
                          $("#kparating").empty();
                          $("#kparating").append($("<option />",{text:"--please select--", value:""}));
                          $.each(ratingScales, function(index, ratingScale){
                             $("#kparating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));
                          });                    
                    }
             }
         })    
    } ,
    
    _editKpaEvaluate                    : function(componentid, kpaId)
    {
       var self = this;
       if($("#edit_evaluation_"+componentid).length)
       {
          $("#edit_evaluation_"+componentid).remove();       
       }
       var ratingHtml  = [];
       var html        = [];
       var evaluations = self.options.evaluations[kpaId][self.options.userNext];
       //var editable    = self._getEditableStatus(kpaId);
       $("<div />",{id:"edit_evaluation_"+componentid})
         .append($("<table />",{width:"100%"})
           .append($("<tr />")
             .append($("<th />",{html:"Rating :"}))
             .append($("<td />")
               .append($("<select />",{name:"kparating", id:"kparating"})
                 .append($("<option />",{text:"--please select--", value:""}))
               )
             )
           )
           .append($("<tr />")
             .append($("<th />",{html:"Comment :"}))
             .append($("<td />")
               .append($("<textarea />",{name:"kpacomment", id:"kpacomment", cols:"50", rows:"7", text:evaluations['comment']}))
             )
           )  
           .append($("<tr />")
             .append($("<th />",{html:"Recommendation :"}))
             .append($("<td />")
               .append($("<textarea />",{name:"kparecommendation", id:"kparecommendation", cols:"50", rows:"7", text:evaluations['recommendation']}))
             )
           )                      
           .append($("<tr />")
             .append($("<td />",{colspan:"2"})
               .append($("<span />",{id:"message"}))
             )
           )
         ).dialog({
             autoOpen   : true,
             title      : "Edit KPA Evaluation",
             modal      : true,
             position   : "top",
             width      : 500,
             buttons    : {
                            "Save Changes"      : function()
                            {                            
                               if($("#kparating").val() == "")
                               {
                                   $("#message").addClass("ui-state-error").css({padding:"5px"}).html("Please select the rating")
                                   return false;
                               } else if($("#kpacomment").val() == ""){
                                   $("#message").addClass("ui-state-error").css({padding:"5px"}).html("Please enter the comment");
                                   return false;
                               } else {
                                  if(self.options.jointByManager)
                                  {
                                     self._showConfirmJointEvaluationDialog(kpaId, componentid);
                                  } else {
                                     self._updateKpaEvaluation(kpaId, componentid, evaluations['id']);
                                  }                                 
                               }
                            } ,
                            
                            "Cancel"    : function()
                            {
                               $("#edit_evaluation_"+componentid).remove();
                               $("#edit_evaluation_"+componentid).dialog("remove");     
                            }
             } ,
             close       : function(event, ui)
             {
                $("#edit_evaluation_"+componentid).remove();
                $("#edit_evaluation_"+componentid).dialog("remove");
             } ,
             open        : function(event, ui)
             {
			     var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			     var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
			     var saveButton = buttons[0];
			     $(saveButton).css({"color":"#090"});               
                 //load 
                var ratingScales = $('body').data('ratingScales');
                if($.isEmptyObject(ratingScales))
                {
                     $.getJSON("main.php?controller=ratingscale&action=getAll",{status:1} ,function(ratingScales){
                        if(!$.isEmptyObject(ratingScales))
                        {
                           $('body').data('ratingScales', ratingScales);
                           $("#kparating").empty();
                           $("#kparating").append($("<option />",{text:"--please select--", value:""}));
                           $.each(ratingScales, function(index, ratingScale){
                             if(evaluations['ratingid'] == ratingScale.id)
                             {
                                $("#kparating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id, selected:"selected"}));
                             } else{
                                $("#kparating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));                     
                             }                                
                           });
                        }
                     });
                } else {
                      $("#kparating").empty();
                      $("#kparating").append($("<option />",{text:"--please select--", value:""}));
                      $.each(ratingScales, function(index, ratingScale){
                         if(evaluations['ratingid'] == ratingScale.id)
                         {
                            $("#kparating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id, selected:"selected"}));
                         } else{
                            $("#kparating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));                     
                         }
                      });                    
                }
             }
         })    
    } ,
    
    _showConfirmJointEvaluationDialog   : function(kpaid, componentid)
    {
        var self = this;
        if($("#confirmjointdialog").length > 0)
        {      
          $("#confirmjointdialog").remove();
        }
        var html = [];
        if(self.options.userlogged['tkid'] == self.options.userdata['tkid'])
        {
           html.push("<div>")
            html.push("I, "+self.options.userdata['user']+", reporting to "+self.options.userdata['managername']+" hereby:");
            html.push("<ul>");
             html.push("<li>");
               html.push("Confirm that I met with "+self.options.userdata['managername']+" to jointly evaluate my performance on this component of the Performance Matrix for the evaluation period "+self.options.userlogged['evaluationperiod']+" for the evaluation year "+self.options.userdata['start']+" to "+self.options.userdata['end']+";");
             html.push("</li>");
             html.push("<li>");
               html.push("Declare that we agreed on this joint rating;");
             html.push("</li>");             
             html.push("<li>");
               html.push("Declare that I have the required authority to confirm this joint evaluation rating.");
             html.push("</li>");            
            html.push("</ul>");
           html.push("</div>");
        } else {
           html.push("<div>")
            html.push("I, "+self.options.userdata['managername']+", manager of "+self.options.userdata['user']+" hereby:");
            html.push("<ul>");
             html.push("<li>");
               html.push("Confirm that I met with "+self.options.userdata['user']+" to jointly evaluate the performance on this component of the Performance Matrix for the evaluation period "+self.options.userlogged['evaluationperiod']+" for the evaluation year "+self.options.userdata['start']+" "+self.options.userdata['end']+";");
             html.push("</li>");
             html.push("<li>");
               html.push("Declare that we agreed on this joint rating;");
             html.push("</li>");             
             html.push("<li>");
               html.push("Declare that I have the required authority to confirm this joint evaluation rating.");
             html.push("</li>");            
            html.push("</ul>");
           html.push("</div>");
        }
        $("<div />", {id:"confirmjointdialog"}).append( html.join(' ') )
        .append($("<div />",{id:"message"}))
        .dialog({
                 autoOpen  : true,
                 modal     : true,
                 position  : "top",
                 width     : 350,
                 height    : 200,
                 title     : "Joint Evaluation of Performance Matrix",
                 buttons   : {
                               "Ok"     : function()
                               {
                                    self._saveKpaEvaluation(kpaid, componentid)
                                    $("#confirmjointdialog").dialog("destroy");
                                    $("#confirmjointdialog").remove();   
                               } ,
                               "Cancel"       : function()
                               {
                                  $("#confirmjointdialog").dialog("destroy");
                                  $("#confirmjointdialog").remove();
                               }
                 } , 
                 close      : function(event, ui)
                 {
                     $("#confirmjointdialog").dialog("destroy");    
                     $("#confirmjointdialog").remove();                     
                 } ,
                 open       : function(event, ui)
                 {
		            var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		            var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		            var saveButton = buttons[0];
		            $(saveButton).css({"color":"#090"}); 
		            $(this).css({height:'auto'});                   
                 }
        });
    },
    
    _updateKpaEvaluation          : function(kpaid, componentid, id)
    {
       var self = this;
        $("#message").addClass("ui-state-info")
                     .css({padding:"5px"}).html("updating ...<img src='public/images/loaderA32.gif' /> " );
        $.post("main.php?controller=kpa&action=update_kpa_evaluation",{
           rating          : $("#kparating").val(),
           comment         : $("#kpacomment").val(),
           recommendation  : $("#kparecommendation").val(),
           type            : "kpa",
           reference       : kpaid,
           componentid     : componentid,
           user            : self.options.user,
           id              : id,
           next_evaluation : self.options.userNext
       }, function(response) {
            if(response.error)
            {
               $("#message").addClass("ui-state-error").css({padding:'5px'}).html(response.text)
            } else {
               jsDisplayResult("ok", "ok", response.text);
               $("#edit_evaluation_"+componentid).remove();
               $("#edit_evaluation_"+componentid).dialog("remove");  
               self._get();   
            document.location.href = "main.php?controller=component&action=evaluate&parent=2&folder=manage&user="+self.options.user+"&component="+componentid;                                                                                      
            }
        },"json")            
    } ,    
    
    _saveKpaEvaluation          : function(kpaid, componentid)
    {
       var self = this;
        $("#message").addClass("ui-state-info")
                     .css({padding:"5px"}).html("saving ...<img src='public/images/loaderA32.gif' /> " );
        $.post("main.php?controller=kpa&action=save_kpa_evaluation",{
           rating          : $("#kparating").val(),
           comment         : $("#kpacomment").val(),
           recommendation  : $("#kparecommendation").val(),
           type            : "kpa",
           reference       : kpaid,
           componentid     : componentid,
           user            : self.options.user,
           next_evaluation : self.options.kpaNextEvaluation[kpaid]
       }, function(response) {
            if(response.error)
            {
               $("#message").addClass("ui-state-error").css({padding:'5px'}).html(response.text)
            } else {
               jsDisplayResult("ok", "ok", response.text);
               $("#assessment_"+componentid+"_"+kpaid).remove();
               $("#assessment_"+componentid+"_"+kpaid).dialog("remove");  
               self._get();   
            document.location.href = "main.php?controller=component&action=evaluate&parent=2&folder=manage&user="+self.options.user+"&component="+componentid;                                                                                      
            }
        },"json")            
    } ,
    
    _getDisplayStatus         : function() 
    {
        var self  = this;
        var  btnDisplayOptions = {}
        btnDisplayOptions['displayTd']        = true;
        btnDisplayOptions['displayAddBtn']    = true;
        btnDisplayOptions['displayEditBtn']   = false;
        btnDisplayOptions['displayUpdateBtn']    = false;
        btnDisplayOptions['displayEvaluateBtn']  = false;
        btnDisplayOptions['displayKPIEvaluateBtn'] = false;
        btnDisplayOptions['displayActionEvaluateBtn'] = false;
        btnDisplayOptions['displayApproveBtn']   = false;
        btnDisplayOptions['displayAssuranceBtn'] = false;
        btnDisplayOptions['displayDeleteBtn']    = true;
        btnDisplayOptions['displayConfigureBtn']    = true;
        btnDisplayOptions['evaluationText']    =  "";
        btnDisplayOptions['kpiEvaluationText']    =  "";
        btnDisplayOptions['actionEvaluationText']    =  "";
        btnDisplayOptions['selfEvaluationRating']    = false
        btnDisplayOptions['selfEvaluationComment']   = false;
        btnDisplayOptions['managerEvaluationRating'] = false;
        btnDisplayOptions['managerEvaluationComment']= false;
        btnDisplayOptions['jointEvaluationRating']   = false;
        btnDisplayOptions['jointEvaluationComment']  = false; 
        btnDisplayOptions['evaluationReviewRating']   = false;
        btnDisplayOptions['evaluationReviewComment']  = false;         
        
        if(self.options.page == "activate")
        {
           btnDisplayOptions['displayTd'] = false;
        } else if(self.options.page == "view"){
           btnDisplayOptions['displayTd'] = false;
        }        
        if(self.options.section == 'manage')
        {
          btnDisplayOptions['displayAddBtn'] = false;
        }
        
        if(self.options.section == 'new')
        {
          btnDisplayOptions['displayEditBtn'] = true;
        }
        if(self.options.section == 'admin')
        {
          btnDisplayOptions['displayEditBtn'] = true;
        }
        if(self.options.page == 'edit')
        {
          btnDisplayOptions['displayEditBtn'] = true;
        }
        if(self.options.page == 'update')
        {
          btnDisplayOptions['displayUpdateBtn'] = true;
          btnDisplayOptions['displayDeleteBtn']    = false;
        }
        if(self.options.page == 'evaluate')
        {
          btnDisplayOptions['selfEvaluationRating']    = true;
          btnDisplayOptions['selfEvaluationComment']   = true;
          btnDisplayOptions['managerEvaluationRating'] = true;
          btnDisplayOptions['managerEvaluationComment']= true;
          btnDisplayOptions['jointEvaluationRating']   = true;
          btnDisplayOptions['jointEvaluationComment']  = true;    
          btnDisplayOptions['evaluationReviewRating']   = true;
          btnDisplayOptions['evaluationReviewComment']  = true;              
                
          //check if evealuation can be done at KPA level
          if(!$.isEmptyObject(self.options.evaluationSetting))
          {
             if(self.options.evaluationSetting['evaluate_on'] == "kpa")
             {
                btnDisplayOptions['displayEvaluateBtn'] = true;     
             } else {
               btnDisplayOptions['displayEvaluateBtn'] = false;
               btnDisplayOptions['evaluationText'] = "No evaluation at KPA Level";
             }
             
             if(self.options.evaluationSetting['evaluate_on'] == "kpi")
             {
                btnDisplayOptions['displayKPIEvaluateBtn'] = true;     
             } else {
               btnDisplayOptions['displayKPIEvaluateBtn'] = false;
               btnDisplayOptions['kpiEvaluationText'] = "No evaluation at KPI Level";
             }
             
             
             if(self.options.evaluationSetting['evaluate_on'] == "action")
             {
                btnDisplayOptions['displayActionEvaluateBtn'] = true;     
             } else {
               btnDisplayOptions['displayActionEvaluateBtn'] = false;
               btnDisplayOptions['actionEvaluationText'] = "No evaluation at action Level";
             }
             
             
             
          } else {
             btnDisplayOptions['displayEvaluateBtn'] = false;
             btnDisplayOptions['evaluationText'] = "No evaluation at KPA Level";
          }
          btnDisplayOptions['displayDeleteBtn']    = false;
        }        
        if(self.options.page == 'approve')
        {
          btnDisplayOptions['displayApproveBtn'] = true;
          btnDisplayOptions['displayDeleteBtn']    = false;
        }                
        if(self.options.page == 'assurance')
        {
          btnDisplayOptions['displayAssuranceBtn'] = true;
          btnDisplayOptions['displayDeleteBtn']    = false;
        }                
	   if( self.options.section != "admin")
	   {
	     btnDisplayOptions['displayConfigureBtn'] = false;			
	   }                
        return btnDisplayOptions;        
    } ,
    
    _kpaEvaluation         : function(kpaId, evaluationType, type)
    {
          var self                         = this;
          var evaluationSettings           = self.options.evaluationSetting;
          var evaluations                  = self.options.evaluations;
          var textOptions                  = {}
          textOptions['selfevaluation']    = false
          textOptions['managerevaluation'] = false;
          textOptions['jointevaluation']   = false;
          textOptions['evaluationreview']  = false;
          if(evaluationSettings['evaluate_on'] == "kpa")
          {
             if(evaluationSettings['evaluations'].hasOwnProperty(kpaId))
             {                    
               if(evaluationSettings['evaluations'][kpaId].hasOwnProperty(evaluationType))
               {
                  if(evaluationSettings['evaluations'][kpaId][evaluationType].hasOwnProperty(type))
                  {                 
                    return evaluationSettings['evaluations'][kpaId][evaluationType][type];
                  }
               }
             }
          }
    } ,

    _isNextEvaluation           : function(index, evaluationType)
    {
      var self = this;
       if(!$.isEmptyObject(self.options.kpaNextEvaluation))
       {
          if(self.options.kpaNextEvaluation[index] === evaluationType)
          {
            return true;
          } else {
            return false;
          }
       }
       return false;
    } , 
     
   _openEvaluationLevel         : function()
   {
     var self = this;
     if(self.options.page === "evaluate")
     {
        if(self.options.evaluationSetting['evaluate_on'] == "kpi")
        {
          self.options.openKpi = true;
        } else if(self.options.evaluationSetting['evaluate_on'] === "action") {
          self.options.openKpi = true;
          self.options.openAction = true;
        }
     }
   }, 
    
    _isEvaluationAtKpa          : function()
    {
       var self = this;
       if(self.options.evaluationSetting['evaluate_on'] == "kpa")
       {
          return true;
       }     
       return false;
    },
    
    _isEvaluationAtKpi          : function()
    {
       var self = this;
       if(self.options.evaluationSetting['evaluate_on'] == "kpi")
       {
          return true;
       }     
       return false;
    },
    
    _isEvaluationAtAction          : function()
    {
       var self = this;
       if(self.options.evaluationSetting['evaluate_on'] == "action")
       {
          return true;
       }     
       return false;
    },        
    
    _canEditEvaluation                      : function(kpaId)
    {
       var self = this;
       if(self._isEvaluationAtKpa() && self.options.page == "evaluate")
       {

          if(self.options.userNext == "self")
          {
            if(!self._canEvaluateKPA(kpaId))
            {                    
               //self evaluation is done by the userlogged in and is the user selected    
               if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
               {
                 return true;
               } else {
                 return false;
               }
            }
          } 
          if(self.options.userNext == "manager")
          {
            if(!self._canEvaluateKPA(kpaId))
            {
               //the manager evaluation part is only done by the manager of the user logged, 
               //so the user selected(userdata)'s manager should be the userlogged(userlogged) or when the manager 
               //is the manager of themselves "S"
               if((self.options.userdata['managerid'] == self.options.userlogged['tkid']) 
               || (self.options.userlogged['managerid'] == "S"))
               {
                   return true;
               } else {
                   return false;
               }           
            }
          }
          
          if(self.options.userNext == "joint")
          {
            if(!self._canEvaluateKPA(kpaId))
            {
               if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
               {
                  return true;
               } else if((self.options.userdata['managerid'] == self.options.userlogged['tkid'])) {
                  return true;
               } else {
                  return false;
               }               
            }            
          }
          
          if(self.options.userNext == "review")
          {
            if(!self._canEvaluateKPA(kpaId))
            {
               if(self.options.inCommittee == true)
               {
                  return true;
               } else {
                  return false;    
               }                
            }                              
          }                
       }
       return false; 
    } , 
    _canEvaluateKPA                        : function(kpaId)
    {
      var self = this;
      if(self._isEvaluationAtKpa() && self.options.page == "evaluate")
      {
         if(!$.isEmptyObject(self.options.evaluationSetting))
         {
            if(self.options.evaluationSetting['next_evaluation'][kpaId] === "")
            {
              return false;
            } else {
               if(self.options.evaluationSetting['next_evaluation'][kpaId] == "manager")
               {
                  if(self.options.userNext == "manager")
                  {
                     //the manager evaluation part is only done by the manager of the user logged, 
                     //so the user selected(userdata)'s manager should be the userlogged(userlogged) or when the manager 
                     //is the manager of themselves "S"
                     if((self.options.userdata['managerid'] == self.options.userlogged['tkid']) 
                     || (self.options.userlogged['managerid'] == "S"))
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                    return false;
                  }
               }
               
               if(self.options.evaluationSetting['next_evaluation'][kpaId] == "self")
               {
                  if(self.options.userNext == "self")
                  {
                    //self evaluation is done by the userlogged in and is the user selected    
                     if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                     return false;
                  }
               }  
               
               if(self.options.evaluationSetting['next_evaluation'][kpaId] == "joint")
               {
                  if(self.options.userNext == "joint")
                  {
                      //joint evaluation is done either by the userlogged or can be done by the manage of the user selected
                     if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
                     {
                        self.options.jointByManager = true;
                        return true;
                     } else if((self.options.userdata['managerid'] == self.options.userlogged['tkid'])) {
                        self.options.jointByManager = true;
                        return true;
                     } else {
                        return false;
                     }
                  } else {
                    return false;
                  }
               }              
               if(self.options.evaluationSetting['next_evaluation'][kpaId] == "review")
               {
                  if(self.options.userNext == "review")
                  {
                     //evaluation review is done by any of the users who are in evaluation committee
                     if(self.options.inCommittee == true)
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                    return false;
                  }
               }                   
            }
            return true;
         }   
      } else {
         return false;
      }
    }    
        
});
