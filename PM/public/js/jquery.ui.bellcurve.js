$.widget("ui.bellcurve", {

    options         : {
     evaluationyear      : 0,
     evaluationperiod    : 0,
     directorate         : 0,
     subdirectorate      : 0,    
     joblevel            : 0,
     orderby             : 0,
     order               : 0,
     evaluationlevel     : "",
     autoLoad            : false
    } ,
    
    _init           : function()
    {
       if(this.options.autoLoad)
       {
          this._get();
       }
    } ,
    
    _create         : function()
    {
       var self = this;
       $(self.element).append($("<table />",{width:"100%"}).addClass("noborder")
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />")
               .append($("<tr />")
                 .append($("<th />",{html:"Evaluation year:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationyear", id:"evaluationyear"}))
                 )
                 .append($("<th />",{html:"Evaluation period:"}))
                 .append($("<td />")
                   .append($("<select />",{name:"evaluationperiod", id:"evaluationperiod"}))             
                 )            
               )
               .append($("<tr />")
                 .append($("<th />",{html:"Evaluation Level:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"evaluationlevel", id:"evaluationlevel"})
                      .append($("<option />",{value:"", text:"All"}))
                      .append($("<option />",{value:1, text:"Self Evaluation"}))
                      .append($("<option />",{value:2, text:"Manager Evaluation"}))
                      .append($("<option />",{value:3, text:"Joint Evaluation"}))
                      .append($("<option />",{value:4, text:"Review Evaluation"}))
                   )
                 )
               )
               .append($("<tr />")
                 .append($("<th />",{html:"Department:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"department", id:"department"}))
                 )
               )
               .append($("<tr />")
                 .append($("<th />",{html:"Job Level:"}))
                 .append($("<td />",{colspan:"3"})
                   .append($("<select />",{name:"joblevel", id:"joblevel"}))
                 )
               )                                
             )             
           )           
         )
         .append($("<tr />")
           .append($("<td />").addClass("noborder")
             .append($("<table />",{width:"100%", id:"bellcurve_table"}))
           )
         )
       )
       if(self.options.evaluationlevel !== "")
       {
          $("#evaluationlevel").val(self.options.evaluationlevel);
       }
       
       self._loadEvaluationYears();
       self._loadDirectorates();
       self._loadJobLevels();
       if(self.options.evaluationyear != "")
       {
          self._loadEvaluationPeriods(self.options.evaluationyear);               
       }
       
       $("#evaluationlevel").on('change', function(){
         self.options.evaluationlevel = $(this).val();
         self._get();
       })
       $("#department").on('change', function(){
         self.options.directorate = $(this).val();
         self._get();
       })
       
       $("#evaluationyear").on('change', function(){
          self.options.evaluationyear = $(this).val();
          self._loadEvaluationPeriods($(this).val());       
       });
       
       $("#evaluationperiod").on('change', function(e){
         self.options.evaluationperiod = $(this).val();
         self._get();
         e.preventDefault();
       });
       
       $("#joblevel").on('change', function(e){
         self.options.joblevel = $(this).val();
         self._get();
         e.preventDefault();
       })

    } ,

	_loadEvaluationYears		: function()
	{
	    var self = this;
	    $("#evaluationyear").append($("<option />",{value:"", text:"--select evaluation year--"}))
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
		   $.each( responseData, function( index, evalYear) {
		    if(self.options.evaluationyear == evalYear.id)
		    {
		        $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end, selected:"selected"}))
		    } else {
		        $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))
		    }
		   });								
		},"json");
	} ,
	
	_loadEvaluationPeriods      : function(evaluationyear)
	{
	    var self = this;
	    $("#evaluationperiod").empty();
	    $("#evaluationperiod").append($("<option />",{value:"", text:"--select evaluation period--"}))
	    $.post("main.php?controller=evaluationperiod&action=get_all", {evaluationyear : evaluationyear},
		 function( responseData ){
		  var period_int = 1;
		  $.each(responseData['periods'][1], function(index, period) {
		    text_str = period.start_date+" -- "+period.end_date+" ("+period.name+" "+period_int+" )";
		    period_int++;
		    if(self.options.evaluationperiod == period.id)
		    {
		        $("#evaluationperiod").append($("<option />",{value:period.id, text:text_str, selected:"selected"}))
		    } else {
		        $("#evaluationperiod").append($("<option />",{value:period.id, text:text_str}))
		    }
		  });								
	    },"json");
	} ,
	
	_loadJobLevels            : function()
	{
	    var self = this;
	    $("#joblevel").empty();
	    $("#joblevel").append($("<option />",{value:"", text:"--select job level--"}))
         $.getJSON("main.php?controller=joblevel&action=get_all", function(joblevels){
	        if(!$.isEmptyObject(joblevels))
	        {
	           $.each(joblevels, function(index, joblevel){
		        if(self.options.joblevel == joblevel.id)
		        {
		           $("#joblevel").append($("<option />",{value:joblevel.id, text:joblevel.name, selected:"selected"}))
		        } else {
		           $("#joblevel").append($("<option />",{value:joblevel.id, text:joblevel.name}))
		        }	           	              
	          });
	        }
         });
	} ,
	
	_loadDirectorates       : function()
	{
	    var self = this;
	    $("#directorate").empty();
	    $("#directorate").append($("<option />",{value:"", text:"All"}))
	    $.post("main.php?controller=directorate&action=get_all_departments",function(directorates){	
	        if(!$.isEmptyObject(directorates))
	        {
	           $.each(directorates, function(index, directorate){
		          if(self.options.directorate == directorate.id)
		          {
		             $("#directorate").append($("<option />",{value:directorate.id, text:directorate.name,  selected:"selected"}))
		          } else {
		             $("#directorate").append($("<option />",{value:directorate.id, text:directorate.name}))
		          }	     	              
	          });
	        }
	    },"json");
	} ,
	
	_loadSubDirectorates     : function(directorate)
	{
	    $("#subdirectorate").empty();
	     $("#subdirectorate").append($("<option />",{value:"", text:"All"}))
		$.post("main.php?controller=directorate&action=get_all_subdirectorate",
		  {directorate:directorate}, function(subdirectorates){	
	        if(!$.isEmptyObject(subdirectorates))
	        {
	           $.each(subdirectorates, function(index, subdirectorate){
	              $("#subdirectorate").append($("<option />",{value:subdirectorate.id, text:subdirectorate.name}))
	          });
	        }
		},"json");
	},
	
    _get            : function()
    {
        var self = this;
        document.location.href = "main.php?controller=report&action=bell_curve&evaluationyear="+self.options.evaluationyear+"&evaluationperiod="+self.options.evaluationperiod+"&directorate="+self.options.directorate+"&joblevel="+self.options.joblevel+"&evaluationlevel="+self.options.evaluationlevel
    },
    
    
    _display          : function(reportData)
    {
      var self = this;
      var html = [];
       html.push("<thead>");
       html.push("<tr>");
        html.push("<td colspan='16'>"+reportData['info']+"</td>");
       html.push("</tr>");                    
       html.push("</thead>");      
       var report = reportData['usermatrix'];
       var len    = report.length;
      for(var i = 0; i < len; i++)
      {
          html.push("<tr>");
           html.push("<th colspan='2'>Emaployee</th>");
           html.push("<td colspan='2'>"+report[i]['user']['user']+"</td>");
           html.push("<th colspan='2'>Manager</th>");
           html.push("<td colspan='2'>"+report[i]['user']['manager']+"</td>");
           html.push("<th colspan='2'>Evaluation Year</th>");
           html.push("<td colspan='2'>"+report[i]['user']['year']+"</td>");
           html.push("<th colspan='2'>Evaluation On</th>");
           html.push("<td colspan='2'></td>");        
          html.push("</tr>");
          html.push("<tr>");
           html.push("<th colspan='2'>Job Level</th>");
           html.push("<td colspan='2'>"+report[i]['user']['joblevel']+"</td>");
           html.push("<th colspan='2'>Perfomance Category</th>");
           html.push("<td colspan='2'>"+report[i]['user']['category']+"</td>");
           html.push("<th colspan='2'>Evaluation Period</th>");
           html.push("<td colspan='2'>"+report[i]['user']['period']+"</td>");
           html.push("<th colspan='2'>Status</th>");
           html.push("<td colspan='2'></td>");        
          html.push("</tr>"); 
          //console.log( typeof report[i] );
          if(report[i].hasOwnProperty('component'))
          {
              $.each(report[i]['component'], function(componentid, component){
                 html.push("<tr>");
                  html.push("<td colspan='16'><b>"+component['componentname']+"</b> <em>"+component['weight']+"</em>%</td>");
                 html.push("</tr>");                 
                 if(component.hasOwnProperty('data'))
                 {
                    html.push("<tr>");
                      html.push("<td colspan='4'></td>");
                      html.push("<td colspan='3'><b>Self Evaluation</b></td>");
                      html.push("<td colspan='3'><b>Manager Evaluation</b></td>");
                      html.push("<td colspan='3'><b>Joint Evaluation</b></td>");
                      html.push("<td colspan='3'><b>Evaluation Review</b></td>");
                    html.push("</tr>");                      
                    html.push("<tr style='background-color:#EEF;'>");
                      html.push("<td>"+component['type']+"</td>");
                      html.push("<td>Deadline Date</td>");  
                      html.push("<td>Status</td>");
                      html.push("<td>Weight</td>");
                      html.push("<td>Score</td>");
                      html.push("<td>Weight</td>");
                      html.push("<td>Comments</td>");
                      html.push("<td>Score</td>");
                      html.push("<td>Weight</td>");
                      html.push("<td>Comments</td>");
                      html.push("<td>Score</td>");
                      html.push("<td>Weight</td>");
                      html.push("<td>Comments</td>");                      
                      html.push("<td>Score</td>");
                      html.push("<td>Weight</td>");
                      html.push("<td>Comments</td>");                      
                    html.push("</tr>");               
                    if(!$.isEmptyObject(component['data']))
                    {
                      $.each(component['data'], function(datakey, data){
                         html.push("<tr>");
                           html.push("<td>"+data['name']+"</td>");
                           html.push("<td>"+data['deadline']+"</td>");  
                           html.push("<td>"+data['status']+"</td>");
                           html.push("<td>"+data['weight']+"</td>");
                           html.push("<td>"+(data['self_rating'] === undefined ? "" : data['self_rating'])+"</td>");
                           html.push("<td>"+(data['self_score'] === undefined ? "" : data['self_score'])+"</td>");
                           html.push("<td>"+(data['self_comment'] === undefined ? "" : data['self_comment'])+"</td>");
                           html.push("<td>"+(data['manager_rating'] === undefined ? "" : data['manager_rating'])+"</td>");
                           html.push("<td>"+(data['manager_score'] === undefined ? "" :data['manager_score'])+"</td>");
                           html.push("<td>"+(data['manager_comment'] === undefined ? "" :data['manager_comment'])+"</td>");
                           html.push("<td>"+(data['joint_rating'] === undefined ? "" : data['joint_rating'])+"</td>");
                           html.push("<td>"+(data['joint_score'] === undefined ? "" : data['joint_score'])+"</td>");
                           html.push("<td>"+(data['joint_comment'] === undefined ? "" : data['joint_comment'])+"</td>");
                           html.push("<td>"+(data['review_rating'] === undefined ? "" : data['review_rating'])+"</td>");
                           html.push("<td>"+(data['review_score'] === undefined ? "" : data['review_score'])+"</td>");
                           html.push("<td>"+(data['review_comment'] === undefined ? "" : data['review_comment'])+"</td>");
                         html.push("</tr>"); 
                      });                                                             
                    }
                 }
                 html.push("<tr>");
                  html.push("<td colspan='16'>&nbsp;&nbsp;&nbsp;</td>");      
                 html.push("</tr>"); 
              });
          } else {
            html.push("<tr>");
              html.push("<td colspan='16'>There are no components setup for this user for the selected criteria</td>");      
            html.push("</tr>");                
          }
          html.push("<tr>");
            html.push("<td colspan='16'>&nbsp;&nbsp;&nbsp;</td>");      
          html.push("</tr>"); 
      }
      $("#evaluation_matrix_table").append("<tbody>"+html.join('')+"</tbody>");
    }


});
