$.widget("ui.matrix", {

   options     : {
        url              : "main.php?controller=component&action=getAll",
        evaluationYear   : 0,
        toEvaluationYear : 0,
        autoLoad         : true,
        tableRef 	     : "matrix_"+Math.floor(Math.random(57)*98),
        evaluationYears  : {}
   } ,
   
   _init         : function()
   {
     if(this.options.autoLoad)
     {
        this._get();
     }
   } ,
   
   _create       : function()
   {
     var self = this;
     
     $(self.element).append($("<table />",{width:"100%"})
       .append($("<tr />")
         .append($("<th />",{html:"Select evaluation year"}))
         .append($("<td />")
           .append($("<select />",{id:"evaluation_year", name:"evaluation_year"}).addClass("evaluation_year"))
         )
         .append($("<th />",{html:"Select to financial year"}))
         .append($("<td />")
           .append($("<select />",{id:"to_evaluation_year", name:"to_evaluation_year"}).addClass("evaluation_year"))
         )         
       )
       .append($("<tr />")
         .append($("<td />",{colspan:"4"})
           .append($("<table />",{id:self.options.tableRef, width:"100%"})
               .append($("<tr />")
                 .append($("<td />",{html:"Please select the to and from evaluation years to copy"}))
               )
           )
         )
       )
     )
     self._loadEvaluationYears();
     
     $("#evaluation_year").live('change', function(e){   
        self.options.evaluationYear = $(this).val();
        self._reloadEvaluationYears($(this).val());
        self._get();
        e.preventDefault();
     });  
     
     $("#to_evaluation_year").live('change', function(e){   
        self.options.toEvaluationYear = $(this).val();
        self._get();
        e.preventDefault();
     });       
     
   } ,
   
   _loadEvaluationYears       : function()
   {
     var self = this;
     $(".evaluatuin_year").empty();
     $(".evaluation_year").append($("<option />",{value:"", text:"--please select--"}))
     $.getJSON("main.php?controller=evaluationyears&action=getAll", function(evaluationYears){
        if(!$.isEmptyObject(evaluationYears))
        {
          self.options.evaluationYears = evaluationYears;
          $.each(evaluationYears, function(index, evaluationyear) {
             $(".evaluation_year").append($("<option />",{value:evaluationyear.id, text:evaluationyear.start+" to "+evaluationyear.end}))
          });
        }
     });
   
   } ,
   
   _reloadEvaluationYears          : function(evaluationyearid)
   {
     var self = this;
     $("#to_evaluation_year").empty();
     $("#to_evaluation_year").append($("<option />",{value:"", text:"--please select--"}))
     if(self.options.evaluationYears)
     {
          $.each(self.options.evaluationYears, function(index, evaluationyear){
             if(evaluationyear.id !== evaluationyearid)
             {
               if(evaluationyear.id === self.options.toEvaluationYear)
               {
                  $("#to_evaluation_year").append($("<option />",{value:evaluationyear.id, text:evaluationyear.start+" to "+evaluationyear.end, selected:"selected"}))
               } else {
                  $("#to_evaluation_year").append($("<option />",{value:evaluationyear.id, text:evaluationyear.start+" to "+evaluationyear.end}))
               }
             }
          });
     }
   } ,
   
   _get         : function()
   {
       var self = this;
	  $(self.element).append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='public/images/loaderA32.gif' />"})
	   .css({position:"absolute", "z-index":"9999", top:"300px", left:"300px", border:"0px solid #FFFFF", padding:"5px"})
	  )
	  $.getJSON(self.options.url, {
	      evaluationyear    : self.options.evaluationYear,
	      page              : self.option.page,
	      section           : self.options.section,
	      to_evaluationyear : self.options.toEvaluationYear
	  },function(components) {
	     $("#loadingDiv").remove(); 
	     $("#"+self.options.tableRef).html(""); 
          if(!$.isEmptyObject(components))
          {  
             self._displayHeaders();
             self._display(components);
          } else {
           $("#"+self.options.tableRef).append($("<tr />")
             .append($("<td >",{html:"There are no components to be copied for the selected evaluation years"}))
           ); 
          }             

	  });   

   } ,           

   _display      : function(components)
   {
       var self = this;
       $.each(components, function(index, component){
          $("#"+self.options.tableRef).append($("<tr />",{id:"tr_"+component.id})
            .append($("<td />",{html:component.id}))
            .append($("<td />",{html:component.shortcode}))
            .append($("<td />",{html:component.name}))
            .append($("<td />",{html:component.description}))
            .append($("<td />")
              .append($("<input />",{type:"button", name:"copy_"+component.id, id:"copy_"+component.id, value:"Copy"})
                .click(function(e){
                   if(self.options.toEvaluationYear == "")
                   {
                     jsDisplayResult("error", "error", "Please select to evaluation year");
                     $("#to_evaluation_year").focus();
                     return false;
                   } else if(self.options.evaluatinYear == ""){
                    jsDisplayResult("error", "error", "Please select from evaluation year");
                     $("#evaluation_year").focus();
                     return false;
                   } else {
                     self._copyComponent(component.id);
                   }
                   e.preventDefault();
                })
              )
            )
          )
       });
   } ,
   
   _displayHeaders       : function()
   {
     var self = this;
     $("#"+self.options.tableRef).append($("<tr />")
       .append($("<th />",{html:"Ref"}))
       .append($("<th />",{html:"Short Code"}))
       .append($("<th />",{html:"Name"}))
       .append($("<th />",{html:"Description"}))
       .append($("<th />",{html:""}))
     )
   } ,
   
   _copyComponent        : function(componentid)
   {
     var self = this;
     jsDisplayResult("info", "info", "copying ..." );
     $.post("main.php?controller=component&action=copyComponent",{
          to_year      : self.options.toEvaluationYear,
          from_year    : self.options.evaluationYear,
          component_id : componentid
     }, function(response){
        if(response.error)
        {
          jsDisplayResult("error", "error", response.text);
        } else {
          jsDisplayResult("ok", "ok", response.text);
          $("#copy_"+componentid).attr("disabled", "disabled");
          $("#tr_"+componentid).fadeOut();
        }
     },"json");
     
     
   }



});
