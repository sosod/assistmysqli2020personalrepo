$(function(){

	Performancecategories.getEvaluationyears();
	$("#addcategory").hide();
	$("#evaluationyear").change(function(){
		var evaluationyear = $(this).val();
		$("#evaluationyearid").val( evaluationyear );
		Performancecategories.get( evaluationyear );
		return false;		
	});	
	
	$("#addnew").click(function(){
		
		if( $("#addnewdialog").length > 0)
		{
			$("#addnewdialog").remove();			
		}
		
		$("<div />",{id:"addnewdialog"})
		 .append($("<table />")
		   .append($("<tr />")
			  .append($("<th />",{html:"Short Code:"}))
			  .append($("<td />")
				 .append($("<input />",{type:"text", name:"shortcode", id:"shortcode", value:""}))	  
			  )
		   )
		   .append($("<tr />")
			  .append($("<th />",{html:"Performance Management Category:"}))
			  .append($("<td />")
				 .append($("<textarea />",{name:"name", id:"name"}))	  
			  )
		   )		   
		   .append($("<tr />")
			  .append($("<th />",{html:"Description of Performance Management Category:"}))
			  .append($("<td />")
				 .append($("<textarea />",{name:"description", id:"description"}))	  
			  )
		   )		   		   
		   .append($("<tr />")
			  .append($("<td />",{colspan:"2"})
				 .append($("<p />")
				    .append($("<span />"))		 
				    .append($("<span />",{id:"message"}))
				 )	  
			  )
		   )		   		   		   
		 )
		 .dialog({
			 		autoOpen		: true, 
			 		modal			: true, 
			 		title			: "Add New Performance Category",
			 		width			: "500px",
			 		position		: "top",
			 		buttons			: {
			 							"Save"		: function()
			 							{
			 									if( $("#shortcode").val() == "")
			 									{
			 										$("#message").html("Please enter the short code");
			 										return false;
			 									} else if( $("#name").val() == ""){
			 										$("#message").html("Please enter the performance category name")
			 										return false;
			 									} else if( $("#description").val() == ""){
			 										$("#message").html("Please enter the performance description")
			 										return false;
			 									} else {
			 										Performancecategories.save();	 										
			 									}
			 
			 							} , 
			 							"Cancel"	: function()
			 							{
			 								$(this).dialog("destroy");			 								
			 							}
		 			} , 		 			
		 			open			: function(event, ui)
		 			{
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});			 				
		 			}
		 });
		
		return false;
	});

});

var Performancecategories 	= {
		
		getEvaluationyears		: function()
		{
			$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
				$.each( responseData, function( index, evalYear) {
					$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end }))					
				});								
			},"json");
		} , 
				
		get			: function( evaluationyear )
		{
			$("#categories").remove();
			$(".cat").remove();
			$("#addcategory").fadeIn();
			$.getJSON("main.php?controller=performancecategories&action=getAll",{
				evaluationyear : evaluationyear				
			}, function( responseData ){
				if( $.isEmptyObject(responseData) )
				{
					$("#table_categories").append($("<tr />").addClass("cat")
					  .append($("<td />",{colspan:"6", html:"There are no categories setup for the selected evaluation year"}))	
					)
				} else {
					$.each( responseData, function( index, category ){
						Performancecategories.display( category );					
					});			
				}
			});
		} , 
		
		display		 : function( category )
		{
			$("#table_categories").append($("<tr />").addClass("cat")
			  .append($("<td />",{html:category.id}))
			  .append($("<td />",{html:category.shortcode}))
			  .append($("<td />",{html:category.name}))
			  .append($("<td />",{html:category.description}))
			  .append($("<td />",{html:((category.status & 1) == 1 ? "Active" : "Inactive" )}))
			  .append($("<td />")
				 .append($("<input />",{type:"button", name:"edit_"+category.id, id:"edit_"+category.id, value:"Edit"}))	  
			  )
			)
			
			$("#edit_"+category.id).live("click", function(){
				
				if( $("#editdialog").length > 0)
				{
					$("#editdialog").remove();			
				}
				
				$("<div />",{id:"editdialog"})
				 .append($("<table />")
				   .append($("<tr />")
					  .append($("<th />",{html:"Short Code:"}))
					  .append($("<td />")
						 .append($("<input />",{type:"text", name:"shortcode", id:"shortcode", value:category.shortcode}))	  
					  )
				   )
				   .append($("<tr />")
					  .append($("<th />",{html:"Performance Management Category:"}))
					  .append($("<td />")
						 .append($("<textarea />",{name:"name", id:"name", value:category.name}))	  
					  )
				   )		   
				   .append($("<tr />")
					  .append($("<th />",{html:"Description of Performance Management Category:"}))
					  .append($("<td />")
						 .append($("<textarea />",{name:"description", id:"description", text:category.description}))	  
					  )
				   )		   		   
				   .append($("<tr />")
					  .append($("<td />",{colspan:"2"})
						 .append($("<p />")
						    .append($("<span />"))		 
						    .append($("<span />",{id:"message"}))
						 )	  
					  )
				   )		   		   		   
				 )
				 .dialog({
					 		autoOpen		: true, 
					 		modal			: true, 
					 		title			: "Edit Performance Category",
					 		width			: "500px",
					 		position		: "top",
					 		buttons			: {
					 							"Save Changes"		: function()
					 							{
					 									if( $("#shortcode").val() == "")
					 									{
					 										$("#message").html("Please enter the short code");
					 										return false;
					 									} else if( $("#name").val() == ""){
					 										$("#message").html("Please enter the performance category name")
					 										return false;
					 									} else if( $("#description").val() == ""){
					 										$("#message").html("Please enter the performance description")
					 										return false;
					 									} else {
					 										Performancecategories.updateCategory( category.id );	 										
					 									}
					 
					 							} , 
					 							"Cancel"	: function()
					 							{
					 								$(this).dialog("destroy");			 								
					 							} , 
					 							
					 							"Delete"	: function()
					 							{
					 								Performancecategories.deleteCategory( category.id );					 								
					 							} , 
					 							
					 							"Deactivate"	: function()
					 							{
					 								Performancecategories.updateStatus( category.id , 0);
					 							} , 
					 							
					 							"Activate"		: function()
					 							{
					 								Performancecategories.updateStatus( category.id , 1);
					 							}
				 			} , 		 			
				 			open			: function(event, ui)
				 			{
				 				var displayDelete = false;
				 				var displayDeactivate = false;
				 				var displayActivate = false; 
				 				//if the status is for the default , then dont display delete
				 				if( (category.status & 1) == 1)
				 				{
				 					displayDeactivate = true;
				 				} else {
				 					displayActivate = true;
				 				}
				 				
				 				if(category.used == false)
				 				{
				 				   displayDelete 	  = true;
				 				   displayDeactivate  = false;
				 				   displayActivate    = false;
				 				}
				 				
				 				
				 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
				 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
				 				var saveButton = buttons[0];
				 				var deleteButton = buttons[2]
				 				var deactivateButton = buttons[3];
				 				var activateButton   = buttons[4];
				 				$(saveButton).css({"color":"#090"});
				 				$(deleteButton).css({"color":"red", display:(displayDelete ? "inline" : "none")});
				 				$(deactivateButton).css({"color":"red", display:(displayDeactivate ? "inline" : "none")});
				 				$(activateButton).css({"color":"#090", display:(displayActivate ? "inline" : "none")});		 				
				 			}
				 });
				
				return false;
			});
			
		} , 
		
		
		save		: function()
		{
			var evalYear = $("#evaluationyearid").val();
			$.post("main.php?controller=performancecategories&action=savePerformancecategories", {
				data	: {
							name 		: $("#name").val(),
							shortcode	: $("#shortcode").val(),
							description : $("#description").val(),
							evaluationyear : evalYear	
				}
			}, function( response ){
				if( response.error )
				{
					$("#message").html( response.text )
				} else {				
					Performancecategories.get( evalYear );
					jsDisplayResult("ok", "ok", response.text );
					$("#addnewdialog").dialog("destroy");
					$("#addnewdialog").remove();	
				}	
			},"json")
		}, 
		
		updateCategory		: function( id )
		{
			$.post("main.php?controller=performancecategories&action=updatePerformancecategories",{
				data 	: {
					name 	: $("#name").val(),
					shortcode: $("#shortcode").val(),
					description : $("#description").val()
				} , 
				id 		: id
			}, function( response ){
				if( response.error )
				{
					$("#message").html( response.text )
				} else {
					Performancecategories.get( $("#evaluationyearid").val() )
					jsDisplayResult("ok", "ok", response.text );
					$("#editdialog").dialog("destroy");
					$("#editdialog").remove();	
				}	
			},"json");
			
		} , 
		
		updateStatus		: function( id, status)
		{
			$.post("main.php?controller=performancecategories&action=updatePerformancecategories",{
				data 	: { status : status } , 
				id 		: id
			}, function( response ){
				if( response.error )
				{
					$("#message").html( response.text )
				} else {		
					Performancecategories.get( $("#evaluationyearid").val() )
					jsDisplayResult("ok", "ok", response.text );
					$("#editdialog").dialog("destroy");
					$("#editdialog").remove();	
				}	
			},"json");
		} , 
		
		deleteCategory		: function( id )
		{
			if( confirm("Are you sure you want to delete this category"))
			{
				Performancecategories.updateStatus( id , 2);
			}	
			
		}
}; 
