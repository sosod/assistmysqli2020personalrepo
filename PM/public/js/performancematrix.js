$(function(){
	/*
	$("#save_changes").click(function(){
		Performancematrix.save();
		return false;
	});
	
	$(".configurejoblevels").click(function(){
		var catId = $(this).attr("id");
		Performancematrix.displayJobLevels( catId );
		return false;
	})
	
	$(".usecategory").change(function(){
		var catId = $(this).val();		
		alert("Configure the jobs levels for this category . . .")
		$("#joblevels_"+catId).attr("disabled", "");	
		return false;
	});
	*/
	$("#evaluationyear").change(function(e){
		var evalYear = $(this).val(); 
		$("#tabs").html("");
		$.getJSON("main.php?controller=performancecategories&action=getAll",{
			evaluationyear 		: evalYear			
		}, function(performanceCategories) { 	
			if($.isEmptyObject(performanceCategories))
			{
				$("#tabs").html("There are no components setup for the selected evaluation year").addClass("ui-state-info").css({padding:"5px", "margin-top":"0px", position:"absolute"});
			} else {
				var li = $("<ul />",{id:"tablist"})
				$.each(performanceCategories, function(index, category){
					li.append($("<li />")
					  .append($("<a />",{href:"main.php?controller=performancematrix&action=viewcontent&id="+category.id+"&evaluationyear="+evalYear, html:category.name}))	
					)				
				});	
			    /*
			    if(componentData.weight < 100)
			    {
				    li.append($("<li />")
				      .append($("<span />",{html:"Sum of components not equalt to 100%"}).addClass("ui-state-error").css({padding:"5px", "margin-top":"0px", position:"absolute"}))
				    )								      
			    }*/				
				$("#tabs").removeClass('ui-state-info').append(li).tabs();				
			}
		});
		return false;
	});
	
	var evaluationyear = $("#evaluationyearid").val();
	if( evaluationyear !== "")
	{		
		$("#tabs").tabs();
	}	
	
	Performancematrix.getEvaluationyears(evaluationyear);
});

var Performancematrix   = {
		getEvaluationyears		: function( evalyear )
		{
			$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
				$.each( responseData, function( index, evalYear) {
					if( evalyear == evalYear.id )
					{
						$("#evaluationYear").fadeIn().html("Evaluation Year : "+evalYear.start+" -- "+evalYear.end );
					}
					if(evalyear == evalYear.id)
					{
                            $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end, selected:"selected"}))
					} else {
                            $("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end}))
					}		
				});								
			},"json");
		} ,

		save		: function()
		{
			jsDisplayResult("info", "info", "saving performance matrix . . .  <img src='public/images/loaderA32.gif' />");
			$.post("main.php?controller=performancematrix&action=save_matrix", {
				data 	: $("#performancematrixform").serialize()				
			}, function( response ) {
				if( response.error )
				{
					jsDisplayResult("error", "error", response.text);
				} else {
					jsDisplayResult("ok", "ok", response.text);
				}		
			},"json");
		}  , 
		
		
		displayJobLevels			: function( catID )
		{
			$("<div />",{id:"jobleveldialog_"+catID})
			 .append($("<table />")
			   .append($("<tr />")
				  .append($("<th />",{html:"Job Levels"}))
				  .append($("<td />")
					 .append($("<select />",{id:"cat_joblevel", name:"cat_joblevel", multiple:"multiple"}))	  
				  )
			    )
			   .append($("<tr />")
				 .append($("<td />",{colspan:"2"})
				   .append($("<p />")
					  .append($("<span />"))
					  .append($("<span />",{id:"message"}))
				    )		 
				 )	   
			   )
			 )
			 .dialog({
				 		autoOpen		: true, 
				 		modal			: true, 
				 		title 			: "Assign Job Levels", 
				 		width			: "400px",
				 		position		: "top",	
				 		buttons			: {
				 							"Assign"	: function()
				 							{
				 								if( $("#cat_joblevel").val() == null )
				 								{
				 									$("#message").html("Please assign at least one job level");
				 								} else {
				 									Performancematrix.assign( catID );	
				 								}			
				 							} , 
				 							
				 							"Cancel"		: function()
				 							{
				 								$(this).dialog("destroy");				 								
				 							}
			 			}
			 });
			
			Performancematrix.getJobLevels();
			
		} , 
		
		assign						: function( catID )
		{			
			$.post("main.php?controller=performancecategories&action=assignJobLevels",{
				joblevels 	: $("#cat_joblevel").val(), 
				catId 		: catID 
			}, function( response ){
				if( response.error )
				{
					$("#message").html( response.text );
				} else {
					var jobLevels = $("#cat_joblevel").val()
					if(jobLevels != null)
					{
						var levellst = "<ul id='"+catID+"_levellist'>";
						$.each( jobLevels, function( i, val){
							levellst += "<li>"+$("select#cat_joblevel option[value="+val+"]").text()+"</li>";
						});
						levellst += "</ul>";
						var catId = catID.replace("joblevels_", "");
						$("#td_"+catId).html( levellst );
					}
					$("#"+catID).attr("disabled", "disabled");
					jsDisplayResult("ok", "ok", response.text);
					$("#jobleveldialog_"+catID).dialog("destroy");
					$("#jobleveldialog_"+catID).remove();
				}	
			},"json");
			
		} , 
		
		getJobLevels			: function()
		{
			$.getJSON("main.php?controller=joblevel&action=getAll", 
			{
				data : { status : 1 }
			}, function( responseData ) {
				$.each( responseData , function( index, joblevel){
				   if( joblevel.used ==  false)
				   {
					$("#cat_joblevel").append($("<option />",{text:(joblevel.title == "" ? "" : joblevel.title+" - ")+joblevel.joblevel, value:joblevel.id,}))
				   } 			
				})				
			});
		}
			
};
