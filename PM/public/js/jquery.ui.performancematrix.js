$.widget("ui.performancematrix",{
	
	options			: {
		start		: 0,
		limit		: 10,
		current		: 1,
		total		: 0,
		tableId		: "component_"+(Math.random()),
		addKpa		: false,
		addKpi		: false, 
		addAction	: false, 
		evaluationyear	: ""
	} , 
	
	_init			: function()
	{
		this._getComponents();		
	} , 
	
	_create			: function()
	{
		var self = this;
		$(self.element).append($("<table />",{id:"componentTable"}).addClass("noborder"))		
	} , 	
	
	_getComponents		: function()
	{
		var self = this;
		$.getJSON("performancecomponents?action=getAllComponents", {
			evaluationyear		: self.options.evaluationyear		
		}, function( responseData ) {
			self._displayHeaders( responseData.headers );
			self._display( responseData.data, responseData.cols, responseData.kpa );
		});		
	} ,
	
	_displayHeaders		: function( headers )
	{
		var self = this;
		var tr = $("<tr />")
		tr.append($("<th />"))
		tr.append($("<th />"))
		tr.append($("<th />"))
		tr.append($("<th />"))
		$.each( headers, function( index, head){
			tr.append($("<th />",{html:head}))
		});
		tr.append($("<td />").addClass("noborder")
			.append($("<input />",{type:"button", name:"add_component", id:"add_component", value:"Add Component"}))	
		)
		$("#componentTable").append( tr );
	} , 
	
	_display			: function( components, cols, kpas)
	{
		var self = this;		
		$.each( components , function( index, component) {
			var tr = $("<tr />",{id:"tr_"+index});
			tr.append($("<td />")
			  .append($("<a />",{href:"#", id:"viewkpa_"+component.id})
				 .append($("<span />",{id:"iconcontent_"+component.id}).addClass("ui-icon").addClass("ui-icon-plus"))
			   )		
			)
			tr.append($("<td />"))
			tr.append($("<td />"))
			tr.append($("<td />",{colspan:cols+1, html:"<h5>"+component.name+"</h5>"}))
			tr.append($("<td />")
			   .append($("<input />",{type:"button", name:"addkpa_"+component.id, id:"addkpa_"+component.id, value:"Add Kpa"}))
 			)						
			$("#addkpa_"+component.id).live("click", function(){
				//self._addKpi( index )
				document.location.href = "kpa?action=addkpa&orgtype="+component.id+"&type="+self.options.goaltype;
				return false;
			});
	
			$("#componentTable").append( tr )
			if(!$.isEmptyObject(kpas))
			{
				$.each( kpas[component.id], function( i, kpaid){
					$("#componentTable").append($("<tr />",{id:component.id+"_kpa_"+kpaid})).addClass("componentKPA")					
				});				
			}			
			$("#viewkpa_"+component.id).live("click", function(){
				$("#iconcontent_"+component.id).removeClass("ui-icon-plus").addClass("ui-icon-minus")
				self._getKpa( component.id );				
				return false;
			});
			
		});
	} , 
	
	_getKpa				: function( componentId )
	{
		var self = this;
		$.getJSON("kpa?action=getAll", {
			evaluationyear 	: self.options.evaluationyear,
			componentid		: componentId, 
		},function( kpaData ){
			self._displayKpa( kpaData.data, kpaData.goals, kpaData.cols, kpaData.kpiInfo , componentId );
		});
	} , 
	
	_displayKpa		: function( kpa, goals, cols, kpi, componentId )
	{
		var self  = this;				
		$.each( kpa , function( index, data){
			$("#"+componentId+"_kpa_"+index).append($("<td />").css({"background-color":"#EBECE4"}))
			$("#"+componentId+"_kpa_"+index).append($("<td />")
			   .append($("<a />",{href:"#", id:"viewkpi_"+index}).css({"margin-right":"10px", "text-align":"left"})
				 .append($("<span />",{id:"kpiiconcontent_"+index}).addClass("ui-icon").addClass("ui-icon-plus").css({"margin-right":"10px", "text-align":"left"}))	   
			   )		
			)
			//$("#"+componentId+"_kpa_"+index).append($("<td />"))
			$("#"+componentId+"_kpa_"+index).append($("<td />"))			
			$("#"+componentId+"_kpa_"+index).append($("<td />"))
			$.each( data, function( key , val){
				$("#"+componentId+"_kpa_"+index).append($("<td />",{html:val}))				
			})			
			$("#"+componentId+"_kpa_"+index).append($("<td />")
			   .append($("<input />",{type:"button", name:"addkpi_"+index, id:"addkpi_"+index, value:"Add Kpi"}))
   			   .append($("<input />",{type:"button", name:"editkpi_"+index, id:"editkpi_"+index, value:"Edit Kpi"}))
			)						
			$("#addKpi_"+index).live("click", function(){
				//self._addKpi( index )
				document.location.href = "kpi?action=addkpi&kpaid="+index+"&type="+self.options.goaltype;
				return false;
			});

			$("#editKpi_"+index).live("click", function(){
				self._editKpi( index );			
				return false;
			});
			
			//$("#table_"+self.options.goaltype).append( tr );	
			if(!$.isEmptyObject(kpi))
			{
				$.each( kpi[index]['ids'], function( i , kpiVal){
					$("#"+componentId+"_kpa_"+index).after($("<tr />",{id:index+"_kpi_"+i})).toggle();				
				});				
			}
						
			$("#viewkpi_"+index).live("click", function(){
				$("#kpiiconcontent_"+index).removeClass("ui-icon-plus").addClass("ui-icon-minus")
				self._getKpi( index,componentId );
				return false;
			});
		});
	} , 
	
	_getKpi			: function( kpaId, type )
	{
		var self = this;
		$.getJSON("kpi?action=getAll", {
			kpaid		: kpaId, 
		},function( kpiData ){
			self._displayKpi( kpiData.data, kpiData.actionInfo, kpiData.cols, kpaId , type);
		});
	} , 
	
	_displayKpi	: function( data, actionInfo, cols, kpaId , type)
	{
		var self = this;
		$.each( data, function( index, kpi){	
			$("#"+kpaId+"_kpi_"+index).append($("<td />").css({"background-color":"#EBECE4"}))
			$("#"+kpaId+"_kpi_"+index).append($("<td />").css({"background-color":"#EBECE4"}))
			$("#"+kpaId+"_kpi_"+index).append($("<td />")
			   .append($("<a />",{href:"#", id:"viewaction_"+index}).css({"margin-right":"10px", "text-align":"left"})
				 .append($("<span />",{id:"actioniconcontent_"+index}).addClass("ui-icon").addClass("ui-icon-plus").css({"margin-right":"10px", "text-align":"left"}))	   
			   )		
			)
			//$("#"+kpaId+"_kpi_"+index).append($("<td />"))			
			$("#"+kpaId+"_kpi_"+index).append($("<td />"))
			$.each( kpi ,  function( key, val){
				$("#"+kpaId+"_kpi_"+index).append($("<td />",{html:val}))				
			});		
			
			$("#"+kpaId+"_kpi_"+index).append($("<td />")
			  .append($("<input />",{type:"button", name:"addaction_"+index, id:"addaction_"+index, value:"Add Action"}))
			  .append($("<input />",{type:"button", name:"editkpi_"+index, id:"editkpi_"+index, value:"Edit Kpi"}))
			)
			
			$("#addaction_"+index).live("click", function(){
				document.location.href = "action?action=addaction&kpiid="+index+"&type="+type;								
			});	
			
			$("#editkpi_"+index).live("click", function(){
				document.location.href = "action?action=editkpi_&id="+index;								
			});				

			
			if(!$.isEmptyObject(actionInfo[index]))
			{
				$.each(actionInfo[index], function( i ,actionId){
					$("#"+kpaId+"_kpi_"+index).after($("<tr />",{id:index+"_action_"+actionId})).toggle()					
				});				
			}
			
			$("#viewaction_"+index).live("click", function(){
				$("#actioniconcontent_"+index).removeClass("ui-icon-plus").addClass("ui-icon-minus")
				self._getAction( index , type);
				return false;
			});			
		});
	} , 
	
	_getAction			: function( kpiId, type)
	{
		var self = this;
		$.getJSON("action?action=getAll",{
			kpiId 	: kpiId			
		}, function( actionsData ) {
			self._displayActions( actionsData.data, kpiId ,type);			
		});
		
	} , 
	
	_displayActions			: function( actions, kpiId, type )
	{
		var self = this;
		$.each( actions, function( index, action) {
			$("#"+kpiId+"_action_"+index).append($("<td />").css({"background-color":"#EBECE4"}))
			$("#"+kpiId+"_action_"+index).append($("<td />").css({"background-color":"#EBECE4"}))
			$("#"+kpiId+"_action_"+index).append($("<td />").css({"background-color":"#EBECE4"}))
			$("#"+kpiId+"_action_"+index).append($("<td />").css({"text-align":"right"})
			  .append($("<span />").addClass("ui-icon").addClass("ui-icon-carat-1-sw"))		
			)
			$.each( action, function( i , val) {
				$("#"+kpiId+"_action_"+index).append($("<td />",{html:val}))		
			});
			$("#"+kpiId+"_action_"+index).append($("<td />")
			  .append($("<input />",{type:"button", name:"edit_"+index, id:"edit_"+index, value:"Edit Action"}))		
			)
			
			$("#edit_"+index).live("click", function(){
				document.location.href = "editaction?action=editkpa&id="+index;				
				return false;
			})
		});
		
	} ,
	
	
});
