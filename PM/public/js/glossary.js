$(function(){
	
		
    $("#add").click(function(e){
       var field = $("#name :selected").val();
       document.location.href = "main.php?controller=glossary&action=define_glossary&field="+field+"&parent=4";
       e.preventDefault();
    });
    
    $("#save").click(function(e){
      Glossary.save();
      e.preventDefault();
    });

    $("#savechanges").click(function(e){
      Glossary.edit();
      e.preventDefault();
    });    
     
    $(".edit").live('click', function(e){
       var  id = this.id;
       document.location.href = "main.php?controller=glossary&action=edit_glossary&id="+id+"&parent=4";
       e.preventDefault();
    });
    
    $(".delete").live('click', function(){   
     var  id = this.id;
     var glossaryid = id.substr(7);		
     if(confirm("Are you sure you want to delete this glossary"))
     {
        Glossary.deleteGlossary(glossaryid);     
     }
    });
     
          
});

var Glossary = Glossary || {};

Glossary  = {
     
	
	save 		: function(category)
	{
		var data	 	= {};		
		data.category 	 = $("#category").val();
		data.rules      = $("#txtDefaultHtmlArea").val();
		data.purpose    = $("#purpose").val();
		data.field     = $("#field").val();
		jsDisplayResult("info", "info", "Saving  . . . <img src='../images/loaderA32.gif' />");
		 $.post("main.php?controller=glossary&action=saveGlossary", {data : data}, function( response ){
			if( response.error )
			{
			  jsDisplayResult("error", "error", response.text);
			} else {
			   //Glossary.get(category)
			   jsDisplayResult("ok", "ok", response.text);	   
			}
		},"json");		
	},
	
	edit 		: function()
	{
	     var data	 	 = {};		
		data.category 	 = $("#category").val();
		data.rules      = $("#txtDefaultHtmlArea").val();
		data.purpose    = $("#purpose").val();
		data.field      = $("#field").val();
		data.id 		 = $("#id").val();
		jsDisplayResult("info", "info", "Updating  . . . <img src='../images/loaderA32.gif' />");		
		$.post("main.php?controller=glossary&action=updateGlossary", {data :data }, function( response ){	
		   if(response.error)
		   {
			jsDisplayResult("error", "error", response.text);		
		   } else {					
			jsDisplayResult("ok", "ok", response.text);		
		   }	
		},"json");
	} ,
	
	deleteGlossary           : function(id)
	{
	     var data	 	 = {};		
		jsDisplayResult("info", "info", "deleting glossary  . . . <img src='../images/loaderA32.gif' />");		
		$.post("main.php?controller=glossary&action=deleteGlossary", {data : id}, function( response ){	
		   if(response.error)
		   {
			jsDisplayResult("error", "error", response.text);		
		   } else {			
		     $("#tr_"+id).remove();
			jsDisplayResult("ok", "ok", response.text);		
		   }	
		},"json");
	}

};
