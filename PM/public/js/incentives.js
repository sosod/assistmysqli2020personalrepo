$(function(){
	
	$("#incent").hide();
	
	$("#evaluationyear").change(function(){
		var evalYear = $(this).val();
		
		$("#evaluationyearid").val( evalYear )
		
		if( evalYear == undefined || evalYear == "")
		{
			$("#incent").fadeOut();			
		} else {
			$("#incent").fadeIn();
		}	
		Incentives.get( evalYear );
		return false;
	});
	
	
	Incentives.getEvaluationyears();
	
	$("#addnew").click(function(){
		
		if( $("#addnewdialog").length > 0)
		{
			$("#addnewdialog").remove();
		}
		
		$("<div />",{id:"addnewdialog"})
		 .append($("<table />")
		   .append($("<tr />")
			  .append($("<th />",{html:"Rating(%):"}))
			  .append($("<td />")
				 .append($("<span />",{html:"From :"}))	  
				 .append($("<input />",{type:"text", id:"ratingfrom",name:"ratingfrom", value:"", size:"15"}))
				 .append($("<span />",{html:"To :"}))
				 .append($("<input />",{type:"text", id:"ratingto",name:"ratingto", value:"", size:"15"}))
			  )
		   )
		   .append($("<tr />")
			  .append($("<th />",{html:"Incentive Percentage:"}))
			  .append($("<td />")
				 .append($("<input />",{type:"text", name:"incentive", id:"incentive", value:""}))	  
			  )
		   )
		   .append($("<tr />")
			  .append($("<th />",{html:"Rating Description:"}))
			  .append($("<td />")
				 .append($("<textarea />",{type:"text", name:"description", id:"description"}))	  
			  )
		   )
		   .append($("<tr />")
			  .append($("<th />",{html:"Colour Assigned to rating:"}))
			  .append($("<td />")
				 .append($("<input />",{type:"text", name:"color", id:"color", value:""}))	  
			  )
		   )
		   .append($("<tr />")
			  .append($("<td />",{colspan:"2"})
				 .append($("<p />")
				   .append($("<span />"))
				   .append($("<span />",{id:"message"}))
				 )	  
			  )	   
		   )
		 ).dialog({
			 		autoOpen 		: true, 
			 		title			: "Add New Incentive/Bonus Parameters",
			 		modal			: true, 
			 		width			: "500px",
			 		position		: "top",
			 		buttons			: {
			 							"Save"		: function()
			 							{
			 								if( $("#evaluationyear :selected").val() == "")
			 								{
			 									$("#message").html("Please select the evaluation year");
			 									return false;
			 								} else if( $("#ratingfrom").val() == "'"){
			 									$("#message").html("Please enter the rating from ")
			 									return false;
			 								} else if( isNaN($("#ratingfrom").val() ) ){
			 									$("#message").html("Please enter a valid rating to ");
			 									return false;
			 								} else if( $("#ratingto").val() == ""){
			 									$("#message").html( "Please enter the rating to ");
			 									return false;
			 								} else if( isNaN( $("#ratingto").val() )){
			 									$("#message").html("Please enter a valid rating to");
			 									return false;
			 								} else if( $("#incentive").val() == ""){
			 									$("#message").html("Please enter the incentive percentage");
			 									return false;
			 								} else if( isNaN( $("#incentive").val() )){
			 									$("#message").html("Please enter a valid incentive percentage");
			 									return false;
			 								} else if( $("#description").val() == ""){
			 									$("#message").html("Please enter the description")
			 									return false;
			 								} else {
			 									Incentives.save();
			 								}
			 							} , 
			 							
			 							"Cancel"	: function()
			 							{
			 								$(this).dialog("destroy");			 								
			 							}
		 			}, 
		 			
		 		  open				: function( event, ui)
		 		  {
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});
		 		  }
		 })
		 var myPicker = new jscolor.color(document.getElementById('color'), {})
		 myPicker.fromString('99FF33')  // now you can access API via 'myPicker' variable
		
		return false;
	});
	
});

var Incentives 		= {
		
	get			: function( evalYear )
	{
		$(".incentives").remove();
		$.getJSON("main.php?controller=incentives&action=getAll",{
			evaluationyear 	: evalYear
		} ,function( responseData ) {
			if( $.isEmptyObject(responseData))
			{
				$("#table_incentives").append($("<tr />").addClass("incentives")
				   .append($("<td />",{colspan:"7", html:"There are no incentive/bonus setup for this evaluation year"}))	
				)				
			} else {
				$.each( responseData, function( index, incentive){
					Incentives.display( incentive );				
				});		
			}		
		});	
	
	} , 
	
	getEvaluationyears		: function()
	{
		$.post("main.php?controller=evaluationyears&action=getAll", { status : 1}, function( responseData ){
			$.each( responseData, function( index, evalYear){
				$("#evaluationyear").append($("<option />",{value:evalYear.id, text:evalYear.start+" -- "+evalYear.end}))					
			});								
		},"json");
	} ,
		
	
	save		: function()
	{
		var evalYear = $("#evaluationyearid").val();
		$.post("main.php?controller=incentives&action=saveIncentive",{
			data	: {
						color 		: $("#color").val(),
						ratingfrom : $("#ratingfrom").val(),
						ratingto   : $("#ratingto").val(),
						evaluationyear	: evalYear,
						description		: $("#description").val(),
						incentive		: $("#incentive").val()
					}
		},function( response ){
			if( response.error )
			{
				$("#message").html( response.text );
			} else {
				Incentives.get( evalYear );
				jsDisplayResult("ok", "ok", response.text );
				$("#addnewdialog").dialog("destroy");
				$("#addnewdialog").remove();
			}
		},"json");
		
	} , 
	
	update			: function( id )
	{
		$.post("main.php?controller=incentives&action=updateIncentive",{
			data	: {
						color 		: $("#color").val(),
						ratingfrom : $("#ratingfrom").val(),
						ratingto   : $("#ratingto").val(),
						description		: $("#description").val(),
						incentive		: $("#incentive").val()
					},
			id		: id
		},function( response ){
			if( response.error )
			{
				$("#message").html( response.text );
			} else {
				Incentives.get( $("#evaluationyearid").val() );
                    if(response.updated)
                    {
                       jsDisplayResult("info", "info", response.text );
                    } else {
                      jsDisplayResult("ok", "ok", response.text );					
                    } 	
				$("#editdialog").dialog("destroy");
				$("#editdialog").remove();
			}
		},"json");
	} , 
	
	updateStatus		: function( id, status )
	{
		$.post("main.php?controller=incentives&action=updateIncentive", {
			data 	: { status : status }, 
			id		: id
		}, function( response ){
			if( response.error )
			{
				$("#message").html( response.text )
			} else {
				Incentives.get( $("#evaluationyearid").val() );
		       if(response.updated)
		       {
			     jsDisplayResult("info", "info", response.text );
		       } else {
			     jsDisplayResult("ok", "ok", response.text );					
		       } 	
				$("#editdialog").dialog("destroy");
				$("#editdialog").remove();				
			}
		},"json");
	}, 
	
	deleteIncentive		: function( id )
	{
		if( confirm("Are you sure you want to delete this incentive parameter"))
		{
			Incentives.updateStatus( id, 2);			
		}
		
	} , 
	
	display					: function( incentive )
	{
		
		$("#table_incentives").append($("<tr />").addClass("incentives")
		  .append($("<td />",{html:incentive.id}))
		  .append($("<td />",{html:incentive.ratingfrom+" to "+incentive.ratingto}))
		  .append($("<td />",{html:incentive.incentive}))
		  .append($("<td />",{html:incentive.description}))
		  .append($("<td />")
			 .append($("<span />",{html:incentive.color}).css({"background-color":"#"+incentive.color}))	  
		  )
		  .append($("<td />",{html:((incentive.status & 1) == 1 ? "Active" : "Inactive")}))
		  .append($("<td />")
			 .append($("<input />",{type:"button", name:"edit_"+incentive.id, id:"edit_"+incentive.id, value:"Edit"}))	  
		  )
		)
		
		$("#edit_"+incentive.id).live("click", function(){
			
			if( $("#editdialog").length > 0 )
			{
				$("#editdialog").remove();				
			} 

			$("<div />",{id:"editdialog"})
			 .append($("<table />")			 
			   .append($("<tr />")
				  .append($("<th />",{html:"Rating(%):"}))
				  .append($("<td />")
					 .append($("<span />",{html:"From :"}))	  
					 .append($("<input />",{type:"text", id:"ratingfrom",name:"ratingfrom", value:incentive.ratingfrom, size:"15"}))
					 .append($("<span />",{html:"To :"}))
					 .append($("<input />",{type:"text", id:"ratingto",name:"ratingto", value:incentive.ratingto, size:"15"}))
				  )
			   )
			   .append($("<tr />")
				  .append($("<th />",{html:"Incentive Percentage:"}))
				  .append($("<td />")
					 .append($("<input />",{type:"text", name:"incentive", id:"incentive", value:incentive.incentive}))	  
				  )
			   )
			   .append($("<tr />")
				  .append($("<th />",{html:"Rating Description:"}))
				  .append($("<td />")
					 .append($("<textarea />",{type:"text", name:"description", id:"description", text:incentive.description}))	  
				  )
			   )
			   .append($("<tr />")
				  .append($("<th />",{html:"Colour Assigned to rating:"}))
				  .append($("<td />")
					 .append($("<input />",{type:"text", name:"color", id:"color", value:""}))	  
				  )
			   )	
			   .append($("<tr />")
				  .append($("<td />",{colspan:"2"})
					 .append($("<p />")
					   .append($("<span />"))
					   .append($("<span />",{id:"message"}))
					 )	  
				  )	   
			   )			   
			 ).dialog({
			 		autoOpen 	: true, 
			 		title	: "Edit Incentive/Bonus Parameters",
			 		modal	: true, 
			 		width	: "500px",
			 		position  : "top",
			 		buttons	: {
	 							"Save"		: function()
	 							{
	 								if( $("#evaluationyear :selected").val() == "")
	 								{
	 									$("#message").html("Please select the evaluation year");
	 									return false;
	 								} else if( $("#ratingfrom").val() == "'"){
	 									$("#message").html("Please enter the rating from ")
	 									return false;
	 								} else if( isNaN($("#ratingfrom").val() ) ){
	 									$("#message").html("Please enter a valid rating to ");
	 									return false;
	 								} else if( $("#ratingto").val() == ""){
	 									$("#message").html( "Please enter the rating to ");
	 									return false;
	 								} else if( isNaN( $("#ratingto").val() )){
	 									$("#message").html("Please enter a valid rating to");
	 									return false;
	 								} else if( $("#incentive").val() == ""){
	 									$("#message").html("Please enter the incentive percentage");
	 									return false;
	 								} else if( isNaN( $("#incentive").val() )){
	 									$("#message").html("Please enter a valid incentive percentage");
	 									return false;
	 								} else if( $("#description").val() == ""){
	 									$("#message").html("Please enter the description")
	 									return false;
	 								} else {
	 									Incentives.update( incentive.id );
	 								}
	 							} , 
	 							
	 							"Cancel"	: function()
	 							{
	 								$(this).dialog("destroy");			 								
	 							} , 
	 							
	 							"Delete"		: function()
	 							{
	 								Incentives.deleteIncentive( incentive.id );
	 							} , 
	 							
	 							"Deactivate"	: function()
	 							{
	 								Incentives.updateStatus( incentive.id, 0 );
	 							} , 
	 							
	 							"Active"		: function()
	 							{
	 								Incentives.updateStatus( incentive.id, 1 );
	 							}
		 			}, 
		 			
		 		  open				: function( event, ui)
		 		  {
	 				var displayDelete = false;
	 				var displayDeactivate = false;
	 				var displayActivate = false; 
	 				//if the status is for the default , then dont display delete
	 				if( (incentive.status & 1) == 1)
	 				{
	 					displayDeactivate = true;
	 				} else {
	 					displayActivate = true;
	 				}
	 				
	 				if( incentive.used == false)
	 				{
	 					displayDelete 	  = true;
	 					displayDeactivate = false;
	 					displayActivate	  = false;
	 				}	
	 				
	 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
	 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");			 				
	 				var saveButton = buttons[0];
	 				var deleteButton = buttons[2]
	 				var deactivateButton = buttons[3];
	 				var activateButton   = buttons[4];
	 				$(saveButton).css({"color":"#090"});
	 				$(deleteButton).css({"color":"red", display:(displayDelete ? "inline" : "none")});
	 				$(deactivateButton).css({"color":"red", display:(displayDeactivate ? "inline" : "none")});
	 				$(activateButton).css({"color":"#090", display:(displayActivate ? "inline" : "none")});
		 		  }
			 })
			 
			 var myPicker = new jscolor.color(document.getElementById('color'), {})
			 myPicker.fromString( incentive.color)  // now you can access API via 'myPicker' variable
						  			
			return false;
		})
		
	}
		
		
		
};
