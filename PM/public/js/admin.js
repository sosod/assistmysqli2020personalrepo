$(function(){
	
	$("#admintable tr:even").css({"background-color":"#F7F7F7"});
	
	$("input[value='Configure']").click(function(){
		var controller = $(this).attr("name");
		var action = $(this).attr("id");		
		document.location.href = "main.php?controller="+controller+"&action="+(action == controller ? "admin" : action)+"&parent="+$("#parentid").val()+"&folder=admin";
		return false;
	});
});
