$.widget("ui.goals", {
	
	options		: {
			goaltype	: "",
			evaluationyear	: "",
			section		: "",
			sectionName	: "",
			start 		: 0, 
			limit		: 10, 
			current		: 1, 
			total		: 0,
			id			: (Math.random() * Math.random()),
			url 		: "", 
			addKpa		: false,
			editKpa		: false,
			addKpi		: false,
			addAction	: false
	} , 
		
	_init		: function()
	{
		this._getGoals();		
	} , 
	
	_create		: function()
	{
		var self = this;
		$(self.element).append($("<table />",{id:"table_"+self.options.goaltype}).addClass("noborder"))		
	} , 
	
	_getGoals		: function()
	{
		var self = this;
		$.getJSON(self.options.url, {
			evaluationyear	: self.options.evaluationyear,
			componentid		: self.options.goaltype, 			
			kpastatus		: { kpastatus : self.options.section } 
		},function( goalsData ){
			
			$("#table_"+self.options.goaltype).html("");
			self._displayComponent( goalsData.goals, goalsData.cols  );			
			self._displayHeaders( goalsData.headers );
			if( $.isEmptyObject(goalsData.data))
			{
				$("#table_"+self.options.goaltype).append($("<tr />")
				  .append($("<td />",{colspan:goalsData.cols+4})
					 .append($("<p />").addClass("ui-widget").addClass("ui-state").addClass("ui-state-info").css({padding:"5px"})
					   .append($("<span />",{html:"No results returned for this evaluation year"}))
					   )
					 )	  	
				)
			} else {
				self._display( goalsData.data, goalsData.goals, goalsData.cols, goalsData.kpiInfo );
			}
		});
		
	} , 
	
	_displayComponent	: function( goal, cols )
	{
		var self  = this;
		$("#table_"+self.options.goaltype).append($("<tr />").css({"color":"#151B8D"})
		   .append($("<td />").css({display:(self.options.addKpa ? "table-cell" : "none")}).addClass("noborder")
			  .append($("<input />",{type:"button", name:"addkpa_"+goal.id, id:"addkpa_"+goal.id, value:"Add KPA"}))	   
		   )
		   //.append($("<td />"))
		);		
		
		$("#addkpa_"+goal.id).live("click", function(){
			document.location.href = "kpa?action=setup_addkpa&componentid="+self.options.goaltype+"&evaluationyear="+self.options.evaluationyear;
			//self._addKpa( goal.id )			
			return false;
		})
		
	} , 
	
	_display			: function( kpa, goal, cols , kpiInfo)
	{
		var self  = this;				
		$.each( kpa , function( index, data){
			var tr = $("<tr />",{id:"tr_"+index})
			tr.append($("<td />")
			   .append($("<a />",{href:"#", id:"viewkpi_"+index}).css({"margin-right":"10px", "text-align":"left"})
				 .append($("<span />").addClass("ui-icon").addClass("ui-icon-plus").css({"margin-right":"10px", "text-align":"left"}))	   
			   )		
			)
			tr.append($("<td />",{colspan:"2"}))
			$.each( data, function( key , val){
				tr.append($("<td />",{html:val}))				
			})			
			tr.append($("<td />")
			   .append($("<input />",{type:"button", name:"addkpi_"+index, id:"addkpi_"+index, value:"Add KPI"}))
   			   .append($("<input />",{type:"button", name:"editkpi_"+index, id:"editkpi_"+index, value:"Edit KPA"}))
			)						
			$("#addKpi_"+index).live("click", function(){
				//self._addKpi( index )
				document.location.href = "kpi?action=setup_addkpi&kpaid="+index+"&componentid="+self.options.goaltype+"&evaluationyear="+self.options.evaluationyear;
				return false;
			});

			$("#editKpi_"+index).live("click", function(){
				self._editKpi( index );			
				return false;
			});
			
			$("#table_"+self.options.goaltype).append( tr );	
			if(!$.isEmptyObject(kpiInfo))
			{
				$.each( kpiInfo[index]['ids'], function( i , kpiVal){
					$("#table_"+self.options.goaltype).append($("<tr />",{id:index+"_kpi_"+i}).css({"background-color":"#D8D8D8"}));				
				});				
			}
						
			$("#viewkpi_"+index).live("click", function(){
				self._getKpi( index );
				return false;
			});
		});
		
		var displayBtn = true;
		/*if( self.options.goaltype == 1 )
		{
			displayBtn = false;			
		}*/
		
		if( self.options.section == 2 )
		{
			displayBtn = false;			
		}	

		$("#table_"+self.options.goaltype).append($("<tr />").css({display:(displayBtn  ? "table-cell" : "none")})
		   .append($("<td />",{colspan:cols}).css({"text-align":"right"}).addClass("noborder")
			  .append($("<input />",{type:"button", name:"configure_"+self.options.goaltype , id:"configure_"+self.options.goaltype, value:"Configure"}))	   
		   )		
		)
		$("#configure_"+self.options.goaltype).live("click", function(){
			document.location.href = "kpa?action=configure&type="+self.options.goaltype+"&evaluationyear="+self.options.evaluationyear
			return false;
		});
		
	} ,
	
	_getKpi				: function( kpaId )
	{
		var self = this;
		$.getJSON("kpi?action=getAll", {
			kpaid	: kpaId			
		},function( kpiData  ){
			if( $.isEmptyObject(kpiData.data ))
			{
					
			} else {
				self._displayKpi(kpiData.data, kpiData.cols, kpaId, kpiData.actionInfo );	
			}
		});
	} ,
	
	_displayKpi			: function( kpiData, cols, tableId, actionInfo )
	{		
		var self = this;		
		
		$.each( kpiData, function( id , kpi) {
			$("#"+tableId+"_kpi_"+id).append($("<td />",{colspan:"2"}))
			$("#"+tableId+"_kpi_"+id).append($("<td />")
			   .append($("<a />",{href:"#", id:"viewaction_"+id}).css({"margin-right":"10px", "text-align":"right"})
				 .append($("<span />").addClass("ui-icon").addClass("ui-icon-plus").css({"margin-right":"10px", "text-align":"right"}))	   
			   )		
			)			
			$.each( kpi, function( key, val) {
				$("#"+tableId+"_kpi_"+id).append($("<td />",{html:val}))				
			});
			
			//$("#"+tableId+"_kpi_"+index).append($("<td />"))
			$("#"+tableId+"_kpi_"+id).append($("<td />")
			   .append($("<input />",{type:"button", name:"addaction_"+id, id:"addaction_"+id, value:"Add Action"}))
			   .append($("<input />",{type:"button", name:"editkpi_"+id, id:"editkpi_"+id, value:"Edit KPI"}))
			)
			//$("#"+tableId+"_kpi_"+index).append($("<td />"))			
			
			$("#addaction_"+id).live("click", function(){
				document.location.href = "action?action=setup_addaction&kpiid="+id+"&componentid="+self.options.goaltype+"&evaluationyear="+self.options.evaluationyear+"&kpiid="+id;
				//self._addAction( id );
				return false;
			});
			$("#editkpi_"+id).live("click", function(){
				self._addKpi( id );
				return false;
			});	
			if(!$.isEmptyObject(actionInfo[id]))
			{
				$.each( actionInfo[id] ,function( actionId, action) {
					$("#"+tableId+"_kpi_"+id).after($("<tr />",{id:id+"_action_"+actionId}).css({"background-color":"#E6E6E6"}))
				});	
			}
			
			$("#viewaction_"+id).live("click", function(){
				self._getActions( id );				
				return false;
			});			
			
		});
	} ,
	
	_getActions			: function( index )
	{
		var self = this;
		$.getJSON("action?action=getAll",{
			kpiId 	: index			
		}, function( actionsData ) {
		    if($.isEmptyObject(actionsData.data))
		    {
		        //$("#"+index+"_action").append($("<td />",{html:"There are no actions"}))
		    } else {
			    self._displayActions(actionsData.data, index );			
			}
		});
		
	} , 
	
	_displayActions			: function( actions, kpiId )
	{
		var self = this;
		$.each( actions, function( index, action) {
			$("#"+kpiId+"_action_"+index).append($("<td />",{colspan:3}).css({"text-align":"right"})
			  .append($("<span />").addClass("ui-icon").addClass("ui-icon-minus"))		
			)
			$.each( action, function( i , val) {
				$("#"+kpiId+"_action_"+index).append($("<td />",{html:val}))		
			});
			$("#"+kpiId+"_action_"+index).append($("<td />")
			  .append($("<input />",{type:"button", name:"edit_"+index, id:"edit_"+index, value:"Edit Action"}))		
			)
		});
		
	} ,
	
	_displayHeaders		: function( headers )
	{
		var self = this;
		var tr   = $("<tr />");
		tr.append($("<th />",{colspan:"3"}))
		$.each( headers, function( key , head){
			tr.append($("<th />",{html:head}));			
		});
		tr.append($("<th />").css({display:(self.options.addKpa ? "table-cell" : "none")}))
		//tr.append($("<th />").css({display:(self.options.editKpa ? "table-cell" : "none")}))
		$("#table_"+self.options.goaltype).append( tr );
	} ,

	_addKpa				: function( goalid )
	{
		var self = this;
		var showOwner 		 = true;
		var showCompetency   = true;
		var showProficiency  = true;
		var showTargetPeriod = true;
		var showWeighting	 = true;
		var showDealine 	 = true; 
		if( self.options.goaltype = 2)
		{
			showOwner 		 = false;
			showCompetency   = false;
			showProficiency  = false;
			showTargetPeriod = false
			showWeighting	 = false;
			showDealine		 = false;
		}
		
		$("<div />",{id:"addkpadialog_"+goalid})
		 .append($("<form />",{id:"addkpaform"})
			 .append($("<table />")
			   .append($("<tr />")
				   .append($("<th />",{html:"Name:"}))
				   .append($("<td />")
					 .append($("<textarea />",{name:"name", id:"name", value:""}))	   
				   )			 
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Objective:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"objective", name:"objective"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Outcome:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"outcome", name:"outcome"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Measurement:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"measurement", name:"measurement"}))		 
				 )
			   )
			   .append($("<tr />").css({display:(showOwner ? "table-row" : "none")})
				 .append($("<th />",{html:"Owner:"}))
				 .append($("<td />")
				   .append($("<select />",{id:"owner", name:"owner"})
					 .append($("<option />",{value:"", text:"--owner--"}))	   
				   )		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Proof of Evidence:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"proof_of_evidence", name:"proof_of_evidence"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Baseline:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"baseline", name:"baseline"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Target Unit:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"targetunit", name:"targetunit"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Performance Standard:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"performance_standard", name:"performance_standard"}))		 
				 )
			   )
			   .append($("<tr />").css({display:(showTargetPeriod ? "table-row" : "none")})
				 .append($("<th />",{html:"Target per period:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"target_period", name:"target_period"}))		 
				 )
			   )	
			   .append($("<tr />").css({display:(showWeighting ? "table-cell" : "none")})
				 .append($("<th />",{html:"Weighting:"}))
				 .append($("<td />")
				   .append($("<input />",{type:"text", id:"weighting", name:"weighting"}))		 
				 )
			   )	
			   .append($("<tr />").css({display:(showCompetency ? "table-cell" : "none")})
				 .append($("<th />",{html:"Competency:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"competency", name:"competency"}))		 
				 )
			   )		
			   .append($("<tr />").css({display:(showProficiency ? "table-cell" : "none")})
				 .append($("<th />",{html:"Proficiency:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"proficiency", name:"proficiency"}))		 
				 )
			   )
			   .append($("<tr />").css({display:(showDealine ? "table-row" : "none")})
				 .append($("<th />",{html:"Deadline:"}))
				 .append($("<td />")
				   .append($("<input />",{type:"text", id:"deadline", name:"deadline"}).addClass("datepicker"))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Comments:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"comments", name:"comments"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<td />",{colspan:"2"})
				   .append($("<p />").addClass("ui-widget").addClass("ui-class")
					  .append($("<span />"))
					  .append($("<span />",{id:"message"}))
				   )		 
				 )
			   )		   
			 )
			)
		 .dialog({
			 		autoOpen	: true, 
			 		modal		: true,
			 		width		: "500px",
			 		position	: "top",
			 		title		: "Add New Kpa",
			 		buttons		: {
			 						"Save"		: function()
			 						{
			 								self._saveKpa( goalid )			 
			 						} , 
			 						
			 						"Cancel"	: function()
			 						{
			 							$(this).dialog("destroy")			 							
			 						}
		 			} , 
		 			open			: function( event, ui)
		 			{
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});	 	
		 				
		 				$(".datepicker").datepicker({
		 					showOn			: "both",
		 					buttonImage 	: "/library/jquery/css/calendar.gif",
		 					buttonImageOnly	: true,
		 					changeMonth		: true,
		 					changeYear		: true,
		 					dateFormat 		: "dd-M-yy",
		 					altField		: "#startDate",
		 					altFormat		: 'd_m_yy'
		 				}).attr("readonly", "readonly");
		 			}
			 
		 })
		
	} , 
	
	
	_addKpi				: function( kpaid )
	{
		var self = this;
		var showOwner 		 = true;
		var showCompetency   = true;
		var showProficiency  = true;
		var showTargetPeriod = true;
		var showWeighting	 = true;
		var showDealine 	 = true; 
		if( self.options.goaltype = 2)
		{
			showOwner 		 = false;
			showCompetency   = false;
			showProficiency  = false;
			showTargetPeriod = false
			showWeighting	 = false;
			showDealine		 = false;
		}		
		$("<div />",{id:"addkpidialog_"+kpaid})
		 .append($("<form />",{id:"addkpiform"})
			 .append($("<table />")
			   .append($("<tr />")
				   .append($("<th />",{html:"Name:"}))
				   .append($("<td />")
					 .append($("<textarea />",{name:"name", id:"name", value:""}))	   
				   )			 
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Objective:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"objective", name:"objective"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Outcome:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"outcome", name:"outcome"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Measurement:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"measurement", name:"measurement"}))		 
				 )
			   )
			   .append($("<tr />").css({display:(showOwner ? "table-row" : "none")})
				 .append($("<th />",{html:"Owner:"}))
				 .append($("<td />")
				   .append($("<select />",{id:"owner", name:"owner"})
					 .append($("<option />",{value:"", text:"--owner--"}))	   
				   )		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Proof of Evidence:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"proof_of_evidence", name:"proof_of_evidence"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Baseline:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"baseline", name:"baseline"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Target Unit:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"targetunit", name:"targetunit"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"National Kpa:"}))
				 .append($("<td />")
				   .append($("<select />",{id:"nationalkpa", name:"nationalkpa"})
					  .append($("<option />",{value:"", text:"--national kpa--"}))	   
				   )		 
				 )
			   )			   
			   .append($("<tr />")
				 .append($("<th />",{html:"IDP Objectives:"}))
				 .append($("<td />")
				   .append($("<select />",{id:"idpobjectives", name:"idpobjectives"})
					  .append($("<option />",{value:"", text:"--idp objectives--"}))	   
				   )		 
				 )
			   )			   			   
			   .append($("<tr />")
				 .append($("<th />",{html:"Performance Standard:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"performance_standard", name:"performance_standard"}))		 
				 )
			   )
			   .append($("<tr />").css({display:(showTargetPeriod ? "table-row" : "none")})
				 .append($("<th />",{html:"Target per period:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"target_period", name:"target_period"}))		 
				 )
			   )	
			   .append($("<tr />").css({display:(showWeighting ? "table-row" : "none")})
				 .append($("<th />",{html:"Weighting:"}))
				 .append($("<td />")
				   .append($("<input />",{type:"text", id:"weighting", name:"weighting"}))		 
				 )
			   )	
			   .append($("<tr />").css({display:(showDealine ? "table-row" : "none")})
				 .append($("<th />",{html:"Deadline:"}))
				 .append($("<td />")
				   .append($("<input />",{type:"text", id:"deadline", name:"deadline"}).addClass("datepicker"))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Comments:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"comments", name:"comments"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<td />",{colspan:"2"})
				   .append($("<p />").addClass("ui-widget").addClass("ui-class")
					  .append($("<span />"))
					  .append($("<span />",{id:"message"}))
				   )		 
				 )
			   )		   
			 )
			)
		 .dialog({
			 		autoOpen	: true, 
			 		modal		: true,
			 		width		: "500px",
			 		position	: "top",
			 		title		: "Add New Kpi",
			 		buttons		: {
			 						"Save"		: function()
			 						{
			 								self._saveKpi( kpaid )			 
			 						} , 
			 						
			 						"Cancel"	: function()
			 						{
			 							$(this).dialog("destroy");
			 						}
		 			} , 
		 			open			: function( event, ui)
		 			{
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});	 
		 				
		 				$(".datepicker").datepicker({
		 					showOn			: "both",
		 					buttonImage 	: "/library/jquery/css/calendar.gif",
		 					buttonImageOnly	: true,
		 					changeMonth		: true,
		 					changeYear		: true,
		 					dateFormat 		: "dd-M-yy",
		 					altField		: "#startDate",
		 					altFormat		: 'd_m_yy'
		 				}).attr("readonly", "readonly");
		 				

	 					
		 			}
		 })
	} ,
	
	_addAction				: function( kpiid )
	{
		var self = this;
		var showOwner 		 = true;
		var showCompetency   = true;
		var showProficiency  = true;
		var showTargetPeriod = true;
		var showWeighting	 = true;
		var showDealine 	 = true; 
		if( self.options.goaltype = 2)
		{
			showOwner 		 = false;
			showCompetency   = false;
			showProficiency  = false;
			showTargetPeriod = false
			showWeighting	 = false;
			showDealine		 = false;
		}				
		$("<div />",{id:"addactiondialog_"+kpiid})
		 .append($("<form />",{id:"addactionform"})
			 .append($("<table />")
			   .append($("<tr />")
				   .append($("<th />",{html:"Name:"}))
				   .append($("<td />")
					 .append($("<textarea />",{name:"name", id:"name", value:""}))	   
				   )			 
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Objective:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"objective", name:"objective"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Outcome:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"outcome", name:"outcome"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Measurement:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"measurement", name:"measurement"}))		 
				 )
			   )
			   .append($("<tr />").css({display:(showOwner ? "table-row" : "none")})
				 .append($("<th />",{html:"Owner:"}))
				 .append($("<td />")
				   .append($("<select />",{id:"owner", name:"owner"})
					 .append($("<option />",{value:"", text:"--owner--"}))	   
				   )		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Proof of Evidence:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"proof_of_evidence", name:"proof_of_evidence"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Baseline:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"baseline", name:"baseline"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Target Unit:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"targetunit", name:"targetunit"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Performance Standard:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"performance_standard", name:"performance_standard"}))		 
				 )
			   )
			   .append($("<tr />").css({display:(showTargetPeriod ? "table-row" : "none")})
				 .append($("<th />",{html:"Target per period:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"target_period", name:"target_period"}))		 
				 )
			   )	
			   .append($("<tr />").css({display:(showWeighting ? "table-row" : "none")})
				 .append($("<th />",{html:"Weighting:"}))
				 .append($("<td />")
				   .append($("<input />",{type:"text", id:"weighting", name:"weighting"}))		 
				 )
			   )	
			   .append($("<tr />")
				 .append($("<th />",{html:"Competency:"}))
				 .append($("<td />")
				   .append($("<input />",{type:"text", id:"competency", name:"competency"}))		 
				 )
			   )	
			   .append($("<tr />")
				 .append($("<th />",{html:"Proficiency:"}))
				 .append($("<td />")
				   .append($("<input />",{type:"text", id:"proficiency", name:"proficiency"}))		 
				 )
			   )				   			   			   
			   .append($("<tr />").css({display:(showDealine ? "table-row" : "none")})
				 .append($("<th />",{html:"Deadline:"}))
				 .append($("<td />")
				   .append($("<input />",{type:"text", id:"deadline", name:"deadline"}).addClass("datepicker"))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<th />",{html:"Comments:"}))
				 .append($("<td />")
				   .append($("<textarea />",{id:"comments", name:"comments"}))		 
				 )
			   )
			   .append($("<tr />")
				 .append($("<td />",{colspan:"2"})
				   .append($("<p />").addClass("ui-widget").addClass("ui-class")
					  .append($("<span />"))
					  .append($("<span />",{id:"message"}))
				   )		 
				 )
			   )		   
			 )
			)
		 .dialog({
			 		autoOpen	: true, 
			 		modal		: true,
			 		width		: "500px",
			 		position	: "top",
			 		title		: "Add New Action",
			 		buttons		: {
			 						"Save"		: function()
			 						{
			 								self._saveAction( kpiid )			 
			 						} , 
			 						
			 						"Cancel"	: function()
			 						{
			 							$(this).dialog("destroy");
			 						}
		 			} , 
		 			open			: function( event, ui)
		 			{
		 				var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		 				var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		 				var saveButton = buttons[0];
		 				$(saveButton).css({"color":"#090"});	 
		 				
		 				$(".datepicker").datepicker({
		 					showOn			: "both",
		 					buttonImage 	: "/library/jquery/css/calendar.gif",
		 					buttonImageOnly	: true,
		 					changeMonth		: true,
		 					changeYear		: true,
		 					dateFormat 		: "dd-M-yy",
		 					altField		: "#startDate",
		 					altFormat		: 'd_m_yy'
		 				}).attr("readonly", "readonly");
	 					
		 			}
			 
		 })
		
	} ,

	
	_saveKpa			: function( id )
	{
		$.post("kpa?action=saveKpa",{
			data 		 : $("#addkpaform").serialize() , 
			componentid	 : id			
		}, function( response ) {
			if( response.error )
			{
				$("#message").html( response.text )
			} else {
				jsDisplayResult("ok", "ok", response.text );
				$("#addkpadialog_"+id).dialog("destroy");
				$("#addkpadialog_"+id).remove();
			}			
		},"json");		
		
	} ,
	
	_saveKpi			: function( id )
	{
		$.post("kpi?action=saveKpi",{
			data 		: $("#addkpiform").serialize() , 
			kpaid	 	: id			
		}, function( response ) {
			if( response.error )
			{
				$("#message").html( response.text )
			} else {
				jsDisplayResult("ok", "ok", response.text );
				$("#addkpidialog_"+id).dialog("destroy");
				$("#addkpidialog_"+id).remove();
			}			
		},"json");		
		
	} , 
	
	_saveAction				: function( id )
	{
		$.post("action?action=saveAction",{
			data 	: $("#addactionform").serialize(),
			kpiId	: id		
		}, function( response ){
			if( response.error )
			{
				$("#message").html( response.text )
			} else {
				jsDisplayResult("ok", "ok", response.text );
				$("#addactiondialog_"+id).dialog("destroy");
				$("#addactiondialog_"+id).remove();
			}	
		},"json");	
	}
	
});

