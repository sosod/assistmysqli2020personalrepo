$.widget("ui.kpi", {
    
    options     : {
        start                 : 0,
        limit                 : 10,   
        current               : 1,
        total                 : 1,
        evaluationyear        : 0,
        componentid           : 0, 
        kpaid                 : 0,
        section               : "",
        page                  : "",
        displayOptions        : {},
        url                   : "",
        kpiSettings           : {},
        user                  : "",
        evaluations           : {},
        statusOfEvaluations   : {},
        autoLoad              : true,
        openAction            : false,
        kpiNextEvaluation     : {},
        kpiStatuses           : {},
        userdata              : {},
        userlist              : {},
        userlogged            : {},
        jointByManager        : false,
        inCommittee           : false,
        userNext              : ""
    } , 
    
    _init        : function()
    {
      if(this.options.autoLoad)
      {
        this._get();
      }
    } , 
    
    _create     : function()
    {
       var self = this;
    } , 
    
    _get        : function()
    {
        var self = this;
          var kpiUrl = (self.options.url == "" ? "main.php?controller=kpi&action=getAll" : self.options.url);
		$.getJSON(kpiUrl, {
		   kpaid	: self.options.kpaid,
		   section	: self.options.section,
		   componentid : self.options.componentid,
		   user        : self.options.user,
		   page        : self.options.page 		
		},function(kpiData){
            $(".childfor_"+self.options.kpaid).html("");
            self.options.userdata     = kpiData.userdata
            self.options.userlogged   = kpiData.userlogged
            self.options.inCommittee  = kpiData.in_committee;

            if(kpiData.hasOwnProperty('settings'))
            {
              self.options.kpiSettings = kpiData['settings'];
              if(kpiData['settings'].hasOwnProperty('next_evaluation'))
              {
                self.options.kpiNextEvaluation = kpiData['settings']['next_evaluation'];
              }        
              if(kpiData['settings'].hasOwnProperty('evaluations'))
              {
                self.options.evaluations = kpiData['settings'].evaluations;    
              }                  
               
            } 
            if(kpiData.hasOwnProperty('statuses'))
            {
              self.options.kpiStatuses = kpiData.statuses;
            }                            
			if(!$.isEmptyObject(kpiData.data ))
			{
			   self._display(kpiData.data, kpiData.cols);	
			}
		});
    } , 
    
    _display        : function(data, cols)
    {
       var self        = this;
       var kpiSettings = self.options.kpiSettings;
       $.each(data, function(index, kpi) {
         var html        = [];
         html.push("<td></td>");
         html.push("<td>");
          html.push("<a href='#' id='viewaction_"+index+"' style='padding:0; margin:0;'>");
            html.push("<span id='viewactionicon_"+index+"' class='ui-icon ui-icon-"+(self.options.openAction ? "minus" : "plus")+"'>");
          html.push("</a>");
         html.push("</td>");
         html.push("<td>&nbsp;&nbsp;</td>");
         $("#viewaction_"+index).live("click", function(){
            if($(".child_action_for_"+index).is(":hidden"))
            {
               $("#viewactionicon_"+index).removeClass("ui-icon-plus").addClass("ui-icon-minus");
            } else {
                $("#viewactionicon_"+index).removeClass("ui-icon-minus").addClass("ui-icon-plus");
            }
            $(".child_action_for_"+index).toggle();
            return false;
         });         
         $.each(kpi, function(key, value) {
             html.push("<td style='color:#333333;'>"+value+"</td>");
         });   
       
         var displayOptions              = self.options.displayOptions;
         var displayEvaluationButton     = self._canEvaluateKpi(index);
         var displayEvaluationEditButton = self._canEditKpiEvaluation(index);
          //var kpiEvaluationStatuses = self._getKpiEvaluationStatuses(index, evaluationStatus)       
         var canAddAction            = true;
         if(self.options.kpiStatuses.hasOwnProperty(index))
         {
             if((self.options.kpiStatuses[index] & 4) == 4)
             {
               canAddAction = false;
             }
         }
         if(displayOptions['displayTd'])
         {
           html.push("<td>");
             if(displayOptions['displayAddBtn'])
             {

                html.push("<input type='button' name='"+self.options.kpaid+"_addaction_"+index+"' id='"+(canAddAction ? self.options.kpaid+"_addaction_"+index : "add_action_"+index)+"' value='Add Action' />");
                $("#"+self.options.kpaid+"_addaction_"+index).live("click", function(){
                   document.location.href = "main.php?controller=action&action=addaction&kpiid="+index+"&kpaid="+self.options.kpaid+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section+"&user="+self.options.user            			
                     return false;
                });                
                    
             }
             if(displayOptions['displayEditBtn'])
             {
                html.push("<input type='button' name='"+self.options.kpaid+"_edit_"+index+"' id='"+self.options.kpaid+"_edit_"+index+"' value='Edit KPI' />");
                
              $("#"+self.options.kpaid+"_edit_"+index).live("click", function(){
                if(self.options.section == "setup"){
                   document.location.href = "main.php?controller=kpi&action=setup_edit&id="+index+"&kpaid="+self.options.kpaid+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section           
                } else {
                   document.location.href = "main.php?controller=kpi&action=edit&id="+index+"&kpaid="+self.options.kpaid+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section
                }
                return false;
              });                
                
                html.push("<input type='button' name='"+self.options.kpaid+"_delete_"+index+"' id='"+self.options.kpaid+"_delete_"+index+"' value='Delete KPI' />");  
                
               $("#"+self.options.kpaid+"_delete_"+index).live("click", function(){
                 jsDisplayResult("info", "info", "deleting kpi . . . <img src='public/images/loaderA32.gif' /> " );  
                 if(confirm("Are you sure you want to delete this KPI"))
                 {
	              $.post("main.php?controller=kpi&action=updateStatus",{
		              kpistatus	: 2,
		              id		: index
	              }, function(response) {
		              if(response.error)
		              {
			           jsDisplayResult("error", "error", response.text );
		              } else {
		               if(response.updated)
		               {
                            $("#"+self.options.kpaid+"_childtr_"+index).remove();		
                            jsDisplayResult("info", "info", response.text);
		               } else {
		                  $("#"+self.options.kpaid+"_childtr_"+index).remove();		
			             jsDisplayResult("ok", "ok", response.text);
			             self._get();		               
		               }
		             }		
	              },"json");                    
                 }           
                   return false;
               });                                  
                    
             }   
             if(displayOptions['displayUpdateBtn'])
             {
                html.push("<input type='button' name='"+self.options.kpaid+"_update_"+index+"' id='"+self.options.kpaid+"_update_"+index+"' value='Update KPI' />");
                
                $("#"+self.options.kpaid+"_update_"+index).live("click", function(){
                   document.location.href = "main.php?controller=kpi&action=updatekpi&id="+index+"&kpaid="+self.options.kpaid+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section
                    return false;
                });                     
             }                
             if(displayEvaluationButton)
             {
                html.push("<input type='button' name='"+self.options.kpaid+"_evaluate_"+index+"' id='"+self.options.kpaid+"_evaluate_"+index+"' value='Evaluate KPI' />");
                
                $("#"+self.options.kpaid+"_evaluate_"+index).live("click", function(e){
                     self._evaluateKpi(index);
                     e.preventDefault();   
                });
             }  
             if(displayEvaluationEditButton)
             {
                html.push("<input type='button' name='"+self.options.componentid+"_edit_evaluate_"+index+"' id='"+self.options.componentid+"_edit_evaluate_"+index+"' value='Edit KPI Evaluation' />");   
                
	            $("#"+self.options.componentid+"_edit_evaluate_"+index).live("click", function(e){   
	                self._editKpiEvaluate(self.options.componentid, index); 
	                e.preventDefault();
	            });		                                                     
              }             
             
             if(displayOptions['displayKPIEvaluateBtn'])
             {
                html.push("<br />\r\n\n\n\n");   
                html.push("<span style='padding:5px;'></span>");
                html.push("<span style='float:left;' class='"+kpiSettings['evaluationstatuses'][index]['self']['status']+"'></span>");
                html.push("<span style='color:"+(self._isNextEvaluation(index, "self") ? "#000000" : "#AAAAAA")+"'>");
                    html.push("Self Evaluation");
                html.push("</span>");
                
                html.push("<br />\r\n\n\n\n");
                  html.push("<span style='float:left;' class='"+kpiSettings['evaluationstatuses'][index]['manager']['status']+"'></span>");
                html.push("<span style='color:"+(self._isNextEvaluation(index, "manager") ? "" : "#AAAAAA")+"'>");
                  html.push("Manager Evaluation");
                html.push("</span>");
                html.push("<br />\r\n\n\n\n");
                html.push("<span style='float:left;' class='"+kpiSettings['evaluationstatuses'][index]['joint']['status']+"'></span>");
                html.push("<span style='color:"+(self._isNextEvaluation(index, "joint") ? "" : "#AAAAAA")+"'>");
                  html.push("Joint Evaluation");
                html.push("</span>");
                html.push("<br />\r\n\n\n\n");
                  html.push("<span style='float:left;' class='"+kpiSettings['evaluationstatuses'][index]['review']['status']+"'></span>");
                html.push("<span style='color:"+(self._isNextEvaluation(index, "review") ? "" : "#AAAAAA")+"'>");                  
                  html.push("Evaluation Review");
                html.push("</span>");                                                
             }               
             if(displayOptions['displayAssuranceBtn'])
             {
                html.push("<input type='button' name='"+self.options.kpaid+"_assurance_"+index+"' id='"+self.options.kpaid+"_assurance_"+index+"' value='Assurance KPI' />");          
                
                  $("#"+self.options.kpaid+"_assurance_"+index).live("click", function(){
                       document.location.href = "main.php?controller=kpi&action=assurancekpi&id="+index+"&kpaid="+self.options.kpaid+"&componentid="+self.options.componentid+"&evaluationyear="+self.options.evaluationyear+"&parent="+$("#parent").val()+"&folder="+self.options.section
                    return false;
                  });                    
             }  
           html.push("</td>");
         }                        

         if(self._isEvaluationAtKpi() && displayOptions['displayKPIEvaluateBtn'])
         {
             var evaluations = self.options.evaluations;
             if(displayOptions['selfEvaluationRating'])
             {
                html.push("<td>"+self._kpiEvaluation(index, 'self', 'rating')+"</td>");
             }
             if(displayOptions['selfEvaluationComment'])
             {
                html.push("<td>"+self._kpiEvaluation(index, 'self', 'comment')+"</td>");
             }
             if(displayOptions['managerEvaluationRating'])
             {
                html.push("<td>"+self._kpiEvaluation(index, 'manager', 'rating')+"</td>");
             }      
             if(displayOptions['managerEvaluationComment'])
             {
                html.push("<td>"+self._kpiEvaluation(index, 'manager', 'comment')+"</td>");
             } 
             if(displayOptions['jointEvaluationRating'])
             {
                html.push("<td>"+self._kpiEvaluation(index, 'joint', 'rating')+"</td>");
             }      
             if(displayOptions['jointEvaluationComment'])
             {
                html.push("<td>"+self._kpiEvaluation(index, 'joint', 'comment')+"</td>");
             }   
             if(displayOptions['evaluationReviewRating'])
             {
                html.push("<td>"+self._kpiEvaluation(index, 'review', 'rating')+"</td>");
             }      
             if(displayOptions['evaluationReviewComment'])
             {
                html.push("<td>"+self._kpiEvaluation(index, 'review', 'comment')+"</td>");             
             }                
         } else {
             if(self.options.page == "evaluate")
             {
                html.push("<td colspan='8' style='color:#AAAAAA'>");
                 html.push("<b>"+displayOptions['kpiEvaluationText']+"</b>");
                html.push("</td>");                
             }   
         }
         $("#"+self.options.kpaid+"_childtr_"+index).append( html.join(' ') ); 
         if(canAddAction === false)
         {
            $("#add_action_"+index).attr("disabled", "disabled");
         }
       });         
    } ,
    
    _editKpiEvaluate        : function(componentid, kpiId)
    {
        var self = this;
        if($("#kpieditevaluation_"+kpiId).length > 0)
        {
           $("#kpieditevaluation_"+kpiId).remove();
        }    
        var evaluations = self.options.evaluations[kpiId][self.options.userNext];
        
          $("<div />",{id:"kpieditevaluation_"+kpiId}).append($("<table />")
            .append($("<tr />")
               .append($("<th />",{html:"Rating :"}))
               .append($("<td />")
                  .append($("<select />",{id:"kpirating", name:"kpirating"}))
               )
            )
            .append($("<tr />")
               .append($("<th />",{html:"Comment :"}))
               .append($("<td />")
                  .append($("<textarea />",{id:"kpicomment", name:"kpicomment", rows:"7", cols:"50", text:evaluations['comment']}))
               )
            )
            .append($("<tr />")
               .append($("<th />",{html:"Recommendation :"}))
               .append($("<td />")
                  .append($("<textarea />",{id:"kpirecommendation", name:"kpirecommendation", rows:"7", cols:"50", text:evaluations['recommendation']}))
               )
            ) 
            .append($("<tr />")
               .append($("<td />",{colspan:2})
                  .append($("<p />").css({padding:"5px"})
                    .append($("<span />",{id:"message_"+kpiId}))
                  )
               )
            )         
          ).dialog({
              autoOpen    : true,
              title       : "Edit KPI Evaluation",
              position    : "top",
              width       : 500,
              modal       : true,
              buttons     : {
                               "Save Changes"       : function()
                               {
                                  if($("#kpirating").val() == "")
                                  {
                                    $("#message_"+kpiId).html("Please select the rating");
                                    return false;
                                  } else if($("#kpicomment").val() == "") {
                                    $("#message_"+kpiId).html("Please enter the comment for this evaluation");
                                    return false
                                  } else {
                                      if(self.options.jointByManager)
                                      {
                                         self._showConfirmJointEvaluationDialog(kpiId);
                                      } else {
                                         self._updateKpiEvaluation(kpiId, evaluations['eval_id']);
                                      }
                                  }
                               } ,
                               "Cancel"     : function()
                               {
                                  $("#kpieditevaluation_"+kpiId).dialog("destroy");
                                  $("#kpieditevaluation_"+kpiId).remove();
                               }
                            },
              open        : function(event, ui)
              {
			     var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			     var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
			     var saveButton = buttons[0];
			     $(saveButton).css({"color":"#090"});  
			              
                    var ratingScales = $('body').data('ratingScales');
                    if($.isEmptyObject(ratingScales))
                    {
                         $.getJSON("main.php?controller=ratingscale&action=getAll",{status:1} ,function(ratingScales){
                            if(!$.isEmptyObject(ratingScales))
                            {
                               $('body').data('ratingScales', ratingScales);
                               $("#kpirating").empty();
                               $("#kpirating").append($("<option />",{text:"--please select--", value:""}));
                               $.each(ratingScales, function(index, ratingScale){
                                 if(ratingScale.id == evaluations['ratingid'])
                                 {
                                  $("#kpirating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id, selected:"selected"}));
                                  } else {
                                  $("#kpirating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));                                  
                                  }
                               });
                            }
                         });
                    } else {
                          $("#kpirating").empty();
                          $("#kpirating").append($("<option />",{text:"--please select--", value:""}));
                          $.each(ratingScales, function(index, ratingScale) {
                             if(ratingScale.id == evaluations['ratingid'])
                             {
                              $("#kpirating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id, selected:"selected"}));
                              } else {
                              $("#kpirating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));                      
                              }                                
                          });                    
                    } 
                    $(this).css({height:'auto'})			      
              } ,
              close       : function(event, ui)
              {
                   $("#kpieditevaluation_"+kpiId).dialog("destroy");
                   $("#kpieditevaluation_"+kpiId).remove();          
              }   
          })
    },
    
    _evaluateKpi            : function(kpiId)
    {
      var self = this;
      if($("#kpievaluation_"+kpiId).length > 0)
      {
         $("#kpievaluation_"+kpiId).remove();
      }    
      
      $("<div />",{id:"kpievaluation_"+kpiId}).append($("<table />")
        .append($("<tr />")
           .append($("<th />",{html:"Rating :"}))
           .append($("<td />")
              .append($("<select />",{id:"kpirating", name:"kpirating"}))
           )
        )
        .append($("<tr />")
           .append($("<th />",{html:"Comment :"}))
           .append($("<td />")
              .append($("<textarea />",{id:"kpicomment", name:"kpicomment", rows:"7", cols:"50"}))
           )
        )
        .append($("<tr />")
           .append($("<th />",{html:"Recommendation :"}))
           .append($("<td />")
              .append($("<textarea />",{id:"kpirecommendation", name:"kpirecommendation", rows:"7", cols:"50"}))
           )
        ) 
        .append($("<tr />")
           .append($("<td />",{colspan:2})
              .append($("<p />").css({padding:"5px"})
                .append($("<span />",{id:"message_"+kpiId}))
              )
           )
        )         
      ).dialog({
          autoOpen    : true,
          title       : "Evaluation",
          position    : "top",
          width       : 500,
          modal       : true,
          buttons     : {
                           "Save"       : function()
                           {
                              if($("#kpirating").val() == "")
                              {
                                $("#message_"+kpiId).addClass("ui-state-error").css({padding:'5px'}).html("Please select the rating");
                                return false;
                              } else if($("#kpicomment").val() == "") {
                                $("#message_"+kpiId).addClass("ui-state-error").css({padding:'5px'}).html("Please enter the comment for this evaluation");
                                return false
                              } else {
                                    if(self.options.jointByManager)
                                    {
                                       self._showConfirmJointEvaluationDialog(kpiId);
                                    } else {
                                        self._saveKpiEvaluation(kpiId);
                                    }
                              }
                           } ,
                           "Cancel"     : function()
                           {
                              $("#kpievaluation_"+kpiId).dialog("destroy");
                              $("#kpievaluation_"+kpiId).remove();
                           }
                        },
          open        : function(event, ui)
          {
			 var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			 var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
			 var saveButton = buttons[0];
			 $(saveButton).css({"color":"#090"});  
			          
                var ratingScales = $('body').data('ratingScales');
                if($.isEmptyObject(ratingScales))
                {
                     $.getJSON("main.php?controller=ratingscale&action=getAll",{status:1} ,function(ratingScales){
                        if(!$.isEmptyObject(ratingScales))
                        {
                           $('body').data('ratingScales', ratingScales);
                           $("#kpirating").empty();
                           $("#kpirating").append($("<option />",{text:"--please select--", value:""}));
                           $.each(ratingScales, function(index, ratingScale){
                              $("#kpirating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));
                           });
                        }
                     });
                } else {
                      $("#kpirating").empty();
                      $("#kpirating").append($("<option />",{text:"--please select--", value:""}));
                      $.each(ratingScales, function(index, ratingScale) {
                         $("#kpirating").append($("<option />",{text:ratingScale.definition+" ("+ratingScale.ratingto+")", value:ratingScale.id}));
                      });                    
                } 
                $(this).css({height:'auto'})			      
          } ,
          close       : function(event, ui)
          {
               $("#kpievaluation_"+kpiId).dialog("destroy");
               $("#kpievaluation_"+kpiId).remove();          
          }   
      })
    } ,

    _showConfirmJointEvaluationDialog   : function(kpiId)
    {
        var self = this;
        if($("#confirmjointdialog").length > 0)
        {      
          $("#confirmjointdialog").remove();
        }
        var html = [];
        if(self.options.userlogged['tkid'] == self.options.userdata['tkid'])
        {
           html.push("<div>")
            html.push("I, "+self.options.userdata['user']+", reporting to "+self.options.userdata['managername']+" hereby:");
            html.push("<ul>");
             html.push("<li>");
               html.push("Confirm that I met with "+self.options.userdata['managername']+" to jointly evaluate my performance on this component of the Performance Matrix for the evaluation period "+self.options.userlogged['evaluationperiod']+" for the evaluation year "+self.options.userdata['start']+" to "+self.options.userdata['end']+";");
             html.push("</li>");
             html.push("<li>");
               html.push("Declare that we agreed on this joint rating;");
             html.push("</li>");             
             html.push("<li>");
               html.push("Declare that I have the required authority to confirm this joint evaluation rating.");
             html.push("</li>");            
            html.push("</ul>");
           html.push("</div>");
        } else {
           html.push("<div>")
            html.push("I, "+self.options.userdata['managername']+", manager of "+self.options.userdata['user']+" hereby:");
            html.push("<ul>");
             html.push("<li>");
               html.push("Confirm that I met with "+self.options.userdata['user']+" to jointly evaluate the performance on this component of the Performance Matrix for the evaluation period "+self.options.userlogged['evaluationperiod']+" for the evaluation year "+self.options.userdata['start']+" "+self.options.userdata['end']+";");
             html.push("</li>");
             html.push("<li>");
               html.push("Declare that we agreed on this joint rating;");
             html.push("</li>");             
             html.push("<li>");
               html.push("Declare that I have the required authority to confirm this joint evaluation rating.");
             html.push("</li>");            
            html.push("</ul>");
           html.push("</div>");
        }
        $("<div />", {id:"confirmjointdialog"}).append( html.join(' ') )
        .append($("<div />",{id:"message"}))
        .dialog({
                 autoOpen  : true,
                 modal     : true,
                 position  : "top",
                 width     : 350,
                 title     : "Joint Evaluation of Performance Matrix",
                 buttons   : {
                               "Ok"     : function()
                               {
                                   self._saveKpiEvaluation(kpiId);
                                   $("#confirmjointdialog").dialog("destroy");
                                   $("#confirmjointdialog").remove();   
                               }
                 } , 
                 close      : function(event, ui)
                 {
                     $("#confirmjointdialog").dialog("destroy");    
                     $("#confirmjointdialog").remove();                     
                 } ,
                 open       : function(event, ui)
                 {
		            var dialog = $(event.target).parents(".ui-dialog.ui-widget");
		            var buttons = dialog.find(".ui-dialog-buttonpane").find("button");	
		            var saveButton = buttons[0];
		            $(saveButton).css({"color":"#090"});  
		            $(this).css({height:'auto'})                  
                 }
        });
    } ,
    
    _saveKpiEvaluation      : function(kpiId)
    {
        var self = this;
        $("#message_"+kpiId).addClass("ui-state-info").css({padding:"5px"}).html("saving ...<img src='public/images/loadingA32.gif' />")
       $.post("main.php?controller=kpi&action=save_evaluation",{
            rating          : $("#kpirating").val(),
            comment         : $("#kpicomment").val(),
            recommendation  : $("#kpirecommendation").val(),
            componentid     : self.options.componentid,
            reference       : kpiId,
            user            : self.options.user,
            evaluationyear  : self.options.evaluationyear,
            next_evaluation : self.options.kpiSettings['next_evaluation'][kpiId]
       }, function(response) {
            if(response.error)
            {
               $("#message").addClass("ui-state-error").css({padding:'5px'}).html(response.text)
            } else {
               jsDisplayResult("ok", "ok", response.text);
               $("#kpievaluation_"+kpiId).dialog("destroy");
               $("#kpievaluation_"+kpiId).remove();
            document.location.href = "main.php?controller=component&action=evaluate&parent=2&folder=manage&user="+self.options.user+"&component="+self.options.componentid;             
            }
       },"json");
    } ,
    
    _updateKpiEvaluation      : function(kpiId, eval_id)
    {
        var self = this;
        $("#message_"+kpiId).addClass("ui-state-info").css({padding:"5px"}).html("updating ...<img src='public/images/loadingA32.gif' />")
       $.post("main.php?controller=kpi&action=save_evaluation",{
            rating          : $("#kpirating").val(),
            comment         : $("#kpicomment").val(),
            recommendation  : $("#kpirecommendation").val(),
            componentid     : self.options.componentid,
            reference       : kpiId,
            user            : self.options.user,
            evaluationyear  : self.options.evaluationyear,
            next_evaluation : self.options.userNext,
            id              : eval_id
       }, function(response) {
            if(response.error)
            {
               $("#message").addClass("ui-state-error").css({padding:'5px'}).html(response.text)
            } else {
               jsDisplayResult("ok", "ok", response.text);
               $("#kpieditevaluation_"+kpiId).dialog("destroy");
               $("#kpieditevaluation_"+kpiId).remove();
               document.location.href = "main.php?controller=component&action=evaluate&parent=2&folder=manage&user="+self.options.user+"&component="+self.options.componentid;             
            }
       },"json");
    } ,
    _kpiEvaluation         : function(kpiId, evaluationType, type)
    {
        var self  = this;
        var evaluations = self.options.kpiSettings;
        if(evaluations['evaluate_on'] == "kpi")
        {
          if(evaluations['evaluations'].hasOwnProperty(kpiId))
          {                    
             if(evaluations['evaluations'][kpiId].hasOwnProperty(evaluationType))
             {
               if(evaluations['evaluations'][kpiId][evaluationType].hasOwnProperty(type))
               {                 
                  if(evaluations['evaluations'][kpiId][evaluationType][type] == 0 || evaluations['evaluations'][kpiId][evaluationType][type] == "")
                  {
                    return "";
                  } else {
                    return evaluations['evaluations'][kpiId][evaluationType][type];
                  }
               }
             }
          }
        }
        return "";
    } ,
    _isEvaluationAtKpi        : function()
    {
       var self = this;
       if(self.options.kpiSettings.hasOwnProperty("evaluate_on"))
       {
         if(self.options.kpiSettings['evaluate_on'] === "kpi")
         {
             return true;
         } else {
            return false;
         }
       }       
       return false;
    } ,
    
    _checkEvaluated           : function(kpiId, evaluationType)
    {
      var self = this;
      if(self.options.kpiSettings['evaluations'].hasOwnProperty(kpiId))
      {    
         if(self.options.kpiSettings['evaluations'].hasOwnProperty(evaluationType))
         {
            return true;
         }
      } 
      return false;
    } ,
    
    _isNextEvaluation           : function(index, evaluationType)
    {
      var self = this;
      if(!$.isEmptyObject(self.options.kpiNextEvaluation))
      {
          if(self.options.kpiNextEvaluation[index] == evaluationType)
          {
            return true;
          } else {
            return false;
          }
      }
      return false;
    } ,
    
    _kpiEvaluationStatus        : function(kpiId, evaluationType, evaluationStatuses)
    {
      var self = this; 
      if(evaluationStatuses !== undefined)
      {
           if(evaluationStatuses.hasOwnProperty(kpiId))
           {
             if(evaluationStatuses[kpiId].hasOwnProperty(evaluationType))
             {
               return evaluationStatuses[kpiId][evaluationType]['status'];
             } else {
               return "";
             }        
           } else {
               return "";
           }          
      } else {
         return "";
      }
    } ,
    
    _canEditKpiEvaluation     : function(kpiId)
    {
       var self = this;
       if(self._isEvaluationAtKpi() && self.options.page == "evaluate")
       {

          if(self.options.userNext == "self")
          {
            if(!self._canEvaluateKpi(kpiId))
            {
               //self evaluation is done by the userlogged in and is the user selected    
               if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
               {
                  return true;
               } else {
                 return false;
               }               
            }
          } 
          if(self.options.userNext == "manager")
          {
            if(!self._canEvaluateKpi(kpiId))
            {
                //the manager evaluation part is only done by the manager of the user logged, 
                //so the user selected(userdata)'s manager should be the userlogged(userlogged) or when the manager 
                //is the manager of theirselves S
                if((self.options.userdata['managerid'] == self.options.userlogged['tkid']) 
                || (self.options.userlogged['managerid'] == "S"))
                {
                  return true;
                } else {
                  return false;
                }           
            }
          }
          
          if(self.options.userNext == "joint")
          {
            if(!self._canEvaluateKpi(kpiId))
            {
                //joint evaluation is done either by the userlogged or can be done by the manage of the user selected
                if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
                {
                   return true;
                } else if((self.options.userdata['managerid'] == self.options.userlogged['tkid'])){
                    return true;
                } else {
                    return false;
                }             
            }            
          }
          
          if(self.options.userNext == "review")
          {
            if(!self._canEvaluateKpi(kpiId))
            {
               if(self.options.inCommittee == true)
               {
                 return true;
               } else {
                 return false;
               }             
            }                              
          }                
       }
       return false; 
    } ,
    
    _canEvaluateKpi           : function(kpiId)
    {
      var self = this;
      if(self._isEvaluationAtKpi() && self.options.page == "evaluate")
      {
          if(self.options.kpiSettings['next_evaluation'].hasOwnProperty(kpiId))
          {
             if(self.options.kpiSettings['next_evaluation'][kpiId] == "")
             {
               return false;   
             } else {
             
               if(self.options.kpiSettings['next_evaluation'][kpiId] == "manager")
               {
                  if(self.options.userNext == "manager")
                  {
                     //the manager evaluation part is only done by the manager of the user logged, 
                     //so the user selected(userdata)'s manager should be the userlogged(userlogged) or when the manager 
                     //is the manager of theirselves S
                     if((self.options.userdata['managerid'] == self.options.userlogged['tkid']) 
                     || (self.options.userlogged['managerid'] == "S"))
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                    return false;
                  }
               }
               
               if(self.options.kpiSettings['next_evaluation'][kpiId] == "self")
               {
                  //console.log("COming here now ");
                  //console.log( self.options.userNext );
                  if(self.options.userNext == "self")
                  {
                    //self evaluation is done by the userlogged in and is the user selected    
                     if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                     return false;
                  }
               }  
               
               if(self.options.kpiSettings['next_evaluation'][kpiId] == "joint")
               {
                  if(self.options.userNext == "joint")
                  {
                      //joint evaluation is done either by the userlogged or can be done by the manage of the user selected
                     if((self.options.userdata['tkid'] == self.options.userlogged['tkid']))
                     {
                        self.options.jointByManager = true;
                        return true;
                     } else if((self.options.userdata['managerid'] == self.options.userlogged['tkid'])){
                        self.options.jointByManager = true;
                        return true;
                     } else {
                        return false;
                     }
                  } else {
                    return false;
                  }
                  /*if((self.options.userdata['tkid'] == self.options.userlogged['tkid']) 
                     || (self.options.userdata['managerid'] == self.options.userlogged['tkid'])
                   
                  )
                  {
                     return true;
                  } else {
                     return false;
                  }*/
               }                 
               if(self.options.kpiSettings['next_evaluation'][kpiId] == "review")
               {
                  if(self.options.userNext == "review")
                  {
                     //evaluation review is done by any of the users who are in evaluation committee
                     if(self.options.inCommittee == true)
                     {
                       return true;
                     } else {
                       return false;
                     }
                  } else {
                    return false;
                  }
               }   
               
               return true;
             }
          }  
      } else {
          return false;
      }

    }


});
