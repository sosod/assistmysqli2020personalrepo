$(function(){
    Evaluate.getUserComponents();
})


var Evaluate   = {

     getUserComponents        : function(user)
     {
          $("#tabs").html("");
          var userid = "";
          if(user == undefined)
          {
               userid = "";
          } else {
               userid = user
          }
          
          $.getJSON("main.php?controller=usercomponent&action=getAll", {
               user     : userid,
               page     : $("#page").val(),
               section  : $("#section").val()                             
          }, function(componentData) {
		          if(componentData.error)
		          {
			         $("#tabs").append($("<p />").addClass("ui-state-highlight").css({"padding":"5px"})
				              .append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))
				              .append($("<span />",{html:componentData.text}).css({"margin-left":"3px"}))
				         )
				         $("#activatecomponent").hide();
		          } else {
		               
			         if($.isEmptyObject(componentData.components))
			         {
				         $("#tabs").append($("<p />").addClass("ui-state-highlight").css({"padding":"5px"})
				           .append($("<span />").addClass("ui-icon").addClass("ui-icon-info").css({"float":"left"}))
				           .append($("<span />",{html:"There are no components setup for the selected evaluation year"}).css({"margin-left":"3px"}))
				         )
				         $("#activatecomponent").hide();
			         } else {
			             $("#tabs").removeClass("ui-state").removeClass("ui-state-highlight");
				         var li = $("<ul />",{id:"tablist"})
				         $.each(componentData.components, function( index, component){
					         li.append($("<li />")
					           .append($("<a />",{href:"main.php?controller=component&action=evaluationcomponent&id="+component.id+"&user="+user, html:component.name+" (<b>"+component.percentage+"</b>)"}))
					         )				
				         });
				         if(componentData.totalWeight < 100)
				         {
					         li.append($("<li />")
					           .append($("<span />",{html:"Sum of components not equalt to 100%"}).addClass("ui-state-error").css({padding:"5px", "margin-top":"0px", position:"absolute"}))
					         )								      
         				         $("#activatecomponent").attr("disabled", "disabled");
				         }
				         $("#tabs").append(li).tabs({ajaxOptions: { async: false }});				
			         }	
			    }     
          });
     } 
     
     
}
