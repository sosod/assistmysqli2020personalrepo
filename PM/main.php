<?php
//start session
if(ini_get('session.auto_start') || (strtolower(ini_get('session.auto_start')) == false))
{
     if(!session_id())  session_regenerate_id();
     @session_start();
}


require_once("inc_ignite.php");
define('ROOT', str_replace('\\', '/', dirname(__FILE__)));
define('APPLICATION_PATH', ROOT.'/application');
define('RESOURCES', APPLICATION_PATH.'/public');
define('APPLICATION_VIEW', APPLICATION_PATH.'/views');
define('MODELS', APPLICATION_PATH.'/models');
define('TEMPLATES', APPLICATION_VIEW.'/templates');
define('DEFAULT_CONTROLLER', 'component');
define('DEFAULT_ACTION', 'view');

set_include_path(ROOT . PATH_SEPARATOR . APPLICATION_PATH);

require_once(APPLICATION_PATH."/functions.php");
function __autoload($classname)
{
	try
	{
		$filename   = strtolower($classname).".php";
		//echo "Applucation path is ".APPLICATION_PATH."/models/".strtolower($classname)."/"." looking for ".$filename." \r\n\n<br />";
		loadFile(APPLICATION_PATH."/models", $filename);	
		loadFile(APPLICATION_PATH."/controllers", $filename);
		loadFile(APPLICATION_PATH."/libraries", $filename);	
		loadFile("../library/dbconnect",$filename);	
	} catch(Exception $e) {
		echo "Exception occured ".$e->getMessage();
	}     
}
require_once(APPLICATION_PATH."/libraries/application.php");
Application::getInstance()->execute(new Request());
//add user setting to the session array
?>
