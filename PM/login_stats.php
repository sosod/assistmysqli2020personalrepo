<?php
/* requires $sdb as object of ASSIST_DB class => created in main/stats.php */


$now    = strtotime(date("d F Y"));
$future = strtotime(date("d F Y",($today+($next_due*7*24*3600))));
$sql = "SELECT 
	   A.deadline AS taskdeadline,
	   A.status,
	   count(A.id) as c 
	   FROM assist_".$cmpcode."_".$modref."_action A 
       INNER JOIN assist_".$cmpcode."_".$modref."_kpi K ON K.id = A.kpiid 
       INNER JOIN assist_".$cmpcode."_".$modref."_kpa KA ON KA.id = K.kpaid 
       INNER JOIN assist_".$cmpcode."_".$modref."_user_component UC ON UC.componentid = KA.componentid 		  
       WHERE  A.owner = '".$tkid."' 
       AND STR_TO_DATE(A.deadline,'%d-%M-%Y') <= ADDDATE(now(), ".$next_due.") 
	   AND A.status <> 3 
	   AND A.actionstatus & 2 <> 2 
	   AND UC.userid = '".$tkid."' AND UC.status & 8 = 8	  
	   GROUP BY A.deadline";
$rows = $sdb->mysql_fetch_all($sql);
foreach($rows as $row) {
	$d = strtotime($row['taskdeadline']);
	if($d < $now) {
		$count['past']+=$row['c'];
	} elseif($d==$now) {
		$count['present']+=$row['c'];
	} else {
		$count['future']+=$row['c'];
	}
}

?>
