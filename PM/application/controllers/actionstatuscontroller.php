<?php
class ActionstatusController extends Controller
{

	function __construct()
	{
		parent::__construct();	
	}
	
	function setup()
	{
        $this->template->scripts = array("actionstatus.js", "jscolor.js");
        $this->template->pagetitle = "Action Status";	
	   $this->template->view("setup/actionstatus");
	}

	function getAll($options = array())
	{
        $actionStatuses = Actionstatus::getActionStatuses($options);
        return $actionStatuses;
	}
	
	function saveActionStatus( $data )
	{
		$id  = Actionstatus::saveData( $data['data'] );
		return Message::save( $id, " action status #".$id);
	}
	
	function updateActionStatus($data)
	{
        $statusObj = new Actionstatus();
        $res       = $statusObj -> update_action_status($data);      
	   return Message::update($res, " action status #".$data['id']);
	}
	
}

