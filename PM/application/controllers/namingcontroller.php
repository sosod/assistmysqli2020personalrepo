<?php
class NamingController extends Controller
{
     public function __construct()
     {
        parent::__construct();
     }
     
     function setup()
     {
       $this -> template -> scripts   = array("naming.js");
       $this -> template -> pagetitle = "Header Naming";     
       $this -> template -> view("setup/naming"); 
     }
     
     function columns()
     {
       $this->template->scripts   = array("columns.js");
       $this->template->pagetitle = "Columns Sorting";     
       $this->template->view("setup/columns"); 
     }     
     
     function getAll($options = array())
     {
       $optionSql = "";
       if(isset($options['orderby']))
       {
         $optionSql = "  ORDER BY position ";
       }
       $results = Naming::findAll($optionSql);
       return $results;   
     }
     
     function updateNaming($data)
     {
        $res = Naming::updateWhere($data, array("id" => $data['id']));
        return Message::update($res, "header name ref #".$data['id']);
     }
     
	function updateColumns( $data )
	{	
		$position = array();
		$manage   = array();
		$new 	  = array();
		if(!empty($data['data']) )
		{
			foreach( $data['data'] as $index => $columns)
			{
				if( $columns['name'] == "position")
				{
					$position[]  = $columns['value'];
				}
				
				if( strstr($columns['name'], "manage") )
				{
					$manage[] = $columns['value'];
				}

				if( strstr($columns['name'], "new"))
				{
					$new[]  = $columns['value'];
				}
			}			
		}
		$res = 0;
		foreach( $position as $pos => $id )
		{
			$updatedata = array();
			
			if( in_array($id, $manage))
			{
				$updatedata = array("status" => 4, "position" => $pos + 1);
			} else {
				$updatedata = array("status" => 0, "position" => $pos + 1);
			}

			if( in_array($id, $new))
			{
				$updatedata = array("status" => $updatedata['status'] + 8, "position" => $pos + 1);
			} else {
				$updatedata = array("status" => $updatedata['status'] + 0, "position" => $pos + 1);
			}

			$res += Naming::updateWhere($updatedata, array("id" => $id));
		}		
		return Message::update( (!empty($res) ? 1 : 0) , "colums");		
	}     
}
?>
