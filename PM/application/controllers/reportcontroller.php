<?php
class ReportController extends Controller
{
    
    function __construct()
    {
        parent::__construct();
    }
    
    function view()
    {
       $this -> template -> header = Naming::getHeaderList();
       $this->template->scripts = array("generate_report.js");
       $this->template->pagetitle = "Generate KPA Report";
	  $this->template->view("report/generate");
    }    
        
    function generate($options = array())
    {
      $this -> template -> header = Naming::getHeaderList();
      $this->template->scripts = array("generate_report.js");
      $this->template->pagetitle = "Generate KPA Report";	    
      $this -> template -> view("report/generate");
    }
    
    function generate_action()
    {
      $this -> template -> header = Naming::getHeaderList();
      $this->template->scripts = array("generate_action.js");
      $this->template->pagetitle = "Generate Action Report";	    
      $this -> template -> view("report/generate_action");
    }
    
    function generate_kpi()
    {
      $this -> template -> header = Naming::getHeaderList();
      $this -> template -> scripts = array("generate_kpi.js");
      $this -> template -> pagetitle = "Generate Kpi Report";	    
      $this -> template -> view("report/generate_kpi");
    }
    
    function quick($options = array())
    {
      $this -> template -> scripts = array("generate_kpi.js");
      $this -> template -> pagetitle = "Quick Report";	        
      $this -> template -> view("report/quick");
    }
    
    function fixed($options = array())
    {
      $this -> template -> scripts = array("fixed.js");
      $this -> template -> pagetitle = "Fixed Reports";	         
      $this -> template -> view("report/fixed");
    }          
    
    function creation_status()
    {
      $this -> template -> layout    = false;
      $this -> template -> scripts   = array("creation_status.js");
      $this -> template -> pagetitle = "Perfomance Matrix Creation Status Report";	         
      $this -> template -> view('report/creation_status');
    }  
    
    function evaluation_status()
    {
      $this -> template -> layout    = false;
      $this -> template -> scripts   = array("evaluation_status.js");
      $this -> template -> pagetitle = "Perfomance Matrix Evaluation Status";	         
      $this -> template -> view('report/evaluation_status');
    }  

    function department_evaluation_status()
    {
      $this -> template -> layout    = false;
      $this -> template -> scripts   = array("evaluation_status.js");
      $this -> template -> pagetitle = "Perfomance Matrix Evaluation Status";          
      $this -> template -> view('report/department_evaluation_status');
    }   

    function evaluation_score()
    {
      $this -> template -> layout    = false; 
      $this -> template -> scripts   = array("evaluation_score.js");
      $this -> template -> pagetitle = "Perfomanace Matrix Evaluation Score";	         
      $this -> template -> view('report/evaluation_score');
    }          
    
    function performance_matrix()
    {
      $this -> template -> layout    = false; 
      $this -> template -> scripts   = array("evaluation_score.js");
      $this -> template -> pagetitle = "Perfomanace Matrix Evaluation Score";	         
      $this -> template -> view('report/performance_matrix');    
    }
    
    function evaluation_matrix()
    {
      $this -> template -> layout    = false; 
      $this -> template -> scripts   = array("evaluation_score.js");
      $this -> template -> pagetitle = "Perfomanace Matrix Evaluation Score";	         
      $this -> template -> view('report/evaluation_matrix');    
    }
    
    function recommendation_matrix()
    {
      $this -> template -> layout    = false; 
      $this -> template -> scripts   = array("evaluation_score.js");
      $this -> template -> pagetitle = "Perfomanace Matrix Evaluation Score";	         
      $this -> template -> view('report/recommendation_matrix');    
    }    

    function fish_bowl()
    {
      $this -> template -> layout    = false; 
      $this -> template -> scripts   = array("evaluation_score.js");
      $this -> template -> pagetitle = "Perfomanace Matrix Evaluation Score";	         
      $this -> template -> view('report/fish_bowl');    
    }
    
    function bell_curve($option = array())
    {
      $this -> template -> layout    = false; 
      $this -> template -> scripts   = array("evaluation_score.js");
      $this -> template -> pagetitle = "Perfomanace Matrix Evaluation Score";	         
      $this -> template -> view('report/bell_curve');    
    }    
}
?>
