<?php
class PerformancecategoriesController extends Controller
{
     
     function __construct()
     {
        parent::__construct();
     }     
     
     function setup()
     {
        $this->template->scripts = array("performancecategories.js");
        $this->template->pagetitle = "Performance Categories";	
        $this->template->view("setup/performancecategories");
     }     
     
     function getAll($options=array())
     {
         $categories = Performancecategories::getPerformanceCategories($options);
         return $categories;
     }
     
	function savePerformancecategories( $data )
	{
		$id = Performancecategories::saveData($data['data']);
		return Message::save( $id, "performance categories #".$id  );
	}
	
	function updatePerformancecategories($data)
	{	
		$res = Performancecategories::updateWhere($data['data'], array("id" => $data['id']), new SetupAuditLog("Performance Category"));
		return Message::update($res, "performance categories #".$data['id']);
	}
	
	function assignJobLevels( $data )
	{
		$categoryId = str_replace("joblevels_", "", $data['catId']);
		if(empty($data['joblevels']))
		{
			return array("text" => "Please assign at least one job level for this category", "error" => true );
		} else {
			$joblevels = implode(",", $data['joblevels'] );	
			$data = array("category_id" => $categoryId, "joblevel" => $joblevels ); 
			$res = Performancecategories::saveJobLevels($data );
			return Message::save( $res, "job levels ");
		}
	}	
     
}
?>
