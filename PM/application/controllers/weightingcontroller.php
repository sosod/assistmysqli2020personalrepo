<?php
class WeightingController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getAll( $options = "")
	{
		$sqlOption = $this->sqlOption($options);
		$results   = $this->Model->fetchAll( $sqlOption );
		return $results;
	}
	
}
