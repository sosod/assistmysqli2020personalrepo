<?php
class KpiMappingController extends Controller
{
    private $modules = array();
    
    function __construct()
    {
        parent::__construct();
    }
    
    function getModules()
    {
        $taskmodules = Kpimapping::taskModules(); 
        $modules =  array('Risk' => 'rsk', 'Query' => 'qry', 'Contract SLA' => 'sla', 'Compliance' => 'compl');
        if(!empty($taskmodules))
        {
          foreach($taskmodules as $index => $mod)
          {
            $modules[$mod['modtext']]  = strtolower($mod['modref']);  
          }
        }
        return $modules;
    }
    
    function getModuleTypes($options = array())
    {
        switch($options['module'])
        {
           case "rsk":
            return $this->getRiskTypes();
           break;
           case "qry":
            return $this->getQueryTypes();
           break;
           case "sla":
            return $this->getContractSLA();
           break;
           case "compl":
            return $this->getLegislations();
           break;
           case "task":
            return $this->getModuleData($options['module']);
           break; 
           default:
             return $this->getModuleData($options['module']);
            break;          
        }
    }
    
    
    //for risk get the risk categories
    function getSubModuleTypes($options = array())
    {
        $cats = array();
        if(isset($options['refid']) && !empty($options['refid']))
        {
          if($options['type'] == "qry")
          {
               $rkSql  = "";
               $cats["title"] = "Query Category";
               if($options['refid'] != "all")
               {
                    $rkSql = " AND type_id = '".intval($options['refid'])."'";
                    $categories = KpiMapping::getQueryCategories($rkSql);
                    $cats["categories"] = $categories;                  
               }
          } else if($options['type'] == "risk"){
               $rkSql = "";
               $cats["title"] = "Risk Category";
               if($options['refid'] != "all")
               {
                 $rkSql = " AND type_id = '".intval($options['refid'])."'";
                 $categories = KpiMapping::getRiskCategories($rkSql);
                 $cats["categories"] = $categories;                  
               }
          } 
        }
        return $cats;
    }
    
    
    function getRiskTypes()
    {
         $rkSql = "";
        //$rkSql = " AND RA.action_owner = '".$_SESSION['tid']."'";
        $risktypes = KpiMapping::getRiskTypes($rkSql);
        return array("title" => "Risk Type", "types" => $risktypes);
    }
   
    function getQueryTypes()
    {
        $qrySql = "";
        //$qrySql = " AND QA.action_owner = '".$_SESSION['tid']."'";
        $querytypes = KpiMapping::getQueryTypes($qrySql);
        return array("title" => "Query Type", "types" => $querytypes);
    }   
    
    function getLegislations()
    {
        $legislations = KpiMapping::getLegislations();
        return array("title" => "Legislations", "types" => $legislations);    
    } 
    
    function getContractSLA()
    {
        $contracts = KpiMapping::getContractsTypes();
        return array("title" => "Contracts Types ", "types" => $contracts);        
    }

    function getModuleData($modref)
    {
        $topics = KpiMapping::getModuleData($modref);
        return array("title" => "Topics ", "types" => $topics);             
    }

    function _sortModuleData($data, $mappingData, $modref)
    {
        $moduleData = array();
        foreach($data as $dIdnex => $dVal)
        {
            if(isset($mappingData) && !empty($mappingData))
            {
                foreach($mappingData as $mIndex => $mVal)
                {
                   if(($mVal['modref_id'] == $dVal['id']) && ($mVal['module'] === $modref))
                   {
                      $moduleData[$dVal['id']] = $dVal;
                      $moduleData[$dVal['id']]['used'] = 1;
                   } else {
                      $moduleData[$dVal['id']] = $dVal;
                      $moduleData[$dVal['id']]['used'] = false;                   
                   }     
                }            
            } else {
              $moduleData[$dVal['id']] = $dVal;
              $moduleData[$dVal['id']]['used'] = false;                               
            }
        }
        return $moduleData;
    }
    
    function _displayKpa($key, $mVal ,$mappingData)
    {
        $kpis   = Kpi::findAll(" AND status & 4 = 4");
        $kpiData = array();
        $usedKpi = array();
        $select  = "<select id='".$key."_kpi_".$mVal['id']."' name='".$key."_kpi_".$mVal['id']."'>"; 
        $select  .= "<option value=''>--please select--</option>";
        foreach($kpis as $kpiId => $kpi)
        {            
            if(isset($mappingData) && !empty($mappingData))
            {
                $select  .= "<option ";
                foreach($mappingData as $mIndex => $val)
                {
                    if($kpi['id'] == $val['kpi'])
                    {
                       $select  .= "disabled='disabled'";   
                       if(($val['modref_id'] == $mVal['id']) && ($val['module'] == $key))
                       {
                          $select  .= "selected='selected'";
                       }
                    } else {
                        $select  .= "";   
                    }
                }
                $select  .= "value='".$kpi['id']."'>".$kpi['name']."</option>";
            } else {
                $select  .= "<option value='".$kpi['id']."'>".$kpi['name']."</option>";
            }
        }       
        $select  .= "</select>";
        return $select;
    }
    
    function _displayButton($key, $mVal,$mappingData)
    {
        $used = false;
        foreach($mappingData as $mIndex => $val)
        {
            if(($mVal['id'] == $val['modref_id']) && ($val['module'] == $key))
            {
                $used = true;
            }
        }
        if($used)
        {
            return "<input type='button' name='".$key."_update_".$mVal['id']."' id='".$key."_update_".$mVal['id']."' value='Update' class='update' />";
        } else {
            return "<input type='button' name='".$key."_assign_".$mVal['id']."' id='".$key."_assign_".$mVal['id']."' value='Assign' class='assign' />";
        }
    }
    
    function saveMapping($data)
    {
        $data['modref_id'] = substr($data['modref_id'], -1, strpos($data['modref_id'], "_"));
        $data['module'] = substr($data['module'], 0, strpos($data['module'], "_"));    
        $id = KpiMapping::saveData($data);
        return Message::save($id, " kpi mapping ref #".$id);
    }
    
    function updateMapping($data)
    {
        $data['modref_id'] = substr($data['modref_id'], -1, strpos($data['modref_id'], "_"));
        $data['module'] = substr($data['module'], 0, strpos($data['module'], "_"));        
        $res = KpiMapping::updateWhere($data, array("module"    => $data['module'],
                                                      "modref_id" => $data['modref_id'],
                                                      "user"      => $data['user'] 
                                                     )
                                        );
        return Message::update($res, " kpi mapping ref #".$res);
    }
        
    
}
?>
