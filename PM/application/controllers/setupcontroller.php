<?php
class SetupController extends Controller
{
     
    public function __construct()
    {
      parent::__construct();
    }
    
    function defaults()
    {
      $this->template->scripts   = array("setup.js");
      $this->template->pagetitle = "Setup Defaults";     
      $this->template->view("setup/index");           
    }      
     
}
?>
