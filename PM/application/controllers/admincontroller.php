<?php
class AdminController extends Controller
{

     function __construct()
     {
       parent::__construct();
     }
     
     function index()
     {
        $this -> template -> scripts = array('admin.js');
        $this -> template -> pagetitle = "Administration";
        $this -> template -> view("admin/index");
     }
     
     
}
?>
