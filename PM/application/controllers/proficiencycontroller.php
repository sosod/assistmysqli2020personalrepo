<?php
class ProficiencyController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	
	function getAll( $options = "")
	{
        return Proficiency::fetchAll();  		
	}
}
