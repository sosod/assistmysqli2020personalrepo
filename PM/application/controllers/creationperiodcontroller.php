<?php
class CreationPeriodController extends Controller
{
     
     function __construct()
     {
        parent::__construct();
     }     
     
     function admin()
     {
        $this -> template -> scripts = array("creationperiod.js");
        $this -> template -> pagetitle = "Creation Periods";
	   $this -> template -> view("admin/creationperiods");	
     }
     
     function save($data)
     {    
        $response = array();
        if(!(Validation::dateDiff($data['start_date'], $data['end_date'])))
        {
           echo json_encode(array("text" => "Start date cannot be after end date", "error" => true));
           exit();
        } else {
           $data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
           $data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
           $res = $this -> Model -> saveData($data);
           return Message::save($res, "creation period");
        } 
     }
     
     function update($data)
     {
        if(!(Validation::dateDiff($data['start_date'], $data['end_date'])))
        {
           echo json_encode(array("text" => "Start date cannot be after end date", "error" => true));
           exit();
        } else {
           $data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
           $data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
		 $res = CreationPeriod::updateWhere($data, array("id" => $data['id']), new SetupAuditLog());
		 return Message::update($res, " creation period #".$data['id']);
        }
     }
     
     function getAll($options= array())
     {
        $results = $this -> Model -> getCreationPeriod($options);
        return $results;
     }
     
     function employeespecific()
     {
        $this->template->scripts = array("employeespecific.js");
        $this->template->pagetitle = "Employee Creation ";          
        $this->template->view("admin/employeespecific");          
     } 
     
     function getUserAll($options)
     {
        $period = CreationPeriod::getUserEvaluationPeriod($options);
        return $period;
     }
        
     function saveUserSpecific($data)
     {
       $res = CreationPeriod::saveUserSpecific($data);
       return Message::save($res, " user specific creation period ");
     }   
 
     function updateUserSpecific($data)
     {
       $res = CreationPeriod::updateUserSpecific($data);
       return Message::update($res, " user specific creation period ");
     }
         
}
?>
