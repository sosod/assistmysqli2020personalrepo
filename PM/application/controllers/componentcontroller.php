<?php
class ComponentController extends Controller
{
     protected $userDetails;
     
     public function __construct()
     {
          parent::__construct();
          
     }
     
     function index()
     {
        $this->template->components = $this -> Model -> findAll();
        $this->template->scripts   = array("component.js");
        $this->template->pagetitle = "Performance Components";     
        $this->template->view("component/view");           
     } 
     
     function organisational()
     {
          $this->template->scripts = array("organisational.js", "jquery.ui.kpa.js", "jquery.ui.kpi.js", "jquery.ui.action.js");
          $this->template->pagetitle = "Organisational Goals Template";
          $this->template->view("admin/organisational"); 
     }
     
     function managerial()
     {
          $this->template->scripts = array("managerial.js", "jquery.ui.kpa.js", "jquery.ui.kpi.js", "jquery.ui.action.js");
          $this->template->pagetitle = "Managerial Goals Template";
          $this->template->view("admin/managerial"); 
     }     
     
     function individual()
     {
          $this->template->scripts = array("individual.js", "jquery.ui.kpa.js", "jquery.ui.kpi.js", "jquery.ui.action.js");
          $this->template->pagetitle = "Individual Goals Template";
          $this->template->view("admin/individual"); 
     }          
     
     function view()
     {
 
        //$this->template->components = $this->Model->findAll();
        $this->template->scripts = array("jquery.ui.evaluation.js");
        $this->template->pagetitle = "Performance Components";  
        $userdata                  = User::getInstance() -> getUser();
        $this->template->userdata = $userdata;
        $this->template->userDetails = $this->template->setTemplate("userdetails", $userdata);     
        $this->template->view("component/view");           
     }     
     
     function newcomponent($options = array())
     {
        $userdata = User::getInstance() -> getUser();
        if(!empty($userdata))
        {
          $this -> template -> userdata = $userdata;      
           //setup the inital setup for this user     
           if(!UserSetting::initialSetupDone($userdata))
           {
              Component::setupInitialSettings($userdata);
              UserSetting::setupDone($userdata);
           }              
        }
        $this -> template -> scripts     = array('jquery.ui.evaluation.js');
        $this -> template -> pagetitle   = "Performance Components";     
        $this -> template -> userDetails = $this -> template -> setTemplate("userdetails", $userdata);         
        $this -> template -> view("component/new"); 
     }
    
    function copy_evaluationyear()
    {
        $this->template->scripts = array("jquery.ui.matrix.js");
        $userdata                  = User::getInstance() -> getUser();
        $this->template->userdata = $userdata;
        $this->template->userDetails = $this->template->setTemplate("userdetails", $userdata);         
     
        $this->template->pagetitle = "Copy Evaluation Year";          
        $this->template->view("admin/copy_evaluationyear");     
    }
     
	function matrixcomponents($data)
	{
        $this->template->scripts   = array("performancecomponents.js");
        $this->template->pagetitle = "Performance Components";
	   $this->template->layout = null;
        $this->template->view("component/component");
	   exit();
	}  

	function evaluationcomponent($data)
	{          
        $this->template->scripts   = array("performancecomponents.js");
        $this->template->pagetitle = "Performance Components";
	   $this->template->layout = null;
        $this->template->view("component/evaluationcomponent");
	   exit();
	}  	
	  
	function confirm()
	{
        $this->template->scripts   = array("jquery.ui.evaluation.js");
        $this->template->pagetitle = "Confirm Performance Matrix";	          
        $this->template->view("component/confirm");
	}	
	
	function activate()
	{
        $this->template->scripts = array("jquery.ui.evaluation.js");
        $this->template->pagetitle   = "Activate Performance Matrix";	
        $userdata                  = User::getInstance() -> getUser();
        $this->template->userdata = $userdata;
        $this->template->userDetails = $this->template->setTemplate("userdetails", $userdata);            
	   $this->template->view("component/activate");
	}	
    
	function update()
	{
        $this->template->scripts = array("jquery.ui.evaluation.js");
        $this->template->pagetitle = "Update Performance Matrix";	  
	   $this->template->view("component/update");
	}    
	
	function edit()
	{
        $this->template->scripts = array("jquery.ui.evaluation.js");
        $this->template->pagetitle = "Edit Performance Matrix";	   
        $this->template->view("component/edit");
	}
	
	function approve()
	{
        $this->template->scripts = array("jquery.ui.approve.js");
        $this->template->pagetitle = "Approve Performance Matrix";	 
	   $this->template->view("component/approve");
	}	    
	
	function assurance()
	{
        $this->template->scripts = array("jquery.ui.evaluation.js");
        $this->template->pagetitle = "Performance Matrix Assurance";	 
	   $this->template->view("component/assurance");
	}	    
	
	function evaluate()
	{
        $this->template->scripts = array("jquery.ui.evaluation.js");
        $this->template->pagetitle = "Evaluate Performance Matrix";	  
	   $this->template->view("component/evaluate");
	}
	
	function componentEvaluation($options = array())
	{
	  $components = Component::getComponents();
	}
	
	function setup()
	{
        $this->template->scripts = array("component.js");
        $this->template->pagetitle = "Performance Components";	
	   $this->template->view("setup/performancecomponents");
	}	

     function download($option=array())
     {
        //echo "This is where we just get the data to download the info . . .";
        exit();  
     }
		          
     function getAll($options = array())
     {
        $components = SetupComponent::getComponents($options);
        return $components;
     }		     
		     
     function copyComponent($data)
     {
         $results = Component::copy($data);
         return $results; 
     }		     
  
     function getPerformanceComponents($options = array())
	{
	   $sqlOptions = "";

	   if(isset($options['page']))
	   {
	     if($options['page'] == "activate")
	     {
	       $sqlOptions .= " AND status & ".Component::CONFIRMED." = ".Component::CONFIRMED." 
	                        AND status & ".Component::ACTIVATED." <> ".Component::ACTIVATED."
	                      ";
	     }
	     if($options['page'] == "confirm")
	     {
	       $sqlOptions .= " AND status & ".Component::CONFIRMED." <> ".Component::CONFIRMED." ";
	     }	  
	     if($options['page'] == "new")
	     {
	       $sqlOptions .= " AND status & ".Component::CONFIRMED." <> ".Component::CONFIRMED." ";
	     }	  	        	     
	     unset($options['page']);  
	   }
	   if(isset($options['section']))
	   {
	     if($options['section'] == "manage")
	     {
	       $sqlOptions .= " AND status & ".Component::ACTIVATED." = ".Component::ACTIVATED."";
	     }
	     unset($options['section']);
	   }
	   $userArr = array();
	   if(isset($options['user']) && !empty($options['user']))
	   {
	     $userArr['user'] = $options['user'];
	   }
	   //secho $sqlOptions."<br /><br />";
	   $sqlOptions .= $this->sqlOption($sqlOptions);
   	   $resultArr  = array();
        //$resultArr  = $this->_getUserComponentData($options, $sqlOptions);
        
	   $user = User::getInstance()->getUser($userArr);  
	   if(!empty($user))
	   {
	        $sqlOptions .= " AND status & 2 <> 2 AND evaluationyear = '".$user['default_year']."'";	   
	        $results = Performancecomponents::findAll($sqlOptions);	
	        $userCat = Performancecategories::findById($user['categoryid']);
	        $matrix  = Performancematrix::find(" AND categoryid = '".$user['categoryid']."' AND evaluationyear = '".$user['default_year']."' ");	
	        $totalWeight = 0;
	        $resultArr = array();
	        if(!empty($matrix))
	        {
	            $matrixData = array();
	            if(!empty($matrix['matrix']))
	            {
	              $matrixData = @unserialize(base64_decode($matrix['matrix']));
	            }
	            foreach($results as $componetIndex => $componentVal)
	            {
	               //if the user component is selected to be used for this user/categoryid
	               if(isset($matrixData['usecat']))
	               {
	                  if(isset($matrixData['usecat'][$componentVal['id']]))
	                  {
	                    $resultArr[$componentVal['id']] = $componentVal;
                         if(isset($matrixData['weighting'][$componentVal['id']]))
                         {
                           $totalWeight += $matrixData['weighting'][$componentVal['id']];
                           $resultArr[$componentVal['id']]['percentage'] = $matrixData['weighting'][$componentVal['id']]."%";
                         }	               
	                  }
	               }
	            }
	           $resultArr =  array("components" => $resultArr, "totalWeight" => $totalWeight);
	        } else {
	          $resultArr = array("text" => "User category ".$user['performancecategory']."'s performance matrix is not configured yet under setup", "error" => true);
	        }
	    } else {
	      $resultArr = array("text" => "Please configure user category,  performance matrix correctly under setup", "error" => true);
	    }
        return $resultArr;
	}
	
	function saveComponent( $data )
	{
		$res = Performancecomponents::saveData( $data['data'] );
		return Message::save($res , "performance component #".$res );
	}
	
	function updateComponent($data)
	{
		$res = Performancecomponents::updateWhere($data['data'], array("id" => $data['id']), new SetupAuditLog("Component"));
		return Message::update($res ,"performmance component #".$data['id'] );
	}
	
	function getAllComponents($options = "" )
	{
		$components = Component::findAll($options['evaluationyear']);
		$compArr = array();
		foreach( $components as $key => $val)
		{	
			$kpaIds[$val['id']] = $this->getComponentKpa( $options['evaluationyear'], $val['id'] );
		}		
		$headers 	= Naming::getHeaderList();
		return array("headers" => $headers, "data" => $components, "cols" => count($headers), "kpa" => $kpaIds ); 
	}
	
	function getComponentKpa($data)
	{
		$kpa 	= Kpa::findAll(" AND evaluationyear = '".$data['evaluationyear']."' AND componentid = '".$data['componentId']."'");
		$kpaIdArr = array(); 
		foreach( $kpa as $index => $kpaVal)
		{
			$kpaIdArr[$kpaVal['id']] = $index;
		}
		return $kpaIdArr; 
	}
	
	function getComponents($data)
	{
	     $component = Component::findById($data['componentId']);
	     $categoriesConfigs = array();

	     if(!empty($component['categories']))
	     {
	        $categoriesConfigs  =  unserialize(base64_decode($component['categories']));
	     }
	     /*print "<pre>";
	       print_r($categoriesConfigs);
	     print "</pre>";
	     */
		$kpa 	= Kpa::findAll(" AND evaluationyear = '".$data['evaluationyear']."' 
		                           AND componentid = '".$data['componentId']."' 
		                           AND kpastatus & ".Kpa::TEMPLATEKPA." = ".Kpa::TEMPLATEKPA."
		                         ");
		$kpadata  = array(); 
		$kpiArr   = array();
		$actionArr = array();
	     foreach($kpa as $index => $value)
	     {    
	          $kpadata[$value['id']] = $value; 
	          $kpidata = Kpi::findAll(" AND kpaid = '".$value['id']."' ");
	          foreach($kpidata as $kpiIndex => $kpi)
	          {
	               $actiondata = Action::findAll(" AND kpiid = '".$kpi['id']."' ");
                    $kpiArr[$value['id']][$kpi['id']]  = $kpi;
                    foreach($actiondata as $actionIndex => $actionVal)
                    {
                         $actionArr[$value['id']][$kpi['id']][$actionVal['id']] = $actionVal;
                    }
	          }
	     }
		$categories = Performancecategories::findAll(" AND evaluationyear = '".$data['evaluationyear']."' ");
		$kpaData = array("categories" => $categories,
		                 "headers"    => Naming::getHeaderList(),
		                 "kpa"        => $kpadata,
		                 "kpi"        => $kpiArr,
		                 "actions"    => $actionArr,
		                 "component"  => $component,
		                 "configs"    => $categoriesConfigs
		                ); 
		return $kpaData; 	
	}
	
	function updateComponentCategories($data )
	{
		parse_str($data['data'], $dataArr);
		$matrix = array(); 
		foreach( $dataArr as $key => $val)
		{
			$catId = substr($key, 0, strpos($key, "_"));
			$keyId = substr($key, -1 , strpos($key, "_"));
			if( strstr($key, "_kpa_"))
			{
				$matrix['kpa'][$keyId][$catId] = $val;		
			}
			if( strstr($key, "_kpi_"))
			{
				$matrix['kpi'][$keyId][$catId] = $val;		
			}
			if( strstr($key, "_action_"))
			{
				$matrix['action'][$keyId][$catId] = $val;		
			}																	
		}
		$res = Component::updateWhere(array("categories" => base64_encode(serialize($matrix))), array("id" => $data['id']));
		return Message::update( $res , "performance categories #".$data['id']);
	}

     	
}
?>
