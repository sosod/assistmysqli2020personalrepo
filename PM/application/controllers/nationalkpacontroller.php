<?php
class NationalkpaController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function setup()
	{
        $this->template->scripts  = array("nationalkpa.js");
        $this->template->pagetitle = "National KPA";	
	   $this->template->view("setup/nationalkpa");	
	}
	
	function getAll($options = "" )
	{
		$nkpaObj 	  = new Nationalkpa();
		$nationalkpas = $nkpaObj -> get_national_kpas();
		return $nationalkpas;
	}
	
	function updateNationalKpa($data)
	{
	    $nationalkpaObj = new Nationalkpa();
	    $res            = $nationalkpaObj -> update_national_kpa($data);
	   //$res = Nationalkpa::updateWhere($data['data'], array("id" =>  $data['id']));
	   return Message::update( $res, "national kpa #".$data['id']);
	}
	
	function saveNationalKpa( $data )
	{
		$res = Nationalkpa::saveData($data['data']);
		return Message::save($res , "national kpa #".$res );
	}
	
}
