<?php
class UserController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function user()
	{
        $this->template->scripts = array("user.js");
        $this->template->pagetitle = "User Setup";    
        $this->template->view("setup/user");
	}
	
     function edituser($user)
	{
	   $this->template->user = UserSetting::findByUser_Id($user['userid']);
        $this->template->scripts = array("user.js");
        $this->template->pagetitle = "Edit User Setup";        
        $this->template->view("setup/edit_user");	    
	}
	
     function define_evaluationyear()
	{
        $this->template->scripts = array("user.js");
        $this->template->pagetitle = "Set Default Evaluation Year";          
        $this->template->view("admin/user_evaluation_year");    
	}	

	function myprofile()
	{
        $this->template->scripts = array("user.js");
        $this->template->pagetitle = "My Profile";    
        $this->template->view("myprofile");
	}
	
	function getAll($options = array())
	{
	   $option_sql = "";
	   if(isset($options['departmentid']) && !empty($options['departmentid']))
	   {
	     $option_sql = " AND TK.tkdesig = '".$options['departmentid']."' ";
	   }
	   $userObj = new User();
	   $results = $userObj -> get_users($option_sql);
	   $users = array();
	   foreach($results as $index => $user)
	   {
		    $users[$user['tkid']] = array("username" => $user['tkname']." ".$user['tksurname'],
		                                  "status"   => (($user['status'] & 2) == 2 ? "0" : $user['status']) ,
		                                  "tkname"   => $user['tkname'],
		                                  "tksurname" => $user['tksurname'],
		                                  "tkid"      => $user['tkid'],
		                                  "tkstatus"  => $user['tkstatus'],
 		                                  "email"     => $user['tkemail'],
		                                  "modref"    => (isset($user['usrmodref']) ? $user['usrmodref'] : ""),
		                                  "default_year" => $user['default_year']
		                                );
	  }
	  return $users;
	}
	
	function getUser($options=array())
	{
	    $user = User::getInstance() -> getUser($options);
	    if(!empty($user))
	    {
	       $user['evaluationyear'] = $user['start']." to ".$user['end'];
	       $user['manager'] = $user['managername'];
	       $user['user'] = $user['tkname']." ".$user['tksurname'];
	    }  
	    return $user;
	}
	
	function updateUser($data)
     {
        $user = UserSetting::findByUser_Id($data['user_id']);
        if(!empty($user))
        {
            $res = UserSetting::updateWhere($data, array("user_id" => $data['user_id']), new UserAuditLog());
            return Message::update($res, "user setting ");	
        } else {
            $id = UserSetting::saveData($data);
            return Message::save($id, "user setting ref#".$id);	        
        }
	}
	
	function deleteUser($user)
	{
        $res = UserSetting::updateWhere(array("status" => 2), array("user_id" => $user['userid']));
        return Message::delete($res, "user setting ");		    
	}
	
	function saveUserAccess($data)
	{
	    parse_str($data['usersetup'], $postData);
         $userObj = new UserSetting();
         $results = $userObj -> update_user_setting($postData);
         return $results;	 
	}
	
	function getSettings()
	{
	   $usersettings = UserSetting::fetchUserSetting();
	   return $usersettings;
	}

}
