<?php
class KpastatusController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function setup()
	{
        $this->template->scripts = array("kpastatus.js", "jscolor.js");
        $this->template->pagetitle = "KPA Status";	    
	   $this->template->view("setup/kpastatus");	
	}
	
	function getAll($options = array())
	{
        $kpastatuses = Kpastatus::getKpaStatuses($options);
        return $kpastatuses;
	}
	
	function saveStatus( $data )
	{
		$res = Kpastatus::saveData($data['data']);
		return Message::save($res, "kpa status #".$res);
	}
	
	function updateStatus($data)
	{
	   $statusObj = new Kpastatus();
	   $res       =  $statusObj -> update_kpa_status($data);
	   return Message::update($res, "kpa status #".$data['id']);
	}
}
