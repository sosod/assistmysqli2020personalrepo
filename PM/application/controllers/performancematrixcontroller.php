<?php
class PerformancematrixController extends Controller
{

	function __construct()
	{
		parent::__construct();
	}     
     
	function setup()
	{
	   //$this->template->categories = Performancecategories::findAll(" AND status  & 1 = 1");
	   //$this->template->totalCat   = count($this->template->categories);
	   //$this->template->evalfreq   = Evaluationfrequencies::findAll(" AND status & 1 = 1");
        $this->template->scripts = array("performancematrix.js");
        $this->template->pagetitle = "Performance Matrix";	
	   $this->template->view("setup/performancematrix");
	}
	
	function viewcontent($options)
	{
	  $matrixObj = new PerformanceMatrix();
	   $weights   = $matrixObj -> get_total_weights($options['id'], $options['evaluationyear']);
        $matrix    = $matrixObj -> get_component_matrix($options['id'], $options['evaluationyear']); 
        $this -> template -> matrix = $matrix;
       
	   $this -> template -> components = Component::findAll(" AND evaluationyear = '".$options['evaluationyear']."' AND status  & 1 = 1");
	   $this -> template -> evalfreq   = Evaluationfrequencies::findAll(" AND status & 1 = 1");	
	   $this -> template -> totalCat   = count($this -> template -> components);
	   /*  
	   $matrix = Performancematrix::find(" AND categoryid = '".$options['id']."' AND evaluationyear = '".$options['evaluationyear']."'");
	   $matrixData = array();
	   if(isset($matrix) && !empty($matrix))
	   {
	       $this->template->matrixid        = $matrix['id'];
	       if(!empty($matrix['matrix']))
	       {
	         $matrixData = @unserialize(base64_decode($matrix['matrix']));
	       }
	   }
	   $this->template->compData = $matrixData;

	   $matrix = $this->_getComponentWeighting($options['id'], Performancematrix::findAll(" AND evaluationyear = '".$options['evaluationyear']."'")); 
	   $this->template->componentWeighting   = $matrix['weightings'];
	   $this->template->compData             = $matrix['currentmatrix'];
	   $this->template->matrixid             = $matrix['matrixid'];	     
	    */
	   $this -> template -> totalW    = $weights['totalW'];
        $this -> template -> scripts   = array("performancematrix.js");
        $this -> template -> pagetitle = "Performance Matrix";		
        $this -> template -> layout    = false;
	   $this -> template -> view("setup/matrix");	    
	   exit();
	}	

	function update_matrix($data)
	{
       parse_str($data['data'], $matrixData);
       $matrixObj = new PerformanceMatrix();
       $res       = $matrixObj -> update_matrix($matrixData);
       return Message::update($res, ' performance matrix ');
	}		
	
	function save_matrix($data)
	{
    	    parse_str($data['data'], $matrixData);
         $matrixObj = new PerformanceMatrix();
         $res       = $matrixObj -> save_matrix($matrixData);
         return Message::save($res, ' performance matrix ');
	}	
	
	function _getComponentWeighting($componentid, $matrixes = array())
	{
	     $componentWeighting = array();
	     $componentMatrix = array();
	     $matrixId = "";
          if(!empty($matrixes))
          {
               foreach($matrixes as $index => $matrixdata)
               {                    
                    if(!empty($matrixdata['matrix']))
                    {    
                        $componentMatrixData = @unserialize(base64_decode($matrixdata['matrix']));
                        if(!empty($componentMatrixData))
                        {
                            if(isset($componentMatrixData['weighting']) && !empty($componentMatrixData['weighting']))
                            {
                              foreach($componentMatrixData['weighting'] as $wIndex => $wVal)
                              {
                                  $componentWeighting[$wIndex][$matrixdata['componentid']] = $wVal;
                              }
                            }
                            //$componentWeighting[$matrixdata['id']]  = $componentMatrixData['weighting'];
                        }
                        
                         if($matrixdata['componentid'] == $componentid)
                         {
                              $matrixId = $matrixdata['id'];
                              $componentMatrix = $componentMatrixData;
                         }                                      
                    }
               }
          }	     
          return array("weightings" => $componentWeighting, "currentmatrix" => $componentMatrix, "matrixid" => $matrixId);
	}
	
	function savePerformanceMatrix( $data )
	{
		parse_str($data['data'], $matrixData );
          $performanceMatrix = $this->_componentCategoriesData($matrixData);  
		$insertdata        = array("matrix"         => $performanceMatrix['matrix'],
		                           "evaluationyear" => $data['evaluationyear'],
		                           "categoryid"    => $data['categoryid']
		                           );
		$res = Performancematrix::saveData($insertdata);
		return Message::save($res , "performance matrix ");
	}

	function updatePerformanceMatrix($data)
	{
		parse_str($data['data'], $matrixData);
        $updatedata = $this->_componentCategoriesData($matrixData);
		$res = Performancematrix::updateWhere($updatedata, array("id" => $data['id']));
		return Message::update($res , "performance matrix #".$data['id']);
	}
	
	/*
	 Prepare the categories component data for database saving	
     */
     private function _componentCategoriesData($matrixData)
     {
		$performanceMatrix = array();		
		$componentToUse = array(); //components selected to be used
		//get the components selected to be used for this category
		foreach($matrixData as $k => $valArr)
		{
		  if(strstr($k , "usecat"))
		  {
               $componentCategory = substr($k, strpos($k, "_")+1);
               $componentID       = substr($componentCategory, 0, strpos($componentCategory, "_")); 
			$componentToUse[] =  $componentID;
		  }
		}		
		//match the category ids for this component to the values selected
		foreach($matrixData as $key => $val)
		{
		     $keyname = substr($key, 0, strpos($key, "_"));
		     
		     $componentCategory = substr($key, strpos($key, "_")+1);
		     $componentID       = substr($componentCategory, 0, strpos($componentCategory, "_")); 
		     
			$key = substr_replace($key, "", -2 , strpos($key, "_"));
			$field  = substr( $key , 0 , strpos($key, "_")); 
			$catId  = substr($key ,strpos($key, "_")+1);
			if( in_array($componentID, $componentToUse))
			{
                  if($keyname == "usecat") 
                  {
                    $performanceMatrix[$field][$componentID] = substr($val, 0, strpos($key, "_")-1);
                  } else {
                    $performanceMatrix[$field][$componentID] = $val;
                  }			     
			}		
		}
		return array("matrix" => base64_encode(serialize($performanceMatrix))) ;
     }
     
     function activateComponent($data)
     {    
	   $res = Performancematrix::updateWhere($updatedata, array("id" => $data['id']));
	   return Message::update($res , "performance matrix #".$data['id']);
     }
	
	function get_matrix_report($options)
	{
	   $performancematrixObj = new Performancematrix();
	   $report               = $performancematrixObj -> matrix_report($options);
	   return $report;
	}
	
	function get_recommendation_matrix($options)
	{
	   $performancematrixObj = new Performancematrix();
	   $report               = $performancematrixObj -> recommendation_matrix($options);
	   return $report;
	}	
	
	function get_bell_curve_data($options = array())
	{
	   $performancematrixObj = new Performancematrix();
	   $report               = $performancematrixObj -> bell_curve_report($options);
	   echo $report;
	}

	
}
