<?php
class ComponentstatusController extends Controller
{

	function __construct()
	{
		parent::__construct();
	}
	
	function setup()
	{
        $this->template->scripts = array("componentstatus.js", "jscolor.js");
        $this->template->pagetitle = "Performance Component Status";
	   $this->template->view("setup/componentstatus");
	}
	
	function getAll( $options = "")
	{
		$sqlOptions = $this->sqlOption($options);
		$sqlOptions .= " AND status & 2 <> 2";
		$used = array();
		$statusArr = array();
		$results = ComponentStatus::findAll( $sqlOptions );
		foreach( $results as $index => $status)
		{
			if( in_array($status['id'], $used))
			{
				$status['used'] = true;
			} else {
				$status['used'] = false;
			}
			$statusArr[$index] = $status;
		}
		return $statusArr;
	}

	function updateStatus($data)
	{
	   $statusObj = new ComponentStatus();
	   $res       = $statusObj -> update_component_status($data);
	   return Message::update($res ,"component status #".$data['id']);
	}
	
	function saveStatus( $data )
	{
		$id = ComponentStatus::saveData($data['data'] );
		return Message::save($id, "component status #".$id );
	}
	
}
