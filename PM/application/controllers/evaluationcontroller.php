<?php
class EvaluationController extends Controller
{
     
    public function __construct()
    {
      parent::__construct();
    }
    
    function getAll($options=array())
    {
       $data = Evaluation::getEvaluations($options);
    }
    
    function get_evaluation_statuses_report($options)
    {
        $evaluationObj = new Evaluation();
        $response      = $evaluationObj -> evaluation_status_report($options);
        return $response;
    }
    
    function get_evaluation_score_report($options)
    {
        $evaluationObj = new Evaluation();
        $response      = $evaluationObj -> scores_report($options);
        return $response;    
    }
   

}
?>
