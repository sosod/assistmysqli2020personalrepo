<?php
class DirectorateController extends Controller
{

     public function __construct()
     {
          parent::__construct();
     }     
     
     function get_all()
     {
        $dirObj       = new Directorate();
        $directorates = $dirObj -> get_all_directorates();
        return $directorates;
     }
     
     function get_all_subdirectorate($options)
     {
        $dirObj          = new Directorate();
        $subdirectorates = $dirObj -> get_subdirectorates($options['directorate']);
        return $subdirectorates;
     }
     
     function get_all_departments()
     {
        $dirObj       = new Directorate();
        $directorates = $dirObj -> get_departments();
        return $directorates;
     }
}

?>
