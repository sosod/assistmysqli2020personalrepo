<?php
class KpistatusController extends Controller
{

	function __construct()
	{
		parent::__construct();	
	}
	
	function setup()
	{
        $this->template->scripts = array("kpistatus.js", "jscolor.js");
        $this->template->pagetitle = "KPI Status";	
	   $this->template->view("setup/kpistatus");
	}

	function getAll($options = array())
	{
        $statuses = Kpistatus::getKpiStatuses($options);
        return $statuses;
	}
	
	function saveKpi( $data )
	{
		$id  = Kpistatus::saveData($data['data'] );
		return Message::save($id, " kpi status #".$id);
	}
	
	function updateKpi($data)
	{
		$res = Kpistatus::updateWhere($data['data'], array("id" =>  $data['id']), new SetupAuditLog("Kpi Status"));
		return Message::update($res, "kpi status #".$data['id']);
	}
	
}

