<?php
class EvaluationyearsController extends Controller 
{
    
    function __construct()
    {
       parent::__construct();
    }

    function setup()
    {
       $this->template->scripts = array("evaluationyears.js");
       $this->template->pagetitle = "Evaluation Years";    
       $this->template->view("setup/evaluationyears");
    }
    
    function editevaluationyears($data)
    {   
       $this->template->frequency = array();    
       $evaluationyear = Evaluationyear::findById($data['id']);   
       /*if(!empty($evaluationyear['frequency']))
       {
          
          = unserialize(base64_decode($evaluationyear['frequency']));
       }*/
       $this->template->frequency = EvaluationPeriod::getFrequencyEvaluationPeriods($data['id']);
       $this->template->evaluationyear  = $evaluationyear;
       $this->template->evalPeriods = Evaluationfrequencies::findAllByEvaluationyear($evaluationyear['id']);
       $this->template->scripts = array("evaluationyears.js");
       $this->template->pagetitle = "Evaluation Years";          
       $this->template->view("setup/editevaluationyears");
    }
    
    function admin()
    {   
        $this->template->scripts = array("evaluationyears.js");
        $this->template->pagetitle = "Evaluation Years";          
        $this->template->view("admin/evaluationyears");
    }   


    function addevaluationyears()
    {   
        $this->template->scripts = array("evaluationyears.js");
        $this->template->pagetitle = "Evaluation Years";          
        $this->template->view("setup/addevaluationyears", $data);
    }       
    
    function getAll($options = array())
    {
       $option = "";
       if(isset($options['status']))
       {
	          $option = " AND status & 8 <> 8";
       }
       $results =  Evaluationyear::findAll($option);
       $evalYears = array();
       $used 	   = array();
       foreach( $results as $index => $result )
       {
         if( in_array($result['id'], $used))
	    {
	       $result['used'] = true;
	       $evalYears[$index] = $result;				
	     } else {
	       $result['used'] = false;
	       $evalYears[$index] =  $result;
	     }
       }
        return $evalYears;
	}
	
	
	function saveEvaluationYears( $data )
	{
		$errors = array();
		if(!Validation::dateDiff($data['end'], $data['lastday']))
		{
		   return array("text" => "End date cannot be after the last day of the financial year", "error" => true );
		} else if(!Validation::dateDiff($data['start'], $data['end'])){
		   return array("text" => "Start date cannot be after the end date", "error" => true );
		} else {
		   $result = Evaluationyear::saveData( $data );
		   return Message::save( $result, "evaluation year #".$result );	
		}
	}

	function updateEvaluationYear($data)
	{
		$errors = array();
		if(!Validation::dateDiff($data['end'], $data['lastday']))
		{
		   return array("text" => "End date cannot be after the last day of the financial year", "error" => true );
		} else if(!Validation::dateDiff($data['start'], $data['end'])) {
		   return array("text" => "Start date cannot be after the end date", "error" => true );
		} else {
		   $res = Evaluationyear::updateWhere($data, array("id" => $data['id']), new SetupAuditLog("Evaluation Year"));
		   return Message::update($res, " evaluation year " );	
		}
	}

	function updateChanges($data )
	{
		$checked = array();
		$periods = array();
		$startdate = "";
		$enddate   = "";

		foreach($data['data'] as $index => $dataArr)
		{
			if( strstr($dataArr['name'], "check"))
			{
				$checked[] = $dataArr['value'];
			} else {
				$position = substr( $dataArr['name'], 0 , strpos($dataArr['name'], "_") );
				$freqId   =  substr( $dataArr['name'], -1, strpos($dataArr['name'], "_") );
				if( preg_match('/startdate/', $dataArr['name']))
				{
					$startdate = $dataArr['value'];
				}
				if( preg_match('/enddate/', $dataArr['name']))
				{
					$enddate = $dataArr['value'];
				}				
				$periods[$freqId][$position]  = array("startdate" => $startdate, "enddate" => $enddate );   
			}			
		}
		$checkedPeriods = array();
		foreach($checked as $k => $id)
		{
			if( array_key_exists($id, $periods))
			{
				$checkedPeriods[$id]  = $periods[$id]; 	
			}
		}
		$res = Evaluationyear::updateWhere(array("frequency" => serialize($checkedPeriods)), array("id" => $data['id']), new SetupAuditLog());
		return Message::update( $res , "evaluation year #".$data['id']);
	}
	
	function getEvaluationYearFrequencies($data)
	{
		$evalFreq  = new Evaluationfrequencies();
		$result = Evaluationyear::findById($data['id']);
		$freqquency 	= unserialize( $result['frequency'] ); 
		$freqData = array();
		foreach( $freqquency as $index => $freqArr)
		{	
			$freq = Evaluationfrequencies::findById($index);
			$freqData[$index] = $freq;
			$freqData[$index]['periods'] = $freqArr;
		}
		return $freqData;
	}
	
	function updateEvaluationYearPeriods($data)
	{
		$checked   = array();
		$periods   = array();		
		foreach($data['data'] as $index => $dataArr)
		{	
			if( strstr($dataArr['name'], "period"))
			{
				$checked[] = $dataArr['value'];
			} else {
				$freqId  = substr($dataArr['name'], 0, strpos($dataArr['name'], "_"));
				$newstr  = strrev($dataArr['name']);
				//echo $dataArr['name']." reversed to ".$newstr."\r\n\n";
				$position = strrev(substr($newstr, 0, strpos($newstr, "_")));				
				if( preg_match('/start/', $dataArr['name']))
				{
					$startdate = $dataArr['value'];
				}
				if( preg_match('/end/', $dataArr['name']))
				{
					$enddate = $dataArr['value'];
				}	
				if( $freqId !== "" || $position !== "") 
				{				
					$periods[$freqId][$position] = array("startdate" => $startdate, "enddate" => $enddate );
				}
			}
		}
		$periodObj = new EvaluationPeriod();
		$res 	   = $periodObj -> process_evaluation_periods($data['id'], $checked, $periods);
		return Message::update($res , " evaluation periods ");			
	}
	
	function close($data)
	{
		$evaluationyear = Evaluationyear::findById($data['id']);
		//add closed status
		$updatedata = array("status" => $evaluationyear['status'] + Evaluationyear::CLOSED);
		$res = Evaluationyear::updateWhere($updatedata, array("id" => $data['id']));
		return Message::update( $res, "evaluation year #".$data['id'] );
	} 
	
	function open($data)
	{
		$evaluationyear = Evaluationyear::findById($data['id']);
		//add closed status
		$updatedata = array("status" => ($evaluationyear['status'] & ~Evaluationyear::CLOSED));

		$res = Evaluationyear::updateWhere($updatedata, array("id" => $data['id']));
		return Message::update( $res, "evaluation year #".$data['id']);	
	}
    
}
?>
