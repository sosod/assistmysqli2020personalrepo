<?php
class NotificationsController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	
    function notification()
    {
        $this->template->scripts   = array("notifications.js");
        $this->template->pagetitle = "Notifications";    
        $this->template->view("admin/notifications");
    }
    
	function admin()
	{
	    $notificationObj                       = new Notifications();
        $user_notification                     = $notificationObj -> get_user_notifications();	    
        $this -> template -> user_notification = $user_notification;
        $this -> template -> scripts           = array("notifications.js");    
        if(isset($user_notification) && !empty($user_notification))
        {
          $this -> template -> pagetitle         = "Edit Notifications";
          $this->template->view("admin/edit_notifications");
        } else {
          $this -> template -> pagetitle         = "Notifications";
          $this->template->view("admin/notifications");
        }	     
	}
	
	
	function getAll()
	{
		
	}
	
	function save( $data )
	{
	   $notificationObj = new Notifications();
	   parse_str($data['data'], $post_data);
	   $id = $notificationObj -> save_user_notifications($post_data);
	   return Message::save($id, " user notifications #".$id);
	}

	function update($id, $data )
	{
	
	}
	
	
}
