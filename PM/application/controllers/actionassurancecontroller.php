<?php
class ActionAssuranceController extends Controller
{

    function __construct()
    {
      parent::__construct();
    }
   
   	function get_action_assurance($options = array())
	{
	    $assuranceObj = new ActionAssurance();
	    $assurance    = $assuranceObj -> get_assurances();
	    return $assurance;
	}
	 
    function save($data)
    {
	    $assuranceObj = new ActionAssurance();
	    $res          = $assuranceObj -> save_assurance($data);
	    return Message::save($res, "Action assurance #".$res);	        
    }
    
    function update($data)
    {
	    $assuranceObj = new ActionAssurance();
	    $res          = $assuranceObj -> update_assurance($data['id'], $data);
	    return Message::update($res, "Action assurance #".$res);	            
    }
    
    function delete()
    {
    
    
    }
    
    function upload()
    {
    
    }
    
    function delete_attachment()
    {
    
    
    }
    
    function remove_attachment()
    {
    
    
    }
        
    
}
?>
