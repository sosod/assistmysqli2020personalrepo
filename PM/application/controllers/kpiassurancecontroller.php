<?php
class KpiAssuranceController extends Controller
{
    
   
    function __construct()
    {
      parent::__construct();
    }
   
   	function get_kpi_assurance($options = array())
	{
	    $assuranceObj = new KpiAssurance();
	    $assurance    = $assuranceObj -> get_assurances();
	    return $assurance;
	}
	 
    function save($data)
    {
       if(isset($_SESSION['uploads']))
       {
          if(isset($_SESSION['uploads']['pmadded']))
          {
            if(isset($_SESSION['uploads']['pmadded']['kpi_assurance_'.$data['kpi_id']])  &&
              !empty($_SESSION['uploads']['pmadded']['kpi_assurance_'.$data['kpi_id']])
            )
            {
               $data['attachment'] = base64_encode(serialize($_SESSION['uploads']['pmadded']['kpi_assurance_'.$data['kpi_id']]));
               unset($_SESSION['uploads']['pmadded']['kpi_assurance_'.$data['kpi_id']]);
            }
          }
       }
	    $assuranceObj = new KpiAssurance();
	    $res          = $assuranceObj -> save_assurance($data);
	    return Message::save($res, "KPI assurance #".$res);	        
    }
    
    function update($data)
    {
       $assuranceObj = new KpiAssurance();
       $kpiassurance = $assuranceObj -> get_assurance($data['id']) ;
       Attachment::processAttachmentChange($kpiassurance['attachment'], "kpi_assurance_".$kpiassurance['kpi_id'], "kpi");
       if(isset($_SESSION['uploads']))
       {
          if(isset($_SESSION['uploads']['attachments']))
          {
             $data['attachment'] = $_SESSION['uploads']['attachments'];
          }
       }
	   $res          = $assuranceObj -> update_assurance($data['id'], $data);
	   if(isset($_SESSION['uploads']))
	   {
          if(isset($_SESSION['uploads']['pmadded']))
          {
            if(isset($_SESSION['uploads']['pmadded']['kpi_assurance_'.$kpiassurance['kpi_id']]))
            {
              unset($_SESSION['uploads']['pmadded']['kpi_assurance_'.$kpiassurance['kpi_id']]);
            }   
          }
          if(isset($_SESSION['uploads']['pmdeleted']))
          {
            if(isset($_SESSION['uploads']['pmdeleted']['kpi_assurance_'.$kpiassurance['kpi_id']]))
            {
               unset($_SESSION['uploads']['pmdeleted']['kpi_assurance_'.$kpiassurance['kpi_id']]);
            }   
          }		     
          if(isset($_SESSION['uploads']['actionchanges']))
          {
            unset($_SESSION['uploads']['actionchanges']);
          }
          if(isset($_SESSION['uploads']['attachments']))
          {
            unset($_SESSION['uploads']['attachments']);
          }
	   } 	    
	    return Message::update($res, "KPI assurance #".$res);	            
    }
    
	
	function download_file($data)
	{
	   $file  = $data['file'];
	   $name  = $data['name'];
	   $type  = $data['type'];
	   $ext   = substr($file, strpos($file, ".") + 1);
       header("Content-type:".$ext);
       header("Content-disposition: attachment; filename=".$name);
       readfile("../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$type."/".$file);  
	   exit();
	}
	
    function upload_attachment($data)
    {
        $id       = $data['id'];
	    $attObj   = new Attachment('kpi_assurance_'.$id, 'attach_kpi_'.$id); 
	    $results  = $attObj -> upload('kpi_assurance_'.$id);
        echo json_encode($results);
        exit();
    }
    
    function delete_attachment($action_info)
    {
	   $file      = $action_info['attachment'];
	   $action_id = $action_info['id'];
	   $attObj    = new Attachment('kpi_assurance_'.$action_id, 'attach_kpi_'.$action_id);
	   $response  = $attObj -> deleteFile($file, 'kpi_assurance_'.$action_id);
       echo json_encode($response);
       exit();
    }
    
    function remove_attachment($action_info)
    {
	    $action_id= $action_info['action_id'];
	    $file     = $action_info['attachment'].".".$action_info['ext'];
	    $attObj   = new Attachment('kpi_assurance_'.$action_id, 'attach_kpi_'.$action_id);
	    $response = $attObj -> removeFile($file, 'kpi_assurance_'.$action_id);
        echo json_encode($response);
        exit();
    }
        
    
}
?>
