<?php
class RatingScaleController extends Controller
{

	function __construct()
	{	
	   parent::__construct();	
	}
	
	function setup()
	{
        $this->template->scripts = array("ratingscale.js", "jscolor.js");
        $this->template->pagetitle = "Rating Scale";	    
	   $this->template->view("setup/ratingscale");
	}

	function getAll($options = array())
	{
          $ratingscales = RatingScale::getRatingScales($options);
          return $ratingscales;
	}
		
	function updateRatingScale($data)
	{
	   $ratingObj = new Ratingscale();
	   $res       =  $ratingObj -> update_rating_scale($data);
        return Message::update($res, "rating scale #".$data['id']); 
	}
	
	function saveRatingScale($data)
	{
		$id = Ratingscale::saveData( $data['data'] );
		return Message::save($id , "rating scale #".$id);
	}
	
}
