<?php
class SearchController extends Controller
{

     public function __construct()
     {
          parent::__construct();
     }     
     
     function index()
     {
        $this -> template -> pagetitle = "Simple Search";
        $this -> template -> view("search/simple");
     }
     
}

?>
