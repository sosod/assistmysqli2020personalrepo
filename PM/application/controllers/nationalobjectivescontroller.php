<?php
class NationalobjectivesController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function setup()
	{
        $this->template->scripts  = array("nationalobjectives.js");
        $this->template->pagetitle = "National Objectives";	
	   $this->template->view("setup/nationalobjectives");
	}
	
	function getAll($options = "")
	{

		$nobjObj 	  		= new Nationalobjectives();
		$nationalobjectives = $nobjObj -> get_national_objectives();
		return $nationalobjectives;
	}
	
	function updateNationalObj($data)
	{
	   $natObj = new Nationalobjectives();
	   $res    = $natObj -> update_national_objectives($data);
	   //$res = Nationalobjectives::updateWhere($data['data'], array("id" => $data['id']) );
	   return Message::update($res, "national objective #".$data['id']);
	}
	
	function saveNationalObj( $data )
	{
		$res = Nationalobjectives::saveData( $data['data'] );
		return Message::save($res ,"national objective #".$res );
	}
	

}
