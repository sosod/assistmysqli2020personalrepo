<?php
class EvaluationcommitteeController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	function setup()
	{
        $this->template->scripts = array("evaluationcommittee.js");
        $this->template->pagetitle = "Evaluation Committee";	    
	   $this->template->view("setup/evaluationcommittee");
	}
	
	function get_all($options = array())
	{
	   $option_sql = " AND EC.evaluationyear = '".$options['evaluationyear']."' AND EC.status & 2 <> 2 ";
	   $evalObj    = new Evaluationcommittee();
	   $results    = $evalObj -> get_all_evaluation_committee($option_sql);
	   return $results;
	} 
	
	/*function getAll( $options = "" )
	{
	    $options = " AND evaluationyear = '".$options['evaluationyear']."' AND status & 2 <> 2";
	    $results = Evaluationcommittee::findAll($options);
	    $evaluationCommittee = array();
	    $userList = $this->_getUserList( User::getUsers() );
	    foreach($results as $index => $evalComm)
	    {
	        $userStr = "";
	        $userIds = array();
	        if(isset($evalComm['user_id']) && !empty($evalComm['user_id']))
	        {
	           $userIds = explode(",", $evalComm['user_id']);
	           foreach($userIds as $uIndex => $val)
	           {
	                if(isset($userList[$val]))
	                {
	                   $userStr .= $userList[$val].",";
	                } 
	           }
	        } 
	        $usertextStr = "";
	        $usertextArr = array();
	        if(isset($evalComm['user']) && !empty($evalComm['user']))
	        {
	           $usersText = unserialize(base64_decode($evalComm['user']));
	           $usertextArr = $usersText;
	           foreach($usersText as $uIndex => $val)
	           {
                   $usertextStr .= $val['firstname']." ".$val['lastname']." ,";
	           }
	        } 	        
	        
	        $evaluationCommittee[$evalComm['id']] = array("title"    => $evalComm['title'],
	                                                      "users"    => str_replace(",", "<br />", $userStr." ".$usertextStr),
	                                                      "status"   => $evalComm['status'],
	                                                      "ref"      => $evalComm['id'],
	                                                      "userids" => $userIds, 
	                                                      "usertext" => $usertextArr
	                                                     );
	    }
        return $evaluationCommittee;
	}
     */
    function _getUserList($users)
    {
        $userlist = array();
        foreach($users as $index => $user)
        {
          $userlist[$user['tkid']] = $user['tkname']." ".$user['tksurname'];
        }
        return $userlist; 
    }

	function saveCommittee( $data )
	{
	    $evalObj = new Evaluationcommittee();
	    $res     = $evalObj -> save_evaluation_committee($data);
        return Message::save( $res, " evaluation committee ref #".$res );
	}
	
	function edit_committee($data)
	{
         $committeeObj = new Evaluationcommittee();
         $res          = $committeeObj -> update_committee($data);          
	    return Message::update( $res, " evaluation committee ref #".$data['data']['id'] );
	
	}
	
	function updateCommittee($data)
	{
		$res = Evaluationcommittee::updateWhere(array("status" => $data['status']), array("id" => $data['id']), new SetupAuditLog());
		return Message::update( $res, " evaluation committee ref #".$data['id'] );	    
	}

	
	
}
