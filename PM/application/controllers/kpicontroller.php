<?php
class KpiController extends Controller
{
	private $actionInfo = array(); 
	
	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$this->template->view("kpi");
	}
	
	function addkpi($options)
	{
	   $userdata                  = User::getInstance() -> getUser($options);
	   $evaluationyear            = ($options['evaluationyear'] ? $options['evaluationyear'] : $userdata['default_year']);
	   $data['header']            = Naming::getHeaderList();
	   $data['kpa']               = Kpa::findById($options['kpaid']);
	   $data['component']         = Component::findById($options['componentid']);
	   $data['componentid']       = $options['componentid'];
	   $data['userdata']          = $userdata;
	   $data['evaluationyear']    = $evaluationyear;
	   $data['year']              = EvaluationYear::findById($evaluationyear);
       $data['type']              = $options['componentid'];
       $data['section']           = (isset($options['folder']) ? $options['folder'] : 'new');
       $this->template->scripts   = array("kpi.js");
       $this->template->pagetitle = "Add New KPI";
       $this->template->kpiForm   = $this->template->setTemplate("kpi", $data);          
	   $this->template->view("kpi/addkpi");
	}

	function edit($type)
	{
	  $this->template->header = Naming::getHeaderList();
	   $this->template->kpa = Kpa::findById($type['kpaid']);
	   $this->template->component = Component::findById($type['componentid']);
	   $this->template->componentid = $type['componentid'];
	   $userdata                  = User::getInstance() -> getUser();
	   $this->template->evaluationyear = $userdata['default_year'];
	   $this->template->kpi = Kpi::findById($type['id']);
	   $this->template->weighting  = Weighting::fetchAll();
	   $this->template->nationalkpa  = Nationalkpa::findAll(" AND evaluationyear = '".$userdata['default_year']."' AND status & 2 <> 2");
	   $this->template->nationalobjectives  = Nationalobjectives::findAll(" AND evaluationyear = '".$userdata['default_year']."' AND status & 2 <> 2");
        $this->template->scripts = array("kpi.js");
        $this->template->pagetitle = "Edit KPI";
	   $this->template->view("kpi/editkpi");
	}	
	
	function setup_addkpi($type)
	{
	   $data['header'] = Naming::getHeaderList();
	   $data['kpa'] = Kpa::findById($type['kpaid']);
	   $data['component'] = Component::findById($type['componentid']);
	   $data['componentid'] = $type['componentid'];
	   $data['evaluationyear'] = $type['evaluationyear'];
	   $data['year'] = EvaluationYear::findById($type['evaluationyear']);
        $data['type'] = $type['componentid'];
        $data['section'] = "admin";	
        $this->template->kpiForm = $this->template->setTemplate("kpi", $data); 
        $this->template->scripts = array("kpi.js");
        $this->template->pagetitle = "Add New KPI";
	   $this->template->view("kpi/addkpi");
	}	
	
	function setup_edit($type)
	{
	   $this->template->header = Naming::getHeaderList();
	   $this->template->kpa = Kpa::findById($type['kpaid']);
	   $this->template->component = Component::findById($type['componentid']);
	   $this->template->componentid = $type['componentid'];
	   $this->template->evaluationyear = $type['evaluationyear'];	
	   $this->template->kpi = Kpi::findById($type['id']);
	   $this->template->weighting = Weighting::fetchAll();
	   $this->template->nationalkpa = Nationalkpa::findAll(" AND evaluationyear = '".$type['evaluationyear']."' AND status & 2 <> 2");
	   $this->template->nationalobjectives = Nationalobjectives::findAll(" AND evaluationyear = '".$type['evaluationyear']."' AND status & 2 <> 2");
        $this->template->scripts = array("kpi.js");
        $this->template->pagetitle = "Edit KPI";
	   $this->template->view("admin/editkpi");
	}			

	function kpimapping($type)
	{
        $this->template->scripts = array("kpimapping.js");
        $this->template->pagetitle = "KPI Mapping";
	   $this->template->view("admin/kpi/kpimapping");
	}				
	
	function getAll($options = array())
	{
	   $userdata           = User::getUser($options);
	   $kpiObj             = new Kpi(); 
	   $data               = $kpiObj -> get_kpi($userdata, $options);
        return $data;
	} 
	
	function getKpiEvaluationData($options= array())
	{
	     $options['headers'] = array('id', 'owner', 'deadline', 'name', 'progress');
	     $userdata           = User::getUser($options);
	     $kpiObj             = new Kpi(); 
	     $data               = $kpiObj -> get_kpi($userdata, $options);
	     //$data['evaluationStatuses']  = Evaluation::getEvaluationStatuses($data['data'], $userdata, $options['componentid']);	
	     //$data['nextEvaluation']  = Evaluation::nextEvaluation();	        	     
	     return $data;
	}

	function saveKpi($data)
	{	
        parse_str($data['data'], $kpiData );

	   $kpa   = Kpa::findById($data['kpaid']);
        $mapping = array();
        if(!empty($data['modulemapping']))
        {
            parse_str($data['modulemapping'], $mapping);
            $kpiData['modulemapping'] = serializeEncode($mapping);
        }	    
	   unset($kpiData['parent']);
	   if(!Validation::dateDiff($kpiData['deadline'], $kpa['deadline']))
	   {
	       return array("error" => true , "text" => "KPI deadline cannot be after the KPA deadline date");
	   } else {
            if(isset($kpiData['kpiactions']))
            {
               if($kpiData['kpiactions'] == "N")
               {
                  $kpiData['kpistatus'] = Kpi::ACTIVE + Kpi::KPIACTION_IMPORTED;
               }
               unset($kpiData['kpiactions']);
            }
            if(!isset($kpiData['owner']) || empty($kpiData['owner']))
            {
               $kpiData['owner'] = $_SESSION['tid'];
            }
		  unset($kpiData['evaluationyear']);
		  $kpiData['kpaid'] = $data['kpaid'];
		  $res = Kpi::saveData($kpiData);
		  return Message::save($res ," KPI #".$res );
        }
	}
	
	function editKpi($data)
	{
         parse_str($data['data'], $kpiData );	
         unset($kpiData['parent']);	
	    $kpa    = Kpa::findById($kpiData['kpaid']);
	    
	    if(!Validation::dateDiff($kpiData['deadline'],$kpa['deadline']))
	    {
	        return array("error" => true , "text" => "KPI deadline cannot be after the KPA deadline date(".$kpa['deadline'].")");
	    } else {		
		    $res = Kpi::updateWhere($kpiData, array("id" => $data['kpiid']), new KpiAuditLog());
		    return Message::update( $res, " KPI #".$data['kpiid'] ); 
		}		
	}
	
	function update($data)
	{
	    $kpi = Kpi::findById($data['id']);
	    if(!Validation::dateDiff($data['remindon'],$kpi['deadline']))
	    {
	      return array("error" => true, "text" => "Remind on date cannot be after the KPI deadline date ".$kpi['deadline']);
	    } else {	
          $res = Kpi::updateWhere(array("status" => $data['status'], "remindon" => $data['remindon']), array('id' => $data['id']), new KpiAuditLog());
          return Message::update($res , "KPI #".$data['id']);		  
         }
	}	
	
	function updateStatus($data)
	{
	  $res = Kpi::updateWhere(array("kpistatus" => $data['kpistatus']), array("id" => $data['id']));
	  if($res > 0)
	  {
	     $actions = Action::findAllByKpiid($data['id']);
	     if(!empty($actions))
	     {
	        Action::deleteKpiActions($actions);
	     }
	  }
	  return Message::update($res, "KPI #".$data['id']);
	}
	
	function updatekpi($data)
	{
	   $this->template->header = Naming::getHeaderList();
	   $this->template->kpa = Kpa::findById($data['kpaid']);
	   $this->template->component = Component::findById($data['componentid']);
	   $this->template->componentid = $data['componentid'];
	   $this->template->evaluationyear = $data['evaluationyear'];	
	   $this->template->kpi = Kpi::findById($data['id']);
	   $this->template->weighting  = Weighting::fetchAll();
	   $this->template->nationalkpa  = Nationalkpa::findAll();
	   $this->template->nationalobjectives  = Nationalobjectives::findAll();
	   $this->template->kpistatus  = Kpistatus::getStatusInUpdateOrder();
        $this->template->scripts = array("kpi.js");
        $this->template->pagetitle = "Update KPI";
	   $this->template->view("kpi/updatekpi");
	}
	
	function evaluatekpi($data)
	{
	   $this->template->header = Naming::getHeaderList();
	   $this->template->kpa = Kpa::findById($data['kpaid']);
	   $this->template->component = Component::findById($data['componentid']);
	   $this->template->componentid = $data['componentid'];
	   $this->template->evaluationyear = $data['evaluationyear'];	
	   $this->template->kpi = Kpi::findById($data['id']);
	   $this->template->weighting  = Weighting::fetchAll();
	   $this->template->nationalkpa  = Nationalkpa::findAll();
	   $this->template->nationalobjectives  = Nationalobjectives::findAll();
        $this->template->scripts = array("kpi.js");
        $this->template->pagetitle = "Evaluate KPI";
	   $this->template->view("kpi/evaluatekpi");
	}	
	
	function approvekpi($data)
	{
	   $this->template->header = Naming::getHeaderList();
	   $this->template->kpa = Kpa::findById($data['kpaid']);
	   $this->template->component = Component::findById($data['componentid']);
	   $this->template->componentid = $data['componentid'];
	   $this->template->evaluationyear = $data['evaluationyear'];	
	   $this->template->kpi = Kpi::findById($data['id']);
	   $this->template->weighting  = Weighting::fetchAll();
	   $this->template->nationalkpa  = Nationalkpa::findAll();
	   $this->template->nationalobjectives  = Nationalobjectives::findAll();
        $this->template->scripts = array("kpi.js");
        $this->template->pagetitle = "Approve KPI";
	   $this->template->view("kpi/approvekpi");
	}	
	
	function assurancekpi($data)
	{
	   $this->template->header          = Naming::getHeaderList();
	   $this->template->kpa             = Kpa::findById($data['kpaid']);
	   $this->template->component       = Component::findById($data['componentid']);
	   $this->template->componentid     = $data['componentid'];
	   $this->template->evaluationyear  = $data['evaluationyear'];	
	   $this->template->kpi             = Kpi::findById($data['id']);
	   $this->template->weighting       = Weighting::fetchAll();
	   $this->template->nationalkpa     = Nationalkpa::findAll();
	   $this->template->nationalobjectives  = Nationalobjectives::findAll();
        $this->template->scripts        = array('ajaxfileupload.js', 'kpi.js', 'jquery.assurance.kpi.js');
        $this->template->pagetitle      = "KPI Assurance";
	   $this->template->view("kpi/assurancekpi");
	}	
	
    function save_evaluation($data)
    {
        $evaluationObj = new KPIEvaluation();
        $userdata      = User::getUser($data);  
        $result        =  $evaluationObj -> save_evaluation($data, $userdata);        
        return $result;
    }     
   		
}
