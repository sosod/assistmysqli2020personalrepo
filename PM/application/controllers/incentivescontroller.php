<?php
class IncentivesController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function setup()
	{
        $this->template->scripts = array("incentives.js", "jscolor.js");
        $this->template->pagetitle = "Incentives/Bonus Parameters";	
	   $this->template->view("setup/incentives");
	}
	
	function getAll($options = "")
	{
		$sqlOption = $this->sqlOption($options);
		$sqlOption .= " AND status & 2 <> 2";
		$used 	   = array();
		$inArr = array();
		$results = Incentives::findAll($sqlOption);
		foreach( $results as $index => $incentive)
		{
			if( in_array($incentive['id'], $used))
			{
				$incentive['used'] = true;	
			} else {
				$incentive['used'] = false;
			}
			$inArr[$index] = $incentive;
		} 
		return $inArr;
	}
	
	function saveIncentive( $data )
	{
		$res = Incentives::saveData( $data['data'] );
		return Message::save($res, " incentives/bonus parameter #".$res); 
	}
	
	function updateIncentive($data)
	{
	    $incentiveObj = new Incentives();
	    $res          = $incentiveObj -> update_incentives($data);
	    //$res  = Incentives::updateWhere($data['data'], array("id" => $data['id']));
	    return Message::update($res, " incentives/bonus parameter #".$data['id']);
	}

}
