<?php
class MenuController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function setup()
	{
       $this->template->scripts   = array("menu.js");
       $this->template->pagetitle = "Menu Headings";     
       $this->template->view("setup/menu");   
	}	
	
	function getMenu()
	{
       $response = $this->Model->findAll();
       return $response;	     
	}
	
	function updateMenu($data) 
	{
        $res = $this->Model->updateWhere($data, array("id" => $data['id']) );
	   return Message::update($res, "menu ref #".$data['id']); 
	}
}	
?>	
