<?php
class GlossaryController extends Controller
{

     public function __construct()
     {
          parent::__construct();
     }     
     
     function index()
     {
        $glossaries = Glossary::findAll(); 
        $this -> template -> glossaries = Glossary::findAll();     
        $headers = self::_glossaryheaders();
        $fields = $headers;
        foreach($glossaries as $index => $glossary)
        {
           if($headers[$glossary['field']])
           {
               unset($fields[$glossary['field']]);
           }
        }
        $this -> template -> fields = $fields; 
        $this -> template -> headers = $headers;
        $this -> template -> pagetitle = "Glossary";
        $this -> template -> pagetitle = "Glossary";
        $this -> template -> view("glossary/glossary");
     }
     
     function _glossaryheaders()
     {
        $headers = Naming::getHeaderList();
        $components = Component::findAll();
        foreach($components as $index => $component)
        {
          $headers[$component['id']] = $component['description'];
        }
        return $headers;
     }
     
     function setup()
     {
        $glossaries = Glossary::findAll(); 
        $this -> template -> glossaries = Glossary::findAll();     
        $headers = self::_glossaryheaders();
        $fields = $headers;
        foreach($glossaries as $index => $glossary)
        {
           if($headers[$glossary['field']])
           {
               unset($fields[$glossary['field']]);
           }
        }
        $this -> template -> fields = $fields; 
        $this -> template -> headers = $headers;
        $this -> template -> pagetitle = "Glossary";
        $this -> template -> scripts = array('glossary.js');
        $this -> template -> view("setup/glossary");
     }
     
     function define_glossary($field)
     {
         $headers = self::_glossaryheaders();
         $this -> template -> scripts = array('glossary.js');
         $this -> template -> fieldname =  (isset($headers[$field['field']]) ? $headers[$field['field']] : "");
         $this -> template -> pagetitle = "Add Glossary";
         $this -> template -> view("setup/define_glossary");
     }
     
     function edit_glossary($data)
     {
         $headers = self::_glossaryheaders();
         $glossary = Glossary::findById($data['id']);

         $this -> template -> glossary = $glossary;
         $this -> template -> scripts = array('glossary.js');
         $this -> template -> fieldname =  (isset($headers[$glossary['field']]) ? $headers[$glossary['field']] : "");
         $this -> template -> pagetitle = "Edit Glossary";
         $this -> template -> view("setup/edit_glossary");
     }
     
     function saveGlossary($data)
     {
        $res = Glossary::saveData($data['data']);     
        return Message::save($res, "glossary ");         
     }
     
     function updateGlossary($data)
     {
        $res = Glossary::updateWhere($data['data'], array("id" => $data['data']['id']));
        return Message::update($res, "glossary");
     }
     
     function deleteGlossary($data)
     {
        //$id = substr($data['data'], 7);
        $res = Glossary::deleteGlossary($data['data']);
        return Message::delete($res, "glossary");
     }
          
}

?>
