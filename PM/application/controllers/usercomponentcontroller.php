<?php
class UserComponentController extends Controller
{
     
     public function __construct()
     {
        parent::__construct();
     } 
	
	function confrimComponent($data)
	{         
	    $user = "";
	    if(isset($data['user']) && !empty($data['user']))
	    {
	       $user =  $data['user'];
	    } else {
	       $user = $_SESSION['tid'];
	    }
         $usercomponent = UserComponent::findAND(array("componentid" => $data['componentid'], "userid" => $user));
         $res = 0;
         if(!empty($usercomponent))
         {
            $res = UserComponent::updateWhere(array('status'    => $usercomponent['status'] + UserComponent::CONFIRMED,
                                                    'confirmed' => date('Y-m-d H:i:s')
                                                   ),
                                              array("componentid" => $data['componentid'], "userid" => $usercomponent['userid'])
                                             );    
         }
         return Message::update($res, " component ref #".$data['componentid']);
	}
	
	function activateComponent($options=array())
	{    
	    $userid = "";
	    if(isset($options['user']))
	    {
	      $userid = toUserId($options['user']); 
	    } else {
	      $userid = $_SESSION['tid'];
	    }
	    $usercomponentObj = new UserComponent();
         $res              = $usercomponentObj -> activate($userid);
	    return Message::update($res, " performance matrix ");
	}	

     function getUserAll($options = array())
     {
        $userdata = User::getInstance() -> getUser($options);
        $secError = FALSE; 
        $updatedComponents = array();


        if(!empty($userdata))
        {
           
           //setup the inital setup for this user        
          $done = UserSetting::initialSetupDone($userdata);
          if(is_bool($done))
          {
             if(!$done)
             {
               if(isset($options['section']) && $options['section'] == "new")
               {
                   Component::setupInitialSettings($userdata);
                   UserSetting::setupDone($userdata);              
               } else {
                  $secError = TRUE;
                  $components = array("text" => "Configure components for <b>".$userdata['tkname']." ".$userdata['tksurname']."</b> under the new section ", "error" => TRUE);  
               }
             }
          } else {
             $updatedComponents = $done;
          }
        }
        if($secError)
        {
          return $components;          
        } else {
          $componentObj                = new UserComponent();
          $components                  = $componentObj -> get_user_components($userdata, $options);
          $components['matrix_update'] = $updatedComponents;
          if(isset($_SESSION['evaluation_period']))
          {
             $components['evaluation_period'] = $_SESSION['evaluation_period'];
          }
          return $components;
        }
     }
     
     function updatePerfomanceMatrix($options)
     {
        $res = UserComponent::updateComponents($options['components'], $options['userdata']);    
     }
     
     function download($options = array())
     {
        $usercomponents = UserComponent::getComponentTesting($options);
        //debug($usercomponents);
        $pdf = new UserMatrixPdf($usercomponents);
        $pdf -> AliasNbPages();
        $pdf -> AddPage('', 'A3');
        $pdf -> MainHeader();
        $pdf -> Main();
        $pdf -> Output();
     }
     
     function get_creation_statuses_report($data)
     {
        $userComponentObj = new UserComponent();
        $creationStatuses = $userComponentObj -> get_creation_status($data);
        echo json_encode($creationStatuses);
        exit();
     }
     
     function get_confirmed_matrix_report($data)
     {
        $userComponentObj = new UserComponent();
        $creationStatuses = $userComponentObj -> get_matrix_confirmed_report($data);
        return $creationStatuses;
     }     
     
     function notify_manager($data)
     {
         $emailObj      = new EvaluationPhaseComplete();
         $userObj       = new User();
         $evaluationObj = new Evaluation();
         $userdata      = $userObj -> get_user($data);
         $response      = array(); 
         $res           = $evaluationObj -> save_user_evaluations($data);
         if($res > 0)
         {
             if($options['prev_eval'] == "self")
             {
                $manager['user'] = $userdata['managerid'];
                $managerdata     = $userObj -> get_user($manager);
                $res             = $emailObj -> sendSelfEvaluationEmail($userdata, $managerdata); 
                if($res)
                {
                   $response = array('text' => 'Self evaluation email successfully sent', 'error' => false);
                } else {
                   $response = array('text' => 'Error sending the email', 'error' => false);
                }
             } else if($options['prev_eval'] == "manager") {       
                $res          = $emailObj -> sendManagerEvaluationEmail($userdata);
                if($res)
                {
                   $response = array('text' => 'Manager evaluation email successfully sent', 'error' => false);
                } else {
                   $response = array('text' => 'Error sending the email', 'error' => true);
                }            
             }
         } else {
            $response = array('text' => 'An error occured saving the overall comment, please try again', 'error' => false);   
         }
         return $response;
     }
}
?>
