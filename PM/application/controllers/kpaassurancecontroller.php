<?php
class KpaAssuranceController extends Controller
{
    
    function __construct()
    {
      parent::__construct();
    }
   
   	function get_kpa_assurance($options = array())
	{
	   $assuranceObj = new KpaAssurance();
	   $assurance    = $assuranceObj -> get_assurances($options);
	   return $assurance;
	    
	}
	
    function save($data)
    {  
       if(isset($_SESSION['uploads']))
       {
          if(isset($_SESSION['uploads']['pmadded']))
          {
            if(isset($_SESSION['uploads']['pmadded']['kpa_assurance_'.$data['kpa_id']])  &&
              !empty($_SESSION['uploads']['pmadded']['kpa_assurance_'.$data['kpa_id']])
            )
            {
               $data['attachment'] = base64_encode(serialize($_SESSION['uploads']['pmadded']['kpa_assurance_'.$data['kpa_id']]));
               unset($_SESSION['uploads']['pmadded']['kpa_assurance_'.$data['kpa_id']]);
            }
          }
       }
	   $assuranceObj = new KpaAssurance();
	   $res          = $assuranceObj -> save_assurance($data);
	   return Message::save($res, "KPA assurance #".$res);	        
    }
    
    function update($data)
    {
       $assuranceObj = new KpaAssurance();
       $kpaassurance = $assuranceObj -> get_assurance($data['id']) ;
       Attachment::processAttachmentChange($kpaassurance['attachment'], "kpa_assurance_".$kpaassurance['kpa_id'], "kpa");
       if(isset($_SESSION['uploads']))
       {
          if(isset($_SESSION['uploads']['attachments']))
          {
             $data['attachment'] = $_SESSION['uploads']['attachments'];
          }
       }
	   $assuranceObj = new KpaAssurance();
	   $res          = $assuranceObj -> update_assurance($data['id'], $data);
	   if(isset($_SESSION['uploads']))
	   {
          if(isset($_SESSION['uploads']['pmadded']))
          {
            if(isset($_SESSION['uploads']['pmadded']['kpa_assurance_'.$kpaassurance['kpa_id']]))
            {
              unset($_SESSION['uploads']['pmadded']['kpa_assurance_'.$kpaassurance['kpa_id']]);
            }   
          }
          if(isset($_SESSION['uploads']['pmdeleted']))
          {
            if(isset($_SESSION['uploads']['pmdeleted']['kpa_assurance_'.$kpaassurance['kpa_id']]))
            {
               unset($_SESSION['uploads']['pmdeleted']['kpa_assurance_'.$kpaassurance['kpa_id']]);
            }   
          }		     
          if(isset($_SESSION['uploads']['actionchanges']))
          {
            unset($_SESSION['uploads']['actionchanges']);
          }
          if(isset($_SESSION['uploads']['attachments']))
          {
            unset($_SESSION['uploads']['attachments']);
          }
	   } 
	   return Message::update($res, "KPA assurance #".$res);       
    }
    
    function delete()
    {
    
    
    }
	
	function download_file($data)
	{
	   $file  = $data['file'];
	   $name  = $data['name'];
	   $type  = $data['type'];
	   $ext   = substr($file, strpos($file, ".") + 1);
       header("Content-type:".$ext);
       header("Content-disposition: attachment; filename=".$name);
       readfile("../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$type."/".$file);  
	   exit();
	}
	
    function upload_attachment($data)
    {
        $id       = $data['id'];
	    $attObj   = new Attachment('kpa_assurance_'.$id, 'attach_kpa_'.$id); 
	    $results  = $attObj -> upload('kpa_assurance_'.$id);
        echo json_encode($results);
        exit();
    }
    
    function delete_attachment($action_info)
    {
	   $file      = $action_info['attachment'];
	   $action_id = $action_info['id'];
	   $attObj    = new Attachment('kpa_assurance_'.$action_id, 'attach_kpa_'.$action_id);
	   $response  = $attObj -> deleteFile($file, 'kpa_assurance_'.$action_id);
       echo json_encode($response);
       exit();
    }
    
    function remove_attachment($action_info)
    {
	    $action_id= $action_info['action_id'];
	    $file     = $action_info['attachment'].".".$action_info['ext'];
	    $attObj   = new Attachment('kpa_assurance_'.$action_id, 'attach_kpa_'.$action_id);
	    $response = $attObj -> removeFile($file, 'kpa_assurance_'.$action_id);
        echo json_encode($response);
        exit();
    }
}
?>
