<?php
class ActionController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	function view($data)
	{
        $this->template->header    = Naming::getHeaderList();
        $this->template->componentid = (isset($data['componentid']) ? $data['componentid'] : "");
        $this->template->kpi       = Kpi::findById($data['kpiid']);
        $this->template->nationalkpa = Nationalkpa::findAll();
        $this->template->nationalobjectives = Nationalobjectives::findAll();
        $this->template->competency = Competency::fetchAll();
        $this->template->proficiency    = Proficiency::fetchAll();
        $this->template->weighting = Weighting::fetchAll();
        $this->template->action       = Action::findById($data['id']);
        $this->template->actionstatus = Actionstatus::getStatusInUpdateOrder();
        $this->template->scripts   = array("action.js");
        $this->template->pagetitle = "Update Action";	
        $this->template->view("action/updateaction");
	}		
	
	function addaction($data)
	{
	   $data['header']            = Naming::getHeaderList();
	   $data['type']              = $data['componentid'];
	   $userdata                  = User::getInstance() -> getUser($data);
	   $data['userdata']          = $userdata;
	   $data['evaluationyear']    = $userdata['default_year'];
	   $data['year']              = EvaluationYear::findById($userdata['default_year']);
	   $data['component']         = Component::findById($data['componentid']);
	   $matrixObj                 = new Performancematrix();
        $settings                  = $matrixObj -> get_matrix_setting($userdata['categoryid'], $userdata['default_year'], $data['componentid'],
                                                           array('use_competincies', 'use_proficiencies', 'use_action_weighting')
                                                          );
        $data['useCompetencies']   = ($data['folder'] == "admin" ? true : (boolean)$settings['use_competincies']);
        $data['useProficiencies']  = ($data['folder'] == "admin" ? true : (boolean)$settings['use_proficiencies']);
	   $data['kpa']               = Kpa::findById($data['kpaid']);
	   $data['kpi']               = Kpi::findById($data['kpiid']);
	   $data['section']           = $data['folder'];
        $this->template->scripts   = array("action.js");
        $this->template->pagetitle = "Add New Action";	
        $this -> template -> actionForm = $this -> template -> setTemplate("action", $data);
	   $this->template->view("action/addaction");
	}
	
	/*
	function setup_addaction($data)
	{
	   $this->template->header = Naming::getHeaderList();
	   $this->template->type = $data['componentid'];	   
	   $this->template->evaluationyear = $data['evaluationyear'];
	   $this -> template -> year = EvaluationYear::findById($data['evaluationyear']);
	   $this->template->component = Component::findById($data['componentid']);
	   $this->template->kpa = Kpa::findById($data['kpaid']);
	   $this->template->kpi = Kpi::findById($data['kpiid']);	    
        $this->template->scripts   = array("action.js");
        $this->template->pagetitle = "Add New Action";	
        $this->template->view("admin/addaction");
	}	
	*/
	function setup_editaction($data)
	{
        $this->template->header    = Naming::getHeaderList();
        $this->template->type = $data['componentid'];
	   $this -> template -> year = EvaluationYear::findById($data['evaluationyear']);        
        $this->template->kpi       = Kpi::findById($data['kpiid']);
        $this->template->nationalkpa = Nationalkpa::findAll();
        $this->template->nationalobjectives = Nationalobjectives::findAll();
        $this->template->competency = Competency::fetchAll();
        $this->template->proficiency    = Proficiency::fetchAll();
        $this->template->weighting = Weighting::fetchAll();
        $this->template->action       = Action::findById($data['id']);	    
        $this->template->scripts   = array("action.js");
        $this->template->pagetitle = "Edit Action";	
        $this->template->view("admin/edit_action");
	}	
	
	function edit_action($data)
	{
        $this->template->header    = Naming::getHeaderList();
        $this->template->type = $data['componentid'];
        
   	   $userdata                  = User::getInstance() -> getUser();
	   $settings = Performancematrix::getASetting($userdata['categoryid'], $userdata['default_year'], array("usecompetencies", "useproficiencies"));
        $this -> template -> useCompetencies = $settings['usecompetencies'][$data['componentid']];
        $this -> template -> useProficiencies = $settings['useproficiencies'][$data['componentid']];        
        $this->template->kpi       = Kpi::findById($data['kpiid']);
        $this->template->nationalkpa = Nationalkpa::findAll();
        $this -> template -> year = EvaluationYear::findById($userdata['default_year']);   
        $this->template->nationalobjectives = Nationalobjectives::findAll();
        $this->template->competency = Competency::fetchAll();
        $this->template->proficiency    = Proficiency::fetchAll();
        $this->template->weighting = Weighting::fetchAll();
        $this->template->action       = Action::findById($data['id']);
        $this->template->scripts   = array("action.js");
        $this->template->pagetitle = "Edit Action";	
        $this->template->view("action/edit_action");
	}		
	
	function getAll($options = array())
	{

        //$options['headers'] = $headers  = array("owner", "deadline", "name", "status", "progress");
	   $userdata  = User::getUser($options);
	   $actionObj = new Action();
	   $data      = $actionObj -> get_actions($options, $userdata); 
	   return $data;
	}
	
	function getActionEvaluationData($options = array())
	{
	   $options['headers'] = array('id', 'owner', 'deadline', 'name', 'progress');
	   //$data = Action::getActions($options);
	   $userdata  = User::getUser($options);
	   $actionObj = new Action();
	   $data      = $actionObj -> get_actions($options, $userdata); 
	   
	   //$data['evaluationStatuses']  = Evaluation::getEvaluationStatuses($data['data'], $userdata, $options['componentid']);	 
	   //$data['nextEvaluation']  = Evaluation::nextEvaluation();	   
	   //debug($nextEvaluation);
	   return $data;
	}
	
	function saveAction( $data )
	{
		parse_str( $data['data'], $actionData);
		unset($actionData['parent']);
		$kpi = Kpi::findById($actionData['kpiid']);
		
		if(!Validation::dateDiff($actionData['deadline'], $kpi['deadline']))
		{
           return array("error" => true, "text" => "Action deadline date cannot be greater/after the kpi deadline date"); 		
		} else {
		    $actionData['kpiid']  = $data['kpiId'];
		    unset($actionData['evaluationyear']);
		    $id = Action::saveData($actionData);
		    return Message::save( $id, "action #".$id);		
		}
	}
	
	function editAction( $data )
	{
		parse_str( $data['data'], $actionData );	
		unset($actionData['parent']);
		$kpi = Kpi::findById($actionData['kpiid']);	
		if(!Validation::dateDiff($actionData['deadline'], $kpi['deadline']))
		{
           return array("error" => true, "text" => "Action deadline date cannot be greater/after the kpi deadline date(".$kpi['deadline'].")"); 		
		} else {	
		    $res = Action::updateWhere($actionData, array("id" => $data['actionid']), new ActionAuditLog());
		    return Message::update($res, "action #".$data['actionid']);
		}
	}
	
	function update($data)
	{
	      $action = Action::findById($data['id']);
          if(!Validation::dateDiff($data['remindon'], $action['deadline']))
          {
               return array("error" => true, "text" => "Remind on date cannot be after the action deadline date ".$action['deadline']);
          } else {
              $updatedata['status']   = $data['status'];
              $updatedata['progress'] = $data['progress'];
              $updatedata['remindon'] = $data['remindon'];
              $updatedata['actionon'] = $data['actionon'];
              if($data['progress'] == 100 && $data['status'] == 3)
              {
                $updatedata['actionstatus'] = $action['actionstatus'] + Action::AWAITING;
              }
              if(isset($data['requestapproval']) && $data['requestapproval'] == 1)
              {
                 $kpiObj         = new Kpi();
                 $kpi            = $kpiObj -> get_a_kpi(" AND id = ".$action['kpiid']." ");
                 $emailObj       = new RequestApproval();
                 $data['action'] = $action['name'];
                 $emailObj -> sendEmail($data, $kpi);
              }
              
		      if(isset($_SESSION['uploads']))
		      {
		         if(isset($_SESSION['uploads']['pmadded']))
		         {
		             if(isset($_SESSION['uploads']['pmadded']['action_'.$data['id']]))
		             {
			            $updatedata['attachment'] = base64_encode(serialize($_SESSION['uploads']['pmadded']['action_'.$data['id']]));
		             }		            
		         }
		         
		         if(isset($_SESSION['uploads']['pmdeleted']))
		         {
		             if(isset($_SESSION['uploads']['pmdeleted']['action_'.$data['id']]))
		             {
			            //$updatedata['attachment'] = $_SESSION['uploads']['attachments'];
		             }			            
		         }		         
		      }
              $res = Action::updateWhere($updatedata, array('id' => $data['id']), new ActionAuditLog());
		      if(isset($_SESSION['uploads']))
		      {
		         if(isset($_SESSION['uploads']['pmadded']))
		         {
		            if(isset($_SESSION['uploads']['pmadded']['action_'.$data['id']]))
		            {
		                unset($_SESSION['uploads']['pmadded']['action_'.$data['id']]);
		            }   
		         }
		         if(isset($_SESSION['uploads']['pmdeleted']))
		         {
		            if(isset($_SESSION['uploads']['pmdeleted']['action_'.$data['id']]))
		            {
		                unset($_SESSION['uploads']['pmdeleted']['action_'.$data['id']]);
		            }   
		         }		     
		         if(isset($_SESSION['uploads']['actionchanges']))
		         {
		            unset($_SESSION['uploads']['actionchanges']);
		         }
		         if(isset($_SESSION['uploads']['attachments']))
		         {
		            unset($_SESSION['uploads']['attachments']);
		         }
		       }              
              return Message::update($res , "Action #".$data['id']);		  
          }
	}
	
	function updateStatus($data)
	{
	   return Message::update(Action::updateWhere(array("actionstatus" => $data['actionstatus']), array("id" => $data['id']) ), "action #".$data['id']);
	}
	
	function updateaction($data)
	{
        $this->template->header    = Naming::getHeaderList();
        $this->template->componentid = (isset($data['componentid']) ? $data['componentid'] : "");
        $this->template->kpi       = Kpi::findById($data['kpiid']);
        $this->template->nationalkpa = Nationalkpa::findAll();
        $this->template->nationalobjectives = Nationalobjectives::findAll();
        $this->template->competency = Competency::fetchAll();
        $this->template->proficiency    = Proficiency::fetchAll();
        $this->template->weighting = Weighting::fetchAll();
        $this->template->action       = Action::findById($data['id']);
        $this->template->actionstatus = Actionstatus::getStatusInUpdateOrder();
        $this->template->scripts   = array("ajaxfileupload.js", "action.js");
        $this->template->pagetitle = "Update Action";	
        $this->template->view("action/updateaction");
	}		
				
	function evaluateaction($data)
	{
        $this->template->header    = Naming::getHeaderList();
        $this->template->componentid = $data['componentid'];
        $this->template->kpi       = Kpi::findById($data['kpiid']);
        $this->template->nationalkpa = Nationalkpa::findAll();
        $this->template->nationalobjectives = Nationalobjectives::findAll();
        $this->template->competency = Competency::fetchAll();
        $this->template->proficiency    = Proficiency::fetchAll();
        $this->template->weighting = Weighting::fetchAll();
        $this->template->action       = Action::findById($data['id']);
        $this->template->scripts   = array("action.js");
        $this->template->pagetitle = "Evaluate Action";	
        $this->template->view("action/evaluateaction");
	}		
					
	function assuranceaction($data)
	{
        $this->template->header         = Naming::getHeaderList();
        $this->template->componentid    = $data['componentid'];
        $this->template->kpi            = Kpi::findById($data['kpiid']);
        $this->template->nationalkpa    = Nationalkpa::findAll();
        $this->template->nationalobjectives = Nationalobjectives::findAll();
        $this->template->competency     = Competency::fetchAll();
        $this->template->proficiency    = Proficiency::fetchAll();
        $this->template->weighting      = Weighting::fetchAll();
        $this->template->action         = Action::findById($data['id']);
        $this->template->scripts        = array("action.js", "jquery.assurance.action.js");
        $this->template->pagetitle      = "Assurance Action";	
        $this->template->view("action/assuranceaction");
	}
	
	function approveaction($data)
	{
        $this->template->header         = Naming::getHeaderList();
        $this->template->componentid    = $data['componentid'];
        $this->template->kpi            = Kpi::findById($data['kpiid']);
        $this->template->nationalkpa    = Nationalkpa::findAll();
        $this->template->nationalobjectives = Nationalobjectives::findAll();
        $this->template->competency     = Competency::fetchAll();
        $this->template->proficiency    = Proficiency::fetchAll();
        $this->template->weighting      = Weighting::fetchAll();
        $this->template->action         = Action::findById($data['id']);
        $this->template->scripts        = array("jquery.ui.approve.js");
        $this->template->pagetitle      = "Approve Action";	
        $this->template->view("action/approveaction");
	}	
	
	function approvalActions($options = array())
	{
	   $headerList         = Naming::getHeaderList();
	   $options['headers'] = array('id', 'owner', 'deadline', 'name', 'status', 'progress'); 
	   $headers            = array();
	   foreach($options['headers'] as $index => $head)
	   {
	     if(isset($headerList[$head]))
	     {
	       $headers[$head] = $headerList[$head];
	     } else {
	        $headers[$head] = ucwords($head);
	     }
	   }
	   $approvedActions = Action::getApprovedActions($options);
       $awaitingActions = Action::getAwaitingActions($options);	   
	   $actions         = array("awaiting" => $awaitingActions, "approved" => $approvedActions, "headers" => $headers);
	   return $actions;
	}		
		
     function approve($options)
     {
        $actionObj = new Action();
        $result    = $actionObj -> approveAction($options);
        return $result;
     }				
     
     function decline($options)
     {
        $result = Action::declineAction($options);
        return $result;
     }				     	
							
    function save_evaluation($data)
    {
        $evaluationObj = new ActionEvaluation();
        $userdata      = User::getUser($data);  
        $result        =  $evaluationObj -> save_evaluation($data, $userdata);        
        return $result;
    }     										
					
    function upload_attachment($action)
    {
        $action_id = $action['action_id'];
	    $attObj    = new Attachment('action_'.$action_id, 'action_attachment_'.$action_id); 
	    $results   = $attObj -> upload('action_'.$action_id);
        echo json_encode($results);
        exit();
    }
	
	function download_file($file, $name)
	{
	   $ext = substr($file, strpos($file, ".") + 1);
       header("Content-type:".$ext);
       header("Content-disposition: attachment; filename=".$name);
       readfile("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/action/".$file);  
	   exit();
	}
	
	function removeattachment($action_info)
	{
	    $action_id= $action_info['action_id'];
	    $file     = $action_info['attachment'].".".$action_info['ext'];
	    $attObj   = new Attachment('action_'.$action_id, 'action_attachment_'.$action_id);
	    $response = $attObj -> removeFile($file, 'action_'.$action_id);
        echo json_encode($response);
        exit();
	}

	function delete_attachment($action_info)
	{
	   $file      = $action_info['attachment'];
	   $action_id = $action_info['action_id'];
	   $attObj    = new Attachment('action_'.$action_id, 'action_attachment_'.$action_id);
	   $response  = $attObj -> deleteFile($file, 'action_'.$action_id);
       echo json_encode($response);
       exit();
	}    
    					
}
