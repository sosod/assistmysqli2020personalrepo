<?php
class EvaluationfrequenciesController extends Controller
{

	function __construct()
	{
		parent::__construct();
	}
	
	function setup()
	{
        $this->template->scripts = array("evaluationfrequencies.js");
        $this->template->pagetitle = "Evaluation Frequencies";
	   $this->template->view("setup/evaluationfrequencies");
	}

	
	function employeespecific()
	{
        $this->template->scripts = array("evaluationfrequencies.js");
        $this->template->pagetitle = "Employee Specifics";
	   $this->template->view("admin/employeespecific");	
	} 			
		
	function evaluationperiods()
	{
        $this->template->scripts = array("evaluationfrequencies.js");
        $this->template->pagetitle = "Evaluation Periods";
	   $this->template->view("admin/evaluationperiods");		
	}

	function getAll($options = array())
	{
		$eveluationFreqObj 	   = new Evaluationfrequencies();
		$evaluationfrequencies = $eveluationFreqObj -> get_evaluation_frequencies($options);
		return $evaluationfrequencies;
	}
	
	function saveEvalFreq( $data )
	{
		$id = Evaluationfrequencies::saveData( $data );
		return Message::save( $id, "evaluation frequency #".$id );
	}
	
	function updateFreq($data)
	{
	     $evalObj = new Evaluationfrequencies();
	     $res     = $evalObj -> update_evaluation_frequency($data);
		return Message::update( $res, "evaluation frequency #".$data['id'] ); 
	}
	
	function close($data)
	{
		$evalfreq = Evaluationfrequencies::findById($data['id']);
		//add closed status
		$updatedata = array("status" => $evalfreq['status'] + Evaluationyear::CLOSED );
		$res = Evaluationfrequencies::updateWhere($updatedata, array("id" => $data['id']) );
		return Message::update( $res, "evaluation period #".$data['id'] );
	} 
	
	function open($data)
	{
		$evalfreq = Evaluationfrequencies::findById($data['id']);
		//add closed status
		$updatedata = array("status" => ($evalfreq['status'] & ~Evaluationyear::CLOSED));
		$res = Evaluationfrequencies::updateWhere($updatedata, array("id" => $data['id']) );
		return Message::update($res, "evaluation period #".$data['id']);	
	}	
	
	
}
