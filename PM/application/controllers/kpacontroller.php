<?php
class KpaController extends Controller
{
	
	private $kpiArr     = array();
	
	private $actionInfo = array();

	function __construct()
	{
		parent::__construct();
	}
	
	function configure( $type )
	{
        $this->template->scripts = array("configure.js", "jquery.ui.componentmatrix.js");
        $this->template->pagetitle = "Configure Matrix";
	   $this->template->view("setup/configure");
	}
	
	function addkpa($options)
	{
	   $userdata                      = User::getInstance() -> getUser($options);
	   $data['year']                  = EvaluationYear::findById($userdata['default_year']);
	   $data['evaluationyear']        = $userdata['default_year'];
	   $data['userdata']              = $userdata;
	   $data['header']                = Naming::getHeaderList();
	   $component                     = Component::findById($options['componentid']);
	   $data['component']             = $component;
	   $data['section']               = "new";
	   $data['componentid']           = $options['componentid'];
        $this -> template -> scripts   = array("kpa.js");
        $this -> template -> pagetitle = "Add New KPA";	
        $this -> template -> kpaForm   = $this -> template -> setTemplate("kpa", $data);  
	   $this -> template -> view("kpa/addkpa");
	}	
	
	function editkpa($type)
	{
	   $this->template->header = Naming::getHeaderList();
	   $this->template->kpa = Kpa::findById($type['kpaid']);
	   $this->template->componentid = $type['componentid'];
	   $userdata                  = User::getInstance()->getUser();
	   $evaluationyear = $userdata['default_year'];
	   $this->template->evaluationyear = $evaluationyear;	   
	   $this->template->weighting = Weighting::fetchAll();
	   $this->template->nationalkpa   = Nationalkpa::findAll(" AND evaluationyear = '".$evaluationyear."' AND status & 2 <> 2");
	   $this->template->nationalobjectives = Nationalobjectives::findAll(" AND evaluationyear = '".$evaluationyear."' AND status & 2 <> 2");
        $this->template->scripts = array("kpa.js");
        $this->template->pagetitle = "Edit KPA";	
	   $this->template->view("kpa/editkpa");
	}	
	
	function admin_addkpa($options)
	{    
	   $userdata                  = User::getInstance() -> getUser($options);
	   $data['header']            = Naming::getHeaderList();
	   $data['year']              = EvaluationYear::findById($options['evaluationyear']);
	   $data['component']         = Component::findById($options['componentid']);
	   $data['componentid']       = $options['componentid'];
	   $data['evaluationyear']    = $options['evaluationyear'];
	   $data['section']           = $options['section'];
        $this->template->kpaForm   = $this -> template->setTemplate("kpa", $data); 
        $this->template->scripts   = array("kpa.js");
        $this->template->pagetitle = "Add New KPA";	
	   $this->template->view("admin/addkpa");
	}			
	
	function setup_editkpa($type)
	{
	   $this->template->header = Naming::getHeaderList();
	   $this->template->kpa = Kpa::findById($type['kpaid']);
	   $this->template->componentid = $type['componentid'];
	   $this->template->evaluationyear = $type['evaluationyear'];
	   $this->template->weighting = Weighting::fetchAll();	
	   $this -> template -> nationalkpa  = Nationalkpa::findAll(" AND evaluationyear = '".$type['evaluationyear']."' AND status & 2 <> 2");
	   $this->template->nationalobjectives  = Nationalobjectives::findAll(" AND evaluationyear = '".$type['evaluationyear']."' AND status & 2 <> 2");	
        $this->template->scripts = array("kpa.js");
        $this->template->pagetitle = "Edit KPA";	
	   $this->template->view("admin/editkpa");
	}	
	
	function getAll($options = array())
	{	
	   $userdata = User::getUser($options);
        $kpaObj   = new Kpa();
        $data     = $kpaObj -> get_kpa($userdata, $options);
	   return $data;
	} 
	
	function update_template_change($data)
	{

	   if(isset($data['kpa_change']) && !empty($data['kpa_change']))
	   {
	     $kpaObj = new Kpa();
	     if(isset($data['kpa_change']['added']) && !empty($data['kpa_change']['added']))
	     {
	        $kpaObj -> copy_from_template_save($data['kpa_change']['added'], $data['componentid'], $data['userdata']);   
	     } 
	     if(isset($data['kpa_change']['updated']) && !empty($data['kpa_change']['updated'])) 
	     {
	        $kpaObj -> update_template_changes($data['kpa_change']['updated']);
	     }
	   }
	   if(isset($data['kpi_change']) && !empty($data['kpi_change']))
	   {
	     $kpiObj = new Kpi();
	     if(isset($data['kpi_change']['kpi_added']) && !empty($data['kpi_change']['kpi_added']))
	     {
	       $kpiObj -> copy_from_template_save($data['kpi_change']['kpi_added'], $data['userdata']);   
	     } 
	     if(isset($data['kpi_change']['kpi_updated']) && !empty($data['kpi_change']['kpi_updated'])) 
	     {          
	        $kpiObj -> update_kpi_template_changes($data['kpi_change']['kpi_updated']);
	     }
	   }	   
	}
	
	function getEvaluationData($options = array())
	{
        $options['headers']  = array('id', 'owner', 'deadline', 'name', 'progress');
        $userdata            = User::getUser($options);
        $kpaObj              = new Kpa();
        $data                = $kpaObj -> get_kpa($userdata, $options);
        return $data;
        
	}
	
	function saveKpa($data)
	{    
	   parse_str($data['data'], $kpaData);
	   $kpaObj = new Kpa();
       $res    = $kpaObj -> save_kpa($kpaData, $data['componentid'], $data['section']);
       return $res;  
	}
	
	function updateStatus($data)
	{
	  $res = Kpa::updateWhere(array("kpastatus" => $data['kpastatus']), array("id" => $data['id']) );
	  if($res > 0)
	  {
	     $kpis = Kpi::findAllByKpaid($data['id']);
	     if(!empty($kpis))
	     {
	        Kpi::deleteKpaKpis($kpis);     
	     }
	  }
	  return Message::update($res, "KPA #".$data['id']);
	}
		
	function editKpaData($data)
	{
		$componentId = $data['componentid'];
		parse_str($data['data'], $kpaData );	    
		unset($kpaData['goaltype']);
		unset($kpaData['kpaid']);
		unset($kpaData['section']);
		unset($kpaData['parent']);
		$kpaData['componentid'] = $data['componentid'];
		$res = Kpa::updateWhere($kpaData, array('id' => $data['id']), new KpaAuditLog());
		return Message::update($res , "KPA #".$data['id']);		
	}
	
	function update($data)
	{
	     $kpa = Kpa::findById($data['id']);
	     if(!Validation::dateDiff($data['remindon'], $kpa['deadline']))
	     {
             return array("error" => true, "text" => "The remind on date cannot be after the deadline date ".$kpa['deadline'] );
	     } else {
             $res = Kpa::updateWhere(array("status" => $data['status'], "remindon" => $data['remindon']), array('id' => $data['id']), new KpaAuditLog());
             return Message::update($res , "KPA #".$data['id']);		  
          }
	}
	
	function updatekpa($data)
	{
          $this->template->header = Naming::getHeaderList();
          $this->template->kpa = Kpa::findById($data['kpaid']);
          $this->template->componentid = $data['componentid'];
          $this->template->evaluationyear = $data['evaluationyear'];
          $this->template->weighting = Weighting::fetchAll();
          $this->template->kpastatus = Kpastatus::getStatusInUpdateOrder();
          $this->template->scripts = array("kpa.js");
          $this->template->pagetitle = "Update KPA";	
          $this->template->view("kpa/updatekpa");
	}
	
	function evaluatekpa($data)
	{
          $this->template->header = Naming::getHeaderList();
          $this->template->kpa = Kpa::findById($data['kpaid']);
          $this->template->componentid = $data['componentid'];
          $this->template->evaluationyear = $data['evaluationyear'];
          $this->template->weighting = Weighting::fetchAll();
          $this->template->scripts = array("kpa.js");
          $this->template->pagetitle = "Evaluate KPA";	
          $this->template->view("kpa/evaluatekpa");
	}	
	
	function approvekpa($data)
	{
          $this->template->header = Naming::getHeaderList();
          $this->template->kpa = Kpa::findById($data['kpaid']);
          $this->template->componentid = $data['componentid'];
          $this->template->evaluationyear = $data['evaluationyear'];
          $this->template->weighting = Weighting::fetchAll();
          $this->template->scripts = array("kpa.js");
          $this->template->pagetitle = "Approve KPA";	
          $this->template->view("kpa/approvekpa");
	}	
		
	function assurancekpa($data)
	{
          $this -> template -> header         = Naming::getHeaderList();
          $this -> template -> kpa            = Kpa::findById($data['kpaid']);
          $this -> template -> componentid    = $data['componentid'];
          $this -> template -> evaluationyear = $data['evaluationyear'];
          $this -> template -> weighting      = Weighting::fetchAll();
          $this -> template -> scripts        = array('ajaxfileupload.js', 'kpa.js', 'jquery.assurance.kpa.js');
          $this -> template -> pagetitle      = "KPA Assurance";	
          $this -> template -> view("kpa/assurancekpa");
	}		

    function save_kpa_evaluation($data)
    {
        $evaluationObj = new KPAEvaluation();
        $userdata      = User::getUser($data);  
        $result        =  $evaluationObj -> save_evaluation($data, $userdata);        
        return $result;
     } 
    	
    function update_kpa_evaluation($data)
    {
       $evaluationObj = new KPAEvaluation();
       $userdata      = User::getUser($data);  
       $result        =  $evaluationObj -> save_evaluation($data, $userdata);        
       return $result;    
    }
}
