<?php 
$allow = array();
if( $componentid == "1")
{
	$allow = array();
} else if( $componentid == 2){
	$allow = array();
} else if( $componentid == 3){
	$allow = array("owner", "proof_of_evidence", "baseline", "targetunit", "performance_standard", "target_period", "weighting");		
}
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder" width="100%">
  <tr>
    <td class="noborder">
          <table width="100%">
              <tr>
                  <th><?php echo $header['id']; ?></th>
                  <td>
	                  <?php echo $action['id']; ?>
                  </td>
              </tr>          
              <tr>
                  <th><?php echo $header['name']; ?></th>
                  <td>
	                  <?php echo $action['name']; ?>
                  </td>
              </tr>
              <tr>
                  <th><?php echo $header['objective']; ?></th>
                  <td>
	                  <?php echo $action['objective']; ?>
                  </td>
              </tr>	
              <tr>
                  <th><?php echo $header['outcome']; ?></th>
                  <td>
	                 <?php echo $action['outcome']; ?>
                  </td>
              </tr>		
              <tr>
                  <th><?php echo $header['measurement']; ?></th>
                  <td>
	                  <?php echo $action['measurement']; ?>
                  </td>
              </tr>			
              <tr  style="display:<?php echo (in_array('proof_of_evidence', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['proof_of_evidence']; ?></th>
                  <td>
	                  <?php echo $action['proof_of_evidence']; ?>
                  </td>
              </tr>	
              <tr  style="display:<?php echo (in_array('baseline', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['baseline']; ?></th>
                  <td>
	                  <?php echo $action['baseline']; ?>
                  </td>
              </tr>	
              <tr  style="display:<?php echo (in_array('targetunit', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['targetunit']; ?></th>
                  <td>
	                 <?php echo $action['targetunit']; ?>
                  </td>
              </tr>																
              <tr  style="display:<?php echo (in_array('performance_standard', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['performance_standard']; ?></th>
                  <td>
	                  <?php echo $action['performance_standard']; ?>
                  </td>
              </tr>									
              <tr  style="display:<?php echo (in_array('target_period', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['target_period']; ?></th>
                  <td>
	                <?php echo $action['target_period']; ?>
                  </td>
              </tr>										
              <tr  style="display:<?php echo (in_array('weighting', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['weighting']; ?> of Action</th>
                  <td>
			              <?php 
			                  foreach($weighting as $index => $weight) 
			                  {
			                   echo ($weight['id'] === $action['weighting'] ? $weight['value']."(".$weight['weight'].")" : ""); 
				              }
				          ?>
                  </td>
              </tr>		
              <tr  style="display:<?php echo (in_array('competency', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['competency']; ?></th>
                  <td>
	                  <?php
	                    foreach($competency as $cIndex => $cVal)
	                    {
	                       if($cVal['id'] == $action['competency'])
	                       {
	                         echo $cVal['value'];
	                       }
	                    }
	                   ?>
                  </td>
              </tr>	
              <tr  style="display:<?php echo (in_array('proficiency', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['proficiency']; ?></th>
                  <td>
	                  <?php
	                    foreach($proficiency as $pIndex => $pVal)
	                    {
	                       if($pVal['id'] == $action['proficiency'])
	                       {
                               echo $pVal['value'];
                            } 
	                    }
	                   ?>
                  </td>
              </tr>													
              <tr  style="display:<?php echo (in_array('deadline', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['deadline']; ?></th>
                  <td>
                    <?php echo $action['deadline']; ?>
                  </td>
              </tr>
              <tr  style="display:<?php echo (in_array('comments', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['comments']; ?></th>
                  <td>
	                 <?php echo $action['comments']; ?>
                  </td>
              </tr>														
	          <tr>
		          <td><?php displayGoBack("", ""); ?></td>
		          <td>
	                 <input type="hidden" class="logid" name="logid" id="actionid" value="<?php echo $action['id']; ?>">
	                 <?php displayAuditLogLink("action_logs", false); ?>     		               
		          </td>
	          </tr>																								
          </table>     
    </td>
    <td class="noborder">
          <form name="editkpaform" id="editkpaform">     
           <table width="100%">
             <tr>
               <th>Response</th>
               <td><textarea name="response" id="response"></textarea></td>
             </tr>
             <tr>
               <th>Status</th>
               <td>
                    <select id="actionstatus" name="actionstatus">
                      <option value="">--please select--</option>
                      <?php
                       foreach($actionstatus as $index => $status)
                       {
                      ?>   
                       <option value="<?php echo $status['id']; ?>"
                     <?php
                         if($status['id'] == 1)
                         {
                              echo "disabled='disabled'";
                         } 
                         if($status['id'] == $action['status'])
                         {
                              echo "selected='selected'";
                         } 
                     ?>
                       ><?php echo $status['name']; ?></option>
                      <?php 
                       }
                      ?>
                    </select>
			      <p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px; clear:both; margin-top:10px;"><small>Completed actions will be sent to the relevant person(s) for approval and cannot be updated further.</small></p>                   
			      <input type="hidden" name="action_status" id="action_status" value="<?php echo $action['status']; ?>"/> 
               </td>
             </tr>  
             <tr>
               <th>Action On:</th>
               <td>
                  <input type="text" name="actionon" id="actionon" class="historydate" readonly="readonly" value="<?php echo ($action['actionon'] == '' ? date('d-M-Y') : $action['actionon']) ; ?>" />
               </td>
             </tr>              
             <tr>
               <th>Progress</th>
               <td>
                  <input type="text" name="progress" id="progress" value="<?php echo $action['progress']; ?>" />
                  <input type="hidden" name="currentprogress" id="currentprogress" value="<?php echo $action['progress']; ?>" />
                  <em>%</em>
               </td>
             </tr>                 
             <tr>
               <th>Remind On</th>
               <td>
                  <input type="text" name="remindon" id="remindon" class="datepicker" readonly="readonly" value="<?php echo $action['remindon']; ?>" />
               </td>
             </tr> 
             <tr>
               <th>Attach Document:</th>
               <td>
                  <input type="file" name="action_attachment_<?php echo $action['id']; ?>" id="action_attachment_<?php echo $action['id']; ?>" onChange="uploadActionAttachment(this);" />
				  <span class="ui-state ui-state-info" style="padding:5px; width:150px;"><small>You can attach more than 1 file</small></span> <p></p>
				<b>Attached Files : </b><br />
				<div id="uploaded_image">
					<?php							 
					Attachment::displayAttachmentList($action['attachment']);
					?>
				</div>
				<div id="file_upload">There are no files attached</div>					                   
               </td>
             </tr>  
				<tr>
					<th>Send Approval Notification:</th>
					<td>
						<input type="checkbox" id="requestapproval" name="requestapproval"
							<?php if($action['progress'] !== "100" || $action['status'] != 3) { ?> disabled="disabled" <?php } ?> value="1"  />
					    <span class="ui-state ui-state-info" style="padding:5px; width:250px;"><small>This will send an email notification to the relevant person(s) responsible for approving this Action.</small></span>
					</td>
				</tr>                                  
             <tr>
               <th></th>
               <td>
                    <input type="button" name="update" id="update" value=" Save Changes " class="isubmit" />
                    <input type="hidden" name="actionid" id="actionid" value="<?php echo $action['id']; ?>" />
                    <input type="reset" name="cancel" id="cancel" value=" Cancel " />
                    <input type="hidden" name="goaltype" id="goaltype" value="<?php echo $_GET['componentid']; ?>" />
                    <input type="hidden" name="parent" id="parent" value="<?php echo $_GET['parent']; ?>" />
                    <!-- <input type="hidden" name="evaluationyear" id="evaluationyear" value="<?php echo $evaluationyear; ?>" /> -->
               </td>
	          </tr>	         
           </table>              
          </form>
    </td>
  </tr>
</table>
