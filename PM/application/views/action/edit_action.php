<?php 
$allow = array();
if( $type == "1")
{
	$allow = array();
} else if( $type == 2){
	$allow = array();
} else if( $type == 3){
	$allow = array("owner", "proof_of_evidence", "baseline", "targetunit", "performance_standard", "target_period", "nationalkpa", "idpobjectives");		
}
?>
<?php JSdisplayResultObj(""); ?>
<form name="editactionform" id="editactionform">
<table>
    <tr>
        <th><?php echo $header['id']; ?></th>
        <td><?php echo $_GET['id']; ?></textarea></td>
    </tr>
    <tr>
        <th><?php echo $header['name']; ?></th>
        <td>
	        <textarea name="name" id="name"><?php echo $action['name']; ?></textarea>
        </td>
    </tr>
    <tr>
        <th><?php echo $header['objective']; ?></th>
        <td>
	        <textarea name="objective" id="objective"><?php echo $action['objective']; ?></textarea>
        </td>
    </tr>	
    <tr>
        <th><?php echo $header['outcome']; ?></th>
        <td>
	        <textarea name="outcome" id="outcome"><?php echo $action['outcome']; ?></textarea>
        </td>
    </tr>		
    <tr>
        <th><?php echo $header['measurement']; ?></th>
        <td>
	        <textarea name="measurement" id="measurement"><?php echo $action['measurement']; ?></textarea>
        </td>
    </tr>			
    <!-- <tr  style="display:<?php echo (in_array('owner', $allow) ? 'none' : 'table-row'); ?>">
        <th><?php echo $header['owner']; ?></th>
        <td>
	        <select name="owner" id="owner">
	          <option value="">--owner--</option>
	        </select>
        </td>
    </tr> -->
    <tr  style="display:<?php echo (in_array('proof_of_evidence', $allow) ? 'none' : 'table-row'); ?>">
        <th><?php echo $header['proof_of_evidence']; ?></th>
        <td>
	        <textarea name="proof_of_evidence" id="proof_of_evidence"><?php echo $action['proof_of_evidence']; ?></textarea>
        </td>
    </tr>	
    <tr  style="display:<?php echo (in_array('baseline', $allow) ? 'none' : 'table-row'); ?>">
        <th><?php echo $header['baseline']; ?></th>
        <td>
	        <textarea name="baseline" id="baseline"><?php echo $action['baseline']; ?></textarea>
        </td>
    </tr>	
    <tr  style="display:<?php echo (in_array('targetunit', $allow) ? 'none' : 'table-row'); ?>">
        <th><?php echo $header['targetunit']; ?></th>
        <td>
	        <textarea name="targetunit" id="targetunit"><?php echo $action['targetunit']; ?></textarea>
        </td>
    </tr>																
    <tr  style="display:<?php echo (in_array('performance_standard', $allow) ? 'none' : 'table-row'); ?>">
        <th><?php echo $header['performance_standard']; ?></th>
        <td>
	        <textarea name="performance_standard" id="performance_standard"><?php echo $action['performance_standard']; ?></textarea>
        </td>
    </tr>									
    <tr  style="display:<?php echo (in_array('target_period', $allow) ? 'none' : 'table-row'); ?>">
        <th><?php echo $header['target_period']; ?></th>
        <td>
	        <textarea name="target_period" id="target_period"><?php echo $action['target_period']; ?></textarea>
        </td>
    </tr>										
    <tr  style="display:<?php echo (in_array('weighting', $allow) ? 'none' : 'table-row'); ?>">
        <th><?php echo $header['weighting']; ?> of Action</th>
        <td>
			<select name="weighting" id="weighting">
			    <option value="">--please select--</option>
			    <?php 
			        foreach($weighting as $index => $weight) 
			        {
			    ?>
				    <option value="<?php echo $weight['id']; ?>"
				        <?php echo ($weight['id'] === $action['weighting'] ? "selected='selected'" : ""); ?>
				    >
				        <?php echo $weight['value']."(".$weight['weight'].")"; ?>
				    </option>
				<?php
				    }
				?>
			</select>against the KPI
        </td>
    </tr>		
    <tr  style="display:<?php echo (in_array('competency', $allow) ? 'none' : 'table-row'); ?>">
        <th><?php echo $header['competency']; ?></th>
        <td>
	        <select name="competency" id="competency">
	        <option>--please select--</option>
	        <?php
	          foreach($competency as $cIndex => $cVal){
	        ?>
	           <option
	           <?php
	             if($cVal['id'] == $action['competency'])
	             {
	           ?>
	              selected="selected"
	           <?php } ?>
	            value="<?php echo $cVal['id']; ?>"><?php echo $cVal['value']; ?></option>
	        <?php 
	          }
	         ?>
	        </select>
        </td>
    </tr>	
    <tr  style="display:<?php echo (in_array('proficiency', $allow) ? 'none' : 'table-row'); ?>">
        <th><?php echo $header['proficiency']; ?></th>
        <td>
	        <select name="proficiency" id="proficiency">
	          <option>--proficiency--</option>
	        <?php
	          foreach($proficiency as $pIndex => $pVal){
	        ?>
	           <option
	           <?php
	             if($pVal['id'] == $action['proficiency'])
	             {
	           ?>
	              selected="selected"
	           <?php } ?>
	            value="<?php echo $pVal['id']; ?>"><?php echo $pVal['value']; ?></option>
	        <?php 
	          }
	         ?>
	        </select>
	        <!-- <p>
		        <input type="button" name="addanother" id="addanother" value="Add Another" class="profieciency_competency" />
            </p> -->
        </td>
    </tr>													
    <tr  style="display:<?php echo (in_array('deadline', $allow) ? 'none' : 'table-row'); ?>">
        <th><?php echo $header['deadline']; ?></th>
        <td>
          <input type="text" name="deadline" id="deadline" class="datepicker" value="<?php echo $action['deadline']; ?>" readonly="readonly" />
        </td>
    </tr>
    <tr  style="display:<?php echo (in_array('comments', $allow) ? 'none' : 'table-row'); ?>">
        <th><?php echo $header['comments']; ?></th>
        <td>
	       <textarea name="comments" id="comments"><?php echo $action['comments']; ?></textarea>
        </td>
    </tr>													
    <tr>
        <th></th>
        <td>
	        <input type="button" name="edit" id="edit" value=" Save Changes " class="isubmit" />
	        <input type="button" name="cancel" id="cancel" value=" Cancel " />
	        <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>" />
	        <input type="hidden" name="kpiid" id="kpiid" value="<?php echo $_GET['kpiid']; ?>" />
	        <input type="hidden" name="parent" id="parent" value="<?php echo $_GET['parent']; ?>" />
        </td>
    </tr>
    <tr>
        <td>
	        <?php  displayGoBack("", ""); ?>
	     </td>
        <td></td>
    </tr>																													
</table>    
</form>
