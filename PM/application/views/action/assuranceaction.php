<?php 
$allow = array();
if( $componentid == "1")
{
	$allow = array();
} else if( $componentid == 2){
	$allow = array();
} else if( $componentid == 3){
	$allow = array("owner", "proof_of_evidence", "baseline", "targetunit", "performance_standard", "target_period", "weighting");		
}
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder" width="100%">
  <tr>
    <td class="noborder" width="50%">
          <table width="100%">
              <tr>
                  <th><?php echo $header['name']; ?></th>
                  <td>
	                  <?php echo $action['name']; ?>
                  </td>
              </tr>
              <tr>
                  <th><?php echo $header['objective']; ?></th>
                  <td>
	                  <?php echo $action['objective']; ?>
                  </td>
              </tr>	
              <tr>
                  <th><?php echo $header['outcome']; ?></th>
                  <td>
	                 <?php echo $action['outcome']; ?>
                  </td>
              </tr>		
              <tr>
                  <th><?php echo $header['measurement']; ?></th>
                  <td>
	                  <?php echo $action['measurement']; ?>
                  </td>
              </tr>			
              <tr  style="display:<?php echo (in_array('proof_of_evidence', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['proof_of_evidence']; ?></th>
                  <td>
	                  <?php echo $action['proof_of_evidence']; ?>
                  </td>
              </tr>	
              <tr  style="display:<?php echo (in_array('baseline', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['baseline']; ?></th>
                  <td>
	                  <?php echo $action['baseline']; ?>
                  </td>
              </tr>	
              <tr  style="display:<?php echo (in_array('targetunit', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['targetunit']; ?></th>
                  <td>
	                 <?php echo $action['targetunit']; ?>
                  </td>
              </tr>																
              <tr  style="display:<?php echo (in_array('performance_standard', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['performance_standard']; ?></th>
                  <td>
	                  <?php echo $action['performance_standard']; ?>
                  </td>
              </tr>									
              <tr  style="display:<?php echo (in_array('target_period', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['target_period']; ?></th>
                  <td>
	                <?php echo $action['target_period']; ?>
                  </td>
              </tr>										
              <tr  style="display:<?php echo (in_array('weighting', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['weighting']; ?> of Action</th>
                  <td>
			              <?php 
			                  foreach($weighting as $index => $weight) 
			                  {
			                   echo ($weight['id'] === $action['weighting'] ? $weight['value']."(".$weight['weight'].")" : ""); 
				              }
				          ?>
                  </td>
              </tr>		
              <tr  style="display:<?php echo (in_array('competency', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['competency']; ?></th>
                  <td>
	                  <?php
	                    foreach($competency as $cIndex => $cVal)
	                    {
	                       if($cVal['id'] == $action['competency'])
	                       {
	                         echo $cVal['value'];
	                       }
	                    }
	                   ?>
                  </td>
              </tr>	
              <tr  style="display:<?php echo (in_array('proficiency', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['proficiency']; ?></th>
                  <td>
	                  <?php
	                    foreach($proficiency as $pIndex => $pVal)
	                    {
	                       if($pVal['id'] == $action['proficiency'])
	                       {
                               echo $pVal['value'];
                            } 
	                    }
	                   ?>
                  </td>
              </tr>													
              <tr  style="display:<?php echo (in_array('deadline', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['deadline']; ?></th>
                  <td>
                    <?php echo $action['deadline']; ?>
                  </td>
              </tr>
              <tr  style="display:<?php echo (in_array('comments', $allow) ? 'none' : 'table-row'); ?>">
                  <th><?php echo $header['comments']; ?></th>
                  <td>
	                 <?php echo $action['comments']; ?>
                  </td>
              </tr>														
	          <tr>
		          <td><?php displayGoBack("", ""); ?></td>
		          <td></td>
	          </tr>																								
          </table>     
    </td>
    <td class="noborder" width="50%">
			<script type="text/javascript">
				$(function(){
					$("#action_assurance").action({actionid:$("#actionid").val()});
				});
			</script>
			<div id="action_assurance" width="100%"></div>
			<input type="hidden" name="actionid" id="actionid" value="<?php echo $action['id']; ?>" />
    </td>
  </tr>
</table>
