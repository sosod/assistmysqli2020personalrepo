<?php JSdisplayResultObj(""); ?>   
<table id="table_glossary" width="100%">
  <?php
    if(!empty($glossaries))
    {
  ?>
     <tr>
        <th>Ref</td>
        <th>Field</th>
        <th>Purpose</th>
        <th>Rules</th>
     </tr>
  <?php
       foreach($glossaries as $index => $glossary)
       {  
   ?>
     <tr id="tr_<?php echo $glossary['id']; ?>">
        <td><?php echo $glossary['id']; ?></td>
        <td><?php echo $headers[$glossary['field']]; ?></td>
        <td><?php echo $glossary['purpose']; ?></td>
        <td><?php echo $glossary['rules']; ?></td>
     </tr>
   <?php    
       }  
    }
  ?>
<table>

