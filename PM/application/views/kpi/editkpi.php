<?php
$allow = array();
if( $componentid == "1")
{
	$allow = array();
} else if( $componentid == 2){
	$allow = array();
} else if( $componentid == 3){
	$allow = array();
	$allow = array("owner", "nationalkpa", "idpobjectives");		
}
//debug($allow);
//debug($componentid);
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
 <tr>
   <td class="noborder">
     <form name="editkpiform" id="editkpiform">
     <table>
         <tr>
               <th><?php echo $header['id']; ?></th>
               <td>
                  <?php echo $kpi['id']; ?>
               </td>
          </tr>             
          <tr>
	          <th><?php echo $header['name']; ?></th>
	          <td>
		          <textarea name="name" id="name"><?php echo $kpi['name']; ?></textarea>
	          </td>
          </tr>
          <tr>
	          <th><?php echo $header['objective']; ?></th>
	          <td>
		          <textarea name="objective" id="objective"><?php echo $kpi['objective']; ?></textarea>
	          </td>
          </tr>	
          <tr>
	          <th><?php echo $header['outcome']; ?></th>
	          <td>
		          <textarea name="outcome" id="outcome"><?php echo $kpi['outcome']; ?></textarea>
	          </td>
          </tr>		
          <tr>
	          <th><?php echo $header['measurement']; ?></th>
	          <td>
		          <textarea name="measurement" id="measurement"><?php echo $kpi['measurement']; ?></textarea>
	          </td>
          </tr>			
          <!-- <tr style="display:<?php echo (in_array('owner', $allow) ? 'none' : 'table-row'); ?>">
	          <th><?php echo $header['owner']; ?></th>
	          <td>
		          <select name="owner" id="owner">
		            <option value="">--please select--</option>
		          </select>
	          </td>
          </tr> -->
          <tr style="display:<?php echo (in_array('proof_of_evidence', $allow) ? 'none' : 'table-row'); ?>">
	          <th><?php echo $header['proof_of_evidence']; ?></th>
	          <td>
		          <textarea name="proof_of_evidence" id="proof_of_evidence"><?php echo $kpi['proof_of_evidence']; ?></textarea>
	          </td>
          </tr>	
          <tr style="display:<?php echo (in_array('baseline', $allow) ? 'none' : 'table-row'); ?>">
	          <th><?php echo $header['baseline']; ?></th>
	          <td>
		          <textarea name="baseline" id="baseline"><?php echo $kpi['baseline']; ?></textarea>
	          </td>
          </tr>	
          <tr style="display:<?php echo (in_array('targetunit', $allow) ? 'none' : 'table-row'); ?>">
	          <th><?php echo $header['targetunit']; ?></th>
	          <td>
		          <textarea name="targetunit" id="targetunit"><?php echo $kpi['targetunit']; ?></textarea>
	          </td>
          </tr>		
          <tr style="display:<?php echo (in_array('nationalkpa', $allow) ? 'none' : 'table-row'); ?>">
	          <th><?php echo $header['nationalkpa']; ?></th>
	          <td>
		          <select name="nationalkpa" id="nationalkpa">
		          <option value="">--please select--</option>
		          <?php
		              foreach($nationalkpa as $nIndex => $natVal){
		           ?>
		              <option value="<?php echo $natVal['id'] ?>"
		              <?php 
		                if($natVal['id'] == $kpi['nationalkpa']){
		              ?>
		                selected="selected"
		              <?php 
		              }
		              ?>			    
		              ><?php echo $natVal['name']; ?></option>
		           <?php
		           }
		            ?>
		          </select>
	          </td>
          </tr>							
          <tr style="display:<?php echo (in_array('idpobjectives', $allow) ? 'none' : 'table-row'); ?>">
	          <th><?php echo $header['idpobjectives']; ?></th>
	          <td>
		          <select name="idpobjectives" id="idpobjectives">
		          <option value="">--please select--</option>
		          <?php
		              foreach($nationalobjectives as $noIndex => $natoVal){
		           ?>
		              <option value="<?php echo $natoVal['id'] ?>"
		              <?php 
		                if($natoVal['id'] == $kpi['idpobjectives']){
		              ?>
		                selected="selected"
		              <?php 
		              }
		              ?>
		              ><?php echo $natoVal['name']; ?></option>
		           <?php
		           }
		            ?>
		          </select>
	          </td>
          </tr>														
          <tr style="display:<?php echo (in_array('performance_standard', $allow) ? 'none' : 'table-row'); ?>">
	          <th><?php echo $header['performance_standard']; ?></th>
	          <td>
		          <textarea name="performance_standard" id="performance_standard"><?php echo $kpi['performance_standard']; ?></textarea>
	          </td>
          </tr>									
          <tr style="display:<?php echo (in_array('target_period', $allow) ? 'none' : 'table-row'); ?>">
	          <th><?php echo $header['target_period']; ?></th>
	          <td>
		          <textarea name="target_period" id="target_period"><?php echo $kpi['target_period']; ?></textarea>
	          </td>
          </tr>										
          <tr style="display:<?php echo (in_array('weighting', $allow) ? 'none' : 'table-row'); ?>">
	          <th><?php echo $header['weighting']; ?>of KPI</th>
	          <td>
		          <select name="weighting" id="weighting">
		              <option value="">--please select--</option>
		              <?php 
		                  foreach($weighting as $index => $weight) 
		                  {
		              ?>
			              <option value="<?php echo $weight['id']; ?>"
			                  <?php echo ($weight['id'] === $kpi['weighting'] ? "selected='selected'" : ""); ?>
			              >
			                  <?php echo $weight['value']."(".$weight['weight'].")"; ?>
			              </option>
			          <?php
			              }
			          ?>
		          </select>against the KPA
	          </td>
          </tr>														
          <tr style="display:<?php echo (in_array('deadline', $allow) ? 'none' : 'table-row'); ?>">
	          <th><?php echo $header['deadline']; ?></th>
	          <td>
	            <input type="text" name="deadline" id="deadline" class="datepicker" value="<?php echo $kpi['deadline']; ?>" readonly="readonly" />
	          </td>
          </tr>
          <tr style="display:<?php echo (in_array('comments', $allow) ? 'none' : 'table-row'); ?>">
	          <th><?php echo $header['comments']; ?></th>
	          <td>
		          <textarea name="comments" id="comments"><?php echo $kpi['comments']; ?></textarea>
	          </td>
          </tr>													
          <tr>
	          <th></th>
	          <td>
		          <input type="button" name="edit" id="edit" value=" Save Changes " class="isubmit" />
		          <input type="button" name="cancel" id="cancel" value=" Cancel " />
		          <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>" />
		          <input type="hidden" name="kpaid" id="kpaid" value="<?php echo $_GET['kpaid']; ?>" />
		          <input type="hidden" name="parent" id="parent" value="<?php echo $_GET['parent']; ?>" />
	          </td>
          </tr>																							
     </table>
     </form>    
   </td>
 </tr>
 <tr>
     <td class="noborder">
       <table width="100%" class="noborder">
         <tr>
          <td class="noborder"><?php displayGoBack("", ""); ?></td>
          <td class="noborder">
            <input type="hidden" class="logid" name="logid" id="kpiid" value="<?php echo $kpi['id']; ?>">
            <?php displayAuditLogLink("kpi_logs", false); ?>     
          </td>
         </tr>
       </table>
     </td>
 </tr> 
</table>

