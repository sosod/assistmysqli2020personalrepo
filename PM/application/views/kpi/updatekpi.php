<?php 
$allow = array();
if( $componentid == "1")
{
	$allow = array();
} else if( $componentid == 2){
	$allow = array();
} else if( $componentid == 3){
	$allow = array("owner", "proof_of_evidence", "baseline", "targetunit", "performance_standard", "target_period", "weighting");		
}
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder" width="100%">
  <tr>
    <td class="noborder">
          <table width="100%">
              <tr>
                    <th><?php echo $kpi['id']; ?></th>
                    <td>
                       <?php echo $kpi['id']; ?>
                    </td>
               </tr>                       
	          <tr>
		          <th><?php echo $header['name']; ?></th>
		          <td>
			          <?php echo $kpi['name']; ?>
		          </td>
	          </tr>
	          <tr>
		          <th><?php echo $header['objective']; ?></th>
		          <td>
			          <?php echo $kpi['objective']; ?>
		          </td>
	          </tr>	
	          <tr>
		          <th><?php echo $header['outcome']; ?></th>
		          <td>
			          <?php echo $kpi['outcome']; ?>
		          </td>
	          </tr>		
	          <tr>
		          <th><?php echo $header['measurement']; ?></th>
		          <td>
			          <?php echo $kpi['measurement']; ?>
		          </td>
	          </tr>			
	          <!-- <tr style="display:<?php echo (in_array('owner', $allow) ? 'none' : 'table-row'); ?>">
		          <th><?php echo $header['owner']; ?></th>
		          <td>
			          <select name="owner" id="owner">
			            <option value="">--owner--</option>
			          </select>
		          </td>
	          </tr> -->
	          <tr style="display:<?php echo (in_array('proof_of_evidence', $allow) ? 'none' : 'table-row'); ?>">
		          <th ><?php echo $header['proof_of_evidence']; ?></th>
		          <td>
			          <?php echo $kpi['proof_of_evidence']; ?>
		          </td>
	          </tr>	
	          <tr style="display:<?php echo (in_array('baseline', $allow) ? 'none' : 'table-row'); ?>">
		          <th><?php echo $header['baseline']; ?></th>
		          <td>
			          <?php echo $kpi['baseline']; ?>
		          </td>
	          </tr>	
	          <tr style="display:<?php echo (in_array('targetunit', $allow) ? 'none' : 'table-row'); ?>">
		          <th><?php echo $header['targetunit']; ?></th>
		          <td>
			          <?php echo $kpi['targetunit']; ?>
		          </td>
	          </tr>															
	          <tr style="display:<?php echo (in_array('performance_standard', $allow) ? 'none' : 'table-row'); ?>">
		          <th><?php echo $header['performance_standard']; ?></th>
		          <td>
			          <?php echo $kpi['performance_standard']; ?>
		          </td>
	          </tr>									
	          <tr style="display:<?php echo (in_array('target_period', $allow) ? 'none' : 'table-row'); ?>">
		          <th><?php echo $header['target_period']; ?></th>
		          <td>
			          <?php echo $kpi['target_period']; ?>
		          </td>
	          </tr>										
	          <tr style="display:<?php echo (in_array('weighting', $allow) ? 'none' : 'table-row'); ?>">
		          <th><?php echo $header['weighting']; ?>of KPA</th>
		          <td>
		              <?php 
		                  foreach($weighting as $index => $weight) 
		                  {
			               echo ($weight['id'] === $kpa['weighting'] ? $weight['value'].'('.$weight['weight'].')' : "");
			              }
			          ?>
		          </td>
	          </tr>			
	          <tr style="display:<?php echo (in_array('deadline', $allow) ? 'none' : 'table-row'); ?>">
		          <th><?php echo $header['deadline']; ?></th>
		          <td>
		            <?php echo $kpi['deadline']; ?>
		          </td>
	          </tr>										
	          <tr style="display:<?php echo (in_array('comments', $allow) ? 'none' : 'table-row'); ?>">
		          <th><?php echo $header['comments']; ?></th>
		          <td>
			          <?php echo $kpi['comments']; ?>
		          </td>
	          </tr>													
	          <tr>
		          <td><?php displayGoBack("", ""); ?></td>
		          <td>
                      <input type="hidden" class="logid" name="logid" id="kpiid" value="<?php echo $kpi['id']; ?>">
                      <?php displayAuditLogLink("kpi_logs", false); ?>     		          
		          </td>
	          </tr>																								
          </table>     
    </td>
    <td class="noborder">
          <form name="editkpaform" id="editkpaform">     
           <table width="100%">
             <tr>
               <th>Response</th>
               <td><textarea name="response" id="response"></textarea></td>
             </tr>
             <tr>
               <th>Status</th>
               <td>
                    <select id="kpistatus" name="kpistatus">
                      <option value="">--please select--</option>
                      <?php
                       foreach($kpistatus as $index => $status)
                       {
                      ?>   
                         <option 
                       <?php
                         if($status['id'] == 1)
                         {
                            echo "disabled='disabled'";
                         }
                         if($status['id'] == $kpi['status'])
                         {
                            echo "selected='selected'";
                         }                         
                       ?>
                         value="<?php echo $status['id']; ?>"><?php echo $status['name']; ?></option>                      
                      <?php 
                       }
                      ?>
                    </select>
               </td>
             </tr>    
             <tr>
               <th>Remind On</th>
               <td>
                  <input type="text" name="remindon" id="remindon" class="datepicker" readonly="readonly" value="<?php echo $kpi['remindon']; ?>" />
               </td>
             </tr>          
             <tr>
               <th></th>
               <td>
                    <input type="button" name="update" id="update" value=" Save Changes " class="isubmit" />
                    <input type="hidden" name="kpiid" id="kpiid" value="<?php echo $kpi['id']; ?>" />
                    <input type="button" name="cancel" id="cancel" value=" Cancel " />
                    <input type="hidden" name="goaltype" id="goaltype" value="<?php echo $_GET['componentid']; ?>" />
                    <input type="hidden" name="parent" id="parent" value="<?php echo $_GET['parent']; ?>" />
                    <!-- <input type="hidden" name="evaluationyear" id="evaluationyear" value="<?php echo $evaluationyear; ?>" /> -->
               </td>
	          </tr>	         
           </table>              
          </form>
    </td>
  </tr>
</table>
