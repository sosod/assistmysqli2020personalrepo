<?php 
$allow = array();
if( $componentid == "1")
{
	$allow = array();
} else if( $componentid == 2){
	$allow = array();
} else if( $componentid == 3){
	$allow = array("owner", "proof_of_evidence", "baseline", "targetunit", "performance_standard", "target_period", "weighting");		
}
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder" width="100%" style="table-layout:fixed;">
	<tr>
		<td class="noborder" width="50%">
            <table>
	            <tr>
		            <th><?php echo $header['name']; ?></th>
		            <td>
			            <?php echo $kpa['name']; ?>
		            </td>
	            </tr>
	            <tr>
		            <th><?php echo $header['objective']; ?></th>
		            <td>
		              <?php echo $kpa['objective']; ?>
		            </td>
	            </tr>	
	            <tr>
		            <th><?php echo $header['outcome']; ?></th>
		            <td>
		               <?php echo $kpa['outcome']; ?>
		            </td>
	            </tr>		
	            <tr>
		            <th><?php echo $header['measurement']; ?></th>
		            <td>
		               <?php echo $kpa['measurement']; ?>
		            </td>
	            </tr>			
	            <tr style="display:<?php echo (in_array('proof_of_evidence', $allow) ? 'none' : 'table-row'); ?>">
		            <th ><?php echo $header['proof_of_evidence']; ?></th>
		            <td>
			            <?php echo $kpa['proof_of_evidence']; ?>
		            </td>
	            </tr>	
	            <tr style="display:<?php echo (in_array('baseline', $allow) ? 'none' : 'table-row'); ?>">
		            <th><?php echo $header['baseline']; ?></th>
		            <td>
		               <?php echo $kpa['baseline']; ?>
		            </td>
	            </tr>	
	            <tr style="display:<?php echo (in_array('targetunit', $allow) ? 'none' : 'table-row'); ?>">
		            <th><?php echo $header['targetunit']; ?></th>
		            <td>
		                <?php echo $kpa['targetunit']; ?>
		            </td>
	            </tr>															
	            <tr style="display:<?php echo (in_array('performance_standard', $allow) ? 'none' : 'table-row'); ?>">
		            <th><?php echo $header['performance_standard']; ?></th>
		            <td>
			            <?php echo $kpa['performance_standard']; ?>
		            </td>
	            </tr>									
	            <tr style="display:<?php echo (in_array('target_period', $allow) ? 'none' : 'table-row'); ?>">
		            <th><?php echo $header['target_period']; ?></th>
		            <td>
			            <?php echo $kpa['target_period']; ?>
		            </td>
	            </tr>										
	            <tr style="display:<?php echo (in_array('weighting', $allow) ? 'none' : 'table-row'); ?>">
		            <th><?php echo $header['weighting']; ?>of KPA</th>
		            <td>
		                <?php 
		                   foreach($weighting as $index => $weight) 
		                   {
		                     if($weight['id'] === $kpa['weighting'])
		                     {
		                       echo $weight['value']."(".$weight['weight'].")";
		                     }
			                //echo ($weight['id'] === $kpa['weighting'] ? "selected='selected'" : ""); 
			              }
			            ?>
		                 <em>against the component</em>
		            </td>
	            </tr>			
	            <tr style="display:<?php echo (in_array('deadline', $allow) ? 'none' : 'table-row'); ?>">
		            <th><?php echo $header['deadline']; ?></th>
		            <td>
		             <?php echo $kpa['deadline']; ?>
		            </td>
	            </tr>										
	            <tr style="display:<?php echo (in_array('comments', $allow) ? 'none' : 'table-row'); ?>">
		            <th><?php echo $header['comments']; ?></th>
		            <td>
		               <?php echo $kpa['comments']; ?>
		            </td>
	            </tr>														
	            <tr>
		            <td><?php displayGoBack("", ""); ?></td>
		            <td></td>
	            </tr>																					
            </table>
		</td>
		<td class="noborder" width="50%">
			<script type="text/javascript">
				$(function(){
					$("#kpa_assurance").kpa({kpaid:$("#kpaid").val()});
				});
			</script>
			<div id="kpa_assurance" width="100%"></div>
			<input type="hidden" name="kpaid" id="kpaid" value="<?php echo $kpa['id']; ?>" />	
		</td>
	</tr>
</table>


<form name="editkpaform" id="editkpaform">

</form>
