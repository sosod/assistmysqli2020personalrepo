<?php JSdisplayResultObj(""); ?>  
<table class="noborder">
	<tr class="gray">
		<td class="noborder" style="text-align:center;" colspan="2">
			<select id="evaluationyear" name="evaluationyear">
				<option value="">--select evaluation year--</option>
			</select>		
		</td>
	</tr>
	<tr>
	   <td colspan="2" class="noborder">
			<table id="table_performancecomponents">
			   <tr id="addcomponent">
			     <td colspan="3">
				       <input type="button" name="addnew" id="addnew" value="Add New" />
				       <input type="hidden" name="evaluationyearid" id="evaluationyearid" value="" />  
			     </td>
			     <td colspan="3" style="text-align:right">
			       <!-- <input type="button" name="copy" id="copy" value="Copy from prior year" />  -->
			     </td>			     
			   </tr>
			   <tr>
			     <th>Ref</th>
			     <th>Component Code</th>
			     <th>Performance Management Component Name</th>
			     <th>Performanace Management Component Description</th>
			     <th>Status</th>
			     <th></th>               
			   </tr>
			   <tr>
			   	 <td colspan="6" class="comp">Select the evaluation year to view the component for that evaluation year</td>
			   </tr>
			</table>
	   </td>
	</tr>
	<tr>
	  <td class="noborder"><?php displayGoBack("",""); ?></td>
	  <td class="noborder"><?php displayAuditLogLink("list_perfomancecomponents_logs", true)?></td>
	</tr>
</table>
