<?php JSdisplayResultObj(""); ?>
<form method="post" id="useraccess-form" name="useraccess-form">
<table border="1" id="useraccess_table" >
<tr>
    <th>Ref</th>  
    <td><?php echo $user['id'] ?></td>		  				    
</tr>
<tr>
    <th>User</th>
    <td><?php echo $user['user_id']; ?></td>				    
</tr>
<tr>
    <th>Module Admin</th>
    <td>
	    <select id="module_admin" name="module_admin">
            <option value="1" <?php echo (($user['status'] & 1) == 1 ? "selected='selected'" : "") ?>>yes</option>
            <option value="0" <?php echo (($user['status'] & 1) == 0 ? "selected='selected'" : "") ?>>no</option>            
        </select>				    
    </td>
</tr>
<tr>
    <th>Create Components</th>
    <td>
	     <select id="create_component" name="create_component">
            <option value="1" <?php echo (($user['status'] & 4) == 4 ? "selected='selected'" : "") ?>>yes</option>
            <option value="0" <?php echo (($user['status'] & 4) == 0 ? "selected='selected'" : "") ?>>no</option>            
        </select>
    </td>
</tr>
<tr>
    <th>Admin All</th>
    <td>
	     <select id="admin_all" name="admin_all">
            <option value="1" <?php echo (($user['status'] & 8) == 8 ? "selected='selected'" : "") ?>>yes</option>
            <option value="0" <?php echo (($user['status'] & 8) == 0 ? "selected='selected'" : "") ?>>no</option>            
        </select>
    </td>
</tr>
<tr>
    <th>View All</th>
    <td>
	     <select id="view_all" name="view_all">
            <option value="1" <?php echo (($user['status'] & 32) == 32 ? "selected='selected'" : "") ?>>yes</option>
            <option value="0" <?php echo (($user['status'] & 32) == 0 ? "selected='selected'" : "") ?>>no</option>            
        </select>
    </td>
</tr>
<tr>
    <th>Edit All</th>
    <td>
	    <select id="edit_all" name="edit_all">
            <option value="1" <?php echo (($user['status'] & 64) == 64 ? "selected='selected'" : "") ?>>yes</option>
            <option value="0" <?php echo (($user['status'] & 64) == 0 ? "selected='selected'" : "") ?>>no</option>            
        </select>				    
    </td>
</tr>
<tr>
    <th>Update All</th>
    <td>
	    <select id="update_all" name="update_all">
            <option value="1" <?php echo (($user['status'] & 16) == 16 ? "selected='selected'" : "") ?>>yes</option>
            <option value="0" <?php echo (($user['status'] & 16) == 0 ? "selected='selected'" : "") ?>>no</option>            
        </select>                    
    </td>
</tr>
<tr>
    <th>Reports</th>
    <td>
	     <select id="reports" name="reports">
            <option value="1" <?php echo (($user['status'] & 128) == 128 ? "selected='selected'" : "") ?>>yes</option>
            <option value="0" <?php echo (($user['status'] & 128) == 0 ? "selected='selected'" : "") ?>>no</option>            
        </select>				    
    </td>
</tr>
<tr>
    <th>Assuarance</th>
    <td>
	     <select id="assurance" name="assurance">
            <option value="1" <?php echo (($user['status'] & 256) == 256 ? "selected='selected'" : "") ?>>yes</option>
            <option value="0" <?php echo (($user['status'] & 256) == 0 ? "selected='selected'" : "") ?>>no</option>            
        </select>				    
    </td>
</tr>
<tr>
    <th>Evaluation</th>
    <td>
	     <select id="evaluation" name="evaluation">
            <option value="1" <?php echo (($user['status'] & 512) == 512 ? "selected='selected'" : "") ?>>yes</option>
            <option value="0" <?php echo (($user['status'] & 512) == 0 ? "selected='selected'" : "") ?>>no</option>            
        </select>
    </td>
</tr>
<tr>
    <th>Setup</th>
    <td>
	     <select id="setup" name="setup">
            <option value="1" <?php echo (($user['status'] & 1024) == 1024 ? "selected='selected'" : "") ?>>yes</option>
            <option value="0" <?php echo (($user['status'] & 1024) == 0 ? "selected='selected'" : "") ?>>no</option>            
        </select>
    </td>
</tr>
<tr>
    <td colspan="2">
      <input type="hidden" value="<?php echo $user['id']; ?>" id="id" name="id" />
       <input type="hidden" value="<?php echo $user['user_id']; ?>" id="userselect" name="userselect" />
      <input type="submit" value="Save Changes" id="save_changes" name="save_changes" />
    </td>
</tr>
<tr>
    <td class="noborder"><?php displayGoBack("",""); ?></td>
    <td class="noborder"><?php //displayAuditLogLink("nationalkpa_logs", true)?></td>
</tr>			  
</table>
</form>	

