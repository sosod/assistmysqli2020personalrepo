 <?php JSdisplayResultObj(""); ?>
<table class="noborder">	
	<tr>
	    <td colspan="2" class="noborder gray" style="text-align:center;">
			<select id="evaluationyear" name="evaluationyear">
			  <option>--select evaluation year--</option>
			</select>    
	    </td>	
	</tr>
	<tr>
	   <td colspan="2" class="noborder">
			<table id="table_categories">		
			  <tr id="addcategory">
			     <td colspan="6">
			       <input type="button" name="addnew" id="addnew" value="Add" />
				   <input type="hidden" name="evaluationyearid" id="evaluationyearid" value="">
			     </td>
			  </tr>
			  <tr>
			    <th>Ref</th>
			    <th>Short Code</th>
			    <th>Performance Management Category</th>
			    <th>Description of Performance Management Category</th>
			    <th>Status</th>
			    <th></th>
			  </tr>
			  <tr id="categories">
				  <td colspan="6">
					<p>Select evaluation year to view the categories</p>
				  </td>
			  </tr>			  
			</table>
	   </td>
	</tr>
     <tr>
          <td class="noborder"><?php displayGoBack("", "")?></td>
          <td class="noborder"><?php displayAuditLogLink("list_categories_logs", true)?></td>
     </tr>
</table>
