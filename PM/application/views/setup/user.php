<?php JSdisplayResultObj(""); ?>
<table width="100%" class="noborder" >
	<tr>
	   <td class="noborder" colspan="2"  width="100%">
				<form method="post" id="useraccess-form" name="useraccess-form">
				<table border="1" id="useraccess_table">
				  <tr>
				    <th>Ref</th>
				    <th>User</th>
				    <th>Module Admin</th>
				    <th>Create Components</th>
				    <th>Add KPA</th>
				    <th>Add KPI</th>
				    <th>Add Action</th>
				    <th>Admin All</th>
				    <th>View All</th>
				    <th>Edit All</th>
					<th>Update All</th>
				    <th>Reports</th>
				    <th>Assuarance</th>
				    <th>Evaluation</th>
				    <th>Setup</th>
				    <th>Action</th>
				  </tr>
				  <tr>
				    <td>#</td>
				    <td>
						<select name="userselect" id="userselect">
				        	<option value="">--select user--</option>
				        </select>
				    </td>
				    <td>
				    	<select id="module_admin" name="module_admin">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
                       <td>
				    	 <select id="add_kpa" name="add_kpa">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td>
				    	 <select id="admin_kpi" name="admin_kpi">
				            <option value="1" selected="selected">yes</option>
				            <option value="0">no</option>            
				        </select>
				    </td>     
				    <td>
				    	 <select id="add_action" name="add_action">
				            <option value="1" selected="selected">yes</option>
				            <option value="0">no</option>            
				        </select>
				    </td>		    
				    <td>
				    	 <select id="create_component" name="create_component">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td>
				    	 <select id="admin_all" name="admin_all">
				            <option value="1" selected="selected">yes</option>
				            <option value="0">no</option>            
				        </select>
				    </td>     
				    <td>
				    	 <select id="view_all" name="view_all">
				            <option value="1" selected="selected">yes</option>
				            <option value="0">no</option>            
				        </select>
				    </td>
				    <td>
				    	<select id="edit_all" name="edit_all">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td>
				    	<select id="update_all" name="update_all">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>				    
				    <td>
				    	 <select id="reports" name="reports">
				            <option value="1" selected="selected">yes</option>
				            <option value="0">no</option>            
				        </select>
				    </td>
				    <td>
				    	 <select id="assurance" name="assurance">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td> 
				    <td>
				    	 <select id="evaluation" name="evaluation">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td> 				       
				    <td>
				    	 <select id="setup" name="setup">
				            <option value="1">yes</option>
				            <option value="0" selected="selected">no</option>            
				        </select>
				    </td>
				    <td><input type="submit" value="Add" id="setup_access" name="setup_access" /></td>
				  </tr>
				</table>
				</form>	
	   </td>
	</tr>
	<tr>
	  <td class="noborder"><?php displayGoBack("",""); ?></td>
	  <td class="noborder"><?php displayAuditLogLink("user_logs", true)?></td>
	</tr>
</table>
