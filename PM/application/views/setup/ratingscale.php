<?php JSdisplayResultObj(""); ?>  
<table class="noborder">
	<tr>
		<td colspan="2" class="gray noborder" style="text-align:center;">
			<select id="evaluationyear" name="evaluationyear">
				<option value="">--select evaluation year--</option>
			</select>
		</td>
	</tr>
	<tr>
	   <td colspan="2" class="noborder">
			<table id="table_ratingscale">
			  <tr>
			     <td colspan="7" id="addratingscale">
		      		<input type="button" id="addnew" name="addnew" value="Add" />
		      		<input type="hidden" id="evaluationyearid" name="evaluationyearid"  value="" />
			     </td>
			  </tr>
			  <tr>
				   <th>Ref</th>
				   <th>Numerical Rating</th>
				   <th>Rating Description</th>
				   <th>Rating Definition</th>
				   <th>Colour Assigned to Score</th>
				   <th>Status</th>
				   <th></th>
			  </tr>
			  <tr class="rating">
			  	 <td colspan="7">Select the evaluation year to view the rating scales</td>
			  </tr>
			</table>
	   </td>
	</tr>
	<tr>
	  <td class="noborder"><?php displayGoBack("",""); ?></td>
	  <td class="noborder"><?php displayAuditLogLink("list_ratingscale_logs", true)?></td>
	</tr>
</table>
