<?php JSdisplayResultObj(""); ?>   
<table class="noborder" width="100%">
     <tr id="add_glossary">
       <td align="center"  class="noborder" >  
          <select id="name">
            <?php
             foreach($fields as $index => $header)
             {
            ?>
              <option value="<?php echo $index; ?>"><?php echo $header; ?></option>
            <?php       
             }
            ?>
          </select>
       </td>
       <td align="center"  class="noborder" >
          <input type="button" name="add" id="add" value="Define Glossary" />
          <input type="hidden" name="_category" id="_category" value="<?php echo $category; ?>" />
       </td>
     </tr> 		
	<tr>   
	   <td colspan="2"  class="noborder" >
	     <table id="table_glossary" width="100%">
	       <?php
	         if(!empty($glossaries))
	         {
            ?>
	          <tr>
	             <th>Ref</td>
	             <th>Field</th>
	             <th>Purpose</th>
	             <th>Rules</th>
	             <th></td>
	          </tr>
            <?php
	            foreach($glossaries as $index => $glossary)
	            {  
	        ?>
	          <tr id="tr_<?php echo $glossary['id']; ?>">
	             <td><?php echo $glossary['id']; ?></td>
	             <td><?php echo $headers[$glossary['field']]; ?></td>
	             <td><?php echo $glossary['purpose']; ?></td>
	             <td><?php echo $glossary['rules']; ?></td>
	             <td>
	                <input type="button" name="edit" value="Edit" id="<?php echo $glossary['id']; ?>" class="edit" />
	                <input type="button" name="delete" value="Delete" id="delete_<?php echo $glossary['id']; ?>" class="delete" />                
	             </td>
	          </tr>
	        <?php    
	            }  
	         }
	       ?>
	     <table>
	   </td>
	</tr>
</table>

