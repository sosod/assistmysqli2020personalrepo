<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder gray"  style="text-align:center;" colspan="2">
			<select id="evaluationyear" name="evaluationyear">
				<option value="">--select evaluation year--</option>
			</select>
		</td>
	</tr>
	<tr>
	   <td colspan="2" class="noborder">
			<table id="table_incentives">
			  <tr id="incent">
			    <td colspan="7">
			      <input type="button" name="addnew" id="addnew" value=" Add " />
			      <input type="hidden" name="evaluationyearid" id="evaluationyearid" value="" />
			    </td>
			  </tr>
			  <tr>
			    <th>Ref</th>
			    <th>Rating(%)</th>
			    <th>Incentives Percentage</th>
			    <th>Rating Description</th>
			    <th>Colour assigned to rating</th>
			    <th>Status</th>
			    <th></th>
			  </tr>
			  <tr>
			  	<td class="incentives" colspan="7">Select the evaluation year to view the incentives setups</td>
			  </tr>
			</table> 
	   </td>
	</tr>
	<tr>
	  <td class="noborder"><?php displayGoBack("",""); ?></td>
	  <td class="noborder"><?php displayAuditLogLink("list_incentives_logs", true)?></td>
	</tr>
</table>
