<?php
$scripts = array("evaluationfrequencies.js");
$pagetitle = "Evaluation Frequencies";
require_once 'header.php';
$section = key($_SESSION['breadcrumb']);
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder gray" style="text-align:center;">
			<select id="evaluationyear" name="evaluationyear">
				<option>--evaluation year--</option>
			</select>
		</td>
	</tr>
	<tr>
	   <td colspan="2" class="noborder">
			<table id="table_evaluationfreq">
			 <tr id="addfrequency" style="display:<?php echo ($section == "admin" ? "none" : "table-row"); ?>">
			     <td colspan="2">
			       <input type="button" name="addnew" id="addnew" value="Add New" />
			       <input type="hidden" name="evaluationyearid" id="evaluationyearid" value="" />
			       <input type="hidden" name="section" id="section" value="<?php echo $section; ?>" /> 
			     </td>
			     <td colspan="3" style="text-align:right">
			       <input type="button" name="copy" id="copy" value="Copy from prior year" /> 
			     </td>
			 </tr>
			 <tr>
			  <th>Ref</th>
			   <th>Frequency Name</th>
			   <th>Number of evaluation periods</th>
			   <th>Status</th>
			   <th></th>
			 </tr>
			 <tr>
			 	<td colspan="5" class="freq">
			 		Select the evaluation year to view the evaluation frequencies
			 	</td>
			 </tr>
			</table>  
	   </td>
	</tr>
	<tr>
	  <td class="noborder">
	  	<?php displayGoBack("",""); ?>
 
	  </td>
	  <td class="noborder"><?php displayAuditLogLink("evaluationcommittee_logs", true)?></td>
	</tr>
</table>
