<?php
//include_once("../header.php");
?>
<table id="setuptable">
	<tr>
		<th>Menu Headings</th>
		<td>Define Menu Headings Naming Convention</td>
		<td>
			<input type="button" name="menu" id="menu" value="Configure" />
		</td>
	</tr>
	<tr>
		<th>Naming Convention</th>
		<td>Define Naming Convention</td>
		<td>
			<input type="button" name="naming" id="naming" value="Configure" />
		</td>
	</tr>
	<tr>
		<th>Evaluation Years</th>
		<td>Define Evaluation Years (and Frequency Periods - i.e. Start and End Date)</td>
		<td>
			<input type="button" name="evaluationyears" id="evaluationyears" value="Configure" />
		</td>
	</tr>		
	<tr>
		<th>Categories</th>
		<td>Define Performance Management Categories</td>
		<td>
			<input type="button" name="performancecategories" id="performancecategories" value="Configure" />
		</td>
	</tr>	
	<tr>
		<th>Job Levels</th>
		<td>Link Organisational Job Levels to Performance Management Categories</td>
		<td>
			<input type="button" name="joblevel" id="joblevel" value="Configure" />
		</td>
	</tr> 
	<tr>
		<th>Components</th>
		<td>Define Performance Management Components</td>
		<td>
			<input type="button" name="component" id="component" value="Configure" />
		</td>
	</tr>				
	<tr>
		<th>Evaluation Frequencies</th>
		<td>Define Evaluation Frequencies</td>
		<td>
			<input type="button" name="evaluationfrequencies" id="evaluationfrequencies" value="Configure" />
		</td>
	</tr>						
	<tr>
		<th>Rating Scale</th>
		<td>Define Rating Scale</td>
		<td>
			<input type="button" name="ratingscale" id="ratingscale" value="Configure" />
		</td>
	</tr>
	<tr>
		<th>Component Statuses</th>
		<td>Define Performance Management Component Statuses</td>
		<td>
			<input type="button" name="componentstatus" id="componentstatus" value="Configure" />
		</td>
	</tr>									
	<tr>
		<th>KPA Statuses</th>
		<td>Define KPA Statuses</td>
		<td>
			<input type="button" name="kpastatus" id="kpastatus" value="Configure" />
		</td>
	</tr>										
	<tr>
		<th>KPI Statuses</th>
		<td>Define KPI Statuses</td>
		<td>
			<input type="button" name="kpistatus" id="kpistatus" value="Configure" />
		</td>
	</tr>											
	<tr>
		<th>Action Statuses</th>
		<td>Define Action Statuses</td>
		<td>
			<input type="button" name="actionstatus" id="actionstatus" value="Configure" />
		</td>
	</tr>												
	<tr>
		<th>Evaluation Committee</th>
		<td>Define Evaluation Committee</td>
		<td>
			<input type="button" name="evaluationcommittee" id="evaluationcommittee" value="Configure" />
		</td>
	</tr>													
	<tr>
		<th>Incentive/Bonus</th>
		<td>Define Incentive / Bonus Parameters</td>
		<td>
			<input type="button" name="incentives" id="incentives" value="Configure" />
		</td>
	</tr>
<!--	<tr>
		<th>Notification Rules</th>
		<td>Define Notification Rules</td>
		<td>
			<input type="button" name="notificationrules" id="notificationrules" value="Configure" />
		</td>
	</tr>														-->
	<tr>
		<th>Columns View</th>
		<td>Define Columns to view</td>
		<td>
			<input type="button" name="naming" id="columns" value="Configure" />
		</td>
	</tr>
	<tr>
		<th>National KPAs</th>
		<td>Define National KPAs</td>
		<td>
			<input type="button" name="nationalkpa" id="nationalkpa" value="Configure" />
		</td>
	</tr>	
	<tr>
		<th>Company Objectives</th>
		<td>Define Company Objectives</td>
		<td>
			<input type="button" name="nationalobjectives" id="nationalobjectives" value="Configure" />
		</td>
	</tr>																
	<!-- <tr>
		<th>Organisational Goals</th>
		<td>Define Organisational Goals (If no SBDIP) - Objectives, Priorities</td>
		<td>
			<input type="button" name="component" id="organisational" value="Configure" />
		</td>
	</tr>																	
	<tr>
		<th>Managerial KPAs</th>
		<td>Define Master Managerial KPAs and assign to PM Categories</td>
		<td>
			<input type="button" name="component" id="managerial" value="Configure" />
		</td>
	</tr>
	<tr>
		<th>Master Individual KPAs</th>
		<td>Define Master Individual KPAs and assign to PM Categories</td>
		<td>
			<input type="button" name="component" id="individual" value="Configure" />
		</td>
	</tr> -->																				
	<tr>
		<th>Performance Matrix</th>
		<td>Build Master Performance Matrix</td>
		<td>
			<input type="button" name="performancematrix" id="performancematrix" value="Configure" />
		</td>
	</tr>																		
	<!--
	<tr>
		<th>Template Performance</th>
		<td>Define Templace Performance Contract(s)<b>(Per PM Category)</b></td>
		<td>
			<input type="button" name="templateperformance" id="templateperformance" value="Configure" />
		</td>
	</tr>																			
	
	<tr>
		<th>SBDIP</th>
		<td>Import SBDIP</td>
		<td>
			<input type="button" name="templateperformance" id="templateperformance" value="Configure" />
		</td>
	</tr>
	<tr>
		<th>Approval Matrix</th>
		<td>Define Manage >> Edit approval matrix (incl. Manager - Select Default)</td>
		<td>
		  <input type="button" name="approvalmatrix" id="approvalmatrix" value="Configure" />
		</td> 
	<tr>-->
	<tr>
		<th>Glossary</th>
		<td>Define Glossary</td>
		<td>
		  <input type="button" name="glossary" id="glossary" value="Configure" />
		  <input type="hidden" name="parentid" id="parentid" value="<?php echo $_GET['parent']; ?>" />
		</td> 
	<tr>	
</table>
