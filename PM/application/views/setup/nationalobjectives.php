<?php JSdisplayResultObj(""); ?> 
<table class="noborder">
	<tr id="national">
		<td class="noborder gray" colspan="2" style="text-align:center;">
			<select name="evaluationyear" id="evaluationyear">
				<option value="">--select evaluation year--</option>
			</select>
		</td>
	</tr>
	<tr>
	   <td colspan="2" class="noborder">
			<table id="table_nationalobjectives">
			  <tr id="natObj">
			    <td colspan="6">
			      <input type="button" name="addnew" id="addnew" value="Add" />
			      <input type="hidden" name="evaluationyearid" id="evaluationyearid" value="" />
			    </td>
			  </tr>
			  <tr>
			    <th>Ref</th>
			    <th>Short Code</th>
			    <th>National Objectives</th>
			    <th>National Objectives Description</th>
			    <th>Status</th>
			    <th></th>
			  </tr>
			  <tr>
			  	<td class="national" colspan="6">
			  		Select the evaluation year to view the national objectives setup
			  	</td>
			  </tr>
			</table>
	   </td>
	</tr>
	<tr>
	  <td class="noborder"><?php displayGoBack("",""); ?></td>
	  <td class="noborder"><?php displayAuditLogLink("list_nationalobjectives_logs", true)?></td>
	</tr>
</table>
