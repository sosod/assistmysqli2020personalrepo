<?php JSdisplayResultObj(""); ?>
<form id="joblevel_form" name="joblevel_form" method="post">
     <table class="noborder">
	     <tr>
	         <td colspan="2" class="noborder gray" style="text-align:center;">
			     <select id="evaluationyear" name="evaluationyear">
			       <option>--select evaluation year--</option>
			     </select>    
	         </td>	
	     </tr> 
         <tr>
           <td colspan="2"  class="noborder">
             <table id="joblevel_table">
                 <tr>
                     <th>Job Level</th>
                     <th>Organisation Position - Job Level</th>
                     <th>Perfomance Categories</th>
                     <th></th>
                 </tr> 
                 <tr class="joblevel">
                     <td colspan="5">Please select the evaluation year to configure the job levels</td>
                 </tr>             
             </table>      
       
           </td>
         </tr>
       <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("category_joblevel_logs", true)?></td>
       </tr>    
     </table>
</form>
