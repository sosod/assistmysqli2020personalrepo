<?php JSdisplayResultObj(""); ?> 
<table class="noborder">
	<tr>
		<td class="noborder gray" colspan="2" style="text-align:center;">
			<select id="evaluationyear" name="evaluationyear">
			  <option value="">--select evaluation year--</option>
			</select>
		</td>
	</tr>
	<tr>
	   <td colspan="2" class="noborder">
			<table id="table_nationalkpa">
			  <tr id="tr_addnew">
			    <td colspan="6">
			      <input type="button" name="addnew" id="addnew" value="Add" />
			      <input type="hidden" name="evaluationyearid" id="evaluationyearid" value="" />
			    </td>
			  </tr>
			  <tr>
			    <th>Ref</th>
			    <th>Short Code</th>
			    <th>National KPA</th>
			    <th>National KPA Description</th>
			    <th>Status</th>
			    <th></th>
			  </tr>
			  <tr class="natKpa">
			  	<td colspan="6">
			  		Select the evaluation year to view the national KPA setup
			  	</td>
			  </tr>
			</table>
	   </td>
	</tr>
	<tr>
	  <td class="noborder"><?php displayGoBack("",""); ?></td>
	  <td class="noborder"><?php displayAuditLogLink("list_nationalkpa_logs", true)?></td>
	</tr>
</table>

