<?php JSdisplayResultObj(""); 

?> 
<table class="noborder">
	<tr>
	   <td colspan="2" class="noborder">
      		<form action="" id="editevaluationyears_form" name="editevaluationyears_form">
			<table id="tableevaluationyears">
				<tr>
				  <th>Ref</th>
				  <th>Last Day of the Evaluation Year</th>
				  <th>Start Date</th>
				  <th>End Date</th>
				</tr>
				
				<?php 
					if(!empty($evaluationyear))
					{
			     ?>		
						<tr>
						  <td><?php echo $evaluationyear['id']; ?></td>
						  <td><?php echo $evaluationyear['lastday']; ?></td>
						  <td><?php echo $evaluationyear['start']; ?></td>
						  <td><?php echo $evaluationyear['end']; ?></td>
						</tr>	
				<?php 	
					}
				?>
				<?php 
				     $x = 1;
					foreach($evalPeriods as $index => $period)
					{				               
						$periodFreq = array();
						$keyId = "";
						if(isset($frequency[$period['id']]) && !empty($frequency[$period['id']]))
						{						
							$keyId = key($frequency);
							$periodFreq = $frequency[$period['id']];
						}
					?>		
						<tr>
						  <td><input type="checkbox" name="period_<?php echo $period['id']; ?>" id="period_<?php echo $period['id']; ?>" value="<?php echo $period['id']; ?>" checked="<?php echo ($period['id'] == $keyId ? "checked" : "")?>"/></td>
						  <td><?php echo $period['name']; ?></td>
						  <td colspan="2">
						  	<table>
						 <?php 
                                   $count = $period['period'];			     
                                   if(empty($frequency))
                                   {
						 	for($i = 1; $i <= $count; $i++)
						 	{
						 	     $x++;
						     	$startdate = "";
						     	$enddate   = "";	
	
						 ?>
						 	<tr>
							  <td><input type="text" name="<?php echo $period['id']; ?>_start_<?php echo $i; ?>" id="<?php echo $period['period'].'_'.($index+1); ?>_start_<?php echo $i; ?>" value="<?php echo ($i == 1 ? $evaluationyear['start'] : $startdate ); ?>" class="date" readonly="readonly" /></td>
							  <td><input type="text" name="<?php echo $period['id']; ?>_end_<?php echo $i; ?>" id="<?php echo $period['period'].'_'.($index+1); ?>_end_<?php echo $i; ?>" value="<?php echo ($i == $period['period'] ? $evaluationyear['end'] : $enddate ); ?>" class="date" readonly="readonly" /></td>
							</tr>
						  <?php 
						       $x++;
						 	}
						    } else {
						      $i = 0;
						      $count = count($frequency[$period['id']]);
						      
						      foreach($frequency[$period['id']] as $freqId => $freqPeriod)
						      {
						          $i++;
                                        $startdate = "";
                                        $enddate   = "";	
                                        $id = $freqPeriod['id'];
					          	if(isset($freqPeriod['start_date']))
					          	{
					          	   $startdate = $freqPeriod['start_date'];
					          	}
					          	if(isset($freqPeriod['end_date']))
					          	{
					          	   $enddate = $freqPeriod['end_date'];
					          	}						      
						      
						  ?>
						 	<tr>
							  <td><input type="text" name="<?php echo $period['id']; ?>_start_<?php echo $id; ?>" id="<?php echo $period['period'].'_'.($index+1); ?>_start_<?php echo $id; ?>" value="<?php echo ($i == 1 ? $evaluationyear['start'] : $startdate ); ?>" class="date" readonly="readonly" /></td>
							  <td><input type="text" name="<?php echo $period['id']; ?>_end_<?php echo $id; ?>" id="<?php echo $period['period'].'_'.($index+1); ?>_end_<?php echo $id; ?>" value="<?php echo ($x == $count ? $evaluationyear['end'] : $enddate ); ?>" class="date" readonly="readonly" /></td>
							</tr>
						  <?php
						          $x++;
						     }
						  }
						  ?>
							</table>
						  </td>
						</tr>	
				<?php 						
					}
				?>
				<tr>
					<td colspan="4" style="text-align:right;">
						<input type="button" name="savechanges" id="savechanges" value="Save Changes " />
					</td>
				</tr>
			</table>
			</form>	   
	   </td>
	</tr>
	<tr>
	  <td class="noborder">
	  	<?php displayGoBack("",""); ?>
        <input type="hidden" name="evaluationyearid" id="evaluationyearid" value="<?php echo $_GET['id']; ?>" />
	  </td>
	  <td class="noborder"><?php displayAuditLogLink("evaluationyears_logs", true)?></td>
	</tr>
</table>
