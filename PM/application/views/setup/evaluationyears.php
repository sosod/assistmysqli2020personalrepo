<?php
$section = "";
if(isset($_SESSION['breadcrumb']))
{
    $section = key($_SESSION['breadcrumb']);
}

?>
<?php JSdisplayResultObj(""); ?> 
<table class="noborder">
	<tr>
	   <td colspan="2" class="noborder">
		<table id="table_evaluationyears">
		    <tr style="display:<?php echo ($section == "admin" ? "none" : "table-row"); ?>">	
		     <td colspan="6">
		        <input type="button" name="add" id="add" value="Add New" />
		        <input type="hidden" name="section" id="section" value="<?php echo $section; ?>" /> 
		     </td>
		    </tr>
			<tr>
			  <th>Ref</th>
			  <th>Last Day of the Evaluation Year</th>
			  <th>Start Date</th>
			  <th>End Date</th>
			  <th style="display:<?php echo ($section == "admin" ? "table-cell" : "none"); ?>">Status</th>
		  	  <th></th>
		  	  <th></th>	  	  
			</tr>
		</table>	   
	   </td>
	</tr>
	<tr>
	  <td class="noborder">
	  	<?php displayGoBack("",""); ?>
         <input type="hidden" name="section" id="section" value="<?php echo $section; ?>" />
  	  </td>
	  <td class="noborder"><?php displayAuditLogLink("list_evaluation_years_logs", true)?></td>
	</tr>
</table>
