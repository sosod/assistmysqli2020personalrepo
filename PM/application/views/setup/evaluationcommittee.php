<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder gray" style="text-align:center;">
			<select id="evaluationyear" name="evaluationyear">
				<option>--select evaluation year--</option>
			</select>
		</td>
	</tr>
	<tr>
	   <td colspan="2" class="noborder">
			<table id="table_evaluationcommittee">
			 <tr id="addevaluationcommittee" style="display:none">
			     <td colspan="5">
			       <input type="button" name="addnew" id="addnew" value="Add New" />
			       <input type="hidden" name="evaluationyearid" id="evaluationyearid" value="" />
			       <input type="hidden" name="section" id="section" value="<?php echo $section; ?>" /> 
			     </td>
			 </tr>
			 <tr>
			  <th>Ref</th>
			   <th>Evaluation Committee Title</th>
			   <th>Committee Members</th>
			   <th>Status</th>
			   <th></th>
			 </tr>
			 <tr>
			 	<td colspan="5" class="evaluationcommittee">
			 		Select the evaluation year to view the evaluation committee
			 	</td>
			 </tr>
			</table>  
	   </td>
	</tr>
	<tr>
	  <td class="noborder">
	  	<?php displayGoBack("",""); ?>
 
	  </td>
	  <td class="noborder"><?php displayAuditLogLink("list_evaluation_committee_logs", true)?></td>
	</tr>
</table>
