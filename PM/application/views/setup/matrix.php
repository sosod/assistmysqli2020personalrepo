<script type="text/javascript" src="/library/jquery/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="/library/jquery/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/library/jquery/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="public/js/default.js"></script>
<script src="public/js/matrix.js"></script> 
<?php
$compId = $_GET['id'];
//debug($matrix);
?>
<?php 
JSdisplayResultObj(""); 
if(isset($components) && !empty($components))
{
?>
<div id="loading" style="display:none; position:absolute; z-index:999; background-color:#000000; color:#FFFFFF;"><img src="../resources/images/ajax-loader.gif" /></div>
<form id="performancematrixform_<?php echo $compId; ?>" name="performancematrixform_<?php echo $compId; ?>">
     <table id="performancematrixtable_<?php echo $compId; ?>" class="matrixtable" class="noborder">
        <tr>
          <td class="noborder"></td>
          <td class="noborder" colspan="<?php echo ($totalCat + 1); ?>"><b>Performance Management Components</b></td>
        </tr>
        <tr>
          <th></th>
          <?php
          foreach($components as $index => $cat)
          {
	        echo "<th>".$cat['name']."</th>";					
          }
          ?>
        </tr>
        <tr>
		<th>Use Perfomanace Components</th>
		  <?php
		   foreach($components as $index => $cat)
		   {		
            ?>
		     <td>
		     <?php
	             if(isset($matrix[$cat['id']]['use_component']))
	             {
	           ?>
                     <input type="checkbox" name="use_component[<?php echo $cat['id']; ?>][<?php echo $compId ?>]; ?>" id="usecat_<?php echo $cat['id'].'_'.$compId; ?>" value="<?php echo $cat['id'].'_'.$compId; ?>" class="usecomponent_<?php echo $compId; ?>" checked="checked" />
	           <?php
	             } else {
                ?>
                   <input type="checkbox" name="use_component[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]; ?>" id="usecat_<?php echo $cat['id'].'_'.$compId; ?>" value="<?php echo $cat['id'].'_'.$compId; ?>" class="usecomponent_<?php echo $compId; ?>" />
               <?php
	            }
	          ?>
	          </td>
	     <?php				
		  }
		?>
        </tr>
	   <tr>
		 <th>Performance Component(<em>%</em>)</th>
           <?php
              foreach($components as $index => $cat)
              { 
                  $currentValue = 0;
                  if(isset($matrix[$cat['id']]['component_weight']) && !empty($matrix[$cat['id']]['component_weight']))
                  {
                    $currentValue = $matrix[$cat['id']]['component_weight'];
                  }
                  $upperLimit = 100 - $totalW + $currentValue;
	      ?>
	      <td>
		    <select name="component_weight[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]; ?>" id="weighting_<?php echo $cat['id'].'_'.$compId; ?>" class="componentweight_<?php echo $compId; ?> weighting">
	          <?php     
	           if($upperLimit != 0)
	           {
	                for($i = 1; $i <= $upperLimit; $i++) 
	                {
	                ?>
		             <option value="<?php echo $i; ?>"
		             <?php 
			            if(isset($matrix[$cat['id']]['component_weight']))
			            {
				         if($currentValue == $i)
				          {
				                  echo "selected='selected'";
				          }	
			            }
		               ?>
		               ><?php echo $i; ?></option>
	               <?php
	                } 
	          } else { 
               ?>
	           <option value="<?php echo (isset($matrix[$cat['id']]['component_weight']) ? $matrix[$cat['id']]['component_weight'] : '1'); ?>">
	              <?php echo (isset($matrix[$cat['id']]['component_weight']) ? $matrix[$cat['id']]['component_weight'] : "1"); ?>
	           </option>
              <?php
                }
              ?>
	          </select>
	      </td>
          <?php 					
	       }
          ?>
		 <td><span id="total_<?php echo $compId; ?>" style="padding:5px;" class="<?php echo ($totalW != 100 ? 'ui-state ui-state-error' : ''); ?>" ><b><?php echo $totalW."%"; ?></b></span></td>
	   </tr> 
        <tr>
		<th>Evaluation On</th>
		<?php
			foreach($components as $index => $cat)
			{
			?>
				<td>
					<select name="evaluate_on[<?php echo $cat['id']; ?>][<?php echo $compId ?>]; ?>" id="ratelevel_<?php echo $cat['id'].'_'.$compId; ?>">
						<option value="kpa"
						<?php 
							if(isset($matrix[$cat['id']]['evaluate_on']))
							{
								if($matrix[$cat['id']]['evaluate_on'] == "kpa")
								{
								 echo "selected='selected'";
								}	
							}
						?>>KPA</option>
						<option value="kpi"
						<?php 
							if(isset($matrix[$cat['id']]['evaluate_on']))
							{
								if($matrix[$cat['id']]['evaluate_on'] == "kpi")
								{
								 echo "selected='selected'";
								}	
							}
						?>								
						>KPI</option>
						<option value="action"
						<?php 
							if(isset($matrix[$cat['id']]['evaluate_on']))
							{
								if($matrix[$cat['id']]['evaluate_on'] == "action")
								{
								 echo "selected='selected'";
								}	
							}
						?>															
						>Action</option>
					</select>
				</td>
			<?php 					
			}
		?>
	   </tr>	
	   <tr>
	     <th>Use Competencies</th>
		  <?php
		  foreach($components as $index => $cat)
		   {
		  ?>
			<td>
			  <select name="use_competincies[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]; ?>" id="usecompetencies_<?php echo $cat['id']."_".$compId; ?>">
			     <option value="1"
				 <?php 
					if(isset($matrix[$cat['id']]['use_competincies']))
					{
					  if($matrix[$cat['id']]['use_competincies'] == "1")
					  {
					     echo "selected='selected'";
					  }	
					}
				  ?>															
					>Yes</option>
					<option value="0"
					<?php 
					if(isset($matrix[$cat['id']]['use_competincies']))
					{
					   if($matrix[$cat['id']]['use_competincies'] == "0")
					   {
					     echo "selected='selected'";
					   }	
					}
					?>																			
					>No</option>
				</select>
			</td>
		 <?php 					
			}
		 ?>
        </tr>	       
	   <tr>
		<th>Use Proficiencies</th>
		 <?php
		   foreach($components as $index => $cat)
		   {
		 ?>
			<td>
			  <select name="use_proficiencies[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]" id="useproficiencies_<?php echo $cat['id'].'_'.$compId; ?>">
				<option value="1"
					<?php 
						if(isset($matrix[$cat['id']]['use_proficiencies']))
						{
							if($matrix[$cat['id']]['use_proficiencies'] == "1")
							{
							 echo "selected='selected'";
							}	
						}
					?>										
					>Yes</option>
					<option value="0"
					<?php 
						if(isset($matrix[$cat['id']]['use_proficiencies']))
						{
							if($matrix[$cat['id']]['use_proficiencies'] == "0")
							{
							 echo "selected='selected'";
							}	
						}
					?>										
					>No</option>
				</select>
			</td>
			<?php 					
			}
		?>
        </tr>   
	   <tr>
		<th>Self Evaluation</th>
	     <?php
		  foreach($components as $index => $cat)
		  {
		 ?>
		  <td>
			<select name="self_evaluation[<?php echo $cat['id'] ?>][<?php echo $compId; ?>]" id="selfevaluation_<?php echo $cat['id'].'_'.$compId; ?>">
				<option value="1"
				<?php 
					if(isset($matrix[$cat['id']]['self_evaluation']))
					{
						if($matrix[$cat['id']]['self_evaluation'] == "1")
						{
						 echo "selected='selected'";
						}	
					}
				?>										
				>Yes</option>
				<option value="0"
				<?php 
					if(isset($matrix[$cat['id']]['self_evaluation']))
					{
					   if($matrix[$cat['id']]['self_evaluation'] == "0")
					   {
						 echo "selected='selected'";
					    }	
					}
				?>											
				>No</option>
			</select>
		  </td>
		<?php 					
		 }
		?>
	   </tr>   
	   <tr>
		 <th>Manager Evaluation</th>
		  <?php
			foreach($components as $index => $cat)
			{
		   ?>
		  <td>
			<select name="manager_evaluation[<?php echo $cat['id'] ?>][<?php echo $compId; ?>]" id="managerevaluation_<?php echo $cat['id'].'_'.$compId; ?>">
				<option value="1"
				<?php 
					if(isset($matrix[$cat['id']]['manager_evaluation']))
					{
					   if($matrix[$cat['id']]['manager_evaluation'] == "1")
					   {
						 echo "selected='selected'";
					   }	
					}
				?>																			
				>Yes</option>
				<option value="0"
				<?php 
					if(isset($matrix[$cat['id']]['manager_evaluation']))
					{
						if($matrix[$cat['id']]['manager_evaluation'] == "0")
						{
						 echo "selected='selected'";
						}	
					}
				?>										
				>No</option>
			</select>
		  </td>
		 <?php 					
			}
		 ?>
	   </tr>		             
	   <tr>
		 <th>Joint Evaluation</th>
		<?php
		 foreach($components as $index => $cat)
		 {
		?>
		<td>
	       <select name="joint_evaluation[<?php echo $cat['id'] ?>][<?php echo $compId; ?>]" id="jointevaluation_<?php echo $cat['id'].'_'.$compId; ?>">
				<option value="1"
				<?php 
					if(isset($matrix[$cat['id']]['joint_evaluation']))
					{
						if($matrix[$cat['id']]['joint_evaluation'] == "1")
						{
						 echo "selected='selected'";
						}	
					}
				?>										
				>Yes</option>
				<option value="0"
				<?php 
					if(isset($matrix[$cat['id']]['joint_evaluation']))
					{
						if($matrix[$cat['id']]['joint_evaluation'] == "0")
						{
						 echo "selected='selected'";
						}	
					}
				?>										
				>No</option>
			</select>
		</td>
		<?php 					
		}
		?>
	   </tr>		
	   <tr>
		<th>Evaluation Review</th>
		<?php
		  foreach($components as $index => $cat)
		  {
		 ?>
		<td>
		  <select name="evaluation_review[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]" id="evaluationreview_<?php echo $cat['id'].'_'.$compId; ?>">
				<option value="1"
				<?php 
					if(isset($matrix[$cat['id']]['evaluation_review']))
					{
						if($matrix[$cat['id']]['evaluation_review'] == "1")
						{
						 echo "selected='selected'";
						}	
					}
				?>									
				>Yes</option>
				<option value="0"
				<?php 
					if(isset($matrix[$cat['id']]['evaluation_review']))
					{
						if($matrix[$cat['id']]['evaluation_review'] == "0")
						{
						 echo "selected='selected'";
						}	
					}
				?>										
				>No</option>
			</select>
		</td>
		<?php 					
		  }
		?>
	   </tr>	  
	   <tr>
	     <th>Evaluation Frequency</th>
	     <?php
		   foreach($components as $index => $cat)
		   {
		?>
	     <td>
		  <select name="evaluation_frequency[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]" id="evaluationfreq_<?php echo $cat['id'].'_'.$compId; ?>" class="evaluationfrequency">
			     <?php 
				   foreach($evalfreq as $i => $freq)
				   {
			     ?>
				     <option value="<?php echo $freq['id']; ?>"
				     <?php 
					     if(isset($matrix[$cat['id']]['evaluation_frequency']))
					     {
						     if($matrix[$cat['id']]['evaluation_frequency'] == $freq['id'])
						     {
						       echo "selected='selected'";
						     }	
					     }
				     ?>											
				     ><?php echo $freq['name']; ?></option>
			     <?php 
				   }
			     ?>
		     </select>
	     </td>
		<?php 					
		   }
	     ?>
	   </tr>
        <tr>
		<th>Template to be used</th>
		<?php
		  foreach($components as $index => $cat)
		  {
		?>
		 <td>
		   <select name="use_template[<?php echo $cat['id'] ?>][<?php echo $compId; ?>]" id="masterlist_<?php echo $cat['id'].'_'.$compId; ?>">
				<option value="1"
					<?php 
						if(isset($matrix[$cat['id']]['use_template']))
						{
							if($matrix[$cat['id']]['use_template'] == 1)
							{
							 echo "selected='selected'";
							}	
						}
					?>																			
				>Yes</option>
				<option value="0"
				<?php 
					if(isset($matrix[$cat['id']]['use_template']))
					{
						if($matrix[$cat['id']]['use_template'] == 0)
						{
						 echo "selected='selected'";
						}	
					}
				?>										
				>No</option>
			</select>
		</td>
	     <?php 					
		  }
		?>
	   </tr>
        <tr>
		<th>KPA Weighting to be used</th>
		<?php
		   foreach($components as $index => $cat)
		   {
		?>
		  <td>
			<select name="use_kpa_weighting[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]" id="kpaweighting_<?php echo $cat['id'].'_'.$compId; ?>">
				<option value="1"
				<?php 
					if(isset($matrix[$cat['id']]['use_kpa_weighting']))
					{
						if($matrix[$cat['id']]['use_kpa_weighting'] == 1)
						{
						 echo "selected='selected'";
						}	
					}
				?>																		
				>Yes</option>
				<option value="0"
				<?php 
					if(isset($matrix[$cat['id']]['use_kpa_weighting']))
					{
						if($matrix[$cat['id']]['use_kpa_weighting'] == 0)
						{
						 echo "selected='selected'";
						}	
					}
				?>										
				>No</option>
			</select>
		   </td>
		<?php 					
		   }
		?>
	   </tr>	 
	   <tr>
		<th>KPI Weighting to be used</th>
		<?php
		   foreach($components as $index => $cat)
		   {
		?>
		 <td>
			<select name="use_kpi_weighting[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]" id="kpiweighting_<?php echo $cat['id'].'_'.$compId; ?>">
				<option value="1"
				<?php 
					if(isset($matrix[$cat['id']]['use_kpi_weighting']))
					{
					   if($matrix[$cat['id']]['use_kpi_weighting'] == 1)
					   {
						 echo "selected='selected'";
					   }	
					}
				?>										
				>Yes</option>
				<option value="0"
				<?php 
					if(isset($matrix[$cat['id']]['use_kpi_weighting']))
					{
						if($matrix[$cat['id']]['use_kpi_weighting'] == 0)
						{
						 echo "selected='selected'";
						}	
					}
				?>																		
				>No</option>
			</select>
		   </td>
		<?php 					
		  }
		?>
	   </tr>	 
	   <tr>
	     <th>Action Weighting to be used</th>
	     <?php
		   foreach($components as $index => $cat)
		   {
		?>
	     <td>
		     <select name="use_action_weighting[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]" id="actionweighting_<?php echo $cat['id'].'_'.$compId; ?>">
			     <option value="1"
			     <?php 
				     if(isset($matrix[$cat['id']]['use_action_weighting']))
				     {
					     if($matrix[$cat['id']]['use_action_weighting'] == 1)
					     {
					      echo "selected='selected'";
					     }	
				     }
			     ?>																			
			     >Yes</option>
			     <option value="0"
			     <?php 
				     if(isset($matrix[$cat['id']]['use_action_weighting']))
				     {
					   if($matrix[$cat['id']]['use_action_weighting'] == 0)
					   {
					      echo "selected='selected'";
					   }	
				     }
			     ?>									
			     >No</option>
		     </select>
	     </td>
		<?php 					
		   }
	     ?>
	   </tr>	  
	   <tr>
	     <th>Minimum Number of KPAs</th>
	     <?php
		  foreach($components as $index => $cat)
		  {
			 $minKpa = 1;
			 if(isset($matrix[$cat['id']]['min_kpa']))
			 {
			   $minKpa =  $matrix[$cat['id']]['min_kpa'];
			 }			          
		 ?>
		 <td>
		       <select name="min_kpa[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]" id="minkpa_<?php echo $cat['id'].'_'.$compId ?>">
		        <?php 
		          for($k = 1; $k <= 50; $k++)
		          {
		        ?>
		          <option value="<?php echo $k; ?>"  <?php echo ($minKpa == $k ? "selected='selected'" : ""); ?>><?php echo $k; ?></option>
		        <?php
		          }
		        ?>					     
		       </select> 
		 </td>
	     <?php 					
	       }
	     ?>		
	   </tr>	 
        <tr>
		<th>Maximum Number of KPAs</th>
		<?php
		   foreach($components as $index => $cat)
		   {
			 $maxKpa = 50;
			 if(isset($matrix[$cat['id']]['max_kpa']))
			 {
			   $maxKpa = $matrix[$cat['id']]['max_kpa'];
			 }			     
		?>
		<td>
		       <select name="max_kpa[<?php echo $cat['id']; ?>][<?php echo $compId ?>]" id="maxkpa_<?php echo $cat['id'].'_'.$compId ?>">
		        <?php 
		          for($n = 1; $n <= 50; $n++)
		          {
		        ?>
		          <option value="<?php echo $n; ?>"  <?php echo ($maxKpa == $n ? "selected='selected'" : ""); ?>><?php echo $n; ?></option>
		        <?php
		          }
		        ?>					     
		       </select>						
	     </td>
		<?php 					
		  }
		?>					
	   </tr>	
	   <tr>
		<th>Minimum Number of KPIs</th>
		<?php
		  foreach($components as $index => $cat)
		  {
			 $minKpi = 1;
			 if(isset($matrix[$cat['id']]['min_kpi']))
			 {
			   $minKpi = $matrix[$cat['id']]['min_kpi'];
			 }			  
		 ?>
		   <td>
			  <select name="min_kpi[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]" id="minkpi_<?php echo $cat['id'].'_'.$compId ?>">
			   <?php 
			     for($x = 1; $x <= 50; $x++)
			     {
			   ?>
			     <option value="<?php echo $x; ?>" <?php echo ($minKpi == $x ? "selected='selected'" : ""); ?>><?php echo $x; ?></option>
			   <?php
			     }
			   ?>					     
			  </select>
		   </td>
		<?php 					
		  }
		?>								
	   </tr>
        <tr>
		<th>Maximum Number of KPIs</th>
		   <?php
		     foreach($components as $index => $cat)
		     {
			   $maxkPI = 50;
			   if(isset($matrix[$cat['id']]['max_kpi']))
			   {
			      $maxkPI = $matrix[$cat['id']]['max_kpi'];
			   }			      
		    ?>
		     <td>
		       <select name="max_kpi[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]" id="max_kpi_<?php echo $cat['id'].'_'.$compId ?>">
		        <?php 
		          for($c = 1; $c <= 50; $c++)
		          {
		        ?>
		          <option value="<?php echo $c; ?>" <?php echo ($maxkPI == $c ? "selected='selected'" : ""); ?>><?php echo $c; ?></option>
		        <?php
		          }
		        ?>					     
		       </select>					
		     </td>
		<?php 					
		   }
		?>											
	   </tr>
	   <tr>
		<th>Minimum Number of Actions</th>
		<?php
			foreach($components as $index => $cat)
			{
			   $minAction = 1;
			   if(isset($matrix[$cat['id']]['min_action']))
			   {
			      $minAction = $matrix[$cat['id']]['min_action'];
			   }				     
			?>
			<td>
		       <select name="min_action[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]" id="minaction_<?php echo $cat['id'].'_'.$compId ?>">
		        <?php 
		          for($d = 1; $d <= 50; $d++)
		          {
		        ?>
		          <option value="<?php echo $d; ?>" <?php echo ($minAction == $d ? "selected='selected'" : ""); ?>><?php echo $d; ?></option>
		        <?php
		          }
		        ?>					     
		       </select>					     
			</td>
		<?php 					
		  }
		?>					
	   </tr>		   		   	             	      
	   <tr>
		<th>Maximum Number of Actions</th>
		 <?php
		   foreach($components as $index => $cat)
		   {
			 $maxAct = 50;
			 if(isset($matrix[$cat['id']]['max_action']))
			 {
			   $maxAct = $matrix[$cat['id']]['max_action'];
			 }	
		  ?>
		  <td>
		    <select name="max_action[<?php echo $cat['id']; ?>][<?php echo $compId; ?>]" id="maxaction_<?php echo $cat['id'].'_'.$compId ?>">
		      <?php 
		          for($i = 1; $i <= 50; $i++)
		          {
		       ?>
		         <option value="<?php echo $i; ?>" <?php echo ($maxAct == $i ? "selected='selected'" : ""); ?> ><?php echo $i; ?></option>
		       <?php
		          }
		       ?>					     
		     </select>
		   </td>
	      <?php 					
		   }
		 ?>										
	   </tr>        
        <tr>
            <td colspan="<?php echo ($totalCat+1); ?>" style="text-align:right;">
               <?php if(empty($matrix)) { ?>
	             <input type="button" name="save_<?php echo $compId; ?>" id="save_<?php echo $compId; ?>" value="Save" class="save" />
               <?php } else { ?>
	             <input type="button" name="savechanges_<?php echo $compId; ?>" id="savechanges_<?php echo $compId; ?>" value="Save Changes" class="savematrix" />
	             <!-- <input type="hidden" name="matrixid" id="matrixid_<?php echo $compId; ?>" value="<?php echo $matrixid  ; ?>" /> -->
               <?php } ?>
               <input type="hidden" name="categorytid" id="categorytid" value="<?php echo $_GET['id']; ?>" />
               <input type="hidden" name="evaluationyear" id="evaluationyear" value="<?php echo $_GET['evaluationyear']; ?>" />
            </td>
         </tr>
     </table>         				
</form>
<?php 
} else {
?>
<p>There are no performance components setup for the selected evaluation year</p>
<?php } ?>
