<?php 
$allow = array();
$key   = "";
if($component['id'] == "1")
{
   $key   = "organisational";
   $allow = array();
} else if($component['id'] == 2) {
   $key   = "managerial";
   $allow = array();
} else if($component['id'] == 3) {
   $key   = "individual";
   $allow = array("owner", "proof_of_evidence", "baseline", "targetunit", "performance_standard", "target_period", "nationalkpa", "idpobjectives");
}
?>
<?php JSdisplayResultObj(""); ?>
<form name="addactionform" id="addactionform">
<table class="noborder" width="100%">
 <tr>
    <td class="noborder">
      <table width="100%">
        <?php
         if($section != "admin")
         {
        ?>
        <tr>
            <th>Full Name</th>
            <td><span style="background-color:#EEE;"><?php echo $userdata['user']; ?></td>       
        </tr>	
        <?php 
        }
        ?>	
        <tr>
            <th><?php echo $header['evaluation_year']; ?></th>
            <td><span style="background-color:#EEE;" id="evalyeartext"><?php echo $year['start']." to ".$year['end']; ?></span></td>	        
        </tr> 
        <tr>
            <th>Component Name</th>
            <td><span style="background-color:#EEE;"><?php echo $component['name']; ?></td>
        </tr>	
        <tr>
            <th>KPI Ref</th>
            <td><span style="background-color:#EEE;"><?php echo $_GET['kpiid']; ?></td>       
        </tr>	         		    	        
        <tr>
            <th>KPI Name</th>
            <td><?php echo $kpi['name']; ?></td>	            
        </tr>
        <tr>
            <th><?php echo $header['objective']; ?></th>
            <td><?php echo $kpi['objective']; ?></td>	            
        </tr>
        <tr>
            <th><?php echo $header['outcome']; ?></th>
            <td><?php echo $kpi['outcome']; ?></td>	            
        </tr>
        <tr>
            <th><?php echo $header['measurement']; ?></th>
            <td><?php echo $kpi['measurement']; ?></td>	            
        </tr>
        <tr>
            <th><?php echo $header['proof_of_evidence']; ?></th>
            <td><?php echo $kpi['proof_of_evidence']; ?></td>	            
        </tr>
        <tr>
            <th><?php echo $header['deadline']; ?></th>
            <td><?php echo $kpi['deadline']; ?></td>	            
        </tr>      
        <?php
         if($section == "admin")
         {
        ?>			    	    
        <tr>
            <td>
           <?php displayGoBack("main.php?controller=component&action=".$key."&componentid=".$component['id']."&evaluationyear=".$evaluationyear."&parent=5&section=".$section."&folder=".$section."", ""); ?>	            
            </td>
            <td>&nbsp;</td>	        		    
        </tr>
        <?php 
        } else {
        ?>
        <tr>
            <td><?php displayGoBack("", ""); ?></td>
            <td>&nbsp;</td>	        		    
        </tr>        
        <?php 
        }
        ?>                     
      </table>
    </td>
    <td class="noborder">
        <table width="70%">
	        <tr>
		        <th><?php echo $header['name']; ?></th>
		        <td>
			        <textarea name="name" id="name"></textarea>
		        </td>
	        </tr>
	        <tr>
		        <th><?php echo $header['objective']; ?></th>
		        <td>
			        <textarea name="objective" id="objective"></textarea>
		        </td>
	        </tr>	
	        <tr>
		        <th><?php echo $header['outcome']; ?></th>
		        <td>
			        <textarea name="outcome" id="outcome"></textarea>
		        </td>
	        </tr>		
	        <tr>
		        <th><?php echo $header['measurement']; ?></th>
		        <td>
			        <textarea name="measurement" id="measurement"></textarea>
		        </td>
	        </tr>			
	        <!-- <tr  style="display:<?php echo (in_array('owner', $allow) ? 'none' : 'table-row'); ?>">
		        <th><?php echo $header['owner']; ?></th>
		        <td>
			        <select name="owner" id="owner">
			          <option value="">--owner--</option>
			        </select>
		        </td>
	        </tr> -->
	        <tr  style="display:<?php echo (in_array('proof_of_evidence', $allow) ? 'none' : 'table-row'); ?>">
		        <th><?php echo $header['proof_of_evidence']; ?></th>
		        <td>
			        <textarea name="proof_of_evidence" id="proof_of_evidence"></textarea>
		        </td>
	        </tr>	
	        <tr  style="display:<?php echo (in_array('baseline', $allow) ? 'none' : 'table-row'); ?>">
		        <th><?php echo $header['baseline']; ?></th>
		        <td>
			        <textarea name="baseline" id="baseline"></textarea>
		        </td>
	        </tr>	
	        <tr  style="display:<?php echo (in_array('targetunit', $allow) ? 'none' : 'table-row'); ?>">
		        <th><?php echo $header['targetunit']; ?></th>
		        <td>
			        <textarea name="targetunit" id="targetunit"></textarea>
		        </td>
	        </tr>																
	        <tr  style="display:<?php echo (in_array('performance_standard', $allow) ? 'none' : 'table-row'); ?>">
		        <th><?php echo $header['performance_standard']; ?></th>
		        <td>
			        <textarea name="performance_standard" id="performance_standard"></textarea>
		        </td>
	        </tr>									
	        <tr  style="display:<?php echo (in_array('target_period', $allow) ? 'none' : 'table-row'); ?>">
		        <th><?php echo $header['target_period']; ?></th>
		        <td>
			        <textarea name="target_period" id="target_period"></textarea>
		        </td>
	        </tr>										
	        <tr  style="display:<?php echo (in_array('weighting', $allow) ? 'none' : 'table-row'); ?>">
		        <th><?php echo $header['weighting']; ?> of action</th>
		        <td>
			        <select name="weighting" id="weighting">
				        <option value="">--weighting--</option>
			        </select> against the KPI
		        </td>
	        </tr>	
	        <?php
	          if($useCompetencies)
	          {
	        ?>	
	        <tr  style="display:<?php echo (in_array('competency', $allow) ? 'none' : 'table-row'); ?>">
		        <th><?php echo $header['competency']; ?></th>
		        <td>
			        <select name="competency" id="competency">
			          <option>--competency--</option>
			        </select>
		        </td>
	        </tr>
	        <?php 
	         } 
	         if($useProficiencies)
	         {
	        ?>	
	        <tr  style="display:<?php echo (in_array('proficiency', $allow) ? 'none' : 'table-row'); ?>">
		        <th><?php echo $header['proficiency']; ?></th>
		        <td>
			        <select name="proficiency" id="proficiency">
			          <option>--proficiency--</option>
			        </select>
			        <!-- <p>
				        <input type="button" name="addanother" id="addanother" value="Add Another" class="profieciency_competency" />
		            </p> -->
		        </td>
	        </tr>	
	        <?php 
	        }
	        ?>												
	        <tr  style="display:<?php echo (in_array('deadline', $allow) ? 'none' : 'table-row'); ?>">
		        <th><?php echo $header['deadline']; ?></th>
		        <td>
		          <input type="text" name="deadline" id="deadline" class="datepicker" value="" readonly="readonly" />
		        </td>
	        </tr>
	        <tr  style="display:<?php echo (in_array('comments', $allow) ? 'none' : 'table-row'); ?>">
		        <th><?php echo $header['comments']; ?></th>
		        <td>
			        <textarea name="comments" id="comments"></textarea>
		        </td>
	        </tr>													
	        <tr>
		        <th></th>
		        <td>
			        <input type="button" name="save" id="save" value=" Save " class="isubmit" />
			        <input type="button" name="cancel" id="cancel" value=" Cancel " />
			        <input type="hidden" name="kpiid" id="kpiid" value="<?php echo $_GET['kpiid']; ?>" />
			        <input type="hidden" name="evaluationyear" id="evaluationyear" value="<?php echo $evaluationyear; ?>" />
			        <input type="hidden" name="parent" id="parent" value="<?php echo $_GET['parent']; ?>" />
		        </td>
	        </tr>																													
        </table>    
    </td>
 </tr>
</table>
</form>
