<?php 
if(isset($component) && !empty($component))
{
$allow = array();
$key   = "";
if( $componentid == "1")
{
   $key   = "organisational";
   $allow = array();
} else if( $componentid == 2) {
   $key   = "managerial";
   $allow = array();
} else if( $componentid == 3) {
   $key   = "individual";
   $allow = array("owner", "proof_of_evidence", "baseline", "targetunit", "performance_standard", "target_period", "weighting");		
}
    ?>
    <?php JSdisplayResultObj(""); ?>
    <form name="addkpaform" id="addkpaform">
    <table class="noborder" width="100%">
     <tr>
        <td class="noborder">
          <table width="100%">
            <?php
             if($section != "admin")
             {
            ?>
            <tr>
                <th>Full Name</th>
                <td><span style="background-color:#EEE;"><?php echo $userdata['tkname']." ".$userdata['tksurname']; ?></td>       
            </tr>	
            <?php 
            }
            ?>
            <tr>
                <th>Evaluation Year</th>
                <td>
                  <span style="background-color:#EEE;" id="evalyeartext"><?php echo (!empty($year) ? $year['start']." to ".$year['end'] : ""); ?></span>
                </td>	        		    
            </tr>
            <tr>
                <th>Component Name</th>
                <td><span style="background-color:#EEE;"><?php echo (!empty($component) ? $component['name'] : ""); ?></td>
            </tr>
            <?php
             if($section == "admin")
             {
            ?>			    	    
            <tr>
                <td>
               <?php displayGoBack("main.php?controller=component&action=".$key."&componentid=".$component['id']."&evaluationyear=".$evaluationyear."&parent=5&section=".$section."&folder=".$section."", ""); ?>	            
                </td>
                <td>&nbsp;</td>	        		    
            </tr>
            <?php 
            } else {
            ?>
            <tr>
                <td><?php displayGoBack("", ""); ?></td>
                <td>&nbsp;</td>	        		    
            </tr>        
            <?php 
            }
            ?>
          </table>    
        </td>
        <td class="noborder">
            <table>
                 <tr>
	                 <th><?php echo $header['name']; ?></th>
	                 <td>
		                 <textarea name="name" id="name"></textarea>
	                 </td>
                 </tr>
                 <tr>
	                 <th><?php echo $header['objective']; ?></th>
	                 <td>
		                 <textarea name="objective" id="objective"></textarea>
	                 </td>
                 </tr>	
                 <tr>
	                 <th><?php echo $header['outcome']; ?></th>
	                 <td>
		                 <textarea name="outcome" id="outcome"></textarea>
	                 </td>
                 </tr>		
                 <tr>
	                 <th><?php echo $header['measurement']; ?></th>
	                 <td>
		                 <textarea name="measurement" id="measurement"></textarea>
	                 </td>
                 </tr>			
                 <?php 
                  if($section !== "admin")
                  {
                 ?>
                 <!--
                 <tr style="display:<?php echo (in_array('owner', $allow) ? 'none' : 'table-row'); ?>">
	                 <th><?php echo $header['owner']; ?></th>
	                 <td>
		                 <select name="owner" id="owner">
		                   <option value="">--owner--</option>
		                 </select>
	                 </td>
                 </tr>
                 --> 
                 <?php 
                 }
                 ?>
                 <tr style="display:<?php echo (in_array('proof_of_evidence', $allow) ? 'none' : 'table-row'); ?>">
	                 <th ><?php echo $header['proof_of_evidence']; ?></th>
	                 <td>
		                 <textarea name="proof_of_evidence" id="proof_of_evidence"></textarea>
	                 </td>
                 </tr>	
                 <tr style="display:<?php echo (in_array('baseline', $allow) ? 'none' : 'table-row'); ?>">
	                 <th><?php echo $header['baseline']; ?></th>
	                 <td>
		                 <textarea name="baseline" id="baseline"></textarea>
	                 </td>
                 </tr>	
                 <tr style="display:<?php echo (in_array('targetunit', $allow) ? 'none' : 'table-row'); ?>">
	                 <th><?php echo $header['targetunit']; ?></th>
	                 <td>
		                 <textarea name="targetunit" id="targetunit"></textarea>
	                 </td>
                 </tr>			
                   <tr style="display:<?php echo (in_array('nationalkpa', $allow) ? 'none' : 'table-row'); ?>">
                       <th><?php echo $header['nationalkpa']; ?></th>
                       <td>
                            <select name="nationalkpa" id="nationalkpa">
                              <option>--National KPAs--</option>
                            </select>
                       </td>
                   </tr>							
                   <tr style="display:<?php echo (in_array('idpobjectives', $allow) ? 'none' : 'table-row'); ?>">
                       <th><?php echo $header['idpobjectives']; ?></th>
                       <td>
                            <select name="idpobjectives" id="idpobjectives">
                              <option>--Company Objectives--</option>
                            </select>
                       </td>
                   </tr>		        												
                 <tr style="display:<?php echo (in_array('performance_standard', $allow) ? 'none' : 'table-row'); ?>">
	                 <th><?php echo $header['performance_standard']; ?></th>
	                 <td>
		                 <textarea name="performance_standard" id="performance_standard"></textarea>
	                 </td>
                 </tr>									
                 <tr style="display:<?php echo (in_array('target_period', $allow) ? 'none' : 'table-row'); ?>">
	                 <th><?php echo $header['target_period']; ?></th>
	                 <td>
		                 <textarea name="target_period" id="target_period"></textarea>
	                 </td>
                 </tr>										
                 <tr style="display:<?php echo (in_array('weighting', $allow) ? 'none' : 'table-row'); ?>">
	                 <th><?php echo $header['weighting']; ?> of KPA</th>
	                 <td>
		                 <select name="weighting" id="weighting">
		                 </select>
		                 against the component
	                 </td>
                 </tr>			
                 <tr style="display:<?php echo (in_array('deadline', $allow) ? 'none' : 'table-row'); ?>">
	                 <th><?php echo $header['deadline']; ?></th>
	                 <td>
	                   <input type="text" name="deadline" id="deadline" class="datepicker" value="" readonly="readonly" />
	                 </td>
                 </tr>										
                 <tr style="display:<?php echo (in_array('comments', $allow) ? 'none' : 'table-row'); ?>">
	                 <th><?php echo $header['comments']; ?></th>
	                 <td>
		                 <textarea name="comments" id="comments"></textarea>
	                 </td>
                 </tr>													
                 <tr>
	                 <th></th>
	                 <td>
		                 <input type="button" name="save" id="save" value=" Save " class="isubmit" />
		                 <input type="button" name="cancel" id="cancel" value=" Cancel " />
		                 <input type="hidden" name="goaltype" id="goaltype" value="<?php echo $componentid; ?>" />
		                 <?php 
		                  if($section != "admin")
		                  {
		                 ?>
		                    <input type="hidden" name="owner" id="owner" value="<?php echo $userdata['tkid']; ?>" />
		                 <?php
		                  }
		                 ?>
		                 <input type="hidden" name="kpastatus" id="kpastatus" value="" />
		                 <input type="hidden" name="section" id="section" value="<?php echo $section; ?>" />
		                 <input type="hidden" name="parent" id="parent" value="<?php echo $_GET['parent']; ?>" />
		                 <input type="hidden" name="evaluationyear" id="evaluationyear" value="<?php echo $evaluationyear; ?>" />
	                 </td>
                 </tr>	     
            </table>
        </td>																												
    </table>
    </form>
<?php
} else {
    echo "The component data could not be found <br /><br />";
    displayGoBack("", "");
} 
?>
