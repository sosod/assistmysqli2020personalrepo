<?php
$allow = array();
$key   = "";
if($type == "1")
{
   $key   = "organisational";
   $allow = array();
} else if($type == 2) {
   $key   = "managerial";
   $allow = array();
} else if($type == 3) {
   $key   = "individual";
   $allow = array("owner", "nationalkpa", "idpobjectives");	
}
?>
<?php JSdisplayResultObj(""); ?>
<form name="addkpiform" id="addkpiform">
<table class="noborder" width="100%">
    <td class="noborder">
      <table width="100%">
        <?php
         if($section != "admin")
         {
        ?>
        <tr>
            <th>Full Name</th>
            <td><span style="background-color:#EEE;"><?php echo $userdata['tkname']." ".$userdata['tksurname']; ?></td>       
        </tr>	
        <?php 
        }
        ?>	
        <tr>
            <th><?php echo $header['evaluation_year']; ?></th>
            <td><span style="background-color:#EEE;" id="evalyeartext"><?php echo $year['start']." to ".$year['end']; ?></span></td>	        
        </tr> 
        <tr>
            <th>Component Name</th>
            <td><span style="background-color:#EEE;"><?php echo $component['name']; ?></td>
        </tr>	
        <tr>
            <th>KPA Ref</th>
            <td><span style="background-color:#EEE;"><?php echo $_GET['kpaid']; ?></td>       
        </tr>	        		    	        
        <tr>
            <th>KPA Name</th>
            <td><?php echo $kpa['name']; ?></td>	            
        </tr>
        <tr>
            <th><?php echo $header['objective']; ?></th>
            <td><?php echo $kpa['objective']; ?></td>	            
        </tr>
        <tr>
            <th><?php echo $header['outcome']; ?></th>
            <td><?php echo $kpa['outcome']; ?></td>	            
        </tr>
        <tr>
            <th><?php echo $header['measurement']; ?></th>
            <td><?php echo $kpa['measurement']; ?></td>	            
        </tr>
        <tr>
            <th><?php echo $header['proof_of_evidence']; ?></th>
            <td><?php echo $kpa['proof_of_evidence']; ?></td>	            
        </tr>
        <tr>
            <th><?php echo $header['deadline']; ?></th>
            <td><?php echo $kpa['deadline']; ?></td>	            
        </tr>      
        <?php
         if($section == "admin")
         {
        ?>			    	    
        <tr>
            <td>
           <?php displayGoBack("main.php?controller=component&action=".$key."&componentid=".$component['id']."&evaluationyear=".$evaluationyear."&parent=5&section=".$section."&folder=".$section."", ""); ?>	            
            </td>
            <td>&nbsp;</td>	        		    
        </tr>
        <?php 
        } else {
        ?>
        <tr>
            <td><?php displayGoBack("", ""); ?></td>
            <td>&nbsp;</td>	        		    
        </tr>        
        <?php 
        }
        ?>                    
      </table>
    </td>
    <td class="noborder">
      <table width="70%"> 
	    <?php
	     if($type == 3 && $section !== "admin")
	     {
	    ?>
	    <tr>
		    <th>Does Actions For This KPI Come From This Module</th>
		    <td>
			    <select name="kpiactions" id="kpiactions">
			      <option value="Y">Yes</option>
			      <option value="N">No</option>
			    </select>
			    <span id="maplist"></span>
		    </td>
	    </tr>      
	    <?php } ?>
	    <tr>
		    <th><?php echo $header['name']; ?></th>
		    <td>
			    <textarea name="name" id="name"></textarea>
		    </td>
	    </tr>
	    <tr>
		    <th><?php echo $header['objective']; ?></th>
		    <td>
			    <textarea name="objective" id="objective"></textarea>
		    </td>
	    </tr>	
	    <tr>
		    <th><?php echo $header['outcome']; ?></th>
		    <td>
			    <textarea name="outcome" id="outcome"></textarea>
		    </td>
	    </tr>		
	    <tr>
		    <th><?php echo $header['measurement']; ?></th>
		    <td>
			    <textarea name="measurement" id="measurement"></textarea>
		    </td>
	    </tr>			
	    <!-- <tr style="display:<?php echo (in_array('owner', $allow) ? 'none' : 'table-row'); ?>">
		    <th><?php echo $header['owner']; ?></th>
		    <td>
			    <select name="owner" id="owner">
			      <option value="">--owner--</option>
			    </select>
		    </td>
	    </tr> -->
	    <tr style="display:<?php echo (in_array('proof_of_evidence', $allow) ? 'none' : 'table-row'); ?>">
		    <th><?php echo $header['proof_of_evidence']; ?></th>
		    <td>
			    <textarea name="proof_of_evidence" id="proof_of_evidence"></textarea>
		    </td>
	    </tr>	
	    <tr style="display:<?php echo (in_array('baseline', $allow) ? 'none' : 'table-row'); ?>">
		    <th><?php echo $header['baseline']; ?></th>
		    <td>
			    <textarea name="baseline" id="baseline"></textarea>
		    </td>
	    </tr>	
	    <tr style="display:<?php echo (in_array('targetunit', $allow) ? 'none' : 'table-row'); ?>">
		    <th><?php echo $header['targetunit']; ?></th>
		    <td>
			    <textarea name="targetunit" id="targetunit"></textarea>
		    </td>
	    </tr>		
	    <tr style="display:<?php echo (in_array('nationalkpa', $allow) ? 'none' : 'table-row'); ?>">
		    <th><?php echo $header['nationalkpa']; ?></th>
		    <td>
			    <select name="nationalkpa" id="nationalkpa">
			      <option>--National KPAs--</option>
			    </select>
		    </td>
	    </tr>							
	    <tr style="display:<?php echo (in_array('idpobjectives', $allow) ? 'none' : 'table-row'); ?>">
		    <th><?php echo $header['idpobjectives']; ?></th>
		    <td>
			    <select name="idpobjectives" id="idpobjectives">
			      <option>--Company Objectives--</option>
			    </select>
		    </td>
	    </tr>														
	    <tr style="display:<?php echo (in_array('performance_standard', $allow) ? 'none' : 'table-row'); ?>">
		    <th><?php echo $header['performance_standard']; ?></th>
		    <td>
			    <textarea name="performance_standard" id="performance_standard"></textarea>
		    </td>
	    </tr>									
	    <tr style="display:<?php echo (in_array('target_period', $allow) ? 'none' : 'table-row'); ?>">
		    <th><?php echo $header['target_period']; ?></th>
		    <td>
			    <textarea name="target_period" id="target_period"></textarea>
		    </td>
	    </tr>										
	    <tr style="display:<?php echo (in_array('weighting', $allow) ? 'none' : 'table-row'); ?>">
		    <th><?php echo $header['weighting']; ?> of KPI</th>
		    <td>
			    <select name="weighting" id="weighting">
				    <option value="">--weighting--</option>
			    </select> against the KPA
		    </td>
	    </tr>														
	    <tr style="display:<?php echo (in_array('deadline', $allow) ? 'none' : 'table-row'); ?>">
		    <th><?php echo $header['deadline']; ?></th>
		    <td>
		      <input type="text" name="deadline" id="deadline" class="datepicker" value="" readonly="readonly" />
		    </td>
	    </tr>
	    <tr style="display:<?php echo (in_array('comments', $allow) ? 'none' : 'table-row'); ?>">
		    <th><?php echo $header['comments']; ?></th>
		    <td>
			    <textarea name="comments" id="comments"></textarea>
		    </td>
	    </tr>													
	    <tr>
		 <th></th>
		 <td>
		    <input type="button" name="save" id="save" value=" Save " class="isubmit" />
		    <input type="button" name="cancel" id="cancel" value=" Cancel " />
		    <input type="hidden" name="kpaid" id="kpaid" value="<?php echo $_GET['kpaid']; ?>" />
             <?php 
              if($section != "admin")
              {
             ?>
                <input type="hidden" name="owner" id="owner" value="<?php echo $userdata['tkid']; ?>" />
             <?php
              }
             ?>		    
		    <input type="hidden" name="evaluationyear" id="evaluationyear" value="<?php echo $evaluationyear; ?>" />
		    <input type="hidden" name="parent" id="parent" value="<?php echo $_GET['parent']; ?>" />
		 </td>
	    </tr>  
      </table>
    </td>																												
</table>
</form>
