<?php
//debug($vars);
if(isset($vars) && !empty($vars))
{
     $users  = User::getSubOrdinatesList();
     ?>
     <table width="100%">
          <tr>
               <th>Full Name</th>
               <td>
                    <select name="selectuser" id="selectuser">
                    <option>--please select--</option>
                    <?php
                        if(isset($users) && !empty($users))
                        {    
                          foreach($users as $uIndex => $uVal)
                          {
                              echo "<option value='".$uIndex."' ".($uIndex == $vars['tkid'] ? "selected='selected'" : "").">".$uVal."</option>";
                          }
                        }
                    ?>               
                    </select>
               </td>
               <th>Manager</th>
               <td><span style="background-color:#EEE;"><b><em><?php  echo $vars['tkmanager'];  ?></em></b></span></td>	
               <th>Evaluation Year</th>
               <td><span style="background-color:#EEE;" id="evalyeartext"><b>From : </b><em><?php echo $vars["start"]; ?></em> <b> to </b><em><?php echo $vars["end"]; ?></em></b></span>
               </td>	                  
          </tr>	
          <tr>
               <th>Job Level</th>
               <td><span style="background-color:#EEE;"><?php echo $vars["orgposition"].' - '.$vars["joblevel"].' ('.$vars["joblevelid"].")"; ?></span>
               </td>
               <th>No of Evaluations outstanding</th>
               <td></td>	        
          <th>Perfomance Category</th>
          <td><span style="background-color:#EEE;"><?php echo $vars["performancecategory"]; ?></span></td>          
          </tr>			         	    
     </table>
<?php
} else {
?>
    <p class="ui-state ui-state-error" style="padding:10px;">
      <span class="ui-icon ui-icon-info" style="float:left; margin-right:10px;"></span>
      <span>
           Please make sure the following configuration have been configured correctly
           <ul class="ui-state ui-state-error" style="padding-top:5px;">
              <li>Please setup your profile and configure the default evaluation year</li>
              <li>Please configure and map job levels to the perfomance categories</li>
           </ul>          
      </span>
    </p>
 <?php
}
?>
