<?php JSdisplayResultObj(""); ?>
<script type="text/javascript">
$(function(){
	$(".generate").click(function(e){
	    var action = $(this).attr("id");
         document.location.href = "main.php?controller=report&action="+action+"&parent=3&folder=report";;
	   e.preventDefault();
	});
});
</script> 
<table class="noborder">
  <tr>
   <td colspan="2" class="noborder">
	<table>
       <tr>
		 <th>Ref</th>
	 	 <th>Name</th>
		 <th>Description</th>
		 <th>Report Format</th>
		 <th></th>
	   </tr>
	   <tr>
		 <td>1</td>
		 <td>Perfomance Matrix Creation Status Report</td>
		 <td>Overview Perfomance Matrix Creation process</td>
		 <td>Table</td>
		 <td><input type="button" name="creation_status" id="creation_status" value="Generate" class="generate" /></td>
	   </tr>				
	   <tr>
		 <td>2</td>
		 <td>Evaluation Status Report  per Manager</td>
		 <td>Evaluation Status Report  per Manager</td>
		 <td>Table</td>
		 <td><input type="button" name="evaluation_status" id="evaluation_status" value="Generate" class="generate" /></td>
	   </tr>
	   <tr>
		 <td>3</td>
		 <td>Evaluation Status Report per Department</td>
		 <td>Evaluation Status Report per Department</td>
		 <td>Table</td>
		 <td><input type="button" name="department_evaluation_status" id="department_evaluation_status" value="Generate" class="generate" /></td>
	   </tr>	   
       <tr>
		 <td>4</td>
		 <td>Evaluation Score Report</td>
		 <td>Evaluation Score Report</td>
		 <td>Table</td>
		 <td><input type="button" name="evaluation_score" id="evaluation_score" value="Generate" class="generate" /></td>
       </tr>
       <tr>
		 <td>5</td>
		 <td>Performance Matrix</td>
		 <td>Confirmed Performance Matrix per User</td>
		 <td>Table</td>
		 <td><input type="button" name="performance_matrix" id="performance_matrix" value="Generate" class="generate" /></td>
       </tr>
       <tr>
		 <td>6</td>
		 <td>Evaluation Matrix</td>
		 <td>Evaluation Matrix</td>
		 <td>Table</td>
		 <td><input type="button" name="evaluation_matrix" id="evaluation_matrix" value="Generate" class="generate" /></td>
       </tr>
       <tr>
		 <td>7</td>
		 <td>Recommendation Matrix</td>
		 <td>Recommendation Matrix</td>
		 <td>Table</td>
		 <td>
		  <input type="button" name="recommendation_matrix" id="recommendation_matrix" value="Generate" class="generate" />
		 </td>
       </tr>        
       <tr>
		 <td>8</td>
		 <td>Evaluation Review</td>
		 <td>View all the User Evaluations</td>
		 <td>Table</td>
		 <td><input type="button" name="fish_bowl" id="fish_bowl" value="Generate" class="generate" /></td>
       </tr>        									
       <tr>
		 <td>9</td>
		 <td>Bell Curve</td>
		 <td>View the Bell Curve graphs for departments and job levels</td>
		 <td>Graph</td>
		 <td><input type="button" name="bell_curve" id="bell_curve" value="Generate" class="generate" /></td>
       </tr>
	</table>
    </td>
  </tr>
<tr>
	<td class="noborder"><?php displayGoBack("", ""); ?></td>
	<td class="noborder"></td>
</tr>
</table>
