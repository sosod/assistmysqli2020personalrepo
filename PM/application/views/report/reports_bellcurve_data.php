<?
  include("inc_ignite.php");

  $selectedYear = EvalYear::load( $_REQUEST["eval_year_id"] );
  $selectedCategory = Category::load( $_REQUEST["category_id"] );  
  $selectedPeriod = $_REQUEST["period"];
  $departmentId = $_REQUEST["departmentId"];
  $jobLevel = $_REQUEST["jobLevel"];  
  
  function getCntEvaluations( $selectedCategoryId, $selectedYearId, $selectedPeriod, $departmentId, $jobLevel, $rating )
  {
    $sql = " select count(es.id) AS cnt from assist_".getCompanyCode(). "_" . EvaluationStatus::$TABLE_NAME . " es, " .
           " assist_".getCompanyCode()."_" . UserSetup::$TABLE_NAME . " us, " .
           " assist_".getCompanyCode()."_" . User::$TABLE_NAME . " u " .
           " WHERE es.eval_year_id = ". $selectedYearId .
           " AND es.period = ". $selectedPeriod .
           " AND us.category_id = ". $selectedCategoryId .           
           " AND es.user_id = u.tkid " .
           " AND es.user_id = us.user_id " .
           " AND es.fishbowl_approved is not null ";
    
    if( exists($departmentId) )
    {
      $sql .= " AND u.tkdept = " . $departmentId;
    }
           
    if( exists($jobLevel) )
    {
      $sql .= " AND us.job_level = " . $jobLevel;
    }
    
    if( exists($rating) )
    {
      $sql .= " AND es.fishbowl_rating = " . $rating;
    }
           
    $record = getRecord( getResultSet( $sql ) );
    
    return $record["cnt"];
  }
  
  $cntEvaluations = getCntEvaluations( $selectedCategory->getId(), $selectedYear->getId(), $selectedPeriod, $departmentId, $jobLevel, null );
  
  $cnt1 = getCntEvaluations( $selectedCategory->getId(), $selectedYear->getId(), $selectedPeriod, $departmentId, $jobLevel, 1 );
  $cnt2 = getCntEvaluations( $selectedCategory->getId(), $selectedYear->getId(), $selectedPeriod, $departmentId, $jobLevel, 2 );
  $cnt3 = getCntEvaluations( $selectedCategory->getId(), $selectedYear->getId(), $selectedPeriod, $departmentId, $jobLevel, 3 );
  $cnt4 = getCntEvaluations( $selectedCategory->getId(), $selectedYear->getId(), $selectedPeriod, $departmentId, $jobLevel, 4 );
  $cnt5 = getCntEvaluations( $selectedCategory->getId(), $selectedYear->getId(), $selectedPeriod, $departmentId, $jobLevel, 5 );
  $cnt6 = getCntEvaluations( $selectedCategory->getId(), $selectedYear->getId(), $selectedPeriod, $departmentId, $jobLevel, 6 );
  $cnt7 = getCntEvaluations( $selectedCategory->getId(), $selectedYear->getId(), $selectedPeriod, $departmentId, $jobLevel, 7 );    
  
  $percentage1 = $cntEvaluations != 0 ? $cnt1 / $cntEvaluations * 100 : 0; 
  $percentage2 = $cntEvaluations != 0 ? $cnt2 / $cntEvaluations * 100 : 0;
  $percentage3 = $cntEvaluations != 0 ? $cnt3 / $cntEvaluations * 100 : 0;
  $percentage4 = $cntEvaluations != 0 ? $cnt4 / $cntEvaluations * 100 : 0;
  $percentage5 = $cntEvaluations != 0 ? $cnt5 / $cntEvaluations * 100 : 0;
  $percentage6 = $cntEvaluations != 0 ? $cnt6 / $cntEvaluations * 100 : 0;
  $percentage7 = $cntEvaluations != 0 ? $cnt7 / $cntEvaluations * 100 : 0;
  
  $xml = '<?xml version="1.0" encoding="UTF-8"?>
          <chart>
          	<series>
          		<value xid="1">1</value>
          		<value xid="2">2</value>
          		<value xid="3">3</value>
          		<value xid="4">4</value>
          		<value xid="5">5</value>
          		<value xid="6">6</value>
          		<value xid="7">7</value>
          	</series>
          	<graphs>
          		<graph gid="1">
          			<value xid="1">5</value>
          			<value xid="2">10</value>
          			<value xid="3">30</value>
          			<value xid="4">50</value>
          			<value xid="5">30</value>
          			<value xid="6">10</value>
          			<value xid="7">5</value>                                
          		</graph>
          		<graph gid="2">';
  $xml .= '			<value xid="1">' . $percentage1 . '</value>';
  $xml .= '			<value xid="2">' . $percentage2 . '</value>';
  $xml .= '			<value xid="3">' . $percentage3 . '</value>';
  $xml .= '			<value xid="4">' . $percentage4 . '</value>';
  $xml .= '			<value xid="5">' . $percentage5 . '</value>';
  $xml .= '			<value xid="6">' . $percentage6 . '</value>';    
  $xml .= '			<value xid="7">' . $percentage7 . '</value>';  
  $xml .= '   </graph>
          	</graphs>
          </chart>';
  echo $xml;        
?>