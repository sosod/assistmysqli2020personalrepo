<script type="text/javascript" src="/library/jquery-ui-1.9.1/js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="/library/jquery-ui-1.9.1/js/jquery-ui-1.9.1.custom.js"></script>
<script type="text/javascript" src="public/js/default.js"></script>
<script type="text/javascript" src="public/js/default.js"></script>
<script type="text/javascript" src="application/views/report/amline/swfobject.js"></script>
<script type="text/javascript" src="public/js/jquery.report.bellcurve.js"></script>
<link rel="stylesheet" href="/assist.css" type="text/css">
<?php
$period     = 0;
$year       = 0;
$dir        = 0;
$eval_level = 0;
$job_level  = 0;
if(isset($_GET['evaluationperiod']) && !empty($_GET['evaluationperiod']))
{
   $period = $_GET['evaluationperiod'];
}
if(isset($_GET['evaluationyear']) && !empty($_GET['evaluationyear']))
{
   $year = $_GET['evaluationyear'];
}
if(isset($_GET['directorate']) && !empty($_GET['directorate']))
{
   $dir = $_GET['directorate'];
}
if(isset($_GET['joblevel']) && !empty($_GET['joblevel']))
{
   $job_level = $_GET['joblevel'];
}
if(isset($_GET['evaluationlevel']) && !empty($_GET['evaluationlevel']))
{
   $eval_level = $_GET['evaluationlevel'];
}
$options['evaluationyear']    = $year;
$options['evaluationperiod']  = $period;
$options['directorate']       = $dir;
$options['job_level']        = $job_level;
$performancematrixObj         = new Performancematrix();
$report                       = $performancematrixObj -> matrix_report($options);
$eval_values                  = array(); 

$year_str = array();        
$_year    = EvaluationYear::findById($year);        
$year_str = $_year['start']." - ".$_year['end']; 

if(!empty($report))
{
   foreach($report['usermatrix'] as $index => $r_data)
   {
     $eval_values[$index]['self']    = (float)$r_data['self'];
     $eval_values[$index]['manager'] = (float)$r_data['manager'];
     $eval_values[$index]['joint']   = (float)$r_data['joint'];
     $eval_values[$index]['review']  = (float)$r_data['review'];
   }
}
$ratingObj      = new RatingScale();
$_rating        = array(array('id' => -1, 'ratingfrom' => 0, 'ratingto' => 0, 'description' => '', 'definition' => ''));
$ratings        = array_merge($_rating, $ratingObj -> getRatingScales());
$_ratings       = array();
$from           = 0;
foreach($ratings as $_index => $_val)
{
   $_id        = ($_index+1);
   $to         = ($_val['ratingto'] + 0.4);
   $_ratings[] = array('from' => $from, 'to' => $to, 'description' => $_val['description'], 'ratingfrom' => $_val['ratingfrom'], 'ratingto' => $_val['ratingto'], 'id' => $_id);
   $from       = $to +  0.1;
}
$ratings_data   = array();
$ratings_info   = array();
$from           = 0; 
foreach($_ratings as $index => $rating)
{
    foreach($eval_values as $index => $val)
    {
        if($val['self'] >= $rating['from'] && $val['self'] < $rating['to'])
        {
            $ratings_data[$rating['id']]['self'][] = 1; 
        }   
         
        if($val['manager'] >= $rating['from'] && $val['manager'] < $rating['to'])
        {
            $ratings_data[$rating['id']]['manager'][] = 1; 
        }      

        if($val['joint'] >= $rating['from'] && $val['joint'] < $rating['to'])
        {
            $ratings_data[$rating['id']]['joint'][] = 1; 
        }                     
         
        if($val['review'] >= $rating['from'] && $val['review'] < $rating['to'])
        {
            $ratings_data[$rating['id']]['review'][] = 1; 
        }
    }   
}
foreach($_ratings as $rating_index => $rate)
{
    
   if(isset($ratings_data[$rate['id']]))
   {
     if(isset($ratings_data[$rate['id']]['self']))
     {
        $rating_info[$rate['id']]['self'] = count($ratings_data[$rate['id']]['self']);
     } else {
        $rating_info[$rate['id']]['self'] = 0;
     }
     if(isset($ratings_data[$rate['id']]['manager']))
     {
        $rating_info[$rate['id']]['manager'] = count($ratings_data[$rate['id']]['manager']);
     } else {
        $rating_info[$rate['id']]['manager'] = 0;
     }
     if(isset($ratings_data[$rate['id']]['joint']))
     {
        $rating_info[$rate['id']]['joint'] = count($ratings_data[$rate['id']]['joint']);
     } else {
        $rating_info[$rate['id']]['joint'] = 0;
     }
     if(isset($ratings_data[$rate['id']]['review']))
     {
        $rating_info[$rate['id']]['review'] = count($ratings_data[$rate['id']]['review']);
     }  else {
       $rating_info[$rate['id']]['review'] = 0;
     }
   } else {
      $rating_info[$rate['id']]['self']    = 0;
      $rating_info[$rate['id']]['manager'] = 0;
      $rating_info[$rate['id']]['joint']   = 0;
      $rating_info[$rate['id']]['review']  = 0;
   }
}
?>
<script type="text/javascript">
$(function(){
     $("#bellcurve").bellcurve({evaluationperiod:<?php echo $period; ?>, evaluationyear:<?php echo $year; ?>, directorate:<?php echo $dir; ?>, joblevel:<?php echo $job_level; ?>, evaluationlevel:<?php echo $eval_level; ?>});
})
</script>
<div id="bellcurve"></div>
<?php 
$evaluationlevels = array(1 => 'self', 2 => 'manager', 3 => 'joint', 4 => 'review');
if(isset($_GET['evaluationlevel']))
{
  if(!empty($_GET['evaluationlevel']))
  {
    if(isset($evaluationlevels[$_GET['evaluationlevel']]))
    {
       $evaluationlevels = array(1 => $evaluationlevels[$_GET['evaluationlevel']]);
    }
  }
}
$total = count($evaluationlevels);
if(isset($_GET['evaluationperiod']) && !empty($_GET['evaluationperiod']))
{
    for($i = 1; $i <= $total; $i++)
    { 
      echo "<h2>".ucwords($evaluationlevels[$i])." Evaluation for ".$year_str." as at ".date("d-M-Y")."</b><br /><br />";
    ?>
    <script type="text/javascript">
    $(function(){
        var date = new Date(); 
        time = date.getMinutes()+""+date.getSeconds();
          
	    var so = new SWFObject("application/views/report/amline/amline.swf", "amline", "520", "400", "8", "#FFFFFF");
	    so.addVariable("path", "application/views/report/amline/");
	    so.addVariable("settings_file", encodeURIComponent("application/views/report/bell_curve_settings.xml?"+time)); // you can set two or more different settings files here (separated by commas)
	    so.addVariable("data_file", encodeURIComponent("main.php?controller=performancematrix&action=get_bell_curve_data&evaluationperiod=<?php echo $period; ?>&evaluationyear=<?php echo $year; ?>&directorate=<?php echo $dir; ?>&joblevel=<?php echo $job_level; ?>&evaluationlevel=<?php echo $evaluationlevels[$i]; ?>"));
        so.addVariable("loading_settings", "LOADING SETTINGS");
        so.addVariable("loading_data", "LOADING DATA");
        so.addVariable("preloader_color", "#666666");
	    so.write("tablebellcurve<?php echo ($i+1); ?>");
    })
    </script>
    <div id="tablebellcurve<?php echo ($i+1); ?>"></div>
    <?php 
    }
}
?>
