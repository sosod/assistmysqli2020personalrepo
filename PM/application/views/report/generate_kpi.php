<?php
$displayReport = false;
$saveReport    = false; 
$error = "";
//debug($header);
if(isset($_POST) && !empty($_POST))
{
   if(empty($_POST['value']['evaluationyear']))
   {
     $error = "Please select the report evaluation year";
   } else {
      if(isset($_POST['generate']))
      {
        if(empty($_POST['report_title'])) 
        {
          $error = "Please enter the report title";
        } else {      
          $displayReport = true;
        }
      } else if ($_POST['save']) {
        if(empty($_POST['report_name'])) 
        {
          $error = "Please enter the report name";
        } else {      
          $saveReport = true;
        }          
      }  
   }
} 
if($displayReport)
{
  //include_once("../loader.php];
  //@session_start();
  //spl_autoload_register("Loader::autoload];	
  $reportObj = new kpi();
  $reportObj -> generateReport($_POST, new Report());
} else {
 // $scripts = array("report.js];
  //include("../header.php];
  //$naming = new kpiNaming();
  //$headers = $naming -> getNaming();
  if(!empty($error))
  {
?>   
     <p class="ui-state-error" style="margin:0 0 8px 0; padding:5px; width:1000px;" id="message">
        <span class="ui-icon ui-icon-closethick" style="float:left;"></span>
        <span><?php echo $error; ?></span>
     </p>  
<?php  
 } else if($saveReport) {
    $reportObj = new Report();
    $reportObj -> saveQuickReport($_POST, "kpi");
 }
?>
<form method="post">
<table class="noborder"> 
	<tr>
		<th colspan="3">1.Select the fields to be displayed on the report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
	  <td><input type="checkbox" name="kpi[name]" id="kpi_name" class="kpi" /> <?php echo $header['name']; ?></td>
	  <td><input type="checkbox" name="kpi[objective]" id="kpi_objective" class="kpi" /><?php echo $header['objective']; ?></td>
	  <td><input type="checkbox" name="kpi[outcome]" id="kpi_outcome" class="kpi" /><?php echo $header['outcome']; ?></td>
	</tr>		
	<tr>
	  <td><input type="checkbox" name="kpi[measurement]" id="kpi_measurement" class="kpi" /><?php echo $header['measurement']; ?></td>
	  <td><input type="checkbox" name="kpi[proof_of_evidence]" id="kpi_proof_of_evidence" class="kpi" /><?php echo $header['proof_of_evidence']; ?></td>
	  <td><input type="checkbox" name="kpi[baseline]" id="kpi_baseline" class="kpi" /><?php echo $header['baseline']; ?></td>
	</tr>			
	<tr>	     
      <td><input type="checkbox" name="kpi[idpobjectives]" id="kpi_idpobjectives" class="kpi" /><?php echo $header['idpobjectives']; ?></td>
	 <td><input type="checkbox" name="kpi[performance_standard]" id="performance_standard" class="kpi" /><?php echo $header['performance_standard']; ?></td>
		<td><input type="checkbox" name="kpi[competency]" id="kpi_competency" class="kpi" /><?php echo $header['competency']; ?></td>
	</tr>				
	<tr>	     
	  <td><input type="checkbox" name="kpi[status]" id="kpi_status" class="kpi" /><?php echo $header['status']; ?></td>
	  <td><input type="checkbox" name="kpi[comments]" id="kpi_comments" class="kpi" /><?php echo $header['comments']; ?></td>
	  <td><input type="checkbox" name="kpi[target_period]" id="target_period" class="kpi" /><?php echo $header['target_period']; ?></td>		
	</tr>
	<tr>	     
	  <td><input type="checkbox" name="kpi[weighting]" id="kpi_weighting" class="kpi" /><?php echo $header['weighting']; ?></td>
	  <td><input type="checkbox" name="kpi[deadline]" id="kpi_deadline" class="kpi" /><?php echo $header['deadline']; ?></td>
	  <td><input type="checkbox" name="kpi[owner]" id="kpi_owner" class="kpi" /><?php echo $header['owner']; ?></td>		
	</tr>
	<tr>
	  <td><input type="checkbox" name="kpi[targetunit]" id="kpi_targetunit" class="kpi" /><?php echo $header['targetunit']; ?></td>
	  <td><input type="checkbox" name="kpi[nationalkpa]" id="kpi_nationalkpa" class="kpi" /><?php echo $header['nationalkpa']; ?></td>
	  <td><input type="checkbox" name="kpi[progress]" id="kpi_progress" class="kpi" /><?php echo $header['progress']; ?></td>		
	</tr>						
	<tr>
	  <td></td>
	  <td><input type="button" name="check_kpi" id="check_kpi" value=" Check All" /></td>
	  <td><input type="button" name="invert_kpi" id="invert_kpi" value=" Invert" /></td>
	</tr>
	<tr>
	  <td colspan="3" class="noborder"></td>
	</tr>																												
	<tr>
	   <td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
	   <th colspan="3">2. Select the filters you wish to apply</th>
	</tr>
	<tr>
	  <td colspan="3" class="noborder"></td>
	</tr>
	<tr>
    	<th><?php echo $header['evaluation_year']; ?>:</th>    
    	<td>
        <select name="value[evaluationyear]" id="evaluation_year" class="match">
		<option value="1">--please select--</option>
    	   </select>
    	</td>
    	<td>
    	</td>
    </tr>
	<tr>
    	<th><?php echo $header['name']; ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" id="name" name="value[name]"></textarea>
    	</td>
    	<td>
    		<select name="match[name]" id="match_name" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select>
    	</td>
    </tr>
	<tr>
    	<th><?php echo $header["objective"]; ?>:</th>    
    	<td><textarea rows="5" cols="30" id="objective" name="value[objective]"></textarea></td>
    	<td>
    		<select name="match[objective]" id="match_objective" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select>  	
    	</td>
    </tr>
	<tr>
    	<th><?php echo $header["outcome"]; ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" name="value[outcome]" id="outcome"></textarea>
        </td>
        <td>
			<select name="match[outcome]" id="match_outcome" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select> 		        
        </td>  
    </tr>  
	<tr>
    	<th><?php echo $header["measurement"]; ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" name="value[measurement]" id="measurement"></textarea>
        </td>
        <td>
			<select name="match[measurement]" id="match_measurement" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select> 		        
        </td>  
    </tr>  
	<tr>
    	<th><?php echo $header["proof_of_evidence"]; ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" name="value[proof_of_evidence]" id="proof_of_evidence"></textarea>
        </td>
        <td>
			<select name="match[proof_of_evidence]" id="match_proof_of_evidence" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select> 		        
        </td>  
    </tr>  
	<tr>
    	<th><?php echo $header["baseline"]; ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" name="value[baseline]" id="baseline"></textarea>
        </td>
        <td>
			<select name="match[baseline]" id="baseline" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select> 		        
        </td>  
    </tr>   
	<tr>
    	<th><?php echo $header["targetunit"]; ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" name="value[targetunit]" id="targetunit"></textarea>
        </td>
        <td>
			<select name="match[targetunit]" id="targetunit" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select> 		        
        </td>  
    </tr>   
	<tr>
    	<th><?php echo $header["nationalkpa"]; ?>:</th>    
    	<td>
        	<select name="value[nationalkpa][]" id="nationalkpa" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr> 
	<tr>
    	<th><?php echo $header["idpobjectives"]; ?>:</th>    
    	<td>
        	<select name="value[idpobjectives][]" id="idpobjectives" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr>     
	<tr>
    	<th><?php echo $header["performance_standard"]; ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" name="value[performance_standard]" id="performance_standard"></textarea>
        </td>
        <td>
			<select name="match[performance_standard]" id="performance_standard" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select> 		        
        </td>  
    </tr>   
	<!-- 
	<tr>
    	<th><?php echo $header["competency"]; ?>:</th>    
    	<td>
        	<select name="value[competency][]" id="competency" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr> 
	<tr>
    	<th><?php echo $header["proficiency"]; ?>:</th>    
    	<td>
        	<select name="value[proficiency][]" id="proficiency" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr>   
    -->     
	<tr>
    	<th><?php echo $header["comments"]; ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" name="value[comments]" id="comments"></textarea>
        </td>
        <td>
			<select name="match[comments]" id="comments" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select> 		        
        </td>  
    </tr>  
	<tr>
    	<th><?php echo $header["target_period"]; ?>:</th>    
    	<td>
    		<textarea rows="5" cols="30" name="value[target_period]" id="target_period"></textarea>
        </td>
        <td>
			<select name="match[target_period]" id="target_period" class="match">
    			<option value="any">Match any word</option>
    			<option value="all">Match all words</option>
    			<option value="excact">Match excact phrase</option>
    		</select> 		        
        </td>  
    </tr>                                         
    <tr>
    	<th><?php echo $header["weighting"]; ?>:</th>    
    	<td>
        	<select name="value[weighting][]" id="weighting" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr> 
	<tr>
    	<th><?php echo $header["deadline"]; ?>:</th>    
    	<td> 
          From : <input type="text" name="value[kpi_deadline_date][from]" id="from_deadline_date" class="datepicker" value="" readonly="readonly"/>
        </td>
       <td> To	: <input type="text" name="value[kpi_deadline_date][to]" id="to_deadline_date" class="datepicker" value="" readonly="readonly"/>
       </td>
    </tr>       
	<tr>
    	<th><?php echo $header["owner"]; ?>:</th>    
    	<td>
        	<select name="value[owner][]" id="owner" multiple="multiple">
            </select>        
        </td>
        <td></td>
    </tr>    
 
  
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>    
    <tr>
    	<th colspan="3">3. Choose the group and sort options </th>
 	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr> 	
  <tr>
    <th>Group By:</th>
    <td>
    	<select name="group_by" id="group">
    		<option value="no_grouping">No Grouping</option>
    		<option value="nationalkpa_"><?php echo $header['nationalkpa']; ?></option>
    		<option value="idpobjectives_"><?php echo $header['idpobjectives']; ?></option>
    		<!--
    		<option value="competency_"><?php echo $header['competency']; ?></option>
    		<option value="proficiency_"><?php echo $header['proficiency']; ?></option>
    		-->
    		<option value="weighting_"><?php echo $header['weighting']; ?></option>
    	</select>
    </td>
     <td></td>
  </tr>
  <tr>
  	<th>Sort By:</th>
  	<td>
		<ul id="sortable" style="list-style:none;">
			<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort"  id="__name"></span><input type=hidden name=sort[] value="__name"><?php echo $header["name"]; ?></li>
			<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__objective"></span><input type=hidden name=sort[] value="__objective"><?php echo $header["objective"]; ?></li>
			<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__outcome"></span><input type=hidden name=sort[] value="__outcome"><?php echo $header["outcome"]; ?></li>
			<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__measurement"></span><input type=hidden name=sort[] value="__measurement"><?php echo $header["measurement"]; ?></li>
			<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__proof_of_evidence"></span><input type=hidden name=sort[] value="__proof_of_evidence"><?php echo $header["proof_of_evidence"]; ?></li>
			<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__baseline"></span><input type=hidden name=sort[] value="__baseline"><?php echo $header["baseline"]; ?></li>
            <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__targetunit"></span><input type=hidden name=sort[] value="__targetunit"><?php echo $header['targetunit']; ?></li>
			<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__nationalkpa"></span><input type=hidden name=sort[] value="__nationalkpa"><?php echo $header['nationalkpa']; ?></li>
			<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__idpobjectives"></span><input type=hidden name=sort[] value="__idpobjectives"><?php echo $header['idpobjectives']; ?></li>
			<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__performance_standard"></span><input type=hidden name=sort[] value="__performance_standard"><?php echo $header['performance_standard']; ?></li>
		<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__comments"></span><input type=hidden name=sort[] value="__comments"><?php echo $header['comments']; ?></li>
		<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__target_period"></span><input type=hidden name=sort[] value="__target_period"><?php echo $header['target_period']; ?></li>
		<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__weighting"></span><input type=hidden name=sort[] value="__weighting"><?php echo $header['weighting']; ?></li>
		<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__deadline"></span><input type=hidden name=sort[] value="__deadline"><?php echo $header['deadline']; ?></li>	
<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__owner"></span><input type=hidden name=sort[] value="__owner"><?php echo $header['owner']; ?></li>
		<!-- <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s sort" id="__progress"></span><input type=hidden name=sort[] value="__progress"><?php echo $header['progress']; ?></li> -->
  	</td>
    <td></td>
  </tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>  
	<tr>
	    <th colspan="3">4. Choose the document format of your report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	  
 <tr>
    <td colspan="3"><input type="radio" name="document_format" id="on_screen" value="on_screen" checked="checked" />On Screen</td>
 </tr>
 <!--<tr>
    <td colspan="3"><input type="radio" name="document_format" id="microsoft_excell" value="microsoft_excell" />Microsoft Excell ( <em> plain </em> )</td>
 </tr>
 <tr>
    <td colspan="3"><input type="radio" name="document_format" id="microsoft_excell" value="microsoft_excell" />Microsoft Excell ( <em>formated</em> )</td>
  </tr>
  <tr>
  	<td colspan="3"><input type="radio" name="document_format" id="save_pdf" value="save_pdf" />Save to Pdf</td>
  </tr> -->
 	<tr>
		<td colspan="3" class="noborder"></td>
	</tr> 
	<tr>
		<th colspan="3">5. Generate Report</th>
	</tr>
	<tr>
		<td colspan="3" class="noborder"></td>
	</tr>	
	<tr>
		<th>Report Title</th>
		<td colspan="2"><input type="text" name="report_title" id="report_title" value="" /></td>
	</tr>
	<tr>
		<th></th>
		<td colspan="2"><input type="submit" name="generate" id="generate" value="Generate Report" /></td>
	</tr>
	<tr>
		<th>Report Name:</th>
		<td colspan="2"><input type="text" name="report_name" id="report_name" value="" /></td>
	</tr>	
	<tr>
		<th>Description</th>
		<td colspan="2"><textarea name="report_description" id="report_description" cols="30" rows="7"></textarea></td>
	</tr>
	<tr>
		<th></th>
		<td colspan="2"><input type="submit" name="save" id="save" value="Save Report" /></td>
	</tr>		
<tr>
 <td style="font-size:8pt;border:1px solid #AAAAAA;" colspan="3">* Please note the following with regards to the formatted Microsoft Excel report:<br />
		<ol>
			<li>Formatting is only available when opening the document in Microsoft Excel.  If you open this document in OpenOffice, it will lose all formatting and open as plain text.</li>
			<li>When opening this document in Microsoft Excel <u>2007</u>, you might receive the following warning message: <br />
			<span style="font-style:italic;">"The file you are trying to open is in a different format than specified by the file extension.
			Verify that the file is not corrupted and is from a trusted source before opening the file. Do you want to open the file now?"</span><br />
			This warning is generated by Excel as it picks up that the file has been created by software other than Microsoft Excel.
			It is safe to click on the "Yes" button to open the document.</li>
		</ol>
	</td>
</tr>
</table>
</form>
<?php 
}
?>
