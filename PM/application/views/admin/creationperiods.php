<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder gray" style="text-align:center;">
			<select id="evaluationyear" name="evaluationyear">
				<option value="">--select evaluation year--</option>
			</select>
		</td>
	</tr>
	<tr>
	   <td colspan="2" class="noborder">
			<table id="table_creationperiod">
			 <tr>
			   <th>Ref</th>
			   <th>Start Start</th>
			   <th>End Date</th>
			   <th></th>
			 </tr>		 
			 <tr>
			 	<td colspan="4" class="creationperiod">
			 		Select the evaluation year to view the creation periods
			 	</td>
			 </tr>
			</table>  
	   </td>
	</tr>
	<tr>
	  <td class="noborder"><?php displayGoBack("",""); ?></td>
	  <td class="noborder">
          <input type="hidden" name="evaluationyearid" id="evaluationyearid" value="" />
          <input type="hidden" name="section" id="section" value="<?php echo $section; ?>" /> 	  
	     <?php displayAuditLogLink("creationperiods_logs", true)?>
	  </td>
	</tr>
</table>
