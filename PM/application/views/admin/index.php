<?php
//include_once("../header.php");
?>
<table id="admintable">
	<tr>
		<th>Evaluation Years</th>
		<td>Open and Close Evaluation Years</td>
		<td>
			<input type="button" name="evaluationyears" id="evaluationyears" value="Configure" />
		</td>
	</tr>
	<tr>
		<th>User Evaluation Years</th>
		<td>Define Default Evaluation Year for a user </td>
		<td>
			<input type="button" name="user" id="define_evaluationyear" value="Configure" />
		</td>
	</tr>	
	<tr>
		<th>Creation Periods</th>
		<td>Define Creation Periods</td>
		<td>
			<input type="button" name="creationperiod" id="creationperiod" value="Configure" />
		</td>
	</tr>	
	<tr>
		<th>Evaluation Periods</th>
		<td>Open and Close Evaluation Periods</td>
		<td>
			<input type="button" name="evaluationperiod" id="evaluationperiod" value="Configure" />
		</td>
	</tr>		
	<tr>
		<th>Employee Creation</th>
		<td>Define Employee Specific Creation Periods</td>
		<td>
		  <input type="button" name="creationperiod" id="employeespecific" value="Configure" />
		</td>
	</tr>
	<tr>
		<th>Employee Evaluation</th>
		<td>Define Employee Specific Evaluation Periods</td>
		<td>
		  <input type="button" name="evaluationperiod" id="employeespecific" value="Configure" />
		</td>
	</tr>						
	<tr>
		<th>Organisational Goals</th>
		<td>Define Master Organisational KPA's and assign to PM Categories</td>
		<td>
			<input type="button" name="component" id="organisational" value="Configure" />
		</td>
	</tr>																	
	<tr>
		<th>Managerial KPAs</th>
		<td>Define Master Managerial KPAs and assign to PM Categories</td>
		<td>
			<input type="button" name="component" id="managerial" value="Configure" />
		</td>
	</tr>
	<tr>
		<th>Master Individual KPAs</th>
		<td>Define Master Individual KPAs and assign to PM Categories</td>
		<td>
			<input type="button" name="component" id="individual" value="Configure" />
		</td>
	</tr>																		
	<!-- <tr>
		<th>SBDIP</th>
		<td>Import SBDIP</td>
		<td>
			<input type="button" name="templateperformance" id="templateperformance" value="Configure" />
		</td>
	</tr> -->
     <tr>
	     <th>Copy Evaluation Years</th>
	     <td>Copy Evaluation Years</td>
	     <td>
	       <input type="button" name="component" id="copy_evaluationyear" value="Configure" />
	       <input type="hidden" name="parentid" id="parentid" value="<?php echo $_GET['parent']; ?>" />
	     </td>
	</tr>	
	<tr>
		<th>Notifications</th>
		<td>Define Notifications</td>
		<td>
			<input type="button" name="notifications" id="notifications" value="Configure" />
		</td>
	</tr>																													
</table>
