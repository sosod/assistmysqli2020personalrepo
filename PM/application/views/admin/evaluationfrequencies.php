<?php
$section = (isset($_SESSION['breadcrumb']) ? key($_SESSION['breadcrumb']) : "");
?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder gray" style="text-align:center;">
			<select id="evaluationyear" name="evaluationyear">
				<option>--select evaluation year--</option>
			</select>
		</td>
	</tr>
	<tr>
	   <td colspan="2" class="noborder">
			<table id="table_evaluationfreq">
			 <tr>
			  <th>Ref</th>
			   <th>Frequency Name</th>
			   <th>Number of evaluation periods</th>
			   <th>Status</th>
			   <th></th>
			 </tr>
			 <tr>
			 	<td colspan="5" class="freq">
			 		Select the evaluation year to view the evaluation frequencies
			 	</td>
			 </tr>
			</table>  
	   </td>
	</tr>
	<tr>
	  <td class="noborder">
	  	<?php displayGoBack("",""); ?>
        <input type="hidden" name="section" id="section" value="admin" />
	  </td>
	  <td class="noborder"><?php displayAuditLogLink("evaluationcommittee_logs", true)?></td>
	</tr>
</table>
