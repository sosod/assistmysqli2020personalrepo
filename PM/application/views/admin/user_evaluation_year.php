<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td colspan="2" class="noborder gray" style="text-align:center;">
			<select id="adminuser" name="adminuser">
				<option>--users--</option>
			</select>
		</td>
	</tr>
	<tr>
	   <td colspan="2" class="noborder">
			<table id="table_user_default_year">
			 <tr class="user_default">
			 	<td colspan="5" class="freq">
			 		Select the user to configure the default evaluation year
			 	</td>
			 </tr>
			</table>  
	   </td>
	</tr>
	<tr>
	  <td class="noborder">
	  	<?php displayGoBack("",""); ?>
        <input type="hidden" name="section" id="section" value="admin" /> 
         <input type="hidden" name="user_id" id="user_id" value="<?php echo $_SESSION['tid']; ?>" />
	  </td>
	  <td class="noborder"><?php displayAuditLogLink("evaluationcommittee_logs", true)?></td>
	</tr>
</table>
