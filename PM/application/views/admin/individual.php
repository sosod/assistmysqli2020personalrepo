<?php JSdisplayResultObj(""); ?>
<table width="100%" class="noborder" >
	<tr>
		<td style="text-align:center;" width="50%" class="noborder" colspan="2">
				<select id="evaluationyear" name="evaluationyear" class="gray">
					<option value="">--select evaluation year--</option>	
				</select>
				<input type="hidden" name="evaluationyearid" id="evaluationyearid" value="<?php echo (isset($_GET['evaluationyear']) ? $_GET['evaluationyear'] : ""); ?>" />
		</td>		
	</tr>
	<tr id="detailed" style="text-align:center; display:none;" width="50%">
		<td colspan="2" class="noborder">
		  <table width="100%">
		    <tr>
		        <th>Component Name</th>
		        <td><span style="background-color:#EEE;">Individual</td>
		        <th>Evaluation Year</th>
		        <!-- <td><span style="background-color:#EEE;" id="evalyeartext"></span></td>	-->
                  <td style="text-align:center;" width="50%" class="noborder">
                     <span id="evaluationYear" class="evaluationyear"></span>
                  </td>				        
		    </tr>		    
		  </table>
		</td>		
	</tr>
	<tr>
	   <td class="noborder" colspan="2"  width="100%">
			<table id="table_nationalkpa" width="100%" class="noborder">
			  <tr>
			   <td class="noborder">
			     <div id="goalsdiv"></div>
			   </td>
			  </tr>
			</table>
	   </td>
	</tr>
	<tr>
	  <td class="noborder"><?php displayGoBack("",""); ?></td>
	  <td class="noborder">
	     <input type="hidden" name="page" id="page" value="admin" />
	     <input type="hidden" name="section" id="section" value="admin" />
	     <input type="hidden" name="parent" id="parent" value="<?php echo $_GET['parent']; ?>" />
	  <?php //displayAuditLogLink("nationalkpa_logs", true)?></td>
	</tr>
</table>
