<?php
$section = (isset($_SESSION['breadcrumb']) ? key($_SESSION['breadcrumb']) : ""); 
?>
<?php JSdisplayResultObj(""); ?>
<form id="notification_form" name="notification_form">
<table id="admintable">
	<tr>
		<th>No</th>
		<th>Notification Name</th>
		<th>Report Type</th>
		<th>Use</th>
		<th>Type</th>
	</tr>
	<tr>
		<td>1</td>
		<td>Notification: Opening of the Performance Matrix creation period</td>
		<td>All Users</td>
		<td>
		  <select id="all_user_pm_creation_period" name="all_user_pm_creation_period[allow]">
		    <option value='1' >Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
		    <input type="radio" name="all_user_pm_creation_period[notification]" value='1'>Email
		    <input type="radio" name="all_user_pm_creation_period[notification]" value='2'>SMS
		    <input type="radio" name="all_user_pm_creation_period[notification]" value='3'>Both
		   <!--
		  <select id="use_creation" name="use_creation">
		    <option value='1'>Email</option>
		    <option value='2'>SMS</option>
		    <option value='3'>Both</option>
		  </select>		
		  -->
		</td>
	</tr>	
	<tr>
		<td>2</td>
		<td>Reminder e-mail: Creation period will close in 7 days</td>
		<td>All Users</td>
		<td>
		  <select id="all_user_creation_period_in_7" name="all_user_creation_period_in_7[allow]">
		    <option value='1'>Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
		    <input type="radio" name="all_user_creation_period_in_7[notification]" value='1'>Email
		    <input type="radio" name="all_user_creation_period_in_7[notification]" value='2'>SMS
		    <input type="radio" name="all_user_creation_period_in_7[notification]" value='3'>Both
		  <!--
		    <select id="use_creation" name="use_creation">
		    <option value='1'>Email</option>
		    <option value='2'>SMS</option>
		    <option value='3'>Both</option>
		   -->
		  </select>		
		</td>
	</tr>
	<tr>
		<td>3</td>
		<td>Reminder e-mail: Creation period will close in 1 day</td>
		<td>All Users</td>
		<td>
		  <select id="all_user_creation_period_in_1" name="all_user_creation_period_in_1[allow]">
		    <option value='1'>Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
		    <input type="radio" name="all_user_creation_period_in_1[notification]" value='1'>Email
		    <input type="radio" name="all_user_creation_period_in_1[notification]" value='2'>SMS
		    <input type="radio" name="all_user_creation_period_in_1[notification]" value='3'>Both		    		
		  <!--
		  <select id="use_creation" name="use_creation">
		    <option value='1'>Email</option>
		    <option value='2'>SMS</option>
		    <option value='3'>Both</option>
		  </select>		
		  -->
		</td>
	</tr>
	<tr>
		<td>4</td>
		<td>Notification: Opening of an Evaluation period</td>
		<td>All Users</td>
		<td>
		  <select id="all_user_eval_period_open" name="all_user_eval_period_open[allow]">
		    <option value='1'>Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
	      <input type="radio" name="all_user_eval_period_open[notification]" value='1'>Email
	      <input type="radio" name="all_user_eval_period_open[notification]" value='2'>SMS
	      <input type="radio" name="all_user_eval_period_open[notification]" value='3'>Both
		 <!--
		 <select id="use_creation" name="use_creation">
		    <option value='1'>Email</option>
		    <option value='2'>SMS</option>
		    <option value='3'>Both</option>
		  </select>		
		  -->
		</td>
	</tr>
	<tr>
		<td>5</td>
		<td>Reminder e-mail: Evaluation period will close in 7 days</td>
		<td>All Users</td>
		<td>
		  <select id="all_user_evaluation_period_close_in_7" name="all_user_evaluation_period_close_in_7[allow]">
		    <option value='1'>Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
		  <!--
		  <select id="use_creation" name="use_creation">
		    <option value='1'>Email</option>
		    <option value='2'>SMS</option>
		    <option value='3'>Both</option>
		  </select>		
		  -->
            <input type="radio" name="all_user_evaluation_period_close_in_7[notification]" value='1'>Email
            <input type="radio" name="all_user_evaluation_period_close_in_7[notification]" value='2'>SMS
            <input type="radio" name="all_user_evaluation_period_close_in_7[notification]" value='3'>Both
		</td>
	</tr>
	<tr>
		<td>6</td>
		<td>Reminder e-mail: Evaluation period will close in 1 day</td>
		<td>All Users</td>
		<td>
		  <select id="all_user_evaluation_period_close_1" name="all_user_evaluation_period_close_1[allow]">
		    <option value='1'>Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
		  <!--
		  <select id="use_creation" name="use_creation">
		    <option value='1'>Email</option>
		    <option value='2'>SMS</option>
		    <option value='3'>Both</option>
		  </select>		
		  -->
            <input type="radio" name="all_user_evaluation_period_close_1[notification]" value='1'>Email
            <input type="radio" name="all_user_evaluation_period_close_1[notification]" value='2'>SMS
            <input type="radio" name="all_user_evaluation_period_close_1[notification]" value='3'>Both
		</td>
	</tr>
	<tr>
		<td>7</td>
		<td>Notification: Opening of the Performance Matrix creation period</td>
		<td>User Specific</td>
		<td>
		  <select id="pm_matrix_creation_open" name="pm_matrix_creation_open[allow]">
		    <option value='1'>Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
		  <!--
		  <select id="use_creation" name="use_creation">
		    <option value='1'>Email</option>
		    <option value='2'>SMS</option>
		    <option value='3'>Both</option>
		  </select>		
		  -->
            <input type="radio" name="pm_matrix_creation_open[notification]" value='1'>Email
            <input type="radio" name="pm_matrix_creation_open[notification]" value='2'>SMS
            <input type="radio" name="pm_matrix_creation_open[notification]" value='3'>Both
		</td>
	</tr>
	<tr>
		<td>8</td>
		<td>Reminder e-mail: Creation period will close in 7 days</td>
		<td>User Specific</td>
		<td>
		  <select id="creation_period_close_7" name="creation_period_close_7[allow]">
		    <option value='1'>Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
		  <!--
		  <select id="use_creation" name="use_creation">
		    <option value='1'>Email</option>
		    <option value='2'>SMS</option>
		    <option value='3'>Both</option>
		  </select>		
		  -->		  
            <input type="radio" name="creation_period_close_7[notification]" value='1'>Email
            <input type="radio" name="creation_period_close_7[notification]" value='2'>SMS
            <input type="radio" name="creation_period_close_7[notification]" value='3'>Both
		</td>
	</tr>
	<tr>
		<td>9</td>
		<td>Reminder e-mail: Creation period will close in 1 day</td>
		<td>User Specific</td>
		<td>
		  <select id="creation_period_close_1" name="creation_period_close_1[allow]">
		    <option value='1'>Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
		  <!--
		  <select id="use_creation" name="use_creation">
		    <option value='1'>Email</option>
		    <option value='2'>SMS</option>
		    <option value='3'>Both</option>
		  </select>		
		  -->
            <input type="radio" name="creation_period_close_1[notification]" value='1'>Email
            <input type="radio" name="creation_period_close_1[notification]" value='2'>SMS
            <input type="radio" name="creation_period_close_1[notification]" value='3'>Both
		</td>
	</tr>
	<tr>
		<td>10</td>
		<td>Notification: Opening of an Evaluation period</td>
		<td>User Specific</td>
		<td>
		  <select id="evaluation_period_open" name="evaluation_period_open[allow]">
		    <option value='1'>Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
		  <!--
		  <select id="use_creation" name="use_creation">
		    <option value='1'>Email</option>
		    <option value='2'>SMS</option>
		    <option value='3'>Both</option>
		  </select>		
		  -->
            <input type="radio" name="evaluation_period_open[notification]" value='1'>Email
            <input type="radio" name="evaluation_period_open[notification]" value='2'>SMS
            <input type="radio" name="evaluation_period_open[notification]" value='3'>Both
		</td>
	</tr>
	<tr>
		<td>11</td>
		<td>Reminder e-mail: Evaluation period will close in 7 days</td>
		<td>User Specific</td>
		<td>
		  <select id="evaluation_period_close_7" name="evaluation_period_close_7[allow]">
		    <option value='1'>Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
            <input type="radio" name="evaluation_period_close_7[notification]" value='1'>Email
            <input type="radio" name="evaluation_period_close_7[notification]" value='2'>SMS
            <input type="radio" name="evaluation_period_close_7[notification]" value='3'>Both
		</td>
	</tr>
	<tr>
		<td>12</td>
		<td>Reminder e-mail: Evaluation period will close in 1 day</td>
		<td>All Users</td>
		<td>
		  <select id="evaluation_period_close_1" name="evaluation_period_close_1[allow]">
		    <option value='1'>Yes</option>
		    <option value='0' selected="selected">No</option>
		  </select>
		</td>
		<td>
            <input type="radio" name="evaluation_period_close_1[notification]" value='1'>Email
            <input type="radio" name="evaluation_period_close_1[notification]" value='2'>SMS
            <input type="radio" name="evaluation_period_close_1[notification]" value='3'>Both
		</td>
	</tr>
	<tr>
		<td colspan='5' style="text-align:right;">
  		  <input type="submit" name="save_changes" id="save_changes" value="Save Changes" /> 
  		  <input type="hidde" name="id" id="id" value="<?php echo $user_notification['id']; ?>" /> 
		</td>
	</tr>
	<tr>
		<td colspan='5' style="text-align:right;">
  		<?php displayGoBack("",""); ?>
		</td>
	</tr>																														
</table>
</form>
