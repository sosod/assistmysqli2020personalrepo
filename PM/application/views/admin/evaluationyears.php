<?php
$section = "";
if(isset($_SESSION['breadcrumb']))
{
    $section = key($_SESSION['breadcrumb']);
}
?>
<?php JSdisplayResultObj(""); ?> 
<table class="noborder">
	<tr>
	   <td colspan="2" class="noborder">
				<table id="table_evaluationyears">
					<tr>
					  <th>Ref</th>
					  <th>Last Day of the Evaluation Year</th>
					  <th>Start Date</th>
					  <th>End Date</th>
					  <th>Status</th>
				  	  <th></th>	  	  
					</tr>
				</table>	   
	   </td>
	</tr>
	<tr>
	  <td class="noborder">
	  	<?php displayGoBack("",""); ?>
	  	<input type="hidden" name="section" id="section" value="admin" /> 
  	  </td>
	  <td class="noborder"><?php displayAuditLogLink("evaluationyears_logs", true)?></td>
	</tr>
</table>
