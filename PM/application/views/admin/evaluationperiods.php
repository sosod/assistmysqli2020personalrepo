<?php JSdisplayResultObj(""); ?> 
<table class="noborder">
	<tr>
	   <td colspan="2" class="noborder">
		<table id="table_evaluationyears">
			<tr>
			  <th>Ref</th>
			  <th>Last Day of the Evaluation Year</th>
			  <th>Start Date</th>
			  <th>End Date</th>
		  	  <th></th>	  	  
			</tr>
			<?php
			  foreach($evaluatinyears as $index => $evaluationyear)
			  {
			?>  
			<tr>
			  <td><?php echo $evaluationyear['id']; ?></td>
			  <td><?php echo $evaluationyear['lastday']; ?></td>
			  <td><?php echo $evaluationyear['start']; ?></td>
			  <td><?php echo $evaluationyear['end']; ?></td>	  
		  	  <td><input type="button" name="configure" class="configure" id="<?php echo $evaluationyear['id']; ?>" value="Open and Close Evaluation Periods" /></td>
			</tr>			
			<?php  
			  }
			?>
		</table>	   
	   </td>
	</tr>
	<tr>
	  <td class="noborder">
	  	<?php displayGoBack("",""); ?>
         <input type="hidden" name="section" id="section" value="<?php echo $section; ?>" />
  	  </td>
	  <td class="noborder"><?php displayAuditLogLink("evaluationyears_logs", true)?></td>
	</tr>
</table>
