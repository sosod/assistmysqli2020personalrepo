<?php
//require_once("class/lib/application.php");
//Application::loadFile("/PM/", "inc_ignite.php");
//Application::loadFile("../PM/", "inc_session.php");
//Application::getInstance();
?>
<html>
<head>
<script type="text/javascript" src="/library/jquery-ui-1.9.1/js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="/library/jquery-ui-1.9.1/js/jquery-ui-1.9.1.custom.js"></script>
<script type="text/javascript" src="/library/jquery/js/common.js"></script>
<script type="text/javascript">
$(function() {
	$(".datepicker").datepicker({
		showOn		  : "both",
		buttonImage 	  : "/library/jquery/css/calendar.gif",
		buttonImageOnly  : true,
		changeMonth	  : true,
		changeYear	  : true,
		dateFormat 	  : "dd-M-yy",
		altField		  : "#startDate",
		altFormat		  : 'd_m_yy'
	});	
	
	$(".historydate").datepicker({	
		showOn			: "both",
		buttonImage 	     : "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		: true,
		changeYear		: true,
		dateFormat 		: "dd-M-yy",
		maxDate             : 0
	});	
	
	$(".futuredate").datepicker({	
		showOn			: "both",
		buttonImage 	     : "/library/jquery/css/calendar.gif",
		buttonImageOnly	: true,
		changeMonth		: true,
		changeYear		: true,
		dateFormat 		: "dd-M-yy",
		minDate             : +1
	});	
	
	$("#clear_remind").click(function(e){
		$("#remindon").val("");
		$("#reminder").val("");
		e.preventDefault();
	});		
		
     $("table").find("th").css({"text-align":"left"});
	$("textarea").attr("cols", "50").attr("rows", "5");
})
</script>
<script type="text/javascript" src="public/js/default.js"></script>
<?php 
if( isset($scripts) ){
	foreach($scripts as $index => $script)
	{
	?>
		<script type="text/javascript" src="public/js/<?php echo $script; ?>"></script>
	<?php 	
	}	
}
if( isset($styles))
{
	foreach($styles as $index => $css)
	{
	?>
		<link href="public/css/<?php echo $css; ?>" rel="stylesheet" type="text/css"/>
	<?php 	
	}		
}
?>
<link rel="stylesheet" href="/library/jquery-ui-1.9.1/css/jquery-ui-1.9.1.custom.css" type="text/css" />
<link rel="stylesheet" href="/assist.css" type="text/css">
</head>	
<body>
<?php 
//echo request_parentref()."<br /><br />";
$menuObj  = Menu::instance();
//$menuObj -> menuItems(request_parentref());
//echo "This current url is ".$menuObj -> getParent();
//get_url(request_controller(), request_action(), request_parentref(), "", (isset($pagetitle) ? $pagetitle : ""));
//echo request_folder();
//debug($_REQUEST);
$parent = $menuObj -> getParent(request_parentref());
$menuObj -> displayMenu(request_folder());
//$children = $menuObj -> menus();
echo "<h4>";
if(!empty($parent))
{
    if(strpos($parent['name'], ".") > 0)
    {
       $url = "main.php?controller=".substr($parent['name'], 0, strpos($parent['name'],"."))."&parent=".$parent['id']."&folder=".request_folder();		
    } else {
       $url = "main.php?controller=".$parent['url']."&parent=".$parent['id']."&folder=".request_folder();
    }
  echo "<a href='".$url."'>".$parent['client_terminology']."</a> >> ";
}
echo "<a href='".$menuObj -> getUrl()."'>".(isset($pagetitle) ? $pagetitle : "")."</a>";
echo "</h4>";
//echo "File constant is ".__FILE__." and <br />";
//echo "Directory constant is ".__DIR__." and <br />";
//Application::loadSubMenu();
//$menuObj = new UpperMenu();
//$menuObj -> loadSubMenu(Request::getFolderLocation());
?>
