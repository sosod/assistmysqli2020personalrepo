<?php
class Performancecategories extends Model
{
     protected static $table = "list_categories";

	function __construct()
	{
		parent::__construct();
	}
	
	public static function fetchUserCategory($options = "")
	{
		$res = DBConnect::getInstance()->getRow("SELECT * FROM #_list_categories LC
		                                         INNER JOIN #_category_joblevel CJL ON LC.id = CJL.category_id
		                                         WHERE CJL.joblevel = '".$_SESSION['userjoblevel']['joblevelid']."' $options
		                                       ");
		return $res; 
	}
	
	public static function getPerformanceCategories($options = array())
	{
          $sqlOption    = parent::createANDSqlOptions($options);
          $sqlOption   .= " AND status & 2 <> 2"; 
          $results      = Performancecategories::findAll($sqlOption);
          $used 	    = self::get_used_performance_categories();
          $categoryArr  = array();
          foreach( $results as $index => $category)
          {
	          if( in_array($category['id'], $used))
	          {
		        $category['used'] = true;
	          } else {
		        $category['used'] = false;
	          }
	          $categoryArr[$index] = $category;
          }
          return $categoryArr;
	}

     function get_all_categories($option_sql = "")
     {
         $results = $this -> db -> get("SELECT * FROM #_".static::$table." WHERE status & 2 <> 2 $option_sql");
         return $results;
     }

     function get_used_performance_categories()
     {
     	 $matriObj 	   			= new Performancematrix();
     	 $matrix_used_categories = $matriObj -> get_categories_used();

     	 $joblevelObj 			= new Joblevel();
     	 $joblevels_categories 	= $joblevelObj -> get_categories_used();

     	return array_merge($joblevels_categories, $matrix_used_categories);
     }

}
