<?php
class Competency extends Model
{

	function __construct()
	{
		parent::__construct();
	}
	
	static function fetchAll( $options = "")
	{
		$results = DBConnect::getInstance()->get("SELECT id, value ,status, description FROM assist_".$_SESSION['cc']."_master_competencies 
		                                          WHERE status & 2 <> 2 $options
		                                        ");
		return $results;
	}
	
	function fetch( $id )
	{
		$result = DBConnect::getInstance()->getRow("SELECT id, value, status, description FROM assist_".$_SESSION['cc']."_master_competencies 
		                                            WHERE id = $id
		                                          ");
		return $result;
	}
	
	public static function findList($options="")
	{
	   $competenciesList = array();
	   $competicies = self::fetchAll();
	   foreach($competicies as $cIndex => $cVal)
	   {
	     $competenciesList[$cVal['id']] = $cVal['value'];
	   }
	   return $competenciesList;
	}
	
}
