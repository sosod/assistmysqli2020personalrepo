<?php
class EvaluationCommitteeAuditlog extends AuditLog
{
 
     private $committee_update = 0;
 
     function notify($postData, $where, $tablename)
     {
        $committee  = $this -> db -> getRow("SELECT id, title FROM #_".$tablename." WHERE id = '".$where['id']."' ");
        //debug($_POST);
        $postData  = $_POST;
        //debug($committee);
        //debug($where);
       
        //debug($tablename);

        $changes    = array();
        if($committee['title'] != $postData['data']['title'])
        {
          $changes['evaluation_committee_title'] = array("from" => $committee['title'], "to" => $postData['data']['title']);
        }
        
        $changes['evaluation_committee_users']     = $this -> _get_users_changes($where['id'], $postData['data']['user']);
        //$changes['other_users'] = $this -> _get_other_users_changes($where['id'], $postData['data']['usertext']);
        ///debug($changes);
        if(!empty($changes))
        {
           $changes['user']          = $_SESSION['tkn'];
           $insertdata['changes']    = serializeEncode($changes);
           $insertdata['ref']        = $where['id'];   
           $insertdata['insertuser'] = $_SESSION['tid'];
           $_SESSION['committee_changes'] = $this -> committee_update; 
           $this -> db -> insert($tablename."_logs", $insertdata);
        }
     }

     function _get_users_changes($committee_id, $posted_users)
     {
        $userObj    = new User();
        $user_list  = $userObj -> get_users_list();          
        $evaluation_committee_users = $this -> db -> get("SELECT * FROM #_list_evaluation_committee_users 
                                                          WHERE evaluation_committee_id = '".$committee_id."'
                                                         ");                                                        
        $current_users_committee    = array();        
        if(!empty($evaluation_committee_users))
        {      
           foreach($evaluation_committee_users as $index => $user)
           {
              $current_users_committee[$user['user_id']] = $user['user_id'];
           }
        }                                                       
        $users_changes              = array();                                             
        if(!empty($posted_users))
        {
          foreach($posted_users as $index => $user)
          {
            if(!isset($current_users_committee[$user]))
            {
              $users_changes[]                       = " Added user ".$user_list[$user];
              $insertdata['user_id']                 = $user;
              $insertdata['evaluation_committee_id'] = $committee_id;
              $this -> committee_update             += $this -> db -> insert('list_evaluation_committee_users', $insertdata);
              unset($posted_users[$index]);
            }
          }      
        }
        if(!empty($current_users_committee))
        {
           foreach($current_users_committee as $c_index => $c_val)
           {
             if(!in_array($c_val, $posted_users))
             {
                $users_changes[]           = "Deleted user ".$user_list[$c_val];
                $where_str                 = " user_id = '".$c_val."' AND evaluation_committee_id = '".$committee_id."' "; 
                $this -> committee_update += $this -> db -> delete('list_evaluation_committee_users', $where_str);
             }
           }
        }
        return $users_changes;
     }
     
     function _get_other_users_changes($committee_id, $posted_users)
     {
        $current_other_users = $this -> db -> get("SELECT * FROM #_list_evaluation_committee_other_users 
                                                   WHERE evaluation_committee_id = '".$committee_id."' 
                                                  ");
        $changes = array();
        if(!empty($current_other_users))
        {
           if(count($current_other_users) == count($posted_users))
           {
              foreach($posted_users  as $index => $user)
              {
                 if(isset($current_other_users[$index]))
                 {
                     if($user['firstname'] != $current_other_users[$index]['firstname'])
                     {
                       $changes[]['firstname'] = array('from' => $current_other_users[$index]['firstname'], 'to' => $user['firstname']); 
                     }
                     if($user['lastname'] != $current_other_users[$index]['lastname'])
                     {
                       $changes[]['lastname'] = array('from' => $current_other_users[$index]['lastname'], 'to' => $user['lastname']); 
                     }        
                     if(!empty($changes))
                     {
                        //$updatedata['firstname'] = $user['firstname'];
                        //$updatedata['lastname']  = $user['lastname'];
                        ///$this -> committee_update += $this -> db -> updateData($updatedata, array('id' => $current_other_users[$index]['id']));
                     }             
                 }             
              }           
           } else {
               foreach($posted_users as $index => $val)
               {
                  if(!isset($current_other_users[$index]))
                  {
                    $changes[] = "Added evaluation commitee user ".$posted_users[$index]['firstname']." ".$posted_users[$index]['lastname'];
                    $insertdata['evaluation_committee_id'] = $committee_id;
                    $insertdata['firstname']               = $posted_users[$index]['firstname'];
                    $insertdata['lastname']                = $posted_users[$index]['lastname'];
                    ///$this -> committee_update += $this -> db -> insert("list_evaluation_committee_other_users", $insertdata);                     
                    unset($posted_users[$index]);
                  }
               }
               foreach($current_other_users as $index => $val)
               {
                   if(!isset($posted_users[$index]))
                   {
                     $where_str                 = " id = '".$val['id']."' AND evaluation_committee_id = '".$committee_id."' "; 
                     //$this -> committee_update += $this -> db -> delete("list_evaluation_committee_other_users", $where_str);
                     $changes[] = "Deleted evaluation committee user ".$val['firstname']." ".$val['lastname'];
                   }
               }
           }        
        }
        return $changes;
     }


}
