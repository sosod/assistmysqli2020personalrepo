<?php
class Evaluationcommittee extends  Model
{

     protected static $table = "list_evaluation_committee";

     private $user_ids       = array();

     private $user_texts     = array();

	function __construct()
	{
		parent::__construct();
	}	
	
	function get_all_evaluation_committee($option_sql = "")
	{
	   $results              = $this -> db -> get("SELECT * FROM #_".static::$table." EC WHERE 1 $option_sql");
	   $evaluation_committee = array();
        if(!empty($results))
        {
           foreach($results as $index => $val)
           {
              $evaluation_committee[$val['id']]['id']          = $val['id'];
              $evaluation_committee[$val['id']]['ref']         = $val['id'];
              $evaluation_committee[$val['id']]['title']       = $val['title'];
              $evaluation_committee[$val['id']]['users']       = $this -> _get_evaluation_committee_users($val['id']);
              $evaluation_committee[$val['id']]['other_users'] = $this -> _get_evaluation_committee_other_users($val['id']);
              $evaluation_committee[$val['id']]['user_ids']    = $this -> user_ids;
              $evaluation_committee[$val['id']]['user_texts']  = $this -> user_texts;
              $evaluation_committee[$val['id']]['status']      = $val['status'];
           }
        }
	   return $evaluation_committee; 
	}
	
	function _get_evaluation_committee_users($eval_id)
	{
	   $results   = $this -> db -> get("SELECT CONCAT(TK.tkname,' ', TK.tksurname) AS user, ECU.user_id 
	                                    FROM #_list_evaluation_committee_users ECU
	                                    INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON ECU.user_id = TK.tkid
	                                    WHERE evaluation_committee_id = '".$eval_id."'
	                                   ");
	   $user_list = "";
	   if(!empty($results))
	   {
	     foreach($results as $index => $val)
	     {
	        $user_list .= $val['user']."<br />";
	        $this -> user_ids[$val['user_id']] = $val['user_id'];
	     } 
	   }
	   return $user_list;
	}
	
	function _get_evaluation_committee_other_users($eval_id)
	{	     
	   $results = $this -> db -> get("SELECT * FROM #_list_evaluation_committee_other_users 
	                                  WHERE evaluation_committee_id = '".$eval_id."'
	                                 ");
        $user_list = "";
        if(!empty($results))
        {
          foreach($results as $index => $val)
          {
             $user_list .= $val['firstname']." ".$val['lastname']."<br />";
          }
          $this -> user_texts = $results;
        }	                                 
	   return $user_list;	
	}

	
	function save_evaluation_committee($data)
	{    
	    $users                         = $data['data']['user'];
	    $other_users                   = $data['data']['usertext'];
	    $insertdata['title']           = $data['data']['title'];
	    $insertdata['evaluationyear']  = $data['data']['evaluationyear'];
	    $evaluation_committee_id       = $this -> save($insertdata);
	    if($evaluation_committee_id > 0)
	    {
	       if(!empty($users))
	       {
	          $this -> _save_evaluation_committee_users($evaluation_committee_id, $users);
	       }
	       
	       if(!empty($other_users))
	       {
	          $this -> _save_other_evaluation_committee_users($evaluation_committee_id, $other_users);
	       }
	    }
	   return $evaluation_committee_id;
	}	
		
	function _save_evaluation_committee_users($committee_id, $users)
	{
        foreach($users as $index => $user)
        {
           $insertdata['evaluation_committee_id'] = $committee_id;
           $insertdata['user_id']                 = $user;
           $this -> db -> insert("list_evaluation_committee_users", $insertdata); 
        }
	}

	function _save_other_evaluation_committee_users($committee_id, $users)
	{
        foreach($users as $index => $user)
        {
           if($user['firstname'] != "" || $user['lastname'] != "")
           {
             $insertdata['evaluation_committee_id'] = $committee_id;
             $insertdata['firstname']               = $user['firstname'];
             $insertdata['lastname']                = $user['lastname'];
             $this -> db -> insert("list_evaluation_committee_other_users", $insertdata); 
           }
        }
	}	
	
	function update_committee($data)
	{
	    $committee_id = $data['data']['id']; 
	    $this -> attach(new EvaluationCommitteeAuditlog());
	    $res  = 0;
	    $res  = $this -> update_where(array('title' => $data['data']['title']), array('id' => $committee_id));
	    if(isset($_SESSION['committee_changes']) && !empty($_SESSION['committee_changes']))
	    {
	       $res += $_SESSION['committee_changes'];
	       unset($_SESSION['committee_changes']); 
	    }
	    //$res += $this -> _update_evaluation_committee_users($data['data']['user'], $committee_id);    
	    //$res += $this -> _update_evaluation_committee_other_users($data['data']['usertext'], $committee_id);
         /*
         $userStr = "";
	    foreach($data['data']['user'] as $index => $user)
	    {
	        $userStr .= $user.","; 
	    }
	    $usertext = "";	    
	    if(isset($data['data']['usertext']) && !empty($data['data']['usertext']))
	    {
	        $usertext = base64_encode(serialize($data['data']['usertext']));
	    }
	    $insertdata['user']            = $usertext;
	    $insertdata['user_id']         = rtrim($userStr, ",");
	    $insertdata['title']           = $data['data']['title'];
	    $insertdata['evaluationyear']  = $data['data']['evaluationyear'];
	    $this -> attach(new EvaluationCommitteeAuditlog());
	    $res = $this -> update_where($insertdata, array("id" => $data['data']['id']));
	    */
	    return $res;
	}	
	
     function _update_evaluation_committee_users($users, $committee_id)
     {
        //$this -> db -> delete("list_evaluation_committee_users", "id='".$committee_id."'");
        //$this -> _save_evaluation_committee_users($committer_id, $users);
     }
     
     function _update_evaluation_committee_other_users($usertext, $committee_id)
     {
         //$this -> db -> delete("list_evaluation_committee_other_users", "id='".$committee_id."'");
         //$this -> _save_other_evaluation_committee_users($committee_id, $usertext);
     }
     
     public function is_user_in_evaluation_committee($evaluationyear, $userid = "")
     {
        $user_id = "";
        if(empty($userid))
        {
          $user_id = $_SESSION['tid'];
        }
        $result = $this -> db -> getRow("SELECT EC.user_id FROM #_list_evaluation_committee_users EC 
                                         INNER JOIN #_list_evaluation_committee E ON E.id = EC.evaluation_committee_id
                                         WHERE EC.user_id = '".$user_id."' AND E.evaluationyear = '".$evaluationyear."' 
                                        ");
        if(!empty($result))
        {
          return TRUE;
        }
       return FALSE; 
     } 
    
     
     	
}


