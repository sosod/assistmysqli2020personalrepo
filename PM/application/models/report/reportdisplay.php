<?php
class ReportDisplay
{
     
    public static function displayHtml($data, $reportType, $evaluationYear = "")
    {
       header("Content:text/html; charset=utf8");
       echo "<link rel='stylesheet' href='../../assist.css' type='text/css' />";
       echo "<center>";
       echo "<h1>".$_SESSION['ia_cmp_name']."</h1>";
       echo "<h1>".$data['title']."</h1>";  
       if(!empty($financialYear))
       {
          //$financialyearObj = new EvaluationYear();
          //$finYear = $financialyearObj -> fetch($financialYear);       
          //echo "<p><em><b>".$finYear['value']."</b> (".$finYear['start_date']." to ".$finYear['end_date'].")</em></p>";              
       }
       $cols = count($data['headers']);
       $table = "<table>";
       $table .= "<tr>";   
       foreach($data['headers'] as $key => $head)
       {
          $table .= "<th>".$head."</th>";      
       }
       $table .= "</tr>"; 
       if(!empty($data[$reportType]))
       {
          foreach($data[$reportType] as $id => $dataArr)
          {
             $table .= "<tr>";        
             foreach($dataArr as $key => $val)
             {
               $table .= "<td>".$val."</td>";      
             }
             $table .= "</tr>";       
          }       
       } else {
          $table .= "<tr><td colspan='".$cols."'>There is no reporting data for the selected reporting criteria</td></tr>";                 
       } 
       $table .= "</table>
       <p class=i style=\"font-size: 7pt;\">Report generated on ".date("d M Y H:i:s")."</p>
       ";
       echo $table;
       exit(); 	
    }     
    
    public static function displayCsv($data, $reportType, $financialYear)
    {
       $csvStr = "";       
	  $csvStr = $_SESSION['ia_cmp_name']."\r\n";
	  $csvStr .= $data['title']."\r\n";
       if(!empty($financialYear))
       {
          $financialyearObj = new FinancialYear();
          $finYear = $financialyearObj -> fetch($financialYear);       
          $csvStr .= $finYear['start_date']." to ".$finYear['end_date']." (".$finYear['value'].") \r\n";          
       }	  
	  $csvStr .= "\r\n";
	  foreach($data['headers'] as $field => $head)
	  {
		$csvStr .= $head.",";
	  }
	  foreach($data[$reportType] as $index => $valArr)
	  {
		 $csvStr .= "\r\n";
		 foreach($valArr as $key => $value)
		 { 
		    $value   = str_replace(",", ";", $value);
		    $csvStr .= htmlentities($value).",";
		 }
	   }
	   $csvStr .= "";
	   $filename =  "report_".date("YmdHis").".csv";
	   /*$fp       =  fopen($filename, "w+");
	   fwrite($fp, $csvStr);
	   fclose($fp);
	   */
	   $filename =  "report_".date("YmdHis").".csv";
	   file_put_contents($filename, $csvStr, FILE_APPEND | LOCK_EX);
	   header('Content-Type: application/csv'); 
	   header("Content-length: ".filesize($filename)); 
	   header('Content-Disposition: attachment; filename="'.$filename.'"'); 
	   readfile($filename);
	   //unlink($filename);
	   exit();	       
    }
    
    public static function displayPdf($data, Response $response)
    {
       print "<pre>";
          print_r($data);
       print "</pre>";    
    } 
}
?>
