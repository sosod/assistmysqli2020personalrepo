<?php
class ActionAuditLog extends AuditLog
{
 
     private $currentStatus = "";
     
     function __construct()
     {
        parent::__construct();
     }
     
     function notify($postData, $where, $tablename)
     {
        $action         = Action::findById($where['id']);
        $changes        = array();
        $nationalkpa    = NationalKpa::findAll();
        $idpobjectives  = Nationalobjectives::findAll();
        $weightings     = Weighting::fetchAll();
        $statuses       = ActionStatus::findAll();
        $competencies   = Competency::findList();
        $proficiencies  = Proficiency::findList();
        $result         = array();
        Attachment::processAttachmentChange($action['attachment'], 'action_'.$action['id']);                       
        if(isset($_SESSION['uploads']))
        {
           if(isset($_SESSION['uploads']['actionchanges']))
           {
              $changes['attachments'] = $_SESSION['uploads']['actionchanges'];
             
           }
        }  
        unset($action['attachment']);
        unset($postData['attachment']);  
        foreach($action as $postIndex => $value)
        {
          if($postIndex == "status")
          {
             $toStatus  = "";
             if(isset($postData[$postIndex]))
             {
               $toStatus = $postData[$postIndex];
             }
             $statusChanges = self::_getStatusChanges($value, $toStatus, $statuses);
             if(!empty($statusChanges))
             {
               $changes[$postIndex] = $statusChanges;
             }          
          } else if(isset($postData[$postIndex])) {
            
             if($postIndex == "nationalkpa")
             {
                $nkpaChanges = self::_getNationalKpaChanges($value, $postData[$postIndex], $nationalkpa);
                if(!empty($nkpaChanges))
                {
                   $changes[$postIndex]  = $nkpaChanges;
                }
             } else if($postIndex == "idpobjectives") {
                $idpChanges = self::_getIDPChanges($value, $postData[$postIndex], $idpobjectives);
                if(!empty($idpChanges))
                {
                    $changes[$postIndex] = $idpChanges;
                }   
                   
             } else if($postIndex == "competency") {
               $competencyChanges = self::_getCompetencyChanges($value, $postData[$postIndex], $competencies);
               if(!empty($competencyChanges))
               {
                  $changes[$postIndex] = $competencyChanges;
               }   
             } else if($postIndex == "proficiency") {
               $proficiencyChanges = self::_getProficiencyChanges($value, $postData[$postIndex], $proficiencies);
               if(!empty($proficiencyChanges))
               {
                  $changes[$postIndex] = $proficiencyChanges;
               }   
             } else if($postIndex == "weighting") {
               $weightChanges = self::_getWeightingChanges($value, $postData[$postIndex], $weightings);
               if(!empty($weightChanges))
               {
                  $changes[$postIndex] = $weightChanges;
               }   
             } else if($postIndex == "actionstatus") {
               $statusChanges = self::_getActionStatusChange($value, $postData[$postIndex]);
               if(!empty($statusChanges))
               {
                  //$changes['action_status'] = $statusChanges;
               }   
             } else if($postIndex == "remindon") {
               if($postData[$postIndex] !=  $value)
               {
                 $changes["remind_on"] = array("to" => $postData[$postIndex], "from" => ($value == "" ? date("d-M-Y") : $value));
               }             
             }  else if($postIndex == "actionon") {
               if($postData[$postIndex] !=  $value)
               {
                 $changes["action_on"] = array("to" => $postData[$postIndex], "from" => ($value == "" ? date("d-M-Y") : $value));
               }             
             } else if($postIndex == "progress") {
               if($postData[$postIndex] !=  $value)
               {
                 $changes["progress"] = array("to" => $postData[$postIndex]."%", "from" => $value."%");
               }             
             } else {
               if($postData[$postIndex] !=  $value)
               {
                 $changes[$postIndex] = array("to" => $postData[$postIndex], "from" => $value);
               }
             }
          }
        }
    
        if(isset($_POST['response']) && !empty($_POST['response']))
        {
           $changes['response'] = $_POST['response'];  
        } 
        $res = 0;
        if(!empty($changes))
        { 
             $changes['user']          = $_SESSION['tkn'];
             $insertdata['insertuser'] = $_SESSION['tid'];
             $changes['currentstatus'] = $this-> currentStatus;
             $insertdata['actionid']   = $where['id'];
             $insertdata['changes']    = serializeEncode($changes);
             static::$table = $tablename."_logs";
             if(!empty($insertdata))
             {
               $res = parent::saveData($insertdata);
             }   
        }
        return $res;
     }     
     
	
    function _getActionStatusChange($fromStatus, $toStatus)
    {
       $statusTo = abs($fromStatus - $toStatus);
         //if the status to is less than the status from , then the a status has been removed from the deliverable     
       $changes = array();
       if($toStatus == 2)
       {
         $changes['action_status'] = Action::statusMessage($toStatus, TRUE);
       } else if($statusTo < $fromStatus) {
          $changes['action_status'] = Action::statusMessage($statusTo, FALSE);
       } else { 
         if($statusTo != 0)
         {
          $changes['action_status'] = Action::statusMessage($statusTo, TRUE);
         }	
       }	
       return $changes['action_status'];     
   }	     
     
     private function _getNationalKpaChanges($fromVal, $toVal, $nationalkpa)
     {
        $from = $to = "";
        $changes = array();
        if(isset($nationalkpa[$toVal]))
        {
          $to = $nationalkpa[$toVal]['name'];
        }
        if(isset($nationalkpa[$fromVal]))
        {
          $from = $nationalkpa[$fromVal]['name'];
        }       
                   
        if($from != $to)
        {
          $changes = array("from" => $from, "to" => $to);    
        }     
       return $changes;   
     }
     
     private function _getIDPChanges($fromVal, $toVal, $idpobjectives)
     {
        $from = $to = "";
        $changes = array();
        if(isset($idpobjectives[$toVal]))
        {
          $to = $idpobjectives[$toVal]['name'];
        }
        if(isset($idpobjectives[$fromVal]))
        {
          $from = $idpobjectives[$fromVal]['name'];
        }                  
        if($from != $to)
        {
          $changes = array("from" => $from, "to" => $to);    
        }     
       return $changes;   
     }     
     
     private function _getWeightingChanges($fromVal, $toVal, $wieghtings)
     {
        $from = $to = "";
        $changes = array();
        if(isset($wieghtings[$toVal]))
        {
          $to = $wieghtings[$toVal]['value'];
        }
        if(isset($wieghtings[$fromVal]))
        {
          $from = $wieghtings[$fromVal]['value'];
        }                  
        if($from != $to)
        {
          $changes = array("from" => $from, "to" => $to);    
        }     
       return $changes;   
     }       
     
     private function _getCompetencyChanges($fromVal, $toVal, $competencies)
     {
        $from = $to = "";
        $changes = array();
        if(isset($competencies[$toVal]))
        {
          $to = $competencies[$toVal];
        }
        if(isset($competencies[$fromVal]))
        {
          $from = $competencies[$fromVal];
        }                  
        if($from != $to)
        {
          $changes = array("from" => $from, "to" => $to);    
        }     
       return $changes;   
     }
            
     private function _getProficiencyChanges($fromVal, $toVal, $proficiencies)
     {
        $from = $to = "";
        $changes = array();
        if(isset($proficiencies[$toVal]))
        {
          $to = $proficiencies[$toVal];
        }
        if(isset($proficiencies[$fromVal]))
        {
          $from = $proficiencies[$fromVal];
        }                  
        if($from != $to)
        {
          $changes = array("from" => $from, "to" => $to);    
        }     
       return $changes;   
     }          
     
     function _getStatusChanges($fromVal, $toVal, $statuses)
     {
        $from = $to = "";  
        $changes = array();
        if(isset($statuses[$fromVal]))
        {
          $from = $statuses[$fromVal]['name'];
        }  
        if(isset($statuses[$toVal]))
        {
          $to = $statuses[$toVal]['name'];
        }          
        if(($from != $to) && ($to != ""))
        {
          $this -> currentStatus = $to;
          $changes = array("from" => $from, "to" => $to);          
        } else {
          $this -> currentStatus = $from;
        }
        return $changes;
     }
     
}
?>
