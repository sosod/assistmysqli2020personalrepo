<?php
class Action extends Model
{
	
	protected static $table = "action";
	
	const ACTIVE            =  1;
	
	const DELETED           = 2;
	
	const COPIED            = 4;
	
	const CONFIRMED         = 8;

    const ACTIVATED         = 16;
     
    const AWAITING          = 32;
     
    const APPROVED          = 64;
     
    const IMPORTED          = 128;
    
    /*
    protected $self_evaluation     = 0;
    
    protected $manager_evaluation  = 0;
    
    protected $joint_evaluation    = 0;
    
    protected $evaluation_review   = 0;    
    */ 
	function __construct()
	{
		parent::__construct();
	}
	/*
	public function set_self_evaluation($value)
	{
	   $this -> self_evaluation += $value;
	}
	
	public function set_manager_evaluation($value)
	{
	   $this -> manager_evaluation += $value;
	}	
     
	public function set_joint_evaluation($value)
	{
	   $this -> joint_evaluation += $value;
	}
	
	public function set_evaluation_review($value)
	{
	   $this -> evaluation_review += $value;
	}	     
    */
     function get_kpi_actions($kpiid, $setting, $options, $actionevalautionObj)
     {
        $get_evaluations = false;
        if(isset($options['page']) && $options['page'] == "evaluate")
        {             
          if(isset($setting['evaluate_on']) && $setting['evaluate_on'] == "action")
          {
             if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
             {
                 $get_evaluations = true;
                 $option_sql .= " AND STR_TO_DATE(A.deadline, '%d-%M-%Y') 
                                  BETWEEN '".date("Y-m-d", strtotime($_SESSION['evaluation_period']['start_date']))."' 
                                  AND '".date("Y-m-d", strtotime($_SESSION['evaluation_period']['end_date']))."'
                                ";     
             }
          }          
        }                
        $sql              =  "SELECT A.id, A.actionstatus, A.progress FROM #_".static::$table." A WHERE A.kpiid = '".$kpiid."' 
                              AND A.actionstatus & ".Action::DELETED." <> ".Action::DELETED." $option_sql ";
        $actions          =  $this -> db -> get($sql);
        $results          = array();
        $response['ids']  = array();
        $total_actions    = 0;
        $total_progress   = 0;
        $average_progress = 0;
        if(!empty($actions))
        {
          foreach($actions as $index => $action)
          {
             if($get_evaluations)
             {
                $actionevalautionObj -> get_action_evaluations($action['id'], $setting, $kpiid);             
             }
             $total_actions                 += 1;  
             $total_progress                += $action['progress'];
             $response[$action['id']]        = $action['actionstatus'];
             $response['ids'][$action['id']] = $action['actionstatus'];
          }   
        }
        if($total_actions != 0)
        {
          $average_progress = round(($total_progress/$total_actions), 2);
        }
        //$response['action_evaluations']  = $this -> _get_total_next_evaluations();
        $response['total_progress']   = $total_progress;
        $response['avg_progress']     = $average_progress;
        $response['total_actions']    = $total_actions;        
        return $response;
     }
     
     function get_actions($options, $userdata)
     {
	    $headers = array();
        if(isset($options['headers']) && !empty($options['headers']))
        {
           $headers = $options['headers'];
           unset($options['headers']);
        }	
	    $settingObj       = new Performancematrix();
	    $_setting_options = array('evaluate_on',
	                              'use_component',
	                              'self_evaluation',
	                              'manager_evaluation',
	                              'joint_evaluation',
	                              'evaluation_review'
	                              );
	    $setting           = $settingObj -> get_matrix_setting($userdata['categoryid'],
	                                                           $userdata['default_year'],
	                                                           $options['componentid'], 
	                                                           $_setting_options
	                                                           );     
        if(isset($options['kpistatus']) && (($options['kpistatus'] & 4) == 4))
        {
            $kpiModuleActions = array();
            $kpi              = Kpi::findById($options['kpiid']);
            //echo $options['kpiid']."\r\n\n";
            //debug($kpi);
            $kpimappingObj    = new KpiMapping();
            //$kpi, $modulemapping, $userdata, $setting, $options = array(), $actionevalautionObj = null
            $kpiModuleActions = $kpimappingObj -> getModuleActions($kpi, $kpi['modulemapping'], $userdata, $setting, $options);
            unset($options['kpistatus']);      
            $kpiActions       = array();
            if(isset($kpiModuleActions['actions']) && !empty($kpiModuleActions['actions']))
            {
                $kpiActions = $kpiModuleActions['actions'];
            } else {
               $kpiActions  = array();
            }
            $kpi_settings       = $this -> _get_kpi_settings($options['kpiid']);
            $action_evaluations = $this -> _get_imported_actions_evaluations($options['kpiid']);
            $actions            = ActionSorter::sort_imported_actions($kpiActions, array('headers'     => $headers, 
                                                                                         'section'     => $options['section'],
                                                                                         'evaluations' => $action_evaluations,
                                                                                         'settings'    => $setting, 
                                                                                         'user'        => $userdata 
                                                                                          ) 
                                                                  );                                                             
        } else {    
           $option_sql         = "";     
           $action_evaluations = array();
	       if(isset($options['page']) && $options['page'] == "evaluate")
	       {             
             if(isset($setting['evaluate_on']) && $setting['evaluate_on'] == "action")
             {
               if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
               {
                    $option_sql .= " AND STR_TO_DATE(A.deadline, '%d-%M-%Y') 
                                     BETWEEN '".date("Y-m-d", strtotime($_SESSION['evaluation_period']['start_date']))."' 
                                     AND '".date("Y-m-d", strtotime($_SESSION['evaluation_period']['end_date']))."' 
                                   ";     
                    $action_evaluations = $this -> _get_actions_evaluations($options['kpiid'], $option_sql);
               }
             }          
           }
           //A.owner = '".toUserId($options['user'])."'
            /* $sql = "SELECT AE.*, A.*, PM.evaluate_on, PM.use_component, PM.self_evaluation, PM.manager_evaluation,
	                 PM.joint_evaluation, PM.evaluation_review, LAS.name AS status
	                 FROM #_".static::$table." A INNER JOIN #_kpi KPI ON KPI.id = A.kpiid 
	                 INNER JOIN #_list_actionstatus LAS ON LAS.id = A.status 
	                 INNER JOIN #_kpa KPA ON KPA.id = KPI.kpaid 
                     INNER JOIN #_matrix PM ON PM.component_id = KPA.componentid 
                     LEFT JOIN #_action_evaluations AE ON AE.action_id = A.id
                     WHERE A.kpiid = '".$options['kpiid']."' AND actionstatus & ".Action::DELETED." <>  ".Action::DELETED." 
                     AND PM.category_id = '".$userdata['categoryid']."' $option_sql
                    ";  
           */
           $sql   = "SELECT A.*, PM.evaluate_on, PM.use_component, PM.self_evaluation, PM.manager_evaluation,
	                 PM.joint_evaluation, PM.evaluation_review, LAS.name AS status
	                 FROM #_".static::$table." A INNER JOIN #_kpi KPI ON KPI.id = A.kpiid 
	                 INNER JOIN #_list_actionstatus LAS ON LAS.id = A.status 
	                 INNER JOIN #_kpa KPA ON KPA.id = KPI.kpaid 
                     INNER JOIN #_matrix PM ON PM.component_id = KPA.componentid 
                     WHERE A.kpiid = '".$options['kpiid']."' AND actionstatus & ".Action::DELETED." <>  ".Action::DELETED." 
                     AND PM.category_id = '".$userdata['categoryid']."'
                    ";     
           $results                       = $this -> db -> get($sql);   
           $action_options['section']     = $options['section'];
           $action_options['headers']     = $headers;
           $action_options['componentid'] = $options['componentid'];
           $action_options['user']        = $userdata;
           $action_options['evaluations'] = $action_evaluations;
           $action_options['setting']     = $setting;
           $action_options['kpiid']       = $options['kpiid'];
           $actions = ActionSorter::sort_actions($results, $action_options);   
        }
        return $actions;
     }
     
     function _get_kpi_settings($kpiid)
     {
        $results = $this -> db -> getRow("SELECT PM.evaluate_on, PM.use_component, PM.self_evaluation, PM.manager_evaluation, PM.joint_evaluation, 
                                          PM.evaluation_review FROM #_matrix PM INNER JOIN #_kpa KPA ON KPA.componentid = PM.component_id
                                          INNER JOIN #_kpi K ON K.kpaid = KPA.id WHERE K.id = '".$kpiid."'
                                         ");                               
         return $results;
     }
     
     function _get_imported_actions_evaluations($kpi_id)
     {
       $results     = $this -> db -> get("SELECT * FROM #_action_evaluations WHERE status & 5 = 5 AND kpi_id = '".$kpi_id."' ");
       $evaluations = array(); 
       if(!empty($results))
       {
         foreach($results as $index => $evaluation)
         {
            $evaluations[$evaluation['action_id']] = $evaluation;
         }
       }
       return $evaluations;
     }
     
     function _get_actions_evaluations($kpi_id, $option_sql)
     {
       $results     = $this -> db -> get("SELECT * FROM #_action_evaluations AE INNER JOIN #_action A ON A.id = AE.action_id
                                          WHERE AE.kpi_id = '".$kpi_id."' 
                                          AND AE.evaluation_period_id = '".$_SESSION['evaluation_period']['id']."' 
                                         ");
       $evaluations = array(); 
       if(!empty($results))
       {
         foreach($results as $index => $evaluation)
         {
            $evaluations[$evaluation['action_id']] = $evaluation;
         }
       }
       return $evaluations;
     }
        
     
     public static function copySave($kpiid, $newkpiid, $evaluationyear, $userdata)
     {
        $actions = Action::findAll(" AND kpiid = '".$kpiid."' AND actionstatus & ".Action::DELETED." <>  ".Action::DELETED." ");
        if(!empty($actions))
        {
           $res = 0;
           foreach($actions as $aIndex => $action)
           {
               $actionid             = $action['id'];
               unset($action['id']);
               $action['kpiid']      = $newkpiid;
               $action['actionref']  = $actionid;
               $action['owner']      = $userdata['tkid'];
               $action['created']    = date('Y-m-d H:i:s');
               $res += Action::saveData($action);
           }
        }
     }
     
     public static function copySaveToNewYear($kpiid, $newkpiid, $toYear)
     {
        $actions = Action::findAll(" AND kpiid = '".$kpiid."' AND actionstatus & ".Action::DELETED." <>  ".Action::DELETED." ");
        if(!empty($actions))
        {
           $res = 0;
           foreach($actions as $aIndex => $action)
           {
               $actionid               = $action['id'];
               unset($action['id']);
               $action['kpiid']        = $newkpiid;
               $action['actionref']    = $actionid;
               $action['actionstatus'] = Action::ACTIVE + Action::COPIED;
               $action['created']      = date('Y-m-d H:i:s');
               $res += Action::saveData($action);
           }
        }
     }

     public static function getActions($options, $userdata)
     {
         $section  = "";	
         $compdata = Component::getComponentCategorySettings($options['componentid'], array("action"));
         $headers  = array();		
         if(isset($options['headers']) && !empty($options['headers']))
         {
            $headers  = $options['headers'];
            unset($options['headers']);
         }
          if(isset($options['section']))
          {
            $section = $options['section'];
            unset($options['section']);
          }
         if(isset($options['kpistatus']) && (($options['kpistatus'] & 5) == 5))
         {
            $kpiModuleActions = array();
            $kpi              = Kpi::findById($options['kpiid']);
            $kpiModuleActions = KpiMapping::getModuleActions($kpi, $kpi['modulemapping'], $userdata);
            unset($options['kpistatus']);      
            $kpiActions       = array();
            if(isset($kpiModuleActions['actions']) && !empty($kpiModuleActions['actions']))
            {
                $kpiActions = $kpiModuleActions['actions'];
            } else {
               $kpiActions  = array();
            }
            $response  = Sorter::sortHeaders($kpiActions, array("component" => $compdata,
                                                                "type"      => "action",
                                                                "headers"   => $headers,
                                                                "section"   => $section
                                                               )
                                            );             
         } else { 
             if(isset($options['kpistatus']))
             {
               unset($options['kpistatus']);
             }
             unset($options['componentid']);
             unset($options['user']);		

             $sqlOption  = parent::createANDSqlOptions($options);
             $sqlOption .= " AND actionstatus & ".Action::DELETED." <> ".Action::DELETED."";
             $data       = Action::findAll($sqlOption);
             //debug($data);
             $response   = Sorter::sortHeaders($data, array("component" => $compdata, "type" => "action", "headers" => $headers, "section" => $section));
        }
        return $response;    
     }
     
     function getkpiActions($user, $componentCategories, $options)
     {
	    unset($options['componentid']);
	    unset($options['kpaid']);		
	    $section  = "";
	    if(isset($options['section']))
	    {
		     $section = $options['section'];
		     unset($options['section']);
	    }
	    $headers = array();		
	    if(isset($options['headers']) && !empty($options['headers']))
	    {
	       $headers  = $options['headers'];
	       unset($options['headers']);
	    }
	    
         $sqlOption     = parent::createANDSqlOptions($options);
         $sqlOption    .= " AND actionstatus & ".Action::DELETED." <>  ".Action::DELETED." ";
	    $data          = Action::findAll($sqlOption);
	    $response      = Sorter::sortHeaders($data, array('component'  => $componentCategories,
	                                                      'type'       => 'action',
	                                                      'headers'    => $headers,
	                                                      'section'    => $section)
	                                          );
	    $actionResults = array();
	    foreach($response['data'] as $aIndex => $aVal)
	    {
	      $actionResults[$aIndex] = $aVal['name'];
	    }
	    //return $response['data'];   
	    return $actionResults;
     }
     
     public static function getActionProgressStats($optionSql = "")
     {
        $sql     = "SELECT COUNT(*) AS total, SUM(A.progress) AS totalProgress, AVG(A.progress) AS averageProgress FROM #_action A WHERE 1 $optionSql ";
        $results = Action::queryRow($sql);
        return $results;
     }
     
     public function approveAction($data)
     {
        $action       = $this -> db -> getRow("SELECT * FROM #_action WHERE id = '".$data['id']."' ");
        $actionstatus = $action['actionstatus'];
        if(($action['actionstatus'] & Action::AWAITING) == Action::AWAITING)
        {
          $actionstatus = $actionstatus - Action::AWAITING;
        } 
        $actionstatus = $actionstatus + Action::APPROVED;
        $updatedata   = array("actionstatus" => $actionstatus);
        $res          = Action::updateWhere($updatedata, array('id' => $data['id']), new ActionAuditLog());    
        $response     = array();
        if($res > 0)
        {
          $response = array("text" => "Action approved", "error" => false);
        } else {
          $response = array("text" => "Error approving action, please try again", "error" => true);
        }
        return $response;
     }
    
     public static function declineAction($data)
     {
        $action       = Action::findById($data['id']);
        $actionstatus = $action['actionstatus'];
        if(($action['actionstatus'] & Action::AWAITING) == Action::AWAITING)
        {
          $actionstatus = $actionstatus - Action::AWAITING;
        } 
        $updatedata = array("actionstatus" => $actionstatus, "status" => 2);      
        $res        = Action::updateWhere($updatedata, array('id' => $action['id']), new ActionAuditLog());    
        $response   = array();
        if($res > 0)
        {
          $response = array("text" => "Action declined", "error" => false);
        } else {
          $response = array("text" => "Error declining action, please try again", "error" => true);
        }
        return $response;
     }    
     
     public static function getApprovedActions($options)
     {
        $userObj       = new User();
        $subordinates  = $userObj -> get_user_subordinates_list();
        $owners_sql    = "";
        if(!empty($subordinates))
        {
           if(isset($subordinates[$_SESSION['tid']]))
           {
              unset($subordinates[$_SESSION['tid']]);
           }
           foreach($subordinates as $subordinate_id => $subordinate)
           {
              $owners_sql .= " A.owner = '".$subordinate_id."' OR";
           }
           $owners_sql = " AND (".rtrim($owners_sql, "OR").")";
        }
        $data = Action::query("SELECT A.* FROM #_action A 
                               INNER JOIN #_kpi K ON K.id = A.kpiid 
                               WHERE A.actionstatus & ".Action::DELETED." <> ".Action::DELETED." 
                               AND A.actionstatus & ".Action::APPROVED." = ".Action::APPROVED."  
                               $owners_sql
                              ");  
                                
        $actions = Sorter::sortHeaders($data, array("type" => "action", "headers" => $options['headers'], "section" => "manage"));  
        return $actions;
     }
     
     public static function getAwaitingActions($options)
     {
        $userObj       = new User();
        $subordinates  = $userObj -> get_user_subordinates_list();
        $owners_sql    = "";
        if(!empty($subordinates))
        {
           if(isset($subordinates[$_SESSION['tid']]))
           {
              unset($subordinates[$_SESSION['tid']]);
           }            
           foreach($subordinates as $subordinate_id => $subordinate)
           {
              $owners_sql .= " A.owner = '".$subordinate_id."' OR";
           }
           $owners_sql = " AND (".rtrim($owners_sql, "OR").")";
        }
        $data = Action::query("SELECT A.* FROM #_action A 
                               INNER JOIN #_kpi K ON K.id = A.kpiid 
                               WHERE A.actionstatus & ".Action::DELETED." <> ".Action::DELETED." 
                               AND A.actionstatus & ".Action::AWAITING." = ".Action::AWAITING."  
                               AND A.progress = '100' AND A.status = '3' 
                               $owners_sql
                              ");  
        $actions = Sorter::sortHeaders($data, array("type" => "action", "headers" => $options['headers'], "section" => "manage"));        
        return $actions;
     }     
     
	function generateReport($postData, Report $reportObj)
	{
	   $joinWhere = $this -> createJoins($postData['value']);
	   $whereStr  = "";
	   if(isset($postData['value']['evaluationyear']) && !empty($postData['value']['evaluationyear']))
	   {
	      $whereStr = " AND KPA.evaluationyear = '".$postData['value']['evaluationyear']."' ";
	      unset($postData['value']['evaluationyear']);
	   }
	   $fields    = $reportObj -> prepareFields($postData['paction'], "A");
	   $headers   = array(); 
	   foreach($postData['paction'] as $field => $val)
	   {
	     $headers[] =  $field;
	   }
	   $groupBy           = $reportObj -> prepareGroupBy($postData['group_by'], "A"); 
	   $sortBy            = $reportObj -> prepareSortBy($postData['sort'], "A");
	   $whereStr         .= $reportObj -> prepareWhereString($postData['value'], $postData['match'], "A");   
	   $whereStr         .= $joinWhere;
	   $results           = $this -> filterResults($fields, $whereStr, $groupBy, $sortBy, "");
        $response          = ActionSorter::sort_actions($results, array("type" => "action", "headers" => $headers));		
        $response['title'] = $postData['report_title'];
        $reportObj -> displayReport($response, "on_screen", "data");	   
	   exit();
	}
     
     function filterResults($fields, $values = "", $groupBy = "", $sortBy = "", $joins = "")
     {
       $orderBy = (!empty($sortBy) ? " ORDER BY $sortBy " : "");
       $groupBy = (!empty($groupBy) ? " GROUP BY $groupBy " : "");
	  $results = $this -> db -> get("SELECT A.id, A.actionstatus, ".$fields." FROM #_action A
	                                INNER JOIN #_kpi KPI ON KPI.id = A.kpiid
	                                INNER JOIN #_kpa KPA ON KPA.id = KPI.kpaid
	                                INNER JOIN #_user_component UC ON UC.componentid = KPA.componentid 
	                                $joins
				                 WHERE 1  AND A.owner <> '' AND A.actionstatus & ".Action::DELETED." <> ".Action::DELETED."
				                 AND UC.status & ".UserComponent::ACTIVATED." = ".UserComponent::ACTIVATED."
				                 $values
				                 $groupBy
				                 $orderBy 
				                ");
	  return $results;	          
     }       
     
     function createJoins($options = array())
     {
        $whereStr = "";
        return $whereStr;  
     }
     
     public static function statusMessage($statusTo, $changeStatus)
     {
        $constants = Action::actionConstants();
        foreach($constants as $key => $value)
        {
           if(($value & $statusTo) == $value)
           {
             if($changeStatus)
             {
               return "Action status changed to <b>".strtolower(str_replace("_", " ", $key))."</b>";
             } else {
               return "<b>".ucfirst(strtolower(str_replace("_", " ", $key)))."</b> status has been removed ";
             }  
           }
        }  
     }	
     
	/*
	 get an array of the user-access class constants
	*/
     public static function actionConstants()
     {
        $oClass      = new ReflectionClass("Action");
        $constants   = $oClass->getConstants();
        return $constants;
     }     
     
     public static function deleteKpiActions($actions)
     {
        foreach($actions as $index => $action)
        {
           Action::updateWhere(array('actionstatus' => 2), array('id' => $action['id']));
        }
     }  
     

  function get_statuses_used($option_sql = "")
  {
     $results  = $this -> db -> get("SELECT status FROM #_action WHERE 1 $option_sql  GROUP BY status");
     $statuses = array();
     foreach($results as $index => $val)
     {
         $statuses[$val['status']] = $val['status'];
     }
    return $statuses;
  }

  function get_used_national_kpas()
  {
     $results     = $this -> db -> get("SELECT nationalkpa FROM #_action WHERE 1 $option_sql  GROUP BY nationalkpa");
     $nationalkpa = array();
     foreach($results as $index => $val)
     {
         $nationalkpa[$val['nationalkpa']] = $val['nationalkpa'];
     }
     return $nationalkpa;
  }  

  function get_used_nation_objective()
  {
     $results       = $this -> db -> get("SELECT idpobjectives FROM #_action WHERE 1 $option_sql  GROUP BY idpobjectives");
     $idpobjectives = array();
     foreach($results as $index => $val)
     {
         $idpobjectives[$val['idpobjectives']] = $val['idpobjectives'];
     }
     return $idpobjectives;
  }     

}
