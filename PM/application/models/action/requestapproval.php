<?php
class RequestApproval extends EmailNotification
{

	function sendEmail($action, $kpi)
	{
	    $this -> message  = "Dear ".$kpi['tkname']." ".$kpi['tksurname']."\r\n\n";
        $this -> message .= $_SESSION['tkn']." updated action #".$action['id']." related to KPI #".$kpi['id']."(".$kpi['name'].") : \r\n\n";		    	
		$this -> message .= "Action : ".$action['action']." \r\n";		
		$this -> message .= "Response : ".$action['response']." \r\n";
		$this -> message .= "Progress : ".$action['progress']."% \r\n";
		$this -> message .= "Status : Completed\r\n\n";
		$this -> message .= "Please log onto Ignite Assist in order to view the action.\r\n\n";						
        $this -> message .= $this -> emailSignature();							
		$this -> subject  = "Request Approval of Complete Action";
		$this -> to    	  = $kpi['tkemail'];
		return $this->notify();
	}	
	
}
?>
