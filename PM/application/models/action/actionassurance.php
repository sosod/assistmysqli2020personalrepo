<?php
class ActionAssurance extends Model
{
    
    
    static $table = "action_assurance";
    
    function __construct()
    {
        parent::__construct();
    }    
    
    function save_assurance($data)
    {
       $data['insertuser']  = $_SESSION['tid']; 
       $res                 = $this -> save($data);
       return $res;
    }
    
    function update_assurance($id, $data)
    {
       $res = $this -> update_where($data, array("id" => $id));
       return $res;
    }
            
    function get_assurances($option_sql = "")
    {
        $results = $this -> db -> get("SELECT K.*, date_format(now(), '%d-%b-%Y %H:%i:%s') AS system_date,
                                       CONCAT(TK.tkname, ' ', TK.tksurname) AS assurance_by 
                                       FROM #_".static::$table." K
                                       INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = K.insertuser
                                       WHERE status & 1 = 1
                                      ");
        return $results;    
    }
    
    function get_assurance($id)
    {
        $result = $this -> db -> getRow("SELECT K.*, date_format(now(), '%d-%b-%Y %H:%i:%s') AS system_date,
                                         CONCAT(TK.tkname, ' ', TK.tksurname) AS assurance_by 
                                         INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = K.insertuser
                                         FROM #_".static::$table." K WHERE id = '".$id."' 
                                       ");
        return $result;    
    }    
    
    
}
?>
