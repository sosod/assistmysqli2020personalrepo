<?php
class ActionSorter extends Sorter
{
     static $next_evaluation   = "";
     
     static $evaluation_status = array();
     
     function __construct()
     {
        parent::__construct();
     }     
     
     public static function sort_actions($data, $options)
     {
        $headers = array();
        $section = (isset($options['section']) ? $options['section'] : "");
        if(isset($options['headers']) && !empty($options['headers']))
        {
           $headerNames = Naming::getHeaderList();
           foreach($options['headers'] as $hIndex => $hval)
           {
               $headers[$hval] = (isset($headerNames[$hval]) ? $headerNames[$hval] : "");   
           }
        } else {
          $headerNames = Naming::getHeaderList($section);
          $headers = $headerNames;
        }   
        $committeeObj         = new EvaluationCommittee();
        $weighting            = Weighting::findList();
        $proficiency          = Proficiency::findList();        
        $nationalkpa          = Nationalkpa::findList();
        $nationalobjectives   = Nationalobjectives::findList();        
        $competency           = Competency::findList(); 
        $userObj              = new User();        
        $statusObj            = new ActionStatus();
        $statuses             = $statusObj -> findList();
        $userdata             = (isset($options['user']) ? $options['user'] : array());
        $users_list           = $userObj -> get_users_list();
        $user_logged          = $userObj -> get_user();
        $datalist['headers']  = array();
        $datalist['data']     = array();
        $datalist['statuses'] = array();
        $datalist['settings'] = array();
        $datalist['userdata'] = $userdata;
        $datalist['users']    = $user_list;
        $datalist['userlogged']   = $user_logged;
        $datalist['in_committee'] = $committeeObj -> is_user_in_evaluation_committee($user_logged['default_year']);
        if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
        {
           $datalist['userlogged']['evaluationperiod']   = $_SESSION['evaluation_period']['open_from']." to ".$_SESSION['evaluation_period']['open_to'];
        }        
        $kpiObj               = new Kpi(); 
        $ratingObj            = new Ratingscale();
        $rating               = $ratingObj -> getRatingInfo();
        
        foreach($data as $action_index => $action)
        {          
           $action_id                            = $action['id'];        
           $datalist['statuses'][$action_id]     = $action['actionstatus'];
           $datalist['settings']['evaluate_on']  = $action['evaluate_on'];
           $evaluations                          = array('self'     => $action['self_evaluation'],
                                                          'manager' => $action['manager_evaluation'],
                                                          'joint'   => $action['joint_evaluation'],
                                                          'review'  => $action['evaluation_review']
                                                        );  
           $action_evaluations             = array('self'    => array(                         
                         'rating'         => ($action['self_rating'] == 0 ? "" : $rating[$action['self_rating']]),
                         'comment'        => $action['self_comment'],
                         'recommendation' => $action['self_recommendation']
                                  ),
                    'manager' => array(
                           'rating'         => ($action['manager_rating'] == 0 ? "" : $rating[$action['manager_rating']]),
                           'comment'        => $action['manager_comment'],
                           'recommendation' => $action['manager_recommendation']
                                       ),
                    'joint'   => array(
                           'rating'         => ($action['joint_rating'] == 0 ? "" : $rating[$action['joint_rating']]),
                           'comment'        => $action['joint_comment'],
                           'recommendation' => $action['joint_recommendation']
                                       ),
                    'review'  => array(
                           'rating'         => ($action['review_rating'] == 0 ? "" : $rating[$action['review_rating']]),
                           'comment'        => $action['review_comment'],
                           'recommendation' => $action['review_recommendation']
                                       )
                    );                                      
           $datalist['settings']['evaluation_type']             = $evaluations;          
           $datalist['settings']['next_evaluation'][$action_id] = self::get_next_evaluation($action_id, $evaluations, $action_evaluations);
           $datalist['settings']['evaluations'][$action_id]     = $action_evaluations;
           $datalist['settings']['evaluationstatuses'][$action_id] = self::$evaluation_status; 
           foreach($headers as $key => $header)
           {
              if(isset($action[$key]))
              {
                 $datalist['headers'][$key]  = $header;
                 $value                      = (isset($action[$key]) ? $action[$key] : "");     
                 if($key == 'progress')
                 {
                   $datalist['data'][$action_id][$key] = $value;  
                 } else if($key == "weighting") { 
                    $datalist['data'][$action_id][$key]  = "<em>".(isset($weighting[$value]) ? $weighting[$value] : '')."</em>";
                 } else if($key == "proficiency") {
                    $datalist['data'][$action_id][$key]  = "<em>".(isset($proficiency[$value]) ? $proficiency[$value] : '')."</em>";
                 } else if($key == "competency") {
                    $datalist['data'][$action_id][$key]  = (isset($competency[$value]) ? $competency[$value] : '');
                 } else if($key == "nationalkpa") {
                    $datalist['data'][$action_id][$key]  = (isset($nationalkpa[$value]) ? $nationalkpa[$value] : '');
                 } else if($key == "idpobjectives") {
                    $datalist['data'][$action_id][$key]  = (isset($nationalobjectives[$value]) ? $nationalobjectives[$value] : '');
                 } else if($key == "owner") {
                    $datalist['data'][$action_id][$key]  = (isset($users_list[$value]) ? $users_list[$value] : "");
                 } else {
                    $datalist['data'][$action_id][$key]  = $value;
                 }                            
             }                      
           }     
        }   
        $datalist['cols'] = count($datalist['headers']);
        return $datalist;
     }
     
     protected static function get_next_evaluation($actionid, $evaluations, $action_evaluations)
     {
         $next_evaluation   = "";
         $evaluation_status = array();
         foreach($evaluations as $key => $status)
         {
           if($status == 1)
           {
               if(isset($action_evaluations[$key]))
               {    
                 if(empty($action_evaluations[$key]['rating']) || empty($action_evaluations[$key]['comment']) 
                    || empty($action_evaluations[$key]['recommendation']))
                 {
                    if(empty($next_evaluation))
                    {
                       $next_evaluation               = $key;
                       self::$evaluation_status[$key] = array("status" => "ui-icon ui-icon-arrow-1-e", "name" => $status);
                    } else {
                       self::$evaluation_status[$key] = array("status" => "", "name" => $status);  
                    }
                 } else {
                    self::$evaluation_status[$key] = array("status" => "ui-icon ui-icon-check", "name" => $status);
                 }
               }
           } else {
               self::$evaluation_status[$key] = array("status" => "ui-icon ui-icon-close", "name" => $status);
           }
         }
         return $next_evaluation;
     }
     
     public static function sort_imported_actions($data, $options = array())
     {
        $headers = array();
        $section = (isset($options['section']) ? $options['section'] : "");
        if(isset($options['headers']) && !empty($options['headers']))
        {
           $headerNames = Naming::getHeaderList();
           foreach($options['headers'] as $hIndex => $hval)
           {
               $headers[$hval] = (isset($headerNames[$hval]) ? $headerNames[$hval] : "");   
           }
        } else {
          $headerNames = Naming::getHeaderList($section);
          $headers = $headerNames;
        }   
        $committeeObj         = new EvaluationCommittee();
        $weighting            = Weighting::findList();
        $proficiency          = Proficiency::findList();        
        $nationalkpa          = Nationalkpa::findList();
        $nationalobjectives   = Nationalobjectives::findList();        
        $competency           = Competency::findList(); 
        $userObj              = new User();        
        $statusObj            = new ActionStatus();
        $statuses             = $statusObj -> findList();
        $userdata             = (isset($options['user']) ? $options['user'] : array());
        $users_list           = $userObj -> get_users_list();
        $user_logged          = $userObj -> get_user();
        $datalist['headers']  = array();
        $datalist['data']     = array();
        $datalist['statuses'] = array();
        $datalist['settings'] = array();
        $datalist['userdata'] = $userdata;
        $datalist['users']    = $user_list;
        $datalist['userlogged']   = $user_logged;
        $datalist['in_committee'] = $committeeObj -> is_user_in_evaluation_committee($user_logged['default_year']);    
        if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
        {
           $datalist['userlogged']['evaluationperiod']   = $_SESSION['evaluation_period']['open_from']." to ".$_SESSION['evaluation_period']['open_to'];
        }         
        $kpiObj               = new Kpi(); 
        $ratingObj            = new Ratingscale();
        $rating               = $ratingObj -> getRatingInfo();
        $evaluations          = $options['evaluations'];
        $settings             = $options['settings'];
        foreach($data as $action_index => $action)
        {          
           $action_id                            = $action['id'];        
           $datalist['statuses'][$action_id]     = $action['actionstatus'];
           $datalist['settings']['evaluate_on']  = $settings['evaluate_on'];
           $_evaluations                         = array('self'     => $settings['self_evaluation'],
                                                          'manager' => $settings['manager_evaluation'],
                                                          'joint'   => $settings['joint_evaluation'],
                                                          'review'  => $settings['evaluation_review']
                                                        );  
          $datalist['settings']['evaluation_type']                  = $_evaluations;                                                   
          if(!empty($evaluations))
          {
             $evals = $evaluations[$action_id];
          
             if($evals['self_rating'] == 0)
             {
                $action_evaluations['self']['rating']  = "";  
             } else {
               $action_evaluations['self']['rating']   = $rating[$evals['self_rating']];
             }
             $action_evaluations['self']['comment']        = $evals['self_comment'];
             $action_evaluations['self']['recommendation'] = $evals['self_recommendation'];
             
             if($evals['manager_rating'] == 0)
             {
                $action_evaluations['manager']['rating']  = "";  
             } else {
               $action_evaluations['manager']['rating']   = $rating[$evals['manager_rating']];
             }
             $action_evaluations['manager']['comment']        = $evals['manager_comment'];
             $action_evaluations['manager']['recommendation'] = $evals['manager_recommendation'];
             
             if($evals['joint_rating'] == 0)
             {
                $action_evaluations['joint']['rating']  = "" ; 
             } else {
               $action_evaluations['joint']['rating']   = $rating[$evals['joint_rating']];
             }
             $action_evaluations['joint']['comment']        = $evals['joint_comment'];
             $action_evaluations['joint']['recommendation'] = $evals['joint_recommendation'];
             
             if($evals['review_rating'] == 0)
             {
                $action_evaluations['review']['rating']  = "";  
             } else {
               $action_evaluations['review']['rating']   = $rating[$evals['review_rating']];
             }
             $action_evaluations['review']['comment']        = $evals['review_comment'];
             $action_evaluations['review']['recommendation'] = $evals['review_recommendation'];
                                                                                    
          } else {
             $action_evaluations['self']['rating']               = "";
             $action_evaluations['self']['comment']              = "";
             $action_evaluations['self']['recommendation']       = "";
             $action_evaluations['manager']['rating']            = "";
             $action_evaluations['manager']['comment']           = "";
             $action_evaluations['manager']['recommendation']    = "";
             $action_evaluations['joint']['rating']              = "";
             $action_evaluations['joint']['comment']             = "";
             $action_evaluations['joint']['recommendation']      = "";
             $action_evaluations['review']['rating']             = "";
             $action_evaluations['review']['comment']            = "";
             $action_evaluations['review']['recommendation']     = "";          
          }                                          
           $datalist['settings']['next_evaluation'][$action_id]    = self::get_next_evaluation($action_id, $_evaluations, $action_evaluations);
           $datalist['settings']['evaluations'][$action_id]        = $action_evaluations;
           $datalist['settings']['evaluationstatuses'][$action_id] = self::$evaluation_status;              
           foreach($headers as $key => $header)
           {
              if(isset($action[$key]))
              {
                 $datalist['headers'][$key]  = $headers[$key];
                 $value                      = $action[$key];
                 if($key == 'progress')
                 {
                   $datalist['data'][$action_id][$key] = $value;  
                 } else  if($key == 'status') {
                   $datalist['data'][$action_id][$key]  = (isset($statses[$value]) ? $statuses[$value] : ''); 
                   $datalist['headers'][$key]  = "Status";                
                 } else if($key == "weighting") { 
                    $datalist['data'][$action_id][$key]  = "<em>".(isset($weighting[$value]) ? $weighting[$value] : '')."</em>";                         
                 } else if($key == "proficiency") {
                    $datalist['data'][$action_id][$key]  = "<em>".(isset($proficiency[$value]) ? $proficiency[$value] : '')."</em>";
                 } else if($key == "competency") {
                    $datalist['data'][$action_id][$key]  = (isset($competency[$value]) ? $competency[$value] : '');
                 } else if($key == "nationalkpa") {
                    $datalist['data'][$action_id][$key]  = (isset($nationalkpa[$value]) ? $nationalkpa[$value] : '');
                 } else if($key == "idpobjectives") {
                    $datalist['data'][$action_id][$key]  = (isset($nationalobjectives[$value]) ? $nationalobjectives[$value] : '');
                 } else if($key == "owner") {
                    $datalist['data'][$action_id][$key]  = (isset($users_list[$value]) ? $users_list[$value] : $value);
                 } else {
                    $datalist['data'][$action_id][$key]  = $value;
                 }                            
              }                              
           }    
        }   
        if($setting['evaluate_on'] == "action")
        {
           $total_to_evaluate = array();
           $next_overall      = "";
           $count             = 0;
           $overall_str       = "";       
           foreach($datalist['settings']['next_evaluation'] as $action_id => $evaluationtype)
           {
             if($evaluationtype == "self")
             {
               $next_overall = "self";
               $overall_str  = "Self Evaluation";
               $count++;
             } else if($evaluationtype == "manager") {
               if(empty($next_overall))
               {
                  $next_overall = "manager";
                  $overall_str  = "Manager Evaluation";
                  $count++;
               }
             } else if($evaluationtype == "joint") {
               if(empty($next_overall))
               {
                  $next_overall = "joint";
                  $overall_str  = "Joint Evaluation";
                  $count++;
               }        
             } else if($evaluationtype == "review") {
               if(empty($next_overall))
               {
                  $next_overall = "review";
                  $overall_str  = "Evaluation Review";
                  $count++;
               }          
             }
             $total_to_evaluate[$evaluationtype][] = $action_id; 
           }
           //$_SESSION[$_SESSION['modref']][$userdata['tkid']]['total_kpa_to_evaluate'] = $count;
           //$_SESSION[$_SESSION['modref']][$userdata['tkid']]['overall_kpa_next']      = $next_overall;
        }
        $datalist['overall_next']      = $next_overall;
        $datalist['overall_str']       = $overall_str;
        $datalist['total_to_evaluate'] = $count;        
        $datalist['cols']              = count($datalist['headers']);        
        $datalist['cols'] = count($datalist['headers']);
        //debug($datalist);
        return $datalist;    
     }   

     public static function component_action($data, $userdata, $headers)
     {

        $committeeObj              = new EvaluationCommittee();
        $weighting                 = Weighting::findList();
        $proficiency               = Proficiency::findList();        
        $nationalkpa               = Nationalkpa::findList();
        $nationalobjectives        = Nationalobjectives::findList();        
        $competency                = Competency::findList(); 
        $userObj                   = new User();        
        $statusObj                 = new ActionStatus();
        $statuses                  = $statusObj -> findList();       
        $users_list                = $userObj -> get_users_list();
        $user_logged               = $userObj -> get_user();    
        $datalist['data']          = array();
        foreach($data as $action_index => $action)
        {             
           $action_id  = $action['id'];
           foreach($headers as $key => $header)
           {              
              if(isset($action[$key]))
              {
                 $value                      = $action[$key];
                 if($key == "weighting") { 
                    $datalist['data'][$action_id][$key]  = "<em>".(isset($weighting[$value]) ? $weighting[$value] : '')."</em>";
                 } else if($key == "proficiency") {
                    $datalist['data'][$action_id][$key]  = "<em>".(isset($proficiency[$value]) ? $proficiency[$value] : '')."</em>";
                 } else if($key == "competency") {
                    $datalist['data'][$action_id][$key]  = (isset($competency[$value]) ? $competency[$value] : '');
                 } else if($key == "nationalkpa") {
                    $datalist['data'][$action_id][$key] = (isset($nationalkpa[$value]) ? $nationalkpa[$value] : '');
                 } else if($key == "idpobjectives") {
                    $datalist['data'][$action_id][$key] = (isset($nationalobjectives[$value]) ? $nationalobjectives[$value] : '');
                 } else if($key == "owner") {
                    $datalist['data'][$action_id][$key] = (isset($users_list[$value]) ? $users_list[$value] : '');
                 } else {
                    $datalist['data'][$action_id][$key] = $value;
                 }                            
             }               
           }    
        }  
        return $datalist;
     } 
     
     public static function component_module_action($data, $userdata, $headers)
     {
        $committeeObj              = new EvaluationCommittee();
        $weighting                 = Weighting::findList();
        $proficiency               = Proficiency::findList();        
        $nationalkpa               = Nationalkpa::findList();
        $nationalobjectives        = Nationalobjectives::findList();        
        $competency                = Competency::findList(); 
        $userObj                   = new User();        
        $statusObj                 = new ActionStatus();
        $statuses                  = $statusObj -> findList();       
        $users_list                = $userObj -> get_users_list();
        $user_logged               = $userObj -> get_user();    
        $datalist['data']          = array();
        foreach($data as $module_index => $module_actions)
        {
            foreach($module_actions as $action_index => $action)
            {             
               $action_id  = $action['id'];
               foreach($headers as $key => $header)
               {              
                  if(isset($action[$key]))
                  {
                     $value                      = $action[$key];
                     if($key == "weighting") { 
                        $datalist['data'][$action_id][$key]  = "<em>".(isset($weighting[$value]) ? $weighting[$value] : '')."</em>";
                     } else if($key == "proficiency") {
                        $datalist['data'][$action_id][$key]  = "<em>".(isset($proficiency[$value]) ? $proficiency[$value] : '')."</em>";
                     } else if($key == "competency") {
                        $datalist['data'][$action_id][$key]  = (isset($competency[$value]) ? $competency[$value] : '');
                     } else if($key == "nationalkpa") {
                        $datalist['data'][$action_id][$key] = (isset($nationalkpa[$value]) ? $nationalkpa[$value] : '');
                     } else if($key == "idpobjectives") {
                        $datalist['data'][$action_id][$key] = (isset($nationalobjectives[$value]) ? $nationalobjectives[$value] : '');
                     } else if($key == "owner") {
                        $datalist['data'][$action_id][$key] = (isset($users_list[$value]) ? $users_list[$value] : '');
                     } else {
                        $datalist['data'][$action_id][$key] = $value;
                     }                            
                 }               
               }    
            }  
        }
        return $datalist;
     }      
}
?>
