<?php
class Evaluationfrequencies extends Model
{
     
     protected static $table = "list_evaluationfrequencies";
     
     public function __construct()
     {
       parent::__construct();
     }

     function update_evaluation_frequency($data)
     {
        $this -> attach(new SetupAuditLog("Evaluation Frequency"));
	      $res = $this -> update_where($data, array("id" => $data['id']));
	      return $res;       
     }
     
     function get_evaluation_frequencies($options = array())
     {
        $option_sql  = "";
        $option_sql  = parent::createANDSqlOptions($options);
        $option_sql .= " AND status & 2 <> 2 "; 
        $used        = $this -> get_used_frequencies(); 
        $results     = $this -> db -> get("SELECT * FROM #_".static::$table." WHERE 1 $option_sql");
        $data        = array();
        foreach( $results as $index => $valArr)
        {
            //those that that have been used should be allow deactivate only and those never use should allow delete
            if( in_array($valArr['id'], $used) )
            {
              $valArr['used'] = true;
              $data[$index]  = $valArr;
            } else {
              $valArr['used'] = false;
              $data[$index]  = $valArr;
            } 
        } 
        return $data;
     }

     function get_used_frequencies()
     {
        $matrixObj = new Performancematrix();
        $_results   = $matrixObj -> get_frequencies_used();

        $evaluationPeriodObj = new EvaluationPeriod();
        $results             = $evaluationPeriodObj -> get_frequencies_used();

        $used_frequencies    = array_merge($results, $_results); 
        return $used_frequencies;
     }


}
?>
