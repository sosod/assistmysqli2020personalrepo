<?php
class Kpastatus extends Model
{

     protected static $table = "list_kpastatus";
	
	function __construct()
	{
		parent::__construct();
	}
     
     public static function getKpaStatuses($options=array())
     {
		$sqlOption   = parent::createANDSqlOptions($options);
		$sqlOption  .= " AND status & 2 <> 2";
		$used        = self::get_used_kpastatuses();
		$statusArr   = array();
		$results     = Kpastatus::findAll($sqlOption);
		foreach( $results as $index => $status)
		{
			if( in_array($status['id'], $used))
			{
				$status['used'] = true;
			} else {
				$status['used']  = false;
			}
			$statusArr[$index] = $status;
		}
		return $statusArr;     
     }
     
     public static function getStatusInUpdateOrder()
     {
        $kpastatuses = Kpastatus::findAll(" AND status & 2 <> 2 AND status & 1 = 1");
        $tmpArr = array();
        $statuses = array();
        foreach($kpastatuses as $index => $status)
        {
          if($status['id'] == "3")
          {
             $tmpArr[] = $status;
          } else {
             $statuses[] = $status;
          }
        }
        $newStatuses = array_merge($statuses, $tmpArr);
        return $newStatuses;
     } 
     
     function update_kpa_status($data)
     {
         $this -> attach(new SetupAuditLog("Kpa Status"));
         $res = $this -> update_where($data['data'], array("id" => $data['id']));
         return $res;
     }

     function get_used_kpastatuses()
     {
         $kpaObj  = new Kpa();
         $results = $kpaObj -> get_used_kpastatuses();
         return $results;
     }
	
}
