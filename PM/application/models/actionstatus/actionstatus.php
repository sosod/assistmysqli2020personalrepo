<?php
class Actionstatus extends Model
{

     protected static $table = "list_actionstatus";
	
	function __construct()
	{
		parent::__construct();
	}
	
	public static function getActionstatuses($options = array())
	{
		$sqlOption   = parent::createANDSqlOptions($options);
		$sqlOption  .= " AND status & 2 <> 2";
		$used 		 = self::get_used_actionstatuses();
		$statusArr   = array();
		$results     = Actionstatus::findAll($sqlOption);
		foreach($results as $index => $status)
		{
			if( in_array($status['id'], $used))
			{
				$status['used'] = true;
			} else {
				$status['used'] = false;
			}
			$statusArr[$index] = $status;
		}
		return $statusArr;
	} 
	
	//get the statuses in update order
	public static function getStatusInUpdateOrder()
	{
	  $actionstatuses = Actionstatus::findAll(" AND status & 2 <> 2 AND status & 1 = 1");
	  $tmpArr = array();
	  $statusArr = array();
	  foreach($actionstatuses as $index => $status)
	  {
	     if($status['id'] == "3")
	     {
	        $tmpArr[] = $status;
	        unset($actionstatuses[$index]);
	     } else {
	        $statusArr[] =  $status;
	     }
	  }
	  $newStatuses = array_merge($statusArr, $tmpArr);
	  return $newStatuses;
	}
	
	function update_action_status($data)
	{
	   $this -> attach(new SetupAuditLog("Action Status"));
	   $res = $this -> update_where($data['data'], array("id" => $data['id']));	     
	   return $res;
	}

	function get_used_actionstatuses()
	{
		$actionObj 	     = new Action();
		$actionstatuses  = $actionObj -> get_statuses_used();
		return $actionstatuses;
	}

}
