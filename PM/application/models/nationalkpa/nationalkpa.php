<?php
class Nationalkpa extends Model
{

     protected static $table = "list_nationalkpa";

	function __construct()
	{
		parent::__construct();
	}
          
     function update_national_kpa($data)
     {
        $this -> attach(new SetupAuditLog("National Kpa"));
        $res = $this -> update_where($data['data'], array('id' => $data['id']));
        return $res;
     }     

     function get_national_kpas($options = array())
     {
        $option_sql = parent::createANDSqlOptions($options);;
        $option_sql.= " AND status & 2 <> 2";
        $used       = $this -> get_used_nation_kpas();
        $nkpaArr    = array();
        $results    = $this -> db -> get("SELECT * FROM #_".static::$table." WHERE 1 $option_sql ");
        foreach( $results as $index => $nationalkpa)
        {
            if( in_array($nationalkpa['id'], $used) )
            {
                $nationalkpa['used'] = true;
            } else {
                $nationalkpa['used'] = false;
            }
            $nkpaArr[$index] = $nationalkpa;
        }
        return $nkpaArr;
     }

     function get_used_nation_kpas()
     {
        $kpaObj           = new Kpa();
        $kpa_national_kpa = $kpaObj -> get_used_national_kpas();

        $kpiObj           = new Kpi();
        $kpi_national_kpa = $kpiObj -> get_used_national_kpas();

        $actionObj           = new Action();
        $action_national_kpa = $actionObj -> get_used_national_kpas();

        $used_national_kpas  = array_merge($kpa_national_kpa, $kpi_national_kpa, $action_national_kpa);
        return $used_national_kpas; 
     }
}
