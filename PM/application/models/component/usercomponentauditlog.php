<?php
class UserComponentAuditLog extends AuditLog
{
 
     private $currentStatus = "";
     
     function __construct()
     {
        parent::__construct();
     }
     
     function notify($postData, $where, $tablename)
     {
        $componentStatuses = "";
        $usercomponents    = UserComponent::fetchAll("AND UC.userid =  '".$where['userid']."' AND UC.evaluationyear = '".$where['evaluationyear']."' ");
        $changes           = array();
        $currentStatus     = "";
        if(!empty($usercomponents))
        {
          foreach($usercomponents as $index => $usercomponent)
          {
              if($usercomponent['componentstatus'] != $postData['status'])
              {
                 $fromStatus = $usercomponent['componentstatus'] - UserComponent::ACTIVE;
                 $toStatus   = $postData['status'] - UserComponent::ACTIVE;
                 
                 $change = $usercomponent['name']." component status changed ";
                 if(($fromStatus & UserComponent::CONFIRMED) == UserComponent::CONFIRMED)
                 {
                    $change .= " from confirmed";
                 }
                 if(($toStatus & UserComponent::ACTIVATED) == UserComponent::ACTIVATED)
                 {
                    $change .= " to activated ";
                    $currentStatus = "Activated";
                 }     
                 if(($toStatus & UserComponent::CONFIRMED) == UserComponent::CONFIRMED)
                 {
                    $change .= " to confirmed ";
                    $currentStatus = "Confirmed";
                 }
                 if(($fromStatus & UserComponent::ACTIVATED) == UserComponent::ACTIVATED)
                 {
                    $change .= " from activated ";
                 }     
                 $changes['component_ref_'.$usercomponent['id']] = $change;
              }
          }
        }

        if(!empty($changes))
        { 
             $changes['user']          = $_SESSION['tkn'];
             $insertdata['insertuser'] = $_SESSION['tid'];
             $changes['currentstatus'] = $currentStatus;
             //$insertdata['id']         = $where['userid'];
             $insertdata['changes']    = serializeEncode($changes);
             static::$table = $tablename."_logs";
             if(!empty($insertdata))
             {
               parent::saveData($insertdata);
             }   
        }
     }     
     
}
?>
