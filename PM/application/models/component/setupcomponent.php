<?php
class SetupComponent extends BaseComponent
{
     
     function __construct()
     {
          parent::__construct();
     }

     public static function getComponents($options=array())
     {
        $sqlOptions = "";
        if(isset($options['to_evaluationyear']) && !empty($options['to_evaluationyear']))
        {
          $copiedComponents = parent::query("SELECT * FROM #_copied_components WHERE from_year = '".$options['evaluationyear']."' 
                                             AND to_year = '".$options['to_evaluationyear']."' ");
          if(!empty($copiedComponents))
          {
             foreach($copiedComponents as $index => $val)
             {
               $sqlOptions .= " AND id <> '".$val['id']."' ";
             }
          }
        }
        $sqlOptions .= " AND evaluationyear = '".$options['evaluationyear']."' ";
        $sqlOptions .= " AND status & 2 <> 2 ";
        $used        = self::get_used_components();
        $components  = array();
        $results     = Component::findAll($sqlOptions); 
        foreach($results as $index => $component)
        {
           if(in_array($component['id'], $used))
           {
              $component['used']  = true;
           } else {
              $component['used']  = false;
           }
           $components[$component['id']]  = $component;
        }
        return $components;   
     }

     public static function get_used_components()
     {
        $usercomponentObj     = new UserComponent();
        $user_component_used  = $usercomponentObj -> get_used_components();

        $matrixObj             = new Performancematrix();
        $matric_component_used = $matrixObj -> get_matrix_component_used();

        return array_merge($user_component_used, $matric_component_used);
     }

}
?>
