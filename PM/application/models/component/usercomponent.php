<?php
class UserComponent extends BaseComponent
{
     const ACTIVE = 1;
     
     const DELETED = 2;
     
     const CONFIRMED = 4;
     
     const ACTIVATED = 8;
     
     protected static $table = "user_component";
     
     private $self_evaluation    = false;
     
     private $manager_evaluation = false;
     
     private $joint_evaluation   = false;
     
     private $review_evaluation  = false;
     
     function __construct()
     {
          parent::__construct();
     }
     /*
      Fetches all the components that the user is linked to 
      @option string  $options, options used to filter the user components
      @return array , array of the user components 
     */
     public static function fetchAll($options = "")
     {
         $results = DBconnect::getInstance() -> get("SELECT UC.userid, UC.componentid, UC.insertuser, UC.created, UC.modified, UC.confirmed, UC.activated,
                                                   UC.status AS componentstatus, LP.shortcode, LP.name, UC.evaluationyear, LP.categories, LP.id
                                                   FROM #_user_component UC
                                                   INNER JOIN #_list_perfomancecomponents LP ON UC.componentid = LP.id 
                                                   WHERE 1 $options");
         return $results;
     }
     
     //useed in matrix report
     function get_component($option_sql = "")
     {
        $results = $this -> db -> get("SELECT UC.userid, UC.componentid, UC.created, UC.modified, UC.confirmed, UC.activated,
                                       M.*, LP.shortcode, LP.name, UC.evaluationyear, LP.categories, LP.id
                                       FROM #_user_component UC 
                                       INNER JOIN #_list_perfomancecomponents LP ON UC.componentid = LP.id 
                                       INNER JOIN #_matrix M ON UC.componentid = M.component_id
                                       WHERE 1  $option_sql
                                     ");
       return $results;                            
     }
     
     public function get_all_components($option_sql = "")
     {
        $results = $this -> db -> get("SELECT UC.userid, UC.componentid, UC.insertuser, UC.created, UC.modified, UC.confirmed, UC.activated,
                                       UC.status AS componentstatus, LP.shortcode, LP.name, UC.evaluationyear, LP.categories, LP.id, M.use_component,
                                       M.component_weight, M.evaluate_on, M.self_evaluation, M.manager_evaluation, M.joint_evaluation, M.evaluation_review,
                                       M.evaluation_frequency FROM #_user_component UC
                                       INNER JOIN #_list_perfomancecomponents LP ON UC.componentid = LP.id 
                                       INNER JOIN #_matrix M ON UC.componentid = M.component_id 
                                       WHERE 1 $option_sql
                                      ");
        return $results;                              
     } 
   
     /*
       Get the the user components depending on the section or page which we are at
       @param array $options contains the array of option to filter the components
       @return array components data
     */     
     function get_user_components($userdata, $options = array())
     {
        $response      = array();
        $error_period  = array();
        $userObj       = new User();
        $matrixObj     = new Performancematrix();
        $evaluationObj = new Evaluation();                
        $committeeObj  = new EvaluationCommittee();        
        $user_logged   = $userObj -> get_user();
        $subordinates  = $userObj -> get_user_subordinates_list();
        $in_committee  = $committeeObj -> is_user_in_evaluation_committee($user_logged['default_year']);
        if($in_committee)
        {
           $evaluated_user_list = $evaluationObj -> get_awaiting_review_list($userObj);
           
           if(!empty($evaluated_user_list))
           {
               $subordinates = array_merge($subordinates, $evaluated_user_list);
           }
           //debug($evaluated_user_list);
        }        
        //debug($subordinates);
        //exit();
        $opts_sql      = "";        
        $user_id       = "";
        $eval_commitee = array();
        if($options['section'] == "new" && !CreationPeriod::isValid($userdata))
        {
          $error_period = array('text' => 'The creation period has expired for '.$userdata['tkname'].' '.$userdata['tksurname'].' 
                                       for the selected evaluation year <b>'.$userdata['start'].' to '.$userdata['end'].'</b>', 
                             'error' => true, "preference" => 2
                           );             
        }         
        if(isset($options['user']) && !empty($options['user']))
        {
           $user_id    = $options['user'];
        } else {
           $user_id    = $_SESSION['tid'];
        }
        $opts_sql  = " AND UC.userid = '".$user_id."' ";               
        $opts_sql .= parent::generateSectionSql($options); 
        $opts_sql .= parent::generatePageSql($options);
        $matrix      = $matrixObj -> get_component_matrix($userdata['categoryid'], $userdata['default_year']);
        if(!empty($userdata))
        {
          $opts_sql   .= " AND UC.evaluationyear = '".$userdata['default_year']."' 
                         AND M.category_id = '".$userdata['categoryid']."' ";
          $components  = $this -> get_all_components($opts_sql);
          if(!empty($components))
          {
            $total_weight    = $matrixObj -> get_total_weights($userdata['categoryid'], $userdata['default_year']);
            $component_list  = array();
            $evaluation_list = array();
            $evaluate_types  = array();
            foreach($components as $index => $component)
            {
               $evaluation  = array();                
               if($matrix[$component['id']]['evaluate_on'] == "action")
               {
                  $evaluation = $this -> _get_action_evaluation($component['id'], $userdata['default_year'], $userdata, $matrix[$component['id']], $options);
               } else if($matrix[$component['id']]['evaluate_on'] == "kpi") {
                  $evaluation = $this -> _get_kpi_evaluation($component['id'], $userdata['default_year'], $userdata, $matrix[$component['id']]);   
               } else if($matrix[$component['id']]['evaluate_on'] == "kpa") {
                  $evaluation = $this -> _get_kpa_evaluation($component['id'], $userdata['default_year'], $userdata, $matrix[$component['id']]);   
               }
                $evaluation_list[$component['id']]  = $evaluation['next_key'];
                $component_list[$component['id']]   = array('name'            => $component['name'], 
                                                            'percentage'      => $component['component_weight']."%",
                                                            'weight'          => $component['component_weight'],
                                                            'short_code'      => $component['shortcode'],
                                                            'next_evaluation' => $evaluation['next'],
                                                            'next_key'        => $evaluation['next_key'],
                                                            'total_next'      => $evaluation['total'],
                                                            'evaluation_type' => $matrix[$component['id']]['evaluate_on']
                                                         );       
                if(isset($matrix[$component['id']]['self_evaluation']))
                {
                   $evaluate_types['self_evaluation'] = 1;
                }
                if(isset($matrix[$component['id']]['manager_evaluation']))
                {
                   $evaluate_types['manager_evaluation'] = 1;
                }                          
                if(isset($matrix[$component['id']]['joint_evaluation']))
                {
                   $evaluate_types['joint_evaluation'] = 1;
                }                        
                if(isset($matrix[$component['id']]['evaluation_review']))
                {
                   $evaluate_types['evaluation_review'] = 1;
                }
            }
            $next         = $evaluationObj -> get_components_next_evaluation($evaluation_list);
            $prev         = $evaluationObj -> get_components_previous_evaluation($evaluate_types, $next);
            $change_str   = "";
            $prev_eval    = "";
            $is_confirmed = true;
            $c_str        = "";
            $can_evaluate = false;
            $result       = array();
            /*
            debug($userdata);
            debug($user_logged);
            debug($_SESSION);
            debug($evaluation_list);
            //debug($evaluate_types);
            echo "Is this user in committee --- ".var_dump($in_commitee)."\r\n\n";
            echo "Next is ".$next." and previuos evaluation is ".$prev." \r\n\n\n";
            //exit();
            */
            if($options['page'] == 'evaluate')
            {
              if(isset($_SESSION[$_SESSION['modref']][$userdata['tkid']]))
              {                    
                 if(isset($_SESSION[$_SESSION['modref']][$userdata['tkid']]['overall_next']))
                 {                
                    $overall_next = $_SESSION[$_SESSION['modref']][$userdata['tkid']]['overall_next'];
                    $is_confirmed = $evaluationObj -> is_previous_evaluation_confirmed($userdata['tkid'], $prev);    
                    //echo "Is the previous evaluation accpeted ---- \r\n";
                    //echo "\r\n\n".var_dump($is_confirmed)."\r\n\n";
                    //echo "Overall next --- ".$overall_next."---";
                     /*
                      If the next evaluation for the components is not the same now as the current overall next evaluation, 
                      then raise an complete evaluation flag, that when the evavlation has not been confirmed
                      If the evaluation has not been confirmed then, change next to be the previous evaluation...
                     */
                     if($next != $overall_next)
                     {
                        if(!$is_confirmed)
                        {
                           $next       = $prev;
                           $prev_eval  = $prev;  
                        }
                        $result     = $this -> _get_evaluation_msg($next, $user_logged, $userdata, $in_committee);
                     } else {
                        /*
                         if the previous evaluation is not the same as the current overall evaluation, then check if the 
                         previous was confirmed, if not then raise complete evauation flag 
                        */                        
                        if($prev != $overall_next)
                        {
                          if(!$is_confirmed)
                          {
                             $next       = $prev;
                             $prev_eval  = $prev;                               
                             $result     = $this -> _get_evaluation_msg($prev, $user_logged, $userdata, $in_committee);
                          }
                        }
                     }
                 }
              } else {
                $is_confirmed = $evaluationObj -> is_previous_evaluation_confirmed($userdata['tkid'], $prev);
                if(!$is_confirmed)
                {
                   $next      = $prev;
                   $prev_eval = $prev;  
                   $result     = $this -> _get_evaluation_msg($prev, $user_logged, $userdata, $in_committee);
                }              
              }
            }  
             $response['components']                                           = $component_list;
             $response['totalWeight']                                          = $total_weight['totalW'];
             $response['next']                                                 = $next;
             if(isset($result))
             {
               $response['can_evaluate']                                       = (boolean)$result['can_evaluate'];
               $response['transition_message']                                 = (string)$result['msg_str'];
             } else {
               $response['transition_message']                                 = "";
               $response['can_evaluate']                                       = (boolean)$can_evaluate; 
             }
             $response['previous_evaluation']                                  = $prev;
             $_SESSION[$_SESSION['modref']][$userdata['tkid']]['overall_next'] = $next;
             $response['is_cofirmed']                                          = $is_confirmed;
             
            /*
            echo "After all is done next is ".$next."\r\n\n\n";
            debug($confirmed);
            debug($result);
            debug($response);
            debug($_SESSION);
            exit();          
            */
          } else {
             //if the user components for this section were empty , check the status they are in 
             $sql        = " AND UC.status & 2 <> 2 AND UC.evaluationyear = '".$userdata['default_year']."' 
                             AND UC.userid = '".$userdata['tkid']."' 
                           ";   
             $components = $this -> get_all_components($sql); 
             if(!empty($components))
             {
               $componentStatus = self::checkComponentStatuses($components, $userdata);
               $response        = array("text" => $componentStatus, "error" => true, "preference" => 1);
             } else {
               $response = array('text' => 'Performance components for <b>'.$userdata['performancecategory'].'</b> 
                                            category not configured for the selected evaluation year', 
                                  'error' => true
                                );
             }
          }                  
        } else {
           $msg = '<ul>';
             $msg .= '<li>User data not found.';
             $msg .= '<li>Please make sure you configure the following user settings correcty under 
                          <b>Setup > User Access</b> , 
                          <b>Setup > Job Level  - Perfomance Categories</b> and <b>Manage > My Profile > Configure 
                           Evaluation Year</b></li>';
           $msg .= '</ul>';
           $response = array('text' => $msg, 'error' => true);            
        }
        $_response = array();     
        if(isset($error_period) && !empty($error_period))
        {
          if(isset($response['preference']) && $response['preference'] == 1)
          {
            $_response = $response;
          } else {
            $_response = $error_period;
          }
          $response = $_response;
        }
        $response['userdata']     = $userdata;
        $response["subordinates"] = $subordinates;     
        return $response; 
     }
     
     function _get_evaluation_msg($eval_type, $user_logged, $userdata, $in_committee)
     {  
        $c_str          = "";
        $can_evaluate   = false;
        if($eval_type == "self")
        {  
           if($user_logged['tkid'] == $userdata['tkid'])
           {
              $can_evaluate = true;     
               $c_str  = "I, ".$userdata['user'].", employee of ".$userdata['managername']." hereby :";
               $c_str  .= "<ul>";
                 $c_str .= "<li>Confirm that I have completed the self evaluation of all the components of my Perfomance Matrix for the evaluation period <b>".$_SESSION['evaluation_period']['start_date']." - ".$_SESSION['evaluation_period']['end_date']."</b> of the evaluation year <b>".$userdata['start']." - ".$userdata['end']."</b>; </li>";
                 $c_str .= "<li>Acknowledge that an e-mail can be sent to my manager ".$userdata['managername']." requesting the completion of my manager evaluation.</li>";
              $c_str .= "</ul>";
            }
         } else if($eval_type == "manager") {
            if(($user_logged['managerid'] ==  "S") || ($user_logged['tkid'] == $userdata['managerid']))
            {
               $can_evaluate = true;                                                                    
               $c_str  = "I, ".$userdata['managername'].", manager of employee ".$userdata['user']." hereby :";
               $c_str  .= "<ul>";
                 $c_str .= "<li>Confirm that I have completed the manager evaluation of all the components of ".$userdata['user']."'s Perfomance Matrix for the evaluation period <b>".$_SESSION['evaluation_period']['start_date']." - ".$_SESSION['evaluation_period']['end_date']."</b> of the evaluation year <b>".$userdata['start']." - ".$userdata['end']."</b>; </li>";
                 $c_str .= "<li>Acknowledge that an e-mail can be sent to ".$userdata['user']." informing the employee that the process of completing the joint evaluation can start.</li>";
               $c_str .= "</ul>";      
            }                   
         } else if($eval_type == "joint") {
            if(($user_logged['tkid'] ==  $userdata['managerid']) 
                || ($user_logged['tkid'] == $userdata['tkid']))
            {
               $can_evaluate = true;
               $c_str        = " Joint evaluation has been completed.";
            }                                    
            //$change_str = ""
         } else {
            if($in_committee)
            {
               $can_evaluate = true;
               $c_str        = "Evaluation review evaluation has been completed.";
            }  
         }
         return array('msg_str' => $c_str, 'can_evaluate' => $can_evaluate);         
     }   
     
     
     /*
      Get the status name , description from constants 
      @param integer $status , the value of the status requested
      @return string , the description of the status of the user components 
     */
     public static function getStatus($status)
     {  
        $constants = self::userComponentConstants();
        foreach($constants as $constantkey => $constantVal)
        {
          if(($constantVal & $status) == $constantVal)
          {
               return strtolower(str_replace("_", " ", $constantkey));
          }
        }
     }
     
     /*
       Creates the array of the constans of this class
       #params void 
       @return array , an array of this class's constants list
     */
     public static function userComponentConstants()
     {
        $reflectionObj = new ReflectionClass("UserComponent");
        return $reflectionObj->getConstants();     
     } 
     
     function _get_action_evaluation($component_id, $evaluationyear, $userdata, $setting, $options)
     {
       $evaluatioObj      = new ActionEvaluation();
       $evaluatioObj -> get_evaluations($component_id, $evaluationyear, $userdata, $setting);
       $evaluatioObj -> get_imported_actions_evaluations($component_id, $evaluationyear, $userdata, $setting, $options, $evaluatioObj);
       $evaluations  = $evaluatioObj -> get_total_next_evaluations();   
       return $evaluations;
     }
     
     function _get_kpi_evaluation($component_id, $evaluationyear, $userdata, $setting)
     {
       $evaluatioObj        = new KpiEvaluation();
       $evaluatioObj -> get_evaluations($component_id, $evaluationyear, $userdata, $setting); 
       $evaluations         = $evaluatioObj -> get_total_next_evaluations();    

       return $evaluations;
     }
     
     function _get_kpa_evaluation($component_id, $evaluationyear, $userdata, $setting)
     {
       $evaluatioObj          = new KpaEvaluation();
       $evaluatioObj -> get_evaluations($component_id, $evaluationyear, $userdata, $setting); 
       $evaluations         = $evaluatioObj -> get_total_next_evaluations();        
       return $evaluations;       
     }  
             
     protected function _get_next_evaluation($evaluation_settings, $evaluations)
     {
         $next_evaluation   = "";
         $evaluation_status = array();
         $settings          = array("self_evaluation", "manager_evaluation", "joint_evaluation", "evaluation_review");
         foreach($evaluation_settings as $key => $status)
         {
           if(in_array($key, $settings) && $status == 1)
           {
               if(isset($evaluations[$key]))
               {
                 if(empty($evaluations[$key]['rating']) || empty($evaluations[$key]['comment']) 
                    || empty($evaluations[$key]['recommendation']))
                 {
                    if(empty($next_evaluation))
                    {
                       $next_evaluation               = $key;
                    }
                 }
               } else {
                 if(empty($next_evaluation))
                 {
                    $next_evaluation               = $key;
                 }                   
               }
           }
         }
         return $next_evaluation;
     }
     
     /*public static function getUserComponents($options = array(), $user)
     {
         $optionalSql = "";
         $userID  = "";
         $response = array();
         if(isset($options['user']) && !empty($options['user']))
         {
           $optionalSql = " AND UC.userid = '".$options['user']."'";
           $userID      = $options['user'];
         } else {
           $optionalSql .= " AND UC.userid = '".$_SESSION['tid']."'";
           $userID       = $_SESSION['tid'];
         }
         $subordinates = User::getSubOrdinatesList();
         $optionalSql  .= parent::generateSectionSql($options);
         $optionalSql  .= parent::generatePageSql($options);
         if(!empty($user))
         {
           $optionalSql .= " AND UC.evaluationyear = '".$user['default_year']."' ";
         }
         $components = self::fetchAll($optionalSql); 
         if(!empty($user))
         {
            if($options['section'] == "new" && !CreationPeriod::isValid($user))
            { 
                  $response = array("text" => "The creation period has expired for ".$user['tkname']." ".$user['tksurname']." for the selected evaluation year <b>".$user['start']." to ".$user['end']."</b>", "error" => true);
            } else {               
              if(!empty($components))
              {
                  $performanaceMatrix = Performancematrix::getASetting($user['categoryid'], $user['default_year'], array("weighting", "usecat", "selfevaluation", "managerevaluation", "jointevaluation", "evaluationreview", "evaluationfreq"));
                  $componentsArr      = array(); 
                  if(!empty($performanaceMatrix))
                  {
                     $outstanding_evaluations = Evaluation::outstandingEvaluations($performanaceMatrix);                  
                     $totalWeight = 0;
                     foreach($components as $cIndex => $component)
                     {
                         if(isset($performanaceMatrix['usecat'][$component['id']]))
                         {
                           $totalWeight = array_sum($performanaceMatrix['weighting']);
                           $componentsArr[$component['id']] = array('name'        => $component['name'],
                                                                    'percentage'  => $performanaceMatrix['weighting'][$component['id']]."%",
                                                                    'weight'      => $performanaceMatrix['weighting'][$component['id']],
                                                                    'shortcode'   => $component['shortcode']
                                                                   );
                         }
                     }
                     $response = array("components" => $componentsArr, "totalWeight" => $totalWeight);
                    } else {
                    $response = array("text" => "Category <b>".$user['performancecategory']."'s</b> performance matrix is not fully configured for the selected evaluation year (<b>".$user['start']." to ".$user['end']."</b>) under setup", "error" => true);
                    }
                 } else {
                      //if the user components for this section were empty , check the status they are in 
                      $sql = " AND UC.status & 2 <> 2 
                               AND UC.evaluationyear = '".$user['default_year']."' AND UC.userid = '".$user['tkid']."' ";   
                      $components = self::fetchAll($sql); 
                      if(!empty($components))
                      {
                        $componentStatus = self::checkComponentStatuses($components, $user);
                        $response       = array("text" => $componentStatus, "error" => true);
                      } else {
                        
                          
                        $response = array("text" => "Performance components for <b>".$user['performancecategory']."</b> category not configured for the selected evaluation year", "error" => true);
                      }
                 } 
              } 
            } else {
                      $response = array("text" => "<ul><li>User data not found.</li><li>Please make sure you configure the following user settings correcty under <b>Setup > User Access</b> , <b>Setup > Job Level  - Perfomance Categories</b> and <b>Manage > My Profile > Configure Evaluation Year</b></li></ul>", "error" => true);
         } 
         $response['userdata']     = $user;
         $response["subordinates"] = $subordinates;
         //debug($response);
         return $response; 
     }*/
     /*
       Get the statuses of the user components depending on the section were are in
       @param array $components , list of user components
       @param array $user   - user array
     */
     static function checkComponentStatuses($components, $user)
     {
          //get the list of the status of the components now;
          $statusText = "<span><b>User component status </b></span><br /><ul>";                
          $totalComponents = count($components);
          $notConfrimed = 0;
          $confirmed = 0; 
          $notAllConfirmedStr = "";
          $confirmedStr = "";
          foreach($components as $rIndex => $rVal)
          {
             if(($rVal['componentstatus'] & UserComponent::ACTIVATED) == UserComponent::ACTIVATED)
             {
               $confirmedStr  = "Performanace matrix for <b>".$user['tkname']." ".$user['tksurname']."</b> for the evaluation year <b>".$user['start']." to ".$user['end']." </b> has been Confirmed and Activated";
               $confirmed  += 1;
             } else if(($rVal['componentstatus'] & UserComponent::DELETED) !== UserComponent::DELETED) {
               $componentStatus = $rVal['componentstatus'] - UserComponent::ACTIVE;
               if($componentStatus == 0)
               {
                  $notConfrimed  += 1; 
                  $notAllConfirmedStr = "Performanace matrix for <b>".$user['tkname']." ".$user['tksurname']."</b> for the evaluation year <b>".$user['start']." to ".$user['end']."</b> has not been confirmed";
               } else {
                 $confirmed += 1;
                 //echo $componentStatus."\r\n\n";
                 $confirmedStr .= "<li><b>".$rVal['name']."</b> component  is <i>".UserComponent::getStatus($componentStatus)."</i></li>";
               }
             }
          }	
          if($confirmed == $totalComponents)
          {
             $statusText = $confirmedStr;
          } else {
             $statusText = $notAllConfirmedStr;
          }
          //echo "Total of ".$confirmed." have been confirmed and ".$notConfrimed." have not been confirmed out of ".$totalComponents."\r\n\n";
          $statusText .= "</ul>";
          return $statusText;
     }
     
     /*
        Activate user components that have been confirmed
        @param string $userid - the user id for which the components are activated
     */
     public function activate($userid)
     {
         $userObj             = new User();
         $userdata            = $userObj -> getUser(array('user' => $userid));
	    $confirmedComponents = UserComponent::findAll(" AND status & ".UserComponent::CONFIRMED." = ".UserComponent::CONFIRMED." 
	                                                    AND userid =  '".$userid."' AND evaluationyear = '".$userdata['default_year']."' ");
	    $res = 0;
	    if(!empty($confirmedComponents))
	    {
	       $this -> attach(new UserComponentAuditLog());
	       $res = $this -> update_where( array('status'    => UserComponent::ACTIVE + UserComponent::ACTIVATED,
	                                                'activated' => date('Y-m-d H:i:s')
	                                                ),
	                                            array('userid' => $userid, 'evaluationyear' => $userdata['default_year'])
	                                         );
	       if($res > 0)
	       {
	          Kpa::createUnspecifiedKPA($userid);
	       }
	    }          
	    return $res;
     }
     
     public static function getComponents($options)
     {
        $user = User::getUser($options);
        unset($options['page']);
        $userSql = " AND userid = '".$user['tkid']."' AND evaluationyear = '".$user['default_year']."' ";
        $components      = UserComponent::fetchAll($userSql);
        $categorySetting = Performancematrix::getASetting($user['categoryid'], $user['default_year'], array("weighting", "usecat"));
        $totalWeight     = 0; 
        $usercomponents  = array();
        foreach($components as $index => $usercomponent)
        {
          if($categorySetting['usecat'][$usercomponent['id']])
          {
            $usercomponents[$usercomponent['id']]['componentid'] = $usercomponent['id'];
            $usercomponents[$usercomponent['id']]['name'] = $usercomponent['name']; 
            $usercomponents[$usercomponent['id']]['weight'] = $categorySetting['weighting'][$usercomponent['id']]; 
            $totalWeight += $categorySetting['weighting'][$usercomponent['id']];
            $options['componentid'] = $usercomponent['id'];
            $componentCategories = serializeDecode($usercomponent['categories']);
            $componentKpa = Kpa::getComponentKPA($user, $componentCategories, $options);
            $usercomponents[$usercomponent['id']]['kpa'] = $componentKpa;
            
          } 
        }
        return array("usercomponents" => $usercomponents, "user" => $user);
     }
     
     function get_used_components($option_sql = "")
     {
        $results    = $this -> db -> get("SELECT componentid FROM #_user_component WHERE 1 $option_sql GROUP BY componentid");
        $components = array();
        foreach($results as $index => $result)
        {
            $components[$result['componentid']] = $result['componentid'];
        }
        return $components;
     }

     public static function getComponentTesting($options)
     {
        $user = User::getUser($options);
        unset($options['page']);
        $userSql = " AND userid = '".$user['tkid']."' AND evaluationyear = '".$user['default_year']."' ";
        $components      = UserComponent::fetchAll($userSql);
        $categorySetting = Performancematrix::getASetting($user['categoryid'], $user['default_year'], array("weighting", "usecat"));
        $totalWeight     = 0; 
        $usercomponents  = array();
        foreach($components as $index => $usercomponent)
        {
          if($categorySetting['usecat'][$usercomponent['id']])
          {
            $usercomponents[$usercomponent['name']] = array(); 
            //$usercomponents[$usercomponent['id']]['weight'] = $categorySetting['weighting'][$usercomponent['id']]; 
            $totalWeight += $categorySetting['weighting'][$usercomponent['id']];
            $options['componentid'] = $usercomponent['id'];
            $componentCategories = serializeDecode($usercomponent['categories']);
            $componentKpa = Kpa::getComponentKPA($user, $componentCategories, $options);
            $usercomponents[$usercomponent['name']] = $componentKpa;
            //$usercomponents[$usercomponent['id']]['kpa'] = $componentKpa;
            
          } 
        }
        return array("usercomponents" => $usercomponents, "user" => $user);
     }
     
     public static function copySaveUserComponents($data)
     {
        $components = UserComponent::findAll(" AND componentid = '".$data['component_id']."' AND evaluationyear = '".$data['from_year']."' ");
        if(!empty($components))
        {
          foreach($components as $index => $component)
          {
             $saveData = array('evaluationyear' => $data['to_year'], 'componentid' => $data['component_id'], 'userid' => $component['userid'], 'insertuser' => $_SESSION['tid']);
             UserComponent::saveData($saveData);
             Kpa::copySaveToNewYear($data['component_id'], $data['from_year'], $data['to_year'], $component['userid']);
          }
        }
     }
     
     /*
       Get the components that are not used yet since the initial setup, ie those added to the matrix after the initial setup
     */
     public static function getUnUsedComponents($user)
     {
       $optionalSql = " AND UC.evaluationyear = '".$user['default_year']."' AND UC.userid = '".$user['tkid']."' ";
       $currentUserComponent = UserComponent::fetchAll($optionalSql);
       $performanaceMatrix = Performancematrix::getASetting($user['categoryid'], $user['default_year'], array("usecat"));
       $usedComponents = array();
       $unusedComponents = array();
       $unusedSql  = "";//" AND evalutionyear = '".$user['default_year']."' (";
       if(isset($performanaceMatrix) && !empty($performanaceMatrix))
       {
          foreach($performanaceMatrix['usecat'] as $componentId => $catInfo)
          {
             if(isset($currentUserComponent[$componentId]))
             {
               $usedComponents[] = $componentId;
             } else {
               $unusedSql .= " id = ".$componentId." OR";
               $unusedComponents[] = $componentId;
             }
          }
       }         
       return $unusedComponents;      
     }
     /*
      Update components that were added to the perfomance matrix after the initial setup was done for this user
      @param - $components to update
      @param - $userdata , contains information of the user to be updated 
     */
     function updateComponents($components, $userdata)
     {
        foreach($components as $catIndex => $compoennts)
        {
            //save the user components 
            $res = UserComponent::saveData(array("componentid" => $catIndex, "userid" => $userdata['tkid'], "insertuser" => $_SESSION['tid'], "evaluationyear" => $userdata['default_year']));
            if($res > 0)
            {
                Kpa::copySave($catIndex, $userdata['default_year'], $userdata);
            }
        }
     }
     
     public static function get_creation_status($options)
     {
	    $users      = array();
        $user_obj   = new User();
        $user_sql   = "";
        $userdata   = array(); 
        $year       = array();        
        if(isset($options['evaluationyear']))
        {
            $year  = EvaluationYear::findById($options['evaluationyear']);        
        }
        if(isset($options['directorate']) && !empty($options['directorate']))
        {
           $user_sql .= " AND LD.id = '".$options['directorate']."' ";
        }
        if(isset($options['manager']) && !empty($options['manager']))
        {
           $userdata  = $user_obj -> get_user(array('user' => $options['manager']));
           $user_sql .= " AND TK.tkmanager = '".$options['manager']."' ";
        }        
        $users  = $user_obj -> get_users($user_sql);
        if(isset($userdata) && !empty($userdata))
        {
           if(!isset($options['employee']) || empty($options['employee']))
           {
              $users = array_merge(array(array_slice($userdata, 0, -1)), $users);
           }          
        }
        $subordinates         = User::getInstance() -> getUserSubOrdinates($options);
        $statusesOfComponents = array();
        $allusers             = $user_obj -> get_users_list();
        if(!empty($users))
        {
           //if($userdata['tkmanager'] == "Self")
           //{
              //$subordinates =  array_merge($subordinates, array(array_slice($userdata, 0, 3)));
           //}                
           $i = 0; 
           foreach($users as $s_index => $subordinate)
           {    
              $optionalSql  = "";              
              $optionalSql .= " AND UC.userid = '".$subordinate['tkid']."' ";
              if(isset($options['evaluationyear']))
              {
                $optionalSql .= " AND UC.evaluationyear = '".$options['evaluationyear']."' ";
              }         
              $manager = "";
              if($subordinate['tkmanager'] == "S")
              {
                $manager = $subordinate['user'];
              } else if(isset($allusers[$subordinate['tkmanager']])){
                $manager = $allusers[$subordinate['tkmanager']];
              } else {
                $manager = $subordinate['tkmanager'];
              }
              $components                          = self::fetchAll($optionalSql);       
              $dates                               = array();     
              $statusesOfComponents[$i]['user']    = $subordinate['tkname']." ".$subordinate['tksurname'];
              $statusesOfComponents[$i]['manager'] = $manager;
              //$userdata['tkname']." ".$userdata['tksurname'];
              //$statusesOfComponents[$i]['manager'] = $subordinate['tkname']." ".$subordinate['tksurname'];
              if(!empty($components))
              {
                foreach($components as $index => $component)
                {
                   $statusesOfComponents[$i]['created'] = "<span style='color:#FF0000';>".date('d-M-Y', strtotime($component['created']))."</span>";             
                   if(($component['componentstatus'] & UserComponent::ACTIVATED) == UserComponent::ACTIVATED)
                   {
                     $statusesOfComponents[$i]['confirmed'] = "<span style='color:#FE9900';>".date('d-M-Y', strtotime($component['confirmed']))."</span>";
                     $statusesOfComponents[$i]['activated'] = "<span style='color:#009900';>".date('d-M-Y', strtotime($component['activated']))."</span>";
                     continue;
                   } else if(($component['componentstatus'] & UserComponent::CONFIRMED) == UserComponent::CONFIRMED)
                   {
                     $statusesOfComponents[$i]['confirmed'] = "<span style='color:#009900';>".date('d-M-Y', strtotime($component['confirmed']))."</span>";
                     $statusesOfComponents[$i]['activated'] = "Not Yet Done";                     
                     continue;
                   } else {
                     $statusesOfComponents[$i]['confirmed'] = "Not Yet Done";
                     $statusesOfComponents[$i]['activated'] = "Not Yet Done";                                          
                   }
                }
              }  else {
                 $statusesOfComponents[$i]['status'] = "There are no components setup for this user yet";
              }  
              $i++;       
           }
         }
         $report['data']        = $statusesOfComponents;
         $report['report_date'] = date("d-M-Y");
         $report['year']        = $year;
         return $report;
     }
     
     function get_matrix_confirmed_report($options)
     {
	    $users      = array();
        $user_obj   = new User();
        $user_sql   = "";
        $userdata   = array(); 
        $year       = array();        
        if(isset($options['evaluationyear']))
        {
            $year  = EvaluationYear::findById($options['evaluationyear']);        
        }        
        if(isset($options['directorate']) && !empty($options['directorate']))
        {
           $user_sql .= " AND LD.id = '".$options['directorate']."' ";
        }        
        if(isset($options['employee']) && !empty($options['employee']))
        {
           $user_sql = " AND TK.tkid = '".$options['employee']."' ";
           $userdata = $user_obj -> get_user(array('user' => $options['employee']));
        }
        $users  = $user_obj -> get_users($user_sql);
        if(isset($userdata) && !empty($userdata))
        {
           if(!isset($options['employee']) || empty($options['employee']))
           {
              $users = array_merge(array(array_slice($userdata, 0, -1)), $users);
           }          
        }
        $evaluationyear = array();
        if(isset($options['evaluationyear']) && !empty($options['evaluationyear']))
        {
          $evaluationyear = Evaluationyear::findById($options['evaluationyear']);
        }        
        $allusers   = $user_obj -> get_users_list();
        $usermatrix = array();
        $headers    = Naming::getHeaderList();
        if(!empty($users))
        {
           $i = 0;
           foreach($users as $index => $user)
           {
              $usermatrix[$i]['user']['user']      = $user['user'];
              $usermatrix[$i]['user']['manager']   = ($user['tkmanager'] == "S" ? $user['user'] : $allusers[$user['tkmanager']]);
              $usermatrix[$i]['user']['joblevel']  = $user['joblevel'];
              $usermatrix[$i]['user']['category']  = $user['performancecategory'];
              //$usermatrix[$i]['user']['period']    = $period['start']." - ".$period['end'];  
              $usermatrix[$i]['user']['year']      = $evaluationyear['start']." - ".$evaluationyear['end']; 

              //$optional_sql  = " componentstatus & ".UserComponent::CONFIRMED." = ".UserComponent::CONFIRMED." ";
              $optional_sql  = ""; 
              $optional_sql .= " AND UC.userid = '".$user['tkid']."' AND M.category_id = '".$user['categoryid']."'";              
              if(isset($options['evaluationyear']))
              {
                $optional_sql .= " AND UC.evaluationyear = '".$options['evaluationyear']."' ";
              } 
              $usercomponentObj = new UserComponent();
              $usercomponents   = $usercomponentObj -> get_component($optional_sql);   
              if(!empty($usercomponents))
              {
                 foreach($usercomponents as $index => $usercomponent)
                 {
                    $usermatrix[$i]['component'][$usercomponent['id']]['componentname'] = $usercomponent['name'];
                    $usermatrix[$i]['component'][$usercomponent['id']]['weight']        = $usercomponent['component_weight'];
                    
                    $kpas       = $this -> _get_component_kpas($user, $usercomponent, $options);
                    if(!empty($kpas))
                    {
                      $sorted_kpa = KpaSorter::component_kpa($kpas, $user, $headers); 
                      $usermatrix[$i]['component'][$usercomponent['id']]['kpa']  = $sorted_kpa;
                    }
                    $kpis       = $this -> _get_component_kpis($user, $usercomponent, $options);
                    if(!empty($kpis))
                    {
                        $sorted_kpi = KpiSorter::component_kpi($kpis, $user, $headers);
                        $usermatrix[$i]['component'][$usercomponent['id']]['kpi']  = $sorted_kpi;                        
                    }
                       
                    $actions       = $this -> _get_component_actions($user, $usercomponent, $options);
                    if(!empty($actions))
                    {
                      $sorted_action = ActionSorter::component_action($actions, $user, $headers);
                      $usermatrix[$i]['component'][$usercomponent['id']]['actions']  = $sorted_action;
                    }
                    if($usercomponent['id'] == 3)
                    {
                       $module_actions = $this -> _get_component_module_actions($user, $usercomponent, $options);
                       if(!empty($module_actions))
                       {
                          $sorted_module_action = ActionSorter::component_module_action($module_actions, $user, $headers);
                          $usermatrix[$i]['component'][$usercomponent['id']]['module_actions']  = $sorted_module_action;
                       }           
                    }
                    
                 }
              }
               $i++;
           }
        }        
        //debug($usermatrix);
        $report['report_date'] = date('d-M-Y');
        $report['usermatrix']  = $usermatrix;
        $report['year']        = $year;
        return $report;
     }
     
     function _get_component_kpas($user, $component, $options)
     {
        $results  = $this -> db -> get("SELECT K.* FROM #_kpa K WHERE K.owner = '".$user['tkid']."' 
                                        AND K.componentid = '".$component['id']."' 
                                        AND K.evaluationyear = '".$options['evaluationyear']."'
                                        AND K.kpastatus & ".Kpa::DELETED." <> ".Kpa::DELETED."
                                      ");                                  
        return $results;
     }
     
     function _get_component_kpis($user, $component, $options)
     {
        $results  = $this -> db -> get("SELECT K.* FROM #_kpi K 
                                        INNER JOIN #_kpa KPA ON KPA.id = K.kpaid
                                        WHERE K.owner = '".$user['tkid']."' 
                                        AND KPA.componentid = '".$component['id']."' 
                                        AND KPA.evaluationyear = '".$options['evaluationyear']."'
                                        AND K.kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED."
                                      ");
        return $results;       
     }
     
     function _get_component_actions($user, $component, $options)
     {
        $results  = $this -> db -> get("SELECT A.* FROM #_action A 
                                        INNER JOIN #_kpi KPI ON KPI.id = A.kpiid
                                        INNER JOIN #_kpa KPA ON KPA.id = KPI.kpaid
                                        WHERE A.owner = '".$user['tkid']."' 
                                        AND KPA.componentid = '".$component['id']."' 
                                        AND KPA.evaluationyear = '".$options['evaluationyear']."'
                                        AND A.actionstatus & ".Action::DELETED." <> ".Action::DELETED."
                                      ");
        return $results;              
     }        
     
     function _get_component_module_actions($user, $component, $options)
     {
        $results = $this -> db -> get("SELECT KPI.* FROM #_kpi KPI 
                                       INNER JOIN #_kpa KPA ON KPI.kpaid = KPA.id 
                                       AND KPA.componentid = '".$component['id']."' 
                                       AND KPA.evaluationyear = '".$options['evaluationyear']."'
                                       AND KPI.kpistatus & ".Kpi::KPIACTION_IMPORTED." = ".Kpi::KPIACTION_IMPORTED."
                                       AND KPI.kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED."  
                                     ");   
         $moduleactions = array();
         if(!empty($results))
         {
           $kpimappingObj = new KpiMapping();
           foreach($results as $index => $kpi)
           {
	         $module_actions = $kpimappingObj -> getModuleActions($kpi, $kpi['modulemapping'], $user, $component);
	         $moduleactions[]  = $module_actions['actions'];
           }                
         }
        return $moduleactions;
     }  
     
}
?>
