<?php
class Component extends BaseComponent
{

    //stores status the user components  that have been confirmed
    const CONFIRMED           = 16;
    //store status the the user compoents that have been confirmed and now awaiting activation
    const AWAITINGACTIVATION = 32;
    //store ths status of the user components that have been activated
          
    const ACTIVATED          = 64;
    
    protected static $table = "list_perfomancecomponents";

     function __construct()
     {
          parent::__construct();
     }   
     /*
       Get a specific component settings defined in setup perfomance matrix
       @params integer @componentid - component id
       @return array  component settings
     */
     private static function getComponentCategory($componentid)
     {
        $componentCategories = array();
        $component           = Component::findById($componentid);
        if(!empty($component))
        {
           if(!empty($component['categories']))
           {                          
             $componentCategories = serializeDecode($component['categories']);
           }
        }
        return $componentCategories;
     }  
     /*
       Get the component's default settings defined under setup perfomance matrix
       @params integer $compoentid - specifies the component that we need the settings for
       @params array  $optionskeys - used to filter specific settings that we want to uses at that point 
       @return array - contains the component setting
     */
     public static function getComponentCategorySettings($componentid, $optionskeys = array())
     {
        $categorySettings      = self::getComponentCategory($componentid);
        $requestedCategoryKeys = array();
        if($categorySettings)
        {
           if(!empty($optionskeys))
           {
              foreach($optionskeys as $index => $key)
              {
                 if(isset($categorySettings[$key]))
                 {
                    $requestedCategoryKeys  = $categorySettings[$key];
                 }
              }
           } else {
               $requestedCategoryKeys = $categorySettings;
           }          
        }
        return $requestedCategoryKeys;        
     }
     
     /*
      Creates a user specific perfomance matrix , importing templates from setup, as defined by the perfomance matrix link to the user categories
      @params array $userdata , user information used to filter and create user specific perfomance matrix
      @return void
     */
     public static function setupInitialSettings($userdata)
     {        
        $matrix = Performancematrix::getASetting($userdata['categoryid'], $userdata['default_year'], array("usecat", "masterlist"));
        if(!empty($matrix))
        {
          $kpaid = 0;
          foreach($matrix['usecat'] as $catIndex => $cat)
          {
              //save the user components 
              $res = UserComponent::saveData(array('componentid'    => $catIndex, 
                                                   'userid'         => $userdata['tkid'],
                                                   'insertuser'     => $_SESSION['tid'],
                                                   'evaluationyear' => $userdata['default_year'],
                                                   'created'        => date('Y-m-d H:i:s') 
                                                   )
                                             );
              if($res > 0)
              {
                if(isset($matrix['masterlist'][$catIndex]) && !empty($matrix['masterlist'][$catIndex]))
                {
                    Kpa::copySave($catIndex, $userdata['default_year'], $userdata);
                }
              }
          }
          //update the user status that, the component, kpa, kpi and actions have been imported
          UserSetting::updateWhere(array("status" => $userdata['status'] + UserSetting::DONEINTIALSETUP) , array("user_id" => $userdata['tkid']));
        }         
     }     
     
     public static function copy($data)
     {
        $data['insertuser'] = $_SESSION['tid'];
        parent::setTable("copied_components");
        $res      = parent::saveData($data);
        $response = array();
        if($res > 0)
        {
          Component::copySaveComponent($data);
          Kpa::copySaveToNewYear($data['component_id'], $data['from_year'], $data['to_year']);
          $response = array("text" => "Component was copied successfully", "error" => false);
        } else {
          $response = array("text" => "error copying compoent", "error" => true);
        }
        return $response;
     }
     
     private static function copySaveComponent($data)
     {
        parent::setTable("list_perfomancecomponents");
        $component = Component::findById($data['component_id']);
        if(!empty($component))
        {
           unset($component['id']);
           $component['evaluationyear'] = $data['to_year'];
           $component['created']        = date('Y-m-d H:i:s');
           Component::saveData($component);
        }
     }
          
          
}
?>
