<?php
class UserMatrixPdf extends PDF_Tree
{
    
   private $user = array();
   
   private $userComponents = array();
    
   function __construct($userComponents)
   {
     parent::__construct();
     $this -> user = $userComponents['user'];
     $this -> userComponents = $userComponents['usercomponents'];
     //$this->MainHeader();
     
   } 
           
   function MainHeader()
   {
     $this->setFont("Arial", "B", 12);
     $this->setY(10);
     $this->Cell(0, 0, 'Perfomance Agreement', 0, 2, 'C');
         
	$this->setY(30);
	$this->Cell(0, 0, 'Of', '0', 2, 'C');         

     $this->Ln(20);
     $this->Cell(0, 0, $this->user['tkname']." ".$this->user['tksurname'], 0, 2, 'C');

     $this->Ln(20);     
     $this->Cell(0, 0, "Manager : ".$this->user['managername'], 0, 2, 'C');

     $this->Ln(20);              
     $this->Cell(0, 0, "Organisation Position : ".$this->user['orgposition'], 0, 2, 'C');

     $this->Ln(20);
     $this->Cell(0, 0, "Job Level : ".$this->user['joblevel']." (".$this->user['joblevelid'].")", 0, 2, 'C');

     $this->Ln(20);         
     $this->Cell(0, 0, "Job Title : ".$this->user['jobtitle'], 0, 2, 'C');

     $this->Ln(20);          
     $this->Cell(0, 0, "Evaluation Year : ".$this->user['start']." to ".$this->user['end'], 0, 2, 'C');

     $this->Ln(2);
   } 
   
   
   function Main()
   {
     $data = array(
			'Operating Systems'=>array(
									'Microsoft Windows'=>array(
															'3.1'=>'NotAvailable',
															'NT'=>'$120.00',
															'95'=>'$120.00',
															'98'=>'$120.00',
															'2000'=>array(
																		'Home'=>'$120.00',
																		'Professional'=>'$320.00',
																		'Server'=>'$1200.00'
																		),
															'ME'=>'NotAvailable',
															'XP'=>'NotAvailable'
															),
									'Linux'=>array(
												'Red Hat',
												'Debian',
												'Mandrake'
												),
									'FreeBSD',
									'AS400',
									'OS/2'
									),
			'Food'=>array(
						'Fruits'=>array(
										'Apple',
										'Pear'
									),
						'Vegetables'=>array(
										'Carot',
										'Salad',
										'Bean'
										),
						'Chicken',
						'Hamburger'
						)
			);
       foreach($this->userComponents as $compoentID => $component)
       {
           $this->setY(-5);
           $this->setFillColor(247, 247, 247);
           $this->setTextColor(0, 0, 255);
           $this->setLineWidth(0);
           $this->setFont('', 'B');
           $this->Cell(0, 15, $compoentID, 1, 1, 'C', true);
           
          // TREE 5
          //$this->SetMargins(5,0,5);
          //$this->SetAutoPageBreak(true,0);
          //$this->AddPage();
          $this->SetFont('Arial','',10);
          //$this->SetFillColor(150,150,150);
          $this->SetDrawColor(20,20,20);
          $this->SetTextColor(0,0,0);

          // TREE 1       
          $startX      = 15;
          $nodeFormat  = '%k';
          $childFormat = '%v';
          $w           = 30;
          $h           = 3;
          $border      = 0;
          $fill        = 1;
          $align       = 'L';
          $indent      = 18;
          $vspacing    = 4;
          $drawlines   = false;
          //$this->SetY(120);
          $this->MakeTree($component,$startX,$nodeFormat,$childFormat,$w,$h,$border,$fill,$align,$indent,$vspacing,$drawlines);           
           /*if(isset($component['kpa']) && !empty($component['kpa']))
           {
              $headers = $component['kpa']['columns'];
              foreach($component['kpa']['columns'] as $header => $head)
              {
                $this->setFillColor(0, 0, 128);
                $this->setTextColor(255);
                $this->setDrawColor(255);
                $this->setLineWidth(0.2);
                $this->setFont('', 'B');
                $this->Cell(40, 7, $head, 1, 0, 'C', true);               
              }
              unset($component['kpa']['columns']);
              foreach($component['kpa'] as $kpaId => $kpa)
              {
                if(is_array($kpa))
                {
                   foreach($kpa as $key => $val)
                   {     
                     $this->setFillColor(255);
                     $this->setTextColor(0);
                     $this->setDrawColor(255);
                     $this->setLineWidth(0.2);
                     $this->setFont('', '');
                     $this->Cell(40, 7, $val, 1, 0, 'C', false); 
                   }
                 $this->Ln();
                }
              }
              
           }*/
           $this->Ln();
       }
   }
   
   
   function Footer()
   {
     $this->setY(-15);
     
     $this->setFont('Arial', 'I', 8);
     
     $this->Cell(0, 10, 'For '.$this->user['tkname']." ".$this->user['tksurname'] , 0, 0, 'C');
   }
     

}
?>
