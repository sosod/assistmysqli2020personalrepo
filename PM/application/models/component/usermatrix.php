<?php
class UserMatrixPdf extends FPDF
{
           
   function Header()
   {
     $this->setFont("Arial", "B", 15);
     
     $this->Cell(80);
     
     $this->Cell(30, 20, "Title", 1, 0, "C");
     
     $this->Ln(20);
   } 
   
   function Footer()
   {
     $this->setY(-15);
     
     $this->setFont('Arial', 'I', 8);
     
     $this->Cell(0, 10, 'Page '.$this->pageNo().' /(nb)', 0, 0, 'C');
   }
     

}
?>
