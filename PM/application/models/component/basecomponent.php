<?php
abstract class BaseComponent extends Model
{
    protected static $table = "list_perfomancecomponents";
   
    const ACTIVE             = 1;
    
    const DELETED            = 2;
       
    const CONFIRMED          = 4;
    
    const ACTIVATED          = 8;     
     
     public function __construct()
     {
        parent::__construct();
     }     
     
     protected static function generateSectionSql($options = array())
     {
        $secSql = "";
         if(isset($options['section']))
         {
	     if($options['section'] == "manage")
	     {
	       $secSql .= " AND UC.status & ".UserComponent::ACTIVATED." = ".UserComponent::ACTIVATED."";
	     }
	     unset($options['section']);
         }
               
         return $secSql;
     }
     
     protected static function generatePageSql($options = array())
     {
        $pageSql  = "";
	   if(isset($options['page']))
	   {
	     if($options['page'] == "activate")
	     {
	       $pageSql .= " AND UC.status & ".UserComponent::CONFIRMED." = ".UserComponent::CONFIRMED." ";
	     }
	     if($options['page'] == "confirm")
	     {
	       $pageSql .= " AND UC.status & ".UserComponent::CONFIRMED." <> ".UserComponent::CONFIRMED." 
	                     AND UC.status & ".UserComponent::ACTIVATED." <> ".UserComponent::ACTIVATED." ";
	     }	  
	     if($options['page'] == "new")
	     {
	       $pageSql .= " AND UC.status & ".UserComponent::CONFIRMED." <> ".UserComponent::CONFIRMED." AND UC.status & ".UserComponent::ACTIVATED." <> ".UserComponent::ACTIVATED." ";
	     }	  	        	     
	     unset($options['page']);  
	   }
	   return $pageSql;
     }
}
?>
