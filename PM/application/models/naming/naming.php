<?php
class Naming extends BaseNaming
{
     const MANAGEPAGE = 4;
     
     const NEWPAGE = 8;
     
     protected static $table = "naming";     
     
     public function __construct()
     {
          parent::__construct();
     }
     
     public static function getHeaderList($section = "")
     {
         $sql = "";
         if(isset($section) && !empty($section))
         {
              if($section == "new")
              {
                $sql = " AND status & ".Naming::NEWPAGE." = ".Naming::NEWPAGE."";
              } else if($section == "manage"){
                $sql = " AND status & ".Naming::MANAGEPAGE." = ".Naming::MANAGEPAGE."";
              }
              $sql .= " AND type = 'kpa' OR type = 'component' ";          
         }
         $sql .= " ORDER BY position ";
         $results = self::findAll($sql);
         $headerlist = array();
         foreach($results as $index => $header)
         {
           $headerlist[$header['name']] = $header['client_terminology'];   
         }
         return $headerlist;
     }
     
     /*Does nothing, required for other audit logs, from other module to work as well since they use same code to display audit logs 
     TODO find a new way to get audit logs work independent of the module , without the need to declare functions like this ("_"); sad
     */
     function getHeaderNames()
     {
          
     }
     
     function setHeader($key)
     {
        $headers = self::getHeaderList();
        if(isset($headers[$key]))
        {
          $name = $headers[$key];
        } else {
          $name = ucfirst(str_replace("_", " ", $key));
        }
        return $name;
     }
} 
?>
