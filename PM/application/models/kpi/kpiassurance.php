<?php
class KpiAssurance extends Model
{
    
    static $table = "kpi_assurance";
    
    function __construct()
    {
        parent::__construct();
    }    
    
    function save_assurance($data)
    {
       $data['insertuser']  = $_SESSION['tid']; 
       $res                 = $this -> save($data);
       return $res;
    }
    
    function update_assurance($id, $data)
    {
       $res = $this -> update_where($data, array("id" => $id));
       return $res;
    }
            
    function get_assurances($option_sql = "")
    {
        $option_sql = "";
        $limit_sql  = "";
        if(isset($options['kpiid']))
        {
          $option_sql .= " AND K.kpi_id = '".$options['kpiid']."' ";
        }
        if(isset($options['limit']) && !empty($options['limit']))
        {
          $limit_sql = " LIMIT ".(int)$options['start'].",".(int)$options['limit'];
        }
        $results = $this -> db -> get("SELECT K.*, date_format(now(), '%d-%b-%Y %H:%i:%s') AS system_date,
                                       CONCAT(TK.tkname, ' ', TK.tksurname) AS assurance_by 
                                       FROM #_".static::$table." K
                                       INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = K.insertuser
                                       WHERE status & 1 = 1 $option_sql $limit_sql
                                      ");
        $kpa_assurances = array();                                      
        foreach($results as $r_index => $result)
        {
            $kpa_assurances[$result['id']]  = $result;
            if(isset($result['attachment']) && !empty($result['attachment']))
            {
              $kpa_assurances[$result['id']]['attachment'] = Attachment::getAtachmentsList($result['attachment'], 'kpi');
            }
        }
        return $kpa_assurances;       
    }
    
    function get_assurance($id)
    {
        $result = $this -> db -> getRow("SELECT K.*, date_format(now(), '%d-%b-%Y %H:%i:%s') AS system_date,
                                         CONCAT(TK.tkname, ' ', TK.tksurname) AS assurance_by 
                                         FROM #_".static::$table." K
                                         INNER JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = K.insertuser
                                         WHERE id = '".$id."' 
                                       ");
        return $result;    
    }    
    
    
}
?>
