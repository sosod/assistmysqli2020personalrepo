<?php
class Kpi extends Model
{

    const ACTIVE = 1;
    
    const DELETED  = 2;
    
    const KPIACTION_IMPORTED = 4;
    
    const KPIACTION_CREATED  = 8;
    
    const UNSPECIFIED_KPI  = 16;
    
    const COPIED           = 32;
    
    protected static $table = "kpi";
    
    protected static $actionInfo;
    
    private $self_evaluation     = 0;
    
    private $manager_evaluation  = 0;
    
    private $joint_evaluation    = 0;
    
    private $evaluation_review   = 0; 
	
	function __construct()
	{
		parent::__construct();
	}

     function get_kpa_kpi($userdata, $setting, $actionObj, $kpimappingObj, $options = array())
     {
        $option_sql = "";
        if($options['section'] != "admin")
        {
          $option_sql = " AND K.owner = '".$userdata['tkid']."' ";
        }

        $sql          = "SELECT K.* FROM #_".static::$table." K WHERE 
                         K.kpaid = '".$options['kpaid']."' AND kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED." $option_sql ";
        $results      = $this -> db -> get($sql);
        $kpi_ids      = array();
        $kpi_actions  = array();
        $total_kpi    = 0;
        $kpi_progress = 0;
        $avg_progress = 0;
        $kpi_change   = 0;
        $action_evaluations        = array();
        $module_action_evaluations = array();
        if($options['section'] == 'new' || $options['options'] == 'confirm')
        {
            $kpi_change = $this -> _is_kpi_template_changed($results, $userdata, $options);
        }
        //debug($kpi_change);
        if(!empty($results))
        {
          foreach($results as $index => $kpi)
          {                 
             if(isset($setting['evaluate_on']) && $setting['evaluate_on'] == "kpi")
             {
                //if(isset($options['page']) && $options['page'] == "evaluate")
                //{
                if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
                {
                   $kpi_evaluations = $this -> get_evaluations($kpi['id'], $setting, $options['componentid'], $userdata['default_year']);
                }
                //}  
             }
             $kpi_ids[$kpi['id']] = $kpi['kpistatus'];
             $total_kpi          += 1;
	        if(($kpi['kpistatus'] & Kpi::KPIACTION_IMPORTED) == Kpi::KPIACTION_IMPORTED)
	        {
	          //$ids[$kpi['id']] = $kpi['kpistatus'];
	          $moduleActions             = $kpimappingObj -> getModuleActions($kpi, $kpi['modulemapping'], $userdata, $setting);
	         // debug($moduleActions);
	          $module_action_evaluations = $moduleActions['action_evaluations'];
	          if(empty($moduleActions['ids']))
	          {
	             $kpi_progress           += $moduleActions['avg_progress'];
	          }
	          $kpi_actions[$kpi['id']] = $moduleActions['ids'];
	        } else {
	          //$ids[$kpi['id']]        = $kpi['kpistatus'];
	          //self::_actionInfo($kpi);	  
	          $actions                   = $actionObj -> get_kpi_actions($kpi['id'], $setting);
	          $action_evaluations        = $actions['action_evaluations'];
	          if(!empty($actions['ids']))
	          {
	             $kpi_actions[$kpi['id']] = $actions;
	             $kpi_progress           += $actions['avg_progress'];
	          }
	        }
          }
        }
        if(isset($module_action_evaluations) && !empty($module_action_evaluations))
        {
           $this -> _module_action_evaluations($module_action_evaluations);
        }          
        $kpi['kpi_evaluations']    = $this -> _get_total_next_evaluations();
        $kpi['action_evaluations'] = $action_evaluations;
        if($total_kpi != 0)
        {
           $avg_progress = round(($kpi_progress/$total_kpi), 2);
        }
        $kpi['progress']   = $avg_progress;
        $kpi['actionInfo'] = $kpi_actions;
        $kpi['ids']        = $kpi_ids;     
        if(!empty($kpi_change))
        {
          $kpi['kpi_change'] = $kpi_change;
        }
        return $kpi;
     }
     
     function _module_action_evaluations($module_actions)
     {
         if($module_actions['self_evaluation'])
         {
           $this -> self_evaluation += 1; 
         } else if($module_actions['manager_evaluation']) {
           $this -> manager_evaluation += 1;
         } else if($module_actions['joint_evaluation']) {
           $this -> joint_evaluation += 1;
         } else if($module_actions['evaluation_review']) {
           $this -> evaluation_review += 1;
         } 
     }
     
     function _get_total_next_evaluations()
     {
         $evaluations = array();
         if($this -> self_evaluation != 0)
         {
           $evaluations['next']  = "Self Evaluation"; 
           $evaluations['total'] = $this -> self_evaluation;
         } else if($this -> manager_evaluation != 0) {
           $evaluations['next']   = "Manager Evaluation";
           $evaluations['total']  = $this-> manager_evaluation;
         } else if($this -> joint_evaluation != 0) {
           $evaluations['next']  = "Joint Evaluation";
           $evaluations['total'] = $this -> joint_evaluation;
         } else if($this -> evaluation_review != 0) {
           $evaluations['next']  = "Evaluation Review";
           $evaluations['total'] = $this -> evaluation_review;
         }     
        return $evaluations;
     }
     
     function get_evaluations($kpi_id, $setting, $component_id, $evaluation_year)
     {
        if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
        {
          $results = $this -> db -> get("SELECT * FROM #_kpi_evaluations KE INNER JOIN #_kpi K ON KE.kpi_id = K.id
                                       INNER JOIN #_kpa KPA ON KPA.id = K.kpaid 
                                       WHERE KE.kpi_id = '".$kpi_id."' 
                                       AND KE.evaluation_period_id = '".$_SESSION['evaluation_period']['id']."'
                                     ");
            if(!empty($results))
            {
              
            } else {
              if($setting['self_evaluation'])
              {
                $this -> self_evaluation += 1; 
              } else if($setting['manager_evaluation']) {
                $this -> manager_evaluation += 1;
              } else if($setting['joint_evaluation']) {
                $this -> joint_evaluation += 1;
              } else if($setting['evaluation_review']) {
                $this -> evaluation_review += 1;
              }
            }                          
        }                            
     }
     
     function get_kpi($userdata, $options)
     {
	   $headers    = array();
        $option_sql = "";
	   
	   $settingObj = new Performancematrix();
	   $setting    = $settingObj -> get_matrix_setting($userdata['categoryid'], $userdata['default_year'], $options['componentid'], 
	                                                        array('evaluate_on', 'use_component, self_evaluation',
	                                                               'manager_evaluation', 'joint_evaluation', 'evaluation_review'
	                                                              )
	                                                   );        
        
        if(isset($options['headers']) && !empty($options['headers']))
        {
           $headers = $options['headers'];
           unset($options['headers']);
        }	            
        if($options['section'] != "admin")
        {
          $option_sql = " AND K.owner = '".toUserId($options['user'])."' AND PM.category_id = '".$userdata['categoryid']."' ";
        }
        $kpi_evaluations = array();
	   //if evaluation on kpa, then get all the kpa that are in the evaluation period
	   if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
	   {
	      if($setting['evaluate_on'] == "kpi")
	      {
	          /*$option_sql .= " AND STR_TO_DATE(K.deadline, '%d-%M-%Y') 
	                             BETWEEN '".date("Y-m-d", strtotime($_SESSION['evaluation_period']['start_date']))."' 
	                             AND '".date("Y-m-d", strtotime($_SESSION['evaluation_period']['end_date']))."'
	                           ";*/
	             $kpi_evaluations = $this -> _get_kpi_evaluations($setting, $options['componentid'], $userdata['default_year']);
	      }
	   } 
        $sql =  "SELECT KE.*, K.*, PM.evaluate_on, PM.use_component, PM.self_evaluation, PM.manager_evaluation, 
                 PM.joint_evaluation, PM.evaluation_review
                 FROM #_".static::$table." K INNER JOIN #_kpa KPA ON K.kpaid = KPA.id 
                 LEFT JOIN #_matrix PM ON PM.component_id = KPA.componentid 
                 LEFT JOIN #_kpi_evaluations KE ON KE.kpi_id = K.id WHERE K.kpaid = '".$options['kpaid']."' 
                 $option_sql
                ";
        $results = $this -> db -> get($sql);
        $data    = KpiSorter::sort_kpi($results, array('user'         => $userdata,               'headers'  => $headers,
                                                        'componentid' => $options['componentid'], 'section'  => $options['section'],
                                                        'evaluations' => $kpi_evaluations,         'setting' => $setting
                                                    )
                                       );
        return $data;
     }
     
	function _get_kpi_evaluations($setting, $componentid, $evaluationyear)
	{
	   $results = $this -> db -> get("SELECT E.* FROM #_kpa_evaluations E 
	                                  INNER JOIN #_kpa K ON K.id = E.kpa_id
	                                  WHERE E.evaluation_period_id = '".$_SESSION['evaluation_period']['id']."'
	                                  AND K.componentid = '".$componentid."' AND K.evaluationyear = '".$evaluationyear."'
	                                ");   
	   return $results;                              
	}     
     	
	function get_kpi_evaluations($userdata, $componentid)
	{
	   $results    = array();
	   $self_sql   = "SELECT MAX(KE.self_date) AS self_date FROM #_kpi K LEFT JOIN #_kpi_evaluations KE ON K.id = KE.kpi_id 
	                  INNER JOIN #_kpa KPA ON KPA.id = K.kpaid WHERE K.owner = '".$userdata['tkid']."' AND KPA.componentid = '".$componentid."'
	                  HAVING MAX(KE.self_date) ORDER BY KE.self_date LIMIT 0,1
	                  ";
	   $self_results = $this -> db -> getRow($self_sql);
	   if(!empty($self_results))
	   {
	      $results['self_evaluation'] = $self_results['self_date'];
	   }
	   
	   $manager_sql   = "SELECT MAX(KE.manager_date) AS manager_date FROM #_kpi K LEFT JOIN #_kpi_evaluations KE ON K.id = KE.kpi_id 
	                  INNER JOIN #_kpa KPA ON KPA.id = K.kpaid WHERE K.owner = '".$userdata['tkid']."' AND KPA.componentid = '".$componentid."'
	                  HAVING MAX(KE.manager_date) ORDER BY KE.manager_date LIMIT 0,1
	                  ";
        $manager_results = $this -> db -> getRow($manager_sql);
	   if(!empty($manager_results))
	   {
	      $results['manager_evaluation'] = $manager_results['manager_date'];
	   }
	   
	   $joint_sql    = "SELECT MAX(KE.joint_date) AS joint_date FROM #_kpi K LEFT JOIN #_kpi_evaluations KE ON K.id = KE.kpi_id 
	                    INNER JOIN #_kpa KPA ON KPA.id = K.kpaid WHERE K.owner = '".$userdata['tkid']."' AND KPA.componentid = '".$componentid."'
	                    HAVING MAX(KE.joint_date) ORDER BY KE.joint_date LIMIT 0,1
	                   ";
        $join_results = $this -> db -> getRow($joint_sql);        
	   if(!empty($joint_results))
	   {
	      $results['joint_evaluation'] = $joint_results['joint_date'];
	   }        
	   $review_sql    = "SELECT MAX(KE.review_date) AS review_date FROM #_kpi K LEFT JOIN #_kpi_evaluations KE ON K.id = KE.kpi_id 
	                     INNER JOIN #_kpa KPA ON KPA.id = K.kpaid WHERE K.owner = '".$userdata['tkid']."' AND KPA.componentid = '".$componentid."'
	                     HAVING MAX(KE.review_date) ORDER BY KE.review_date
	                    ";
        $review_results = $this -> db -> getRow($review_sql);                
	   if(!empty($joint_results))
	   {
	      $results['evaluation_review'] = $review_results['review_date'];
	   }	   
        return $results;
	}
     
	private function _is_kpi_template_changed($current_kpi, $user, $options)
	{
	   $option_sql     = " AND kpaid = '".$options['kparef']."' AND kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED." ";
	   $kpi_templates  = $this -> db -> get("SELECT * FROM #_kpi WHERE 1 $option_sql");
        $response       = array();
	   if(!empty($current_kpi))
	   {	     
	      //debug($current_kpi);
	      foreach($current_kpi as $index => $kpi)
	      {
	         $template_kpi_ref         = $kpi['kpiref'];
	         $kpi_id                   = $kpi['id'];
	         if(isset($kpi_templates[$template_kpi_ref]))
	         {
	            $current_kpi_timestamp  = strtotime($kpi['modified']);
	            $template_kpi_timestamp = strtotime($kpi_templates[$template_kpi_ref]['modified']);
	            //there was an update to the template data, since the initial setup of the user kpas
	            if($template_kpi_timestamp > $current_kpi_timestamp)
	            {
	               unset($kpi['owner']);
	               unset($kpi['modified']);
	               unset($kpi['created']);
	               unset($kpi['kpistatus']);
	               unset($kpi['kpaid']);
	               unset($kpi['id']);
	               unset($kpi['kpiref']);
	               
	               unset($kpi_templates[$template_kpi_ref]['owner']);
	               unset($kpi_templates[$template_kpi_ref]['modified']);
	               unset($kpi_templates[$template_kpi_ref]['created']);
	               unset($kpi_templates[$template_kpi_ref]['kpistatus']);	               	               
	               unset($kpi_templates[$template_kpi_ref]['kpaid']);
	               unset($kpi_templates[$template_kpi_ref]['id']);
	               unset($kpi_templates[$template_kpi_ref]['kpiref']);
	               
                    $diff = array_diff($kpi_templates[$template_kpi_ref], $kpi); 
                    if(!empty($diff))
                    {          	               
	                 $response['kpi_updated'][$template_kpi_ref][$kpi_id] = $kpi_id;
	               }
	            } 
	            unset($kpi_templates[$template_kpi_ref]);
	         }
	      }
	      if(isset($kpi_templates) && !empty($kpi_templates))
	      {
	        foreach($kpi_templates as $t_index => $template)
	        {
	          $response['kpi_added'][$options['kparef']] = array('kpaid' => $options['kpaid'], 'template_kpiid' => $template['id']);
	        }	          
              //$response['kpi_added'] = $kpi_templates;
	      }
	      return $response;
	   } else {	     
	     if(!empty($kpi_templates))
	     {
	        foreach($kpi_templates as $t_index => $template)
	        {
	          $response['kpi_added'][$options['kparef']] = array('kpaid' => $options['kpaid'], 'template_kpiid' => $template['id']);
	        }
	        return $response;
	     } 
	     return FALSE;
	   }
     }
     
     function copy_from_template_save($kpi_added, $userdata)
     {
        $res = 0;
        foreach($kpi_added as $kparef => $kpi)
        {        
          $kpi_data = $this -> db -> getRow("SELECT name, objective, outcome, measurement, proof_of_evidence, baseline, targetunit, nationalkpa,           
                                             idpobjectives, performance_standard, target_period, weighting, competency, proficiency,
                                             categories, comments, deadline FROM #_kpi WHERE id = '".$kpi['template_kpiid']."' 
                                           ");                                
          $kpi_data['kpiref']         = $kpi['template_kpiid'];
          $kpi_data['kpaid']          = $kpi['kpaid'];
          $kpi_data['owner']          = $userdata['tkid'];
          $kpi_data['evaluationyear'] = $userdata['default_year'];
          $kpi_data['created']        = date('Y-m-d H:i:s');
          $kpi_data['kpistatus']      = 1;
          $this -> save($kpi_data);
        }
        return $res;          
     }
     
     function update_kpi_template_changes($kpi_updated)
     {
       $res = 0; 
       foreach($kpi_updated as $template_id => $kpi)
       {
            $template  = $this -> db -> getRow("SELECT name, objective, outcome, measurement, proof_of_evidence, baseline, targetunit, nationalkpa,
                                                idpobjectives, performance_standard, target_period,weighting, competency, proficiency, categories,
                                                comments FROM #_kpi WHERE id = '".$template_id."'
                                               ");
             $this -> attach(new KpiAuditLog());
             $res += $this -> update_where($template, array('kpiref' => $template_id)); 
       }
       return $res;
     }
     
	function totalKpi($id)
	{
         $stats = $this -> db -> getRow("SELECT COUNT(*) AS total FROM  #_kpi WHERE kpaid = {$id}");
	    return $stats;
	}
	
	public static function copySave($kpaid, $newkpaid, $evaluationyear, $userdata)
	{
	   $kpis = Kpi::findAll(" AND kpaid = '".$kpaid."' AND kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED." ");
	   if(!empty($kpis))
	   {
	      foreach($kpis as $kpiIndex => $kpi)
	      {
	          $kpiid          = $kpi['id'];
	          unset($kpi['id']);
	          $kpi['kpaid']   = $newkpaid;
	          $kpi['owner']   = $userdata['tkid'];
	          $kpi['kpiref']  = $kpiid;
	          $kpi['created'] = date('Y-m-d H:i:s');
	          $newkpiid       = Kpi::saveData($kpi);
	          if($newkpiid > 0)
	          {
	              Action::copySave($kpiid, $newkpiid, $evaluationyear, $userdata);
	              //echo "Kpis saved so far have id ".($newkpiid)."<br />";
	          }
	      }
	   }
	}
	
	public static function copySaveToNewYear($kpaid, $newkpaid, $toYear)
	{
	   $kpis = Kpi::findAll(" AND kpaid = '".$kpaid."' AND kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED."  ");
	   if(!empty($kpis))
	   {
	      foreach($kpis as $kpiIndex => $kpi)
	      {
	          $kpiid                 = $kpi['id'];
	          unset($kpi['id']);
	          $kpi['kpaid']          = $newkpaid;
	          $kpi['evaluationyear'] = $toYear;
	          $kpi['kpiref']         = $kpiid;
	          $kpi['kpistatus']      = Kpi::ACTIVE + Kpi::COPIED;
	          $kpi['created']        = date('Y-m-d H:i:s');
	          $newkpiid              = Kpi::saveData($kpi);
	          if($newkpiid > 0)
	          {
	              Action::copySaveToNewYear($kpiid, $newkpiid, $toYear);
	              //echo "Kpis saved so far have id ".($newkpiid)."<br />";
	          }
	      }
	   }
	}	
	
	public static function getKpi($options= array())
	{
		$section = "";	
		$compdata = Component::getComponentCategorySettings($options['componentid'], array("kpi"));

		unset($options['componentid']);
		unset($options['user']);
		if(isset($options['section']))
		{
		   $section = $options['section'];
		   unset($options['section']);
		}	
		$headers = array();
		if(isset($options['headers']) && !empty($options['headers']))
		{
		   $headers = $options['headers'];
		   unset($options['headers']);
		}
		$sqlOption   = parent::createANDSqlOptions($options);
		$sqlOption  .= " AND kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED." ";
		$results     = Kpi::findAll($sqlOption);
		$kpiStatuses = self::_getKPIStatuses($results);
		$response    = Sorter::sortHeaders($results, array("component" => $compdata ,
		                                                   "type"      => "kpi",
		                                                   "headers"   => $headers,
		                                                   "section" => $section)
		                                   );		
		$kpiData   = array("data"       => $response['data'],
		                  "cols"       => $response['cols'],
		                  "headers"    => $response['columns'],
		                  "total"      => $response['total'],
		                  "actionInfo" => self::$actionInfo,
		                  "kpiStatus"  => $response['statuses'],
		                 );
		return $kpiData;	
	} 
	
	function getKpaKpi($user, $componentCategories, $options)
	{
        unset($options['componentid']);
        unset($options['page']);
        if(isset($options['section']))
        {
          $section = $options['section'];
          unset($options['section']);
        }
        $sqlOption    = parent::createANDSqlOptions($options);	
        $sqlOption   .= " AND kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED." ";
        $headers      = array();
	   if(isset($options['headers']) && !empty($options['headers']))
	   {
		 $headers = $options['headers'];
		 unset($options['headers']);
	   }
	   $results    = Kpi::findAll($sqlOption);
	   $response   = Sorter::sortHeaders($results, array("component"=>$componentCategories, "type"=>"kpi", "headers"=>$headers, "section"=>$section));
	   $dataResult = array();
	   if(isset($response['data']) && !empty($response['data']))
	   {
	     //$dataResult  = $response['data'];
	     foreach($response['data'] as $kpiId => $kpi)
	     {
	        $dataResult[$kpi['name']] = array();
	        $options['kpiid']         = $kpiId;
	        $dataResult[$kpi['name']] = Action::getkpiActions($user, $componentCategories, $options);
	        //$dataResult[$kpiId]['actions'] = Action::getkpiActions($user, $componentCategories, $options);
	     }
	   }
	   return $dataResult;
	}
	
	private static function _getKPIStatuses($kpis)
	{
	    $statusData = array();
	    foreach($kpis as $index => $val)
	    {
	        $statusData[$val['id']] = $val['kpistatus']; 
	    }
	    return $statusData;
	}	
	
	private static function _actionInfo($kpiData)
	{
	   $actionObj = new Action();
	   foreach( $kpiData as $index => $kpi)
	   {
		$sqlOption  = " AND kpiid = '".$index."' AND actionstatus & ".Action::DELETED." <> ".Action::DELETED." ";
		$kpiActions = $actionObj -> fetchAll($sqlOption);
		if(!empty($kpiActions) )
		{
		   foreach( $kpiActions as $aId => $action)
		   {
			 self::$actionInfo[$index][$action['id']] = $action['id'];
		   }
		}
	  }
    }	
    
    public static function calculateKpiProgress($id, $kpiStatus, $userdata)
    {
      if(($kpiStatus & Kpi::KPIACTION_IMPORTED) == Kpi::KPIACTION_IMPORTED)
      {
          $kpi      = Kpi::findById($id);
          $progress = KpiMapping::getActionsProgress($kpi, $userdata);
          return round($progress['avgProgress'], 2);
      } else {
        $sqlOption   = " AND A.kpiid = '".$id."' AND actionstatus & ".Action::DELETED." <> ".Action::DELETED."  ";
        $actionStats = Action::getActionProgressStats($sqlOption);
        return round($actionStats['averageProgress'], 2);
      }
    } 
    
    public static function createUnspecifiedKpi($kpaid, $userid, $evaluationyear, $deadline)
    {
	   $kpi = array();
	   $kpi['kpaid']                = $kpaid;
	   $kpi['kpiref']               = "";
	   $kpi['name']                 = "Unspecified";
	   $kpi['objective']            = "Unspecified";
	   $kpi['outcome']	            = "Unspecified";
	   $kpi['measurement']          = "Unspecified";
	   $kpi['owner']                = $userid;
	   $kpi['proof_of_evidence']    = "Unspecified";
	   $kpi['baseline']	            = "Unspecified";
	   $kpi['targetunit']           = "Unspecified";
	   $kpi['nationalkpa']          = 0;
	   $kpi['idpobjectives']	       = 0;
	   $kpi['performance_standard'] = "Unspecified";
	   $kpi['target_period']        = "Unspecified"; 
	   $kpi['weighting']            = 0;
	   $kpi['competency']           = 0;
	   $kpi['proficiency']	       = 0;
	   $kpi['categories']           = 0;
	   $kpi['deadline']	            = $evaluationyear['end']; 
	   $kpi['remindon']	            = "";
	   $kpi['comments']	            = "";
	   $kpi['status']               = 1;
	   $kpi['kpistatus']            = 1 + Kpi::UNSPECIFIED_KPI;
	   $kpi['evaluationyear']       = $evaluationyear;
	   $kpi['insertuser']           = $_SESSION['tid'];
	   $kpi['created']              = date('Y-m-d H:i:s');
	   $res                         = Kpi::saveData($kpi);
    }
    
     function generateReport($postData, Report $reportObj)
	{
	   $joinWhere = $this -> createJoins($postData['value']);
	   $includeprogress = false;
	   if(isset($postData['kpi']['progress']))
	   {
	     $includeprogress = true;
	     unset($postData['kpi']['progress']);
	   }
	   if(isset($postData['value']['evaluationyear']))
	   {
	     $whereStr =  " AND KPA.evaluationyear = '".$postData['value']['evaluationyear']."' ";
	     unset($postData['value']['evaluationyear']);
	   }	   
	   $fields    = $reportObj -> prepareFields($postData['kpi'], "K");
	   $headers   = array();
	   foreach($postData['kpi'] as $field => $val)
	   {
	     $headers[] = $field;
	   }
	   if($includeprogress)
	   {
	     array_push($headers, "progress");
	   }
	   $groupBy    = $reportObj -> prepareGroupBy($postData['group_by'], "K"); 
	   $sortBy     = $reportObj -> prepareSortBy($postData['sort'], "K");
	   $whereStr  .= $reportObj -> prepareWhereString($postData['value'], $postData['match'], "K");   
	   $whereStr  .= $joinWhere;
	   $results    = $this -> filterResults($fields, $whereStr, $groupBy, $sortBy, "");
        $response   = KpiSorter::sort_kpi($results, array("type" => "kpi", "headers" => $headers));		
        $response['title'] = $postData['report_title'];
        $reportObj -> displayReport($response, "on_screen", "data");	   
	   exit();
	}
     
     function filterResults($fields, $values = "", $groupBy = "", $sortBy = "", $joins = "")
     {
       $orderBy = (!empty($sortBy) ? " ORDER BY $sortBy " : "");
       $groupBy = (!empty($groupBy) ? " GROUP BY $groupBy " : "");
	  $results = $this -> db -> get("SELECT K.id, kpistatus, ".$fields." FROM #_kpi K
	                                 INNER JOIN #_kpa KPA ON KPA.id = K.kpaid
	                                 INNER JOIN #_user_component UC ON UC.componentid = KPA.componentid
	                                 $joins
				                  WHERE K.owner <> '' AND K.kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED."
				                  AND UC.status & ".UserComponent::ACTIVATED." = ".UserComponent::ACTIVATED."
				                  $values
				                  $groupBy
				                  $orderBy 
				                ");
	  return $results;	          
     }  	
	
	function createJoins($options = array())
	{
        $joinStr  = "";
        $whereStr = "";	   
        if(isset($options['nationalkpa']) && !empty($options['nationalkpa']))
        {
           $natStr = "";
           foreach($options['nationalkpa'] as $nkpaIndex => $nationalkpa)
           {
              $natStr .= "  K.nationalkpa = '".$nationalkpa."' OR";
           }  
           if(!empty($natStr))
           {
             $whereStr .= " AND (".rtrim($natStr, 'OR').") ";
           }
        }
        if(isset($options['idpobjectives']) && !empty($options['idpobjectives']))
        {
           $idpStr = "";
           foreach($options['idpobjectives'] as $idpIndex => $idp)
           {
              $idpStr .= " K.idpobjectives = '".$idp."' OR";
           }  
           if(!empty($idpStr))
           {
             $whereStr .= " AND (".rtrim($idpStr, 'OR').") ";
           }
        }
        if(isset($options['weighting']) && !empty($options['weighting']))
        {
           $weightStr = "";
           foreach($options['weighting'] as $weigtIndex => $weighting)
           {
              $weightStr .= "  K.weighting = '".$weighting."' OR";
           } 
           if(!empty($weightStr))
           {
              $whereStr .= " AND (".rtrim($weightStr, 'OR').") ";
           }
        }  
        if(isset($options['owner']) && !empty($options['owner']))
        {
           $userStr = "";
           foreach($options['owner'] as $ownerIndex => $owner)
           {
              $userStr .= "  K.owner = '".$owner."' OR";
           }  
           if(!empty($userStr))
           {
              $whereStr .= " AND (".rtrim($userStr, "OR").") ";
           } 
        }          
       return $whereStr;          
	}  
	
	public static function deleteKpaKpis($kpis)
	{
	   foreach($kpis as $index => $kpi)
	   {
	      $res = Kpi::updateWhere(array("kpistatus" => 2), array("id" => $kpi['id']));
	      if($res > 0)
	      {
	         $actions = Action::findAllByKpiid($kpi['id']);         
	         if(!empty($actions))
	         {
	            Action::deleteKpiActions($actions);
	         }
	      }
	   }
	}
}
