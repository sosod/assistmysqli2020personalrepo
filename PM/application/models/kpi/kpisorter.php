<?php
class KpiSorter extends Sorter
{
     static $next_evaluation   = "";
     
     static $evaluation_status = array();
     
     function __construct()
     {
        parent::__construct();
     }     
     
     public static function sort_kpi($data, $options)
     {
        $headers = array();
        $section = (isset($options['section']) ? $options['section'] : "");
        if(isset($options['headers']) && !empty($options['headers']))
        {
           $headerNames = Naming::getHeaderList();
           foreach($options['headers'] as $hIndex => $hval)
           {
               $headers[$hval] = (isset($headerNames[$hval]) ? $headerNames[$hval] : "");   
           }
        } else {
          $headerNames = Naming::getHeaderList($section);
          $headers = $headerNames;
        }   
        $committeeObj             = new EvaluationCommittee();
        $weighting                = Weighting::findList();
        $proficiency              = Proficiency::findList();        
        $nationalkpa              = Nationalkpa::findList();
        $nationalobjectives       = Nationalobjectives::findList();        
        $competency               = Competency::findList(); 
        $userObj                  = new User();        
        $statusObj                = new KpiStatus();
        $statuses                 = $statusObj -> findList();
        $userdata                 = (isset($options['user']) ? $options['user'] : array());
        $users_list               = $userObj -> get_users_list();
        $user_logged              = $userObj -> get_user();
        $datalist['headers']      = array();
        $datalist['data']         = array();
        $datalist['statuses']     = array();
        $datalist['settings']     = array();
        $datalist['userdata']     = $userdata;
        $datalist['users']        = $user_list;
        $datalist['userlogged']   = $user_logged;
        $datalist['in_committee'] = $committeeObj -> is_user_in_evaluation_committee($user_logged['default_year']);
        if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
        {
           $datalist['userlogged']['evaluationperiod']   = $_SESSION['evaluation_period']['open_from']." to ".$_SESSION['evaluation_period']['open_to'];
        }
        $kpiObj                   = new Kpi(); 
        $ratingObj                = new Ratingscale();
        $rating                   = $ratingObj -> getRatingInfo();    
        $evaluations              = $options['evaluations'];
        $setting                  = $options['setting'];      
        $kpiEvaluationObj         = new KpiEvaluation();  
        foreach($data as $kpi_index => $kpi)
        {          
           $kpi_id                               = $kpi['id'];        
           $datalist['statuses'][$kpi_id]        = $kpi['kpistatus'];
           $datalist['settings']['evaluate_on']  = $kpi['evaluate_on'];
           $evaluations                          = array('self'     => $setting['self_evaluation'],
                                                          'manager' => $setting['manager_evaluation'],
                                                          'joint'   => $setting['joint_evaluation'],
                                                          'review'  => $setting['evaluation_review']
                                                        );                                     
           $kpievals                             = $kpiEvaluationObj -> get_evaluation_data($kpi_id, $setting, $rating);
           $datalist['settings']['evaluation_type']             = $evaluations;      
           $datalist['settings']['next_evaluation'][$kpi_id]    = $kpievals['next_evaluation'];
           $datalist['settings']['evaluations'][$kpi_id]        = $kpievals['evaluations'];
           $datalist['settings']['evaluationstatuses'][$kpi_id] = $kpievals['statuses'];       
           foreach($headers as $key => $header)
           {     
              if($key == 'progress')
              {
                 $datalist['data'][$kpi_id][$key]  = self::calculateProgress('kpi', $kpi_id, $datalist['statuses'][$kpi_id], $userdata);   
                 $datalist['headers'][$key]  = $header;
              }               
              if(isset($kpi[$key]))
              {
                 $value                      = $kpi[$key];
                 $datalist['headers'][$key]  = $header;
                if($key == "weighting") 
                { 
                    $datalist['data'][$kpi_id][$key]  = "<em>".(isset($weighting[$value]) ? $weighting[$value] : '')."</em>";                         
                 } else if($key == "proficiency") {
                    $datalist['data'][$kpi_id][$key]  = "<em>".(isset($proficiency[$value]) ? $proficiency[$value] : '')."</em>";
                 } else if($key == "competency") {
                    $datalist['data'][$kpi_id][$key]  = (isset($competency[$value]) ? $competency[$value] : '');
                 } else if($key == "nationalkpa") {
                    $datalist['data'][$kpi_id][$key] = (isset($nationalkpa[$value]) ? $nationalkpa[$value] : '');
                 } else if($key == "idpobjectives") {
                    $datalist['data'][$kpi_id][$key] = (isset($nationalobjectives[$value]) ? $nationalobjectives[$value] : '');
                 } else if($key == "owner") {
                    $datalist['data'][$kpi_id][$key] = (isset($users_list[$value]) ? $users_list[$value] : '');
                 } else {
                    $datalist['data'][$kpi_id][$key] = $value;
                 }                            
              }           
           }      
        }     
        if($setting['evaluate_on'] == "kpi")
        {
           $total_to_evaluate = array();
           $next_overall      = "";
           $count             = 0;
           $overall_str       = "";       
           foreach($datalist['settings']['next_evaluation'] as $kpi_id => $evaluationtype)
           {
             if($evaluationtype == "self")
             {
               $next_overall = "self";
               $overall_str  = "Self Evaluation";
               $count++;
             } else if($evaluationtype == "manager") {
               if(empty($next_overall))
               {
                  $next_overall = "manager";
                  $overall_str  = "Manager Evaluation";
                  $count++;
               }
             } else if($evaluationtype == "joint") {
               if(empty($next_overall))
               {
                  $next_overall = "joint";
                  $overall_str  = "Joint Evaluation";
                  $count++;
               }        
             } else if($evaluationtype == "review") {
               if(empty($next_overall))
               {
                  $next_overall = "review";
                  $overall_str  = "Evaluation Review";
                  $count++;
               }          
             }
             $total_to_evaluate[$evaluationtype][] = $kpi_id; 
           }
           //$_SESSION[$_SESSION['modref']][$userdata['tkid']]['total_kpa_to_evaluate'] = $count;
           //$_SESSION[$_SESSION['modref']][$userdata['tkid']]['overall_kpa_next']      = $next_overall;
        }
        $datalist['overall_next']      = $next_overall;
        $datalist['overall_str']       = $overall_str;
        $datalist['total_to_evaluate'] = $count;        
        $datalist['cols']              = count($datalist['headers']);           
        return $datalist;
     }
     
     protected static function get_next_evaluation($kpaid, $evaluations, $kpi_evaluations)
     {
         $next_evaluation   = "";
         $evaluation_status = array();
         foreach($evaluations as $key => $status)
         {
           if($status == 1)
           {
               if(isset($kpi_evaluations[$key]))
               {
                 if(empty($kpi_evaluations[$key]['rating']) || empty($kpi_evaluations[$key]['comment']) 
                    || empty($kpi_evaluations[$key]['recommendation']))
                 {
                    if(empty($next_evaluation))
                    {
                       $next_evaluation               = $key;
                       self::$evaluation_status[$key] = array("status" => "ui-icon ui-icon-arrow-1-e", "name" => $status);
                    } else {
                       self::$evaluation_status[$key] = array("status" => "", "name" => $status);  
                    }
                 } else {
                    self::$evaluation_status[$key] = array("status" => "ui-icon ui-icon-check", "name" => $status);
                 }
               }
           } else {
               self::$evaluation_status[$key] = array("status" => "ui-icon ui-icon-close", "name" => $status);
           }
         }
         return $next_evaluation;
     }
     
     public static function component_kpi($data, $userdata, $headers)
     {

        $committeeObj              = new EvaluationCommittee();
        $weighting                 = Weighting::findList();
        $proficiency               = Proficiency::findList();        
        $nationalkpa               = Nationalkpa::findList();
        $nationalobjectives        = Nationalobjectives::findList();        
        $competency                = Competency::findList(); 
        $userObj                   = new User();        
        $statusObj                 = new KpiStatus();
        $statuses                  = $statusObj -> findList();       
        $users_list                = $userObj -> get_users_list();
        $user_logged               = $userObj -> get_user();    
        $datalist['data']          = array();
        foreach($data as $kpa_index => $kpi)
        {             
           $kpi_id  = $kpi['id'];
           foreach($headers as $key => $header)
           {     
              if($key == 'progress')
              {
                 $datalist['data'][$kpi_id][$key]  = self::calculateProgress("kpi", $kpi_id, $kpa['kpistatus'], $userdata);
              }                
              if(isset($kpi[$key]))
              {
                 $value                      = $kpi[$key];
                 if($key == "weighting") { 
                    $datalist['data'][$kpi_id][$key]  = "<em>".(isset($weighting[$value]) ? $weighting[$value] : '')."</em>";
                 } else if($key == "proficiency") {
                    $datalist['data'][$kpi_id][$key]  = "<em>".(isset($proficiency[$value]) ? $proficiency[$value] : '')."</em>";
                 } else if($key == "competency") {
                    $datalist['data'][$kpi_id][$key]  = (isset($competency[$value]) ? $competency[$value] : '');
                 } else if($key == "nationalkpa") {
                    $datalist['data'][$kpi_id][$key] = (isset($nationalkpa[$value]) ? $nationalkpa[$value] : '');
                 } else if($key == "idpobjectives") {
                    $datalist['data'][$kpi_id][$key] = (isset($nationalobjectives[$value]) ? $nationalobjectives[$value] : '');
                 } else if($key == "owner") {
                    $datalist['data'][$kpi_id][$key] = (isset($users_list[$value]) ? $users_list[$value] : '');
                 } else {
                    $datalist['data'][$kpi_id][$key] = $value;
                 }                            
             }               
           }    
        }  
        return $datalist;
     } 
}
?>
