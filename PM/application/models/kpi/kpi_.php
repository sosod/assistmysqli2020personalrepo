<?php
class Kpi extends Model
{

    const ACTIVE = 1;
    
    const DELETED  = 2;
    
    const KPIACTION_IMPORTED = 4;
    
    const KPIACTION_CREATED  = 8;
    
    const UNSPECIFIED_KPI  = 16;
    
    const COPIED           = 32;
    
    protected static $table = "kpi";
    
    protected static $actionInfo;
	
	function __construct()
	{
		parent::__construct();
	}

	function totalKpi( $id )
	{
		$stats = $this -> db -> getRow("SELECT COUNT(*) AS total FROM  #_kpi WHERE kpaid = {$id}");
		return $stats;
	}
	
	public static function copySave($kpaid, $newkpaid, $evaluationyear, $userdata)
	{
	   $kpis = Kpi::findAll(" AND kpaid = '".$kpaid."' AND kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED." ");
	   if(!empty($kpis))
	   {
	      foreach($kpis as $kpiIndex => $kpi)
	      {
	          $kpiid = $kpi['id'];
	          unset($kpi['id']);
	          $kpi['kpaid'] = $newkpaid;
	          $kpi['owner'] = $userdata['tkid'];
	          $kpi['kpiref']    = $kpiid;
	          $newkpiid = Kpi::saveData($kpi);
	          if($newkpiid > 0)
	          {
	              Action::copySave($kpiid, $newkpiid, $evaluationyear, $userdata);
	              //echo "Kpis saved so far have id ".($newkpiid)."<br />";
	          }
	      }
	   }
	}
	
	
	public static function copySaveToNewYear($kpaid, $newkpaid, $toYear)
	{
	   $kpis = Kpi::findAll(" AND kpaid = '".$kpaid."' AND kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED."  ");
	   if(!empty($kpis))
	   {
	      foreach($kpis as $kpiIndex => $kpi)
	      {
	          $kpiid = $kpi['id'];
	          unset($kpi['id']);
	          $kpi['kpaid'] = $newkpaid;
	          $kpi['evaluationyear'] = $toYear;
	          $kpi['kpiref']    = $kpiid;
	          $kpi['kpistatus'] = Kpi::ACTIVE + Kpi::COPIED;
	          $newkpiid = Kpi::saveData($kpi);
	          if($newkpiid > 0)
	          {
	              Action::copySaveToNewYear($kpiid, $newkpiid, $toYear);
	              //echo "Kpis saved so far have id ".($newkpiid)."<br />";
	          }
	      }
	   }
	}	
	
	public static function getKpi($options= array())
	{
		$section = "";	
		$compdata = Component::getComponentCategorySettings($options['componentid'], array("kpi"));

		unset($options['componentid']);
		unset($options['user']);
		if(isset($options['section']))
		{
		   $section = $options['section'];
		   unset($options['section']);
		}	
		$headers = array();
		if(isset($options['headers']) && !empty($options['headers']))
		{
		   $headers = $options['headers'];
		   unset($options['headers']);
		}
		$sqlOption   = parent::createANDSqlOptions($options);
		$sqlOption  .= " AND kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED." ";
		$results     = Kpi::findAll($sqlOption);
		$kpiStatuses = self::_getKPIStatuses($results);
		$response = Sorter::sortHeaders($results, array("component" => $compdata , "type" => "kpi", "headers" => $headers, "section" => $section));		
		$kpiData  = array("data" => $response['data'],
		                  "cols" => $response['cols'],
		                  "headers"    => $response['columns'],
		                  "total"      => $response['total'],
		                  "actionInfo" => self::$actionInfo,
		                  "kpiStatus"  => $response['statuses'],
		                 );
		return $kpiData;	
	} 
	
	function getKpaKpi($user, $componentCategories, $options)
	{
        unset($options['componentid']);
        unset($options['page']);
        if(isset($options['section']))
        {
          $section = $options['section'];
          unset($options['section']);
        }
        $sqlOption   = parent::createANDSqlOptions($options);	
        $sqlOption   .= " AND kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED." ";
        $headers = array();
	   if(isset($options['headers']) && !empty($options['headers']))
	   {
		 $headers = $options['headers'];
		 unset($options['headers']);
	   }
	   $results     = Kpi::findAll($sqlOption);
	   $response   = Sorter::sortHeaders($results, array("component"=>$componentCategories, "type"=>"kpi", "headers"=>$headers, "section"=>$section));
	   $dataResult = array();
	   if(isset($response['data']) && !empty($response['data']))
	   {
	     //$dataResult  = $response['data'];
	     foreach($response['data'] as $kpiId => $kpi)
	     {
	        $dataResult[$kpi['name']] = array();
	        $options['kpiid'] = $kpiId;
	        $dataResult[$kpi['name']] = Action::getkpiActions($user, $componentCategories, $options);
	        //$dataResult[$kpiId]['actions'] = Action::getkpiActions($user, $componentCategories, $options);
	     }
	   }
	   return $dataResult;
	}
	
	private static function _getKPIStatuses($kpis)
	{
	    $statusData = array();
	    foreach($kpis as $index => $val)
	    {
	        $statusData[$val['id']] = $val['kpistatus']; 
	    }
	    return $statusData;
	}	
	
	private static function _actionInfo($kpiData)
	{
	   $actionObj = new Action();
	   foreach( $kpiData as $index => $kpi)
	   {
		$sqlOption = " AND kpiid = '".$index."' AND actionstatus & ".Action::DELETED." <> ".Action::DELETED." ";
		$kpiActions = $actionObj -> fetchAll($sqlOption);
		if(!empty($kpiActions) )
		{
		   foreach( $kpiActions as $aId => $action)
		   {
			 self::$actionInfo[$index][$action['id']] = $action['id'];
		   }
		}
	  }
    }	
    
    public static function calculateKpiProgress($id, $kpiStatus)
    {
      if(($kpiStatus & Kpi::KPIACTION_IMPORTED) == Kpi::KPIACTION_IMPORTED)
      {
          $kpi = Kpi::findById($id);
          $progress = KpiMapping::getActionsProgress($kpi);
          return round($progress['avgProgress'], 2);
      } else {
        $sqlOption = " AND A.kpiid = '".$id."' AND actionstatus & ".Action::DELETED." <> ".Action::DELETED."  ";
        $actionStats = Action::getActionProgressStats($sqlOption);
        return round($actionStats['averageProgress'], 2);
      }
    } 
    
    public static function createUnspecifiedKpi($kpaid, $userid, $evaluationyear, $deadline)
    {
	   $kpi = array();
	   $kpi['kpaid'] = $kpaid;
	   $kpi['kpiref']      = "";
	   $kpi['name']        = "Unspecified";
	   $kpi['objective']   = "Unspecified";
	   $kpi['outcome']	   = "Unspecified";
	   $kpi['measurement'] = "Unspecified";
	   $kpi['owner'] = $userid;
	   $kpi['proof_of_evidence']  = "Unspecified";
	   $kpi['baseline']	   = "Unspecified";
	   $kpi['targetunit']  = "Unspecified";
	   $kpi['nationalkpa'] = 0;
	   $kpi['idpobjectives']	= 0;
	   $kpi['performance_standard'] = "Unspecified";
	   $kpi['target_period'] = "Unspecified"; 
	   $kpi['weighting']     = 0;
	   $kpi['competency']    = 0;
	   $kpi['proficiency']	= 0;
	   $kpi['categories']    = 0;
	   $kpi['deadline']	     = $evaluationyear['end']; 
	   $kpi['remindon']	     = "";
	   $kpi['comments']	     = "";
	   $kpi['status']        = 1;
	   $kpi['kpistatus']     = 1 + Kpi::UNSPECIFIED_KPI;
	   $kpi['evaluationyear'] = $evaluationyear;
	   $kpi['insertuser']     = $_SESSION['tid'];
	   $res = Kpi::saveData($kpi);
    }
    
     function generateReport($postData, Report $reportObj)
	{
	   $joinWhere = $this -> createJoins($postData['value']);
	   $includeprogress = false;
	   if(isset($postData['kpi']['progress']))
	   {
	     $includeprogress = true;
	     unset($postData['kpi']['progress']);
	   }
	   if(isset($postData['value']['evaluationyear']))
	   {
	     $whereStr =  " AND KPA.evaluationyear = '".$postData['value']['evaluationyear']."' ";
	     unset($postData['value']['evaluationyear']);
	   }	   
	   $fields    = $reportObj -> prepareFields($postData['kpi'], "K");
	   $headers   = array();
	   foreach($postData['kpi'] as $field => $val)
	   {
	     $headers[] = $field;
	   }
	   if($includeprogress)
	   {
	     array_push($headers, "progress");
	   }
	   $groupBy   = $reportObj -> prepareGroupBy($postData['group_by'], "K"); 
	   $sortBy    = $reportObj -> prepareSortBy($postData['sort'], "K");
	   $whereStr  .= $reportObj -> prepareWhereString($postData['value'], $postData['match'], "K");   
	   $whereStr  .= $joinWhere;
	   $results  = $this -> filterResults($fields, $whereStr, $groupBy, $sortBy, "");
        $response = Sorter::sortHeaders($results, array("type" => "kpi", "headers" => $headers));		
        $response['title'] = $postData['report_title'];
        $reportObj -> displayReport($response, "on_screen", "data");	   
	   exit();
	}
     
     function filterResults($fields, $values = "", $groupBy = "", $sortBy = "", $joins = "")
     {
       $orderBy = (!empty($sortBy) ? " ORDER BY $sortBy " : "");
       $groupBy = (!empty($groupBy) ? " GROUP BY $groupBy " : "");
	  $results = parent::query("SELECT K.id, kpistatus, ".$fields." FROM #_kpi K
	                            INNER JOIN #_kpa KPA ON KPA.id = K.kpaid
	                            INNER JOIN #_user_component UC ON UC.componentid = KPA.componentid
	                            $joins
				             WHERE K.owner <> '' AND K.kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED."
				             AND UC.status & ".UserComponent::ACTIVATED." = ".UserComponent::ACTIVATED."
				             $values
				             $groupBy
				             $orderBy 
				            ");
	  return $results;	          
     }  	
	
	function createJoins($options = array())
	{
        $joinStr  = "";
        $whereStr = "";	   
        if(isset($options['nationalkpa']) && !empty($options['nationalkpa']))
        {
           $natStr = "";
           foreach($options['nationalkpa'] as $nkpaIndex => $nationalkpa)
           {
              $natStr .= "  K.nationalkpa = '".$nationalkpa."' OR";
           }  
           if(!empty($natStr))
           {
             $whereStr .= " AND (".rtrim($natStr, 'OR').") ";
           }
        }
        if(isset($options['idpobjectives']) && !empty($options['idpobjectives']))
        {
           $idpStr = "";
           foreach($options['idpobjectives'] as $idpIndex => $idp)
           {
              $idpStr .= " K.idpobjectives = '".$idp."' OR";
           }  
           if(!empty($idpStr))
           {
             $whereStr .= " AND (".rtrim($idpStr, 'OR').") ";
           }
        }
        if(isset($options['weighting']) && !empty($options['weighting']))
        {
           $weightStr = "";
           foreach($options['weighting'] as $weigtIndex => $weighting)
           {
              $weightStr .= "  K.weighting = '".$weighting."' OR";
           } 
           if(!empty($weightStr))
           {
              $whereStr .= " AND (".rtrim($weightStr, 'OR').") ";
           }
        }  
        if(isset($options['owner']) && !empty($options['owner']))
        {
           $userStr = "";
           foreach($options['owner'] as $ownerIndex => $owner)
           {
              $userStr .= "  K.owner = '".$owner."' OR";
           }  
           if(!empty($userStr))
           {
              $whereStr .= " AND (".rtrim($userStr, "OR").") ";
           } 
        }          
       return $whereStr;          
	}  
	
	public static function deleteKpaKpis($kpis)
	{
	   foreach($kpis as $index => $kpi)
	   {
	      $res = Kpi::updateWhere(array("kpistatus" => 2), array("id" => $kpi['id']));
	      if($res > 0)
	      {
	         $actions = Action::findAllByKpiid($kpi['id']);         
	         if(!empty($actions))
	         {
	            Action::deleteKpiActions($actions);
	         }
	      }
	   }
	}
}
