<?php
class Kpistatus extends Model
{
	
	protected static $table = "list_kpistatus";
	
	function __construct()
	{
		parent::__construct();
	}
	
	public static function getKpiStatuses($options=array())
	{
		$sqlOption   = parent::createANDSqlOptions($options);
		$sqlOption  .= " AND status & 2 <> 2";
		$used 		 = self::get_used_kpistatus();
		$statusArr   = array();
		$results  	 = Kpistatus::findAll( $sqlOption );
		foreach( $results as $index => $status)
		{
			if( in_array($status['id'], $used))
			{
				$status['used'] = true;
			} else {
				$status['used'] = false;
			}
			$statusArr[$index] = $status;
		}
		return $statusArr;	
	}
	
	public static function getStatusInUpdateOrder()
	{
	   $kpiStatuses = Kpistatus::findAll(" AND status & 2 <> 2 AND status & 1 = 1");
	   $tmpArr = array();
	   $statuses = array();
	   foreach($kpiStatuses as $index => $status)
	   {
	      if($status['id'] == "6")
	      {
	         $tmpArr[] = $status;
	      } else {
	        $statuses[] = $status;
	      }
	   }
	   $newStatusOrder = array_merge($statuses, $tmpArr);
	   return $newStatusOrder;
	} 

	function get_used_kpistatus()
	{
		$kpiObj 	  = new Kpi();
		$kpistatuses  = $kpiObj -> get_statuses_used();
		return $kpistatuses;
	}
}
