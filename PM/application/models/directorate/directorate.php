<?php
class Directorate extends Model
{

    
    function __construct($options=array())
    {
	  parent::__construct();
    }
	
    function get_all_directorates()
    {
       $result = $this -> db -> get("SELECT dirid AS id,dirtxt AS name ,dirsort,diryn 
                                     FROM assist_".$_SESSION['cc']."_list_dir 
                                   ");
	  return $result;
    }
    
    function get_directorate( $id )
    {
		$result = $this -> db -> getRow("SELECT dirid AS id,dirtxt AS name ,dirsort,diryn 
		                                 FROM assist_".$_SESSION['cc']."_list_dir WHERE dirid = $id
		                                ");
		return $result;
    } 
    
    function get_departments( $id )
    {
		$result = $this -> db -> get("SELECT id AS id, value AS name FROM assist_".$_SESSION['cc']."_list_dept WHERE 1");
		return $result;
    } 
        
    
    function get_subdirectorates($id)
    {
	  $result = $this -> db -> get("SELECT subid AS id,subtxt AS name,subyn,subdirid AS dirid, subsort, subhead 
		                              FROM assist_".$_SESSION['cc']."_list_dirsub 
							     WHERE subdirid = $id
							     ORDER  BY subsort
							   ");
	  return $result;
    }    
}
?>
