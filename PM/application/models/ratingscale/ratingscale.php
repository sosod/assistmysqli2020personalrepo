<?php
class Ratingscale extends  Model
{
     protected static $table = "list_ratingscale";
     	
	function __construct()
	{
        parent::__construct();
	}
     
     public static function getRatingScales($options=array())
     {
        $sqlOptions     = parent::createANDSqlOptions($options);
	    $sqlOptions    .= " AND status & 2 <> 2";
	    $results        = Ratingscale::findAll($sqlOptions);
	    $used           = self::get_used_rating_scales();
	    $resultsArr	    = array(); 
	    foreach($results as $index => $rating)
	    {
		  if(in_array($rating['id'], $used))
		  {
		    $rating['used'] = false;
		    $resultsArr[$index] = $rating;
		  } else {
		    $rating['used'] = true;
		    $resultsArr[$index] = $rating;
		  }
	   }
	   return $resultsArr;          
     } 
     
     public static function getRatingInfo()
     {
        $ratingScales = Ratingscale::findAll();
        $ratingScaleList = array();
        foreach($ratingScales as $index => $ratingscale)
        {
          $ratingScaleList[$ratingscale['id']] = "<span style='background-color:#".$ratingscale['color']."'; padding:10px;'>".$ratingscale['definition']."(".$ratingscale['ratingto'].")</span>";
        }
        return $ratingScaleList;
     }
     
     function update_rating_scale($data)
     {
        $this -> attach(new SetupAuditLog("Rating Scale"));
	   $res = $this -> update_where($data['data'], array("id" => $data['id']));        
	   return $res;
     }
     
     function get_max_rating_scale($option_sql = "")
     {
        $result = $this -> db -> getRow("SELECT  MAX(ratingto) AS ratingto FROM #_".static::$table." WHERE 1");
        return $result['ratingto'];     
     }
     
     function get_used_rating_scales()
     {
         $action_evaluation_obj = new ActionEvaluation();
         $kpi_evaluation_obj    = new KpiEvaluation();
         $kpa_evaluation_obj    = new KpaEvaluation();
         $action_evaluations    = $action_evaluation_obj -> get_used_rating_scales(); 
         $kpi_evaluations       = $kpi_evaluation_obj -> get_used_rating_scales();
         $kpa_evaluations       = $kpa_evaluation_obj -> get_used_rating_scales();    
         $used_rating_ids       = array();
         foreach($action_evaluations as $a_index => $rating_id)
         {
            $used_rating_ids[$rating_id] = $rating_id;
         }
         foreach($kpi_evaluations as $a_index => $rating_id)
         {
            $used_rating_ids[$rating_id] = $rating_id;
         }
         foreach($kpa_evaluations as $a_index => $rating_id)
         {
            $used_rating_ids[$rating_id] = $rating_id;
         }
         return $used_rating_ids;
     }

}
