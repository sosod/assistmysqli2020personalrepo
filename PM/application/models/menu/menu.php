<?php
class Menu extends Model
{
     public static $instance;
      
     protected static $table = "menu";     
          
     private static $id;
     
     private static $name;
     
     private static $url;
     
     private static $folder;
     
     private static $ignite_terminology;
     
     private static $client_terminology;
     
     private static $parent_id;
     
     private static $status;
     
     private static $sort;
     
     
     public function __construct()
     {
        parent::__construct();
     }
     
     public function menuItems($parentref)
     {
        $options  = " AND parent_id = '".$parentref."'";
        self::$parent_id = $parentref;
        $results = self::findAll($options);
        $menuT = array();
        $action = "";
        if(!isset($_GET['action']))
        {
          $action = DEFAULT_ACTION;
        } else {
          $action = $_GET['action'];
        }
        //debug($results);
        foreach($results as $index => $menu)
        {
          $activeUrl = "";
          $url = "main.php?controller=".$menu['url']."&parent=".$menu['parent_id'];
          if($menu['name'] == $action)
          {
            //$activeUrl = "Y";
            //self::$url = $url;
          }
          $menuT[$menu['id']] = array( "id"  => $menu['id'], 
							    "url"	=> "main.php?controller=".$menu['url']."&parent=".$menu['parent_id'], 
							    "active"  => ($menu['name'] == $action  ? "Y" : ""),
							    "display" => ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])) ;
        }
        echo echoNavigation(1, $menuT);	
     }
     
     public static function instance()
     {
        if(!self::$instance)
        {
          self::$instance = new Menu();
        }
        return self::$instance;
     }    
          
     function getUrl()
     {
        return self::$url;
     }
     
     function getParent($parentref)
     {
        $parent = Menu::queryRow("SELECT * FROM #_menu WHERE id = '".$parentref."' ");
        return $parent;
     }

     function displayMenu($folder)
     {
        $periodObj      = new EvaluationPeriod();
        $is_period_open = $periodObj -> is_open();
        //debug($is_period_open);
        $actionRef = "";
        if(!isset($_GET['action']))
        { 
          $action = $folder."_".DEFAULT_ACTION;
          $actionRef = DEFAULT_ACTION;
        } else {
          $action = $folder."_".$_GET['action'];
          $actionRef  = $_GET['action'];
        }             
        $results   = Menu::query("SELECT * FROM #_menu WHERE folder = '".$folder."' AND parent_id <> '0' AND status & 1 = 1 "); 
        $mainMenu  = array();
        $subMenu   = array();
        $keyRef    = "";
        foreach($results as $index => $menu)
        {
          $key = str_replace($folder."_", "", $menu['name']); 
          if(strpos($key, '_'))
          {
              
             $parentRef  = $results[$menu['parent_id']]['name'];
             $keyRef = $folder."_".substr($actionRef, 0, strpos($key, '_'));
             $subMenu[$parentRef][$menu['id']] = $this -> prepareMenu($key, $menu, $action);
          } else {
           if($menu['name'] == "manage_evaluate" && !$is_period_open)
             {
               continue;
             } else {
               $mainMenu[$menu['id']] = $this -> prepareMenu($key, $menu, $action);
             }               
          }
        }
        echo echoNavigation(1, $mainMenu);	
        if(isset($subMenu[$keyRef]) && !empty($subMenu[$keyRef]))
        {
          echo echoNavigation(2, $subMenu[$keyRef]);	
        }
     }
     
     function prepareMenu($key, $menu, $action)
     {
          $activeUrl = "";
          $url = "main.php?controller=".$menu['url']."&parent=".$menu['parent_id'];
          if($key == $action)
          {
            $activeUrl = "Y";
          }
          $menuT = array( "id"      => $menu['id'], 
	                     "url"	 => "main.php?controller=".$menu['url']."&parent=".$menu['parent_id']."&folder=".$menu['folder'], 
	                     "active"  => ($menu['name'] == $action  ? "Y" : ""),
	                     "display" => ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])) ;
                     
          return $menuT;
     }

     /*function menus()
     {
       $menuItems = Menu::query("SELECT * FROM #_menu ");
       $parentMenus = array();
       $parenIds = array();
       $parentSubId = array();
       $parentSub   = array();
       foreach($menuItems as $index => $menu)
       {
          if($menu['parent_id'] == 0 && !empty($menu['folder']))
          {
             $parentMenus[$menu['folder']] = $menu;
             $parenIds[] = $menu['id'];
             unset($menuItems[$index]);
          } 
          if($menu['parent_id'] != 0)
          {
             if(isset($parentMenus[$menu['folder']]))
             {
               if(in_array($menu['parent_id'], $parenIds))
               {
                 $parentMenus[$menu['folder']]['submenu'][$menu['name']] = $menu;
                 $parentSubId[] = $menu['id'];
                 $parentSub[$menu['id']] = $menu['name'];
                 unset($menuItems[$index]);
               }
             }
          }
          if(in_array($menu['parent_id'], $parentSubId))
          {
             $key = $parentSub[$menu['parent_id']];
             $parentMenus[$menu['folder']]['submenu'][$key]['sub'][$menu['name']] = $menu;
          }
       }
       return $parentMenus; 
     }
     
     function displayMenu($parentref)
     {
        $menuItems = self::menus();
        if(!isset($_GET['action']))
        {
          $action = DEFAULT_ACTION;
        } else {
          $action = $_GET['action'];
        }        
        if(isset($menuItems[$parentref]))
        {
          
          if(isset($menuItems[$parentref]['submenu']))
          {
             $menuT = self::prepareMenu($menuItems[$parentref]['submenu'], $action);
             $secondTier  = array();
             //debug($menuT);            
             if(isset($menuT['sub']))
             {
               $secondTier = $menuT['sub'];
               unset($menuT['sub']);
             }

             echo echoNavigation(1, $menuT);	
             echo echoNavigation(2, $secondTier);	
          }
        }
     }
     
     
     
     function prepareMenu($results, $action)
     {
        $menuT  = array();
        foreach($results as $index => $menu)
        {
          if(isset($menu['sub']) && !empty($menu['sub']))
          {
               $activeUrl = "";
               $url = "main.php?controller=".$menu['url']."&parent=".$menu['parent_id'];
               if($menu['name'] == $action)
               {
                 //$activeUrl = "Y";
                 //self::$url = $url;
               }
               $menuT[$menu['id']] = array( "id"  => $menu['id'], 
							         "url"	=> "main.php?controller=".$menu['url']."&parent=".$menu['parent_id']."&folder=".$menu['folder'], 
							         "active"  => ($menu['name'] == $action  ? "Y" : ""),
							         "display" => ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])) ;
              $menuT['sub'] =  self::prepareMenu($menu['sub'], $action);
              unset($menu['sub']);
          } else {
               $activeUrl = "";
               $url = "main.php?controller=".$menu['url']."&parent=".$menu['parent_id'];
               if($menu['name'] == $action)
               {
                 //$activeUrl = "Y";
                 //self::$url = $url;
               }
               $menuT[$menu['id']] = array( "id"  => $menu['id'], 
							         "url"	=> "main.php?controller=".$menu['url']."&parent=".$menu['parent_id']."&folder=".$menu['folder'], 
							         "active"  => ($menu['name'] == $action  ? "Y" : ""),
							         "display" => ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology'])) ;
	     }
        }  
        return $menuT;   
     }*/
}
?>
