<?php
class Weighting extends Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public static function fetchAll( $options = "")
	{
		$results = DBConnect::getInstance()->get("SELECT id, value, description, weight, sort, status 
		                                          FROM assist_".$_SESSION['cc']."_master_list_weightings 
		                                          WHERE status & 2 <> 2
		                                        ");
		 return $results;
	}
	
	public static function findList($options = "")
	{
	   $weightings = self::fetchAll();
	   $weightinglist = array();
	   foreach($weightings as $index => $weighting)
	   {
	     $weightinglist[$weighting['id']] = $weighting['value']." (".$weighting['weight'].")";
	   }
	   return $weightinglist;
	} 
}
