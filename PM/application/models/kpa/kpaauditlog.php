<?php
class KpaAuditLog extends AuditLog
{
 
     private $currentStatus = "";
     
     function __construct()
     {
        parent::__construct();
     }
     
     function notify($postData, $where, $tablename)
     {
        $kpa = array();
        if(isset($where['id']))
        {
          $kpa = Kpa::findById($where['id']);
        } else {
          $whereStr = " WHERE 1 ";
          foreach($where as $field => $value)
          {
             $where_str .= " AND ".$field." = '".$value."' ";
          }
          $kpa = Kpa::find($where_str);
        }
        $changes = array();
        $nationalkpa = NationalKpa::findAll();
        $idpobjectives  = Nationalobjectives::findAll();
        $weightings  = Weighting::fetchAll();
        $statuses    = KpaStatus::findAll();
        foreach($kpa as $postIndex => $value)
        {
          if($postIndex == 'status')
               {
             $toStatus = "";
             if(isset($postData['status']))
             {
               $toStatus = $postData['status'];
             }
             $statusChanges = self::_getStatusChanges($value, $toStatus, $statuses);
             if(!empty($statusChanges))
             {
               $changes[$postIndex] = $statusChanges;
             }
          } else if(isset($postData[$postIndex])) {
             
             if($postIndex == "nationalkpa")
             {
                $nkpaChanges = self::_getNationalKpaChanges($value, $postData[$postIndex], $nationalkpa);
                if(!empty($nkpaChanges))
                {
                   $changes[$postIndex]  = $nkpaChanges;
                }
             } else if($postIndex == "idpobjectives"){
                $idpChanges = self::_getIDPChanges($value, $postData[$postIndex], $idpobjectives);
                if(!empty($idpChanges))
                {
                    $changes[$postIndex] = $idpChanges;
                }   
                   
             } else if($postIndex == "weighting"){
               $weightChanges = self::_getWeightingChanges($value, $postData[$postIndex], $weightings);
               if(!empty($weightChanges))
               {
                  $changes[$postIndex] = $weightChanges;
               }   
             } else if($postIndex == "remindon") {
               if($postData[$postIndex] !=  $value)
               {
                 $changes["remind_on"] = array("to" => $postData[$postIndex], "from" => ($value == "" ? date("d-M-Y") : $value));
               }             
             }  else {
               if($postData[$postIndex] !==  $value)
               {
                 $changes[$postIndex] = array("from" => $value, "to" => $postData[$postIndex]);
               }
             }
          }
        }
        if(isset($_POST['response']) && !empty($_POST['response']))
        {
           $changes['response'] = $_POST['response'];  
        }         
        $res = 0;
        if(!empty($changes))
        { 
             $changes['user']          = $_SESSION['tkn'];
             $insertdata['insertuser'] = $_SESSION['tid'];
             $changes['currentstatus'] = $this-> currentStatus;
             $insertdata['kpaid']      = $kpa['id'];
             $insertdata['changes']    = serializeEncode($changes);
             static::$table = $tablename."_logs";
             if(!empty($insertdata))
             {
               $res = parent::saveData($insertdata);
             }   
        }
        return $res;
     }     
     
     private function _getNationalKpaChanges($fromVal, $toVal, $nationalkpa)
     {
        $from = $to = "";
        $changes = array();
        if(isset($nationalkpa[$toVal]))
        {
          $to = $nationalkpa[$toVal]['name'];
        }
        if(isset($nationalkpa[$fromVal]))
        {
          $from = $nationalkpa[$fromVal]['name'];
        }       
                   
        if($from != $to)
        {
          $changes = array("from" => $from, "to" => $to);    
        }     
       return $changes;   
     }
     
     private function _getIDPChanges($fromVal, $toVal, $idpobjectives)
     {
        $from = $to = "";
        $changes = array();
        if(isset($idpobjectives[$toVal]))
        {
          $to = $idpobjectives[$toVal]['name'];
        }
        if(isset($idpobjectives[$fromVal]))
        {
          $from = $idpobjectives[$fromVal]['name'];
        }                  
        if($from != $to)
        {
          $changes = array("from" => $from, "to" => $to);    
        }     
       return $changes;   
     }     
     
     private function _getWeightingChanges($fromVal, $toVal, $wieghtings)
     {
        $from = $to = "";
        $changes = array();
        if(isset($wieghtings[$toVal]))
        {
          $to = $wieghtings[$toVal]['value'];
        }
        if(isset($wieghtings[$fromVal]))
        {
          $from = $wieghtings[$fromVal]['value'];
        }                  
        if($from != $to)
        {
          $changes = array("from" => $from, "to" => $to);    
        }     
       return $changes;   
     }       
     
     function _getStatusChanges($fromVal, $toVal, $statuses)
     {
        $from = $to = "";  
        $changes = array();
        if(isset($statuses[$fromVal]))
        {
          $from = $statuses[$fromVal]['name'];
        }  
        if(isset($statuses[$toVal]))
        {
          $to = $statuses[$toVal]['name'];
        }          
        
        if(($from != $to) && !empty($to))
        {
          $this -> currentStatus = $to;
          $changes = array("from" => $from, "to" => $to);          
        } else {
          $this -> currentStatus = $from;
        }
        return $changes;
     }
     
}
?>
