<?php
class KpaSorter extends Sorter
{
     static $next_evaluation   = "";
     
     static $evaluation_status = array();
     
     private $self_evaluation    = false;
     
     private $manager_evaluation = false;
     
     private $joint_evaluation   = false;
     
     private $review_evaluation  = false;     
     
     function __construct()
     {
        parent::__construct();
     }     
     
     public static function sort_kpa($data, $kpaObj, $options)
     {
        $headers = array();
        $section = (isset($options['section']) ? $options['section'] : "");
        if(isset($options['headers']) && !empty($options['headers']))
        {
           $headerNames = Naming::getHeaderList();
           foreach($options['headers'] as $hIndex => $hval)
           {
               $headers[$hval] = (isset($headerNames[$hval]) ? $headerNames[$hval] : "");   
           }
        } else {
          $headerNames = Naming::getHeaderList($section);
          $headers = $headerNames;
        }   
        $setting  = array();
        if(isset($options['setting']))
        {
           $setting = $options['setting'];
        }    
        
        $committeeObj              = new EvaluationCommittee();
        $weighting                 = Weighting::findList();
        $proficiency               = Proficiency::findList();        
        $nationalkpa               = Nationalkpa::findList();
        $nationalobjectives        = Nationalobjectives::findList();        
        $competency                = Competency::findList(); 
        $userObj                   = new User();        
        $statusObj                 = new KpaStatus();
        $statuses                  = $statusObj -> findList();
        $userdata                  = (isset($options['user']) ? $options['user'] : array());
        $users_list                = $userObj -> get_users_list();
        $user_logged               = $userObj -> get_user();
        $datalist['headers']       = array();
        $datalist['data']          = array();
        $datalist['statuses']      = array();
        $datalist['settings']      = array();
        $datalist['component']     = array();   
        $datalist['userdata']      = $userdata;
        $datalist['users']         = $user_list;
        $datalist['userlogged']    = $user_logged;
        $datalist['in_committee']  = $committeeObj -> is_user_in_evaluation_committee($user_logged['default_year']);
        $ratingObj                 = new Ratingscale();
        $rating                    = $ratingObj -> getRatingInfo();              
        if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
        {
           $datalist['userlogged']['evaluationperiod']   = $_SESSION['evaluation_period']['open_from']." to ".$_SESSION['evaluation_period']['open_to'];
        }
        if(!empty($data))
        {
          $datalist['hasActions'] = TRUE;  
        } else {
          $datalist['hasActions'] = FALSE;  
        }
        $evaluations                 = $options['evaluations'];
        $setting                     = $options['setting'];
        $datalist['kpi_evaluations'] = array();     
        $datalist['action_evaluations'] = array();     
        $datalist['kpa_evaluations'] = array();     
        $kpiObj                      = new Kpi();   
        $actionObj                   = new Action();
        $kpimappingObj               = new KpiMapping();
        $total_kpa                   = 0;        
        $total_kpi                   = 0;
        $total_actions               = 0; 
        $actionevalautionObj         = new ActionEvaluation();
        $kpievaluationObj            = new KpiEvaluation();
        
        $kpaObj -> get_evaluations($options['componentid'], $user_logged['default_year'], $userdata, $setting);
        foreach($data as $kpa_index => $kpa)
        {  
           $total_kpa      += 1;
           $kpa_id          = $kpa['id'];   
           $kpa_options     = array('kpaid' => $kpa_id, 'section' => $section, 'kparef' => $kpa['kparef'], 'page' => $options['page']);
           $kpa_kpi_actions = $kpiObj -> get_kpa_kpi($userdata, $setting, $actionObj, $kpimappingObj, $kpa_options, $actionevalautionObj, $kpievaluationObj);    
  
           $datalist['kpi_evaluations']                     = $kpa_kpi_actions['kpi_evaluations'];
           $datalist['kpi_evaluations']['total_kpi']        = $kpa_kpi_actions['total_kpi'];
           $datalist['action_evaluations']                  = $kpa_kpi_actions['action_evaluations'];
           $datalist['action_evaluations']['total_actions'] = $kpa_kpi_actions['total_actions'];
           if(isset($kpa_kpi_actions['kpi_change']) && !empty($kpa_kpi_actions['kpi_change']))
           {
              $datalist['kpi_change'] = $kpa_kpi_actions['kpi_change'];
           }
           
           if(isset($kpa_kpi_actions['total_kpi']))
           {
              $total_kpi  = $kpa_kpi_actions['total_kpi'];
           }
           if(isset($kpa_kpi_actions['total_actions']))
           {
              $total_actions = $kpa_kpi_actions['total_actions'];
           }           
           if(!empty($kpa_kpi_actions['ids']))
           {  
              if(count($kpa_kpi_actions['actionInfo']) !=  count($kpa_kpi_actions['ids']))
              {
                 $datalist['hasActions']  = FALSE;                  
              } 
              $datalist['kpi_data'][$kpa_id] = $kpa_kpi_actions; 
           } else {
              $datalist['hasActions']         = FALSE;
              $datalist['kpi_data'][$kpa_id]  = $kpa_kpi_actions; 
           }

           $datalist['statuses'][$kpa_id]        = $kpa['kpastatus'];
           $datalist['settings']['evaluate_on']  = $setting['evaluate_on'];
           $_evaluations                         = array('self'     => $setting['self_evaluation'],
                                                          'manager' => $setting['manager_evaluation'],
                                                          'joint'   => $setting['joint_evaluation'],
                                                          'review'  => $setting['evaluation_review']
                                                        );  
                                   
           $datalist['settings']['evaluation_type']          = $_evaluations;  
           $kpa_evaluations                                  = array();
           $kpaevaluationObj                                 = new KpaEvaluation();
           $eval_res                                         = $kpaevaluationObj -> get_kpa_next_evaluation($kpa['id'], $setting, $rating);
           $datalist['settings']['next_evaluation'][$kpa_id]    = $eval_res['next_evaluation'];
           $datalist['settings']['evaluations'][$kpa_id]        = $eval_res['evaluations'];;
           $datalist['settings']['evaluationstatuses'][$kpa_id] = $eval_res['statuses'];
           $datalist['componentname']                           = $kpa['componentname'];           
           foreach($headers as $key => $header)
           {     
              if($key == 'progress')
              {
                 $datalist['data'][$kpa_id][$key]  = $kpa_kpi_actions['progress'];
                 $datalist['headers'][$key]        = $header;
              }                
              if(isset($kpa[$key]))
              {
                 $value                      = $kpa[$key];
                 $datalist['headers'][$key]  = $header;
                 if($key == "weighting") { 
                    $datalist['data'][$kpa_id][$key]  = "<em>".(isset($weighting[$value]) ? $weighting[$value] : '')."</em>";
                 } else if($key == "proficiency") {
                    $datalist['data'][$kpa_id][$key]  = "<em>".(isset($proficiency[$value]) ? $proficiency[$value] : '')."</em>";
                 } else if($key == "competency") {
                    $datalist['data'][$kpa_id][$key]  = (isset($competency[$value]) ? $competency[$value] : '');
                 } else if($key == "nationalkpa") {
                    $datalist['data'][$kpa_id][$key] = (isset($nationalkpa[$value]) ? $nationalkpa[$value] : '');
                 } else if($key == "idpobjectives") {
                    $datalist['data'][$kpa_id][$key] = (isset($nationalobjectives[$value]) ? $nationalobjectives[$value] : '');
                 } else if($key == "owner") {
                    $datalist['data'][$kpa_id][$key] = (isset($users_list[$value]) ? $users_list[$value] : 'Unspecified');
                 } else {
                    $datalist['data'][$kpa_id][$key] = $value;
                 }                            
             }               
           }    
        }  
        $overall_next       = $_SESSION[$_SESSION['modref']][$userdata['tkid']]['overall_next'];
        $kpi_evaluations    = $kpievaluationObj -> get_total_next_evaluations();
        $action_evaluations = $actionevalautionObj -> get_total_next_evaluations();      
        $kpa_evaluations    = array();
        $is_accepted        = true;

        if($setting['evaluate_on'] == "kpa")
        {   
           $kpa_evaluations                      = $kpaObj -> get_total_next_evaluations();
           $is_kpa_completed                     = $kpaObj -> evaluation_complete;
           $total_left                           = $kpa_evaluations['total'];
           $datalist['kpa_evaluations']['total'] = $total_kpa;
           //debug($_SESSION);
            //debug($kpa_evaluations);
            //echo $overall_next;

            //exit();
           //if the overall next evaluation is not same as the current component next evaluation, then keep the overall status but change done of the current component to total of that component;
           //echo $overall_next." -- ".$kpa_evaluations['next_key']."\r\n\n";
           if($overall_next == $kpa_evaluations['next_key'])
           {
             $progress                                = (round( (($total_kpa - $total_left)/$total_kpa), 2) * 100)." % ";
             $datalist['kpa_evaluations']['progress'] = $progress;             
             $datalist['kpa_evaluations']['next']     = $kpa_evaluations['next'];
             $datalist['kpa_evaluations']['done']     = ($total_kpa - $total_left);                             
           } else {   
             if($overall_next !== "")
             {
                $is_kpa_completed = false;
             }
             $datalist['kpa_evaluations']['progress'] = "100%";             
             $datalist['kpa_evaluations']['done']     = $total_kpa;                
             $datalist['kpa_evaluations']['next']    = ucwords(str_replace("_", " ", $overall_next))." Evaluation";
           }
           $datalist['kpa_evaluations']['next_key']  = $kpa_evaluations['next_key'];
           $kpa_next_evaluation                      = $kpa_evaluations['next_key'];
           $datalist['kpa_evaluations']['total_kpa'] = $total_kpa;           
           $datalist['evaluation_complete']          = $is_kpa_completed;
        }
        
        
        if($setting['evaluate_on'] == "action")
        {
           $is_action_complete     = $actionevalautionObj -> evaluation_complete;
           $action_next_evaluation = $action_evaluations['next_key'];
           $total_left             = $action_evaluations['total'];
           $total_actions          = $datalist['action_evaluations']['total_actions'];
           if($overall_next == $action_next_evaluation)
           {
             $progress                                  = (round((($total_actions - $total_left)/$total_actions), 2) * 100)." % ";
             $datalist['action_evaluations']['progress']= $progress;             
             $datalist['action_evaluations']['next']    = $action_evaluations['next'];
             $datalist['action_evaluations']['done']    = ($total_actions - $total_left);
           } else {
              if($overall_next !== "")
              {
                $is_action_complete = false;
              }            
              $datalist['action_evaluations']['progress']= "100%";             
              $datalist['action_evaluations']['done']    = $total_actions;                
              $datalist['action_evaluations']['next']    = ucwords(str_replace("_", " ", $overall_next))." Evaluation";
           }
           $datalist['action_evaluations']['next_key'] = $action_next_evaluation;
           $datalist['action_evaluations']['total']    = $total_actions;
           $datalist['evaluation_complete']            = $is_action_complete;
        }
        
        if($setting['evaluate_on'] == "kpi")
        {
           $is_kpi_complete     = $kpievaluationObj -> evaluation_complete;   
           $kpi_next_evaluation = $kpi_evaluations['next_key'];
           $total_left          = $kpi_evaluations['total'];
           //if the overall next evaluation is not same as the current component next evaluation, then keep the overall status but change done of the current component to total of that component;
           if($overall_next == $kpi_next_evaluation)
           {
              $progress                                = (round( (($total_kpi - $total_left)/$total_kpi), 2) * 100)." % ";
              $datalist['kpi_evaluations']['progress'] = $progress;             
              $datalist['kpi_evaluations']['next']     = $kpi_evaluations['next'];
              $datalist['kpi_evaluations']['done']     = ($total_kpi - $total_left);
           } else {
              if($overall_next !== "")
              {
                $is_kpi_complete = false;
              }
              $datalist['kpi_evaluations']['next']     = ucwords(str_replace("_", " ", $overall_next))." Evaluation";       
              $datalist['kpi_evaluations']['progress'] = "100%";             
              $datalist['kpi_evaluations']['done']     = $total_kpi;
           }           
           $datalist['kpi_evaluations']['next_key'] = $kpi_next_evaluation;
           $datalist['kpi_evaluations']['total']    = $total_kpi;           
           $datalist['evaluation_complete']         = $is_kpi_complete;
        }
        $datalist['overall_next']      = $next_overall;
        $datalist['overall_str']       = $overall_str;
        $datalist['total_to_evaluate'] = $count;        
        $datalist['cols']              = count($datalist['headers']);
        //debug($datalist);
        return $datalist;
     }
     
     protected static function get_next_evaluation($kpaid, $evaluations, $kpa_evaluations)
     {
         $next_evaluation   = "";
         $evaluation_status = array();
         foreach($evaluations as $key => $status)
         {
           if($status == 1)
           {
               if(isset($kpa_evaluations[$key]))
               {
                 if(empty($kpa_evaluations[$key]['rating']) || empty($kpa_evaluations[$key]['comment']) 
                    || empty($kpa_evaluations[$key]['recommendation']))
                 {
                    if(empty($next_evaluation))
                    {
                       $next_evaluation               = $key;
                       self::$evaluation_status[$key] = array("status" => "ui-icon ui-icon-arrow-1-e", "name" => $status);
                    } else {
                       self::$evaluation_status[$key] = array("status" => "", "name" => $status);  
                    }
                 } else {
                    self::$evaluation_status[$key] = array("status" => "ui-icon ui-icon-check", "name" => $status);
                 }
               }
           } else {
               self::$evaluation_status[$key] = array("status" => "ui-icon ui-icon-close", "name" => $status);
           }
         }
         return $next_evaluation;
     }
     
     public static function component_kpa($data, $userdata, $headers)
     {
        $committeeObj             = new EvaluationCommittee();
        $weighting                 = Weighting::findList();
        $proficiency               = Proficiency::findList();        
        $nationalkpa               = Nationalkpa::findList();
        $nationalobjectives        = Nationalobjectives::findList();        
        $competency                = Competency::findList(); 
        $userObj                   = new User();        
        $statusObj                 = new KpaStatus();
        $statuses                  = $statusObj -> findList();       
        $users_list                = $userObj -> get_users_list();
        $user_logged               = $userObj -> get_user();    
        $datalist['data']          = array();
        foreach($data as $kpa_index => $kpa)
        {             
           $kpa_id  = $kpa['id'];
           foreach($headers as $key => $header)
           {     
              if($key == 'progress')
              {
                 $datalist['data'][$kpa_id][$key]  = self::calculateProgress("kpa", $kpa_id, $kpa['kpastatus'], $userdata);
              }                
              if(isset($kpa[$key]))
              {
                 $value                      = $kpa[$key];
                 if($key == "weighting") { 
                    $datalist['data'][$kpa_id][$key]  = "<em>".(isset($weighting[$value]) ? $weighting[$value] : '')."</em>";
                 } else if($key == "proficiency") {
                    $datalist['data'][$kpa_id][$key]  = "<em>".(isset($proficiency[$value]) ? $proficiency[$value] : '')."</em>";
                 } else if($key == "competency") {
                    $datalist['data'][$kpa_id][$key]  = (isset($competency[$value]) ? $competency[$value] : '');
                 } else if($key == "nationalkpa") {
                    $datalist['data'][$kpa_id][$key] = (isset($nationalkpa[$value]) ? $nationalkpa[$value] : '');
                 } else if($key == "idpobjectives") {
                    $datalist['data'][$kpa_id][$key] = (isset($nationalobjectives[$value]) ? $nationalobjectives[$value] : '');
                 } else if($key == "owner") {
                    $datalist['data'][$kpa_id][$key] = (isset($users_list[$value]) ? $users_list[$value] : '');
                 } else {
                    $datalist['data'][$kpa_id][$key] = $value;
                 }                            
             }               
           }    
        }  
        return $datalist;
     }
     
     
}
?>
