<?php
class Kpa extends Model
{
     protected static $table      = "kpa";
     
     static $actionInfo           = array();
     
     static $kpiArr               = array(); 
     
     private static $hasActions   = true;
     
     private static $hasKpi       = true;
     
     const ACTIVE                 = 1;
     
     const DELETED                = 2;
     
     const TEMPLATEKPA            = 4;
     
     const IMPORTED               = 8;
     
     const INDIVIDUAL_OWNED       = 16;
     
     const UNSPECIFIED_KPA        = 32;
     
     const COPIED                 = 64;
    
     private $total_actions       = 0;
   
     private $total_kpi           = 0;     
     
	 function __construct()
	 {
	   parent::__construct();
	 }
	
	 public function get_kpa($userdata, $options)
	 {
	   $section    = $options['section'];
	   $headers    = array();
	   $kpa_change = array();
	   $settingObj = new Performancematrix();
	   $_options   = array('evaluate_on',
	                       'use_component',
	                       'self_evaluation',
	                       'manager_evaluation', 
	                       'joint_evaluation',
	                       'evaluation_review'
	                      );
	   $setting    = $settingObj -> get_matrix_setting($userdata['categoryid'],
	                                                   $userdata['default_year'],
	                                                   $options['componentid'],
	                                                   $_options
	                                                  );
        if(isset($options['headers']) && !empty($options['headers']))
        {
           $headers = $options['headers'];
           unset($options['headers']);
        }	     
	   $option_sql       = self::_kpaOptions($userdata, $options);
	   //if evaluation on kpa, then get all the kpa that are in the evaluation period
	   $kpa_evaluations  = array();
	   if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
	   {
	      if($setting['evaluate_on'] == "kpa")
	      {
	          /*$option_sql .= " AND STR_TO_DATE(K.deadline, '%d-%M-%Y') 
	                             BETWEEN '".date("Y-m-d", strtotime($_SESSION['evaluation_period']['start_date']))."' 
	                             AND '".date("Y-m-d", strtotime($_SESSION['evaluation_period']['end_date']))."'
	                           ";*/
	             $kpa_evaluations = $this -> _get_kpa_evaluations($setting, $options['componentid'], $userdata['default_year']);
	      }
	   } 
	   $sql        = "SELECT K.*, C.name AS componentname, KS.name AS status FROM #_".static::$table." K 
	                  INNER JOIN #_list_kpastatus KS ON KS.id = K.status  
   	                  INNER JOIN #_list_perfomancecomponents C ON C.id = K.componentid
	                  WHERE 1 $option_sql ";
	   $results    = $this -> db -> get($sql);
	   $userid     = (isset($userdata['tkid']) ? $userdata['tkid'] : "");
       if(isset($options['page']) && !empty($options['page']))
       {
          if($options['page'] == "new" || $options['page'] == "confirm")
          {
            $kpa_change  = self::_is_template_updated($results, $userdata, $options);     
          }
       }	   
        $response = KpaSorter::sort_kpa($results, new KpaEvaluation(), array('user'        => $userdata, 'section' => $section, 
                                                               'headers'     => $headers,  'setting' => $setting,
                                                               'evaluations' => $kpa_evaluations,
                                                               'componentid' => $options['componentid'],
                                                               'page'        => $options['page']
                                                               )
                                       );
       $response['kpa_change'] = $kpa_change;
       $response['userdata']   = $userdata;
	   return $response;
	}
	
	function _get_kpa_evaluations($setting, $componentid, $evaluationyear)
	{
	   $results = $this -> db -> get("SELECT E.* FROM #_kpa_evaluations E 
	                                  INNER JOIN #_kpa K ON K.id = E.kpa_id
	                                  WHERE E.evaluation_period_id = '".$_SESSION['evaluation_period']['id']."'
	                                  AND K.componentid = '".$componentid."' AND K.evaluationyear = '".$evaluationyear."'
	                                ");   
        $response = array();
        if(!empty($results))
        {
          foreach($results as $index => $eval)
          {
            $response[$eval['kpa_id']] = $eval; 
          }
        }	                                
	   return $response;                              
	}
	
	
	public static function _is_template_updated($current_kpa, $user, $options)
	{
	   $option_sql     = " AND evaluationyear = '".$user['default_year']."' AND componentid = '".$options['componentid']."' 
	                       AND kpastatus & ".Kpa::TEMPLATEKPA." = ".Kpa::TEMPLATEKPA." ";
	   $kpa_templates  = Kpa::findAll($option_sql);
        $response       = array();     
	   if(!empty($current_kpa))
	   {
	      foreach($current_kpa as $index => $kpa)
	      {
	         $kpa_ref        = $kpa['kparef']; 
	         if(isset($kpa_templates[$kpa_ref]))
	         {
	            $current_kpa_timestamp  = strtotime($kpa['modified']);
	            $template_kpa_timestamp = strtotime($kpa_templates[$kpa_ref]['modified']);
	            //there was an update to the template data, since the initial setup of the user kpas
	            if($template_kpa_timestamp > $current_kpa_timestamp)
	            {
	               $response['updated'][$kpa_ref][$kpa['id']] = $kpa['id'];
	               //echo "Diffference of the 2 should be ".$currentKpaTimeStamp." ".$templateKpaTimeStamp." means an update was done \r\n\n";
	            } 
	            unset($kpa_templates[$kpa_ref]);
	         }
	      }
	      if(isset($kpa_templates) && !empty($kpa_templates))
	      {
              $response['added'] = $kpa_templates;
	      }
	      return $response;
	   } else {
	     if(!empty($kpa_templates))
	     {
	        $response['added'] = $kpa_templates;
	        return $response;
	     } 
	     return FALSE;
	   }
	}
     
     function copy_from_template_save($kpa_added, $componentid, $userdata)
     {
        $res = 0;
        foreach($kpa_added as $kpa_id => $kpa)
        {        
          $kpa_data = $this -> db -> getRow("SELECT name, componentid,objective, outcome,measurement,proof_of_evidence, baseline, targetunit,
                                             nationalkpa, idpobjectives, performance_standard, target_period, weighting, competency, proficiency,
                                             categories, comments, deadline FROM #_kpa WHERE id = '".$kpa['id']."' 
                                           ");
          $kpa_data['kparef']         = $kpa['id'];
          $kpa_data['owner']          = $userdata['tkid'];
          $kpa_data['evaluationyear'] = $userdata['default_year'];
          $kpa_data['created']        = date('Y-m-d H:i:s');
          $kpa_data['kpastatus']      = 1 + Kpa::IMPORTED;
          $this -> save($kpa_data);
        }
        return $res;
     }
     
     function update_template_changes($kpa_updated)
     {
        $res  = 0;
        foreach($kpa_updated as $template_id => $kpa)
        {
          $template_updated = $this -> db -> get("SELECT name, componentid,objective, outcome,measurement,proof_of_evidence, baseline, targetunit,
                                                  nationalkpa, idpobjectives, performance_standard, target_period, weighting, competency, proficiency,
                                                  categories, comments FROM #_kpa WHERE id = '".$template_id."' ");
          if(!empty($kpa_updated))
          {    
            foreach($template_updated as $index => $template)
            {
               $this -> attach(new KpaAuditLog());
               $res += $this ->  update_where($template, array('kparef' => $template_id));   
            }
          }
        }
        return $res;
     }
     
	public static function copySave($componentid, $evaluationyear, $userdata)
	{
	    $kpas = Kpa::findAll(" AND componentid = '".$componentid."' AND kpastatus & ".Kpa::TEMPLATEKPA." = ".Kpa::TEMPLATEKPA." 
	                           AND evaluationyear = '".$evaluationyear."' 
	                        ");
	     if(!empty($kpas))
	     {
	          foreach($kpas as $kpaIndex => $kpa)
	          {
	             $kpaid = $kpa['id'];
	             unset($kpa['id']);
	             unset($kpa['insertuser']);
	             $kpa['owner']      = $userdata['tkid'];
	             $kpa['kparef']     = $kpaid;
	             $kpa['kpastatus']  = Kpa::ACTIVE + Kpa::IMPORTED;
	             $kpa['insertuser'] = $_SESSION['tid'];
	             $kpa['created']    = date('Y-m-d H:i:s');
	             $newkpaid          =  Kpa::saveData($kpa); 
	             if($newkpaid > 0)
	             {
	                 Kpi::copySave($kpaid, $newkpaid, $evaluationyear, $userdata);    
	             }
	          }
	     }
	}
	
	public static function copySaveToNewYear($componentid, $fromYear, $toYear)
	{
	    $kpas = Kpa::findAll(" AND componentid = '".$componentid."' AND evaluationyear = '".$fromYear."' AND kpastatus & ".Kpa::TEMPLATEKPA." = '".Kpa::TEMPLATEKPA."' ");
	     if(!empty($kpas))
	     {
	        foreach($kpas as $kpaIndex => $kpa)
	        {
	           $kpaid = $kpa['id'];
	           unset($kpa['id']);
	           unset($kpa['insertuser']);
	           $kpa['evaluationyear'] = $toYear;
	           $kpa['kparef']         = $kpaid;
	           $kpa['kpastatus']      = Kpa::ACTIVE + Kpa::COPIED + Kpa::TEMPLATEKPA;
	           $kpa['insertuser']     = $_SESSION['tid'];
	           $kpa['created']        = date('Y-m-d H:i:s');
	           $newkpaid              = Kpa::saveData($kpa); 
	           if($newkpaid > 0)
	           {
	               Kpi::copySaveToNewYear($kpaid, $newkpaid, $toYear);    
	           }
	        }
	     }
	}
	
	public function save_kpa($kpaData, $componentId, $section)
	{
	    $year     = EvaluationYear::findById($kpaData['evaluationyear']);
	    $response = array();
	    if(!Validation::dateDiff($kpaData['deadline'], $year['end']))
	    {
	       $response = array("text" => "KPA deadline date cannot be after the evaluation end date (".$year['end'].") ", "error" => true);
	    } else {
		  $userdata                  = User::getInstance() -> getUser();
		  $owner                     = ""; 
		  if(isset($kpaData['owner']) && !empty($kpaData['owner']))
		  {
		     $owner = $kpaData['owner'];
		     unset($kpaData['owner']);
		  } else {
		     $owner = $userdata['tkid'];
		  }
		  unset($kpaData['goaltype']);
		  unset($kpaData['parent']);
		  if(isset($kpaData['section']) && !empty($kpaData['section']))
		  {
		      if($section == "admin")
	           {
			    $kpaData['kpastatus'] = Kpa::ACTIVE + Kpa::TEMPLATEKPA;
		      }	
		      if($section == "new")
		      {
		         $kpaData['owner']     = $owner;
		         $kpaData['kpastatus'] = Kpa::ACTIVE + Kpa::INDIVIDUAL_OWNED;
		      }					
		  }		
		  unset($kpaData['section']);
		  $userDefaultYear = (isset($userdata['default_year']) ? $userdata['default_year'] : "");

		  if(isset($userdata['default_year']) && !empty($userdata['default_year']))
		  {
		      $kpaData['evaluationyear'] = $userDefaultYear; 
		  } else if(isset($kpaData['evaluationyear']) && !empty($kpaData['evaluationyear'])) {
		      $kpaData['evaluationyear'] = $kpaData['evaluationyear']; 
		  }
		  $kpaData['componentid'] = $componentId;
		  $kpaData['insertuser']  = $_SESSION['tid'];
		  $kpa['created']         = date('Y-m-d H:i:s');
		  $res                    = Kpa::saveData($kpaData);
	       return Message::save($res, " KPA #".$res);
	     }
	   return $response;
	}
	
	static function _kpaOptions($userdata, $options=array())
	{
	   $option_sql = "";
        $section    = "";
        if(isset($options['section']))
	   {
	      $section = $options['section'];
	      if($section == "admin")
	      {
	       $option_sql .= " AND K.kpastatus & ".Kpa::TEMPLATEKPA." = ".Kpa::TEMPLATEKPA." ";
	      }
	      if($section == "new")
	      {
	        $option_sql .= " AND (K.kpastatus & ".Kpa::IMPORTED." = ".Kpa::IMPORTED." 
	                         OR K.kpastatus & ".Kpa::INDIVIDUAL_OWNED." = ".Kpa::INDIVIDUAL_OWNED." ) AND K.owner = '".$userdata['tkid']."'  ";
     	   //$option_sql .= " AND PM.category_id = '".$userdata['categoryid']."' ";
	      } 		  
	      if($section == "manage")
	      {
	        $option_sql .= " AND K.owner = '".$userdata['tkid']."'  ";
	        //$option_sql .= " AND PM.category_id = '".$userdata['categoryid']."' ";
	      } 		  	         
	      unset($options['user']);
	      unset($options['section']);
	      unset($options['page']);
	    }	
     
	    if(!empty($options))
	    {
		 if(isset($options['evaluationyear']) && !empty($options['evaluationyear']))
		 {
		   $option_sql .= " AND K.evaluationyear = '".$options['evaluationyear']."'"; 
		   unset($options['evaluationyear']);
		 } else {
		   if(isset($userdata['default_year']) && !empty($userdata['default_year']))
		   {
               $option_sql .= " AND K.evaluationyear = '".$userdata['default_year']."'";		   
		   }
		   unset($options['evaluationyear']);
		 }
		 
	      $option_sql  .= parent::createANDSqlOptions($options);
	    }  
	    return $option_sql;  
	}
	
	public static function getKpa($userdata, $options)
	{
        $headers = array();
        if(isset($options['headers']) && !empty($options['headers']))
        {
           $headers = $options['headers'];
           unset($options['headers']);
        }
        $sqlOption  = self::_kpaOptions($userdata, $options);
        $sqlOption .= " AND kpastatus & ".Kpa::DELETED." <> ".Kpa::DELETED." ";
        $results    = Kpa::findAll($sqlOption);	
        $section    = (isset($options['section']) ? $options['section'] : "");
        $kpaChange  = array();
        if(isset($options['page']) && !empty($options['page']))
        {
          if($options['page'] == "new" || $options['page'] == "confirm")
          {
            $kpaChange  = Kpa::_isTemplateUpdated($results, $userdata, $options);     
          }
        }
        $compdata   = Component::findById($options['componentid']);
	    $userid     = (isset($userdata['tkid']) ? $userdata['tkid'] : "");
        $response   = Sorter::sortHeaders($results, array("component" => $compdata,
                                                        "type"      => "kpa",
                                                        "user"      => $userid,
                                                        "section"   => $section,
                                                        "headers" => $headers
                                                       )
                                         );		
	
	    $evaluationData     = array();
	    $evaluationStatuses = array();
	    $evaluationSettings = array();
		if(isset($options['section']) && $options['section'] !== "admin")
		{
             $evaluationSettings = Performancematrix::getASetting( $userdata['categoryid'],
                                                                   $userdata['default_year'],
                                                         array("selfevaluation", "managerevaluation", "jointevaluation", "evaluationreview", "ratelevel")
                                                        );		     
             $evaluationOption  = array("componentid"    => $options['componentid'],
                                        "assessfor"      => $userdata['tkid'],
                                        "evaluationyear" => $userdata['default_year']
                                       );    	   
             $evaluationData     = Evaluation::getUserEvaluations($evaluationOption);  		
		}
        $evaluationStatuses = Evaluation::getEvaluationStatuses($response['data'], $userdata, $options['componentid']); 		
		self::_kpiData($response['data'], $userdata); 
		$kpaData  = array('data'                => $response['data'],
		                  'cols'                => $response['cols'],
		                  'headers'             => $response['columns'],
		                  'total'               => $response['total'],
		                  'goals'               => $compdata,
		                  'kpiInfo'             => self::$kpiArr,
		                  'actionInfo'          => self::$actionInfo,
                          'evaluationSetting'   => $evaluationSettings,
                          'evaluations'         => $evaluationData,
                          'statusOfEvaluations' => $evaluationStatuses,
                          'hasActions'          => self::$hasActions,
                          'hasKpi'              => self::$hasKpi,
                          'kpaChange'           => $kpaChange, 
                          'userdata'            => $userdata
		                 );	
		return $kpaData;	     
	}

	
	function getComponentKpa($user, $componentCategories, $options)
	{
        $sqlOption = self::_kpaOptions($user, $options);
        $results   = Kpa::findAll($sqlOption);   
        $section   = (isset($options['section']) ? $options['section'] : "");
	   $userid    = (isset($userdata['tkid']) ? $userdata['tkid'] : "");
	   $response  = Sorter::sortHeaders($results, array("component" => $componentCategories,
	                                                   "type"      => "kpa",
	                                                   "user"      => $user,
	                                                   "section"   => $section,
	                                                   "headers"   => array(),
	                                                  )
	                                  );	
	   $componentKpa = array();
	   if(isset($response['data']) && !empty($response['data']))
	   {
	     //$componentKpa = $response['data'];
	     //$componentKpa['columns'] = $response['columns'];
	     //$componentKpa['cols']    = $response['cols'];
	     foreach($response['data'] as $kpaId => $kpa)
	     {
	        $componentKpa[$kpa['name']] = array();
	        $options['kpaid']           = $kpaId;
	        $componentKpa[$kpa['name']] = Kpi::getKpaKpi($user, $componentCategories, $options);                                
	     }	   
	   }                                                              
	   return $componentKpa;
	}
	
	static function _kpiData($kpaData, $userdata)
	{
	   foreach($kpaData as $index => $kpa)
        {
	      $sqlOption = " AND kpaid = '".$index."' AND kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED." AND owner = '".$userdata['tkid']."'";
	 	 $kpis      = Kpi::findAll($sqlOption); 
	 	 if(empty($kpis))
	 	 {
	 	    self::$hasKpi = false;
	 	 }
	      $ids  = array();
	      //$kpaProgress = 0;
	      foreach($kpis as $i => $kpi)
	      {
             //echo $kpi['id']." ".$kpi['kpistatus']." \r\n\n";
	        if(($kpi['kpistatus'] & Kpi::KPIACTION_IMPORTED) == kpi::KPIACTION_IMPORTED)
	        {
	          $ids[$kpi['id']] = $kpi['kpistatus'];
	          $moduleActions   = KpiMapping::getModuleActions($kpi, $kpi['modulemapping']);	
	          if(empty($moduleActions['ids']))
	          {
	             //self::$hasActions = false;
	          }
	          self::$actionInfo[$kpi['id']] = $moduleActions['ids'];
	        } else {
	          $ids[$kpi['id']] = $kpi['kpistatus'];
	          self::_actionInfo($kpi);	
	        }
	        //$kpiProgress = Kpi::calculateKpiProgress($kpi['id']);
	        //$kpaProgress += $kpiProgress;
	      }
	      self::$kpiArr[$index]  = array( "total" => count($kpis), "ids" => $ids ); 
		 
	   }
	}
	
	static function _actionInfo($kpi)
	{
          $sqlOption  = " AND kpiid = '".$kpi['id']."' AND actionstatus & ".Action::DELETED." <> ".Action::DELETED." ";
          $kpiActions = Action::findAll($sqlOption);
          if(!empty($kpiActions) )
          {
             foreach($kpiActions as $aId => $action)
             {
                self::$actionInfo[$kpi['id']][$action['id']] = $action['id'];
             }
          } else {
             self::$hasActions = false;
          }
	}
	
	public static function createUnspecifiedKPA($userid)
	{
	   $kpa = array();
	   $componentData         = Component::findById(2);
	   $evaluationYear        = Evaluationyear::findById($componentData['evaluationyear']);
	   $kpa['componentid']    = 3;
	   $kpa['kparef']         = "";
	   $kpa['name']           = "Unspecified KPA";
	   $kpa['objective']      = "Unspecified";
	   $kpa['outcome']	      = "Unspecified";
	   $kpa['measurement']    = "Unspecified";
	   $kpa['owner']          = $userid;
	   $kpa['proof_of_evidence']  = "Unspecified";
	   $kpa['baseline']	      = "Unspecified";
	   $kpa['targetunit']     = "Unspecified";
	   $kpa['nationalkpa']    = 0;
	   $kpa['idpobjectives']  = 0;
	   $kpa['performance_standard'] = "Unspecified";
	   $kpa['target_period']  = "Unspecified"; 
	   $kpa['weighting']      = 1;
	   $kpa['competency']     = 1;
	   $kpa['proficiency']	  = 1;
	   $kpa['categories']     = 1;
	   $kpa['deadline']	      = date("d-M-Y", strtotime($evaluationYear['end'])); 
	   $kpa['remindon']	      = "";
	   $kpa['comments']	      = "";
	   $kpa['status']         = 1;
	   $kpa['kpastatus']      = 1 + Kpa::UNSPECIFIED_KPA;
	   $kpa['evaluationyear'] = $componentData['evaluationyear'];
	   $kpa['insertuser']     = $_SESSION['tid'];
	   $kpa['created']        = date('Y-m-d H:i:s');
	   $res                   = Kpa::saveData($kpa);
	   if($res > 0)
	   {
	     Kpi::createUnspecifiedKpi($res , $userid, $componentData['evaluationyear'], $kpa['deadline']);
	   }
	}

     public static function calculateKpaProgress($id, $status, $userdata)
     {
        $optionSql    = " AND kpaid = '".$id."' AND kpistatus & ".Kpi::DELETED." <> ".kpi::DELETED." ";
        $kpis         = Kpi::findAll($optionSql);
        $kpaProgress  = 0;
        $totalKpi     = 0;
        foreach($kpis as $index => $kpi)
        {
          $kpiProgress  = Kpi::calculateKpiProgress($kpi['id'], $kpi['kpistatus'], $userdata);
          $kpaProgress += $kpiProgress;
          $totalKpi    += 1;
        } 
        $progress = 0;
        if($totalKpi !== 0)
        {
           $progress = round(($kpaProgress / $totalKpi), 2);
        } 
        return $progress;
     }
	
	function generateReport($postData, Report $reportObj)
	{
	   $joinWhere       = $this -> createJoins($postData['value']);
	   $includeprogress = false;
	   if(isset($postData['kpa']['progress']))
	   {
	     $includeprogress = true;
	     unset($postData['kpa']['progress']);
	   }
	   $fields   = $reportObj -> prepareFields($postData['kpa'], "K");
	   $headers  = array();
	   foreach($postData['kpa'] as $field => $val)
	   {
	     $headers[] = $field;
	   }
	   if($includeprogress)
	   {
	     array_push($headers, "progress");
	   }	   
	   $groupBy           = $reportObj -> prepareGroupBy($postData['group_by'], "K"); 
	   $sortBy            = $reportObj -> prepareSortBy($postData['sort'], "K");
	   $whereStr          = $reportObj -> prepareWhereString($postData['value'], $postData['match'], "K");   
	   $whereStr         .= $joinWhere;
	   $results           = $this -> filterResults($fields, $whereStr, $groupBy, $sortBy, "");
        $response          = KpaSorter::sort_kpa($results, array("type" => "kpa", "headers" => $headers));	
        $response['title'] = $postData['report_title'];
        $reportObj -> displayReport($response, "on_screen", "data");	   
	   exit();
	}
     
     function filterResults($fields, $values = "", $groupBy = "", $sortBy = "", $joins = "")
     {
       $orderBy = (!empty($sortBy) ? " ORDER BY $sortBy " : "");
       $groupBy = (!empty($groupBy) ? " GROUP BY $groupBy " : "");
	  $results = $this -> db -> get("SELECT K.id, K.kpastatus, K.status, ".$fields." FROM #_kpa K
	                                   INNER JOIN #_user_component UC ON UC.componentid = K.componentid 
	                                   $joins
				                    WHERE 1  AND K.kpastatus & ".Kpa::TEMPLATEKPA." <> ".Kpa::TEMPLATEKPA." 
				                    AND K.kpastatus & ".Kpa::DELETED." <> ".Kpa::DELETED." 
				                    AND UC.status & ".UserComponent::ACTIVATED." = ".UserComponent::ACTIVATED."
				                    $values
				                    $groupBy
				                    $orderBy 
				                  ");	                  
	  return $results;	          
     }  	
	
	function createJoins($options = array())
	{
        $joinStr  = "";
        $whereStr = "";	   
        if(isset($options['nationalkpa']) && !empty($options['nationalkpa']))
        {
           $natStr = "";
           foreach($options['nationalkpa'] as $nkpaIndex => $nationalkpa)
           {
              $natStr .= "  K.nationalkpa = '".$nationalkpa."' OR";
           }  
           if(!empty($natStr))
           {
             $whereStr .= " AND (".rtrim($natStr, 'OR').") ";
           }
        }
        if(isset($options['idpobjectives']) && !empty($options['idpobjectives']))
        {
           $idpStr = "";
           foreach($options['idpobjectives'] as $idpIndex => $idp)
           {
              $idpStr .= " K.idpobjectives = '".$idp."' OR";
           }  
           if(!empty($idpStr))
           {
             $whereStr .= " AND (".rtrim($idpStr, 'OR').") ";
           }
        }
        if(isset($options['weighting']) && !empty($options['weighting']))
        {
           $weightStr = "";
           foreach($options['weighting'] as $weigtIndex => $weighting)
           {
              $weightStr .= "  K.weighting = '".$weighting."' OR";
           } 
           if(!empty($weightStr))
           {
              $whereStr .= " AND (".rtrim($weightStr, 'OR').") ";
           }
        }  
        if(isset($options['owner']) && !empty($options['owner']))
        {
           $userStr = "";
           foreach($options['owner'] as $ownerIndex => $owner)
           {
              $userStr .= "  K.owner = '".$owner."' OR";
           }  
           if(!empty($userStr))
           {
              $whereStr .= " AND (".rtrim($userStr, "OR").") ";
           } 
        }          
       return $whereStr;          
	}

	function get_used_kpastatuses($option_sql = "")
	{
		$results  = $this -> db -> get("SELECT status FROM #_kpa WHERE 1 $option_sql GROUP BY status");
		$statuses = array();
		foreach($results as $index => $val)
		{
			$statuses[$val['status']] = $val['status'];
		}
		return $statuses;
	}
	
  function get_used_national_kpas()
  {
     $results     = $this -> db -> get("SELECT nationalkpa FROM #_kpa WHERE 1 $option_sql  GROUP BY nationalkpa");
     $nationalkpa = array();
     foreach($results as $index => $val)
     {
         $nationalkpa[$val['nationalkpa']] = $val['nationalkpa'];
     }
     return $nationalkpa;
  }  

  function get_used_nation_objective()
  {
     $results       = $this -> db -> get("SELECT idpobjectives FROM #_kpa WHERE 1 $option_sql  GROUP BY idpobjectives");
     $idpobjectives = array();
     foreach($results as $index => $val)
     {
         $idpobjectives[$val['idpobjectives']] = $val['idpobjectives'];
     }
     return $idpobjectives;
  }    	
}
