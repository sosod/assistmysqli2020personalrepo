<?php
class UserAuditLog extends AuditLog
{
     
    function notify($postData, $where, $tablename)
    {
       $user     = array();
       if(!empty($where))
       {
          $user = UserSetting::findById($where['id']);
       }
       $userdata = User::getUser(array('user' => $postData['user_id']));           
        //get all the user access constants uses in setting user access statuses
       $userAccessConstants = UserSetting::userConstants();
       $changes             = array();
       if($postData['status'] != $user['status'])
       { 
            foreach($userAccessConstants as $constant => $constantValue)
            {
                $constactName = ucfirst(strtolower(str_replace("_", " ",$constant)));
                  //if the user has the status alreay set , ie its already a yes, then check if its been changed to a no 
                if(($user['status'] & $constantValue) == $constantValue)
                {
                    if(($postData['status'] & $constantValue) != $constantValue)
                    {
                       $changes[$constant] =  $userdata['user']." ".$constactName." status has changed from <b>Yes</b> to <b>No</b> ";
                    }
                } else {
                    //if the user status constant was not already set , then it changed from No to Yes
                   if(($postData['status'] & $constantValue) == $constantValue)
                   {
                      if($constantValue == 2)
                      {
                         $changes[$constant] =  $userdata['user']." status has changed from <b>active</b> to <b>deleted</b>";
                      } else {
                         $changes[$constant] =  $userdata['user']." ".$constactName." status has changed from <b>No</b> to <b>Yes</b>";
                      }
                   }
                }
            }
       }              
       if(!empty($changes))
       { 
         $changes['user']          = $_SESSION['tkn'];
         $insertdata['insertuser'] = $_SESSION['tid'];
         $insertdata['changes']    = serializeEncode($changes);
         static::$table = "user_logs";
         if(!empty($insertdata))
         {
           parent::saveData($insertdata);
         }   
       }       
    }     
          
     
}
