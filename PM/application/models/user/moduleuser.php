<?php
class ModuleUser extends BaseUser
{
     function __construct()
     {
        parent::__construct();
     }     
     
     public static function getUsers($options="")
     {
        $results  = DBConnect::getInstance()->debug("SELECT * FROM assist_".$_SESSION['cc']."_timekeep TK 
                                                   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid
                                                   WHERE TK.tkid <> 0000 AND TK.tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."'
                                                   ORDER BY TK.tkname $options
                                                  ");   
        return $results;    
     }
     
     public static function getUser($options="")
     {
        $results  = DBConnect::getInstance()->getRow("SELECT * FROM assist_".$_SESSION['cc']."_timekeep TK 
                                                      INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid
                                                      WHERE TK.tkid <> 0000 AND TK.tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."'
                                                      ORDER BY TK.tkname $options
                                                     ");   
        return $results;    
     }
     
     function getSubOrdinates($options="")
     {
       $result = parent::query("SELECT TK.tkid, TK.tksurname, TK.tkname FROM assist_".$_SESSION['cc']."_timekeep TK
                                WHERE TK.tkmanager = '".$_SESSION['tid']."'
                               ");
       return $result;         
     } 

}
?>
