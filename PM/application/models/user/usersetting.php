<?php
class UserSetting extends BaseUser
{
     protected static $table = "user_setting";
     
    //defining user access constants
	const MODULE_ADMIN 		= 1;//1000000000	
	const DELETED  	     = 2;   //00000001
	const CREATE_COMPONENT   = 4;
	const ADMIN_ALL   		= 8;   //00000010
	const UPDATE_ALL		= 16;  //00001000
	const VIEW_ALL		     = 32;	//00010000		 
	const EDIT_ALL			= 64;  //00100000
	const REPORTS			= 128; //01000000
	const ASSURANCE 		= 256; //10000000
	const EVALUATION 		= 512; //10000000
	const SETUP		 	= 1024; //100000000	    
	const DONEINTIALSETUP    = 2048;  
     
     public function __construct()
     {
        parent::__construct();
     }
     
    public static function fetchUserSetting($options = "")
    {
        $result = DBConnect::getInstance()->getRow("SELECT U.id, U.default_year, U.status, EY.start, EY.end, EY.lastday  
                                                    FROM #_user_setting U
                                                    INNER JOIN #_list_evaluation_years EY ON EY.id = U.default_year
                                                    WHERE U.user_id = '".$_SESSION['tid']."' $options
                                                  ");
        return $result;
    }   
    
     
     public static function getUsers($options="")
     {
        $results  = DBConnect::getInstance()->get("SELECT * FROM assist_".$_SESSION['cc']."_timekeep TK 
                                                   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid
                                                   LEFT JOIN #_user_setting US ON US.user_id = TK.tkid 
                                                   WHERE TK.tkid <> 0000 AND TK.tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."'
                                                   ORDER BY TK.tkname $options");   
        return $results;  
     }
     
     public static function getUser($options="")
     {
        $results = DBConnect::getInstance() -> getRow("SELECT * FROM assist_".$_SESSION['cc']."_timekeep TK 
                                                   INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid
                                                   LEFT JOIN #_user_setting US ON US.user_id = TK.tkid 
                                                   WHERE TK.tkid <> 0000 AND TK.tkstatus = 1 AND MU.usrmodref = '".$_SESSION['modref']."'
                                                   ORDER BY TK.tkname $options");   
        return $results;    
     }

    public static function initialSetupDone($user)
    {       
       $unusedComponents   = UserComponent::getUnusedComponents($user);
       $userevaluationyear = DBConnect::getInstance() -> getRow("SELECT * FROM #_user_evaluation_years 
                                                                 WHERE user_id = '".$user['tkid']."' 
                                                                 AND evaluationyear = '".$user['default_year']."' 
                                                               ");
       if(!empty($userevaluationyear))
       {
          if(!empty($unusedComponents))
          {
             //edited on 2013-29-01;
             $u_sql = "";
             foreach($unusedComponents as $u_index => $val)
             {
                $u_sql .= " id = '".$val."' OR";
             }
             $unusedSql  = "AND evaluationyear = '".$user['default_year']."' AND (".rtrim($u_sql, "OR").")";
             $components = Component::findAll($unusedSql);          
             return $components;
          }
         return TRUE;
       }
       return FALSE;
    }
       
    public static function setupDone($user)
    {
       $insertdata = array('user_id' => $user['tkid'], 'evaluationyear' => $user['default_year']);
       $res = DBConnect::getInstance() -> insert('user_evaluation_years', $insertdata);
       return $res;
    }
     
    public function update_user_setting($data)
    {
       $userdata['status']  = self::calculateUserSettingStatus($data);  
       $userdata['user_id'] = $data['userselect'];
       if(isset($data['id']) && !empty($data['id']))
       {
          $this -> attach(new UserAuditLog());    
          $res = $this -> update_where($userdata, array('id' => $data['id']));
          return Message::update($res, 'user access settings ');
       } else {
          $userdata['created']  = date('Y-m-d H:i:s');
          $res                  = $this -> save($userdata);  
          return Message::save($res, ' user access settings ');
       }
    }
      
	/*
	  calculate the user status of the user as per user-access constants 
	*/
	public static function calculateUserSettingStatus($userStatuses)
	{
		$userAccessConstants = self::userConstants();
		$status  = 0;	
		foreach($userAccessConstants as $constant => $constantVal)
		{
		   $constantName = strtolower($constant);
		   if(isset($userStatuses[$constantName]))
		   {
		     if($userStatuses[$constantName] == 1)
		     {     
		       $status += $constantVal;
		     }
		   }
		}    
		return $status;
	}
	/*
	 get an array of the user-access class constants
	*/
     public static function userConstants()
     {
        $oClass              = new ReflectionClass("UserSetting");
        $userAccessConstants = $oClass->getConstants();        
        return $userAccessConstants;
     }
      
}
?>
