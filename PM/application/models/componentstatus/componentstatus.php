<?php
class ComponentStatus extends Model
{
	protected static $table = "list_componentstatus";
	
	function __construct()
	{
		parent::__construct();
	}
     
     function update_component_status($data)
     {
        $this -> attach(new SetupAuditLog("Component Status"));
	   $res = $this -> update_where($data['data'], array("id" => $data['id']));
	   return $res;
     }
	
}
