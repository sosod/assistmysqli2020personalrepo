<?php
class PerformanceMatrixAuditLog extends AuditLog
{
     private $currentStatus = "";
     
     function __construct()
     {
        parent::__construct();
     }
     
     function notify($postData, $where, $tablename)
     {

	    $result = $this -> db -> getRow("SELECT * FROM #_matrix WHERE category_id = '".$where['category_id']."' 
	                                     AND evaluation_year = '".$where['evaluation_year']."' 
	                                     AND component_id = '".$where['component_id']."' 
	                                     LIMIT 0, 1
	                                    ");
        $changes = array();	 
        $yesno   = array('use_competincies', 'use_proficiencies', 'self_evaluation', 'manager_evaluation', 'use_kpi_weighting', 
                          'manager_evaluation', 'joint_evaluation', 'evaluation_review', 'use_template', 'use_kpa_weighting',
                          'use_action_weighting'
                        );      
        if(!empty($result))
        {
           foreach($result as $key => $val)
           {
             if(isset($postData[$key]) && $postData[$key] != $val)
             {
                if($key == "evaluation_frequency")
                {
                   $from_freq = Evaluationfrequencies::findById($val);
                   $to_freq   = Evaluationfrequencies::findById($postData[$key]);
                   $changes[$key] = array("from" => $from_freq['name'], "to" => $to_freq['name']);                     
                } else { 
                   if(in_array($key, $yesno))
                   {
                      $from = ($val == 1 ? "Yes" : "No");
                      $to   = ($postData[$key] == 1 ? "Yes" : "No");
                      $changes[$key] = array("from" => $from , "to" => $to);                    
                   } else {
                      $changes[$key] = array("from" => ucwords($val), "to" => ucwords($postData[$key]));
                   }        
                }
             }
           }
        }         
        if(!empty($changes))
        { 
           $changes['user']          = $_SESSION['tkn'];
           $insertdata['insertuser'] = $_SESSION['tid'];
           $insertdata['changes']    = serializeEncode($changes);
           static::$table = $tablename."_logs";
           if(!empty($insertdata))
           {
             parent::saveData($insertdata);
           }   
        }
     }     
   
     
}
?>
