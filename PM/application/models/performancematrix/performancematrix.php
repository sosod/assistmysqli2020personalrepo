<?php
class Performancematrix extends Model
{

     protected static $table = "performancematrix";

	function __construct()
	{
		parent::__construct();
	}
	
	/*
	 get perfomanace matrix settings for a specified user-category
	 @param integer usercategory id
	 @param integer evaluation year
	*/
	private static function matrixSettings($usercategoryid, $evaluationyear)
	{
	   $matrixSettings = array();
	   $optionSql = " AND categoryid = '".$usercategoryid."' AND evaluationyear = '".$evaluationyear."' "; 
	   $categoryMatrix = Performancematrix::find($optionSql);
	   if(!empty($categoryMatrix))
	   {
	      $matrixSettings = @unserialize(base64_decode($categoryMatrix['matrix']));
	   }
	   return $matrixSettings;
	}
	
	/*
	 get specific settings from the performance matric of a performance category
	 @param integer user category id
	 @params array $options specififying matrix setting requested
     */
	public static function getASetting($usercategoryid, $evaluationyear, $options = array())
	{
	   $matrixSettings = self::matrixSettings($usercategoryid, $evaluationyear);
 	   $requestedSettings = array();
	   if(!empty($matrixSettings))
	   {
	     foreach($options as $index => $setting)
	     {
	        if($matrixSettings[$setting])
	        {
	           $requestedSettings[$setting] = $matrixSettings[$setting];
	        }
	     }	     
	   }
	   return $requestedSettings;
	}

	
	public function get_matrix_setting($categoryid, $evaluationyear, $componentid,  $setting = array())
	{
	   $fields = "";
	   $fields  = (!empty($setting) ? implode(',', $setting) : '*');
	   $results = $this -> db -> getRow("SELECT ".$fields." FROM #_matrix WHERE category_id = '".$categoryid."' 
	                                     AND evaluation_year = '".$evaluationyear."' AND component_id = '".$componentid."' 
	                                     LIMIT 0, 1
	                                    ");
        return $results;
	}
	
  function get_frequencies_used($option_sql = "")
  {
      $results      = $this -> db -> get("SELECT evaluation_frequency FROM #_matrix $option_sql");
      $matrix_freq  = array();  
      foreach($results as $index => $matrix)
      {
         $matrix_freq[$matrix['evaluation_frequency']] =  $matrix['evaluation_frequency']; 
      }
      return $matrix_freq;
  }

  function get_categories_used($option_sql = "")
  {
      $results      = $this -> db -> get("SELECT category_id FROM #_matrix WHERE 1 $option_sql GROUP BY category_id");
      $matrix_cats  = array();  
      foreach($results as $index => $matrix)
      {
         $matrix_cats[$matrix['category_id']] =  $matrix['category_id']; 
      }
      return $matrix_cats;
  }

  function get_matrix_component_used($option_sql = "")
  {
      $results       = $this -> db -> get("SELECT component_id FROM #_matrix WHERE 1 $option_sql GROUP BY component_id");
      $matrix_compts = array();  
      foreach($results as $index => $matrix)
      {
         $matrix_compts[$matrix['component_id']] =  $matrix['component_id']; 
      }
      return $matrix_compts;
  }  

	
	public function get_component_matrix($categoryid, $evaluationyear)
	{
	   $results = $this -> db -> get("SELECT * FROM #_matrix  WHERE category_id = '".$categoryid."' 
	                                  AND evaluation_year = '".$evaluationyear."'
	                                ");
	   $matrix  = array();
	   if(!empty($results))
	   {
	     foreach($results as $index => $component_matrix)
	     {
	        $matrix[$component_matrix['component_id']] = $component_matrix;
	     }
	   }
	   return $matrix;
	}
	
	public function get_total_weights($categoryid, $evaluationyear)
	{
	   $results = $this -> db -> getRow("SELECT SUM(component_weight) as totalW FROM #_matrix WHERE category_id = '".$categoryid."' 
	                                     AND evaluation_year = '".$evaluationyear."'
	                                    ");
	   return $results;
	}	
	
	function update_matrix($matrixData)
	{
	    $categoryid     = $matrixData['categorytid'];
	    $evaluationyear = $matrixData['evaluationyear'];
	    $usecomponent   = $matrixData['use_component'];
	    unset($matrixData['evaluationyear']);
	    unset($matrixData['categorytid']);
	    unset($matrixData['use_component']);
	    $update_matrix_data = $this -> _prepare_matrix($matrixData, $usecomponent, $categoryid);  
	    $res                = 0;
	    parent::setTable('matrix');
	    $this -> attach(new PerformanceMatrixAuditLog());
	    foreach($update_matrix_data as $component_id => $matrix)
	    {
	      $where = array('component_id' => $component_id, 'category_id' => $categoryid, 'evaluation_year' => $evaluationyear);
          $res += $this -> update_where($matrix, $where);    	          
	    }
	    
	   return $res;  	     
	}
	
	function save_matrix($matrixData)
	{
	    $categoryid     = $matrixData['categorytid'];
	    $evaluationyear = $matrixData['evaluationyear'];
	    $usecomponent   = $matrixData['use_component'];
	    unset($matrixData['evaluationyear']);
	    unset($matrixData['categorytid']);
	    unset($matrixData['use_component']);
	    $update_matrix_data = $this -> _prepare_matrix($matrixData, $usecomponent, $categoryid);    	    
	    $res                = 0;
	    parent::setTable('matrix');
	    foreach($update_matrix_data as $component_id => $matrix)
	    {
	        $matrix['component_id']    = $component_id;
	        $matrix['category_id']     = $categoryid;
	        $matrix['evaluation_year'] = $evaluationyear;    	        
         $res += $this -> save($matrix);    	          
	    }
	   return $res;  	     
	}
	
	function _prepare_matrix($matrixData, $usecomponent, $categoryid)
	{
	    $matrix_data    = array();
	    foreach($matrixData as $key => $matrix)
	    {
	       foreach($usecomponent as $component_id => $component)
	       {
	          if(isset($matrix[$component_id]))
	          {
	             if(isset($matrix[$component_id][$categoryid]))
	             {
	               $matrix_data[$component_id][$key] = $matrix[$component_id][$categoryid];
	             }
	          }
	       }
	    }
	    return $matrix_data;	     
	}
	
	function matrix_report($options)
	{
	    $users      = array();
        $user_obj   = new User();
        $ratingObj  = new Ratingscale();
        $ratingto   = $ratingObj -> get_max_rating_scale();
        
        $user_sql   = "";
        $userdata   = array(); 
        $allusers   = $user_obj -> get_users_list();
        $year       = array();        
        if(isset($options['evaluationyear']))
        {
           $year  = EvaluationYear::findById($options['evaluationyear']);        
        }            
        if(isset($options['employee']) && !empty($options['employee']))
        {
           $user_sql = " AND TK.tkid = '".$options['employee']."' ";
           $userdata = $user_obj -> get_user(array('user' => $options['employee']));
        }
        if(isset($options['department']) && !empty($options['department']))
        {
           $user_sql .= " AND LD.id = '".$options['department']."' ";
        }
        if(isset($options['job_level']) && !empty($options['job_level']))
        {
            $user_sql .= " AND JL.id = '".$options['job_level']."' ";
        }
        $users  = $user_obj -> get_users($user_sql);
        if(isset($userdata) && !empty($userdata))
        {
           if(!isset($options['employee']) || empty($options['employee']))
           {
              $users =  array_merge(array(array_slice($userdata, 0, -1)), $users);
           }
        }
        $period = array();
        if(isset($options['evaluationperiod']) && !empty($options['evaluationperiod']))
        {
          $evaluationPeriodObj = new EvaluationPeriod();
          $period_sql          = " AND id = '".$options['evaluationperiod']."' "; 
          $period              = $evaluationPeriodObj -> get_evaluation_period($period_sql);                
        }
        $evaluationyear = array();
        if(isset($options['evaluationyear']) && !empty($options['evaluationyear']))
        {
          $evaluationyear = Evaluationyear::findById($options['evaluationyear']);
        }
        $usermatrix = array();
        if(!empty($users))
        {
            $i = 0;
           foreach($users as $index => $user)
           {
              //echo "User is ".$user['user']." -- \r\n\n";
              $usermatrix[$i]['user']['user']      = $user['user'];
              $usermatrix[$i]['user']['manager']   = ($user['tkmanager'] == "S" ? $user['user'] : $allusers[$user['tkmanager']]);
              $usermatrix[$i]['user']['joblevel']  = $user['joblevel'];
              $usermatrix[$i]['user']['category']  = $user['performancecategory'];
              $usermatrix[$i]['user']['period']    = $period['start']." - ".$period['end'];  
              $usermatrix[$i]['user']['year']      = $evaluationyear['start']." - ".$evaluationyear['end']; 

              $optional_sql  = "";              
              $optional_sql .= " AND UC.userid = '".$user['tkid']."' AND M.category_id = '".$user['categoryid']."'";              
              if(isset($options['evaluationyear']))
              {
                $optional_sql .= " AND UC.evaluationyear = '".$options['evaluationyear']."' ";
              } 

              $usercomponentObj = new UserComponent();
              $usercomponents   = $usercomponentObj -> get_component($optional_sql);   
              $self_score     = 0;
              $manager_score  = 0;
              $joint_score    = 0;
              $review_score   = 0;
              $evaluationon   = 0;
              if(!empty($usercomponents))
              {
                 $self          = 0;
                 $manager       = 0;
                 $joint         = 0;
                 $review        = 0;
                 $evaluation_on = array();  
                 foreach($usercomponents as $index => $usercomponent)
                 {
                    $usermatrix[$i]['component'][$usercomponent['id']]['componentname'] = $usercomponent['name'];
                    $usermatrix[$i]['component'][$usercomponent['id']]['weight']        = $usercomponent['component_weight'];
                    if($usercomponent['evaluate_on'] == "kpa")
                    {
                       $usermatrix[$i]['component'][$usercomponent['id']]['type'] = "KPA";
                       $kpa_matrix     = $this -> _get_kpa_matrix($usercomponent, $options);
                       $self    = $kpa_matrix['self_avg'];
                       $manager = $kpa_matrix['manager_avg'];
                       $joint   = $kpa_matrix['joint_avg'];
                       $review  = $kpa_matrix['review_avg'];
                       $self    = round(($kpa_matrix['self_avg'] * ($usercomponent['component_weight']/100)), 2);
                       $manager = round(($kpa_matrix['manager_avg'] * ($usercomponent['component_weight']/100)), 2);
                       $joint   = round(($kpa_matrix['joint_avg'] * ($usercomponent['component_weight']/100)), 2);
                       $review  = round(($kpa_matrix['review_avg'] * ($usercomponent['component_weight']/100)), 2);         
                       if(!empty($kpa_matrix))
                       {
                        //echo "Kpa evaluation -- ".$kpa_matrix['evaluation_date']."\r\n\n\n";
                        array_push($evaluation_on, $kpa_matrix['evaluation_date']);
                       $usermatrix[$i]['component'][$usercomponent['id']]['self_total']    = $kpa_matrix['self_total'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['manager_total'] = $kpa_matrix['manager_total'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['joint_total']   = $kpa_matrix['joint_total'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['review_total']  = $kpa_matrix['review_total'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['self_avg']      = $kpa_matrix['self_avg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['manager_avg']   = $kpa_matrix['manager_avg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['joint_avg']     = $kpa_matrix['joint_avg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['review_avg']    = $kpa_matrix['review_avg'];
                       
                       $usermatrix[$i]['component'][$usercomponent['id']]['selfrating']      = $kpa_matrix['selfratingavg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['managerrating']   = $kpa_matrix['managerratingavg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['jointrating']     = $kpa_matrix['jointratingavg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['reviewrating']    = $kpa_matrix['reviewratingavg'];

                       $usermatrix[$i]['component'][$usercomponent['id']]['totalweighting'] = $kpa_matrix['totalweight'];
                       
                       $usermatrix[$i]['component'][$usercomponent['id']]['self_score']    = $self;
                       $usermatrix[$i]['component'][$usercomponent['id']]['manager_score'] = $manager;
                       $usermatrix[$i]['component'][$usercomponent['id']]['joint_score']   = $joint;
                       $usermatrix[$i]['component'][$usercomponent['id']]['review_score']  = $review;                         
                       }
                       $usermatrix[$i]['component'][$usercomponent['id']]['data'] = $kpa_matrix['data'];
                    } else if($usercomponent['evaluate_on'] == "kpi") {
                       $usermatrix[$i]['component'][$usercomponent['id']]['type'] = "KPI";
                       $kpi_matrix = $this -> _get_kpi_matrix($usercomponent, $options);
                       $self    = $kpi_matrix['self_avg'];
                       $manager = $kpi_matrix['manager_avg'];
                       $joint   = $kpi_matrix['joint_avg'];
                       $review  = $kpi_matrix['review_avg'];               
                       $self    = round(($kpi_matrix['self_avg'] * ($usercomponent['component_weight']/100)), 2);
                       $manager = round(($kpi_matrix['manager_avg'] * ($usercomponent['component_weight']/100)), 2);
                       $joint   = round(($kpi_matrix['joint_avg'] * ($usercomponent['component_weight']/100)), 2);
                       $review  = round(($kpi_matrix['review_avg'] * ($usercomponent['component_weight']/100)), 2);         
                       if(!empty($kpi_matrix))
                       {
                        //echo "Kpi evaluation -- ".$kpi_matrix['evaluation_date']."\r\n\n\n";
                        array_push($evaluation_on, $kpi_matrix['evaluation_date']);
                       $usermatrix[$i]['component'][$usercomponent['id']]['self_total']    = $kpi_matrix['self_total'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['manager_total'] = $kpi_matrix['manager_total'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['joint_total']   = $kpi_matrix['joint_total'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['review_total']  = $kpi_matrix['review_total'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['self_avg']      = $kpi_matrix['self_avg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['manager_avg']   = $kpi_matrix['manager_avg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['joint_avg']     = $kpi_matrix['joint_avg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['review_avg']    = $kpi_matrix['review_avg'];
                       
                       $usermatrix[$i]['component'][$usercomponent['id']]['selfrating']    = $kpi_matrix['selfratingavg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['managerrating'] = $kpi_matrix['managerratingavg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['jointrating']   = $kpi_matrix['jointratingavg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['reviewrating']  = $kpi_matrix['reviewratingavg'];
                       
                       $usermatrix[$i]['component'][$usercomponent['id']]['totalweighting'] = $kpi_matrix['totalweight'];
                       
                       $usermatrix[$i]['component'][$usercomponent['id']]['self_score']    = $self;
                       $usermatrix[$i]['component'][$usercomponent['id']]['manager_score'] = $manager;
                       $usermatrix[$i]['component'][$usercomponent['id']]['joint_score']   = $joint;
                       $usermatrix[$i]['component'][$usercomponent['id']]['review_score']  = $review; 
                       }                           
                       $usermatrix[$i]['component'][$usercomponent['id']]['data'] = $kpi_matrix['data'];
                    } else if($usercomponent['evaluate_on'] == "action") {
                       $usermatrix[$i]['component'][$usercomponent['id']]['type'] = "Action";
                       $action_matrix = $this -> _get_action_matrix($usercomponent, $options);
                       $self    = round(($action_matrix['self_avg'] * ($usercomponent['component_weight']/100)), 2);
                       $manager = round(($action_matrix['manager_avg'] * ($usercomponent['component_weight']/100)), 2);
                       $joint   = round(($action_matrix['joint_avg'] * ($usercomponent['component_weight']/100)), 2);
                       $review  = round(($action_matrix['review_avg'] * ($usercomponent['component_weight']/100)), 2);  
                       if(!empty($action_matrix['data']))
                       {       
                          //echo "Action evaluation -- ".$kpi_matrix['evaluation_date']."\r\n\n\n";
                        array_push($evaluation_on, $action_matrix['evaluation_date']);
                        $usermatrix[$i]['component'][$usercomponent['id']]['self_total']    = $action_matrix['self_total'];
                        $usermatrix[$i]['component'][$usercomponent['id']]['manager_total'] = $action_matrix['manager_total'];
                        $usermatrix[$i]['component'][$usercomponent['id']]['joint_total']   = $action_matrix['joint_total'];
                        $usermatrix[$i]['component'][$usercomponent['id']]['review_total']  = $action_matrix['review_total'];
                       
                       $usermatrix[$i]['component'][$usercomponent['id']]['self_avg']      = $action_matrix['self_avg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['manager_avg']   = $action_matrix['manager_avg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['joint_avg']     = $action_matrix['joint_avg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['review_avg']    = $action_matrix['review_avg'];
                       
                       $usermatrix[$i]['component'][$usercomponent['id']]['selfrating']    = $action_matrix['selfratingavg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['managerrating'] = $action_matrix['managerratingavg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['jointrating']   = $action_matrix['jointratingavg'];
                       $usermatrix[$i]['component'][$usercomponent['id']]['reviewrating']  = $action_matrix['reviewratingavg']; 
                       
                       $usermatrix[$i]['component'][$usercomponent['id']]['totalweighting'] = $action_matrix['totalweight'];
                       
                       $usermatrix[$i]['component'][$usercomponent['id']]['self_score']    = $self;
                       $usermatrix[$i]['component'][$usercomponent['id']]['manager_score'] = $manager;
                       $usermatrix[$i]['component'][$usercomponent['id']]['joint_score']   = $joint;
                       $usermatrix[$i]['component'][$usercomponent['id']]['review_score']  = $review;     
                       }                  
                       $usermatrix[$i]['component'][$usercomponent['id']]['data'] = $action_matrix['data'];
                    }
                    $self_score       += $self;
                    $manager_score    += $manager;
                    $joint_score      += $joint;
                    $review_score     += $review;
                 }
                  //debug($evaluation_on);
                  $evaluationon      = max($evaluation_on);   
                  //echo "Evaluation on becomes ".date("d-M-Y", $evaluationon)." -- ".strtotime($evaluationon)."\r\n\n";               
                  $evaluationon                      = max($evaluation_on);   
                  $usermatrix[$i]['self']            = $self_score;
                  $usermatrix[$i]['manager']         = $manager_score;
                  $usermatrix[$i]['joint']           = $joint_score;
                  $usermatrix[$i]['review']          = $review_score;                 
                  $usermatrix[$i]['evaluation_date'] = ($evaluationon == "" ? "" : date("d-M-Y", $evaluationon));
              }
              $i++;
           }
        }        
        //debug($usermatrix);
        $data['report_date'] = date('d-M-Y');
        $data['usermatrix']  = $usermatrix;
        $data['ratingto']    = $ratingto;
        $data['year']        = $year;
        return $data;
	}
	
	function _get_kpa_matrix($usercomponent, $options)
	{
	    $kpaStatusObj = new KpaStatus();
	    $statuses     = $kpaStatusObj -> getKpaStatuses();
        $results = $this -> db -> get("SELECT KPA.* FROM #_kpa KPA WHERE 1 
                                        AND KPA.componentid = '".$usercomponent['id']."' 
                                        AND KPA.evaluationyear = '".$options['evaluationyear']."'
                                        AND KPA.kpastatus & ".Kpa::TEMPLATEKPA." <> ".Kpa::TEMPLATEKPA."
                                        AND KPA.kpastatus & ".Kpa::DELETED." <> ".Kpa::DELETED."
                                        AND KPA.owner = '".$usercomponent['userid']."'
                                      ");
         $res           = array();
         $self_total    = 0;
         $manager_total = 0;
         $joint_total   = 0;
         $review_total  = 0;     
         
         $selfrating    = 0;
         $managerrating = 0;
         $jointrating   = 0;
         $reviewrating  = 0;           
         
         $total_weight     = 0;               
         $evaluation_dates = array();
         $count            = 0;
         if(!empty($results))
         {   
           foreach($results as $index => $kpa)
           {
              $count++;
              $eval_result    = $this -> get_kpa_evaluations($kpa, $usercomponent, $options);
              $self_total    += $eval_result['self_score'];
              $manager_total += $eval_result['manager_score'];
              $joint_total   += $eval_result['joint_score'];
              $review_total  += $eval_result['review_score'];      
              
              $selfrating    += $eval_result['self_rating'];
              $managerrating += $eval_result['manager_rating'];
              $jointrating   += $eval_result['joint_rating'];
              $reviewrating  += $eval_result['review_rating']; 
              
              $total_weight  += $kpa['weighting'];
                 
              $res['data'][$kpa['id']]             = $eval_result;
              $res['data'][$kpa['id']]['deadline'] = $kpa['deadline'];
              $res['data'][$kpa['id']]['status']   = $statuses[$kpa['status']]['name'];
              $res['data'][$kpa['id']]['weight']   = $kpa['weighting'];
              $res['data'][$kpa['id']]['name']     = $kpa['name'];               
              array_push($evaluation_dates, $eval_result['date']);  
           }
           $res['evaluation_date'] = max($evaluation_dates);
           $res['self_total']    = $self_total;
           $res['manager_total'] = $manager_total;
           $res['joint_total']   = $joint_total;
           $res['review_total']  = $review_total;     
 
           $res['totalweight']    = $total_weight;
                             
           if($count > 0)
           {
               $res['selfratingavg']    = round(($selfrating/$count), 2);
               $res['managerratingavg'] = round(($managerrating/$count), 2);
               $res['jointratingavg']   = round(($jointrating/$count), 2);
               $res['reviewratingavg']  = round(($reviewrating/$count), 2);                
           }
           
           if($total_weight > 0)
           {
             $res['self_avg']    = round(($self_total/$total_weight), 2);
             $res['manager_avg'] = round(($manager_total/$total_weight), 2);
             $res['joint_avg']   = round(($joint_total/$total_weight), 2);
             $res['review_avg']  = round(($review_total/$total_weight), 2);
           }           
         }  
        return $res;
	}
	
	function _get_kpi_matrix($usercomponent, $options)
	{
	    $kpiStatusObj = new KpiStatus();
	    $statuses     = $kpiStatusObj -> getKpiStatuses();
         $results = $this -> db -> get("SELECT KPI.* FROM #_kpi KPI 
                                        INNER JOIN #_kpa KPA ON KPI.kpaid = KPA.id 
                                        AND KPA.componentid = '".$usercomponent['id']."' 
                                        AND KPA.evaluationyear = '".$options['evaluationyear']."'
                                        AND KPI.kpistatus & ".Kpi::KPIACTION_IMPORTED." <> ".Kpi::KPIACTION_IMPORTED."
                                        AND KPI.kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED."
                                        AND KPA.kpastatus & ".Kpa::TEMPLATEKPA." <> ".Kpa::TEMPLATEKPA."
                                        AND KPI.owner = '".$usercomponent['userid']."'
                                      ");
                                      
         $res              = array();
         $self_total       = 0;
         $manager_total    = 0;
         $joint_total      = 0;
         $review_total     = 0;    
         
         $selfrating       = 0;
         $managerrating    = 0;
         $jointrating      = 0;
         $reviewrating     = 0;         
         
         $total_weight     = 0;          
         $count            = 0;     
         $evaluation_dates = array();  
         if(!empty($results))
         {   
           foreach($results as $index => $kpi)
           {
              $count++;
              $eval_result    = $this -> get_kpi_evaluations($kpi, $usercomponent, $options);
              $self_total    += $eval_result['self_score'];
              $manager_total += $eval_result['manager_score'];
              $joint_total   += $eval_result['joint_score'];
              $review_total  += $eval_result['review_score']; 

              $selfrating    += $eval_result['self_rating'];
              $managerrating += $eval_result['manager_rating'];
              $jointrating   += $eval_result['joint_rating'];
              $reviewrating  += $eval_result['review_rating']; 

              $total_weight    += $kpi['weighting'];
              
              $res['data'][$kpi['id']]  = $eval_result;
              $res['data'][$kpi['id']]['deadline'] = $kpi['deadline'];
              $res['data'][$kpi['id']]['status']   = $statuses[$kpi['status']]['name'];
              $res['data'][$kpi['id']]['weight']   = $kpi['weighting'];    
              $res['data'][$kpi['id']]['name']     = $kpi['name'];     
              array_push($evaluation_dates, $eval_result['date']);        
           }
            $res['evaluation_date'] = max($evaluation_dates);
            $res['self_total']    = $self_total;
            $res['manager_total'] = $manager_total;
            $res['joint_total']   = $joint_total;
            $res['review_total']  = $review_total;
            
            $res['totalweight']    = $total_weight;      
            if($count > 0)
            {
               $res['selfratingavg']    = round(($selfrating/$count), 2);
               $res['managerratingavg'] = round(($managerrating/$count), 2);
               $res['jointratingavg']   = round(($jointrating/$count), 2);
               $res['reviewratingavg']  = round(($reviewrating/$count), 2);               
              
            }     
            
           if($total_weight > 0)
           {
             $res['self_avg']    = round(($self_total/$total_weight), 2);
             $res['manager_avg'] = round(($manager_total/$total_weight), 2);
             $res['joint_avg']   = round(($joint_total/$total_weight), 2);
             $res['review_avg']  = round(($review_total/$total_weight), 2);
           }       
         }   
        return $res;
	}
	
	function _get_action_matrix($usercomponent, $options)
	{
        $option_sql  = "";
        $actionStatusObj = new ActionStatus();
        $statuses        = $actionStatusObj -> getActionStatuses();
        if(isset($options['evaluationperiod']) && !empty($options['evaluationperiod']))
        {
          $evaluationPeriodObj = new EvaluationPeriod();
          $period_sql          = " AND id = '".$options['evaluationperiod']."' "; 
          $period              = $evaluationPeriodObj -> get_evaluation_period($period_sql); 
          if(!empty($period))
          {
             $option_sql = "AND STR_TO_DATE(A.deadline, '%d-%M-%Y') 
                             BETWEEN '".date('Y-m-d', strtotime($period['start_date']))."'
                             AND '".date('Y-m-d', strtotime($period['end_date']))."'
                           ";
          }                 
        }
        $results = $this -> db -> get("SELECT A.*, KPI.id AS kpiid FROM #_action A 
                                        INNER JOIN #_kpi KPI ON A.kpiid = KPI.id
                                        INNER JOIN #_kpa KPA ON KPI.kpaid = KPA.id 
                                        AND KPA.componentid = '".$usercomponent['id']."' 
                                        AND KPA.evaluationyear = '".$options['evaluationyear']."'
                                        AND KPI.kpistatus & ".Kpi::KPIACTION_IMPORTED." <> ".Kpi::KPIACTION_IMPORTED."
                                        AND KPA.kpastatus & ".Kpa::TEMPLATEKPA." <> ".Kpa::TEMPLATEKPA."
                                        AND KPI.kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED."
                                        AND A.owner = '".$usercomponent['userid']."' $option_sql
                                      "); 
         $res              = array();     
         $self_total       = 0;
         $manager_total    = 0;
         $joint_total      = 0;
         $self_total       = 0;  
         $total_weight     = 0;           
         
         $selfrating       = 0;
         $managerrating    = 0;
         $jointrating      = 0;
         $reviewrating     = 0;             
         $count            = 0;             
         $evaluation_dates = array();                                                                               
         if(!empty($results))
         {   
           foreach($results as $index => $action)
           {
              $count++;
              $eval_result    = $this -> get_action_evaluations($action, $usercomponent, $options);
              $self_total    += $eval_result['self_score'];
              $manager_total += $eval_result['manager_score'];
              $joint_total   += $eval_result['joint_score'];
              $review_total  += $eval_result['review_score'];                   

              $selfrating    += $eval_result['self_rating'];
              $managerrating += $eval_result['manager_rating'];
              $jointrating   += $eval_result['joint_rating'];
              $reviewrating  += $eval_result['review_rating']; 

              $total_weight    += $action['weighting'];              
                     
              $res['data'][$action['id']]             = $eval_result;
              $res['data'][$action['id']]['deadline'] = $action['deadline'];
              $res['data'][$action['id']]['status']   = $statuses[$action['status']]['name'];
              $res['data'][$action['id']]['weight']   = $action['weighting'];    
              $res['data'][$action['id']]['name']     = $action['name'];    
              array_push($evaluation_dates, $eval_result['date']);            
           }
            $res['evaluation_date'] = max($evaluation_dates);
            $res['self_total']    = $self_total;
            $res['manager_total'] = $manager_total;
            $res['joint_total']   = $joint_total;
            $res['review_total']  = $review_total;
            
            $res['totalweight']    = $total_weight;   
            if($count > 0)
            {
               $res['selfratingavg']    = round(($selfrating/$count), 2);
               $res['managerratingavg'] = round(($managerrating/$count), 2);
               $res['jointratingavg']   = round(($jointrating/$count), 2);
               $res['reviewratingavg']  = round(($reviewrating/$count), 2);     
            }           
            
           if($total_weight > 0)
           {
             $res['self_avg']    = round(($self_total/$total_weight), 2);
             $res['manager_avg'] = round(($manager_total/$total_weight), 2);
             $res['joint_avg']   = round(($joint_total/$total_weight), 2);
             $res['review_avg']  = round(($review_total/$total_weight), 2);
           }              
         }
        return $res; 	     
	}	
	

     function get_imported_actions_evaluations($usercomponent, $options)
     {             
         $results = $this -> db -> get("SELECT KPI.* FROM #_kpi KPI 
                                        INNER JOIN #_kpa KPA ON KPI.kpaid = KPA.id 
                                        AND KPA.componentid = '".$usercomponent['id']."' 
                                        AND KPA.evaluationyear = '".$options['evaluationyear']."'
                                        AND KPI.kpistatus & ".Kpi::KPIACTION_IMPORTED." = ".Kpi::KPIACTION_IMPORTED."   
                                      ");   
         $module_action_evaluations = array();
         if(!empty($results))
         {
           $kpimappingObj = new KpiMapping();
           foreach($results as $index => $kpi)
           {
    	        $moduleActions             = $kpimappingObj -> getModuleActions($kpi, $kpi['modulemapping'], $usercomponent);
    	        $module_action_evaluations = $moduleActions['action_evaluations'];
           }                
         }
         $this -> _module_action_evaluations($module_action_evaluations);
     }	
	
	
	function get_kpa_evaluations($kpa, $setting, $options)
	{
	   $option_sql = "";
	   if(isset($options['evaluationperiod']))
	   {
	      $option_sql  = "AND KE.evaluation_period_id = '".$options['evaluationperiod']."' ";  
	   }
        $evals       = $this -> db -> getRow("SELECT KE.* FROM #_kpa_evaluations KE WHERE KE.kpa_id = '".$kpa['id']."'
                                        $option_sql");              	   
        $res         = array();
        $user_weight = (boolean)$setting['use_kpa_weighting']; 
        $weight      = $kpa['weighting'];
        $date        = "";
        if(!empty($evals))
        {
          if($setting['self_evaluation'])
          {   
            $res['self_rating']          = $evals['self_rating'];
            $res['self_score']           = ($user_weight ? ($evals['self_rating']*$weight) : $evals['self_rating']);
            $res['self_comment']         = $evals['self_comment'];
            $res['self_recommendation']  = $evals['self_recommendation']; 
          } 
          if($setting['manager_evaluation'])
          {
            $res['manager_rating']          = $evals['manager_rating'];
            $res['manager_score']           = ($user_weight ? ($evals['manager_rating']*$weight) : $evals['manager_rating']);
            $res['manager_comment']         = $evals['manager_comment'];
            $res['manager_recommendation']  = $evals['manager_recommendation'];             
          }
          if($setting['joint_evaluation']) 
          {
            $res['joint_rating']         = $evals['joint_rating'];
            $res['joint_score']          = ($user_weight ? ($evals['joint_rating']*$weight) : $evals['joint_rating']);
            $res['joint_comment']        = $evals['joint_comment'];
            $res['joint_recommendation'] = $evals['joint_recommendation'];             
          } 
          if($setting['evaluation_review']) 
          {
            $res['review_rating']          = $evals['review_rating'];
            $res['review_score']           = ($user_weight ? ($evals['review_rating']*$weight) : $evals['review_rating']);
            $res['review_comment']         = $evals['review_comment'];
            $res['review_recommendation']  = $evals['review_recommendation'];             
          }
          $date = max(strtotime($evals['self_date']), strtotime($evals['manager_date']), strtotime($evals['joint_date']), strtotime($evals['review_date'])); 
        } else {
          if($setting['self_evaluation'])
          {   
            $res['self_rating']         = 0;
            $res['self_score']          = 0;
            $res['self_comment']        = "";
            $res['self_recommendation'] = ""; 
          } 
          if($setting['manager_evaluation']) 
          {
            $res['manager_rating']         = 0;
            $res['manager_score']          = 0;
            $res['manager_comment']        = "";
            $res['manager_recommendation'] = "";             
          }
          if($setting['joint_evaluation']) 
          {
            $res['joint_rating']         = 0;
            $res['joint_score']          = 0;
            $res['joint_comment']        = "";
            $res['joint_recommendation'] = "";             
          } 
          if($setting['evaluation_review']) 
          {
            $res['review_rating']         = 0;
            $res['review_score']          = 0;
            $res['review_comment']        = "";
            $res['review_recommendation'] = "";             
          }
        }    
        $res['date']  = $date;
        return $res;                                    
	}
	
	
	function get_kpi_evaluations($kpi, $setting, $options)
	{
  	   $option_sql   = "";
  	   if(isset($options['evaluationperiod']))
  	   {
  	      $option_sql = "AND KE.evaluation_period_id = '".$options['evaluationperiod']."'";  
  	   }
        $evals       = $this -> db -> getRow("SELECT KE.* FROM #_kpi_evaluations KE WHERE KE.kpi_id = '".$kpi['id']."'
                                        $option_sql");              	   
        $res         = array();
        $user_weight = (boolean)$setting['use_kpi_weighting']; 
        $weight      = $kpi['weighting'];
        $date        = "";
        if(!empty($evals))
        {
          if($setting['self_evaluation'])
          {   
            $res['self_rating']          = $evals['self_rating'];
            $res['self_score']           = ($user_weight ? ($evals['self_rating']*$weight) : $evals['self_rating']);
            $res['self_comment']         = $evals['self_comment'];
            $res['self_recommendation']  = $evals['self_recommendation']; 
          } 
          if($setting['manager_evaluation'])
          {
            $res['manager_rating']          = $evals['manager_rating'];
            $res['manager_score']           = ($user_weight ? ($evals['manager_rating']*$weight) : $evals['manager_rating']);
            $res['manager_comment']         = $evals['manager_comment'];
            $res['manager_recommendation']  = $evals['manager_recommendation'];             
          }
          if($setting['joint_evaluation']) 
          {
            $res['joint_rating']         = $evals['joint_rating'];
            $res['joint_score']          = ($user_weight ? ($evals['joint_rating']*$weight) : $evals['joint_rating']);
            $res['joint_comment']        = $evals['joint_comment'];
            $res['joint_recommendation'] = $evals['joint_recommendation'];             
          } 
          if($setting['evaluation_review']) 
          {
            $res['review_rating']          = $evals['review_rating'];
            $res['review_score']           = ($user_weight ? ($evals['review_rating']*$weight) : $evals['review_rating']);
            $res['review_comment']         = $evals['review_comment'];
            $res['review_recommendation']  = $evals['review_recommendation'];             
          }
          $date = max(strtotime($evals['self_date']), strtotime($evals['manager_date']), strtotime($evals['joint_date']), strtotime($evals['review_date'])); 
        } else {
          if($setting['self_evaluation'])
          {   
            $res['self_rating']         = 0;
            $res['self_score']          = 0
            ;
            $res['self_comment']        = "";
            $res['self_recommendation'] = ""; 
          } 
          if($setting['manager_evaluation']) 
          {
            $res['manager_rating']         = 0;
            $res['manager_score']          = 0;
            $res['manager_comment']        = "";
            $res['manager_recommendation'] = "";             
          }
          if($setting['joint_evaluation']) 
          {
            $res['joint_rating']         = 0;
            $res['joint_score']          = 0;
            $res['joint_comment']        = "";
            $res['joint_recommendation'] = "";             
          } 
          if($setting['evaluation_review']) 
          {
            $res['review_rating']         = 0;
            $res['review_score']          = 0;
            $res['review_comment']        = "";
            $res['review_recommendation'] = "";             
          }
        }    
        $res['date'] = $date;
        return $res;          
	}

	function get_action_evaluations($action, $setting, $options)
	{
	   $option_sql = "";
	   if(isset($options['evaluationperiod']))
	   {
	      $option_sql = "AND AE.evaluation_period_id = '".$options['evaluationperiod']."'";  
	   }
        $evals = $this -> db -> getRow("SELECT AE.* FROM #_action_evaluations AE WHERE AE.action_id = '".$action['id']."' 
                                        AND AE.kpi_id = '".$action['kpiid']."' $option_sql");              	   
        $res         = array();
        $user_weight = (boolean)$setting['use_action_weighting']; 
        $weight      = $action['weighting'];
        $date        = "";
        if(!empty($evals))
        {
          if($setting['self_evaluation'])
          {   
            $res['self_rating']          = $evals['self_rating'];
            $res['self_score']           = ($user_weight ? ($evals['self_rating']*$weight) : $evals['self_rating']);
            $res['self_comment']         = $evals['self_comment'];
            $res['self_recommendation']  = $evals['self_recommendation']; 
          } 
          if($setting['manager_evaluation'])
          {
            $res['manager_rating']          = $evals['manager_rating'];
            $res['manager_score']           = ($user_weight ? ($evals['manager_rating']*$weight) : $evals['manager_rating']);
            $res['manager_comment']         = $evals['manager_comment'];
            $res['manager_recommendation']  = $evals['manager_recommendation'];             
          }
          if($setting['joint_evaluation']) 
          {
            $res['joint_rating']         = $evals['joint_rating'];
            $res['joint_score']          = ($user_weight ? ($evals['joint_rating']*$weight) : $evals['joint_rating']);
            $res['joint_comment']        = $evals['joint_comment'];
            $res['joint_recommendation'] = $evals['joint_recommendation'];             
          } 
          if($setting['evaluation_review']) 
          {
            $res['review_rating']          = $evals['review_rating'];
            $res['review_score']           = ($user_weight ? ($evals['review_rating']*$weight) : $evals['review_rating']);
            $res['review_comment']         = $evals['review_comment'];
            $res['review_recommendation']  = $evals['review_recommendation'];             
          }
          $date = max(strtotime($evals['self_date']), strtotime($evals['manager_date']), strtotime($evals['joint_date']), strtotime($evals['review_date'])); 
        } else {
          if($setting['self_evaluation'])
          {   
            $res['self_rating']         = 0;
            $res['self_score']          = 0;
            $res['self_comment']        = "";
            $res['self_recommendation'] = ""; 
          } 
          if($setting['manager_evaluation']) 
          {
            $res['manager_rating']         = 0;
            $res['manager_score']          = 0;
            $res['manager_comment']        = "";
            $res['manager_recommendation'] = "";             
          }
          if($setting['joint_evaluation']) 
          {
            $res['joint_rating']         = 0;
            $res['joint_score']          = 0;
            $res['joint_comment']        = "";
            $res['joint_recommendation'] = "";             
          } 
          if($setting['evaluation_review']) 
          {
            $res['review_rating']         = 0;
            $res['review_score']          = 0;
            $res['review_comment']        = "";
            $res['review_recommendation'] = "";             
          }
        }    
        $res['date'] = $date;
        return $res; 
	}

	function recommendation_matrix($options)
	{
	   $users = array();
        $user_obj   = new User();

        $user_sql   = "";
        $userdata   = array(); 
        $year       = array();        
        if(isset($options['evaluationyear']))
        {
           $year  = EvaluationYear::findById($options['evaluationyear']);        
        }          
        if(isset($options['employee']) && !empty($options['employee']))
        {
           $user_sql = " AND TK.tkid = '".$options['employee']."' ";
           $userdata = $user_obj -> get_user(array('user' => $options['employee']));
        }
        if(isset($options['directorate']) && !empty($options['directorate']))
        {
           $user_sql .= " AND LD.id = '".$options['directorate']."' ";
        }
        $users  = $user_obj -> get_users($user_sql);
        if(isset($userdata) && !empty($userdata))
        {
           if(!isset($options['employee']) || empty($options['employee']))
           {
              $users = array_merge(array(array_slice($userdata, 0, -1)), $users);
           }          
        }
        $period = array();
        if(isset($options['evaluationperiod']) && !empty($options['evaluationperiod']))
        {
          $evaluationPeriodObj = new EvaluationPeriod();
          $period_sql          = " AND id = '".$options['evaluationperiod']."' "; 
          $period              = $evaluationPeriodObj -> get_evaluation_period($period_sql);                
        }
        $evaluationyear = array();
        if(isset($options['evaluationyear']) && !empty($options['evaluationyear']))
        {
          $evaluationyear = Evaluationyear::findById($options['evaluationyear']);
        }        
        $allusers   = $user_obj -> get_users_list();
        $usermatrix = array();

        if(!empty($users))
        {
            $i = 0;
           foreach($users as $index => $user)
           {
              $usermatrix[$i]['user']['user']      = $user['user'];
              $usermatrix[$i]['user']['manager']   = ($user['tkmanager'] == "S" ? $user['user'] : $allusers[$user['tkmanager']]);
              $usermatrix[$i]['user']['joblevel']  = $user['joblevel'];
              $usermatrix[$i]['user']['category']  = $user['performancecategory'];
              $usermatrix[$i]['user']['period']    = $period['start']." - ".$period['end'];  
              $usermatrix[$i]['user']['year']      = $evaluationyear['start']." - ".$evaluationyear['end']; 

              $optional_sql  = "";              
              $optional_sql .= " AND UC.userid = '".$user['tkid']."' AND M.category_id = '".$user['categoryid']."'";              
              if(isset($options['evaluationyear']))
              {
                $optional_sql .= " AND UC.evaluationyear = '".$options['evaluationyear']."' ";
              } 
              $usercomponentObj = new UserComponent();
              $usercomponents   = $usercomponentObj -> get_component($optional_sql);   
              if(!empty($usercomponents))
              {
                 foreach($usercomponents as $index => $usercomponent)
                 {
                    $usermatrix[$i]['component'][$usercomponent['id']]['componentname'] = $usercomponent['name'];
                    $usermatrix[$i]['component'][$usercomponent['id']]['weight']        = $usercomponent['component_weight'];
                    
                    
                    
                    
                    if($usercomponent['evaluate_on'] == "kpa")
                    {
                       $usermatrix[$i]['component'][$usercomponent['id']]['type'] = "KPA";
                       $kpa_matrix     = $this -> _get_kpa_matrix($usercomponent, $options);
                       $usermatrix[$i]['component'][$usercomponent['id']]['data'] = $kpa_matrix['data'];
                    } else if($usercomponent['evaluate_on'] == "kpi") {
                       $usermatrix[$i]['component'][$usercomponent['id']]['type'] = "KPI";
                       $kpi_matrix = $this -> _get_kpi_matrix($usercomponent, $options);
                       $usermatrix[$i]['component'][$usercomponent['id']]['data'] = $kpi_matrix['data'];
                    } else if($usercomponent['evaluate_on'] == "action") {
                       $usermatrix[$i]['component'][$usercomponent['id']]['type'] = "Action";
                       $action_matrix = $this -> _get_action_matrix($usercomponent, $options);
                       $usermatrix[$i]['component'][$usercomponent['id']]['data'] = $action_matrix['data'];
                    }
                 }
              }
               $i++;
           }
        }        
        $data['info']       = "Recommendation Matrix for ".$year['start']." - ".$year['end']."  as at ".date('d-M-Y');
        $data['usermatrix'] = $usermatrix;
        return $data;
	}
	
	
	function bell_curve_report($options)
	{
        $period     = 0;
        $year       = 0;
        $dir        = 0;
        $eval_level = "";
        $job_level  = 0;
        $evaluationlevel = $options['evaluationlevel'];
        if(isset($_GET['evaluationperiod']) && !empty($_GET['evaluationperiod']))
        {
           $period = $_GET['evaluationperiod'];
        }
        if(isset($_GET['evaluationyear']) && !empty($_GET['evaluationyear']))
        {
           $year = $_GET['evaluationyear'];
        }
        if(isset($_GET['directorate']) && !empty($_GET['directorate']))
        {
           $dir = $_GET['directorate'];
        }
        if(isset($_GET['joblevel']) && !empty($_GET['joblevel']))
        {
           $job_level = $_GET['joblevel'];
        }
        if(isset($_GET['evaluationlevel']) && !empty($_GET['evaluationlevel']))
        {
           $eval_level = $_GET['evaluationlevel'];
        }
        $options['evaluationyear']    = $year;
        $options['evaluationperiod']  = $period;
        $options['directorate']       = $dir;
        $options['job_level']         = $job_level;
        $report                       = $this -> matrix_report($options);
        $eval_values                  = array(); 
        if(!empty($report))
        {
           foreach($report['usermatrix'] as $index => $r_data)
           {
             $eval_values[$index]['self']    = (float)$r_data['self'];
             $eval_values[$index]['manager'] = (float)$r_data['manager'];
             $eval_values[$index]['joint']   = (float)$r_data['joint'];
             $eval_values[$index]['review']  = (float)$r_data['review'];
           }
        }
          $ratingObj      = new RatingScale();
          $_rating        = array(array('id' => -1, 'ratingfrom' => 0, 'ratingto' => 0, 'description' => '', 'definition' => ''));
          $ratings        = array_merge($_rating, $ratingObj -> getRatingScales());
          $_ratings       = array();
          $from           = 0;
          foreach($ratings as $_index => $_val)
          {
             $_id        = ($_index+1);
             $to         = ($_val['ratingto'] + 0.4);
             $_ratings[] = array('from' => $from, 'to' => $to, 'description' => $_val['description'], 'ratingfrom' => $_val['ratingfrom'], 'ratingto' => $_val['ratingto'], 'id' => $_id);
             $from       = $to +  0.1;
          }
          $ratings_data   = array();
          $ratings_info   = array();
          $from           = 0; 
          foreach($_ratings as $index => $rating)
          {                 
              foreach($eval_values as $index => $val)
              {
                  if($val['self'] >= $rating['from'] && $val['self'] < $rating['to'])
                  {
                      $ratings_data[$rating['id']]['self'][] = 1; 
                  }   
                   
                  if($val['manager'] >= $rating['from'] && $val['manager'] < $rating['to'])
                  {
                      $ratings_data[$rating['id']]['manager'][] = 1; 
                  }      

                  if($val['joint'] >= $rating['from'] && $val['joint'] < $rating['to'])
                  {
                      $ratings_data[$rating['id']]['joint'][] = 1; 
                  }                     
                   
                  if($val['review'] >= $rating['from'] && $val['review'] < $rating['to'])
                  {
                      $ratings_data[$rating['id']]['review'][] = 1; 
                  }
              }  
          }
        foreach($_ratings as $rating_index => $rate)
        {
            
           if(isset($ratings_data[$rate['id']]))
           {
             if(isset($ratings_data[$rate['id']]['self']))
             {
                $rating_info[$rate['id']]['self'] = count($ratings_data[$rate['id']]['self']);
             } else {
                $rating_info[$rate['id']]['self'] = 0;
             }
             if(isset($ratings_data[$rate['id']]['manager']))
             {
                $rating_info[$rate['id']]['manager'] = count($ratings_data[$rate['id']]['manager']);
             } else {
                $rating_info[$rate['id']]['manager'] = 0;
             }
             if(isset($ratings_data[$rate['id']]['joint']))
             {
                $rating_info[$rate['id']]['joint'] = count($ratings_data[$rate['id']]['joint']);
             } else {
                $rating_info[$rate['id']]['joint'] = 0;
             }
             if(isset($ratings_data[$rate['id']]['review']))
             {
                $rating_info[$rate['id']]['review'] = count($ratings_data[$rate['id']]['review']);
             }  else {
               $rating_info[$rate['id']]['review'] = 0;
             }
           } else {
              $rating_info[$rate['id']]['self']    = 0;
              $rating_info[$rate['id']]['manager'] = 0;
              $rating_info[$rate['id']]['joint']   = 0;
              $rating_info[$rate['id']]['review']  = 0;
           }
        }

        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <chart>
                	<series>'; 
                      $y = 0; 
                      foreach($_ratings as $index => $rating)
                      { 
                          $y++;               	
                	        $xml .=	'<value xid="'.$y.'">'.$index.'</value>';
                      }
                   
        $xml    .='</series>
                	<graphs>
                		<graph gid="1">';
                		 $x = 0;
                     foreach($rating_info as $i => $val)
                     {
                            $x++;
                            $xml .= '<value xid="'.$x.'">'.$val[$evaluationlevel].'</value>';
                      }
                      $xml .= '   </graph>
                  </graphs>
            </chart>';   
        /*
        $ratingObj = new RatingScale();
        $ratings   = $ratingObj -> getRatingScales();
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <chart>
                	<series>';
                        foreach($ratings as $index => $rating)
                        {                	
                	       $xml .=	'<value xid="'.$rating['ratingto'].'">'.$rating['ratingto'].'</value>';
                        }
                   
        $xml    .='</series>
                	<graphs>
                		<graph gid="1">
                			<value xid="1">5</value>
                			<value xid="2">10</value>
                			<value xid="3">30</value>
                			<value xid="4">10</value>
                			<value xid="5">5</value>
                		</graph>
                		<graph gid="2">';
        $xml .= '			<value xid="1">19.73</value>';
        $xml .= '			<value xid="2">18.43</value>';
        $xml .= '			<value xid="3">18.08</value>';
        $xml .= '			<value xid="4">19.01</value>';
        $xml .= '			<value xid="5">19.43</value>';
        //$xml .= '			<value xid="6">19.43</value>';    
        //$xml .= '			<value xid="7">19.43</value>';  
        $xml .= '   </graph>
                		<graph gid="3">
                			<value xid="1">5</value>
                			<value xid="2">15</value>
                			<value xid="3">40</value>
                			<value xid="4">10</value>
                			<value xid="5">2</value>
                		</graph>
                	</graphs>
                </chart>';
        */
        return $xml; 
	}
}
