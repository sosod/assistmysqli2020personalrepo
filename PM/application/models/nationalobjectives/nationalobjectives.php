<?php
class Nationalobjectives extends Model
{

     protected static $table = "list_nationalobjectives";

	function __construct()
	{
		parent::__construct();
	}
	
	function update_national_objectives($data)
	{
	   $this -> attach(new SetupAuditLog("National Objectives"));
	   $res = $this -> update_where($data['data'], array('id' => $data['id']));
	   return $res;
	}
	

     function get_national_objectives($options = array())
     {
        $option_sql = parent::createANDSqlOptions($options);;
        $option_sql.= " AND status & 2 <> 2";
        $used       = $this -> get_used_nation_objective();
        $nobjArr    = array();
        $results    = $this -> db -> get("SELECT * FROM #_".static::$table." WHERE 1 $option_sql ");
        foreach( $results as $index => $nationalobjective)
        {
            if( in_array($nationalobjective['id'], $used) )
            {
                $nationalobjective['used'] = true;
            } else {
                $nationalobjective['used'] = false;
            }
            $nobjArr[$index] = $nationalobjective;
        }
        return $nobjArr;
     }

     function get_used_nation_objective()
     {
        $kpaObj           		= new Kpa();
        $kpa_national_objective = $kpaObj -> get_used_nation_objective();

        $kpiObj           		= new Kpi();
        $kpi_national_objective = $kpiObj -> get_used_nation_objective();

        $actionObj           	   = new Action();
        $action_national_objective = $actionObj -> get_used_nation_objective();

        $used_national_objective  = array_merge($kpa_national_objective, $kpi_national_objective, $action_national_objective);
        return $used_national_objective; 
     }
}
