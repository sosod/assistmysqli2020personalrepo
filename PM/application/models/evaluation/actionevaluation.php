<?php
class ActionEvaluation extends Evaluation
{
    
    private $id    = ""; 
    
    static $table  = "action_evaluations";
    
    function __construct()
    {
      $this -> id = ($id ? $id : "");
      parent::__construct();
    }
  
    function save_evaluation($data, $userdata)
    {    
      $evaluation = $this -> get_evaluation(" AND action_id = '".$data['reference']."' AND kpi_id = '".$data['kpi_id']."' ");
      $insertdata = $this -> _prepare_evaluation($data);  
      if(!empty($evaluation))
      {
         $res   = $this -> update_where($insertdata, array('id' => $evaluation['id']));
         return Message::update($res, " action evaluation ");
      } else {
         if(isset($data['kpi_id']) && !empty($data['kpi_id']))
         {
            $insertdata['kpi_id']  = $data['kpi_id'];
            $insertdata['status']  = 5;     
         }
         $insertdata['action_id'] = $data['reference'];
         $insertdata['created']   = date('Y-m-d H:i:s');
         $id                      = $this -> save($insertdata);
         return Message::save($id, " action evaluation ");
      }
    }

    function get_user_action_evaluation_status($component_id, $evaluationyear, $userdata)
    {
      $results = $this -> db -> get("SELECT AE.* FROM #_action_evaluations AE 
                                     INNER JOIN #_action A ON A.id = AE.action_id
                                     INNER JOIN #_kpi K ON K.id = A.kpiid
                                     INNER JOIN #_kpa KPA ON KPA.id = K.kpaid 
                                     WHERE KPA.componentid = '".$component_id."' 
                                     AND KPA.evaluationyear = '".$evaluationyear."' 
                                     AND A.owner = '".$userdata['tkid']."' 
                                     AND A.actionstatus & ".Action::DELETED." <> ".Action::DELETED."
                                   ");
      return $results;
    } 

    function get_evaluations($componentid, $evaluationyear, $userdata, $setting)
    {
         $results = $this -> db -> get("SELECT A.id FROM #_action A 
                                        INNER JOIN #_kpi KPI ON A.kpiid = KPI.id
                                        INNER JOIN #_kpa KPA ON KPI.kpaid = KPA.id 
                                        AND KPA.componentid = '".$componentid."' AND KPA.evaluationyear = '".$evaluationyear."'
                                        AND KPI.kpistatus & ".Kpi::KPIACTION_IMPORTED." <> ".Kpi::KPIACTION_IMPORTED."
                                        AND KPA.kpastatus & ".Kpa::TEMPLATEKPA." <> ".Kpa::TEMPLATEKPA."
                                        AND KPI.kpistatus & ".Kpi::DELETED." <> ".Kpi::DELETED."
                                        AND STR_TO_DATE(A.deadline, '%d-%M-%Y') 
                                            BETWEEN '".date('Y-m-d', strtotime($_SESSION['evaluation_period']['start_date']))."'
                                            AND '".date('Y-m-d', strtotime($_SESSION['evaluation_period']['end_date']))."'
                                        AND A.owner = '".$userdata['tkid']."'
                                      ");                                                            
        if(!empty($results))
        {   
          foreach($results as $index => $action)
          {
            $this -> get_action_evaluations($action['id'], $setting);
          }
        }                                                          
    }      
    
    function get_action_evaluations($actionid, $setting, $kpiid = "")
    {
        $option_sql = "";
        if(!empty($kpiid))
        {
           $option_sql = " AND AE.kpi_id = '".$kpiid."' ";
        }
        $evals = $this -> db -> getRow("SELECT AE.* FROM #_action_evaluations AE WHERE AE.action_id = '".$actionid."' 
                                         AND AE.evaluation_period_id = '".$_SESSION['evaluation_period']['id']."'  
                                        $option_sql ");                                                              
        if(!empty($evals))
        {        
            if(($evals['self_rating'] == "" || $evals['self_comment'] == "" || $evals['self_recommendation'] == "") && 
                $setting['self_evaluation'])
            {
               $this -> self_evaluation += 1; 
            } else if(($setting['manager_evaluation']) && 
                   ($evals['manager_rating'] == "" || $evals['manager_comment'] == "" || $evals['manager_recommendation'] == "")) 
            {
              $this -> manager_evaluation += 1; 
            } else if(($setting['joint_evaluation']) && 
                      ($evals['joint_rating'] == "" || $evals['joint_comment'] == "" || $evals['joint_recommendation'] == ""))
            {
               $this -> joint_evaluation += 1;       
            } else if(($setting['evaluation_review']) && 
                     ($evals['review_rating'] == "" || $evals['review_comment'] == "" || $evals['review_recommendation'] == "")) 
            {
               $this -> evaluation_review += 1; 
            }
        } else {
          if($setting['self_evaluation'])
          {   
            $this -> self_evaluation += 1; 
          } else if($setting['manager_evaluation']) {
            $this -> manager_evaluation += 1;
          } else if($setting['joint_evaluation']) {
            $this -> joint_evaluation += 1;
          } else if($setting['evaluation_review']) {
            $this -> evaluation_review += 1;
          }
        }     
    }

     function get_total_next_evaluations()
     {
         $evaluations = array();
         if($this -> self_evaluation != 0)
         {
           $evaluations['next']     = "Self Evaluation"; 
           $evaluations['total']    = $this -> self_evaluation;
           $evaluations['next_key'] = "self";
         } else if($this -> manager_evaluation != 0) {
           $evaluations['next']     = "Manager Evaluation";
           $evaluations['total']    = $this-> manager_evaluation;
           $evaluations['next_key'] = "manager";
         } else if($this -> joint_evaluation != 0) {
           $evaluations['next']     = "Joint Evaluation";
           $evaluations['total']    = $this -> joint_evaluation;
           $evaluations['next_key'] = "joint";
         } else if($this -> evaluation_review != 0) {
           $evaluations['next']     = "Evaluation Review";
           $evaluations['total']    = $this -> evaluation_review;
           $evaluations['next_key'] = "review";
         } else {
            $this -> evaluation_complete = true;            
         }     
        return $evaluations;
     } 

     function get_imported_actions_evaluations($componentid, $evaluationyear, $userdata, $setting, $options, $evaluatioObj)
     {             
         $results = $this -> db -> get("SELECT KPI.* FROM #_kpi KPI 
                                        INNER JOIN #_kpa KPA ON KPI.kpaid = KPA.id 
                                        AND KPA.componentid = '".$componentid."' AND KPA.evaluationyear = '".$evaluationyear."'
                                        AND KPI.kpistatus & ".Kpi::KPIACTION_IMPORTED." = ".Kpi::KPIACTION_IMPORTED."   
                                      ");   
         $module_action_evaluations = array();
         if(!empty($results))
         {
           $kpimappingObj = new KpiMapping();
           foreach($results as $index => $kpi)
           {
	          $kpimappingObj -> getModuleActions($kpi, $kpi['modulemapping'], $userdata, $setting, $options, $evaluatioObj);
	        //$module_action_evaluations = $moduleActions['action_evaluations'];
           }                
         }
         //$this -> _module_action_evaluations($module_action_evaluations);
     }

     function _module_action_evaluations($module_actions)
     {
         if($module_actions['next_key'] == "self")
         {
           $this -> set_self_evaluation($module_actions['total']); 
         } else if($module_actions['next_key'] == "manager") {
           $this -> set_manager_evaluation($module_actions['total']);
         } else if($module_actions['next_key'] == 'joint') {
           $this -> set_joint_evaluation($module_actions['total']);
         } else if($module_actions['next_key'] == 'review') {
           $this -> set_evaluation_review($module_actions['total']);
         } 
     }
     
	function get_evaluation_statuses($userdata, $componentid, $options)
	{
	   $results    = array();
	   $option_sql = "";
	   if(isset($options['evaluationperiod']) && !empty($options['evaluationperiod']))
	   {
	     $option_sql = " AND AE.evaluation_period_id = '".$options['evaluationperiod']."' ";
	   }	   
	   $self_sql   = "SELECT MAX(AE.self_date) AS self_date FROM #_action A 
	                  LEFT JOIN #_action_evaluations AE ON A.id = AE.action_id 
	                  INNER JOIN #_kpi K ON K.id = A.kpiid INNER JOIN #_kpa KPA ON KPA.id = K.kpaid 
	                  WHERE K.owner = '".$userdata['tkid']."' AND KPA.componentid = '".$componentid."'
	                  $option_sql
	                  HAVING MAX(AE.self_date) 
	                  ORDER BY AE.self_date
	                  ";
	   $self_results = $this -> db -> getRow($self_sql);
	   if(!empty($self_results))
	   {
	      $results['self_evaluation'] = $self_results['self_date'];
	   }
	   
	   $manager_sql   = "SELECT MAX(AE.manager_date) AS manager_date FROM #_action A 
	                     LEFT JOIN #_action_evaluations AE ON A.id = AE.action_id 
	                     INNER JOIN #_kpi K ON K.id = A.kpiid INNER JOIN #_kpa KPA ON KPA.id = K.kpaid 
	                     WHERE K.owner = '".$userdata['tkid']."' AND KPA.componentid = '".$componentid."'
	                     $option_sql
	                     HAVING MAX(AE.manager_date)
	                     ORDER BY AE.manager_date
	                  ";
        $manager_results = $this -> db -> getRow($manager_sql);
	   if(!empty($manager_results))
	   {
	      $results['manager_evaluation'] = $manager_results['manager_date'];
	   }
	   
	   $joint_sql    = "SELECT MAX(AE.joint_date) AS joint_date FROM #_action A 
	                    LEFT JOIN #_action_evaluations AE ON A.id = AE.action_id 
	                    INNER JOIN #_kpi K ON K.id = A.kpiid INNER JOIN #_kpa KPA ON KPA.id = K.kpaid 
	                    WHERE K.owner = '".$userdata['tkid']."' AND KPA.componentid = '".$componentid."'
	                    $option_sql
	                    HAVING MAX(AE.joint_date) 
	                    ORDER BY AE.joint_date
	                   ";
        $joint_results = $this -> db -> getRow($joint_sql);        
	    if(!empty($joint_results))
	    {
	      $results['joint_evaluation'] = $joint_results['joint_date'];
	    }        
	    $review_sql    = "SELECT MAX(AE.review_date) AS review_date FROM #_action A 
	                     LEFT JOIN #_action_evaluations AE ON A.id = AE.action_id 
	                     INNER JOIN #_kpi K ON K.id = A.kpiid INNER JOIN #_kpa KPA ON KPA.id = K.kpaid 
	                     WHERE K.owner = '".$userdata['tkid']."' AND KPA.componentid = '".$componentid."'
	                     $option_sql
	                     HAVING MAX(AE.review_date)
	                     ORDER BY AE.review_date
	                    ";
        $review_results = $this -> db -> getRow($review_sql);                
	    if(!empty($review_results))
	    {
	      $results['evaluation_review'] = $review_results['review_date'];
	    }	   
        return $results;
	}  
	
     function get_evaluation_scores($userdata, $componentid, $options, $setting)
     {
	   $results    = array();
	   $option_sql = "";
	   $self_sql = "SELECT SUM(AE.self_rating) AS totalscore, COUNT(AE.id) AS totalcount FROM #_action_evaluations AE 
	                INNER JOIN #_kpi KPI ON KPI.id = AE.kpi_id 
	                WHERE KPI.owner = '".$userdata['tkid']."' AND AE.evaluation_period_id = '".$options['evaluationperiod']."'
	               ";
	   $self_result = $this -> db -> getRow($self_sql);
        if(!empty($self_result))
        {
           $result['self_evaluation'] = $self_result;
        }

	   $manager_sql = "SELECT SUM(AE.manager_rating) AS totalscore, COUNT(AE.id) AS totalcount FROM #_action_evaluations AE 
	                   INNER JOIN #_kpi KPI ON KPI.id = AE.kpi_id 
	                   WHERE KPI.owner = '".$userdata['tkid']."' AND AE.evaluation_period_id = '".$options['evaluationperiod']."'
	               ";
	   $manager_result = $this -> db -> getRow($manager_sql);
        if(!empty($manager_result))
        {
           $result['manager_evaluation'] = $manager_result;
        }
	   
	     $joint_sql = "SELECT SUM(AE.joint_rating) AS totalscore, COUNT(AE.id) AS totalcount FROM #_action_evaluations AE 
	                 INNER JOIN #_kpi KPI ON KPI.id = AE.kpi_id 
	                 WHERE KPI.owner = '".$userdata['tkid']."' AND AE.evaluation_period_id = '".$options['evaluationperiod']."'
	                ";
	    $joint_result = $this -> db -> getRow($joint_sql);
        if(!empty($joint_result))
        {
           $result['joint_evaluation'] = $joint_result;
        }	
        
	    $review_sql = "SELECT SUM(AE.review_rating) AS totalscore, COUNT(AE.id) AS totalcount FROM #_action_evaluations AE 
	                  INNER JOIN #_kpi KPI ON KPI.id = AE.kpi_id 
	                  WHERE KPI.owner = '".$userdata['tkid']."' AND AE.evaluation_period_id = '".$options['evaluationperiod']."'
	                 ";
	    $review_result = $this -> db -> getRow($review_sql);
        if(!empty($review_result))
        {
           $result['review_evaluation'] = $review_result;
        }	        
        return $result;   
     }  

     function get_used_rating_scales()
     {
        $results = $this -> db -> get("SELECT self_rating, manager_rating, joint_rating, review_rating FROM #_action_evaluations");
        $ratings = array();
        if(!empty($results))
        {
           foreach($results as $index => $result)
           {
              array_push($ratings, $result['self_rating']);
              array_push($ratings, $result['manager_rating']);
              array_push($ratings, $result['joint_rating']);
              array_push($ratings, $result['review_rating']);
           }
        }
        return $ratings;        
     }  
     
     function get_evaluation_data($action_id, $kpi_id, $setting, $rating)
     {
       $evaluations     = $this -> db -> getRow("SELECT AE.* FROM #_action_evaluations AE
                                                 WHERE AE.kpi_id = '".$kpi_id."' AND AE.action_id = '".$action_id."'
                                                 AND AE.evaluation_period_id = '".$_SESSION['evaluation_period']['id']."' 
                                               ");
       $evaluation_res = $this -> get_next_evaluation($setting, $evaluations, $rating);
       //$evaluation_res['evaluations'] = $evaluations;
	   return $evaluation_res;        
     }

}
?>
