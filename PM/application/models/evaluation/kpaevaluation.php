<?php
class KpaEvaluation extends Evaluation
{    
      /*
       	int
      */     
	 private $id;
	 /*
	 	int
	 */
	 private $kpa_id;	 
     
      protected static $table = "kpa_evaluations";
     
     
     function __construct($id = "")
     { 
       if($id)
       {
          $this -> id = $id;
       }
       parent::__construct();
     }
     
     function save_evaluation($data, $userdata)
     {    
        $evaluation = $this -> get_evaluation(" AND kpa_id = '".$data['reference']."' ");
        $insertdata = $this -> _prepare_evaluation($data);
        if(!empty($evaluation))
        {
          $res   = $this -> update_where($insertdata, array('id' => $evaluation['id']));
          return Message::update($res, " kpa evaluation ");
        } else {
          $insertdata['kpa_id']   = $data['reference'];
          $insertdata['created']  = date('Y-m-d H:i:s');
          $id                     = $this -> save($insertdata);
          return Message::save($id, " kpa evaluation ");
        }
     }
     
     function get_evaluations($componentid, $evaluationyear, $userdata, $setting)
     {
         $results = $this -> db -> get("SELECT KPA.* FROM #_kpa KPA WHERE 1 
                                        AND KPA.componentid = '".$componentid."' 
                                        AND KPA.evaluationyear = '".$evaluationyear."'
                                        AND KPA.kpastatus & ".Kpa::TEMPLATEKPA." <> ".Kpa::TEMPLATEKPA."
                                        AND KPA.kpastatus & ".Kpa::DELETED." <> ".Kpa::DELETED."
                                        AND KPA.owner = '".$userdata['tkid']."'
                                      ");                          
         if(!empty($results))
         {   
           foreach($results as $index => $kpa)
           {
              $this -> get_kpa_evaluations($kpa['id'], $setting);
           }
         }   
     }
     
     function get_kpa_evaluations($kpaid, $setting)
     {
         $evals = $this -> db -> getRow("SELECT KE.* FROM #_kpa_evaluations KE WHERE KE.kpa_id = '".$kpaid."' 
                                         AND KE.evaluation_period_id = '".$_SESSION['evaluation_period']['id']."'  
                                        ");                                                                                  
         if(!empty($evals))
         {
            if(($evals['self_rating'] == "" || $evals['self_comment'] == "" || $evals['self_recommendation'] == "") && 
                $setting['self_evaluation'])
            {
               $this -> self_evaluation += 1; 
            } else if(($setting['manager_evaluation']) && 
                    ($evals['manager_rating'] == "" || $evals['manager_comment'] == "" || $evals['manager_recommendation'] == ""))
            {
              $this -> manager_evaluation += 1; 
            } else if(($setting['joint_evaluation']) && 
                      ($evals['joint_rating'] == "" || $evals['joint_comment'] == "" || $evals['joint_recommendation'] == ""))
            {
               $this -> joint_evaluation += 1;       
            } else if(($setting['evaluation_review']) && 
                     ($evals['review_rating'] == "" || $evals['review_comment'] == "" || $evals['review_recommendation'] == "")) 
            {
               $this -> evaluation_review += 1; 
            }
         } else {
          if($setting['self_evaluation'])
          {   
            $this -> self_evaluation += 1; 
          } else if($setting['manager_evaluation']) {
            $this -> manager_evaluation += 1;
          } else if($setting['joint_evaluation']) {
            $this -> joint_evaluation += 1;
          } else if($setting['evaluation_review']) {
            $this -> evaluation_review += 1;
          }
        }     
     }
     
	function get_evaluation_statuses($userdata, $componentid, $options)
	{
	   $option_sql = "";
	   if(isset($options['evaluationperiod']) && !empty($options['evaluationperiod']))
	   {
	     $option_sql = " AND KE.evaluation_period_id = '".$options['evaluationperiod']."' ";
	   }
	
	   $results    = array();
	   $self_sql   = "SELECT MAX(KE.self_date) AS self_date FROM #_kpa K LEFT JOIN #_kpa_evaluations KE ON K.id = KE.kpa_id 
	                  WHERE K.owner = '".$userdata['tkid']."' AND K.componentid = '".$componentid."' $option_sql
	                  HAVING MAX(KE.self_date) 
	                  ORDER BY KE.self_date LIMIT 0,1
	                  ";
	   $self_results = $this -> db -> getRow($self_sql);
	   if(!empty($self_results))
	   {
	      $results['self_evaluation'] = $self_results['self_date'];
	   }
	   
	   $manager_sql   = "SELECT MAX(KE.manager_date) AS manager_date FROM #_kpa K LEFT JOIN #_kpa_evaluations 
	                     KE ON K.id = KE.kpa_id 
	                     WHERE K.owner = '".$userdata['tkid']."' AND K.componentid = '".$componentid."' $option_sql
	                     HAVING MAX(KE.manager_date)
	                     ORDER BY KE.manager_date LIMIT 0,1
	                  ";
        $manager_results = $this -> db -> getRow($manager_sql);
	   if(!empty($manager_results))
	   {
	      $results['manager_evaluation'] = $manager_results['manager_date'];
	   }
	   
	   $joint_sql    = "SELECT MAX(KE.joint_date) AS joint_date FROM #_kpa K LEFT JOIN #_kpa_evaluations KE ON K.id = KE.kpa_id 
	                    WHERE K.owner = '".$userdata['tkid']."' AND K.componentid = '".$componentid."' $option_sql
	                    HAVING MAX(KE.joint_date)
	                    ORDER BY KE.joint_date LIMIT 0,1
	                   ";
        $joint_results = $this -> db -> getRow($joint_sql);        
	    if(!empty($joint_results))
	    {
	      $results['joint_evaluation'] = $joint_results['joint_date'];
	    }        
	    $review_sql    = "SELECT MAX(KE.review_date) AS review_date FROM #_kpa K LEFT JOIN #_kpa_evaluations 
	                     KE ON K.id = KE.kpa_id
	                     WHERE K.owner = '".$userdata['tkid']."' AND K.componentid = '".$componentid."'  $option_sql
	                     HAVING MAX(KE.review_date)
	                     ORDER BY KE.review_date
	                    ";
        $review_results = $this -> db -> getRow($review_sql);                
	    if(!empty($review_results))
	    {
	      $results['evaluation_review'] = $review_results['review_date'];
	    }	   
        return $results;
	}     
	
     function get_evaluation_scores($userdata, $componentid, $options, $setting)
     {
	    $results    = array();
	    $option_sql = "";
	    $self_sql = "SELECT SUM(KE.self_rating) AS totalscore, COUNT(KE.id) AS totalcount FROM #_kpa K 
	                INNER JOIN #_kpa_evaluations KE ON KE.kpa_id = K.id 
	                WHERE K.owner = '".$userdata['tkid']."' AND KE.evaluation_period_id = '".$options['evaluationperiod']."'
	               ";
	    $self_result = $this -> db -> getRow($self_sql);
        if(!empty($self_result))
        {
           $result['self_evaluation'] = $self_result;
        }

	    $manager_sql = "SELECT SUM(KE.manager_rating) AS totalscore, COUNT(KE.id) AS totalcount FROM #_kpa K 
	                INNER JOIN #_kpa_evaluations KE ON KE.kpa_id = K.id 
	                WHERE K.owner = '".$userdata['tkid']."' AND KE.evaluation_period_id = '".$options['evaluationperiod']."'
	               ";
	    $manager_result = $this -> db -> getRow($manager_sql);
        if(!empty($manager_result))
        {
           $result['manager_evaluation'] = $manager_result;
        }
	   
	    $joint_sql = "SELECT SUM(KE.joint_rating) AS totalscore, COUNT(KE.id) AS totalcount FROM #_kpa K 
	                INNER JOIN #_kpa_evaluations KE ON KE.kpa_id = K.id 
	                WHERE K.owner = '".$userdata['tkid']."' AND KE.evaluation_period_id = '".$options['evaluationperiod']."'
	               ";
	    $joint_result = $this -> db -> getRow($joint_sql);
        if(!empty($joint_result))
        {
           $result['joint_evaluation'] = $joint_result;
        }	
        
	    $review_sql = "SELECT SUM(KE.review_rating) AS totalscore, COUNT(KE.id) AS totalcount FROM #_kpa K 
	                INNER JOIN #_kpa_evaluations KE ON KE.kpa_id = K.id 
	                WHERE K.owner = '".$userdata['tkid']."' AND KE.evaluation_period_id = '".$options['evaluationperiod']."'
	               ";
	    $review_result = $this -> db -> getRow($review_sql);
        if(!empty($review_result))
        {
           $result['review_evaluation'] = $review_result;
        }	        
        return $result;   
     }	

     function get_used_rating_scales()
     {
        $results = $this -> db -> get("SELECT self_rating, manager_rating, joint_rating, review_rating FROM #_kpa_evaluations");
        $ratings = array();
        if(!empty($results))
        {
           foreach($results as $index => $result)
           {
              array_push($ratings, $result['self_rating']);
              array_push($ratings, $result['manager_rating']);
              array_push($ratings, $result['joint_rating']);
              array_push($ratings, $result['review_rating']);
           }
        }
        return $ratings;        
     } 
     
     function get_kpa_next_evaluation($kpaid, $setting, $rating)
     {
	   $evaluations          = $this -> db -> getRow("SELECT E.* FROM #_kpa_evaluations E 
	                                                  WHERE E.evaluation_period_id = '".$_SESSION['evaluation_period']['id']."'
	                                                  AND E.kpa_id = '".$kpaid."' ");   
       $evaluation_res = $this -> get_next_evaluation($setting, $evaluations, $rating);
       //$evaluation_res['evaluations'] = $evaluations;
	   return $evaluation_res;    
     }

}
?>
