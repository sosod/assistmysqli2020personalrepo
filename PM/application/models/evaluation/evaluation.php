<?php
class Evaluation extends Model
{

    const SELF_EVALUATION   = 8;
    
    const MANAGER_EVALUATIO = 16;
    
    const JOINT_EVALUATION  = 32;
    
    const REVIEW_EVALUATION = 64;

    protected static $table = "evaluation";
     
    private static $id;
     
    static $userEvaluations = array();
     
    static $evaluationList  = array("selfevaluation" => "Self Evaluation", "managerevaluation" => "Manager Evaluation", 
                                    "jointevaluation" => "Joint Evaluation", "evaluationreview" => "Evaluation Review"
                                   );
    protected $evaluation_keys  = array("self_evaluation" => array("text" => "Self Evaluation", "alias" =>"self"), 
                                        "manager_evaluation" => array("text" => "Manager Evaluation", "alias" => "manager"), 
                                        "joint_evaluation" => array("text" => "Joint Evaluation", "alias" =>"joint"),
                                        "evaluation_review" => array("text" => "Evaluation Review", "alias" => "review")
                                   );                                   

	var $self_evaluation_rating;
	 /*
	   text
	 */
	var $self_evaluation_comment;
	 /*
	  text
	 */
	var $manage_evaluation_rating;
	 /*
	  text
	 */
	var $manage_evaluation_comment;
	 /*
	   int
	 */
	var $joint_evaluation_rating;
	 /*
	  text
	 */
	var $joint_evaluation_comment;
	 /*
	  int
	 */
	var $evaluation_review_rating;
	 /*
	  text
	 */
	var $evaluation_review_comment;
         
    var $self_evaluation         = 0;
    
    var $manager_evaluation      = 0;
    
    var $joint_evaluation        = 0;
    
    var $evaluation_review       = 0;           
     
    var $evaluation_complete     = false;
     
    public function __construct()
    {
        parent::__construct();
    }
     
	public function set_self_evaluation($value)
	{
	   $this -> self_evaluation += $value;
	}
	
	public function set_manager_evaluation($value)
	{
	   $this -> manager_evaluation += $value;
	}	
     
	public function set_joint_evaluation($value)
	{
	   $this -> joint_evaluation += $value;
	}
	
	public function set_evaluation_review($value)
	{
	   $this -> evaluation_review += $value;
	}	        
     
     function get_evaluations($optionSql = "")
     {
         $results = $this -> db -> get("SELECT * FROM #_".static::$table." WHERE 1 $optionSql");
         return $results;
     }
     
     function get_evaluation($optionSql = "")
     {
         $results = $this -> db -> getRow("SELECT * FROM #_".static::$table." WHERE 1 $optionSql");
         return $results;
     }
     
     function update_evaluation($data, $where)
     {
        $res = $this -> db -> updateWhere($data, $where);
        return $res;     
     }
     
     function delete_evaluation($where)
     {
        $res = $this -> db -> delete(static::$table, $where);
        return $res;
     }
     
     protected function _prepare_evaluation($data)
     {
        $insertdata[$data['next_evaluation']."_rating"]         = $data['rating'];
        $insertdata[$data['next_evaluation']."_comment"]        = $data['comment'];
        $insertdata[$data['next_evaluation']."_recommendation"] = $data['recommendation'];
        $insertdata['evaluation_period_id']                     = $_SESSION['evaluation_period']['id'];
        $insertdata[$data['next_evaluation']."_date"]           = date('Y-m-d H:i:s');
        return $insertdata;
     }
     
     function save_user_evaluations($data)
     {
        $results = $this -> db -> getRow("SELECT * FROM #_user_evaluations UE WHERE UE.user = '".$data['user_selected']."' 
                                          AND evaluation_period_id = '".$_SESSION['evaluation_period']['id']."'
                                         ");  
        $prev_eval                          = $data['prev_eval']; 
        $insertdata['user']                 = $data['user_selected'];
        $insertdata[$prev_eval]             = 1;
        $insertdata[$prev_eval.'_comment']  = $data['comment'];
        $insertdata['evaluation_by']        = $data['user'];
        $insertdata['evaluation_period_id'] = $_SESSION['evaluation_period']['id'];     
        if(!empty($results))
        {       
           $res                   = $this -> db -> updateData('user_evaluations', $insertdata, array("id" => $results['id']));
        } else {
           $insertdata['created'] = date('Y-m-d H:i:s');
           $res                   = $this -> db -> insert('user_evaluations', $insertdata);
        }
        return $res;
     }    
     
     function is_previous_evaluation_confirmed($user, $evaluation_type)
     {
        $option_sql = "";
        $results    = array();
        if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
        {
           if(!empty($evaluation_type))
           {
               $sql = " AND UE.".$evaluation_type." = 1";
               $results = $this -> db -> getRow("SELECT * FROM #_user_evaluations UE WHERE UE.user = '".$user."' 
                                                 AND evaluation_period_id = '".$_SESSION['evaluation_period']['id']."'
                                                 $sql
                                               ");             
            }                                               
        }
        if(!empty($results))
        {
          return TRUE;
        }
        return FALSE;
     } 
     
     function get_awaiting_review_list(User $userObj)
     {
        $users = array();
        if(isset($_SESSION['evaluation_period']) && !empty($_SESSION['evaluation_period']))
        {        
          $results = $this -> db -> get("SELECT * FROM #_user_evaluations UE WHERE 
                                         UE.evaluation_period_id = '".$_SESSION['evaluation_period']['id']."'
                                         AND (UE.joint = 1 AND joint_comment <> '') AND (review <> '1' AND  review_comment = '')
                                       "); 
          if(!empty($results))
          {
            foreach($results as $index => $evaluation)
            {
                $user                       = $userObj -> get_user(array('user' => $evaluation['user']));
                $users[$evaluation['user']] = $user['tkname']." ".$user['tksurname'];
            }
          }    
        }                                   
        return $users; 
     }
          
     function get_total_next_evaluations()
     {
         $evaluations = array();      
         if($this -> self_evaluation != 0)
         {
           $evaluations['next']     = "Self Evaluation"; 
           $evaluations['total']    = $this -> self_evaluation;
           $evaluations['next_key'] = "self";
         } else if($this -> manager_evaluation != 0) {
           $evaluations['next']     = "Manager Evaluation";
           $evaluations['total']    = $this-> manager_evaluation;
           $evaluations['next_key'] = "manager";
         } else if($this -> joint_evaluation != 0) {
           $evaluations['next']     = "Joint Evaluation";
           $evaluations['total']    = $this -> joint_evaluation;
           $evaluations['next_key'] = "joint";
         } else if($this -> evaluation_review != 0) {
           $evaluations['next']     = "Evaluation Review";
           $evaluations['total']    = $this -> evaluation_review;
           $evaluations['next_key'] = "review";
         } else {
            $this -> evaluation_complete = true;            
         }  
        return $evaluations;
     }     
     
     function evaluation_status_report($options = array())
     {
         $user_obj   = new User();
         $user_sql   = "";
         $userdata   = array(); 
         $year       = array();        
         if(isset($options['evaluationyear']))
         {
           $year  = EvaluationYear::findById($options['evaluationyear']);        
         }            
         if(isset($options['manager']) && !empty($options['manager']))
         {
           $user_sql = " AND TK.tkmanager = '".$options['manager']."' ";
           $userdata = $user_obj -> get_user(array('user' => $options['manager']));
         }
         if(isset($options['department']) && !empty($options['department']))
         {
           $user_sql .= " AND LD.id = '".$options['department']."' ";
         }
         $users  = $user_obj -> get_users($user_sql);
         if(isset($userdata) && !empty($userdata))
         {
           $users =  array_merge(array(array_slice($userdata, 0, -1)), $users);
         }
         $userstatuses = array();
         $allusers             = $user_obj -> get_users_list();
         if(isset($users) && !empty($users))
         {
            $i = 0;
            foreach($users as $index => $user)
            {
              $component_sql  = "";              
              $component_sql .= " AND UC.userid = '".$user['tkid']."' ";              
              if(isset($options['evaluationyear']))
              {
                $component_sql .= " AND UC.evaluationyear = '".$options['evaluationyear']."' ";
              } 
              $manager = "";
              if($user['tkmanager'] == "S")
              {
                $manager = $user['user'];
              } else if(isset($allusers[$user['tkmanager']])){
                $manager = $allusers[$user['tkmanager']];
              } else {
                $manager = $user['tkmanager'];
              }                      
              $components                  = UserComponent::fetchAll($component_sql);             
              $dates                       = array();     
              $userstatuses[$i]['user']    = $user['tkname']." ".$user['tksurname'];
              $userstatuses[$i]['manager'] = $manager;
              if(!empty($components))
              {             
                 $self_evaluation    = "";
                 $manager_evaluation = "";
                 $joint_evaluation   = "";
                 $review_evaluation  = "";

                 foreach($components as $index => $component)
                 {
                   $user_evals = self::_get_user_evaluation_status($component['id'], $options['evaluationyear'], $user, $options);
                   $evals      = $user_evals['evals'];
                   if(isset($evals['self_evaluation']))
                   {
                      //echo $user['tkid']." -- old - ".$self_evaluation." new self evals ".$evals['self_evaluation']."\r\n\n\n";
                      $self_evaluation  = ($self_evaluation > $evals['self_evaluation'] ? $self_evaluation : $evals['self_evaluation']); 
                   }
                   if(isset($evals['manager_evaluation']))
                   {
                      $manager_evaluation  = ($manager_evaluation > $evals['manager_evaluation'] ? $manager_evaluation : $evals['manager_evaluation']); 
                   }
                   //debug($evals);
                   if(isset($evals['joint_evaluation']))
                   {                      
                      $joint_evaluation  = ($joint_evaluation > $evals['joint_evaluation'] ? $joint_evaluation : $evals['joint_evaluation']);
                   }
                   if(isset($evals['evaluation_review']))
                   {
                      $review_evaluation  = ($review_evaluation > $evals['evaluation_review'] ? $review_evaluation : $evals['evaluation_review']); 
                   }                               
                 }
                 //echo $joint_evaluation."\r\n\n";
                 $userstatuses[$i]['selfeval']    = (empty($self_evaluation) ? "In Progress" : date("d-M-Y H:i:s", strtotime($self_evaluation)));
                 $userstatuses[$i]['managereval'] = (empty($manager_evaluation) ? "In Progress" : date("d-M-Y H:i:s", strtotime($manager_evaluation)));
                 $userstatuses[$i]['jointeval']   = (empty($joint_evaluation) ? "In Progress" : date("d-M-Y H:i:s", strtotime($joint_evaluation)));
                 $userstatuses[$i]['revieweval']  = (empty($review_evaluation) ? "In Progress" : date("d-M-Y H:i:s", strtotime($review_evaluation)));  
               } else {
                 $userstatuses[$i]['status'] = "There are no components setup for this user yet";
               }       
               $i++;                  
            }
         }
         $report['report_date'] = date("d-M-Y");
         $report['data']        = $userstatuses;
         $report['year']        = $year;
         return $report;
     }
     
     
     function _get_user_evaluation_status($componentid, $evaluationyear, $userdata, $options)
     {    
        $settings = $this-> db -> getRow("SELECT evaluate_on, self_evaluation, manager_evaluation, 
                                          joint_evaluation, evaluation_review 
                                          FROM #_matrix WHERE category_id = '".$userdata['categoryid']."' 
                                          AND evaluation_year = '".$evaluationyear."' 
                                          AND component_id = '".$componentid."'
                                        ");
        $evaluate_on = $settings['evaluate_on'];

        if($evaluate_on == "kpa")
        {
           $kpaObj                 = new KpaEvaluation();          
           $user_kpa_evaluation    = $kpaObj -> get_evaluation_statuses($userdata, $componentid, $options);
           /*foreach($settings as $key => $val)
           {
             if($val != 1)
             {
               $user_kpa_evaluation[$key] = "N/A";
             }
           }*/ 
           //return  $user_kpa_evaluation;    
           return array('evals' => $user_kpa_evaluation, 'settings' => $settings);
        } else if($evaluate_on == "kpi") {
           $kpiObj                 = new KpiEvaluation();
           $user_kpi_evaluations   = $kpiObj -> get_evaluations_statuses($userdata, $componentid, $options);         
           /*foreach($settings as $key => $val
           {
             if($val != 1)
             {
               $user_kpi_evaluations[$key] = "N/A";
             }
           }*/            
           return array('evals' => $user_kpi_evaluations, 'settings' => $settings);
        } else if($evaluate_on == "action") {
           $actionObj               = new ActionEvaluation();
           $user_action_evaluations = $actionObj -> get_evaluation_statuses($userdata, $componentid, $options);                  
           /*foreach($settings as $key => $val)
           {
             if($val != 1)
             {
               $user_action_evaluations[$key] = "N/A";
             }
           }*/           
           return array('evals' => $user_action_evaluations, 'settings' => $settings);
        }
     }
    
     function scores_report($options = array())
     {
           $user_obj   = new User();
           $user_sql   = "";
           $userdata   = array(); 
           $year       = array();        
           if(isset($options['evaluationyear']))
           {
              $year  = EvaluationYear::findById($options['evaluationyear']);        
           }           
           if(isset($options['manager']) && !empty($options['manager']))
           {
             $user_sql = " AND TK.tkmanager = '".$options['manager']."' ";
             $userdata = $user_obj -> get_user(array('user' => $options['manager']));
           }
           $users  = $user_obj -> get_users($user_sql);
           if(isset($userdata) && !empty($userdata))
           {
              $users =  array_merge(array(array_slice($userdata, 0, -1)), $users);
           }
           $i = 0;
           $allusers             = $user_obj -> get_users_list();
           foreach($users as $s_index => $user)
           {
              $optional_sql  = "";              
              $optional_sql .= " AND UC.userid = '".$user['tkid']."' ";              
              if(isset($options['evaluationyear']))
              {
                $optional_sql .= " AND UC.evaluationyear = '".$options['evaluationyear']."' ";
              } 
 
              $manager = "";
              if($user['tkmanager'] == "S")
              {
                $manager = $user['user'];
              } else if(isset($allusers[$user['tkmanager']])){
                $manager = $allusers[$user['tkmanager']];
              } else {
                $manager = $user['tkmanager'];
              }     
                      
              $components                    = UserComponent::fetchAll($optional_sql);             
              $dates                         = array();     
              $userstatuses[$i]['user']      = $user['tkname']." ".$user['tksurname'];
              $userstatuses[$i]['manager']   = $manager;
              if(!empty($components))
              {             
                $self          = "";
                $self_total    = 0;
                $manager       = "";
                $manager_total = 0;
                $joint         = "";
                $joint_total   = 0; 
                $review        = "";
                $review_total  = 0;
                foreach($components as $index => $component)
                {
                   $evals  = self::_get_user_evaluation_scores($component['id'], $user, $options);
                   $e      = $evals['evals']; 
                   if(isset($e['self_evaluation']))
                   {
                      $self       = $self + $e['self_evaluation']['totalscore']; 
                      $self_total = $e['self_evaluation']['totalcount'];
                   }
                   if(isset($e['manager_evaluation']))
                   {
                      $manager       = $manager + $e['manager_evaluation']['totalscore']; 
                      $manager_total = $e['manager_evaluation']['totalcount'];
                   }
                   if(isset($e['joint_evaluation']))
                   {
                      $joint       = $joint + $e['joint_evaluation']['totalscore'];
                      $joint_total = $e['joint_evaluation']['totalcount']; 
                   }
                   if(isset($e['review_evaluation']))
                   {
                      $review       = $review + $e['review_evaluation']['totalscore'];
                      $review_total = $e['review_evaluation']['totalcount']; 
                   }
                }
                 $userstatuses[$i]['selfeval']    = ($self_total != 0 ? round(((int)$self/($self_total*4)), 2) : 0); 
                 $userstatuses[$i]['managereval'] = ($manager_total != 0 ? round(((int)$manager/($manager_total*4)), 2) : 0);
                 $userstatuses[$i]['jointeval']   = ($joint_total != 0 ? round(((int)$joint/($joint_total*4)), 2) : 0) ;
                 $userstatuses[$i]['revieweval']  = ($review_total != 0 ? round(((int)$review/($review_total*4)), 2) : 0);
              } else {
                 $userstatuses[$i]['status'] = "There are no components setup for this user yet";
              }       
              $i++;
           }
           $report['data']        = $userstatuses;
           $report['report_date'] = date('d-M-Y');
           $report['year']        = $year;
         return $report;
     }
     
     function _get_user_evaluation_scores($componentid, $userdata, $options)
     {
        $settings = $this-> db -> getRow("SELECT evaluate_on, self_evaluation, manager_evaluation, 
                                          joint_evaluation, evaluation_review 
                                          FROM #_matrix WHERE category_id = '".$userdata['categoryid']."' 
                                          AND evaluation_year = '".$options['evaluationyear']."' 
                                          AND component_id = '".$componentid."'
                                        ");
        $evaluate_on = $settings['evaluate_on'];
        if($evaluate_on == "kpa")
        {
           $kpaObj                 = new KpaEvaluation();          
           $user_kpa_evaluation    = $kpaObj -> get_evaluation_scores($userdata, $componentid, $options);
           /*foreach($settings as $key => $val)
           {
             if($val != 1)
             {
               $user_kpa_evaluation[$key] = "N/A";
             }
           }*/ 
           return array('evals' => $user_kpa_evaluation, 'settings' => $settings);
        } else if($evaluate_on == "kpi") {
           $kpiObj                 = new KpiEvaluation();
           $user_kpi_evaluations   = $kpiObj -> get_evaluation_scores($userdata, $componentid, $options, $settings);         
           /*foreach($settings as $key => $val
           {
             if($val != 1)
             {
               $user_kpi_evaluations[$key] = "N/A";
             }
           }*/            
           return array('evals' => $user_kpi_evaluations, 'settings' => $settings);
        } else if($evaluate_on == "action") {
           $actionObj               = new ActionEvaluation();
           $user_action_evaluations = $actionObj -> get_evaluation_scores($userdata, $componentid, $options);                  
           /*foreach($settings as $key => $val)
           {
             if($val != 1)
             {
               $user_action_evaluations[$key] = "N/A";
             }
           }*/
           return array('evals' => $user_action_evaluations, 'settings' => $settings);
        }    
     }
     
     /*
        Save evaluation information for the specified user 
        @param array $userdata - specified the user information for which the evaluation information is saved for 
        @param $data - contains the information to be saved 
        @return id evaluation done
     
     public static function saveEvaluation($userdata, $data)
     {    
        $eveluationType = self::getEvalautionType($userdata, $data['componentid'], $data['reference']);
        if($eveluationType == FALSE)
        {
          return -2;
        } else { 
             $userEvaluation = self::_createEvaluations($eveluationType, $data);
             $insertdata = array();
             $insertdata['evaluation'] = $userEvaluation;
             $insertdata['insertuser'] = $_SESSION['tid'];
             $insertdata['assessfor']  = $userdata['tkid'];
             $insertdata['evaluationyear'] = $userdata['default_year'];
             $insertdata['componentid'] = $data['componentid'];
             $insertdata['reference'] = $data['reference'];
             $res = 0;
             if(!empty(self::$id))
             {
               $res = Evaluation::updateWhere($insertdata, array("id" => self::$id));
             } else {
                $res = Evaluation::saveData($insertdata);
             }
           return $res;
         }
     }*/
     
     /*
        Prepare evaluation information to be saved in the database
        @param string $evaluationType 
        @param array $data 
        @return string - information format that can be saved into the databse
     */
     private static function _createEvaluations($eveluationType, $data)
     {
        $newEvaluations = array($eveluationType => array("comment" => $data['comment'], "rating" => $data['rating'], "recommendation" => $data['recommendation']));
        $insertEvaluations = array();

        if(!empty(self::$userEvaluations))
        {
           $insertEvaluations = array_merge(self::$userEvaluations[$data['componentid']], $newEvaluations);
        } else {
          $insertEvaluations  = $newEvaluations;
        }  
        return serializeEncode($insertEvaluations);
     }
     
     /*
       Get the next evaluation type for specified filters
       @param array $user  - user information to get the evaluation for
       @param integer $componentid - componentid 
       @param integer $reference key used to get a specific evaluation data
       @return string - if there is still more evaluation to be done, @return TRUE if there are no more evaluations to be done
     */
     public static function getEvalautionType($user, $componentid, $reference)
     {
        $evaluationSettings = Performancematrix::getASetting($user['categoryid'], $user['default_year'],
                                                             array("selfevaluation", "managerevaluation", "jointevaluation", "evaluationreview")
                                                            );
        $sql = " AND assessfor = '".$user['tkid']."' AND componentid = '".$componentid."' AND evaluationyear = '".$user['default_year']."' AND reference= '".$reference."'";
        $userEvaluations    = self::getUserComponentEvaluation($sql);       
        self::$userEvaluations = $userEvaluations;      
        $evaluation = array();                                                
        foreach($evaluationSettings as $key => $evaluationSetting)
        {
            //if the evalation type is allowed for this users
            if(isset($evaluationSetting[$componentid]) && $evaluationSetting[$componentid] == 1)
            {
               //if evaluation type is allowed then check if the evaluation type is already saved for this user component 
               if(isset($userEvaluations[$componentid]) && !empty($userEvaluations[$componentid]))
               {
                  if(isset($userEvaluations[$componentid][$key]))
                  {
                     continue;  
                  } else {
                     return $key;
                  }
               } else {
                 return $key; 
               }  
            }
        }  
        return FALSE;
     }
     
     /*
       Get the component evaluation details
       @params string  - string with the filter to get a specific component evaluation information
       @return array component-evaluation information
     */
     public static function getUserComponentEvaluation($optionSql="")
     {
        $evaluation = Evaluation::find($optionSql);
        $userEvaluation = array();
        if(!empty($evaluation))
        {
          self::$id = $evaluation['id'];
          if(isset($evaluation['evaluation']) && !empty($evaluation['evaluation']))
          {
              $reference = $evaluation['componentid'];
              $userEvaluation[$reference] = serializeDecode($evaluation['evaluation']);
          }  
        }      
        return $userEvaluation;
     }
     /*
      Get the evaluation details of the user
      @params array $options - contains the filter data to get the evaluation data
      @return array $data eveluation information for the specified filters
     */
     public static function getUserEvaluations($options=array())
     {
         $sqlOption   = parent::createANDSqlOptions($options);
         //Evaluation::debug("SELECT * FROM #_".static::$table." WHERE 1 $sqlOption");
         $evaluations = Evaluation::findAll($sqlOption);
         $ratings     = RatingScale::getRatingInfo();
         
         $evaluationsData = array();
         $data            = array();
         if(!empty($evaluations))
         {
           foreach($evaluations as $index => $evaluation)
           {
              $component = $evaluation['componentid'];
              $reference = $evaluation['reference'];
              if(isset($evaluation['evaluation']) && !empty($evaluation['evaluation']))
              {                 
                $evaluationsData = serializeDecode($evaluation['evaluation']);                      
                foreach($evaluationsData as $evalType => $evaluationDetails)
                {             
                    if(isset($evaluationDetails['rating']))
                    {
                       $data[$reference][$evalType]['rating'] = $ratings[$evaluationDetails['rating']];
                    }
                    $data[$reference][$evalType]['comment'] = $evaluationDetails['comment'];
                }
              } 
           }
         }          
         return $data;
     }
     
     /*
      Get the evaluation statuses of the specified data ie kpa ,kpi or actions
      @param array $data - data containing the kpa, kpi or actions 
      @param array $user - user information of the user we are viewing the evaluation for
      @param integer $componentid - id of the component we are viewing the evaluation statuses for
      @return array containing the statuses of the specified data type, ie kpa,kpi or actions
    */
     public static function getEvaluationStatuses($data, $user, $componentid)
     {
        $evaluationStatuses  = array();
        foreach($data as $referenceId => $dataArr)
        { 
          //get the next evaluation type 
          $nextEvaluation = "";
          if(isset($user) && !empty($user))
          {
             $nextEvaluation                     = self::getEvalautionType($user, $componentid, $referenceId);
             self::$nextEvaluation[$referenceId] = $nextEvaluation; 
          }
          $evaluations = array();
          if(isset(self::$userEvaluations[$componentid]))
          {
            $evaluations  = self::$userEvaluations[$componentid];
          }
          
          foreach(Evaluation::$evaluationList as $eIndex => $eVal)
          {             
             if($nextEvaluation === $eIndex)
             {
               $evaluationStatuses[$referenceId][$eIndex] = array("status" => "ui-icon ui-icon-arrow-1-e", "name" => $eVal);               
             } else if(($nextEvaluation === TRUE) || array_key_exists($eIndex, $evaluations)) {
               $evaluationStatuses[$referenceId][$eIndex] = array("status" => "ui-icon ui-icon-check", "name" => $eVal);
             } else {
               $evaluationStatuses[$referenceId][$eIndex] = array("status" => "", "name" => $eVal);
             }        
          }
        }
        return $evaluationStatuses;
     }
     
     public static function outstandingEvaluations($categoryMatrix = array())
     {
        //debug($categoryMatrix);
        //exit();
     }
     
     public static function nextEvaluation()
     {
         return self::$nextEvaluation;
     }
     
     public function status_report($options = array())
     {
         /*$user_obj        = new User();
         $userdata        = $user_obj -> getUser($options);
         $subordinates    = $user_obj -> get_user_subordinates($options);
         $userstatuses    = array();

         if(!empty($subordinates))
         {
           $subordinates =  array_merge($subordinates, array(array_slice($userdata, 0, -1)));
           ///$evaluationperiodObj = new EvaluationPeriod();
           //$period_is_open      = $evaluationperiodObj -> is_open();
           $i = 0;
           foreach($subordinates as $s_index => $user)
           {
              $optional_sql  = "";              
              $optional_sql .= " AND UC.userid = '".$user['tkid']."' ";              
              if(isset($options['evaluationyear']))
              {
                $optional_sql .= " AND UC.evaluationyear = '".$options['evaluationyear']."' ";
              } 
                      
              $components                      = UserComponent::fetchAll($optional_sql);             
              $dates                         = array();     
              $userstatuses[$i]['user']      = $user['tkname']." ".$user['tksurname'];
              $userstatuses[$i]['manager']   = $userdata['tkname']." ".$userdata['tksurname'];
 
           }
         }*/
         return $userstatuses;
     }
     
    
    function get_components_next_evaluation($evaluation_list)
    {     
      $next = "";
      if(in_array('self', $evaluation_list))
      {
         return  "self";
      } else if(in_array('manager', $evaluation_list)) {
         return "manager";
      } else if(in_array('joint', $evaluation_list)) {
         return "joint";
      } else if(in_array('review', $evaluation_list)) {
         return "review";
      }          
      return $next;
    }
    
     function get_components_previous_evaluation($setting, $eval)
     {
        if(empty($eval)) {
           if($setting['evaluation_review'])
           {
             return "review";
           } else if($setting['joint_evaluation']) {
             return "joint";
           } else if($setting['manager_evaluation']) {
             return "manager";
           } else if($setting['self_evaluation']) {
             return "self";
           } 
        } else if($eval == "self") {
           return "self";
        } else if($eval == "manager") {
           if($setting['self_evaluation'])
           {
             return "self";
           } else {
             return "manager";
           }
        } else if($eval == "joint") {
           if($setting['manager_evaluation'])
           {
             return "manager";
           } else if($setting['self_evaluation']) {
             return "self";
           } else {
             return "joint";
           }
        } else if($eval == "review") {
           if($setting['joint_evaluation'])
           {
             return "joint";
           } else if($setting['manager_evaluation']) {
             return "manager";
           } else if($setting['self_evaluation']) {
             return "self";
           } else {
             return "review";
           }        
        }
     }
     
     public function get_next_evaluation($settings, $evaluations, $rating)
     {
         $next_evaluation   = "";
         $evaluation_status = array();
         $_evaluations      = array();
         foreach($settings as $eval_type => $status)
         {
            if(isset($this -> evaluation_keys[$eval_type]))
            {
               $key  = $this -> evaluation_keys[$eval_type]['alias'];
               //can you evaluate at this evaluation type
               if($status == 1)
               {
                    if(!empty($evaluations))
                    {
                       //if there hasn't been an evaluation of the current evaluation type, then make the current and fist on the next evaluation type
                       if(empty($evaluations[$key.'_rating']) || empty($evaluations[$key.'_comment']) 
                        || empty($evaluations[$key.'_recommendation']))
                        {
                           if(empty($next_evaluation))
                           {
                                $next_evaluation         = $key;
                                $evaluation_status[$key] = array("status"      => "ui-icon ui-icon-arrow-1-e",
                                                                 "name"        => $status, 
                                                                 "description" => $this -> evaluation_keys[$eval_type]['text']
                                                                 );
                            } else {
                                $evaluation_status[$key] = array("status"      => "",
                                                                  "name"        => $status, 
                                                                  "description" => $this -> evaluation_keys[$eval_type]['text']
                                                                  );     
                            }                        
                        } else {
                           $_evaluations[$key]['rating']         = $rating[$evaluations[$key."_rating"]];
                           $_evaluations[$key]['comment']        = $evaluations[$key."_comment"];
                           $_evaluations[$key]['recommendation'] = $evaluations[$key."_recommendation"];
                           $_evaluations[$key]['ratingid']       = (int)$evaluations[$key."_rating"];
                           $_evaluations[$key]['eval_id']        = (int)$evaluations['id'];
                           $evaluation_status[$key]              = array("status"      => "ui-icon ui-icon-check",
                                                                         "name"        => $status, 
                                                                         "description" => $this -> evaluation_keys[$eval_type]['text']);                           
                        }
                    } else {
                       if(empty($next_evaluation))
                       {
                            $next_evaluation         = $key;
                            $evaluation_status[$key] = array("status"      => "ui-icon ui-icon-arrow-1-e",
                                                             "name"        => $status, 
                                                             "description" => $this -> evaluation_keys[$eval_type]['text']
                                                                  );                                                   
                        } else {
                            $evaluation_status[$key] = array("status"      => "",
                                                              "name"        => $status, 
                                                              "description" => $this -> evaluation_keys[$eval_type]['text']
                                                              );     
                        }
                    }
               } else { 
                 $evaluation_status[$key] = array("status"=> "ui-icon ui-icon-close",
                                                  "name"        => $status, 
                                                  "description" => $this -> evaluation_keys[$eval_type]['text']
                                                  );
               }
            }
         }
         $result['next_evaluation'] = $next_evaluation;
         $result['statuses']        = $evaluation_status;
         $result['evaluations']     = $_evaluations;
         return $result;
     }
     
     private static $nextEvaluation = array();
}
?>
