<?php
class EvaluationPhaseComplete extends EmailNotification
{

	function sendSelfEvaluationEmail($user, $managerdata)
	{
	    $this -> message  = "Dear ".$user['managername']."\r\n\n";
        //$this -> message .= "This is a notification : \r\n\n";		    	
		$this -> message .= "Your employee ".$user['user'].", has completed the self-evaluation of their Perfomance Matrix for the evaluation period ".$_SESSION['evaluation_period']['start_date']." - ".$_SESSION['evaluation_period']['end_date']." of the evaluation year ".$user['start']." - ".$user['end'].". \r\n";		
		$this -> message .= "Please access the system to complete the manager evaluation.\r\n\n";						
        $this -> message .= $this -> emailSignature();							
		$this -> subject  = "Manage evaluation for employee ".$user['user']." is ready for completion";
		$this -> to    	  = $managerdata['tkemail'];
		return $this->notify();
	}	
	
	function sendManagerEvaluationEmail($employeedata)
	{
	    $this -> message  = "Dear ".$employeedata['user']."\r\n\n";
        //$this -> message .= "This is a notification : \r\n\n";		    	
		$this -> message .= "Your manager ".$employeedata['managername'].", has completed the manager evaluation of your Perfomance Matrix for the evaluation period ".$_SESSION['evaluation_period']['start_date']." - ".$_SESSION['evaluation_period']['end_date']." of the evaluation year ".$employeedata['start']." - ".$employeedata['end'].". \r\n";		
		$this -> message .= "Please arrange a meeting with your manager to complete the joint evaluation on the system.\r\n\n";						
        $this -> message .= $this -> emailSignature();							
		$this -> subject  = "Joint evaluation for employee ".$employeedata['user']." is ready for completion";
		$this -> to    	  = $employeedata['tkemail'];
		return $this->notify();
	}	
		
}
?>
