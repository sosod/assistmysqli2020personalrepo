<?php
/*
This defined the period in which the New functionality is available to the users
Specified the start and the end date
*/
class CreationPeriod extends Model
{
     private $start_date;
     
     private $end_date;
     
     private $evaluationyear;
     
     protected static $table = "creationperiods";
     
     function __construct()
     {
        parent::__construct();
     }     
     
     function getCreationPeriod($options = array())
     {
        $optionSql = parent::createANDSqlOptions($options);          
        $result = parent::find($optionSql);  
        if(!empty($result))
        {
          $result['start_date'] = date("d-M-Y", strtotime($result['start_date']));
          $result['end_date'] = date("d-M-Y", strtotime($result['end_date']));
        }          
        return $result;  
     }

     public static function isValid($user)
     {
        $creationPeriod  = array();
        $userPeriods     = array();
        if(isset($user) && !empty($user))
        {
          $userPeriods     = DBConnect::getInstance() -> getRow("SELECT * FROM #_user_creation_periods 
                                                                 WHERE user_id = '".$user['tkid']."' 
                                                                 AND evaluationyear = '".$user['default_year']."' 
                                                                ");                                                      
         if(!empty($userPeriods))
         {
            $user_period = date('Y-m-d 23:59:59', strtotime($userPeriods['end_date']));
            if(Validation::dateDiff(date("Y-m-d H:i:s"), $user_period))
            {
              return TRUE; 
            }
         } else {
            $creationPeriod  = CreationPeriod::find(" AND evaluationyear = '".$user['default_year']."'");             
            if(isset($creationPeriod) && !empty($creationPeriod))
            {
               $creation_period = date('Y-m-d 23:59:59', strtotime($creationPeriod['end_date']));
               if(Validation::dateDiff(date("Y-m-d H:i:s"), $creation_period))
               {
                 return TRUE; 
               }
            }         
          }
        }
        return FALSE;
     }
     
     
     public static function getUserEvaluationPeriod($options = array())
     {
        $creationPeriod = array();
        $creationPeriod = DBConnect::getInstance() -> getRow("SELECT * FROM #_user_creation_periods WHERE user_id = '".$options['user']."' AND evaluationyear = '".$options['evaluationyear']."' ");
        if(!empty($creationPeriod))
        {
           $creationPeriod['start_date'] = date("d-M-Y", strtotime($creationPeriod['start_date']));
           $creationPeriod['end_date'] = date("d-M-Y", strtotime($creationPeriod['end_date']));
           $creationPeriod['using_user'] = true;
        } else {          
            $creationPeriod = parent::find(" AND evaluationyear = '".$options['evaluationyear']."'");  
            if(!empty($creationPeriod))
            {
               $creationPeriod['start_date'] = date("d-M-Y", strtotime($creationPeriod['start_date']));
               $creationPeriod['end_date'] = date("d-M-Y", strtotime($creationPeriod['end_date']));
            }  
            $creationPeriod['comment'] = "";
            $creationPeriod['creation_period'] = $creationPeriod['id'];
            $creationPeriod['using_user'] = false;
        }
        
        return $creationPeriod;
     }
     
     public static function saveUserSpecific($data)
     {
        $data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
        $data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
        $res = DBConnect::getInstance() -> insert('user_creation_periods', $data);
        return $res;
     }
     
     public static function updateUserSpecific($data)
     {
        $data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
        $data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
        $res = DBConnect::getInstance() -> updateData('user_creation_periods', $data, array("id" => $data['id']));
        return $res;  
     }

}
?>
