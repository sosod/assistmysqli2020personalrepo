<?php
class Notifications extends Model
{
    protected static $table = 'user_notifications';


	function __construct()
	{
		parent::__construct();
	}
	
	function save_user_notifications($user_notifications)
	{
	   $insert_data = array();
	   if(!empty($user_notifications))
	   {	    
	      foreach($user_notifications as $key => $notification)
	      {
	         if($notification['allow'] == 1)
	         {
	           $notification_type = "";
	           foreach($notification as $n_index => $type)
	           {
	             if($type == 1)
	             {
	                $notification_type = "email";
	             } else if($type == 2){
	                $notification_type = "sms";
	             } else if($type == 3){
	                $notification_type = "both";
	             } else {
	                $notification_type = "email";
	             }
	           }
	           $insert_data[$key] = $notification_type;	      
	         } else {
	           $insert_data[$key] = "";
	         }
	      }
	   }
	   $insert_data['user_id'] = $_SESSION['tid'];
	   $insert_data['created'] = date("Y-m-d H:i:s");  
       $res                    = $this -> db -> insert(static::$table, $insert_data);
       return $res;
	}
	
	function get_user_notifications($options_sql)
	{
	   $sql     = " AND user_id = '".$_SESSION['tid']."' ";
	   $results = $this -> db -> getRow("SELECT * FROM #_".static::$table." WHERE 1 $sql");
       return $results;
	}

	
}
