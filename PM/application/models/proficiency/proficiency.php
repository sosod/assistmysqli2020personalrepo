<?php
class Proficiency extends Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	static function fetchAll( $options = "")
	{
		$results = DBConnect::getInstance()->get("SELECT id , value, sort, status , description FROM assist_".$_SESSION['cc']."_master_proficiencies 
		                                          WHERE status & 2 <> 2");
		return $results;
	}
	
	function fetch( $id )
	{
		$result = DBConnect::getInstance()->get("SELECT id, value, sort, status , description FROM assist_".$_SESSION['cc']."_master_proficiencies 
		                                         WHERE id = $id");
		return $result;
	}
	
	public static function findList($options="")
	{
	   $proficiencyList = array();
	   $proficiencies = self::fetchAll();
	   foreach($proficiencies as  $pindex => $pVal)
	   {
	     $proficiencyList[$pVal['id']] = "<em>".$pVal['value']."</em>";
	   }
	   return $proficiencyList;
	}
	
}
