<?php
class Joblevel extends Model
{

    protected static $table = 'category_joblevel';

	function __construct()
	{
		parent::__construct();
	}
	
	public static function fetchAll( $options = "")
	{
		$results = DBConnect::getInstance()->get("SELECT JL.id, JL.value AS name, JL.orgpositionid, JL.joblevel,
		                                          OP.value as orgposition, OP.description AS description, OP.orglevel
								            FROM assist_".$_SESSION['cc']."_master_list_joblevel JL
								            INNER JOIN assist_".$_SESSION['cc']."_master_list_orgposition OP 
								            ON OP.id = JL.orgpositionid
								            WHERE 1 ORDER BY JL.joblevel DESC
            								  ");
		return $results;
	}
	
	public static function getOrgpositions()
	{
		$results = DBConnect::getInstance()->get("SELECT JL.id, JL.value AS joblevel
								                  FROM assist_".$_SESSION['cc']."_master_list_joblevel JL
								                  WHERE 1
            								   ");
		return $results;	    
	}
	
	function getUsed()
	{
		$results = $this->db->get("SELECT joblevel FROM #_category_joblevel");
		return $results;
	}
	
	function getCategoryJobLevel( $catid )
	{
	    $joblevels = $this->db->getRow("SELECT joblevel FROM #_category_joblevel WHERE category_id = ".$catid);
	    $levels =  $this->db->get("SELECT * FROM assist_".$_SESSION['cc']."_master_list_joblevel WHERE id IN(".$joblevels['joblevel'].")");
		return $levels;
	}	
	
	function update_job_level($data)
	{
	   $this -> attach(new JobLevelAuditLog());
        $res = $this -> update_where($data, array("joblevel" => $data['joblevel']));
        return Message::update($res, " category job level #".$res);	     
	}
	
	public function save_multiple($data)
	{
	   $res             = 0;    
	   $joblevels       = $this -> db -> get("SELECT * FROM #_".static::$table." WHERE evaluationyear = '".$data['evaluationyear']."' ");
	   $postedJobLevels = array();
	   foreach($data as $jIndex => $jVal)
	   {
	     if((substr($jIndex, 0, 9) == "category_") && !empty($jVal))
	     {
	       $levelRef                   = substr($jIndex, 9);
	       $postedJobLevels[$levelRef] = $jVal;
	     }
	   }	    
	   if(!empty($joblevels))
	   {
	     foreach($joblevels as $index => $val)
	     {
	        if(isset($postedJobLevels[$val['joblevel']]))
	        {
	          if($postedJobLevels[$val['joblevel']] !== $val['category_id'])
	          {	             	             
	             $where = array('joblevel' => $val['joblevel'], 'evaluationyear' => $data['evaluationyear']);
	             //echo "ref ss -- ".$val['joblevel']." From category ".$val['category_id']." to ".$postedJobLevels[$val['joblevel']]."\r\n\n\n";
	             $this -> attach(new JobLevelAuditLog());
	             $res  += $this -> update_where(array('category_id' => $postedJobLevels[$val['joblevel']]), $where);
	          }
	          unset($postedJobLevels[$val['joblevel']]);
	        }  
	     }     
	   }   
       if(!empty($postedJobLevels))
       {
          foreach($postedJobLevels as $jobLevel => $categoryId)
          {       
             $insertdata = array('joblevel' => $jobLevel, 'category_id' => $categoryId, 'evaluationyear' => $data['evaluationyear']);
             $res 	    += JobLevel::saveData($insertdata);
          }
       }	   
	   return $res;
	}
	
  function get_categories_used($option_sql = "")
  {
      $results      = $this -> db -> get("SELECT category_id FROM #_".static::$table." WHERE 1 $option_sql GROUP BY category_id");
      $matrix_cats  = array();  
      foreach($results as $index => $matrix)
      {
         $matrix_cats[$matrix['category_id']] =  $matrix['category_id']; 
      }
      return $matrix_cats;
  }

}
