<?php
class JobLevelAuditLog extends AuditLog
{
     
    public function __construct()
    {
        parent::__construct();
    }

    function notify($postData, $where, $tablename)
    { 

        $joblevel    = $this -> db -> getRow("SELECT * FROM #_".$tablename." WHERE joblevel = '".$where['joblevel']."' ");
        $categoryObj = new Performancecategories();
        $categories  = $categoryObj -> get_all_categories();
        $changes     = array();
        if($joblevel['category_id'] != $postData['category_id'])
        {
           $from                            = $categories[$joblevel['category_id']]['name'];
           $to                              = $categories[$postData['category_id']]['name'];
           $changes['performance_category'] = array('from' => $from, 'to' => $to);
        }
        if(!empty($changes))
        {
            $changes['user']          = $_SESSION['tkn']; 
            $changes                  = serializeEncode($changes);
            $insertdata['ref']        = $where['joblevel']."".$postData['category_id'];
            $insertdata['changes']    = $changes;
            $insertdata['insertuser'] = $_SESSION['tid']; 
            static::$table            = $tablename."_logs";
            $this -> save($insertdata);        
        }       
    }     
     


}
?>
