<?php
class EvaluationPeriod extends Model
{
   protected static $table = "list_evaluation_periods";  
   
   const OPENED = 4;
   
   const CLOSED = 8;    
       
   function __construct()
   {
     parent::__construct();
   }    

   function is_open($options = array())
   {
      $userObj = new User();
      $user    = $userObj -> get_user($options);
      $results = $this -> db -> get("SELECT EP.id, DATE_FORMAT(EP.start_date, '%d-%b-%Y') AS start_date, 
                                     DATE_FORMAT(EP.end_date, '%d-%b-%Y') AS end_date, 
                                     DATE_FORMAT(EP.open_from, '%d-%b-%Y') AS open_from, 
                                     DATE_FORMAT(EP.open_to, '%d-%b-%Y') AS open_to, EF.name,
                                     EP.evaluation_frequency_id, EP.status, M.evaluation_frequency
                                     FROM #_list_evaluationfrequencies EF 
                                     INNER JOIN #_list_evaluation_periods EP ON EF.id = EP.evaluation_frequency_id 
                                     INNER JOIN #_matrix M ON M.evaluation_frequency = EP.evaluation_frequency_id  
                                     WHERE EF.evaluationyear = '".$user['default_year']."' AND M.category_id = '".$user['categoryid']."'
                                    ");
      $evaluate = FALSE;
      //debug($results);
      if(!empty($results))
      {
         foreach($results as $index => $evaluation)
         {
            if(!empty($evaluation['open_from']) && !empty($evaluation['open_to']))
            {
               $from  = strtotime($evaluation['open_from']);
               $to    = strtotime($evaluation['open_to']);
               $today = strtotime(date("Y-m-d"));
               if($today >= $from && $today <= $to)
               {
                   $modref = $_SESSION['modref'];
                   $_SESSION['evaluation_period'] = $evaluation;
                   $evaluate = TRUE;
               }
            }
         }
      }
      return $evaluate;
   }
     
   function get_evaluation_period($option_sql = "")
   {
      $result = $this -> db -> getRow("SELECT date_format(EP.start_date, '%d-%b-%Y') AS start, start_date,
                                       date_format(EP.end_date, '%d-%b-%Y') AS end, end_date,
                                       date_format(EP.open_from, '%d-%b-%Y') AS open, open_from,
                                       date_format(EP.open_to, '%d-%b-%Y') AS close, open_to,
                                       EP.status, EP.evaluation_frequency_id, EP.id AS evaluation_period_id
                                       FROM #_".static::$table." EP WHERE 1 $option_sql
                                      ");
      return $result;
   }  
     
   public static function updateEvaluationPeriods($periods)
   {
      $res = 0;   
      foreach($periods as $freqId => $period)
      {           
          foreach($period as $id => $val)
          {
             $updatedata['start_date'] = date('Y-m-d', strtotime($val['startdate']));
             $updatedata['end_date']   = date('Y-m-d', strtotime($val['enddate']));
            $res += parent::updateWhere($updatedata, array('id' => $id, 'evaluation_frequency_id' => $freqId));
          }
      }
      return $res;
   }
     
     
   public static function saveEvaluationPeriods($periods)
   {
       $ids = 0;
      foreach($periods as $freqId => $period)
      {
          foreach($period as $index => $periodTime)
          {
            $start_date = ($periodTime['startdate'] == "" ? "" : date('Y-m-d', strtotime($periodTime['startdate'])));
            $end_date   = ($periodTime['enddate'] == "" ? "" : date('Y-m-d', strtotime($periodTime['enddate'])));
            $ids += parent::saveData(array('evaluation_frequency_id' => $freqId,
                                           'start_date'             => $start_date,
                                           'end_date'               => $end_date
                                        )
                                    );
          }
      }
      return $ids;
   }
   
   public static function getEvaluationPeriods($evaluationYear)
   {
     $periods  = EvaluationPeriod::query("SELECT EP.id, DATE_FORMAT(EP.start_date, '%d-%b-%Y') AS start_date, 
                                          DATE_FORMAT(EP.end_date, '%d-%b-%Y') AS end_date, EF.name,
                                          EP.evaluation_frequency_id, EP.status
                                          FROM #_list_evaluationfrequencies EF 
                                          INNER JOIN #_list_evaluation_periods EP ON EF.id = EP.evaluation_frequency_id 
                                          WHERE EF.evaluationyear = '".$evaluationYear."'
                                          ");
      return $periods;                                    
   }

  public function process_evaluation_periods($evaluationyearid, $frequencies, $periods)
  {     
     //$evaluationPeriods = EvaluationPeriod::getEvaluationPeriods($evaluationyear);
     $frequency_periods = array();
     $res               = 0; 
     foreach($frequencies as $k => $id)
     {
        if(array_key_exists($id, $periods))
        {
           $frequency_periods    = $this -> db -> get("SELECT * FROM #_".static::$table." WHERE evaluation_frequency_id = '".$id."' ");
           if(!empty($frequency_periods))
           {
              //update the evaluation periods
              $res += $this -> update_evaluation_periods($periods[$id], $id);     
              //debug($periods);
              //echo "updatre";
           } else {
             //save the evaluation periods";
             $res += $this -> save_evaluation_periods($periods[$id], $id);
           }            	
        }
     }    
     return $res;
  }
  
  function save_evaluation_periods($periods, $freqId)
  {
      $ids = 0;
      foreach($periods as $index => $periodTime)
      {
        $insertdata['start_date']              = ($periodTime['startdate'] == "" ? "" : date('Y-m-d', strtotime($periodTime['startdate'])));
        $insertdata['end_date']                = ($periodTime['enddate'] == "" ? "" : date('Y-m-d', strtotime($periodTime['enddate'])));
        $insertdata['created']                 = date('Y-m-d');
        $insertdata['evaluation_frequency_id'] = $freqId;
        $ids                                  += $this -> save($insertdata);
      }
      return $ids;
  }
  
   public function update_evaluation_periods($periods, $freqId)
   {
      $res = 0;              
      foreach($periods as $id => $val)
      {
         $updatedata['start_date'] = date('Y-m-d', strtotime($val['startdate']));
         $updatedata['end_date']   = date('Y-m-d', strtotime($val['enddate']));
         $res                     += $this -> updateWhere($updatedata, array('id' => $id, 'evaluation_frequency_id' => $freqId));
      }
      return $res;
   }  

   function get_evaluation_periods($evaluationyear)
   {
      $results = $this -> db -> get("SELECT EP.id, DATE_FORMAT(EP.start_date, '%d-%b-%Y') AS start_date, 
                                     DATE_FORMAT(EP.end_date, '%d-%b-%Y') AS end_date, 
                                     DATE_FORMAT(EP.open_from, '%d-%b-%Y') AS open_from, 
                                     DATE_FORMAT(EP.open_to, '%d-%b-%Y') AS open_to,
                                     EF.name, EP.evaluation_frequency_id, EP.status
                                     FROM #_list_evaluationfrequencies EF 
                                     INNER JOIN #_list_evaluation_periods EP ON EF.id = EP.evaluation_frequency_id 
                                     WHERE EF.evaluationyear = '".$evaluationyear."'
                                    ");
      $frequency_periods = array();
      if(!empty($results))
      {
          foreach($results as $periodId => $period)
          {       
             $frequency_periods[$period['evaluation_frequency_id']][$period['id']] = $period;
          }
      }                                  
      return $frequency_periods;
   }

   public static function getFrequencyEvaluationPeriods($evaluationyear)
   {
     $evaluationPeriods = array();
     $evaluationPeriods = EvaluationPeriod::getEvaluationPeriods($evaluationyear);
     $frequencyPeriods = array();
     if(!empty($evaluationPeriods))
     {
          foreach($evaluationPeriods as $periodId => $period)
          {       
             $frequencyPeriods[$period['evaluation_frequency_id']][$period['id']] = $period;
          }
     }
     return $frequencyPeriods;
   }

   function open_evaluation_period($data)
   {
      $updatedata['open_from'] = date('Y-m-d', strtotime($data['open_from']));
      $updatedata['open_to']   = date('Y-m-d', strtotime($data['open_to']));
      $updatedata['status']    = 5;
      $res                     = $this -> update_where($updatedata, array('id' => $data['id']));
      return $res; 
   }

   function get_user_evaluation_period($options = array())
   {
      $results = $this -> db -> getRow("SELECT UP.id,
                                        DATE_FORMAT(UP.open_from, '%d-%b-%Y') AS open, UP.open_from,
                                        DATE_FORMAT(UP.open_to, '%d-%b-%Y') AS close, UP.open_to,
                                        UP.comment, UP.evaluation_period_id,
                                        DATE_FORMAT(EP.start_date, '%d-%b-%Y') AS start, 
                                        DATE_FORMAT(EP.end_date, '%d-%b-%Y') AS end                                        
                                        FROM #_user_evaluation_periods UP
                                        LEFT JOIN #_list_evaluation_periods EP ON UP.evaluation_period_id = EP.id
                                        INNER JOIN #_list_evaluationfrequencies EF ON EF.id = EP.evaluation_frequency_id
                                        WHERE UP.user_id = '".$options['employee']."'
                                        AND EP.id = '".$options['evaluationperiod']."'
                                        AND EF.evaluationyear = '".$options['evaluationyear']."'
                                       ");
       $evaluation_periods = array();
       if(empty($results))
       {
          $option_sql               = " AND EP.id = '".$options['evaluationperiod']."' "; 
          $ep_results               = $this -> get_evaluation_period($option_sql);
          $ep_results['new_period'] = true;
          return $ep_results;
       } else {
         $user_results               = $results;
         $user_results['new_period'] = false;
         return $user_results;
       }
   }
   
   function update_user_evaluation_period($data)
   {
      $user_period = array();
      $user_period['user_id']              = $data['user_id'];
      $user_period['open_from']            = date('Y-m-d', strtotime($data['open_from']));
      $user_period['open_to']              = date('Y-m-d', strtotime($data['open_to']));
      $user_period['evaluation_period_id'] = $data['evaluation_period_id'];
      $user_period['comment']              = $data['comment'];
      $user_period['created']              = date('Y-m-d');
      if(isset($data['savenew']) && $data['savenew'] == "true")
      {
        $user_period['insertuser'] = $_SESSION['tid'];
        $res =  $this -> db -> insert("user_evaluation_periods", $user_period);
      } else {
        $where                         = " user_id = '".$data['user_id']."' 
                                          AND evaluation_period_id = '".$data['evaluation_period_id']."' ";
        $res = $this -> db -> update("user_evaluation_periods", $user_period, $where);
      }
      return $res;
   }

    function get_frequencies_used($option_sql = "")
    {
      $results     = $this -> db -> get("SELECT evaluation_frequency_id FROM #_list_evaluation_periods EP WHERE 1");
      $frequencies = array();
      foreach($results as $index => $result)
      {
         $frequencies[$result['evaluation_frequency_id']] = $result['evaluation_frequency_id'];
      }
      return $frequencies;
    }   
}
?>
