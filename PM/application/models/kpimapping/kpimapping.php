<?php
class KpiMapping extends Model
{
    private $risk = null;
    
    private $qry  = null;
    
    private $compl = null;
    
    private $sla   = null; 
    
    protected static $table = 'kpi_mapping';
    
    private $self_evaluation     = 0;
    
    private $manager_evaluation  = 0;
    
    private $joint_evaluation    = 0;
    
    private $evaluation_review   = 0;       
    
    function __construct()
    {
        parent::__construct();
     }    
    
    public static function getQueryTypes($optionSql="")
    {
          $results = DBConnect::getInstance()->get("SELECT DISTINCT(QT.id), QT.name, QT.shortcode, QT.description, QT.active
                                                    FROM assist_".$_SESSION['cc']."_qry_types QT
                                                    WHERE QT.active & 2 <> 2 $optionSql
                                                   ");    
        /*$results = DBConnect::getInstance()->get("SELECT DISTINCT(QT.id), QT.name, QT.shortcode, QT.description, QT.active
                                                  FROM assist_".$_SESSION['cc']."_qry_types QT
                                                  INNER JOIN assist_".$_SESSION['cc']."_qry_query_register QR ON QR.type = QT.id
                                                  INNER JOIN assist_".$_SESSION['cc']."_qry_actions QA ON QA.risk_id = QR.id
                                                  WHERE QT.active & 2 <> 2 $optionSql
                                               ");
                                               */
        return $results;
    }
    
    public static function getRiskTypes($optionSql="")
    {
        $results = DBConnect::getInstance()->get("SELECT DISTINCT(RT.id), RT.shortcode, RT.description, RT.name, RT.active 
                                                  FROM assist_".$_SESSION['cc']."_risk_types RT
                                                  WHERE RT.active & 2 <> 2 $optionSql
                                                ");
        return $results;
    }
    
    public static function getRiskCategories($optionSql="")
    {     
        $results = DBConnect::getInstance()->get("SELECT id, shortcode, description, name, active 
                                                  FROM assist_".$_SESSION['cc']."_risk_categories
                                                  WHERE active & 2 <> 2 $optionSql
                                                ");
        return $results;
    }
    
    public static function getQueryCategories($optionSql="")
    {
        $results = DBConnect::getInstance()->get("SELECT id, shortcode, description, name, active 
                                                  FROM assist_".$_SESSION['cc']."_qry_categories
                                                  WHERE active & 2 <> 2 $optionSql
                                                ");
        return $results;
    }    
    
    public static function getLegislations($optionSql = "")
    {
        $results = DBConnect::getInstance()->get("SELECT id, name, reference, legislation_status 
                                                  FROM assist_".$_SESSION['cc']."_compl_legislation
                                                  WHERE 1 $optionSql
                                                ");
        return $results;     
    }
    
    public static function getContractsTypes($optionSql = "")
    {
        $results = DBConnect::getInstance()->get("SELECT id, shortcode, name, description, status 
                                                  FROM assist_".$_SESSION['cc']."_sla_contract_type
                                                  WHERE 1 $optionSql
                                                ");
        return $results;     
    }
     
     
    public static function taskModules()
    {
       $results =  DBConnect::getInstance() -> get("SELECT modtext, modref FROM assist_menu_modules WHERE modyn ='Y' AND modlocation = 'ACTION'");       
       return $results;
    } 
        
    public static function getModuleData($modref, $optionSql = "")
    {
          $results = DBConnect::getInstance()->get("SELECT id, value AS name 
                                                   FROM assist_".$_SESSION['cc']."_".strtolower($modref)."_list_topic
                                                   WHERE yn = 'Y' $optionSql ORDER BY value
                                                ");      
        $moduledata = array();
        foreach($results as $index => $moddata)
        {
          $moduledata[] = array("id" => $moddata['id'], "name" => html_entity_decode($moddata['name'],ENT_QUOTES,'ISO-8859-1'));
        }   
        return $moduledata;     
    }        
        
    function fetchAll($options = "") 
    {
        $results = DBConnect::getInstance()->get("SELECT * FROM #_kpimapping WHERE 1 $options");
        return $results;
    }
    
    function fetch($option = "")
    {
        $result = DBConnect::getInstance()->get("SELECT * FROM #_kpimapping WHERE 1 $options");
        return $result;
    }
    
    public function getModuleActions($kpi, $modulemapping, $userdata, $setting, $options = array(), $actionevalautionObj = null)
    {  
       $mapping = serializeDecode($modulemapping);
       if(!empty($mapping))
       {
          switch($mapping['module'])
          {
             case "rsk":
               $risktype = $mapping['module_rsk'];
               $actions =  $this -> getRiskActions($kpi, $risktype, $mapping['subtype_'.$risktype], $setting, $options, $actionevalautionObj);        
                  return $actions;
             break;
             case "qry":
                  $risktype = $mapping['module_qry'];
                  $actions =  $this -> getQueryActions($kpi, $risktype, $mapping['subtype_'.$risktype], $setting, $options, $actionevalautionObj);
                  return $actions;
             break;
             case "sla":
                $risktype = $mapping['module_sla'];
                $actions  =  $this -> getSlaActions($kpi, $risktype, $mapping['subtype_'.$risktype], $setting, $options, $actionevalautionObj);    
                return $actions;
             break;
             case "task":            
                $moduletype = $mapping['module'];
                $flavour    = $mapping['module_'.$moduletype];
                $actions    = $this -> getTaskActions($kpi, $moduletype, $flavour, $userdata, $setting, $options, $actionevalautionObj);
                return $actions;
             break;   
             case "compl":
                $actions =  $this -> getComplActions($kpi, $mapping['module_compl'], $setting, $options, $actionevalautionObj);
                return $actions;
             break;                              
             default:
               $moduletype = $mapping['module'];
               $flavour    = $mapping['module_'.$moduletype];
               $actions =  $this -> getTaskActions($kpi, $moduletype, $flavour, $userdata, $setting, $options, $actionevalautionObj);
               return $actions;                
             break;
          }
       }
    } 
    
    public function getActionsProgress($kpi, $userdata)
    {
       $mapping = serializeDecode($kpi['modulemapping']);  
       if(!empty($mapping))
       {
          switch($mapping['module'])
          {
               case "rsk":
                  $risktype = $mapping['module_rsk'];
                  $actions =  $this -> getRiskActionsProgress($kpi, $risktype, $mapping['subtype_'.$risktype]);           
                  return $actions;
               break;
               case "qry":
                  $risktype = $mapping['module_qry'];
                  $actions =  $this -> getQueryActionsProgess($kpi, $risktype, $mapping['subtype_'.$risktype]);           
                  return $actions;
               break;
               case "sla":
                  $risktype = $mapping['module_sla'];
                  $actions =  $this -> getSlaActionsProgress($kpi, $risktype, $mapping['subtype_'.$risktype]);           
                  return $actions;
               break;
               case "task":
                  $moduletype = $mapping['module'];
                  $actions =  $this -> getTaskActionsProgress($kpi, $moduletype, $mapping['module_'.$moduletype], $userdata);           
                  return $actions;
               break;   
               case "compl":
                  $actions =  $this -> getComplActionsProgress($kpi, $mapping['module_compl']);           
                  return $actions;
               break;                              
               default:
                  $moduletype = $mapping['module'];
                  $actions =  $this -> getTaskActionsProgress($kpi, $moduletype, $mapping['module_'.$moduletype], $userdata);           
                  return $actions;               
               break;
          }
       }
    }     
    
    public function getRiskActions($kpi, $risktype, $subtype, $setting, $options, $actionevalautionObj)
    {
       $sql = "SELECT A.*, RR.description FROM assist_".$_SESSION['cc']."_risk_actions A 
               INNER JOIN assist_".$_SESSION['cc']."_risk_risk_register RR ON RR.id = A.risk_id 
               WHERE A.action_owner = '".$kpi['owner']."'
               ";
       if(strtolower($risktype) !== "all")
       {
          $sql .= " AND RR.type = '".$risktype."' ";
       }
       if(strtolower($subtype) != "all")
       {
          $sql .= " AND RR.category = '".$subtype."' ";
       }  
       $get_evaluations = false;
       if(isset($options['section']) && $options['section'] != "new")
       {
          if(isset($setting['evaluate_on']) && $setting['evaluate_on'] == "action")
          {
             if(isset($_SESSION['evaluation_period']) && ($_SESSION['evaluation_period']))
             {
                $get_evaluations = true; 
                $sql .= " AND STR_TO_DATE(A.deadline, '%d-%M-%Y')  
                          BETWEEN '".date('Y-m-d', strtotime($_SESSION['evaluation_period']['start_date']))."' 
                          AND '".date('Y-m-d', strtotime($_SESSION['evaluation_period']['end_date']))."' ";
             }
          }
       }   
       $results          = DBConnect::getInstance() -> get($sql);
       $actions          = array();
       $actions['ids']   = array();
       $total_actions    = 0;
       $total_progress   = 0;
       $avg_progress     = 0; 
       if(!empty($results))
       {
          foreach($results as $index => $action)
          {
             if($get_evaluations)
             {
                if(is_object($actionevalautionObj))
                {
                   $actionevalautionObj -> get_action_evaluations($action['id'], $setting, $kpi['id']);             
                }
             }               
             $total_actions   += 1;
             $total_progress  += $action['progress']; 
             $actions['actions'][$action['id']]['id'] = $action['id'];  
             $actions['actions'][$action['id']]['kpiid'] = $kpi['id'];
             $actions['actions'][$action['id']]['name'] = $action['action'];
             $actions['actions'][$action['id']]['objective'] = $action['deliverable'];
             $actions['actions'][$action['id']]['outcome'] = $action['deliverable'];
             $actions['actions'][$action['id']]['measurement'] = $action['deliverable'];
             $actions['actions'][$action['id']]['owner'] = $action['action_owner'];
             $actions['actions'][$action['id']]['proof_of_evidence'] = $action['deliverable'];
             $actions['actions'][$action['id']]['baseline'] = $action['deliverable'];
             $actions['actions'][$action['id']]['targetunit'] = $action['deliverable'];
             $actions['actions'][$action['id']]['nationalkpa'] = "";
             $actions['actions'][$action['id']]['idpobjectives'] = "";
             $actions['actions'][$action['id']]['performance_standard'] = $action['deliverable'];
             $actions['actions'][$action['id']]['target_period'] = $action['timescale'];
             $actions['actions'][$action['id']]['weighting'] = "";
             $actions['actions'][$action['id']]['competency'] = "";
             $actions['actions'][$action['id']]['proficiency'] = "";
             $actions['actions'][$action['id']]['categories'] = $action['deliverable'];
             $actions['actions'][$action['id']]['deadline'] = $action['deadline'];
             $actions['actions'][$action['id']]['remindon'] = $action['remindon'];
             $actions['actions'][$action['id']]['comments'] = $action['deliverable'];             
             $actions['actions'][$action['id']]['status'] = $action['status'];    
             $actions['actions'][$action['id']]['actionstatus'] = $action['action_status'] + Action::IMPORTED; 
             $actions['actions'][$action['id']]['progress'] = $action['progress'];
             $actions['actions'][$action['id']]['actionon'] = $action['timescale'];                      
             $actions['actions'][$action['id']]['attachment'] = "";   
             $actions['actions'][$action['id']]['insertuser'] = $action['insertuser'];   
             $actions['actions'][$action['id']]['insertdate'] = $action['insertdate'];             
             $actions['ids'][$action['id']]                   = $action['id'];
          }
       }
       //$actions['action_evaluations']  = $this -> _get_total_next_evaluations();
       if($total_actions != 0)
       {
          $avg_progress  = round(($total_progress/$total_actions), 2);
       }
       $actions['total_progress'] = $total_progress;
       $actions['avg_progress']   = $avg_progress;
       $actions['total_actions']  = $total_actions;
       return $actions;
    }

    public function getRiskActionsProgress($kpi, $risktype, $subtype, $setting)
    {
       $sql = "SELECT SUM(A.progress) AS totalProgress, AVG(A.progress) AS avgProgress, COUNT(A.id) AS totalActions
               FROM assist_".$_SESSION['cc']."_risk_actions A 
               INNER JOIN assist_".$_SESSION['cc']."_risk_risk_register RR ON RR.id = A.risk_id 
               WHERE A.action_owner = '".$kpi['owner']."'
               ";
       if(strtolower($risktype) !== "all")
       {
          $sql .= " AND RR.type = '".$risktype."' ";
       }
       
       if(strtolower($subtype) != "all")
       {
          $sql .= " AND RR.category = '".$subtype."' ";
       }
       $get_evaluations = false;
       if(isset($setting['evaluate_on']) && $setting['evaluate_on'] == "action")
       {
          if(isset($_SESSION['evaluation_period']) && ($_SESSION['evaluation_period']))
          {
              $get_evaluations = true; 
              $sql .= " AND STR_TO_DATE(A.deadline, '%d-%M-%Y')  
                        BETWEEN '".date('Y-m-d', strtotime($_SESSION['evaluation_period']['start_date']))."' 
                        AND '".date('Y-m-d', strtotime($_SESSION['evaluation_period']['end_date']))."' 
                      ";   
          }
       }            
      $results = DBConnect::getInstance() -> getRow($sql);
      return $results;
    }

    public function getQueryActions($kpi, $querytype, $subtype, $setting, $options, $actionevalautionObj)
    {
       $sql = "SELECT A.*, RR.description FROM assist_".$_SESSION['cc']."_qry_actions A 
               INNER JOIN assist_".$_SESSION['cc']."_qry_query_register RR ON RR.id = A.risk_id 
               WHERE A.action_owner = '".$kpi['owner']."'
               ";
       if(strtolower($querytype) !== "all")
       {
          $sql .= " AND RR.type = '".$querytype."' ";
       }
       
       if(strtolower($subtype) != "all")
       {
          $sql .= " AND RR.category = '".$subtype."' ";
       }
       $get_evaluations = false;
       if(isset($options['section']) && ($options['section'] != "new"))
       {
         if(isset($setting['evaluate_on']) && $setting['evaluate_on'] == "action")
         {
            if(isset($_SESSION['evaluation_period']) && ($_SESSION['evaluation_period']))
            {
              $get_evaluations = true; 
              $sql .= " AND STR_TO_DATE(A.deadline, '%d-%M-%Y') 
                        BETWEEN '".date('Y-m-d', strtotime($_SESSION['evaluation_period']['start_date']))."' 
                        AND '".date('Y-m-d', strtotime($_SESSION['evaluation_period']['end_date']))."' ";      
            }
         }
       }          
       $results          = DBConnect::getInstance() -> get($sql);
       $actions          = array();
       $actions['ids']   = array();
       $total_actions    = 0;
       $total_progress   = 0;
       $avg_progress     = 0;        
       if(!empty($results))
       {
          foreach($results as $index => $action)
          {
             if($get_evaluations)
             {
                if(is_object($actionevalautionObj))
                {                
                   $actionevalautionObj -> get_action_evaluations($action['id'], $setting, $kpi['id']);             
                }
             }                  
             $total_actions   += 1;
             $total_progress  += $action['progress']; 
             $actions['actions'][$action['id']]['id'] = $action['id'];  
             $actions['actions'][$action['id']]['kpiid'] = $kpi['id'];
             $actions['actions'][$action['id']]['name'] = $action['action'];
             $actions['actions'][$action['id']]['objective'] = $action['deliverable'];
             $actions['actions'][$action['id']]['outcome'] = $action['deliverable'];
             $actions['actions'][$action['id']]['measurement'] = $action['deliverable'];
             $actions['actions'][$action['id']]['owner'] = $action['action_owner'];
             $actions['actions'][$action['id']]['proof_of_evidence'] = $action['deliverable'];
             $actions['actions'][$action['id']]['baseline'] = $action['deliverable'];
             $actions['actions'][$action['id']]['targetunit'] = $action['deliverable'];
             $actions['actions'][$action['id']]['nationalkpa'] = "";
             $actions['actions'][$action['id']]['idpobjectives'] = "";
             $actions['actions'][$action['id']]['performance_standard'] = $action['deliverable'];
             $actions['actions'][$action['id']]['target_period'] = $action['timescale'];
             $actions['actions'][$action['id']]['weighting'] = "";
             $actions['actions'][$action['id']]['competency'] = "";
             $actions['actions'][$action['id']]['proficiency'] = "";
             $actions['actions'][$action['id']]['categories'] = $action['deliverable'];
             $actions['actions'][$action['id']]['deadline'] = $action['deadline'];
             $actions['actions'][$action['id']]['remindon'] = $action['remindon'];
             $actions['actions'][$action['id']]['comments'] = $action['deliverable'];             
             $actions['actions'][$action['id']]['status'] = $action['status'];    
             $actions['actions'][$action['id']]['actionstatus'] = $action['action_status'] + Action::IMPORTED;
             $actions['actions'][$action['id']]['progress'] = $action['progress'];
             $actions['actions'][$action['id']]['actionon'] = $action['timescale'];                      
             $actions['actions'][$action['id']]['attachment'] = "";   
             $actions['actions'][$action['id']]['insertuser'] = $action['insertuser'];   
             $actions['actions'][$action['id']]['insertdate'] = $action['insertdate'];             
             $actions['ids'][$action['id']] = $action['id']; 
          }
       }
       //$actions['action_evaluations']  = $this -> _get_total_next_evaluations();
       if($total_actions != 0)
       {
          $avg_progress  = round(($total_progress/$total_actions), 2);
       }
       $actions['total_progress'] = $total_progress;
       $actions['avg_progress']   = $avg_progress;
       $actions['total_actions']  = $total_actions;       
       return $actions;
    }
  
    public function getQueryActionsProgess($kpi, $querytype, $subtype)
    {
       $sql = "SELECT SUM(A.progress) AS totalProgress, AVG(A.progress) AS avgProgress, COUNT(A.id) AS totalActions
               FROM assist_".$_SESSION['cc']."_qry_actions A 
               INNER JOIN assist_".$_SESSION['cc']."_qry_query_register RR ON RR.id = A.risk_id 
               WHERE A.action_owner = '".$kpi['owner']."'
               ";
       if(strtolower($querytype) !== "all")
       {
          $sql .= " AND RR.type = '".$querytype."' ";
       }
       if(strtolower($subtype) != "all")
       {
          $sql .= " AND RR.category = '".$subtype."' ";
       }
       if(isset($setting['evaluate_on']) && $setting['evaluate_on'] == "action")
       {
          if(isset($_SESSION['evaluation_period']) && ($_SESSION['evaluation_period']))
          {
              $sql .= " AND STR_TO_DATE(A.deadline, '%d-%M-%Y')  
                        BETWEEN '".date('Y-m-d', strtotime($_SESSION['evaluation_period']['start_date']))."' 
                        AND '".date('Y-m-d', strtotime($_SESSION['evaluation_period']['end_date']))."' 
                      ";      
          }
       }            
       $results = DBConnect::getInstance() -> getRow($sql);
       return $results;
    }  
  
    public function getSlaActions($kpi, $setting, $actionevalautionObj)
    {
       $sql = "SELECT A.*, SLA.name FROM assist_".$_SESSION['cc']."_qry_action A 
               INNER JOIN assist_".$_SESSION['cc']."_sla_contract SLA ON SLA.id = A.risk_id 
               WHERE A.owner = '".$kpi['owner']."'
               ";
       if(strtolower($risktype) !== "all")
       {
          $sql .= " AND SLA.type = '".$risktype."' ";
       }
       
       if(strtolower($subtype) != "all")
       {
          $sql .= " AND SLA.category = '".$subtype."' ";
       }
       $get_evaluations = false;
       if(isset($setting['evaluate_on']) && $setting['evaluate_on'] == "action")
       {
          if(isset($_SESSION['evaluation_period']) && ($_SESSION['evaluation_period']))
          {
              $get_evaluations = true; 
              $sql .= " AND A.deadline BETWEEN '".$_SESSION['evaluation_period']['start_date']."' 
                        AND '".$_SESSION['evaluation_period']['end_date']."' ";      
          }
       }            
       $results          = DBConnect::getInstance() -> get($sql);
       $actions          = array();
       $actions['ids']   = array();
       $total_actions    = 0;
       $total_progress   = 0;
       $avg_progress     = 0;        
       if(!empty($results))
       {
          foreach($results as $index => $action)
          {
             if($get_evaluations)
             {
                if(is_object($actionevalautionObj))
                {                
                   $actionevalautionObj -> get_action_evaluations($action['id'], $setting, $kpi['id']);             
                }
             }                  
             $total_actions   += 1;
             $total_progress  += $action['progress'];                
             $actions['actions'][$action['id']]['id'] = $action['id'];  
             $actions['actions'][$action['id']]['kpiid'] = $kpi['id'];
             $actions['actions'][$action['id']]['name'] = $action['action'];
             $actions['actions'][$action['id']]['objective'] = $action['measurable'];
             $actions['actions'][$action['id']]['outcome'] = $action['deliverable'];
             $actions['actions'][$action['id']]['owner'] = $action['owner'];
             $actions['actions'][$action['id']]['deadline'] = $action['deadline'];
             $actions['actions'][$action['id']]['progress'] = $action['progress'];
             $actions['actions'][$action['id']]['status'] = $action['status'];
             $actions['actions'][$action['id']]['actionstatus'] = $action['actionstatus'];
             $actions['ids'][$action['id']] = $action['id'];
          }
       }
       $actions['action_evaluations']  = $this -> _get_total_next_evaluations();
       if($total_actions != 0)
       {
          $avg_progress  = round(($total_progress/$total_actions), 2);
       }
       $actions['total_progress'] = $total_progress;
       $actions['avg_progress']   = $avg_progress;
       $actions['total_actions']  = $total_actions;          
       return $actions;
    }  
    
    public function getSlaActionsProgress($kpi)
    {
    
       $sql = "SELECT SUM(A.progress) AS totalProgress, AVG(A.progress) AS avgProgress, COUNT(A.id) AS totalActions
               FROM assist_".$_SESSION['cc']."_qry_action A 
               INNER JOIN assist_".$_SESSION['cc']."_sla_contract SLA ON SLA.id = A.risk_id 
               WHERE A.owner = '".$kpi['owner']."'
               ";
       if(strtolower($risktype) !== "all")
       {
          $sql .= " AND SLA.type = '".$risktype."' ";
       }
       
       if(strtolower($subtype) != "all")
       {
          $sql .= " AND SLA.category = '".$subtype."' ";
       }       
      $results = DBConnect::getInstance() -> getRow($sql);
      return $results;
    }    
    
    public function getComplActions($kpi, $legislation, $setting, $actionevalautionObj)
    {
      $sql  = "SELECT * FROM assist_".$_SESSION['cc']."_compl_action A
               INNER JOIN assist_".$_SESSION['cc']."_compl_deliverable D ON D.id = A.deliverable_id
               INNER JOIN assist_".$_SESSION['cc']."_compl_legislation L ON L.id = D.legislation
               WHERE A.owner = '".$kpi['owner']."' ";      
      if(strtolower($legislation) !== "all") 
      {
         $sql .= " AND L.id = '".$legislation."' ";      
      }
      $get_evaluations = false;
      if(isset($setting['evaluate_on']) && $setting['evaluate_on'] == "action")
      {
        if(isset($_SESSION['evaluation_period']) && ($_SESSION['evaluation_period']))
        {
          $get_evaluations = true;
          $sql .= " AND A.deadline BETWEEN '".$_SESSION['evaluation_period']['start_date']."' 
                    AND '".$_SESSION['evaluation_period']['end_date']."' ";      
        }
      }      
      $results           = DBConnect::getInstance() -> get($sql);
      $actions           = array();
      $actions['ids']    = array();
      $total_actions     = 0;
      $total_progress    = 0;
      $avg_progress      = 0;         
      if(!empty($results))
      {
         foreach($results as $index => $action)
         {
             if($get_evaluations)
             {
                if(is_object($actionevalautionObj))
                {                
                   $actionevalautionObj -> get_action_evaluations($action['id'], $setting, $kpi['id']);             
                }
             }             
             $total_actions   += 1;
             $total_progress  += $action['progress'];  
             $actions['actions'][$action['id']]['id'] = $action['id'];  
             $actions['actions'][$action['id']]['kpiid'] = $kpi['id'];
             $actions['actions'][$action['id']]['name'] = $action['action'];
             $actions['actions'][$action['id']]['objective'] = $action['action_deliverable'];
             $actions['actions'][$action['id']]['outcome'] = $action['action_deliverable'];
             $actions['actions'][$action['id']]['measurement'] = $action['action_deliverable'];
             $actions['actions'][$action['id']]['owner'] = $action['owner'];
             $actions['actions'][$action['id']]['proof_of_evidence'] = $action['action_deliverable'];
             $actions['actions'][$action['id']]['baseline'] = $action['action_deliverable'];
             $actions['actions'][$action['id']]['targetunit'] = $action['action_deliverable'];
             $actions['actions'][$action['id']]['nationalkpa'] = "";
             $actions['actions'][$action['id']]['idpobjectives'] = "";
             $actions['actions'][$action['id']]['performance_standard'] = $action['action_deliverable'];
             $actions['actions'][$action['id']]['target_period'] = "";
             $actions['actions'][$action['id']]['weighting'] = "";
             $actions['actions'][$action['id']]['competency'] = "";
             $actions['actions'][$action['id']]['proficiency'] = "";
             $actions['actions'][$action['id']]['categories'] = "";
             $actions['actions'][$action['id']]['deadline'] = $action['deadline'];
             $actions['actions'][$action['id']]['remindon'] = $action['reminder'];
             $actions['actions'][$action['id']]['comments'] = "";             
             $actions['actions'][$action['id']]['status'] = $action['status'];    
             $actions['actions'][$action['id']]['actionstatus'] = $action['actionstatus'] + Action::IMPORTED;
             $actions['actions'][$action['id']]['progress'] = $action['progress'];
             $actions['actions'][$action['id']]['actionon'] = $action['date_completed'];                      
             $actions['actions'][$action['id']]['attachment'] = "";   
             $actions['actions'][$action['id']]['insertuser'] = $action['insertuser'];   
             $actions['actions'][$action['id']]['insertdate'] = $action['insertdate'];             
             $actions['ids'][$action['id']] = $action['id']; 
         }
      }   
       if($total_actions != 0)
       {
          $avg_progress  = round(($total_progress/$total_actions), 2);
       }
       //$actions['action_evaluations']  = $this -> _get_total_next_evaluations();
       $actions['total_progress'] = $total_progress;
       $actions['avg_progress']   = $avg_progress;
       $actions['total_actions']  = $total_actions;          
      return $actions;
    }
    
    public function getComplActionsProgress($kpi, $legislation)
    {
      $sql  = "SELECT SUM(A.progress) AS totalProgress, AVG(A.progress) AS avgProgress, COUNT(A.id) AS totalActions
               FROM assist_".$_SESSION['cc']."_compl_action A
               INNER JOIN assist_".$_SESSION['cc']."_compl_deliverable D ON D.id = A.deliverable_id
               INNER JOIN assist_".$_SESSION['cc']."_compl_legislation L ON L.id = D.legislation
               WHERE A.owner = '".$kpi['owner']."' ";      
      if(strtolower($legislation) !== "all")
      {
         $sql .= " AND L.id = '".$legislation."' ";      
      }
      $results = DBConnect::getInstance() -> getRow($sql);
      return $results;
    }

    public function getTaskActions($kpi, $moduleRef, $topicRef, $userdata, $setting, $options, $actionevalautionObj)
    {
       $mindate   = date("d M Y", strtotime($userdata['start']));
       $maxdate   = date("d M Y", strtotime($userdata['end']));       
       $get_evaluations = false;
       if(isset($options['section']) && ($options['section'] != "new"))
       {
          if(isset($setting['evaluate_on']) && $setting['evaluate_on'] == "action")
          { 
             if(isset($_SESSION['evaluation_period']) && ($_SESSION['evaluation_period']))
             {
                 $get_evaluations = true;  
                 $mindate   = date("d M Y", strtotime($_SESSION['evaluation_period']['start_date']));
                 $maxdate   = date("d M Y", strtotime($_SESSION['evaluation_period']['end_date']));  
             }
          }
       }
       $topicRef            = ($topicRef == "all" ? "" : $topicRef);
       $actionObj           = new PM_ACTION($moduleRef, $kpi['owner'], $mindate, $maxdate, $topicRef);
       $results             = $actionObj -> getActions();
       $actions['ids']      = array();
       $actions['actions']  = array();
       $total_actions       = 0;
       $total_progress      = 0;
       $avg_progress        = 0;     
     
       if($results[0])
       {
         foreach($results[1] as $index => $action)
         {
             if($get_evaluations)
             {
               if(is_object($actionevalautionObj))
               {
                 $actionevalautionObj -> get_action_evaluations($action['id'], $setting, $kpi['id']);             
               }
             }             
             $total_actions   += 1;
             $total_progress  += $action['progress'];              
             $actions['actions'][$action['id']]['id']                   = $action['id'];  
             $actions['actions'][$action['id']]['kpiid']                = $kpi['id'];
             $actions['actions'][$action['id']]['name']                 = $action['name'];
             $actions['actions'][$action['id']]['objective']            = $action['name'];
             $actions['actions'][$action['id']]['outcome']              = $action['outcome'];
             $actions['actions'][$action['id']]['measurement']          = $action['outcome'];
             $actions['actions'][$action['id']]['owner']                = $action['owner'];
             $actions['actions'][$action['id']]['proof_of_evidence']    = "";
             $actions['actions'][$action['id']]['baseline']             = "";
             $actions['actions'][$action['id']]['targetunit']           = "";
             $actions['actions'][$action['id']]['nationalkpa']          = "";
             $actions['actions'][$action['id']]['idpobjectives']        = "";
             $actions['actions'][$action['id']]['performance_standard'] = "";
             $actions['actions'][$action['id']]['target_period']        = "";
             $actions['actions'][$action['id']]['weighting']            = "";
             $actions['actions'][$action['id']]['competency']           = "";
             $actions['actions'][$action['id']]['proficiency']          = "";
             $actions['actions'][$action['id']]['categories']           = "";
             $actions['actions'][$action['id']]['deadline']             = date("d-M-Y", strtotime($action['deadline']));
             $actions['actions'][$action['id']]['remindon']             = "";//date("d-M-Y", strtotime($action['remindon']));
             $actions['actions'][$action['id']]['comments']             = "";             
             $actions['actions'][$action['id']]['status']               = $action['status'];    
             $actions['actions'][$action['id']]['actionstatus']         = 1 + Action::IMPORTED;                      
             $actions['actions'][$action['id']]['progress']             = $action['progress'];
             $actions['actions'][$action['id']]['actionon']             = $action['actionon'];                      
             $actions['actions'][$action['id']]['attachment']           = "";   
             $actions['actions'][$action['id']]['insertuser']           = "";   
             $actions['actions'][$action['id']]['insertdate']           = "";             
             $actions['ids'][$action['id']]                             = $action['id']; 
         }
      }                      
       if($total_actions != 0)
       {
          $avg_progress  = round(($total_progress/$total_actions), 2);
       }
       //$actions['action_evaluations']  = $this -> _get_total_next_evaluations();
       $actions['total_progress'] = $total_progress;
       $actions['avg_progress']   = $avg_progress;
       $actions['total_actions']  = $total_actions;          
      return $actions;
    }    
    
    public function getTaskActionsProgress($kpi, $moduleRef, $topicRef, $userdata)
    {          
       $mindate   = date("d M Y", strtotime($userdata['start']));
       $maxdate   = date("d M Y", strtotime($userdata['end']));
       $topicRef  = ($topicRef == "all" ? "" : $topicRef);
       $actionObj = new PM_ACTION($moduleRef, $kpi['owner'], $mindate, $maxdate, $topicRef);
       $results   = $actionObj -> getActions();
       $actions['totalProgress']   = 0;
       $actions['avgProgress']     = 0;
       $actions['totalActions']    = 0;
       $totalProgress              = 0;
       $totalActions               = 0;
       $averageProgress            = 0;
       if(!$results[0])
       {
         foreach($results as $index => $action)
         {
            $totalProgress += $action['progress'];
            $totalActions  += 1;
         } 
      }   
      
      if($totalActions != 0)
      {
          $averageProgress = round(($totalProgress / $totalActions), 2);
      }
       $actions['totalProgress']   = $totalProgress;
       $actions['avgProgress']     = $averageProgress;
       $actions['totalActions']    = $totalActions;
      return $actions;
    }    
      
}
?>
