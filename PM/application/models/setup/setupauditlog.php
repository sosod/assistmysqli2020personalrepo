<?php
class SetupAuditLog extends AuditLog
{
      private $log_key;

     function __construct($log_key)
     {
          parent::__construct();
          $this -> log_key = $log_key; 
     }
     
     function notify($postData, $where, $tablename)
     {
          static::$table = $tablename;
          $result = array();
          $id     = ""; 
          if(isset($where['id']))
          {
            $result = self::findById($where['id']);
            $id     = $where['id'];
          } else {
            //$result = self::find 
          }
          $keys    = array('lastday' => 'last day', 'end' => 'end date', 'start' => 'start date', 'ratingto' => 'rating to');
          $changes = array();
          if(isset($result) && !empty($result))
          {
             foreach($result as $key => $value)
             {
                if(isset($postData[$key]))
                {
                  if($value != $postData[$key])
                  {
                     $keyStr = $this -> log_key." ".(isset($keys[$key]) ? $keys[$key] : $key);
                     //(!empty($id) ? $key." for ref #".$id : $key);
                     if($key == "status")
                     {    
                       $stataus_key           = self::statusChange($value, $postData[$key]);
                       $changes[$stataus_key] = $stataus_key." ".$this -> log_key." ".$id;
                     } else {
                       $changes[$keyStr]  = array("from" => $value ,  "to" => $postData[$key]);       
                     }
                  }
                }
             }
          }
          if(!empty($changes))
          {
             $_changes['ref_']          = "Ref ".$id;
             $changes['user']           = $_SESSION['tkn'];
             $_changes                  = array_merge($_changes, $changes);          
             $insertdata['insertuser'] = $_SESSION['tid'];
             $insertdata['changes']    = serializeEncode($_changes);
             static::$table = $tablename."_logs";
             if(!empty($insertdata))
             {
               parent::saveData($insertdata);
             }
          }
     }
     
     static function statusChange($from , $to)
     {
         $statuses = array("0" => "Deactivated", "1" => "Activated", "2" => "Deleted");
         $fromStr = "";
         $toStr   = ""; 
         if(isset($statuses[$from]))
         {
           $fromStr = $statuses[$from];
         }
         if(isset($statuses[$to]))
         {
           $toStr = $statuses[$to];
         }
         return $toStr; 
     } 
     
}
?>
