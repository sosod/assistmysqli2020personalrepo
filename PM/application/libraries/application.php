<?php
ini_set('error_reporting', 1);
ini_set('display_errors', 1);
class Application
{
    private static $instance; 
          
    private $request;
    
    private $response;
    
    /*
     Load required class using the autoload function
    */
    private function __construct()
    {
      spl_autoload_register("Application::autoload");
    } 
     
         
    static function autoload($classname)
    {
		try
		{
			$filename   = strtolower($classname).".php";
               if($filename == "pm_action.php")
               {
                  self::loadPMAction();
               } else {
			     self::loadFile(APPLICATION_PATH."/models", $filename);	
			     self::loadFile(APPLICATION_PATH."/controllers", $filename);
			     self::loadFile(APPLICATION_PATH."/libraries", $filename);	
			     self::loadFile("../library/dbconnect", $filename);	               
               }		     
		} catch(Exception $e) {
			echo "Exception occured ".$e->getMessage();
		}
	}
	
	public static function loadPMAction()
	{
	  if(file_exists('../ACTION/class/pm_action.php'))
	  {
	     require_once '../library/class/assist_dbconn.php';
	     require_once '../library/class/assist_db.php';
          require_once '../ACTION/class/pm_action.php';        
	  } 
	}
	
	//look for the file specified in the class directory and its sub directories
	public static function loadFile($dir, $filename)
	{
		$directories = array();
		if(is_dir($dir))
		{
		   $directories = array_diff(scandir($dir), array(".", ".."));		
		} else {
	        self::loadFile("../".$dir, $filename);
		}				
		foreach($directories as $index => $val)
		{
			if(is_dir($dir."/".$val))
			{
			  self::loadFile($dir."/".$val, $filename);			
			} else {
			  if($val == $filename)
			  {   		
			     //echo "require file .....".$filename." <br /><br /><br />\r\n\n\n";     
                 require_once($dir."/".$filename);
			  }			
			} 
		}
	}    
    
    //do no clone this class
    private function __clone(){}
    
    //create a singleton instance of this class
    public static function getInstance()
    {
      //if the is not yet available
      if(!self::$instance)
      {
        self::$instance = new Application();
      }
      return self::$instance;
    }
    
    //execute the application controller and actions for this request
    function execute($request)
    {

       $action      = $request -> getAction();
       $controller  = $request -> getController();
       $params      = $request -> getParams(); 
       $responseObj = $request->getResponseType();     
       $response   = array();	
       try{
          $reflectionObj = new ReflectionMethod($controller, $action);
          $response      = $reflectionObj->invokeArgs(new $controller(), $params);				
       } catch(ReflectionException $ref) {
          $response = array("text" => "An reflection exception occured: ".$ref -> getMessage(), "error" => true); 
       } catch(Exception $e) {
          $response = array("text" => "An exception  ".$e -> getMessage(), "error" => true); 
       }
       $responseObj -> displayResponse( $response ) ;
    }

     
}
?>
