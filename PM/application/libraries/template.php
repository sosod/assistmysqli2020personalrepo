<?php
class Template
{
     protected static $instance;
     
     //is there a header to the layout
     var $layout = true;
     
     //template variables 
     protected $vars  = array();
     
     private $template;
     
     public function __get($key)
     {
          return $this->vars[$key];     
     }     
     
     public function __set($key, $value)
     {
          $this->vars[$key] = $value;     
     }
     
     function view($viewnmae)
     {
          try
          {
               //die("Coming this side now . . . ");               
               $file = APPLICATION_VIEW."/".strtolower($viewnmae).".php";
               if(!is_file($file))
               {
                  throw new Exception("View file ".$file." does not exist");
               }
               extract($this->vars);            
               if($this->layout)
               {
                  include_once APPLICATION_VIEW."/header.php";      
               }
               include_once $file;                   
          } catch(Exception $e) {
               echo $e->getMessage();
               exit();
          }
     }
     
     public function getTemplate()
     {
         return $this->template;
     }
     
     public function setTemplate($layoutname, $vars)
     {
          try
          {
               $file = APPLICATION_VIEW."/template/".strtolower($layoutname).".php";
               //echo "Template file name is ".strtolower($layoutname)."<br /><br />";
               if(!file($file))
               {
                 throw new Exception("View file ".$file." does not exist");
               }
               extract($vars);
               ob_start();
               include $file;
               return ob_get_clean();
          } catch(Exception $e) {
               echo $e->getMessage();
               exit();
          }          
     }  
     
     public static function getInstance()
     {
          if(!self::$instance)
          {
               self::$instance = new Template();;
          }
         return self::$instance;
     }   
               
}

?>
