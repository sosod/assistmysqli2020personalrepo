<?php
abstract class Model
{
     //holds the table name of the models
     protected static $table;
     //db object
     protected $db;
     
     protected $fieldValues = array(); 
     //holds array of update observers, to check update on specified model fields
     protected $updateObservers = array();

     
     public function __construct()
     {
         $this -> db = DBConnect::getInstance();  
     }
     
     //get tht tablename for this model
     public static function getTable()
     {
        return static::$table;
     }
     
     //set the table for this model
     public static function setTable($tablename)
     {
        static::$table = $tablename;
     }
     
     function save($data)
     {
       if(!empty($data))
       {
         $this -> db -> insert(static::$table, $data);   
       }
       return $this -> db -> insertedId();
     }
     
     function update_where($data, $where)
     {
	   //handle the logging of changes made
	    $res = 0;
        $res = $this -> notifyChanges($data, $where);
  	    $res += $this -> db -> updateData(static::$table, $data, $where);	
  	    return $res;
     }
     
     //insert data to the specified table
     public static function saveData($data)
     {
       if(!empty($data))
       {
         DBConnect::getInstance()->insert(static::$table, $data);   
       }
       return DBConnect::getInstance()->insertedId();
     }
     //update data
     public static function updateWhere($data, $where, $obj = null)
     {
       $res = 0;
       self::setTable(static::$table); 
       if(is_object($obj) && ($obj instanceof AuditLog))
       {
         $res = $obj -> notify($data, $where, static::$table);
       }
       //self::notifyChanges($data, $where);
       $res += DBConnect::getInstance()->updateData(static::$table, $data, $where);
       return $res;
     }
     
     public function attach(AuditLog $obj)
     {  
        $this -> updateObservers[] = $obj;
     }

  	private function notifyChanges($data, $where)
  	{
  	   $res = 0;
  	   if(isset($this -> updateObservers) && !empty($this -> updateObservers))
  	   {
             foreach($this -> updateObservers as $index => $obj)
             {
                $res += $obj -> notify($data, $where, static::$table);
             }	     
  	   }
  	   return $res;
  	}     
     
     //find all results from the the specified dataabase table;
     public static function findAll($options = "")
     {
       $results = DBConnect::getInstance()->get("SELECT * FROM #_".static::$table." WHERE 1 $options");      
       return $results;
     }
     
     //find the list of results with id as the key
     public static function findList($options = "")
     {
       $results = DBConnect::getInstance()->get("SELECT * FROM #_".static::$table." WHERE 1 $options");       
       $resultList = array();
       foreach($results as $index => $result)
       {
          $resultList[$result['id']] = $result['name'];
       }
       return $resultList;
     }
     
     //fetch a single row
     public static function find($options)
     {
       $result = DBConnect::getInstance()->getRow("SELECT * FROM #_".static::$table." WHERE 1 $options");       
       return (array)$result;
     }     
     
     //fetch results by their ids
     public static function findById($id)
     {
       $result = DBConnect::getInstance()->getRow("SELECT * FROM #_".static::$table." WHERE id = '".$id."'");       
       return $result;
     }
       
     //find data using an AND   
      public static function findAND($options = array())
      {
          $sql     = "";
          $sql     = self::_getWhere($options, "AND");
          $results = self::find($sql);
          return $results;
      }  
       
     //find data using an OR   
      public static function findOR($options = array())
      {
          $sql     = "";
          $sql     = self::_getWhere($options, "OR");
          $results = self::find($sql);
          return $results;
      }  
      
     //find query
     public static function findQuery($fields = array(), $where = array(), $groupBy = array(), $orderBy = array(), $limits = array())
     {
          $results = DBConnect::getInstance()->get("SELECT ".implode("," , $fields)."  
                                                    FROM ".static::$table." 
                                                    WHERE 1 ".$where."
                                                    GROUP BY ".implode(",", $groupBy)."
                                                    ORDER BY ".implode(",", $orderBy)."
                                                    LIMIT ".$limts['start'].",".$limits['limit']."
                                                   ");
         $results;                                           
     }  
      
     //get the total 
     public static function findCount($optionSql ="")
     {
         $results = DBConnect::getInstance()->getRow("SELECT COUNT(*) FROM #_".static::$table." WHERE 1 $optionSql");
         return $results;
     }      
      
     //fetch data using a custom query     
     public static function query($query)
     {
         $results = DBConnect::getInstance()->get($query);
         return $results;
     }    
     
     //debug  a custom query     
     public static function debug($query)
     {
         $results = DBConnect::getInstance()->debug($query);
         return $results;
     }    

     //fetch data using a custom query     
     public static function queryRow($query)
     {
         $results = DBConnect::getInstance()->getRow($query);
         return $results;
     }          
          
     //call undefined methods
     public static function __callStatic($methodname, $args)
     {
        if(preg_match("/^findAllBy/", $methodname))
        {
          $field = preg_replace("/^findAllBy(\w*)$/", '${1}', $methodname);
          $result = DBConnect::getInstance()->get("SELECT * FROM #_".static::$table." WHERE ".strtolower($field)." = '".$args[0]."'");               
        } else {
          $field = preg_replace("/^findBy(\w*)$/", '${1}', $methodname);
          $result = DBConnect::getInstance()->getRow("SELECT * FROM #_".static::$table." WHERE ".strtolower($field)." = '".$args[0]."'");              
        }
        return $result;        
     }

     public static function _getWhere($options, $condition)
     {
          $sql = "";
          foreach($options as $key => $val)
          {
             if(is_array($val))
             {
               $sql .= self::_getWhere($val);
             } else {
               $sql .= " ".$condition." ".$key." = ".$val." ";
             }
          }
        return $sql;  
     }
     
     public static function createANDSqlOptions($options=array())
     {
    		$sqlOption = "";
    		if( !empty($options) && is_array($options))
    		{
    			foreach($options as $field => $value)
    			{
    			    if(is_array($value))
    			    {
    			        if(!empty($value))
    			        {
    			            $sqlOption .= self::createANDSqlOptions($value);
    			        }			    
    			    } else {
    				    if( $field == "status" || strstr($field, "status"))
    				    {
    					    if(!empty($value))
    					    {
    					      if($value == 2)
    					      {
    					          $sqlOption .= " AND ".$field." & ".$value." <> ".$value." ";
    					      } else {
    						    $sqlOption .= " AND ".$field." & ".$value." = ".$value." ";
    						 }
    					    }	
    				    } else {
    				        if(!empty($value))
    				        {				    
    					      $sqlOption .= " AND ".$field." = '".$value."' ";
    					   }
    				    }
    			   }
    			}
    		} 	
    		return rtrim($sqlOption, " AND ");	
     }


     
}
?>
