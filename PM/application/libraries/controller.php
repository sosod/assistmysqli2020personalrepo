<?php
abstract class Controller
{
     //holds the instance of the called model class
     protected $Model;
      
     //holds the instance of the template object used to load views 
     protected $template;
     
     //stores the layout to be used for a view
     static $layout;
     
     //stores the variables passed to the view
     protected $vars;
     
     private $session;
     
     protected $user;
     
     public function __construct()
     {
		$controllername = get_class($this);
		$classname      = substr( $controllername, 0, -10);
		//echo "Coming here in the controller class for ".$classname." \r\n\n";
		if(class_exists($classname))
		{
		   //die("Class loaded is  . . . ".$classname);
		   $this -> Model =  new $classname();		
		} 
		//echo "Template is  ".var_dump($this -> template)." \r\n\n";
        $this -> template = Template::getInstance();  
        //echo "Template is  ".var_dump($this -> template)." \r\n\n";
     }
     
     //set the template to view
     function setTemplate($value)
     {
        $this -> template = $value;
     }
     
	function sqlOption($options)
	{
		$sqlOption = "";
		if( !empty($options) && is_array($options))
		{
			foreach($options as $field => $value)
			{
			    if(is_array($value))
			    {
			        if(!empty($value))
			        {
			            $sqlOption .= " AND ".$this->sqlOption($value);
			        }			    
			    } else {
				    if( $field == "status" || strstr($field, "status"))
				    {
					    if(!empty($value))
					    {
					      if($value == 2)
					      {
					          $sqlOption .= " AND ".$field." & ".$value." <> ".$value." ";
					      } else {
						    $sqlOption .= " AND ".$field." & ".$value." = ".$value." ";
						 }
					    }	
				    } else {
				        if(!empty($value))
				        {				    
					      $sqlOption .= " AND ".$field." = '".$value."' ";
					   }
				    }
			   }
			}
		} 	
		return rtrim($sqlOption, " AND ");	
	}     
}
?>
