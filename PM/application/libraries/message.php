<?php
class Message
{
     
    public static function update($state, $context = "")
    {
		$response = array();
		if($state >= 1 )
		{
			if( $context == "")
			{
				$msg = "Updated successfuly  . . . ";				
			} else {
				$msg = ucfirst(($context))." has been successfully updated";
			}
			$response = array("text" => $msg, "error" => false );
		} else if( $state == 0){
			$response = array("text" => "There were no changes updated", "error" => false, "updated" => true);
		}  else {
			$response = array("text" => "Error updating . . please try again", "error" => true);
		}
		return $response;
    }

    
    public static function save($state, $context = "")
    {
       $response = array();
        if( $state > 0)
        {
	        if( $context == "")
	        {
		        $msg = "Saved successfuly  . . . ";				
	        } else {
		        $msg = ucfirst(($context)." has been successfully saved\n");
	        }
	        $response = array("text" => $msg, "error" => false , "id" => $state);
        } else {
	        $response = array("text" => "Error saving . . please try again", "error" => true);
        }
        return $response;   
    }
    
    public static function activate($state, $context = "")
    {
		$response = array();
		if( $state == 1)
		{
			if( $context == "")
			{
				$msg = "Activated successfully  . . . ";				
			} else {
				$msg = ucfirst(($context))." has been successfully activated";
			}
			$response = array("text" => $msg, "error" => false );
		}  else {
			$response = array("text" => "Error activating . . please try again", "error" => true);
		}
		return $response;    
    }
    
    public static function deactivated($state, $context = "")
    {
		$response = array();
		if( $state == 1)
		{
			if( $context == "")
			{
				$msg = "Deactivated successfully  . . . ";				
			} else {
				$msg = ucfirst(($context))." has been successfully deactivated";
			}
			$response = array("text" => $msg, "error" => false );
		}  else {
			$response = array("text" => "Error deactivating . . please try again", "error" => true);
		}
		return $response;
    }
    
    public static function delete($state, $context = "")
    {
		$response = array();
		if( $state == 1)
		{
			if( $context == "")
			{
				$msg = "Deleted successfully  . . . ";				
			} else {
				$msg = ucfirst(($context))." has been successfully deleted";
			}
			$response = array("text" => $msg, "error" => false );
		}  else {
			$response = array("text" => "Error deleting . . please try again", "error" => true);
		}
		return $response;
    }
     
} 
?>
