<?php
class Request
{
     private $controller;
     
     private $action;
     
     private $params = array();
     
     function __construct()
     {
          $this->controller = !empty($_REQUEST['controller']) ? $_REQUEST['controller'] : DEFAULT_CONTROLLER;
          $this->action =  !empty($_REQUEST['action']) ? $_REQUEST['action'] : DEFAULT_ACTION;	
     }
     
     function getAction()
     {
         return $this->action;
     }
     
     function getController()
     {
         return ucfirst($this->controller)."Controller";
     }
     
     function getParams()
     {
          unset($_REQUEST['action']);
          unset($_REQUEST['controller']);
          $this->params = array();
          foreach($_REQUEST as $key => $value)
          {
             $this->params['data'][$key] = $value;
          }
         return $this->params;
     }
     
	function getResponseType()
	{
		$response = explode(',',$_SERVER['HTTP_ACCEPT']);
		$responseMethod = "";
		if(preg_match('/[*]/', $response[0], $match))
		{	
			$responseMethod = "";
		} else {
			if( isset($response[0]) && !empty($response[0]))
			{
				$responseMethod = substr($response[0],strpos($response[0],'/')+1);
			}
		}
		$responseClass = "";
		if(self::isAjax() && $responseMethod == "")
		{
			$responseMethod = "json";	
		}				
		if(!empty($responseMethod))
		{
			if($responseMethod == "plain")
			{
				$responseClass = 'HtmlResponse';			
			} else {
				$responseClass = ucwords($responseMethod).'Response';			
			}
			
		} else {
			$responseClass = 'HtmlResponse';		
		}
		return new $responseClass();
	}     
	
	public static function isAjax()
	{	
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])  && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
	 	{
	   		return TRUE;
	 	} else {
	    	// was a non-ajax request - do a normal redirect
	    	return FALSE;
	 	}				
	}	

}
?>
