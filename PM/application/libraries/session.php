<?php
class Session
{
     //saves the session instance
     protected static $instance;
     
     //module location
     protected static $modlocation;
     
     //module reference 
     private static $modref;
     
     function __construct()
     {
          self::$modlocation = $_SESSION['modlocation'];
          self::$modref      = $_SESSION['modref'];
     }
     
     function __set($key, $value)
     {
        $_SESSION[self::$modlocation][self::$modref][$key] = $value; 
     }  
     
     function __get($key)
     {
          return $_SESSION[self::$modlocation][self::$modref][$key];
     }   
     
     public static function getInstance()
     {
          if(!self::$instance)
          {
             self::$instance = new Session();
          }
         return self::$instance;
     }
     
     
}
?>
