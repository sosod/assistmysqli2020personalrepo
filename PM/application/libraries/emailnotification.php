<?php
abstract class EmailNotification
{

	protected $message = "";
	
	protected $to = "";
	
	protected $from  =  "";
	
	protected $subject = "";
	

	function send()
	{
        $headers = "From: <no-reply@ignite4u.co.za>\r\n";
        $headers .= "MIME-Version: 1.0\r\n"; 
        $headers .= "Content-type: text/plain; charset=utf-8\r\n";
        $headers .="Content-Transfer-Encoding: 8bit";	    
	
		if(@mail($this -> to, $this -> subject, $this -> message, $headers))
		{
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function emailSignature()
	{
	   $signature = "";
       $signature .= "Kind Regards,\r\n\n";
	   $signature .= "Ignite Assist\r\n\n";	
	   return $signature;
	}
	
	function notify()
	{
		return $this -> send();
	}

}
?>
