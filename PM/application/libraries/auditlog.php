<?php
abstract class AuditLog extends Model
{

     function __construct()
     {
          parent::__construct();
     }

     abstract function notify($postData, $where, $tablename);
}
?>
