<?php
class Sorter
{
                    
     public static function sortHeaders($data, $options = array())
     {
        $section = (isset($options['section']) ? $options['section'] : "");
        $type    = (isset($options['type']) ? $options['type'] : "");    
        $componetCats = (isset($options['component']) ? $options['component'] : array());
        $headers     = array(); 
        if(isset($options['headers']) && !empty($options['headers']))
        {
           $headerNames = Naming::getHeaderList();
           foreach($options['headers'] as $hIndex => $hval)
           {
               $headers[$hval] = (isset($headerNames[$hval]) ? $headerNames[$hval] : "");   
           }
        } else {
          $headerNames = Naming::getHeaderList($section);
          $headers = $headerNames;
        }   
        
        $headersData= array();
        $datalist = array();

        $weighting = Weighting::findList();
   
        $proficiency    = Proficiency::findList();        
    
        $nationalkpa = Nationalkpa::findList();
 
        $nationalobjectives = Nationalobjectives::findList();        

        $competency = Competency::findList(); 
        $userObj     = new User();
        
        $classname = ucfirst($type."Status");
        $statusObj = new $classname();
        $statuses  = $statusObj -> findList();
        $userdata  = (isset($options['user']) ? $options['user'] : array());
        $usersList   = $userObj -> getUserList();
        $datalist['headers'] = array();
        $datalist['data'] = array();
        $datalist['statuses'] = array();
        //sort data according to the headers as setup in the column setup , so we view data with columns specified     
        foreach($headers as $headerIndex => $headerVal)
        {
           foreach($data as $dIndex => $value)
           {   
                 //check if the current $value is supposed to be displayed
                 $referenceDisplayable = TRUE;
                 if(isset($userdata) && !empty($userdata))
                 {               
                   $referenceDisplayable = self::isCurrentReferenceDisplayable($componetCats, $value, $userdata['categoryid'], $section, $type);
                 }

                 if($type == "kpa")
                 {
                   $datalist['statuses'][$value['id']]  = $value['kpastatus'];
                 }
                 if($type == "kpi")
                 {
                   $datalist['statuses'][$value['id']]  = $value['kpistatus'];
                 }
                 if($type == "action")
                 {
                   $datalist['statuses'][$value['id']]  = $value['actionstatus'];
                 }                 
                 
                 //if this reference is displayable
                 if($referenceDisplayable)
                 {
                    //if the current header is in the data list 
                    if($headerIndex == "progress")
                    {    
                      $datalist['headers'][$headerIndex]  = $headerVal;
                      $progress = self::calculateProgress($type, $value['id'], $datalist['statuses'][$value['id']]);   
                      $datalist['data'][$value['id']][$headerIndex] = (isset($value[$headerIndex]) ? $value[$headerIndex] : $progress)."%";
                    } else if($headerIndex == "status") {
                         $statusStr = "";
                         if(isset($statuses[$value[$headerIndex]]))
                         {
                            $statusStr = $statuses[$value[$headerIndex]];
                         }
                         $datalist['data'][$value['id']][$headerIndex]  = $statusStr; 
                         $datalist['headers'][$headerIndex]  = "Status"; 
                    } else if(isset($value[$headerIndex])) {
                       $datalist['headers'][$headerIndex]  = $headerVal; 
                       if($headerIndex == "weighting")
                       { 
                         $wStr = "";
                         if(isset($weighting[$value[$headerIndex]]))
                         {
                           $wStr = $weighting[$value[$headerIndex]];
                         }
                         $datalist['data'][$value['id']][$headerIndex]  = "<em>".$wStr."</em>";                         
                       } else if($headerIndex == "proficiency") {
                         $proficiencyStr = "";
                         if(isset($proficiency[$value[$headerIndex]]))
                         {
                           $proficiencyStr = $proficiency[$value[$headerIndex]];
                         }
                         $datalist['data'][$value['id']][$headerIndex] = "<em>".$proficiencyStr."</em>";
                       } else if($headerIndex == "competency") {
                         $competencyStr = "";
                         if(isset($competency[$value[$headerIndex]]))
                         {
                           $competencyStr = $competency[$value[$headerIndex]];
                         }
                         $datalist['data'][$value['id']][$headerIndex] = $competencyStr;
                       } else if($headerIndex == "nationalkpa") {
                         $nationalKpaStr = "";
                         if(isset($nationalkpa[$value[$headerIndex]]))
                         {
                           $nationalKpaStr = $nationalkpa[$value[$headerIndex]];
                         }
                         $datalist['data'][$value['id']][$headerIndex] = $nationalKpaStr;
                       } else if($headerIndex == "idpobjectives") {
                         $nationalobjectivesStr = "";
                         if(isset($nationalobjectives[$value[$headerIndex]]))
                         {
                           $nationalobjectivesStr = $nationalobjectives[$value[$headerIndex]];
                         }
                         $datalist['data'][$value['id']][$headerIndex] = $nationalobjectivesStr;
                       } else if($headerIndex == "owner") {
                         $userStr = "";
                         if(isset($usersList[$value[$headerIndex]]))
                         {
                            $userStr = $usersList[$value[$headerIndex]];
                         }
                         $datalist['data'][$value['id']][$headerIndex] = $userStr;
                       } else{
                         $datalist['data'][$value['id']][$headerIndex] = $value[$headerIndex];
                       }  
                    }
                 }              
           }
         }
          return array("data"    => $datalist['data'],
                       "columns" => $datalist['headers'],
                       "cols"    => count($datalist['headers']),
                       "total"   => count($datalist),
                       "statuses" => $datalist['statuses']
                       );
     }     
     
     public static function isCurrentReferenceDisplayable($componentcats, $value, $categoryId, $section, $type)
     {
       $componentcats = array(); 
       if(!empty($componentcats))
       {
          //if this current template reference has been configured
          $ref     = $value[$type."ref"];
          if(isset($componentcats[$ref]))
          {
            //if this reference for this category id is set to 0, then its not supposed to be displayed 
            if(($componentcats[$ref][$categoryId] == 0) && ($section !== "setup"))
            {
               return FALSE;
            } 
          }
       }    
        return TRUE;
     } 
     
     public static function calculateProgress($type, $id, $status)
     {
        $progress = 0;
        switch(strtolower($type))
        {
           case "kpa":
             $progress = Kpa::calculateKpaProgress($id, $status);
           break;
           case "kpi":
              $progress = Kpi::calculateKpiProgress($id, $status);
           break;
           case "action":
            return; 
           break;
        }
        return $progress;
     }
     
}
?>

