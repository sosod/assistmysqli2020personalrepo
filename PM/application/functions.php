<?php
/*function setUserSettings()
{
     $userSetting = UserSetting::fetchUserSetting();
     $userJoblevel = User::userJoblevel();
     if(!empty($userSetting))
     {    
          $modref = $_SESSION['modref'];
          $modlocation = $_SESSION['modlocation'];
          $_SESSION[$_SESSION['modlocation']][$_SESSION['modref']]['usersetting'] = $userSetting;
     }
     if(!empty($userJoblevel))
     {
          $modref = $_SESSION['modref'];
          $modlocation = $_SESSION['modlocation'];
          $_SESSION['userjoblevel'] = $userJoblevel;
     }     
}
*/
function toUserId($id)
{
   return str_pad($id, 4, '0', STR_PAD_LEFT);
}

function serializeEncode($data)
{
   return base64_encode(serialize($data));
}

function serializeDecode($data)
{
   return @unserialize(base64_decode($data));
}

function debug($data)
{
   if(is_array($data) || is_object($data))
   {
      print "<pre>";
          print_r($data);
      print "</pre>";
   } else {
      print "<pre>";
          var_dump($data);
      print "</pre>";     
   }
}

function get_url($controller_name = null, $action_name = null, $parentref, $params = null, $pagetitle = "")
{
     $querystring = preg_split('/[&]/',$_SERVER['QUERY_STRING'], -1 );
     $parent_id = "";
     $qStrArr = array();
     foreach($querystring as $qIndex => $string)
     {
          $qval = explode("=", $string);
          if(isset($qval[0]))
          {
            $qStrArr[$qval[0]] = $qval[1];
          }
     }
     $qStrArr['pagetitle']  = $pagetitle;
     //debug($qStrArr);
    //debug($_SESSION['pmcrumbs']);
     
     if(!empty($querystring))
     {
          if(isset($qStrArr['parent']))
          {
            if(isset($_SESSION['pmcrumbs'][$qStrArr['parent']]))
            {
                 //if at some point in the loop the current query string is the same , unset the crumb and add the current one only
                $currentInCrumbs = FALSE;
                foreach($_SESSION['pmcrumbs'][$qStrArr['parent']] as $bIndex => $crumbArr)
                {
                    //check if there is difference in the current query string and previous
                    $diff = array_diff($crumbArr, $qStrArr);
                    //if its the same parent but on different parameters , load the new parameters array to this parent
                    if($crumbArr['pagetitle'] == $qStrArr['pagetitle'])
                    {
                         $currentInCrumbs = TRUE;
                    } else {
                         if(!empty($diff))
                         {    
                            $_SESSION['pmcrumbs'][$qStrArr['parent']][] = $qStrArr;
                         } else {
                            $currentInCrumbs = TRUE;
                         }       
                    }                            
                }    
                if($currentInCrumbs)
                {
                    // set the parent id as new one , remove the current 
                    unset($_SESSION['pmcrumbs']);
                    //load the new parent id
                    $_SESSION['pmcrumbs'][$qStrArr['parent']][] = $qStrArr;                       
                }
            } else {
               // if the parent id is a new one , remove the current 
               unset($_SESSION['pmcrumbs']);
               //load the new parent id
               $_SESSION['pmcrumbs'][$qStrArr['parent']][] = $qStrArr;   
            }         
          }
     }
     $crumbString = "";
     foreach($_SESSION['pmcrumbs'][$qStrArr['parent']] as $parentId => $crumbs)
     {
        $keyval = array();
        foreach($crumbs as $key => $value)
        {
          if($key != "pagetitle")
          {
            $keyval[] = $key."=".$value;
          }
        }
        $crumbString .= "<a href='main.php?".implode('&amp;', $keyval)."'>".$crumbs['pagetitle']."</a> >> ";
     }
     //echo $crumbString."";
     echo "<h4>".rtrim($crumbString, " >> ")."</h4>";
     //debug($querystring);
     //unset($_SESSION['breadcrumb']);
     /*echo "Parent refence is ".$parentref."<br >";
     
     $controller = trim($controller_name) ? $controller_name : DEFAULT_CONTROLLER;
     $action = trim($action_name) ? $action_name : DEFAULT_ACTION;
     
     if(!is_array($params) && !is_null($params))
     {
          $params = array("id" => $params);
     }
     $url_params = array("controller=".$controller, "action=".$action, "parent=".$parentref);
     if(is_array($params))
     {
        foreach($params as $key => $value)
        {
          if(is_bool($value))
          {
             $url_params[] .= $key."=".$value;  
          } else {
             $url_params[] .= $key."=".urlencode($value);
          }  
        }
     }
     */
     //$_SESSION['breadcrumb'][] = $url_params;
     //$_SESSION['breadcrumb'][] = $url_params;
     //return "main.php?".implode("&amp;", $querystring);
}

function request_controller()
{
     $controller = "";
     if(isset($_REQUEST['controller']) && !empty($_REQUEST['controller']))
     {
          $controller = trim($_REQUEST['controller']);
     } else {
          $controller = DEFAULT_CONTROLLER;
     }
     return $controller;
}

function request_action()
{
     $action = "";
     if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
     {
          $action = trim($_REQUEST['action']);
     } else {
          $action = DEFAULT_ACTION;
     }
     return $action;     
}

function request_parentref()
{
     $parent = "";
     
     if(isset($_REQUEST['parent']) && !empty($_REQUEST['parent']))
     {
        $parent = trim($_REQUEST['parent']);
     } else {
        $parent = 2;  
     }
     return $parent;     
}

function request_folder()
{
   $folder = "";
   if(isset($_REQUEST['folder']) && !empty($_REQUEST['folder']))
   {
     $folder = $_REQUEST['folder'];
   } else {
     $folder = "manage";
   }
   return $folder;
}


function loadFile($dir, $filename)
{
	$directories = array();
	if(is_dir($dir))
	{
		$directories = array_diff(scandir($dir), array(".", ".."));		
	} else {
		loadFile("../".$dir, $filename);
	}
			
	foreach($directories as $index => $val)
	{
		if(is_dir($dir."/".$val))
		{
		 loadFile($dir."/".$val, $filename);			
		} else {
		  if($val == $filename)
		  {   
               require_once($dir."/".$filename);
		  }			
		} 
	}
}  
?>
