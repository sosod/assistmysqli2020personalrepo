<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");

include("inc_ta.php");

$result = "<p>&nbsp;</p>";
//GET VARIABLES PASSED BY FORM
$atkid = $_POST['atkid'];
$access = $_POST['access'];
//IF THE VARIABLES WERE PASSED CORRECTLY THEN PERFORM UPDATE
if($atkid != "X" && $access != "X" && strlen($access) > 0)
{
    //CHECK IF NEW USER EXISTS IN ACCESS TABLE ALREADY
    $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_access WHERE tkid = '".$atkid."' AND yn = 'Y'";
    include("inc_db_con.php");
    $trow = mysql_num_rows($rs);
    if($trow > 0)
    {
        //IF THE USER WAS FOUND IN THE ACCESS TABLE - ERROR
        $err = "Y";
        $result = "<p>Error - the requested action could not be completed as the user already exists in Task Assist.</p>";
        //SET TRANSACTION LOG VALUES
        $row = mysql_fetch_array($rs);
        $tsql = implode("|",$row);
        $trans = "Error - User ".$atkid." could not be added to TA as user already had access.";
    }
    else
    {
        //ELSE SET VARIABLE TO CONTINUE
        $err = "N";
        $result = "<p>Success!  User added to Task Assist.</p>";
    }
    mysql_close();
    //IF RECORD WAS NOT FOUND THEN PERFORM UPDATE
    if($err == "N")
    {
        $sql = "INSERT INTO assist_".$cmpcode."_ta_list_access SET tkid = '".$atkid."', view = ".$access.", act = ".$access.", yn = 'Y'";
        include("inc_db_con.php");
        //SET TRANSACTION LOG VALUES FOR UPDATE
        $tsql = $sql;
        $trans = "User ".$atkid." added to TA.";
    }
    //PERFORM TRANSACTION LOG UPDATE
    $tref = "TA";
    include("inc_transaction_log.php");
}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function editUser(id) {
    document.location.href="setup_access_edit.php?id="+id;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Task Assist: Setup - User Access</b></h1>
<?php echo($result); ?>
<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td class="tdheader">ID</td>
		<td class="tdheader">User</td>
		<td class="tdheader">Access</td>
		<td class="tdheader">&nbsp;</td>
	</tr>
<?php
    //GET SETUP ADMIN DETAILS AND DISPLAY
    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$taadmin."'";
    include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $taadminname = $row['tkname']." ".$row['tksurname'];
    mysql_close();
?>
	<tr>
		<td class="tdgeneral"><?php echo($taadmin); ?></td>
		<td class="tdgeneral"><?php echo($taadminname); ?></td>
		<td class="tdgeneral" colspan=2>Task Assist Administrator</td>
	</tr>
<?php
    //GET ALL OTHER TA USER DETAILS AND DISPLAY
    $sql = "SELECT a.tkid, t.tkname, t.tksurname, a.act, r.ruletext, a.id ";
    $sql .= "FROM assist_".$cmpcode."_ta_list_access a, assist_".$cmpcode."_ta_list_access_rules r, assist_".$cmpcode."_timekeep t ";
    $sql .= "WHERE r.ruleint = a.act AND r.rulefn = 'act' AND yn = 'Y' AND t.tkid = a.tkid AND ruleint < 100";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
?>
        <tr>
            <td class="tdgeneral"><?php echo($row['tkid']); ?></td>
            <td class="tdgeneral"><?php echo($row['tkname']." ".$row['tksurname']); ?></td>
            <td class="tdgeneral"><?php echo($row['ruletext']); ?></td>
            <td class="tdheader"><input type=button value=Edit onclick="editUser(<?php echo($row['id']); ?>)"></td>
        </tr>
<?php
    }
    mysql_close();

//IF A USER HAS ACCESS TO TA BUT IS NOT IN THE ABOVE RESULT THEN DISPLAY THE ADD FORM
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m WHERE t.tkid = m.usrtkid AND usrmodref = 'TA' AND t.tkid NOT IN (SELECT tkid FROM assist_".$cmpcode."_ta_list_access WHERE yn = 'Y')";
include("inc_db_con.php");
$ta = mysql_num_rows($rs);
if($ta > 0)
{
?>
<form method=POST action=setup_access.php>
	<tr>
		<td class="tdgeneral">&nbsp;</td>
		<td class="tdgeneral">
            <select name=atkid>
                <option selected value=X>--- SELECT ---</option>
                <?php
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
                    }
                ?>
            </select>
        </td>
		<td class="tdgeneral">Add/Update/View&nbsp;
            <select name=access>
                <option selected value=X>--- SELECT ---</option>
                <option value=20>own tasks only</option>
                <option value=90>all tasks</option>
            </select>
        </td>
		<td class="tdheader"><input type=submit value=Add></td>
	</tr>
</form>
<?php
}
?>
</table>
</body>

</html>
