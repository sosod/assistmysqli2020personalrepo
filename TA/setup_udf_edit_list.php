<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function delList(i,val,u) {
    if(confirm("Are you sure you wish to delete '"+val+"'?")==true)
    {
        document.location.href = "setup_udf_edit_list_del.php?id="+i+"&udf="+u;
    }
}
function editList(i,u) {
    if(i > 0 && u > 0)
    {
        document.location.href = "setup_udf_edit_list_edit.php?id="+i+"&udf="+u;
    }
    else
    {
        alert("An error has occurred.  Please try again.");
    }
}
function Validate(me) {
    var val = me.udfvvalue.value;
    if(val.length==0)
    {
        document.getElementById('uv').className = 'req';
        alert("Please enter a List Item before clicking the 'Add' button.");
        document.getElementById('uv').focus();
        return false;
    }
    else
    {
        return true;
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: -10px"><b>Task Assist: Setup - Edit UDF List</b></h1>
<?php
$id = $_GET['id'];
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = 'TA' AND udfiid = ".$id;
include("inc_db_con.php");
    $uindex = mysql_fetch_array($rs);
mysql_close();
?>
<h2 class=fc>UDF: <u><?php echo($uindex['udfivalue']); ?></u></h2>
<form name=addlist action=setup_udf_edit_list_process.php method=POST onsubmit="return Validate(this);" language=jscript>
<table border=1 cellpadding=3 cellspacing=0 width=500 style="border-collapse: collapse; border: 1px solid;">
    <tr class=tdheader>
        <td>Ref</td>
        <td>List Item</td>
        <td colspan=2>Action</td>
    </tr>
    <?php include("inc_tr.php"); ?>
        <td class=tdgeneral valign=top align=center width=25>&nbsp;</td>
        <td class=tdgeneral valign=top><input type=text size=50 maxlength=50 name=udfvvalue id=uv class=blank><input type=hidden name=udfvindex value="<?php echo($id); ?>"></td>
        <td class=tdgeneral valign=top colspan=2 align=center><input type=submit value=Add style="padding: 0px 1px 0px 1px"></td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$id." AND udfvyn = 'Y' ORDER BY udfvvalue";
include("inc_db_con.php");
    if(mysql_num_rows($rs)==0)
    {
        ?>
        <tr>
            <td class=tdgeneral colspan=6>No list items to display</td>
        </tr>
        <?php
    }
    else
    {
        while($row = mysql_fetch_array($rs))
        {
            $valjs = html_entity_decode($row['udfvvalue'], ENT_QUOTES, "ISO-8859-1");
            $valjs = str_replace("'","\'",$valjs);
            $valjs = str_replace("\"","&#034;",$valjs);

        ?>
    <?php include("inc_tr.php"); ?>
        <td class=tdgeneral valign=top align=center width=25><?php echo($row['udfvid']); ?></td>
        <td class=tdgeneral valign=top><?php echo($row['udfvvalue']); ?></td>
        <td class=tdgeneral valign=top width=35 align=center><input type=button value=Edit style="padding: 0px 1px 0px 1px" onclick="editList(<?php echo($row['udfvid']); ?>,<?php echo($id); ?>)"></td>
        <td class=tdgeneral valign=top width=35 align=center><input type=button value=Del style="padding: 0px 1px 0px 1px" onclick="delList(<?php echo($row['udfvid']); ?>,'<?php echo($valjs); ?>',<?php echo($id); ?>)"></td>
    </tr>
        <?php
        }
    }
mysql_close();
?>
</table>
</form>
<script language=JavaScript>
    document.getElementById('uv').focus();
</script>
</body>

</html>
