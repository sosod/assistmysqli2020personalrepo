<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");

//GET USER ACCESS PERMISSIONS
$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_access WHERE tkid = '".$tkid."'";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$tact = $row['act'];
mysql_close();

//GET TASKS DETAILS
$taskid = $_GET['i'];
$sql = "SELECT * FROM assist_".$cmpcode."_ta_task WHERE taskid = ".$taskid;
include("inc_db_con.php");
$task = mysql_fetch_array($rs);
mysql_close();

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Task Assist: View Task</b></h1>
<h2 class=fc>Task Details</h2>
<table border="1" id="table1" cellspacing="0" cellpadding="4" width=600>
	<tr>
		<td class=tdheaderl width=140 valign="top"><b>Reference:</b></td>
		<td class=tdgeneral width=460 valign="top"><?php echo($taskid); ?></td>
    </tr>
	<tr>
		<td class=tdheaderl width=140 valign="top"><b>Assigned to:</b></td>
		<td class=tdgeneral width=460 valign="top"><?php
            $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$task['tasktkid']."'";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                echo($row['tkname']." ".$row['tksurname']);
            mysql_close();
        ?></td>
    </tr>
    <tr>
		<td class=tdheaderl valign="top"><b>Assigned by:</b></td>
		<td class=tdgeneral valign="top"><?php
            $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$task['taskadduser']."'";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                echo($row['tkname']." ".$row['tksurname']);
            mysql_close();
        ?></td>
	</tr>
	<tr>
		<td class=tdheaderl valign="top"><b>Topic:</td>
		<td class=tdgeneral valign="top"><?php
            $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE id = '".$task['tasktopicid']."'";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                echo($row['value']);
            mysql_close();
        ?></td>
    </tr>
    <tr>
		<td class=tdheaderl valign="top"><b>Priority:</td>
		<td class=tdgeneral valign="top"><?php
            $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_urgency WHERE id = '".$task['taskurgencyid']."'";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                echo($row['value']);
            mysql_close();
        ?></td>
	</tr>
	<tr>
		<td class=tdheaderl valign="top"><b>Status:</td>
		<td class=tdgeneral valign="top"><?php
            $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE pkey = '".$task['taskstatusid']."'";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                echo($row['value']);
                if($task['taskstatusid'] == "3")
                {
                    echo(" (".$task['taskstate']."%) ");
                }
                else
                {
                    if($task['taskstatusid'] != "5")
                    {
                        echo(" (".$row['state']."%) ");
                    }
                }
            mysql_close();
        ?></td>
    </tr>
    <tr>
		<td class=tdheaderl valign="top"><b>Deadline:</td>
		<td class=tdgeneral valign="top"><?php echo(date("d-M-Y",$task['taskdeadline'])); ?></td>
	</tr>
	<tr>
		<td class=tdheaderl valign="top"><b>Task Instructions:</b></td>
		<td class=tdgeneral valign=top>
        <?php
            $action = explode(chr(10),$task['taskaction']);
            $taskaction = implode("<br>",$action);
            echo($taskaction);
        ?></td>
    </tr>
    <tr>
		<td class=tdheaderl valign="top"><b>Task Deliverables:</b></td>
		<td class=tdgeneral valign=top>
        <?php
            $deliver = explode(chr(10),$task['taskdeliver']);
            $taskdeliver = implode("<br>",$deliver);
            echo($taskdeliver);
        ?></td>
	</tr>
	<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
    include("inc_db_con2.php");
        $udf = mysql_num_rows($rs2);
        $row2 = mysql_fetch_array($rs2);
    mysql_close($rs2);
    ?>
	<tr>
		<td class=tdheaderl valign="top"><b><?php echo($row['udfivalue']); ?>:</b></td>
		<td class=tdgeneral valign="top">
        <?php
        if($udf>0)
        {
            switch($row['udfilist'])
            {
                case "Y":
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                    include("inc_db_con2.php");
                        $row3 = mysql_fetch_array($rs2);
                        echo($row3['udfvvalue']);
                    mysql_close($rs2);
                    break;
                default:
                    echo(str_replace(chr(10),"<br>",$row2['udfvalue']));
                    break;
            }
        }
        else
        {
            echo("N/A");
        }
        ?>
        &nbsp;</td>
	</tr>
<?php
}
mysql_close();
?>
</table>
<?php
//IF THE TASK IS NOT NEW = THERE MUST BE UPDATES TO VIEW
//OR IF THE TASK TKID = CURRENT USER THEN MUST ALLOW ADDING OF UPDATE
//THEREFORE DISPLAY TABLE
if($task['taskstatusid'] != "NW" || $task['tasktkid'] == $tkid)
{
?>
<h2 class=fc>Task Updates</h2>
<form name=taskupdate method=post action=view_all_update_process.php onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=taskid value=<?php echo($taskid);?>>
<table border="1" id="table1" cellspacing="0" cellpadding="5" width=600>
	<tr>
		<td valign="top" class=tdheader>Date</td>
		<td valign="top" class=tdheader>Task Update</td>
		<td valign="top" class=tdheader>Task Status</td>
	</tr>
<?php

//DISPLAY THE UPDATES ON THE CURRENT TASK
$sql = "SELECT * FROM assist_".$cmpcode."_ta_log l, assist_".$cmpcode."_ta_list_status s WHERE l.logstatusid = s.pkey AND l.logtaskid = ".$taskid." ORDER BY logid DESC";
include("inc_db_con.php");
$l = mysql_num_rows($rs);
if($l>0)
{
$l2 = 0;
while($row = mysql_fetch_array($rs))
{
    $l2++;
    if($l > $l2)
    {
?>
	<?php include("inc_tr.php"); ?>
		<td valign="top" class=tdgeneral align=center><?php echo(date("d-M-Y H:i",$row['logdate'])); ?>&nbsp;</td>
		<td valign="top" class=tdgeneral ><?php
		$logarr = explode(chr(10),html_entity_decode($row['logupdate']));
		$logupdate = implode("<br>",$logarr);
		echo($logupdate);
        ?>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php
            echo($row['value']);
            if($row['logstatusid'] == "3")
            {
                echo("<br>(".$row['logstate']."%)");
            }
        ?>&nbsp;</td>
	</tr>
<?php
    }
}
}
else
{
    ?>
    <tr class=tdgeneral><td colspan=3>No updates available.</td></tr>
    <?php
}
mysql_close();
?>
</table>

</form>
<?php
}
?>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

</body>

</html>
