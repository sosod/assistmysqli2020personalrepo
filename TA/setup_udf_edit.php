<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>

</head>
<script language=JavaScript>
function delUDF(i,val) {
    if(confirm("Are you sure you wish to delete '"+val+"'?")==true)
    {
        document.location.href = "setup_udf_edit_del.php?id="+i;
    }
}
function listUDF(i) {
    document.location.href = "setup_udf_edit_list.php?id="+i;
}
function editUDF(i) {
    document.location.href = "setup_udf_edit_view.php?id="+i;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: -10px"><b>Task Assist: Setup - Edit UDFs</b></h1>
<p>&nbsp;</p>
<table border=1 cellpadding=3 cellspacing=0 width=500 style="border-collapse: collapse; border: 1px solid;">
    <tr class=tdheader>
        <td>Ref</td>
        <td>Field Name</td>
        <td>Field Type</td>
        <td colspan=3>Action</td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = 'TA' AND udfiyn = 'Y' ORDER BY udfisort";
include("inc_db_con.php");
    if(mysql_num_rows($rs)==0)
    {
        ?>
        <tr>
            <td class=tdgeneral colspan=6>No UDFs to display</td>
        </tr>
        <?php
    }
    else
    {
        while($row = mysql_fetch_array($rs))
        {
        ?>
    <?php include("inc_tr.php"); ?>
        <td class=tdgeneral align=center width=25><?php echo($row['udfiid']); ?></td>
        <td class=tdgeneral><?php echo($row['udfivalue']); ?></td>
        <td class=tdgeneral><?php
            switch($row['udfilist'])
            {
                case "M":
                    echo("Large Text");
                    break;
                case "T":
                    echo("Small Text");
                    break;
                case "Y":
                    echo("Preset List");
                    break;
                default:
                    echo("N/A");
                    break;
            }
        ?></td>
        <td class=tdgeneral width=35 align=center><input type=button value=Edit style="padding: 0px 1px 0px 1px" onclick="editUDF(<?php echo($row['udfiid']); ?>)"></td>
        <td class=tdgeneral width=35 align=center><input type=button value=Del style="padding: 0px 1px 0px 1px" onclick="delUDF(<?php echo($row['udfiid']); ?>,'<?php echo(str_replace("'","\'",html_entity_decode($row['udfivalue'], ENT_QUOTES, "ISO-8859-1"))); ?>')"></td>
        <td class=tdgeneral width=35 align=center><?php
            if($row['udfilist']=="Y")
            {
                echo("<input type=button value=List style=\"padding: 0px 1px 0px 1px\" onclick=\"listUDF(".$row['udfiid'].")\">");
            }
            else
            {
                echo("&nbsp;");
            }
            ?>
        </td>
    </tr>
        <?php
        }
    }
mysql_close();
?>
</table>
</body>

</html>
