<?php include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
 ?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Task Assist: Setup - Update Status - Add new</b></h1>
<p>&nbsp;</p>
<?php
//GET NEW TOPIC DETAILS
$txt = $_POST['addtxt'];
$stt = $_POST['addstate'];
//FORMAT
$txt = htmlentities($txt,ENT_QUOTES,"ISO-8859-1");
//GET SORT VALUE
$sql = "SELECT max(sort) as sort FROM assist_".$cmpcode."_ta_list_status";
include("inc_db_con.php");
    $sortrow = mysql_fetch_array($rs);
mysql_close();
$sort = $sortrow['sort']+1;
//ADD NEW TOPIC
$sql = "INSERT INTO assist_".$cmpcode."_ta_list_status SET";
$sql.= " id = ''";
$sql.= ", value = '".$txt."'";
$sql.= ", heading = '".$txt."'";
$sql.= ", sort = '".$sort."'";
$sql.= ", custom = 'Y'";
$sql.= ", state = '".$stt."'";
$sql.= ", yn = 'Y'";
include("inc_db_con.php");
    //LOG DETAILS
    $tsql = $sql;
    $trans = "New status ".$txt." added to TA.";
    $tref = "TA";
    include("inc_transaction_log.php");

//SET FORM FOR REDIRECT
echo("<form name=add><input type=hidden name=txt value=Y></form>");

?>
<script language=JavaScript>
//IF REDIRECT FORM EXISTS THEN UPDATE IS DONE - SEND ON TO SETUP_STATUS PAGE
var txt = document.add.txt.value;
if(txt == 'Y')
    document.location.href="setup_status.php";
</script>
</body>

</html>
