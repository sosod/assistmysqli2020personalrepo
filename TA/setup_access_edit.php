<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");

include("inc_ta.php");
//GET EDIT VARIABLES
$result = "";
$aid = $_GET['id'];
$type = $_GET['t'];
$access = $_GET['a'];

if($type == "e")    //IF EDIT TYPE = EDIT
{
    //GET TRANSACTION LOG INFO
    $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_access WHERE id = ".$aid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $ttkid = $row['tkid'];
        $taccess = $row['view'];
    mysql_close();
    //PERFORM EDIT UPDATE
    $sql = "UPDATE assist_".$cmpcode."_ta_list_access SET view = ".$access.", act = ".$access.", yn = 'Y' WHERE id = ".$aid;
    include("inc_db_con.php");
    //SET TRANSACTION LOG VALUES
    $tref = "TA";
    $tsql = $sql;
    $trans = "Updated user ".$ttkid." access from ".$taccess." to ".$access.".";
    //PERFORM TRANSACTION LOG UPDATE
    include("inc_transaction_log.php");
    $result = "done";
}
else
{
    if($type == "d")    //IF EDIT TYPE = DELETE
    {
        //UPDATE LIST-ACCESS AND SET USER TO N
        $sql = "UPDATE assist_".$cmpcode."_ta_list_access SET yn = 'N' WHERE id = ".$aid;
        include("inc_db_con.php");
            //Set transaction log sql value for delete
            $transsql[0] = $sql;
        //GET DETAILS FOR TRANSFERRAL OF TASKS BACK TO TASK OWNER
        $sql = "SELECT tkid FROM assist_".$cmpcode."_ta_list_access WHERE id = ".$aid;
        include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
            $tid = $row['tkid'];
        mysql_close();
            //Set transaction log transaction for delete
            $transaction[0] = "Removed user ".$tid." from TA user access.";
        //GET TASK IDs FOR ALL INCOMPLETE OR ONGOING TASKS
        $sql = "SELECT taskid, taskadduser FROM assist_".$cmpcode."_ta_task WHERE tasktkid = '".$tid."' AND taskstatusid <> 'CL' AND taskstatusid <> 'CN'";
        include("inc_db_con.php");
            $t = 0;
            while($row = mysql_fetch_array($rs))
            {
                $taskid[$t] = $row['taskid'];
                $taskown[$t] = $row['taskadduser'];
                $t++;
            }
        mysql_close();
        $tr = 1;    //Used for transaction log values array
        //LOOP THROUGH TASK IDs OBTAINS FROM PREVIOUS SQL STATEMENT AND UPDATE TASKTKID
        for($t2=0;$t2<$t;$t2++)
        {
            //UPDATE TASKTKID
            $sql = "UPDATE assist_".$cmpcode."_ta_task SET tasktkid = '".$taskown[$t2]."' WHERE taskid = ".$taskid[$t2];
            include("inc_db_con.php");
                //Set transaction log details
                $transaction[$tr] = "Reverted task ".$taskid[$t2]." back to task owner ".$taskown[$t2]." due to removal of ".$tid." access to TA.";
                $transsql[$tr] = $sql;
                $tr++;
            //GET TASK DETAILS FOR LOG UPDATE
            $sql = "SELECT * FROM assist_".$cmpcode."_ta_task WHERE taskid = ".$taskid[$t2];
            include("inc_db_con.php");
                $rowtask = mysql_fetch_array($rs);
            mysql_close();
            //UPDATE TASK LOG TO INDICATE CHANGE OF OWNER
            $logupdate = "Reverted task back to task owner as assigned user was removed from Task Assist.";
            $sql = "INSERT INTO assist_".$cmpcode."_ta_log SET ";
            $sql .= "logdate = '".$today."', ";
            $sql .= "logtkid = '".$taskown[$t2]."', ";
            $sql .= "logupdate = '".$logupdate."', ";
            $sql .= "logstatusid = '".$rowtask['taskstatusid']."', ";
            $sql .= "logstate = ".$rowtask['taskstate'].", ";
            $sql .= "logemail = 'N', ";
            $sql .= "logsubmittkid = '".$tkid."', ";
            $sql .= "logtaskid = '".$taskid[$t2]."', ";
            $sql .= "logtasktkid = '".$taskown[$t2]."'";
            include("inc_db_con.php");
                //Set transaction log details
                $transaction[$tr] = "Reverted task ".$taskid[$t2]." back to task owner ".$taskown[$t2]." due to removal of ".$tid." access to TA.";
                $transsql[$tr] = $sql;
                $tr++;
        }
        //LOOP THROUGH TRANSACTION AND TRANSSQL ARRAY AND EXECURE SQL
        for($tr2=0;$tr2<$tr;$tr2++)
        {
            $tref = "TA";
            $tsql = $transsql[$tr2];
            $trans = $transaction[$tr2];
            include("inc_transaction_log.php");
        }
        $result = "done";
    }
}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function editUser(id) {
    var acc = document.edit.access.value;
    document.location.href = "setup_access_edit.php?id="+id+"&t=e&a="+acc;
}

function delUser(id,tkn) {
    while(tkn.indexOf("_")>0)
    {
        tkn = tkn.replace("_"," ");
    }
    //CONFIRM DELETE ACTION
    if(confirm("Are you sure you wish to delete "+tkn+".\n\nAny incomplete or ongoing tasks will be reassigned to their task owner."))
    {
        document.location.href = "setup_access_edit.php?id="+id+"&t=d";
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Task Assist: Setup - User Access</b></h1>
<?php
echo($transaction[1]);
print_r($taskid);
if($result != "done")   //IF NO ACTION WAS TAKEN THEN DISPLAY EDIT FORM
{
?>
<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td class="tdheader">ID</td>
		<td class="tdheader">User</td>
		<td class="tdheader">Access</td>
	</tr>
<?php

    //GET USER DETAILS AND DISPLAY
    $sql = "SELECT a.tkid, t.tkname, t.tksurname, a.act, a.id, r.ruletext ";
    $sql .= "FROM assist_".$cmpcode."_ta_list_access a, assist_".$cmpcode."_ta_list_access_rules r, assist_".$cmpcode."_timekeep t ";
    $sql .= "WHERE r.ruleint = a.act AND r.rulefn = 'act' AND yn = 'Y' AND t.tkid = a.tkid AND ruleint < 100 AND a.id = ".$aid;
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $tkn = $row['tkname']." ".$row['tksurname'];
?><form name=edit>
        <tr>
            <td class="tdgeneral"><?php echo($row['tkid']); ?></td>
            <td class="tdgeneral"><?php echo($tkn); ?></td>
		<td class="tdgeneral">Add/Update/View&nbsp;
            <select name=access>
                <?php if($row['act'] == 20)
                {
                ?>
                <option selected value=20>own tasks only</option>
                <option value=90>all tasks</option>
                <?php
                }
                else
                {
                ?>
                <option value=20>own tasks only</option>
                <option selected value=90>all tasks</option>
                <?php
                }
                ?>
            </select>
        </td>
        </tr></form>
<?php
    }
    mysql_close();
?>
        <tr>
            <td colspan=3 class="tdgeneral"><input type=button value=Edit onclick="editUser(<?php echo($aid) ;?>)"> <input type=button value=Delete onclick="delUser(<?php echo($aid) ;?>,'<?php echo(str_replace(" ","_",$tkn)) ;?>')"></td>
        </tr>
</table>
<?php
}
?>
<form name=result>
<input type=hidden name=res value=<?php echo($result); ?>>
</form>
<script language=JavaScript>
var res = document.result.res.value;
//IF ACTION WAS TAKEN (result = done) THEN REDIRECT
if(res == "done")
{
    document.location.href="setup_access.php";
}
</script>
</body>

</html>
