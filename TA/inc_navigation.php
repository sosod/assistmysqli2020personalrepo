<form method="post" action="<?php echo($_SERVER['PHP_SELF']);?>" name="paginateForm">
    <input type="submit" id="firstPage" name="firstPage" <?php echo($page == 1 ? "disabled" : "");?> value="|<" />
    <input type="submit" id="prevBtn" name="prevBtn" <?php echo($page == 1 ? "disabled" : "");?> value="<" />
    Page <?php echo($page);?>/<?php echo ($maxPage);?>
    <input type="submit" id="nextBtn" name="nextBtn" <?php echo($page == $maxPage ? "disabled" : "");?> value=">" />
    <input type="submit" id="lastPage" name="lastPage" <?php echo($page == $maxPage ? "disabled" : "");?> value=">|" />
    <input type="hidden"  name="maxPage" value="<?php echo($maxPage);?>" />
    <input type="hidden" name="page" value="<?php echo($page);?>" />
    <input type="hidden" name="taskstatusid" id="taskstatusid" value="<?php echo($sid);?>" />
    <input type="hidden" name="cmpcode" value="<?php echo($cmpcode);?>" />
    <input type="hidden" name="where" value="<?php echo($where);?>" />
    <input type="hidden" name="searchfield" value="<?php echo($searchfield);?>" />
    <?php if($searchfield == 'tasktkid'){?>
	<?php foreach($_POST['tasktkid'] as $key => $value){?>
	      <input type="hidden" name="tasktkid[]" value="<?php echo($value);?>" />
	<?php }?>
    <?php }else{?>
	<input type="hidden" name="<?php echo($searchfield);?>" value="<?php echo($_POST[$searchfield]);?>" />
    <?php }?>
</form>