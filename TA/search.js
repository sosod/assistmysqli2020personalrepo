            $(function(){

                $('#searchfield').change(function(){
                    var selectedVal = $('#searchfield').val();
                    switch(selectedVal){
                        case 'taskadddate':
                            $('#field-container').html('<input type="text" name="taskadddate" id="taskadddate"/>');
                            $("#taskadddate").datepicker({
                                showOn: 'both',
                                buttonImage: '../lib/orange/calendar.gif',
                                buttonImageOnly: true,
                                dateFormat: 'dd M yy',
                                altField: '#startDate',
                                altFormat: 'd_m_yy',
                                changeMonth:true,
                                changeYear:true
                            });
                            break;
                        case 'taskdeadline':
                            $('#field-container').html('<input type="text" name="taskdeadline" id="taskdeadline"/>');
                            $("#taskdeadline").datepicker({
                                showOn: 'both',
                                buttonImage: '../lib/orange/calendar.gif',
                                buttonImageOnly: true,
                                dateFormat: 'dd M yy',
                                altField: '#startDate',
                                altFormat: 'd_m_yy',
                                changeMonth:true,
                                changeYear:true
                            });
                            break;
                        case 'tasktkid':
                            $.post("inc_get_recipients.php",{recipients:1},function(data){
                                $('#field-container').html(data);
                            });
                            break;
                        case 'taskadduser':
                            $.post("inc_get_recipients.php",{taskadduser:1},function(data){
                                $('#field-container').html(data);
                            });
                            break;
                        case 'taskid':
                            $('#field-container').html('<input type="text" name="taskid" id="taskid"/>');
                            break;
                        case 'tasktopicid':
                            $.post("inc_get_recipients.php",{tasktopicid:1},function(data){
                                $('#field-container').html(data);
                            });
                            break;
                        case 'taskurgencyid':
                            $.post("inc_get_recipients.php",{taskurgencyid:1},function(data){
                                $('#field-container').html(data);
                            });
                            break;
                        case 'taskstatusid' :
                            $.post("inc_get_recipients.php",{taskstatusid:1},function(data){
                                $('#field-container').html(data);
                            });
                            break;
                        case 'taskaction':
                            $('#field-container').html('<input type="text" name="taskaction" id="taskaction" size="50"/>');
                            break;
                        case 'taskdeliver':
                            $('#field-container').html('<input type="text" name="taskdeliver" id="taskdeliver" size="50"/>');
                            break;
                        default:
                            $.post("inc_get_recipients.php",{udf:selectedVal},function(data){
                                $('#field-container').html(data);
                            });
                            break;
                        }

                    });
                });
