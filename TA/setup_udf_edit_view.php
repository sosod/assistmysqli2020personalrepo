<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var val = me.udfivalue.value;
    var typ = me.udfilist.value;
    var err;
    var valid8 = "false";
    
    if(val.length > 50 || val.length == 0)
    {
        err = " - Field Name";
        document.getElementById('u1').className = 'req';
    }
    if(typ.length == 0 || (typ != "Y" && typ != "T" && typ != "M"))
    {
        if(err.length > 0)
        {
            err = err + "\n";
        }
        err = err + " - Field Type";
        document.getElementById('u2').className = 'req';
    }
    if(err.length == 0)
    {
        return true;
    }
    else
    {
        alert("Please complete the missing fields:\n"+err);
        return false;
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>

<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: -10px"><b>Task Assist: Setup - Edit UDF</b></h1>
<p>&nbsp;</p>
<?php
$udfiid = $_GET['id'];
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiid = ".$udfiid;
include("inc_db_con.php");
    $uindex = mysql_fetch_array($rs);
mysql_close();


?>
<form name=newudf action=setup_udf_edit_process.php method=POST onsubmit="return Validate(this);" language=jscript>
<input type=hidden name=id value=<?php echo($udfiid); ?>>
<table width=500 border=1 cellpadding=3 cellspacing=0 style="border-collapse: collapse; border: 1px solid;">
    <tr>
        <td class=tdheaderl height=27><b>Old Field Name:</b></td>
        <td class=tdgeneral><?php echo($uindex['udfivalue']); ?></td>
        <td class=tdgeneral>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl><b>New Field Name:</b></td>
        <td class=tdgeneral><input type=text name=udfivalue maxlength=50 size=42 id=u1 class=blank value="<?php echo(html_entity_decode($uindex['udfivalue'], ENT_QUOTES, "ISO-8859-1")); ?>"></td>
        <td class=tdgeneral><i><small>(Max: 50 characters)</small></i></td>
    </tr>
    <tr>
        <td class=tdheaderl height=27><b>Old Field Type:</b></td>
        <td class=tdgeneral>
            <?php
            switch($uindex['udfilist'])
            {
                case "M":
                    echo("Text (No limit)");
                    break;
                case "T":
                    echo("Text (Max: 200 characters)");
                    break;
                case "Y":
                    echo("Preset drop down list");
                    break;
                default:
                    echo("N/A");
                    break;
            }
            ?>
        </td>
        <td class=tdgeneral>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl width=120><b>New Field Type:</b></td>
        <td class=tdgeneral width=260>
            <select name=udfilist id=u2>
                <option <?php if($uindex['udfilist']!="T" && $uindex['udfilist']!="M") { echo(" selected "); } ?> value=Y>Preset drop down list</option>
                <option <?php if($uindex['udfilist']=="T") { echo(" selected "); } ?> value=T>Text (Max: 200 characters)</option>
                <option <?php if($uindex['udfilist']=="M") { echo(" selected "); } ?> value=M>Text (No limit)</option>
            </select>
        </td>
        <td class=tdgeneral width=120>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdheaderl width=120>Required</td>
        <td class=tdgeneral width=260><input type="checkbox" name="udfirequired" id="udfirequired" <?php echo($uindex['udfirequired'] == 1 ? "checked=checked" : "");?>/></td>
    </tr>
    <tr>
       <td class=tdheaderl width=120>Default</td>
       <td class=tdgeneral width=260>
           <?php if($uindex['udfilist'] == 'M'){?>
            <textarea name="udfidefault" id="udfidefault" cols="40" rows="4"><?php echo ($uindex['udfidefault']);?></textarea>
           <?php }else if($uindex['udfilist'] == 'T'){?>
            <input type="text" name="udfidefault" id="udfidefault" size="50" value="<?php echo($uindex['udfidefault']);?>"/>
           <?php }?>
       </td>
    </tr>
    <tr class=tdgeneral>
        <td colspan=3><input type=submit value="Submit" style="margin-left: 115px"></td>
    </tr>
</table>
</form>
</body>

</html>
