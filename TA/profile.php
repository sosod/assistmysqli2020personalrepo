<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function whenMe() {
var me = document.getElementById('r2');
if(me.value == "daily")
    document.getElementById('r3').disabled = true;
else
    document.getElementById('r3').disabled = false;
}
function Validate(me) {
    return true;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
.tdgeneral {
    border: 1px solid #dddddd;
}
.tdheaderl {
    border-bottom: 1px solid #ffffff;
}
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Task Assist: My Profile</b></h1>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_ta_profile_section WHERE id = 1 AND yn = 'Y'";
include("inc_db_con.php");
$mnr = mysql_num_rows($rs);
mysql_close($con);
if($mnr>0)
{
?>
<h2 class=fc>Reminder Emails</h2>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_ta_profile WHERE sectionid = 1 AND tkid = '".$tkid."'";
include("inc_db_con.php");
$pnr = mysql_num_rows($rs);
$row = mysql_fetch_array($rs);
mysql_close($con);

if($pnr>0)
{
    $r = array();
    $r[0] = $pnr;
    for($i=1;$i<5;$i++)
    {
        $r[$i] = $row['field'.$i];
    }
    $r[3] = $r[3] * 1;
}
else
{
    $r = array();
    $r[0] = 0;
    $r[1] = "N";
    $r[2] = "weekly";
    $r[3] = 1;
    $r[4] = "week";
}
?>
<form id=rem action=profile_process.php method=post>
<table cellpadding="3" cellspacing="0" width=600>
	<tr>
        <td class=tdheaderl width=100>Receive?</td>
        <td class=tdgeneral><select name=rem1 id=r1><option <?php if($r[1]!="N") { echo("selected"); } ?> value=Y>Yes</option><option <?php if($r[1]=="N") { echo("selected"); } ?> value=N>No</option></select></td>
    </tr>
	<tr>
        <td class=tdheaderl>When?</td>
        <td class=tdgeneral><select name=rem2 id=r2 onchange="whenMe();">
            <option <?php if($r[2]!="daily") { echo("selected"); } ?> value=weekly>Weekly</option>
            <option <?php if($r[2]=="daily") { echo("selected"); } ?> value=daily>Daily</option>
        </select> on <select name=rem3 id=r3>
            <option <?php if($r[3]<1 || $r[3]>5 || !is_numeric($r)) { echo("selected"); } ?> value=1>Monday</option>
            <option <?php if($r[3]==2) { echo("selected"); } ?> value=2>Tuesday</option>
            <option <?php if($r[3]==3) { echo("selected"); } ?> value=3>Wednesday</option>
            <option <?php if($r[3]==4) { echo("selected"); } ?> value=4>Thursday</option>
            <option <?php if($r[3]==5) { echo("selected"); } ?> value=5>Friday</option>
        </select></td>
    </tr>
	<tr>
        <td class=tdheaderl>What?</td>
        <td class=tdgeneral><select name=rem4 id=r4>
            <option <?php if($r[4]!="today" && $r[4]!="all"  && $r[4]!="today2"  && $r[4]!="week2" ) { echo("selected"); } ?> value=week>Tasks due this week</option>
            <option <?php if($r[4]=="week2") { echo("selected"); } ?> value=week2>Tasks due on or before this week</option>
            <option <?php if($r[4]=="today") { echo("selected"); } ?> value=today>Tasks due today</option>
            <option <?php if($r[4]=="today2") { echo("selected"); } ?> value=today2>Tasks due on or before today</option>
            <option <?php if($r[4]=="all") { echo("selected"); } ?> value=all>All incomplete tasks</option>
        </select></td>
    </tr>
	<tr>
        <td class=tdgeneral style="border-right: 1px solid #ffffff;">&nbsp;</td>
        <td class=tdgeneral style="border-left: 1px solid #ffffff;"><input type=submit value="Update Profile"> <input type=reset></td>
    </tr>
</table>
</form>
<?php
} //if reminderemail section under profile exists
?>
<p>&nbsp;</p>
<script type=text/javascript>
whenMe();
</script>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_ta_profile_section WHERE id = 2 AND yn = 'Y'";
include("inc_db_con.php");
$mnr = mysql_num_rows($rs);
mysql_close($con);
if($mnr>0)
{
?>
<h2 class=fc>Tasks per Page (List View)</h2>
<form id='tasksperpage_form' name='tasksperpage_form' action='tasksperpage_process.php' method='post'>
<table cellpadding='3'>
    <tr>
        <td class='tdheaderl'>How many tasks do you want listed per page? </td>
        <td>
    <select name='taskperpage' id='taskperpage'>
        <option value='10'>10</option>
        <option value='15' selected>15</option>
        <option value='20'>20</option>
        <option value='25'>25</option>
        <option value='30'>30</option>
        <option value='35'>35</option>
        <option value='40'>40</option>
        <option value='45'>45</option>
        <option value='50'>50</option>
    </select>
    </td>
    </tr>
    <tr>
        <td colspan='2' align='right'>
            <input type='submit' name='btnSave' id='btnSave' value='Save' />
        </td>
    </tr>
    </table>
    </form>
<?php
   


}	//if tasksperpage section under profile exists
?>
</body>

</html>
