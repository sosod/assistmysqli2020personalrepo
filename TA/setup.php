<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");

//GET SETUP ADMIN TKID
$ta0 = $_POST['TA0'];
//IF SETUP ADMIN TKID WAS SENT THEN PERFORM UPDATE
if(strlen($ta0) > 0)
{
    //GET CURRENT SETUP ADMIN TKID
    $sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = 'TA' AND refid = 0";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $ta0z = $row['value'];
    mysql_close();
    $sql = "";
    //UPDATE ASSIST-SETUP TABLE
    $sql = "UPDATE assist_".$cmpcode."_setup SET value = '".$ta0."' WHERE ref = 'TA' AND refid = 0";
    include("inc_db_con.php");
        //Set transaction log values for this update
        $transsql[0] = str_replace("'","|",$sql);
        $transaction[0] = "Updated TA Setup Administrator to ".$ta0;
    $sql = "";
    //WRITE DOWN PREVIOUS SETUP ADMIN'S ACCESS
    $sql = "UPDATE assist_".$cmpcode."_ta_list_access SET act = 90, view = 90 WHERE tkid = '".$ta0z."' AND yn = 'Y'";
    include("inc_db_con.php");
        //set transaction log values
        $transsql[1] = str_replace("'","|",$sql);
        $transaction[1] = "Updated TA user access for the previous setup admin ".$ta0z." down to 90.";

    if($ta0 != "0000")  //If the setup admin is not the IA admin then change user access to 100
    {
        //Get access details from assist-ta-list-access
        $sql = "SELECT id FROM assist_".$cmpcode."_ta_list_access WHERE tkid = '".$ta0."' AND yn='Y'";
        include("inc_db_con.php");
        $ta0c = mysql_num_rows($rs);
        if($ta0c > 0)   //If there is a result then set the sql variable to update the access value
        {
            $err = "N";
            $row = mysql_fetch_array($rs);
            $taccid = $row['id'];
            $sql0 = "UPDATE assist_".$cmpcode."_ta_list_access SET act = 100, view = 100 WHERE id = ".$taccid;
                //set transaction log values
                $transsql[2] = str_replace("'","|",$sql);
                $transaction[2] = "Updated TA user access for ".$ta0." to 100.";
        }
        else    //If there was no result then set the sql variable to insert a new record for the setup admin
        {
            $err = "Y";
            
            $sql0 = "INSERT INTO assist_".$cmpcode."_ta_list_access SET tkid = '".$ta0."', act = 100, view = 100, yn = 'Y'";
                //Set the transaction log values
                $transsql[2] = str_replace("'","|",$sql);
                $transaction[2] = "Added TA user access for ".$ta0." as 100.";
        }
        mysql_close();
        $sql = "";
        //RUN THE UPDATE SET IN PREVIOUS IF STATEMENT
        $sql = $sql0;
        include("inc_db_con.php");
        $sql = "";
    }
    else
    {
        //Set transaction log values if setup admin = IA admin
        $transsql[2] = "No work was done.";
        $transaction[2] = "Did not add TA user access for ".$ta0." as tkid = 0000 (Administrator).";
    }

    //PERFORM TRANSACTION LOG UPDATES OF VALUES SET PREVIOUSLY
    for($t=0;$t<3;$t++)
    {
        $trans = $transaction[$t];
        $tsql = $transsql[$t];
        $tref = 'TA';
        include("inc_transaction_log.php");
    }

    $result = "<p><i>Administrator update complete.</i></p>";
}
else
{
    $result = "<p>&nbsp;</p>";
}
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: -10px"><b>Task Assist: Setup</b></h1>
<?php echo($result); ?>
<ul>
<form name=update method=post action=setup.php>
<li style="margin-bottom: 15px">Task Assist Administrator: <select name=TA0>
<?php
//GET SETUP ADMIN TKID FROM ASSIST-SETUP
$sql = "SELECT value FROM assist_".$cmpcode."_setup WHERE ref = 'TA' AND refid = 0";
include("inc_db_con.php");
$t = mysql_num_rows($rs);
if($t > 0)
{
    //IF RESULT THEN GET TKID
    $row = mysql_fetch_array($rs);
    $taadmin = $row['value'];
    $sqlt = "";
}
else
{
    //IF NO RESULT THEN SET SQL VARIABLE TO ADD DEFAULT 0000
    $sqlt = "INSERT INTO assist_".$cmpcode."_setup SET ref = 'TA', refid = 0, value = '0000', comment = 'Setup administrator', field = ''";
}
mysql_close();
//IF SQL STATEMENT WAS SET THEN RUN UPDATE
if(strlen($sqlt) > 0)
{
    $sql = $sqlt;
    include("inc_db_con.php");
    $taadmin = "0000";
}
$sql = "";
//SET THE SELECTED OPTION IF SETUP ADMIN = 0000
if($taadmin == "0000")
{
?>
    <option selected value=0000>Ignite Assist Administrator</option>
<?php
}
else
{
?>
    <option value=0000>Ignite Assist Administrator</option>
<?php
}
//GET USERS (NOT 0000) THAT HAVE ACCESS TO TA AND COULD POTENTIALLY BE SETUP ADMIN
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users m ";
$sql .= "WHERE t.tkstatus = 1 AND t.tkid <> '0000' ";
$sql .= "AND t.tkid = m.usrtkid AND m.usrmodref = 'TA' ";
$sql .= "ORDER BY t.tkname, t.tksurname";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $tid = $row['tkid'];
    if($tid == $taadmin)    //IF THE USER IS SETUP ADMIN THEN SELECT THAT OPTION
    {
        echo("<option selected value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
    else
    {
        echo("<option value=".$tid.">".$row['tkname']." ".$row['tksurname']."</option>");
    }
}
mysql_close();

?>
</select> <input type=submit value=Update></li>

<li style="margin-bottom: 15px"><a href=setup_access.php>User Access</a></li>

<li style="margin-bottom: 15px"><a href=setup_topic.php>Update Topics</a></li>

<li style="margin-bottom: 15px"><a href=setup_status.php>Update Status</a></li>

<li style="margin-bottom: 15px">User Defined Fields (UDFs)<ul class=ul2>
<li><a href=setup_udf_new.php>Create UDF</a></li>
<li><a href=setup_udf_edit.php>Edit UDFs</a></li>
<li><a href=setup_udf_order.php>Sort UDFs</a></li>
</ul></li>

<li style="margin-bottom: 15px">"View Tasks" List Columns<ul class=ul2>
<li><a href=setup_columns_my.php>My Tasks</a></li>
<li><a href=setup_columns_own.php>Tasks Owned</a></li>
<li><a href=setup_columns_all.php>All Tasks</a></li>
</ul><br>&nbsp;</li>

</ul>
<?php
$helpfile = "../help/TA_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
</form>
</body>

</html>
