<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
$sid = $_GET['s'];
/****************************pagination logic***************************/
$page = 1;
if(isset($_POST['page'])) {
    $cmpcode = $_POST['cmpcode'];
    $sid = $_POST['taskstatusid'];
    if(isset($_POST['nextBtn'])) {
        $page = $_POST['page'];
        $maxPage = $_POST['maxPage'];
        if($page + 1 > $maxPage)
            $page = $maxPage;
        else
            $page++;
    }else if(isset($_POST['prevBtn'])) {
        $page = $_POST['page'];
        $maxPage = $_POST['maxPage'];
        if($page - 1 < 1)
            $page = 1;
        else
            $page--;
    }else if(isset($_POST['lastPage'])) {
        $page = $_POST['maxPage'];
    }else if(isset($_POST['firstPage'])) {
        $page = 1;
    }else {
        $page = 1;
    }
}

$offset = ($page - 1) * $rowsPerPage;
/***********************************************************************/

$d=0;
$displayY = array();
$sql = "SELECT headingfull, field, type FROM assist_".$cmpcode."_ta_list_display WHERE alldisp = 'Y' AND yn = 'Y' AND allyn = 'Y' ORDER BY allsort";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs)) {
    $displayY[$d] = $row;
    $d++;
}
mysql_close();


//GET STATUS TEXT
if($sid == "ALL") {
    $statusheading = "All incomplete tasks";
    $statsql = "SELECT t.taskid";
    $statsql .= ", u.tkname utn";
    $statsql .= ", u.tksurname uts";
    $statsql .= ", l.value topval";
    $statsql .= ", s.value statval";
    $statsql .= ", t.taskaction";
    $statsql .= ", t.taskdeliver";
    $statsql .= ", t.taskdeadline ";
    $statsql .= ", p.value urgval";
    $statsql .= ", t.taskadddate";
    $statsql .= ", o.tkname otn, o.tksurname ots ";
    $statsql .= " FROM assist_".$cmpcode."_ta_task t";
    $statsql .= ", assist_".$cmpcode."_ta_list_topic l";
    $statsql .= ", assist_".$cmpcode."_ta_list_urgency p";
    $statsql .= ", assist_".$cmpcode."_timekeep u";
    $statsql .= ", assist_".$cmpcode."_timekeep o";
    $statsql .= ", assist_".$cmpcode."_ta_list_status s ";
    $statsql .= " WHERE t.tasktkid = u.tkid";
    $statsql .= " AND t.taskstatusid = s.pkey";
    $statsql .= " AND s.id <> 'CL'";
    $statsql .= " AND s.id <> 'CN'";
    $statsql .= " AND t.tasktopicid = l.id";
    $statsql .= " AND t.taskurgencyid = p.id";
    $statsql .= " AND t.taskadduser = o.tkid";
}
else {
    $sql = "SELECT value, heading FROM assist_".$cmpcode."_ta_list_status WHERE pkey = '".$sid."'";
    include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $statusheading = $row['heading'];
    mysql_close();
    $statsql = "SELECT t.taskid";
    $statsql .= ", u.tkname utn";
    $statsql .= ", u.tksurname uts";
    $statsql .= ", l.value topval";
    $statsql .= ", s.value statval";
    $statsql .= ", p.value urgval";
    $statsql .= ", t.taskaction";
    $statsql .= ", t.taskdeliver";
    $statsql .= ", t.taskdeadline ";
    $statsql .= ", t.taskadddate";
    $statsql .= ", o.tkname otn, o.tksurname ots ";
    $statsql .= " FROM assist_".$cmpcode."_ta_task t";
    $statsql .= ", assist_".$cmpcode."_ta_list_topic l";
    $statsql .= ", assist_".$cmpcode."_ta_list_urgency p";
    $statsql .= ", assist_".$cmpcode."_timekeep u";
    $statsql .= ", assist_".$cmpcode."_timekeep o";
    $statsql .= ", assist_".$cmpcode."_ta_list_status s ";
    $statsql .= " WHERE t.tasktkid = u.tkid";
    $statsql .= " AND t.taskstatusid = s.pkey";
    $statsql .= " AND t.taskstatusid = '".$sid."'";
    $statsql .= " AND t.tasktopicid = l.id";
    $statsql .= " AND t.taskurgencyid = p.id";
    $statsql .= " AND t.taskadduser = o.tkid";
}
?>
<html>

    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
        <link href="../lib/calendar/css/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css"/>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-1.4.2.min.js"></script>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-ui-1.8.1.custom.min.js"></script> 
        <script type="text/javascript" src="search.js"></script>
        <style type="text/css">
            table.search-paginate {
                width:650px;
            }
            table.search-paginate td{
                border:none;
                padding: 5px 5px 0px 5px;
                vertical-align: top;
            }
        </style>

    </head>
    <script language=JavaScript>
            function viewTask(id) {
                if(id > 0)
                {
                    document.location.href="view_all_update.php?i="+id;
                }
            }
    </script>
    <link rel="stylesheet" href="/default.css" type="text/css">
    <?php include("inc_style.php"); ?>

    <?php
//GET LIST OF TASKS THAT ARE WITHIN STATUS
    if($sid != "ALL") {
        $sql = "SELECT COUNT(DISTINCT(t.taskid)) AS numrows
                FROM assist_".$cmpcode."_ta_task t INNER JOIN  assist_".$cmpcode."_ta_list_topic l ON (t.tasktopicid=l.id)
                INNER JOIN  assist_".$cmpcode."_ta_list_urgency p ON(t.taskurgencyid=p.id)
                 INNER JOIN assist_".$cmpcode."_timekeep u ON(t.taskadduser =u.tkid)
                     INNER JOIN  assist_".$cmpcode."_ta_list_status s ON(t.taskstatusid=s.pkey)
                        INNER JOIN assist_".$cmpcode."_ta_task_recipients r ON(t.taskid=r.taskid)
            WHERE  t.taskstatusid = s.pkey AND t.taskstatusid =$sid AND t.tasktopicid = l.id AND t.taskurgencyid = p.id GROUP BY s.pkey";
        //echo $sql;
      /*  $sql = "SELECT COUNT(t.taskid) AS numrows
                FROM assist_".$cmpcode."_ta_task t
                     INNER JOIN  assist_".$cmpcode."_ta_list_status s ON(t.taskstatusid=s.pkey)
            WHERE  t.taskstatusid = s.pkey AND t.taskstatusid =$sid  GROUP BY s.pkey";*/
        //AND ((t.taskadduser = '".$tkid."') OR (r.tasktkid='".$tkid."'))

        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $numrows = $row['numrows'];
        mysql_close($con);
        $sql = "SELECT DISTINCT(t.taskid), l.value topval, s.value statval, p.value urgval, t.taskaction, t.taskdeliver, t.taskdeadline , t.taskadddate
                FROM assist_".$cmpcode."_ta_task t INNER JOIN  assist_".$cmpcode."_ta_list_topic l ON (t.tasktopicid=l.id)
                INNER JOIN  assist_".$cmpcode."_ta_list_urgency p ON(t.taskurgencyid=p.id)
                 INNER JOIN assist_".$cmpcode."_timekeep u ON(t.taskadduser =u.tkid)
                     INNER JOIN  assist_".$cmpcode."_ta_list_status s ON(t.taskstatusid=s.pkey)
                     INNER JOIN assist_".$cmpcode."_ta_task_recipients r ON(t.taskid=r.taskid)
            WHERE  t.taskstatusid = s.pkey AND t.taskstatusid =$sid AND t.tasktopicid = l.id AND t.taskurgencyid = p.id                 
                LIMIT $offset,$rowsPerPage";
       // echo $sql;
        //AND ((t.taskadduser = '".$tkid."') OR (r.tasktkid='".$tkid."'))
    }else {
        $sql = "SELECT COUNT(DISTINCT(t.taskid)) AS numrows
                FROM assist_".$cmpcode."_ta_task t INNER JOIN  assist_".$cmpcode."_ta_list_topic l ON (t.tasktopicid=l.id)
                INNER JOIN  assist_".$cmpcode."_ta_list_urgency p ON(t.taskurgencyid=p.id)
                 INNER JOIN assist_".$cmpcode."_timekeep u ON(t.taskadduser =u.tkid)
                     INNER JOIN  assist_".$cmpcode."_ta_list_status s ON(t.taskstatusid=s.pkey)
                         INNER JOIN assist_".$cmpcode."_ta_task_recipients r ON(t.taskid=r.taskid)
            WHERE  t.taskstatusid = s.pkey AND s.id <> 'CL' AND s.id <> 'CN' AND t.tasktopicid = l.id AND t.taskurgencyid = p.id
            ";
       /* $sql = "SELECT COUNT(DISTINCT(t.taskid)) AS numrows
                FROM assist_".$cmpcode."_ta_task t 
                     INNER JOIN  assist_".$cmpcode."_ta_list_status s ON(t.taskstatusid=s.pkey)                         
            WHERE  t.taskstatusid = s.pkey AND s.id <> 'CL' AND s.id <> 'CN' GROUP BY
            ";*/
        //AND ((t.taskadduser = '".$tkid."') OR (r.tasktkid='".$tkid."'))
      //echo $sql;
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $numrows = $row['numrows'];
        mysql_close($con);

        $sql = "SELECT DISTINCT(t.taskid), l.value topval, s.value statval, p.value urgval, t.taskaction, t.taskdeliver, t.taskdeadline , t.taskadddate
                FROM assist_".$cmpcode."_ta_task t INNER JOIN  assist_".$cmpcode."_ta_list_topic l ON (t.tasktopicid=l.id)
                INNER JOIN  assist_".$cmpcode."_ta_list_urgency p ON(t.taskurgencyid=p.id)
                 INNER JOIN assist_".$cmpcode."_timekeep u ON(t.taskadduser =u.tkid)
                     INNER JOIN  assist_".$cmpcode."_ta_list_status s ON(t.taskstatusid=s.pkey)
                         INNER JOIN assist_".$cmpcode."_ta_task_recipients r ON(t.taskid=r.taskid)
            WHERE  t.taskstatusid = s.pkey AND s.id <> 'CL' AND s.id <> 'CN' AND t.tasktopicid = l.id AND t.taskurgencyid = p.id            
                LIMIT $offset,$rowsPerPage";
        //AND ((t.taskadduser = '".$tkid."') OR (r.tasktkid='".$tkid."'))
        //echo $sql;
    }

    include("inc_db_con.php");
  
    $t = mysql_num_rows($rs);
    $maxPage = ceil($numrows/$rowsPerPage);
    if($maxPage == 0)
        $maxPage = 1;
    if($t > 0) {

        ?>
    <base target="main">
    <body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
        <h1 class=fc><b>Task Assist: View Tasks - <?php echo($statusheading); ?></b></h1>
        <table class="search-paginate">
            <tr>

                <td nowrap="nowrap">
                        <?php include ('inc_navigation.php');?>
                </td>
                <td align="right" nowrap="nowrap" valign="top">
                        <?php include ('inc_all_search_box.php');?>
                </td>               
            </tr>
        </table>

        <table class="search-paginate" style="border:none;margin-bottom: 5px;">
            <tr>
                <td align="right" >
                    <a href="view_all_adv_search.php?s=<?php  echo($sid);?>" >Advanced Search</a>
                </td>
            </tr>
        </table>

        <table border="1" id="table1" cellspacing="0" cellpadding="3" style="border-collapse: collapse; border: 1px solid #ededed">
            <tr>
                <td class=tdheader valign="top">Ref</td>
                    <?php
                    foreach($displayY as $dy) {
                        echo("<td class=tdheader valign=\"top\">".$dy['headingfull']."</td>");
                    }
                    ?>
                <td class=tdheader valign="top">&nbsp;</td>
            </tr>            
                <?php
                while($row = mysql_fetch_array($rs)) {
                    $taskaction = str_replace(chr(10),"<br>",$row['taskaction']);
                    $taskdeliver = str_replace(chr(10),"<br>",$row['taskdeliver']);
                    $valarr['tasktopicid'] = $row['topval'];
                    $valarr['taskstatusid'] = $row['statval'];
                    $valarr['taskdeadline'] = $row['taskdeadline'];
                    $valarr['taskurgencyid'] = $row['urgval'];
                    $valarr['taskadddate'] = date("d M Y",$row['taskadddate']);
                    $valarr['tasktkid'] = $row['utn']." ".$row['uts'];
                    $valarr['taskadduser'] = $row['otn']." ".$row['ots'];
                    ?>
                    <?php include("inc_tr.php"); ?>
            <td class=tdheader valign="top"><?php echo($row['taskid']); ?>&nbsp;</td>
                    <?php
                    foreach($displayY as $dy) {

                        if($dy['type']=="U") {
                            $tid = $row['taskid'];
                            $sql2 = "SELECT udfilist FROM assist_".$cmpcode."_udfindex WHERE udfiref = 'TA' AND udfiid = ".$dy['field'];
                            include("inc_db_con2.php");
                            $rowu = mysql_fetch_array($rs2);
                            mysql_close($con2);
                            if($rowu['udfilist']!="Y") {
                                $sql2 = "SELECT udfvalue FROM assist_".$cmpcode."_udf WHERE udfref = 'TA' AND udfnum = ".$tid." AND udfindex = ".$dy['field'];
                                include("inc_db_con2.php");
                                $row2 = mysql_fetch_array($rs2);
                                mysql_close($con2);
                            }
                            else {
                                $sql2 = "SELECT udfvvalue as udfvalue FROM assist_".$cmpcode."_udf u, assist_".$cmpcode."_udfvalue v WHERE u.udfref = 'TA' AND u.udfnum = ".$tid." AND u.udfindex = ".$dy['field']." AND u.udfvalue = v.udfvid";
                                include("inc_db_con2.php");
                                $row2 = mysql_fetch_array($rs2);
                                mysql_close($con2);
                            }
                            echo("<td class=tdgeneral valign=\"top\" width=150>".$row2['udfvalue']."&nbsp;</td>");
                        }
                        else {
                            switch($dy['field']) {
                                case "taskaction":
                                    echo("<td class=tdgeneral valign=\"top\" width=250>".$taskaction."&nbsp;</td>");
                                    break;
                                case "taskdeliver":
                                    echo("<td class=tdgeneral valign=\"top\" width=250>".$taskdeliver."&nbsp;</td>");
                                    break;
                                case "taskadddate":
                                    echo("<td class=tdgeneral valign=\"top\" align=center>".$valarr[$dy['field']]."&nbsp;</td>");
                                    break;
                                case "taskdeadline":
                                    if($sid != "ON") {
                                        echo("<td class=tdgeneral valign=top align=center>");
                                        $tdeadline = $row['taskdeadline'];
                                        if($tdeadline < $today && $sid != 'CL' && $row['statval'] != "On-going") {
                                            echo("<b><font class=fc>".date("d M Y",$row['taskdeadline'])."</font></b>");
                                        }
                                        else {
                                            if($row['statval'] != "On-going") {
                                                echo(date("d M Y",$row['taskdeadline']));
                                            }
                                        }
                                        echo("&nbsp;</td>");
                                    }
                                    break;
                                default:

                                    if($dy['field'] == 'tasktkid') {

                                        $sql2 = "SELECT t.tkname,t.tksurname FROM assist_".$cmpcode."_ta_task_recipients r INNER JOIN assist_".$cmpcode."_timekeep t ON (r.tasktkid=t.tkid) WHERE taskid=".$row['taskid'];
                                        //echo $sql;
                                        include("inc_db_con2.php");
                                        $recipients = "";
                                        while($rowQ = mysql_fetch_array($rs2)) {
                                            $recipients .= $rowQ['tkname']." ".$rowQ['tksurname'].",";
                                        }
                                        $recipients = substr($recipients, 0,-1);
                                        $valarr[$dy['field']] = $recipients;
                                        mysql_close($con2);

                                    }else if($dy['field'] == 'taskadduser') {
                                        $sql2 = "SELECT k.tkname,k.tksurname FROM assist_".$cmpcode."_timekeep k INNER JOIN assist_".$cmpcode."_ta_task t ON (k.tkid=t.taskadduser) WHERE t.taskid=".$row['taskid'];
                                        include("inc_db_con2.php");
                                        $rowH = mysql_fetch_array($rs2);
                                        $valarr[$dy['field']] = $rowH['tkname']." ".$rowH['tksurname'];
                                    }
                                    echo("<td class=tdgeneral valign=\"top\">".$valarr[$dy['field']]."&nbsp;</td>");
                                    break;
                            }
                        }
                    }
                    ?>
                    <?php
                    ?>
            <td class=tdheader valign="top">&nbsp;<input type="button" value="View" name="B4" onclick="viewTask(<?php echo($row['taskid']); ?>)">&nbsp;</td>
        </tr>
                <?php
            }
        }//IF MYSQL_NUM_ROWS > 0
        else {
            echo("<p>There are currently no tasks to view.</p>");
        }
        mysql_close();
        ?>
    </table>


    <p>&nbsp;</p>
    <p><img src=/pics/tri_left.gif align=absmiddle > <a href=# onclick="document.location.href = 'view.php';" class=grey>Go back</a></p>
</body>

</html>
