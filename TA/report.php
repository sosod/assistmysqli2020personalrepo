<?php include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
<link href="../lib/calendar/css/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css"/>
  <script type ="text/javascript" src="../lib/calendar/js/jquery-1.4.2.min.js"></script>
  <script type ="text/javascript" src="../lib/calendar/js/jquery-ui-1.8.1.custom.min.js"></script>
  
<?php

if(substr_count($_SERVER['HTTP_USER_AGENT'],"MSIE")>0) {
    include("inc_head_msie.php");
} else {
    include("inc_head_ff.php");
}
?>
<style type=text/css>
table { border: 0px solid #ffffff;}
table td { border: 0px solid #ffffff; }
</style>
<script type="text/javascript">
    $(function(){
                
                //Start
                $('#datepickerFrom').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });

                //End
                $('#datepickerTo').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#datepickerDeadlineTo').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $('#datepickerDeadlineFrom').datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#endDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });

    });
</script>
</head>

<script language=JavaScript>
function Validate(me) {
    return true;
}

			
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5 style='font-size: 62.5%'>
<h1 class=fc><b>Task Assist: Report</b></h1>
<form name=taskreport method=post action=report_process.php language=jscript onsubmit="return Validate(this);">
<h3 class=fc>1. Select the details you want in your report:</h3>
<div style="margin-left: 17px">
<table border="0" id="table1" cellpadding="3" cellspacing="0" style="border-width: 0px;">
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="adddate"></td>
		<td class="tdgeneral">Date tasked</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="tkid"></td>
		<td class="tdgeneral">User tasked</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="adduser"></td>
		<td class="tdgeneral">Task owner</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="topicid"></td>
		<td class="tdgeneral">Task topic</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="action"></td>
		<td class="tdgeneral">Task instructions</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="deliver"></td>
		<td class="tdgeneral">Task deliverables</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="deadline"></td>
		<td class="tdgeneral">Task deadline</td>
	</tr>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="field[]" value="update"></td>
		<td class="tdgeneral">Updates: <select name=upfilter><option selected value=AGE>Most recent update</option><option value=ALL>All updates</option></select></td>
	</tr>
        <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = 'TA' ORDER BY udfisort";
            include("inc_db_con.php");
            while($row = mysql_fetch_array($rs)){
        ?>
	<tr>
		<td class="tdgeneral" align=center><input type="checkbox" checked name="udf[]" value="<?php echo($row['udfiid']); ?>"></td>
		<td class="tdgeneral"><?php echo($row['udfivalue']); ?></td>
	</tr>
<?php
        }
    mysql_close();
?>
</table>
</div>
<h3 class=fc>2. Select the filter you wish to apply:</h3>
<div style="margin-left: 17px">
<table border="0" id="table2" cellspacing="0" cellpadding="3" style="border-width: 0px;">
	<tr>
		<td class="tdgeneral" style="vertical-align:top;">Task assigned to:</td>

		<td class="tdgeneral">
                     <?php
                            /*$sql = "SELECT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' ORDER BY t.tkname, t.tksurname";*/

                            $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = 'TA'
                                ORDER BY t.tkname, t.tksurname";

                            include("inc_db_con.php");
                            $size = mysql_num_rows($rs);
				if($size>10) { $size = 10; }
                            ;?>
                        
                        <select  id="tasktkidfilter" name="tasktkidfilter[]" multiple="multiple" size="<?php echo($size);?>">
                            <!--<option value=X>--- SELECT ---</option>-->
                                <?php
                                while($row = mysql_fetch_array($rs)) {
                                    echo("<option name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>");
                                }
                                mysql_close();
                                ?>
                        </select><br />
				<i>Ctrl + left click to select multiple users</i>
                </td>
	</tr>
	<tr>
		<td class="tdgeneral">Task owner:</td>
		<td class="tdgeneral"><select size="1" name="taskaddfilter">
			<option selected value=ALL>All users</option>
			<?php
                    $sql = "SELECT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a WHERE t.tkid = a.tkid AND t.tkid <> '0000' ORDER BY t.tkname, t.tksurname";
                    include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['tkid'].">".$row['name']."</option>");
                    }
                    mysql_close();
            ?>
		</select></td>
	</tr>
	<tr>
		<td class="tdgeneral">Task topic:</td>
		<td class="tdgeneral"><select size="1" name="tasktopicfilter">
			<option selected value=ALL>All topics</option>
			<?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE yn = 'Y' ORDER BY value";
                    include("inc_db_con.php");
                    while($row = mysql_fetch_array($rs))
                    {
                        echo("<option value=".$row['id'].">".$row['value']."</option>");
                    }
                    mysql_close();
            ?>
		</select></td>
	</tr>
	<tr>
		<td class="tdgeneral">Task instructions:</td>
		<td class="tdgeneral"><input type=text name=taskactfilter size=40> <select size="1" name="taskactfiltertype">
			<option selected value=ALL>Match all words</option>
			<option value=ANY>Match any word</option>
			<option value=EXACT>Match exact phrase</option>
		</select></td>
	</tr>
	<tr>
		<td class="tdgeneral">Task deliverables:</td>
		<td class="tdgeneral"><input type=text name=taskdelfilter size=40> <select size="1" name="taskdelfiltertype">
			<option selected value=ALL>Match all words</option>
			<option value=ANY>Match any word</option>
			<option value=EXACT>Match exact phrase</option>
		</select></td>
	</tr>
	<tr>
		<td class="tdgeneral">Status:</td>
		<td class="tdgeneral"><select size="1" name="statusfilter">
			<option selected value=ALL>All tasks</option>
			<option value=INC>All incomplete tasks</option>
        <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE yn = 'Y' ORDER BY sort";
            include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                            echo("<option value=".$row['pkey'].">".$row['value']."</option>");
            }
            mysql_close();
        ?>

		</select></td>
	</tr>
	<tr>
             <td class="tdgeneral" valign=top>Date created:</td>
		<td class="tdgeneral"><input type=radio name=taskadddatefilter value=ANY checked id=tadf1> <label for=tadf1>Any date</label><br />
            <input type=radio name=taskadddatefilter value=EXACT id=tadf2> <label for=tadf2>From </label>
            <input type=text id="datepickerFrom" name="datepickerFrom"   readonly="readonly"/>
            <label for=tadf2>&nbsp;to&nbsp;</label>
            <input type=text id="datepickerTo" name="datepickerTo"   readonly="readonly"/>
        </td>
	</tr>
	<tr>
		<td class="tdgeneral" valign=top>Deadline:</td>
		<td class="tdgeneral"><input type=radio name=taskdlfilter value=ANY checked id=tdl1> <label for=tdl1>Any date</label><br>
            <input type=radio name=taskdlfilter value=EXACT id=tdl2> <label for=tdl2>From </label>
            <input type=text id="datepickerDeadlineFrom" name="datepickerDeadlineFrom"   readonly="readonly"/>
            <!--<input type=text name=tdl1[] size=4 value=<?php //echo(date("Y")); ?> onclick="document.getElementById('tdl2').checked=true;">-->
            <label for=tdl2>&nbsp;to&nbsp;</label>
            <!--<input type=text name=tdl2[] size=2 value=<?php //echo(date("d")); ?> onclick="document.getElementById('tdl2').checked=true;">-->
            <input type=text id="datepickerDeadlineTo" name="datepickerDeadlineTo"   readonly="readonly"/>
            <!--<input type=text name=tdl2[] size=4 value=<?php //echo(date("Y")); ?> onclick="document.getElementById('tdl2').checked=true;">-->
        </td>
	</tr>
<?php
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = 'TA' ORDER BY udfisort";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
?>
	<tr>
		<td class="tdgeneral"><?php echo($row['udfivalue']); ?>:</td>
		<td class="tdgeneral">
        <?php
            switch($row['udfilist'])
            {
                case "Y":
                    echo("<select size=1 name=\"udf".$row['udfiid']."filter\">");
                    echo("<option selected value=ALL>All</option>");
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                    include("inc_db_con2.php");
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            echo("<option value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                        }
                    mysql_close($rs2);
                    echo("</select>");
                    break;
                default:
                    echo("<input type=text name=\"udf".$row['udfiid']."filter\">");
                    break;
            }
        ?>
        </td>
	</tr>
<?php
        }
    mysql_close();
?>
</table>
</div>
<h3 class=fc>3. Select the report output method:</h3>
<div style="margin-left: 17px">
<table border="0" id="table3" cellspacing="0" cellpadding="3" style="border-width: 0px;">
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="csvfile" value="N" checked id=t1></td>
		<td class="tdgeneral"><label for=t1>Onscreen display</label></td>
	</tr>
	<tr>
		<td class=tdgeneral align=center><input type="radio" name="csvfile" value="Y" id=t2></td>
		<td class=tdgeneral><label for=t2>Save to file (Microsoft Excel file)</label></td>
	</tr>
</table>
</div>
<h3 class=fc>4. Click the button:</h3>
<p style="margin-left: 17px"><input type="submit" value="Generate Report" name="B1">
<input type="reset" value="Reset" name="B2"></p>
</form>
<p>&nbsp;</p>


</body>

</html>
