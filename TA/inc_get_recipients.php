<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
if(isset($_POST) AND isset($_POST['recipients'])) {
    $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = 'TA'
                                ORDER BY t.tkname, t.tksurname";

    include("inc_db_con.php");
    $size = mysql_num_rows($rs);
    $size = ($size > 10) ? $size = 5 : $size;
    $personTasked ="<select  id='tasktkid' name='tasktkid[]' multiple='multiple' size='$size'>";
    while($row = mysql_fetch_array($rs)) {
        $personTasked .= "<option name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>";
    }
    mysql_close($con);
    $personTasked .= "</select>";
    echo $personTasked;
}else if(isset($_POST) AND isset($_POST['taskadduser'])) {
    $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = 'TA'
                                ORDER BY t.tkname, t.tksurname";

    include("inc_db_con.php");
    $size = mysql_num_rows($rs);
    $personTasked ="<select  id='taskadduser' name='taskadduser' ><option value='X'>---SELECT---</option>";
    while($row = mysql_fetch_array($rs)) {
        $personTasked .= "<option name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>";
    }
    mysql_close($con);
    $personTasked .= "</select>";
    echo $personTasked;

}else if(isset($_POST) AND isset($_POST['tasktopicid'])) {
    $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE yn = 'Y' ORDER BY value";
    include("inc_db_con.php");
    $tasktopic = "<select id='tasktopicid' name='tasktopicid'><option value='X'>---SELECT---</option>";
    while($row = mysql_fetch_array($rs)) {
        $tasktopic .= "<option value=".$row['id'].">".$row['value']."</option>";
    }
    $tasktopic .="</select>";
    echo $tasktopic;
    mysql_close($con);
}else if(isset($_POST) AND isset($_POST['taskurgencyid'])) {
    $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_urgency ORDER BY sort";
    include("inc_db_con.php");
    $taskstatusid = '<select id="taskurgencyid" name="taskurgencyid"><option value="X">---SELECT---</option>';
    while($row = mysql_fetch_array($rs)) {
        $taskstatusid .= "<option value=".$row['id'].">".$row['value']."</option>";
    }
    $taskstatusid .= "</select>";
    echo $taskstatusid;
    mysql_close();
}else if(isset($_POST) AND isset($_POST['taskstatusid'])) {
    $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE  yn = 'Y' ORDER BY sort";
    include("inc_db_con.php");
    $taskstatusid = "<select id='taskstatusid' name='taskstatusid'><option value='X'>---SELECT---</option>";
    while($row = mysql_fetch_array($rs)) {
        $taskstatusid .= "<option value='".$row['pkey']."'>".$row['value']."</option>";
    }
    echo $taskstatusid .= "</select>";
    mysql_close();
}else if(isset($_POST) AND isset($_POST['udf'])) {
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = 'TA' AND udfiid=".$_POST['udf']." ORDER BY udfisort";
    include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $udffield = "";
    switch($row['udfilist']) {
        case 'Y':
            $udffield .= "<select name='".$row['udfiid']."'><option value='X'>---SELECT---</option>";
            $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
            include("inc_db_con2.php");
            while($row2 = mysql_fetch_array($rs2)) {
                $udffield .= "<option value='".$row2['udfvid']."'>".$row2['udfvvalue']."</option>";
            }
            $udffield .=  "</select>";
            echo $udffield;
            mysql_close($con2);
            break;
        case 'T':
            $udffield .= "<input name='".$row['udfiid']."' size='50' type='text' />";
            echo $udffield;
            break;
        case 'M':
            $udffield .= "<input name=".$row['udfiid']." size='50' type='text' />";
            echo $udffield;
            break;
    }
    mysql_close($con);
}
?>