<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
//save a task edit
if(isset($_POST) && isset($_POST['btnSave'])) {   
    $taskid = $_POST['taskid'];

    //this is just to maintain the code in the form it is...
    $tref = "TA";
    $udfFields = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs)) {
        $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
        include("inc_db_con2.php");
        $udf = mysql_num_rows($rs2);
        $row2 = mysql_fetch_array($rs2);
        mysql_close($con2);
        $udfFields[$row['udfivalue']] = '';
        switch($row['udfilist']) {
            case "Y":
                if(checkIntRef($row2['udfvalue'])) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                    include("inc_db_con2.php");
                    if(mysql_num_rows($rs2)>0) {
                        $row3 = mysql_fetch_array($rs2);
                    }
                    mysql_close($con2);
                }
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2)) {
                    if($row3['udfvid'] == $row2['udfvid'])
                        $udfFields[$row['udfivalue']] = $row2['udfvvalue'];
                }
                mysql_close($con2);
                break;
            case "T":
                $udfFields[$row['udfivalue']] = $row2['udfvalue'];
                break;
            case "M":
                $udfFields[$row['udfivalue']] = $row2['udfvalue'];
                break;
            default:
                break;
        }
    }//crazy code ends here..

    //get the original task record before editing.
    $sql = "SELECT * FROM assist_".$cmpcode."_ta_task WHERE taskid=$taskid";
    include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $taskstate = $row['taskstate'];
    if($_POST['taskstatusid'] == 4) {
        $taskstate = 0;
    }else if($_POST['taskstatusid'] == 1) {
        $taskstate = 100;
    }
    /*
     * If the Task Status is changed from “On-going�? to something else the taskstate field must be set to 0.
    */
    if($row['taskstatusid'] == 5 && $_POST['taskstatusid'] != 5) {
        $taskstate = 0;
    }
    /*
     * If the Task status is changed from “Complete�? to any other status then the taskstate field must be returned to the previous state
     * (as per the most recent _log record before the one that closed the task)
    */
    $statusid = $_POST['taskstatusid'];
    if($row['taskstatusid'] == 1 && $_POST['taskstatusid'] != 1) {
        $sql = "SELECT * FROM assist_" .$cmpcode."_ta_log WHERE logtaskid=$taskid ORDER BY logdate DESC LIMIT 2";
        include("inc_db_con.php");
        mysql_data_seek($rs, 1);
        $rowR = mysql_fetch_array($rs);
        $taskstate = $rowR['logstate'];
    }
    $taskdeadline = strtotime($_POST['datepicker']);
    $taskdeliver = addslashes($_POST['taskdeliver']);
    $taskaction = addslashes($_POST['taskaction']);
    $sql = "UPDATE assist_".$cmpcode."_ta_task SET tasktkid='',tasktopicid=".$_POST['tasktopicid'].",
        taskurgencyid=".$_POST['taskurgencyid'].",taskstatusid=".$_POST['taskstatusid'].",taskdeadline='".$taskdeadline."',
          taskaction='".$taskaction."',taskdeliver='".$taskdeliver."',taskstate=$taskstate
              WHERE taskid=".$_POST['taskid'];
    include("inc_db_con.php");
    /*updating recipients
     * first get the original assignees into aarray,then delete them from the recipients table,
     * insert the new recipients
    */
    //get the old assignees to a task,this will be used later when updating somewhere below.

    $oldRecipients = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_ta_task_recipients WHERE taskid=".$_POST['taskid'];
    include("inc_db_con.php");
    while($rowO = mysql_fetch_array($rs)) {
        array_push($oldRecipients,$rowO['tasktkid']);
    }

    //get the new assignees to the task,this will be used later when updating somewhere below.
    $newRecipients = array();
    if(is_array($_POST['tasktkid'])) {
        $newRecipients = $_POST['tasktkid'];
    }else {
        $newRecipients[] = $_POST['tasktkid'];
    }
    //remove the old recipients to this task.
    $sql = "DELETE FROM assist_".$cmpcode."_ta_task_recipients WHERE taskid=".$_POST['taskid'];
    include("inc_db_con.php");

    //insert the new recipients for this task
    $sql = "INSERT INTO assist_".$cmpcode."_ta_task_recipients (taskid,tasktkid) VALUES ";
    foreach($newRecipients as $value) {
        $sql .= "($taskid,'$value'),";
    }
    $sql = substr($sql, 0,-1);
    include("inc_db_con.php");

    $taskaction = $_POST['taskaction'];
    $taskdeliver = $_POST['taskdeliver'];
    $logdate = $today;
    $logtkid = $tkid;
    $updator = getTK($tkid, $cmpcode, 'tkn');

    $updates = "";
    $usersChanges ="";
    $logupdate = "The task has been edited by <i>$updator</i>.<br/>";
    /*construct logupdate bits for the changes in recipients
      * making use of @newRecipients array and @oldRecipients array
    */

    $removedUsers = array_diff($oldRecipients,$newRecipients);
    if(!empty($removedUsers)) {
        $usersChanges .= "The following user(s) were removed from working on task id ".$taskid."<br />";
        foreach($removedUsers as $key => $tasktkid) {
            $usersChanges .= "<i>".getTK($tasktkid, $cmpcode, 'tkn')."</i><br />";
        }
    }
    $newUsers = array_diff($newRecipients,$oldRecipients);
    if(!empty($newUsers)) {
        $usersChanges .= "The following user(s) were added to work on task id ".$taskid."<br />";
        foreach($newUsers as $key => $tasktkid) {
            $usersChanges .= "<i>".getTK($tasktkid, $cmpcode, 'tkn')."</i><br />";
        }
    }
    //send emails to all affected users
    $toEmails = array();
    foreach ($newRecipients as $key => $value) {
        if(isset($_POST['sendEmail'])) {
            array_push($toEmails,$value);
        }else {
            if($tkid != $value) {//if the logged in user is one of the recipients, do not send email to that user
                array_push($toEmails,$value);
            }
        }
    }
    foreach ($oldRecipients as $key => $value) {
        if(isset($_POST['sendEmail'])) {
            array_push($toEmails,$value);
        }else {
            if($tkid != $value) {//if the logged in user is one of the recipients, do not send email to that user
                array_push($toEmails,$value);
            }
        }
    }
    if(strtolower($cmpcode) != 'wcc0001' && !empty($toEmails)) {
        $sql = "SELECT tkemail FROM assist_".$cmpcode."_timekeep t WHERE t.tkid=";
        for ($i = 0; $i < count($toEmails);$i++) {
            if($i < count($toEmails) - 1)
                $sql .= "'".$toEmails[$i]."' OR t.tkid=";
            else
                $sql .= "'".$toEmails[$i]."'";
        }
        include("inc_db_con.php");
        $strTo = "";
        while($rowRusers = mysql_fetch_array($rs)) {
            $strTo .= $rowRusers['tkemail'].",";
        }
        $strToEmails = substr($strTo,0,-1);

        $sql = "SELECT * FROM assist_".$cmpcode."_timekeep t WHERE t.tkid=".$row['taskadduser'];
        include("inc_db_con.php");
        $rowadduser = mysql_fetch_array($rs);
        $from = $rowadduser['tkemail'];
        $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_urgency u WHERE id=".$row['taskurgencyid'];
        include("inc_db_con.php");
        $rowurg = mysql_fetch_array($rs);

        $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic t WHERE id=".$row['taskurgencyid'];
        include("inc_db_con.php");
        $rowtopic = mysql_fetch_array($rs);
        $userFrom = $rowadduser['tkname']." ".$rowadduser['tksurname'];
        $subject = "Task with id ".$taskid." on Ignite Assist edited.";


        $message .= $rowadduser['tkname']." ".$rowadduser['tksurname']." has made some changes on task with id ".$taskid.":\n";
        $message .= "Prority: ".$rowurg['value']."\n";
        $message .= "Topic: ".$rowtopic['value']."\n";
        $message .= "Task Instructions: ".str_replace(chr(10),"\n",$taskaction)."\n";
        $message .= "Task Deliverables: ".str_replace(chr(10),"\n",(empty($taskdeliver) ? "N/A" : $taskdeliver))."\n";
        $message .= "Deadline: ".date("d F Y",$taskdeadline)."\n";
        $message .= "Please log onto Ignite Assist in order to update this task.\n";
        //Send email
        //echo "from: ".$from."<br />";
        $headers = 'MIME-Version:1.0'."\r\n";
        $headers .= 'From: '.$userFrom.' <'.$from.'>'."\r\n";
        $headers .= 'Reply-to: '.$userFrom.' <'.$from.'>'."\r\n";
//        $headers .= 'Content-Type: text/html; charset=iso-8859-1'."\r\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();
/*        //echo $headers."<br />";
        ini_set("SMTP","smtp.saix.net");
        ini_set("sendmail_from",$from);*/
//$header = "From: ".$userFrom." <".$from.">\r\nReply-to: ".$from."\r\n";
        mail($strToEmails,$subject,$message,$headers);
        // echo ( mail($strToEmails,$subject,$message,$headers) ? "success" : "failed");

    }
    if($row['tasktopicid'] != $_POST['tasktopicid']) {
        $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE id=".$row['tasktopicid'];
        include("inc_db_con.php");
        $row0 = mysql_fetch_array($rs);
        $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE id=".$_POST['tasktopicid'];
        include("inc_db_con.php");
        $row1 = mysql_fetch_array($rs);
        $updates .= "<br />Task Topic changed from <i>".$row0['value']." </i>to <i>".$row1['value']."</i>";
    }
    if($row['taskurgencyid'] != $_POST['taskurgencyid']) {
        $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_urgency WHERE id=".$row['taskurgencyid'];
        include("inc_db_con.php");
        $row0 = mysql_fetch_array($rs);
        $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_urgency WHERE id=".$_POST['taskurgencyid'];
        include("inc_db_con.php");
        $row1 = mysql_fetch_array($rs);
        $updates .= "<br />Task Priority changed from <i>".$row0['value']."</i> to <i>".$row1['value']."</i>";
    }
    if($row['taskaction'] != $taskaction) {
        if($row['taskaction'])
            $updates .= "<br />Task instructions changed from  <i> ".$row['taskaction']." </i>  to <i>$taskaction</i>";
        else
            $updates .= "<br />Task instructions changed to <i>$taskaction</i>";
    }
    if($row['taskdeliver'] != $taskdeliver) {
        if($row['taskdeliver'])
            $updates .= "<br />Task deliverables changed from  <i> ".$row['taskdeliver']." </i>  to <i>$taskdeliver</i>";
        else
            $updates .= "<br />Task deliverables changed to <i>$taskdeliver</i>";
    }

    if($row['taskstatusid'] != $_POST['taskstatusid']) {
        $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE pkey=".$row['taskstatusid'];
        include("inc_db_con.php");
        $row0 = mysql_fetch_array($rs);
        $taskstate0 = $row0['state'];
        $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE pkey=".$_POST['taskstatusid'];
        include("inc_db_con.php");
        $row1 = mysql_fetch_array($rs);
        $taskstate1 = $row0['state'];
        $updates .= "<br />Task status has been changed from <i>".$row0['value']."</i> to <i>".$row1['value']."</i>";
        //$updates .= "<br />Task state changed from $taskstate0 to $taskstate1";
    }
    if($row['taskdeadline'] != $taskdeadline) {
        $updates .= "<br />Task deadline changed from ".date("d-M-Y",$row['taskdeadline'])." to ".date("d-M-Y",$taskdeadline);
    }

    //uploads
    $original_filename = "";
    $system_filename = "";
    $attachments = "";
    if(isset($_FILES)) {
        $folder = "../files/$cmpcode/TA";
        /*if(!is_dir($folder)){
            mkdir($folder, 0777, true);
        }*/
        checkFolder("TA");
        foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
            if($_FILES['attachments']['error'][$key] == 0) {
                $original_filename = $_FILES['attachments']['name'][$key];
                $ext = substr($original_filename, strrpos($original_filename, '.') + 1);
                $parts = explode(".", $original_filename);
                $file = $parts[0];
                $system_filename = $taskid."_".$file."_".date("YmdHi").".$ext";
                $full_path = $folder."/".$system_filename;
                move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
                $sql = "INSERT INTO assist_".$cmpcode."_ta_task_attachments (taskid,logid,original_filename,system_filename) VALUES ($taskid,0,'$original_filename','$system_filename')";
                include("inc_db_con.php");
                $filename = "../files/$cmpcode/TA/".$system_filename;
                $attachments .= "<a href='javascript:download(\"$filename\")'>".$original_filename."</a>"."<br />";
            }
        }
    }
    if($attachments != "") {
        $updates .= "<br />Task Attachment(s):<br /> $attachments";
    }
    if($usersChanges != "") {
        $updates = $usersChanges.$updates;
    }
    if($updates != "") {
        $logupdate .= " The following changes were made: <br />$updates";
    }

    $logupdate = addslashes(htmlentities($logupdate));
    //echo $updates;die;
    $logstatusid = $_POST['taskstatusid'];
    $logstate = $taskstate;
    $logemail = 'N';
    $logsubmittkid = $tkid;
    $logtaskid = $_POST['taskid'];
    $logtasktkid = '';
    $sql = "INSERT INTO assist_".$cmpcode."_ta_log
            (logdate,logtkid,logupdate,logstatusid,logstate,logemail,logsubmittkid,logtaskid,logtasktkid) VALUES
            ($logdate,$logtkid,'$logupdate',$logstatusid,$logstate,'$logemail','$logsubmittkid',$logtaskid,'$logtasktkid')";
    include("inc_db_con.php");
    $logid = mysql_insert_id();

    //&*!@!12891edw&*7821
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
    include("inc_db_con.php");
    $udf = mysql_num_rows($rs);
    if($udf>0) {
        $u = 0;
        while($row = mysql_fetch_array($rs)) {
            $udfindex[$u] = $row['udfiid'];
            $u++;
        }
    }

    mysql_close();

    //GET UDF DATA
    if($udf>0) {
        foreach($udfindex as $udfi) {
            $udfval[$udfi] = str_replace("'","&339",$_POST[$udfi]);
        }
    }
    //$udfidArr = array();
    foreach($_POST as $key => $value) {
        if(is_int($key)) {
            if($key == -1) {
                $sql = "INSERT INTO assist_".$cmpcode."_udf SET udfindex=5,udfvalue='".$_POST[$key]."', udfnum=".$_POST['taskid'].",udfref='".$tref."'";
                include("inc_db_con.php");
            }else {
                $sql = "UPDATE assist_".$cmpcode."_udf SET";
                $sql.= " udfvalue = '".$value."' WHERE udfid=$key";
                include("inc_db_con.php");
            }
        }
    }
    $afterUdfFields = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs)) {
        $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
        include("inc_db_con2.php");
        $udf = mysql_num_rows($rs2);
        $row2 = mysql_fetch_array($rs2);
        mysql_close($con2);
        $afterUdfFields[$row['udfivalue']] = '';
        switch($row['udfilist']) {
            case "Y":
                if(checkIntRef($row2['udfvalue'])) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                    include("inc_db_con2.php");
                    if(mysql_num_rows($rs2)>0) {
                        $row3 = mysql_fetch_array($rs2);
                        //echo($row3['udfvvalue']);
                    }
                    mysql_close($con2);
                }
                //echo("<select name=".($row2['udfid'] == "" ? "-1" : $row2['udfid'])."><option value=X>---SELECT---</option>");
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                include("inc_db_con2.php");
                while($row2 = mysql_fetch_array($rs2)) {

                    //echo("<option ".($row3['udfvid'] == $row2['udfvid'] ? "selected=selected" : "")." value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                    if($row3['udfvid'] == $row2['udfvid'])
                        $afterUdfFields[$row['udfivalue']] = $row2['udfvvalue'];
                }
                mysql_close($con2);
                //echo("</select>");
                break;
            case "T":
            //echo("<input type=text name=".$row2['udfid']." value='".$row2['udfvalue']."' size=50>");
                $afterUdfFields[$row['udfivalue']] = $row2['udfvalue'];
                break;
            case "M":
            //echo("<textarea name=".$row2['udfid']." rows=5 cols=40 >".$row2['udfvalue']."</textarea>");
                $afterUdfFields[$row['udfivalue']] = $row2['udfvalue'];
                break;
            default:
            // echo(str_replace(chr(10),"<br>",$row2['udfvalue']));
                break;
        }
    }
    $logupdate = "";
    foreach($udfFields as $key => $value) {
        if($value != $afterUdfFields[$key])
            $logupdate .= $key." changed ".($value != null ? "from " : "" )."<i>".$value."</i> to <i>".$afterUdfFields[$key]."</i><br />";
    }
    $sql = "UPDATE assist_".$cmpcode."_ta_log SET logupdate=CONCAT_WS('<br />',logupdate,'$logupdate') WHERE logid=$logid";
    //echo $sql;die;
    include("inc_db_con.php");
    $referral_page = $_POST['referal_page'];
    echo "<script type='text/javascript'>document.location.href='$referral_page.php?i='+$taskid</script>";
}

//GET USER ACCESS PERMISSIONS
$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_access WHERE tkid = '".$tkid."'";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$tact = $row['act'];
mysql_close();


//GET TASK ID
$taskid = $_GET['taskid'];


//SET REMINDER
/*$tremind = $_GET['remind'];
if(strlen($tremind) > 0)
{
    $tr = substr($tremind,1,strlen($tremind)-1);
    $sql = "UPDATE assist_".$cmpcode."_ta_task SET taskremind = '".$tr."' WHERE taskid = ".$taskid;
    include("inc_db_con.php");
        $tsql = $sql;
        $tref = "TA";
        $trans = "Set reminder ".$tr." for task ".$taskid.".";
        include("inc_transaction_log.php");
}
*/
//GET TASK DETAILS INTO ARRAY TASK
$sql = "SELECT * FROM assist_".$cmpcode."_ta_task WHERE taskid = ".$taskid;
include("inc_db_con.php");
$task = mysql_fetch_array($rs);
mysql_close();

?>
<html>

    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
        <link href="../lib/calendar/css/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css"/>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-1.4.2.min.js"></script>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-ui-1.8.1.custom.min.js"></script>

        <script type ="text/javascript">

            $(function() {
                $("#datepicker").datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true		});
            });
            $(document).ready(function(){
                $('#attachlink').click(function(){
                    $('<tr><td align="right" colspan="2" ><input type="file" name=attachments[] size="30"/></td>').insertAfter('#firstdoc');
                }),
                $('#attachlinkWS').click(function(){
                    $('<tr><td align="right" colspan="2" ><input type="file" name=attachments[] size="30"/></td>').insertAfter('#firstdocWS');
                });
            });
        </script>
        <script type='text/javascript'>
            function Validate(tform){

                var form = document.forms['edittask'];
                var count = 0;
                var message = '';
                for(var i = 0; i < form.tasktkid.length;i++){
                    if(form.tasktkid.options[i].selected){
                        count++;
                    }
                }
                if(count == 0){

                    var tact = parseInt(form.tact.value);
                    if(isNaN(tact)){
                        message += "Please assign this task to at least 1 user.\n";
                    }
                }
                if(tform.taskurgencyid.value == "X"){
                    message += "Please select the task priority.\n";
                }
                if(tform.taskstatusid.value == "X"){
                    message += "Please select task status.\n";
                }
                if(tform.datepicker.value == ""){
                    message += "Please select task deadline date.\n";
                }
                if(tform.tasktopicid.value == "X"){
                    message += "Please select task topic.\n";
                }
                if(tform.taskaction.value == ""){
                    message += "Please enter task details.\n"
                }
                if(message != '')
                {
                    alert(message);
                    return false;
                }
                return true;
            }
            function validateUpdates(tform) {
                var logupdate = tform.logupdate.value;
                var logstatusid = tform.logstatusid.value;
                var logstate = tform.logstate.value;
                if(logupdate.length == 0)
                {
                    alert("Please complete all the fields.");
                    return false;
                }
                else
                {
                    if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                    {
                        //alert("error in comment");
                        logupdate = logupdate.replace(/select/i,"choose");
                        tform.logupdate.value = logupdate;
                    }
                    if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                    {
                        alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return false;
            }

            function delAttachment(taskid,attachment_id){
                var answer = confirm('Are you sure you wish to completely delete this file? Click Ok to proceed otherwise cancel.');
                if(answer){
                    document.location.href = "delete_attachment.php?taskid="+taskid+"&attachment_id="+attachment_id;
                }
                return false;
            }
            function delUpdateAttachment(id,attachment_id){
                var answer = confirm('Are you sure you wish to completely delete this file? Click Ok to proceed otherwise cancel.');
                if(answer){
                    document.location.href = "delete_update_attachment.php?logid="+id+"&attachment_id="+attachment_id+"&script=1";
                }
                return false;
            }
            function download(path){
                document.location.href = "download.php?path="+path;
            }
            function statusPerc() {
                var stat = document.taskupdate.logstatusid.value;
                stat = parseInt(stat);
                if(stat == 3 || stat > 5)
                {
                    document.taskupdate.logstate.style.backgroundColor="#FFFFFF";
                    document.taskupdate.logstate.disabled = false;
                    if(stat > 5 && lstat[stat] > 0)
                    {
                        document.taskupdate.logstate.value = lstat[stat];
                    }
                    else
                    {
                        document.taskupdate.logstate.value = lstat[0];
                    }
                }
                else
                {
                    document.taskupdate.logstate.value = "";
                    document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                    document.taskupdate.logstate.disabled = true;
                }
            }
            function cnTask(i) {
                if(confirm("Are you sure you want to cancel this task?\n\nIf yes, please press OK,\nelse press Cancel to stop this action.")==true)
                {
                    var url = "view_own_update_cancel.php?i="+i;
                    document.location.href = url;
                }
            }
        </script>


    </head>

    <link rel="stylesheet" href="/default.css" type="text/css">
    <?php include("inc_style.php"); ?>
    <base target="main">
    <body topmargin='0' leftmargin='5' bottommargin='0' rightmargin='5'>
        <h1 class=fc><b>Task Assist: Update Task</b></h1>
        <h2 class=fc>Task Status</h2>
        <form method="post" action="edit_task.php" id="edittask" name="edittask" enctype="multipart/form-data" onsubmit="return Validate(this)">
            <table border="1" id="table1" cellspacing="0" cellpadding="4" width=600>

                <tr>
                    <td class=tdheaderl valign="top" width=140><b>Assigned to:</b></td>
                    <td class=tdgeneral valign="top" width=460>
                        <?php //echo($tkname); ?>
                        <?php

                        if($tact == 20) //IF USER ACCESS = 20 THEN ASSIGN TASKS TO SELF ONLY
                        {
                            echo($tkname."<input type=hidden size=5 name=tasktkid value=".$tkid.">");
                            echo("<input type='hidden' name='tact' id='tact' value='$tact' />");
                        }
                        else //IF USER ACCESS > 20 THEN ASSIGN TASKS TO OTHERS
                        {
                            ?>
                            <?php
                            $recipients = array ();
                            $sql2 = "SELECT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name FROM assist_".$cmpcode."_timekeep t
                                        INNER JOIN assist_".$cmpcode."_ta_task_recipients r ON (t.tkid=r.tasktkid) WHERE taskid=".$task['taskid'];
                            include("inc_db_con2.php");
                            while($rowW = mysql_fetch_array($rs2)) {
                                array_push($recipients,$rowW['tkid']);
                            }
                            mysql_close($con2);
                            //$sql = "SELECT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a WHERE t.tkid = a.tkid AND t.tkid <> '0000' ORDER BY t.tkname, t.tksurname";
                            $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = 'TA'
                                ORDER BY t.tkname, t.tksurname";
                            include("inc_db_con.php");
                            $size = mysql_num_rows($rs);
							$size = $size > 10 ? $size = 5 : $size = $size;
                            ?>
                        <i>Ctrl + left click to select multiple users</i><br />
                        <select  id="tasktkid" name="tasktkid[]" multiple="multiple" size="<?php echo($size);?>">
                                <?php
                                /*$sql = "SELECT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                                            FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a
                                            WHERE t.tkid = a.tkid AND t.tkid <> '0000' ORDER BY t.tkname, t.tksurname";*/
                                $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = 'TA'
                                ORDER BY t.tkname, t.tksurname";
                                include("inc_db_con.php");
                                while($row = mysql_fetch_array($rs)) {
                                    echo("<option  ".(in_array($row['tkid'],$recipients) ? "selected=selected" : "")." value=".$row['tkid'].">".$row['name']."</option>");
                                }
                                mysql_close();
                                ?>
                        </select>
                            <?php
                        }
                        ?>
                    </td>
                </tr>

                <tr>
                    <td class=tdheaderl valign="top"><b>Assigned by:</b></td>
                    <td class=tdgeneral valign="top">

                        <?php
                        $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$task['taskadduser']."'";
                        include("inc_db_con.php");
                        $row = mysql_fetch_array($rs);
                        echo($row['tkname']." ".$row['tksurname']);
                        mysql_close();
                        ?>


                    </td>
                </tr>
                <tr>
                    <td class=tdheaderl valign="top"><b>Topic:</td>
                    <td class=tdgeneral valign="top">
                        <select size="1" name="tasktopicid">
                            <option value=X>--- SELECT ---</option>
                            <?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE yn = 'Y' ORDER BY value";
                            include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs)) {
                                echo("<option ".($row['id'] == $task['tasktopicid'] ? "selected=selected" : "")." value=".$row['id'].">".$row['value']."</option>");
                            }
                            mysql_close();
                            ?>
                        </select>
                        <?php
                        /*$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE id = '".$task['tasktopicid']."'";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                echo($row['value']);
                $topicval = $row['value'];
                $topicyn = $row['yn'];
            mysql_close();*/

                        ?></td>
                </tr>

                <tr>
                    <td class='tdheaderl' valign="top"><b>Priority:</b></td>
                    <td class='tdgeneral' valign="top">
                        <select size="1" name="taskurgencyid">
                            <option value='X'>--- SELECT ---</option>
                            <?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_urgency ORDER BY sort";
                            include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs)) {
                                echo("<option ".($row['id'] == $task['taskurgencyid'] ? "selected=selected" : 2)."  value=".$row['id'].">".$row['value']."</option>");
                            }
                            mysql_close();
                            ?>
                        </select>
                        <?php
                        /*$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_urgency WHERE id = '".$task['taskurgencyid']."'";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                echo($row['value']);
            mysql_close();*/
                        ?>

                    </td>
                </tr>

                <tr>
                    <td class=tdheaderl valign="top"><b>Status:</td>
                    <td class=tdgeneral valign="top">
                        <select size="1" name="taskstatusid">
                            <option value=X>--- SELECT ---</option>
                            <?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE  yn = 'Y' ORDER BY sort";
                            include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs)) {
                                if($row['pkey'] == "4") {
                                    echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                }
                                else {
                                    echo("<option ".($row['pkey'] == $task['taskstatusid'] ? "selected=selected" : "")." value=".$row['pkey'].">".$row['value']."</option>");
                                }
                            }
                            mysql_close();
                            ?>
                        </select>
                        <?php
                        /*$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE pkey = '".$task['taskstatusid']."'";
            include("inc_db_con.php");
                $row = mysql_fetch_array($rs);
                echo($row['value']);
                if($task['taskstatusid'] == "3")
                {
                    echo(" (".$task['taskstate']."%) ");
                }
                else
                {
                    if($task['taskstatusid'] != "5")
                    {
                        echo(" (".$row['state']."%) ");
                    }
                }
            mysql_close();*/
                        ?></td>
                </tr>
                <tr>
                    <td class=tdheaderl valign="top"><b>Deadline:</td>
                    <td class=tdgeneral valign="top">
                        <input type="text" value="<?php echo(date("d-M-Y",$task['taskdeadline']));?>" size="20" id="datepicker" name="datepicker"/>
                        <?php //echo(date("d-M-Y",$task['taskdeadline']));?>
                    </td>
                </tr>
                <tr>
                    <td class=tdheaderl valign="top"><b>Task Instructions:</b></td>
                    <td class=tdgeneral>
                        <?php
//                        $action = explode(chr(10),$task['taskaction']);
//                        $taskaction = implode("<br>",$action);
//echo($taskaction);
$taskaction = $task['taskaction'];
                        ?>
                        <textarea rows="5" name="taskaction" cols="35"><?php echo($taskaction);?></textarea>
                    </td>
                </tr>
                <tr>
                    <td class=tdheaderl valign="top"><b>Task Deliverables:</b></td>
                    <td class=tdgeneral>
                        <?php
                        $deliver = explode(chr(10),$task['taskdeliver']);
                        $taskdeliver = implode("<br>",$deliver);
$taskdeliver = $task['taskdeliver'];
                        ?>
                        <textarea rows="5" name="taskdeliver" cols="35"><?php echo($taskdeliver);?></textarea>
                    </td>
                </tr>


                <?php
                $topicval = "";
                if($topicval == 'Travel Requests' && $topicyn = 'N') {
                    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = 'TATVL' AND udfivalue = 'Travel Request'";
                    include("inc_db_con.php");
                    $udfi = mysql_fetch_array($rs);
                    mysql_close();
                    if(strlen($udfi['udfiid'])>0) {
                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$udfi['udfiid'];
                        include("inc_db_con2.php");
                        $udf = mysql_num_rows($rs2);
                        $row2 = mysql_fetch_array($rs2);
                        mysql_close($con2);
                        ?>
                <tr>
                    <td class=tdheaderl valign="top"><b>Travel Request Ref.:</b></td>
                    <td class=tdgeneral valign="top">
                                <?php
                                if($udf>0) {
                                    echo("<a href=../TVLA/view_my.php?ref=".$row2['udfvalue']." target=_blank>".$row2['udfvalue']."</a>");
                                }
                                else {
                                    echo("N/A");
                                }
                                ?>
                        &nbsp;</td>
                </tr>
                        <?php
                    }
                }
                $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
                include("inc_db_con.php");
                while($row = mysql_fetch_array($rs)) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
                    include("inc_db_con2.php");
                    $udf = mysql_num_rows($rs2);
                    $row2 = mysql_fetch_array($rs2);
                    mysql_close($con2);
                    ?>
                <tr>
                    <td class=tdheaderl valign="top"><b><?php echo($row['udfivalue']); ?>:</b></td>
                    <td class=tdgeneral valign="top">
                            <?php


                            switch($row['udfilist']) {
                                case "Y":
                                    if(checkIntRef($row2['udfvalue'])) {
                                        $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                        include("inc_db_con2.php");
                                        if(mysql_num_rows($rs2)>0) {
                                            $row3 = mysql_fetch_array($rs2);
                                            //echo($row3['udfvvalue']);
                                        }
                                        mysql_close($con2);
                                    }
                                    echo("<select name=".($row2['udfid'] == "" ? "-1" : $row2['udfid'])."><option value=X>---SELECT---</option>");
                                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
                                    include("inc_db_con2.php");
                                    while($row2 = mysql_fetch_array($rs2)) {
                                        echo("<option ".($row3['udfvid'] == $row2['udfvid'] ? "selected=selected" : "")." value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                                    }
                                    mysql_close($con2);
                                    echo("</select>");
                                    break;
                                case "T":
                                    echo("<input type=text name=".$row2['udfid']." value='".$row2['udfvalue']."' size=50>");
                                    break;
                                case "M":
                                    echo("<textarea name=".$row2['udfid']." rows=5 cols=40 >".$row2['udfvalue']."</textarea>");
                                    break;
                                default:
                                //echo(str_replace(chr(10),"<br>",$row2['udfvalue']));
                                    break;
                            }


                            ?>
                        &nbsp;</td>
                </tr>


                    <?php
                }



                ?>
                <?php
                $sql = "SELECT * FROM  assist_".$cmpcode."_ta_task_attachments AS att WHERE att.taskid = ".$taskid;
                include("inc_db_con.php");
                ?>
                <tr>
                    <td class=tdheaderl valign="top"><b>Attachment(s):</b></td>
                    <td class=tdgeneral>
                        <?php
                        if(!mysql_num_rows($rs)) {
                            echo ("<i>No attachments found.</i>");
                        }else {
                            echo "<table style='border:none;'>";
                        }

                        while($row = mysql_fetch_assoc($rs)) {

                            $file = "../files/".$cmpcode."/TA/".$row['system_filename'];
                            if(!file_exists($file))
                                continue;
                            $taskattach_id = $row['id'];

                            echo("<tr>
                       <td  style='border:none;'>
                            <a href='javascript:download(\"$file\")'>".$row['original_filename']."</a>
                       </td>
                       <td style='border:none;'>
                            <input type='button' name='deleteAttachment' id='deleteAttachment' value='Delete' onclick='delAttachment(\"$taskid\",\"$taskattach_id\")' />
                       </td>
                       </tr>");
                        }
                        if(mysql_num_rows($rs) > 0) {
                            echo("</table>");
                        }
                        ?>

                    </td>
                </tr>
                <tr id="firstdoc">
                    <td class=tdgeneral colspan="2" align="right"><input type="file" name="attachments[]" id="attachment" size="30"/></td>
                </tr>
                <tr>
                    <td align="right" colspan="2"><a href="javascript:void(0)" id="attachlink">Attach another file</a></td>
                </tr>
                <?php
                if(in_array($tkid,$recipients)) {?>
                    <tr><td class="tdheaderl">Send Email:</td><td class="tdgeneral"><input type="checkbox" name="sendEmail" id="sendEmail" value="1"/> </td></tr>
                <?php }?>


                <tr>
                    <td colspan="2">
                        <?php
                        $paths = pathinfo(getenv('HTTP_REFERER'));
                        ?>
                        <input type="hidden" name="taskid" id="taskid" value="<?php echo($taskid);?>" />
                        <input type="hidden" name="referal_page" id="referal_page" value="<?php echo($paths['filename']);?>" />
                        <input type="button" value="Delete Task" onclick="cnTask(<?php echo($taskid); ?>)" />
                        <!--<select name="redirect_option" id="redirect_option">
                            <option value="">Update and go to next task</option>
                            <option value="">Update and go to previous task</option>
                            <option value="">Update and go back to oldstatus list</option>
                            <option value="">Update and go to newstatus list</option>
                        </select>-->
                        <input value="Save" name="btnSave" id="btnSave" type="submit" />
                    </td>
                </tr>
            </table>
        </form>
        <h2 class=fc>Task Updates</h2>
        <form enctype="multipart/form-data" name=taskupdate method=post action=view_task_update_process.php onsubmit="return validateUpdates(this);" language=jscript>
            <input type=hidden name=taskid value=<?php echo($taskid);?>>
            <table border="1" id="table1" cellspacing="0" cellpadding="5" width=600>
                <tr>
                    <td valign="top" class=tdheader>Date</td>
                    <td valign="top" class=tdheader>Task Update</td>
                    <td valign="top" class=tdheader>Task Status</td>
                </tr>
                <?php
//IF THE TASK IS NOT CLOSED OFFER THE OPTION TO UPDATE
                if($task['taskstatusid'] != "1") {
                    ?>
                <tr>
                    <td valign="top" class=tdgeneral align=center><?php echo(date("d-M-Y H:i",$today)); ?></td>
                    <td valign="top" class=tdgeneral><textarea rows="5" name="logupdate" cols="35"></textarea></td>
                    <td valign="top" class=tdgeneral>
                        <select size="1" name="logstatusid" onchange=statusPerc()>
                                <?php
                                $lstat = "lstat[0] = ".$task['taskstate'].";";
                                $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE yn = 'Y' AND pkey NOT IN (2,4) ORDER BY sort";
                                include("inc_db_con.php");
                                while($row = mysql_fetch_array($rs)) {
                                    if( ($row['pkey']==3 && $task['taskstatusid']==4 ) || ($task['taskstatusid']==$row['pkey'])) {
                                        echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                    } else {
                                        echo("<option  value=".$row['pkey'].">".$row['value']."</option>");
                                    }
                                    //echo("<option ".($task['taskstatusid'] == $row['pkey'] ? "selected=selected" : "")." value=".$row['pkey'].">".$row['value']."</option>");
                                    $lstat = $lstat.chr(10)."lstat[".$row['pkey']."] = ".$row['state'].";";
                                }
                                mysql_close();
                                /*
                                $lstat = "lstat[0] = ".$task['taskstate'].";";
                                $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE yn = 'Y' AND state > -100 ORDER BY sort";
                                include("inc_db_con.php");
                                while($row = mysql_fetch_array($rs)) {
                                    if($task['taskstatusid'] != "5") {
                                        if($row['pkey'] == "3") {
                                            echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                            $lstat = $lstat.chr(10)."lstat[".$row['pkey']."] = ".$row['state'].";";
                                        }
                                        else {
                                            if($row['pkey'] != "4") {
                                                echo("<option value=".$row['pkey'].">".$row['value']."</option>");
                                                $lstat = $lstat.chr(10)."lstat[".$row['pkey']."] = ".$row['state'].";";
                                            }
                                        }
                                    }
                                    else {
                                        if($row['pkey'] == "5") {
                                            echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                            $lstat = $lstat.chr(10)."lstat[".$row['pkey']."] = ".$row['state'].";";
                                        }
                                        else {
                                            if($row['pkey'] != "4") {
                                                echo("<option value=".$row['pkey'].">".$row['value']."</option>");
                                                $lstat = $lstat.chr(10)."lstat[".$row['pkey']."] = ".$row['state'].";";
                                            }
                                        }
                                    }
                                }
                                mysql_close();
                                 * */

                                ?>
                        </select><br>&nbsp;
                        <script language=JavaScript>
                            var lstat = new Array();
    <?php echo($lstat); ?>
                        </script>
                        <br>
                        <input type="text" name="logstate" size="4" value=<?php echo($task['taskstate']); ?>>% complete</td>
                </tr>
                <tr id="firstdocWS">
                    <td class=tdgeneral colspan="2" align="right"><input type="file" name="attachments[]" id="attachment" size="30"/></td>
                </tr>
                <tr>
                    <td align="right" colspan="2"><a href="javascript:void(0)" id="attachlinkWS">Attach another file</a></td>
                </tr>

                <tr>
                    <td valign="top" colspan="3" class=tdgeneral>
                        <input type="hidden" name="taskid" id="taskid" value="<?php echo($taskid);?>"/>
                        <input type="submit" value="Submit" name="B1">
                        <input type="reset" value="Reset" name="B2"></td>
                </tr>
                    <?php
                }
                else {
                    echo("<input type=hidden name=logstatusid value=CL>");
                }
//DISPLAY PAST UPDATES
                $sql = "SELECT * FROM assist_".$cmpcode."_ta_log l, assist_".$cmpcode."_ta_list_status s WHERE l.logstatusid = s.pkey AND l.logtaskid = ".$taskid." ORDER BY logid DESC";
                include("inc_db_con.php");
                $l = mysql_num_rows($rs);
                $l2 = 0;
                while($row = mysql_fetch_array($rs)) {
                    $l2++;
                    if($l > $l2) {
                        ?>
                <tr>
                    <td valign="top" class=tdgeneral align=center><?php echo(date("d-M-Y H:i",$row['logdate'])); ?>&nbsp;</td>
                    <td valign="top" class=tdgeneral><?php
                                $logid = $row['logid'];
                                $logarr = explode(chr(10),$row['logupdate']);
                                $logupdate = implode("<br>",$logarr);
                                $logupdate = html_entity_decode($logupdate);
                                //check if there are any update attachments and display them.
                                $sql = "SELECT * FROM assist_".$cmpcode."_ta_task_attachments WHERE logid=".$row['logid'];
                                $rs2 = mysql_query($sql);
                                $attachments = "";
                                while($row3 = mysql_fetch_array($rs2)) {
                                    $file = "../files/$cmpcode/TA/".$row3['system_filename'];
                                    if(!file_exists($file))
                                        continue;
                                    $attachments .= "<a href='javascript:download(\"".$file."\")'>".$row3['original_filename']."</a>&nbsp;&nbsp;";
                                    if($task['taskadduser'] == $tkid) {
                                        $button = "<input type='button' onclick='delUpdateAttachment(\"$logid\",\"".$row3['id']."\")' value='Delete' id='deleteAttachment' name='deleteAttachment'><br />";
                                    }
                                    $attachments .= $button;
                                }
                                echo(html_entity_decode($logupdate));
                                if(!empty($attachments))
                                    echo "<br />Attachments:<br />".$attachments;

                                /*$sql = "SELECT * FROM assist_".$cmpcode."_attachments WHERE taskid=$taskid AND type_id=1";
                include("inc_db_con.php");
                while($row0 = mysql_fetch_array($rs)){
                    $file = "../files/".$cmpcode."/TA/".$row['system_filename'];
                    //echo ("<a href='javascript:download(\"$file\")'>".$row0['original_filename']."</a><br />");
                    
                }*/
                                ?>&nbsp;</td>
                    <td valign="top" class=tdgeneral><?php
                                echo($row['value']);
                                if($row['pkey'] == "3" || $row['pkey'] > 5) {
                                    echo("<br>(".$row['logstate']."%)");
                                }
                                ?>&nbsp;</td>
                </tr>
                        <?php
                    }
                }
                ?>
            </table>

        </form>
        <script type="text/javascript">
            var statu = document.taskupdate.logstatusid.value;
            if(statu == "5")
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }
        </script>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </body>

</html>
