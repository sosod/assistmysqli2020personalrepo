<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");

//GET USER ACCESS - IF 20 THEN NEW TASKS ONLY TO SELF ELSE CAN ASSIGN TO OTHERS
$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_access WHERE tkid = '".$tkid."'";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$tact = $row['act'];
mysql_close();
?>
<html>
    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
        <link href="../lib/calendar/css/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css"/>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-1.4.2.min.js"></script>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-ui-1.8.1.custom.min.js"></script>

        <script type ="text/javascript">

            $(function() {
                $("#datepicker").datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true		});
            });
            $(document).ready(function(){
                $('#attachlink').click(function(){
                    $('<tr><td align="right" colspan="2" ><input type="file" name=attachments[] size="30"/></td>').insertAfter('#firstdoc');
                });
            });

        </script>
    </head>
    <script language="JavaScript">
        function Validate(tform){
            var form = document.forms['newtask'];
            var count = 0;
            var message = '';
            for(var i = 0; i < form.tasktkid.length;i++){
                if(form.tasktkid.options[i].selected){
                    count++;
                }
            }
            if(count == 0){
                if(form.elements['tact']){
                    var tact = parseInt(form.tact.value);
                    if(isNaN(tact)){
                        message += "Please assign this task to at least 1 user.\n";
                    }
                }else{
                    message += "Please assign this task to at least 1 user.\n";
                }

            }
            if(tform.taskurgencyid.value == "X"){
                message += "Please select the task priority.\n";
            }
            if(tform.taskstatusid.value == "X"){
                message += "Please select task status.\n";
            }
            if(tform.datepicker.value == ""){
                message += "Please select task deadline date.\n";
            }
            if(tform.tasktopicid.value == "X"){
                message += "Please select task topic.\n";
            }
            if(tform.taskaction.value == ""){
                message += "Please enter task details.\n"
            }
            if(message != '')
            {
                alert(message);
                return false;
            }
            return true;
        }
    </script>
    <link rel="stylesheet" href="/default.css" type="text/css">
    <?php include("inc_style.php"); ?>
    <base target="main">

    <body style="font-size:62.5%;">
        <h1 class=fc><b>Task Assist: New Task</b></h1>
        <p><i>Please note that all fields are required unless otherwise indicated.</i></p>
        <form name=newtask method=post action=new_process.php onsubmit="return Validate(this);" language=jscript enctype="multipart/form-data">
            <table border="1" id="table1" cellspacing="0" cellpadding="4">
                <tr>
                    <td class=tdheaderl valign="top" width=140><b>Assigned by:</b></td>
                    <td class=tdgeneral valign="top"><?php echo($tkname); ?><input type=hidden size=5 name=taskadduser value=<?php echo($tkid); ?>></td>
                </tr>
                <tr>
                    <td class=tdheaderl valign="top"><b>Assign to:</b></td>
                    <td class=tdgeneral valign="top">
                        <?php
                        if($tact == 20) //IF USER ACCESS = 20 THEN ASSIGN TASKS TO SELF ONLY
                        {
                            echo($tkname."<input type=hidden size=5 name=tasktkid value=".$tkid.">");
                           echo("<input type='hidden' name='tact' id='tact' value='$tact' />");
                        }
                        else //IF USER ACCESS > 20 THEN ASSIGN TASKS TO OTHERS
                        {
                            ?>
                            <?php
                            /*$sql = "SELECT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' ORDER BY t.tkname, t.tksurname";*/

                            $sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = 'TA'
                                ORDER BY t.tkname, t.tksurname";

                            include("inc_db_con.php");
                            $size = mysql_num_rows($rs);
				$size = ($size > 10) ? $size = 5 : $size;
                            ;?>
                        <i>Ctrl + left click to select multiple users</i><br />
                        <select  id="tasktkid" name="tasktkid[]" multiple="multiple" size="<?php echo($size);?>">
                            <!--<option value=X>--- SELECT ---</option>-->
                                <?php
                                while($row = mysql_fetch_array($rs)) {
                                    echo("<option name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>");
                                }
                                mysql_close();
                                ?>
                        </select>

    <?php
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class=tdheaderl valign="top"><b>Topic:</td>
                    <td class=tdgeneral valign="top">
                        <select size="1" name="tasktopicid">
                            <option value=X>--- SELECT ---</option>
<?php
                            $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE yn = 'Y' ORDER BY value";
                            include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs)) {
                                echo("<option value=".$row['id'].">".$row['value']."</option>");
                            }
                            mysql_close();
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class=tdheaderl valign="top"><b>Priority:</td>
                    <td class=tdgeneral valign="top">
                        <select size="1" name="taskurgencyid">
                            <option value=X>--- SELECT ---</option>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_urgency ORDER BY sort";
                            include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs)) {
                                echo("<option ".($row['id']== 2 ? "selected='selected'" : '')." value=".$row['id'].">".$row['value']."</option>");
                            }
                            mysql_close();
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class=tdheaderl valign="top"><b>Status:</td>
                    <td class=tdgeneral valign="top">
                        <select size="1" name="taskstatusid" >
                            <option value=X>--- SELECT ---</option>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE (pkey = 4 OR pkey = 5) AND yn = 'Y' ORDER BY sort";
include("inc_db_con.php");
                            while($row = mysql_fetch_array($rs)) {
                                if($row['pkey'] == "4") {
                                    echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                }
                                else {
                                    echo("<option value=".$row['pkey'].">".$row['value']."</option>");
                                }
                            }
                            mysql_close();
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td  class=tdheaderl valign="top"><b>Deadline:</b></td>
                    <td class=tdgeneral valign="top"><input type=text size=15 id="datepicker" name="datepicker" readonly="readonly"></td>
                </tr>
                <tr>
                    <td class=tdheaderl valign="top"><b>Task Instructions:</b></td>
                    <td class=tdgeneral><textarea rows="5" name="taskaction" cols="35"></textarea></td>
                </tr>
                <tr>
                    <td class=tdheaderl valign="top"><b>Task Deliverables:</b></td>
                    <td class=tdgeneral><textarea rows="5" name="taskdeliver" cols="35"></textarea></td>
                </tr>

<?php
$sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";

include("inc_db_con.php");
while($row = mysql_fetch_array($rs)) {
    ?>
                <tr>
                    <td class=tdheaderl valign="top"><b><?php echo($row['udfivalue']); ?>:</b></td>
                    <td class=tdgeneral valign="top">
                    <?php
                    switch($row['udfilist']) {
                        case "Y":
                            echo("<select name=".$row['udfiid']."><option value=X>---SELECT---</option>");
            $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvyn = 'Y' ORDER BY udfvvalue";
            include("inc_db_con2.php");
            while($row2 = mysql_fetch_array($rs2)) {
                                        echo("<option value=".$row2['udfvid'].">".$row2['udfvvalue']."</option>");
                                    }
                                    mysql_close($con2);
                                    echo("</select>");
                                    break;
                                case "T":
                                    echo("<input type=text name=".$row['udfiid']." size=50>");
                                    break;
                                case "M":
                                    echo("<textarea name=".$row['udfiid']." rows=5 cols=40></textarea>");
                                    break;
                                default:
                                    echo("<input type=text name=".$row['udfiid']." size=50>");
                                    break;
                            }
                            ?>
                    </td>
                </tr>
                
               
                            <?php
                        }
                        mysql_close($con);
                        ?>
                <tr id="firstdoc">
                    <td class=tdheaderl valign="top"><b>Attach document(s):</b></td>
                    <td class=tdgeneral align="right"><input type="file" name="attachments[]" id="attachment" size="30"/></td>
                </tr>
                <tr>
                    <td align="right" colspan="2"><a href="javascript:void(0)" id="attachlink">Attach another file</a></td>
                </tr>
                 <tr><td class="tdheaderl">Send Email:</td><td class="tdgeneral"><input type="checkbox" name="sendEmail" id="sendEmail" value="1"/> </td></tr>
                <tr>
                    <td class=tdgeneral colspan="2" valign="top">
                        <input type="submit" value="Submit" name="B1" style="margin-left: 147px;">
                        <input type="reset" value="Reset" name="B2"></td>
                </tr>
            </table>
        </form>

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </body>
</html>

