<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
<script type ="text/javascript" src="../lib/calendar/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
         $('#u2').change(function(){             
             if($("#u2 option:selected").val()=='M'){
                $('#defaultValue').html('<td class="tdheaderl" valign="top"><b>Default Value:</b></td><td class="tdgeneral" colspan="2"><textarea cols="40" rows="4" name="default" id="default" ></textarea></td>');
             }else{
                $('#defaultValue').html('<td class="tdheaderl" valign="top"><b>Default Value:</b></td><td class="tdgeneral" colspan="2"><input type="text" name="default" id="default" size="50" /></td>');
             }
         });
     });
</script>
</head>
<script language=JavaScript>
function Validate(me) {
    var val = me.udfivalue.value;
    var typ = me.udfilist.value;
    var err;
    var valid8 = "false";
    
    if(val.length > 50 || val.length == 0)
    {
        err = " - Field Name";
        document.getElementById('u1').className = 'req';
    }
    if(typ.length == 0 || (typ != "Y" && typ != "T" && typ != "M"))
    {
        if(err.length > 0)
        {
            err = err + "\n";
        }
        err = err + " - Field Type";
        document.getElementById('u2').className = 'req';
    }
    if(err.length == 0)
    {
        return true;
    }
    else
    {
        alert("Please complete the missing fields:\n"+err);
        return false;
    }
}

</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>

<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc style="margin-bottom: -10px"><b>Task Assist: Setup - Create UDF</b></h1>
<p>&nbsp;</p>
<form name=newudf action=setup_udf_new_process.php method=POST onsubmit="return Validate(this);" language=jscript>
<table width=500 border=1 cellpadding=3 cellspacing=0 style="border-collapse: collapse; border: 1px solid;">
    <tr class=tdgeneral>
        <td class=tdheaderl><b>Field Name:</b></td>
        <td class=tdgeneral><input type=text name=udfivalue maxlength=50 size=50 id=u1 class=blank></td>
        <td class=tdgeneral><i><small>(Max: 50 characters)</small></i></td>
    </tr>
    <tr class=tdgeneral>
        <td class=tdheaderl><b>Field Type:</b></td>
        <td class=tdgeneral>
            <select name='udfilist' id='u2'>
                <option selected value=Y>Preset drop down list</option>
                <option value=T>Text (Max: 200 characters)</option>
                <option value=M>Text (No limit)</option>
            </select>
        </td>
        <td class=tdgeneral>&nbsp;</td>
    </tr>
    <tr class=tdgeneral>
        <td class=tdheaderl><b>Required:</b></td>
        <td class="tdheader1" colspan="2"><input type="checkbox" name="required" id="required" />&nbsp;&nbsp;<i>Click if this field is required.</i></td>
    </tr>
    <tr class="tdgeneral" id="defaultValue">
        <td class="tdheaderl" valign="top" ><b>Default Value:</b></td>
        <td class="tdgeneral" colspan="2"><input type="text" name="default" id="default" size="50"/></td>
    </tr>
    <tr class=tdgeneral>
        <td colspan=3><input type=submit value=" Add "> <input type=reset></td>
    </tr>
</table>
</form>
</body>

</html>
