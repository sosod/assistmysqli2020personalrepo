<?php

include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
//GET VARIABLES
$field = $_POST['field'];
$udf = $_POST['udf'];
$sfilter = $_POST['statusfilter'];
$tfilter = $_POST['tasktkidfilter'];

$pfilter = $_POST['tasktopicfilter'];
$afilter = $_POST['taskaddfilter'];
$csvfile = $_POST['csvfile'];
$ifilter = $_POST['taskactfilter'];
$ifiltertype = $_POST['taskactfiltertype'];
$dfilter = $_POST['taskdelfilter'];
$dfiltertype = $_POST['taskdelfiltertype'];
$adfilter = $_POST['taskadddatefilter'];
$dlfilter = $_POST['taskdlfilter'];
$upfilter = $_POST['upfilter'];

//CONVERT FROM array[#] = text TO array[text] = Y
foreach($field as $f)
{
    $farray[$f] = "Y";
}
$uarray = "";
foreach($udf as $u)
{
    $uarray[$u] = "Y";
}
$udffilter = "";

    $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiyn = 'Y' AND udfiref = 'TA' ORDER BY udfisort";
    include("inc_db_con.php");
        $udfnum = mysql_num_rows($rs);
        $u = 0;
        while($row = mysql_fetch_array($rs))
        {
            $udfindex[$u]['id'] = $row['udfiid'];
            $udfindex[$u]['value'] = $row['udfivalue'];
            $udfindex[$u]['list'] = $row['udfilist'];
            $u++;
            $udfname = "udf".$row['udfiid']."filter";
            $ufilter = $_POST[$udfname];
            //echo("<p>ind: ".$row['udfiid']."<Br>val: ".$row['udfivalue']."<Br>list: ".$row['udfilist']."<Br>filter: ".$ufilter."<Br>len: ".strlen($ufilter));
            if((strlen($ufilter) > 0 && ($row['udfilist'] == "T" || $row['udfilist'] == "M")) || ($ufilter != "ALL" && ($row['udfilist']=="Y" || $row['udfilist'] == "L")))
            {
                $udffilter[$row['udfiid']] = $ufilter;
            }
            else
            {
                $udfnum = $udfnum - 1;
            }
        }
    mysql_close();
/*echo("<p>udfs: ");
print_r($udfindex);
    echo("<P>udfnum ".$udfnum);
    echo("<p>uarray: ");
print_r($uarray);
    echo("<P>count ".count($udf));
echo("<p>udffilter: ");
print_r($udffilter);
*/
//SET SQL VARIABLE
$sql = "SELECT s.pkey sid, a.taskid tid, t.tkname tname, t.tksurname tsurname, l.value topic, a.taskaction taction, a.taskdeliver tdeliver, s.value tstatus, a.taskstate tstate, a.taskdeadline tdeadline, a.taskadddate tadddate, k.tkname toname, k.tksurname tosurname";
$sql .= " FROM assist_".$cmpcode."_ta_task a";
$sql .= ", assist_".$cmpcode."_timekeep t";
$sql .= ", assist_".$cmpcode."_ta_list_topic l";
$sql .= ", assist_".$cmpcode."_timekeep k";
$sql .= ", assist_".$cmpcode."_ta_list_status s";
$sql .= ", assist_".$cmpcode."_ta_task_recipients r";
$sql .= " WHERE a.tasktkid = t.tkid";
$sql .= " AND a.taskstatusid = s.pkey";
$sql .= " AND a.tasktopicid = l.id";
$sql .= " AND a.taskadduser = k.tkid";

$sql = "SELECT DISTINCT(a.taskid) AS tid,s.pkey sid,  t.tkname tname, t.tksurname tsurname, l.value topic, a.taskaction taction, a.taskdeliver tdeliver, s.value tstatus, a.taskstate tstate, a.taskdeadline tdeadline, a.taskadddate tadddate,
        t.tkname toname, t.tksurname tosurname";
$sql .= " FROM assist_".$cmpcode."_ta_task a
               INNER JOIN assist_".$cmpcode."_ta_list_topic l ON(a.tasktopicid = l.id)
               INNER JOIN assist_".$cmpcode."_ta_list_status s ON(a.taskstatusid=s.pkey)
                INNER JOIN assist_".$cmpcode."_timekeep t ON(a.taskadduser=t.tkid)
                   INNER JOIN assist_".$cmpcode."_ta_task_recipients r ON(a.taskid=r.taskid) WHERE 1 ";
if(!empty($tfilter)){
    $recipients = implode(",",$tfilter);
}
switch($tfilter)
{
    case "ALL":
        break;
    default:
        if($recipients)
            $sql .= " AND r.tasktkid IN ($recipients)"; //'".$tfilter."'";
        break;
}
switch($afilter)
{
    case "ALL":
        break;
    default:
        $sql .= " AND a.taskadduser = '".$afilter."'";
        break;
}
switch($pfilter)
{
    case "ALL":
        break;
    default:
        $sql .= " AND l.id = '".$pfilter."'";
        break;
}
switch($adfilter)
{
    case "ALL":
        break;
    case "EXACT":
        $tadf1 = $_POST['tadf1'];
        $tadf2 = $_POST['tadf2'];
        //$df1 = mktime(0,0,0,$tadf1[1],$tadf1[0],$tadf1[2]);
        //$df2 = mktime(23,59,59,$tadf2[1],$tadf2[0],$tadf2[2]);
        $datepickerFrom = strtotime($_POST['datepickerFrom']);
        $datepickerTo = strtotime($_POST['datepickerTo']);
        //$sql .= " AND a.taskadddate > ".$df1." AND a.taskadddate < ".$df2;
        $sql .= " AND a.taskadddate > '".$datepickerFrom."' AND a.taskadddate < '".$datepickerTo."'";
        break;
    default:
        break;
}
switch($dlfilter)
{
    case "ALL":
        break;
    case "EXACT":
        $tdl1 = $_POST['tdl1'];
        $tdl2 = $_POST['tdl2'];
       // $dl1 = mktime(0,0,0,$tdl1[1],$tdl1[0],$tdl1[2]);
       // $dl2 = mktime(23,59,59,$tdl2[1],$tdl2[0],$tdl2[2]);
        $datepickerDeadlineFrom = strtotime($_POST['datepickerDeadlineFrom']);
        $datepickerDeadlineTo = strtotime($_POST['datepickerDeadlineTo']);

       // $sql .= " AND a.taskdeadline > ".$dl1." AND a.taskdeadline < ".$dl2;
         $sql .= " AND a.taskdeadline > '".$datepickerDeadlineFrom."' AND a.taskdeadline < '".$datepickerDeadlineTo."'";
        break;
    default:
        break;
}
if(strlen($ifilter)>0)
{
    switch($ifiltertype)
    {
        case "ALL":
            $ifilter = explode(" ",$ifilter);
            foreach($ifilter as $ifil)
            {
                $sql .= " AND a.taskaction LIKE '%".$ifil."%'";
            }
            break;
        case "ANY":
            $ifilter = explode(" ",$ifilter);
            $sql .= " AND (";
            $i=0;
            foreach($ifilter as $ifil)
            {
                if($i>0) { $sql .= " OR "; }
                $sql .= "a.taskaction LIKE '%".$ifil."%'";
                $i++;
            }
            $sql .= ")";
            break;
        case "EXACT":
            $sql .= " AND a.taskaction LIKE '%".$ifilter."%'";
            break;
        default:
            break;
    }
}
if(strlen($dfilter)>0)
{
    switch($dfiltertype)
    {
        case "ALL":
            $dfilter = explode(" ",$dfilter);
            foreach($dfilter as $dfil)
            {
                $sql .= " AND a.taskdeliver LIKE '%".$dfil."%'";
            }
            break;
        case "ANY":
            $dfilter = explode(" ",$dfilter);
            $sql .= " AND (";
            $f=0;
            foreach($dfilter as $dfil)
            {
                if($f>0) { $sql .= " OR "; }
                $sql .= "a.taskdeliver LIKE '%".$dfil."%'";
                $f++;
            }
            $sql .= ")";
            break;
        case "EXACT":
            $sql .= " AND a.taskdeliver LIKE '%".$dfilter."%'";
            break;
        default:
            break;
    }
}
switch($sfilter)
{
    case "ALL":
        $sql .= " AND (s.id <> 'CN')";
        break;
    case "INC":
        $sql .= " AND (s.id <> 'CN' AND s.id <> 'CL')";
        break;
    default:
        $sql .= " AND (s.pkey = ".$sfilter.")";
        break;
}
$sql .= " ORDER BY a.taskid";
//echo $sql;
//Set transaction log
$tsql2 = $sql;
$tsql = str_replace("'","|",$tsql2);
$tref = 'TA';
$trans = "Generated task report to view Task id";
if($farray['adddate'] == "Y") {$trans .= ", Date added";}
if($farray['tkid'] == "Y") {$trans .= ", Person tasked";}
if($farray['adduser'] == "Y") {$trans .= ", Task owner";}
if($farray['topicid'] == "Y") {$trans .= ", Task topic";}
if($farray['action'] == "Y") {$trans .= ", Task instructions";}
if($farray['deliver'] == "Y") {$trans .= ", Task deliverables";}
if($farray['deadline'] == "Y") {$trans .= ", Task deadline";}
if($farray['update'] == "Y") {$trans .= ", Task update";}
$trans .= ", Task status; Report output as ";
if($csvfile == "Y") {$trans .= "CSVfile.";} else {$trans .= "onscreen display.";}
include("inc_transaction_log.php");

//RUN SQL QUERY
$sql = $tsql2;

include("inc_db_con.php");


if($csvfile == "Y") //IF OUTPUT SELECTED IS CSV FILE
{
        //CREATE HEADER ROW
            $fdata = "\"Ref\"";
            if($farray['adddate'] == "Y") {$fdata .= ",\"Date added\"";}
    		if($farray['tkid'] == "Y") {$fdata .= ",\"Person tasked\"";}
            if($farray['adduser'] == "Y") {$fdata .= ",\"Task owner\"";}
            if($farray['topicid'] == "Y") {$fdata .= ",\"Task topic\"";}
            if($farray['action'] == "Y") {$fdata .= ",\"Task instructions\"";}
            if($farray['deliver'] == "Y") {$fdata .= ",\"Task deliverables\"";}
            if($farray['deadline'] == "Y") {$fdata .= ",\"Task deadline\"";}
            $fdata .= ",\"Task status\"";
            if($farray['update'] == "Y") {$fdata .= ",\"Most recent update\",\"Date of update\"";}
            if(count($udf)>0)
            {
                foreach($udfindex as $udfi)
                {
                    if($uarray[$udfi['id']] == "Y") {$fdata .= ",\"".$udfi['value']."\"";}
                }
            }
    //LOOP TROUGH QUERY RESULTS AND ADD DETAILS TO ROW IF array[text] = Y
    while($row = mysql_fetch_array($rs))
    {
            $tchk = "X";
        if(count($udf) > 0 || $udfnum > 0)
        {
            foreach($udfindex as $udfi)
            {
                $uchk = "N";
                //echo("<P>uarray[id] ".$uarray[$udfi['id']]);
                if(($uarray[$udfi['id']] == "Y" || strlen($udffilter[$udfi['id']]) > 0) && $tchk != "N")
                {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udf u";
                    $sql2.= " WHERE u.udfref = 'TA'";
                    $sql2.= " AND u.udfnum = ".$row['tid'];
                    $sql2.= " AND u.udfindex = ".$udfi['id'];
                    if(($udfi['list'] == "T" || $udfi['list'] == "M") && strlen($udffilter[$udfi['id']]) > 0)
                    {
                        $sql2.= " AND u.udfvalue LIKE '%".$udffilter[$udfi['id']]."%'";
                    }
                    else
                    {
                        if(($udfi['list'] == "L" || $udfi['list'] == "Y") && strlen($udffilter[$udfi['id']]) > 0)
                        {
                            $sql2.= " AND u.udfvalue = '".$udffilter[$udfi['id']]."'";
                        }
                    }
                    //echo("<P>".$sql2);
                    include("inc_db_con2.php");
                        $rscount = mysql_num_rows($rs2);
                        //echo("<P>".$sql2);
                        //echo("<P>".$rscount);
                        if(strlen($udffilter[$udfi['id']]) > 0 && $rscount == 0)
                        {
                            $tchk = "N";
                        }
                        if($rscount > 0 && $tchk != "N" && $uarray[$udfi['id']] == "Y")
                        {
                            $uchk = "Y";
                            $row2 = mysql_fetch_array($rs2);
                            //echo("<br>row ");
                            //print_r($row2);
                            $udfvalue[$udfi['id']] = $row2['udfvalue'];
                        }
                    mysql_close($con2);
                    if($uchk == "Y")
                    {
                        if($udfi['list'] == "L" || $udfi['list'] == "Y")
                        {
                            $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvid = ".$udfvalue[$udfi['id']];
                            include("inc_db_con2.php");
                                $row2 = mysql_fetch_array($rs2);
                                $udfvalue[$udfi['id']] = $row2['udfvvalue'];
                            mysql_close($con2);
                        }
                        else
                        {
                            //$udfvalue[$udfi['id']] = str_replace(chr(10),"<br>",$udfvalue[$udfi['id']]);
                        }
                    }
                }
                //echo("<P>".$tchk);
            }
        }
        else
        {
            $tchk = "Y";
        }
//echo("<P>tchk ".$tchk);
//echo("<P>udfvalue: ");
//print_r($udfvalue);
        if($tchk == "Y" || $tchk == "X")
        {

	   $fdata .= "\r\n";
		    $fdata .= "\"".$row['tid']."\"";
		    if($farray['adddate'] == "Y") {$fdata .= ",\"".date("d-M-Y",$row['tadddate'])."\"";}
		    $tname = $row['tname']." ".$row['tsurname'];
		    $tname = str_replace("&#39","'",$tname);
		    if($farray['tkid'] == "Y") {$fdata .= ",\"".$tname."\"";}
		    $toname = $row['toname']." ".$row['tosurname'];
		    $toname = str_replace("&#39","'",$toname);
		    if($farray['adduser'] == "Y") {$fdata .= ",\"".$toname."\"";}
		    $topic = $row['topic'];
		    $topic = str_replace("&#39","'",$topic);
		    if($farray['topicid'] == "Y") {$fdata .= ",\"".$topic."\"";}
		    $action = $row['taction'];
		    $action = str_replace("&#39","'",$action);
		    $action = str_replace("\"","'",$action);
		    if($farray['action'] == "Y") {$fdata .= ",\"".$action."\"";}
		    $deliver = $row['tdeliver'];
		    $deliver = str_replace("&#39","'",$deliver);
		    $deliver = str_replace("\"","'",$deliver);
		    if($farray['deliver'] == "Y") {$fdata .= ",\"".$deliver."\"";}
		    if($farray['deadline'] == "Y") {$fdata .= ",\"".date("d-M-Y",$row['tdeadline'])."\"";}
    		$fdata .= ",\"".$row['tstatus'];
            if($row['sid'] == "3" || $row['sid'] == "4" || $row['sid'] > 5)
            {
                $fdata .= " (".$row['tstate']."%)";
            }
            $fdata .= "\"";
		    if($farray['update'] == "Y")
            {
                $fdata .= ",\"";
                $sql2 = "SELECT logupdate FROM assist_".$cmpcode."_ta_log WHERE logtaskid = ".$row['tid']." AND logstatusid <> '4' ORDER BY logid DESC";
                include("inc_db_con2.php");
                if(mysql_num_rows($rs2)>0)
                {
                    $rowlog = mysql_fetch_array($rs2);
                    if(mysql_num_rows($rs2) > 1)
                    {
                        $fdata .= str_replace("&#39","'",$rowlog['logupdate']);
                    }
                    else
                    {
                        if($row['sid'] != "5")
                        {
                            $fdata .= str_replace("&#39","'",$rowlog['logupdate']);
                        }
                    }
                }
                mysql_close($con2);
                $fdata .= "\"";
            }

            if(count($udf)>0)
            {
                foreach($udfindex as $udfi)
                {
                    if($uarray[$udfi['id']] == "Y")
                    {
                        $fdata .= ",\"".$udfvalue[$udfi['id']]."\"";
                    }
                }
            }
        }
    }
    mysql_close();
    //WRITE DATA TO FILE
    $filename = "../files/".$cmpcode."/ta_report_".date("Ymd_Hi",$today).".csv";
    $newfilename = "task_report_".date("Ymd_Hi",$today).".csv";
    $file = fopen($filename,"w");
    fwrite($file,$fdata."\n");
    fclose($file);
    //SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
    header('Content-type: text/plain');
    header('Content-Disposition: attachment; filename="'.$newfilename.'"');
    readfile($filename);
}
else    //ELSE OUTPUT IS ONSCREEN DISPLAY
{
//SET DISPLAY OF HTML HEAD & BODY
$display = "<html><link rel=stylesheet href=/default.css type=text/css>";
include("inc_style.php");
$display .= "<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5><h1 class=fc><b>Task Assist: Report</b></h1>";
//IF RESULT = 0
$tar = mysql_num_rows($rs);
if($tar == 0)
{
    $display .= "<p>There are no tasks that meet your criteria.<br>Please go back and select different criteria.</p>";
}
//ELSE
else
{
    //CREATE TABLE AND SET TDHEADER
    $display .= "<table border=1 cellspacing=0 cellpadding=2 style=\"border-collapse: collapse; border: 1px solid #ededed\">	<tr>		<td class=tdheaderorange>Ref</td>";
            if($farray['adddate'] == "Y") {$display .= "<td class=tdheaderorange>Date added</td>";}
    		if($farray['tkid'] == "Y") {$display .= "<td class=tdheaderorange>Person tasked</td>";}
            if($farray['adduser'] == "Y") {$display .= "<td class=tdheaderorange>Task owner</td>";}
            if($farray['topicid'] == "Y") {$display .= "<td class=tdheaderorange>Task topic</td>";}
            if($farray['action'] == "Y") {$display .= "<td class=tdheaderorange>Task instructions</td>";}
            if($farray['deliver'] == "Y") {$display .= "<td class=tdheaderorange>Task deliverables</td>";}
            if($farray['deadline'] == "Y") {$display .= "<td class=tdheaderorange>Task deadline</td>";}
            $display .= "<td class=tdheaderorange>Task status</td>";
            if($farray['update'] == "Y") {$display .= "<td class=tdheaderorange>Most recent update</td><td class=tdheaderorange>Date of update</td>";}
            if(count($udf)>0)
            {
                foreach($udfindex as $udfi)
                {
                    if($uarray[$udfi['id']] == "Y") {$display .= "<td class=tdheaderorange>".$udfi['value']."</td>";}
                }
            }

    $display .= "<td class=tdheaderorange>&nbsp;</td>";
	$display .= "</tr>";
    //LOOP THROUGH QUERY RESULT AND DISPLAY DATA WHERE array[text] = Y
    while($row = mysql_fetch_array($rs))
    {
        $logcount = 1;
        if($farray['update'] == "Y" && $upfilter == "ALL")
        {
            $sql2 = "SELECT * FROM assist_".$cmpcode."_ta_log l, assist_".$cmpcode."_ta_list_status s WHERE l.logstatusid = s.pkey AND logstatusid <> 4 AND l.logtaskid = ".$row['tid']." ORDER BY logid DESC";
            include("inc_db_con2.php");
                $logcount = mysql_num_rows($rs2);
                if($logcount ==0)
                {
                    $logcount = 1;
                }
            mysql_close($con2);
        }
        $tchk = "X";
        if(count($udf) > 0 || $udfnum > 0)
        {
            foreach($udfindex as $udfi)
            {
                $uchk = "N";
                //echo("<P>uarray[id] ".$uarray[$udfi['id']]);
                if(($uarray[$udfi['id']] == "Y" || strlen($udffilter[$udfi['id']]) > 0) && $tchk != "N")
                {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udf u";
                    $sql2.= " WHERE u.udfref = 'TA'";
                    $sql2.= " AND u.udfnum = ".$row['tid'];
                    $sql2.= " AND u.udfindex = ".$udfi['id'];
                    if(($udfi['list'] == "T" || $udfi['list'] == "M") && strlen($udffilter[$udfi['id']]) > 0)
                    {
                        $sql2.= " AND u.udfvalue LIKE '%".$udffilter[$udfi['id']]."%'";
                    }
                    else
                    {
                        if(($udfi['list'] == "L" || $udfi['list'] == "Y") && strlen($udffilter[$udfi['id']]) > 0)
                        {
                            $sql2.= " AND u.udfvalue = '".$udffilter[$udfi['id']]."'";
                        }
                    }
                    //echo("<P>".$sql2);
                    include("inc_db_con2.php");
                        $rscount = mysql_num_rows($rs2);
                        //echo("<P>".$sql2);
                        //echo("<P>".$rscount);
                        if(strlen($udffilter[$udfi['id']]) > 0 && $rscount == 0)
                        {
                            $tchk = "N";
                        }
                        if($rscount > 0 && $tchk != "N" && $uarray[$udfi['id']] == "Y")
                        {
                            $uchk = "Y";
                            $row2 = mysql_fetch_array($rs2);
                            //echo("<br>row ");
                            //print_r($row2);
                            $udfvalue[$udfi['id']] = $row2['udfvalue'];
                        }
                    mysql_close($con2);
                    if($uchk == "Y")
                    {
                        if(($udfi['list'] == "L" || $udfi['list'] == "Y") && strlen($udfvalue[$udfi['id']])>0)
                        {
                            $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvid = ".$udfvalue[$udfi['id']];
                            include("inc_db_con2.php");
                                $row2 = mysql_fetch_array($rs2);
                                $udfvalue[$udfi['id']] = $row2['udfvvalue'];
                            mysql_close($con2);
//                            $udfvalue[$udfi['id']] = $sql2;
                        }
                        else
                        {
                            $udfvalue[$udfi['id']] = str_replace(chr(10),"<br>",$udfvalue[$udfi['id']]);
                        }
                    }
                }
                //echo("<P>".$tchk);
            }
        }
        else
        {
            $tchk = "Y";
        }
//echo("<P>tchk ".$tchk);
//echo("<P>udfvalue: ");
//print_r($udfvalue);
        if($tchk == "Y" || $tchk == "X")
        {
        $tr++;
    	$display .= "<tr id=tr".$tr." onmouseover=\"hovCSS('tr".$tr."');\" onmouseout=\"hovCSS2('tr".$tr."');\">";
	   	    $display .= "<td class=tdheaderorange rowspan=".$logcount." valign=top>".$row['tid']."</td>";
		    if($farray['adddate'] == "Y") {$display .= "<td class=tdgeneral rowspan=".$logcount." valign=top align=center>".date("d-M-Y",$row['tadddate'])."</td>";}
		    if($farray['tkid'] == "Y") {
                            $sql2 = "SELECT t.tkname,t.tksurname FROM assist_".$cmpcode."_ta_task_recipients r INNER JOIN assist_".$cmpcode."_timekeep t ON (r.tasktkid=t.tkid) WHERE taskid=".$row['tid'];
                                            //echo $sql;
                                            include("inc_db_con2.php");
                                            $recipients = "";
                                            while($rowQ = mysql_fetch_array($rs2)) {
                                                $recipients .= $rowQ['tkname']." ".$rowQ['tksurname'].",";
                                            }
                                            $recipients = substr($recipients, 0,-1);

                                            mysql_close($con2);
                            $display .= "<td class=tdgeneral rowspan=".$logcount." valign=top>".$recipients."</td>";

                    }
		    if($farray['adduser'] == "Y") {$display .= "<td class=tdgeneral rowspan=".$logcount." valign=top>".$row['toname']." ".$row['tosurname']."</td>";}
		    if($farray['topicid'] == "Y") {$display .= "<td class=tdgeneral rowspan=".$logcount." valign=top>".$row['topic']."</td>";}
		    if($farray['action'] == "Y") {$display .= "<td class=tdgeneral rowspan=".$logcount." valign=top>".str_replace(chr(10),"<br>",$row['taction'])."</td>";}
		    if($farray['deliver'] == "Y") {$display .= "<td class=tdgeneral rowspan=".$logcount." valign=top>".str_replace(chr(10),"<br>",$row['tdeliver'])."</td>";}
		    if($farray['deadline'] == "Y") {$display .= "<td class=tdgeneral rowspan=".$logcount." valign=top align=center>".date("d-M-Y",$row['tdeadline'])."</td>";}
		$display .= "<td class=tdgeneral rowspan=".$logcount." valign=top>".$row['tstatus'];

            if($row['sid'] == "3" || $row['sid'] == "4" || $row['sid'] > 5)
            {
                $display .= " (".$row['tstate']."%)";
            }
        $display .= "</td>";
        $ld = "";
		    if($farray['update'] == "Y")
            {
        		$display .= "<td class=tdgeneral valign=top>";
                $sql2 = "SELECT logid, logupdate, logdate FROM assist_".$cmpcode."_ta_log WHERE logtaskid = ".$row['tid']." AND logstatusid <> '4'  ORDER BY logid DESC";
                include("inc_db_con2.php");
                if(mysql_num_rows($rs2)>0)
                {
                    $rowlog = mysql_fetch_array($rs2);
                    $pastlogid = $rowlog['logid'];
                    //$display .= mysql_num_rows($rs2);
                    if(mysql_num_rows($rs2) > 1)
                    {
                        $display .= str_replace(chr(10),"<br>",html_entity_decode($rowlog['logupdate']));
                        $ld = date("d-M-Y H:i",$rowlog['logdate']);
                    }
                    else
                    {
                        if($row['sid'] != "5")
                        {
                            $display .= str_replace(chr(10),"<br>",html_entity_decode($rowlog['logupdate']));
                            $ld = date("d-M-Y H:i",$rowlog['logdate']);
                        }
                    }
                }
                mysql_close($con2);
                $display .= "&nbsp;</td>";
                $display .= "<td class=tdgeneral valign=top>".$ld."&nbsp;</td>";
            }
            if(count($udf)>0)
            {
                foreach($udfindex as $udfi)
                {
                    if($uarray[$udfi['id']] == "Y")
                    {
                        $display .= "<td class=tdgeneral rowspan=".$logcount." valign=top>".$udfvalue[$udfi['id']]."&nbsp;</td>";
                    }
                }
            }
	$display .= "<td class=tdgeneral  rowspan=".$logcount." align=center valign=top><input type=button value=View onclick=\"document.location.href = 'view_report.php?i=".$row['tid']."'\"></td>";
        $display .= "</tr>";
		    if($farray['update'] == "Y" && $upfilter == "ALL" && $logcount > 1)
            {
                $sql2 = "SELECT logid, logupdate, logdate FROM assist_".$cmpcode."_ta_log WHERE logtaskid = ".$row['tid']." AND logstatusid <> '4' ORDER BY logid DESC";
                include("inc_db_con2.php");
                if(mysql_num_rows($rs2)>0)
                {
                    while($rowlog = mysql_fetch_array($rs2))
                    {
                        if($rowlog['logid']!=$pastlogid)
                        {
                            $display .= "<tr id=tr".$tr." onmouseover=\"hovCSS('tr".$tr."');\" onmouseout=\"hovCSS2('tr".$tr."');\">";
                    		$display .= "<td class=tdgeneral valign=top>";
                            //$display .= mysql_num_rows($rs2);
                                $display .= str_replace(chr(10),"<br>",html_entity_decode($rowlog['logupdate']));
                                $ld = date("d-M-Y H:i",$rowlog['logdate']);
                            $display .= "&nbsp;</td>";
                      		$display .= "<td class=tdgeneral valign=top>".$ld."&nbsp;</td></tr>";
                        }
                    }
                }
                mysql_close($con2);
            }

$logcount = 1;
        $udfvalue = array();
        }   //if tchk = yes

    }
    $display .= "</table>";
}
mysql_close();
$display .= "</body></html>";
//WRITE DISPLAY TO SCREEN
echo($display);


}   //ENDIF CSVFILE = Y
?>

