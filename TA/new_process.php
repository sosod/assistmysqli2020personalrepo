

<?php
//error_reporting(E_ALL);

include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");

$cmpcode = $_SESSION['cc'];
$nextTaskID;
$attachment = false;
$original_filename = "";
$system_filename = "";
$sql = "SHOW TABLE STATUS LIKE  '%assist_".$cmpcode."_ta_task%'";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$nextTaskID = $row['Auto_increment'];
if(isset($_FILES)) {
    $folder = "../files/$cmpcode/TA";
    if(!is_dir($folder)) {
        mkdir($folder);
    }
    foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
        if(strlen($tmp_name)>0) {
            $original_filename = $_FILES['attachments']['name'][$key];
            $ext = substr($original_filename, strrpos($original_filename, '.') + 1);
            $parts = explode(".", $original_filename);
            $file = $parts[0];
            $system_filename = $nextTaskID."_".$file."_".date("YmdHi").".$ext";
            $full_path = $folder."/".$system_filename;
            if($_FILES['attachments']['error'][$key] == 0) {
                move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
                $sql = "INSERT INTO assist_".$cmpcode."_ta_task_attachments (taskid,original_filename,system_filename) VALUES ($nextTaskID,'$original_filename','$system_filename')";
                include("inc_db_con.php");
            }
        }

    }
}
?>
<html>

    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
    </head>
    <link rel="stylesheet" href="/default.css" type="text/css">
    <?php include("inc_style.php"); ?>
    <base target="main">
    <body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
        <h1 class=fc><b>Task Assist: New Task</b></h1>
        <?php
//GET DATA FROM FORM
        $tasktkid = $_POST['tasktkid'];
        $taskurgencyid = $_POST['taskurgencyid'];
        $tasktopicid = $_POST['tasktopicid'];
        $taskaction = $_POST['taskaction'];
        $taskdeliver = $_POST['taskdeliver'];
        $taskstatusid = $_POST['taskstatusid'];
        $dday = $_POST['dday'];
        $dmon = $_POST['dmon'];
        $dyear = $_POST['dyear'];
        $taskadduser = $_POST['taskadduser'];
//GET UDFINDEX
        $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
        include("inc_db_con.php");
        $udf = mysql_num_rows($rs);
        if($udf>0) {
            $u = 0;
            while($row = mysql_fetch_array($rs)) {
                $udfindex[$u] = $row['udfiid'];
                $u++;
            }
        }
        mysql_close();

//GET UDF DATA
        if($udf>0) {
            foreach($udfindex as $udfi) {
                $udfval[$udfi] = str_replace("'","&#39",$_POST[$udfi]);
            }
        }
//SET DEADLINE DATE
//$taskdeadline = getdate(mktime(23,59,59,$dmon,$dday,$dyear));        //DATE ARRAY
        $taskdeadline = strtotime($_POST['datepicker']);
//print_r($taskdeadline);die;
//REMOVE ' FROM ACTION
        $apos = explode("'",$taskaction);
        $taskaction = implode("&#39",$apos);
        $apos = "";

//REMOVE ' FROM DELIVER
        $apos = explode("'",$taskdeliver);
        $taskdeliver = implode("&#39",$apos);
        $apos = "";

//GET STATE FOR GIVEN STATUS
        $sql = "SELECT state FROM assist_".$cmpcode."_ta_list_status WHERE pkey = ".$taskstatusid."";
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $taskstate = $row['state'];
        mysql_close();

//ADD RECORD TO TA_TASK
        $sql = "INSERT INTO assist_".$cmpcode."_ta_task SET ";
//$sql .= "tasktkid = '".$tasktkid."', ";
        $sql .= "taskurgencyid = ".$taskurgencyid.", ";
        $sql .= "tasktopicid = ".$tasktopicid.", ";
        $sql .= "taskaction = '".$taskaction."', ";
        $sql .= "taskdeliver = '".$taskdeliver."', ";
        $sql .= "taskstatusid = '".$taskstatusid."', ";
        $sql .= "taskstate = ".$taskstate.", ";
        $sql .= "taskdeadline = '".$taskdeadline."', ";
        $sql .= "taskadddate = UNIX_TIMESTAMP(), ";
        $sql .= "taskadduser = '".$taskadduser."'";

//$sql .= "taskremind = '0'";
        $sqltask = $sql;
        include("inc_db_con.php");
        $taskid = mysql_insert_id();
        mysql_close();
//store task recipients in ta_task_recipients
        if(is_array($_POST['tasktkid'])) {
            $recipientsArr = $_POST['tasktkid'];
        }else {
            $recipientsArr[] = $_POST['tasktkid'];
        }
        $sql = "INSERT INTO assist_".$cmpcode."_ta_task_recipients (taskid,tasktkid) VALUES ";
        foreach($recipientsArr as $value) {
            $sql .= "($taskid,'$value'),";
        }
//chuck the last "," away
        $sql = substr($sql, 0,-1);
        include("inc_db_con.php");
//ADD UDF DATA
        foreach($udfindex as $udfi) {
            if($udfval[$udfi] != "X") {
                $sql = "INSERT INTO assist_".$cmpcode."_udf SET";
                $sql.= " udfindex = ".$udfi;
                $sql.= ", udfvalue = '".$udfval[$udfi]."'";
                $sql.= ", udfnum = ".$taskid;
                $sql.= ", udfref = '".$tref."'";
                include("inc_db_con.php");
            }
        }


//ADD RECORD TO TA_LOG
        $logupdate = "New task added.".chr(10)."Task instructions: ".$taskaction.chr(10)."Task deliverables: ".$taskdeliver;
        $sql = "INSERT INTO assist_".$cmpcode."_ta_log SET ";
        $sql .= "logdate = '".$today."', ";
        $sql .= "logtkid = '".$tasktkid."', ";
        $sql .= "logupdate = '".$logupdate."', ";
        $sql .= "logstatusid = '".$taskstatusid."', ";
        $sql .= "logstate = ".$taskstate.", ";
        $sql .= "logemail = 'N', ";
        $sql .= "logsubmittkid = '".$taskadduser."', ";
        $sql .= "logtaskid = '".$taskid."', ";
        $sql .= "logtasktkid = '".$tasktkid."'";
        $sqllog = $sql;
        include("inc_db_con.php");

//GET LOG ID
        $sql = "SELECT logid FROM assist_".$cmpcode."_ta_log WHERE logtaskid = ".$taskid." ORDER BY logid ASC";
        include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $logid = $row['logid'];
        mysql_close();

//CMP LOG NEW TA_TASK
        $trans = "Added TA task record ".$taskid.".";
        $tsql = str_replace("'","|",$sqltask);
        $tref = 'TA';
        include("inc_transaction_log.php");

//CMP LOG NEW TA_LOG
        $trans = "Added TA log record ".$logid.".";
        $tsql = str_replace("'","|",$sqllog);
        $tref = 'TA';
        include("inc_transaction_log.php");


        //Get tasktkid details
        $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$tasktkid."'";
        include("inc_db_con.php");
        $rowtkid = mysql_fetch_array($rs);
        mysql_close();
        //Get taskadduser details
        $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$taskadduser."'";
        include("inc_db_con.php");
        $rowadduser = mysql_fetch_array($rs);
        mysql_close();
        //Get topic details
        $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE id = ".$tasktopicid;
        include("inc_db_con.php");
        $rowtopic = mysql_fetch_array($rs);
        mysql_close();
        $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_urgency WHERE id = ".$taskurgencyid;
        include("inc_db_con.php");
        $rowurg = mysql_fetch_array($rs);
        mysql_close();
        //Set email variables
        $subject = "";
        $message = "";
        $to = $rowtkid['tkemail'];
        $from = $rowadduser['tkemail'];
        $recipientEmails = "";
        $toEmails = array();
        foreach ($recipientsArr as $key => $value) {
            if(isset($_POST['sendEmail'])) {
                    array_push($toEmails,$value);
            }else {
                if($tkid != $value) {//if the logged in user is one of the recipients, do not send email to that user
                    array_push($toEmails,$value);
                }
            }
        }
        if(strtolower($cmpcode) != 'wcc0001' && !empty($toEmails)) {
            $sql = "SELECT tkemail FROM assist_".$cmpcode."_timekeep t WHERE t.tkid=";
            for ($i = 0; $i < count($toEmails);$i++) {
                if($i < count($toEmails) - 1)
                    $sql .= "'".$toEmails[$i]."' OR t.tkid=";
                else
                    $sql .= "'".$toEmails[$i]."'";
            }
            include("inc_db_con.php");
            $strTo = "";
            while($rowFA = mysql_fetch_array($rs)) {
                $strTo .= $rowFA['tkemail'].",";
            }
            $userFrom = $rowadduser['tkname']." ".$rowadduser['tksurname'];
            $strToEmails = substr($strTo,0,-1);
            $subject = "New Task on Ignite Assist";
            $message .= $rowadduser['tkname']." ".$rowadduser['tksurname']." has assigned a new task to you:\n";
            $message .= "Prority: ".$rowurg['value']."\n";
            $message .= "Topic: ".$rowtopic['value']."\n";
            $message .= "Task Instructions:".str_replace(chr(10),"\n",$taskaction)."\n";
            $message .= "Task Deliverables:".str_replace(chr(10),"\n",(empty($taskdeliver) ? "N/A" : $taskdeliver))."\n";
//            $message .= "Task Instructions: ".$taskaction.chr(10);
//            $message .= "Task Deliverables: ".(empty($taskdeliver) ? "N/A" : $taskdeliver).chr(10);
            $message .= "Deadline: ".date("d F Y",$taskdeadline)."\n";
            $message .= "Please log onto Ignite Assist in order to update this task.\n";
            //Send email
//$from = "no-reply@ignite4u.co.za";
/*if(strpos($to,"@ignite4u.co.za")!==false && strpos($from,"@ignite4u.co.za")!==false)
{
	$from = "";
	$header = "";
}
else
{
	$header = "From: ".$from."\r\nReply-to: ".$from."\r\n";
}
*/
//$header = "From: ".$userFrom." <".$from.">\r\nReply-to: ".$from."\r\n";
        $headers = 'MIME-Version:1.0'."\r\n";
        $headers .= 'From: no-reply@ignite4u.co.za'."\r\n";
        $headers .= 'Reply-to: '.$userFrom.' <'.$from.'>'."\r\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();
            mail($strToEmails,$subject,$message,$headers);

            //Update transaction log
            $trans = "Sent email to the following for new task with id $taskid";
            foreach ($recipientsArr as $key => $value) {
                $trans .= getTK($value,$cmpcode,'tkn').",";
            }
            //chuck last comma away
            $strToEmails = substr($strTo,0,-1);
            //$trans = "Sent email to ".$rowtkid['tkname']." ".$rowtkid['tksurname']." for new task ".$taskid.".";
            $tsql = str_replace("'","|",$message);
            $tref = 'TA';
            include("inc_transaction_log.php");
//    mail("assist@ignite4u.co.za",$subject,$message,$header);
            $header = "";
            $message = "";
        }

echo("<form name=nw><input type=hidden name=tsk value=Y></form>");
?>
        <script language=JavaScript>
            var ntask = document.nw.tsk.value;
            if(ntask == "Y")
            {
                document.location.href = "view.php";
            }
        </script>
    </body>

</html>
