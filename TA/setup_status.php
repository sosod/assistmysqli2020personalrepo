<?php include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
 ?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate() {
    var addtxt = document.add.addtxt.value;
    var astate = document.add.addstate.value;
    var err = "";
    var valid8 = "true";

    if(addtxt.length > 0)
    {
    }
    else
    {
        err = err + "- Please enter a status description.";
        var target2 = document.getElementById('s2');
        target2.className = "req";
        target2.focus();
        valid8 = "false";
    }


    if(astate.length == 0)
    {
        document.add.addstate.value = 0;
    }
    else
    {
        if(astate != escape(astate))
        {
            var target1 = document.getElementById('s1');
            target1.className = "req";
            if(err.length > 0)
            {
                err = err+"\n";
            }
            else
            {
                target1.focus();
            }
            err = err+"- Please enter only numbers as the 'Default %'.";
            valid8 = "false";
        }
        else
        {
            if(astate > 99 || astate < 0)
            {
                var target1 = document.getElementById('s1');
                target1.className = "req";
                if(err.length > 0)
                {
                    err = err+"\n";
                }
                else
                {
                    target1.focus();
                }
                err = err+"- 'Default %' must be a number between 0 and 99 (inclusive).";
                valid8 = "false";
            }
        }
    }

    if(err.length>0 || valid8 == "false")
    {
        alert("Please complete all the fields as indicated below:\n"+err);
        return false;
    }
    else
    {
        if(valid8 == "true")
        {
            return true;
        }
    }
return false;
}

function editTop(id,val) {
    while(val.indexOf("_")>0)
    {
        val = val.replace("_"," ");
    }
    while(val.indexOf("|")>0)
    {
        val = val.replace("|","'");
    }
    if(confirm("Warning: Editing a status will affect all task updates associated with the status.\n\nAre you sure you wish to continue editing '"+val+"'?") == true)
    {
        document.location.href = "setup_status_edit.php?i="+id;
    }
}


</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Task Assist: Setup - Update Status</b></h1>
<p>&nbsp;</p>
<table border=1 width=400 cellpadding=3 cellspacing=0 style="border-collapse: collapse; border: 1px solid #ededed">
    <tr>
        <td class=tdheader height=25>ID</td>
        <td class=tdheader>Status</td>
        <td class=tdheader>Default %</td>
        <td class=tdheader>&nbsp;</td>
    </tr>
<form name=add method=post action=setup_status_add.php onsubmit="return Validate();" language=jscript>
    <?php include("inc_tr.php"); ?>
        <td class=tdheader width=30>&nbsp;</td>
        <td class=tdgeneral width=230><input type=text name=addtxt maxlength=50 size=30 clas=blank id=s2></td>
        <td class=tdgeneral width=100 align=center><input type=text name=addstate size=5 class=blank id=s1>%</td>
        <td class=tdheader width=40><input type=submit value=Add></td>
    </tr>
</form>
<?php
//GET CURRENT TOPIC DETAILS AND DISPLAY
$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE yn = 'Y' and custom = 'Y' ORDER BY value";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $val = $row['value'];
/*    $valarr = explode(" ",$val);
    $val = implode("_",$valarr);
    $valarr = explode("&#39",$val);
    $val = implode("|",$valarr);
*/
            $valjs = html_entity_decode($row['value'], ENT_QUOTES, "ISO-8859-1");
            $valjs = str_replace("'","\'",$valjs);
            $valjs = str_replace("\"","&#034;",$valjs);

?>
    <?php include("inc_tr.php"); ?>
        <td class=tdheader><?php echo($row['pkey']); ?></td>
        <td class=tdgeneral><?php echo($row['value']); ?></td>
        <td class=tdgeneral align=center><?php if($row['state']<=0) { echo("&nbsp;"); } else { echo($row['state']."%"); } ?></td>
        <td class=tdheader><?php echo("<input type=button value=Edit onclick=\"editTop(".$row["pkey"].",'".$valjs."')\">"); ?></td>
    </tr>
<?php
}
mysql_close();
?>
</table>
</body>

</html>
