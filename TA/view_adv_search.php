<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
$sid = $_GET['s'];
if($sid != 'ALL') {
    $sql = "SELECT value, heading FROM assist_".$cmpcode."_ta_list_status WHERE pkey = '".$sid."'";
    include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $statusheading = $row['heading'];
    mysql_close();
}else {
    $statusheading = "All incomplete tasks";
}
$sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = 'TA'
                                ORDER BY t.tkname, t.tksurname";
echo("<option name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>");
include("inc_db_con.php");
$strTaskOwner = "<select id='taskadduser' name='taskadduser'> <option value=X>--- SELECT ---</option>";
while($row = mysql_fetch_array($rs)) {
    $strTaskOwner .= "<option name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>";
}
$strTaskOwner .="</select>";
mysql_close();
$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE yn = 'Y' ORDER BY value";
include("inc_db_con.php");
$strtaskTopic = "<select size='1' name='tasktopicid' id='tasktopicid'>
                    <option value=X>--- SELECT ---</option>";
while($row = mysql_fetch_array($rs)) {
    $strtaskTopic .= "<option value='".$row['id']."'>".$row['value']."</option>";
}
$strtaskTopic .= "</select>";
mysql_close($con);
$sql = "SELECT DISTINCT t.tkid, CONCAT_WS(' ',t.tkname,t.tksurname) AS name
                            FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_ta_list_access a, assist_".$cmpcode."_menu_modules_users mmu
                                WHERE t.tkid = a.tkid AND t.tkid <> '0000' AND t.tkstatus = 1 AND t.tkid = mmu.usrtkid AND mmu.usrmodref = 'TA'
                                ORDER BY t.tkname, t.tksurname";

include("inc_db_con.php");
$size = mysql_num_rows($rs);
$strPopleTasked = "<select  id='tasktkid' name='tasktkid[]' multiple='multiple' size='".$size."'>";
while($row = mysql_fetch_array($rs)) {
    $strPopleTasked .= "<option name=".$row['tkid']." value=".$row['tkid'].">".$row['name']."</option>";
}
$strPopleTasked .= "</select>";
mysql_close($con);
?>
<html>

    <head>
        <link rel="stylesheet" href="/default.css" type="text/css">
        <?php include("inc_style.php"); ?>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
        <link href="../lib/calendar/css/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css"/>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-1.4.2.min.js"></script>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-ui-1.8.1.custom.min.js"></script>       
        <script type="text/javascript">
            $(function() {
                $("#taskadddate").datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
                $("#taskdeadline").datepicker({
                    showOn: 'both',
                    buttonImage: '../lib/orange/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd M yy',
                    altField: '#startDate',
                    altFormat: 'd_m_yy',
                    changeMonth:true,
                    changeYear:true
                });
            });
        </script>
        <script type="text/javascript">
                function validate(tform){
                    var message = 'Please enter your search criteria.';
                    for(var i = 0; i< tform.elements.length;i++){
                        if(tform.elements[i].type != 'hidden' && tform.elements[i].type != 'submit'){
                            if(tform.elements[i].value != '' && tform.elements[i].value != 'X'){
                               // alert(tform.elements[i].value);
                                return true;
                            }
                        }
                    }
                    alert(message);
                    return false;
                }
        </script>
    </head>
    <body>
        <h1 class=fc><b>Task Assist: View Tasks - <?php echo($statusheading); ?></b></h1>
        <div id="search-box">
            <form method="post" name="search_task" id="search_task" action="view_adv_search_results.php" onsubmit="return validate(this);">
                <table style="border: none;" >
                    <tr>
                        <td class="tdheader" >Task instructions</td>
                        <td style="border: none;" ><input type="text" name="taskaction" id="taskaction" size="60"/></td>

                    </tr>
                    <tr>
                        <td class="tdheader" >Task deliverables</td>
                        <td style="border: none;"><input type="text" name="taskdeliver" id="taskdeliver" size="60"/></td>
                    </tr>

                    <tr>

                        <td class="tdheader">Date tasked</td>
                        <td><input type=text size=15 id="taskadddate" name="taskadddate" readonly="readonly"></td>
                    </tr>
                    <tr>

                       <!-- <td class="tdheader">Task owner</td><td><?php //echo($strTaskOwner);?></td>-->
                    </tr>
                    <tr>

                        <td class="tdheader">Task topic</td>
                        <td><?php echo($strtaskTopic);?></td>
                    </tr>

                    <tr>

                        <td class="tdheader">Task deadline</td>
                        <td><input type=text size=15 id="taskdeadline" name="taskdeadline" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td class="tdheader" valign="top">Users tasked</td>
                        <td>
                            <i>Ctrl + left click to select multiple users</i><br />
                            <?php echo($strPopleTasked);?>
                        </td>
                    </tr>
                    <tr>
                    <input type="hidden" name="taskstatusid" id="taskstatusid" value="<?php echo($sid);?>" />
                    <td style="border: none;" colspan="2" align="right" ><input type="submit" name="btnSearch" id="btnSearch" value="Search" </td>
                    </tr>
                </table>
            </form>
        </div>
        <p>&nbsp;</p>
        <p><img src=/pics/tri_left.gif align=absmiddle ><a href='javascript:history.go(-1)' class=grey>Go back</a></p>
    </body>
</html>
