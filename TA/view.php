<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");

//GET USER'S ACCESS PERMISSIONS
$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_access WHERE tkid = '".$tkid."'";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$tact = $row['act'];
mysql_close();

//GET LIST OF POSSIBLE TASK STATUS INTO ARRAY
$mystat = "";
$sql = "SELECT pkey, heading FROM assist_".$cmpcode."_ta_list_status WHERE yn = 'Y' AND custom='N' AND id <> 'CN' AND id <> 'CL' ORDER BY sort";
//$sql = "SELECT pkey, heading,COUNT(t.taskid) AS count FROM assist_".$cmpcode."_ta_list_status s LEFT JOIN assist_".$cmpcode."_ta_task t ON (s.pkey=t.taskstatusid) LEFT JOIN assist_".$cmpcode."_ta_task_recipients r ON(t.taskid=r.taskid)
// WHERE s.yn = 'Y' AND s.id <> 'CN' AND s.id <> 'CL' AND (r.tasktkid='".$tkid."' OR r.tasktkid IS NULL) GROUP BY s.pkey ORDER BY s.heading";
//echo $sql;
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $id = $row['pkey'];
    $mystat[$id]['c'] = 0;
    $mystat[$id]['i'] = $id;
    $mystat[$id]['h'] = $row['heading'];
    
    $allstat[$id]['c'] = 0;
    $allstat[$id]['i'] = $id;
    $allstat[$id]['h'] = $row['heading'];

    $ownstat[$id]['c'] = 0;
    $ownstat[$id]['i'] = $id;
    $ownstat[$id]['h'] = $row['heading'];
}
$sql = "SELECT pkey, heading FROM assist_".$cmpcode."_ta_list_status WHERE yn = 'Y' AND custom='Y' AND id <> 'CN' AND id <> 'CL' ORDER BY heading";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $id = $row['pkey'];
    $mystat[$id]['c'] = 0;
    $mystat[$id]['i'] = $id;
    $mystat[$id]['h'] = $row['heading'];

    $allstat[$id]['c'] = 0;
    $allstat[$id]['i'] = $id;
    $allstat[$id]['h'] = $row['heading'];

    $ownstat[$id]['c'] = 0;
    $ownstat[$id]['i'] = $id;
    $ownstat[$id]['h'] = $row['heading'];
}
mysql_close();
$mystat['ALL']['c'] = 0;
$mystat['ALL']['i'] = 0;
$mystat['ALL']['h'] = "All incomplete tasks";
$allstat['ALL']['c'] = 0;
$allstat['ALL']['i'] = 0;
$allstat['ALL']['h'] = "All incomplete tasks";
$ownstat['ALL']['c'] = 0;
$ownstat['ALL']['i'] = 0;
$ownstat['ALL']['h'] = "All incomplete tasks";

//COUNT NUMBER OF TASKS PER STATUS FOR THE CURRENT USER
//

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Task Assist: View Tasks</b></h1>
<p style="margin-top: -10px;">&nbsp;</p>
<table cellpadding=3 cellspacing=0 border=1 <?php if($tact > 20) { echo("width=800"); } else { echo("width=400"); } ?>>
    <tr>
        <td width=200 class=tdheader>My Tasks</td>
<?php if($tact > 20) { ?>
        <td width=200 class=tdheader>Tasks I've Assigned</td>
        <td width=200 class=tdheader>All Tasks</td>
<?php } ?>
    </tr>
    <tr>
        <td class=tdgeneral align=center valign=center>
            <table border=0 cellpadding=0 cellspacing=0 style="border-color: #ffffff;">
                <tr>
                    <td class=tdgeneral valign=top>
            <ul class=ul2>
<?php

$sql = "SELECT s.pkey, count(t.taskid) AS tcount, s.heading FROM assist_".$cmpcode."_ta_list_status s, assist_".$cmpcode."_ta_task t,
    assist_".$cmpcode."_ta_task_recipients r ";
$sql .= "WHERE t.taskstatusid = s.pkey AND r.tasktkid = '".$tkid."' AND t.taskid=r.taskid AND s.yn = 'Y' AND s.id <> 'CN' AND s.id <> 'CL'";
$sql .= "GROUP BY s.pkey,r.tasktkid ORDER BY s.sort";
/*$sql ="SELECT s.pkey,COUNT(t.taskid) AS tcount, s.heading FROM assist_".$cmpcode."_ta_list_status s INNER JOIN  assist_".$cmpcode."_ta_task t
    ON (s.pkey=t.taskstatusid) INNER JOIN  assist_".$cmpcode."_ta_task_recipients r ON (t.taskid=r.taskid)
    WHERE  r.tasktkid = '".$tkid."' AND s.yn = 'Y' AND s.id <> 'CN' AND s.id <> 'CL' GROUP BY s.pkey";*/
include("inc_db_con.php");

while($row = mysql_fetch_array($rs))
{
   
    $id = $row['pkey'];
    $tc = $row['tcount'];
    $mystat[$id]['c'] = $tc;
}
mysql_close();

foreach($mystat as $ts)
{
    $mystat['ALL']['c'] = $mystat['ALL']['c'] + $ts['c'];
}
//debug($mystat);

//DISPLAY TASK STATUS'
foreach($mystat as $ts)
{
    if($ts['i'] > 0)
    {
        if($ts['c'] > 0)
        {
            ?><li><a href=view_task.php?s=<?php echo($ts['i']); ?> style='text-decoration: none'><?php echo($ts['h']); ?></a> (<?php echo($ts['c']);?>)</li><?php
        }
        else
        {
            echo("<li>".$ts['h']." (0)</li>");
        }
    }
}
?>
<?php
    if($mystat['ALL']['c'] > 0)
    {
         ?>           <li><a href=view_task.php?s=ALL style="text-decoration: none">All incomplete tasks</a> (<?php echo($mystat['ALL']['c']); ?>)</li><?php
    }
    else
    {
       echo("<li>All incomplete tasks (0)</li>");
    }
?>
<?php
/*$sql = "SELECT s.pkey, count(t.taskid) AS tcount, s.heading FROM assist_".$cmpcode."_ta_list_status s, assist_".$cmpcode."_ta_task t ";
$sql .= "WHERE t.taskstatusid = s.pkey AND t.tasktkid = '".$tkid."' AND s.yn = 'Y' AND s.id = 'CL'";
$sql .= "GROUP BY s.pkey";*/
$sql = "
    SELECT s.pkey,COUNT(t.taskid) AS tcount, s.heading FROM assist_".$cmpcode."_ta_list_status s INNER JOIN  assist_".$cmpcode."_ta_task t
    ON (s.pkey=t.taskstatusid) INNER JOIN  assist_".$cmpcode."_ta_task_recipients r ON (t.taskid=r.taskid)
    WHERE  r.tasktkid = '".$tkid."' AND s.yn = 'Y' AND s.id = 'CL' GROUP BY s.pkey
";

include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    if($row['tcount'] > 0)
    {
        ?>            <li><a href=view_task.php?s=<?php echo($row['pkey']); ?> style='text-decoration: none'>Completed tasks</a> (<?php echo($row['tcount']);?>)</li><?php
    }
    else
    {
        echo("            <li>Completed tasks (0)</li>");
    }
mysql_close();
?>
        </ul></td></tr></table>
</td>
<?php if($tact > 20) { ?>

        <td class=tdgeneral valign=top align=center>
            <table border=0 cellpadding=0 cellspacing=0 style="border-color: #ffffff;">
                <tr>
                    <td class=tdgeneral valign=top>
            <ul class=ul2>
<?php
$sql = "SELECT s.pkey, count(t.taskid) AS tcount, s.heading FROM assist_".$cmpcode."_ta_list_status s, assist_".$cmpcode."_ta_task t ";
$sql .= "WHERE t.taskstatusid = s.pkey AND t.taskadduser = '".$tkid."' AND s.yn = 'Y' AND s.id <> 'CN' AND s.id <> 'CL' ";
$sql .= "GROUP BY s.pkey";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $id = $row['pkey'];
    $tc = $row['tcount'];
    $ownstat[$id]['c'] = $tc;
}
mysql_close();
// print_r($ownstat);
foreach($ownstat as $ts)
{
    $ownstat['ALL']['c'] = $ownstat['ALL']['c'] + $ts['c'];
}
// print_r($ownstat);
//DISPLAY TASK STATUS'
foreach($ownstat as $ts)
{
    if($ts['i'] > 0)
    {
        if($ts['c'] > 0)
        {
            ?>            <li><a href=view_own.php?s=<?php echo($ts['i']); ?> style='text-decoration: none'><?php echo($ts['h']); ?></a> (<?php echo($ts['c']);?>)</li><?php
        }
        else
        {
            echo("            <li>".$ts['h']." (0)</li>");
        }
    }
}
?>
<?php
    if($ownstat['ALL']['c'] > 0)
    {
         ?>           <li><a href=view_own.php?s=ALL style="text-decoration: none">All incomplete tasks</a> (<?php echo($ownstat['ALL']['c']); ?>)</li><?php
    }
    else
    {
        echo("            <li>All incomplete tasks (0)</li>");
    }
?>
<?php
$sql = "SELECT s.pkey, count(t.taskid) AS tcount, s.heading FROM assist_".$cmpcode."_ta_list_status s, assist_".$cmpcode."_ta_task t ";
$sql .= "WHERE t.taskstatusid = s.pkey AND t.taskadduser = '".$tkid."' AND s.yn = 'Y' AND s.id = 'CL' ";
$sql .= "GROUP BY s.pkey";
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    if($row['tcount'] > 0)
    {
        ?>            <li><a href=view_own.php?s=<?php echo($row['pkey']); ?> style='text-decoration: none'>Completed tasks</a> (<?php echo($row['tcount']);?>)</li><?php
    }
    else
    {
        echo("            <li>Completed tasks (0)</li>");
    }
mysql_close();
?>
        </ul></td></tr></table><?php //print_r($ownstat); ?>
</td>
        <td class=tdgeneral valign=top align=center>
            <table border=0 cellpadding=0 cellspacing=0 style="border-color: #ffffff;">
                <tr>
                    <td class=tdgeneral valign=top>
            <ul class=ul2>
<?php
$sql = "SELECT s.pkey, count(t.taskid) AS tcount, s.heading FROM assist_".$cmpcode."_ta_list_status s, assist_".$cmpcode."_ta_task t ";
$sql .= "WHERE t.taskstatusid = s.pkey AND s.yn = 'Y' AND s.id <> 'CN' AND s.id <> 'CL' ";
$sql .= "GROUP BY s.pkey";
//echo $sql;
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $allstat[$row['pkey']]['c'] = $row['tcount'];
}
mysql_close($con);

foreach($allstat as $ts)
{
    $allstat['ALL']['c'] = $allstat['ALL']['c'] + $ts['c'];
}
//debug($allstat);
//DISPLAY TASK STATUS'
foreach($allstat as $ts)
{
    if($ts['i'] > 0)
    {
        if($ts['c'] > 0)
        {
            ?>            <li><a href=view_all.php?s=<?php echo($ts['i']); ?> style='text-decoration: none'><?php echo($ts['h']); ?></a> (<?php echo($ts['c']);?>)</li><?php
        }
        else
        {
            echo("            <li>".$ts['h']." (0)</li>");
        }
    }
}
?>
<?php
    if($allstat['ALL']['c'] > 0)
    {
         ?>           <li><a href=view_all.php?s=ALL style="text-decoration: none">All incomplete tasks</a> (<?php echo($allstat['ALL']['c']); ?>)</li><?php
    }
    else
    {
        echo("            <li>All incomplete tasks (0)</li>");
    }
?>
<?php
$sql = "SELECT s.pkey, count(t.taskid) AS tcount, s.heading FROM assist_".$cmpcode."_ta_list_status s, assist_".$cmpcode."_ta_task t ";
$sql .= "WHERE t.taskstatusid = s.pkey AND s.yn = 'Y' AND t.taskstatusid =1 ";
$sql .= "GROUP BY s.pkey";
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    if($row['tcount'] > 0)
    {
        ?>            <li><a href=view_all.php?s=<?php echo($row['pkey']); ?> style='text-decoration: none'>Completed tasks</a> (<?php echo($row['tcount']);?>)</li><?php
    }
    else
    {
        echo("            <li>Completed tasks (0)</li>");
    }
mysql_close();
?>
        </ul></td></tr></table>
</td>
<?php } ?>
    </tr>
</table>

</body>

</html>
