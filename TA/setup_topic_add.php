<?php include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
 ?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Task Assist: Setup - Update Topics - Add new</b></h1>
<p>&nbsp;</p>
<?php
//GET NEW TOPIC DETAILS
$txt = code($_POST['addtxt']);
//ADD NEW TOPIC
$sql = "INSERT INTO assist_".$cmpcode."_ta_list_topic SET value = '".$txt."', yn = 'Y'";
include("inc_db_con.php");
//SET TRANSACTION LOG DETAILS
$tsql = $sql;
$trans = "New topic ".$txt." added to TA.";
$tref = "TA";
//PERFORM TRANSACTION LOG UPDATE
include("inc_transaction_log.php");

//SET FORM FOR REDIRECT
echo("<form name=add><input type=hidden name=txt value=Y></form>");

?>
<script language=JavaScript>
//IF REDIRECT FORM EXISTS THEN UPDATE IS DONE - SEND ON TO SETUP_TOPIC PAGE
var txt = document.add.txt.value;
if(txt == 'Y')
    document.location.href="setup_topic.php";
</script>
</body>

</html>
