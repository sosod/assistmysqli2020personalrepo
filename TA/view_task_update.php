<?php
include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");

//GET USER ACCESS PERMISSIONS
$sql = "SELECT * FROM assist_".$cmpcode."_ta_list_access WHERE tkid = '".$tkid."'";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$tact = $row['act'];
mysql_close();

//GET TASK ID
$taskid = $_GET['i'];

//SET REMINDER
$tremind = $_GET['remind'];
if(strlen($tremind) > 0) {
    $tr = substr($tremind,1,strlen($tremind)-1);
    $sql = "UPDATE assist_".$cmpcode."_ta_task SET taskremind = '".$tr."' WHERE taskid = ".$taskid;
    include("inc_db_con.php");
    $tsql = $sql;
    $tref = "TA";
    $trans = "Set reminder ".$tr." for task ".$taskid.".";
    include("inc_transaction_log.php");
}

//GET TASK DETAILS INTO ARRAY TASK
$sql = "SELECT * FROM assist_".$cmpcode."_ta_task WHERE taskid = ".$taskid;
include("inc_db_con.php");
$task = mysql_fetch_array($rs);
mysql_close();

?>
<html>

    <head>
        <meta http-equiv="Content-Language" content="en-za">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>www.Ignite4u.co.za</title>
        <link href="../lib/calendar/css/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css"/>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-1.4.2.min.js"></script>
        <script type ="text/javascript" src="../lib/calendar/js/jquery-ui-1.8.1.custom.min.js"></script>
        <script type="text/javascript">
            function delAttachment(id,attachment_id){
                var answer = confirm('Are you sure you wish to completely delete this file? Click Ok to proceed otherwise cancel.');
                if(answer){
                    document.location.href = "delete_update_attachment.php?logid="+id+"&attachment_id="+attachment_id;
                }
            }
        </script>
    </head>
    <script type="text/javascript">
        function validateUpdates(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        function statusPerc(me) {
            var stat = me.value;
            stat = parseInt(stat);
            if(stat == 3 || stat > 5)
            {
                document.taskupdate.logstate.style.backgroundColor="#FFFFFF";
                document.taskupdate.logstate.disabled = false;
                if(stat > 5 && lstat[stat] > 0)
                {
                    document.taskupdate.logstate.value = lstat[stat];
                }
                else
                {
                    document.taskupdate.logstate.value = lstat[0];
                }
            }
            else
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }
        }

        function Validate(tform) {
            var logupdate = tform.logupdate.value;
            var logstatusid = tform.logstatusid.value;
            var logstate = tform.logstate.value;



            if(logupdate.length == 0)
            {
                alert("Please complete all the fields.");
                return false;
            }
            else
            {
                if(logupdate.search(/select/i)>0 && logupdate.search(/user/i)>0)
                {
                    //alert("error in comment");
                    logupdate = logupdate.replace(/select/i,"choose");
                    tform.logupdate.value = logupdate;
                }
                if(logstatusid == "3" && (isNaN(parseInt(logstate)) || !(escape(logstate)==logstate) || logstate.length==0))
                {
                    alert("Please complete all the fields.\n\nPlease Note:\nOnly numbers may be entered \ninto the % complete box.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        function redirect(taskid){
            document.location.href = "edit_task.php?taskid="+taskid;
        }

        function download(path){
            document.location.href = "download.php?path="+path;
        }


        $(function() {
            $("#datepicker").datepicker({
                showOn: 'both',
                buttonImage: '../lib/orange/calendar.gif',
                buttonImageOnly: true,
                dateFormat: 'dd M yy',
                altField: '#startDate',
                altFormat: 'd_m_yy',
                changeMonth:true,
                changeYear:true		});
        });
        $(document).ready(function(){
            $('#attachlink').click(function(){
                $('<tr><td align="right" colspan="2" ><input type="file" name=attachments[] size="30"/></td>').insertAfter('#firstdoc');
            })
        });

    </script>

    <link rel="stylesheet" href="/default.css" type="text/css">
    <?php include("inc_style.php"); ?>
    <base target="main">
    <body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
        <h1 class=fc><b>Task Assist: Update Task</b></h1>
        <h2 class=fc>Task Status</h2>
        <table border="1" id="table1" cellspacing="0" cellpadding="4" width=600>
            <tr>
                <td class=tdheaderl valign="top" width=140><b>Assigned to:</b></td>
                <td class=tdgeneral valign="top" width=460><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep u INNER JOIN assist_".$cmpcode."_ta_task_recipients r ON (u.tkid=r.tasktkid)
                WHERE r.taskid= '".$task['taskid']."' ";
                    // echo $sql;
                    include("inc_db_con.php");
                    $assignees = "";
                    while($row = mysql_fetch_array($rs)) {
                        $assignees .= $row['tkname']." ".$row['tksurname'].", ";
                    }
                    $assignees = substr(trim($assignees), 0,-1);
                    echo($assignees);
                    mysql_close();
                    ?>
                </td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Assigned by:</b></td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$task['taskadduser']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['tkname']." ".$row['tksurname']);
                    mysql_close();
                    ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Topic:</td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE id = '".$task['tasktopicid']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['value']);
                    $topicval = $row['value'];
                    $topicyn = $row['yn'];
                    mysql_close();
                    ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Priority:</td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_urgency WHERE id = '".$task['taskurgencyid']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['value']);
                    mysql_close();
                    ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Status:</td>
                <td class=tdgeneral valign="top"><?php
                    $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE pkey = '".$task['taskstatusid']."'";
                    include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['value']);
                    if($task['taskstatusid'] == "3") {
                        echo(" (".$task['taskstate']."%) ");
                    }
                    else {
                        if($task['taskstatusid'] != "5") {
                            echo(" (".$row['state']."%) ");
                        }
                    }
                    mysql_close();
                    ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Deadline:</td>
                <td class=tdgeneral valign="top"><?php echo(date("d-M-Y",$task['taskdeadline']));?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Task Instructions:</b></td>
                <td class=tdgeneral>
                    <?php
                    $action = explode(chr(10),$task['taskaction']);
                    $taskaction = implode("<br>",$action);
                    echo($taskaction);
                    ?></td>
            </tr>
            <tr>
                <td class=tdheaderl valign="top"><b>Task Deliverables:</b></td>
                <td class=tdgeneral>
                    <?php
                    $deliver = explode(chr(10),$task['taskdeliver']);
                    $taskdeliver = implode("<br>",$deliver);
                    echo($taskdeliver);
                    ?></td>
            </tr>

<!--<tr id="firstdoc">

    <td class=tdgeneral colspan="2" align="right"><input type="file" name="attachments[]" id="attachment" size="30"/></td>
</tr>
<tr>
    <td align="right" colspan="2"><a href="javascript:void(0)" id="attachlink">Attach another file</a></td>
</tr>-->


            <?php
            if($topicval == 'Travel Requests' && $topicyn = 'N') {
                $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = 'TATVL' AND udfivalue = 'Travel Request'";
                include("inc_db_con.php");
                $udfi = mysql_fetch_array($rs);
                mysql_close();
                if(strlen($udfi['udfiid'])>0) {
                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$udfi['udfiid'];
                    include("inc_db_con2.php");
                    $udf = mysql_num_rows($rs2);
                    $row2 = mysql_fetch_array($rs2);
                    mysql_close($con2);
                    ?>
            <tr>
                <td class=tdheaderl valign="top"><b>Travel Request Ref.:</b></td>
                <td class=tdgeneral valign="top">
                            <?php
                            if($udf>0) {
                                echo("<a href=../TVLA/view_my.php?ref=".$row2['udfvalue']." target=_blank>".$row2['udfvalue']."</a>");
                            }
                            else {
                                echo("N/A");
                            }
                            ?>
                    &nbsp;</td>
            </tr>
                    <?php
                }
            }
            $sql = "SELECT * FROM assist_".$cmpcode."_udfindex WHERE udfiref = '".$tref."' AND udfiyn = 'Y' ORDER BY udfisort, udfivalue";
            include("inc_db_con.php");
            while($row = mysql_fetch_array($rs)) {
                $sql2 = "SELECT * FROM assist_".$cmpcode."_udf WHERE udfnum = ".$taskid." AND udfindex = ".$row['udfiid'];
                include("inc_db_con2.php");
                $udf = mysql_num_rows($rs2);
                $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                ?>
            <tr>
                <td class=tdheaderl valign="top"><b><?php echo($row['udfivalue']); ?>:</b></td>
                <td class=tdgeneral valign="top">
                        <?php
                        switch($row['udfilist']) {
                            case "Y":
                                if(checkIntRef($row2['udfvalue'])) {
                                    $sql2 = "SELECT * FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = ".$row['udfiid']." AND udfvid = ".$row2['udfvalue']." ORDER BY udfvvalue";
                                    include("inc_db_con2.php");
                                    $row3 = mysql_fetch_array($rs2);
                                    echo($row3['udfvvalue']);
                                    mysql_close($rs2);
                                }else {
                                    echo ("N/A");
                                }
                                break;
                            case "T":
                                if($row2['udfvalue'])
                                    echo $row2['udfvalue'];
                                else
                                    echo "N/A";
                                break;
                            default:
                                echo(str_replace(chr(10),"<br>",$row2['udfvalue']));
                                break;
                        }
                        ?>
                    &nbsp;</td>
            </tr>
                <?php
            }
            mysql_close();
            ?>
            <?php
            $sql = "SELECT * FROM  assist_".$cmpcode."_ta_task_attachments AS att WHERE att.taskid = ".$taskid;
            include("inc_db_con.php");
            if(mysql_num_rows($rs) > 0) {
                echo ("<tr><td  class=tdheaderl valign='top'>Attachment(s):</td><td><table  style='border:none;'>");
                while($row = mysql_fetch_assoc($rs)) {
                    $taskid = $row['taskid'];
                    $taskattach_id = $row['id'];
                    $file = "../files/$cmpcode/TA/".$row['system_filename'];
                    if(!file_exists($file))
                        continue;
                    echo ("<tr><td style='border:none;'><a href='javascript:download(\"$file\")' >");
                    echo($row['original_filename']);
                    echo ("</a></td>");
                    //if($task['taskadduser'] == $tkid)
                    //   echo ("<td  style='border:none;'><input type='button' name='deleteAttachment' id='deleteAttachment' value='Delete' onclick='deleteAttachment($taskid,$taskattach_id)' /></td></tr>")
                    ?>
                    <?php    }
                echo ("</table></td></tr>");
            }else {
                // echo ("<i>No attachments found.</i>");
            }
            ?>
            <?php
            if($tkid == $task['taskadduser']) {?>
            <tr><td colspan="2"><input type="button" value="Edit"  onclick="redirect(<?php echo($taskid);?>);"/></td></tr>
                <?php }?>
        </table>

        <h2 class=fc>Task Updates</h2>
        <form name=taskupdate method=post action=view_task_update_process.php onsubmit="return validateUpdates(this);" language=jscript enctype="multipart/form-data">
            <input type=hidden name=taskid value=<?php echo($taskid);?>>
            <table border="1" id="table1" cellspacing="0" cellpadding="5" width=600>
                <tr>
                    <td valign="top" class=tdheader>Date</td>
                    <td valign="top" class=tdheader>Task Update</td>
                    <td valign="top" class=tdheader>Task Status</td>
                </tr>
                <?php
//IF THE TASK IS NOT CLOSED OFFER THE OPTION TO UPDATE
                if($task['taskstatusid'] != "1") {
                    ?>
                <tr>
                    <td valign="top" class=tdgeneral align=center><?php echo(date("d-M-Y H:i",$today)); ?></td>
                    <td valign="top" class=tdgeneral><textarea rows="5" name="logupdate" cols="35"></textarea></td>
                    <td valign="top" class=tdgeneral>
                        <select size="1" name="logstatusid" onchange=statusPerc(this)>
                                <?php
                                $lstat = "lstat[0] = ".$task['taskstate'].";";
                                $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE yn = 'Y' AND pkey NOT IN (2,4) ORDER BY sort";
                                include("inc_db_con.php");
                                while($row = mysql_fetch_array($rs)) {
                                    if( ($row['pkey']==3 && $task['taskstatusid']==4 ) || ($task['taskstatusid']==$row['pkey']))  {
                                        echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                    } else {
                                        echo("<option  value=".$row['pkey'].">".$row['value']."</option>");
                                    }
                                    $lstat = $lstat.chr(10)."lstat[".$row['pkey']."] = ".$row['state'].";";
                                }
                                    //echo("<option ".($task['taskstatusid'] == $row['pkey'] ? "selected=selected" : "")." value=".$row['pkey'].">".$row['value']."</option>");
                                    /*
                                    if($task['taskstatusid'] != "5") {
                                        if($row['pkey'] == "3") {
                                            echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                            $lstat = $lstat.chr(10)."lstat[".$row['pkey']."] = ".$row['state'].";";
                                        }
                                        else {
                                            if($row['pkey'] != "4") {
                                                echo("<option value=".$row['pkey'].">".$row['value']."</option>");
                                                $lstat = $lstat.chr(10)."lstat[".$row['pkey']."] = ".$row['state'].";";
                                            }
                                        }
                                    }
                                    else {
                                        if($row['pkey'] == "5") {
                                            echo("<option selected value=".$row['pkey'].">".$row['value']."</option>");
                                            $lstat = $lstat.chr(10)."lstat[".$row['pkey']."] = ".$row['state'].";";
                                        }
                                        else {
                                            if($row['pkey'] != "4") {
                                                echo("<option value=".$row['pkey'].">".$row['value']."</option>");
                                                $lstat = $lstat.chr(10)."lstat[".$row['pkey']."] = ".$row['state'].";";
                                            }
                                        }
                                    }
                                     * */

                                
                                mysql_close();
                                ?>
                        </select><br>&nbsp;
                        <script language=JavaScript>
                            var lstat = new Array();
    <?php                   echo($lstat); ?>
                        </script>
                        <br>
                        <input type="text" name="logstate" size="4" value=<?php echo($task['taskstate']); ?>>% complete</td>
                </tr>
                <tr id="firstdoc">
                    <td class=tdgeneral colspan="2" align="right"><input type="file" name="attachments[]" id="attachment" size="30"/></td>
                </tr>
                <tr>
                    <td align="right" colspan="2"><a href="javascript:void(0)" id="attachlink">Attach another file</a></td>
                </tr>
                <tr>
                    <td valign="top" colspan="3" class=tdgeneral>
                        <input type="submit" value="Submit" name="B1">
                        <input type="reset" value="Reset" name="B2"></td>
                </tr>
                    <?php
                }
                else {
                    echo("<input type=hidden name=logstatusid value=CL>");
                }
//DISPLAY PAST UPDATES
                $sql = "SELECT * FROM assist_".$cmpcode."_ta_log l, assist_".$cmpcode."_ta_list_status s WHERE l.logstatusid = s.pkey AND l.logtaskid = ".$taskid." ORDER BY logid DESC";
                include("inc_db_con.php");
                $l = mysql_num_rows($rs);
                $l2 = 0;
                while($row = mysql_fetch_array($rs)) {
                    $l2++;
                    if($l > $l2) {
                        ?>
                <tr>
                    <td valign="top" class=tdgeneral align=center><?php echo(date("d-M-Y H:i",$row['logdate'])); ?>&nbsp;</td>
                    <td valign="top" class=tdgeneral><?php
                                $logid = $row['logid'];
                                $logarr = explode(chr(10),$row['logupdate']);
                                $logupdate = html_entity_decode(implode("<br>",$logarr));
                                $sql = "SELECT * FROM assist_".$cmpcode."_ta_task_attachments WHERE logid=".$row['logid'];
                                $rs2 = mysql_query($sql);
                                $attachments = "";
                                while($row3 = mysql_fetch_array($rs2)) {
                                    $file = "../files/$cmpcode/TA/".$row3['system_filename'];
                                    if(!file_exists($file))
                                        continue;
                                    $attachments .= "<a href='javascript:download(\"".$file."\")'>".$row3['original_filename']."</a>&nbsp;&nbsp;";
                                    if($task['tasktkid'] == $tkid) {
                                        $button = "<input type='button' onclick='delAttachment(\"$logid\",\"".$row3['id']."\")' value='Delete' id='deleteAttachment' name='deleteAttachment'><br />";
                                    }
                                    $attachments .= $button;
                                }
                                echo($logupdate);
                                if(!empty($attachments))
                                    echo "<br />Attachments:<br />".$attachments;

                                ?>&nbsp;</td>
                    <td valign="top" class=tdgeneral><?php
                                echo($row['value']);
                                if($row['pkey'] == "3" || $row['pkey'] > 5) {
                                    echo("<br>(".$row['logstate']."%)");
                                }
                                ?>&nbsp;</td>
                </tr>
                        <?php
                    }
                }
                ?>
            </table>

        </form>
        <script language=JavaScript>
            var statu = document.taskupdate.logstatusid.value;
            if(statu == "5")
            {
                document.taskupdate.logstate.value = "";
                document.taskupdate.logstate.style.backgroundColor="#FFCC7B";
                document.taskupdate.logstate.disabled = true;
            }
        </script>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </body>

</html>
