<?php include("inc_ignite.php");
include("inc_logfile.php");
include("inc_errorlog.php");
 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Task Assist: Task Update</b></h1>
<?php
//GET DATA FROM FORM
$logdate = $today;
$logupdate = "Task canceled by task owner.";
$logstatusid = "2";
$logstate = 0;
$taskid = $_GET['i'];


//GET STATE FOR GIVEN STATUS IF NOT IP
if($logstatusid != "IP")
{
    $sql = "SELECT state FROM assist_".$cmpcode."_ta_list_status WHERE pkey = '".$logstatusid."'";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $logstate = $row['state'];
    mysql_close();
}

//GET OLD STATUSID - USED FOR REDIRECT AFTER UPDATE
$sql = "SELECT * FROM assist_".$cmpcode."_ta_task WHERE ";
$sql .= "taskid = ".$taskid;
include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
    $oldstatus = $row['taskstatusid'];
    $tasktkid = $row['tasktkid'];
    $taskadduser = $row['taskadduser'];
    $tasktopicid = $row['tasktopicid'];
    $taskaction = $row['taskaction'];
    $taskdeliver = $row['taskdeliver'];
    $taskdeadline = $row['taskdeadline'];
mysql_close();
$row = "";

//UPDATE TA_TASK RECORD
$sql = "UPDATE assist_".$cmpcode."_ta_task SET ";
$sql .= "taskstatusid = '".$logstatusid."', ";
$sql .= "taskstate = ".$logstate." WHERE ";
$sql .= "taskid = ".$taskid;
$sqltask = $sql;
include("inc_db_con.php");

//move all documents associated with this task to deleted folder
 $deletedFolder = "../files/$cmpcode/deleted";
 checkFolder("deleted");
 $sql = "SELECT * FROM assist_".$cmpcode."_ta_task_attachments WHERE taskid=$taskid";
 include("inc_db_con.php");
 while($row = mysql_fetch_array($rs)){
     $original_filename = $row['original_filename'];
     $system_filename = $row['system_filename'];
     $newfile = "$deletedFolder/TA_".$taskid."_".date("YmdHi",$today)."_$original_filename";
     $file = "../files/$cmpcode/TA/".$system_filename;
     if(copy($file,$newfile)){
        unlink($file);
     }
 }
//GET TASK DETAILS
$sql = "SELECT * FROM assist_".$cmpcode."_ta_task WHERE taskid = ".$taskid;
include("inc_db_con.php");
$task = mysql_fetch_array($rs);
mysql_close();


//ADD RECORD TO TA_LOG
$sql = "INSERT INTO assist_".$cmpcode."_ta_log SET ";
$sql .= "logdate = '".$today."', ";
$sql .= "logtkid = '".$tkid."', ";
$sql .= "logupdate = '".$logupdate."', ";
$sql .= "logstatusid = '".$logstatusid."', ";
$sql .= "logstate = ".$logstate.", ";
$sql .= "logemail = 'N', ";
$sql .= "logsubmittkid = '".$tkid."', ";
$sql .= "logtaskid = '".$taskid."', ";
$sql .= "logtasktkid = '".$task['tasktkid']."'";
$sqllog = $sql;
include("inc_db_con.php");

//GET LOG ID
$sql = "SELECT logid FROM assist_".$cmpcode."_ta_log WHERE logtaskid = ".$taskid." ORDER BY logid DESC";
include("inc_db_con.php");
$row = mysql_fetch_array($rs);
$logid = $row['logid'];
mysql_close();
//delete any associated attachments


//CMP LOG UPDATE TA_TASK
$trans = "Updated TA task record ".$taskid.".";
$tsql = str_replace("'","|",$sqltask);
$tref = "TA";
include("inc_transaction_log.php");

//CMP LOG NEW TA_LOG
$trans = "Added TA log record ".$logid.".";
$tsql = str_replace("'","|",$sqllog);
$tref = 'TA';
include("inc_transaction_log.php");

/*//NOTIFY TASK-ADDUSER
    //Get tasktkid details
    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$tasktkid."'";
    include("inc_db_con.php");
        $rowtkid = mysql_fetch_array($rs);
    mysql_close();
    //Get taskadduser details
    $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$taskadduser."'";
    include("inc_db_con.php");
        $rowadduser = mysql_fetch_array($rs);
    mysql_close();
    //Get topic details
    $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_topic WHERE id = ".$tasktopicid;
    include("inc_db_con.php");
        $rowtopic = mysql_fetch_array($rs);
    mysql_close();
    //Get status details
    $sql = "SELECT * FROM assist_".$cmpcode."_ta_list_status WHERE id = '".$logstatusid."'";
    include("inc_db_con.php");
        $rowstatus = mysql_fetch_array($rs);
    mysql_close();
    //Set email variables
    $subject = "";
    $message = "";
    $to = $rowadduser['tkemail'];
    $from = $rowtkid['tkemail'];
    $subject = "Update to Task on Ignite Assist";
    $message = "<font face=arial>";
    $message .= "<p><small><i>".$rowtkid['tkname']." ".$rowtkid['tksurname']." has updated a task assigned to them by you:</i><br> <br>";
    $message .= "<b><u>Task details:</u></b><br>";
    $message .= " <b>Topic:</b> ".$rowtopic['value']."<br>";
    $message .= " <b>Task Instructions:</b><br>".str_replace(chr(10),"<br>",$taskaction)."<br>";
    $message .= " <b>Task Deliverables:</b><br>".str_replace(chr(10),"<br>",$taskdeliver)."<br>";
    $message .= " <b>Deadline:</b> ".date("d F Y",$taskdeadline)."<br> <br>";
    $message .= "<b><u>Update details:</u></b><br>";
    $message .= " <b>Task Update:</b><br>".str_replace(chr(10),"<br>",$logupdate)."<br>";
    $message .= " <b>Task Status:</b> ".$rowstatus['value'];
    if($logstatusid == "IP") {$message .= " (".$logstate."%)<br>";} else {$message .= "<br>";}
    $message .= "<p><i>Please log onto Ignite Assist in order to view all the updates to this task.</i></p>";
    $message .= "</small></font>";
    //Send email
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    mail($to,$subject,$message,$header);
    //Update transaction log
        $trans = "Sent email to ".$rowtkid['tkname']." ".$rowtkid['tksurname']." for new task ".$taskid.".";
        $tsql = str_replace("'","|",$message);
        $tref = 'TA';
        include("inc_transaction_log.php");
    $header = "";
    $message = "";
*/

//CREATE FORM TO REDIRECT BACK TO TASK LIST
echo("<form name=nw><input type=hidden name=tsk value=".$oldstatus."></form>");
?>
<script language=JavaScript>
var ntask = document.nw.tsk.value;
if(ntask.length > 0)
{
    document.location.href = "view_own.php?s="+ntask;
}
</script>
</body>

</html>
