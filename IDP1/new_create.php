<?php
//require_once("inc_header.php");
//ASSIST_HELPER::arrPrint(explode("_",$_SERVER['PHP_SELF']));


$_REQUEST['object_type'] = "IDP";
$page_action = "EDIT";
$page_section = "NEW";
$child_object_type = "IDP";
$button_label = "|build|";
$button_icon = "circlesmall-plus";



$add_button = true;
$add_button_label = "|add| |idp|";
$add_button_function = "showAddDialog();";

$page_direct = "new_create_idp.php";

include("common/generic_list_page.php");


?>


<div id=dlg_add_idp title="<?php echo $helper->replaceAllNames("|add| |idp|"); ?>">
	<?php
	
	$section = "NEW";
	$page_redirect_path = "new_create.php?";
	$page_action = "Add";
	
	$uaObject = new IDP1_USERACCESS();

	$child_object_type = "IDP";
	$child_object_id = 0;
	$childObject = new IDP1_IDP();
	
	$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, "", null, 0, $page_action, $page_redirect_path);
	echo $data['display'];

?>
</div>
<script type="text/javascript">
$(function () {
	$("#dlg_add_idp").dialog({
		autoOpen: false,
		modal: true
	});
	<?php echo $data['js']; ?>
	
});
function showAddDialog() {
	$(function() {
		$("#dlg_add_idp").dialog("open");
		var my_window = AssistHelper.getWindowSize();
		if(my_window['width']>800) {
			$("#dlg_add_idp form[name=frm_object] table.form").css("width","800px");
			$("#dlg_add_idp").dialog("option","width",850);
		} else {
			$("#dlg_add_idp form[name=frm_object] table.form").css("width",(my_window['width']-100)+"px");
			$("#dlg_add_idp").dialog("option","width",(my_window['width']-50));
		}
		$("#dlg_add_idp").dialog("option","height",my_window['height']-50);
	});
}
</script>
