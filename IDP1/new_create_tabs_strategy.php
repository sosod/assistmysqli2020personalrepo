<?php

$visionObject = new IDP1_VISION();
$latest[$visionObject->getMyObjectType()] = $visionObject->getMostRecentObject($idp_object_id);
$missionObject = new IDP1_MISSION();
$latest[$missionObject->getMyObjectType()] = $missionObject->getMostRecentObject($idp_object_id);
$valuesObject = new IDP1_VALUES();
$latest[$valuesObject->getMyObjectType()] = $valuesObject->getMostRecentObject($idp_object_id);

//echo "<h3>".$visionObject->getObjectName($visionObject->getMyObjectName())."</h3>";
//echo "<h3>".$missionObject->getObjectName($missionObject->getMyObjectName())."</h3>";
//echo "<h3>".$valuesObject->getObjectName($valuesObject->getMyObjectName())."</h3>";





function drawVisibleBlock($activeObject,$lot,$uot) {
	
	global $latest;
?>

<div style='width:500px;' id=div_<?php echo $lot; ?> class=div_strategy page=<?php echo $activeObject->getMyObjectType(); ?>>
	<?php 
	echo "<h3 class=object_title>".$activeObject->getObjectName($activeObject->getMyObjectName())."</h3>";
	?>
	<Table class=form width=100%>
		<tr>
			<td class=b><?php 
				if(isset($latest[$uot]['is_final'])) {
					if($latest[$uot]['is_final']) {
						echo "Final:";
					} else {
						echo "Latest Draft:";
					}
				} else {
					echo "Not yet created";
				}
				?></td>
		</tr>
			<?php
				if(isset($latest[$uot]['name'])) {
					echo "<tr><td>".$latest[$uot]['name']."</td></tr>";
				}
			?>
	</Table>
	<p class=right><button class=btn_open id=btn_<?php echo $uot; ?>_open>Open</button></p>
</div>
<?php	
	
	
	
}





function drawHiddenDialog($activeObject,$lot,$uot) {

global $displayObject;
global $idp_object_id;
$js = "";

echo "<div id=dlg_".$lot." class=div_dialog>
	<h3>".$activeObject->getObjectName($activeObject->getMyObjectName())."</h3>
	
	<form name=frm_".$lot.">
	
	";
	?>
	<input type=hidden name=strat_object_type value='<?php echo $activeObject->getMyObjectType(); ?>' />
	<input type=hidden name=strat_idp_id value='<?php echo $idp_object_id; ?>' />
	<table>
		<tr>
			<th>Ref</th>
			<th><?php 
				echo "".$activeObject->getObjectName($activeObject->getMyObjectName())."";
			?></th>
			<th>Comment</th>
			<th>Status</th>
			<th></th>
		</tr>
		<tr>
			<td>#</td>
			<td><?php
			$js.=$displayObject->drawFormField("TEXT",array('id'=>"strat_object",'name'=>"strat_object"));
			?></td>
			<td><?php
			$js.=$displayObject->drawFormField("TEXT",array('id'=>"strat_comment",'name'=>"strat_comment"));
			?></td>
			<td><?php
			$options = array(
				IDP1::STRATEGY_DRAFT=>"Draft",
				IDP1::STRATEGY_FINAL=>"Final",
			);
			$js.=$displayObject->drawFormField("LIST",array('id'=>"strat_status",'name'=>"strat_status",'options'=>$options,'unspecified'=>false,'required'=>true),IDP1::STRATEGY_DRAFT);
			?></td>
			<td class=center><button class=btn_add>Add</button></td>
		</tr>
<?php		
		$rows = $activeObject->getOrderedObjects($idp_object_id); 
		foreach($rows as $i => $r) {
			echo "
			<tr id=tr_VISION_".$i." class='".($r['is_final']==1 ? "tr_final" : "tr_draft")."'>
				<td>".$r['reftag']."</td>
				<td>".$r['name']."</td>
				<td>".$r['comment']."</td>
				<td>".($r['is_final']==1 ? "Final" : "Draft")."</td>
				<td class=center width=120px>
				<button id='btn_".$activeObject->getMyObjectType()."_del_".$i."' class='xbutton del_button' row_id=".$i." object_type=".$activeObject->getMyObjectType().">Delete</button>
				<button id='btn_".$activeObject->getMyObjectType()."_status_".$i."' class='xbutton status_".($r['is_final']==0 ? "final" : "draft")."_button' row_id=".$i." object_type=".$activeObject->getMyObjectType().">Mark as ".($r['is_final']==0 ? "Final" : "Draft")."</button>
				</td>
			</tr>";
		}
		?>
	</table>
	</form>
</div>
<?php

return $js;
	
} 










$activeObject = $visionObject;
$lot = strtolower($activeObject->getMyObjectType());
$uot = ($activeObject->getMyObjectType());
drawVisibleBlock($activeObject, $lot, $uot);

$activeObject = $missionObject;
$lot = strtolower($activeObject->getMyObjectType());
$uot = ($activeObject->getMyObjectType());
drawVisibleBlock($activeObject, $lot, $uot);


$activeObject = $valuesObject;
$lot = strtolower($activeObject->getMyObjectType());
$uot = ($activeObject->getMyObjectType());
drawVisibleBlock($activeObject, $lot, $uot);

?>

<script type="text/javascript">
	
$(function() {
	var div_h = 0;
	var tbl_h = 0;
	$("div.div_strategy").button().click(function(e) {
		e.preventDefault();
	});
	/*.each(function() {
		if(parseInt(AssistString.substr($(this).css("height"),0,-2))>div_h) {
			div_h = parseInt(AssistString.substr($(this).css("height"),0,-2));
		}
		if(parseInt(AssistString.substr($(this).find("table.form tr:last td").css("height"),0,-2))>tbl_h) {
			tbl_h = parseInt(AssistString.substr($(this).find("table.form tr:last td").css("height"),0,-2));
		}
		$(this).find("table td").css({"background-color":"#FFFFFF"});
		$(this).find("table tr:first td").addClass("b");
	});
	$("div.div_strategy").css({"height":div_h+"px"});
	$("div.div_strategy").find("table.form tr:last td").css({"height":tbl_h+"px"});
	*/
	$("div.div_strategy").css({"margin":"10px","background":"url()","padding-bottom":"30px"});
});
	
</script>



<?php

$activeObject = $visionObject;
$lot = strtolower($activeObject->getMyObjectType());
$uot = ($activeObject->getMyObjectType());
$js.=drawHiddenDialog($activeObject, $lot, $uot);


$activeObject = $missionObject;
$lot = strtolower($activeObject->getMyObjectType());
$uot = ($activeObject->getMyObjectType());
$js.=drawHiddenDialog($activeObject, $lot, $uot);


$activeObject = $valuesObject;
$lot = strtolower($activeObject->getMyObjectType());
$uot = ($activeObject->getMyObjectType());
$js.=drawHiddenDialog($activeObject, $lot, $uot);


?>
<div id=dlg_delete title="Confirm Delete">
	<form name=frm_delete>
		<input type=hidden name=object_type id=del_object_type value='' />
		<input type=hidden name=object_id id=del_object_id value='' />
	</form>
<p>Are you sure you wish to delete <label id=lbl_delete_msg></label>?</p>
<p class=center><button id=btn_confirm_delete_yes>Yes</button>&nbsp;&nbsp;<button id=btn_confirm_delete_no>No</button></p>	
</div>

<script type="text/javascript" >
$(function() {
	var changes_since_reload = 0;
	var scr_dimensions = AssistHelper.getWindowSize();
	//console.log(scr_dimensions);
	$("#dlg_delete").dialog({
		autoOpen: false
	});
	$("#btn_confirm_delete_yes").button({
		icons:{primary:"ui-icon-check"}
	}).removeClass("ui-state-default").addClass("ui-button-state-green").click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var ot = $("#del_object_type").val();
		var id = $("#del_object_id").val();
		var result = AssistHelper.doAjax("inc_controller.php?action="+ot+".Delete","id="+id);
		AssistHelper.finishedProcessing(result[0],result[1]);
		if(result[0]=="ok") {
			changes_since_reload++;
			$("#tr_"+ot+"_"+id).hide();
		}
		$("#dlg_delete").dialog("close");
	});
	

	$("#btn_confirm_delete_no").button({
		icons:{primary:"ui-icon-closethick"}
	}).removeClass("ui-state-default").addClass("ui-button-state-red");
	//.hover(function() { $(this).removeClass("ui-button-state-error").addClass("ui-button-state-info"); }, function() { $(this).removeClass("ui-button-state-info").addClass("ui-button-state-error"); });
	
	
	
	$("div.div_dialog").dialog({
		autoOpen: false,
		modal: true,
		width: "1000px",
		height: "auto",
		buttons: [{
			text: "Close",
			click: function(e) {
				e.preventDefault();
				if(changes_since_reload>0) {
					AssistHelper.processing();
					var url = "new_create_idp.php?object_id=<?php echo $idp_object_id; ?>&tab=strategy";
					document.location.href = url;
				} else {
					$(this).dialog("close");
				}
			}
		}]
	});
	AssistHelper.hideDialogTitlebar("class","div_dialog");

	$("button.btn_open").button({
		icons:{primary:"ui-icon-newwin"}
	}).css({
		"position":"absolute","bottom":"5px","right":"5px"
	}).click(function(e) {
		e.preventDefault();
		var page = $(this).closest("div").attr("page");
		//alert(page.toLowerCase());
		$("#dlg_"+page.toLowerCase()).dialog("open").find("textarea:first").focus();
	});
	$("button.btn_add").button({
		icons:{primary:"ui-icon-circle-plus"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var dta = AssistForm.serialize($(this).closest("form"));
		var ot = $(this).closest("form").find("input[name=strat_object_type]").val();
		var result = AssistHelper.doAjax("inc_controller.php?action="+ot+".Add",dta);
		AssistHelper.finishedProcessing(result[0],result[1]);
		if(result[0]=="ok") {
			if($(this).closest("form").find("select").val()=="2048") {
				var is_draft = true;
				$(this).closest("table").append("<tr><td>"+result['ref']+"</td><td>"+$(this).closest("tr").find("textarea:first").val()+"</td><td>"+$(this).closest("tr").find("textarea:last").val()+"</td><td>"+$(this).closest("tr").find("select option:selected").text()+"</td><td class=center></td></tr>");
			} else {
				var is_draft = false;
				changes_since_reload++;
				$(this).closest("table").find("tr:first").next().after("<tr><td>"+result['ref']+"</td><td>"+$(this).closest("tr").find("textarea:first").val()+"</td><td>"+$(this).closest("tr").find("textarea:last").val()+"</td><td>"+$(this).closest("tr").find("select option:selected").text()+"</td><td class=center></td></tr>");
			}
				var $del_btn = $('<button />',
					{
						text: 'Delete',
						id: 'btn_'+ot+'_del_'+result['object_id'],
						object_type: ot,
						row_id: result['object_id'],
						click: function (e) { 
							e.preventDefault();
							deleteLineItem($(this));
						}
					}).button({
						icons:{primary:"ui-icon-trash"}
					}).addClass('btn_delete').addClass('xbutton');
				if($(this).closest("form").find("select").val()=="2048") {
					var status_btn_text = "Mark as Final";
					var icon = "ui-icon-check";
					var btn_class = "status_final_button";
				} else {
					var status_btn_text = "Mark as Draft";
					var icon = "ui-icon-document"
					var btn_class = "status_draft_button";
				}
				var $status_btn = $('<button />',
					{
						text: status_btn_text,
						id: 'btn_'+ot+'_status_'+result['object_id'],
						object_type: ot,
						row_id: result['object_id'],
						click: function (e) { 
							e.preventDefault();
							statusLineItem($(this));
						}
					}).button({
						icons:{primary:icon}
					}).addClass('xbutton').addClass('btn_promote').addClass(btn_class);
				if(is_draft) {
					$(this).closest("table").find("tr:last td:last").append($del_btn);
					$(this).closest("table").find("tr:last td:last").append($status_btn);
				} else {
					$(this).closest("table").find("tr:eq(2) td:last").append($del_btn);
					$(this).closest("table").find("tr:eq(2) td:last").append($status_btn);
				}
				formatButtons();
			$(this).closest("form").find("textarea").text("");
			$(this).closest("form").find("select").val("2048");
		}
	});
	$("button.del_button").button({icons:{primary:"ui-icon-trash"}}).click(function(e) {
		e.preventDefault();
		deleteLineItem($(this));
	});
	$("button.status_draft_button").button({icons:{primary:"ui-icon-document"}}).click(function(e) {
		e.preventDefault();
		statusLineItem($(this));
	});
	$("button.status_final_button").button({icons:{primary:"ui-icon-check"}}).click(function(e) {
		e.preventDefault();
		statusLineItem($(this));
	});
	function deleteLineItem($btn) {
		var row_id = $btn.attr("row_id");
		var ot = $btn.attr("object_type");
		var on = $btn.closest("table").find("tr:first").find("th:eq(1)").html(); 
		var id = $btn.parent().parent().find("td:first").html();
		$("#lbl_delete_msg").text(on+" "+id);
		$("#del_object_type").val(ot);
		$("#del_object_id").val(row_id);
		$("#dlg_delete").dialog("open");
		$("#btn_confirm_delete_yes").blur();
	}
	
	
	
	function statusLineItem($btn) {
		AssistHelper.processing();
		var row_id = $btn.attr("row_id");
		var ot = $btn.attr("object_type");
		var url = "new_create_idp.php?object_id=<?php echo $idp_object_id; ?>&tab=strategy&section="+ot;
		//alert(ot+"="+row_id);
		if($btn.hasClass("status_final_button")) {
			//mark as final
			var result = AssistHelper.doAjax("inc_controller.php?action="+ot+".markMeAsFinal","id="+row_id);
		} else {
			//mark as draft
			var result = AssistHelper.doAjax("inc_controller.php?action="+ot+".markMeAsDraft","id="+row_id);
		}
		if(result[0]=="ok") {
			AssistHelper.finishedProcessingWithRedirect(result[0],result[1],url);
		} else {
			AssistHelper.finishedProcessing(result[0],result[1]);
		}
	}
	function formatButtons() {
		$("button.xbutton").children(".ui-button-text").css({"font-size":"80%","padding":"4px","padding-left":"24px"});
		$("button.xbutton").css({"margin":"2px"});
	}
	formatButtons();
	<?php
	if(isset($_REQUEST['section'])) {
		echo "
		$('#btn_".$_REQUEST['section']."_open').trigger('click');
		";
	}
	?>
});
</script>
