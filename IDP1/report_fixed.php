<?php
require_once("inc_header.php");


$reports = array(
	array(
		'ref'=>"mscoa_projects",
		'name'=>"IDP mSCOA report: Projects, Wards & Functions"
	),
	array(
		'ref'=>"projects_wards",
		'name'=>"Projects per Ward"
	),
	array(
		'ref'=>"projects_functions",
		'name'=>"Projects per Function"
	),
	array(
		'ref'=>"projects_dir",
		'name'=>"Projects per Sub-Directorate"
	),
	array(
		'ref'=>"pmkpi_functions",
		'name'=>"Performance Measures per Function"
	),
	array(
		'ref'=>"pmkpi_dir",
		'name'=>"Performance Measures per Sub-Directorate"
	),
);


$idpObject = new IDP1_IDP();
$objects = $idpObject->getObject(array('type'=>"LIST",'section'=>"REPORT",'page'=>"VIEW"));

//ASSIST_HELPER::arrPrint($objects['rows']);

$filter = array();
foreach($objects['rows'] as $idp_id => $idp) {
	$filter[$idp_id] = $idp[$idpObject->getNameFieldName()]['display']."&nbsp;(".$idp[$idpObject->getTableField()."_ref"]['display'].")&nbsp;&nbsp;<span class=float>[".$idp[$idpObject->getIDFieldName()]['display']."]</span>";
}

if(count($filter) == 0) {
	ASSIST_HELPER::displayResult(array("error","No activated ".$idpObject->getObjectName($idpObject->getMyObjectName())." available.  Please complete an ".$idpObject->getObjectName($idpObject->getMyObjectName())."."));
} else {
	
	

	
	?>
	
	
	<table>
		<tr>
			<td class=b colspan=3>
				<span class=float>
					Filter: <select name=sel_filter><option selected value=X>--- SELECT <?php echo $idpObject->getObjectName($idpObject->getMyObjectName()); ?> ---</option>
		<?php
			foreach($filter as $idp_id => $idp) {
				echo "<option value=".$idp_id.">".$idp."</option>";
			}
		?>
			</select>
				</span>
			</td>
		</tr>
		<tr>
			<th></th>
			<th>Report</th>
			<th></th>
		</tr>
	<?php 
	
	foreach($reports as $r => $report) {
		echo "
		<tr>
			<td>".($r+1)."</td>
			<td>".$report['name']."</td>
			<td><button class=btn_report id=".$report['ref'].">Generate</button></td>
		</tr>
		";
	}
	?>
	</table>
<?php 
} //end if check if filter is populated
?>
<script type="text/javascript">
$(function() {
	$(".btn_report").button({
		icons:{primary:'ui-icon-clock'}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		AssistHelper.finishedProcessing("info","Under development");
	});
});
</script>