<?php
/**
 * To manage the Glossary
 * Created on: 21 January 2015
 * 
 * Authors: Janet Currie, Duncan Cosser
 * 
 * Version 1
 */
 
class IDP1_GLOSSARY extends IDP1 {
    
	private $additional_headings = array(
		'admins'=>'contract_owner_id',
		'menu'=>"Menu",
		'headings'=>"Headings",
		'lists'=>"Lists",
	);
	
	
    
    const TABLE = "glossary";
	const TABLE_FIELD = "name";
    /*************
     * CONSTANTS
     */
    /**
     * Can a heading be renamed by the client
     */
    const CAN_RENAME = 16;  
         
    
    
    public function __construct() {
        parent::__construct();
    }
    
    
	
	
	
	
	

	
	
	/***********************************
	 * CONTROLLER functions
	 */
	public function editObject($var) {
		$section = $var['section'];
		unset($var['section']);
		$mar = $this->editTerms($section,$var);
		return $mar;
		if($mar>0) {
			return array("ok","Glossary Terms updated successfully.");
		} else {
			return array("info","No changes were found to be saved.");
		}
		return array("error",$mar);
	}
	
	
	
	
    /*******************
     * GET functions
     * */
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    
	protected function getFormattedNames($type) {
		$names = array(); 
		$sql = "SELECT name_section as section, IF(LENGTH(name_client)>0,name_client,name_default) as name 
				FROM ".$this->getTableName()."
				WHERE (name_status & ".$type." = ".$type.")";
		$rows = $this->mysql_fetch_all($sql);
		foreach($rows as $r){
			$s = explode("_",$r['section']);
			$names[end($s)] = $r['name'];
		}
		return $names;
	}
	
   /**
    * Get the object names from the table in easy readable format
    */
	public function fetchObjectNames() {
		return $this->getFormattedNames(IDP1_NAMES::OBJECT_HEADING);
	}
   /**
    * Get the object names from the table in easy readable format
    */
	public function fetchActivityNames() {
		return $this->getFormattedNames(IDP1_NAMES::ACTIVITY_HEADING);
	}
	
	     
	    
		
		
	protected function getNamesFromDB($type,$sections=array()){
		$sql = "SELECT name_id as id, name_section as section, name_default as mdefault, name_client as client 
				FROM ".$this->getTableName()." 
				WHERE (name_status & ".$type." = ".$type.")"
				.(count($sections)>0 ? " AND name_section IN ('".implode("','",$sections)."')" : "");
		$rows = $this->mysql_fetch_all_by_id($sql,"section");
		return $rows;
		//return array($sql);
	}
		 
	/** 
	 * Get the object names from the table for editing in Setup
	 */
	public function getObjectNamesFromDB($sections=array()){
		return $this->getNamesFromDB(IDP1_NAMES::OBJECT_HEADING,$sections);
	}
	/** 
	 * Get the activity names from the table for editing in Setup
	 */
	public function getActivityNamesFromDB($sections=array()){
		return $this->getNamesFromDB(IDP1_NAMES::ACTIVITY_HEADING,$sections);
	}
		
	public function getGlossary(){
		$gloss = array();
		$db = new ASSIST_DB();
		$dbref = $this->getDBRef()."_";	
		//Fetch all the info
		$sql = "SELECT * FROM ".$dbref."glossary WHERE (status && ".IDP1::ACTIVE." = ".IDP1::ACTIVE.")";
		$cats = $db->mysql_fetch_all_by_id($sql,"field");
		
		$cats = $this->replaceObjectNames($cats);	
			
		//Split the glossary into headings
		foreach($cats as $key=>$val){
			if(stripos($val['section'],'CONTRACT')!==false){
				$gloss['contract'][$key]=$val;
			}else if(stripos($val['section'],'CONTRACT_UPDATE')!==false){
				$gloss['contract_update'][$key]=$val;
			}else if(stripos($val['section'],'DELIVERABLE')!==false){
				$gloss['deliverable'][$key]=$val;
			}else if(stripos($val['section'],'DELIVERABLE_UPDATE')!==false){
				$gloss['deliverable_update'][$key]=$val;
			}else if(stripos($val['section'],'ACTION')!==false){
				$gloss['action'][$key]=$val;
			}else if(stripos($val['section'],'ACTION_UPDATE')!==false){
				$gloss['action_update'][$key]=$val;
			}else if(stripos($val['section'],'ASSESSMENT')!==false){
				$gloss['assessment'][$key]=$val;
			}else if(stripos($val['section'],'CONTRACT_ASSURANCE')!==false){
				$gloss['contract_assurance'][$key]=$val;
			}else if(stripos($val['section'],'DELIVERABLE_ASSURANCE')!==false){
				$gloss['deliverable_assurance'][$key]=$val;
			}else if(stripos($val['section'],'ACTION_ASSURANCE')!==false){
				$gloss['action_assurance'][$key]=$val;
			}else if(stripos($val['section'],'FINANCE_BUDGET')!==false){
				$gloss['finance_budget'][$key]=$val;
			}else if(stripos($val['section'],'FINANCE_DELIVERABLE_BUDGET')!==false){
				$gloss['finance_deliverable_budget'][$key]=$val;
			}else if(stripos($val['section'],'FINANCE_ADJUSTMENT')!==false){
				$gloss['finance_adjustment'][$key]=$val;
			}else if(stripos($val['section'],'FINANCE_DELIVERABLE_ADJUSTMENT')!==false){
				$gloss['finance_deliverable_adjustment'][$key]=$val;
			}else if(stripos($val['section'],'FINANCE_INCOME')!==false){
				$gloss['finance_income'][$key]=$val;
			}else if(stripos($val['section'],'FINANCE_DELIVERABLE_INCOME')!==false){
				$gloss['finance_deliverable_income'][$key]=$val;
			}else if(stripos($val['section'],'FINANCE_EXPENSE')!==false){
				$gloss['finance_expense'][$key]=$val;
			}else if(stripos($val['section'],'FINANCE_DELIVERABLE_EXPENSE')!==false){
				$gloss['finance_deliverable_expense'][$key]=$val;
			}else if(stripos($val['section'],'FINANCE_RETENTION')!==false){
				$gloss['finance_retention'][$key]=$val;
			}else if(stripos($val['section'],'FINANCE_DELIVERABLE_RETENTION')!==false){
				$gloss['finance_deliverable_retention'][$key]=$val;
			}
		}
		return $gloss;
	}
	 
	 
	 
	 
	 
    /****************************************
     * SET / UPDATE Functions
     */
    
    
    /**
	 * Function to edit term
	 */
    private function editTerms($sect,$var) {
    	$sql = "SELECT * FROM ".$this->getDBRef()."_".self::TABLE." WHERE (status && ".IDP1_LIST::ACTIVE." = ".IDP1_LIST::ACTIVE.")";
    	$currents = $this->mysql_fetch_all_by_id($sql, "field");
    	foreach($var as $key=>$val){
    		//See if there are matches, overwrite conflicts, log before and afters
    		if(strlen($val)==0){
    			unset($var[$key]);
    		}else if($val !== $currents[$key]['gloss']){
    			//Non-zero change
    			$s = explode("_",$key);
				$delta = "|".end($s)."|";
				$c[$delta] = array('to'=>$val,'from'=>$currents[$key]['gloss']);
    		}else{
    			unset($var[$key]);
    		}
    	}
		//return array("info",$c);
		foreach($var as $key=>$val){
			$upd = "UPDATE ".$this->getDBRef()."_".self::TABLE." SET gloss = '$val' WHERE field = '$key'";
			$mar[$key]=$this->db_update($upd);
			if($mar[$key]>0){
				
			}else{
				$errors[]=$key;
			}
			
		}
		if(count($errors)==0 && count($c)>0) {
			$changes = array_merge(array('user'=>$this->getUserName()),$c);
			$log_var = array(
				'section'	=> "GL_".strtoupper($sect),
				'object_id'	=> 0,
				'changes'	=> $changes,
				'log_type'	=> IDP1_LOG::EDIT,		
			);
			$me = new IDP1();
			$me->addActivityLog("setup", $log_var);
			return array("ok","Terms updated successfully");
		}else{
			return array("info","Changes were not saved");
		}
		
		return $mar;
    }
    
    
    
    
    
    
    
    
    /**********************************************
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    
	 
     
	 
	 
	 
	 
	 
	 
	 
     
     /***********************
     * PRIVATE functions: functions only for use within the class
     */	
	
	
	
	
	    
    public function __destruct() {
    	parent::__destruct();
    }
    
    
    
}


?>