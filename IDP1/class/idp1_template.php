<?php
/**
 * To manage the TEMPLATE object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class IDP1_TEMPLATE extends IDP1 {
    
    
    
    /*************
     * CONSTANTS
     */
    const TABLE = "template";
    const TABLE_FLD = "temp";
    const REFTAG = "CT";
    
    public function __construct() {
        parent::__construct();
    }
    
	
	/*********
	 * CONTROLLER functions
	 */
	
	public function addObject($contract_id) {
		$con_id = $contract_id['id'];
		//---------------------Template data-------------------//	
		$tmp_name = $contract_id['template_name'];
		unset($contract_id['template_name']);
		$insert_user = $this->getUserID();
		$tmp_sql = "INSERT INTO ".$this->getTableName()." SET temp_name = '$tmp_name', temp_source_contract_id = $con_id, temp_status = 2, temp_insertuser = '$insert_user', temp_insertdate = NOW()";
		$tmp_result[] = $this->db_insert($tmp_sql);

		//---------------------Contract data-------------------//	
		$contractObject = new IDP1_CONTRACT();
		//$contract_argument = array('type'=>"DETAILS",'id'=>$con_id);
		$contract_fields = $contractObject->getRawObject($con_id);
		foreach($contract_fields as $key=>$val){
			$nkey = "temp_" . $key;
			if($key == "contract_supplier"){
				$temp_sup=$val;
			}else{
				$temp_contract_fields[$nkey]=$val;
			}
		}
		foreach($temp_sup as $key=>$val){
			$con_sup[$key]= array();
			foreach($val as $index=>$item){
				if($index != "cs_id"){
					$con_sup[$key][$index]=$item;
				}
			}
		}
		$data['values']=$temp_contract_fields;
		$data['con_sup']=$con_sup;
		$temp_contract_fields['temp_parent_id']=$temp_contract_fields['temp_contract_id'];
		unset($temp_contract_fields['temp_contract_id']);
		unset($temp_contract_fields['temp_contract_insertuser']);
		unset($temp_contract_fields['temp_contract_insertdate']);
		unset($temp_contract_fields['temp_contract_template_id']);
		$sql = $this->convertArrayToSQL($temp_contract_fields);
		$data['valuesSQL']=$sql;
		$sql_arr = array();
		foreach($con_sup as $val){
			foreach($val as $index=>$item){
				if($index != "cs_contract_id"){
					$nkey = "temp_" . $index;
					$val[$nkey] = $item;
				}else{
					$val['temp_cs_contract_id']=$tmp_result[0];
				}
				unset($val[$index]);
			}
			$sql_arr[] = $this->convertArrayToSQL($val);
		}
		$data['con_supSQL']=$sql_arr;
		$statement = "INSERT INTO ".$this->getTableName()."_contract SET temp_contract_template_id = $tmp_result[0], ".$sql;
		$result = $this->db_insert($statement);
		//return $statement;
		foreach($sql_arr as $val){
			$statement_arr[]="INSERT INTO ".$this->getTableName()."_contract_supplier SET ".$val;
		}
		//return $statement_arr;
		foreach($statement_arr as $val){
			$result_arr[]=$this->db_insert($val);
		}
		
		//---------------------Deliverable data-------------------//	
		$delObject = new IDP1_DELIVERABLE();
		$dels = $delObject->getDelIds($con_id);
		foreach($dels as $d=>$f){
			$dels[]=$f['del_id'];
			unset($dels[$d]);
		}
		//return $dels;
		$del_items = $delObject->getRawObject($dels);
	 	foreach($del_items as $index => $item){
	 		
	 		foreach($item as $key=>$val){
				if($key == "del_id"){
					$item['temp_old_id']=$val;
				}else if($key != "del_insertdate" && $key != "del_insertuser" && $key != "del_attachment" && $key != "del_contract_id" && $key != "del_progress"){
		 			$nkey = "temp_".$key;
					$item[$nkey] = $val;
					unset($item[$key]);
	 			}else if($key == "del_progress"){
	 				$item['temp_del_progress'] = 0;
					unset($item[$key]);
	 			}else if($key == "del_contract_id"){
	 				$item['temp_del_contract_id'] = $tmp_result[0];
					unset($item[$key]);
	 			}
				unset($item[$key]);
				unset($item['temp_del_id']);
	 		}
	
			$del_statement = $this->convertArrayToSQL($item);
			$del_statement_string = "INSERT INTO ".$this->getTableName()."_deliverable SET ". $del_statement;
			//return $del_statement_string;
			$del_results[] = $this->db_insert($del_statement_string);
	 	}
		//return $del_results;
		
		//---------------------Action data-------------------//	
		$actObject = new IDP1_ACTION();
		$act_items = $actObject->getActionsFromObject($con_id);
		foreach($act_items as $thing){
			foreach($thing as $key=>$val){
				unset($thing[$key]);
				if($key == "action_progress"){
					$thing['temp_action_progress'] = 0;
				}else if($key == "action_deliverable_id"){
					$thing['temp_action_deliverable_id']=$this->getNewDeliverableId($val, $tmp_result[0]);
				}else if($key != "action_attachment" && $key != "action_insertuser" && $key != "action_insertdate" && $key != "action_id"){
					$nkey = "temp_".$key;	
					$thing[$nkey]=$val;
				}
			}
			$act_statement = $this->convertArrayToSQL($thing);
			$act_statement_string = "INSERT INTO ".$this->getTableName()."_action SET ". $act_statement;
			$act_results[] = $this->db_insert($act_statement_string);
		}
		
		if(!empty($act_results) && !empty($del_results) && !empty($result_arr) && !empty($tmp_result)){
			return array("ok", "Template creation successful.");
		}else{
			return array("error","An error occurred during template creation. Please try again.");
		}
		
	}
	
    /******************
	 * Function for switching the status of the Template to deleted, selectively recursively (to all children objects as well if arguments are supplied)
	 * @param obect_type(String) the object being fed in for deletion (CON, DEL, ACT, TEM)
	 * @param obect_id(String) the ID of the object being fed in for deletion
	 * */
	 
	public function deleteObject($data){
		$obj	= $data['obj'];	
		$id	= $data['id'];	
		//-----------------Contract---------------------//
		if($obj == "CON"){
			
			$sql = "UPDATE ".$this->getTableName()."_contract SET temp_contract_status = (temp_contract_status - ".IDP1::ACTIVE." + ".IDP1::DELETED.") WHERE temp_contract_id = $id";
			$result = $this->db_query($sql);
			if($result && $result > 0){
				$data[0]="ok";
				$data[1]="Template contract deleted successfully";
			}else{
				$data[0]="fail";
				$data[1]="Template contract deletion failed. Please try again. Error: ".$result;
			}
			
		}else if($obj == "DEL"){
			$sql = "UPDATE ".$this->getTableName()."_deliverable SET temp_del_status = (temp_del_status - ".IDP1::ACTIVE." + ".IDP1::DELETED.") WHERE temp_del_id = $id";
			$result = $this->db_query($sql);
			if($result && $result > 0){
				$data[0]="ok";
				$data[1]="Template deliverable deleted successfully";
			}else{
				$data[0]="fail";
				$data[1]="Template deliverable deletion failed. Please try again. Error: ".$result;
			}
			
		}else if($obj == "ACT"){
			$sql = "UPDATE ".$this->getTableName()."_action SET temp_action_status = (temp_action_status - ".IDP1::ACTIVE." + ".IDP1::DELETED.") WHERE temp_action_id = $id";
			$result = $this->db_query($sql);
			if($result && $result > 0){
				$data[0]="ok";
				$data[1]="Template action deleted successfully";
			}else{
				$data[0]="fail";
				$data[1]="Template action deletion failed. Please try again. Error: ".$result;
			}
			
		}else if($obj == "TEM"){
			$sql = "UPDATE ".$this->getTableName()." SET temp_status = (temp_status - ".IDP1::ACTIVE." + ".IDP1::DELETED.") WHERE temp_id = $id";
			$result = $this->db_query($sql);
			if($result && $result > 0){
				$data[0]="ok";
				$data[1]="Template deleted successfully";
			}else{
				$data[0]="fail";
				$data[1]="Template deletion failed. Please try again. Error: ".$result;
			}
			
		}else{
			$data[0]="error";
			$data[1]="Invalid parameter(s) supplied for function - please read documentation (accepts TEM, CON, DEL, ACT)";
		}
		/*	
			$data[0]=$obj;
			$data[1]=$id;
		*/
		return $data;
	}
	
    /******************
	 * Function for switching the status of the Template to inactive, recursively (to all children objects as well)
	 * */
	 
	public function deactivateObject($raw_id){
		$id	= $raw_id['id'];
		//-----------------Template---------------------//
		$tmp_sql = "UPDATE ".$this->getTableName()." SET  temp_status = (temp_status - ".IDP1::ACTIVE." + ".IDP1::INACTIVE.") WHERE temp_id = $id";
		$tmp_res = $this->db_update($tmp_sql);
		if($tmp_res && $tmp_res > 0){
			$data[0]="ok";
			$data[1]="Template deactivated successfully";
		}else{
			$data[0]="fail";
			$data[1]="Template deactivation failed. Please try again. Error: ".$tmp_res;
		}
		return $data;
	}
	
    /******************
	 * Function for switching the status of the Template to active, recursively (to all children objects as well)
	 * */
	 
	public function restoreObject($raw_id){
		$id	= $raw_id['id'];	
		//-----------------Template---------------------//
		$tmp_sql = "UPDATE ".$this->getTableName()." SET  temp_status = (temp_status - ".IDP1::INACTIVE." + ".IDP1::ACTIVE.") WHERE temp_id = $id";
		$tmp_res = $this->db_update($tmp_sql);
		if($tmp_res && $tmp_res > 0){
			$data[0]="ok";
			$data[1]="Template activated successfully";
		}else{
			$data[0]="fail";
			$data[1]="Template activation failed. Please try again. Error: ".$tmp_res;
		}
		return $data;
	}
	
    /******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return self::REFTAG; }
	public function getFullIDField() { return $this->getTableField()."_id"; }
	public function getFullNameField() { return $this->getTableField()."_name"; }
	public function getActiveSQLScript($tn = "C") {
		return "(( ".(strlen($tn)>0 ? $tn."." : "").$this->getTableField()."_status & ".self::ACTIVE." ) = ".self::ACTIVE.")";
	}
    
	public function getTemplates($options="") {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE (".$this->getTableField()."_status & ".IDP1::DELETED." <> ".IDP1::DELETED.") ".(strlen($options)>0 ? " AND (".$options.")" : "")." ORDER BY ".$this->getFullNameField();
		return $this->mysql_fetch_all_by_id($sql, $this->getFullIDField());
	}
	
	public function getActiveTemplates($options="") {
		$options = (strlen($options)>0 ? "(".$options.") AND " : "")." ( ".$this->getTableField()."_status & ".IDP1::ACTIVE." ) = ".IDP1::ACTIVE;
		return $this->getTemplates($options);
	}
	
	public function getInActiveTemplates($options="") {
		$options = (strlen($options)>0 ? "(".$options.") AND " : "")." ( ".$this->getTableField()."_status & ".IDP1::INACTIVE." ) = ".IDP1::INACTIVE;
		return $this->getTemplates($options);
	}
	
	public function getActiveTemplatesFormattedForSelect($options="") {
		$rows = $this->getActiveTemplates($options);
		$data = $this->formatRowsForSelect($rows,$this->getFullNameField());
		return $data;
	}     
     
	public function getTemplateName($temp_id){
		$sql = "SELECT temp_name FROM ".$this->getTableName()." WHERE temp_id = $temp_id";
		$data = $this->mysql_fetch_one_value($sql, "temp_name");
		return $data;
	}
     
    public function getObject($var){
    	$obj_type = $var['type'];
    	$id = $var['id'];	
    	$data = $this->getDetailedObject($obj_type, $id);
    	return $data;
    }
	
	public function getListObject($id, $type){
		$headObject = new IDP1_HEADINGS();	
		switch ($type) {
			case 'CON':
				$head_arr = $headObject->getMainObjectHeadings("CONTRACT", "DETAILS", "ADMIN");
				$sql = "SELECT * FROM ".$this->getTableName()."_contract WHERE temp_contract_id = $id";
				return $sql;
				//$data['rows'] = $this->mysql_fetch_one($sql);
				
				$cObject = new IDP1_CONTRACT();
				$displayObject = new IDP1_DISPLAY();
				$d_headings = array('rows'=>array());
				$a_headings = array('rows'=>array());
				$where_tblref = $c_tblref;
				$ref_tag = $cObject->getRefTag();
				$c_headings = $headObject->getMainObjectHeadings("CONTRACT","DETAILS"); 
				$c_tblref = "C";
				$c_table = $cObject->getTableName();
				$c_id = $c_tblref.".".$cObject->getIDFieldName();
				$c_status = $c_tblref.".".$cObject->getStatusFieldName();
				$c_name = $cObject->getNameFieldName();
				$c_field = $c_tblref.".".$cObject->getTableField();	
				$final_data['head'] = $c_headings['rows'];
				unset($final_data['head']['cs_supplier_id']);
				unset($final_data['head']['cs_project_value']);
				unset($final_data['head']['contract_template_id']);
				
				$id_fld = $cObject->getIDFieldName();
				$r = $this->mysql_fetch_one($sql);
				$row = array();
			$i = $r[$id_fld];
			foreach($final_data['head'] as $fld=> $head) { 
				$val = isset($r[$fld]) ? $r[$fld] : $fld;
				if($headObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS") {
					$row[$fld] = (is_null($r[$head['list_table']]) ? $this->getUnspecified() : $r[$head['list_table']]);
				} elseif($head['type']=="DEL_TYPE"){
					$row[$fld] = $this->deliverable_types[$val]; 
				} elseif($head['type']=="DELIVERABLE") {
					if($r["del_type"]=='SUB') {
						if($r[$fld]==0) {
							$row[$fld] = $this->getUnspecified();
						} else {
							if(!isset($deliverable_names_for_subs)) {
								$deliverable_names_for_subs = $dObject->getDeliverableNamesForSubs($i);
							}
							$row[$fld] = $deliverable_names_for_subs[$r[$fld]];
						}
					} else {
						unset($final_data['head'][$fld]);
					}
				} elseif($head['section']=="DELIVERABLE_ASSESS") {
					$assess_status = $r['contract_assess_type'];
					$display_me = true;
						if($cObject->mustIDoAssessment($r['contract_do_assessment'])===FALSE) {
							$display_me = false;
						} else {
							//validate the assessment status field here
							$lt = explode("_",$head['list_table']);
							switch($lt[1]) {
								case "quality": $display_me = $cObject->mustIAssessQuality($assess_status); break;
								case "quantity": $display_me = $cObject->mustIAssessQuantity($assess_status); break;
								case "other": $display_me = $cObject->mustIAssessOther($assess_status); break;
							}
						}
					if($display_me) {
						$row[$fld] = is_null($r[$head['list_table']]) ? $this->getUnspecified() : $r[$head['list_table']];
						if($lt[1]=="other" && strlen($r['contract_assess_other_name'])>0) {
							$final_data['head'][$fld]['name'] = str_ireplace("Other", $r['contract_assess_other_name'], $final_data['head'][$fld]['name']);
						}
					} else {
						unset($final_data['head'][$fld]);
						//$row[$fld] = $assess_status;
					}
				} elseif($this->isDateField($fld) || $head['type']=="DATE") {
					$row[$fld] = $displayObject->getDataField("DATE", $val,array('include_time'=>false));
				//format by fld
				} elseif($fld==$id_fld) {
					$row[$fld] = $ref_tag.$val;
				} elseif($fld=="contract_supplier") {
					$csObject = new IDP1_CONTRACT_SUPPLIER();
					$sub_rows = $csObject->getObjectByContractID($i);
					$sub_head = $c_headings['sub'][$head['id']];
					if(count($sub_rows)>0) {
						$val = array();
						foreach($sub_rows as $sr) {
							$x = array();
							foreach($sub_head as $shead) {
								switch($shead['type']) {
									case "LIST":
										$v = $sr[$shead['list_table']];
										break;
									case "CURRENCY":
										$v = $displayObject->getDataField($shead['type'], $sr[$shead['field']]);
										$v = $v['display'];
										break;
									default:
										$v = $sr[$shead['field']];
								}
								$x[$shead['name']] = $v;
							}
							$val[] = $x;
						}
					} else {
						$val = "";
					}
					$row[$fld] = $val; 
				} elseif($fld=="contract_assessment_statuses") {
					$val = $r['contract_assess_status'];
					$listObject = new IDP1_LIST("deliverable_status");
					$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
					$last_deliverable_status = 0;
					$sub_head = array();
					$sh_type = "BOOL";
					foreach($list_items as $key => $status) {
						$sub_head[$key] = array(
							'field'=>$key,
							'type'=>$sh_type,
							'name'=>$status,
							'required'=>1,
							'parent_link'=>"",
						);
						$last_deliverable_status = $key;
					}
					$td = array();
					foreach($sub_head as $key => $shead) {
						$sh_type = $shead['type'];
						$sfld = $shead['field'];
						if($sh_type=="BOOL"){
							$pow = pow(2,$key);
							$test = ( (($val & $pow) == $pow) ? "1" : "0");
							$sval = $displayObject->getDataField("BOOL", $test);
						}
						$td[]=array($shead['name']=>$sval['display']);
					}
					$row[$fld] = $td;
				} elseif($fld=="contract_assess") {
					$sub_head = $c_headings['sub'][$head['id']];
					$val = $r['contract_assess_type'];
					$sub_display = true;
					$td = array();
					foreach($sub_head as $shead) {
						$v = $shead['field'];
						switch($v) {
							case "contract_assess_other_name":
								if($cObject->mustIAssessOther($val)) { 
									$v = $r[$v];
								} else {
									$sub_display = false;
								} 
								break;
							case "contract_assess_qual": 	$cas = !isset($cas) ? $cObject->mustIAssessQuality($val) : $cas;
							case "contract_assess_quan": 	$cas = !isset($cas) ? $cObject->mustIAssessQuantity($val) : $cas;
							case "contract_assess_other":	$cas = !isset($cas) ? $cObject->mustIAssessOther($val) : $cas; 
								if($cas) {
									$v = $displayObject->getDataField("BOOL", "1");
								} else {
									$v = $displayObject->getDataField("BOOL", "0");
								}
								$v = $v['display'];
								break;
						}
						if($sub_display) {
							$td[] = array($shead['name']=>$v);
						}
						unset($cas);
					}
					$row[$fld] = $td;
				} else {
					//$row[$fld] = $val;
					$row[$fld] = $displayObject->getDataField($head['type'], $val);
				}
			}
			$final_data['rows'] = $row;
				break;
			
			case 'DEL':
				$head_arr = $headObject->getMainObjectHeadings("DELIVERABLE", "LIST", "ADMIN");
				break;
			
			case 'ACT':
				$head_arr = $headObject->getMainObjectHeadings("ACTION", "LIST", "ADMIN");
				break;
			
			default:
				$data = "Invalid parameters - feed me an ID then a type (3-digit) like CON, DEL, or ACT";
				break;
		}
		$data['head']=$head_arr;
		return $data;
	}
	
	public function getSourceContract($temp_id){
		$sql = "SELECT temp_source_contract_id FROM ".$this->getTableName()." WHERE temp_id = $temp_id";
		$data = $this->mysql_fetch_one_value($sql, "temp_source_contract_id");
		return $data;
	}
	
	public function getNewDeliverableId($old_id, $con_id){
		$sql = "SELECT temp_del_id FROM ".$this->getTableName()."_deliverable WHERE temp_old_id = $old_id AND temp_del_contract_id = $con_id";
		$data = $this->mysql_fetch_one_value($sql, "temp_del_id");
		return $data;
	}	
	
    /***
     * SET / UPDATE Functions
     */

         
    
	
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>