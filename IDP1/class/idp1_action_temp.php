<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 */
 
class IDP1_ACTION extends IDP1 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $progress_status_field = "_status_id";
	protected $id_field = "_id";
	protected $parent_field = "_deliverable_id";
	protected $name_field = "_name";
	protected $deadline_field = "_deadline";
	protected $owner_field = "_owner";
	protected $progress_field = "_progress";
	protected $attachment_field = "_attachment";
	protected $update_attachment_field = "_update_attachment";
	protected $target_field = "_target";
	protected $actual_field = "_actual";
	protected $target_unit_field = "_target_unit";

    
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "ACTION"; 
     
    const TABLE = "action";
    const TABLE_FLD = "action";
    const REFTAG = "CA";
	
	const LOG_TABLE = "action";
    
    public function __construct($action_id=0) {
        parent::__construct();
		$this->has_target = true;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->progress_status_field = self::TABLE_FLD.$this->progress_status_field;
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->deadline_field = self::TABLE_FLD.$this->deadline_field;
		$this->owner_field = self::TABLE_FLD.$this->owner_field;
		$this->progress_field = self::TABLE_FLD.$this->progress_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		$this->update_attachment_field = self::TABLE_FLD.$this->update_attachment_field;
		$this->target_field = self::TABLE_FLD.$this->target_field;
		$this->target_unit_field = self::TABLE_FLD.$this->target_unit_field;
		$this->actual_field = self::TABLE_FLD.$this->actual_field;
		if($action_id>0) {
			$this->object_id = $action_id;
			$this->object_details = $this->getAObject($action_id);
		}
		$this->object_form_extra_js = "
		//variable to remember the previous progress value when defaulting to 100
		var old_progress = $('#".$this->getProgressFieldName()."').val();
		 
		//on change of the status drop down
		$('#".$this->getProgressStatusFieldName()."').change(function() {
		                //get the selected option value
		                var v = ($(this).val())*1;
		                //if it is set to \"Completed\" (always id=3 except deliverable which is 5)
		                if(v==3) {
		                                //if the progress field is not already set to 100, then remember the current value
		                                if(($('#".$this->getProgressFieldName()."').val())*1!=100) {
		                                                old_progress = $('#".$this->getProgressFieldName()."').val();
		                                }
		                                //update progress field to 100 and disable
		                                $('#".$this->getProgressFieldName()."').val('100').prop('disabled',true);
		                //else if the status field is not Completed, check if the progress field was previously disabled
		                } else if($('#".$this->getProgressFieldName()."').prop('disabled')==true) {
		                                //cancel the disable and reset the value back to the last remembered value
		                                $('#".$this->getProgressFieldName()."').val(old_progress).prop('disabled',false);
		                }
		});";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		unset($var['attachments']);
		unset($var['action_id']); //remove incorrect deliverable_id value from add form
		unset($var['object_id']); //remove incorrect deliverable_id value from add form
		foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_progress'] = 0;
		$var[$this->getTableField().'_status_id'] = 1;
		$var[$this->getTableField().'_status'] = IDP1::ACTIVE;

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|action| created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'	=> $id,
				'changes'	=> $changes,
				'log_type'	=> IDP1_LOG::CREATE,
				'progress'	=> 0,
				'status_id'	=> 1,		
			);
			$note = $this->notify($log_var, "EMAIL", $this->getMyObjectType());
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			//$this->addActivityLog(self::LOG_TABLE, $log_var);
			//$this->addActivityLog(self::LOG_TABLE, $log_var);
			//return array("ok","New ".$this->getActionObjectName()." ".$this->getRefTag().$id." has been successfully created.");
		//	ASSIST_HELPER::arrPrint($note);
		//	return array("info",$note);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;
		}
		return array("error","Testing: ".$sql);
	}

	public function updateObject($var,$attach=array()) {
		$object_id = $var['object_id'];
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$p_field = $this->getProgressFieldName();
		$si_field = $this->getProgressStatusFieldName();
		$s_field = $this->getStatusFieldName();
		$progress = $var[$p_field];
		$status_id = $var[$si_field];
		if($status_id==3) {
			$progress = 100;
			$var[$p_field] = 100;
		}
		$dc_field = $this->getTableField()."_date_completed";
		$actual_field = $this->getActualFieldName();
		$actual = $var[$actual_field];
		$unit_field = $this->getTargetUnitFieldName();
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		$unit = $old_data[$unit_field];
		
		$update_data = array(
			$p_field=>$progress,
			$si_field=>$status_id,
			$actual_field=>$actual,
		);
		//if completed then send for approval
		if($progress==100 && $status_id==3) {
			$new_status = $old_data[$s_field] + IDP1::AWAITING_APPROVAL;
			$update_data[$s_field] = $new_status;
			if((strtotime($var['action_on']) != strtotime($old_data[$dc_field]))) {
				$update_data[$dc_field] = date("Y-m-d",strtotime($var['action_on']));
				$c[$dc_field] = array('to'=>date("Y-m-d",strtotime($var['action_on'])),'from'=>$old_data[$dc_field]);
			}
		} else {
			$new_status = $old_data[$s_field];
		}
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);
		
		if($mar>0 || strlen($var['response'])>0) {
			$changes = array(
				'response'=>$var['response'],
				'user'=>$this->getUserName(),
				'action_on'=>$var['action_on'],
			);
			if($old_data[$actual_field] != $actual) {
				$changes[$actual_field] = array('to'=>$actual." ".$unit,'from'=>$old_data[$actual_field]." ".$unit);
			}
			if($old_data[$p_field] != $progress) {
				$changes[$p_field] = array('to'=>$progress,'from'=>$old_data[$p_field]);
			}
			if($old_data[$si_field] != $status_id) {
				$listObject = new IDP1_LIST(substr($si_field,0,-3));
				$items = $listObject->getAListItemName(array($status_id,$old_data[$si_field]));
				$changes[$si_field] = array('to'=>$items[$status_id],'from'=>$items[$old_data[$si_field]]);
			}
			if(isset($c[$dc_field])) {
				$changes[$dc_field] = $c[$dc_field];
			}
			$x = array();
			foreach($attach as $a) {
				$x[] = "|attachment| ".$a['original_filename']." has been added.";
			}
			$changes[$this->getUpdateAttachmentFieldName()] = $x;
			//		return array("error",$si_field.":".serialize($var));
			if( ($new_status & IDP1::AWAITING_APPROVAL) == IDP1::AWAITING_APPROVAL) {
				$changes['status'] = "|action| is now awaiting approval.";
			}
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> IDP1_LOG::UPDATE,
				'progress'	=> $progress,
				'status_id'	=> $status_id,		
			);
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
		//ASSIST_HELPER::arrPrint($note);
			unset($log_var['recipients']);
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$this->addActivityLog(self::LOG_TABLE, $log_var);
			if(($new_status & IDP1::AWAITING_APPROVAL) == IDP1::AWAITING_APPROVAL) {
				//email deliverable owner to notify of action waiting approval
			}
			
		//	return array("info", $note);
			return array("ok",$this->getObjectName(self::OBJECT_TYPE)." ".self::REFTAG.$object_id." has been updated successfully.");
		}
		
		return array("error","An error occurred.  Please try again.");
		
		
	}



	public function editObject($var,$attach=array()) {
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		$include = $var['include_rec'];
		unset($var['include_rec']);
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$result = $this->editMyObject($var,$attach,$recipients,$noter,$include);
	//	$for_note = $result[2];
	//	$for_note[0]['recipients']=$recipients;
	//	$for_note[0]['include']=$include;
	//	$note = $this->notify($for_note[0], $noter ,$for_note[1]);
	//	unset($result[2]);	
//		ASSIST_HELPER::arrPrint($note);	
//		return $note;
		return $result;
	}

	public function approveObject($var) {
		$object_id = $var['object_id'];
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$i_field = $this->getIDFieldName();
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$dc_field = $this->getTableField()."_date_completed";
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$new_status = $old_data[$s_field] - IDP1::AWAITING_APPROVAL + IDP1::APPROVED;
		
		$update_data = array(
			$s_field=>$new_status,
		);
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$i_field." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			if(strlen($var['approve_response'])>0) {
				$changes['response']=$var['approve_response'];
			}
			$changes['status'] = "|action| has been approved.";
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> IDP1_LOG::APPROVE,
				'progress'	=> $old_data[$p_field],
				'status_id'	=> $old_data[$si_field],		
			);
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			unset($log_var['recipients']);
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$this->addActivityLog(self::LOG_TABLE, $log_var);
			/** NOTIFY ACTION OWNER THAT ACTION HAS BEEN APPROVED **/
	//	ASSIST_HELPER::arrPrint($note);
	//	return array("info", $note);
			return array("ok",$this->getActionObjectName()." ".$this->getRefTag().$object_id." has been successfully approved.");	
		} else {
			return array("info","No change was made.  Please try again.");
		}
		
	}

	public function declineObject($var) {
		$object_id = $var['object_id'];
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$i_field = $this->getIDFieldName();
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$dc_field = $this->getTableField()."_date_completed";
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$new_status = $old_data[$s_field] - IDP1::AWAITING_APPROVAL;
		
		$update_data = array(
			$p_field=>"99",
			$si_field=>"2",
			$dc_field=>"0000-00-00",
			$s_field=>$new_status,
		);
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$i_field." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			if(strlen($var['approve_response'])>0) {
				$changes['response']=$var['approve_response'];
			}
			if($old_data[$p_field] != $update_data[$p_field]) {
				$changes[$p_field] = array('to'=>$update_data[$p_field],'from'=>$old_data[$p_field]);
			}
			if($old_data[$si_field] != $update_data[$si_field]) {
				$listObject = new IDP1_LIST(substr($si_field,0,-3));
				$items = $listObject->getAListItemName(array($update_data[$si_field],$old_data[$si_field]));
				$changes[$si_field] = array('to'=>$items[$update_data[$si_field]],'from'=>$items[$old_data[$si_field]]);
			}
			if($old_data[$dc_field] != $update_data[$dc_field]) {
				$changes[$dc_field] = array('to'=>$update_data[$dc_field],'from'=>$old_data[$dc_field]);
			}
			$changes['status'] = "|action| has been declined.";
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> IDP1_LOG::DECLINE,
				'progress'	=> $update_data[$p_field],
				'status_id'	=> $update_data[$si_field],		
			);
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			unset($log_var['recipients']);
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$this->addActivityLog(self::LOG_TABLE, $log_var);
			
			/** NOTIFY ACTION OWNER THAT ACTION HAS BEEN DECLINED **/
			//ASSIST_HELPER::arrPrint($note['changes']);
			//return array("info", $note['changes']);
			
			return array("ok",$this->getActionObjectName()." ".$this->getRefTag().$object_id." has been successfully declined.");	
		} else {
			return array("info","No change was made.  Please try again.");
		}
		
	}



    public function unlockApprovedObject($var) {
		$object_id = $var['object_id'];
		/* New stuff for SMS notifications /*/
		unset($var['radio']);
		$noter = strtoupper($var['notification_chooser']);
		$recipients = $var['recipient_mobile'];
		unset($var['recipient_mobile']);
		unset($var['notification_chooser']);
		/*/ New stuff for SMS notifications */
		$i_field = $this->getIDFieldName();
		$p_field = $this->getTableField()."_progress";
		$si_field = $this->getTableField()."_status_id";
		$s_field = $this->getTableField()."_status";
		$dc_field = $this->getTableField()."_date_completed";
		$c = array();
		
		$old_data = $this->getRawObject($object_id);
		
		$new_status = $old_data[$s_field] - IDP1::APPROVED;
		
		$update_data = array(
			$p_field=>"99",
			$si_field=>"2",
			$dc_field=>"0000-00-00",
			$s_field=>$new_status,
		);
		
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($update_data)." WHERE ".$i_field." = ".$object_id;
		$mar = $this->db_update($sql);
		if($mar>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			if(strlen($var['approve_response'])>0) {
				$changes['response']=$var['approve_response'];
			}
			if($old_data[$p_field] != $update_data[$p_field]) {
				$changes[$p_field] = array('to'=>$update_data[$p_field],'from'=>$old_data[$p_field]);
			}
			if($old_data[$si_field] != $update_data[$si_field]) {
				$listObject = new IDP1_LIST(substr($si_field,0,-3));
				$items = $listObject->getAListItemName(array($update_data[$si_field],$old_data[$si_field]));
				$changes[$si_field] = array('to'=>$items[$update_data[$si_field]],'from'=>$items[$old_data[$si_field]]);
			}
			if($old_data[$dc_field] != $update_data[$dc_field]) {
				$changes[$dc_field] = array('to'=>$update_data[$dc_field],'from'=>$old_data[$dc_field]);
			}
			$changes['status'] = "Approval of |action| has been reversed.";
			$log_var = array(
				'object_id'	=> $object_id,
				'changes'	=> $changes,
				'log_type'	=> IDP1_LOG::UNLOCK,
				'progress'	=> $update_data[$p_field],
				'status_id'	=> $update_data[$si_field],		
			);
			$log_var['recipients']=$recipients;
			$note = $this->notify($log_var, $noter, $this->getMyObjectType());
			unset($log_var['recipients']);
			if($note[0] == "ok"){
				$log_var['changes']['notification'] = "no notifications sent";
			}else if($note[0] == "info"){
				$log_var['changes']['notification'] = $note.". Assist couldn't log the notifications, however.";
			}else{
				$log_var['changes']['notification'] = $note;
			}
			$this->addActivityLog(self::LOG_TABLE, $log_var);
			
			/** NOTIFY ACTION OWNER THAT APPROVAL OF ACTION HAS BEEN REVERSED **/
			
			/** NOTIFY ACTION OWNER THAT ACTION HAS BEEN APPROVED **/
		//	ASSIST_HELPER::arrPrint($note);
		//	return array("info", $note['changes']);
			
			return array("ok",$this->getActionObjectName()." ".$this->getRefTag().$object_id." has been successfully unlocked.");	
		} else {
			return array("info","No change was made.  Please try again.");
		}
		
	}



    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return self::REFTAG; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		$sql = "SELECT action_deliverable_id FROM ".$this->getTableName()." WHERE action_id = ".$i;
		return $this->mysql_fetch_one_value($sql, "action_deliverable_id");
	}
	public function getTargetFieldName() { return $this->target_field; }
	public function getTargetUnitFieldName() { return $this->target_unit_field; }
	public function getActualFieldName() { return $this->actual_field; }
    
    
	public function getList($section,$options) {
		return $this->getMyList("ACTION", $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject("ACTION", $id,$options);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_id = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	public function getRawUpdateObject($obj_id) {
		$raw = $this->getRawObject($obj_id); //$this->arrPrint($raw);
		$data = array(
			$this->getTargetFieldName()=>$raw[$this->getTargetFieldName()],
			$this->getTargetUnitFieldName()=>$raw[$this->getTargetUnitFieldName()],
			$this->getActualFieldName()=>$raw[$this->getActualFieldName()],
			$this->getUpdateAttachmentFieldName()=>$raw[$this->getUpdateAttachmentFieldName()],
			$this->getProgressFieldName()=>$raw[$this->getProgressFieldName()],
			$this->getProgressStatusFieldName()=>$raw[$this->getProgressStatusFieldName()],
		);
		return $data;
	}
	
	public function getActionsFromObject($obj_id) {
		$delObject = new IDP1_DELIVERABLE();	
		$delids = $delObject->getDelIds($obj_id);
		foreach($delids as $item){
			foreach($item as $key=>$val){
				$dels[]=$val;
			}
		}
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_deliverable_id IN (".implode(",", $dels). ") AND action_status = 2";
		$data = $this->mysql_fetch_all($sql);
		return $data;
	}

	public function getOrderedObjects($contract_id=0,$del_id=0){
		if($del_id==0) {
			$delObject = new IDP1_DELIVERABLE();
			$sql_from = "INNER JOIN ".$delObject->getTableName()." D ON A.".$this->getParentFieldName()." = D.".$delObject->getIDFieldName()." AND D.".$delObject->getParentFieldName()." = ".$contract_id." WHERE ";
		} else {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$del_id." AND ";
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getDeadlineField()." as deadline
				, ".$this->getParentFieldName()." as parent_id
				, CONCAT(TK.tkname, ' ', TK.tksurname) as responsible_person
				FROM ".$this->getTableName()." A
				INNER JOIN assist_".$this->getCmpCode()."_timekeep TK ON TK.tkid = A.".$this->getOwnerFieldName()." 
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		return $res2;
	}


	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	 
	public function hasDeadline() { return true; }
	public function getDeadlineField() { return $this->deadline_field; }
	
	/**
	 * Function to get recipients and pass them to the controller
	 */
	public function getObjectRecipients($data){
		$id = $data['id'];
		$action = $data['activity'];
		$recipients = $this->getRecipients($id, $action,true);
		$extra = strlen($data['extra'])>0 ? $data['extra'] : "";
		$owner = ucwords($this->getObjectName(self::OBJECT_TYPE))." Owner";
		if(strlen($extra)>0 && $extra !== $recipients[$owner] && $extra !== $this->getUserID()){
			$recipients["Old ".$owner]=$recipients[$owner];
			$recipients["New ".$owner]=$extra;
			unset($recipients[$owner]);
		}
		$mailObj = new ASSIST_MODULE_EMAIL("Action","CA");
		$smsObj = new ASSIST_SMS();
		foreach($recipients as $key=>$val){
			if($val != $this->getUserID()){
				$adds = $mailObj->getEmailAddresses(array($val), array());
				$phone = $smsObj->getMobilePhones(array($val));
				$adds['to']['mobile'] = $phone[$val]['num'];
				$adds['to']['tkid'] = $val;
				foreach($adds['to'][0] as $a=>$b){
					$adds['to'][$a] = $b;
				}
				unset($adds['to'][0]);
				$recipes[$key] = $adds['to'];
			}
		}
		//$recipients = $mailObj->getEmailAddresses($recipients, array());
		//return $recipients;
		return $recipes;
	}
	
	public function getRecipients($id,$action,$mode=false){
		$headObj = new IDP1_HEADINGS();
		switch (strtoupper($action)) {
			case 'UNLOCK':
			case 'APPROVE':
			case 'DECLINE':
			case 'UPDATE':
				$sql = "SELECT action_owner FROM ".$this->getTableName()." WHERE action_id = $id ";
				if($mode){
					//$owner = ucwords($this->getObjectName(self::OBJECT_TYPE))." Owner";
					$owner = $headObj->getAHeadingNameByField("action_owner");
					$tkid[$owner] = $this->mysql_fetch_one_value($sql, "action_owner");
				}else{
					$tkid[] = $this->mysql_fetch_one_value($sql, "action_owner");
				}
				$parent_id = $this->getParentID($id);
				$delObject = new IDP1_DELIVERABLE();
				$res = $delObject->getRawObject($parent_id);
				$owner = $delObject->getOwnerFieldName();
				if($mode){
					//$owner_cps = ucwords($owner)." Owner";
					$owner_cps = $headObj->getAHeadingNameByField($owner);
					$tkid[$owner_cps] = $res[$owner];
				}else{
					$tkid[] = $res[$owner];
				}
				break;
			
			case 'EDIT':
				$sql = "SELECT action_owner FROM ".$this->getTableName()." WHERE action_id = $id ";
				if($mode){
					//$owner = ucwords($this->getObjectName(self::OBJECT_TYPE))." Owner";
					$owner = $headObj->getAHeadingNameByField("action_owner");
					$tkid[$owner] = $this->mysql_fetch_one_value($sql, "action_owner");
				}else{
					$tkid[] = $this->mysql_fetch_one_value($sql, "action_owner");
				}
				$parent_id = $this->getParentID($id);
				$delObject = new IDP1_DELIVERABLE();
				$res = $delObject->getRawObject($parent_id);
				$owner = $delObject->getOwnerFieldName();
				if($mode){
					//$owner_cps = ucwords($owner)." Owner";
					$owner_cps = $headObj->getAHeadingNameByField($owner);
					$tkid[$owner_cps] = $res[$owner];
				}else{
					$tkid[] = $res[$owner];
				}
				break;
				
			case 'CREATE':
				$sql = "SELECT action_owner FROM ".$this->getTableName()." WHERE action_id = $id ";
				if($mode){
					//$owner = ucwords($this->getObjectName(self::OBJECT_TYPE))." Owner";
					$owner = $headObj->getAHeadingNameByField("action_owner");
					$tkid[$owner] = $this->mysql_fetch_one_value($sql, "action_owner");
				}else{
					$tkid[] = $this->mysql_fetch_one_value($sql, "action_owner");
				}
				$parent_id = $this->getParentID($id);
				$delObject = new IDP1_DELIVERABLE();
				$res = $delObject->getRawObject($parent_id);
				$owner = $delObject->getOwnerFieldName();
				if($mode){
					//$owner_cps = ucwords($owner)." Owner";
					$owner_cps = $headObj->getAHeadingNameByField($owner);
					$tkid[$owner_cps] = $res[$owner];
				}else{
					$tkid[] = $res[$owner];
				}
				break;
		}	
		return $tkid;
	}
	
	public function getActionsForNote($user,$filter="ALL"){
		switch (strtoupper($filter)) {
			case 'ALL':
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".IDP1::ACTIVE.") = ".IDP1::ACTIVE." ORDER BY action_deadline ASC";
				break;
			case 'THISWEEK':
				$days_to_saturday = 6 - date("w");
				$sat_cutoff = ($days_to_saturday > 0)?strtotime('+'.$days_to_saturday.' day'):strtotime('now');
				$days_to_sunday = date("w"); 
				$sun_cutoff = ($days_to_sunday > 0)?strtotime('-'.$days_to_sunday.' day'):strtotime('now');
				$date = date('Y-m-d',$sat_cutoff);
				$date2 = date('Y-m-d',$sun_cutoff);
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".IDP1::ACTIVE.") = ".IDP1::ACTIVE." AND action_deadline <= '$date' AND action_deadline >= '$date2' ORDER BY action_deadline ASC";
				//return array($date,$date2);
				break;
			case 'OVERTHISWEEK':
				$days_to_saturday = 6 - date("w");
				$sat_cutoff = ($days_to_saturday > 0)?strtotime('+'.$days_to_saturday.' day'):strtotime('now');
				$date = date('Y-m-d',$sat_cutoff);
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".IDP1::ACTIVE.") = ".IDP1::ACTIVE." AND action_deadline <= '$date' ORDER BY action_deadline ASC";
				break;
			case 'TODAY':
				$now = strtotime('now');
				$date = date('Y-m-d', $now);
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".IDP1::ACTIVE.") = ".IDP1::ACTIVE." AND action_deadline = '$date' ORDER BY action_id DESC";
				break;
			case 'OVERTODAY':
				$now = strtotime('now');
				$date = date('Y-m-d', $now);
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".IDP1::ACTIVE.") = ".IDP1::ACTIVE." AND action_deadline <= '$date' ORDER BY action_id DESC";
				break;
			default:
				$sql = "SELECT * FROM ".$this->getTableName()." WHERE action_owner = $user AND action_progress < 100 AND action_status_id <> 3 AND (action_status & ".IDP1::ACTIVE.") = ".IDP1::ACTIVE." ORDER BY action_deadline ASC";
				break;
		}
			
		$res = $this->mysql_fetch_all($sql);
		return $res;
	}
	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>