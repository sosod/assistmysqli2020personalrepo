<?php
/**
 * To manage the ACTION object
 * 
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 * 
 * 
 * KPI
 * pk_id
 * pk_project_id
 * pk_kpi_id
 * pk_owner_id
 * pk_status
 * pk_insertdate
 * pk_insertuser
 * 
 * 
 * KPI_YEAR
 * pky_id			AUTO ID
 * pky_pi_id		INCOME ID
 * pky_iy_id		IDP_YEAR ID
 * pky_original		CURRENCY Original budget (imported from prior year)	- only allowed if first IDP
 * pky_adjustment	CURRENCY Adjustments (changes to original budget)	- only allowed if not first IDP
 * pky_total		CURRENCY Total
 * pky_status		AUTO status
 * pky_insertdate	AUTO timestamp
 * pky_insertuser	AUTO user
 * 
 */
 
class IDP1_PMKPIYEARS extends IDP1 {
    
    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "_status";
	protected $id_field = "_id";
	protected $parent_field = "_kpi_id";
	protected $name_field = "_iy_id";
	protected $attachment_field = "_attachment";
	
	protected $has_attachment = false;

    protected $ref_tag = "IDP/PKY";
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = "PROJECTKPIYEARS"; 
    const OBJECT_NAME = "indicator"; 
     
    const TABLE = "project_kpi_target";
    const TABLE_FLD = "pky";
    const REFTAG = "ERROR/CONST/REFTAG";
	
	const LOG_TABLE = "object";
    
    public function __construct($obj_id=0) {
        parent::__construct();
		$this->id_field = self::TABLE_FLD.$this->id_field;
		$this->status_field = self::TABLE_FLD.$this->status_field;
		$this->parent_field = self::TABLE_FLD.$this->parent_field;
		$this->name_field = self::TABLE_FLD.$this->name_field;
		$this->attachment_field = self::TABLE_FLD.$this->attachment_field;
		if($obj_id>0) {
			$this->object_id = $obj_id;
			$this->object_details = $this->getAObject($obj_id);
		}
		$this->object_form_extra_js = "";
    }
    
	
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
	//	unset($var['attachments']);
	//	unset($var['action_id']); //remove incorrect field value from add form
	//	unset($var['object_id']); //remove incorrect field value from add form
	/*	foreach($var as $key => $v) {
			if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			}
		}*/
		
		
		$var[$this->getTableField().'_status'] = IDP1::ACTIVE;
		$var[$this->getTableField().'_insertdate'] = date("Y-m-d H:i:s");
		$var[$this->getTableField().'_insertuser'] = $this->getUserID();

		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQL($var);
		$id = $this->db_insert($sql);
		if($id>0) {
/*			$changes = array(
				'response'=>"|".self::OBJECT_NAME."| ".$this->getRefTag.$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> IDP1_LOG::CREATE,
			);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectType())." ".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
				'log_var'=>$log_var
			);
			return $result;*/
			return true;
		}
		return array("error","Testing: ".$sql);
	}

	public function addYears($id,$original,$adjustment) {
		foreach($original as $iy_id => $orig) {
			$var = array(
				$this->getParentFieldName()=>$id,
				$this->getNameFieldName()=>$iy_id,
				$this->getTableField()."_original"=>$orig,
				$this->getTableField()."_adjustment"=>$adjustment[$iy_id],
				$this->getTableField()."_total"=>($orig+$adjustment[$iy_id])
			);
			$this->addObject($var);
		}
		return true;
	}

	public function editObject($var,$attach=array()) {
		$result = $this->editMyObject($var,$attach,$recipients,$noter,$include);
		return $result;
	}



    /*******************************
     * GET functions
     * */
    
    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return self::OBJECT_TYPE; }
    public function getMyObjectName() { return self::OBJECT_NAME; }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}
		
	}
    
    
	public function getList($section,$options) {
		return $this->getMyList($this->getMyObjectType(), $section,$options);
	}
     
	public function getAObject($id=0,$options=array()) {
		return $this->getDetailedObject($this->getMyObjectType(), $id,$options);
	}
     
	/***
	 * Returns an unformatted array of an object 
	 */
	public function getRawObject($obj_id) {
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$data = $this->mysql_fetch_one($sql);
		return $data;
	}
	
	public function getRawObjectsByPI($p_id) {
		if(is_array($p_id) && count($p_id)==0) {
			return array();
		}
		$sql = "SELECT *, ".$this->getIDFieldName()." as id FROM ".$this->getTableName()." WHERE ".$this->getParentFieldName();
		if(is_array($p_id)) {
			$sql.=" IN (".implode(",",$p_id).") ";
		} else {
			$sql.=" = ".$p_id;
		}
		$sql.=" AND ( ".$this->getStatusFieldName()." & ".self::ACTIVE." ) = ".self::ACTIVE;
		$rows = $this->mysql_fetch_all_by_id2($sql,$this->getParentFieldName(),$this->getNameFieldName());
		return $rows;
	}

	public function getOrderedObjects($parent_parent_id=0,$parent_id=0){
		if($parent_id==0) {
			$parentObject = new IDP1_PARENT();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
		} else {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");
		return $res2;
	}


	/**
	 * Returns status check for Actions which have not been deleted 
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where 
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false);
	}
     
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false,false);
	}
	 
	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }
	

	
    /***
     * SET / UPDATE Functions
     */
    
    
    
    
    
    
    
    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */    
    /****
     * PRIVATE functions: functions only for use within the class
     */
        
    
    
}


?>