<?php
$headings = $headingObject->getUpdateObjectHeadings($object_type);
//ASSIST_HELPER::arrPrint($headings);



$js.="
		var page_action = '".ucwords($object_type).".".$page_action."';
		var page_direct = '".$page_redirect."';
";


$check_for_object_value = (strtoupper($page_action)=="EDIT" || strtoupper($page_action)=="UPDATE");


?>
<div id=div_error class=div_frm_error>
	
</div>
<form name=frm_object>
	<table class=form width=100%><?php
	$form_valid8 = true;
	$form_error = array();
	foreach($headings['rows'] as $fld => $head) {
		if($head['parent_id']==0) {
			$val = "";
			$h_type = $head['type'];
			if($h_type!="HEADING") {
				$display_me = true;
				$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
				if($head['required']==1) { $options['title'] = "This is a required field."; }
				if($h_type=="REF") {
					$display_me = false;
				} elseif($h_type=="LIST" || $h_type == "FIN_TYPE" || $h_type=="CONTRACT_SUPPLIER") {
					switch($h_type) {
						case "CONTRACT_SUPPLIER":
							$listObject = new IDP1_CONTRACT_SUPPLIER();
							$list_items = $listObject->getObjectForSelectByContractID($child_object_id); 
							$h_type = "LIST";
							break;
						case "FIN_TYPE": 
							$list_items = $myObject->getFinanceTypes(); 
							$h_type = "LIST";
							break;
						default:
							$list_items = array();
							$listObject = new IDP1_LIST($head['list_table']);
							$list_items = $listObject->getActiveListItemsFormattedForSelect();
							break;
					}
					$options['options'] = $list_items;
					if(count($list_items)==0 && $head['required']==1) {
						$form_valid8 = false;
						$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
						$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
					}
					if($check_for_object_value) {
						$val = isset($object[$fld]) && strlen($object[$fld])>0 && $object[$fld]>0 ? $object[$fld] : "X";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
					} 
				} elseif($h_type=="DATE") {
					$options['options'] = $helper->getDateOptions($fld);
					$options['class'] = "";
					if($check_for_object_value) {
						$val = isset($object[$fld]) && strlen($object[$fld])>0 && $object[$fld]>0 && strtotime($object[$fld])>0 ? date("d-M-Y",strtotime($object[$fld])) : "";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
					}
				} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
					$options['extra'] = "processCurrency";
					if($check_for_object_value) {
						$val = isset($object[$fld]) && strlen($object[$fld])>0 && $object[$fld]>0 ? $object[$fld] : "";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
					}
				} elseif($h_type=="BOOL"){
					$h_type = "BOOL_BUTTON";
					$options['yes'] = 1;
					$options['no'] = 0;
					$options['extra'] = "boolButtonClickExtra";
					if($check_for_object_value) {
						$val = isset($object[$fld]) && strlen($object[$fld])>0 && $object[$fld]>0 ? $object[$fld] : "0";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
					}
				} else {
					if($check_for_object_value) {
						$val = isset($object[$fld]) ? $object[$fld] : "";
					} else {
						$val = isset($head['default_value']) ? $head['default_value'] : "";
					}
				}
				if($display_me) {
					$display = $displayObject->createFormField($h_type,$options,$val);
					$js.=$display['js'];
				}
			}
			if($display_me) {
				echo "
				<tr ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
					<th width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
					<td>".$display['display']."
					</td>
				</tr>";
			}
		}
	}
	if(!$form_valid8) {
		$js.="
		$('#div_error').html(error_html).dialog({modal: true,dialogClass:\"dlg_frm_error\"}).addClass('ui-corner-all').css({\"border\":\"2px solid #cc0001\",\"background-color\":\"#fefefe\"});
		AssistHelper.hideDialogTitlebar('id','div_error');
		AssistHelper.hideDialogCSS('class','dlg_frm_error');
		$('#div_error #btn_error').button().click(function() {
			$('#div_error').dialog(\"close\");
		}).blur().parent('p').addClass('float');
		$('html, body').animate({scrollTop:0});
		";
	}
		?><tr>
			<th></th>
			<td>
				<input type=hidden name=object_id value="<?php echo $object_id; ?>" />
				<input type=button value="Save Update" class=isubmit id=btn_save />
				<?php
				if($object_type!="FINANCE") {
					echo "<div id=div_warning style='' class=float>";
					ASSIST_HELPER::displayResult(array("warn","WARNING: Update responses can not be edited after they have been submited.  Please take care to ensure that all information is correct before clicking the Save button."));
					echo "</div>";
				}
				
				?>
			</td>
		</tr>
	</table>
</form>
<?php
$js.="
		var error_html = \"<p>The following errors need to be attended to before any ".$helper->getObjectName($object_type,true)." updates can be added:</p> <ul><li>".implode('</li><li>',$form_error)."</li></ul><p>Where required lists have not been populated, please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>\";
		
		
		
		$(\"#btn_save\").click(function() {
			$"."form = $(\"form[name=frm_object]\");
			
			Idp1Helper.processObjectForm($"."form,page_action,page_direct);
		});
		
		
		$(\"#".$myObject->getProgressStatusFieldName()." option:first\").next().prop(\"disabled\",true);
		
		";
		if(isset($add_another) && count($add_another)>0) {
			foreach($add_another as $key => $aa) {
				if($aa==true) {
					$js.= "
					var ".$key." = $('div.".$key." span:first-child').html();
					$('#btn_".$key."').click(function() {
						$('div.".$key." span').children('table:last').after(".$key.");
					});
					";
				}
			}
		}
		
		$js.="

		
		";
?>