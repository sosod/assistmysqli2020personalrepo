<?php
include("inc_header.php");

$display_type = !isset($display_type) ? "default" : $display_type; 

$object_type = $_REQUEST['object_type'];
$object_id = $_REQUEST['object_id'];
$dlg_id = isset($_REQUEST['dlg']) ? $_REQUEST['dlg'] : "dlg_child";

?>
<table class=tbl-container>
	<tr>
		<td ><?php 
		$js.= $displayObject->drawDetailedView($object_type, $object_id, false,false,false);
		?></td>
	</tr>
	<tr>
		<td><h3>Activity Log</h3>
			<?php
			$logObject = new IDP1_LOG(strtolower($object_type));
			$audit_log = $logObject->getObject(array('object_id'=>$object_id));
			$audit_log = $audit_log[0];
			echo $audit_log;
?>
		</td>
	</tr>
</table>
<script type=text/javascript>
$(function() {
	<?php 
	echo $js; 
	?>
	
	<?php
	if($display_type=="dialog") {
		echo $displayObject->getIframeDialogJS($object_type,$dlg_id,$_REQUEST);
/*		echo "
		var dlg_size_buffer = 70;
		var ifr_size_buffer = 20;
		window.parent.$('#dlg_child').dialog('open');
		";
		if($object_type=="ACTION") {
			echo "
			var my_width = $('table.tbl-container').css('width');
			
			if(AssistString.stripos(my_width,'px')>0) {
				my_width = parseInt(AssistString.substr(my_width,0,-2))+ifr_size_buffer;
				window.parent.$('#dlg_child').find('iframe').prop('width',(my_width)+'px');
				var dlg_width = window.parent.$('#dlg_child').dialog('option','width');
				var test_width = my_width+dlg_size_buffer;
				if(dlg_width > test_width) {
					window.parent.$('#dlg_child').dialog('option','width',test_width);
				}
			}
			";
		}
		echo "
		var my_height = $('table.tbl-container').css('height');
		//alert(my_height+' = '+AssistString.stripos(my_height,'px'));
		if(AssistString.stripos(my_height,'px')>0) {
			my_height = parseInt(AssistString.substr(my_height,0,-2))+ifr_size_buffer;
			window.parent.$('#dlg_child').find('iframe').prop('height',(my_height)+'px');
			var dlg_height = window.parent.$('#dlg_child').dialog('option','height');
			var test_height = my_height+dlg_size_buffer;
			if(dlg_height > test_height) {
				window.parent.$('#dlg_child').dialog('option','height',(test_height));
				var check = !(AssistHelper.hasScrollbar(window.parent.$('#dlg_child')));
				while(!check) {
					window.parent.$('#dlg_child').dialog('option','height',(test_height));
					test_height+=dlg_size_buffer;
					if(!(dlg_height > test_height) || !(AssistHelper.hasScrollbar(window.parent.$('#dlg_child')))) {
						check = true;
					}
				}
			}
		}
		";
		*/
	}
	?>
});
</script>