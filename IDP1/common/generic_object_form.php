<?php
/***********
 * REQUIRES: 
 * 	$formObject IF EDITING!!
 * 	$page_action (ADD || EDIT)
 *  $page_redirect_path = where to go on successful doAjax (page.php?index=request&)
 * 
 * either preset object_type && object_id
 *    OR $_REQUEST['object_type'] && $_REQUEST['object_id']
 * 
 */


if(!isset($child_object_type)) {
	$child_object_type = isset($_REQUEST['object_type']) ? $_REQUEST['object_type'] : "CONTRACT";
}
if(!isset($child_object_id)) {
	$child_object_id = isset($_REQUEST['object_id']) ? $_REQUEST['object_id'] : 0;
}
 
if($child_object_id>0) {
	$object = $childObject->getRawObject($child_object_id);
} else {
	$object = array();
}


$headings = $headingObject->getMainObjectHeadings($child_object_type,"FORM");
//ASSIST_HELPER::arrPrint($headings);



		$pa = ucwords($child_object_type).".".$page_action;
		$pd = $page_redirect_path;

$js.="
		var page_action = '".$pa."';
		var page_direct = '".$pd."';
";

$is_edit_page = (strrpos(strtoupper($page_action),"EDIT")!==FALSE);


?>
<div id=div_error class=div_frm_error>
	
</div>
<form name=frm_object>
	<table class=form width=100%><?php
	$form_valid8 = true;
	$form_error = array();
	foreach($headings['rows'] as $fld => $head) {
		if($head['parent_id']==0) {
			$val = "";
			$h_type = $head['type'];
			if($h_type!="HEADING") {
				$display_me = true;
				$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
				if($head['required']==1) { $options['title'] = "This is a required field."; }
				if($h_type=="LIST") {
					if($head['section']=="DELIVERABLE_ASSESS") {
						if($myObject->mustIDoAssessment()===FALSE) {
							$display_me = false;
						} else {
							//validate the assessment status field here
							$lt = explode("_",$head['list_table']);
							switch($lt[1]) {
								case "quality": $display_me = $myObject->mustIAssessQuality(); break;
								case "quantity": $display_me = $myObject->mustIAssessQuantity(); break;
								case "other": $display_me = $myObject->mustIAssessOther(); break;
							}
						}
					}
					if($display_me) {
						$list_items = array();
						$listObject = new IDP1_LIST($head['list_table']);
						$list_items = $listObject->getActiveListItemsFormattedForSelect();
						$options['options'] = $list_items;
						if(count($list_items)==0 && $head['required']==1) {
							$form_valid8 = false;
							$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
							$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
						}
						if($is_edit_page) {
							$val = isset($object[$fld]) && strlen($object[$fld])>0 && $object[$fld]>0 ? $object[$fld] : "X";
						} else {
							$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
						}
					}
				} elseif(in_array($h_type,array("MASTER","USER","OWNER","TEMPLATE","DEL_TYPE","DELIVERABLE"))) {
					$list_items = array();
					switch($h_type) {
						case "USER":
							$userObject = new IDP1_USERACCESS();
							$list_items = $userObject->getActiveUsersFormattedForSelect();
							break; 
						case "TEMPLATE":
							$templateObject = new IDP1_TEMPLATE();
							$list_items = $templateObject->getActiveTemplatesFormattedForSelect();
							break;
						case "OWNER":
							$ownerObject = new IDP1_CONTRACT_OWNER();
							$list_items = $ownerObject->getActiveOwnersFormattedForSelect();
							break;
						case "MASTER":
							$masterObject = new IDP1_MASTER($head['list_table']);
							$list_items = $masterObject->getActiveItemsFormattedForSelect();
							break; 
						case "DELIVERABLE":
							$delObject = new IDP1_DELIVERABLE();
							$list_items = $delObject->getDeliverablesForParentSelect($parent_id);
							break; 
						case "DEL_TYPE":
							$delObject = new IDP1_DELIVERABLE();
							$list_items = $delObject->getDeliverableTypes($parent_id);
							break; 
						default:
							echo $h_type; 
							break;
					}
					$options['options'] = $list_items;
					$h_type = "LIST";
					if(count($list_items)==0 && $head['required']==1) {
						$form_valid8 = false;
						$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
						$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
					}
					if($is_edit_page) {
						$val = isset($object[$fld]) && strlen($object[$fld])>0 && ($object[$fld]>0 || !is_numeric($object[$fld])) ? $object[$fld] : "X";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
					}
				} elseif($h_type=="DATE") {
					$options['options'] = array();
					$options['class'] = "";
					if($is_edit_page) {
						$val = isset($object[$fld]) && strlen($object[$fld])>0 && $object[$fld]>0 && strtotime($object[$fld])>0 ? date("d-M-Y",strtotime($object[$fld])) : "";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
					}
				} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
					$options['extra'] = "processCurrency";
					if($is_edit_page) {
						$val = isset($object[$fld]) && strlen($object[$fld])>0 && $object[$fld]>0 ? $object[$fld] : "";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
					}
				} elseif($h_type=="BOOL"){
					$h_type = "BOOL_BUTTON";
					$options['yes'] = 1;
					$options['no'] = 0;
					$options['extra'] = "boolButtonClickExtra";
					if($is_edit_page) {
						$val = isset($object[$fld]) && strlen($object[$fld])>0 && $object[$fld]>0 ? $object[$fld] : "0";
					} else {
						$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
					}
				} elseif($h_type=="REF") {
					if($page_action=="Add") {
						$val = "System Generated";
					} else {
						$val = $childObject->getRefTag().$object_id;
					}
				} elseif($h_type=="ATTACH") {
					$attachment_form = true;
					$options['action'] = $pa;
					$options['page_direct'] = $pd;
					$options['can_edit'] = $is_edit_page;
					$val = isset($form_object[$fld]) ? $form_object[$fld] : ""; 
					if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
				} else {
					if($is_edit_page) {
						$val = isset($object[$fld]) ? $object[$fld] : "";
					} else {
						$val = isset($head['default_value']) ? $head['default_value'] : "";
					}
				}
				if($display_me) {
					$display = $displayObject->createFormField($h_type,$options,$val);
					$js.=$display['js'];
				}
			} else {//if($fld=="contract_supplier") {
				if($is_edit_page) {
					$pval = isset($object[$fld]) ? $object[$fld] : "";
				} else {
					$pval = isset($head['default_value']) ? $head['default_value'] : "";
				}
				if($fld=="contract_assessment_statuses") {
					$listObject = new IDP1_LIST("deliverable_status");
					$list_items = $listObject->getActiveListItemsFormattedForSelect("id <> 1");
					$last_deliverable_status = 0;
					$sub_head = array();
					$sh_type = "BOOL";
					foreach($list_items as $key => $status) {
						$sub_head[$fld."-".$key] = array(
							'field'=>$fld."-".$key,
							'type'=>$sh_type,
							'name'=>$status,
							'required'=>1,
							'parent_link'=>"",
						);
						$last_deliverable_status = $key;
					}
				} else {
					$sub_head = $headings['sub'][$head['id']];
				}
				$td = "
				<div class=".$fld."><span class=spn_".$fld.">
					<table class=sub_form width=100%>";
					$add_another[$fld] = false;
					foreach($sub_head as $shead) {
						$sh_type = $shead['type'];
						$sfld = $shead['field'];
						if($fld=="contract_supplier") {
							$options = array('name'=>$sfld."[]",'req'=>$head['required']);
						} else {
							$options = array('id'=>$sfld,'name'=>$sfld,'req'=>$head['required']);
						}
						$val = "";
						if($sh_type=="LIST") {
							$list_items = array();
							$listObject = new IDP1_LIST($shead['list_table']);
							$list_items = $listObject->getActiveListItemsFormattedForSelect();
							$options['options'] = $list_items;
							if(count($list_items)==0 && $shead['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The".$shead['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if(count($list_items)>1) {
								$add_another[$fld] = true;
							}
						} elseif($sh_type=="BOOL"){
							$sh_type = "BOOL_BUTTON";
							$options['yes'] = 1;
							$options['no'] = 0;
							$options['extra'] = "boolButtonClickExtra";
							$val = 1;
						} elseif($sh_type=="CURRENCY") {
							$options['extra'] = "processCurrency";
							$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
						}
						$sdisplay = $displayObject->createFormField($sh_type,$options,$val);
						$js.= $sdisplay['js'];
						$td.="
						<tr ".(strlen($shead['parent_link'])>0 ? "class='tr_".$shead['parent_link']."'" : "").">
							<th width=40% class=th2>".$shead['name'].":</th>
							<td>".$sdisplay['display']."</td>
						</tr>";
					}
				$td.= "
					</table></span>
					".($add_another[$fld] ? "<p><input type=button value='Add Another' id=btn_".$fld." /></p>" : "")."
				</div>";
				$display = array('display'=>$td);
			} 
			if($display_me) {
				echo "
				<tr ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
					<th width=40%>".$head['name'].":".($head['required']==1?"*":"")."</th>
					<td>".$display['display']."
					</td>
				</tr>";
			}
		}
	}
	if(!$form_valid8) {
		$js.="
		$('#div_error').html(error_html).dialog({modal: true,dialogClass:\"dlg_frm_error\"}).addClass('ui-corner-all').css({\"border\":\"2px solid #cc0001\",\"background-color\":\"#fefefe\"});
		AssistHelper.hideDialogTitlebar('id','div_error');
		AssistHelper.hideDialogCSS('class','dlg_frm_error');
		$('#div_error #btn_error').button().click(function() {
			$('#div_error').dialog(\"close\");
		}).blur().parent('p').addClass('float');
		$('html, body').animate({scrollTop:0});
		";
	}
		?><tr>
			<th></th>
			<td>
				<input type=button value="Save <?php echo $helper->getObjectName($child_object_type); ?>" class=isubmit id=btn_save />
				<?php if($child_object_type=="CONTRACT") { echo "<input type=hidden name=last_deliverable_status value='".$last_deliverable_status."' />"; } ?>
				<input type=hidden name=object_id value="<?php echo $object_id; ?>" />
				<?php if($child_object_type!="CONTRACT" && strrpos(strtoupper($page_action), "ADD")!==FALSE) { echo "<input type=hidden name=".$childObject->getParentFieldName()." value=\"$object_id\" />"; } ?>
			</td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	$(function() {
		var error_html = "<p>The following errors need to be attended to before any <?php echo $helper->getObjectName($child_object_type)."s"; ?> can be added:</p> <ul><li><?php echo implode('</li><li>',$form_error); ?></li></ul><p>Where required lists have not been populated, please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>";
		
		
		<?php echo $js; ?>
		
		
		$("form[name=frm_object] select").each(function() {
			if($(this).children("option").length<=1 && !$(this).hasClass("required")) {
//				$(this).prop("disabled",true);
			}
		});
		$("#btn_save").click(function() {
			$form = $("form[name=frm_object]");
			var res = Idp1Helper.processObjectForm($form,page_action,page_direct);
			//var tmp = $res
			//console.log(res.responseText);
			//var dta = AssistForm.serialize($form); //$("<div />",{html:dta}).dialog("width:500px");
			//var result = AssistHelper.doAjax("inc_controller.php?action=<?php echo ucwords($child_object_type); ?>."+page_action,dta);
			//if(result[0]=="ok") {
			//	document.location.href = '<?php echo $page_redirect_path; ?>r[]='+result[0]+'&r[]='+result[1];
			//} else {
			//	AssistHelper.finishedProcessing(result[0],result[1]);
			//}
			return false;
		});
		
		function boolButtonClickExtra($btn) {
			var i = $btn.prop("id");
			var us = i.lastIndexOf("_");
			var tr = "tr_"+i.substr(0,us);
			var act = i.substr(us+1,3);
			if(act=="yes") {
				$("tr."+tr).show();
			} else {
				$("tr."+tr).hide();
			}
		}
		
		function processCurrency($inpt) {
			var h = $inpt.parents("tr").children("th").html();
			h = h.substr(0,h.lastIndexOf(":"));
			alert("Only numbers (0-9) and a period (.) are permitted in the "+h+" field.");		
		}
		
		<?php 
		if(isset($add_another) && count($add_another)>0) {
			foreach($add_another as $key => $aa) {
				if($aa==true) {
					echo "
					var ".$key." = $('div.".$key." span:first-child').html();
					$('#btn_".$key."').click(function() {
						$('div.".$key." span').children('table:last').after(".$key.");
					});
					";
				}
			}
		}
		?>
		
		$("#del_type").change(function() {
			if($(this).val()=="SUB") {
				$(".tr_del_type").show();
			} else {
				$(".tr_del_type").hide();
			}
		});
		
		if($("#del_parent_id").children("option").length<=1) {
			$("#del_type option[value=SUB]").prop("disabled",true);
		}
		$("#del_type").trigger("change");
		
		$("button").each(function() {
				if($(this).attr("button_status")=="active") {
					$(this).trigger("click");
				}
		});
		
		//alert(AssistForm.serialize($("form[name=frm_deliverable]")));
		
		$("form[name=frm_object] select").each(function() {
			//alert($(this).children("option").length);
			if($(this).children("option").length==2) {
				if($(this).children("option:first").val()=="X") {
					$(this).children("option:last").prop("selected",true);
				}
			}
		});
		
	});
</script>