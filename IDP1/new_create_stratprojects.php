<?php

$child_object_id = $_REQUEST['object_id'];

$section = "NEW";
$page_redirect_path = "new_create_idp.php?idp_object_id=".$object_id."&tab=".$my_tab."&tab_act=tree&";
$page_action = "Edit";
$dialog_url = "new_stratplan_object.php";


require_once("inc_header.php");
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());



//TIER1 = PROJECT
$t1Object = new IDP1_PROJECT();
$object_type = $t1Object->getMyObjectType();
$object_name = $t1Object->getMyObjectName();
$t1_object_type = $object_type;
$t1_object_name = $object_name;
$t1_id = $child_object_id;
$t1_object_id = $child_object_id;
$t1_object_details = $t1Object->getSimpleDetails($t1_object_id);

$idp_object_id = $t1_object_details['parent_id'];
$idpObject = new IDP1_IDP();
$idpyearsObject = new IDP1_IDPYEARS();
$idpYears = $idpyearsObject->getYearsForSpecificIDP($idp_object_id);
$iy_id_fld = $idpyearsObject->getIDFieldName();


$t_headings = array();
$t_row_headings = array();
$t_objects = array();
$t_parent_name = array();
$t_object_type = array();

//TIER2 = INCOME
$t2Object = new IDP1_PROJECTINCOME();
$t2_object_type = $t2Object->getMyObjectType();
$t2_object_name = $t2Object->getMyObjectName();

//TIER3 = COST
$t3Object = new IDP1_PROJECTCOST();
$t3_object_type = $t3Object->getMyObjectType();
$t3_object_name = $t3Object->getMyObjectName();
/*
//TIER4 = KPI / INDICATORS
$t4Object = new IDP1_PROJECTKPI();
$t4_object_type = $t4Object->getMyObjectType();
$t4_object_name = $t4Object->getMyObjectName();
*/




echo "<h2>".$t1_object_details['name']."</h2>
<p><button class=btn_view>View ".$helper->getObjectName($t1_object_name)."</button>&nbsp;<button class=btn_edit>Edit ".$helper->getObjectName($t1_object_name)."</button></p>";

echo "<h3>".$helper->getObjectName($t2_object_name)."</h3>";

$tier = "t2";

$js.=$displayObject->drawProjectDetailTable($tier, $t2_object_type, $t1_object_id,true,true);



echo "<h3>".$helper->getObjectName($t3_object_name)."</h3>";

$tier = "t3";

$js.=$displayObject->drawProjectDetailTable($tier, $t3_object_type, $t1_object_id,true,true);

/*
echo "<h3>".$helper->getObjectName($t4_object_name)."</h3>";

$tier = "t4";

$js.=$displayObject->drawProjectDetailTable($tier, $t4_object_type, $t1_object_id,true,true);
*/

?>
<script type=text/javascript>
$(function() {
	$(".btn_add").button({
		icons:{primary:"ui-icon-circle-plus"}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var ot = $(this).attr("object_type");
		var tier = $(this).attr("tier");
		var $form = $("form[name=frm_"+tier+"]");
		var error = false;
		$form.find("input:text").each(function() {
			if($(this).hasClass("required")) {
				//alert($(this).prop("name"));
				error = true;
			} else if($(this).val().length==0) {
				$(this).val("0");
			}
		});
		$form.find("select").each(function() {
			$(this).removeClass("required");
			if($(this).val()=="X") {
				$(this).addClass("required");
				error = true;
			}
		});
		if(error) {
			AssistHelper.finishedProcessing("error","Please correct the errors highlighted in red.");
		} else {
				
			var dta = AssistForm.serialize($form);
			console.log(dta);
			
			var result = AssistHelper.doAjax("inc_controller.php?action="+ot+".Add",dta);
			console.log(result);
			if(result[0]=="ok") {
				//FINISH THIS FUNCTION!!!!
				//$("<tr><td></td></tr>").append($(this).parent().parent().parent());
				var htmlDTA = "&tier="+tier+"&object_type="+ot+"&project_id=<?php echo $t1_object_id; ?>&object_id="+result['object_id'];
				//console.log(htmlDTA);
				var insertHTML = AssistHelper.doAjax("inc_controller.php?action=Display.getProjectDetailChildRow"+htmlDTA, dta);
				//console.log(insertHTML);
				var display = ""+insertHTML['display']+"";
				//console.log($(this).closest("table").prop("id"));
				//$(this).parent().parent().parent().find("tr:last").insertAfter(display);
				$(this).closest("table").append(display);
				var $edit_btn = $('<button />',
					{
						text: 'Edit',
						id: 'btn_'+tier+'_edit',
						object_type: ot,
						tier: tier,
						click: function (e) { 
							e.preventDefault();
							editLineItem($(this));
						}
					}).button({
						icons:{primary:"ui-icon-pencil"}
					}).addClass('btn_edit');
				$(this).closest("table").find("td.td_edit:last").append($edit_btn);
				
				
				$form.find("input:text").each(function() {
					$(this).val("0");
				});
				$form.find("select").each(function() {
					$(this).val("X");
				});
				$form.find("label").each(function() {
					$(this).text("0");
				});
			}
			AssistHelper.finishedProcessing(result[0],result[1]);
		}
	});
	$("input:text.CALC").blur(function() {
		var cf = $(this).attr("calc_field");
		var total = 0;
		$("input:text."+cf).each(function() {
			total+=parseFloat($(this).val());
		});
		$("label."+cf).text(total);
	});

	$(".btn_edit").button({
		icons:{primary:"ui-icon-pencil"}
	}).click(function(e) {
		e.preventDefault();
		editLineItem($(this));
	});

	function editLineItem($btn) {
			AssistHelper.processing();
//			alert($btn.prop("id"));
			AssistHelper.finishedProcessing("info","Still under development");
	}
	
	$(".btn_view").button({
		icons:{primary:"ui-icon-newwin"}
	}).click(function(e) {
		e.preventDefault();
		editLineItem($(this));
	});
	


	
	/*
	 * Adjust tables to display equally
	 */
	/*
	var table = new Array();
	var eq = 0;
	$("table.tbl_project_elements:first tr:first th").each(function() {
		table[eq] = 0;
		eq++;
	});
	//console.log(table);
	$("table.tbl_project_elements").each(function() {
		eq = 0;
		//console.log($(this).prop("id"));
		//$(this).find("tr:first th").each(function() { 
		$(this).find("tr:eq(1) td").each(function() { 
			//console.log($(this).css("width")+" = "+$(this).html());
			var w = parseInt(AssistString.substr($(this).css("width"),0,-2)); console.log(w);
			if(w>table[eq]) { table[eq] = w; }
			eq++;
		});
		//console.log(table);
	});
	
	$("table.tbl_project_items").each(function() {
		eq = 0;
		$(this).find("tr:first th").each(function() {
			$(this).css("width",table[eq]+"px");
			eq++;
		});
	});
	*/
	
	
	
});

</script>
<?php

//ASSIST_HELPER::arrPrint($_SESSION[$helper->getModRef()]['PROJECT']);
?>