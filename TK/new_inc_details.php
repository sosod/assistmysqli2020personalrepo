<?php
/*** SETUP VARIABLES ***/
//$inactive_user_modules = mysql_fetch_one("SELECT count(modid) as mc FROM assist_menu_modules WHERE modref = 'PMSPS' AND modyn = 'Y'");
//if($inactive_user_modules['mc']>0) { $use_inactive_users = true; } else { $use_inactive_users = false; }
$use_inactive_users = false;

$reserved_user_names = array("admin","support");
$reserved_names = array('admin'=>"Assist Administrator",'support'=>"Assist Support");
$sql = "SELECT DISTINCT tkuser FROM assist_".$cmpcode."_timekeep WHERE tkid <> '".$obj_id."' AND tkuser NOT IN ('".implode("','",$reserved_user_names)."') ORDER BY tkuser";
$used_user_names = $helper->mysql_fetch_fld_one($sql,"tkuser");

$defaults = $helper->mysql_fetch_fld2_one("SELECT field, value FROM assist_".$cmpcode."_setup WHERE ref = 'DF'","field","value");

unset($hsection['sys']);

$lists = array();
//STATUS
	$lists['status'] = array(1=>"Yes",2=>"No");
//MANAGER
	$sql = "SELECT tkid, CONCAT_WS(' ',tkname,tksurname) as value FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkuser NOT IN ('support','admin') ORDER BY tkname, tksurname";
	$lists['manager'] = array();
	$lists['manager']['S'] = "Self";
//	$lists['manager'] = array_merge($lists['manager'],mysql_fetch_fld2_one($sql,"tkid","value"));
//	$rs = getRS($sql);
//	while($row = mysql_fetch_assoc($rs)) {
    $rows = $helper->mysql_fetch_all($sql);
    foreach ($rows as $row){
		//arrPrint($row);
		$lists['manager'][$row['tkid']] = $row['value'];
	}
	unset($rs);
//GENDER
	$lists['gender'] = array('M'=>"Male",'F'=>"Female",'U'=>"Unknown");
//RACE
	$sql = "SELECT udfvcode, udfvvalue FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = 4 AND udfvyn <> 'N'";
	$lists['race'] = $helper->mysql_fetch_fld2_one($sql,"udfvcode","udfvvalue");
//OCCUPCATE
	$sql = "SELECT id, value FROM assist_list_value v WHERE v.index = 'OC' ORDER BY v.id ASC";
	$lists['occupcate'] = $helper->mysql_fetch_fld2_one($sql,"id","value");
//TITLE
	$lists['ppltitle'] = $helper->mysql_fetch_fld2_one("SELECT * FROM assist_".$cmpcode."_list_ppltitle","value","value");



/*** DISPLAY FORM ***/
?>
<p><span class=iinform>Note:</span><ul>
<li>All fields marked * are required except in the case of the <?php echo $hsection['contact']; ?>.</li>
<li><?php echo $hsection['contact']; ?> are only required for active <?php echo $site_name; ?> users.</li>
<li>Either a Landline or Mobile/Cellphone contact number is required for active <?php echo $site_name; ?> users.</li>
</ul></p>
<table id=tbl_details>
<?php

//echo '<pre style="font-size: 18px">';
//echo '<p>HSECTION</p>';
//print_r($hsection);
//echo '</pre>';
//
//echo '<pre style="font-size: 18px">';
//echo '<p>HEADINGS - BEFORE</p>';
//print_r($headings);
//echo '</pre>';

unset($hsection['emp']);
unset($headings['emp']);
unset($hsection['stat']);
unset($headings['stat']);

foreach($hsection as $hs => $ht) {
	echo "<tr><th colspan=2 style=\"background-color: #999999;\">$ht:</th></tr>";
	foreach($headings[$hs] as $fld => $h) {
		echo "<tr>";
			echo "<th id=th_".$fld." display_text='".$h['txt']."' >".$h['txt'].":".($h['required'] ? "*" : "")."</th>";
			echo "<td><input type=hidden id=".$fld."-r value=\"".$h['required']."\" />";
				switch($h['type']) {
				case "SYSTEM-TEXT":
					echo $user[$fld]."<input type=hidden name=$fld value=\"".$user[$fld]."\" />";
					break;
				case "SYSTEM-DATE":
					echo $user[$fld]!="0" ? date("d M Y H:i:s",$user[$fld]) : "";
					break;
				case "DATE":
					echo "<input type=text class=jdate size=15 id=$fld name=$fld readonly=readonly value=\"\" />";
					break;
				case "LIST":
					if($fld!="tkstatus" || $use_inactive_users) {
						echo "<select id=$fld name=$fld>".($fld!="tkstatus" ? "<option value=X>--- SELECT ---</option>" : "");
							if(isset($lists[$h['table']])) {
								foreach($lists[$h['table']] as $i => $v) {
								    $fld = isset($user[$fld]) ? $user[$fld] : "";
									echo "<option ".($fld==$i ? "selected" : "")." value=$i>$v</option>";
								}
							} else {
								$sql = "SELECT * FROM assist_".$cmpcode."_".$h['table']." WHERE yn = 'Y'";
								//$rs = getRS($sql);
								//while($row = mysql_fetch_assoc($rs)) {
								$rows = $helper->mysql_fetch_all($sql);
								foreach ($rows as $row){
									$i = $row['id'];
									$v = $row['value'];
									echo "<option ".($user[$fld]==$i ? "selected" : "")." value=$i>$v</option>";
								}
								unset($rs);
							}
						echo "</select>";
					} else {
						echo "Yes<input type=hidden name=$fld id=$fld value=1 />";
					}
					break;
				case "YN":
					echo "<select id=$fld name=$fld>
								<option selected value=N>No</option>
								<option value=Y>Yes</option>
							</select>";
					break;
				case "PREFIX": echo "+";
				case "TEL":
				case "EMAIL":
				case "USER":
				case "TEXT":
					$value = "";
					if(isset($user[$fld]) && strlen($user[$fld])>0) {
						if($fld=="tktel" || $fld == "tkmobile") {
							$v = $user[$fld];
							for($i=0;$i<strlen($v);$i++) {
								if(is_numeric(substr($v,$i,1))) { $value.=substr($v,$i,1); }
							}
						} elseif($fld=="tkidnum") {
							if($user[$fld]!="0") {
								$value = $user[$fld];
							}
						} else {
							$value = $user[$fld];
						}
					} else if(isset($h['default']) && strlen($h['default'])>0) {
						$value = $h['default'];
					}
					echo "<input type=text id=$fld name=$fld value=\"".$value."\" class=".$h['type']
					.(isset($h['width']) && $helper->checkIntRef($h['width']) ? " size=".$h['width'] : "")
					.(isset($h['max']) && $helper->checkIntRef($h['max']) ? " maxlength=".$h['max'] : "")
					." /> "
					.($h['type']=="USER" ? "<label id=user_error for=tkuser class=idelete></label>" : "")
					.(isset($h['example']) && strlen($h['example'])>0 ? "<br /><span class=i style=\"font-size: 7pt;\">".$h['example']."</span>" : "");
					break;
				}
			echo "</td>";
		echo "</tr>";
	}
}
?>
</table>
<script type=text/javascript>
$(function() {
	var exist_user_names = new Array;
	<?php for($u=0;$u<count($used_user_names);$u++) { echo "exist_user_names[$u] = '".$used_user_names[$u]."';"; } ?>
	var res_user_names = new Array;
	<?php for($u=0;$u<count($reserved_user_names);$u++) { echo "res_user_names[$u] = '".$reserved_user_names[$u]."';"; } ?>
	var res_names = new Array;
	<?php foreach($reserved_names as $u => $n) { echo "res_names['$u'] = '".$reserved_names[$u]."';"; } ?>
	$(".jdate").datepicker("option", "yearRange", '<?php echo (date("Y")-100).":".date("Y");?>');
	$(".jdate").datepicker("option", "maxDate", '+1m');
	$("#tbl_details th").addClass("left");
	$("th").addClass("top");
	var load_err = validateForm();
	$("input:button.ilock").click(function() {
		if(confirm("Are you sure you wish to LOCK this user?")==true) {
			$("#frm_details #act").val("LOCK");
			$("#frm_details").submit();
		}
	});
	$("input:button.idelete").click(function() {
		if(confirm("Are you sure you wish to TERMINATE this user?\n\nWARNING: This action cannot be undone!")==true) {
			$("#frm_details #act").val("TERM");
			$("#frm_details").submit();
		}
	});
	$("select").change(function() { var edit_err = validateForm(); });
	$("input:text").keyup(function() { var edit_err = validateForm(); });
	$("input:button.isubmit").click(function() {
AssistHelper.processing();
		var valid = validateForm();
		var err = valid[0];
		var other = valid[1];
		if(err.length>0 || other.length>0) {
			var res = "Please review the following errors:";
			if(err.length>0) {
				res = res+"\n\nRequired fields:"+err;
			}
			if(other.length>0) {
				res = res + "\n\nOther errors:"+other;
			}
			alert(res);
AssistHelper.closeProcessing();
		} else {
			$("#frm_new").submit();
		}
	});
	function validateForm() {
		var stat = $("#tkstatus").val();
		stat *=1;
		var id = ""; 		var r = "";		var v = "";		var t = "";
		var err = ""; var other = "";	var res = "";
		var len = false; var sel = false;
		var contact = new Array("tkphprefix","tktel","tkmobile","tkemail","tkfax");
		var contact2 = new Array("tktel","tkmobile");
		$("input:text, select").each(function() {
			id = $(this).attr("id");
			$(this).removeClass("required");
			if(id=="tkuser") {
				var o2 = validateUser();
				if(o2.length>0) {
					other = other + "\n - "+o2;
				}
			}
			r = $("#"+id+"-r").val();
			if( (stat==1 && r==1 && jQuery.inArray(id,contact2)<0 ) || (stat==2 && r==1 && jQuery.inArray(id,contact)<0 && id!="tkuser")) {
				v = $(this).val();
				len = (v.length == 0);
				sel = ($(this).get(0).tagName =="SELECT" && v.toLowerCase() == "x");
				if(stat==1 && id=="tkemail" && v.length > 0) {
					if( (!(v.indexOf("@") > 0) || !(v.indexOf(".") > 0))
							||
						(v.indexOf("@") != v.lastIndexOf("@"))
							||
						(v.indexOf("@") > v.lastIndexOf("."))
							||
						(v.indexOf(" ") >= 0)
					) {
						other = other + "\n - Invalid email address format";
						$("#tkemail").addClass("required");
					} else {
						var email_test_result = AssistHelper.doAjax("ajax_controller.php?act=ASSIST_TK.validateUserEmail","email="+v);
						if(email_test_result[0]=="error") {
							other = other + "\n - Invalid email address format";
							$("#tkemail").addClass("required");
						}
					}
				}
				if(len || sel) {
					t = $("#th_"+id).attr("display_text");

					$(this).addClass("required");
					err = err + "\n - "+t;
				}
			}
		});
		var charN = new Array("0","1","2","3","4","5","6","7","8","9");
		var tktel = $("#tktel").val();
		var tkmobile = $("#tkmobile").val();
		if(tktel.length==0 && tkmobile.length==0 && stat==1) {
			err = err+"\n - At least one contact number is required.";
			$("#tktel").addClass("required");
			$("#tkmobile").addClass("required");
		} else {
			$("#tktel").removeClass("required");
			$("#tkmobile").removeClass("required");
			if(tktel.length>0) {
				for(i=0;i<tktel.length;i++) {
					if(jQuery.inArray(tktel.substr(i,1),charN)<0) {
						other = other + "\n - Only numbers are allowed for the <?php echo $headings['contact']['tktel']['txt']; ?> number.";
						$("#tktel").addClass("required");
						break;
					}
				}
			}
			if(tkmobile.length>0) {
				for(i=0;i<tkmobile.length;i++) {
					if(jQuery.inArray(tkmobile.substr(i,1),charN)<0) {
						$("#tkmobile").addClass("required");
						other = other + "\n - Only numbers are allowed for the <?php echo $headings['contact']['tkmobile']['txt']; ?> number.";
						break;
					}
				}
			}
		}
		var tkfax = $("#tkfax").val();
		$("#tkfax").removeClass("required");
		if(tkfax.length>0) {
			for(i=0;i<tkfax.length;i++) {
				if(jQuery.inArray(tkfax.substr(i,1),charN)<0) {
					$("#tkfax").addClass("required");
					other = other + "\n - Only numbers are allowed for the <?php echo $headings['contact']['tkfax']['txt']; ?> number.";
					break;
				}
			}
		}
		return [err,other];
	}
	$("#tksurname").blur(function() {
		var n = $.trim($("#tkname").val());
		$("#tkname").val(n);
		var v = $.trim($(this).val());
		$(this).val(v);
		var tku = n.substring(0,1)+v;
		tku = tku.toLowerCase().replace(/[^a-zA-Z 0-9]+/g, '');
		$("#tkuser").val(tku);
		validateUser();
	});
	$("#tkuser").keyup(function() {
		validateUser();
	});
	function validateUser() {
		var allowed_char = new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
		var u = $("#tkuser").val();
		var n = $("#tkname").val();
		var s = $("#tksurname").val();
		var tk = n + " "+ s;
		if(u.length > 0 && jQuery.inArray(u,exist_user_names)>=0) {
			$("#tkuser").addClass("required");
			$("#user_error").attr("innerText","This user name is already in use.");
			var err = "This user name is already in use.";
		} else if(u.length > 0 && jQuery.inArray(u,res_user_names)>=0) {
			$("#tkuser").addClass("required");
			$("#user_error").attr("innerText","This user name is a reserved user name and may not be used.");
			var err = "This user name is a reserved user name and may not be used.";
		} else if(u.length>0) {
			$("#tkuser").removeClass("required");
			var err = "";
			for(i=0;i<u.length;i++) {
				if(jQuery.inArray(u.substr(i,1),allowed_char)<0) {
					$("#tkuser").addClass("required");
					err = "Only alphanumeric characters are allowed for the user name.";
					break;
				}
			}
		} else {
			$("#tkuser").addClass("required");
			$("#user_error").attr("innerText","User name is required.");
			var err = "A user name is required.";
		}
		return err;
	}
});
</script>