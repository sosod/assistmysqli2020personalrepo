<?php
/**
 * - from calling page
 * @var array $nav_section
 * @var string $section
 * @var string $cmpcode
 * @var ASSIST_MODULE_HELPER $helper - from inc_header.php via calling page
 */
?>
<script type=text/javascript>
$(function() {
	<?php //if($section=="new") { echo "$(\"input:button\").attr(\"disabled\",\"disabled\");"; } ?>

	$("input:button.action").click(function() {
		var i = $(this).attr("id");
		var a = $(this).val();
		if(a=="Reset Login Window") {
			$("#dlg_reset #id").val(i);
			$("#dlg_reset").dialog("open");
		} else {
			var s = '<?php echo $section; ?>';
			document.location.href = 'manage.php?page=users_edit&section='+s+'&a='+a+'&i='+i;
		}
	});
});
</script>
<?php

	$empObject = new TK_EMPLOYEE();
	$emp_modref = strtolower($empObject->getEmpModRef());
	$today = $empObject->getToday();
	$title = $nav_section[$section]." Users";

	$headings = array(
		'tkid'=>array('fld'=>"tkid",'txt'=>"Ref",'type'=>"HEAD",'width'=>30),
		'tkn'=>array('fld'=>"tkn",'txt'=>"Name",'type'=>"TEXT",'width'=>200),
		'tkadddate'=>array('fld'=>"tkadddate",'txt'=>"Date Added",'type'=>"DATE",'width'=>100),
		'tklogindate'=>array('fld'=>"tklogindate",'txt'=>"Last Login",'type'=>"DATE",'width'=>100),
		'tktermdate'=>array('fld'=>"tktermdate",'txt'=>"Date Terminated",'type'=>"DATE",'width'=>100),
		'tkstatus'=>array('fld'=>"tkstatus",'txt'=>"Status",'type'=>"LIST",'list'=>"status",'width'=>100),
	);
	$default_button = array(
		'active'	=> array("Edit Details","Edit Access"),
		'inactive'	=> array("Edit Details","Edit Access"),
		'new'		=> array("Edit Details","Edit Access","Reset Login Window"),
		'locked'	=> array("Unlock"),
		'term'		=> array("Restore"),
		'reset'		=> array("Reset Login Window"),
		'employees'	=> array("Convert to Active User"),
	);
	$button = array();
switch($section) {
case "active":
	$where = "tkstatus = 1 AND tklogindate > 0 AND tktermdate = 0 AND tklock <> 'Y'";
	$head = array("tkid","tkn","tkadddate","tklogindate");
	$button = $default_button['active'];
	break;
case "inactive":
	$where = "tkstatus = 2 AND tktermdate = 0 AND tklock <> 'Y'";
	$head = array("tkid","tkn","tkadddate");
	$button = $default_button['inactive'];
	break;
case "new":
	$where = "tkstatus = 1 AND tklogindate = 0 AND tktermdate = 0 AND tklock <> 'Y'";
	$head = array("tkid","tkn","tkadddate","tkstatus");
	$button = $default_button['new'];
	break;
case "locked":
	$where = "tkstatus > 0 AND tkstatus <> 2 AND tklock = 'Y'";
	$head = array("tkid","tkn","tkadddate","tklogindate");
	$button = $default_button['locked'];
	break;
case "term":
	$where = "tkstatus = 0 AND tkid NOT IN (SELECT emp_tkid FROM assist_".$cmpcode."_".$emp_modref."_employee WHERE (emp_status & ".EMP1::NON_USER.") = ".EMP1::NON_USER.")";
	$head = array("tkid","tkn","tkadddate","tktermdate");
	$button = $default_button['term'];
	break;
case "all":
	$where = "tkstatus <> 2 AND tkid NOT IN (SELECT emp_tkid FROM assist_".$cmpcode."_".$emp_modref."_employee WHERE (emp_status & ".EMP1::NON_USER.") = ".EMP1::NON_USER.")";
	$head = array("tkid","tkn","tkadddate","tklogindate","tktermdate","tkstatus");
	break;
case "employees":
	$where = "tkstatus <> 1 AND tkid IN (SELECT emp_tkid FROM assist_".$cmpcode."_".$emp_modref."_employee WHERE (emp_status & ".EMP1::NON_USER.") = ".EMP1::NON_USER.")";
	//$sql = "SELECT tkid, CONCAT_WS(' ',tkname, tksurname) as tkn, tkadddate, tklogindate, tktermdate, tkstatus
	//	FROM assist_".$cmpcode."_timekeep WHERE (tkuser <> 'support' AND tkid <> '0000') "
	//	.(strlen($where)>0 ? "AND $where" : "").
	//	" ORDER BY tkname, tksurname, tkid";
	$head = array("tkid","tkn","tkadddate","tkstatus");
	break;
}

echo "<h1>Manage >> Users >> $title </h1>";

if(isset($_REQUEST['r']) && is_array($_REQUEST['r'])) { $r = $_REQUEST['r']; $r[1] = stripslashes($r[1]); $helper->displayResult($r); }
if($section=="term") {
?>
<p><span class=iinform>Note:</span><ul>
<li>Clicking the "Restore" button will reactive the user on Ignite Assist.</li>
<li>If an email address, user name AND password are detected for the user, they will be reactivated as an ACTIVE user<br />otherwise they will be reactivated as an INACTIVE user.</li>
<li>The user's menu access to the various modules will NOT be restored.<br />To grant the user access to a module, please use the "Edit Details" option after they have been restored.</li>
</ul></p>
<?php
}
//if($section!="employees") {
	$sql = "SELECT tkid, CONCAT_WS(' ',tkname, tksurname) as tkn, tkadddate, tklogindate, tktermdate, tkstatus, tkloginwindow, tklock
		FROM assist_".$cmpcode."_timekeep WHERE (tkuser <> 'support' AND tkname<>'Ignite' AND tksurname<>'Support' AND tkid <> '0000') "
		.(strlen($where)>0 ? "AND $where" : "").
		" ORDER BY tkname, tksurname, tkid";
//}
$users = $helper->mysql_fetch_all($sql);
if($section=="employees") {
	$sql = "SELECT emp_tkid FROM assist_".$cmpcode."_".$emp_modref."_employee WHERE (emp_status & ".EMP1::NON_USER.") = ".EMP1::NON_USER." AND (emp_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE;
	$emps = $helper->mysql_fetch_all_by_id($sql,"emp_tkid");
	foreach($users as $i => $u) { 
		$ti = $u['tkid'];
		if(isset($emps[$ti])) {
			$users[$i]['tkstatus'] = 2;
		} elseif($u['tkstatus']==4) {
			$users[$i]['tkstatus'] = 0;
		}
	}
}
?>
<table>
<thead>
	<tr><?php
	foreach($head as $fld) {
		echo "<th width=".$headings[$fld]['width'].">".$headings[$fld]['txt']."</th>";
	}
	?>
		<th></th>
	</tr>
</thead>
<tbody>
<?php
foreach($users as $tk) {
	echo "	<tr>";
	foreach($head as $fld) {
		$h = $headings[$fld];
		if($h['type']=="HEAD") {
			echo "<th>".$tk[$fld]."</th>";
		} else {
			echo "<td class=\"middle".($h['type']!="TEXT" ? " center" : "")."\">";
			switch($h['type']) {
				case "DATE":
					if($fld!="tktermdate" || $tk['tkstatus']==0) {
						echo ASSIST_HELPER::checkIntRef($tk[$fld]) ? date("d M Y H:i:s",$tk[$fld]) : "";
					}
					break;
				case "LIST":
					switch($h['list']) {
					case "status":
						$s = $tk[$fld];
						switch($s) {
							case 0:	
							case 4:
								echo "<span class=idelete>Terminated</span>"; 
								if($section!="employees") {
									$button = $default_button['term']; 
								} else {
									$button = array();
								}
								break;
							case 1:
								if($tk['tklock']=="Y") {
									echo "<span class=iinform>Locked</span>";
									$button = $default_button['locked'];
								} elseif(ASSIST_HELPER::checkIntRef($tk['tklogindate'])) {
									echo "<span class=isubmit>Active</span>";
									$button = $default_button['active'];
								} else {
									if(strtotime($tk['tkloginwindow']) < $today) {
										echo "<div class=required> New (Locked) </div>";
										$button = $default_button['new'];
									} else {
										echo "<div class=ok>&nbsp;New&nbsp;</div>";
										$button = $default_button['new'];
									}
								}
								break;
							case 2:
								echo "<span class=iinform>Non-User Employee</span>";
								$button = $default_button['employees'];
								break;
						}
						break;
					default:
						echo $tk[$fld];
					}
					break;
				case "TXT":
				default:
					echo $tk[$fld];
					break;
			}
			echo "</td>";
		}
	}
//	echo "		<td class=center><input type=button value=".$button." id=".$tk['tkid']." /></td>
	echo "		<td class=middle>".(count($button)>0 ? "<input type=button class=action value=\"".implode("\" id=".$tk['tkid']." /> <input type=button class=action value=\"",$button)."\" id=".$tk['tkid']." />":"")."</td>
	</tr>";
}
?>
</tbody>
</table>
<div id=dlg_reset title="Reset Login Window">
<form name=frm_reset id=frm_reset action=manage.php method=post><span id=spn_reset>
<input type=hidden name=page value=users_edit />
<input type=hidden name=section value=<?php echo $section; ?> />
<input type=hidden name=a value="reset" />
<input type=hidden name=i id=id value="" />
<input type=hidden name=act id=act value="Yes" />
<p class=center>Do you wish to send an email to the user reminding them of their login details?</p>
<p class=center><input type=button name=yes class=isubmit value=Yes /> <input type=button name=no value=No class=idelete /></p>
</span></form>
<p class=center id=loader>Processing...<br /><img src="../pics/ajax_loader_v2.gif" /></p>
</div>
<script type=text/javascript>
$(document).ready(function() {
	<?php //if($section=="new") { echo "$(\"input:button\").attr(\"disabled\",\"\");"; } ?>

	$("#dlg_reset #loader").hide();
	$("#dlg_reset").dialog({
		autoOpen: false,
		modal: true,
	});
	$("#dlg_reset input:button").click(function() {
		$("#dlg_reset #act").val($(this).val());
		$("form[name=frm_reset]").submit();
		$("#dlg_reset #loader").show();
		$("#dlg_reset #spn_reset").hide();
	});


	/*$("input:button").each(function() {
		alert($(this).attr("disabled"));
	});*/

});
</script>
</body>
</html>