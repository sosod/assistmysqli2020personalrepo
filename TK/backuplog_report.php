<?php
error_reporting(-1);

include("../module/autoloader.php");

$db = new ASSIST_DB("master");
$helper = new ASSIST_HELPER();

$helper->echoPageHeader();

//$sql = "SELECT * FROM assist_backup WHERE status = 2 AND datetime > DATE_SUB(CURDATE(), INTERVAL 1 YEAR) ORDER BY datetime DESC";
$d = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),1,date("Y")-1));

$sql = "SELECT * FROM assist_backup WHERE status & 2 = 2 AND datetime > DATE('$d') ORDER BY datetime DESC";
$logs = $db->mysql_fetch_all($sql);


?>
<h1>System Backup Status Report</h1>
<p class=i>Action iT Assist Backup Policy (<a href='Action iT Assist Backup Policy 2015-07-09 published.pdf' target=_blank>Link</a>)</p>
<table>
	<tr>
		<th>Date</th>
		<th>Backup<br />Frequency</th>
		<th>Backup<br />Type</th>
		<th>Message</th>
	</tr>
<?php
	foreach($logs as $log) {
		switch(trim(strtolower($log['result']))) {
			case "successful":
				$icon = "ok";
				break;
			case "warnings":
				$icon = "info";
				break;
			default:
				$icon = "error";
				break;
		}
		echo "
		<tr>
			<td>".date("d M Y",strtotime($log['datetime']))."</td>
			<td>".ucwords(strtolower($log['backup_freq']))."</td>
			<td>".ucwords(strtolower($log['backup_type']))."</td>
			<td>".($helper->getDisplayIcon($icon))." <span>".($log['message'])."</span></td>
		</tr>";
	}
?>
</table>

<script type="text/javascript">
$(function() {
	$("span").css("display","inline");
	//$("span").css("border","1px dashed #fe9900");
	$("td").find("span:first").css("padding-left","13px");
});
</script>