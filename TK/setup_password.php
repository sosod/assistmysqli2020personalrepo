<?php
//error_reporting(-1);
require_once "../module/autoloader.php";

$me = new ASSIST_TK();
$logs = $me->getPasswordLoginLogsHTML();
$displayObj = new ASSIST_MODULE_DISPLAY();

ASSIST_HELPER::echoPageHeader();



function drawTables($title,$settings,$action) {
	global $displayObj;
	$js = "";
	echo "
	<h2>".$title."</h2>
	<table>
		<tr>
			<th colspan=2>Setting</th>
			<th>Super User</th>
			<th>Normal User</th>
			<th></th>
		</tr>";
	foreach($settings as $code => $pwd) {
		$opt = array('normal'=>"",'super'=>"");
		switch($pwd['form_type']) {
			case "LIST": 
				$opt['normal'] = "<select id=".$code."_normal>";
				$opt['super'] = "<select id=".$code."_super>";
				$o = explode("_",$pwd['form_options']);
				$inc = $o[1];
				$o = explode("|",$o[0]);
				for($i=$o[0];$i<=$o[1];$i+=$inc) {
					$opt['normal'].="<option value=$i ".($pwd['normal_user']==$i ? "selected" : "").">$i</option>";
					$opt['super'].="<option value=$i ".($pwd['super_user']==$i ? "selected" : "").">$i</option>";
				}
				$opt['normal'].="</select> ".$pwd['form_unit'];
				$opt['super'].="</select> ".$pwd['form_unit'];
				break;
			case "BOOL":
				$prop = array();
				$prop['form'] = "vertical";
				$prop['id'] = $code."_normal";
				$dN = $displayObj->getBoolButton($pwd['normal_user'],$prop);
				$prop['id'] = $code."_super";
				$dS = $displayObj->getBoolButton($pwd['super_user'],$prop);
				$opt['normal'] = $dN['display'];
				$opt['super'] = $dS['display'];
				$js.=$dN['js'];
				$js.=$dS['js'];
				break;
		}
		echo "
		<tr>
			<td class=b width=150px>".$pwd['name'].($code=="pwd_age" ? "*" : "")."</td>
			<td width=350px>".$pwd['details']."</td>
			<td width=120px>".$opt['super']."</td>
			<td width=120px>".$opt['normal']."</td>
			<td><button class=btn_save id=".$code." act='".$action."'>Save</button></td>
		</tr>";
	}
	echo "</table>";
	return $js;
}



?>
<h1>Setup >> Passwords / Login Settings</h1>
<div class='float' style='width:506px; ' id=div_logs>
	<?php echo $logs; ?>
</div>
<div>
<?php
$password_settings = $me->getPasswordSettings();
$js = drawTables("Passwords", $password_settings,"savePasswordSetting");
$str = "* Changing the password age setting will have consequences:
<br />&nbsp;-&nbsp;Changing the setting to 0 will reset all user password change date to 'N/A';  This can not be corrected.
<br />&nbsp;-&nbsp;Changing the setting to any number other than 0 will set any user password change dates currently set to 'N/A' to today.";
echo "<div style='width:740px'>"; ASSIST_HELPER::displayResult(array("info",$str)); echo "</div>";

$att_settings = $me->getAttemptSettings();
$js.= drawTables("Login Attempts", $att_settings,"saveAttemptSetting");

$inact_settings = $me->getInactivitySettings();
$js.= drawTables("User Inactivity", $inact_settings,"saveInactivitySetting");

?>


</div>
<script type=text/javascript>
$(function() {
	<?php echo $js; ?>
	$("button.btn_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var i = $(this).prop("id");
		var n = $("#"+i+"_normal").val();
		var s = $("#"+i+"_super").val();
		var act = $(this).attr("act"); 
		var dta = "code="+i+"&normal_user="+n+"&super_user="+s;
		var result = AssistHelper.doAjax("ajax_controller.php?act=ASSIST_TK."+act,dta);
		if(result[0]=="ok") {
			AssistHelper.finishedProcessing(result[0],result[1]);
			$("#div_logs").html(result[2]);
		} else {
			AssistHelper.finishedProcessing(result[0],result[1]);
		}
	});
	$("button").children(".ui-button-text").css({"padding-top":"3px","padding-bottom":"3px","padding-left":"25px","font-size":"80%"});
	//$('button.bool_btn')
});
</script>





