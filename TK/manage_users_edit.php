<?php
$var = $_REQUEST;
$act = strtolower($var['a']);
$obj_id = $var['i'];
$helper = new ASSIST_MODULE_HELPER();
//GET EXISTING USER DETAILS
$user = isset($obj_id) && strlen($obj_id) > 0 ? $helper->mysql_fetch_one("SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$obj_id."'") : die("<P>A fatal error has occurred and the select user could not be found.  Please go back and try again.</p>");

$structure = array(
	'details'	=> array("active","inactive","new","all","employees"),
	'modules'	=> array("active","inactive","new","all"),
	'other'		=> array("locked","term","new","all"),
);
$other_act = array("unlock","reset","restore");
//arrPrint($var);
$title = $nav_section[$section]." Users";
echo "<h1>Manage >> Users >> $title </h1>";

if(($act == "edit details" || $act=="edit" || $act=="convert to active user") && in_array($section,$structure['details'])) {
	if($section=="employees") {
		$use_inactive_users = true;
	} else {
		$use_inactive_users = false;
	}
	include("manage_inc_details.php");
} elseif($act == "edit access" && in_array($section,$structure['modules'])) {
	include("manage_inc_access.php");
} elseif(in_array($act,$other_act) && in_array($section,$structure['other'])) {
	include("manage_inc_other.php");
} else {
	die("<p>An error occurred trying to determine your intended action.  Please go back and try again.</p>");
}
?>
</body>
</html>