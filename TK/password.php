<?php

if($_SERVER["PHP_SELF"]=="/password1.php") {
	include_once("module/autoloader.php");
} else {
	include_once("../module/autoloader.php");
}


$me = new ASSIST_TK();

//error_reporting(-1);

include("inc_headings.php");

$reason = isset($_REQUEST['reason']) ? $_REQUEST['reason'] : "";
$me->setPasswordChangeReason($reason);
$js = "";

$me->displayPageHeaderJQ();

$me->displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


$iframe=false;
switch($reason) {
	case "reset":
		echo "<h1>Manage >> Reset Password</h1>";
		$iframe=true;
		$tkid = "";
		break;
	case "age":
	case "new":
	case "user":
	case "force":
	default:
		echo "<h1>Password</h1><h2>Change Password</h2>";
		$tkid = $me->getUserID();
		break;
}

$default_table_size = 700;
$default_iframe_size = 500;
$buffer = 10;

if($_SESSION['screen']['width']<$default_table_size) {
	$iframe_location="bottom";
	$w = $_SESSION['screen']['width'];
} else {
	$w = $default_table_size;
	if($_SESSION['screen']['width']<($w+$default_iframe_size+$buffer)) {
		$iframe_location="bottom";
	} else {
		$iframe_location = "side";
		if($iframe) {
			echo "<iframe id=ifr_details width=".$default_iframe_size."px height=1px src=\"\" class=float></iframe>";
		}
	}
}
?>
<div style='width:<?php echo $w; ?>px; '>
<?php
$js.= $me->echoPasswordChangeForm($tkid,$hsection,$headings);

if($me->doDisplaySecurityQuestions()) {
	?>
	<h2>Password Security Questions</h2>
	<?php
	$js.= $me->echoPasswordSecurityQuestionForm();
}
if($reason=="new") {
	$js.= $me->echoNewUserPasswordSecurity();
}
?>
</div>
<?php
if($iframe_location=="bottom" && $iframe) {
	echo "<iframe id=ifr_details width=".$default_iframe_size."px height=1px src=\"\" class=float></iframe>";
}
?>
<script type=text/javascript>
	$(function() {
		<?php echo $js;

		if($iframe) {
			?>
			$("#user_id").change(function() {
				var i = $(this).val();
				if(i=="0" || i=="X") {
					$("#ifr_details").prop("src","");
					$("#ifr_details").prop("height","1px");
					$("#ifr_details").css("border","0px solid #FFFFFF");
					$("#ifr_details").hide();
					$("#locked").val("N");
				} else {
					$("#ifr_details").prop("src","details_view.php?i="+i);
					$("#ifr_details").css("border","5px solid #efefef");
					$("#ifr_details").show();
					//$tblCon = $("#ifr_details").find("table:first");
					//alert($tblCon.css("width"));
					var r = AssistHelper.doAjax("ajax_controller.php?act=ASSIST_TK.getUserEmail","tkid="+i);
					//console.log(r);
					$("#lbl_notify").css("padding","5px").html(r[1]);
					if(r[0]=="error") {
						$("#notify_no").trigger("click");
						$("#notify_yes").prop("disabled",true);
						$("#lbl_notify").addClass("required").html(r[1]+"<br />An email can NOT be sent.");
					} else {
						$("#lbl_notify").removeClass("required");
						$("#notify_yes").prop("disabled",false);
						$("#notify_yes").trigger("click");
					}
					r = AssistHelper.doAjax("ajax_controller.php?act=ASSIST_TK.getLockedStatus","tkid="+i);
					//console.log(r);
					if(r=="pwd_locked") {
						//alert("password locked");
						$("#locked").val("Y");
						//$("#tr_locked").show();
						$("#spn_btn_locked").show();
						$("#spn_lbl_locked").hide();
					} else if(r=="admin_locked"){
						//alert("admin locked");
						$("#locked").val("N");
						//$("#tr_locked").show();
						$("#spn_btn_locked").hide();
						$("#spn_lbl_locked").show().html("User manually locked by Administrator.<br />Please use the Unlock function on Manage > Update Users > Locked page.");
					} else {
						//alert("NOT locked");
						$("#locked").val("N");
						//$("#tr_locked").hide();
						$("#spn_btn_locked").hide();
						$("#spn_lbl_locked").show().html("Not locked");
					}
					//console.log($("#locked").val());
				}

			});
			$("#user_id").trigger("change");
		<?php
		} //end iframe code
		?>
	});
</script>