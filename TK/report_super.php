<?php 
/**
 *  * HISTORY:
 * - AA-656: Modified to centralise matching of secondary user validation records with login records to accommodate changes required to Pwd_log report [JC] 23 July 2021
 */
include("inc_header.php");
?>
<style type="text/css" >
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>

<h1>Super User Activity Report</h1>
<p class="no-print"><input type=button value=Filter id=btn_filter /></p>
<?php 
//error_reporting(-1);
$me = new TK_REPORT();
$generate_report = false;
$sql = "SELECT date FROM assist_".$me->getCmpCode()."_timekeep_activity ORDER BY date ASC LIMIT 1";
$z = $me->mysql_fetch_one($sql);
$first_date = strtotime($z['date']);
$now = time();
$days = floor(($now - $first_date)/86400);

if(isset($_REQUEST['action']) && $_REQUEST['action']=="GENERATE") {
	$generate_report = true;
$super_user_types = $_REQUEST['user'];


$sql = "SELECT * FROM assist_".$me->getCmpCode()."_timekeep WHERE tkuser = 'support'";
$support = $me->mysql_fetch_one($sql);


//removed pre-sql filter - must filter once logs have been processed
	switch($super_user_types) {
		case "ADMIN":
			//$super_user_sql = "(A.tkid = '0000')";
			//break;
		case "SUPPORT":
			//$super_user_sql = "(A.tkid = '".$support['tkid']."')";
			//break;
		case "BOTH":
		default:
			$super_user_sql = "(A.tkid = '0000' OR A.tkid = '".$support['tkid']."')";
			break;
	}
	
	$from = $_REQUEST['from'];
	$to = $_REQUEST['to'];



$sql = "SELECT modref, modtext FROM assist_menu_modules";
$modules = $me->mysql_fetch_value_by_id($sql, "modref", "modtext");
$modules['TK'] = "Users";
$modules['S'] = "Master Setups";
$modules['ACTION_DASHBOARD'] = "Frontpage Dashboard";
$modules['USER_PROFILE'] = "My Profile";
$modules['MAN'] = "Help";
$modules['CHANGEPASS'] = "Change Password";

//Moved log generation to class so that it can be accessed by pwd_log report as well #AA-656 JC 2021-07-23
$logs = $me->getSuperUserActivityLogs($from,$to,$super_user_sql,"",$modules);

//ASSIST_HELPER::arrPrint($logs);

	switch($super_user_types) {
		case "ADMIN":
			foreach($logs as $key => $l) {
				if($l['tkid']!="0000") {
					unset($logs[$key]);
				}
			}
			break;
		case "SUPPORT":
			foreach($logs as $key => $l) {
				//echo "<br />".$key;
				if($l['tkid']!=$support['tkid']) {
					unset($logs[$key]);
				}
			}
			break;
		case "BOTH":
		default:
			break;
	}

//ASSIST_HELPER::arrPrint($logs);
/*

echo "
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
";

ASSIST_HELPER::arrPrint($full);
/*
foreach($activity as $key => $a) {
	if(substr($a['action'],0,2)=="IN") {
		$c++;
		if($a['action']=="IN") {
			$d = strtotime($a['date']);
			$logs[$c] = array(
				'date' => $d,
				'super_user'=>$a['tkname'],
				'user' => "",
				'activity'=>array(
					strtotime($a['date'])=>"Login"
				),
			); 
			$x = $full[$d];
			$x = next($full);
			if(!isset($super_activity[$x])) {
				$logs[$c]['user'] = "Unknown";
			} else {
				$y = $super_activity[$x];
				if($y['action']=="IN") {
					$logs[$c]['user'] = $y['tkname'];
					$logs[$c]['activity'][strtotime($y['date'])] = "Second user validation successful";
				} else {
					
				}
			}
		} else {
			$logs[$c] = array(
				'date' => strtotime($a['date']),
				'super_user'=>$a['tkname'],
				'user' => "",
				'activity'=>array(
					strtotime($a['date'])=>"Failed Login attempt"
				),
			);
		}
	} elseif(substr($a['action'],0,3)=="OUT") {
		if(strlen($a['action'])>3) {
			$x = substr($a['action'],4,strlen($a['action']));
			if($x=="TIME") {
			} else {
				$x = base64_decode($x);
			}
			if($x=="TIME") {
				$x = ": Timeout";
			}
		}
		$logs[$c]['activity'][strtotime($a['date'])] = "Logout".$x."";
	} else {
		$logs[$c]['activity'][strtotime($a['date'])] = (isset($modules[$a['action']]) ? $modules[$a['action']] : $a['action'])."";
	}
}

ASSIST_HELPER::arrPrint($logs);*/
?>

<table class=list>
	<tr>
		<th rowspan=2>Date</th>
		<th rowspan=2>Super User</th>
		<th rowspan=2>Validated User</th>
		<th colspan=2>Movements</th>
	</tr>
	<tr>
		<th>Date</th>
		<th>Activity</th>
	</tr>
<?php
foreach($logs as $l) {
	echo "
	<tr>
		<td rowspan=".count($l['activity']).">".date("d M Y H:i:s",$l['date'])."</td>
		<td rowspan=".count($l['activity']).">".$l['super_user']."</td>
		<td rowspan=".count($l['activity']).">".$l['user']."</td>
		";
		$i = 0;
		foreach($l['activity'] as $d => $a) {
			if($i>0) { echo "</tr><tr>"; }
			echo "<td>".date("d M Y H:i:s",$d)."</td><td>".$a."</td>";
			$i++;
		}
	echo "
		</td>
	</tr>";
}
?>
</table>
<p class=i>Report generated: <?php echo date("d F Y H:i:s"); ?></p>



<?php

/*
echo "<table>
<tr>
<td>"; ASSIST_HELPER::arrPrint($supers);echo "</td><td>"; ASSIST_HELPER::arrPrint($acts); echo "</td>
</tr>

</table>";
*/
	
}

?>
<div id=div_filter title=Filter>
	<form name=frm_filter>
		<input type="hidden" name=action value=GENERATE />
		<table id=tbl_filter class=form width="400px">
			<tr>
				<th>Super User:</th>
				<td>
					<input type=radio name=user value=BOTH checked />Both<br />
					<input type=radio name=user value=ADMIN />Assist Administrator<br />
					<input type=radio name=user value=SUPPORT />Assist Support<br />
				</td>
			</tr>
			<tr>
				<th>Date:</th>
				<td>From <input type=text class="datepicker"  value='<?php echo date("d-M-Y",($first_date)); ?>' size=12 name=from /> to <input type=text name=to class="datepicker" value='<?php echo date("d-M-Y"); ?>' size=12 /></td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript" >
$(function(){
	$("#div_filter").dialog({
		modal:true,
		width:"450px",
		autoOpen:<?php echo ($generate_report ? "false" : "true"); ?>,
		buttons:[{
			text:"Apply",
			click:function(){
				AssistHelper.processing();
				document.location.href = 'report_super.php?'+AssistForm.serialize($("form[name=frm_filter]"));
				$(this).dialog("close");
			}
		}]
	}).dialog("widget").find(".ui-dialog-buttonpane button").eq(0).css(AssistHelper.getGreenCSS()).end();
	$("#div_filter .datepicker").datepicker({
        showOn: 'both',
        buttonImage: '/library/jquery/css/calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'dd-M-yy',
        changeMonth:true,
        changeYear:true,
        maxDate: 0,
        minDate: "<?php echo "-".$days."D"; ?>"
	});
        
	$("#btn_filter").button().css(AssistHelper.getGreenCSS()).css("font-size","80%").click(function(){
		$("#div_filter").dialog("open");
		$("#div_filter input").blur();
		
	});
});
</script>

</body>
</html>