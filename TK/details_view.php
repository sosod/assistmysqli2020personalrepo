<?php
include("inc_header.php");

$var = $_REQUEST;
$obj_id = $var['i'];
//GET EXISTING USER DETAILS
$user = isset($obj_id) && strlen($obj_id) > 0 ? $helper->mysql_fetch_one("SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$obj_id."'") : die("<P>A fatal error has occurred and the select user could not be found.  Please go back and try again.</p>");

/*** SETUP VARIABLES ***/
$inactive_user_modules = $helper->mysql_fetch_one("SELECT count(modid) as mc FROM assist_menu_modules WHERE modref = 'PMSPS' AND modyn = 'Y'");
if($inactive_user_modules['mc']>0) { $use_inactive_users = true; } else { $use_inactive_users = false; }

$reserved_user_names = array("admin","support");
$reserved_names = array('admin'=>"Assist Administrator",'support'=>"Assist Support");
$sql = "SELECT DISTINCT tkuser FROM assist_".$cmpcode."_timekeep WHERE tkid <> '".$obj_id."' AND tkuser NOT IN ('".implode("','",$reserved_user_names)."') ORDER BY tkuser";
$used_user_names = $helper->mysql_fetch_fld_one($sql,"tkuser");

$defaults = $helper->mysql_fetch_fld2_one("SELECT field, value FROM assist_".$cmpcode."_setup WHERE ref = 'DF'","field","value");

$lists = array();
//STATUS
	$lists['status'] = array(1=>"Yes",2=>"No");
//MANAGER
	$sql = "SELECT tkid, CONCAT_WS(' ',tkname,tksurname) as value FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkuser NOT IN ('support','admin') AND tkid <> '".$obj_id."' ORDER BY tkname, tksurname"; //echo $sql;
	$lists['manager'] = array();
	$lists['manager']['S'] = "Self";
//	$lists['manager'] = array_merge($lists['manager'],mysql_fetch_fld2_one($sql,"tkid","value"));
	//$rs = getRS($sql);
	//while($row = mysql_fetch_assoc($rs)) {
	$rows = $helper->mysql_fetch_all($sql);
	foreach ($rows as $row){
		//arrPrint($row);
		$lists['manager'][$row['tkid']] = $row['value'];
	}
	unset($rs);
//GENDER
	$lists['gender'] = array('M'=>"Male",'F'=>"Female",'U'=>"Unknown");
//RACE
	$sql = "SELECT udfvcode, udfvvalue FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = 4 AND udfvyn <> 'N'";
	$lists['race'] = $helper->mysql_fetch_fld2_one($sql,"udfvcode","udfvvalue");
//OCCUPCATE
	$sql = "SELECT id, value FROM assist_list_value v WHERE v.index = 'OC' ORDER BY v.id ASC";
	$lists['occupcate'] = $helper->mysql_fetch_fld2_one($sql,"id","value");
//TITLE
	$lists['ppltitle'] = $helper->mysql_fetch_fld2_one("SELECT * FROM assist_".$cmpcode."_list_ppltitle","value","value");
//OTHER LISTS
foreach($hsection as $s => $hs) {
	foreach($headings[$s] as $fld => $h) {
		if($h['type']=="LIST" && !isset($lists[$h['table']])) {
			$sql = "SELECT * FROM assist_".$cmpcode."_".$h['table']." WHERE yn = 'Y'";
			$lists[$h['table']] = $helper->mysql_fetch_fld2_one($sql,"id","value");
		}
	}
}

//arrPrint($lists['manager']);

/*** DISPLAY FORM ***/
?>
<h1>User Validation Information</h1>
<table class=noborder id=tbl_container width=500px><tr><td class=noborder>
<table>
<?php
foreach($hsection as $hs => $ht) {
	echo "<tr><th colspan=2 style=\"background-color: #999999;\">$ht:</th></tr>";
	foreach($headings[$hs] as $fld => $h) {
		echo chr(10)."<tr>";
			echo chr(10)."<th id=th_".$fld." display_text='".$h['txt']."' >".$h['txt'].":"."</th>";
			echo chr(10)."<td>";
				switch($h['type']) {
				case "SYSTEM-TEXT":
					echo $user[$fld];
					break;
				case "SYSTEM-DATE":
					echo !empty($user[$fld]) && $user[$fld]!="0" ? date("d M Y H:i:s",$user[$fld]) : "";
					break;
				case "DATE":
					echo ($user[$fld]!="X" && ($user[$fld])*1>0 ? date("d M Y",$user[$fld]) : "");
					break;
				case "LIST":
					if($fld!="tkstatus" || $use_inactive_users) {
						echo chr(10);
							if(isset($lists[$h['table']])) {
								echo $lists[$h['table']][$user[$fld]];
							} else {
								$sql = "SELECT * FROM assist_".$cmpcode."_".$h['table']." WHERE yn = 'Y'";
								//$rs = getRS($sql);
								//while($row = mysql_fetch_assoc($rs)) {
                                $rows = $helper->mysql_fetch_all($sql);
                                foreach ($rows as $row){
									$i = $row['id'];
									$v = $row['value'];
									echo "<option ".($user[$fld]==$i ? "selected" : "")." value=\"$i\">$v</option>";
								}
								unset($rs);
							}
						echo "".chr(10);
					} else {
						echo "Yes";
					}
					break;
				case "YN":
					echo ($user[$fld]!="Y" ? "No" : "Yes");
					break;
				case "PREFIX": echo "+";
				case "TEL":
				case "EMAIL":
				case "USER":
				case "TEXT":
					$value = "";
					if(isset($user[$fld]) && strlen($user[$fld])>0) {
						if($fld=="tktel" || $fld == "tkmobile") {
							$v = $user[$fld];
							for($i=0;$i<strlen($v);$i++) {
								if(is_numeric(substr($v,$i,1))) { $value.=substr($v,$i,1); }
							}
						} elseif($fld=="tkidnum") {
							if($user[$fld]!="0") {
								$value = $user[$fld];
							}
						} else {
							$value = $user[$fld];
						}
					} else if(isset($h['default']) && strlen($h['default'])>0) {
						$value = $h['default'];
					}
					echo "".$value."";
					break;
				}
			echo "</td>";
		echo "</tr>";
	}
}
?>
</table>
<script type=text/javascript>
$(function() {
	$("th").addClass("left");
	$("th").addClass("top");
});
</script>

<h1>Security Questions</h1>
<?php
$tkObj = new ASSIST_TK();
$tkObj->setPasswordChangeReason("reset");
$js = $tkObj->echoPasswordSecurityQuestionForm($obj_id);

?>
</td></tr></table><!-- end container table -->
<script type="text/javascript">
	$(function() {
		//alert($("#tbl_container").css("width"));
		var w = parseInt(AssistString.substr($("#tbl_container").css("width"),0,-2))+50;
		var h = parseInt(AssistString.substr($("#tbl_container").css("height"),0,-2))+75;
		$('#ifr_details', window.parent.document).width(w+"px");
		$('#ifr_details', window.parent.document).height(h+"px");
	});

</script>
</body>
</html>