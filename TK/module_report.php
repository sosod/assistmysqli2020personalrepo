<?php
include("inc_header.php");
$db = new ASSIST_DB();
?>
<h1>Reports >> Module License Report</h1>
<p class=i>Report drawn on <?php echo date("d F Y H:i:s"); ?>.</p>
<?php
$incfile = "inc_".$db->getCmpCode().".php";
if(file_exists($incfile)) {   //IF CUSTOM REPORT
    include($incfile);
} else {    //IF NO CUSTOM REPORT
?>
<h2>User Count</h2>
<?php 
$tkcount = array(0,0,0);
$tkcount['total'] = 0;
$sql = "SELECT tkstatus, count(tkstatus) as tc FROM assist_".$db->getCmpCode()."_timekeep WHERE tkstatus > 0 AND tkuser <> 'support' AND tkid <> '0000' GROUP BY tkstatus"; 
$tkcount = $db->mysql_fetch_value_by_id($sql,"tkstatus","tc");
$tkcount['total'] = array_sum($tkcount);
?>
<table style="margin-left: 10px">
	<tr>
		<th class=left width=120>Active Users:&nbsp;</td>
		<td class=right width=75>&nbsp;<?php echo(isset($tkcount[1]) ? $tkcount[1] : 0); ?></td>
	</tr>
	<tr>
		<th class=left>Non-User Employee:&nbsp;</td>
		<td class=right>&nbsp;<?php echo(isset($tkcount[2]) ? $tkcount[2] : 0); ?></td>
	</tr>
	<tr>
		<th class=left>Total Users:&nbsp;</td>
		<td class="b right color">&nbsp;<?php echo($tkcount['total']); ?></td>
	</tr>
</table>
<h2>Module Licenses</h2>
<table width=650 style="margin-left:10px;">
    <tr>
        <th>Module</th>
        <th>Licenses</th>
        <th>Total<br>Users</th>
        <th>Active<br>Users</th>
        <th>Inactive<br>Users</th>
        <th>Remaining<br>Licenses</th>
    </tr>
<?php
$sql = "SELECT count(DISTINCT usrtkid) as ucount, UPPER(usrmodref) as usrmodref 
		FROM assist_".$db->getCmpCode()."_menu_modules_users 
		INNER JOIN assist_".$db->getCmpCode()."_timekeep 
		  ON usrtkid = tkid
		WHERE tkstatus <> 0 
		AND tkuser <> 'support' 
		AND tkid <> '0000' 
		GROUP BY usrmodref"; //echo $sql;
$ucount = $db->mysql_fetch_value_by_id($sql,"usrmodref","ucount");

$sql = "SELECT count(DISTINCT usrtkid) as ucount,  UPPER(usrmodref) as usrmodref 
		FROM assist_".$db->getCmpCode()."_menu_modules_users 
		INNER JOIN assist_".$db->getCmpCode()."_timekeep 
		ON usrtkid = tkid 
		WHERE tkstatus = 2 
		AND tkuser <> 'support' 
		AND tkid <> '0000' 
		GROUP BY usrmodref";
$icount = $db->mysql_fetch_value_by_id($sql,"usrmodref","ucount");

$sql = "SELECT * FROM assist_menu_modules m
				, assist_".$db->getCmpCode()."_menu_modules mm 
		WHERE m.modyn = 'Y' 
		AND mm.modmenuid = m.modid 
		ORDER BY m.modtext";
$modules = $db->mysql_fetch_all($sql);
	foreach($modules as $row) {
        $modtext = $row['modtext'];
        $modref = $row['modref'];
        $modlic = $row['moduserallowed'];
        $totmodusr = isset($ucount[$modref]) ? $ucount[$modref] : 0;
        $inactmodusr = isset($icount[$modref]) ? $icount[$modref] : 0;
        $actmodusr = $totmodusr - $inactmodusr;
		if($modlic != 0) {
			if($modlic < 0 || $row['modref'] == "TD") {
				$modlic = "Unlimited";
				$modrem = "Unlimited";
			} else {
				$modrem = $modlic - $modusr; // + $modnon;
			}
			?>
			<tr>
				<td><?php echo($modtext); ?></td>
				<td class=center><?php echo($modlic); ?></td>
				<td class=center><?php echo($totmodusr); ?></td>
				<td class=center><?php echo($actmodusr); ?></td>
				<td class=center><?php echo($inactmodusr); ?></td>
				<td class=center><?php
					if($modrem < 3 && $modlic != "Unlimited")
					{
						echo("<b><span class=color>".$modrem."</span></b>");
					}
					else
					{
						echo($modrem);
					}
				?></td>
			</tr>
			<?php
		}
    }
?>
</table>
<?php
}   //IF NO CUSTOM REPORT FILE FOUND
?>

</body>

</html>
