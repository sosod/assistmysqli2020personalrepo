<?php
/*** SETUP VARIABLES ***/
/**
 * from inc_ignite.php
 * @var ASSIST_MODULE_HELPER $helper
 */
//$inactive_user_modules = mysql_fetch_one("SELECT count(modid) as mc FROM assist_menu_modules WHERE modref = 'PMSPS' AND modyn = 'Y'");
//if($inactive_user_modules['mc']>0) { $use_inactive_users = true; } else {
	 //$use_inactive_users = false;
//}

$today = $helper->getToday();
$reserved_user_names = array("admin","support");
$reserved_names = array('admin'=>"Assist Administrator",'support'=>"Assist Support");
$sql = "SELECT DISTINCT tkuser FROM assist_".$cmpcode."_timekeep WHERE tkid <> '".$obj_id."' AND tkuser NOT IN ('".implode("','",$reserved_user_names)."') ORDER BY tkuser";
$used_user_names = $helper->mysql_fetch_fld_one($sql,"tkuser");

$defaults = $helper->mysql_fetch_fld2_one("SELECT field, value FROM assist_".$cmpcode."_setup WHERE ref = 'DF'","field","value");

$lists = array();
//STATUS
	$lists['status'] = array(1=>"Yes",2=>"No");
//MANAGER
	$sql = "SELECT tkid, CONCAT_WS(' ',tkname,tksurname) as value FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkuser NOT IN ('support','admin') AND tkid <> '".$obj_id."' ORDER BY tkname, tksurname"; //echo $sql;
	$lists['manager'] = array();
	$lists['manager']['S'] = "Self";
//	$lists['manager'] = array_merge($lists['manager'],mysql_fetch_fld2_one($sql,"tkid","value"));
//	$rs = getRS($sql);
//	while($row = mysql_fetch_assoc($rs)) {
    $rows = $helper->mysql_fetch_all($sql);
    foreach ($rows as $row){
		//arrPrint($row);
		$lists['manager'][$row['tkid']] = $row['value'];
	}
	unset($rs);
//GENDER
	$lists['gender'] = array('M'=>"Male",'F'=>"Female",'U'=>"Unknown");
//RACE
	$sql = "SELECT udfvcode, udfvvalue FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = 4 AND udfvyn <> 'N'";
	$lists['race'] = $helper->mysql_fetch_fld2_one($sql,"udfvcode","udfvvalue");
//OCCUPCATE
	$sql = "SELECT id, value FROM assist_list_value v WHERE v.index = 'OC' ORDER BY v.id ASC";
	$lists['occupcate'] = $helper->mysql_fetch_fld2_one($sql,"id","value");
//TITLE
	$lists['ppltitle'] = $helper->mysql_fetch_fld2_one("SELECT * FROM assist_".$cmpcode."_list_ppltitle","value","value");
//OTHER LISTS
foreach($hsection as $s => $hs) {
	foreach($headings[$s] as $fld => $h) {
		if($h['type']=="LIST" && !isset($lists[$h['table']])) {
			$sql = "SELECT * FROM assist_".$cmpcode."_".$h['table']." WHERE yn = 'Y'";
			$lists[$h['table']] = $helper->mysql_fetch_fld2_one($sql,"id","value");
		}
	}
}
//error_reporting(E_ALL ^ E_STRICT);// Hide strict standards error message
$tkObject = new ASSIST_TK();

unset($hsection['emp']);
unset($headings['emp']);
unset($hsection['stat']);
unset($headings['stat']);

//arrPrint($lists['manager']);

/** SAVE CHANGES **/
//arrPrint($_REQUEST);
if(isset($_REQUEST['act'])) {
	switch($_REQUEST['act']) {
	case "LOCK":
		$sql = "UPDATE assist_".$cmpcode."_timekeep SET tklock = 'Y' WHERE tkid = '".$obj_id."'";
        $helper->db_update($sql);
		$c = array(
			'type'=>"L",
			'fld'=>"tklock",
			'new'=>"Y",
			'old'=>"N",
			'txt'=>"Locked user."
		);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'0000','Assist Administrator','DETAILS','".$obj_id."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($sql)."')";
        $helper->db_insert($sql2);
				$result = array("ok","User '".$user['tkname']." ".$user['tksurname']."' has been locked.");
				echo "<script type=text/javascript>
						document.location.href = 'manage.php?page=users&section=".$section."&r[]=".$result[0]."&r[]=".urlencode($result[1])."';
						</script>";
		break;
	case "INACTIVE":
		$sql = "UPDATE assist_".$cmpcode."_timekeep SET tkstatus = 2, tktermdate = '".$today."' WHERE tkid = '".$obj_id."'";
        $helper->db_update($sql);
		$c = array(
			'type'=>"I",
			'fld'=>"tkstatus",
			'new'=>"2",
			'old'=>$user['tkstatus'],
			'txt'=>"Made user Inactive."
		);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'0000','Assist Administrator','DETAILS','".$obj_id."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($sql)."')";
        $helper->db_insert($sql2);
		$empObject = new EMP1_EMPLOYEE("","emp");
		$empObject->makeInactiveUser($obj_id);
				$result = array("ok","User '".$user['tkname']." ".$user['tksurname']."' has been made inactive.");
				echo "<script type=text/javascript>
						document.location.href = 'manage.php?page=users&section=".$section."&r[]=".$result[0]."&r[]=".urlencode($result[1])."';
						</script>";
		break;
	case "TERM":
		//REMOVE MODULE ACCESS
		$sql = "SELECT DISTINCT usrmodref, modtext FROM assist_menu_modules INNER JOIN assist_".$cmpcode."_menu_modules_users ON usrmodref = modref AND usrtkid = '".$obj_id."' WHERE modyn = 'Y'";
		$user_mods = $helper->mysql_fetch_fld2_one($sql,"usrmodref","modtext");
		$sql = "DELETE FROM assist_".$cmpcode."_menu_modules_users WHERE usrtkid = '".$obj_id."'";
        $helper->db_update($sql);
		$c = array(
			'type'=>"T",
			'fld'=>"usrmodref",
			'new'=>"",
			'old'=>implode(",",array_keys($user_mods)),
			'txt'=>"Removed access to the following modules due to termination of user:".chr(10)." - ".implode(chr(10)." - ",$user_mods)
		);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'0000','Assist Administrator','DETAILS','".$obj_id."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($sql)."')";
        $helper->db_insert($sql2);

		$sql = "UPDATE assist_".$cmpcode."_timekeep SET tkstatus = 0, tktermdate = '".$today."' WHERE tkid = '".$obj_id."'";
        $helper->db_update($sql);
		$c = array(
			'type'=>"T",
			'fld'=>"tkstatus",
			'new'=>"0",
			'old'=>$user['tkstatus'],
			'txt'=>"Terminated user."
		);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'0000','Assist Administrator','DETAILS','".$obj_id."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($sql)."')";
        $helper->db_insert($sql2);
				$result = array("ok","User '".$user['tkname']." ".$user['tksurname']."' has been terminated.");
				echo "<script type=text/javascript>
						document.location.href = 'manage.php?page=users&section=".$section."&r[]=".$result[0]."&r[]=".urlencode($result[1])."';
						</script>";
		break;
	case "SAVE":
		$changes = array();
		$arr_sql = array();
		foreach($hsection as $s => $hs) {
			if($s!="sys") {
				foreach($headings[$s] as $fld => $h) {
					$old = isset($user[$fld]) ? $user[$fld] : "";
					switch($h['type']) {
					case "DATE":
						$old = $old*1;
						$new = strtotime($_REQUEST[$fld]);
						$check = date("d M Y",$new) != date("d M Y",$old);
						$trans = "Updated ".$h['txt']." to \'".$_REQUEST[$fld]."\' from \'".date("d M Y",$old)."\'";
						break;
					default:
						$new = $helper->code($_REQUEST[$fld]);
						$check = $new!=$old;
						if($h['type']=="LIST") {
							$trans = "Updated ".$h['txt']." to \'".(isset($lists[$h['table']][$new]) ? $lists[$h['table']][$new] : $new)."\' from \'".(isset($lists[$h['table']][$old]) ? $lists[$h['table']][$old] : $old)."\'";
						} else {
							$trans = "Updated ".$h['txt']." to \'".$new."\' from \'".$old."\'";
						}
						break;
					}
					if($fld=="tkidnum" && strlen($new)=="") { $new = 0; }
					elseif($fld=="tkbdate" && strlen($new)=="") { $new = "X"; }
					if($check) {
						$arr_sql[] = $fld." = '$new'";
						if($fld=="tktel" || $fld == "tkmobile") {
							$v = $user[$fld];
							$value = "";
							for($i=0;$i<strlen($v);$i++) {
								if(is_numeric(substr($v,$i,1))) { $value.=substr($v,$i,1); }
							}
							if($value!=$new) {
								$changes[$fld] = array(
									'fld'=>$fld,
									'old'=>$old,
									'new'=>$new,
									'txt'=>$trans
								);
							}
						} else {
							$changes[$fld] = array(
								'fld'=>$fld,
								'old'=>$old,
								'new'=>$new,
								'txt'=>$trans
							);
						}
						if($fld=="tkstatus") {
							$tkpwd = $tkObject->generateRandomPassword();
							$arr_sql[] = "tkloginwindow = '".date("Y-m-d H:i:s",time()+(60*60*24*7))."'";
							//below not required - logged by ASSIST_TK.changePassword
							/*$changes['tkpwd'] = array(
								'fld'=>"tkpwd",
								'old'=>"",
								'new'=>"",
								'txt'=>"Password reset due to Non-User Employee conversion to Active Assist User",
							);*/
						}
					}
				}
			}
		}
//arrPrint($changes);
//arrPrint($arr_sql);
		if(count($arr_sql)>0) {

			$sql = "UPDATE assist_".$cmpcode."_timekeep SET ".implode(" ,",$arr_sql)." WHERE tkid = '".$obj_id."'";
			//echo "<P>".$sql;
			$client_db = new ASSIST_MODULE_HELPER('client', $cc);
			$mar = $client_db->db_update($sql);
			//LOG CHANGES
			if($mar>0) {
				$employee_made_active = false;
				foreach($changes as $c) {
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'0000','Assist Administrator','DETAILS','".$obj_id."','E','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($sql)."')";
					$client_db->db_insert($sql2);
					//echo "<p>".$sql2;
					$user[$c['fld']] = $c['new'];
					if($c['fld']=="tkstatus" && $c['new']*1==1) {
						$empObject = new EMP1_EMPLOYEE("","emp");
						$empObject->makeActiveUser($obj_id);
						$pwd_var = array(
							'reason'=>"convert",
							'notify'=>"1",
							'force'=>"1",
							'user_id'=>$obj_id,
							'unlock'=>"1",
							'new'=>$tkpwd['pwd'],
						);
						$tkObject->changePassword($pwd_var);
					}
				}
				$result = array("ok","Changes saved to user '".$user['tkname']." ".$user['tksurname']."'.");
				echo "<script type=text/javascript>
						document.location.href = 'manage.php?page=users&section=".$section."&r[]=".$result[0]."&r[]=".urlencode($result[1])."';
						</script>";
			} else {
				$result = array("info","No changes were found to be saved.");
			}
		} else {
			$result = array("info","No changes were found to be saved.");
		}
		break;
	}
}

$me->displayResult(isset($result) && is_array($result) ? $result : array());

/*** DISPLAY FORM ***/
?>
<!-- container table --><table class=noborder style="margin-top: -20px;"><tr><td class=noborder>
<p><span class=iinform>Note:</span><ul>
<li>All fields marked * are required except in the case of the <?php echo $hsection['contact']; ?>.</li>
<!-- <li><?php echo $hsection['contact']; ?> are only required for Active Assist users.</li> -->
<li>Either a Landline or Mobile/Cellphone contact number is required for Active Assist users.</li>
<?php
if($section=="employees") {
	echo "<li>When converting a Non-User Employee to an Active Assist User, a password will be automatically generated and the user will receive a 'Welcome to Assist' email.</li>";
}
?>
</ul></p>
<form id=frm_details method=post action=manage.php>
<input type=hidden name=page value=users_edit />
<input type=hidden name=section value=<?php echo $section; ?> />
<input type=hidden name=a value="edit details" />
<input type=hidden name=i value="<?php echo $obj_id; ?>" />
<input type=hidden name=act id=act value=SAVE />
<table>
<?php


foreach($hsection as $hs => $ht) {
	echo "<tr><th colspan=2 style=\"background-color: #999999;\">$ht:</th></tr>";
	foreach($headings[$hs] as $fld => $h) {
		echo chr(10)."<tr>";
			echo chr(10)."<th id=th_".$fld." display_text='".$h['txt']."' >".$h['txt'].":".($h['required'] ? "*" : "")."</th>";
			echo chr(10)."<td><input type=hidden id=".$fld."-r value=\"".$h['required']."\" />";
				switch($h['type']) {
				case "SYSTEM-TEXT":
					echo $user[$fld]."<input type=hidden name=$fld value=\"".$user[$fld]."\" />";
					break;
				case "SYSTEM-DATE":
					echo $user[$fld]!="0" && !empty($user[$fld]) ? date("d M Y H:i:s",$user[$fld]) : "";
					break;
				case "DATE":
				    $bdate = isset($user[$fld]) && $user[$fld]!="X" && $me->checkIntRef($user[$fld]) ? date("d M Y",$user[$fld]) : "";
					echo "<input type=text class=jdate size=15 id=$fld name=$fld readonly=readonly value=\"".$bdate."\" />";
					break;
				case "LIST":
					if($fld!="tkstatus") {
						echo chr(10)."<select id=$fld name=$fld>".($fld!="tkstatus" ? "<option value=X>--- SELECT ---</option>" : "");
							if(isset($lists[$h['table']])) {
								foreach($lists[$h['table']] as $i => $v) {
									echo chr(10)."<option ".($user[$fld]==$i ? "selected" : "")." value=\"$i\">$v</option>";
								}
							} else {
								$sql = "SELECT * FROM assist_".$cmpcode."_".$h['table']." WHERE yn = 'Y'";
//								$rs = getRS($sql);
//								while($row = mysql_fetch_assoc($rs)) {
                                $rows = $helper->mysql_fetch_all($sql);
                                foreach ($rows as $row){
									$i = $row['id'];
									$v = $row['value'];
									echo "<option ".($user[$fld]==$i ? "selected" : "")." value=\"$i\">$v</option>";
								}
								unset($rs);
							}
						echo "</select>".chr(10);
					} else {
						echo "Yes<input type=hidden name=$fld id=$fld value=1 />";
					}
					break;
				case "YN":
						echo "
							<select id=$fld name=$fld>
								<option ".(!isset($user[$fld]) || $user[$fld]!="Y" ? "selected" : "")." value=N>No</option>
								<option ".($user[$fld]=="Y" ? "selected" : "")." value=Y>Yes</option>
							</select>";
					break;
				case "PREFIX": echo "+";
				case "TEL":
				case "EMAIL":
				case "USER":
				case "TEXT":
					$value = "";
					if(isset($user[$fld]) && strlen($user[$fld])>0) {
						if($fld=="tktel" || $fld == "tkmobile") {
							$v = $user[$fld];
							for($i=0;$i<strlen($v);$i++) {
								if(is_numeric(substr($v,$i,1))) { $value.=substr($v,$i,1); }
							}
						} elseif($fld=="tkidnum") {
							if($user[$fld]!="0") {
								$value = $user[$fld];
							}
						} else {
							$value = $user[$fld];
						}
					} else if(isset($h['default']) && strlen($h['default'])>0) {
						$value = $h['default'];
					}
					echo "<input type=text id=$fld name=$fld value=\"".$value."\" class=".$h['type']
					.(isset($h['width']) && $helper->checkIntRef($h['width']) ? " size=".$h['width'] : "")
					.(isset($h['max']) && $helper->checkIntRef($h['max']) ? " maxlength=".$h['max'] : "")
					." /> "
					.($h['type']=="USER" ? "<label id=user_error for=tkuser class=idelete></label>" : "")
					.(isset($h['example']) && strlen($h['example'])>0 ? "<br /><span class=i style=\"font-size: 7pt;\">".$h['example']."</span>" : "");
					break;
				}
			echo "</td>";
		echo "</tr>";
	}
}
?>
<tr>
	<td colspan=2><input type=button class=isubmit value="Save Changes" /> <input type=reset> <span class=float><?php if($user['tkstatus']==1) { ?><input type=button value="Make Inactive" class=i-make-inactive /><input type=button value=Lock class=ilock /> <input type=button value=Terminate class=idelete /><?php } ?> </span></td>
</tr>
</table>
</form>
<script type=text/javascript>
$(function() {
	var exist_user_names = new Array;
	<?php for($u=0;$u<count($used_user_names);$u++) { echo "exist_user_names[$u] = '".$used_user_names[$u]."';"; } ?>
	var res_user_names = new Array;
	<?php for($u=0;$u<count($reserved_user_names);$u++) { echo "res_user_names[$u] = '".$reserved_user_names[$u]."';"; } ?>
	var res_names = new Array;
	<?php foreach($reserved_names as $u => $n) { echo "res_names['$u'] = '".$reserved_names[$u]."';"; } ?>
	$(".jdate").datepicker("option", "yearRange", '<?php echo (date("Y")-100).":".date("Y");?>');
	$(".jdate").datepicker("option", "maxDate", '+1m');
	$("th").addClass("left");
	$("th").addClass("top");
	var load_err = validateForm();
	$("input:button.i-make-inactive").click(function() {
		if(confirm("Are you sure you wish to make this user INACTIVE?\n\nThe user will no longer be able to login to or transact on Assist but will still be available as an Employee for Individual Performance purposes (if applicable).")==true) {
			$("#frm_details #act").val("INACTIVE");
			$("#frm_details").submit();
		}
	});
	$("input:button.ilock").click(function() {
		if(confirm("Are you sure you wish to LOCK this user?")==true) {
			$("#frm_details #act").val("LOCK");
			$("#frm_details").submit();
		}
	});
	$("input:button.idelete").click(function() {
		if(confirm("Are you sure you wish to TERMINATE this user?\n\nThe user will be no longer be able to login to or transact on Assist AND will no longer be available as an Employee for Individual Performance purposes (if applicable).\n\nWARNING: This action cannot be undone!")==true) {
			$("#frm_details #act").val("TERM");
			$("#frm_details").submit();
		}
	});
	$("select").change(function() { var edit_err = validateForm(); });
	$("input:text").keyup(function() { var edit_err = validateForm(); });
	$("input:button.isubmit").click(function() {
		var valid = validateForm();
		var err = valid[0]; //alert(valid[0]);
		var other = valid[1];
		if(err.length>0 || other.length>0) {
			var res = "Please review the following errors:";
			if(err.length>0) {
				res = res+"\n\nRequired fields:"+err;
			}
			if(other.length>0) {
				res = res + "\n\nOther errors:"+other;
			}
			alert(res);
		} else {
			$("#frm_details").submit();
		}
	});
	function validateForm() {
		var stat = $("#tkstatus").val();
		stat *=1;
		var id = ""; 		var r = "";		var v = "";		var t = "";
		var err = ""; var other = "";
		var len = false; var sel = false;
		var contact = new Array("tkphprefix","tktel","tkmobile","tkemail","tkfax");
		var contact2 = new Array("tktel","tkmobile");
		$("input:text, select").each(function() {
			id = $(this).attr("id");
			$(this).removeClass("required");
			if(id=="tkuser") {
				var o2 = validateUser();
				if(o2.length>0) {
					other = other + "\n - "+o2;
				}
			}
			r = $("#"+id+"-r").val();
			if( (stat==1 && r==1 && jQuery.inArray(id,contact2)<0 ) || (stat==2 && r==1 && jQuery.inArray(id,contact)<0 && id!="tkuser")) {
				v = $(this).val();
				len = (v.length == 0);
				sel = ($(this).get(0).tagName =="SELECT" && v.toLowerCase() == "x");
				if(stat==1 && id=="tkemail" && v.length > 0) {
					if( (!(v.indexOf("@") > 0) || !(v.indexOf(".") > 0))
							||
						(v.indexOf("@") != v.lastIndexOf("@"))
							||
						(v.indexOf("@") > v.lastIndexOf("."))
							||
						(v.indexOf(" ") >= 0)
					) {
						other = other + "\n - Invalid email address format";
						$("#tkemail").addClass("required");
					} else {
						var email_test_result = AssistHelper.doAjax("ajax_controller.php?act=ASSIST_TK.validateUserEmail","email="+v);
						if(email_test_result[0]=="error") {
							other = other + "\n - Invalid email address format";
							$("#tkemail").addClass("required");
						}
					}
				}
				if(len || sel) {
					t = $("#th_"+id).attr("display_text");
					//alert(t);
					$(this).addClass("required");
					err = err + "\n - "+t;//.substring(0,t.length-2);
				}
			}
		}); //alert(err);
		var charN = new Array("0","1","2","3","4","5","6","7","8","9");
		var tktel = $("#tktel").val();
		var tkmobile = $("#tkmobile").val();
		if(tktel.length==0 && tkmobile.length==0 && stat==1) {
			err = err+"\n - At least one contact number is required.";
			$("#tktel").addClass("required");
			$("#tkmobile").addClass("required");
		} else {
			$("#tktel").removeClass("required");
			if(tktel.length>0) {
				for(i=0;i<tktel.length;i++) {
					if(jQuery.inArray(tktel.substr(i,1),charN)<0) {
						other = other + "\n - Only numbers are allowed for the <?php echo $headings['contact']['tktel']['txt']; ?> number.";
						$("#tktel").addClass("required");
						break;
					}
				}
			}
			$("#tkmobile").removeClass("required");
			if(tkmobile.length>0) {
				for(i=0;i<tkmobile.length;i++) {
					if(jQuery.inArray(tkmobile.substr(i,1),charN)<0) {
						$("#tkmobile").addClass("required");
						other = other + "\n - Only numbers are allowed for the <?php echo $headings['contact']['tkmobile']['txt']; ?> number.";
						break;
					}
				}
			}
		}
		var tkfax = $("#tkfax").val();
		$("#tkfax").removeClass("required");
		if(tkfax.length>0) {
			for(i=0;i<tkfax.length;i++) {
				if(jQuery.inArray(tkfax.substr(i,1),charN)<0) {
					$("#tkfax").addClass("required");
					other = other + "\n - Only numbers are allowed for the <?php echo $headings['contact']['tkfax']['txt']; ?> number.";
					break;
				}
			}
		}
		return [err,other];
	}
	$("#tkuser").keyup(function() {
		validateUser();
	});
	function validateUser() {
		var allowed_char = new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
		var u = $("#tkuser").val();
		var n = $("#tkname").val();
		var s = $("#tksurname").val();
		var tk = n + " "+ s;
		if(u.length > 0 && jQuery.inArray(u,exist_user_names)>=0) {
			$("#tkuser").addClass("required");
			$("#user_error").attr("innerText","This user name is already in use.");
			var err = "This user name is already in use.";
		} else if(u.length > 0 && jQuery.inArray(u,res_user_names)>=0) {
			$("#tkuser").addClass("required");
			$("#user_error").attr("innerText","This user name is a reserved user name and may not be used.");
			var err = "This user name is a reserved user name and may not be used.";
		} else if(u.length>0) {
			$("#tkuser").removeClass("required");
			var err = "";
			for(i=0;i<u.length;i++) {
				if(jQuery.inArray(u.substr(i,1),allowed_char)<0) {
					$("#tkuser").addClass("required");
					err = "Only alphanumeric characters are allowed for the user name.";
					break;
				}
			}
		} else {
			$("#tkuser").addClass("required");
			$("#user_error").attr("innerText","User name is required.");
			var err = "A user name is required.";
		}
/*		} else {
			$("#tkuser").removeClass("required");
			$("#user_error").attr("innerText","");
			var err = "";
		}*/
		return err;
	}
});
</script>
<?php
$log_sql = "SELECT l.date, l.tkname, l.transaction
			FROM assist_".$cmpcode."_timekeep_log l
			WHERE l.active = true AND l.ref = ".$obj_id."
			ORDER BY l.id DESC";
$log = $helper->mysql_fetch_all($log_sql);
$helper->displayAuditLog($log,array('date'=>"date",'user'=>"tkname",'action'=>"transaction"),$helper->getGoBack("manage.php?page=users&section=".$section));
?>
<?php
if($section=="employees") {
?>
<div style="border: 0px solid #ababab; width: 700px; margin-top: 10px;">
<p class=iinform>Note:</p>
<ul>
	<li>LOCK: This will prevent the user from logging onto Assist but will not affect their menu access to individual modules.</li>
	<li>TERMINATE: The user's menu access will be removed and the user will no longer be able to log onto Assist.  The user can be restored but their menu access will need to be recreated before they can access individual modules. However a user CANNOT be terminated if there are outstanding actions on Assist associated with them.</li>
</ul>
</div>
<?php } ?>
</td></tr></table><!-- end container table -->