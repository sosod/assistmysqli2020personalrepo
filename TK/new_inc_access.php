<?php
$inactive_users_modules = array("PMS","PMSPS");

$result = array();


?>
<table id=tbl_access>
	<tr>
		<th>Module</th>
		<th>Access</th>
		<th>Licenses<br />Available</th>
	</tr>
<?php
foreach($menu as $m) {
	$modref = $m['modref'];
	$allowed = isset($mm[$m['modid']]['moduserallowed']) ? $mm[$m['modid']]['moduserallowed'] : 0;
//	$rs = getRS("SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep WHERE tkid IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref') AND ".(in_array($modref,$inactive_users_modules) ? "tkstatus > 0" : "tkstatus = 1"));
	/*$rs = getRS("SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep
				WHERE tkid IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref')
				AND tkuser <> 'support' AND tkuser <> 'admin' 
				AND ".(in_array($modref,$inactive_users_modules) ? "tkstatus > 0" : "tkstatus = 1"));*/
    $sql = "SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep 
				WHERE tkid IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref')
				AND tkuser <> 'support' AND tkuser <> 'admin' 
				AND ".(in_array($modref,$inactive_users_modules) ? "tkstatus > 0" : "tkstatus = 1");
	$access = $helper->db_get_num_rows($sql);
	$remain = $allowed - $access;
//	$rs = getRS("SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep WHERE tkid NOT IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref') AND ".(in_array($modref,$inactive_users_modules) ? "tkstatus > 0" : "tkstatus = 1"));
    $sql = "SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep WHERE tkid NOT IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref') AND ".(in_array($modref,$inactive_users_modules) ? "tkstatus > 0" : "tkstatus = 1");
	$without = $helper->db_get_num_rows($sql);
	
	if($allowed<0) { $allowed = "Unlimited"; $remain = "Unlimited"; }
	echo "	<tr>
				<td id=td_".$modref.">".$helper->decode($m['modtext'])."</td>
				<td class=center>";
				if(isset($mmu[$m['modref']]) || !is_numeric($remain) || $remain*1 > 0) {
					echo "<input type=checkbox value=".$m['modref']." name=mr[] ".($m['modref']=="TD" ? "checked" : "")." />";
				} else {
					echo "N/A";
				}
	echo "		</td>
				<td class=center>".$remain." license".($remain==1 ? "" : "s")."</td>
			</tr>";
}
?>
</table>
<?php
//	include("inc_debitorder.php");
//	echo("<p>Click <a href=".$debitfile." target=_blank><b>here</b></a> to apply for more licences (updated: ".$debitdisplay.").</p>");
?>