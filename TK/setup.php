<?php
//error_reporting(-1);
require_once "../module/autoloader.php";

$me = new ASSIST_TK();

ASSIST_HELPER::echoPageHeader();

?>
<h1>Setup</h1>
<table id=user width=650 class=form>
	<tr>
		<th width=150>Super Users: </th>
		<td>Configure which users can login as the Admin user. <button class=btn_configure id=setup_super>Configure</button></td>
	</tr>
	<tr>
		<th>Security Settings: </th>
		<td>Configure the minimum password requirements and<br />login attempt settings. <button class=btn_configure id=setup_password>Configure</button></td>
	</tr>
</table>
<!--
<h2>Lists</h2>
<table id=user width=650 class=form>
	<tr>
		<th width=150>Locations: </th>
		<td>Create the list of company locations. <span class=float><button class=btn_configure id=setup_loc>Configure</button></span></td>
	</tr>
	<tr>
		<th>Departments: </th>
		<td>Create the list of company departments. <button class=btn_configure id=setup_dept>Configure</button></td>
	</tr>
	<tr>
		<th>Job Titles: </th>
		<td>Create the list of company job titles. <button class=btn_configure id=setup_jobtitle>Configure</button></td>
	</tr>
	<tr>
		<th>Defaults: </th>
		<td>Choose the default values. <button class=btn_configure id=setup_default>Configure</button></td>
	</tr>
</table>
-->


<script type=text/javascript>
$(function() {
	$("button.btn_configure").button({
		icons: {
			primary: "ui-icon-gear"
		}
	}).css({"float":"right"})
	.click(function(e) {
		e.preventDefault();
		//alert();
		document.location.href = $(this).prop("id")+".php"; 
	})
	.children(".ui-button-text").css({"padding-top":"3px","padding-bottom":"3px","padding-left":"25px","font-size":"80%"});
	$("table th, table td").css("vertical-align","middle");
});
</script>
