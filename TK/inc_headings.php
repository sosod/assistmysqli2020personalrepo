<?php

if(!isset($defaults)) {
	$defaults = array(
		'tkloc'=>"",
		'phprefix'=>"",
		'tktela'=>"",
		'tkfaxa'=>"",
		'tkemail'=>"",
	);
}

	//'emp'=>"Employment Information",
	//'stat'=>"Statutory Requirements",
$hsection = array(
	'sys'=>"System Information",
	'user'=>"User Information",
	'ignite'=>"Assist Information",
	'contact'=>"Contact Details",
);
$headings = array(
	'sys'=> array(
		'tkid'=>array('fld'=>"tkid",'txt'=>"System Reference",'type'=>"SYSTEM-TEXT",'required'=>false),
		'tkadddate'=>array('fld'=>"tkadddate",'txt'=>"Date Created",'type'=>"SYSTEM-DATE",'required'=>false),
		'tkmoddate'=>array('fld'=>"tkmoddate",'txt'=>"Date Edited",'type'=>"SYSTEM-DATE",'required'=>false),
		'tklogindate'=>array('fld'=>"tklogindate",'txt'=>"Last Login Date",'type'=>"SYSTEM-DATE",'required'=>false),
	),
	'user'=> array(
		'tktitle'=>array('fld'=>"tktitle",'txt'=>"Title",'type'=>"LIST",'required'=>true,'table'=>"ppltitle"),
		'tkname'=>array('fld'=>"tkname",'txt'=>"First Name",'type'=>"TEXT",'required'=>true,'width'=>30,'max'=>50),
		'tksurname'=>array('fld'=>"tksurname",'txt'=>"Surname",'type'=>"TEXT",'required'=>true,'width'=>30,'max'=>50),
		'tkidnum'=>array('fld'=>"tkidnum",'txt'=>"ID Number",'type'=>"TEXT",'required'=>false,'width'=>30,'max'=>50),
		'tkbdate'=>array('fld'=>"tkbdate",'txt'=>"Birthday",'type'=>"DATE",'required'=>false),
	),
	'ignite'=> array(
		'tkstatus'=>array('fld'=>"tkstatus",'txt'=>"Active Login",'type'=>"LIST",'required'=>true,'table'=>"status"),
		'tkuser'=>array('fld'=>"tkuser",'txt'=>"User Name (login)",'type'=>"USER",'required'=>true,'width'=>30,'max'=>30),
	),
	'emp' => array(
		'tkempnum'=>array('fld'=>"tkempnum",'txt'=>"Employee Number",'type'=>"TEXT",'required'=>false,'width'=>20,'max'=>30),
		'tkempdate'=>array('fld'=>"tkempdate",'txt'=>"Employment Date",'type'=>"DATE",'required'=>true),
		'tkdesig'=>array('fld'=>"tkdesig",'txt'=>"Job Title",'type'=>"LIST",'required'=>true,'table'=>"list_jobtitle"),
		'tkdept'=>array('fld'=>"tkdept",'txt'=>"Department",'type'=>"LIST",'required'=>true,'table'=>"list_dept"),
		'tkloc'=>array('fld'=>"tkloc",'txt'=>"Location",'type'=>"LIST",'required'=>true,'default'=>$defaults['tkloc'],'table'=>"list_loc"),
		'tkmanager'=>array('fld'=>"tkmanager",'txt'=>"Manager",'type'=>"LIST",'required'=>true,'table'=>"manager"),
	),
	'contact' => array(
		'tkphprefix'=>array('fld'=>"tkphprefix",'txt'=>"Country Prefix",'type'=>"PREFIX",'required'=>true,'default'=>$defaults['phprefix'],'width'=>10,'max'=>10),
		'tktel'=>array('fld'=>"tktel",'txt'=>"Landline/Office",'type'=>"TEL",'required'=>true,'default'=>$defaults['tktela'],'example'=>"Numbers only, no spaces or special characters e.g. 0219876543",'width'=>20,'max'=>15),
		'tkmobile'=>array('fld'=>"tkmobile",'txt'=>"Mobile/Cellphone",'type'=>"TEL",'required'=>true,'example'=>"Numbers only, no spaces or special characters e.g. 0821234567",'width'=>20,'max'=>15),
		'tkfax'=>array('fld'=>"tkfax",'txt'=>"Fax",'type'=>"TEXT",'required'=>false,'default'=>$defaults['tkfaxa'],'example'=>"Numbers only, no spaces or special characters e.g. 0861234567",'width'=>30,'max'=>30),
		'tkemail'=>array('fld'=>"tkemail",'txt'=>"Email",'type'=>"EMAIL",'required'=>true,'default'=>$defaults['tkemail'],'width'=>50,'max'=>150),
	),
	'stat' => array(
		'tkgender'=>array('fld'=>"tkgender",'txt'=>"Gender",'type'=>"LIST",'required'=>true,'table'=>"gender"),
		'tkrace'=>array('fld'=>"tkrace",'txt'=>"Race",'type'=>"LIST",'required'=>true,'table'=>"race"),
		'tkoccupcate'=>array('fld'=>"tkoccupcate",'txt'=>"Occupational Category",'type'=>"LIST",'required'=>true,'table'=>"occupcate"),
		'tkdisabled'=>array('fld'=>"tkdisabled",'txt'=>"Disabled?",'type'=>"YN",'required'=>true),
	),
);
?>