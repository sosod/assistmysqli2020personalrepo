<?php
//error_reporting(-1);
require_once "../module/autoloader.php";

$me = new ASSIST_TK();

ASSIST_HELPER::echoPageHeader();


$available_users = $me->getAvailableUsersForSuperUser();
$super_users = $me->getSuperUsers();
$logs = $me->getSuperUserLogs();
$permissions = $me->getSuperUserPermissionsFormatted(true);

$bp = $me->getBPAdetails();

?>
<h1>Setup >> Super Users</h1>
<p class=i>Super Users are users who require more stringent security settings (as defined in Users > Setup > Security Settings).
	<br />They can also be granted permission to login as the Admin user.</p>

<?php ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array()); ?>
<div class='float' style='width:506px'>
	<h3>Activity Log</h3>
	<table width=500px>
		<tr>
			<th width=150px>Date</th>
			<th>Activity</th>
		</tr>
		<?php
		if(count($logs)>0) {
			foreach($logs as $l) {
				echo "
				<tr>
					<td class=center>".$l['date']."</td>
					<td>".$l['activity']."</td>
				</tr>";
			}
		} else {
			echo "<tr><td colspan=2>No activity logs found.</td></tr>";
		}
		?>
		
	</table>
</div>
<div style='width:49%'>
	<form name=frm_new_super>
	<table id=tbl_super>
		<tr>
			<th width=50px>Ref</th>
			<th>User Name</th>
			<th>Special permissions</th>
			<th></th>
		</tr>
		<?php
		if(count($available_users)>0) {
			?>
			<tr>
				<td>&nbsp;</td>
				<td><select id=sel_addsuperuser name=tkid><option value=X selected>--- SELECT ---</option><?php
				foreach($available_users as $i=>$n) {
					echo "<option value='$i' >$n</option>";
				}
				?></select></td>
				<td><?php
					$p = array();
					foreach($permissions as $key => $text) {
						$p[] = "<input type=checkbox name=permissions[] value='$key' checked id=chkb_".$key." />&nbsp;<label for='chkb_".$key."'>$text</label>";
					}
					echo implode("<br />",$p);
					?></td>
				<td class=center><button class=btn_configure id=btn_add>Add</button></td>
			</tr>
			<?php
		}
		
		if(count($super_users)>0) {
			foreach($super_users as $i=>$u) {
				$n = $u['name']; //ASSIST_HELPER::arrPrint($u);
				$is_inactive = ($u['status']&ASSIST_TK::INACTIVE)==ASSIST_TK::INACTIVE;
				?>
				<tr <?php echo ($is_inactive ? "class=inactive" : ""); ?>>
					<td class=center><?php echo ASSIST_HELPER::decode($i); ?></td>
					<td><?php echo ASSIST_HELPER::decode($n); ?></td>
					<td><?php
					$p = array();
					foreach($permissions as $key => $text) {
						if(in_array($key,$u['permissions'])) {
							$p[] = $text;
						}
					}
					echo implode("<br />",$p);
					?></td>
					<td class=center><?php
						if($is_inactive) {
							echo "<button class=\"btn_configure btn_restore\" id=\"$i\">Restore</button>";
						} else {
							echo "
							<button class=\"btn_configure btn_edit\" id=\"edit_".$i."\" tkid=$i >Edit</button>
							<button class=\"btn_configure btn_deactivate\" id=\"$i\">Deactivate</button>";
						}
					?></td>
				</tr>
				<?php
			}
		}
		?>
	</table>
	</form>
</div>
<div id=dlg_edit title="Edit Super User">
	<form name=frm_edit>
		<input type=hidden value='' name=edit_tkid id=edit_tkid class=edit />
		<h1 id=h_user>User Name</h1>
		<p><span class=b>Permissions:</span></p>
		<?php 
		foreach($permissions as $key => $perm_text) {
			echo "<p><input type=checkbox name=edit_permissions[] class='chk_edit edit' value='$key' checked id=edit_chkb_".$key." />&nbsp;<label for='edit_chkb_".$key."'>$perm_text</label></p>";
		} ?>
		<span class=float><button class=btn_cancel id=btn_cancel>Cancel</button></span>
		<p><button class=btn_save id=btn_save>Save Changes</button></p>
	</form>
</div>
<script type=text/javascript>
$(function() {
	$("#dlg_edit").dialog({
		modal: true,
		autoOpen: false,
		width: "500px"
		
	});
	
	$("button.btn_edit").button({
		icons: {
			primary: "ui-icon-gear"
		}
	}).click(function(e) {
		e.preventDefault();
		var r = AssistHelper.doAjax("ajax_controller.php?act=ASSIST_TK.getSuperUserForEdit","tkid="+$(this).attr("tkid"));
		//console.log(r);
		$("#h_user").text(r['name']);
		$("#edit_tkid").val(r['id']);
		$(".chk_edit").prop("checked","");
		for(i in r['permissions']) {
			var k = r['permissions'][i];
			$("#edit_chkb_"+k).prop("checked","checked");
		}
		$("#dlg_edit").dialog("open");
	});	
	
	$("button#btn_cancel").button({
	}).click(function(e) {
		e.preventDefault();
		$("#dlg_edit").dialog("close");
	}).children(".ui-button-text").css({"font-size":"80%"});
	
	$("button#btn_save").button({
		icons: {
			primary: "ui-icon-disk"
		}
	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
			var dta = AssistForm.serialize($("form[name=frm_edit]"));
			var result = AssistHelper.doAjax("ajax_controller.php?act=ASSIST_TK.editSuperUser",dta);
			if(result[0]=="ok") {
				document.location.href = 'setup_super.php?r[]=ok&r[]='+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
	}).removeClass("ui-state-default").addClass("ui-button-state-ok").css({"color":"#009900","border":"1px solid #009900"
	}).children(".ui-button-text").css({"padding-top":"5px","padding-bottom":"5px","padding-left":"25px","font-size":"80%"});
	
	$("button#btn_add").button({
		icons: {
			primary: "ui-icon-circle-plus"
		}
	}).css({"float":"center"})
	.click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var u = $("#sel_addsuperuser").val();
		if(u=="X" || u.length == 0) {
			AssistHelper.finishedProcessing("error","Please select the user to be granted Super User access.");
		} else {
			var dta = AssistForm.serialize($("form[name=frm_new_super]"));
			var result = AssistHelper.doAjax("ajax_controller.php?act=ASSIST_TK.AddSuperUser",dta);
			if(result[0]=="ok") {
				document.location.href = 'setup_super.php?r[]=ok&r[]='+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	});

	$("button.btn_deactivate").button({
		icons: {
			primary: "ui-icon-closethick"
		}
	}).css({"float":"center"})
	.click(function(e) {
		e.preventDefault();
		var n = $(this).parent().parent().find("td:first").next().html();
		var u = $(this).prop("id");
		if(confirm("Are you sure you wish to remove Super User permission from "+n+"?")==true) {
			AssistHelper.processing();
			var result = AssistHelper.doAjax("ajax_controller.php?act=ASSIST_TK.deactivateSuperUser","tkid="+u);
			if(result[0]=="ok") {
				document.location.href = 'setup_super.php?r[]=ok&r[]='+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	});

	$("button.btn_restore").button({
		icons: {
			primary: "ui-icon-key"
		}
	}).css({"float":"center"})
	.click(function(e) {
		e.preventDefault();
		var n = $(this).parent().parent().find("td:first").next().html();
		var u = $(this).prop("id");
		if(confirm("Are you sure you wish to reactivate Super User permission for "+n+"?")==true) {
			AssistHelper.processing();
			var result = AssistHelper.doAjax("ajax_controller.php?act=ASSIST_TK.restoreSuperUser","tkid="+u);
			if(result[0]=="ok") {
				document.location.href = 'setup_super.php?r[]=ok&r[]='+result[1];
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}
	});

	$("button.btn_configure").children(".ui-button-text").css({"padding-top":"3px","padding-bottom":"3px","padding-left":"25px","font-size":"80%"});
	$("table th, table td").css("vertical-align","middle");
	
	$("table#tbl_super tr").hover(function(){$(this).addClass("tdhover");},function(){$(this).removeClass("tdhover");});
});
</script>
<?php
//if($_SESSION['cc']=="ait0001") { ASSIST_HELPER::arrPrint($_SESSION); }
?>