<?php
$inactive_users_modules = array("PMS","PMSPS");
$helper = new ASSIST_MODULE_HELPER();
$result = array();
//LICENSES
$sql = "SELECT modmenuid, moduserallowed, modid FROM assist_".$cmpcode."_menu_modules";
$mm = $helper->mysql_fetch_all_fld($sql,"modmenuid");
//MENU
$sql = "SELECT * FROM assist_menu_modules WHERE modyn = 'Y' OR modadminyn = 'Y' ORDER BY modtext";
$menu = $helper->mysql_fetch_all_fld($sql,"modref");
//arrPrint($menu);
//USER ACCESS
$sql = "SELECT usrtkid, usrmodid, UPPER(usrmodref) as usrmodref FROM assist_".$cmpcode."_menu_modules_users WHERE usrtkid = '".$obj_id."'";
$mmu = $helper->mysql_fetch_all_fld($sql,"usrmodref");
//arrPrint($mmu);

//arrPrint($_REQUEST);
if(isset($_REQUEST['act']) && $_REQUEST['act']=="SAVE") {
	$changes = array();
	foreach($_REQUEST['mr'] as $mr => $do) {
		switch($do) {
		case "GRANT":
			if(!isset($mmu[$mr])) {
				$sql = "INSERT INTO assist_".$cmpcode."_menu_modules_users (usrmodid, usrmodref, usrtkid) VALUES (".$mm[$menu[$mr]['modid']]['modid'].",'$mr','".$obj_id."')";
                $helper->db_insert($sql);
				$changes[] = array(
					'type'=>"G",
					'fld'=>"mmu",
					'new'=>$mr,
					'old'=>"",
					'txt'=>"Granted access to ".$menu[$mr]['modtext'].".",
					'sql'=>$sql
				);
			}
			break;
		case "REVOKE":
			if(isset($mmu[$mr])) {
				$sql = "DELETE FROM assist_".$cmpcode."_menu_modules_users WHERE usrtkid = '".$obj_id."' AND usrmodref = '$mr'";
                $helper->db_update($sql);
				$changes[] = array(
					'type'=>"R",
					'fld'=>"mmu",
					'new'=>"",
					'old'=>$mr,
					'txt'=>"Revoked access to ".$menu[$mr]['modtext'].".",
					'sql'=>$sql
				);
			}
			break;
		}	//switch
	}	//foreach
	foreach($changes as $c) {
		$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log 
				(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
				VALUES
				(now(),'0000','Ignite Assist Administrator','MENU','".$obj_id."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($c['sql'])."')";
        $helper->db_insert($sql2);
	}
	$result = array("ok","Module access for user '".$user['tkname']." ".$user['tksurname']."' has been updated.");
	//REDIRECT BACK TO USER LIST
	echo "<script type=text/javascript>
			document.location.href = 'manage.php?page=users&section=".$section."&r[]=".$result[0]."&r[]=".urlencode($result[1])."';
			</script>";
    $helper->displayResult($result);
}	//isset request act


echo "<h2>".$user['tkname']." ".$user['tksurname']."</h2>";
?>
<form id=frm_save_all action=manage.php method=post>
<input type=hidden name=page value=users_edit />
<input type=hidden name=section value=<?php echo $section; ?> />
<input type=hidden name=a value="edit access" />
<input type=hidden name=i value="<?php echo $obj_id; ?>" />
<input type=hidden name=act id=act value=SAVE />
<table>
	<tr>
		<th>Module</th>
		<th>Access</th>
		<th>Licenses<br />Available</th>
		<th></th>
	</tr>
<?php
foreach($menu as $m) {

	$modref = $m['modref'];
	$allowed = isset($mm[$m['modid']]['moduserallowed']) ? $mm[$m['modid']]['moduserallowed'] : 0;
//	$rs = getRS("SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep WHERE tkid IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref') AND ".(in_array($modref,$inactive_users_modules) ? "tkstatus > 0" : "tkstatus = 1"));
//	$rs = getRS("SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep
//				WHERE tkid IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref')
//				AND tkuser <> 'support' AND tkuser <> 'admin'
//				AND ".(in_array($modref,$inactive_users_modules) ? "tkstatus > 0" : "tkstatus = 1"));
    $sql = "SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep 
				WHERE tkid IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref')
				AND tkuser <> 'support' AND tkuser <> 'admin' 
				AND ".(in_array($modref,$inactive_users_modules) ? "tkstatus > 0" : "tkstatus = 1");
	$access = $helper->db_get_num_rows($sql);
	$remain = $allowed - $access;
	//$rs = getRS("SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep WHERE tkid NOT IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref') AND ".(in_array($modref,$inactive_users_modules) ? "tkstatus > 0" : "tkstatus = 1"));
	$sql = "SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep WHERE tkid NOT IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref') AND ".(in_array($modref,$inactive_users_modules) ? "tkstatus > 0" : "tkstatus = 1");
	$without = $helper->db_get_num_rows($sql);

	if($allowed<0) { $allowed = "Unlimited"; $remain = "Unlimited"; }
	echo "	<tr>
				<td id=td_".$modref.">".$helper->decode($m['modtext'])."</td>
				<td class=center>";
				if(isset($mmu[$m['modref']]) || !is_numeric($remain) || $remain*1 > 0) {
					echo "<select name=mr[".$m['modref']."] id=sel_".$m['modref'].">
								<option value=REVOKE class=required ".(!isset($mmu[$m['modref']]) ? "selected" : "").">&nbsp;&nbsp;No&nbsp;&nbsp;</option>
								<option value=GRANT class=ok ".(isset($mmu[$m['modref']]) ? "selected" : "").">&nbsp;Yes</option>
							</select>";
				} else {
					echo "N/A";
				}
	echo "		</td>
				<td class=center>".$remain." license".($remain==1?"":"s")."</td>
				<td class=center>".((isset($mmu[$m['modref']]) || !is_numeric($remain) || $remain*1 > 0) ? "<input type=button value=Save id=".$m['modref']." />" : "")."</td>
			</tr>";
}
?>
<tfoot><tr>
	<td></td>
	<td><input type=button value="Save All" id=all /></td>
	<td colspan=2></td>
</tr></tfoot>
</table>
</form>
<form id=frm_save_one action=manage.php method=post>
<input type=hidden name=page value=users_edit />
<input type=hidden name=section value=<?php echo $section; ?> />
<input type=hidden name=a value="edit access" />
<input type=hidden name=i value="<?php echo $obj_id; ?>" />
<input type=hidden name=act id=act value=SAVE />
<input type=hidden id=val value="" name="" />
</form>
<?php
//	include("inc_debitorder.php");
//	echo("<p>Click <a href=".$debitfile." target=_blank><b>here</b></a> to apply for more licences (updated: ".$debitdisplay.").</p>");
?>
<script type=text/javascript>
$(document).ready(function(){
	$("tr").hover(
		function(){ $(this).addClass("trhover"); },
		function(){ $(this).removeClass("trhover"); }
	);
	$(".no-highlight").unbind('mouseenter mouseleave');
	$("input:button").click(function() {
		var id = $(this).attr("id");
		if(id.toLowerCase()=="all") {
			$("#frm_save_all").submit();
		} else {
			var v = $("#sel_"+id).val();
			$("#frm_save_one #val").val(v);
			$("#frm_save_one #val").attr("name","mr["+id+"]");
			$("#frm_save_one").submit();
		}
	});
});
</script>
<?php $helper->displayGoBack(); ?>
