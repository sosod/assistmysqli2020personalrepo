<?php
//require_once("../library/class/autoload.php");
require_once ("../module/autoloader.php");
$me = new ASSIST();
$db = new ASSIST_DB();
$me->displayPageHeaderJQ();
?>
<style type="text/css" >
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>
<h1>Reports >> User Details Log Report</h1>
<p class="no-print"><input type=button value=Filter id=btn_filter /></p>
<?php

$sql = "SELECT MIN(`date`) as mindate FROM `assist_".$db->getCmpCode()."_timekeep_log` L ";
$first_date = $db->mysql_fetch_one_value($sql,"mindate");
if(strlen($first_date)==0 || strtotime($first_date)==0) {
	$min_date = date("Y").", ".date("m").", ".date("d"); 
} else {
	$min_date = date("Y",strtotime($first_date)).", ".date("m",strtotime($first_date)).", ".date("d",strtotime($first_date)); 
}


$logs = array();
if(isset($_REQUEST['action']) && $_REQUEST['action']=="GENERATE") {
	$generate_report=true;
	if(count($_REQUEST['log'])>0){
		$sql_date = "";
		if(strlen($_REQUEST['from'])>0 || strlen($_REQUEST['to'])>0) {
			$s = array();
			if(strlen($_REQUEST['from'])>0) {
				$s[] = " `date` >= CAST('".date("Y-m-d",strtotime($_REQUEST['from']))." 00:00:00' as DATETIME) ";
			}
			if(strlen($_REQUEST['to'])>0) {
				$s[] = " `date` <= CAST('".date("Y-m-d",strtotime($_REQUEST['to']))." 23:59:59' as DATETIME) ";
			}
			$sql_date = " AND ".implode(" AND ",$s);
		}
		$sql = "SELECT id,`date`, section, action,transaction, TK.tkid, CONCAT(TK.tkname,' ',tksurname) as name 
				FROM `assist_".$db->getCmpCode()."_timekeep_log` L 
				INNER JOIN assist_".$db->getCmpCode()."_timekeep TK 
					ON TK.tkid = ref
				WHERE TK.tkuser <> 'support' AND L.ref <> 0
				AND L.section IN ('".implode("','",$_REQUEST['log'])."')
				".($sql_date)."
				ORDER BY TK.tkname, tksurname, TK.tkid, `date`;";
		$logs = $db->mysql_fetch_all_by_id2($sql,"tkid","id");
	}
} else {
	$generate_report=false;
}
//ASSIST_HELPER::arrPrint($logs);

?>
<table id=tbl_log>
	<tr>
		<th>User ID</th>
		<th>User Name</th>
		<th colspan=2>Activity Log</th>
	</tr>
	<?php
	if(count($logs)>0) {
		$tr = false;
		foreach($logs as $u) {
			$tr = false;
			$rowspan = count($u);
			foreach($u as $l) {
				echo "<tr>";
				if(!$tr) {
					echo "<td rowspan=".$rowspan.">".$l['tkid']."</td>
					<td rowspan=".$rowspan.">".$l['name']."</td>";
					$tr = true;
				}
				echo "<td>".date("d-M-Y H:i",strtotime($l['date']))."</td><td>".str_replace(chr(10),"<br />",$l['transaction'])."</td></tr>";
			}
		}
	} elseif($generate_report) {
		echo "
		<tr>
			<td colspan=4>No logs were found for the filter settings you selected.</td>
		</tr>";
	} else {
		echo "
		<tr>
			<td colspan=4>Please apply a filter in order to view the Activity Logs.</td>
		</tr>";
	}
	?>
</table>
<p class=i>Report drawn on <?php echo date("d F Y H:i:s"); ?>.</p>
<div id=div_filter title=Filter>
	<form name=frm_filter>
		<input type="hidden" name=action value=GENERATE />
		<table id=tbl_filter class=form width="400px">
			<tr>
				<th>Type of Log:</th>
				<td>
					<input type=checkbox name=log[] value=DETAILS checked /> User Details<br />
					<input type=checkbox name=log[] value=MENU checked /> Module Menu Access
				</td>
			</tr>
			<tr>
				<th>Date:</th>
				<td>From <input type=text class="datepicker" size=12 name=from /> to <input type=text name=to class="datepicker" value='<?php echo date("d-M-Y"); ?>' size=12 /></td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript" >
$(function(){
	$("#div_filter").dialog({
		modal:true,
		width:"450px",
		autoOpen:<?php echo ($generate_report ? "false" : "true"); ?>,
		buttons:[{
			text:"Apply",
			click:function(){
				document.location.href = 'auditlog_report.php?'+AssistForm.serialize($("form[name=frm_filter]"));
				$(this).dialog("close");
			}
		}]
	}).dialog("widget").find(".ui-dialog-buttonpane button").eq(0).css(AssistHelper.getGreenCSS()).end();
	$("#div_filter .datepicker").datepicker({
        showOn: 'both',
        buttonImage: '/library/jquery/css/calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'dd-M-yy',
        changeMonth:true,
        changeYear:true,
        maxDate: 0,
        minDate: new Date(<?php echo $min_date; ?>)		
	});
	$("#btn_filter").button().css(AssistHelper.getGreenCSS()).css("font-size","80%").click(function(){
		$("#div_filter").dialog("open");
		$("#div_filter input").blur();
		
	});
});
</script>
</body>

</html>
