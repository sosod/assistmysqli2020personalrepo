<?php
include("inc_header.php");
?>
<style type="text/css" >
@media print
{
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>

<h1>User Activity Report</h1>
<p class="no-print"><input type=button value=Filter id=btn_filter /></p>
<?php
//error_reporting(-1);
$db = new ASSIST_DB();

$generate_report = false;
$sql = "SELECT date FROM assist_".$db->getCmpCode()."_timekeep_activity ORDER BY date ASC LIMIT 1";
$z = $db->mysql_fetch_one($sql);
$first_date = strtotime($z['date']);
$now = time();
$days = floor(($now - $first_date)/86400);

	$sql = "SELECT * FROM assist_".$db->getCmpCode()."_timekeep WHERE tkuser = 'support'";
	$support = $db->mysql_fetch_one($sql);


if(isset($_REQUEST['action']) && $_REQUEST['action']=="GENERATE") {
	$generate_report = true;


	$mdb = new ASSIST_DB("master");
	$sql = "SELECT r.cmpcode, c.cmpname FROM `assist_reseller` r INNER JOIN assist_company c ON c.cmpcode = r.cmpcode";
	$resellers = $mdb->mysql_fetch_value_by_id($sql, "cmpcode", "cmpname");



	$sql = "SELECT modref, modtext FROM assist_menu_modules";
	$modules = $db->mysql_fetch_value_by_id($sql, "modref", "modtext");
	$modules['TK'] = "Users";
	$modules['S'] = "Setup";
	$modules['ACTION_DASHBOARD'] = "Frontpage Dashboard";
	$modules['USER_PROFILE'] = "My Profile";
	$modules['MAN'] = "Help";
	$modules['CHANGEPASS'] = "Change Password";


	$where = array();
	if(isset($_REQUEST['tkid']) && $_REQUEST['tkid']!="ALL" && strlen($_REQUEST['tkid'])>3) {
		$where[] = "A.tkid = '".$_REQUEST['tkid']."'";
	}
	if(isset($_REQUEST['from']) && strlen($_REQUEST['from'])>0) {
		$from = strtotime($_REQUEST['from']." 00:00:00");
		$where[] = "A.date >= '".date("Y-m-d H:i:s",$from)."'";
	}
	if(isset($_REQUEST['to']) && strlen($_REQUEST['to'])>0) {
		$to = strtotime($_REQUEST['to']." 00:00:00");
		$where[] = "A.date <= '".date("Y-m-d H:i:s",$to)."'";
	}

	$sql = "SELECT A.*
			FROM assist_".$db->getCmpCode()."_timekeep_activity A
			WHERE (A.tkid <> '0000' AND A.tkid <> '".$support['tkid']."')
			".(count($where)>0 ? " AND ".implode(" AND ",$where) : "")."
			ORDER BY date ASC";
	$activity = $db->mysql_fetch_all_by_id($sql,"id");
	$acts = array();
	foreach($activity as $sk => $s) {
		$sd = strtotime($s['date']);
		$acts[$sd] = $s;
	}

	$full = $acts;
	ksort($full);

	foreach($full as $key => $f) {
		if(substr($f['action'],0,2)!="IN") {
			unset($full[$key]);
		} else {
			break;
		}
	}

	$keys = array_keys($full);

	$logs = array();
	$c = 0;
	$has_logout = true;
	$failed_login = false;
	//foreach($full as $a) {
	for($i=0;$i<count($keys);$i++) {
		$a = $full[$keys[$i]];
		if(substr($a['action'],0,2)=="IN") {
			if(!$has_logout && !$failed_login) {
				$current_time = strtotime($a['date']);
				$expired_time = strtotime($full[$keys[$i-1]]['date'])+3600;
				$logs[$c]['activity'][($current_time<$expired_time ? $current_time-10 : $expired_time)] = "Logout: Session expired";
			} else {
				$has_logout = false;
			}
			$c++;
			if($a['action']=="IN") {
				$failed_login = false;
				$d = strtotime($a['date']);
				$logs[$c] = array(
					'date' => $d,
					'super_user'=>$a['tkname'],
					'user' => "",
					'activity'=>array(
						strtotime($a['date'])=>"Login"
					),
				);
			} else {
				switch($a['action']) {
					case "IN_FAIL_PWD":
						$display_wording = "Failed Login attempt [Incorrect password]";
						break;
					case "IN_FAIL_LOCKOUT":
						$display_wording = "Failed Login attempt [Account locked]";
						break;
					default:
						$display_wording = "Failed Login attempt [Error code: ".$a['action']."]";
						break;
				}
				$logs[$c] = array(
					'date' => strtotime($a['date']),
					'super_user'=>$a['tkname'],
					'user' => "",
					'activity'=>array(
						strtotime($a['date'])=>$display_wording//"Failed Login attempt [Login Result: ".$a['action']."]"
					),
				);
				$failed_login = true;
			}
		} elseif(substr($a['action'],0,3)=="OUT") {
			$y = "";
			if(strlen($a['action'])>3) {
				$y = substr($a['action'],4,strlen($a['action']));
				if($y=="TIME" || $y=="TIMEOUT") {
				} else {
					$y = base64_decode($y);
				}
				if($y=="TIME" || $y=="TIMEOUT") {
					$y = ": Timeout";
				}
			}
			$logs[$c]['activity'][strtotime($a['date'])] = "Logout".$y."";
			$has_logout = true;
		} else {
			$z = strtoupper($a['action']);
			$logs[$c]['activity'][strtotime($a['date'])] = (isset($modules[$z]) ? $modules[$z] : $z)."";
		}
	} //end for

	//ASSIST_HELPER::arrPrint($a);
	if(substr($a['action'],0,3)!=="OUT") {
				$current_time = strtotime(date("d F Y H:i:s"));
				$expired_time = strtotime($a['date'])+3600;
		if($current_time > $expired_time) {
			$logs[$c]['activity'][$expired_time] = "Logout: Session expired";
		}
	}

	?>
	<table class=list>
		<tr>
			<th rowspan=2>Date</th>
			<th rowspan=2>User</th>
			<th colspan=2>Movements</th>
		</tr>
		<tr>
			<th>Date</th>
			<th>Activity</th>
		</tr>
	<?php
	foreach($logs as $l) {
		echo "
		<tr>
			<td rowspan=".count($l['activity']).">".date("d M Y H:i:s",$l['date'])."</td>
			<td rowspan=".count($l['activity']).">".$l['super_user']."</td>
			";
			$i = 0;
			foreach($l['activity'] as $d => $a) {
				if($i>0) { echo "</tr><tr>"; }
				echo "<td>".date("d M Y H:i:s",$d)."</td><td>".$a."</td>";
				$i++;
			}
		echo "
			</td>
		</tr>";
	}
	?>
	</table>
	<p class=i>Report generated: <?php echo date("d F Y H:i:s"); ?></p>
<?php
}




?>
<div id=div_filter title=Filter>
	<form name=frm_filter>
		<input type="hidden" name=action value=GENERATE />
		<table id=tbl_filter class=form width="400px">
			<tr>
				<th>User:</th>
				<td><select name=tkid><option value=ALL selected>All users</option>
					<?php
					$sql = "SELECT DISTINCT tkid, tkname FROM assist_".$db->getCmpCode()."_timekeep_activity WHERE tkid <> '0000' AND tkid <> '".$support['tkid']."' AND tkname <> '' AND tkid <> '' ORDER BY tkname";
					$u = $db->mysql_fetch_value_by_id($sql,"tkid","tkname");
					foreach($u as $i=>$t) {
						echo "<option value='".$i."'>".ASSIST_HELPER::decode($t)."</option>";
					}
					?>
				</select>
				</td>
			</tr>
			<tr>
				<th>Date:</th>
				<td>From <input type=text class="datepicker"  value='<?php echo date("d-M-Y",($first_date)); ?>' size=12 name=from /> to <input type=text name=to class="datepicker" value='<?php echo date("d-M-Y"); ?>' size=12 /></td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript" >
$(function(){
	$("#div_filter").dialog({
		modal:true,
		width:"500px",
		autoOpen:<?php echo ($generate_report ? "false" : "true"); ?>,
		buttons:[{
			text:"Apply",
			click:function(){
				AssistHelper.processing();
				document.location.href = 'report_user_activity.php?'+AssistForm.serialize($("form[name=frm_filter]"));
				$(this).dialog("close");
			}
		}]
	}).dialog("widget").find(".ui-dialog-buttonpane button").eq(0).css(AssistHelper.getGreenCSS()).end();
	$("#div_filter .datepicker").datepicker({
        showOn: 'both',
        buttonImage: '/library/jquery/css/calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'dd-M-yy',
        changeMonth:true,
        changeYear:true,
        maxDate: 0,
        minDate: "<?php echo "-".$days."D"; ?>"
	});

	$("#btn_filter").button().css(AssistHelper.getGreenCSS()).css("font-size","80%").click(function(){
		$("#div_filter").dialog("open");
		$("#div_filter input").blur();

	});
});
</script>
</body>
</html>