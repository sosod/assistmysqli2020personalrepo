<?php
include("inc_header.php");

//error_reporting(-1);

/*** LEVEL 1 NAVIGATION :: PAGE ***/
	$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "users";
	$page_id = $page=="users_edit" ? "users" : $page;
	if($page=="users" || $page=="users_edit") {
		$section = isset($_REQUEST['section']) ? $_REQUEST['section'] : "active";
		$nav_section = array(
			'active'=>"Active",
//			'inactive'=>"Inactive",
			'new'=>"New",
			'locked'=>"Locked",
			'term'=>"Terminated",
			'all'=>"All",
			'employees'=>"Employees (Non-Users)",
		);
	}
	$level = 1;
	$nav = array(
		'users'=>"Update Users",
		'modules'=>"Update Module Access",
		'pass'=>"Reset Password",
		//'login'=>"New User Login Window",
	);
	$menu = array();
	foreach($nav as $id => $d) {
		//AA-589 TK - standardization [SD] 'url' changed to 'link', 'display' changed to 'name', and 'help' added.
		$menu[$id] = array('id'=>$id,'link'=>"manage.php?page=".$id,'active'=>($page_id==$id ? true : false),'name'=>$d,'help'=>'');
	}
	//echoNavigation($level,$menu);
	echo $helper->generateNavigationButtons($menu, $level);//AA-589 TK - standardization [SD]
/*** LEVEL 2 NAVIGATION :: SECTION ***/
	if(isset($section) && strlen($section)>0 && isset($nav_section) && count($nav_section)>0) {
		$smenu = array();
		foreach($nav_section as $i => $d) {
			//AA-589 TK - standardization [SD] 'url' changed to 'link', 'display' changed to 'name', and 'help' added.
			$smenu[$i] = array('id'=>$i,'link'=>"manage.php?page=".$page_id."&section=".$i,'active'=>($section==$i ? true : false),'name'=>$d,'help'=>'');
		}
		//echoNavigation(2,$smenu);
		echo $helper->generateNavigationButtons($smenu, 2);//AA-589 TK - standardization [SD]
	}


include("manage_".$page.".php");

?>
</body>
</html>