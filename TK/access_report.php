<?php
//error_reporting(-1);
require_once("../library/class/autoload.php");
$me = new ASSIST();
$db = new ASSIST_DB();
$me->displayPageHeaderJQ();
?>
<style type="text/css" >
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>
<h1>Reports >> Login Attempt Log Report</h1>
<p class="no-print"><input type=button value=Filter id=btn_filter /></p>
<?php

//$sql = "SELECT MIN(`date`) as mindate FROM `assist_".$db->getCmpCode()."_timekeep_activity` L WHERE ";
//$first_date = $db->mysql_fetch_one_value($sql,"mindate");
$first_date = strtotime("2015-07-01 00:00:00");
$now = time();
$days = floor(($now - $first_date)/86400);
/*if(strlen($first_date)==0 || strtotime($first_date)==0) {
	$min_date = date("Y").", ".date("n").", ".date("d"); 
} else {
	$min_date = date("Y",strtotime($first_date)).", ".date("n",strtotime($first_date)).", ".date("d",strtotime($first_date)); 
}*/
//echo ":".$min_date;


$logs = array();
if(isset($_REQUEST['action']) && $_REQUEST['action']=="GENERATE") {
	$generate_report=true;
	if(count($_REQUEST['log'])>0){
		$sql_date = "";
		if(strlen($_REQUEST['from'])>0 || strlen($_REQUEST['to'])>0) {
			$s = array();
			if(strlen($_REQUEST['from'])>0) {
				$s[] = " `date` >= CAST('".date("Y-m-d",strtotime($_REQUEST['from']))." 00:00:00' as DATE) ";
			}
			if(strlen($_REQUEST['to'])>0) {
				$s[] = " `date` <= CAST('".date("Y-m-d",strtotime($_REQUEST['to']))." 23:59:59' as DATE) ";
			}
			$sql_date = " AND ".implode(" AND ",$s);
		}
		/*$sql = "SELECT id,`date`, section, action,transaction, TK.tkid, CONCAT(TK.tkname,' ',tksurname) as name 
				FROM `assist_".$db->getCmpCode()."_timekeep_activity` L 
				INNER JOIN assist_".$db->getCmpCode()."_timekeep TK 
					ON TK.tkid = ref
				WHERE TK.tkuser <> 'support' AND L.ref <> 0
				AND L.section IN ('".implode("','",$_REQUEST['log'])."')
				".($sql_date)."
				ORDER BY TK.tkname, tksurname, TK.tkid, `date`;";*/
		$sql = "SELECT L.id, L.tkid, L.date, L.action, L.tkname as name, LD.details 
				FROM `assist_".$db->getCmpCode()."_timekeep_activity` L 
				LEFT OUTER JOIN assist_".$db->getCmpCode()."_timekeep_activity_details LD 
					ON L.id = LD.activity_id
				WHERE L.id > 0
				".($sql_date)."
		";
		$sq = array();
		foreach($_REQUEST['log'] as $lt) {
			switch($lt) {
				case "TERM":
					$sq[]= " L.action = 'IN_FAIL_S_0'";
					break;
				default:
					$sq[]= " L.action = 'IN_FAIL_".$lt."'";
					break;
			}
		} 
		$sql.=" AND (".implode(" OR ",$sq).")
ORDER BY `date`, L.tkname, L.tkid";
		//echo $sql;
		$logs = $db->mysql_fetch_all($sql);
	}
} else {
	$generate_report=false;
}
//ASSIST_HELPER::arrPrint($logs);

?>
<table id=tbl_log>
	<tr>
		<th>Date</th>
		<th>User ID</th>
		<th>User Name</th>
		<th colspan=2>Activity Log</th>
	</tr>
	<?php
	if(count($logs)>0) {
		$tr = false;
		foreach($logs as $l) {
			$trans = "";
			switch($l['action']){
				case "IN_FAIL_PWD":
					$trans = "Incorrect password.";
					break;
				case "IN_FAIL_USER":
					$trans = "Unknown Username";
					$x = !is_null($l['details']) && strlen($l['details'])>0 ? unserialize($l['details']) : array();
					$trans.= isset($x['user']) ? ": ".$x['user'] : "";
					break;
				case "IN_FAIL_S_0":
					$trans = "Account terminated.";
					break;
				case "IN_FAIL_LOCKED":
					$trans = "Account locked by Administrator.";
					break;
				case "IN_FAIL_LOCKOUT":
					$trans = "Account locked due to too many failed login attempts.";
					break;
			}
			echo "
			<tr>
				<td>".date("d-M-Y H:i",strtotime($l['date']))."</td>
				<td>".($l['tkid']=="X" ? "-" : $l['tkid'])."</td>
				<td>".($l['name']=="X" ? "-" : $l['name'])."</td>
				<td>".$trans."</td>
			</tr>";
		}
	} elseif($generate_report) {
		echo "
		<tr>
			<td colspan=4>No logs were found for the filter settings you selected.</td>
		</tr>";
	} else {
		echo "
		<tr>
			<td colspan=4>Please apply a filter in order to view the Activity Logs.</td>
		</tr>";
	}
	?>
</table>
<p class=i>Report drawn on <?php echo date("d F Y H:i:s"); ?>.</p>
<div id=div_filter title=Filter>
	<form name=frm_filter>
		<input type="hidden" name=action value=GENERATE />
		<table id=tbl_filter class=form width="400px">
			<tr>
				<th>Type of Log:</th>
				<td>
					<input type=checkbox name=log[] value=PWD checked /> Failures: Password<br />
					<input type=checkbox name=log[] value=USER checked /> Failures: Unknown Username<br />
					<input type=checkbox name=log[] value=LOCKOUT checked /> Failures: Locked account (Password failures)<br />
					<input type=checkbox name=log[] value=LOCKED checked /> Failures: Locked account (locked by Admin)<br />
					<input type=checkbox name=log[] value=TERM checked /> Failures: Terminated account<br />
				</td>
			</tr>
			<tr>
				<th>Date:</th>
				<td>From <input type=text class="datepicker"  value='<?php echo date("d-M-Y",($first_date)); ?>' size=12 name=from /> to <input type=text name=to class="datepicker" value='<?php echo date("d-M-Y"); ?>' size=12 /></td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript" >
$(function(){
	$("#div_filter").dialog({
		modal:true,
		width:"450px",
		autoOpen:<?php echo ($generate_report ? "false" : "true"); ?>,
		buttons:[{
			text:"Apply",
			click:function(){
				AssistHelper.processing();
				document.location.href = 'access_report.php?'+AssistForm.serialize($("form[name=frm_filter]"));
				$(this).dialog("close");
			}
		}]
	}).dialog("widget").find(".ui-dialog-buttonpane button").eq(0).css(AssistHelper.getGreenCSS()).end();
	$("#div_filter .datepicker").datepicker({
        showOn: 'both',
        buttonImage: '/library/jquery/css/calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'dd-M-yy',
        changeMonth:true,
        changeYear:true,
        maxDate: 0,
        minDate: "<?php echo "-".$days."D"; ?>"
	});
        
	$("#btn_filter").button().css(AssistHelper.getGreenCSS()).css("font-size","80%").click(function(){
		$("#div_filter").dialog("open");
		$("#div_filter input").blur();
		
	});
});
</script>
</body>

</html>
