<?php
$result = array();
switch(strtolower($act)) {
case "unlock":
		$sql = "UPDATE assist_".$cmpcode."_timekeep SET tklock = 'N' WHERE tkid = '".$obj_id."'";
		$helper->db_update($sql);
		$c = array(
			'type'=>"L",
			'fld'=>"tklock",
			'new'=>"N",
			'old'=>"Y",
			'txt'=>"Unlocked user."
		);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'0000','Assist Administrator','DETAILS','".$obj_id."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($sql)."')";
    $helper->db_insert($sql2);
				$result = array("ok","User '".$user['tkname']." ".$user['tksurname']."' has been unlocked.");
	break;
case "reset":
	$new_date = date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("d")+7,date("Y")));
		$sql = "UPDATE assist_".$cmpcode."_timekeep SET tkloginwindow = '".$new_date."' WHERE tkid = '".$obj_id."'";
    $helper->db_update($sql);
		$c = array(
			'type'=>"E",
			'fld'=>"tkloginwindow",
			'new'=>$new_date,
			'old'=>$user['tkloginwindow'],
			'txt'=>"Reset first-time login window to ".date("d M Y",strtotime($new_date))." from ".date("d M Y",strtotime($user['tkloginwindow']))."."
		);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'0000','Assist Administrator','DETAILS','".$obj_id."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($sql)."')";
    $helper->db_insert($sql2);
				$result = array("ok","First-time Login Window for '".$user['tkname']." ".$user['tksurname']."' has been reset to ".date("d M Y",strtotime($new_date)).".");
	//SEND LOGIN DETAILS VIA EMAIL?
	if(isset($_REQUEST['act']) && strtolower($_REQUEST['act']) == "yes" && strlen($user['tkemail'])>0) {
		$cmpadmin = $helper->getCmpAdmin($cmpcode);
		$bpa_details = $helper->getBPAdetails();
		$bpa_con = explode("|",$bpa_details['help_contact']);
		$bpa_contact = array();
		foreach($bpa_con as $b) {
			$b2 = explode("_",$b);
			$bpa_contact[] = $b2[1];
		}
		$tkpwd = hexToStr($user['tkpwd']);
		$subject = "Welcome to ".$site_name."";
		$message = "Dear ".$helper->decode(trim($user['tkname'])." ".trim($user['tksurname'])).chr(10);
		$message.= " ".chr(10);
		$message.= "We would like to welcome you to ".$site_name.". ".chr(10);
		$message.= " ".chr(10);
		$message.= "Your ".$site_name." login details are: ".chr(10);
		$message.= "User name: ".$user['tkuser'].chr(10);
		$message.= "Company ID: ".strtoupper($cmpcode).chr(10);
		$message.= "Password: ".$tkpwd.chr(10);
		$message.= "Please keep your login details confidential. ".chr(10);
		$message.= " ".chr(10);
		$message.= "To login go to: http://assist.action4u.co.za/?".$site_code." ".chr(10);
		$message.= "Please login within 7 days or your account will be locked. ".chr(10);
		$message.= " ".chr(10);
		$message.= "For assistance please contact: ".chr(10);
		$message.= "- Your Assist Administrator, ".$cmpadmin['cmpadmin'].(strlen($cmpadmin['cmpadminemail'])>0 ? ", at ".$cmpadmin['cmpadminemail'] : ",")." or ".chr(10);
//Removed Business Partner name to accommodate the separate reseller & support reseller [AA-534] JC
		$message.= "- Your ".$site_name." Business Partner at ".implode(" or ",$bpa_contact).". ".chr(10);
		$message.= " ".chr(10);
		$message.= "Kind regards ".chr(10);
//Removed Business Partner name to accommodate the separate reseller & support reseller [AA-534] JC
		$message.= "".$site_name." ".chr(10);
		//$message.= " ".chr(10);
		//$message.= "http://www.igniteassist.co.za".chr(10);

		//$to = $user['tkemail'];
		$from = $bpa_details['help_email'];
		$to = array('name'=>$helper->decode(trim($user['tkname'])." ".trim($user['tksurname'])),'email'=>$user['tkemail']);
		//$header = "From: ".$bpa_details['cmpname']." <no-reply@ignite4u.co.za>\r\nReply-to: ".$bpa_details['cmpname']." <".$from.">\r\nCC: ".$from."\r\nBCC: webmaster@ignite4u.co.za";
		//mail($to,$subject,$message,$header);
								$mail = new ASSIST_EMAIL($to, $subject, $message);
								$mail->setSender(array('name'=>$bpa_details['cmpname'],'email'=>$from));
								$mail->setCC($from);
								$mail->setBCC("helpdesk_cc@actionassist.co.za");
								$mail->sendEmail();

		$result[1].=" An email has been sent to ".$to['email'].".";
	}
	break;
case "restore":
	if(isset($user['tkemail']) && strlen($user['tkemail'])>0 && isset($user['tkuser']) && strlen($user['tkuser'])>0 && isset($user['tkpwd']) && strlen($user['tkpwd'])>0) {
		$status = 1;	//active user
		$trans = "Restored user as an active user.";
	} else {
		$status = 2;	//inactive user
		$trans = "Restored user as an inactive user.";
	}
		$sql = "UPDATE assist_".$cmpcode."_timekeep SET tkstatus = $status, tktermdate = '' WHERE tkid = '".$obj_id."'";
    $helper->db_update($sql);
		$c = array(
			'type'=>"T",
			'fld'=>"tkstatus",
			'new'=>$status,
			'old'=>"0",
			'txt'=>$trans
		);
					$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'0000','Assist Administrator','DETAILS','".$obj_id."','".$c['type']."','".$c['fld']."','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($sql)."')";
    $helper->db_insert($sql2);
				$result = array("ok","User '".$user['tkname']." ".$user['tksurname']."' has been restored ".substr($trans,14));
	break;
}
//				$result = array("ok","User '".$user['tkname']." ".$user['tksurname']."' has been locked.");
				echo "<script type=text/javascript>
						document.location.href = 'manage.php?page=users&section=".$section."&r[]=".$result[0]."&r[]=".urlencode($result[1])."';
						</script>";
$helper->displayResult($result);
?>