<?php 
require_once("../module/autoloader.php");

$me = new ASSIST();

$act = $_REQUEST['act'];

$result = array("info","Didn't understand the request.  Please try again.");

if(stripos($act,".")!==false) {
	$var = $_REQUEST;
	unset($var['act']);
	$v = explode(".",$act);
	$class = $v[0];
	$result = array("info",implode("-",$v)." :: ".$class);
	$function = $v[1];
	$object = new $class();
	$result = call_user_func(array($object, $function),$var);
	/*
	 * 
	 */
} else {
	switch($act) {
		case "SAVE_SECURITY":
			$answers = array();
			for($a=1;$a<=3;$a++) {
				$answers['tkpwdanswer'.$a] = $_REQUEST['tkpwdanswer'.$a];
			}
			$result[2] = $me->updateSecurityAnswers($_REQUEST['tkid'],$answers);
			$result[0] = "ok";
			$result[1] = "Update completed successfully.";
			break;
		case "LOG_THAT_USER_HAS_SEEN_SECURITY_NOTICE":
			$db = new ASSIST_DB();
			$sql = "INSERT INTO assist_".$db->getCmpCode()."_timekeep_temp_security (tkid,datetime,action) VALUES ('".$me->getUserID()."',now(),'".$_REQUEST['action']."')";
			$result[2] = $db->db_insert($sql);
			$result[0] = "ok";
			$result[1] = "Update completed successfully.";
			break;
		case "LOG_THAT_USER_HAS_SEEN_LAYOUT_CHANGE_NOTICE":
			$db = new ASSIST_DB();
			$sql = "INSERT INTO assist_".$db->getCmpCode()."_timekeep_temp_security (tkid,datetime,action) VALUES ('".$me->getUserID()."',now(),'layout')";
			$result[2] = $db->db_insert($sql);
			$result[0] = "ok";
			$result[1] = "Update completed successfully.";
			break;
		case "LOG_THAT_USER_HAS_SEEN_POPIA":
			$db = new ASSIST_DB();
			$sql = "INSERT INTO assist_".$db->getCmpCode()."_timekeep_temp_security (tkid,datetime,action) VALUES ('".$me->getUserID()."',now(),'popia')";
			$result[2] = $db->db_insert($sql);
			$result[0] = "ok";
			$result[1] = "Update completed successfully.";
			break;
		case "SAVE_SESSION":
			$_SESSION['TK']['stored']["PASSWORD_RESET"] = $_REQUEST;
			$result[0] = "ok";
			$result[1] = "Session saved.";
			$result[2] = "PASSWORD_RESET";
			break;
		case "LOG_THAT_USER_HAS_SEEN_CLOSURE_WARNING":
			$_SESSION['account_close_notice'] = time();
			break;
	}
}	

echo json_encode($result);

?>