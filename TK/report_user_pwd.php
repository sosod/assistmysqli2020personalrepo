<?php 
include("inc_header.php"); 

$db = new ASSIST_DB();

$sql = "SELECT * FROM assist_".$db->getCmpCode()."_timekeep WHERE tkid <> '0000' AND tkuser <> 'support' AND tkstatus > 0 ORDER BY tkname, tksurname";
$all = $db->mysql_fetch_all_by_id($sql,"tkid");

$sql = "SELECT id, tkid FROM assist_".$db->getCmpCode()."_timekeep_superusers WHERE (status & 2) = 2";
$supers = $db->mysql_fetch_value_by_id($sql, "id", "tkid");

//ASSIST_HELPER::arrPrint($supers);


$sql = "SELECT * FROM assist_".$db->getCmpCode()."_timekeep_setup WHERE code = 'pwd_age'";
$pwd_age = $db->mysql_fetch_one($sql);

foreach($all as $id => $u) {
	$p = $u['tkpwddate'];
	if($p=="0000-00-00 00:00:00") {
		$all[$id]['tkpwddate'] = "Never";
		if(in_array($id,$supers)) {
			if($pwd_age['super_user']>0) {
				$all[$id]['tkpwdnextdate'] = "Next login";
			} else {
				$all[$id]['tkpwdnextdate'] = "Never";
			}
		} else {
			if($pwd_age['normal_user']>0) {
				$all[$id]['tkpwdnextdate'] = "Next login";
			} else {
				$all[$id]['tkpwdnextdate'] = "Never";
			}
		}
	} else {
		$all[$id]['tkpwddate'] = date("d M Y",strtotime($p));
		if(in_array($id,$supers)) {
			if($pwd_age['super_user']>0) {
				$all[$id]['tkpwdnextdate'] = date("d M Y",strtotime($p)+($pwd_age['super_user']*24*60*60));
			} else {
				$all[$id]['tkpwdnextdate'] = "Never";
			}
		} else {
			if($pwd_age['normal_user']>0) {
				$all[$id]['tkpwdnextdate'] = date("d M Y",strtotime($p)+($pwd_age['normal_user']*24*60*60));
			} else {
				$all[$id]['tkpwdnextdate'] = "Never";
			}
		}
	}
}

?>
<h1>Reports >> Password Details</h1>
<table>
<thead>
    <tr>
        <th rowspan=1>ID</th>
        <th rowspan=1>First&nbsp;Name</th>
        <th rowspan=1>Surname</th>
        <th rowspan=1>Username</th>
        <th rowspan=1>Status</th>
        <th rowspan=1>Added&nbsp;On</th>
        <th rowspan=1>Last&nbsp;Login</th>
        <th rowspan=1>#&nbsp;of&nbsp;Logins</th>
        <th>Super<br />User</th>
        <th>Last password change</th>
        <th>Next password change</th>
    </tr>
</thead>
<tbody><?php
foreach($all as $id => $u) {
	
	echo "
    <tr>
        <td>".$id."</td>
        <td>".$u['tkname']."</td>
        <td>".$u['tksurname']."</td>
        <td>".$u['tkuser']."</td>
        <td class=center>".($u['tkstatus']==1 ? "Active" : "Inactive")."</td>
        <td class=center>".(is_numeric($u['tkadddate'])&&$u['tkadddate']>0 ? date("d-M-Y H:i",$u['tkadddate']) : "")."</td>
        <td class=center>".(is_numeric($u['tklogindate'])&&$u['tklogindate']>0 ? date("d-M-Y H:i",$u['tklogindate']) : "")."</td>
        <td class=center>".$u['tklogin']."</td>
        <td class=center>".(in_array($id,$supers) ? ASSIST_HELPER::getDisplayIconAsDiv("ok")." Yes" : ASSIST_HELPER::getDisplayIconAsDiv("error")." No")."</td>
        <td>".($u['tkpwddate'])."</td>
        <td>".($u['tkpwdnextdate'])."</td>
        ";
        
	echo "
    </tr>
	";
}
?></tbody>
</table>

</body>
</html>