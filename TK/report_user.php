<?php 
include("inc_header.php"); 

$db = new ASSIST_DB();

$sql = "SELECT * FROM assist_".$db->getCmpCode()."_timekeep WHERE tkid <> '0000' AND tkuser <> 'support' AND tkstatus > 0 AND tkstatus <> 2 ORDER BY tkname, tksurname";
$all = $db->mysql_fetch_all_by_id($sql,"tkid");

$sql = "SELECT * FROM assist_menu_modules WHERE modyn = 'Y' ORDER BY modtext";
$menu = $db->mysql_fetch_all_by_id($sql,"modref");
$menucount = count($menu);

$sql = "SELECT usrtkid, UPPER(usrmodref) as usrmodref FROM assist_".$db->getCmpCode()."_menu_modules_users ORDER BY usrtkid, usrmodref"; 
$access = $db->mysql_fetch_all_fld2($sql,"usrtkid","usrmodref");

//ASSIST_HELPER::arrPrint($access);
/*

$sql = "SELECT id, value FROM assist_".$db->getCmpCode()."_list_dept WHERE yn = 'Y'";
$dept = $db->mysql_fetch_value_by_id($sql,"id","value");

$sql = "SELECT id, value FROM assist_".$db->getCmpCode()."_list_loc WHERE yn = 'Y'";
$loc = $db->mysql_fetch_value_by_id($sql,"id","value");

$sql = "SELECT id, value FROM assist_".$db->getCmpCode()."_list_jobtitle WHERE yn = 'Y'";
$desig = $db->mysql_fetch_value_by_id($sql,"id","value");

//GENDER
	$gender = array('M'=>"Male",'F'=>"Female",'U'=>"Unknown");
//RACE
	$sql = "SELECT udfvcode, udfvvalue FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = 4 AND udfvyn <> 'N'";
	$race = $db->mysql_fetch_value_by_id($sql,"udfvcode","udfvvalue");

*/
?>
<h1>Reports >> User Details</h1>
<p><input type=button value="Export to Excel (plain text)" onclick="document.location.href = 'report_user_export.php';"/></p>
<table>
<thead>
    <tr>
        <th rowspan=2>ID</th>
        <th rowspan=2>Title</th>
        <th rowspan=2>First&nbsp;Name</th>
        <th rowspan=2>Surname</th><!--
        <th rowspan=2>ID&nbsp;Number</th>
        <th rowspan=2>Birth&nbsp;Date</th>
        <th rowspan=2>Job&nbsp;Title</th>
        <th rowspan=2>Department</th>
        <th rowspan=2>Location</th>
        <th rowspan=2>Manager</th>
        <th rowspan=2>Employee<br>Number</th>
        <th rowspan=2>Employment<br>Date</th> -->
        <th rowspan=2>Tel.<br>Prefix</th>
        <th rowspan=2>Telephone</th>
        <th rowspan=2>Mobile</th>
        <th rowspan=2>Fax</th>
        <th rowspan=2>Email</th>
        <!--<th rowspan=2>Gender</th>
        <th rowspan=2>Race</th>
        <th rowspan=2>Disabled?</th> -->
        <th rowspan=2>Username</th>
        <th rowspan=2>Added&nbsp;On</th>
        <th rowspan=2>Last&nbsp;Login</th>
        <th rowspan=2>#&nbsp;of&nbsp;Logins</th>
        <th rowspan=2>Status</th>
        <th colspan=<?php echo $menucount; ?>>Module Access</th>
    </tr>
    <tr>
    <?php
    foreach($menu as $key=>$m) {
        echo "<th>".$m['modtext']."</th>";
		$menu[$key]['count'] = 0;
    }
    ?>
    </tr>
</thead>
<tbody><?php
foreach($all as $id => $u) {
/*	if($u['tkmanager']=="S") {
		$u['tkmanager'] = "Self";
	} elseif(isset($all[$u['tkmanager']])) {
		$u['tkmanager'] = $all[$u['tkmanager']]['tkname']." ".$all[$u['tkmanager']]['tksurname'];
	} else {
		$u['tkmanager'] = "<span class='idelete'>Error</span>";
	}
	
	$tkempdate = "";
	if(ASSIST_HELPER::checkIntRef($u['tkempdate'])) {
		$tkempdate = date("d F Y",$u['tkempdate']);
		if($tkempdate=="01 January 1970") {
			$tkempdate = "";
		}
	} else {
		$tkempdate = strtotime($u['tkempdate']);
		if($tkempdate!=false) {
			$tkempdate = date("d F Y",$tkempdate);
		} else {
			$tkempdate = "";
		}
	}
*/
	$tktelph = isset($u['tktelph']) ? $u['tktelph'] : '';
	echo "
    <tr>
        <td>".$id."</td>
        <td>".$u['tktitle']."</td>
        <td>".$u['tkname']."</td>
        <td>".$u['tksurname']."</td>";/*
        <td>".$u['tkidnum']."</td>
        <td class=center>".(is_numeric($u['tkbdate']) ? date("d F Y",$u['tkbdate']) : "")."</td>
        <td>".( isset($desig[$u['tkdesig']]) ? $desig[$u['tkdesig']] : "<span class=idelete>Error</span>")."</td>
        <td>".( isset($dept[$u['tkdept']]) ? $dept[$u['tkdept']] : "<span class=idelete>Error</span>")."</td>
        <td>".( isset($loc[$u['tkloc']]) ? $loc[$u['tkloc']] : "<span class=idelete>Error</span>")."</td>
        <td>".$u['tkmanager']."</td>
        <td>".$u['tkempnum']."</td>
        <td>".($tkempdate)."</td>*/echo "
        <td>".$tktelph."</td>
        <td>".$u['tktel']."</td>
        <td>".$u['tkmobile']."</td>
        <td>".$u['tkfax']."</td>
        <td>".$u['tkemail']."</td>
        <td>".$u['tkuser']."</td>
        <td class=center>".(is_numeric($u['tkadddate'])&&$u['tkadddate']>0 ? date("d-M-Y H:i",$u['tkadddate']) : "")."</td>
        <td class=center>".(is_numeric($u['tklogindate'])&&$u['tklogindate']>0 ? date("d-M-Y H:i",$u['tklogindate']) : "")."</td>
        <td class=center>".$u['tklogin']."</td>
        <td class=center>".($u['tkstatus']==1 ? "Active" : "Inactive")."</td>";
        //<td>".( isset($gender[$u['tkgender']]) ? $gender[$u['tkgender']] : "<span class=idelete>Error</span>")."</td>
        //<td>".( isset($race[$u['tkrace']]) ? $race[$u['tkrace']] : "<span class=idelete>Error</span>")."</td>
        //<td>".($u['tkdisabled']=="Y" ? "Yes" : "No")."</td>
	foreach($menu as $key => $m) {
		echo "<td class=center>".( ( isset($access[$id]) && isset($access[$id][$key]) ) ? "Yes" : "" )."</td>";
		$menu[$key]['count'] += ( ( isset($access[$id]) && isset($access[$id][$key]) ) ? 1 : 0 );
	}
	echo "
    </tr>
	";
}
?></tbody>
<tfoot>
	<tr>
		<td colspan=14 class='right b'>Module License Count:</td>
		<?php 
		foreach($menu as $key => $m) {
			echo "<td class='center b'>".$m['count']."</td>";
		}
		?>
	</tr>
</tfoot>
</table>
<p>Report drawn on <?php echo date("d F Y H:i"); ?>.</p>
</body>
</html>