<?php
include("inc_header.php");

//error_reporting(-1);
/*** SETUP VARIABLES ***/
$obj_id = "";
$user = array();
$result = array();

//LICENSES
$sql = "SELECT modmenuid, moduserallowed, modid FROM assist_".$cmpcode."_menu_modules";
$mm = $helper->mysql_fetch_all_fld($sql, "modmenuid");
//MENU
$sql = "SELECT * FROM assist_menu_modules WHERE modyn = 'Y' OR modadminyn = 'Y' ORDER BY modtext";
$menu = $helper->mysql_fetch_all_fld($sql, "modref");
//arrPrint($_REQUEST);

/*** PROCESS FORM ***/
if(isset($_REQUEST['act']) && $_REQUEST['act'] == "CREATE") {
	$changes = array();
	$arr_sql = array();
	foreach($hsection as $s => $hs) {
		if($s != "sys") {
			foreach($headings[$s] as $fld => $h) {
				$old = isset($user[$fld]) ? $user[$fld] : "";
				switch($h['type']) {
					case "DATE":
						$old = $old * 1;
						$new = strtotime($_REQUEST[$fld]);
						$check = date("d M Y", $new) != date("d M Y", $old);
						$trans = "Updated ".$h['txt']." to \'".$_REQUEST[$fld]."\' from \'".date("d M Y", $old)."\'";
						break;
					default:
						$new = $helper->code($_REQUEST[$fld]);
						$check = $new != $old;
						$trans = "Updated ".$h['txt']." to \'".$new."\' from \'".$old."\'";
						break;
				}
				if($fld == "tkidnum" && strlen($new) == "") {
					$new = 0;
				}
				elseif($fld == "tkbdate" && strlen($new) == "") {
					$new = "X";
				}
				if($check) {
					$arr_sql[] = $fld." = '$new'";
				}
			}
		}
		else {
			foreach($headings[$s] as $fld => $h) {
				switch($fld) {
					case "tkid":
						$last = $helper->mysql_fetch_one("SELECT max(tkid) as maxt FROM assist_".$cmpcode."_timekeep");
						$obj_i = $last['maxt'] * 1;
						$obj_i++;
						$obj_id = "";
						if($obj_i < 1000) {
							$obj_id = "0";
						}
						if($obj_i < 100) {
							$obj_id .= "0";
						}
						if($obj_i < 10) {
							$obj_id .= "0";
						}
						$obj_id .= $obj_i;
						$arr_sql[] = $fld." = '".$obj_id."'";
						break;
					case "tklogindate":
						$arr_sql[] = $fld." = '0'";
						break;
					case "tkadddate":
					case "tkmoddate":
						$arr_sql[] = $fld." = '$today'";
						break;
				}
			}
		}
	}
//		arrPrint($changes);

	if(count($arr_sql) > 0) {
		$alpha = array("a",
					   "B",
					   "c",
					   "D",
					   "e",
					   "F",
					   "g",
					   "H",
					   "i",
					   "J",
					   "k",
					   "L",
					   "m",
					   "N",
					   "o",
					   "P",
					   "q",
					   "R",
					   "s",
					   "T",
					   "u",
					   "V",
					   "w",
					   "X",
					   "y",
					   "Z");
		$alphahex = array("61",
						  "42",
						  "63",
						  "44",
						  "65",
						  "46",
						  "67",
						  "48",
						  "69",
						  "4a",
						  "6b",
						  "4c",
						  "6d",
						  "4e",
						  "6f",
						  "50",
						  "71",
						  "52",
						  "73",
						  "54",
						  "75",
						  "56",
						  "77",
						  "58",
						  "79",
						  "5a");
		$tkpwd = "";
		$tkpwdhex = "";
		$i = 1;
		while($i < 8) {
			$p = rand(0, 25);
			if(strlen($alpha[$p]) != 0) {
				$tkpwd = $tkpwd.$alpha[$p];
				$tkpwdhex = $tkpwdhex.$alphahex[$p];
				$alpha[$p] = "";
				$i++;
			}
		}

		$extra_insert_array_stuff = array('tkempnum = \' \'',
										  'tkdesig = \' \'',
										  'tkdept = \' \'',
										  'tkloc = \' \'',
										  'tkmanager = \' \'',
										  'tkempdate = \' \'',
										  'tkgender = \' \'',
										  'tkrace = \' \'',
										  'tkdisabled = \' \'',
										  'tkpwdyn = \' \'',
										  'tkpwddate = \'\'',
										  'tktermdate = \' \'',
										  'tklogin = \'0\'',
										  'tkadduser = \' \'',
										  'tklock = \'0\'',
										  'tklocktime = \'\'',
										  'tkpwdanswer1 = \' \'',
										  'tkpwdanswer2 = \' \'',
										  'tkpwdanswer3 = \' \'',
										  'tkterms = \' \'',
										  'tktermsdate = \' \'',
										  'tkoccupcate = \'0\'',
										  'tkmoduser = \' \'',
										  'tkbillyn = \' \'');

		$arr_sql = array_merge($arr_sql, $extra_insert_array_stuff);

		$sql = "INSERT INTO assist_".$cmpcode."_timekeep SET ".implode(", ", $arr_sql).", tkpwd = '$tkpwdhex', tkloginwindow = '".date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") + 7, date("Y")))."'";
		//echo "<P>".$sql;

		$client_db = new ASSIST_MODULE_HELPER('client', $helper->getCmpCode());

		$client_db->db_insert($sql);
		//LOG CHANGES
		//foreach($changes as $c) {
		$c = array('fld' => "",
				   'old' => "",
				   'new' => "",
				   'txt' => "Created user.");
		$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'0000','Assist Administrator','DETAILS','".$obj_id."','C','','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($sql)."')";
		$client_db->db_insert($sql2);
		//MODULE ACCESS
		if(count($_REQUEST['mr']) > 0) {
			$msql = array();
			$m_txt = array();
			foreach($_REQUEST['mr'] as $mr) {
				$mi = $mm[$menu[$mr]['modid']]['modid'];
				$msql[] = "($mi,'".$obj_id."','$mr')";
				$m_txt[] = $menu[$mr]['modtext'];
			}
			$sql = "INSERT INTO assist_".$cmpcode."_menu_modules_users (usrmodid,usrtkid,usrmodref) VALUES ".implode(", ", $msql);
			$client_db->db_insert($sql);
			//echo "<P>".$sql;
			$c = array('fld' => "",
					   'old' => "",
					   'new' => "",
					   'txt' => "Granted user access to:".chr(10)." - ".implode(chr(10)." - ", $m_txt));
			$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
							(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
							VALUES
							(now(),'0000','Assist Administrator','DETAILS','".$obj_id."','C','','".$c['txt']."','".$c['old']."','".$c['new']."',1,'".base64_encode($sql)."')";
			$client_db->db_insert($sql2);
		}
		//echo "<p>".$sql2;
		//EMAIL USER LOGIN DETAILS
		if($_REQUEST['tkstatus'] == 1 && strlen($_REQUEST['tkemail']) > 0) {
			$cmpadmin = $helper->getCmpAdmin($cmpcode);
			$bpa_details = $helper->getBPAdetails();
			$bpa_con = explode("|", $bpa_details['help_contact']);
			$bpa_contact = array();
			foreach($bpa_con as $b) {
				$b2 = explode("_", $b);
				$bpa_contact[] = $b2[1];
			}

			$tku = $_REQUEST['tkuser'];
			$subject = "Welcome to ".$site_name;
			$message = "Dear ".$helper->decode(trim($_REQUEST['tkname'])." ".trim($_REQUEST['tksurname'])).chr(10);
			$message .= " ".chr(10);
			$message .= "We would like to welcome you to ".$site_name.". ".chr(10);
			$message .= " ".chr(10);
			$message .= "Your login details are: ".chr(10);
			$message .= "User name: ".($helper->decode($tku)).chr(10);
			$message .= "Company ID: ".strtoupper($cmpcode).chr(10);
			$message .= "Password: ".$tkpwd.chr(10);
			$message .= "Please keep your login details confidential. ".chr(10);
			$message .= " ".chr(10);
			$message .= "To login go to: http://assist.action4u.co.za/?".$site_code." ".chr(10);
			$message .= "Please login within 7 days or your account will be locked. ".chr(10);
			$message .= " ".chr(10);
			$message .= "For assistance please contact: ".chr(10);
			$message .= "- Your Assist Administrator, ".$cmpadmin['cmpadmin'].(strlen($cmpadmin['cmpadminemail']) > 0 ? ", at ".$cmpadmin['cmpadminemail'] : ",")." or ".chr(10);
//Removed Business Partner name to accommodate the separate reseller & support reseller [AA-534] JC
			$message .= "- Your ".$site_name." Business Partner at ".implode(" or ", $bpa_contact).". ".chr(10);
			$message .= " ".chr(10);
			$message .= "Kind regards ".chr(10);
//Removed Business Partner name to accommodate the separate reseller & support reseller [AA-534] JC
			$message .= "".$site_name." ".chr(10);
			//$message.= " ".chr(10);
			//$message.= "http://www.igniteassist.co.za".chr(10);

			$to = $_REQUEST['tkemail'];
			$from = $bpa_details['help_email'];
//				$header = "From: ".$bpa_details['cmpname']." <assist@ignite4u.co.za>\r\nReply-to: ".$bpa_details['cmpname']." <".$from.">\r\nCC: ".$from."\r\nBCC: webmaster@ignite4u.co.za";
//				$header = "From: ".$bpa_details['cmpname']." <no-reply@action4u.co.za>\r\nReply-to: ".$bpa_details['cmpname']." <".$from.">\r\nCC: ".$from."\r\nBCC: helpdesk@actionassist.co.za";
//				mail($to,$subject,$message,$header);
			$mail = new ASSIST_EMAIL($to, $subject, $message);
			$mail->setSender(array('name' => $bpa_details['cmpname'], 'email' => $from));
			$mail->setCC($from);
			$mail->setBCC("helpdesk_cc@actionassist.co.za");
			$mail->sendEmail();

		}
		$result = array("ok", "Created user '".$_REQUEST['tkname']." ".$_REQUEST['tksurname']."' [ID: ".$obj_id."].");
	}
	else {
		$result = array("error", "No user details were found.  Please try again.");
	}
}

echo "<script type=text/javascript>
						document.location.href = 'new.php?r[]=".$result[0]."&r[]=".urlencode($result[1])."';
						</script>";
$helper->displayResult($result);

?>