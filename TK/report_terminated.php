<?php 
include("inc_header.php"); 

$db = new TK_EMPLOYEE();
$emp_modref = strtolower($db->getEmpModRef());

$sql = "SELECT * 
FROM assist_".$db->getCmpCode()."_timekeep 
WHERE tkid <> '0000' AND tkuser <> 'support' AND tkstatus = 0 
AND tkid NOT IN (SELECT emp_tkid FROM assist_".$db->getCmpCode()."_".$emp_modref."_employee WHERE (emp_status & ".EMP1::NON_USER.") = ".EMP1::NON_USER.")
ORDER BY tkname, tksurname";
$all = $db->mysql_fetch_all_by_id($sql,"tkid");

$sql = "SELECT * FROM assist_menu_modules WHERE modyn = 'Y' ORDER BY modtext";
$menu = $db->mysql_fetch_all_by_id($sql,"modref");
$menucount = count($menu);

$sql = "SELECT usrtkid, UPPER(usrmodref) as usrmodref FROM assist_".$db->getCmpCode()."_menu_modules_users ORDER BY usrtkid, usrmodref"; 
$access = $db->mysql_fetch_all_fld2($sql,"usrtkid","usrmodref");

//ASSIST_HELPER::arrPrint($access);

$sql = "SELECT id, value FROM assist_".$db->getCmpCode()."_list_dept WHERE yn = 'Y'";
$dept = $db->mysql_fetch_value_by_id($sql,"id","value");

$sql = "SELECT id, value FROM assist_".$db->getCmpCode()."_list_loc WHERE yn = 'Y'";
$loc = $db->mysql_fetch_value_by_id($sql,"id","value");

$sql = "SELECT id, value FROM assist_".$db->getCmpCode()."_list_jobtitle WHERE yn = 'Y'";
$desig = $db->mysql_fetch_value_by_id($sql,"id","value");

//GENDER
	$gender = array('M'=>"Male",'F'=>"Female",'U'=>"Unknown");
//RACE
	$sql = "SELECT udfvcode, udfvvalue FROM assist_".$cmpcode."_udfvalue WHERE udfvindex = 4 AND udfvyn <> 'N'";
	$race = $db->mysql_fetch_value_by_id($sql,"udfvcode","udfvvalue");


?>
<h1>Reports >> Terminated Users Details</h1>
<table>
<thead>
    <tr>
        <th rowspan=2>ID</th>
        <th rowspan=2>Title</th>
        <th rowspan=2>First&nbsp;Name</th>
        <th rowspan=2>Surname</th>
        <th rowspan=2>Job&nbsp;Title</th>
        <th rowspan=2>Department</th>
        <th rowspan=2>Manager</th>
        <th rowspan=2>Employee<br>Number</th>
        <th rowspan=2>Employment<br>Date</th>
        <th>Added&nbsp;On</th>
        <th>Last&nbsp;Login</th>
        <th>#&nbsp;of&nbsp;Logins</th>
        <th>Terminated&nbsp;On</th>
    </tr>
</thead>
<tbody><?php
foreach($all as $id => $u) {
	if($u['tkmanager']=="S") {
		$u['tkmanager'] = "Self";
	} elseif(isset($all[$u['tkmanager']])) {
		$u['tkmanager'] = $all[$u['tkmanager']]['tkname']." ".$all[$u['tkmanager']]['tksurname'];
	} else {
		$u['tkmanager'] = $helper->getUnspecified();
	}
	echo "
    <tr>
        <td>".$id."</td>
        <td>".$u['tktitle']."</td>
        <td>".$u['tkname']."</td>
        <td>".$u['tksurname']."</td>
        <td>".( isset($desig[$u['tkdesig']]) ? $desig[$u['tkdesig']] : "<span class=idelete>Error</span>")."</td>
        <td>".( isset($dept[$u['tkdept']]) ? $dept[$u['tkdept']] : "<span class=idelete>Error</span>")."</td>
        <td>".$u['tkmanager']."</td>
        <td>".$u['tkempnum']."</td>
        <td>".(is_numeric($u['tkempdate']) ? date("d F Y",$u['tkempdate']) : "")."</td>
        <td class=center>".(is_numeric($u['tkadddate'])&&$u['tkadddate']>0 ? date("d-M-Y H:i",$u['tkadddate']) : "")."</td>
        <td class=center>".(is_numeric($u['tklogindate'])&&$u['tklogindate']>0 ? date("d-M-Y H:i",$u['tklogindate']) : "")."</td>
        <td class=center>".$u['tklogin']."</td>
        <td class=center>".(is_numeric($u['tktermdate'])&&$u['tktermdate']>0 ? date("d-M-Y H:i",$u['tktermdate']) : "")."</td>
    </tr>
	";
}
?></tbody>
</table>
<p>Report drawn on <?php echo date("d F Y H:i"); ?>.</p>
</body>
</html>