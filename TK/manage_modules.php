<?php
$inactive_users_modules = array("PMS","PMSPS");

$result = array();

if(isset($_REQUEST['act'])) {
	$ref = $_REQUEST['modref'];
	$is_performance_module = (in_array($ref,$inactive_users_modules) || substr($ref,-2,2)=="PM") ? true : false;
	$msql = "SELECT cc.modid, a.modtext FROM assist_".$cmpcode."_menu_modules cc INNER JOIN assist_menu_modules a ON a.modid = cc.modmenuid AND a.modref = '$ref'";
		$mod = $helper->mysql_fetch_all($msql);
		$mod = $mod[0];
	$modid = $mod['modid'];
	$modtxt = $mod['modtext'];
	$count = 0;
	switch($_REQUEST['act']) {
	case "INACTIVE":
	case "GRANT": //echo $ref;
		$sql = "SELECT DISTINCT tkid
				FROM assist_".$cmpcode."_timekeep
				WHERE tkid NOT IN (
					SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$ref'
				) AND ";
				if($_REQUEST['act']=="GRANT") {
					$sql.=($is_performance_module ? "tkstatus > 0" : "tkstatus = 1");
				} else {
					$sql.="tkstatus = 2";
				}
		//echo $sql;
		$users = $helper->mysql_fetch_fld_one($sql,"tkid");
		foreach($users as $tki) {
			//insert menu_modules_users record
			$sql1 = "INSERT INTO assist_".$cmpcode."_menu_modules_users (usrmodid, usrmodref, usrtkid) VALUES ($modid,'$ref','$tki')";
            $helper->db_insert($sql1);
			//insert timekeep_log record
			$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
					(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
					VALUES
					(now(),'0000','Ignite Assist Administrator','MENU','$tki','G','mmu','Granted access to $modtxt','','$ref',1,'".ASSIST_HELPER::code($sql1)."')";
            $helper->db_insert($sql2);
			$count++;
		}
		$uc = $helper->mysql_fetch_fld_one("SELECT count(usrtkid) as uc FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$ref'","uc");
		$uc = $uc[0];
        $helper->db_update("UPDATE assist_".$cmpcode."_menu_modules SET modusercount = $uc WHERE modid = $modid");
		if($count>0) {
			$result = array("ok",$count." users were added to ".$modtxt);
		} else {
			$result = array("info","No users were added to ".$modtxt);
		}
		break;
	case "REVOKE":
		$users = $helper->mysql_fetch_fld_one("SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep WHERE tkid IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$ref')","tkid");
		foreach($users as $tki) {
			//delete menu_modules_users record
			$sql1 = "DELETE FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$ref' AND usrtkid = '$tki'";
            $helper->db_update($sql1);
			//insert timekeep_log record
			$sql2 = "INSERT INTO assist_".$cmpcode."_timekeep_log
					(date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
					VALUES
					(now(),'0000','Ignite Assist Administrator','MENU','$tki','R','mmu','Revoked access to $modtxt','$ref','',1,'".ASSIST_HELPER::code($sql1)."')";
            $helper->db_insert($sql2);
			$count++;
		}
		$uc = $helper->mysql_fetch_fld_one("SELECT count(usrtkid) as uc FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$ref'","uc");
		$uc = $uc[0];
        $helper->db_update("UPDATE assist_".$cmpcode."_menu_modules SET modusercount = $uc WHERE modid = $modid");
		if($count>0) {
			$result = array("ok",$count." users were removed from ".$modtxt);
		} else {
			$result = array("info","No users were removed from ".$modtxt);
		}
		break;
	default:
		$result = array("error","An error occurred while trying to identify the intended action.  Please try again.");
	}
}

$sql = "SELECT modmenuid, moduserallowed FROM assist_".$cmpcode."_menu_modules";
$mm = $helper->mysql_fetch_all_fld($sql,"modmenuid");
?>
<h1>Manage >> Update Modules Access</h1>
<?php $helper->displayResult($result); ?>
<p>Please note the following:<ul>
	<li>Clicking on the <span class=isubmit>Grant</span> button will give all users access to the module.  This function is only available where the number of licenses remaining exceeds the number of users without access to the module.</li>
	<li>Clicking on the <span class=idelete>Revoke</span> button will remove all access to the module.  You can then grant user access using the Edit User function.</li>
	<li>WARNING: The above actions cannot be undone!</li>
	<li>Actions performed on this page will only grant/revoke the user's access to the module via the MAIN MENU.  None of the actions on this page will affect the module-specific user access, which must still be added/removed on the module's Setup page.</li>
</ul></p>
<table>
	<tr>
		<th>Module</th>
		<th>Licenses<br />Available</th>
		<th>Users<br /><span class=u>With</span><br />Access</th>
		<th>Licenses<br />Remaining</th>
		<th>Users<br /><span class=u>Without</span><br />Access</th>
		<th>Quick Actions</th>
	</tr>
<?php
$sql = "SELECT * FROM assist_menu_modules WHERE modyn = 'Y' OR modadminyn = 'Y' ORDER BY modtext";
$menu = $helper->mysql_fetch_all_fld($sql,"modref");
foreach($menu as $m) {
	$modref = $m['modref'];
	$is_performance_module = (in_array($modref,$inactive_users_modules) || substr($modref,-2,2)=="PM") ? true : false;
	$allowed = isset($mm[$m['modid']]['moduserallowed']) ? $mm[$m['modid']]['moduserallowed'] : 0;
	//$rs = getRS("SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep WHERE tkid <> '0000' AND tkuser <> 'support' AND tkid IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref') AND ".($is_performance_module ? "tkstatus > 0" : "tkstatus = 1"));
    $sql = "SELECT DISTINCT tkid FROM assist_".$cmpcode."_timekeep WHERE tkid <> '0000' AND tkuser <> 'support' AND tkid IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref') AND ".($is_performance_module ? "tkstatus > 0" : "tkstatus = 1");
	$access = $helper->db_get_num_rows($sql);
	$remain = $allowed - $access;
	$without_sql ="SELECT count(tkid) as count, tkstatus FROM assist_".$cmpcode."_timekeep WHERE tkid <> '0000' AND tkuser <> 'support' AND tkid NOT IN (SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = '$modref') AND ".($is_performance_module ? "tkstatus > 0" : "tkstatus = 1")." GROUP BY tkstatus";
	$all_without = $helper->mysql_fetch_all_fld($without_sql,"tkstatus");
    /**
     * Comment : To avoid  Undefined offset notice, the array_key_exists() method was used. this checks if the index indicated exists in the array.
     * This notice was spotted during standardisation. 05/March/2021
     * Author : Sondelani Dumalisile
     */
	$without = (isset($all_without[1]['count']) && array_key_exists(1,$all_without) ? $all_without[1]['count'] : 0) + (isset($all_without[2]['count']) && array_key_exists(2,$all_without) ? $all_without[2]['count'] : 0);
	$inactive_without =  (isset($all_without[2]['count']) && array_key_exists(2,$all_without) ? $all_without[2]['count'] : 0);

	if($allowed<0) { $allowed = "Unlimited"; $remain = "Unlimited"; }
	echo "	<tr>
				<td id=td_".$modref.">".$helper->decode($m['modtext'])."</td>
				<td class=center>".$allowed." licenses</td>
				<td class=center>".$access." users</td>
				<td class=center>".$remain." licenses</td>
				<td class=center>".$without." users</td>
				<td>
					".( ($without>0 && (!is_numeric($remain) || ($remain*1) > ($without*1))) ? "<input type=button class=isubmit value=\"Grant All\" id=".$m['modref']." act=GRANT />" : "").
					( ($inactive_without>0 && (!is_numeric($remain) || ($remain*1) > ($inactive_without*1))) && ($is_performance_module) ? "&nbsp;&nbsp;<input type=button class=isubmit value=\"Grant All Non-User Employees ($inactive_without)\" id=".$m['modref']." act=INACTIVE />" : "").
					($without>0?"&nbsp;&nbsp;":"").($access>0 ? "<input type=button class=idelete value=\"Revoke\" id=".$m['modref']." act=REVOKE />" : "").
				"</td>
			</tr>";
}
?>
</table>
<?php
	include("inc_debitorder.php");
	echo("<p>Click <a href=".$debitfile." target=_blank><b>here</b></a> to apply for more licences (updated: ".$debitdisplay.").</p>");
?>
<form id=frm_mod action=manage.php method=post>
<input type=hidden name=page value="modules" />
<input type=hidden name=act id=act value="" />
<input type=hidden name=modref id=mr value="" />
</form>
<script type=text/javascript>
$(document).ready(function(){
	$("tr").hover(
		function(){ $(this).addClass("trhover"); },
		function(){ $(this).removeClass("trhover"); }
	);
	$(".no-highlight").unbind('mouseenter mouseleave');
	$(".isubmit, .idelete").click(function() {
		var id = $(this).attr("id");
		if(id.length>0) {
			var txt = $("#td_"+id).html();
			if($(this).hasClass("isubmit")) {
				var act = $(this).attr("act");//"GRANT";
			} else if($(this).hasClass("idelete")) {
				var act = "REVOKE";
			} else {
				var act = "ERROR";
			}
			if(act == "ERROR") {
				alert("An error has occurred and your intended action could not be identified.\n\nPlease reload the page and try again.");
			} else {
				if(confirm("Are you sure you wish to "+act+" all access to "+txt+"?\n\nPlease note that this action will take some time.  Please be patient.")) {
					AssistHelper.processing();
					$("#frm_mod #act").val(act);
					$("#frm_mod #mr").val(id);
					//alert("submitting");
					$("#frm_mod").submit();
				}
			}
		} else {
			alert("An error has occurred and the module selected could be not identified.\n\nPlease reload the page and try again.");
		}
	});
});
</script>
<?php $helper->displayGoBack("main.php"); ?>