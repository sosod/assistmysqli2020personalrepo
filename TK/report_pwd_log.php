<?php
/**
 * @var string $hsection - from inc_headings.php
 * @var array $headings - from inc_headings.php
 *
 * HISTORY:
 * - AA-656: Modified to include secondary user name with Assist Administrator [JC] 23 July 2021
 */
require_once("../module/autoloader.php");
$me = new ASSIST_TK();
$me->displayPageHeaderJQ();
$reportObject = new TK_REPORT();
include("inc_headings.php");

?>
<style type="text/css" >
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>
<h1>Reports >> Password Activity Log Report</h1>
<p class="no-print"><input type=button value=Filter id=btn_filter /></p>
<?php

$sql = "SELECT MIN(`date`) as mindate FROM `assist_".$reportObject->getCmpCode()."_timekeep_log` L ";
$first_date = $reportObject->mysql_fetch_one_value($sql,"mindate");
if(strlen($first_date)==0 || strtotime($first_date)==0) {
	$min_date = date("Y").", ".date("m").", ".date("d"); 
} else {
	$min_date = date("Y",strtotime($first_date)).", ".date("m",strtotime($first_date)).", ".date("d",strtotime($first_date)); 
}



$logs = array();
$pwd_records = array();
$user_validation_logs = array();
$user_validation_logs_keys = array();
if(isset($_REQUEST['action']) && $_REQUEST['action']=="GENERATE") {
	$generate_report=true;
	// Start of #AA-656 changes part 1
	$from = $_REQUEST['from'];
	$to = $_REQUEST['to'];
	$sql = "SELECT * FROM assist_".$me->getCmpCode()."_timekeep WHERE tkuser = 'support'";
	$support = $me->mysql_fetch_one($sql);
	$super_user_sql = "(A.tkid = '0000' OR A.tkid = '".$support['tkid']."')"; //need to fetch all records to accommodate match between activity_log and activity_details records;
	$modules = array();//module details don't matter for this report
	$raw_user_validation_logs = $reportObject->getSuperUserActivityLogs($from,$to,$super_user_sql,"",$modules);
	//now convert array key to timestamp to facilitate matching below & drop unnecessary data / user logs
	$user_validation_logs = array();
	foreach($raw_user_validation_logs as $raw_log) {
		if($raw_log['tkid']=="0000" && strlen($raw_log['user'])>0) {
			$logout_time = false;//assume timeout
			$a = $raw_log['activity'];
			$k = array_keys($a);
			$last_key = $k[count($k)-1];
			$last_action = $a[$last_key];
			if(strtolower(trim($last_action))=="logout"){
				$logout_time = $last_key;
			}
			$user_validation_logs[$raw_log['date']] = array('user'=>$raw_log['user'],'to'=>$logout_time,'last_action'=>$last_action);
		}
	}
	$user_validation_logs_keys = array_keys($user_validation_logs);
	//End of #AA-656 changes part 1

	if(isset($_REQUEST['log_user']) || isset($_REQUEST['log_admin'])){
		$sql_date = "";
		if(strlen($_REQUEST['from'])>0 || strlen($_REQUEST['to'])>0) {
			$s = array();
			if(strlen($_REQUEST['from'])>0) {
				$s[] = " `date` >= CAST('".date("Y-m-d",strtotime($_REQUEST['from']))." 00:00:00' as DATETIME) ";
			}
			if(strlen($_REQUEST['to'])>0) {
				$s[] = " `date` <= CAST('".date("Y-m-d",strtotime($_REQUEST['to']))." 23:59:59' as DATETIME) ";
			}
			$sql_date = implode(" AND ",$s);
		}
		$where = array();
		if(isset($_REQUEST['log_user']) && $_REQUEST['log_user']=="USER") {
			$where[] = "(L.tkid <> '0000')";
		}
		if(isset($_REQUEST['log_admin']) && $_REQUEST['log_admin']=="ADMIN") {
			$where[] = "(L.tkid = '0000')";
		}
		
		$sql = "SELECT id,`date`, section, action,transaction, L.tkid, CONCAT(TK.tkname,' ',tksurname) as name, L.ref, TK.tkid as user_ref
				FROM `assist_".$reportObject->getCmpCode()."_timekeep_log` L 
				INNER JOIN assist_".$reportObject->getCmpCode()."_timekeep TK 
					ON TK.tkid = ref
				WHERE TK.tkuser <> 'support' AND L.ref <> 0
				AND L.transaction LIKE '%password%'
				AND L.transaction NOT LIKE '%first time%'
				AND L.transaction NOT LIKE '%unlocked user%'
				".(count($where)>0 ? " AND (".implode(" OR ",$where).")" : "")."
				AND ".($sql_date)."
				ORDER BY `date`"; //echo $sql;
		$logs = $reportObject->mysql_fetch_all_by_id($sql,"id");
		
		$sql = "SELECT id, acting_date, acting_reason, user_tid, user_was_pwd_locked, user_is_super
				, admin_super_user_details, admin_request_src, admin_validated
				, admin_validated_against, admin_unlock_user, admin_notify_user
				, admin_force_reset FROM assist_".$reportObject->getCmpCode()."_timekeep_log_password ".(strlen($sql_date)>0?" WHERE ".str_replace("`date`","`acting_date`",$sql_date):"");
		$pwd_records = $reportObject->mysql_fetch_all_by_id2($sql,"user_tid","acting_date");
		
		$password_change_request_sources = $me->getPasswordChangeRequestSources();
		
		$password_validation_sources = array();
		foreach($hsection as $sec => $h) {
			foreach($headings[$sec] as $fld => $head) {
				$password_validation_sources[$fld] = $head['txt'];
			}
		}
		
	}
} else {
	$generate_report=false;
}
//ASSIST_HELPER::arrPrint($logs);
//ASSIST_HELPER::arrPrint($pwd_records);

?>
<table id=tbl_log>
	<tr>
		<th>Date Performed</th>
		<th>Performed By</th>
		<th>User Affected</th>
		<th>Activity Log</th>
		<th>Source of Request</th>
		<th>User Identity Validated?</th>
		<th>Validation Source</th>
		<th>User Locked Out?</th>
		<th>User Unlocked</th>
		<th>User Notified</th>
		<th>User Forced to Reset</th>
	</tr>
	<?php
	if(count($logs)>0) {
		$uvlk_count = 0;
		$uvlk_test = 1;
		$uvl_current = $user_validation_logs[$user_validation_logs_keys[$uvlk_count]];
		$tr = false;
		foreach($logs as $l) {
			$tr = false;
			//$rowspan = count($u);
			//foreach($u as $l) {
				$lt = $l['transaction'];
				if(substr($lt,0,16)=="Password changed") {
					$lt = substr($lt,18,strlen($lt));
				}
				$extra = array();
				if($l['action']=="R") {
					$d = $l['date'];
					$uid = $l['user_ref'];
					if(isset($pwd_records[$uid][$d])) {
						$extra = $pwd_records[$uid][$d];
					} else {
						$d2 = strtotime($d);
						$c = 1;
						while($c<=10) {
							$dx = date("Y-m-d H:i:s",$d2+$c);
							if(isset($pwd_records[$uid][$dx])) {
								$extra = $pwd_records[$uid][$dx];
								$c = 11;
							} else {
								$c++;
							}
						}
					}
					if(count($extra)>0) {
						$extra['admin_request_src'] = str_replace("_","",$extra['admin_request_src']);
						if(isset($password_change_request_sources[$extra['admin_request_src']])) {
							$extra['admin_request_src'] = $password_change_request_sources[$extra['admin_request_src']];
						} else {
							$extra['admin_request_src'] = $me->getUnspecified();
						}
						if($extra['admin_validated']==1 && isset($password_validation_sources[$extra['admin_validated_against']])) {
							$extra['admin_validated_against'] = $password_validation_sources[$extra['admin_validated_against']];
						} elseif($extra['admin_validated']==0) {
							$extra['admin_validated_against'] = "N/A";
						} else {
							$extra['admin_validated_against'] = $me->getUnspecified();
						}
						$extra['admin_validated'] = ($extra['admin_validated']==1?"Yes":"No");
						$extra['user_was_pwd_locked'] = ($extra['user_was_pwd_locked']==1?"Yes":"No");
						$extra['admin_unlock_user'] = ($extra['admin_unlock_user']==1?"Yes":"No");
						$extra['admin_notify_user'] = ($extra['admin_notify_user']==1?"Yes":"No");
						$extra['admin_force_reset'] = ($extra['admin_force_reset']==1?"Yes":"No");
					} else {
						$extra = array(
							'admin_request_src'=>$me->getUnspecified(),
							'admin_validated'=>"No",
							'admin_validated_against'=>"N/A",
							'user_was_pwd_locked'=>"N/A",
							'admin_unlock_user'=>"N/A",
							'admin_notify_user'=>"N/A",
							'admin_force_reset'=>"N/A",
						);
					}
				} else {
					$extra = array(
						'admin_request_src'=>"-",
						'admin_validated'=>"-",
						'admin_validated_against'=>"-",
						'user_was_pwd_locked'=>"-",
						'admin_unlock_user'=>"-",
						'admin_notify_user'=>"-",
						'admin_force_reset'=>"-",
					);
				}
				//user name for display #AA-656 changes part 2
				if($l['tkid']=="0000") {
					$user_name_for_display = "Assist Administrator";
					$log_action_date = strtotime($l['date']);
					if(isset($user_validation_logs_keys[$uvlk_test])) {
						//if log date is less than (<) the test date (next user_validation_log time) then assume current uvl user is the correct one
						if($log_action_date<$user_validation_logs_keys[$uvlk_test]) {
							$user_name_for_display.= " [".$uvl_current['user']."]";
						} else {
							//other wise test for next
							$uvlk_test++;
							$uvlk_count++;
							while(isset($user_validation_logs_keys[$uvlk_test]) && $log_action_date>$user_validation_logs_keys[$uvlk_test]) {
								$uvlk_test++;
								$uvlk_count++;
							}
							$uvl_current = $user_validation_logs[$user_validation_logs_keys[$uvlk_count]];
							$user_name_for_display.= " [".$uvl_current['user']."]";
						}
					}
				} else {
					$user_name_for_display = $l['name'];
				}
				//end of #AA-656 changes part 2
				echo "
				<tr>
					<td>".date("d-M-Y H:i:s",strtotime($l['date']))."</td>
					<td>".$user_name_for_display."</td>
					<td>".$l['name']."</td>
					<td>".str_replace(chr(10),"<br />",$lt)."</td>
					<td class=center>".$extra['admin_request_src']."</td>
					<td class=center>".$extra['admin_validated']."</td>
					<td class=center>".$extra['admin_validated_against']."</td>
					<td class=center>".$extra['user_was_pwd_locked']."</td>
					<td class=center>".$extra['admin_unlock_user']."</td>
					<td class=center>".$extra['admin_notify_user']."</td>
					<td class=center>".$extra['admin_force_reset']."</td>
				</tr>";
			//}
		}
	} elseif($generate_report) {
		echo "
		<tr>
			<td colspan=11>No logs were found for the filter settings you selected.</td>
		</tr>";
	} else {
		echo "
		<tr>
			<td colspan=11>Please apply a filter in order to view the Activity Logs.</td>
		</tr>";
	}
	?>
</table>
<p class=i>Report drawn on <?php echo date("d F Y H:i:s"); ?>.</p>
<div id=div_filter title=Filter>
	<form name=frm_filter>
		<input type="hidden" name=action value=GENERATE />
		<table id=tbl_filter class=form width="400px">
			<tr>
				<th>Type of Log:</th>
				<td>
					<input type=checkbox name=log_user value=USER checked /> User changes<br />
					<input type=checkbox name=log_admin value=ADMIN checked /> Administrator forced changes
				</td>
			</tr>
			<tr>
				<th>Date:</th>
				<td>From <input type=text class="datepicker" size=12 name=from /> to <input type=text name=to class="datepicker" value='<?php echo date("d-M-Y"); ?>' size=12 /></td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript" >
$(function(){
	$("#div_filter").dialog({
		modal:true,
		width:"450px",
		autoOpen:<?php echo ($generate_report ? "false" : "true"); ?>,
		buttons:[{
			text:"Apply",
			click:function(){
				document.location.href = 'report_pwd_log.php?'+AssistForm.serialize($("form[name=frm_filter]"));
				$(this).dialog("close");
			}
		}]
	}).dialog("widget").find(".ui-dialog-buttonpane button").eq(0).css(AssistHelper.getGreenCSS()).end();
	$("#div_filter .datepicker").datepicker({
        showOn: 'both',
        buttonImage: '/library/jquery/css/calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'dd-M-yy',
        changeMonth:true,
        changeYear:true,
        maxDate: 0,
        minDate: new Date(<?php echo $min_date; ?>)		
	});
	$("#btn_filter").button().css(AssistHelper.getGreenCSS()).css("font-size","80%").click(function(){
		$("#div_filter").dialog("open");
		$("#div_filter input").blur();
		
	});
});
</script>
</body>

</html>
