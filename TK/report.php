<?php

include("inc_header.php");

$reportObject = new TK_REPORT();
$reportObject->drawPageTop("report");
?>
<table width=600>
    <!-- <tr>
        <th>Report Generator:</th><td>Custom report generator. <span class=float><input type=button value=" Go " id="report_generate_user"></span></td>
    </tr> -->
    <tr>
        <th>Users:</th><td>User detail report <span class=float><input type=button value=" View " id="report_user"></span></td>
    </tr>
    <tr>
        <th>Users:</th><td>User detail report (Microsoft Excel, plain text) <span class=float><input type=button value=" View " id="report_user_export"></span></td>
    </tr>
    <tr>
        <th>Terminated Users:</th><td>Terminated users detail report <span class=float><input type=button value=" View " id="report_terminated"></span></td>
    </tr>
    <tr>
        <th>Modules:</th><td>Module Licences Report <span class=float><input type=button value=" View " id="module_report"></span></td>
    </tr>
    <tr>
        <th>User Details Log:</th><td>Log Report of all changes made to user details and module access. <span class=float><input type=button value=" View " id="auditlog_report"></span></td>
    </tr>
    <tr>
        <th>Access Attempt Log:</th><td>Log Report of Login attempts. <span class=float><input type=button value=" View " id="access_report"></span></td>
    </tr>
    <tr>
        <th>Super User Activity Log:</th><td>Log Report of Super User login activity and movements within the Assist system. <span class=float><input type=button value=" View " id="report_super"></span></td>
    </tr>
    <tr>
        <th>User Activity Log:</th><td>Log Report of User login activity and movements within the Assist system. <span class=float><input type=button value=" View " id="report_user_activity"></span></td>
    </tr>
    <tr>
        <th>Password Status:</th><td>Password detail report. <span class=float><input type=button value=" View " id="report_user_pwd"></span></td>
    </tr>
    <tr>
        <th>Password Activity Log:</th><td>Log report of password changes. <span class=float><input type=button value=" View " id="report_pwd_log"></span></td>
    </tr>
</table>
<script type=text/javascript>
$(document).ready(function() {
	$("table th").addClass("left");
	
	$("input:button").click(function() {
		AssistHelper.processing();
		var i = $(this).prop("id");
		document.location.href = i+".php";
		if(i=="report_user_export") {
			AssistHelper.closeProcessing();
		}
	});
});
</script>
</body>
</html>