<?php
/**
 * Class TK_REPORT
 * Created: JC AA-390 31 May 2020
 */


class TK_REPORT extends TK {

//	private $emp_fields_not_allowed = array("job_level","job_skill","job_levelTwo","job_description","job_manager","job_start","job_end","job_score");
	private $emp_job_fields_allowed_for_report = array("job_loc", "job_dept", "job_title", "job_acting", "job_manager_title", "job_team");
	private $emp_sections_allowed_for_report = array("USER","EMPLOYEE","JOB");

	private $list_types = array("LIST","MASTER","USER_STATUS","EMP_STATUS");
	private $text_types = array("TEXT","NUM","VC","MEDVC","SMLVC","MENU_MODULE");
	private $date_types = array("DATE","DATETIME");

	private $emp_table_name = "emp";

	public function __construct() {
		parent::__construct();
		$this->emp_table_name = "assist_".$this->getCmpCode()."_".$this->emp_table_name;
	}

	public function getIDFieldName()  {return "tkid";}
	public function getRefTag() { return ""; }

	public function getHeadings() {

		$sql = "SELECT * 
				FROM ".$this->emp_table_name."_setup_headings 
				WHERE (h_status & ".EMP1::ACTIVE.") = ".EMP1::ACTIVE." 
				AND h_type <> 'HEAD'
				ORDER BY h_section, h_list_order";
		$rows = $this->mysql_fetch_all_by_id2($sql,"h_section","h_field");

		$headings = array();
		$headings['tkid'] = array('h_section' => "USER",
									 'h_default' => "User ID",
									 'h_client'  => "User ID",
									 'h_field'   => "tkid",
									 'h_parent_id'=>0,
									 'h_type'    => "VC",);

		foreach($this->emp_sections_allowed_for_report as $key) {
			foreach($rows[$key] as $fld => $row) {
				if($key!="JOB" || in_array($fld,$this->emp_job_fields_allowed_for_report)) {
					$headings[$fld] = $row;
				}
			}
		}

		/** Add Extra User-specific fields */
		//Username
		$headings['tkuser'] = array('h_section' => "USER",
									 'h_default' => "Username",
									 'h_client'  => "Username",
									 'h_field'   => "tkuser",
									 'h_parent_id'=>0,
									 'h_type'    => "VC",);
		//Added on
		$headings['tkadddate'] = array('h_section' => "USER",
									 'h_default' => "Added On",
									 'h_client'  => "Added On",
									 'h_field'   => "tkadddate",
									 'h_parent_id'=>0,
									 'h_type'    => "DATETIME",);
		//Last Login
		$headings['tklogindate'] = array('h_section' => "USER",
									 'h_default' => "Last Login",
									 'h_client'  => "Last Login",
									 'h_field'   => "tklogindate",
									 'h_parent_id'=>0,
									 'h_type'    => "DATETIME",);
		//# Of Logins
		$headings['tklogin'] = array('h_section' => "USER",
									 'h_default' => "# Of Logins (All Time)",
									 'h_client'  => "# Of Logins (All Time)",
									 'h_field'   => "tklogin",
									 'h_parent_id'=>0,
									 'h_type'    => "NUM",);
		//# Of Logins
		$headings['tklogin_days'] = array('h_section' => "USER",
									 'h_default' => "# Of Logins (Past 30 Days)",
									 'h_client'  => "# Of Logins (Past 30 Days)",
									 'h_field'   => "tklogin_days",
									 'h_parent_id'=>0,
									 'h_type'    => "NUM",);
		//Module Access
		$headings['module_access'] = array('h_section' => "USER",
									 'h_default' => "Module Access",
									 'h_client'  => "Module Access",
									 'h_field'   => "module_access",
									 'h_parent_id'=>0,
									 'h_type'    => "MENU_MODULE",);

		foreach($headings as $fld => $head) {
			foreach($head as $key => $value) {
				$x = explode("_",$key);
				unset($x[0]);
				$key = implode("_",$x);
				$headings[$fld][$key] = $value;
			}
		}

		return $headings;
	}

	public function isTextField($type) {
		return in_array($type,$this->text_types);
	}

	public function isListField($list_type) {
		return in_array($list_type,$this->list_types);
	}

	public function isDateField($type) {
		return in_array($type,$this->date_types);
	}

	public function getListItemsForReport($list_table) {
		//TODO: go this way later when EMP1 module available again - for now, hardcode the SQL call [JC - AA-390 - 31 May 2020]
		//$listObject = new EMP1_LIST($list_table,"EMP");
		//$items = $listObject->getItemsForReport();

		//don't filter for only ACTIVE items but only avoid DELETED items - INACTIVE items might have been used before being deactivated, DELETED items can't have been used
		$sql = "SELECT * 
				FROM ".$this->emp_table_name."_".(strpos($list_table,"list")===false?"list_":"").$list_table." 
				WHERE (status & ".EMP1::DELETED.") <> ".EMP1::DELETED." 
				ORDER BY name";
		$items = $this->mysql_fetch_value_by_id($sql,"id","name");

		//leave room for processing here
//		$items[0]=$this->getUnspecified();
		return $items;

	}

	public function getMasterItemsForReport($list_table) {
		switch($list_table) {
			case "ppltitle":
				$listObject = new ASSIST_MASTER_PPLTITLE();
				$items = $listObject->getAllItemsFormattedForSelect();
				break;
			default:
				$items = array();
				break;
		}
		return $items;
	}


	//TODO: Link this to EMP1_EMPLOYEE->getUserStatusOptions at some point [JC - AA-390 - 31 May 2020]
	public function getUserStatusOptionsFormattedForSelect() {
		$items = array(
			1 => "Active Assist User",
			2 => "Non-Assist User",
			0 => "Terminated User"
		);
		return $items;
	}

	//TODO: Link this to EMP1_EMPLOYEE->getEmployeeStatusOptions at some point [JC - AA-390 - 31 May 2020]
	public function getEmployeeStatusOptionsFormattedForSelect() {
		$items = array(
			EMP1::ACTIVE => "Active Employee",
			EMP1::INACTIVE=> "Terminated Employee",
			0=>"User Only (non-Employee)",
		);
		return $items;
	}



	public function drawPageTop($page) {

	/*** LEVEL 1 NAVIGATION :: PAGE ***/
		$level = 1;
		$nav = array(
			'report_generate_user'=>"Generator",
			'report_quick'=>"Quick",
			'report'=>"Fixed",
			//'login'=>"New User Login Window",
		);
		$menu = array();
		foreach($nav as $id => $d) {
			$menu[$id] = array('id'=>$id,'link'=>"/TK/".$id.".php",'active'=>($page==$id ? true : false),'name'=>$d,'help'=>"");
		}
		echo $this->generateNavigationButtons($menu,$level);



		echo "<h1>Report ".ASSIST_MODULE_HELPER::BREADCRUMB_DIVIDER." ".$nav[$page]."</h1>";

	}


	/**
	 * Added to manage both Super User Activity & Password Change Activity reports #AA-656 [JC] 23 July 2021
	 * @param string $from - date in string format, to limit the records retrieved
	 * @param string $to - date in string form, to limit the records retrieved
	 * @param string $extra_user_where - extra db filtering
	 * @param string $extra_super_where - extra db filtering
	 * @param array $modules - list of modules for processing modrefs to modnames for display - report_super.php
	 * @return array
	 */
	public function getSuperUserActivityLogs($from="",$to="",$extra_user_where="",$extra_super_where="",$modules=array()) {
		$mdb = new ASSIST_DB("master");
		$sql = "SELECT r.cmpcode, c.cmpname FROM `assist_reseller` r INNER JOIN assist_company c ON c.cmpcode = r.cmpcode";
		$resellers = $mdb->mysql_fetch_value_by_id($sql, "cmpcode", "cmpname");
		unset($mdb);
		if(strlen($from) > 0) {
			if(strlen($to) > 0) {
				$from .= " 00:00:00";
				$to .= " 23:59:59";
			} else {
				$to = $from." 23:59:59";
				$from .= " 00:00:00";
			}
			$from = date("Y-m-d H:i:s", strtotime($from));
			$to = date("Y-m-d H:i:s", strtotime($to));
			$super_date_sql = (strlen($extra_super_where)>0?" AND ":"")." date > '$from' AND date < '$to'";
			$user_date_sql = (strlen($extra_user_where)>0?" AND ":"")." date > '$from' AND date < '$to'";
		} else {
			if(strlen($to) > 0) {
				$from = $to." 00:00:00";
				$to .= " 23:59:59";
				$from = date("Y-m-d H:i:s", strtotime($from));
				$to = date("Y-m-d H:i:s", strtotime($to));
				$super_date_sql = (strlen($extra_super_where)>0?" AND ":"")." date > '$from' AND date < '$to'";
				$user_date_sql = (strlen($extra_user_where)>0?" AND ":"")." date > '$from' AND date < '$to'";
			} else {
				$user_date_sql = "";
				$super_date_sql = (strlen($extra_super_where)>0?" AND ":"")." date > '2016-03-06 00:00:00'";
			}
		}

		$sql = "SELECT A.* 
		FROM assist_".$this->getCmpCode()."_timekeep_activity A
		WHERE $extra_user_where
		$user_date_sql
		ORDER BY date ASC";
		//echo $sql;
		$activity = $this->mysql_fetch_all_by_id($sql, "id");
		$acts = array();
		foreach($activity as $sk => $s) {
			$sd = strtotime($s['date']);
			$acts[$sd] = $s;
		}


		$sql = "SELECT A.* 
		FROM assist_".$this->getCmpCode()."_timekeep_activity_superusers A
		WHERE $extra_super_where
		$super_date_sql  
		ORDER BY date ASC";
		$super_activity = $this->mysql_fetch_all_by_id($sql, "id");
		$supers = array();
		foreach($super_activity as $sk => $s) {
			$sd = strtotime($s['date']);
			$supers[$sd] = $s;
		}

		$full = $supers + $acts;
		ksort($full);

//remove any log oddities that sort before a login attempt
		foreach($full as $key => $f) {
			if(substr($f['action'], 0, 2) != "IN") {
				unset($full[$key]);
			} else {
				break;
			}
		}
		$logs = array();
		$c = 0;
		$has_logout = true;
		$failed_login = false;
		if(count($full)>0) {
			$keys = array_keys($full);
//foreach($full as $a) {
		for($i = 0; $i < count($keys); $i++) {
			$a = $full[$keys[$i]];
			if(substr($a['action'], 0, 2) == "IN" && !isset($a['cmpcode'])) {
				if(!$has_logout && !$failed_login) {
					$current_time = strtotime($a['date']);
					$expired_time = strtotime($full[$keys[$i - 1]]['date']) + 3600;
					$logs[$c]['activity'][($current_time < $expired_time ? $current_time - 10 : $expired_time)] = "Logout: Session expired";

					//$logs[$c]['activity'][strtotime($a['date'])] = "Logout: Session expired";
				} else {
					$has_logout = false;
				}
				$c++;
				if($a['action'] == "IN") {
					$failed_login = false;
					$d = strtotime($a['date']);
					$logs[$c] = array(
						'tkid' => $a['tkid'],
						'date' => $d,
						'super_user' => $a['tkname'],
						'user' => "",
						'activity' => array(
							strtotime($a['date']) => "Login"
						),
					);
					if($d > strtotime("6 March 2016 00:00:01")) {
						//$x = next($full);
						$in2 = false;
						do {
							$i++;
							$x = $full[$keys[$i]];
							$hcc = isset($x['cmpcode']);
							if(!$hcc) {
								$logs[$c]['user'] = strlen($logs[$c]['user']) == 0 ? "Unknown" : $logs[$c]['user'];
								$i--;
							} elseif(!$in2) {
								$logs[$c]['user'] = $x['tkname'];
								if(strtoupper($x['cmpcode']) != strtoupper($this->getCmpCode())) {
									$logs[$c]['user'] .= " ("
										.(isset($resellers[strtoupper($x['cmpcode'])]) ? $resellers[strtoupper($x['cmpcode'])] : strtoupper($x['cmpcode']))
										.")";
								}
								if($x['action'] == "IN") {
									$in2 = true;
									$logs[$c]['activity'][strtotime($x['date'])] = "Second user validation successful";
								} elseif($x['action'] == "IN_FAIL_PWD") {
									$logs[$c]['activity'][strtotime($x['date'])] = "Second user validation failed (password error)";
								} elseif($x['action'] == "IN_FAIL_PERM") {
									$logs[$c]['activity'][strtotime($x['date'])] = "Second user validation failed (permission error)";
								} else {
									$logs[$c]['activity'][strtotime($x['date'])] = $x['action'];
								}
							}
						} while($hcc);
					} else {
						$logs[$c]['user'] = "N/A";
					}
				} else {
					$logs[$c] = array(
						'tkid' => $a['tkid'],
						'date' => strtotime($a['date']),
						'super_user' => $a['tkname'],
						'user' => "",
						'activity' => array(
							strtotime($a['date']) => "Failed Login attempt"
						),
					);
					$failed_login = true;
				}
			} elseif(substr($a['action'], 0, 3) == "OUT") {
				$prev = (isset($keys[$i - 1]) && isset($full[$keys[$i - 1]])) ? $full[$keys[$i - 1]] : array();
				$next = (isset($keys[$i + 1]) && isset($full[$keys[$i + 1]])) ? $full[$keys[$i + 1]] : array();
				if(!(isset($prev['cmpcode']) && $prev['action'] == "IN_FAIL_PWD" && isset($next['cmpcode']) && substr($next['action'], 0, 2) == "IN")) {
					$y = "";
					if(strlen($a['action']) > 3) {
						$y = substr($a['action'], 4, strlen($a['action']));
						$y0 = $y;
						if($y == "TIME" || $y == "TIMEOUT" || $y == "SUPER_FAIL") {
						} else {
							$y = base64_decode($y);
						}
						if($y == "TIME" || $y == "TIMEOUT") {
							$y = ": Timeout";
						} elseif($y == "SUPER_FAIL") {
							$y = ": Super user validation failed";
						} else {
							$y = ": ".$y;
						}
					}
					$logs[$c]['activity'][strtotime($a['date'])] = "Logout".$y."";
					$has_logout = true;
				}
			} elseif(substr($a['action'], 0, 2) == "IN" && isset($a['cmpcode'])) {
				if($a['action'] == "IN") {
					$in2 = true;
					$logs[$c]['activity'][strtotime($a['date'])] = "Second user validation successful";
				} elseif($a['action'] == "IN_FAIL_PWD") {
					$logs[$c]['activity'][strtotime($a['date'])] = "Second user validation failed (password error)";
				} elseif($a['action'] == "IN_FAIL_PERM") {
					$logs[$c]['activity'][strtotime($a['date'])] = "Second user validation failed (permission error)";
				} else {
					$logs[$c]['activity'][strtotime($a['date'])] = $a['action'];
				}
			} else {
				$z = strtoupper($a['action']);
				$logs[$c]['activity'][strtotime($a['date'])] = (isset($modules[$z]) ? $modules[$z] : $z)."";
			}
		}
		}

		if(isset($a) && substr($a['action'], 0, 3) !== "OUT") {
			$current_time = strtotime(date("d F Y H:i:s"));
			$expired_time = strtotime($a['date']) + 3600;
			if($current_time > $expired_time) {
				$logs[$c]['activity'][$expired_time] = "Logout: Session expired";
			}
		}

		return $logs;
	}







}
?>