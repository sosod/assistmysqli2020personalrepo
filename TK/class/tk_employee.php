<?php
/**
 * Class TK_EMPLOYEE to manage links between TK & EMP1 module
 * Created by: JC
 * AA-647 14 July 2021
 */

class TK_EMPLOYEE extends TK {


	private $employee_modref = "EMP";
	private $employee_modref_confirmed = false;

	public function __construct() {
		parent::__construct();
		//Set the Employee Assist modref - to catch clients with EMP1 instead of EMP as the modref #AA-644 JC 9 July 2021
		$is_there_a_session_variable_about_employee_modref = isset($_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']);

		if($is_there_a_session_variable_about_employee_modref) {
			$this->employee_modref_confirmed = $_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref_confirmed'];
			$this->employee_modref = $_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref'];
		} else {
			//Look in the menu table for EMP & EMP1 and get the oldest record
			$sql = "SELECT * FROM assist_menu_modules WHERE modref IN ('EMP','EMP1') AND modyn = 'Y' ORDER BY modid DESC";
			$row = $this->mysql_fetch_one($sql);
			if(isset($row['modref'])) {
				$this->employee_modref = $row['modref'];
				$this->employee_modref_confirmed = true;
				$_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref'] = $this->employee_modref;
				$_SESSION[$this->getModRef()]['EMPLOYEE']['MODULE_SETTINGS']['modref_confirmed'] = $this->employee_modref_confirmed;
			}
		}
	}


	public function getEmpModRef() {
		return $this->employee_modref;
	}

}

?>