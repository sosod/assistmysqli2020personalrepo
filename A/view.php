<html>
<head>
<title>Ignite4U.co.za</title>
<meta name="description" content="ignite advisory services, ignite4u">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/default.css" type="text/css">
</head>
<body>
<?php
$appid = $_GET['a'];
                        $months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
?>
<h1>Associates: On-line Application (<?php echo($appid); ?>)</h1>
<h2>Personal details</h2>
<?php
    $sql = "SELECT * FROM assocapp_main, assocapp_list_race WHERE assocapp_main.raceid = assocapp_list_race.ID AND assocapp_main.id = '".$appid."'";
    include("inc_db_con.php");
    $row = mysql_fetch_array($rs);
        $surname = $row['surname'];
        $firstname = $row['firstname'];
        $preferred = $row['preferred'];
        $nation = $row['nation'];
        $idnum = $row['idnum'];
        $passportnum = $row['passportnum'];
        $datebirth = date("d F Y",$row['datebirth']);
        $homelang = $row['homelang'];
        $gender = $row['gender'];
        $raceid = $row['Value'];
        $disabled = $row['disabled'];
        $daytel = $row['daytel'];
        $hometel = $row['hometel'];
        $mobile = $row['mobile'];
        $email = $row['email'];
        $postaladdress1 = $row['postaladdress1'];
        $postaladdress2 = $row['postaladdress2'];
        $postalcity = $row['postalcity'];
        $postalstate = $row['postalstate'];
        $postalzip = $row['postalzip'];
        $postalcountry = $row['postalcountry'];
        $status = $row['appstatus'];
        $refcomp = $row['referredcompany'];
        $refname = $row['referredname'];
        $reftel = $row['referredtel'];
        $docyn = $row['docattachyn'];
    mysql_close();
    
    if($status == "S")
    {
        $idate = getdate();
        $sql = "UPDATE assocapp_main SET appstatus = 'I', ignitedate = '".$idate[0]."' WHERE id = '".$appid."'";
        include("inc_db_con.php");
    }
?>
<table border="1" id="table1" cellspacing="0" cellpadding="5">
	<tr>
		<td valign="top" class=tdgeneral><b>Surname:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($surname);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>First names: </b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($firstname);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Preferred name: </b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($preferred);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Nationality:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($nation);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>ID number: </b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($idnum);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Passport number:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($passportnum);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Date of birth:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($datebirth);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Home language:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($homelang);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Gender:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($gender);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Race:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($raceid);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Disabled?&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($disabled);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Day time telephone:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($daytel);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Home telephone:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($hometel);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Mobile number:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($mobile);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Email address:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($email);?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Postal address:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral>&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td class=tdgeneral>&nbsp;&nbsp;&nbsp;&nbsp;<i>Address line 1:</i>&nbsp;</td>
        <td class=tdgeneral><?php echo($postaladdress1);?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdgeneral>&nbsp;&nbsp;&nbsp;&nbsp;<i>Address line 2:</i>&nbsp;</td>
        <td class=tdgeneral><?php echo($postaladdress2);?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdgeneral>&nbsp;&nbsp;&nbsp;&nbsp;<i>City:</i>&nbsp;</td>
        <td class=tdgeneral><?php echo($postalcity);?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdgeneral style="line-height: 8pt;">&nbsp;&nbsp;&nbsp;&nbsp;<i>Province /<Br>&nbsp;&nbsp;&nbsp;&nbsp;State:</i>&nbsp;</td>
        <td class=tdgeneral><?php echo($postalstate);?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdgeneral style="line-height: 8pt;">&nbsp;&nbsp;&nbsp;&nbsp;<i>Postal code /<br>&nbsp;&nbsp;&nbsp;&nbsp;Zip:</i>&nbsp;</td>
        <td class=tdgeneral><?php echo($postalzip);?>&nbsp;</td>
    </tr>
    <tr>
        <td class=tdgeneral>&nbsp;&nbsp;&nbsp;&nbsp;<i>Country:</i>&nbsp;</td>
        <td class=tdgeneral><?php echo($postalcountry);?>&nbsp;</td>
    </tr>
	<tr>
		<td valign="top" class=tdgeneral><b>Language<br>
		proficiency:</b>&nbsp;</td>
		<td valign="top" class=tdgeneral>
		<table border="0" id="table2" cellspacing="0" cellpadding="4">
		<?php
            $l = 0;
            $sql = "SELECT * FROM assocapp_lang, assocapp_list_lang WHERE assocapp_lang.langid = assocapp_list_lang.ID AND assocapp_lang.assocappid = '".$appid."' ORDER BY Value ASC";
            include("inc_db_con.php");
            if(mysql_num_rows($rs) == 0)
            {
            ?>
			<tr>
				<td class=tdgeneral><i>No language proficiency provided.&nbsp;</i></td>
			</tr>
            <?php
            }
            else
            {
            ?>
   			<tr>
				<td class=tdheader>Language&nbsp;</td>
				<td class=tdheader>Speak&nbsp;</td>
				<td class=tdheader>Read&nbsp;</td>
				<td class=tdheader>Write&nbsp;</td>
			</tr>
            <?php
            while($row = mysql_fetch_array($rs))
            {
                $id = $row['ID'];
                $valu = $row['Value'];
                $lw = $row['lwrite'];
                $ls = $row['lspeak'];
                $lr = $row['lread'];
                
                switch ($lw)
                {
                case 0:
                    $lwrite = "NA";
                    break;
                case 1:
                    $lwrite = "Poor";
                    break;
                case 2:
                    $lwrite = "Average";
                    break;
                case 3:
                    $lwrite = "Excellent";
                    break;
                default:
                    $lwrite = "NA";
                }
                switch ($ls)
                {
                case 0:
                    $lspeak = "NA";
                    break;
                case 1:
                    $lspeak = "Poor";
                    break;
                case 2:
                    $lspeak = "Average";
                    break;
                case 3:
                    $lspeak = "Excellent";
                    break;
                default:
                    $lspeak = "NA";
                }
                switch ($lr)
                {
                case 0:
                    $lread = "NA";
                    break;
                case 1:
                    $lread = "Poor";
                    break;
                case 2:
                    $lread = "Average";
                    break;
                case 3:
                    $lread = "Excellent";
                    break;
                default:
                    $lread = "NA";
                }
        ?>
			<tr>
				<td class=tdgeneral><b><?php echo($valu."</b>"); ?>&nbsp;</td>
				<td class=tdgeneral align=center><?php echo($lspeak);?>&nbsp;</td>
				<td class=tdgeneral align=center><?php echo($lread);?>&nbsp;</td>
				<td class=tdgeneral align=center><?php echo($lwrite);?>&nbsp;</td>
			</tr>
			<?php
            }                           //while($row = mysql_fetch_array($rs))
            }                           //if no rows
            mysql_close($con);
?>
		</table>
		&nbsp;</td>
	</tr>
	</table>
<hr>
<h2>Qualifications Listed</h2>
    <?php
        $sql = "SELECT assocapp_list_qual.value AS qual, assocapp_list_qualtype.value AS qualtype, assocapp_list_qualduration.value AS duration, assocapp_qual.institution, assocapp_qual.name, assocapp_qual.year, assocapp_qual.durationid FROM assocapp_qual, assocapp_list_qualtype, assocapp_list_qual, assocapp_list_qualduration WHERE assocapp_qual.qualid = assocapp_list_qual.id AND assocapp_qual.typeid = assocapp_list_qualtype.id AND assocapp_qual.durationid = assocapp_list_qualduration.id AND assocappid = '".$appid."' ORDER BY assocapp_qual.id ASC";
        include("inc_db_con.php");
            if(mysql_num_rows($rs) == 0)
            {
            ?>
            <p><i>No qualification details provided.&nbsp;</i></p>
            <?php
            }
            else
            {
            ?>
        <table border="1" id="table2" cellspacing="0" cellpadding="5">
	<tr>
		<td class=tdheader>Qualification&nbsp;</td>
		<td class=tdheader>Institution&nbsp;</td>
		<td class=tdheader>Name of Qualification&nbsp;</td>
		<td class=tdheader>Year&nbsp;</td>
		<td class=tdheader>Duration&nbsp;</td>
	</tr>
        <?php
        while($row = mysql_fetch_array($rs))
        {
            $qualid = "";
            $typeid = "";
            $institution = "";
            $name = "";
            $year = "";
            $durationid = "";
            $qual = $row['qual'];
            $typeid = $row['qualtype'];
            $institution = $row['institution'];
            $name = $row['name'];
            $year = $row['year'];
            $duration = $row['duration'];
    ?>
	<tr>
		<td class=tdgeneral>A <u><?php echo $typeid; ?></u> in <u><?php echo($qual); ?></u>&nbsp;&nbsp;</td>
		<td class=tdgeneral><?php echo($institution); ?>&nbsp;&nbsp;</td>
		<td class=tdgeneral><?php echo($name); ?>&nbsp;&nbsp;</td>
		<td class=tdgeneral><?php echo($year); ?>&nbsp;&nbsp;</td>
		<td class=tdgeneral><?php echo($duration); ?>&nbsp;&nbsp;</td>
	</tr>
    <?php
        }
        ?>
        </table>
        <?php
        }
        mysql_close($con);
    ?>
<hr>
<h2>Experience</h2>
<?php
            $sql = "SELECT * FROM assocapp_exp WHERE assocappid = '".$appid."'";
            include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
            $expid = $row['id'];
            $projectexp = $row['projectexp'];
            mysql_close($con);
?>
		<?php
		if(strlen($expid) > 0)
		{
?>
<h4 style="margin-top: 25pt; margin-bottom: 10pt">Experience:</h4>
<?php
            $f = 0;
            $sql = "SELECT * FROM assocapp_exp_field, assocapp_list_expfield WHERE expid = ".$expid." AND assocapp_exp_field.fieldid = assocapp_list_expfield.ID";
            include("inc_db_con.php");
            if(mysql_num_rows($rs) == 0)
            {
            ?>
            <p><i>No experience provided.&nbsp;</i></p>
            <?php
            }
            else
            {
            ?>
<table border="1" id="table1" cellspacing="0" cellpadding="5">
	<tr>
		<td valign="top" class=tdheader>Field of work&nbsp;</td>
		<td valign="top" class=tdheader>
		Years&nbsp;</td>
	</tr>
<?php
            while($row = mysql_fetch_array($rs))
            {
                $valu = $row['value'];
                $years = $row['years'];
                if($years == "-5")
                {
                    $years = "&lt;5";
                }
        ?>
	<tr>
		<td valign="top" class=tdgeneral><?php echo $valu;?>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo $years;?>&nbsp;</td>
	</tr>
			<?php
            }                           //while($row = mysql_fetch_array($rs))
            echo("</table>");
            }                           //if no rows
            mysql_close($con);
?>
<input type=hidden name=f value=<?php echo $f;?>>
<h4 style="margin-top: 25pt; margin-bottom: 0pt;">Project experience:</h4>
<p><?php if(strlen($projectexp)>0){echo($projectexp);}else{echo("<p><i>No project experience provided.</i></p>");} ?></p>
<h4  style="margin-top: 25pt; margin-bottom: 10pt">Skills:</h4>
		<?php
            $sql = "SELECT * FROM assocapp_exp_skill, assocapp_list_expskill WHERE assocapp_exp_skill.skillid = assocapp_list_expskill.ID AND assocapp_exp_skill.expid = ".$expid." ORDER BY Value ASC";
            include("inc_db_con.php");
            if(mysql_num_rows($rs) == 0)
            {
            ?>
            <p><i>No skills details provided.&nbsp;</i></p>
            <?php
            }
            else
            {
            ?>
<table border="1" id="table2" cellspacing="0" cellpadding="5">
	<tr>
		<td class=tdheader>Skill&nbsp;</td>
		<td class=tdheader>Rating&nbsp;</td>
	</tr>
<?php
            while($row = mysql_fetch_array($rs))
            {
                $valu = $row['value'];
                $r = $row['rating'];
                switch ($r)
                {
                case 0:
                    $rating = "NA";
                    break;
                case 1:
                    $rating = "Poor";
                    break;
                case 2:
                    $rating = "Average";
                    break;
                case 3:
                    $rating = "Excellent";
                    break;
                default:
                    $rating = "NA";
                }

        ?>
	<tr>
		<td class=tdgeneral><b><?php echo $valu;?></b>&nbsp;</td>
		<td align="center" class=tdgeneral><?php echo($rating);?>&nbsp;</td>
	</tr>
			<?php
            }                           //while($row = mysql_fetch_array($rs))
            echo("</table>");
            }                           //if no rows
            mysql_close($con);
?>
<?
}
else    //if expid > 0
{
            echo("<p><i>No experience provided.&nbsp;</i></p>");

}
?>

<hr>
<h2>Employers Listed</h2>
<?php
$e = 0;

    $sql = "SELECT * FROM assocapp_emp WHERE assocapp_emp.assocappid = '".$appid."' ORDER BY id DESC";
    include("inc_db_con.php");
            if(mysql_num_rows($rs) == 0)
            {
                $emperr = "Y";
            ?>
            <p><i>No employment details provided.&nbsp;</i></p>
            <?php
            }
            else
            {
                $emperr = "N";
            ?>
<table border="1" id="table2" cellspacing="0" cellpadding="5">
	<tr>
		<td class=tdheader valign="top">Employer&nbsp;</td>
		<td class=tdheader valign="top">Start&nbsp;</td>
		<td class=tdheader valign="top">End&nbsp;</td>
		<td class=tdheader valign="top">Position&nbsp;</td>
		<td class=tdheader valign="top">Department&nbsp;</td>
		<td class=tdheader valign="top">Tasks&nbsp;</td>
		<td class=tdheader valign="top">Reason for leaving&nbsp;</td>
	</tr>
<?php
        while($row = mysql_fetch_array($rs))
        {
            $e = $e + 1;
            $employer[$e] = $row['employer'];
            $start[$e] = $row['start'];
            $end[$e] = $row['end'];
            $positionid[$e] = $row['positionid'];
            $deptid[$e] = $row['deptid'];
            $reasonid[$e] = $row['reasonid'];
            $empid[$e] = $row['id'];
        }
        }                           //if no rows
        mysql_close($con);
if($emperr == "N")
{
        for($z=1;$z<=$e;$z++)
        {
/*            $sdate0 = "";
            $edate0 = "";
            $sdate = "";
            $edate = "";
            $sday = "";
            $smon = "";
            $smonth = "";
            $syear = "";
            $eday = "";
            $emon = "";
            $emonth = "";
            $eyear = "";
//formats start date for display
            $sdate0 = $start[$z];
            $sday = substr($sdate0,8,2);
            $smon = substr($sdate0,5,2);
            $smon = $smon * 1;
            $syear = substr($sdate0,0,4);
            $smonth = $months[$smon-1];
            $sdate = $sday . " " . $smonth . " " . $syear;
//formats end date for display
            $edate0 = $end[$z];
            $eday = substr($edate0,8,2);
            $emon = substr($edate0,5,2);
            $emon = $emon * 1;
            $eyear = substr($edate0,0,4);
            $emonth = $months[$emon-1];
            $edate = $eday . " " . $emonth . " " . $eyear;
*/
?>
	<tr>
		<td class=tdgeneral valign="top"><?php echo($employer[$z]); ?>&nbsp;</td>
		<td class=tdgeneral valign="top"><?php echo(date("d-M-Y",$start[$z])); ?>&nbsp;</td>
		<td class=tdgeneral valign="top"><?php echo(date("d-M-Y",$end[$z])); ?>&nbsp;</td>
<?php
    $sql = "SELECT Value FROM assocapp_list_empposition WHERE ID = ".$positionid[$z];
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
            echo("<td class=tdgeneral valign=top>".$row['Value']."&nbsp;</td>");
        mysql_close($con);

    $sql = "SELECT Value FROM assocapp_list_empdept WHERE ID = ".$deptid[$z];
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
            echo("<td class=tdgeneral valign=top>".$row['Value']."&nbsp;</td>");
        mysql_close($con);


?>
		<td class=tdgeneral valign="top">
		<ul>
<?php
    $sql = "SELECT * FROM assocapp_emp_task, assocapp_list_emptask WHERE assocapp_emp_task.taskid = assocapp_list_emptask.ID AND assocapp_emp_task.empid = ".$empid[$z]." ORDER BY Value ASC";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
			echo("<li>".$row['Value']."</li>");
        }
        mysql_close($con);
?>
		</ul></td>
<?php
    $sql = "SELECT Value FROM assocapp_list_empreason WHERE ID = ".$reasonid[$z];
    include("inc_db_con.php");

        $row = mysql_fetch_array($rs);
            echo("<td class=tdgeneral valign=top>".$row['Value']."&nbsp;</td>");
        mysql_close($con);
?>
	</tr>
<?php
        }
?>
</table>
<?php
}           //if emperr = "N"
?>
<hr>
<h2>References Listed</h2>
<?php
    $sql = "SELECT * FROM assocapp_ref WHERE assocapp_ref.assocappid = '".$appid."' ORDER BY id DESC";
    include("inc_db_con.php");
                if(mysql_num_rows($rs) == 0)
            {
            ?>
            <p><i>No references provided.&nbsp;</i></p>
            <?php
            }
            else
            {
            ?>
<table border="1" id="table2" cellspacing="0" cellpadding="5">
	<tr>
		<td class=tdheader rowspan="2">Name&nbsp;</td>
		<td class=tdheader rowspan="2">Organisation&nbsp;</td>
		<td class=tdheader rowspan="2">Position&nbsp;</td>
		<td class=tdheader colspan="3">&nbsp;Contact details&nbsp;</td>
	</tr>
	<tr>
		<td class=tdheader valign="top">Day time&nbsp;</td>
		<td class=tdheader valign="top">Home&nbsp;</td>
		<td class=tdheader valign="top">Mobile&nbsp;</td>
	</tr>

<?php
        while($row = mysql_fetch_array($rs))
        {
            $name = $row['firstname']." ".$row['surname'];
            $organisation = $row['organisation'];
            $position = $row['position'];
            $daytel = $row['daytel'];
            $hometel = $row['hometel'];
            $mobile = $row['mobile'];
?>
	<tr>
		<td class=tdgeneral valign="top"><?php echo($name); ?>&nbsp;&nbsp;</td>
		<td class=tdgeneral valign="top"><?php echo($organisation); ?>&nbsp;&nbsp;</td>
		<td class=tdgeneral valign="top"><?php echo($position); ?>&nbsp;&nbsp;</td>
		<td class=tdgeneral valign="top"><?php echo($daytel); ?>&nbsp;&nbsp;</td>
		<td class=tdgeneral valign="top"><?php echo($hometel); ?>&nbsp;&nbsp;</td>
		<td class=tdgeneral valign="top"><?php echo($mobile); ?>&nbsp;&nbsp;</td>
	</tr>
<?php
        }       //while
        echo("</table>");
        }       //if no rows
    mysql_close($con);
?>
<hr>
<h2>Referred By</h2>
<table border="1" id="table2" cellspacing="0" cellpadding="5">
	<tr>
		<td class=tdheader>Name&nbsp;</td>
		<td class=tdheader>Organisation&nbsp;</td>
		<td class=tdheader>Contact Tel</td>
	</tr>
	<tr>
		<td class=tdgeneral valign="top"><?php echo($refname); ?>&nbsp;&nbsp;</td>
		<td class=tdgeneral valign="top"><?php echo($refcomp); ?>&nbsp;&nbsp;</td>
		<td class=tdgeneral valign="top"><?php echo($reftel); ?>&nbsp;&nbsp;</td>
	</tr>
</table>
<?php
 if($docyn == 'Y') { 
$sql = "SELECT * FROM assocapp_doc WHERE assocappid = '".$appid."'";
include("inc_db_con.php");
if(mysql_num_rows($rs)>0) { 
?>
<hr>
<h2>Documents</h2>
<table border="1" id="table2" cellspacing="0" cellpadding="5">
    <?php while($row = mysql_fetch_array($rs)) { 
        $url = "<a href=http://www.ignite4u.co.za/IAO/AssociateApplication/".$row['document']." target=_blank>";
        $doc = str_replace("docs/","",$row['document']);
?>
	<tr>
		<td class=tdgeneral valign="top"><?php echo($url.$doc."</a>"); ?>&nbsp;&nbsp;</td>
	</tr>
    <?php } ?>
</table>
<?php 
} //if num rows > 0
mysql_close();
} //if docyn = y ?>
<hr>
<p>&nbsp;</p>

</body></html>
