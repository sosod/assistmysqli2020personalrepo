<?php
include("inc_ignite.php");
?>
<html>
<head>
<title>Ignite4U.co.za</title>
<meta name="description" content="ignite advisory services, ignite4u">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/default.css" type="text/css">
</head>
<?php
include("inc_style.php");
?>
<body>
<h1 style="margin-bottom:40px;">Associates: On-line Application</h1>
<table border="1" cellspacing="0" cellpadding="5" style="border-collapse: collapse; border-style: solid;">
<tr class=tdgeneral><td colspan=8><h2 style="margin-top:5px; margin-bottom:5px;">New Applications</h2></td></tr>
    <?php
        $sql = "SELECT id, surname, firstname, datesubmit, idnum, datebirth, gender, mobile, daytel, raceid FROM assocapp_main WHERE appstatus = 'S' AND CHAR_LENGTH(firstname) > 0 ORDER BY datesubmit ASC";
        include("inc_db_con.php");
    if(mysql_num_rows($rs) > 0)
    {
    ?>
	<tr>
		<td valign="top" class=tdheader>Ref</td>
		<td valign="top" class=tdheader>Date Submitted</td>
		<td valign="top" class=tdheader>Applicant</td>
		<td valign="top" class=tdheader>ID/DOB</td>
		<td valign="top" class=tdheader>Race</td>
		<td valign="top" class=tdheader>Gender</td>
		<td valign="top" class=tdheader>Contact</td>
		<td valign="top" class=tdheader>Qualification</td>
	</tr>
    <?php
        while($row = mysql_fetch_array($rs))
        {
            $sdate = $row['datesubmit'];
            $sdate = date("d-M-Y",$sdate);
            $bdate = $row['datebirth'];
            if(strlen($bdate)>0)
            {
                $bdate = "<br>".date("d-M-Y",$bdate);
            }
            $iddob = $row['idnum'].$bdate;
            $g = $row['gender'];
            switch($g)
            {
                case "M":
                    $g = "Male";
                    break;
                case "F":
                    $g = "Female";
                    break;
                default:
                    $g = "Unknown";
                    break;
            }
            $mob = $row['mobile'];
            $dtel = $row['daytel'];
            $cont = "";
            if(strlen($mob)>4)
            {
                if(strlen($dtel)>4)
                {
                    $cont = "<font color=#888888>-</font> ".str_replace("(27) ","",$mob)."<br><font color=#888888>-</font> ".str_replace("(27) ","",$dtel);
                }
                else
                {
                    $cont = "<font color=#888888>-</font> ".str_replace("(27) ","",$mob);
                }
            }
            else
            {
                if(strlen($dtel)>4) { $cont = "<font color=#888888>-</font> ".str_replace("(27) ","",$dtel); }
            }
            $rid = $row['raceid'];
            if(strlen($rid)>0)
            {
                $sql2 = "SELECT Value FROM assocapp_list_race WHERE ID = ".$rid;
                include("inc_db_con2.php");
                $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $r = $row2['Value'];
            }
            else
            {
                $r = "";
            }
            $aid = $row['id'];
            $sql2 = "SELECT name qname, institution qinst FROM assocapp_qual WHERE assocappid = '".$aid."'";
            include("inc_db_con2.php");
                $qual = "";
                while($row2 = mysql_fetch_array($rs2))
                {
                    $qname = $row2['qname'];
                    $qinst = $row2['qinst'];
                    if(strlen($qname)>0)
                    {
                        if(strlen($qual)>0) { $qual.="<br>"; }
                        $qual.=$qname;
                        if(strlen($qinst)>0) { $qual.=" [".$qinst."]"; }
                    }
                }
            mysql_close($con2);
?>
	<tr>
		<td valign="top" class=tdgeneral><?php echo("<a href=view.php?a=".$row['id'].">".$row['id']."</a>");?></td>
		<td valign="top" class=tdgeneral><?php echo($sdate);?></td>
		<td valign="top" class=tdgeneral><?php echo($row['firstname']." ".$row['surname']);?>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($iddob);?>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($r);?>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($g);?>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($cont);?>&nbsp;</td>
		<td valign="top" class=tdgeneral><small><?php echo($qual);?>&nbsp;</small></td>
	</tr>
<?php
        }
        //echo("</table>");
    }
    else
    {
    echo("<tr class=tdgeneral><td colspan=8>No new applications to display.</td></tr>");
    }
        mysql_close();
?>
<tr class=tdgeneral><td colspan=8><h2 style="margin-bottom:5px;">Past Applications</h2></td></tr>
    <?php
        $sql = "SELECT id, surname, firstname, datesubmit, idnum, datebirth, gender, mobile, daytel, raceid FROM assocapp_main WHERE appstatus = 'I' AND CHAR_LENGTH(firstname) > 0 ORDER BY datesubmit DESC";
        include("inc_db_con.php");
    if(mysql_num_rows($rs) > 0)
    {
    ?>
<?php //<table border="1" id="table1" cellspacing="0" cellpadding="5"> ?>
	<tr>
		<td valign="top" class=tdheader>Ref</td>
		<td valign="top" class=tdheader>Date Submitted</td>
		<td valign="top" class=tdheader>Applicant</td>
		<td valign="top" class=tdheader>ID/DOB</td>
		<td valign="top" class=tdheader>Race</td>
		<td valign="top" class=tdheader>Gender</td>
		<td valign="top" class=tdheader>Contact</td>
		<td valign="top" class=tdheader>Qualification</td>
	</tr>
    <?php
        while($row = mysql_fetch_array($rs))
        {
            $sdate = $row['datesubmit'];
            $sdate = date("d-M-Y",$sdate);
            $bdate = $row['datebirth'];
/*            if(strlen($bdate)>0)
            {
                $bdate = date("d-M-Y",$bdate);
            }
*/            $idnum = $row['idnum'];
            $iddob = "";
            if(strlen($idnum)>0) { $iddob.=$idnum; }
            if(strlen($bdate)>0) {
                if(strlen($iddob)>0) { $iddob.="<br>"; }
                $iddob.= date("d-M-Y",$bdate);
            }
            //$iddob = $row['idnum'].$bdate;
            $g = $row['gender'];
            switch($g)
            {
                case "M":
                    $g = "Male";
                    break;
                case "F":
                    $g = "Female";
                    break;
                default:
                    $g = "Unknown";
                    break;
            }
            $mob = $row['mobile'];
            $dtel = $row['daytel'];
            $cont = "";
            if(strlen($mob)>4)
            {
                if(strlen($dtel)>4)
                {
                    $cont = "<font color=#888888>-</font> ".str_replace("(27) ","",$mob."<br><font color=#888888>-</font> ".$dtel);
                }
                else
                {
                    $cont = "<font color=#888888>-</font> ".str_replace("(27) ","",$mob);
                }
            }
            else
            {
                if(strlen($dtel)>4) { $cont = "<font color=#888888>-</font> ".str_replace("(27) ","",$dtel); }
            }
            $rid = $row['raceid'];
            if(strlen($rid)>0)
            {
                $sql2 = "SELECT Value FROM assocapp_list_race WHERE ID = ".$rid;
                include("inc_db_con2.php");
                $row2 = mysql_fetch_array($rs2);
                mysql_close($con2);
                $r = $row2['Value'];
            }
            else
            {
                $r = "";
            }
            $aid = $row['id'];
            $sql2 = "SELECT name qname, institution qinst FROM assocapp_qual WHERE assocappid = '".$aid."'";
            include("inc_db_con2.php");
                $qual = "";
                while($row2 = mysql_fetch_array($rs2))
                {
                    $qname = $row2['qname'];
                    $qinst = $row2['qinst'];
                    if(strlen($qname)>0)
                    {
                        if(strlen($qual)>0) { $qual.="<br>"; }
                        $qual.=$qname;
                        if(strlen($qinst)>0) { $qual.=" [".$qinst."]"; }
                    }
                }
            mysql_close($con2);
?>
	<tr>
		<td valign="top" class=tdgeneral><?php echo("<a href=view.php?a=".$row['id'].">".$row['id']."</a>");?></td>
		<td valign="top" class=tdgeneral><?php echo($sdate);?></td>
		<td valign="top" class=tdgeneral><?php echo($row['firstname']." ".$row['surname']);?></td>
		<td valign="top" class=tdgeneral><?php echo($iddob);?>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($r);?>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($g);?>&nbsp;</td>
		<td valign="top" class=tdgeneral><?php echo($cont);?>&nbsp;</td>
		<td valign="top" class=tdgeneral><small><?php echo($qual);?>&nbsp;</small></td>
	</tr>
<?php
        }
        echo("</table>");
    }
    else
    {
    echo("<p>No past applications to display.</p>");
    }
        mysql_close();
?>
<p>&nbsp;</p>

</body></html>
