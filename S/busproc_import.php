<?php
/**
 * @var array $version_list - from inc_header
 * @var int $current_version_displayed - from inc_header
 */

$page_section = "MANAGE";
$page_action = "EDIT";
$original_url = "busproc.php";
$my_page = "busproc_import";
$is_add_page = true;
$is_edit_page = TRUE;
$is_view_page = true;
$object_type = "BUSPROC";
$section = $object_type;
$title = "Business Processes";
$page_heading = array("main.php" => "Master Setups", "busproc.php" => $title, 3 => "Import");

$max_rows_to_import = 500;

include("inc_header.php");


$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "STEP0";
//	$section = $_REQUEST['section']; //echo $section;
$class_name = "S_".$object_type;
$segmentObject = new $class_name();
//	$segment_details = $segmentObject->getSegmentDetails(true);


switch($action) {

//Step 1 = generate template
	case "STEP1":
		//handled by manage_edit_segments_import_generate.php
		break;


//Step 2 = populate template - done by user
	case "STEP2":
		break;


//Step 3 = import template for validation
	case "STEP3":
		include($my_page."_step3.php");
		break;


//Step 4 = finalise import
	case "STEP4":
		include($my_page."_step4.php");
		break;


//Step 0 = starting point
	case "STEP0":
	default:

		echo "
<p>&nbsp;</p>
<form name=frm_import method=post action=".$my_page.".php enctype=\"multipart/form-data\">
<input type=hidden name=section value='$section' />
<input type=hidden name=action value='STEP3' />
<input type=hidden name=line_version value='".$current_version_displayed."' />
<table width=550px class=form>
	<tr>
		<th width=60px>Step 1:</th>
		<td>Generate template (if not done already).
		<span class=float><button id=btn_step0_generate class=btn-step0>".$segmentObject->replaceActivityNames("|download|")."</button></span></td>
	</tr>
	<tr>
		<th>Step 2:</th>
		<td>Populate the template.</td>
	</tr>
<!--	<tr>
		<th>Step 3.1:</th>
		<td>Choose the relevant version: <select id='sel_version' name='line_version'><option selected value='X'>--- CHOOSE VERSION ---</option>";
//	foreach($version_list as $i => $v) {
//		echo "<option value=".$i.">".$v."</option>";
//	}
		echo "</select></td>
	</tr> -->
	<tr>
		<th>Step 3.2:</th>
		<td>Import the template.<br />&nbsp;
		<span class=float><input type=file name=mscoa_import_file value='' /> <button id=btn_step0_import class=btn-step0>".$segmentObject->replaceActivityNames("|import|")."</button></span></td>
	</tr>
	<tr>
		<th>Step 5:</th>
		<td>Finalise and Accept the Import.  The line items will not be imported until the \"Accept\" button is clicked.</td>
	</tr>

</table>
</form>
<h3>Guidelines on Using the Template</h3>
<ul>
<li>The template is in comma-separated values (CSV) format.</li>
<li>The columns of the template must be in the order given below.</li>
<li>The first 2 rows will be ignored (assumed to be heading rows).</li>
<li>Where line items (sub-levels / children line items) fall under other line items (parent line items), the parent line items must either be imported first, or must be placed above the children line items within the import template.</li>
<li>It is advisable to import no more than $max_rows_to_import rows at a time, to reduce the risk of the process taking too long and logging you out before it is complete.</li>
</ul>
<h4>Template Columns</h4>
<table>
	<tr>
		<th>Column A:</th>
		<td class=b>Parent Ref</td>
		<td>This is the Internal Ref of the line item (parent) to which the imported line item belongs.  Leave it blank if the line item is to be a top level line item.</td>
	</tr>
	<tr>
		<th>Column B:</th>
		<td class=b>Internal Ref</td>
		<td>This is the Internal Ref of the line item being imported.  It is required and must be <span class='b u red'>unique</span>.</td>
	</tr>
	<tr>
		<th>Column C:</th>
		<td class=b>Line Item Name</td>
		<td>This is the name / short description of the line item being imported.  This is required.</td>
	</tr>
	<tr>
		<th>Column D:</th>
		<td class=b>Line Item Description</td>
		<td>This is the general description or glossary explanation of the line item. What is the purpose of the line item?</td>
	</tr>
	<tr>
		<th>Column E:</th>
		<td class=b>Nickname / Alias</td>
		<td>This is the short name or alternative name for the line item.</td>
	</tr>
	<tr>
		<th>Column F:</th>
		<td class=b>Has Sub-levels?</td>
		<td>Can the line item have sub-levels (children) / be broken down further?  This is required and must be either Yes or No.</td>
	</tr>
	<tr>
		<th>Column G:</th>
		<td class=b>Can be Assigned?</td>
		<td>Can this line item be assigned to objects in other modules in Assist?  E.g. Can KPIs or Projects or Activities be assigned to this line item? This is required and must be either Yes or No.</td>
	</tr>

</table>


";


		break;


}


?>
<script type="text/javascript">
	$(function () {
//STEP 0 CODE
		$(".btn-step0").button()
			.removeClass("ui-state-default")
			.addClass("ui-button-bold-grey")
			.hover(
				function () {
					$(this).removeClass("ui-button-bold-grey").addClass("ui-button-bold-orange");
				},
				function () {
					$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-grey");
				}
			);
		$("#btn_step0_import").button({
			icons: {primary: "ui-icon-arrowreturnthick-1-n"}
		}).click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			/*if($("#sel_version").val()=="X" || $("#sel_version").val().length==0) {
				$("#sel_version").addClass("required");
				AssistHelper.finishedProcessing("error","Version is required.");
			} else {*/
			$("form[name=frm_import]").submit();
			//}
		});
		$("#btn_step0_generate").button({
			icons: {primary: "ui-icon-arrowreturnthick-1-s"}
		}).click(function (e) {
			e.preventDefault();
			document.location.href = "<?php echo $my_page; ?>_generate.php";
		});
	});
</script>