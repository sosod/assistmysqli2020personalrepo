<?php
require_once("../module/autoloader.php");


$fdata = "\"Parent Ref\",\"Line Item Ref\",\"Line Item Name\",\"Description\",\"Nickname or Alias\",\"Has children?\",\"Can be assigned?\"\r\n\"\",\"Required & Unique\",\"Required\",\"\",\"\",\"Yes / No\",\"Yes / No\"\r\n";

$me = new S();
$cmpcode = $me->getCmpCode();
$modref = $me->getModRef();
$today = time();

//WRITE DATA TO FILE
$filename = "../files/".$cmpcode."/".$modref."_template_".date("Ymd_Hi", $today).".csv";
$newfilename = "template.csv";
$file = fopen($filename, "w");
fwrite($file, $fdata."\n");
fclose($file);
//SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
header('Content-type: text/plain');
header('Content-Disposition: attachment; filename="'.$newfilename.'"');
readfile($filename);


?>