var SHelper = {

	processObjectForm : function($form,page_action,page_direct,include_new_id_in_request,new_id_request_field) {
		//console.log("pOF"+AssistForm.serialize($form));
		//alert(page_action);
		var pa = AssistString.substr(page_action,-6,6);
		//if(page_action!="DELETE") {
		if(pa!="DELETE" && pa!="MINATE" && pa!="TIVATE") {	//MINATE => TERMINATE  TIVATE=>DEACTIVATE
			var valid = SHelper.validateForm($form);
		} else {
			var valid = true;
		}
		//var valid=false;
		if(valid) {
			AssistHelper.processing();
			var dta = AssistForm.serialize($form); //alert(dta);
			console.log(dta);

			if(page_action == "serialize") {
				return serial;
			} else {
				var result = AssistHelper.doAjax("inc_controller.php?action="+page_action,dta);
				//Remove console.log when finished, and uncomment redirects!
				// var result = ['ok','test'];
	// console.log(result);
	// console.log(page_direct);
	//console.log(result.responseText);
				if(result[0]=="ok") {
					if(page_direct=="home") {
						SHelper.routeHome();
					} else if(page_direct=="dialog") {
						//alert(result[1]);
						// console.log("REDIRECTING!!!!!");
						window.parent.dialogFinished(result[0],result[1],pa);
					} else {
						var url = page_direct+'r[]='+result[0]+'&r[]='+result[1];
						if(include_new_id_in_request) {
							url = url+"&"+new_id_request_field+"="+result['object_id'];
						}
						document.location.href = url;
					}
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}
		}
	},
	processObjectFormWithAttachment : function($form,page_action,page_direct) {
		console.log("pOFwA"+AssistForm.serialize($form));
		var valid = SHelper.validateForm($form);
		//var valid=false;
		if(valid) {
			AssistHelper.processing();
			$form.prop("target","file_upload_target");
			$form.prop("action","inc_controller.php");
			//$form.append("<input type=hidden name=page_direct value='"+page_direct+"' />");
			//$form.append("<input type=hidden name=action value='"+page_action+"' />");
			//alert(AssistForm.serialize($form));
			$form.submit();
			/*var dta = AssistForm.serialize($form); //alert(page_action+"\n\n"+dta);
			if(page_action == "serialize") {
				return serial;
			} else {
				var result = AssistHelper.doAjax("inc_controller.php?action="+page_action,dta);
				//Remove console.log when finished!
				console.log(result);
				if(result[0]=="ok") {
					if(page_direct=="home") {
						SHelper.routeHome();
					} else {
						document.location.href = page_direct+'r[]='+result[0]+'&r[]='+result[1];
					}
				} else {
					AssistHelper.finishedProcessing(result[0],result[1]);
				}
			}*/
			AssistHelper.closeProcessing();
		}
	},
	routeHome : function() {
		parent.header.$("#backHome").trigger("click");
	},
	validateForm : function($form) {
		console.log("VALIDATING");
	/**
	 * 1. loop through $form input fields and check for any with attr("req")=="required" where no value has been input
	 * 2. if previous loop found missing required field(s)
	 * 		2.a display error message listing missing fields
	 * 		2.b reject form
	 * 	 else
	 *		serialize $form
	 * 		send to inc_controller with action=page_action
	 *  	if ajax response[0] = "ok"
	 * 			redirect to page_direct+r[]=result[0]&r[]=result[1]
	 * 		else
	 * 			display error message
	 */
		$formchild = $form.find('button, input:visible, select, textarea');
		var fields = [];
		var result = [];
		var serial = [];
		var guid_name = "";
		var err_list = "<h2 class=idelete>Error</h2><p>Assist encountered the following errors while processing the form:</p><ol>";
		var num_arr = ['0','1','2','3','4','5','6','7','8','9','.','-'];
		var unique_guid = true;
		$formchild.each(function(){
			if(!$(this).hasClass('i-am-the-submit-button') && $(this).prop("id")!="btn_save" && !$(this).is(":checkbox") && $(this).prop("name").length>0) {
				$(this).removeClass("required");
				var val = $(this).val(); console.log($(this).prop("id"));
				var type = $(this).prop("type");
				var req = $(this).attr("req");
				var role = $(this).prop("role");
				if($(this).parents("tr").children("th").length > 0) {
					var id_raw = $(this).parents("tr").children("th")[0].innerHTML;
					var id_arr = id_raw.split(":");
					var id = id_arr[0];
				} else {
					var id = "Unknown Field ("+$(this).prop("id")+")";
				}
				if(req == 1){
					if(type != "submit" && type != "button"){
						if(type == "select-one"){
							if(val == "X" || val=="0" || val==0 || val==undefined || val==null){
								fields.push(id);
								$(this).addClass("required");
							}
						} else if(type == "select-multiple") {
							if($(this).find("option:selected").length==0) {
								fields.push(id);
								$(this).addClass("required");
								val = $(this).find("option:selected:first").val();
							}
						} else {
							if(val.length == 0 || val == undefined || val == null){
								fields.push(id);
								$(this).addClass("required");
							}else if($(this).hasClass('number-only')) {
								//alert("number!!");
								for ( var i = 0; i < val.length; i++ ) {
									if($.inArray(val.charAt(i),num_arr)<0) {
										fields.push(id);
										$(this).addClass("required");
									}
								}
							}
						}
					}
				} else if($(this).hasClass("number-only")) {
								//alert("number!!");
								for ( var i = 0; i < val.length; i++ ) {
									if($.inArray(val.charAt(i),num_arr)<0) {
										fields.push(id);
										$(this).addClass("required");
									}
								}
				}
				//console.log($(this).prop("id")+" = "+val+" # "+type);
				//val = AssistString.decode(val);
				//serial.push(val);
			}
			if($(this).prop("id")=="ref" && val.length>0) {
				var dta = "object_id="+$form.find("input[name=object_id]").val()+"&segment_section="+$form.find("input[name=segment_section]").val()+"&guid="+AssistString.code(val);
				var r = AssistHelper.doAjax("inc_controller.php?action=SEGMENT.checkForUniqueGUID",dta);
				if(r[0]=="error") {
					unique_guid = false;
					guid_name = id;
				}
			}
		});
		if(fields.length>0) {
			err_list+= "<li>The following fields are required but have not been captured/selected:<ul>";
			for(var i in fields){
				err_list += "<li class=i>" + fields[i] + "</li>";
			}
			err_list += "</ul></li>";
		}
		if(!unique_guid) {
			err_list += "<li>The "+guid_name+" entered is not unique.  Each "+guid_name+" must be unique within each segment.</li>";
		}
		err_list += "</ol><p>Please update the form and try again.</p><br>";
		if(fields.length != 0 || !unique_guid){
			$("#div_error").html(err_list);
			$("#div_error").dialog({
				modal: true,
				resizable: false,
				buttons: {
					"Okay":function(){
						$(this).dialog("close");
					}
				}
			});
			AssistHelper.hideDialogTitlebar("id","div_error");
			return false;
		} else {
			return true;
		}
	}

};