<?php
/**
 * @var int $idp_object_id - from calling page - holdover from old mSCOA code
 * @var string $page_action - from calling page
 * @var string $object_type - from calling page
 * @var string $display_type - from calling page - dialog or document
 * @var S $helper - from inc_header from calling page
 * @var string $js - from inc_header from calling page
 * @var int $parent_object_id - from calling page
 * @var int $child_object_id - from calling page
 * @var $displayObject S_DISPLAY - from inc_header from calling page
 * @var string $page_redirect_path - from calling page
 */


$page_redirect_path_total = "redirect_page.php?object_id=".$idp_object_id."&tab=segments&action=".$object_type."&section=".$_REQUEST['object_type'];


if(strtoupper($page_action) == "EDIT") {
	$is_edit_page = true;
	$is_add_page = false;
	$is_view_page = false;
	$is_copy_page = false;
} else if(strtoupper($page_action)=="ADD") {
	$is_edit_page = false;
	$is_add_page = true;
	$is_view_page = false;
	$is_copy_page = false;
} else if(strtoupper($page_action)=="COPY") {
	$is_edit_page = false;
	$is_add_page = false;
	$is_view_page = false;
	$is_copy_page = true;
} else {
	$is_edit_page = false;
	$is_add_page = false;
	$is_view_page = true;
	$is_copy_page = false;
}


		$parent_object_type = $object_type;
		$class_name = "S_".$object_type;
		$childObject = new $class_name();
		if($display_type=="default") {
			//$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'parent_id'=>$parent_object_id));
		}
		$parentObject = false;
		$child_object_type = $object_type;
//		$parent_id_name = $childObject->getParentFieldName();




$child_redirect = "redirect.php?object_type=".$object_type."&object_id=";
$child_name = $helper->getObjectName($childObject->getMyObjectName());



ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<table class='tbl-container not-max'>
	<tr>
	<?php
	if($display_type=="default") {
		$td2_width = "48%";
	?>
		<td width=47%>
			<?php
			/**  */
			//$js.= $displayObject->drawDetailedView($parent_object_type, $parent_object_id, ($parent_object_type!="IDP"));
			//$js.= isset($go_back) ? $displayObject->drawPageFooter($helper->getGoBack($go_back)) : "";
			?>
		</td>
		<td width=5%>&nbsp;</td>
		<td width=<?php echo $td2_width; ?>><h2>New <?php echo $child_name; ?></h2>
	<?php
	} else {
		echo "<td>";
	}
			$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, $parent_object_type, $parentObject, $parent_object_id, $page_action, $page_redirect_path);
			echo $data['display'];
			$js.= $data['js'];
		?></td>
	</tr>
	<tr><td>
<?php
if($is_edit_page || $is_view_page) {
	$js.=$displayObject->drawPageFooter("",$childObject->getMyLogTable(),"log_object_type=".$object_type."&log_object_id=".$child_object_id,$child_object_id);
}
?>
	</td></tr>
</table>
<script type=text/javascript>
$(function() {
	<?php
	echo $js;
	?>
	$("input:button.btn_edit").click(function() {
		var i = $(this).attr("ref");
		document.location.href = '<?php echo $child_redirect; ?>'+i;
	});
	//console.log("<?php echo $display_type; ?>");

	<?php
	if($display_type=="dialog") {
		echo $displayObject->getIframeDialogJS($child_object_type,"dlg_child",$_REQUEST);
	}
	?>

});
</script>