<?php
/**
 * @var S_DISPLAY $displayObject - from inc_header
 * @var S $helper - from inc_header
 * @var string $js - from inc_header - variable to hold any javascript/jquery code until it can be echoed at the end
 */
$section = "FINYR";
$action = "VIEW";
$title = "Financial Years";
$page_heading = array("main.php" => "Master Setups", 2 => $title);

include("inc_header.php");

$me = new S_FINANCIALYEARS();

//$result = array();

//$dbref = "assist_".$cmpcode."_master_financialyears";
$dbref = $me->getTable();

$page = "financialyears.php";

$assist[0] = array('id' => 0, 'value' => "Unspecified");

if(isset($_REQUEST['act'])) {
	switch($_REQUEST['act']) {
		case "ADD":
//			echo "<hr /><h1 class='green'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//			ASSIST_HELPER::arrPrint($_REQUEST);
//			die();
			$result = $me->addObject($_REQUEST);
			break;
		case "EDIT":
			$result = $me->editObject($_REQUEST);
			break;
		case "DEL":
			$result = $me->deleteObject($_REQUEST);
			break;
		default:
			break;
	}
}
//just in case - prevent double action
unset($_REQUEST['act']);
unset($_REQUEST);
?>
<style type=text/css xmlns="http://www.w3.org/1999/html">
	#tbl_edit th {
		text-align: left;
	}
</style>
<?php
if(isset($result) && is_array($result) && count($result)==0) {
	ASSIST_HELPER::displayResult($result);
} else {
	echo "<div width=350px style='float:right;width:350px;margin-right:20px; margin-left:20px;'>";
	ASSIST_HELPER::displayResult(array("info","The list of Financial Years feeds into many modules within Assist. Care should be taken to ensure that start and end dates are correct as they have critical implications in other modules and mistakes can not be fixed once faulty dates have been used."));
	echo "</div>";
}

?>
<form id=frm_add name=frm_add method=post action=<?php echo $page; ?>>
	<input type=hidden name=act value='ADD'/>
	<table id=tbl_add>
		<tr>
			<th>Ref</th>
			<th>Name</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Financial<br/>System Ref*</th>
			<th>&nbsp;</th>
		</tr>
		<tr>
			<th></th>
			<td><input type=text name=value id=val size=40 maxlength='40'/></td>
			<td class="center"><?php $js .= $displayObject->drawFormField("DATE", array('id' => "sdate", 'name' => "start_date")); ?></td>
			<td class="center"><?php $js .= $displayObject->drawFormField("DATE", array('id' => "edate", 'name' => "end_date")); ?></td>
			<td class="center"><input type=text name=fin_ref id=fin_ref size=10 maxlength='40'/></td>
			<td class=center><input type=button id=add value=Add class=isubmit></td>
		</tr>
		<?php
		$values = $me->getActiveObjects();
		//[S Dumalisile]AA-533 Financial Years - display warning message when years overlap
		$setup_object = new S();

		foreach($values as $row) {
			$outcome = array();
			$overlapping = false;
			$fin_ref = isset($row['fin_ref']) ? $row['fin_ref'] : "";
			$value = isset($row['value']) ? $row['value'] : "";
			$var = array('start_date' => $row['start_date'], 'end_date' => $row['end_date'], 'id' => $row['id'], 'fin_ref' => $fin_ref, 'value' => $value);
			$outcome = $setup_object->checkFinancialYearOverlap($var);
			$financial_year = isset($outcome['FY']) ? $outcome['FY'] : "N/A";
			$existing_financial_year = isset($outcome['existing_fy']) ? $outcome['existing_fy'] : "N/A";
			$start_date = isset($outcome['start_date']) ? $outcome['start_date'] : "N/A";
			$value = isset($outcome['value']) ? $outcome['value'] : "N/A";
			$existing_value = isset($outcome['existing_value']) ? $outcome['existing_value'] : "N/A";
			$new_date = isset($outcome['new_date']) ? $outcome['new_date'] : "N/A";
			$existing_date = isset($outcome['existing_date']) ? $outcome['existing_date'] : "N/A";
			$date_type = isset($outcome['date_type']) ? $outcome['date_type'] : "N/A";

			if($outcome[0] == "overlapping") {
				$overlapping = true;
			}

			echo "<tr>
			<th>".$row['id']."</th>
			<td id=".$row['id']."_val>".$helper->decode($row['value'])."</td>
			<td id=".$row['id']."_sdate class=center>".date("d-M-Y", strtotime($row['start_date']))."</td>
			<td id=".$row['id']."_edate class=center>".date("d-M-Y", strtotime($row['end_date']))."</td>
			<td id=".$row['id']."_fr>".$helper->decode($fin_ref)."</td>
			<td class=center><input type=button class=edit value=Edit id=".$row['id']." /></td>
		</tr>";

			if($overlapping == true) {
				echo "
				<tr>
					<td colspan='6' class='date-overlap'><div class=div-date-overlap>".ASSIST_HELPER::getDisplayIconAsDiv("info", "", "orange")."<span class=b>Warning:</span> This financial year overlaps with financial year ".$value." (".$financial_year.").
					The ".$value." ".$date_type." of ".date("d-M-Y", strtotime($existing_date))." falls within the start/end dates given for ".$existing_value." (".$existing_financial_year.").</div></td>
				</tr>
				";
			}
		}

		?>
	</table>
	<p class=i>* Only required if third-party integration is used. This value must match the reference used in the financial system to identify the financial year when sending financials to Assist.</p>
</form>
<div id=dlg_edit title="Edit <?php echo $title; ?>">
	<h2>Edit <?php echo $title; ?></h2>
	<form id=frm_edit name=frm_edit method=post action=<?php echo $page; ?>>
		<input type=hidden name=act value=EDIT id='hdn_act'/>
		<input type=hidden name=id value="" id='hdn_ref'/>
		<input type=hidden name=assist value="" id='sel_assist'/>
		<table id=tbl_edit width=90%>
			<tr>
				<th>Reference:</th>
				<td id=td_ref></td>
			</tr>
			<tr>
				<th>Name:</th>
				<td><input type=text value="" id=txt_value name=value size=10 maxlength=40/></td>
			</tr>
			<tr>
				<th>Start Date:</th>
				<td><?php $js .= $displayObject->drawFormField("DATE", array('id' => "txt_sdate", 'name' => "start_date")); ?></td>
			</tr>
			<tr>
				<th>End Date:</th>
				<td><?php $js .= $displayObject->drawFormField("DATE", array('id' => "txt_edate", 'name' => "end_date")); ?></td>
			</tr>
			<tr>
				<th>Financial Services Ref:</th>
				<td><input type=text value="" id=txt_fin_ref name=fin_ref size=10 maxlength=40/></td>
			</tr>
			<tr>
				<th></th>
				<td><input type=button value="Save Changes" class='isubmit'/> <span class=float><input type=button value=Delete class='idelete'/></span></td>
			</tr>
		</table>
	</form>
</div>

<div id="fy_warning" style="display:none">
	<h3>Warning!</h3>

</div>
<div id="dialog_btn">
	<br><input type='button' id='btn_dialog' value='' hidden />
</div>
<script type=text/javascript>
	$(document).ready(function () {
		<?php echo $js; ?>
		$('#btn_dialog').click(function () {
			$('.isubmit').prop('disabled', true);

			$("#fy_warning").show().dialog({
				modal: true,
				buttons: [{
					id: "cont",
					text: "Continue",
					click: function () {
						AssistHelper.processing();
						var action = $(this).attr("class"); //gets class which has action set via prop().
						var actionit = action.substring(0, 3);
						if (actionit == "ADD") {
							$("form[name=frm_add]").submit();
						} else if (actionit == "EDI") {
							$("form[name=frm_edit]").submit();
						} else {
							alert("Not sure which action to execute, please refresh page and try again.");
						}
					}
				}, {
					text: "Cancel",
					click: function () {
						$(this).dialog("close");
					}
				}]
			});
			AssistHelper.hideDialogTitlebar("id", "fy_warning");
		});

		$("td.date-overlap").each(function () {
			$(this).find("div:first").css({
				"border-radius": "5px",
				"padding": "5px"
			}).addClass("ui-icon-info").addClass("ui-state-info");
		});

		$("#dlg_edit").dialog({
			autoOpen: false,
			modal: true,
			width: 500,
			height: 300
		});
		$("#tbl_add tr:gt(0)").find("td:eq(3)").addClass("center");
		$("#add").click(function (e) {
			e.preventDefault();
			AssistHelper.processing();
			var $form = $("form[name=frm_add]");
			$("#fy_warning").removeClass("EDIT").prop("class", "ADD");
			$("#tbl_add input:text").removeClass("required");
			var err = false;
			var v = "";
			$("#tbl_add input:text").each(function () {
				v = $(this).val();
				if (v.length == 0) {
					$(this).addClass("required");
					err = true;
				}
			});
			if (!err) {
				var dta = AssistForm.serialize($form);
				var result = AssistHelper.doAjax("inc_controller.php?action=SETUP.OVERLAPCHECK", dta);
				//console.log(dta);
				if (result[0] == "overlapping") {
					$("#fy_warning").empty().append("<h3>Warning!</h3>");
					$("#fy_warning").append("<p>This financial year overlaps with financial year " + result['value'] + " (" + result['FY'] + ").</p>");
					$("#fy_warning").append("<p>The " + result['date_type'] + " of " + result['existing_date'] + " (" + result['FY'] + ") falls within the start/end date given for " + result['existing_value'] + " (" + result['existing_fy'] + ").</p><br><br>");
					AssistHelper.closeProcessing();
					$("#btn_dialog").trigger("click");
				} else if (result[0] == "not-overlapping") {
					AssistHelper.closeProcessing();
					$("#frm_add").submit();
				} else {
					AssistHelper.finishedProcessing(result[0], result[1]);
				}


			} else {
				AssistHelper.closeProcessing();
				alert("Please complete the missing information as highlighted in red.");
			}
		});
		$(".edit").click(function () {
			var id = $(this).attr("id");
			var val = $("#" + id + "_val").prop("innerText");
			var fr = $("#" + id + "_fr").prop("innerText");
			var sdate = $("#" + id + "_sdate").prop("innerText");
			var edate = $("#" + id + "_edate").prop("innerText");
			if (confirm("Warning: Editing a <?php echo $title; ?> will affect all records that are associated with this <?php echo $title; ?>.\n\nAre you sure you wish to continue editing '" + val + "'?") == true) {
				$("#dlg_edit #td_ref").html(id);
				$("#dlg_edit #hdn_ref").val(id);
				$("#dlg_edit #txt_value").val(val);
				$("#dlg_edit #txt_fin_ref").val(fr);
				$("#dlg_edit #txt_sdate").val(sdate);
				$("#dlg_edit #txt_edate").val(edate);
				$("#dlg_edit #sel_assist").val($("#" + id + "_assist").val());
				$("#dlg_edit").dialog("open");
			}
		});
		$("#dlg_edit input:button").click(function (e) {
			if ($(this).hasClass("idelete")) {
				if (confirm("Are you sure you wish to DELETE this <?php echo $title; ?>?")) {
					$("#dlg_edit #hdn_act").val("DEL");
					$("form[name=frm_edit]").submit();
				}
			} else {
				e.preventDefault();
				AssistHelper.processing();
				var $form = $("form[name=frm_edit]");
				$("#fy_warning").removeClass("ADD").prop("class", "EDIT");

				$("#dlg_edit input:text").removeClass("required");
				var err = false;
				var v = "";
				$("#dlg_edit input:text").each(function () {
					v = $(this).val();
					if (v.length == 0) {
						$(this).addClass("required");
						err = true;
					}
				});
				if (!err) {
					var dta = AssistForm.serialize($form);
					var result = AssistHelper.doAjax("inc_controller.php?action=FINYR.OVERLAPCHECK", dta);
					$("#dlg_edit #hdn_act").val("EDIT");
					//console.log(result[0]);
					if (result[0] == "overlapping") {
						$("#fy_warning").empty().append("<h3>Warning!</h3>");
						$("#fy_warning").append("<p>This financial year overlaps with financial year " + result['value'] + " (" + result['FY'] + ").</p>");
						$("#fy_warning").append("<p>The " + result['date_type'] + " of " + result['existing_date'] + " (" + result['FY'] + ") falls within the start/end date given for " + result['existing_value'] + " (" + result['existing_fy'] + ").</p><br><br>");
						AssistHelper.closeProcessing();
						$("#btn_dialog").trigger("click");
					} else if (result[0] == "not-overlapping") {
						AssistHelper.closeProcessing();
						$("form[name=frm_edit]").submit();
					} else {
						AssistHelper.finishedProcessing(result[0], result[1]);
					}

				} else {
					AssistHelper.closeProcessing();
					alert("Please complete the highlighted missing information.");
				}
			}
		});
	});
</script>
<?php
//displayGoBack("main.php","");

$log_sql = "SELECT l.date, l.tkname, l.transaction 
			FROM assist_".$helper->getCmpCode()."_master_log l
			WHERE l.active = true AND l.section = '$section' 
			ORDER BY l.id DESC";
$me->displayAuditLogOldStyle($log_sql, array('date' => "date", 'user' => "tkname", 'action' => "transaction"), $me->getGoBack("main.php"));
//include("inc_hr.php");
?>
<!-- 
<a name=loc></a>
<h1>Default Assist <?php echo $title; ?></h1>
	<table>
		<tr>
			<th><?php echo $title; ?></th>
		</tr>
<?php
foreach($assist as $row) {
	echo "	<tr>
			<td>".$row["value"]."</td>
		</tr>";
}
?>
	</table>
<p><img src=../pics/tri_up.gif border=0 align=absmiddle>&nbsp;<a href=#top>Top</a></p> -->
<?php
//displayGoBack("main.php");
?>
<p>&nbsp;</p>

</body>

</html>
