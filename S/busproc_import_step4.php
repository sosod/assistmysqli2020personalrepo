<?php
/**
 * @var S_BUSPROC $segmentObject - from calling page
 * @var string $section - from calling page
 * @var int $max_rows_to_import - from calling page
 * @var string $my_page - from calling page
 * @var string $original_url - from calling page
 * @var int $current_version_displayed - from inc_header via calling page
 * @var string $name_divider - from inc_header via calling page
 */

//ASSIST_HELPER::arrPrint($_REQUEST);
$import_ref = $_REQUEST['import_ref'];
$sql = "SELECT * FROM ".$segmentObject->getDBRef()."_segment_import WHERE import_ref = '".$import_ref."'";
$data = $segmentObject->mysql_fetch_all_by_id($sql, "import_key");

if(count($data) > 0) {

	$parent_ids_by_import_key = array();

	$copy_fields = array(
		"parent_id", "ref", "name", "description", "nickname", "line_version", "has_child", "can_post"
	);

	echo "<p class=b>Importing:<ul>";
	foreach($data as $key => $row) {
		echo "<li>Row ".$key."...";
		if($row['parent_source'] == "NEW") {
			$row['parent_id'] = $parent_ids_by_import_key[$row['parent_id']];
		}
		$import_data = array();
		foreach($copy_fields as $fld) {
			$import_data[$fld] = $row[$fld];
		}

		$import_data['status'] = MSCOA1::ACTIVE;
		$import_data['insertdate'] = date("Y-m-d H:i:s");
		$import_data['insertuser'] = $segmentObject->getUserID();

		$sql = "INSERT INTO ".$segmentObject->getTableName()." SET ".$segmentObject->convertArrayToSQLForSave($import_data);
		$id = $segmentObject->db_insert($sql);
		$parent_ids_by_import_key[$key] = $id;

		echo "done.</li>";

	}
	echo "</ul>
	";
	$sql = "UPDATE ".$segmentObject->getDBRef()."_segment_import SET import_status = 1 WHERE import_ref = '".$import_ref."'";
	$segmentObject->db_update($sql);
	//ASSIST_HELPER::displayResult(array("ok","Import completed successfully."));

	echo "
	<script type=text/javascript>
	document.location.href = '".$original_url."?section=".$section."&r[]=ok&r[]=Import completed successfully.';
	</script>";

} else {
	ASSIST_HELPER::displayResult(array("error", "No data found to import - the holding table does not contain any data for reference: ".$import_ref.".  Please try again otherwise contact your Business Partner."));

}
?>