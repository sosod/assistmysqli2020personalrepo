<?php
/**
 * @var S_DISPLAY $displayObject - from inc_header
 * @var S $helper - from inc_header
 * @var string $js - from inc_header - variable to hold any javascript/jquery code until it can be echoed at the end
 */


$action = "VIEW";
$head_dir = isset($head_dir) && strlen($head_dir) > 0 ? $head_dir : $_SESSION['cmp_terminology']['DEPT'];
$head_sub = $_SESSION['cmp_terminology']['SUB'];
$section = "DIR";
$section2 = "SUB";
$title = "Organisational Structure";
$page_heading = array("main.php" => "Master Setups", 2 => $title);

include("inc_header.php");

$self = "dir.php";
$me = new S_MASTER();
$cmpcode = $me->getCmpCode();
$dbref = "assist_".$cmpcode."_list";

$result = (isset($_REQUEST['r'])) ? $_REQUEST['r'] : array();

if(isset($_REQUEST['action'])) {
	switch($_REQUEST['action']) {
		case "EDIT-DIR":
			$value = ASSIST_HELPER::code($_REQUEST['dir_value']);
			$id = $_REQUEST['dir_id'];
			if(ASSIST_HELPER::checkIntRef($id)) {
				$sql = "SELECT dirtxt as value FROM ".$dbref."_dir WHERE dirid = ".$id;
				$r = $helper->mysql_fetch_one($sql);
				$sql = "UPDATE ".$dbref."_dir SET dirtxt = '".$value."' WHERE dirid = '".$id."'";
				$mar = $helper->db_update($sql);
				$r_value = isset($r['value']) ? $r['value'] : "*";
				if($mar > 0) {
					$v_log = array(
						'sec' => $section,
						'ref' => $id,
						'action' => "E",
						'fld' => "dirtxt",
						'trans' => addslashes("Changed ".$head_dir." $id name to '".$value."' from '".$r_value."'."),
						'old' => isset($r['value']) ? $r['value'] : "",
						'new' => $value,
						'sql' => addslashes($sql),
					);
					$me->createLog($v_log);
					$result = array("ok", "".$head_dir." updated.");
				} else {
					$result = array("info", "No change found to be made.");
				}
			} else {
				$result = array("error", "An error occurred.  Please try again.");
			}
			break;
		case "ADD-DIR":
			$value = ASSIST_HELPER::code($_REQUEST['dir_value']);
			$subs = $_REQUEST['sub_value'];
			if(strlen($value) > 0) {
				$sql = "SELECT max(dirsort) as maxsort FROM ".$dbref."_dir WHERE diryn = 'Y'";
				$rsort = $helper->mysql_fetch_one($sql);
				$sort = $rsort['maxsort'] + 1;
				$sql = "INSERT INTO ".$dbref."_dir (dirid, dirtxt, diryn, dirsort) VALUES (null,'$value','Y',$sort)";
				$id = $helper->db_insert($sql);
				$res = array();
				if($helper->checkIntRef($id)) {
					$v_log = array(
						'sec' => $section,
						'ref' => $id,
						'action' => "C",
						'fld' => "",
						'trans' => addslashes("Added new ".$head_dir." '".$value."'."),
						'old' => "",
						'new' => "",
						'sql' => addslashes($sql),
					);
					$me->createLog($v_log);
					$res = "".$head_dir." '".$value."' ";
					$s = 1;
					foreach($subs as $svalue) {
						if(strlen($svalue) > 0) {
							$svalue = $helper->code($svalue);
							$sort = $s;
							if($s > 1) {
								$head = "N";
							} else {
								$head = "Y";
							}
							$sql = "INSERT INTO ".$dbref."_dirsub (subid, subtxt, subyn, subsort, subhead, subdirid) VALUES (null, '$svalue','Y',$sort,'$head',$id)";
							$sid = $helper->db_insert($sql);
							if($helper->checkIntRef($sid)) {
								$v_log = array(
									'sec' => $section2,
									'ref' => $sid,
									'action' => "C",
									'fld' => "",
									'trans' => addslashes("Added new ".$head_sub." '".$svalue."' to ".$head_dir." ".$id."."),
									'old' => "",
									'new' => "",
									'sql' => addslashes($sql),
								);
								$me->createLog($v_log);
								$s++;
							}
						}
					}
					if($s > 1) {
						$res .= "and associated ".$head_sub."s have been added.";
					} else {
						$res .= " has been added.";
					}
					$result = array("ok", $res);
				} else {
					$result = array("error", "An error occurred while trying to add ".$head_dir." '$value'.");
				}
			} else {
				$result = array("info", "No ".$head_dir." name given.");
			}
			break;
		case "DELETE-DIR":
			$id = $_REQUEST['dir_id'];
			if($helper->checkIntRef($id)) {
				$sql = "UPDATE ".$dbref."_dir SET diryn = 'N', dirsort = dirsort + 1000 WHERE dirid = ".$id;
				$mar = $helper->db_update($sql);
				if($mar > 0) {
					$v_log = array(
						'sec' => $section,
						'ref' => $id,
						'action' => "D",
						'fld' => "diryn",
						'trans' => addslashes("Deactivated ".$head_dir." '".$id."'."),
						'old' => "Y",
						'new' => "N",
						'sql' => addslashes($sql),
					);
					$me->createLog($v_log);
					$result = array("ok", "".$head_dir." ".$id." has been deactivated.");
				} else {
					$result = array("error", "An error occurred while trying to deactivate ".$head_dir." ".$id.".");
				}
			} else {
				$result = array("error", "An error occurred while trying to deactivate ".$head_dir." ".$id.".");
			}
			break;
		case "RESTORE-DIR":
			$id = $_REQUEST['dir_id'];
			if($helper->checkIntRef($id)) {
				$sql = "UPDATE ".$dbref."_dir SET diryn = 'Y', dirsort = dirsort - 1000 WHERE dirid = ".$id;
				$mar = $helper->db_update($sql);
				if($mar > 0) {
					$v_log = array(
						'sec' => $section,
						'ref' => $id,
						'action' => "R",
						'fld' => "diryn",
						'trans' => addslashes("Restored ".$head_dir." ".$id."."),
						'old' => "N",
						'new' => "Y",
						'sql' => addslashes($sql),
					);
					$me->createLog($v_log);
					$result = array("ok", "".$head_dir." ".$id." has been restored.");
				} else {
					$result = array("error", "An error occurred while trying to restore ".$head_dir." ".$id.".");
				}
			} else {
				$result = array("error", "An error occurred while trying to restore ".$head_dir." ".$id.".");
			}
			break;
		case "EDIT-SUB":
			$value = $helper->code($_REQUEST['sub_value']);
			$id = $_REQUEST['sub_id'];
			$sub_head = $_REQUEST['sub_head'];
			$dir_id = $_REQUEST['dir_id'];
			if($helper->checkIntRef($id) && strlen($value) > 0) {
				$sql = "SELECT subtxt as value FROM ".$dbref."_dirsub WHERE subid = ".$id;
				$r = $helper->mysql_fetch_one($sql);
				$r_value = isset($r['value']) ? $r['value'] : "*";
				if($sub_head == "true") {
					$sql = "SELECT * FROM ".$dbref."_dirsub WHERE subdirid = ".$dir_id." ORDER BY subhead DESC, subsort DESC";
					//$rs = getRS($sql);
					$pri = $helper->mysql_fetch_one($sql);
					if($helper->db_get_num_rows($sql) > 1) {
						$rb = $helper->mysql_fetch_one($sql);
					} else {
						$rb = $pri;
					}
					$rb_sort = isset($rb['sort']) ? $rb['sort'] : 0;
					$sort_head = $rb_sort + 1;
					$sql = "UPDATE ".$dbref."_dirsub SET subhead = 'N', subsort = ".$sort_head." WHERE subhead = 'Y' AND subdirid = ".$dir_id;
					$mar = $helper->db_update($sql);
					$pri_id = isset($pri['id']) ? $pri['id'] : 0;
					if($mar > 0) {
						$v_log = array(
							'sec' => $section2,
							'ref' => $id,
							'action' => "E",
							'fld' => "subhead",
							'trans' => addslashes("Removed ".$head_sub." ".$pri_id." as Primary for ".$head_dir." ".$dir_id."."),
							'old' => "Y",
							'new' => "N",
							'sql' => addslashes($sql),
						);
						$me->createLog($v_log);
						if($value != $r_value) {
							$sql = "UPDATE ".$dbref."_dirsub SET subtxt = '".$value."', subhead = 'Y', subsort = 1 WHERE subid = ".$id;
							$v_log = array(
								'sec' => $section2,
								'ref' => $id,
								'action' => "E",
								'fld' => "",
								'trans' => addslashes("Set ".$head_sub." $id as Primary for ".$head_dir." ".$dir_id."<br />Changed the name to '".$value."' from '".$r_value."'."),
								'old' => "",
								'new' => "",
								'sql' => addslashes($sql),
							);
							$me->createLog($v_log);
						} else {
							$sql = "UPDATE ".$dbref."_dirsub SET subhead = 'Y', subsort = 1 WHERE subid = ".$id;
							$v_log = array(
								'sec' => $section2,
								'ref' => $id,
								'action' => "E",
								'fld' => "subhead",
								'trans' => "Set ".$head_sub." $id as Primary for ".$head_dir." ".$dir_id.".",
								'old' => "N",
								'new' => "Y",
								'sql' => addslashes($sql),
							);
							$me->createLog($v_log);
						}
					} else {
						$result = array("error", "An error occurred while trying to change the Primary ".$head_sub.".");
						break;
					}
				} else {
					$sql = "UPDATE ".$dbref."_dirsub SET subtxt = '".$value."' WHERE subid = ".$id;
					$v_log = array(
						'sec' => $section2,
						'ref' => $id,
						'action' => "E",
						'fld' => "subtxt",
						'trans' => addslashes("Changed ".$head_sub." $id name to '".$value."' from '".$r_value."'."),
						'old' => $r_value,
						'new' => $value,
						'sql' => addslashes($sql),
					);
					$me->createLog($v_log);
				}
				$mar = $helper->db_update($sql);
				if($mar > 0) {
					$me->createLog($v_log);
					$result = array("ok", "".$head_sub." updated.");
				} else {
					$result = array("info", "No change found to be made.");
				}
			} else {
				$result = array("error", "An error occurred.  Please try again.");
			}
			break;
		case "ADD-SUB":
			$value = ASSIST_HELPER::code($_REQUEST['sub_value']);
			$dir_id = $_REQUEST['dir_id'];
			if(strlen($value) > 0 && ASSIST_HELPER::checkIntRef($dir_id)) {
				$sql = "SELECT max(subsort) as maxsort FROM ".$dbref."_dirsub WHERE subdirid = ".$dir_id." AND subyn = 'Y'";
				$row = $helper->mysql_fetch_one($sql);
				if($row['maxsort'] == 0) {
					$sort = 1;
					$head = "Y";
				} else {
					$sort = $row['maxsort'] + 1;
					$head = "N";
				}
				$sql = "INSERT INTO ".$dbref."_dirsub (subid, subtxt, subyn, subsort, subhead, subdirid) VALUES (null,'".$value."','Y',$sort,'$head',".$dir_id.")";
				$id = $helper->db_insert($sql);
				if($helper->checkIntRef($id)) {
					$v_log = array(
						'sec' => $section2,
						'ref' => $id,
						'action' => "C",
						'fld' => "dirsub",
						'trans' => addslashes("Added new ".$head_sub." '".$value."' to ".$head_dir." ".$dir_id."."),
						'old' => "",
						'new' => $value,
						'sql' => addslashes($sql),
					);
					$me->createLog($v_log);
					$result = array("ok", "".$head_sub." '".$value."' has been added.");
				} else {
					$result = array("error", "An error occurred while trying to add ".$head_sub." '".$value."'");
				}
			} else {
				$result = array("info", "No ".$head_sub." name given.");
			}
			break;
		case "DELETE-SUB":
			$id = $_REQUEST['sub_id'];
			if(ASSIST_HELPER::checkIntRef($id)) {
				$sql = "UPDATE ".$dbref."_dirsub SET subyn = 'N', subsort = subsort + 1000 WHERE subid = ".$id;
				$mar = $helper->db_update($sql);
				if($mar > 0) {
					$v_log = array(
						'sec' => $section2,
						'ref' => $id,
						'action' => "D",
						'fld' => "subyn",
						'trans' => "Deactivated ".$head_sub." ".$id.".",
						'old' => "Y",
						'new' => "N",
						'sql' => addslashes($sql),
					);
					$me->createLog($v_log);
					$result = array("ok", "".$head_sub." ".$id." has been deactivated.");
				} else {
					$result = array("error", "An error occurred while trying to deactivate ".$head_sub." ".$id.".");
				}
			} else {
				$result = array("error", "An error occurred while trying to deactivate ".$head_sub." ".$id.".");
			}
			break;
		case "RESTORE-SUB":
			$id = $_REQUEST['sub_id'];
			if(ASSIST_HELPER::checkIntRef($id)) {
				$sql = "UPDATE ".$dbref."_dirsub SET subyn = 'Y', subsort = subsort - 1000 WHERE subid = ".$id;
				$mar = $helper->db_update($sql);
				if($mar > 0) {
					$v_log = array(
						'sec' => $section2,
						'ref' => $id,
						'action' => "R",
						'fld' => "subyn",
						'trans' => "Restored ".$head_sub." ".$id.".",
						'old' => "N",
						'new' => "Y",
						'sql' => addslashes($sql),
					);
					$me->createLog($v_log);
					$result = array("ok", "".$head_sub." ".$id." has been restored.");
				} else {
					$result = array("error", "An error occurred while trying to restore ".$head_sub." ".$id.".");
				}
			} else {
				$result = array("error", "An error occurred while trying to restore ".$head_sub." ".$id.".");
			}
			break;
	}
} else {

	$sql = "SHOW TABLES LIKE '".$dbref."_dir'";
	//$rs = getRS($sql);
	if($helper->db_get_num_rows($sql) == 0) {
		$sql = "CREATE TABLE IF NOT EXISTS ".$dbref."_dir (dirid int(10) unsigned NOT NULL AUTO_INCREMENT,  dirtxt varchar(100) NOT NULL DEFAULT '',  dirsort int(10) unsigned NOT NULL,  diryn varchar(1) NOT NULL DEFAULT 'Y',  PRIMARY KEY (dirid)) ENGINE=InnoDB  DEFAULT CHARSET=latin1";
		//getRS($sql);
		$helper->db_query($sql);

		$sql = "SHOW TABLES LIKE 'assist_".$cmpcode."_sdp10_dir'";
		//$rs = getRS($sql);
		if($helper->db_get_num_rows($sql) > 0) {
			$sql = "INSERT INTO `".$dbref."_dir` (dirid,dirtxt,dirsort,diryn) SELECT dirid, dirtxt, dirsort, diryn FROM assist_".$cmpcode."_sdp10_dir";
			$helper->db_insert($sql);
		}
	}


	$sql = "SHOW TABLES LIKE '".$dbref."_dirsub'";
	//$rs = getRS($sql);
	if($helper->db_get_num_rows($sql) == 0) {
		$sql = "CREATE TABLE IF NOT EXISTS ".$dbref."_dirsub (  subid int(10) unsigned NOT NULL AUTO_INCREMENT,  subtxt varchar(100) NOT NULL DEFAULT '',  subyn varchar(1) NOT NULL DEFAULT 'Y',  subdirid int(10) unsigned NOT NULL,  subsort int(10) unsigned NOT NULL,  subhead varchar(1) NOT NULL DEFAULT 'N',  PRIMARY KEY (subid)) ENGINE=InnoDB  DEFAULT CHARSET=latin1";
		//getRS($sql);
		$helper->db_query($sql);

		$sql = "SHOW TABLES LIKE 'assist_".$cmpcode."_sdp10_dirsub'";
		//$rs = getRS($sql);
		if($helper->db_get_num_rows($sql) > 0) {
			$sql = "INSERT INTO `".$dbref."_dirsub` (subid,subtxt,subyn,subdirid,subsort,subhead) SELECT subid,subtxt,subyn,subdirid,subsort,subhead FROM assist_".$cmpcode."_sdp10_dirsub";
			$helper->db_insert($sql);
		}
	}


}


ASSIST_HELPER::displayResult($result);

?>
<p><input type=button value=Add class=dir-add /> <input type=button value=Sort id=dir-sort /></p>
<?php
$sql = "SELECT dirid as id, dirtxt as value, dirsort as sort, diryn FROM ".$dbref."_dir ORDER BY dirsort";
$dir = $helper->mysql_fetch_all($sql);
foreach($dir as $i => $d) {
	if($d['diryn'] == "Y") {
		$dir[$i]['active'] = true;
	} else {
		$dir[$i]['active'] = false;
	}
}

$subs = $helper->mysql_fetch_all_fld2("SELECT subid as id, subtxt as value, subhead, subdirid as dirid, subsort, subyn FROM ".$dbref."_dirsub ORDER BY dirid, subsort", "dirid", "id");
foreach($subs as $i => $d) {
	foreach($subs[$i] as $a => $s) {
		if($s['subyn'] == "Y") {
			$subs[$i][$a]['active'] = 1;
		} else {
			$subs[$i][$a]['active'] = 0;
		}
		if($s['subhead'] == "Y") {
			$subs[$i][$a]['head'] = 1;
		} else {
			$subs[$i][$a]['head'] = 0;
		}
	}
}
?>
<table width=700>
	<tr>
		<th>Ref</th>
		<th><?php echo $_SESSION['cmp_terminology']['DEPT']; ?></th>
		<!-- <th>&nbsp;</th> -->
		<th><?php echo $_SESSION['cmp_terminology']['SUB']; ?></th>
	<tr>
		<?php
		foreach($dir as $d) {
			echo "<tr>";
			echo "<th ".(!($d['active']) ? "style=\"background-color: #555555;\"" : "").">".$d['id']."</th>";
			echo "<td class=\"top ".(!($d['active']) ? "inact" : "")."\"><label id=dir-".$d['id']."-value>".ASSIST_HELPER::decode($d['value'])."</label> <span class=float>";//</td>";
			//echo "<td class=\"center top\">";
			if($d['active']) {
				echo "<input type=button value=Edit class=dir-edit id=".$d['id']." />";
			} else {
				echo "<input type=button value=Restore id=".$d['id']." class=dirrestore />";
			}
			echo "</span></td>";
			echo "<td style=\"padding: 0 0 0 0;\"><table width=100% class=noborder style=\"background-color: #FFFFFF;\">";
			foreach($subs[$d['id']] as $s) {
				echo "<tr class=hover2>
						<th class=\"th2 noborder\" ".(!$s['active'] || !$d['active'] ? "style=\"background-color: #999999;\"" : "")." width=30>".$s['id']."</th>
						<td class=\"noborder ".(!$s['active'] || !$d['active'] ? "inact2" : "")."\">".($s['head'] ? "<i>" : "")."<label id=sub-".$s['id']."-value>".ASSIST_HELPER::decode($s['value'])."</label></i></td>
						<td class=\"right noborder ".(!$s['active'] || !$d['active'] ? "inact2" : "")."\">";
				if($s['active'] && $d['active']) {
					if($s['head']) {
						echo "<input type=button value=Edit class=sub-edit-head id=".$s['id']." name=".$d['id']." />";
					} else {
						echo "<input type=button value=Edit class=sub-edit id=".$s['id']." name=".$d['id']." />";
					}
				} elseif(!$s['active']) {
					echo "<input type=button value=Restore id=".$s['id']." class=subrestore />";
				}
			}
			echo "<tr class=hover2><th height=19 class=\"".(!$d['active'] ? "inact" : "th2")."\" width=30></th><td colspan=2 class=\"noborder ".(!$d['active'] ? "inact" : "")."\">".($d['active'] ? "<input type=button value=Add class=sub-add name=".$d['id']." /> <input type=button value=Sort class=sub-sort name=\"".$d['id']."\" />" : "")."</td></tr></table></td>";
			echo "</tr>";
		}
		?>
</table>
<div id=dialog-dir-edit title="Edit <?php echo $head_dir; ?>">
	<form id=form-dir-edit action=<?php echo $self; ?> method=post><input type=hidden name=action value=EDIT-DIR />
		<table width=100%>
			<tr>
				<th class=left>Reference:</th>
				<td><input type=hidden name=dir_id value="" id=dir-edit-id><label id=dir-ref for=dir-edit-id></label></td>
			</tr>
			<tr>
				<th class=left>Old Value:</th>
				<td><label for=dir-edit-id id=dir-edit-old></label></td>
			</tr>
			<tr>
				<th class=left>New Values:</th>
				<td><input type=text value="" size=50 id=dir-edit-value name=dir_value /></td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td><input type=submit value="Save Changes" class=isubmit /><span class=float><input type=button value="Deactivate" class=idelete id=DIR /></span></td>
			</tr>
		</table>
		<p><input type=button value=Cancel id=cancel-dir-edit /></p>
	</form>
</div>
<div id=dialog-dir-add title="Add <?php echo $head_dir; ?>">
	<form id=form-dir-add action=<?php echo $self; ?> method=post><input type=hidden name=action value=ADD-DIR />
		<table width=100%>
			<tr>
				<th class=left><?php echo $head_dir; ?>:</th>
				<td><input type=text value="" size=50 id=dir-add-value name=dir_value /></td>
			</tr>
			<tr>
				<th class=left rowspan=5 valign=top><?php echo $head_sub; ?>:</th>
				<td><input type=text value="" size=50 name=sub_value[] id=s1 /> <small><i>(Primary)</i></small></td>
			</tr>
			<tr>
				<td><input type=text value="" size=50 name=sub_value[] id=s2 /></td>
			</tr>
			<tr>
				<td><input type=text value="" size=50 name=sub_value[] id=s3 /></td>
			</tr>
			<tr>
				<td><input type=text value="" size=50 name=sub_value[] id=s4 /></td>
			</tr>
			<tr>
				<td><input type=text value="" size=50 name=sub_value[] id=s5 /></td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td><input type=button value="Save Changes" class=isubmit id=submit-dir-add /></td>
			</tr>
		</table>
		<p><input type=button value=Cancel id=cancel-dir-add /></p>
	</form>
</div>


<div id=dialog-sub-edit title="Edit <?php echo $head_sub; ?>">
	<form id=form-sub-edit action=<?php echo $self; ?> method=post><input type=hidden name=action value=EDIT-SUB />
		<table width=100%>
			<tr>
				<th class=left>Reference:</th>
				<td><input type=hidden name=sub_id value="" id=sub-edit-id><label id=sub-ref for=sub-edit-id></label><input type=hidden name=dir_id value="" id=sub-dir-id></td>
			</tr>
			<tr>
				<th class=left>Old Value:</th>
				<td><label for=sub-edit-id id=sub-edit-old></label></td>
			</tr>
			<tr>
				<th class=left>New Values:</th>
				<td><input type=text value="" size=50 id=sub-edit-value name=sub_value /></td>
			</tr>
			<tr>
				<th class=left>Primary:</th>
				<td><select id=sub-edit-head name=sub_head>
						<option selected value=false>No</option>
						<option value=true>Yes</option>
					</select><label id=sub-edit-head-display></label></td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td><input type=submit value="Save Changes" class=isubmit /><span class=float><input type=button value="Deactivate" id=SUB class=idelete /></span></td>
			</tr>
		</table>
		<p><input type=button value=Cancel id=cancel-sub-edit /></p>
	</form>
</div>
<div id=dialog-sub-add title="Add <?php echo $head_sub; ?>">
	<form id=form-sub-add action=<?php echo $self; ?> method=post><input type=hidden name=action value=ADD-SUB />
		<table width=100%>
			<tr>
				<th class=left><?php echo $head_dir; ?>:</th>
				<td><input type=hidden name=dir_id value="" id=sub-add-id><label id=sub-add-dir></label></td>
			</tr>
			<tr>
				<th class=left><?php echo $head_sub; ?>:</th>
				<td><input type=text value="" size=50 id=sub-add-value name=sub_value /></td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td><input type=submit value="Save Changes" class=isubmit /></td>
			</tr>
		</table>
		<p><input type=button value=Cancel id=cancel-sub-add /></p>
	</form>
</div>


<script type=text/javascript>
	$(function () {
		$("#dialog-dir-edit").dialog({
			autoOpen: false,
			width: 500,
			height: "auto",
			modal: true
		});
		$("#dialog-dir-add").dialog({
			autoOpen: false,
			width: 550,
			height: "auto",
			modal: true
		});
		$("#dialog-sub-edit").dialog({
			autoOpen: false,
			width: 500,
			height: "auto",
			modal: true
		});
		$("#dialog-sub-add").dialog({
			autoOpen: false,
			width: 500,
			height: "auto",
			modal: true
		});
		$(".dir-add").click(function () {
			$("#dialog-dir-add").dialog("open");
		});
		$("#submit-dir-add").click(function () {
			var l = 0;
			var s = 0;
			var t = "";
			t = "#dir-add-value";
			var v = "";
			v = $(t).prop("value");
			if (v.length == 0) {
				alert("Please enter a <?php echo $head_dir; ?> name.");
			} else {
				for (s = 1; s <= 5; s++) {
					t = "#s" + s;
					v = $(t).prop("value");
					l += v.length;
				}
				if (l == 0) {
					alert("Please enter at least one <?php echo $head_sub; ?> to be used as the Primary.");
				} else {
					$("#form-dir-add").submit();
				}
			}
		});
		$(".dir-edit").click(function () {
			var id = $(this).prop("id");
			var t = "#dir-" + id + "-value";
			var v = $(t).prop("innerText");
			$("#dir-edit-id").prop("value", id);
			$("#dir-ref").prop("innerText", id);
			$("#dir-edit-value").prop("value", v);
			$("#dir-edit-value").focus();
			$("#dir-edit-old").prop("innerText", v);
			$("#dialog-dir-edit").dialog("open");
		});
		$(".sub-add").click(function () {
			var id = $(this).prop("name");
			var t = "#dir-" + id + "-value";
			var v = $(t).prop("innerText");
			$("#sub-add-id").prop("value", id);
			$("#sub-add-dir").prop("innerText", v);
			$("#sub-add-value").prop("value", "");
			$("#sub-add-value").focus();
			$("#dialog-sub-add").dialog("open");
		});
		$(".sub-edit").click(function () {
			var id = $(this).prop("id");
			var n = $(this).prop("name");
			var t = "#sub-" + id + "-value";
			var v = $(t).prop("innerText");
			$("#sub-dir-id").prop("value", n);
			$("#sub-edit-id").prop("value", id);
			$("#sub-ref").prop("innerText", id);
			$("#sub-edit-value").prop("value", v);
			$("#sub-edit-old").prop("innerText", v);
			$("#SUB").addClass("ireset");
			$("#SUB").prop("disabled", false);
			$("#sub-edit-head-display").prop("innerText", "");
			$("#sub-edit-head option").remove();
			$("#sub-edit-head").append("<option selected value=false>No</option><option value=true>Yes</option>");
			$("#sub-edit-head").show();
			$("#sub-edit-value").focus();
			$("#dialog-sub-edit").dialog("open");
		});
		$(".sub-edit-head").click(function () {
			var id = $(this).prop("id");
			var t = "#sub-" + id + "-value";
			var v = $(t).prop("innerText");
			var n = $(this).prop("name");
			$("#sub-dir-id").prop("value", n);
			$("#sub-edit-id").prop("value", id);
			$("#sub-ref").prop("innerText", id);
			$("#sub-edit-value").prop("value", v);
			$("#sub-edit-old").prop("innerText", v);
			$("#SUB").removeClass("ireset");
			$("#SUB").prop("disabled", true);
			$("#sub-edit-head-display").prop("innerText", "Yes");
			$("#sub-edit-head option").remove();
			$("#sub-edit-head").append("<option selected value=keep>Keep</option>");
			$("#sub-edit-head").hide();
			$("#dialog-sub-edit").dialog("open");
		});
		$("#cancel-dir-edit").click(function () {
			$("#dialog-dir-edit").dialog("close");
		});
		$("#cancel-dir-add").click(function () {
			$("#dialog-dir-add").dialog("close");
		});
		$("#cancel-sub-edit").click(function () {
			$("#dialog-sub-edit").dialog("close");
		});
		$("#cancel-sub-add").click(function () {
			$("#dialog-sub-add").dialog("close");
		});
		$(".idelete").click(function () {
			var typ = $(this).prop("id");
			switch (typ) {
				case "DIR":
					if (confirm("Are you sure you wish to deactivate this <?php echo $head_dir; ?>?")) {
						var id = $("#dir-edit-id").prop("value");
						document.location.href = "<?php echo $self; ?>?dir_id=" + id + "&action=DELETE-DIR";
					}
					break;
				case "SUB":
					if (confirm("Are you sure you wish to deactivate this <?php echo $head_sub; ?>?")) {
						var id = $("#sub-edit-id").prop("value");
						document.location.href = "<?php echo $self; ?>?sub_id=" + id + "&action=DELETE-SUB";
					}
					break;
			}
		});
		$(".subrestore").click(function () {
			var id = $(this).prop("id");
			document.location.href = "<?php echo $self; ?>?sub_id=" + id + "&action=RESTORE-SUB";
		});
		$(".dirrestore").click(function () {
			var id = $(this).prop("id");
			document.location.href = "<?php echo $self; ?>?dir_id=" + id + "&action=RESTORE-DIR";
		});
		$("#dir-sort").click(function () {
			document.location.href = "dir_sort.php?type=D";
		});
		$(".sub-sort").click(function () {
			var id = $(this).prop("name");
			document.location.href = "dir_sort.php?type=S&i=" + id;
		});
	});
</script>
<?php
$log_sql = "SELECT l.date, l.tkname, l.transaction 
			FROM assist_".$cmpcode."_setup_log l
			WHERE l.active = true AND (l.section = '$section' OR l.section = '$section2')
			ORDER BY l.id DESC";
$me->displayAuditLogOldStyle($log_sql, array('date' => "date", 'user' => "tkname", 'action' => "transaction"), $me->getGoBack("main.php"));
?>
</body>
</html>