<?php
//error_reporting(-1);

require_once("../module/autoloader.php");
$result = array("info","Sorry, I couldn't figure out what you wanted me to do.".serialize($_REQUEST));

/**
 * my_action[0] represents the class
 * my_action[1] represents the activity (add, edit etc)
 */
//echo "<pre>";print_r($_REQUEST);echo "</pre>";

$my_action = explode(".",$_REQUEST['action']);

$class = strtoupper($my_action[0]);
$activity = strtoupper($my_action[1]);
unset($_REQUEST['action']);

$page_direct = isset($_REQUEST['page_direct']) ? $_REQUEST['page_direct'] : false;
unset($_REQUEST['page_direct']);
//$has_attachments = (isset($_REQUEST['has_attachments']) && $has_attachments!=false && $has_attachments!="false") ? $_REQUEST['has_attachments'] : false;
$has_attachments = isset($_REQUEST['has_attachments']) ? $_REQUEST['has_attachments'] : false;
unset($_REQUEST['has_attachments']);
$attach = array();


$activity_log_handled_by_class = array("USERACCESS");

switch($class) {
	/**
	 * Module objects
	 */
	case "LOG":
		$object = new S_LOG();
		break;
	case "FINYR":
		$object = new S_FINANCIALYEARS();
		break;
	case "BUSPROC":
		$object = new S_BUSPROC();
		break;
	case "SETUP":
		$object = new S();
		break;

}
$result[2]=":".implode(".",$my_action).":";
$result[3] = array($class,$activity);



if(isset($object)) {
	switch($activity) {
		case "DELETE_ATTACH":
			$object_id = $_REQUEST['object_id'];
			$i = $_REQUEST['i'];
			$page_activity = $_REQUEST['activity'];
			$attach = $object->getAttachmentDetails($object_id,$i,$page_activity);
			$folder = $object->getParentFolderPath();
			$old = $folder."/".$attach['location']."/".$attach['system_filename'];
			$dest_folder = $object->getDeletedFolder();
			$object->checkFolder("deleted");
			if(file_exists($old)) {
				$new_path = $dest_folder."/".$object->getDeletedFileName($class,$attach['system_filename']);
				if(copy($old,$new_path)) {
					unlink($old);
				}
				//activity log
				$result = $object->deleteAttachment($object_id,$i,$page_activity);
				//$result = array("info","file_exists");
			} else {
				$result = array("error","File could not be found.  Please reload the page and try again.",$old);
			}
			//$result = array("error",$page_activity);
			break;
		case "GET_ATTACH_SIZE":
			$object_id = $_REQUEST['object_id'];
			$i = $_REQUEST['i'];
			$page_activity = $_REQUEST['activity'];
			
			$attach = $object->getAttachmentDetails($object_id,$i,$page_activity);
			$folder = $object->getParentFolderPath();
			$old = $folder."/".$attach['location']."/".$attach['system_filename'];
			$size = filesize($old);
			$FileSizeConverted = $object->FileSizeConvert($size);
			
			$result = $FileSizeConverted;
			break;
		case "GETLISTTABLEHTML":
			unset($_REQUEST['options']['set']);
			$button = $_REQUEST['button'];
			$data = $object->getObject($_REQUEST['options']);
			//$result = $data;
			//$result = array(serialize($data));
			$displayObj = new ADMIN2_DISPLAY();
			$display = $displayObj->getListTableRows($data, $button);
			$result = array('display'=>$display['display'],'paging'=>$data['paging'],'spage'=>serialize($data['paging']));
			break;
		case "GET":
			//if($class=="LOG") {
			//	$result = array("ok","log.get");
			//} else {
			$result = $object->getObject($_REQUEST);
			//}
			break;
		
		case "ADD":
			$data = $object->addObject($_REQUEST);
			if($data[0]=="ok") {
				if($has_attachments) {

				}
				$result = $data;
				/*if(!in_array($class,$activity_log_handled_by_class) && (!isset($data['add_log']) || $data['add_log']===true)) {
					$object->addActivityLog(strtolower($object->getMyLogTable()),$data['log_var']);
				}*/
			} else {
				$result = $data;
			}
			break;
		case "SIMPLEADD":
			$result = $object->addObject($_REQUEST);
			break;
		case "SIMPLEEDIT":
			$result = $object->editObject($_REQUEST,$attach);
			break;
		case "EDIT":  //$has_attachments COMMENTED OUT CAUSE USER CANNOT EDIT FILE. IF USER WANTS TO EDIT FILE THEY HAVE TO DELETE IT AND ADD AGAIN.
			/*if($has_attachments) {
				$res = processAttachments($activity, $object,array('object_id'=>$_REQUEST['object_id']));
				$attach = $res['attachments'];
			}*/
			$result = $object->editObject($_REQUEST);//editObject($_REQUEST,$attach);
			break;
		case "UPDATE":
			if($has_attachments) {
				$res = processAttachments($activity, $object,array('object_id'=>$_REQUEST['object_id']));
				$attach = $res['attachments'];
			}
			$result = $object->updateObject($_REQUEST,$attach);
			break;
		case "TERMINATE":
			$result = $object->terminateObject($_REQUEST);
			break;
		case "APPROVE":
			$result = $object->approveObject($_REQUEST);
			break;
		case "DECLINE":
			$result = $object->declineObject($_REQUEST);
			break;
		case "UNLOCK_APPROVE":
			$result = $object->unlockApprovedObject($_REQUEST);
			break;
		case "CONFIRM":
			$result = $object->confirmObject($_REQUEST);
			break;
		case "UNDOCONFIRM":
			$result = $object->undoConfirmObject($_REQUEST);
			break;
		case "ACTIVATE":
			$result = $object->activateObject($_REQUEST);
			break;
		case "UNDOACTIVATE":
			$result = $object->undoActivateObject($_REQUEST);
			break;
		case "UNLOCK":
			$result = $object->unlockObject($_REQUEST);
			break;
		case "DELETE":
			$result = $object->deleteObject($_REQUEST);
			break;
		case "DEACTIVATE":
			$result = $object->deactivateObject($_REQUEST);
			break;
		case "RESTORE":
			$result = $object->restoreObject($_REQUEST);
			break;
		case "RECIPIENTS":
			$result = $object->getObjectRecipients($_REQUEST);
			break;
		case "GETPROJECTDETAILCHILDROW":
			$tier = $_REQUEST['tier'];
			$object_type = $_REQUEST['object_type'];
			$project_id = $_REQUEST['project_id'];
			//$result = array('display'=>$tier);
			$result = $object->getProjectDetailChildRow($tier,$object_type,$project_id,$_REQUEST);
			break;
		case "VIEW":
			$result = $object->getRawObject($_REQUEST);
			break;
		case "OVERLAPCHECK":
			$result = $object->checkFinancialYearOverlap($_REQUEST);
			break;
		case "PAGELOAD":
			$result = $object->getDefaultAdminDetails();
		default:
			//$result[5] = "unknown activity - using call_user_func($class , $activity , ".implode(":",$_REQUEST).")";
			$result = call_user_func(array($object, $activity),$_REQUEST);
			break;
	}
} else {
	$result[4] = "no object set";
}
//echo json_encode(array_merge($_REQUEST,$result)); die();
//echo ":".$has_attachments.":";
//echo "<script type=text/javascript>window.parent.dialogFinished('".$result[0]."','".$result[1]."');</script>";
if($has_attachments == false) {
	//if($page_direct=="dialog") {
	//	echo "<script type=text/javascript>window.parent.parent.dialogFinished('".$result[0]."','".$result[1]."');</script>";
	//} else {
	echo json_encode($result);
	//}
} else {
	// echo json_encode($result);
	
	if($page_direct =="dialog") {
		echo "<script type=text/javascript> window.parent.dialogFinished('".$result[0]."','".$result[1]."');</script>";
		//echo "<script type=text/javascript>AssistHelper.processing(); AssistHelper.finishedProcessingWithRedirect('".$result[0]."','".$result[1]."','main.php');  </script>";
		
	} else {
		
		$page_direct.="r[]=".$result[0]."&r[]=".$result[1];
		//echo "java script redirect to: ".$page_direct;
		echo "<script type=text/javascript>window.parent.location.href = '".$page_direct."';</script>";
		//	$object->arrPrint($result);
	}
}









/*************** Attachment processing ******************/
/** STILL HAS HELPDESK ATTACHMENT SETTINGS - MUST FIX BEFORE ATTACHMENTS CAN BE USED HERE */
function processAttachments($activity,$object,$data) {
	$object_id = (isset($data['hdr_id']) ? $data['hdr_id'] : $data['object_id'] );
	$default_system_filename = $object_id."_".date("YmdHis")."_".($activity=="UPDATE" ? "update_" : "");
	$result = array();
	$original_filename = "";
	$system_filename = "";
	if(isset($_FILES)) {
		$result[] = "files are set";
		$save_folder = ($activity == 'UPDATE'? $object->getUpdateStorageFolder() : "HELPDESK/object");
		$object->checkFolder($save_folder);
		$result[] = $save_folder." folder check complete";
		$helpdesk_path = $object->getParentFolderPath()."/HELPDESK/object"; //the modref has to stay on HELPDESK
		$folder = ($activity == 'UPDATE'? $object->getFullUpdateFolderPath() : $helpdesk_path);
		$result[] = $folder;
		//Check for existing attachments
		switch($activity) {
			case "ADD":
			case "UPDATE":
			$i = 0;
			$current_attach = array();
			break;
			case "EDIT":
				$current_attach = $object->getAttachmentDetails($object_id,"all",$activity);
				if(count($current_attach)>0) {
					$keys = array_keys($current_attach);
					$i = max($keys);
					$i++;
				} else {
					$i = 0;
				}
				break;
		}
		$new_attach = array();
		/*$record = $object->getUpdateRecord($obj_id,$time_id);
		if(!isset($record['attachment']['attach']) || count($record['attachment']['attach'])==0) {
			$i = 0;
		} else {
			foreach($record['attachment']['attach'] as $i=>$a) {
				//
			}
			$i++;
		}*/
		$result[] = $i;
		foreach($_FILES['attachments']['tmp_name'] as $key => $tmp_name) {
			$x = array('id'=>$key,'tmp_name'=>$tmp_name);
			$x['error'] = $_FILES['attachments']['error'][$key];
			if($_FILES['attachments']['error'][$key] == 0) {
				//trim all spaces from front & back and then replace any spaces in filenames with _
				$original_filename = $object->normalizeOriginalFilenameForLinux($_FILES['attachments']['name'][$key]);//str_replace(" ","_",trim($_FILES['attachments']['name'][$key]));
				//remove all illegal characters from file name -> ALLOWED CHAR: A-Z(case insensitive) 0-9 . - _ ( ) [No spaces allowed]
				//$original_filename = preg_replace("/[^a-zA-Z0-9.-]/","",$original_filename);
				$ext = substr($original_filename, strrpos($original_filename, '.') + 1);
				//$parts = explode(".", $original_filename);
				//$file = $parts[0];
				$system_filename = $object->generateSystemFilenameWithExtension($original_filename,ADMIN2_CLIENT::getMyObjectType(),$object_id);//$default_system_filename.$i.".".$ext;
				$x['sys'] = $system_filename;
				$full_path = $folder."/".$system_filename;
				$x['path'] = $full_path;
				$new_attach[$i] = array('status'=>HELPDESK::ACTIVE,'location'=>$save_folder,'system_filename'=>$system_filename,'original_filename'=>$original_filename,'ext'=>$ext);
				$x['result'] = move_uploaded_file($_FILES['attachments']['tmp_name'][$key], $full_path);
				$i++;
				$result[] = $x;
			}
		}
		$result['attachments'] = $new_attach;
		if($activity != 'UPDATE'){
			$object->saveAttachments($activity,$object_id,array_merge($current_attach,$new_attach));
		}
	}
//$object->arrPrint($result);
	return $result;
}




function getSaveFolder($object) {
	return $object->getStorageFolder();
}

function getFullFolderPath($object) {
	$folder = $object->getFullFolderPath();
	return $folder;
}


function getDeletedFolder($object) {
	$folder = $object->getDeletedFolder();
}


function getParentFolderPath($object) {
	$folder = $object->getParentFolderPath();
	return $folder;
}
//echo "<pre>";print_r($_REQUEST);echo "</pre>";
?>