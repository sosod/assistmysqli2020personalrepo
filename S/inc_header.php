<?php
/**
 * @var array $page_heading - from calling page - what page are we on? array('link'=>"text");
 */

/**************
 * CODE TO MEASURE PAGE LOAD TIME
 * ******************/
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

$now = time();

function markTime($s) {
	global $start;
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	//echo '<p>'.$s.' => '.$total_time.' seconds.';

}


//error_reporting(-1);
require_once("../module/autoloader.php");
//marktime("autoloader");
/*********
 * Get file name to process breadcrumb path within module.
 */
$self = $_SERVER['PHP_SELF'];
$self = explode("/", $self);
$self = explode(".", end($self));
$page = $self[0];
if(isset($my_page)) {
	$page .= "_".$my_page;
}
$navigation_path = explode("_", $page); //ASSIST_HELPER::arrPrint($navigation_path);
//marktime("file name");
/********
 * Check for redirects
 * Note: Admin redirect is determined within the admin.php file as it requires a user access validation check
 */
/*$redirect = array(
);
if(isset($redirect[$page])) {
    header("Location:".$redirect[$page].".php");
}*/
//marktime("redirect");
/************
 * Process Navigation buttons and Breadcrumbs
 */
//$menuObject = new MSCOA1_MENU();
//$menu = $menuObject->getPageMenu($page);

//ASSIST_HELPER::arrPrint($_REQUEST);

//marktime("menu");
/***********
 * Start general header
 */

$headingObject = new S_HEADINGS();
$displayObject = new S_DISPLAY();
$nameObject = new S_NAMES();
$helper = new S();
$name_divider = $helper->getNameDivider();
//$segmentObject = new MSCOA1_SEGMENTS();

$version_list = array();
$version_list[1] = "Version 1";//TODO - make this flexible for later version options
$are_versions_active = false; //flag to enable functions later
$current_version_displayed = 1;
/*
//$abx check for dev to force update - remove in live
if(isset($abx) && isset($_SESSION[$helper->getModRef()]['versions']['expiration']) && $_SESSION[$helper->getModRef()]['versions']['expiration']>time()) {
	$version_list = $_SESSION[$helper->getModRef()]['versions']['list'];
	$default_version_to_display = 1;
	if(isset($_SESSION[$helper->getModRef()]['versions']['default_version_to_display']) && isset($version_list[$_SESSION[$helper->getModRef()]['versions']['default_version_to_display']])) {
		$default_version_to_display = $_SESSION[$helper->getModRef()]['versions']['default_version_to_display'];
	}
	if(isset($_REQUEST['mscoa_version_to_display']) && isset($version_list[$_REQUEST['mscoa_version_to_display']])) {
		$current_version_displayed = $_REQUEST['mscoa_version_to_display'];
	} elseif(isset($_SESSION[$helper->getModRef()]['versions']['current_version_displayed']) && isset($version_list[$_SESSION[$helper->getModRef()]['versions']['current_version_displayed']])) {
		$current_version_displayed = $_SESSION[$helper->getModRef()]['versions']['current_version_displayed'];
	} else {
		$current_version_displayed = $default_version_to_display;
	}
} else {
	$versionListObject = new S_LIST("version");
	$version_list = $versionListObject->getActiveListItemsFormattedForSelect("",true);
	$default_version_to_display = $version_list['current'];
	$version_list = $version_list['list'];
	if(isset($_REQUEST['mscoa_version_to_display']) && isset($version_list[$_REQUEST['mscoa_version_to_display']])) {
		$current_version_displayed = $_REQUEST['mscoa_version_to_display'];
	} elseif(isset($_SESSION[$helper->getModRef()]['versions']['current_version_displayed']) && isset($version_list[$_SESSION[$helper->getModRef()]['versions']['current_version_displayed']])) {
		$current_version_displayed = $_SESSION[$helper->getModRef()]['versions']['current_version_displayed'];
	} else {
		$current_version_displayed = $default_version_to_display;
	}
	unset($versionListObject);
	//store in session for 30 minutes
	$_SESSION[$helper->getModRef()]['versions'] = array(
			'expiration'=>time()+1800,
			'default_version_to_display'=>$default_version_to_display,
			'current_version_displayed'=>$current_version_displayed,
			'list'=>$version_list
	);
}
*/
$s = array("/".$helper->getModLocation()."/common/shelper.js?".time(), "/".$helper->getModLocation()."/common/s.js?".time());
if(isset($scripts)) {
	$scripts = array_merge(
		$s,
		$scripts
	);
} else {
	$scripts = $s;
}
ASSIST_HELPER::echoPageHeader("1.10.0", $scripts, array("/assist_jquery.css?".time(), "/assist3.css?".time()));
$js = ""; //to catch any js put out by the various display classes

marktime("general".__LINE__);
/*********
 * Module-wide Javascript
 */

?>
	<script type="text/javascript">
		$(document).ready(function () {
			//format container and sub-container tables to hide borders
			//$("table.tbl-container").addClass("noborder");
			$("table.tbl-container:not(.not-max)").css("width", "100%");
			//$("table.tbl-container td").addClass("noborder").find("table:not(.tbl-container) td").removeClass("noborder");
			$("table.th2").find("th").addClass("th2");
			$("table tr.th2").find("th").addClass("th2");
			$("table.tbl_audit_log").css("width", "100%").find("td").removeClass("noborder");
			$("table.tbl-subcontainer, table.tbl-subcontainer td").addClass("noborder");

			//$("select").css("padding","1px");

			//jquery ui button formatting
			$("button.abutton").children(".ui-button-text").css({"padding-top": "0px", "padding-bottom": "0px"});


			$("div.tbl-error").addClass("ui-state-error").find("h1").addClass("red");
			$("div.tbl-ok").addClass("ui-state-ok").find("h1").addClass("green");
			$("div.tbl-info").addClass("ui-state-info").find("h1").addClass("orange");
		});

	</script>

	<style type="text/css">
		/* jquery ui button styling */
		/* used by  setup > useraccess */
		#tbl_useraccess .button_class, #tbl_useraccess .ui-widget.button_class {
			font-size: 75%;
			padding: 1px;
		}

		table.tbl-container, table.tbl-container > tbody > tr > td {
			border: 0px solid #ffffff;
		}

		.small-button {
			font-size: 75%;
			padding: 1px;
		}


		/* tbl-ok/error styling - used by Create for user notifications */
		/* generic settings */
		table.tbl-ok, table.tbl-error, div.tbl-ok, div.tbl-error, div.tbl-info {
			-moz-border-radius: 10px;
			-webkit-border-radius: 10px;
			border-radius: 10px;
			margin: 50px auto;
			border-collapse: inherit;
		}

		table.tbl-ok td, table.tbl-error td {
			border: 0px solid #ffffff;
		}

		div.tbl-ok, div.tbl-error, div.tbl-info {
			padding: 0px 15px 10px 15px;
		}

		div.tbl-ok p, div.tbl-error p, div.tbl-info p {
			font-size: 150%;
			line-height: 165%;
		}


		/* green = ok */
		table.tbl-ok {
			border: 2px solid #009900;
		}

		div.tbl-ok {
		}

		/* red = error */
		table.tbl-error {
			border: 2px solid #009900;
		}

		div.tbl-error {
		}

		/* orange = info */
		table.tbl-info {
			border: 2px solid #fe9900;
		}

		div.tbl-info {
		}


		button.cancel_attach {
			font-size: 75%;
			padding: 1px;
		}

		table.attach {
			border: 0px solid #ffffff;
		}

		table.attach td {
			border: 0px solid #ffffff;
			vertical-align: middle;
			background: url();
		}

		/* Results colours */
		.result {
			font-weight: bold;
			color: #ffffff;
			padding: 2px 4px 2px 4px;
			text-align: center;
			margin: 0px;
		}

		.result0 {
			background-color: #777777;
		}

		.result1 {
			background-color: #cc0001;
		}

		.result2 {
			background-color: #fe9900;
		}

		.result3 {
			background-color: #009900;
		}

		.result4 {
			background-color: #005500;
		}

		.result5 {
			background-color: #000077;
		}

		/* Enable this to limit size of autocomplete select list - currently disabled after discussion with Louw/HES0001 on 8 Jan 2020 and agreed that full list easier to work with [JC] */
		/
		/
		.ui-autocomplete {
		/ / max-height: 200 px;
		/ / overflow-y: auto;
			/* prevent horizontal scrollbar */
		/ / overflow-x: hidden;
		/ /
		}

	</style>
<?php
marktime("module wide".__LINE__);
/**********
 * Navigation buttons
 */
if(!isset($no_page_heading) || !$no_page_heading) {
	$active_button_label = $displayObject->drawPageTop($page_heading, (isset($extra_page_heading) ? $extra_page_heading : array()));
}
