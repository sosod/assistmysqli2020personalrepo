<?php
/**
 * @var ASSIST_MODULE_HELPER $helper - from inc_header
 * @var STRING $cmpcode - from inc_header
 */
$section = "TERM";
$action = "VIEW";
$title = "Common Terminology";
$page_heading = array("main.php" => "Master Setups", 2 => $title);

include("inc_header.php");
echo "<P>&nbsp;</P>";
$me = new S_MASTER();
$cmpcode = $me->getCmpCode();

$width['table'] = 700;
$width['head'] = 150;
$result = array();

//PROCESS SUBMITTED FORM
if(isset($_REQUEST['a'])) {
	switch($_REQUEST['a']) {
	case "SAVE_TERM":
		if(isset($_REQUEST['i']) && strlen($_REQUEST['i'])>0) {
			$id = $_REQUEST['i'];
			if(isset($_REQUEST['v']) && strlen($_REQUEST['v'])>0) {
				$val = ASSIST_HELPER::code($_REQUEST['v']);
				$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE field = '$id' AND ref = 'IA_TERM'";
				$record = $helper->mysql_fetch_one($sql);
				$sql2 = "UPDATE assist_".$cmpcode."_setup SET value = '$val' WHERE ref = 'IA_TERM' AND field = '$id'";
				$mar = $helper->db_update($sql2);
				if($mar>0) {
					$n = explode("|_|",$record['comment']);
						$old = $record['value'];
						$trans = addslashes("Updated Client Terminology ".$n[0]." to '".($val)."' from '".($old)."'");
						$me->logMe($section, $action, $id,"value",$trans,$old,$val,$sql2);
					$_SESSION['cmp_terminology'][$id] = $val;
					$result = array("ok",$n[0]." has been updated successfully.");
				} else {
					$result = array("info","No change could be found to save.");
				}
			} else {
				$result = array("error","Please enter a value for the terminology you are trying to update.");
			}
		} else {
			$result = array("error","The terminology you are trying to update could not be identified.  Please try again.");
		}
		break;
	}
}
ASSIST_HELPER::displayResult($result);
?>
<table id=term width=<?php echo $width['table']; ?>>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = 'IA_TERM'";
$rs = $helper->mysql_fetch_all($sql);
foreach($rs as $row) {
	$txt = $row['comment'];
	$txt = explode("|_|",$txt);
	$head = $txt[0];
	$txt = isset($txt[1]) ? $txt[1] : "";
	$value = $row['value'];
	$id = $row['field'];
	echo "<tr>
		<th width=".$width['head'].">".$head.": </th>
		<td>".$txt." <span class=float><input type=text size=25 maxlength=50 value=\"".(strlen($value)>0 ? ASSIST_HELPER::decode($value) : ASSIST_HELPER::decode($head))."\" id=txt_".$id." /> <input type=button value=Save class=isubmit id=".$id." /></span></td>
	</tr>";
}
unset($rs);
?>
</table>
<?php
$log_sql = "SELECT l.date, l.tkname, l.transaction 
			FROM assist_".$cmpcode."_setup_log l
			WHERE l.active = true AND l.section = 'TERM' 
			ORDER BY l.id DESC";
$me->displayAuditLogOldStyle($log_sql,array('date'=>"date",'user'=>"tkname",'action'=>"transaction"),$me->getGoBack("main.php"));
?>
<form id=frm_term method=post action=naming.php>
<input type=hidden name=a value=SAVE_TERM />
<input type=hidden name=i id=i value="" />
<input type=hidden name=v id=v value="" />
</form>
<script type=text/javascript>
	$(document).ready(function() {
		$("table th").addClass("left");
		$("#user input:button, #master input:button").click(function() {
			var id = $(this).attr("id");
			if(id.length>0) {
				document.location.href = id+".php";
			}
		});
		$("#term input:text").keyup(function() {
			var v = $(this).val();
			if(v.length>0) {
				$(this).removeClass("required");
			} else {
				$(this).addClass("required");
			}
		});
		$("#term input:button").click(function() {
			var id = $(this).attr("id");
			var v = $("#txt_"+id).val();
			$("#txt_"+id).removeClass("required");
			if(v.length==0) {
				$("#txt_"+id).addClass("required");
				alert("Please enter a value.");
			} else {
				$("#frm_term #i").val(id);
				$("#frm_term #v").val(v);
				$("#frm_term").submit();
			}
		});
	});
</script>

</body>

</html>
