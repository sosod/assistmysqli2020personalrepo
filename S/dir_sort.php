<?php
/**
 * @var S_DISPLAY $displayObject - from inc_header
 * @var S $helper - from inc_header
 * @var string $js - from inc_header - variable to hold any javascript/jquery code until it can be echoed at the end
 */


$action = "VIEW";
$head_dir = isset($head_dir) && strlen($head_dir) > 0 ? $head_dir : $_SESSION['cmp_terminology']['DEPT'];
$head_sub = $_SESSION['cmp_terminology']['SUB'];
$section = "DIR";
$section2 = "SUB";
$title = "Organisational Structure";
$page_heading = array("main.php" => "Master Setups", "dir.php" => $title, 2 => "Sort");


include("inc_header.php");
$self = "dir_sort.php";//must fall after inc_header to overwrite the $self created there
$me = new S_MASTER();
$cmpcode = $me->getCmpCode();
$dbref = "assist_".$cmpcode."_list";


function drawSortUl($items, $width) {
	echo "	<style type=text/css>
		.ui-state-highlightsort { 
			height: 1.5em; line-height: 1.2em; 
			list-style-image: none;
			border: 1px solid #fe9900;
		}
	</style>
<script type=text/javascript>
	$(function() {
		$( \"#sortable\" ).sortable({
			placeholder: \"ui-state-highlightsort\",
		});
		$( \"#sortable\" ).disableSelection();
	});
	</script>
	<div class=blank><ul id=sortable>";
	foreach($items as $i) {
		drawSortLi($i['id'], $i['value'], $width);
	}
	echo "</ul></div>";
}

function drawSortLi($id, $value, $width) {
	echo "<li class=\"ui-state-default\" style=\"cursor:hand; width: ".$width."px; overflow: auto\">
	<span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>
	&nbsp;<input type=hidden name=sort[] value=\"".$id."\">".$value."</li>";
}


if(isset($_REQUEST['action']) && $_REQUEST['action'] == "SAVE") {
	if(!isset($_REQUEST['sort']) || !isset($_REQUEST['type'])) {
		die("<P>An error has occurred.  Please go back and try again.</p>");
	}
	$sort = $_REQUEST['sort'];
	$type = $_REQUEST['type'];
	if($type == "S" && (!isset($_REQUEST['i']) || !ASSIST_HELPER::checkIntRef($_REQUEST['i']))) {
		die("<P>An error has occurred. Please go back and try again.</p>");
	}
	//$id = $_REQUEST['i'];
	$done_sql = array();
	switch($type) {
		case "D":
			$section = "DIR";
			$tbl = "dir";
			$fld = "dir";
			$text = "Changed the display order of the ".$head_dir."s.";
			foreach($sort as $s => $i) {
				$sql = "UPDATE ".$dbref."_".$tbl." SET ".$fld."sort = $s WHERE ".$fld."id = $i";
				$helper->db_update($sql);
				$done_sql[] = $sql;
			}
			break;
		case "S":
			$section = "SUB";
			$tbl = "dirsub";
			$fld = "sub";
			$text = "Changed the display order of the ".$head_sub."s for ".$head_dir." ".$_REQUEST['i'].".";
			$sql = "UPDATE ".$dbref."_".$tbl." SET ".$fld."sort = 1 WHERE ".$fld."head = 'Y' AND ".$fld."dirid = ".$_REQUEST['i'];
			$helper->db_update($sql);
			$done_sql[] = $sql;
			foreach($sort as $s => $i) {
				$sql = "UPDATE ".$dbref."_".$tbl." SET ".$fld."sort = ".($s + 2)." WHERE ".$fld."id = $i";
				$helper->db_update($sql);
				$done_sql[] = $sql;
			}
			break;
		default:
			die("<P>An error has occurred.  Please go back and try again.");
	}
	$v_log = array(
		'sec' => $section,
		'ref' => isset($_REQUEST['i']) ? $_REQUEST['i'] : 0,
		'action' => "S",
		'fld' => "sort",
		'trans' => addslashes($text),
		'old' => "",
		'new' => "",
		'sql' => addslashes(implode(chr(10), $done_sql)),
	);
	$me->createLog($v_log);
	echo "<script type=text/javascript>";
	echo "document.location.href = 'dir.php?r[]=ok&r[]=Display+order+has+been+updated.';";
	echo "</script>";


}

if(!isset($_REQUEST['type'])) {
	die("<p>An error has occurred.  Please go back and try again.</p>");
} else {
	$type = $_REQUEST['type'];
	switch($type) {
		case "D":
			$id = 0;
			$sql = "SELECT dirtxt as value, dirid as id FROM ".$dbref."_dir WHERE diryn = 'Y' ORDER BY dirsort";
			break;
		case "S":
			if(!isset($_REQUEST['i']) || !ASSIST_HELPER::checkIntRef($_REQUEST['i'])) {
				die("<P>An error has occurred.  Please go back and try again.</p>");
			} else {
				$id = $_REQUEST['i'];
				$sql = "SELECT subtxt as value, subid as id FROM ".$dbref."_dirsub WHERE subyn = 'Y' AND subdirid = $id AND subhead = 'N' ORDER BY subsort";
			}
			break;
		default:
			die("<P>An error has occurred.  Please go back and try again.</p>");
	}
	$items = $helper->mysql_fetch_all($sql);
	?>
	<table width=350 class=noborder>
		<tr>
			<td class="blank noborder">
				<form action=<?php echo $self; ?> method=post>
					<input type=hidden name=action value=SAVE />
					<input type=hidden name=type value=<?php echo $type; ?> />
					<input type=hidden name=i value=<?php echo $id; ?> />
					<?php drawSortUl($items, 350); ?>
					<p><input type=submit class=isubmit value="Save Changes" /><span class=float><input type=button value="Reset" /></span></p>
				</form>
			</td>
		</tr>
	</table>
	<?php
}


$me->displayAuditLogOldStyle(false, array(), $me->getGoBack("main.php"));
?>
<script type=text/javascript>
	$(function () {
		$(".ireset.").click(function () {
			document.location.href = '<?php echo $self."?type=".$type."&i=".(isset($_REQUEST['i']) ? $_REQUEST['i'] : 0); ?>';
		});
	});
</script>
</body>
</html>