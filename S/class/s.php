<?php

class S extends ASSIST_MODULE_HELPER {

	private $my_attachment_download_link = "inc_attachment_controller.php";
	private $my_attachment_download_options = "action=GET_ATTACH";
	private $my_attachment_delete_link = "inc_attachment_controller.php";
	private $my_attachment_delete_options = "action=DELETE_ATTACH";
	private $my_attachment_delete_ajax = true;
	private $my_attachment_delete_function = "doDeleteAttachment";



	protected $segments = array(
		'naming' => array('name' => "Common Terminology", 'help' => "Define common terms used throughout Assist."),
		'financialyears' => array('name' => "Financial Years", 'help' => "Define the financial years used throughout Assist."),
		'dir' => array('name' => "Organisational Structure", 'help' => "Define the organisational structure to be used by non-Performance Suite modules."),
		'busproc' => array('name' => "Business Processes", 'help' => "Define the business processes used throughout Assist."),
	);
//		'functional_service' => array('name' => "Functional Services", 'help' => "Define the business functions / functional services used throughout Assist."),
	protected $name_divider = " >> ";



	/**
	 * Module Wide Constants
	 * */
	const SYSTEM_DEFAULT = 1;
	const ACTIVE = 2;
	const INACTIVE = 4;
	const DELETED = 8;

	const AWAITING_IMPORT_APPROVAL = 16;

	/**********************
	 * CONSTRUCTOR
	 */
	public function __construct($modref = "master") {
		parent::__construct("client", "", false, $modref);
		$this->checkObjectNames($modref);
		$this->checkActivityNames($modref);
	}


	public function getSegmentDetails() {
		return $this->segments;
	}

	public function getNameDivider() {
		return $this->name_divider;
	}

	public function hasTarget() {
		return $this->has_target;
	}
	public function getStatusFieldName() {
		return $this->status_field;
	}
	public function getProgressStatusFieldName() {
		return $this->progress_status_field;
	}
	public function getProgressFieldName() {
		return $this->progress_field;
	}
	public function getIDFieldName() {
		return $this->id_field;
	}
	public function getParentFieldName() {
		return $this->parent_field;
	}
	public function getSecondaryParentFieldName() {
		return $this->secondary_parent_field;
	}
	public function getNameFieldName() {
		return $this->name_field;
	}
	public function getAliasFieldName() {
		return $this->alias_field;
	}
	public function getDeadlineFieldName() {
		return $this->deadline_field;
	}
	public function getDateCompletedFieldName() {
		return $this->getTableField()."_date_completed";
	}
	public function getOwnerFieldName() {
		return $this->owner_field;
	}
	public function getAuthoriserFieldName() {
		return $this->authoriser_field;
	}
	public function getUpdateAttachmentFieldName() {
		return $this->update_attachment_field;
	}
	public function getCopyProtectedFields() {
		return $this->copy_function_protected_fields;
	}
	public function getCopyProtectedHeadingTypes() {
		return $this->copy_function_protected_heading_types;
	}
	public function getExtraObjectFormJS() { return $this->object_form_extra_js; }

	function checkFinancialYearOverlap($var) {
		$dbref = "assist_".$this->getCmpCode()."_master_financialyears";
		$result = array("error", "Nothing done.");
		$start_date = isset($var['start_date']) ? $var['start_date'] : "0000-00-00";
		$end_date = isset($var['end_date']) ? $var['end_date'] : "0000-00-00";
		$existing_FY = isset($var['fin_ref']) ? $var['fin_ref'] : " [unspecified]";
		$existing_value = isset($var['value']) ? $var['value'] : " [unspecified]";

		$id = isset($var['id']) ? $var['id'] : 0;

		$sql = "SELECT * FROM ".$dbref." WHERE start_date between '".date("Y-m-d", strtotime($start_date))."' and '".date("Y-m-d", strtotime($end_date))."' AND status = 1 AND id <> ".$id;
		$row = $this->mysql_fetch_one($sql);
		$financial_year = isset($row['fin_ref']) ? $row['fin_ref'] : "N/A";
		$value = isset($row['value']) ? $row['value'] : "N/A";
		$existing_start_date = isset($row['start_date']) ? $row['start_date'] : "N/A";

		if($this->db_get_num_rows($sql) > 0) {
			$result = array("overlapping", "date_type" => "start date", "FY" => $financial_year, 'value' => $value, 'existing_date' => $existing_start_date, 'new_date' => $start_date, 'existing_fy' => $existing_FY, 'existing_value' => $existing_value);
		} elseif($this->db_get_num_rows($sql) == 0) {

			$sql = "SELECT * FROM ".$dbref." WHERE end_date between '".date("Y-m-d", strtotime($start_date))."' and '".date("Y-m-d", strtotime($end_date))."' AND status = 1 AND id <> ".$id;
			$row = $this->mysql_fetch_one($sql);
			$financial_year = isset($row['fin_ref']) ? $row['fin_ref'] : " [unspecified]";
			$value = isset($row['value']) ? $row['value'] : "N/A";
			$existing_end_date = isset($row['end_date']) ? $row['end_date'] : " [unspecified]";

			if($this->db_get_num_rows($sql) > 0) {
				$result = array("overlapping", "date_type" => "end date", "FY" => $financial_year, 'value' => $value, 'existing_date' => $existing_end_date, 'new_date' => $end_date, 'existing_fy' => $existing_FY, 'existing_value' => $existing_value);
			} else {
				$result = array("not-overlapping", "Not overlapping");
			}
		}
		return $result;
	}


	/*************************************
	 * LOGGING Functions
	 *
	 * Changed to accommodate centralised functions #AA-663 [JC] 3 Aug 2021
	 */
	protected function generateLogTableName($table="") {
		if(!isset($table) || strlen($table)==0) { $table = "setup"; }
		$cmpcode = $this->getCmpCode();
		return strtolower("assist_".$cmpcode."_".$table."_log");
	}
	public function logMe($section,$action,$id,$fld,$trans,$old,$new,$sql,$table="setup") {
		$table = $this->generateLogTableName($table);
		$this->logMeOldStyle($section,$action,$id,$fld,$trans,$old,$new,$sql,$table);
	}
	public function createLog($l,$table="") {
		$table = $this->generateLogTableName($table);
		$this->createLogOldStyle($l,$table);
	}

	/*************************************
	 * NAMING functions
	 */

	protected function checkObjectNames($modref = "master") {
		if(strlen($modref) == 0) {
			$modref = $this->getModRef();
		}
		if(isset($_SESSION[$modref]['OBJECT_NAMES_TIMESTAMP']) && $_SESSION[$modref]['OBJECT_NAMES_TIMESTAMP'] > ($_SERVER['REQUEST_TIME'] - 1800) && isset($_SESSION[$modref]['OBJECT_NAMES'])) {
			$on = $_SESSION[$modref]['OBJECT_NAMES'];
		} else {
			$nameObject = new S_NAMES($modref);
			$on = $nameObject->fetchObjectNames();
			$_SESSION[$modref]['OBJECT_NAMES'] = $on;
			$_SESSION[$modref]['OBJECT_NAMES_TIMESTAMP'] = $_SERVER['REQUEST_TIME'];
		}
		$this->setObjectNames($on);
	}

	protected function checkActivityNames($modref = "master") {
		if(strlen($modref) == 0) {
			$modref = $this->getModRef();
		}
		if(isset($_SESSION[$modref]['ACTIVITY_NAMES_TIMESTAMP']) && $_SESSION[$modref]['ACTIVITY_NAMES_TIMESTAMP'] > ($_SERVER['REQUEST_TIME'] - 1800) && isset($_SESSION[$modref]['ACTIVITY_NAMES'])) {
			$an = $_SESSION[$modref]['ACTIVITY_NAMES'];
		} else {
			$nameObject = new S_NAMES($modref);
			$an = $nameObject->fetchActivityNames();
			$_SESSION[$modref]['ACTIVITY_NAMES'] = $an;
			$_SESSION[$modref]['ACTIVITY_NAMES_TIMESTAMP'] = $_SERVER['REQUEST_TIME'];
		}
		$this->setActivityNames($an);
	}



	/*************************
	 * Module generic functions
	 */


	public function addActivityLog($log_table,$var) {
		$logObject = new S_LOG($log_table);
		$logObject->addObject($var);
	}


	protected function isDateField($fld) {
		if(strrpos($fld,"date")!==FALSE || strrpos($fld, "reminder")!==FALSE || strrpos($fld, "deadline")!==FALSE || strrpos($fld,"action_on")!==FALSE) {
			return true;
		}
		return false;
	}


	public function editMyObject($section,$var,$extra_logs=array()) {
		$object_id = $var['object_id'];
		unset($var['object_id']);
		$headObject = new S_HEADINGS();
		$headings = $headObject->getMainObjectHeadings($section,"FORM");
		$mar=0;
		//return array("error",serialize($headings));
		$insert_data = array();
		foreach($var as $fld=>$v) {
			if(isset($headings['rows'][$fld])) {
				if($this->isDateField($fld) || $headings['rows'][$fld]['type']=="DATE") {
					if(strlen($v)>0) {
						$insert_data[$fld] = date("Y-m-d",strtotime($v));
					}
				} elseif(in_array($headings['rows'][$fld]['type'],array("MULTILIST")) && is_array($v)) {
					$insert_data[$fld] = ASSIST_HELPER::JOIN_FOR_MULTI_LISTS.implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$v).ASSIST_HELPER::JOIN_FOR_MULTI_LISTS;
				} else {
					$insert_data[$fld] = $v;
				}
			}
		}

//return array("error",serialize($insert_data));
		$old = $this->getRawObject($object_id);

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->convertArrayToSQL($insert_data)." WHERE ".$this->getIDFieldName()." = ".$object_id;
		$mar = $this->db_update($sql);

		if($mar>0 || count($extra_logs)>0) {
			$changes = array(
				'user'=>$this->getUserName(),
			);
			foreach($insert_data as $fld => $v) {
				$h = isset($headings['rows'][$fld]) ? $headings['rows'][$fld] : array('type'=>"TEXT");
				if($old[$fld]!=$v || $h['type']=="HEADING") {
					if(in_array($h['type'],array("LIST","MULTILIST","MASTER","USER"))) {
						$list_items = array();
						$ids = array($v,$old[$fld]);
						switch($h['type']) {
							case "LIST":
							case "MULTILIST":
								$listObject = new S_LIST($h['list_table']);
								if($h['type']=="MULTILIST") {
									$ids = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$v);
									$x = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$old[$fld]);
									foreach($x as $y) {
										if(!in_array($y,$ids)) {
											$ids[] = $y;
										}
									}
									$ids = $this->removeBlanksFromArray($ids);
								}
								$list_items = $listObject->getAListItemName($ids);
								break;
							/*case "USER":
								$userObject = new S_USERACCESS();
								$list_items = $userObject->getActiveUsersFormattedForSelect($ids);
								break;
							case "MASTER":
								$masterObject = new MSCOA1_MASTER($h['list_table']);
								$list_items = $masterObject->getActiveItemsFormattedForSelect($ids);
								break;*/
						}
						unset($listObject);
						if($h['type']=="MULTILIST") {
							$old_value = "";
							$new_value = "";
							$old_list = array();
							if(!is_array($old[$fld])) {
								$old[$fld] = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$old[$fld]);
							}
							$old[$fld] = ASSIST_HELPER::removeBlanksFromArray($old[$fld]);
							if(count($old[$fld])>0) {
								foreach($old[$fld] as $o) {
									if(strlen($o)>0 && isset($list_items[$o])) {
										$old_list[] = $list_items[$o];
									}
								}
								$old_value = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS." ",$old_list);
							}
							$new_list = array();
							if(!is_array($v)) {
								$v = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$v);
							}
							$v = ASSIST_HELPER::removeBlanksFromArray($v);
							if(count($v)) {
								foreach($v as $o) {
									if(strlen($o)>0 && isset($list_items[$o])) {
										$new_list[] = $list_items[$o];
									}
								}
								$new_value = implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS." ",$new_list);
							}

						} else {
							$old_value = $list_items[$old[$fld]];
							$new_value = $list_items[$v];
						}
						if($old_value!=$new_value) {
							$changes[$fld] = array('to'=>$new_value,'from'=>$old_value, 'raw'=>array('to'=>$v,'from'=>$old[$fld]));
						}
					} else {
						$changes[$fld] = array('to'=>$v,'from'=>$old[$fld]);
					}
				}
			}
			$changes = array_merge($changes,$extra_logs);
			$log_var = array(
				'object_id'	=> $object_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'	=> $changes,
				'log_type'	=> S_LOG::EDIT,
			);
			$this->addActivityLog($this->getMyLogTable(), $log_var);
			return array("ok","".$this->getRefTag().$object_id." has been updated successfully.",array($log_var,$this->getMyObjectType()));
		}
		return array("info","No changes were found to be saved.  Please try again.");
	}





	/**
	 * Function to generate SQL statement for bitwise status check
	 */
	protected function generateStatusSQL($status,$compare,$t="",$tbl_field_prefix="") {
		if($tbl_field_prefix!==false) {
			if(strlen($tbl_field_prefix)==0) { $tbl_field_prefix = $this->getTableField(); }
			return "( ".(strlen($t)>0 ? $t."." : "")."".$tbl_field_prefix."_status & ".$status.") ".$compare." ".$status;
		} else {
			return "( ".(strlen($t)>0 ? $t."." : "")."status & ".$status.") ".$compare." ".$status;
		}
	}
	/**
	 * Generate Status SQL statement dependent on page
	 */
	protected function getStatusSQL($section="REPORT",$t="",$deleted=false,$is_contract=true,$tbl_field_prefix="") {
		$statuses = array(0=>array(),1=>array());
		if($deleted) {
			$statuses[1][] = MSCOA1::DELETED;
			$statuses[0][] = MSCOA1::ACTIVE;
		} else {
			$statuses[0][] = MSCOA1::DELETED;
			$statuses[1][] = MSCOA1::ACTIVE;
		}
		switch($section) {
			case "CONFIRM":
			case "NEW":
				break;
			case "ACTIVATE":
				break;
			case "ALL":
			case "DELETED":
				break;
			case "TREE":
				$statuses[1][] = MSCOA1::INACTIVE;
				break;
			case "REPORT":
			default:
				break;
		}
		$sql = array();  //print_r($statuses);
		foreach($statuses[0] as $not) {
			$sql[] = $this->generateStatusSQL($not,"<>",$t,$tbl_field_prefix);
		}
		foreach($statuses[1] as $yes) {
			$sql[] = $this->generateStatusSQL($yes,"=",$t,$tbl_field_prefix);
		}
		//ASSIST_HELPER::arrPrint($sql);
		if($section=="TREE") {
			return " (".implode(" OR ",$sql).") ";
		} else {
			return " (".implode(" AND ",$sql).") ";
		}
	}


	public function getNewStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}

	public function getActiveStatusSQL($t="") {
		//Contracts where
			//activestatussql and
		return $this->getStatusSQL("REPORT",$t,false);
	}






	/*********************************
	 * ATTACHMENT FUNCTIONS
	*/


	protected function setAttachmentDownloadOptions($action="GET_ATTACH",$add_to_default=true) {
		if($action=="") {	//reset to default
			$this->my_attachment_download_options = "action=GET_ATTACH";
		} elseif($add_to_default) {
			$this->my_attachment_download_options .= "&action=".$action;
		} else {
			$this->my_attachment_download_options = $action;
		}
		$this->setHelperAttachmentDownloadOptions($this->my_attachment_download_options);
	}

	protected function setAttachmentDeleteOptions($action="",$add_to_default=true) {
		if($action=="") {	//reset to default
			$this->my_attachment_delete_options = "action=DELETE_ATTACH";
		} elseif($add_to_default) {
			$this->my_attachment_delete_options .= "&action=".$action;
		} else {
			$this->my_attachment_delete_options = $action;
		}
		$this->setHelperAttachmentDeleteOptions($this->my_attachment_delete_options);
	}

	public function getAttachmentDeleteOptions() { return $this->my_attachment_delete_options; }
	public function getAttachmentDeleteLink() { return $this->my_attachment_delete_link; }
	public function getAttachmentDownloadOptions() { return $this->my_attachment_download_options; }
	public function getAttachmentDownloadLink() { return $this->my_attachment_download_link; }
	public function getAttachmentDeleteFolder($full=false) { return $this->getModRef()."/deleted"; }
	public function getAttachmentImportFolder($full=false) { return $this->getModRef()."/import"; }
	public function getAttachmentDeleteByAjax() { return $this->my_attachment_delete_ajax; }
	public function getAttachmentDeleteFunction() { return $this->my_attachment_delete_function; }

	public function getAttachmentFieldName() {
		return $this->attachment_field;
	}




}