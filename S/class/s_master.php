<?php

class S_MASTER extends S {

	const LOG_CREATE = "C";
	const LOG_EDIT = "E";
	const LOG_DEACTIVATE = "T";
	const LOG_RESTORE = "R";
	const LOG_DELETE = "D";

	protected $db_table = "";
	protected $sort_by = array();

	/**********************
	 * CONSTRUCTOR
	 */
	public function __construct() {
		parent::__construct();
	}


	public function getTable() { return $this->db_table; }
	public function getTitle() { return $this->title; }
	public function getAllFields() { return $this->fields; }
	public function getFields() { return $this->getAllFields(); }

	public function getSortBy($f="",$as_array=false,$direction="ASC") {
		if(strlen($f)>0) {
			$f = $f.".";
		}
		if($as_array) {
			return $this->sort_by;
		} else {
			return " ORDER BY ".$f.implode(" $direction , ".$f,$this->sort_by)." ".$direction;
		}
	}

	public function fetch($option="",$fields="",$ids="") {
		$f = "fy";
		if($option=="active") {
			$option = " WHERE ".$f.".".$this->getFld("STATUS")." & ".$this->getFld("ACTIVE")." = ".$this->getFld("ACTIVE")." ";
		} elseif($option=="notdeleted") {
			$option = " WHERE (".$f.".".$this->getFld("STATUS")." & ".$this->getFld("ACTIVE")." = ".$this->getFld("ACTIVE")." OR ".$f.".".$this->getFld("STATUS")." & ".$this->getFld("INACTIVE")." = ".$this->getFld("INACTIVE").")";
		} elseif(strlen($option)>0) {
			$option = " WHERE ".$option;
		}
		if(is_array($ids)) {
			if(strlen($option)>0) {
				$option.=" AND ";
			} else {
				$option.=" WHERE ";
			}
			$option.=$this->getFld("ID")." IN (".implode(",",$ids).") ";
		} elseif(strlen($ids)>0) {
			if(strlen($option)>0) {
				$option.=" AND ";
			} else {
				$option.=" WHERE ";
			}
			$option.=$this->getFld("ID")." = ".$ids;
		}
		if(strlen($fields)==0) {
			$fields = $f.".*";
		} else {
			$fields = str_replace("#",$f,$fields);
		}
		$sql = "SELECT ".$fields." FROM ".$this->getTable()." ".$f." ".$option." ".$this->getSortBy($f);
		return $this->mysql_fetch_all_fld($sql,$this->getFld("ID"));
	}

	public function getActive($ids="") {
		return $this->fetch("active","",$ids);
	}

	public function getNotDeleted() {
		return $this->fetch("notdeleted");
	}

	public function getAll() {
		return $this->fetch();
	}

	public function getLinked($table,$fld,$fields) {
		if(strlen($fields)==0) { $fields = "fy.*"; } else { $fields = str_replace("#","fy",$fields); }
		$sql = "SELECT $fields FROM ".$this->getTable()." fy
				INNER JOIN ".$table." t
				ON t.".$fld." = fy.id
				 ".$this->getSortBy("fy");
		return $this->mysql_fetch_all_fld($sql,$this->getFld("ID"));
	}

	public function getLinkedForReport($table,$fld) {
		$sql = "SELECT DISTINCT fy.".$this->getFld("ID")." as id, fy.".$this->getFld("NAME")." as name FROM ".$this->getTable()." fy
				INNER JOIN ".$table." t
				ON t.".$fld." = fy.".$this->getFld("ID")."
				 ".$this->getSortBy("fy");
		return $this->mysql_fetch_value_by_id($sql,"id","name");
	}

	public function getActiveItemsFormattedForSelect() {
		$items = $this->getActive();
		$data = array();
		foreach($items as $key => $r) {
			$data[$key] = $r[$this->getFld("NAME")];
		}
		return $data;
	}
	public function getAllItemsFormattedForSelect() {
		$items = $this->getAll();
		$data = array();
		foreach($items as $key => $r) {
			$data[$key] = $r[$this->getFld("NAME")];
		}
		return $data;
	}

	/**
	 * returns the names of specific list items determined by the input id(s)
	 * @param (int) id = id of the list item to be found
	 * @return (String) name of list item
	 * OR
	 * @param (array) id = array of ids to be found
	 * @return (Array of Strings) names of the list items with the ID as index
	 */
	public function getAListItemName($id) {
    	$data = $this->getAListItem($id);
		$myName = $this::NAME_FLD;
    	if(is_array($id)) {
    		$items = array();
			foreach($data as $i=>$d) {
				$items[$i] = $d[$myName];
			}
			return $items;
    	} else {
	        return $data[$myName];
        }
	}

	/**
     * Returns a specific list item determined by the input id
     * @param (INT) id = The ID of the list item
     * @return (One Record)
	 * OR
	 * @param (Array) id = array of IDs to be found
	 * @return (Array of Arrays) records found
     */
    public function getAListItem($id) {
    	if(is_array($id)) {
        	$data = $this->getListItems("id IN (".implode(",",$id).")");
			return $data;
    	} else {
        	$data = $this->getListItems("id = ".$id);
	        return $data[$id];
        }
    }

    /**
     * Returns list items in an array with the id as key depending on the input options
     * @param (SQL) options = Set any additional options in the WHERE portion of the SQL statement
     * @return (Array of Records)
     */
    public function getListItems($options="") {
        $sql = "SELECT * ";
		if(is_array($this->fields) && in_array("client_name",$this->fields)) {
			$sql.=", IF(LENGTH(client_name)>0,client_name,default_name) as name ";
		}
		$sort_by = count($this->sort_by)>1?implode(", ",$this->sort_by):$this->sort_by[0];
        $sql.= " FROM ".$this->db_table." WHERE (status & ".MASTER::DELETED.") <> ".MASTER::DELETED.(strlen($options)>0 ? " AND ".$options : "")." ORDER BY ".$sort_by;
        return $this->mysql_fetch_all_by_id($sql, "id");
    }

	public function addLog($section,$lsql,$ref,$action,$field,$transaction,$old,$new,$active=1) {
		$field = is_array($field) ? serialize($field) : $field;
		$old = is_array($field) ? serialize($field) : $field;
		$new = is_array($field) ? serialize($field) : $field;
		$lsql = $this->code($lsql);
		$transaction = $this->code($transaction);
		$sql = "INSERT INTO assist_".$this->getCmpCode()."_master_log (id,date,tkid,tkname,section,ref,action,field,transaction,old,new,active,lsql)
				VALUES (null,now(),'".$this->getUserID()."','".$this->getUserName()."','$section',$ref,'$action','$field','$transaction','$old','$new','$active','$lsql')";
		return $this->db_insert($sql);
	}



}