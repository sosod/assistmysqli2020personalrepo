<?php

class S_FINANCIALYEARS extends S_MASTER {

	private $section_title = "Financial Years";

	const SECTION = "FINYR";

	const TABLE = "master_financialyears";

	const ID_FLD = "id";
	const NAME_FLD = "value";
	const SORT_FLD = "start_date";
	const STATUS_FLD = "status";

	const SELECT_NAME = "value";

	const ACTIVE = 1;
	const INACTIVE = 0;

	const REFTAG = "FY";

	public function __construct() {
		parent::__construct();
		$this->db_table = strtolower("assist_".$this->getCmpCode()."_".self::TABLE);
		$this->sort_by = array("start_date","end_date","value");
		$this->fields = array(
			'id'=>array(
				'type'=>"REF",
				'heading'=>"System Ref",
				'field'=>"id",
			),
			'value'=>array(
				'type'=>"MEDTEXT",
				'heading'=>"Name",
				'field'=>"value",
				'default'=>"",
			),
			'fin_ref'=>array(
				'type'=>"SMLVC",
				'heading'=>"Financial Services Ref",
				'field'=>"fin_ref",
				'default'=>"",
			),
			'start_date'=>array(
				'type'=>"DATE",
				'heading'=>"Start Date",
				'field'=>"start_date",
				'default'=>"",
			),
			'end_date'=>array(
				'type'=>"DATE",
				'heading'=>"End Date",
				'field'=>"end_date",
				'default'=>"",
			),
			'status'=>array(
				'type'=>"STATUS",
				'heading'=>"Status",
				'field'=>"status",
				'default'=>2,
			),
		);

	}



	/*********************************
	 * Newer style functions
	 */
	public function addObject($var) {
		$action = "C";
		$title = $this->section_title;
		$v = $this->code($var['value']);
		$fr = $this->code($var['fin_ref']);
		$sd = date("Y-m-d", strtotime($var['start_date']));
		$ed = date("Y-m-d", strtotime($var['end_date']));
		$data = array('value' => $v, 'start_date' => $sd, 'end_date' => $ed,'fin_ref'=>$fr);
		$sql = "INSERT INTO ".$this->getTable()." (id, value, fin_ref, start_date, end_date, status) VALUES (null,'$v','$fr','$sd','$ed',1)";
		$id = $this->db_insert($sql);
		if($this->checkIntRef($id)) {
			//logMe($id,"value)_(assist","Added new $title $id: \'".$v."\' [".$assist[$a]['value']."]","","$v)_($a",$sql);
			$this->logMe(self::SECTION,$action,$id, "", "Added new $title $id: \'".$v."\' (".$var['start_date']." - ".$var['end_date'].")", "", serialize($data), $sql, "master");
			$result = array("ok", "New $title '".$v."' has been added with reference $id.");
		} else {
			$result = array("error", "$title was not added successfully.  Please try again.");
		}
		return $result;
	}

	public function editObject($var) {
		$action = "E";
		$title = $this->getSectionTitle();
		$id = $var['id'];
		if($this->checkIntRef($id)) {
			$old_loc = $this->mysql_fetch_all("SELECT * FROM ".$this->getTable()." WHERE id = $id");
			$old_loc = $old_loc[0];
			$v = $this->code($var['value']);
			$fr = $this->code($var['fin_ref']);
			$sd = date("Y-m-d", strtotime($var['start_date']));
			$ed = date("Y-m-d", strtotime($var['end_date']));
			$data = array('value' => $v, 'start_date' => $sd, 'end_date' => $ed);
			$sql = "UPDATE ".$this->getTable()." SET value = '$v', fin_ref = '$fr', start_date = '$sd', end_date = '$ed' WHERE id = $id";
			$mar = $this->db_update($sql);
			if($this->checkIntRef($mar)) {
				$changes = array('fld' => array(), 'trans' => array(), 'old' => array(), 'new' => array());
				if($v != $old_loc['value']) {
					$changes['fld'][] = "value";
					$changes['trans'][] = "- Changed description to \'$v\' from \'".$old_loc['value']."\'";
					$changes['old']['value'] = $old_loc['value'];
					$changes['new']['value'] = $v;
				}
				if($fr != $old_loc['fin_ref']) {
					$changes['fld'][] = "fin_ref";
					$changes['trans'][] = "- Changed Financial Systems Ref to \'$fr\' from \'".$old_loc['fin_ref']."\'";
					$changes['old']['fin_ref'] = $old_loc['fin_ref'];
					$changes['new']['fin_ref'] = $fr;
				}
				if($sd != $old_loc['start_date']) {
					$changes['fld'][] = "start_date";
					$changes['trans'][] = "- Changed start date to \'$sd\' from \'".$old_loc['start_date']."\'";
					$changes['old']['start_date'] = $old_loc['start_date'];
					$changes['new']['start_date'] = $sd;
				}
				if($ed != $old_loc['end_date']) {
					$changes['fld'][] = "end_date";
					$changes['trans'][] = "- Changed end date to \'$ed\' from \'".$old_loc['end_date']."\'";
					$changes['old']['end_date'] = $old_loc['end_date'];
					$changes['new']['end_date'] = $ed;
				}
				$this->logMe(self::SECTION,$action,$id, implode(",", $changes['fld']), "Updated $title $id as follows:<br />".implode("<br />", $changes['trans']), serialize($changes['old']), serialize($changes['new']), $sql, "master");
				$result = array("ok", "Saved changes to $title $id.");
			} else {
				$result = array("info", "No changes could be found to be made to $title $id.");
			}
		} else {
			$result = array("error", "An error occurred while trying to delete the $title.  Please try again.");
		}
		return $result;
	}

	public function deleteObject($var) {
			$action = "D";
			$title = $this->getSectionTitle();
			$dbref = $this->getTable();
			$id = $var['id'];
			if($this->checkIntRef($id)) {
				$old_loc = $this->mysql_fetch_all("SELECT * FROM ".$dbref." WHERE id = $id");
				$old_loc = $old_loc[0];
				$sql = "UPDATE ".$dbref." SET status = '0' WHERE id = $id";
				$mar = $this->db_update($sql);
				if($mar > 0) {
					//logMe($id,"yn","Deleted $title $id \'".$old_loc['value']."\' [".$assist[$old_loc['assist']]['value']."]","Y","N",$sql);
					$this->logMe($id, "yn", "Deleted $title $id: \'".$old_loc['value']."\'", "1", "0", $sql, "master");
					$result = array("ok", "$title $id has been successfully deleted.");
				} else {
					$result = array("info", "No $title with reference $id was found to be deleted.  If the $title is still available, please try again.");
				}
			} else {
				$result = array("error", "An error occurred while trying to delete the $title.  Please try again.");
			}

	}

	public function getSectionTitle() { return $this->section_title; }


	public function getActiveObjects() {
		$sql = "SELECT * FROM ".$this->getTable()."
				WHERE status = 1
				ORDER BY start_date";

		return $this->mysql_fetch_all($sql);
	}


	/************************************
	 * Old style functions
	 */


	//[JC - 29 Oct 2013] old function - not sure where in use
	//[JC - 10 June 2018] Now in use in SDBP6 module
	public function fetchAll() {
		$sql = "SELECT * FROM ".$this->getTable()." WHERE ".self::STATUS_FLD." & ".self::ACTIVE." = ".self::ACTIVE." ORDER BY ".self::SORT_FLD;
		return $this->mysql_fetch_all_fld($sql,self::ID_FLD);
	}






	public function getFld($f) {
		switch(strtoupper($f)) {
			case "ID": return self::ID_FLD; break;
			case "SORT": return self::SORT_FLD; break;
			case "NAME": return self::NAME_FLD; break;
			case "SELECT_NAME": return self::SELECT_NAME; break;
			case "STATUS": return self::STATUS_FLD; break;
			case "ACTIVE": return self::ACTIVE; break;
			case "INACTIVE": return self::INACTIVE; break;
			case "TABLE": return self::TABLE; break;
		}
	}

}

?>