<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ActionController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $actionObj = new Action();
        $response    = $actionObj->getAll();
        echo json_encode($response);
        exit();
    }

    function getActionAssessment()
    {
        $questionObj = new Action();
        $option_sql  = $questionObj->prepareSql($this->parameters);

        $questions       = $questionObj->getQuestionWithAssessment($option_sql);
        $_option_sql    = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $total_questions = $questionObj->getTotalQuestionWithAssessment($_option_sql);
        //echo $option_sql." \r\n";
        //echo $_option_sql." \r\n";
        //debug($questions);
        //debug(count($questions));
        //debug($total_questions);
        //exit();
        $questions         = $questionObj->prepareAction($questions, $total_questions, $this->parameters);
        if(isset($this->parameters['score_card_id']) && !empty($this->parameters['score_card_id']))
        {
            $scoreCardObj            = new ScoreCard();
            $score_card              = $scoreCardObj->getScoreCardWithSetup($this->parameters['score_card_id']);
            $score_card['logged_user'] = $_SESSION['tkn'];
            $questions['score_card'] = $score_card;

            $overall_rating = $questionObj->getOverallRating($total_questions, $_option_sql, $score_card);
            $questions['overall_rating'] = $overall_rating;
        }
        echo json_encode( $questions );
        exit();
    }

    public function getActions($options = array())
    {
        $actionObj = new Action();
        $option_sql  = $actionObj->prepareSql($this->parameters);

        $action       = $actionObj->getActions($option_sql);
        $_option_sql  = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));

        $total_action = $actionObj->getTotalActions($_option_sql);
        $actions      = $actionObj->prepareAction($action, $total_action, $this->parameters);
        echo json_encode($actions);
        exit();
    }


    function save($options = array())
    {
        $actionObj      = new Action();
        $action         = $this->bind($this->parameters);
        $deliverableObj = new Deliverable();
        $deliverable    = $deliverableObj->getDeliverableDetail($action['deliverable_id']);
        $message        = array();
        if($actionObj->isValid($action, $deliverable))
        {
            $action['insert_user'] = $_SESSION['tid'];
            $action['created_at']  = date('Y-m-d H:i:s');
            $action['status']      = 1;
            $action['status_id']   = 1;
            $result                = $actionObj->save($action);
            if($result)
            {
                //set_flash('success', 'deliverable', 'Deliverable successfully saved');
               $message = array('text' => 'Question successfully saved', 'error' => false);
            } else {
               //set_flash('error', 'deliverable', 'There was an error saving deliverable');
               $message = array('text' => 'There was an error saving question', 'error' => true);
            }
        } else {
            $error   = get_flash('action');
            $message = array('text' => $error, 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    function editAction($options = array())
    {
        $actionObj      = new Action();
        $action         = $this->bind($this->parameters);
        $deliverableObj = new Deliverable();
        $deliverable    = $deliverableObj->getDeliverableDetail($action['deliverable_id']);
        $message        = array();
        if($actionObj->isValid($action, $deliverable))
        {
            $actionObj->attach(new ActionAuditLog());
            $result                = $actionObj->update_where($action, array('id' => $this->parameters['id']));
            if($result)
            {
                $scoreCardObj = new ScoreCard();
                $score_card    = $scoreCardObj->getScoreCardDetail($deliverable['score_card_id']);
                $scoreCardObj->updateActivityOnManage($score_card);
                //set_flash('success', 'deliverable', 'Deliverable successfully saved');
                $message = array('text' => 'Question successfully updated', 'error' => false, 'updated' => true);
            } else if($result == 0) {
                $message = array('text' => 'No change was made to the question', 'error' => false, 'updated' => false);
            } else {
                //set_flash('error', 'deliverable', 'There was an error saving deliverable');
                $message = array('text' => 'There was an error updating question', 'error' => true);
            }
        } else {
            $error   = get_flash('action');
            $message = array('text' => $error, 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function updateAction()
    {
        $actionObj    = new Action();
        parse_str($this->parameters['update_data'], $posted_data);

        $action_data    = $actionObj->getActionDetail($posted_data['action_id']);

        $action_documents = array();
        if(isset($posted_data['action_document']))
        {
            $action_documents = $posted_data['action_document'];
            //unset($posted_data['ac'])
        }
        $action_update = (isset($posted_data['action']) ? $posted_data['action'] : array());
        $action       = $this->bind($action_update, $posted_data);
        $actionObj->attach(new ActionAuditLog());
        $result       = $actionObj->update_where($action, array('id' => $posted_data['action_id']));
        if($result)
        {
            $mail_txt = '';
            if(!empty($action_documents))
            {
                $actionDocumentObj = new ActionDocument();
                foreach($action_documents as $filename => $action_document)
                {
                   $action_document = array( 'filename' => $filename, 'description' => $action_document, 'action_id' => $posted_data['action_id'] );
                   $actionDocumentObj->save($action_document);
                }
                unset($_SESSION['uploads']);
            }
            if(isset($action['status_id']) && $action['status_id'] == 3)
            {
                if(isset($posted_data['approval']) && $posted_data['approval'] == 1)
                {
                    $columnObj = new ActionColumns();
                    $columns   = $columnObj->getHeaders();
                    $_action   = $actionObj->getActionDetail($posted_data['action_id']);
                    if(ActionEmail::sendRequestApprovalEmail($columns, $_action, $posted_data)) $mail_txt = " Notification email successfully send";
                }
                //send mail to Deliverable Owner
            }
            $scoreCardObj = new ScoreCard();
            $score_card    = $scoreCardObj->getScoreCardDetail($action_data['score_card_id']);
            $scoreCardObj->updateActivityOnManage($score_card);

            $message = array('text' => 'Question successfully updated.'.$mail_txt, 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the question', 'error' => false, 'updated' => true);
        } else {
            $message = array('text' => 'There was an error saving question', 'error' => true, 'updated' => false);
        }

        echo json_encode($message);
        exit();
    }

    public function deleteAction()
    {
        $actionObj = new Action();
        $actionObj->attach(new ActionAuditLog());
        $res = $actionObj->update_where(array('status' => Action::DELETED), array('id' => $this->parameters['action_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Question successfully deleted', 'error' => false, 'updated' => true);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the question', 'error' => false, 'updated' => false);
        } else {
            $response = array('text' => 'Error occurred deleting a question', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    function bind($action, $posted_data = array())
    {
        if(isset($action['deadline_date']))
        {
            $deadline_date = strtotime($action['deadline_date']);
            if($deadline_date > 0)
            {
                $action['deadline_date'] = date('Y-m-d', $deadline_date);
            }
        }

        if(isset($action['remind_on']))
        {
            $remind_on = strtotime($action['remind_on']);
            if($remind_on > 0)
            {
                $action['remind_on'] = date('Y-m-d', $remind_on);
            }
        }

        if(isset($action['action_on']))
        {
            $action_on = strtotime($action['action_on']);
            if($action_on > 0)
            {
                $action['action_on'] = date('Y-m-d', $action_on);
            }
        }
        if(isset($action['status_id']) && $action['status_id'] == 3)
        {
            $action['status'] = Action::ACTIVE + Action::AWAITING;
        }
       return $action;
    }

    public function getApprovalActions()
    {
        $actionObj  = new Action();

        $this->parameters['type'] = 'awaiting';
        $awaiting_sql = $actionObj->prepareSql($this->parameters);
        //echo $awaiting_sql;
        $this->parameters['type'] = 'approved';
        $approved_sql = $actionObj->prepareSql($this->parameters);


        $actions          = $actionObj->getActions($awaiting_sql);
        $awaiting_actions = $actionObj->prepareAction($actions, 0, $this->parameters);

        $actions          = $actionObj->getActions($approved_sql);
        $approved_actions = $actionObj->prepareAction($actions, 0, $this->parameters);



        $columnsObj             = new ActionColumns();
        //$_option_sql    = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $actions['awaiting']  = $awaiting_actions;
        $actions['approved'] = $approved_actions;
        $actions['columns']   = $columnsObj->getHeaders();
        echo json_encode($actions);
        exit();
    }

    public function getActionUpdateLog()
    {

       $actionObj      = new ActionUpdateLog($this->parameters['id']);
       $audit_log_html = $actionObj->displayLog();
       echo $audit_log_html;
    }

    public function approveAction()
    {
        $actionObj = new Action();
        $action  = $actionObj->getActionDetail($this->parameters['action_id']);
        $message     = array();
        if($action)
        {
            if(($action['status'] && Action::AWAITING) == Action::AWAITING)
            {
               $action['status'] = $action['status'] - Action::AWAITING;
            }
            $update_data['status']   = $action['status'] + Action::APPROVED;
            $_SESSION['action_approved_'.$this->parameters['action_id']] = true;
            $actionObj->attach(new ActionAuditLog());
            $result                  = $actionObj->update_where($update_data, array('id' => $this->parameters['action_id']));
            if($result)
            {
                $message = array('text' => 'Question successfully approved', 'error' => false, 'updated' => true);
            } elseif($result == 0) {
                $message = array('text' => 'No change was made to the question', 'error' => false, 'updated' => true);
            }else {
                $message = array('text' => 'There was an error approving question', 'error' => true, 'updated' => false);
            }
        }
        echo json_encode($message);
        exit();
    }

    public function declineAction()
    {
        $actionObj = new Action();
        $action    = $actionObj->getActionDetail($this->parameters['action_id']);
        $message   = array();
        if($action)
        {
            if(($action['status'] && Action::AWAITING) == Action::AWAITING)
            {
                $update_data['status'] = $action['status'] - Action::AWAITING;
            }
            $update_data['status_id'] = 2;
            $update_data['progress']  = 99;
            $_SESSION['action_declined_'.$this->parameters['action_id']] = true;
            $actionObj->attach(new ActionAuditLog());
            $result   = $actionObj->update_where($update_data, array('id' => $this->parameters['action_id']));
            if($result)
            {
                $mail_txt = '';
                $columnObj = new ActionColumns();
                $columns   = $columnObj->getHeaders();
                $_action   = $actionObj->getActionDetail($this->parameters['action_id']);
                if(ActionEmail::sendActionDeclinedEmail($columns, $_action, $this->parameters)) $mail_txt = " Notification email successfully send";

                $message = array('text' => 'Question successfully decline.'.$mail_txt, 'error' => false, 'updated' => true);
            } elseif($result == 0) {
                $message = array('text' => 'No change was made to the question', 'error' => false, 'updated' => true);
            }else {
                $message = array('text' => 'There was an error declining question', 'error' => true, 'updated' => false);
            }
        }
        echo json_encode($message);
        exit();
    }

    public function undoApproveAction()
    {
        $actionObj = new Action();
        $action  = $actionObj->getActionDetail($this->parameters['action_id']);
        $message     = array();
        if($action)
        {
            if(($action['status'] && Action::APPROVED) == Action::APPROVED)
            {
                $action['status'] = $action['status'] - Action::APPROVED;
            }
            $update_data['status']   = $action['status'] + Action::AWAITING;
            $_SESSION['action_unapproved_'.$this->parameters['action_id']] = true;
            $actionObj->attach(new ActionAuditLog());
            $result                  = $actionObj->update_where($update_data, array('id' => $this->parameters['action_id']));
            if($result)
            {
                $message = array('text' => 'Question successfully disapproved', 'error' => false, 'updated' => true);
            } elseif($result == 0) {
                $message = array('text' => 'No change was made to the question', 'error' => false, 'updated' => true);
            }else {
                $message = array('text' => 'There was an error disapproving question', 'error' => true, 'updated' => false);
            }
        }
        echo json_encode($message);
        exit();

    }

    public function removeActionAttachment()
    {
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('action_attachment', 'action_attachment');
        $response = $attObj->removeFile($file, 'action_docs');
        echo json_encode($response);
    }

    public function saveAttachment()
    {
        $attObj    = new Attachment('action_attachment', 'action_attachment');
        $results   = $attObj->upload('action_docs');
        echo json_encode($results);
    }

    public function deleteActionAttachment()
    {
        $actionDocumentObj = new ActionDocument();
        $res      = $actionDocumentObj->update_where( array('deleted_at' => date('Y-m-d H:i:s') ), array('id' => $this->parameters['doc_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Attachment successfully deleted', 'error' => false);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the attachment', 'error' => false);
        } else {
            $response = array('text' => 'An error occurred deleting the attachment', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ActionController();
    return $class->execute($_REQUEST['action']);
}
exit();
?>