<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class WeightController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $weightObj = new Weight();
        $response    = $weightObj->getAll();
        echo json_encode($response);
        exit();
    }

    function save($options = array())
    {
        $weightObj             = new Weight();
        $weight                = $this->parameters;
        $weight['insert_user'] = $_SESSION['tid'];
        $weight['created_at']  = date('Y-m-d H:i:s');
        $weight['status']      = 1;
        $weightObj->attach(new SetupAuditLog());
        $result                       = $weightObj->save($weight);
        if($result)
        {
            $message = array('text' => 'Weight successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving weight', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $weightObj             = new Weight();
        $weight                = $this->parameters;
        $weight['insert_user'] = $_SESSION['tid'];
        $weight['created_at']  = date('Y-m-d H:i:s');
        $weightObj->attach(new SetupAuditLog());
        $result                       = $weightObj->update_where($weight, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Weight successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the weight', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error saving weight', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new WeightController();
    return $class->execute($_REQUEST['action']);
}

?>