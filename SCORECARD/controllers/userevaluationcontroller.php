<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class UserEvaluationController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $userEvaluationObj = new UserEvaluation();
        $response    = $userEvaluationObj->getAll();
        echo json_encode($response);
        exit();
    }

    public function getUserEvaluations($options = array())
    {
        $userEvaluationObj = new UserEvaluation();
        $option_sql = '';
        if(isset($this->parameters['user_id'])) $option_sql .= " AND UE.user_id = '".$this->parameters['user_id']."' ";
        $response    = $userEvaluationObj->getAll($option_sql);
        echo json_encode($response);
        exit();
    }

    function save($options = array())
    {
        $userEvaluationObj            = new UserEvaluation();
        $user_evaluation              = $this->parameters;
        $user_evaluation               = $this->bind($this->parameters);
        $user_evaluation['created_at'] = date('Y-m-d H:i:s');
        $user_evaluation['status']     = 1;
        $result                       = $userEvaluationObj->save($user_evaluation);
        if($result)
        {
            $message = array('text' => 'User evaluation period status successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving user evaluation period', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $userEvaluationObj             = new UserEvaluation();
        $user_evaluation               = $this->bind($this->parameters);
        $user_evaluation['updated_at']  = date('Y-m-d H:i:s');
        $userEvaluationObj->attach(new SetupAuditLog());
        $result                       = $userEvaluationObj->update_where($user_evaluation, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'User evaluation period successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the user evaluation period', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error updating the user evaluation period', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

    function bind($user_evaluation_period)
    {
        if(isset($user_evaluation_period['open_from']))
        {
            $open_from = strtotime($user_evaluation_period['open_from']);
            if($open_from > 0) $user_evaluation_period['open_from'] = date('Y-m-d', $open_from);
        }
        if(isset($user_evaluation_period['open_to']))
        {
            $open_to = strtotime($user_evaluation_period['open_to']);
            if($open_to > 0) $user_evaluation_period['open_to'] = date('Y-m-d', $open_to);
        }
        return $user_evaluation_period;
    }


}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new UserEvaluationController ();
    return $class->execute($_REQUEST['action']);
}

?>