<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class TaskStatusController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $statusObj = new TaskStatus();
        $response    = $statusObj->getAll();
        echo json_encode($response);
        exit();
    }

    function save($options = array())
    {
        $statusObj                      = new TaskStatus();
        $status                = $this->parameters;
        $status['created_at']  = date('Y-m-d H:i:s');
        $status['status']      = 1;
        $result                       = $statusObj->save($status);
        if($result)
        {
            $message = array('text' => 'Task status successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving task status', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $statusObj             = new TaskStatus();
        $status                = $this->parameters;
        $status['created_at']  = date('Y-m-d H:i:s');
        $statusObj->attach(new SetupAuditLog());
        $result                       = $statusObj->update_where($status, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Task status successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the task status', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error saving task status', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new TaskStatusController ();
    return $class->execute($_REQUEST['action']);
}

?>