<?php
require_once '../score_card_auto_loader.php';
require_once '../score_card_helper.php';
class ScoreCardAssuranceController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }


    public function getScoreCardAssurances($options = array())
    {
        $scoreCardAssuranceObj = new ScoreCardAssurance();
        $option_sql           = $scoreCardAssuranceObj->prepareSql($this->parameters);

        $score_card_assurances    = $scoreCardAssuranceObj->getScoreCardAssurances($option_sql);

        $_option_sql           = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $total_assurance      = $scoreCardAssuranceObj->getTotalScoreCardAssurance($_option_sql);

        $score_card_assurances  = $scoreCardAssuranceObj->prepareScoreCardAssurance($score_card_assurances, $total_assurance, $this->parameters);
        echo json_encode($score_card_assurances);
        exit();
    }


    function saveScoreCardAssurance($options = array())
    {
        parse_str($this->parameters['assurance_data'], $score_card_assurance);
        $assurance_documents = array();
        if(isset($score_card_assurance['score_card_assurance_docs']) && !empty($score_card_assurance['score_card_assurance_docs']))
        {
           $assurance_documents = $score_card_assurance['score_card_assurance_docs'];
           unset($score_card_assurance['score_card_assurance_docs']);
        }
        $scoreCardAssuranceObj = new ScoreCardAssurance();
        $score_card_assurance = $this->bind($score_card_assurance);
        $response            = array();
        if($scoreCardAssuranceObj->isValid($score_card_assurance))
        {
            $score_card_assurance['score_card_id'] = $this->parameters['score_card_id'];
            $score_card_assurance['insert_user'] = $_SESSION['tid'];
            $score_card_assurance['created_at']  = date('Y-m-d H:i:s');
            //$score_card_payment['status']      = 1;
            $result                          = $scoreCardAssuranceObj->save($score_card_assurance);
            if($result)
            {
                if(isset($assurance_documents) && !empty($assurance_documents))
                {
                    $contractAssuranceDocumentObj = new ScoreCardAssuranceDocument();
                    foreach($assurance_documents as $filename => $file_description)
                    {
                        if(Attachment::isFile($filename))
                        {
                            $docs = array('filename' => $filename, 'description' => $file_description, 'assurance_id' => $result);
                            $contractAssuranceDocumentObj->save($docs);
                        }
                    }
                    unset($_SESSION['uploads']);
                }
                $response = array('text' => 'Score card assurance successfully saved', 'error' => false, 'id' => $result);
            } else {
                $response = array('text' => 'There was an error saving score_card assurance', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }

    function updateScoreCardAssurance($options = array())
    {
        parse_str($this->parameters['assurance_data'], $score_card_assurance);
        $assurance_documents = array();
        if(isset($score_card_assurance['score_card_assurance_docs']) && !empty($score_card_assurance['score_card_assurance_docs']))
        {
           $assurance_documents  = $score_card_assurance['score_card_assurance_docs'];
           unset($score_card_assurance['score_card_assurance_docs']);
        }
        if(isset($score_card_assurance['deleted_assurance_document']) && !empty($score_card_assurance['deleted_assurance_document']))
        {
           unset($score_card_assurance['deleted_assurance_document']);
        }

        $contractPaymentObj = new ScoreCardAssurance();
        $score_card_assurance   = $this->bind($score_card_assurance);
        $response               = array();
        if($contractPaymentObj->isValid($score_card_assurance))
        {
            $contractPaymentObj->attach(new ScoreCardAssuranceAuditLog());
            $result   = $contractPaymentObj->update_where($score_card_assurance, array('id' => $this->parameters['id']));
            if(isset($assurance_documents) && !empty($assurance_documents))
            {
                $contractAssuranceDocumentObj = new ScoreCardAssuranceDocument();
                foreach($assurance_documents as $filename => $file_description)
                {
                    if(Attachment::isFile($filename))
                    {
                        $docs    = array('filename' => $filename, 'description' => $file_description, 'assurance_id' => $this->parameters['id']);
                        $result += $contractAssuranceDocumentObj->save($docs);
                    }
                }
                unset($_SESSION['uploads']);
            }
            if($result > 0)
            {
                $response = array('text' => 'Score card assurance successfully updated', 'error' => false, 'id' => $result, 'updated' => true);
            } elseif($result == 0) {
                $response = array('text' => 'No change was made to the score card assurance', 'error' => false, 'updated' => false);
            } else {
                $response = array('text' => 'There was an error updating assurance', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }


    function bind($score_card_assurance)
    {
        if(isset($score_card_assurance['date_tested']))
        {
            $date_tested = strtotime($score_card_assurance['date_tested']);
            if($date_tested > 0)
            {
                $score_card_assurance['date_tested'] = date('Y-m-d', $date_tested);
            }
        }
        return $score_card_assurance;
    }

    public function deleteScoreCardAssurance()
    {
        $scoreCardAssuranceObj = new ScoreCardAssurance();
        $res                = $scoreCardAssuranceObj->update_where(array('status' => 2), array('id' => $this->parameters['id']));
        $response = array();
        if($res)
        {
            $response = array('text' => 'ScoreCard assurnace successfully deleted', 'error' => false);
        } else {
            $response = array('text' => 'Error deleting the score_card assurnace', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    public function removeScoreCardPaymentAttachment()
    {
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('score_card_assurance_attachment', 'score_card_assurance_attachment');
        $response = $attObj->removeFile($file, 'new_score_card_assurance_document');
        echo json_encode($response);
    }

    public function saveAttachment()
    {
        $attObj    = new Attachment('score_card_assurance_attachment', 'score_card_assurance_attachment');
        $results   = $attObj->upload('new_score_card_assurance_document');
        echo json_encode($results);
    }


    public function deleteAttachment()
    {
        $contractAssuranceDocumentObj = new ScoreCardAssuranceDocument();
        $res      = $contractAssuranceDocumentObj->update_where( array('deleted_at' => date('Y-m-d H:i:s') ), array('id' => $this->parameters['document_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Attachment successfully deleted', 'error' => false);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the attachment', 'error' => false);
        } else {
            $response = array('text' => 'An error occurred deleting the attachment', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    public function download()
    {
        header("Content-type:".$_GET['content']);
        header("Content-disposition: attachment; filename=".$_GET['new']);
        readfile("../../files/".$_GET['company']."/".$_SESSION['modref']."/".$_GET['folder']."/".$_GET['file']);
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ScoreCardAssuranceController();
    return $class->execute($_REQUEST['action']);
}

?>