<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class UserNotificationController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $userNotificationObj = new UserNotification();
        $user_notifications  = $userNotificationObj->getList();
        echo json_encode($user_notifications);
    }

    function getUserNotifications()
    {
        $userNotificationObj = new UserNotification();
        $user_notifications  = $userNotificationObj->getNotifications();
        $data               = array();
        if(!empty($user_notifications))
        {
            foreach($user_notifications as $index => $result)
            {
                $data[$result['id']]['recieve']       = $result['recieve_email'];
                $data[$result['id']]['what']          = $result['recieve_what'];
                $data[$result['id']]['when']          = $result['recieve_when'];
                $data[$result['id']]['day']           = $result['recieve_day'];
                $data[$result['id']]['active']        = $result['status'];
                $data[$result['id']]['id']            = $result['id'];
                $data[$result['id']]['recieve_email'] = ($result['recieve_email'] == 0 ? "No" : "Yes");
                $data[$result['id']]['recieve_what']  = UserNotification::$notification_types[$result['recieve_what']];
                if($result['recieve_when'] == "weekly")
                {
                    $when_str   = "Weekly";
                    if(isset(UserNotification::$recieve_days[$result['recieve_day']]))
                    {
                        $when_str  .= "<em> on ".UserNotification::$recieve_days[$result['recieve_day']]."</em>";
                    }
                    $data[$result['id']]['recieve_when'] = $when_str;
                } else {
                    $data[$result['id']]['recieve_when']  = "Daily";
                }

                $data[$result['id']]['status'] = ($result['status'] == 0 ? "Inactive" : "Active");
            }
        }
        echo json_encode($data);
        exit();
    }

    function save($options = array())
    {
        $user_data = array();
        $userObj   = new UserNotification();
        $user_notification_data   = $this->parameters;

        $user_notification_data['user_id']     = $_SESSION['tid'];
        $user_notification_data['insert_user'] = $_SESSION['tid'];
        $user_notification_data['created_at']  = date('Y-m-d H:i:s');
        $user_notification_data['status']      = 1;
        $result                   = $userObj->save($user_notification_data);
        if($result)
        {
            $response = array('text' => 'User notification successfully saved', 'error' => false);
        } else {
            $response = array('text' => 'There was an error saving the user', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }



    public function update()
    {
        $userObj   = new UserNotification();
        parse_str($this->parameters['data'], $user);
        $user_data['status'] = $userObj->calculateUserSettingStatus($user['status']);
        $userObj->attach(new UserAuditLog());
        $result   = $userObj->update_where($user_data, array('id' => $user['id']));
        $response = array();
        if($result)
        {
            $response = array('text' => 'User successfully updated', 'error' => false, 'updated' => true);
            //$this->setFlash('success', 'User successfully saved');
        } elseif($result == 0) {
            $response = array('text' => 'No change was made to the user', 'error' => false, 'updated' => false);
        } else {
            $response = array('text' => 'An error occurred updating user details', 'error' => true);
            //$this->setFlash('error', 'There was an error saving the user');
        }
        echo json_encode($response);
        exit();
    }

    public function delete_user()
    {
        $userObj             = new UserNotification();
        $user_data['status'] = User::DELETED;
        $userObj->attach(new UserAuditLog());
        $result   = $userObj->delete($user_data, array('id' => $this->parameters['id']));
        if($result)
        {
            $this->setFlash('success', 'User successfully saved');
        } else {
            $this->setFlash('error', 'There was an error saving the user');
        }
        $this->redirect('setup/edit_user.php?id='.$this->parameters['id']);
    }

    private function prepareData($data, $userObj)
    {
        $user_data = array();
        foreach($data as $key => $val)
        {
            if($key == 'status')
            {
                $user_data[$key] = $userObj->calculateUserSettingStatus($val);
            } else {
                $user_data[$key] = $val;
            }
        }
        return $user_data;
    }

    public function setDefaultFinancialYear()
    {
        $userObj = new User();
        $res     = $userObj->update_where(array('financial_year_id' => $this->parameters['default_year']), array('id' => $this->parameters['user_id']));
        $response = array();
        if($res > 0)
        {
            $mod_ref = $_SESSION['modref'];
            $user    = $_SESSION['tid'];
            $_SESSION[$mod_ref][$user]['default_financial_year'] = $this->parameters['default_year'];
            $response = array('text' => 'Your default financial year set successfully', 'error' => false);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the default financial year', 'error' => false);
        } else {
            $response = array('text' => 'Error updating your default financial year', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }


}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new UserNotificationController();
    return $class->execute($_REQUEST['action']);
}

?>