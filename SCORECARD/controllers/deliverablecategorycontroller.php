<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class DeliverableCategoryController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $categoryObj = new DeliverableCategory();
        $response    = $categoryObj->getAll();
        echo json_encode($response);
        exit();
    }

    function save($options = array())
    {
        $categoryObj             = new DeliverableCategory();
        $category                = $this->parameters;
        $category['insert_user'] = $_SESSION['tid'];
        $category['created_at']  = date('Y-m-d H:i:s');
        $category['status']      = 1;
        $categoryObj->attach(new SetupAuditLog());
        $result                       = $categoryObj->save($category);
        if($result)
        {
            $message = array('text' => 'Deliverable category successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving deliverable category', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $categoryObj                      = new DeliverableCategory();
        $category                = $this->parameters;
        $category['insert_user'] = $_SESSION['tid'];
        $category['created_at']  = date('Y-m-d H:i:s');
        $categoryObj->attach(new SetupAuditLog());
        $result                       = $categoryObj->update_where($category, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Deliverable category successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the deliverable category', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error saving deliverable category', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new DeliverableCategoryController();
    return $class->execute($_REQUEST['action']);
}

?>