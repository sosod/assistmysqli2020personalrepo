<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class DeliverableAssuranceController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }


    public function getDeliverableAssurances($options = array())
    {
        $contractAssuranceObj = new DeliverableAssurance();
        $option_sql           = $contractAssuranceObj->prepareSql($this->parameters);

        $deliverable_assurances    = $contractAssuranceObj->getDeliverableAssurances($option_sql);

        $_option_sql           = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $total_assurance      = $contractAssuranceObj->getTotalDeliverableAssurance($_option_sql);

        $deliverable_assurances  = $contractAssuranceObj->prepareDeliverableAssurance($deliverable_assurances, $total_assurance, $this->parameters);
        echo json_encode($deliverable_assurances);
        exit();
    }


    function saveDeliverableAssurance($options = array())
    {
        parse_str($this->parameters['assurance_data'], $deliverable_assurance);
        $assurance_documents = array();
        if(isset($deliverable_assurance['deliverable_assurance_document']) && !empty($deliverable_assurance['deliverable_assurance_document']))
        {
           $assurance_documents = $deliverable_assurance['deliverable_assurance_document'];
           unset($deliverable_assurance['deliverable_assurance_document']);
        }
        $contractAssuranceObj = new DeliverableAssurance();
        $deliverable_assurance = $this->bind($deliverable_assurance);
        $response            = array();

        if($contractAssuranceObj->isValid($deliverable_assurance))
        {
            $deliverable_assurance['deliverable_id'] = $this->parameters['deliverable_id'];
            $deliverable_assurance['insertuser']    = $_SESSION['tid'];
            $deliverable_assurance['created_at']    = date('Y-m-d H:i:s');
            //$contract_payment['status']      = 1;
            $result                          = $contractAssuranceObj->save($deliverable_assurance);
            if($result)
            {
                if(isset($assurance_documents) && !empty($assurance_documents))
                {
                    $contractAssuranceDocumentObj = new DeliverableAssuranceDocument();
                    foreach($assurance_documents as $filename => $file_description)
                    {
                        if(Attachment::isFile($filename))
                        {
                            $docs = array('filename' => $filename, 'description' => $file_description, 'assurance_id' => $result);
                            $contractAssuranceDocumentObj->save($docs);
                        }
                    }
                    unset($_SESSION['uploads']);
                }
                $response = array('text' => 'Deliverable assurance successfully saved', 'error' => false, 'id' => $result);
            } else {
                $response = array('text' => 'There was an error saving deliverable assurance', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }

    function updateDeliverableAssurance($options = array())
    {
        parse_str($this->parameters['assurance_data'], $deliverable_assurance);
        $assurance_documents = array();
        if(isset($deliverable_assurance['deliverable_assurance_document']) && !empty($deliverable_assurance['deliverable_assurance_document']))
        {
           $assurance_documents  = $deliverable_assurance['deliverable_assurance_document'];
           unset($deliverable_assurance['deliverable_assurance_document']);
        }
        if(isset($deliverable_assurance['deleted_assurance_document']) && !empty($deliverable_assurance['deleted_assurance_document']))
        {
           unset($deliverable_assurance['deleted_assurance_document']);
        }

        $contractPaymentObj = new DeliverableAssurance();
        $deliverable_assurance   = $this->bind($deliverable_assurance);
        $response               = array();
        if($contractPaymentObj->isValid($deliverable_assurance))
        {
            $contractPaymentObj->attach(new DeliverableAssuranceAuditLog());
            $result   = $contractPaymentObj->update_where($deliverable_assurance, array('id' => $this->parameters['id']));
            if(isset($assurance_documents) && !empty($assurance_documents))
            {
                $contractAssuranceDocumentObj = new DeliverableAssuranceDocument();
                foreach($assurance_documents as $filename => $file_description)
                {
                    if(Attachment::isFile($filename))
                    {
                        $docs    = array('filename' => $filename, 'description' => $file_description, 'assurance_id' => $this->parameters['id']);
                        $result += $contractAssuranceDocumentObj->save($docs);
                    }
                }
                unset($_SESSION['uploads']);
            }
            if($result > 0)
            {
                $response = array('text' => 'Deliverable assurance successfully updated', 'error' => false, 'id' => $result, 'updated' => true);
            } elseif($result == 0) {
                $response = array('text' => 'No change was made to the deliverable assurance', 'error' => false, 'updated' => false);
            } else {
                $response = array('text' => 'There was an error updating assurance', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }


    function bind($deliverable_assurance)
    {
        if(isset($deliverable_assurance['date_tested']))
        {
            $date_tested = strtotime($deliverable_assurance['date_tested']);
            if($date_tested > 0)
            {
                $deliverable_assurance['date_tested'] = date('Y-m-d', $date_tested);
            }
        }
        return $deliverable_assurance;
    }

    public function deleteDeliverableAssurance()
    {
        $contractAssuranceObj = new DeliverableAssurance();
        $res                = $contractAssuranceObj->update_where(array('status' => 2), array('id' => $this->parameters['id']));
        $response = array();
        if($res)
        {
            $response = array('text' => 'Deliverable assurance successfully deleted', 'error' => false);
        } else {
            $response = array('text' => 'Error deleting the deliverable assurance', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    public function removeDeliverableAttachment()
    {
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('deliverable_assurance_attachment', 'deliverable_assurance_attachment');
        $response = $attObj->removeFile($file, 'new_deliverable_assurance_document');
        echo json_encode($response);
    }

    public function saveAttachment()
    {
        $attObj    = new Attachment('deliverable_assurance_attachment', 'deliverable_assurance_attachment');
        $results   = $attObj->upload('new_deliverable_assurance_document');
        echo json_encode($results);
    }


    public function deleteAttachment()
    {
        $contractAssuranceDocumentObj = new DeliverableAssuranceDocument();
        $res      = $contractAssuranceDocumentObj->update_where( array('deleted_at' => date('Y-m-d H:i:s') ), array('id' => $this->parameters['document_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Attachment successfully deleted', 'error' => false);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the attachment', 'error' => false);
        } else {
            $response = array('text' => 'An error occurred deleting the attachment', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    public function download()
    {
        header("Content-type:".$_GET['content']);
        header("Content-disposition: attachment; filename=".$_GET['new']);
        readfile("../../files/".$_GET['company']."/".$_SESSION['modref']."/".$_GET['folder']."/".$_GET['file']);
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new DeliverableAssuranceController();
    return $class->execute($_REQUEST['action']);
}

?>