<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ContractAssessmentController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $contractAssessmentObj = new ContractAssessment();
        $option_sql            = $contractAssessmentObj->prepareSql($this->parameters);
        $response              = $contractAssessmentObj->getList(null, $option_sql);
        echo json_encode($response);
        exit();
    }

    function getContractAssessment()
    {
        $columnsObj = new ContractColumns();
        $columns    = $columnsObj->getHeaders();

        $contractObj                   = new Contract();
        $contract                      = $contractObj->getContractDetail($this->parameters['contract_id']);

        $contractAssessmentObj         = new ContractAssessment();
        $assessments_due               = $contractAssessmentObj->assessmentCountNow($contract);

        $contract_assessment_done      = $contractAssessmentObj->getDoneContractAssessment($this->parameters['contract_id']);
        $contract_assessment           = $contractAssessmentObj->getActiveContractAssessment($this->parameters['contract_id']);
        //$assessments_due               = ($assessments_due - $contract_assessment_done['total_assessment']);
        $deliverableObj                = new Deliverable();
        $assessment                    = $deliverableObj->getDeliverableAssessmentDetail($contract, $this->parameters, $contract_assessment);
        $assessment['contract']        = $contractObj->getContractDetailList($contract, $this->parameters);
        $assessment['contract_detail'] = $contract;
        $assessment['contract_assessment'] = $contract_assessment;
        $assessment['assessments_due'] = $assessments_due;
        echo json_encode($assessment);
        exit();
    }


    public function getContractAssessments($options = array())
    {
        $contractAssessmentObj = new ContractAssessment();
        $option_sql  = $contractAssessmentObj->prepareSql($this->parameters);

        $contracts      = $contractAssessmentObj->getContracts($option_sql);

        $_option_sql    = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $total_contract = $contractAssessmentObj->getTotalContracts($_option_sql);

        $contracts = $contractAssessmentObj->prepareContract($contracts, $total_contract, $this->parameters);
        echo json_encode($contracts);
        exit();
    }

    public function completeContractAssessment()
    {
        $contractObj = new Contract();
        $contract    = $contractObj->getContractDetail($this->parameters['contract_id']);
        $contractAssessmentObj = new ContractAssessment();
        $contract_assessment         = $contractAssessmentObj->getContractAssessment($this->parameters['contract_assessment_id']);
        $update_data['completed_at'] = date('Y-m-d H:i:s');
        $result                  = $contractAssessmentObj->update_where($update_data, array('id' => $this->parameters['contract_assessment_id']));
        if($result)
        {
          $next_assessment_date = $contractAssessmentObj->getNextContractAssessmentDate($contract);
          if($next_assessment_date) $contractObj->update_where(array('assessment_frequency_date' => $next_assessment_date), array('id' => $this->parameters['contract_id']));
          $message = array('text' => 'Contract assessment successfully completed', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
          $message = array('text' => 'No change was made to the contract assessment', 'error' => false, 'updated' => true);
        }else {
          $message = array('text' => 'There was an error completing contract', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

    public function completeOutstandingContractAssessment()
    {
       $contractObj = new Contract();
       $contract    = $contractObj->getContractDetail($this->parameters['contract_id']);
       $contractAssessmentObj = new ContractAssessment();
       $contract_assessment['completed_at'] = date('Y-m-d H:i:s');
       $contract_assessment['contract_id']  = $this->parameters['contract_id'];
       $contract_assessment['contract_assessment_date'] = date('Y-m-d', strtotime($contract['assessment_frequency_date']));
       $contract_assessment['status']       = 1;

       $result = $contractAssessmentObj->save($contract_assessment);
       $response = array();
       if($result)
       {
          $next_assessment_date = $contractAssessmentObj->getNextContractAssessmentDate($contract);
          if($next_assessment_date) $contractObj->update_where(array('assessment_frequency_date' => $next_assessment_date), array('id' => $this->parameters['contract_id']));

          $auditLog = new ContractAssessmentAuditLog();
          $auditLog->completedOutstandingAssessment($contract_assessment, $contract, $next_assessment_date);

          $response = array('text' => 'Contract assessment successfully completed', 'error' => false, 'updated' => true);
       } else {
          $response = array('text' => 'There was an error completing contract', 'error' => true, 'updated' => false);
       }
       echo json_encode($response);
       exit();
    }

    function bind($contract)
    {
        return $contract;
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ContractAssessmentController();
    return $class->execute($_REQUEST['action']);
}

?>