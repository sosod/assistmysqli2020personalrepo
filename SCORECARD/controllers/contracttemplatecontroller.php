<?php
require_once '../contract_autoloader.php';
require_once '../contract_helper.php';
class ContractTemplateController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $contractObj = new Contract();
        $option_sql  = $contractObj->prepareSql($this->parameters);
        $response    = $contractObj->getList(null, $option_sql);
        echo json_encode($response);
        exit();
    }

    function getContractTemplates()
    {
        $contractTemplateObj = new ContractTemplate();
        $option_sql          = $contractTemplateObj->prepareSql($this->parameters);
        $templates           = $contractTemplateObj->getTemplates($option_sql);
        echo json_encode($templates);
        exit();
    }

    function getTemplateDetail()
    {
        $columnsObj = new ContractColumns();
        $columns    = $columnsObj->getHeaders();

        $contractTemplateObj           = new ContractTemplate();
        $contract_template  = $contractTemplateObj->getTemplateDetail($this->parameters['template_id']);
        $contractObj                   = new Contract();
        $contract                      = $contractObj->getContractDetail($this->parameters['contract_id']);
        $deliverableObj                = new Deliverable();
        $template                      = $deliverableObj->getDeliverableTemplateDetail($contract, $this->parameters);
        $template['contract']          = $contractObj->getContractDetailList($contract, $this->parameters);
        $template['contract_detail']   = $contract;
        $template['contract_template'] = $contract_template;
        echo json_encode($template);
        exit();
    }

    public function saveTemplateSetting()
    {
        parse_str($this->parameters['template_setting_data'], $template_setting);

        $template_setting = $this->bind($template_setting);
        $templateSettingObj = new TemplateSetting();
        $res = 0;
        foreach($template_setting as $index => $setting)
        {
            $setting['created_at']  = date('Y-m-d H:i:s');
            $setting['template_id']  = $this->parameters['template_id'];
            $res += $templateSettingObj->save($setting);
        }
        $response = array();
        if($res)
        {

            $response = array('text' => 'Template successfully saved', 'error' => false);
        } else {
            $response = array('text' => 'There was an error saving template', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    public function updateTemplateSetting()
    {
        parse_str($this->parameters['template_setting_data'], $template_setting);
        $template_setting = $this->bind($template_setting);
        $templateSettingObj = new TemplateSetting();
        $res = 0;
        $templateSettingObj->attach(new TemplateSettingAuditLog());
        foreach($template_setting as $deliverable_id => $setting)
        {
            $res += $templateSettingObj->update_where($setting, array('deliverable_id' => $deliverable_id, 'template_id' => $this->parameters['template_id']));
        }
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Template settings successfully updated', 'error' => false, 'updated' => true);
        } elseif($res == 0) {
            $response = array('text' => 'No change was made to the template settings', 'error' => false, 'updated' => false);
        } else {
            $response = array('text' => 'There was an error updating template settings', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    public function bind($template_setting)
    {
        $template_setting_data = array();
        if(isset($template_setting['required']))
        {
            foreach($template_setting['required'] as $deliverable_id => $state)
            {
                $template_setting_data[$deliverable_id] = array('deliverable_id' => $deliverable_id, 'is_required' => ($state == 1 ? 1 : 0) );
            }
        }
        if(isset($template_setting['editable']))
        {
        }
        foreach($template_setting['editable'] as $deliverable_id => $state)
        {
            $template_setting_data[$deliverable_id]['is_editable'] = ($state == 1 ? 1 : 0);
        }
        return $template_setting_data;
    }

    function updateContractTemplate()
    {
        $contractTemplateObj = new ContractTemplate();
        $res = 0;
        $contractTemplateObj->attach(new SetupAuditLog());
        if(isset($this->parameters['name']))  $contract_template['name'] = $this->parameters['name'];
        if(isset($this->parameters['status'])) $contract_template['status'] = $this->parameters['status'];

        $res = $contractTemplateObj->update_where($contract_template, array('id' => $this->parameters['id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Contract Template successfully updated', 'error' => false, 'updated' => true);
        } elseif($res == 0) {
            $response = array('text' => 'No change was made to the contract template', 'error' => false, 'updated' => false);
        } else {
            $response = array('text' => 'There was an error updating contract template', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    function saveContractTemplate()
    {
        $contract_template = array('name' => $this->parameters['name'], 'contract_id' => $this->parameters['contract_id'], 'created_at' => date('Y-m-d H:i:s'));
        $contractTemplateObj  = new ContractTemplate();
        $contract_template_id = $contractTemplateObj->save($contract_template);
        $response = array();
        if($contract_template_id)
        {
            $contractObj = new Contract();
            $res         = $contractObj->update_where(array('is_template' => 1), array('id' => $this->parameters['contract_id']));
            $response = array('text' => 'Contract Template successfully saved', 'error' => false);
        } else {
            $response = array('text' => 'There was an error saving contract template', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }



}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ContractTemplateController();
    return $class->execute($_REQUEST['action']);
}

?>