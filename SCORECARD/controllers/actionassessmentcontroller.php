<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ActionAssessmentController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $contractObj = new ActionAssessment();
        $option_sql  = $contractObj->prepareSql($this->parameters);
        $response    = $contractObj->getList(null, $option_sql);
        echo json_encode($response);
        exit();
    }

    public function saveActionAssessment()
    {
        $this->parameters['created_at']             = date('Y-m-d H:i:s');
        $actionAssessmentObj = new ActionAssessment();
        //$actionAssessmentObj->bind();
        $deliverable_assessment_id = $actionAssessmentObj->save($this->parameters);
        $response = array();
        if($deliverable_assessment_id)
        {
            $response = array('text' => 'Question assessment successfully saved', 'error' => false);
        } else {
            $response = array('text' => 'There was an error saving question assessment data', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    public function updateActionAssessment()
    {
        $actionAssessmentObj = new ActionAssessment();
        //$actionAssessmentObj->bind();
        $res = $actionAssessmentObj->update_where($this->parameters, array('id' => $this->parameters['id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Question assessment successfully updated', 'error' => false, 'updated' => true);
        } elseif($res == 0) {
            $response = array('text' => 'There was no change to the question assessment data', 'error' => false, 'updated' => false);
        } else {
            $response = array('text' => 'There was an error updating question assessment data', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }


    public function editActionAssessment()
    {
        parse_str($this->parameters['assessment_data'], $assessment_data);
        $contractAssessmentObj = new ContractAssessment();
        $current_assessment     = $contractAssessmentObj->getActiveContractAssessment($this->parameters['contract_id']);
        $response = array();
        $assessment_data['updated_at']             = date('Y-m-d H:i:s');
        $actionAssessmentObj = new ActionAssessment();

        $deliverable_assessment_id = $actionAssessmentObj->update_where($assessment_data, array('id' => $this->parameters['id']));
        $response = array();
        if($deliverable_assessment_id)
        {
            $response = array('text' => 'Deliverable assessment successfully updated', 'error' => false);
        } else {
            $response = array('text' => 'There was an error updating deliverable assessment', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    public function getActionAssessmentHtml()
    {
        $actionAssessmentObj = new ActionAssessment();
        $deliverable_assessment   = $actionAssessmentObj->getAll($this->parameters['deliverable_id']);

        $contractObj = new Contract();
        $contract    = $contractObj->getContractDetail($this->parameters['contract_id']);

        $deliveredScoreObj = new DeliveredScore();
        $d_scores = $deliveredScoreObj ->getAll(1);

        $qualityScoreObj = new QualityScore();
        $q_scores        = $qualityScoreObj->getAll(1);

        $otherScoreObj   = new OtherScore();
        $o_scores        = $otherScoreObj->getAll(1);

        $columnsObj            = new DeliverableColumns();
        $columns               = $columnsObj->getHeaders();

        $html_str = "<form id='assessment_form_".$this->parameters['deliverable_id']."' name='assessment_form_".$this->parameters['deliverable_id']."'><table>";
        $html_str .= "<tr class='th2'>";
        $html_str .= "<th></th>";
        $html_str .= "<th class='th2'>Score</th>";
        $html_str .= "<th class='th2'>Comment</th>";
        $html_str .= "</tr>";
        if($contract['is_assessed_quantitative'])
        {
            $had_status_to_assess = 0;
            $html_str .= "<tr>";
            $html_str .= "<th>".$columns['delivered_weight']."</th>";
            $html_str .= "<td>";
            $html_str .= "<select name='delivered_id' id='delivered_id'>";
            $html_str .= "<option value=''>--please select--</option>";
            foreach($d_scores as $index => $d_score)
            {
                $html_str .= "<option value='".$d_score['id']."'>".$d_score['score']."</option>";
            }
            $html_str .= "</select>";
            $html_str .= "</td>";
            $html_str .= "<td>";
            $html_str .= "<textarea name='delivered_comment' name='delivered_comment' cols='60' rows='8'></textarea>";
            $html_str .= "</td>";
            $html_str .= "</tr>";

            $had_status_to_assess += 1;
        }
        if($contract['is_assessed_qualitative'])
        {
            $html_str .= "<tr>";
            $html_str .= "<th>".$columns['quality_weight']."</th>";
            $html_str .= "<td>";
            $html_str .= "<select name='quality_id' id='quality_score'>";
            $html_str .= "<option value=''>--please select--</option>";
            foreach($q_scores as $index => $q_score)
            {
                $html_str .= "<option value='".$q_score['id']."'>".$q_score['score']."</option>";
            }
            $html_str .= "</select>";
            $html_str .= "</td>";
            $html_str .= "<td>";
            $html_str .= "<textarea name='quality_comment' id='quality_comment' cols='60' rows='8'></textarea>";
            $html_str .= "</td>";
            $html_str .= "</tr>";

        }
        if($contract['is_assessed_other'])
        {
            $html_str .= "<tr>";
            $html_str .= "<th>".(!empty($contract['other_assessed_category']) ?  $contract['other_assessed_category'] : $columns['other_weight'])."</th>";
            $html_str .= "<td>";
            $html_str .= "<select name='other_id' id='other_score'>";
            $html_str .= "<option value=''>--please select--</option>";
            foreach($o_scores as $index => $o_score)
            {
                $html_str .= "<option value='".$o_score['id']."'>".$o_score['score']."</option>";
            }
            $html_str .= "</select>";
            $html_str .= "</td>";
            $html_str .= "<td>";
            $html_str .= "<textarea name='other_comment' id='other_comment' cols='60' rows='8'></textarea>";
            $html_str .= "</td>";
            $html_str .= "</tr>";
        }

        $html_str .= "</table></form>";
        $html_str .= "<span id='message'></span>";
        echo $html_str;
    }



    public function getEditActionAssessmentHtml()
    {
        $contractObj = new Contract();
        $contract    = $contractObj->getContractDetail($this->parameters['contract_id']);

        $deliveredScoreObj = new DeliveredScore();
        $d_scores = $deliveredScoreObj ->getAll(1);

        $qualityScoreObj = new QualityScore();
        $q_scores        = $qualityScoreObj->getAll(1);

        $otherScoreObj   = new OtherScore();
        $o_scores        = $otherScoreObj->getAll(1);

        $columnsObj            = new DeliverableColumns();
        $columns               = $columnsObj->getHeaders();

        $contractAssessmentObj = new ContractAssessment();
        $contract_assessment   = $contractAssessmentObj->getContractAssessment($this->parameters['contract_assessment_id']);

        $actionAssessmentObj = new ActionAssessment();
        $deliverable_assessment   = $actionAssessmentObj->getByAssessmentAndDeliverableId($this->parameters['contract_assessment_id'], $this->parameters['deliverable_id']);
        $html_str = "<form id='assessment_form_".$this->parameters['deliverable_id']."' name='assessment_form_".$this->parameters['deliverable_id']."'><table>";
        $html_str .= "<tr class='th2'>";
        $html_str .= "<th></th>";
        $html_str .= "<th class='th2'>Score</th>";
        $html_str .= "<th class='th2'>Comment</th>";
        $html_str .= "</tr>";
        if($contract['is_assessed_quantitative'])
        {
            $had_status_to_assess = 0;
            $html_str .= "<tr>";
            $html_str .= "<th>".$columns['delivered_weight']."</th>";
            $html_str .= "<td>";
            $html_str .= "<select name='delivered_id' id='delivered_id'>";
            $html_str .= "<option value=''>--please select--</option>";
            foreach($d_scores as $index => $d_score)
            {
                $html_str .= "<option value='".$d_score['id']."' ".($deliverable_assessment['delivered_score'] == $d_score['id'] ? "selected='selected'" : '')." >".$d_score['score']."</option>";
            }
            $html_str .= "</select>";
            $html_str .= "</td>";
            $html_str .= "<td>";
            $html_str .= "<textarea name='delivered_comment' name='delivered_comment' cols='60' rows='8'>".$deliverable_assessment['delivered_comment']."</textarea>";
            $html_str .= "</td>";
            $html_str .= "</tr>";

            $had_status_to_assess += 1;
        }
        if($contract['is_assessed_qualitative'])
        {
            $html_str .= "<tr>";
            $html_str .= "<th>".$columns['quality_weight']."</th>";
            $html_str .= "<td>";
            $html_str .= "<select name='quality_id' id='quality_score'>";
            $html_str .= "<option value=''>--please select--</option>";
            foreach($q_scores as $index => $q_score)
            {
                $html_str .= "<option value='".$q_score['id']."' ".($deliverable_assessment['quality_score'] == $q_score['id'] ? "selected='selected'" : '').">".$q_score['score']."</option>";
            }
            $html_str .= "</select>";
            $html_str .= "</td>";
            $html_str .= "<td>";
            $html_str .= "<textarea name='quality_comment' id='quality_comment' cols='60' rows='8'>".$deliverable_assessment['quality_comment']."s</textarea>";
            $html_str .= "</td>";
            $html_str .= "</tr>";

        }
        if($contract['is_assessed_other'])
        {
            $html_str .= "<tr>";
            $html_str .= "<th>".(!empty($contract['other_assessed_category']) ?  $contract['other_assessed_category'] : $columns['other_weight'])."</th>";
            $html_str .= "<td>";
            $html_str .= "<select name='other_id' id='other_score'>";
            $html_str .= "<option value=''>--please select--</option>";
            foreach($o_scores as $index => $o_score)
            {
                $html_str .= "<option value='".$o_score['id']."' ".($deliverable_assessment['other_score'] == $o_score['id'] ? "selected='selected'" : '').">".$o_score['score']."</option>";
            }
            $html_str .= "</select>";
            $html_str .= "</td>";
            $html_str .= "<td>";
            $html_str .= "<textarea name='other_comment' id='other_comment' cols='60' rows='8'>".$deliverable_assessment['other_comment']."</textarea>";
            $html_str .= "</td>";
            $html_str .= "</tr>";
        }

        $html_str .= "</table><input type='hidden' name='id' id='id' value='".$deliverable_assessment['id']."' /></form>";
        $html_str .= "<span id='message'></span>";
        echo $html_str;
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ActionAssessmentController();
    return $class->execute($_REQUEST['action']);
}

?>