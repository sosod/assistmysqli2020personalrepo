<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class GlossaryController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $contractObj = new Glossary();
        $sql         = '';
        if(isset($this->parameters['category']) && !empty($this->parameters['category']))
        {
            $sql = " AND G.category = '".$this->parameters['category']."' ";
        }
        $results    = $contractObj->getGlossaries($sql);
        $glossary_list = array();
        foreach($results as $index => $glossary)
        {
            $glossary_list[$glossary['id']]             = $glossary;
            if($glossary['category_id'] == 'deliverable') $glossary['category_id'] = 'Heading';
            if($glossary['category_id'] == 'action') $glossary['category_id'] = 'Question';
            $glossary_list[$glossary['id']]['category'] =ucwords( str_replace('_', ' ', $glossary['category_id']) );
        }
        echo json_encode($glossary_list);
        exit();
    }

    function save($options = array())
    {
        $glossaryObj             = new Glossary();
        $glossary                = $this->parameters;
        $glossary['insert_user'] = $_SESSION['tid'];
        $glossary['created_at']  = date('Y-m-d H:i:s');
        $glossary['status']      = 1;
        $glossaryObj->attach(new SetupAuditLog());
        $result                       = $glossaryObj->save($glossary);
        if($result)
        {
            $message = array('text' => 'Glossary successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving glossary', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $contract_typeObj             = new Glossary();
        $contract_type                = $this->parameters;
        $contract_type['insert_user'] = $_SESSION['tid'];
        $contract_type['created_at']  = date('Y-m-d H:i:s');
        $contract_typeObj->attach(new SetupAuditLog());
        $result                       = $contract_typeObj->update_where($contract_type, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Glossary successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the glossary', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error saving glossary', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

    public function delete_user()
    {
        $userObj             = new User();
        $user_data['status'] = User::DELETED;
        $userObj->attach(new UserAuditLog());
        $result   = $userObj->delete($user_data, array('id' => $this->parameters['id']));
        if($result)
        {
            $this->setFlash('success', 'User successfully saved');
        } else {
            $this->setFlash('error', 'There was an error saving the user');
        }
        $this->redirect('setup/edit_user.php?id='.$this->parameters['id']);
    }



}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new GlossaryController();
    return $class->execute($_REQUEST['action']);
}

?>