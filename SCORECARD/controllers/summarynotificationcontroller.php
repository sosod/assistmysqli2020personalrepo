<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class SummaryNotificationController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $summaryNotificationObj = new SummaryNotification();
        $response    = $summaryNotificationObj->getAll();
        echo json_encode($response);
        exit();
    }

    function getSummaryNotifications()
    {
        $summaryNotificationObj = new SummaryNotification();
        $notifications = $summaryNotificationObj->getNotifications();
        echo json_encode($notifications);
    }

    function save($options = array())
    {
        $summaryNotificationObj = new SummaryNotification();
        $summary_notification   = $this->parameters;
        $recieve_who            = array();
        if(isset($summary_notification['recieve_who']))
        {
            if(!empty($summary_notification['recieve_who']))
            {
                $recieve_who = $summary_notification['recieve_who'];
            }
            unset($summary_notification['recieve_who']);
        }
        $summary_notification['insert_user'] = $_SESSION['tid'];
        $summary_notification['created_at']  = date('Y-m-d H:i:s');
        $summary_notification['status']      = 1;
        $summaryNotificationObj->attach(new SetupAuditLog());
        $result                       = $summaryNotificationObj->save($summary_notification, 'SummaryNotification');
        if($result)
        {
            $summaryNotificationUserObj = new SummaryNotificationUser();
            foreach($recieve_who as $index => $user)
            {
                $summaryNotificationUserObj->save(array('summary_notification_id' => $result, 'user_id' => $user));
            }
            $message = array('text' => 'Summary notification successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving summary notification', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $summaryNotificationObj                      = new SummaryNotification();
        $summary_notification                = $this->parameters;
        $summary_notification['insert_user'] = $_SESSION['tid'];
        $recieve_who            = array();
        if(isset($summary_notification['recieve_who']))
        {
            unset($summary_notification['recieve_who']);
        }
        $summaryNotificationObj->attach(new SummaryNotificationAuditLog());
        $result                       = $summaryNotificationObj->update_where($summary_notification, array('id' => $this->parameters['id']));


        if($result)
        {
            $message = array('text' => 'Service successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the service', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error saving service', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

    function deleteSummaryNotification()
    {
        $summaryNotificationObj = new SummaryNotification();
        $res =  $summaryNotificationObj->deleteNotification($_POST['id']);
        echo json_encode(array('error' => false, 'text' => 'Notification Deleted'));
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new SummaryNotificationController ();
    return $class->execute($_REQUEST['action']);
}

?>