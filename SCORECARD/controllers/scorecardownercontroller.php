<?php
require_once '../contract_autoloader.php';
require_once '../helper.php';
spl_autoload_register("Autoload::load");
class ScoreCardOwnerController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $contractOwnerObj = new ScoreCardOwner();
        $response         = $contractOwnerObj->getAll();
        echo json_encode($response);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ScoreCardOwnerController ();
    return $class->execute($_REQUEST['action']);
}

?>