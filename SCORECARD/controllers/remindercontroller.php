<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ReminderController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $reminderObj = new Reminder();
        $response    = $reminderObj->getAll();
        echo json_encode($response);
        exit();
    }

    function save($options = array())
    {
        $reminderObj             = new Reminder();
        $reminder                = $this->parameters;
        $reminder['insert_user'] = $_SESSION['tid'];
        $reminder['created_at']  = date('Y-m-d H:i:s');
        $reminderObj->attach(new SetupAuditLog());
        $result                       = $reminderObj->save($reminder);
        if($result)
        {
            $message = array('text' => 'Reminder successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving reminder', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $reminderObj             = new Reminder();
        $reminder                = $this->parameters;
        $reminder['insert_user'] = $_SESSION['tid'];
        $reminderObj->attach(new ReminderAuditLog());
        $result                       = $reminderObj->update_where($reminder, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Reminder successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the reminder', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error saving reminder', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ReminderController();
    return $class->execute($_REQUEST['action']);
}

?>