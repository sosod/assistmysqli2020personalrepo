<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ScoreCardController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $scoreCardObj = new ScoreCard();
        $option_sql  = $scoreCardObj->prepareSql($this->parameters);
        $response    = $scoreCardObj->getList($option_sql);
        echo json_encode($response);
        exit();
    }

    public function getSetupList($options = array())
    {
        $scoreCardObj = new ScoreCard();
        $option_sql  = $scoreCardObj->prepareSql($this->parameters);
        $response    = $scoreCardObj->getScoreCardSetupList(true, $option_sql);
        echo json_encode($response);
        exit();
    }

    public function getAdminList($option = array())
    {
        $scoreCardObj = new ScoreCard();
        $option_sql  = $scoreCardObj->prepareSql($this->parameters);
        $response    = $scoreCardObj->getList($option_sql);
        echo json_encode($response);
        exit();
    }


    public function getScoreCards($options = array())
    {
        $scoreCardObj = new ScoreCard();
        $option_sql  = $scoreCardObj->prepareSql($this->parameters);

        $score_cards      = $scoreCardObj->getScoreCards($option_sql);

        $_option_sql    = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $total_score_card = $scoreCardObj->getTotalScoreCards($_option_sql);

        $score_cards = $scoreCardObj->prepareScoreCard($score_cards, $total_score_card, $this->parameters);
        echo json_encode($score_cards);
        exit();
    }


    function save($options = array())
    {
        parse_str($this->parameters['score_card'], $posted_score_card);
        $posted_score_card = $posted_score_card['score_card'];
        $documents            = array();
        if(isset($posted_score_card['documents']))
        {
           $documents = $posted_score_card['documents'];
           unset($posted_score_card['documents']);
        }
        $scoreCardObj          = new ScoreCard();
        $score_card           = $this->bind($posted_score_card);
        $message            = array();

        if($scoreCardObj->isValid($score_card))
        {
            $score_card['insert_user'] = $_SESSION['tid'];
            $score_card['created_at']  = date('Y-m-d H:i:s');
            $score_card['status']      = 1;
            $score_card['status_id']   = 1;
            $score_card['has_deliverable'] = 1;
            $score_card_id                  = $scoreCardObj->save($score_card);
            if($score_card_id)
            {
                if(isset($documents) && !empty($documents))
                {
                    $this->saveScoreCardDocuments($documents, $score_card_id);
                }
                if(isset($posted_score_card['template_id']) && !empty($posted_score_card['template_id']))
                {
                   $scoreCardObj->saveScoreCardTemplateData($posted_score_card['template_id'], $score_card_id);
                }
                //set_flash('success', 'score_card', 'ScoreCard successfully saved');
                $message = array('text' => 'Scorecard successfully saved', 'error' => false, 'id' => $score_card_id);
            } else {
                //set_flash('error', 'score_card', 'There was an error saving score card');
                $message = array('text' => 'There was an error saving scorecard', 'error' => true);
            }
        }
        echo json_encode($message);
        exit();
        //$this->redirect('new/index.php');
    }

    protected function saveScoreCardDocuments($documents, $score_card_id)
    {
        $score_cardDocumentObj = new ScoreCardDocument();
        foreach($documents as $filename => $file_description)
        {
            $docs = array('filename' => $filename, 'description' => $file_description, 'score_card_id' => $score_card_id);
            $score_cardDocumentObj->save($docs);
        }
        unset($_SESSION['uploads']);
    }


    public function editScoreCard()
    {
        parse_str($this->parameters['score_card'], $posted_score_card);

        $documents            = array();
        if(isset($posted_score_card['score_card']['documents']))
        {
           $documents = $posted_score_card['score_card']['documents'];
           unset($posted_score_card['score_card']['documents']);
        }
        $scoreCardObj          = new ScoreCard();
        $score_card           = $this->bind($posted_score_card['score_card']);
        $message            = array();
        if($scoreCardObj->isValid($score_card))
        {
            $scoreCardObj->attach(new ScoreCardAuditLog());
            $result                    = $scoreCardObj->update_where($score_card, array('id' => $score_card['id']));

            if(isset($documents) && !empty($documents))
            {
                $score_cardDocumentObj = new ScoreCardDocument();
                foreach($documents as $filename => $file_description)
                {
                    $docs = array('filename' => $filename, 'description' => $file_description, 'score_card_id' => $result);
                    $result +=$score_cardDocumentObj->save($docs);
                }
                unset($_SESSION['uploads']);
            }
            if($result)
            {
                $score_card_data    = $scoreCardObj->getScoreCardDetail($score_card['id']);
                $scoreCardObj->updateActivityOnManage($score_card_data);
                //set_flash('success', 'score_card', 'ScoreCard successfully saved');
                $message = array('text' => 'Scorecard updated successfully', 'error' => false, 'updated' => true);
            } elseif($result == 0){
                $message = array('text' => 'No change was made to the scorecard', 'error' => false, 'updated' => false);
            } else {
                //set_flash('error', 'score_card', 'There was an error saving score card');
                $message = array('text' => 'There was an error updating scorecard', 'error' => true);
            }
        }
        echo json_encode($message);
        exit();
    }

    public function updateScoreCard()
    {
        $scoreCardObj            = new ScoreCard();
        $score_card                = $this->parameters;
        unset($score_card['approval']);
        unset($score_card['response']);
        $score_card_data           = $this->bind($score_card);
        $scoreCardObj->attach(new ScoreCardAuditLog());
        $result                  = $scoreCardObj->update_where($score_card_data, array('id' => $this->parameters['id']));
        if($result)
        {
            if(isset($score_card_data['status_id']) && $score_card_data['status_id'] == 3)
            {
               //send mail to CA
            }
            $score_card    = $scoreCardObj->getScoreCardDetail($this->parameters['id']);
            $scoreCardObj->updateActivityOnManage($score_card);
            $message = array('text' => 'Scorecard successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the scorecard', 'error' => false, 'updated' => false);
        }else {
            $message = array('text' => 'There was an error saving scorecard', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

    public function deleteScoreCard()
    {
        $scoreCardObj = new ScoreCard();
        $scoreCardObj->attach(new ScoreCardAuditLog());
        $res = $scoreCardObj->update_where(array('status' => ScoreCard::DELETED), array('id' => $this->parameters['score_card_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Scorecard successfully deleted', 'error' => false, 'updated' => true);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the scorecard', 'error' => false, 'updated' => false);
        } else {
            $response = array('text' => 'Error occurred deleting a scorecard', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    public function deActivateScoreCard()
    {
        $scoreCardObj = new ScoreCard();
        $score_card   = $scoreCardObj->getScoreCard($this->parameters['score_card_id']);
        $scoreCardObj->attach(new ScoreCardAuditLog());
        $response = array();
        $res      = 0;
        if( ($score_card['status'] & ScoreCard::CLOSED) !=  ScoreCard::CLOSED)
        {
            $status['status'] = $score_card['status'] + ScoreCard::CLOSED;
            $res = $scoreCardObj->update_where($status, array('id' => $this->parameters['score_card_id']));
        }
        if($res > 0)
        {
            $response = array('text' => 'Scorecard successfully deactivated', 'error' => false, 'updated' => true);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the scorecard', 'error' => false, 'updated' => false);
        } else {
            $response = array('text' => 'Error occurred deleting a scorecard', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    public function reActivateScoreCard()
    {
        $scoreCardObj = new ScoreCard();
        $score_card   = $scoreCardObj->getScoreCard($this->parameters['score_card_id']);
        $scoreCardObj->attach(new ScoreCardAuditLog());
        $response = array();
        $res      = 0;
        if( ($score_card['status'] & ScoreCard::CLOSED) ==  ScoreCard::CLOSED)
        {
            $status['status'] = $score_card['status'] - ScoreCard::CLOSED;
            $res = $scoreCardObj->update_where($status, array('id' => $this->parameters['score_card_id']));
        }
        if($res > 0)
        {
            $response = array('text' => 'Scorecard successfully activated', 'error' => false, 'updated' => true);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the scorecard', 'error' => false, 'updated' => false);
        } else {
            $response = array('text' => 'Error occurred deleting a scorecard', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }


    public function getDetailScoreCard()
    {
        $columnsObj = new ScoreCardColumns();
        $columns    = $columnsObj->getHeaders();

        $scoreCardObj      = new ScoreCard();
        $score_card         = $scoreCardObj->getScoreCardDetail($this->parameters['score_card_id']);
        $score_card_detail  = $scoreCardObj->getScoreCardDetailList($score_card);

        $categoryObj      = new DeliverableCategory();
        $categories       = $categoryObj->getList();
        //$deliverableObj           = new Deliverable();
        //$categorized_deliverables = $deliverableObj->getCategoryDeliverable($this->parameters);

        $categorized_deliverables['score_card']= $score_card_detail;
        echo json_encode($categorized_deliverables);
        exit();
    }

    function getConfirmationDetail()
    {
        $columnsObj = new ScoreCardColumn();
        $columns    = $columnsObj->getHeaders();

        $scoreCardObj                  = new ScoreCard();
        $score_card                     = $scoreCardObj->getScoreCardDetail($this->parameters['score_card_id']);

        $deliverableObj               = new Deliverable();
        $confirmation                 = $deliverableObj->getConfirmDeliverableDetail($score_card, $this->parameters);
        $confirmation['score_card']     = $scoreCardObj->getScoreCardDetailList($score_card);
        echo json_encode($confirmation);
        exit();
    }

    public function confirm()
    {
        $scoreCardObj = new ScoreCard();
        $score_card    = $scoreCardObj->getScoreCardDetail($this->parameters['score_card_id']);
        $message     = '';
        if($score_card)
        {
            $update_data['status']   = ScoreCard::ACTIVE + ScoreCard::CONFIRMED;
            $scoreCardObj->attach(new ScoreCardAuditLog());
            $result                  = $scoreCardObj->update_where($update_data, array('id' => $this->parameters['score_card_id']));
            if($result)
            {
                $message = array('text' => 'Scorecard successfully confirmed', 'error' => false, 'updated' => true);
            } elseif($result == 0) {
                $message = array('text' => 'No change was made to the scorecard', 'error' => false, 'updated' => true);
            }else {
                $message = array('text' => 'There was an error confirming scorecard', 'error' => true, 'updated' => false);
            }
        }
        echo json_encode($message);
        exit();
    }

    public function undoConfirmScoreCard()
    {
        $scoreCardObj = new ScoreCard();
        $score_card    = $scoreCardObj->getScoreCardDetail($this->parameters['score_card_id']);
        $message     = '';
        if($score_card)
        {
            if( ($score_card['status'] & ScoreCard::CONFIRMED) == ScoreCard::CONFIRMED)
            {
                $update_data['status']   = $score_card['status'] - ScoreCard::CONFIRMED;
            }
            $scoreCardObj->attach(new ScoreCardAuditLog());
            $result                  = $scoreCardObj->update_where($update_data, array('id' => $this->parameters['score_card_id']));
            if($result)
            {
                $message = array('text' => 'Scorecard successfully unconfirmed', 'error' => false, 'updated' => true);
            } elseif($result == 0) {
                $message = array('text' => 'No change was made to the scorecard', 'error' => false, 'updated' => true);
            }else {
                $message = array('text' => 'There was an error unconfirming scorecard', 'error' => true, 'updated' => false);
            }
        }
        echo json_encode($message);
        exit();
    }


    public function getActivationScoreCards()
    {
        $scoreCardObj  = new ScoreCard();

        $this->parameters['type'] = 'awaiting';
        $awaiting_sql = $scoreCardObj->prepareSql($this->parameters);

        $this->parameters['type'] = 'activated';
        $activated_sql = $scoreCardObj->prepareSql($this->parameters);


        $score_cards          = $scoreCardObj->getScoreCards($awaiting_sql);
        $awaiting_score_cards = $scoreCardObj->prepareScoreCard($score_cards, 0, $this->parameters);

        $score_cards          = $scoreCardObj->getScoreCards($activated_sql);
        $activated_score_cards =$scoreCardObj->prepareScoreCard($score_cards, 0, $this->parameters);

        $columnsObj             = new ScoreCardColumn();
        //$_option_sql    = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $score_cards['awaiting']  = $awaiting_score_cards;
        $score_cards['activated'] = $activated_score_cards;
        $score_cards['columns']   = $columnsObj->getHeaders();;
        echo json_encode($score_cards);
        exit();
    }

    public function activateScoreCard()
    {
        $scoreCardObj = new ScoreCard();
        $score_card    = $scoreCardObj->getScoreCardDetail($this->parameters['score_card_id']);
        $message     = '';
        if($score_card)
        {
            $update_data['status']   = ($score_card['status'] - ScoreCard::CONFIRMED) + ScoreCard::ACTIVATED;
            $scoreCardObj->attach(new ScoreCardAuditLog());
            $result                  = $scoreCardObj->update_where($update_data, array('id' => $this->parameters['score_card_id']));
            if($result)
            {
                $message = array('text' => 'Scorecard successfully activated', 'error' => false, 'updated' => true);
            } elseif($result == 0) {
                $message = array('text' => 'No change was made to the scorecard', 'error' => false, 'updated' => true);
            }else {
                $message = array('text' => 'There was an error activating scorecard', 'error' => true, 'updated' => false);
            }
        }
        echo json_encode($message);
        exit();

    }

    public function undoActivateScoreCard()
    {
        $scoreCardObj = new ScoreCard();
        $score_card    = $scoreCardObj->getScoreCardDetail($this->parameters['score_card_id']);
        $message     = '';
        if($score_card)
        {
            $update_data['status']   = ($score_card['status'] - ScoreCard::ACTIVATED) + ScoreCard::CONFIRMED;
            $scoreCardObj->attach(new ScoreCardAuditLog());
            $result                  = $scoreCardObj->update_where($update_data, array('id' => $this->parameters['score_card_id']));
            if($result)
            {
                $message = array('text' => 'Scorecard successfully deactivated', 'error' => false, 'updated' => true);
            } elseif($result == 0) {
                $message = array('text' => 'No change was made to the scorecard', 'error' => false, 'updated' => true);
            }else {
                $message = array('text' => 'There was an error deactivating scorecard', 'error' => true, 'updated' => false);
            }
        }
        echo json_encode($message);
        exit();
    }

    function bind($score_card)
    {
        if(isset($score_card['remind_on']))
        {
            $remind_on = strtotime($score_card['remind_on']);
            if($remind_on > 0)
            {
                $score_card['remind_on'] = date('Y-m-d', $remind_on);
            }
        }
        $score_card['use_corrective_action']                 = (isset($score_card['use_corrective_action']) ? 1 : 0);
        $score_card['is_corrective_action_owner_identified'] = (isset($score_card['is_corrective_action_owner_identified']) ? 1 : 0);
        $score_card['use_rating_comment']                    = (isset($score_card['use_rating_comment']) ? 1 : 0);
       return $score_card;
    }


    public function removeScoreCardAttachment()
    {
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('score_card_new', 'new_score_card_attachment');
        $response = $attObj -> removeFile($file, 'score_card_new');
        echo json_encode($response);
    }

    public function saveAttachment()
    {
        $attObj    = new Attachment('scorecard_document', 'scorecard_document');
        $results   = $attObj->upload('new_score_card_document');
        echo json_encode($results);
    }

    public function download()
    {
        header("Content-type:".$_GET['content']);
        header("Content-disposition: attachment; filename=".$_GET['new']);
        readfile("../../files/".$_GET['company']."/".$_SESSION['modref']."/".$_GET['folder']."/".$_GET['file']);
    }

    public function deleteScoreCardFile()
    {
        $file      = $_POST['attachment'];
        $attObj    = new Attachment("scorecard_".$_POST['score_card_id'], 'scorecard_'.$_POST['score_card_id']);
        $response  = $attObj->deleteFile($file, $_POST['attachment']);
        echo json_encode($response);
    }

    function getScoreCardDetailHtml()
    {
        $scoreCardObj = new ScoreCard();
        echo $scoreCardObj->getScoreCardHtml($this->parameters['id']);
        exit();
    }

    function getAssessmentDetail()
    {
        $columnsObj = new ScoreCardColumns();
        $columns    = $columnsObj->getHeaders();

        $scoreCardObj                   = new ScoreCard();
        $score_card                      = $scoreCardObj->getScoreCardDetail($this->parameters['score_card_id']);
        $deliverableObj                = new Deliverable();
        $assessment                    = $deliverableObj->getDeliverableAssessmentDetail($score_card, $this->parameters);
        $assessment['score_card']        = $scoreCardObj->getScoreCardDetailList($score_card, $this->parameters);
        $assessment['score_card_detail'] = $score_card;
        echo json_encode($assessment);
        exit();
    }

    public function complete()
    {
        $scoreCardObj = new ScoreCard();
        $score_card    = $scoreCardObj->getScoreCardDetail($this->parameters['score_card_id']);
        $message     = '';
        if($score_card)
        {
            if( ($score_card['status'] & ScoreCard::CLOSED) != ScoreCard::CLOSED) $update_data['status'] = $score_card['status'] + ScoreCard::CLOSED;
            $scoreCardObj->attach(new ScoreCardAuditLog());
            $update_data['completed_by']    = $_SESSION['tid'];
            $update_data['overall_comment'] = $this->parameters['overall_comment'];
            $update_data['completed_at']    = date('Y-m-d');
            $result                  = $scoreCardObj->update_where($update_data, array('id' => $this->parameters['score_card_id']));
            if($result)
            {
                $message = array('text' => 'Scorecard successfully confirmed', 'error' => false, 'updated' => true);
            } elseif($result == 0) {
                $message = array('text' => 'No change was made to the scorecard', 'error' => false, 'updated' => true);
            }else {
                $message = array('text' => 'There was an error confirming scorecard', 'error' => true, 'updated' => false);
            }
        }
        echo json_encode($message);
        exit();
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ScoreCardController();
    return $class->execute($_REQUEST['action']);
}

?>