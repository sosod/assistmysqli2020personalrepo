<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class NamingController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }


    public function getAll()
    {
        $namingObj   = new Naming();
        $namings     = $namingObj->getAll();
        $naming_list = array();
        foreach($namings as $naming)
        {
            if(isset($this->parameters['type']) && !empty($this->parameters['type']))
            {
                if($naming['type'] == $this->parameters['type'])
                {
                    $naming_list[$naming['id']] = $naming;
                }
            } else {
                $naming_list[$naming['id']] = $naming;
            }
        }
        echo json_encode($naming_list);
        exit();
    }


    public function update()
    {
        $namingObj             = new Naming();
        $naming                = $this->parameters;
        $naming['updated_at']  = date('Y-m-d H:i:s');
        $namingObj->attach(new SetupAuditLog());
        $result              = $namingObj->update_where($naming, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Naming successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the naming', 'error' => false, 'updated' => false);
        }else {
            $message = array('text' => 'There was an error updating naming', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

    public function updateOrder()
    {
        parse_str($this->parameters['data'], $columns);
        $namingObj             = new Naming();
        $i = 0;
        foreach($columns['columns'] as $id => $column_id)
        {
            $i++;
            $column_update = array();
            $column_status = 1;
            if(isset($columns['manage'][$column_id]))
            {
               $column_status += Naming::MANAGE_PAGE;
            }
            if(isset($columns['new'][$column_id]))
            {
                $column_status += Naming::NEW_PAGE;
            }
            $column_update = array('status' => $column_status, 'position' => $i);
            $namingObj->update_where($column_update, array('id' => $column_id));
        }
        echo json_encode(array('text' => 'Columns successfully updated', 'error' => false));
        exit();
    }

}

if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new NamingController();
    return $class->execute($_REQUEST['action']);
}