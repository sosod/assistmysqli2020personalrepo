<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ScoreCardCategoryController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $contractObj = new ScoreCardCategory();
        $response    = $contractObj->getAll();
        echo json_encode($response);
        exit();
    }

    function save($options = array())
    {
        $categoryObj                      = new ScoreCardCategory();
        $contract_category                = $this->parameters;
        $contract_category['insert_user'] = $_SESSION['tid'];
        $contract_category['created_at']  = date('Y-m-d H:i:s');
        $contract_category['status']      = 1;
        $categoryObj->attach(new SetupAuditLog());
        $result                       = $categoryObj->save($contract_category, 'Score Card Category');
        if($result)
        {
            $message = array('text' => 'Score card category successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving score card category', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $categoryObj                      = new ScoreCardCategory();
        $contract_category                = $this->parameters;
        $contract_category['insert_user'] = $_SESSION['tid'];
        $contract_category['created_at']  = date('Y-m-d H:i:s');
        $categoryObj->attach(new SetupAuditLog());
        $result                       = $categoryObj->update_where($contract_category, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Score card category successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the score card category', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error saving score card category', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ScoreCardCategoryController();
    return $class->execute($_REQUEST['action']);
}

?>