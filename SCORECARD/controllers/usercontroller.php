<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class UserController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
       $userObj = new User();
       $users   = $userObj->getList();
       echo json_encode($users);
    }

    function getUserSetup()
    {
        $userObj           = new User();
        $users_list        = $userObj->getUsers();
        $user_set          = $userObj->getUserSettings();
        $list_of_new_users = array();
        $list_of_set_users = array();
        foreach($users_list as $index => $user)
        {
            if(!isset($user_set[$user['setting_id']]))
            {
                $list_of_new_users[$user['tkid']] = $user;
            }
        }
        $users = array('available' => $list_of_new_users, 'setup' => $user_set);
        echo json_encode($users);
    }

    function save($options = array())
    {
        $user_data = array();
        $userObj   = new User();
        parse_str($this->parameters['status'], $user_data);
        $user_data = $this->prepareData($user_data, $userObj);

        $user_data['insert_user'] = $_SESSION['tid'];
        $user_data['created_at']  = date('Y-m-d H:i:s');
        $user_data['status']      = $user_data['status'] + User::ACTIVE;
        $result                   = $userObj->save($user_data);
        if($result)
        {
            $response = array('text' => 'User successfully saved', 'error' => false);
        } else {
            $response = array('text' => 'There was an error saving the user', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }



    public function update()
    {
        $userObj   = new User();
        parse_str($this->parameters['data'], $user);
        $user_data['status'] = $userObj->calculateUserSettingStatus($user['status']);
        $userObj->attach(new UserAuditLog());
        $result   = $userObj->update_where($user_data, array('id' => $user['id']));
        $response = array();
        if($result)
        {
            $response = array('text' => 'User successfully updated', 'error' => false, 'updated' => true);
            //$this->setFlash('success', 'User successfully saved');
        } elseif($result == 0) {
            $response = array('text' => 'No change was made to the user', 'error' => false, 'updated' => false);
        } else {
            $response = array('text' => 'An error occurred updating user details', 'error' => true);
            //$this->setFlash('error', 'There was an error saving the user');
        }
        echo json_encode($response);
        exit();
    }

    public function delete_user()
    {
        $userObj             = new User();
        $user_data['status'] = User::DELETED;
        $userObj->attach(new UserAuditLog());
        $result   = $userObj->delete($user_data, array('id' => $this->parameters['id']));
        if($result)
        {
            $this->setFlash('success', 'User successfully saved');
        } else {
            $this->setFlash('error', 'There was an error saving the user');
        }
        $this->redirect('setup/edit_user.php?id='.$this->parameters['id']);
    }

    private function prepareData($data, $userObj)
    {
        $user_data = array();
        foreach($data as $key => $val)
        {
            if($key == 'status')
            {
                $user_data[$key] = $userObj->calculateUserSettingStatus($val);
            } else {
                $user_data[$key] = $val;
            }
        }
        return $user_data;
    }

    public function setDefaultFinancialYear()
    {
       $userObj = new User();
       $res     = $userObj->update_where(array('financial_year_id' => $this->parameters['default_year']), array('id' => $this->parameters['user_id']));
       $response = array();
       if($res > 0)
       {
           $mod_ref = $_SESSION['modref'];
           $user    = $_SESSION['tid'];
           $_SESSION[$mod_ref][$user]['default_financial_year'] = $this->parameters['default_year'];
           $response = array('text' => 'Your default financial year set successfully', 'error' => false);
       } elseif($res == 0){
           $response = array('text' => 'No change made to the default financial year', 'error' => false);
       } else {
           $response = array('text' => 'Error updating your default financial year', 'error' => false);
       }
       echo json_encode($response);
       exit();
    }


}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new UserController();
    return $class->execute($_REQUEST['action']);
}

?>