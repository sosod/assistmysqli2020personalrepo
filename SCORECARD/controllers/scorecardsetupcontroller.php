<?php
require_once '../score_card_auto_loader.php';
require_once '../score_card_helper.php';
class ScoreCardSetupController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getSetupScoreCardList()
    {
        $scoreCardObj = new ScoreCard();
        $score_cards  = $scoreCardObj->getScoreCardsWithSetupByScoreCardId($this->parameters['score_card_id']);
        $list = array();
        foreach($score_cards as $index => $score_card)
        {
          $list[$score_card['setup_id']] = $score_card['setup_reference'];
        }
        echo json_encode($list);
        exit();

    }

    public function getSetupDetail()
    {
       $columnsObj = new ScoreCardColumn();
       $columns    = $columnsObj->getHeaders();

       $userObj = new User();
       $users   = $userObj->getList();

       $scoreCardOwnerObj = new ScoreCardOwner();
       $directorates      = $scoreCardOwnerObj->getList();

       $scoreCardObj           = new ScoreCard();
       $score_card             = $scoreCardObj->getScoreCardDetail($this->parameters['score_card_id']);

       $scoreCardSetupObj       = new ScoreCardSetup();
       $setup                   = $scoreCardSetupObj->getActivateDetail($score_card, $this->parameters);
       $setup['score_card']     = $scoreCardObj->getScoreCardDetailList($score_card);
       $setup['users']          = $users;
       $setup['directorates']   = $directorates;
       echo json_encode($setup);
       exit();
    }

    public function setupScoreCard()
    {
        parse_str($this->parameters['setup_data'], $parameters);
        $score_card_setup  = $parameters['setup'];
        $allocate_to_user  = array();
        $recipient_user    = array();
        $allocate_to_directorate = array();
        $has_users                = false;

        if(isset($score_card_setup['allocate_to_directorate']))
        {
            $allocate_to_directorate = $score_card_setup['allocate_to_directorate'];
            if(!empty($allocate_to_directorate)) $has_users = true;
            unset($score_card_setup['allocate_to_directorate']);
        }
        if(isset($score_card_setup['allocate_to_user']))
        {
            $allocate_to_user = $score_card_setup['allocate_to_user'];
            if(!empty($allocate_to_user))
            {
                $has_users = true;
                foreach($allocate_to_user as $index => $user)
                {
                    if(empty($user)) $has_users = false;
                }
            }
            unset($score_card_setup['allocate_to_user']);
        }
        if(isset($score_card_setup['recipient_user']))
        {
            $recipient_user = $score_card_setup['recipient_user'];
            if(!empty($recipient_user))
            {
               $has_users = true;
            }
            unset($score_card_setup['recipient_user']);
        }
        $response  = array();
        if(!$has_users)
        {
            $response = array('text' => 'No question recipients have been selected', 'error' => true);
        } else {
            $setupScoreCardObj = new ScoreCardSetup();

            $setup_data             = $this->bind($score_card_setup);
            //$setup_data['status']    = 1;

            $result                  = $setupScoreCardObj->save($setup_data);
            if($result)
            {
                $scoreCardObj = new ScoreCard();
                $score_card   = $scoreCardObj->getScoreCardDetail($setup_data['score_card_id']);
                $user_list    = array();
                $score_card_id = 0;
                if(!empty($allocate_to_user))
                {
                    $score_card_id = $scoreCardObj->copyAndActivateToUser($score_card_setup, $result, $allocate_to_user);
                    $user_list     = $allocate_to_user;
                }
                if(!empty($allocate_to_directorate))
                {
                    $allocateToDirectorateObj = new ScoreCardAllocateToDirectorate();
                    foreach($allocate_to_directorate as $index => $user_id)
                    {
                        $allocateToDirectorateObj->save(array('score_card_setup_id' => $result, 'directorate_id' => $user_id));
                    }
                }
                if(!empty($recipient_user))
                {
                    $score_card_id =  $scoreCardObj->copyAndActivate($score_card_setup, $result, $recipient_user);
                    $user_list     = $recipient_user;
                }
                $new_score_card = $scoreCardObj->getScoreCardWithSetup($score_card_id);

                $userObj     = new User();
                $users       = $userObj->getUsersList();
                $send_to_str = '';
                if($user_list && $new_score_card)
                {
                    foreach($user_list as $index => $user_id)
                    {
                        $table_body = ''; //$this->getTableBody($new_score_card, $user_id);
                        //SC 47: ADMIN / Score Card / Notification to Userss
                        $email_str = "Dear: ".$users[$user_id]['user']."<br /><br />";

                        $manager = (isset($users[$new_score_card['manager_id']]) ? $users[$new_score_card['manager_id']]['user'] : '');

                        $email_str .= $manager." as activated a scorecard assessment for you completion. The details of the assessment are as follows:.<br /><br />";
                        $email_str .= "Scorecard Name:  ".$new_score_card['setup_reference']." - ".$new_score_card['score_card_name']."<br /><br />";
                        $email_str .= "Scorecard Completion Instruction: ".$new_score_card['setup_description']."<br /><br />";
                        $email_str .= "Start Date: ".$new_score_card['start_date']."<br /><br />";
                        $email_str .= "End Date: ".$new_score_card['end_date']."<br /><br />";

                        $email_str .= "Please ensure that you have completed the assessment before the listed end date. <br /><br />";
                        $email_str .= "Kind Regards,<br />";
                        $email_str .= "The Action iT Team";
                        if(file_exists("../../library/class/assist_email_summary.php"))
                        {
                            require_once "../../library/class/assist_email.php";
                            require_once "../../library/class/assist_email_summary.php";
                        }

                        $emailObj = new ASSIST_EMAIL();
                        $emailObj->setContentType('HTML');
                        //$emailObj->setRecipient("anesuzina@gmail.com");
                        $emailObj->setRecipient($users[$user_id]['tkemail']);					//REQUIRED
                        $emailObj->setSubject("Score Card Assist: New Score Card Triggered");				//REQUIRED
                        $emailObj->setBody($email_str."<br /><br />".$table_body);			//REQUIRED
                        //send the email

                        //$emailObj       = new ASSIST_EMAIL_SUMMARY($users[$owner_id]['tkemail'], '', '', '', '', 'Score Card Assist', 'New Score Card', $headers , $user_actions[$owner_id], "deadline");
                        //$emailObj->setHTMLBody($email_str);
                        if($emailObj->sendEmail())
                        {
                            $send_to_str = " Email notification(s) have been sent.";
                        }
                    }
                }
                //send email here
                $response = array('text' => 'Scorecard successfully activated and can now be accessed on Manage.'.$send_to_str, 'error' => false, 'updated' => true);
            } else {
                $response = array('text' => 'There was an error activating scorecard', 'error' => true, 'updated' => false);
            }
        }
        echo json_encode($response);
        exit();
    }

    function getTableBody($score_card, $user_id)
    {
        $this->parameters['score_card_id'] = $score_card['id'];
        $deliverableObj               = new Deliverable();
        $questionObj                  = new Action();
        $confirmation                 = $deliverableObj->getConfirmDeliverableDetail($score_card, $this->parameters);
        $questions                    = $questionObj->getActionsByScoreCardIdAndOwnerId($score_card['id'], $user_id);

        $actionColumnObj = new ActionColumns();
        $headers         = $actionColumnObj->getHeaders();
        unset($headers['rating_comment']);
        unset($headers['rating']);
        unset($headers['owner']);
        unset($headers['corrective_action']);

        $table_str  = "<table class='noborder'>";
        $table_str .= "<tr>";
        foreach($headers as $index => $header) $table_str .= "<th>".$header."</th>";
        $table_str .= "</tr>";
        $categories = $confirmation['categories'];
        foreach($confirmation['category_deliverable'] as $category_id => $category_headers)
        {
            if(isset($category_headers['deliverables']))
            {
                foreach($category_headers['deliverables'] as $heading_id => $heading)
                {
                    $table_str .= "<tr>";
                      $table_str .= "<td class='noborder' colspan='".count($headers)."'><b>".$categories[$category_id].": ".(isset($heading['deliverable_name']) ? $heading['deliverable_name'] : '')."</b></td>";
                    $table_str .= "</tr>";
                    if(isset($category_headers['sub_deliverable']) && isset($category_headers['sub_deliverable'][$heading_id]) )
                    {
                      foreach($category_headers['sub_deliverable'][$heading_id] as $sub_heading_id => $sub_heading)
                      {
                          $table_str .= "<tr>";
                            $table_str .= "<td class='noborder'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>".(isset($sub_heading['deliverable_name']) ? $sub_heading['deliverable_name'] : '')."</b></td>";
                          $table_str .= "</tr>";
                          foreach($questions as $question_id => $question)
                          {
                              if(($question['deliverable_id'] == $sub_heading_id))
                              {
                                  $table_str .= "<tr>";
                                  foreach($headers as $index => $header)
                                  {
                                      if(isset($question[$index]))
                                      {
                                          if($index == 'rating_scale_type')
                                          {
                                              $table_str .= "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".RatingScale::getRatingScaleType($question[$index])."</td>";
                                          } else {
                                              $table_str .= "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".(empty($question[$index]) ? '-' : $question[$index])."</td>";
                                          }
                                      }
                                  }
                                  $table_str .= "</tr>";
                              }
                          }
                      }
                    } else {
                        foreach($questions as $question_id => $question)
                        {
                            if(($question['deliverable_id'] == $heading_id))
                            {
                                $table_str .= "<tr>";
                                foreach($headers as $index => $header)
                                {
                                    if(isset($question[$index]))
                                    {
                                        if($index == 'rating_scale_type')
                                        {
                                            $table_str .= "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".RatingScale::getRatingScaleType($question[$index])."</td>";
                                        } else {
                                            $table_str .= "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".(empty($question[$index]) ? '-' : $question[$index])."</td>";
                                        }
                                    }
                                }
                                $table_str .= "</tr>";
                            }
                        }
                    }
                }
            }
        }

/*       foreach($confirmation['categories'] as $category_id => $category)
        {
          if(isset($confirmation['category_deliverable'][$category_id]))
          {
              foreach($confirmation['category_deliverable'][$category_id] as $heading_index => $deliverables)
              {
                  foreach($deliverables as $deliverable_id => $deliverable)
                  {
                      $table_str .= "<tr>";
                      $table_str .= "<td class='noborder'><b>".$category.": ".(isset($deliverable['deliverable_name']) ? $deliverable['deliverable_name'] : '')."</b></td>";
                      $table_str .= "</tr>";
                      foreach($questions as $question_id => $question)
                      {
                          if(($question['deliverable_id'] == $deliverable_id))
                          {
                              $table_str .= "<tr>";
                              foreach($headers as $index => $header)
                              {
                                  if(isset($question[$index]))
                                  {
                                      if($index == 'rating_scale_type')
                                      {
                                          $table_str .= "<td>".RatingScale::getRatingScaleType($question[$index])."</td>";
                                      } else {
                                          $table_str .= "<td>".(empty($question[$index]) ? '-' : $question[$index])."</td>";
                                      }
                                  }
                              }
                              $table_str .= "</tr>";
                          }
                      }
                  }
              }
          }
        }*/
        $table_str .= "</table>";
        return $table_str;
    }


    function bind($setup_score_card)
    {
        if(isset($setup_score_card['start_date']))
        {
            $start_date = strtotime($setup_score_card['start_date']);
            if($start_date > 0) $setup_score_card['start_date'] = date('Y-m-d', $start_date);
        }
        if(isset($setup_score_card['end_date']))
        {
            $end_date = strtotime($setup_score_card['end_date']);
            if($end_date > 0) $setup_score_card['end_date'] = date('Y-m-d', $end_date);
        }
        $setup_score_card['use_corrective_action']                  = (isset($setup_score_card['use_corrective_action']) ? 1 : 0);
        $setup_score_card['are_corrective_action_owner_identified'] = (isset($setup_score_card['are_corrective_action_owner_identified']) ? 1 : 0);
        $setup_score_card['use_rating_comment']                     = (isset($setup_score_card['use_rating_comment']) ? 1 : 0);
        return $setup_score_card;
    }

    public function editScoreCardSetting()
    {
        parse_str($this->parameters['score_card'], $parameters);

        $score_card_setup = $parameters['score_card'];

        $setupScoreCardObj = new ScoreCardSetup();

        $setup_data             = $this->bind($score_card_setup);
        $setupScoreCardObj->attach(new ScoreCardSetupAuditLog());
        $result                  = $setupScoreCardObj->update_where($setup_data, array('id' => $setup_data['id']));
        if($result)
        {
            $message = array('text' => 'Scorecard successfully updated', 'error' => false, 'updated' => true);
        } else if($result == 0) {
            $message = array('text' => 'No change made to the scorecard setup', 'error' => false, 'updated' => true);
        } else {
            $message = array('text' => 'There was an error activating scorecard', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

    public function deleteScoreCardSetting()
    {
       $scoreCardObj = new ScoreCard();
       $score_card   = $scoreCardObj->getScoreCard($this->parameters['score_card_id']);

       if($score_card)
       {
           $auditLog = new ScoreCardAuditLog();
           $auditLog->deleteScoreCardSettingsLog($score_card);

           $result = $scoreCardObj->delete_where(array('id' => $score_card['id']));

           $scoreCardSetupObj   = new ScoreCardSetup();
           $scoreCardSetupObj->delete_where(array('id' => $score_card['score_card_setup_id']));

           $deliverableObj      = new Deliverable();
           $deliverables        = $deliverableObj->getDeliverableByScoreCardId($score_card['id']);
           $deliverableObj->delete_where(array('score_card_id' => $score_card['id']));

           $actionObj      = new Action();
           if($deliverables)
           {
               foreach($deliverables as $deliverable)
               {
                  $actionObj->delete_where(array('deliverable_id' => $deliverable['id']));
               }
           }
          if($result)
          {
              echo json_encode(array('text' => 'Score card successfully deleted', 'error' => false));
          } else {
              echo json_encode(array('text' => 'Error occurred deleting the score card', 'error' => false));
          }
         exit();
       }
    }

    public function complete()
    {
        $scoreCardObj = new ScoreCard();
        $score_card   = $scoreCardObj->getScoreCardWithSetup($this->parameters['score_card_id']);

        //$update_data['completed_by']    = $_SESSION['tid'];
        //$update_data['overall_comment'] = $this->parameters['overall_comment'];
        //$update_data['completed_at']    = date('Y-m-d H:i:s');

        $user_assessment['completed_by']     = $_SESSION['tid'];
        $user_assessment['user_id']     = $_SESSION['tid'];
        $user_assessment['comment'] = $this->parameters['overall_comment'];
        $user_assessment['completed_at'] = date('Y-m-d H:i:s');
        $user_assessment['score_card_id'] = $score_card['id'];
        $user_assessment['status'] = 5;
        $user_assessment['created_at'] = date('Y-m-d H:i:s');

        $userAssessmentObj = new UserAssessmentRating();
        $userAssessmentObj->save($user_assessment);

        //$scoreCardObj->attach(new ScoreCardAuditLog());
        //$scoreCardObj->update_where($update_data, array('id' => $score_card['id']));


        $questionObj = new Action();
        $option_sql  = " AND SC.id = '".$this->parameters['score_card_id']."' AND A.owner_id = '".$_SESSION['tid']."' ";
        $questions   = $questionObj->getQuestionWithAssessment($option_sql);
        $res = 0;
        foreach($questions as $question)
        {
            if( ($question['status'] & Action::COMPLETED) != Action::COMPLETED )
            {
                $action_update_data['status'] = $question['status'] + Action::COMPLETED;
                $questionObj->attach(new ActionAuditLog());
                $res += $questionObj->update_where($action_update_data,  array('id' => $question['id']) );
            }
        }
        $response = array();
        if($res > 0)
        {
           $response = array('text' => 'Questions successfully confirmed', 'error' => false);
        } else {
            $response = array('text' => 'Error confirming the questions', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    public function unconfirm()
    {
        $scoreCardObj = new ScoreCard();
        $score_card   = $scoreCardObj->getScoreCardWithSetup($this->parameters['score_card_id']);

        $questionObj = new Action();
        $option_sql  = " AND SC.id = '".$this->parameters['score_card_id']."' AND A.owner_id = '".$this->parameters['owner_id']."' ";
        $questions   = $questionObj->getQuestionWithAssessment($option_sql);
        $res = 0;
        foreach($questions as $question)
        {
            if( ($question['status'] & Action::COMPLETED) == Action::COMPLETED )
            {
                $update_data['status'] = $question['status'] - Action::COMPLETED;
                $questionObj->attach(new ActionAuditLog());
                $res = $questionObj->update_where($update_data,  array('id' => $question['id']) );
            }
        }
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Questions successfully unconfirmed', 'error' => false);
        } else {
            $response = array('text' => 'Error unconfirming the questions', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    public function setupScoreCardForm()
    {
        $scoreCardColumnObj = new ScoreCardColumn();
        $columns            = $scoreCardColumnObj->getHeaders();

        $scoreCardObj = new ScoreCard();
        $score_cards  = $scoreCardObj->getScoreCardsWithSetupByScoreCardIdList($this->parameters['score_card_id']);

        $html_str = "<table width='100%'>";
        $html_str .= "<tr>";
            $html_str .= "<th style='text-align: left;'>".$columns['score_card_ref'].":</th>";
            $html_str .= "<td>".input_tag('setup[name]', null, array('id' => 'score_card_ref'))."</td>";
        $html_str .= "</tr>";
        $html_str .= "<tr>";
            $html_str .= "<th style='text-align: left;'>".$columns['score_card_instruction_to_user'].":</th>";
            $html_str .= "<td>".textarea_tag('setup[description]', null, array('id' => 'setup_description', 'cols' => '50', 'rows' => '10'))."</td>";
        $html_str .= "</tr>";
        $html_str .= "<tr>";
            $html_str .= "<th style='text-align: left;'>".$columns['link_to'].":</th>";
            $html_str .= "<td>".select_tag('setup[link_to]', options_for_select($score_cards, null, array('include_custom' => 'Do Not Link' )), array('id' => 'link_to'))."</td>";
        $html_str .= "</tr>";
        $html_str .= "<tr>";
            $html_str .= "<th style='text-align: left;'>".$columns['score_card_frequency'].":</th>";
            $html_str .= "<td>";
                $html_str .= "<table>";
                    $html_str .= "<tr>";
                        $html_str .= "<th class='th2'>Start Date</th>";
                        $html_str .= "<td>".input_tag('setup[start_date]', null, array('id' => 'start_date', 'class' => 'datepicker', 'readonly' => 'readonly'))."</td>";
                    $html_str .= "</tr>";
                    $html_str .= "<tr>";
                        $html_str .= "<th class='th2'>End Date</th>";
                        $html_str .= "<td>".input_tag('setup[end_date]', null, array('id' => 'end_date', 'class' => 'datepicker', 'readonly' => 'readonly'))."</td>";
                    $html_str .= "</tr>";
                $html_str .= "</table>";
            $html_str .= "</td>";
        $html_str .= "</tr>";
        $allocations = array('all' => 'All Recipients Update', 'allocated_criteria' => 'Recipients Update Allocated Criteria');
        $html_str .= "<tr>";
            $html_str .= "<th style='text-align: left;'>".$columns['allocate_to'].":</th>";
            $html_str .= "<td>".select_tag('setup[allocate_to]', options_for_select($allocations, null), array('id' => 'score_card_allocate_to'))."</td>";
        $html_str .= "</tr>";
        $user_list = array();
        $userObj   = new User();
        $user_list = $userObj->getList();

        $html_str .= "<tr style='display:none' class='completion-rule all-users-list'>";
            $html_str .= "<th style='text-align: left;'>Users:</th>";
            $html_str .= "<td>".select_tag('setup[recipient_user]', options_for_select($user_list, null), array('id' => 'setup_recipient_user', 'multiple' => true))."</td>";
        $html_str .= "</tr>";

        $assign_to = array('directorate' => 'Directorate/Sub-Directorate', 'action_owner' => 'Action Owner');
        $html_str .= "<tr style='display:none' class='completion-rule allocate'>";
            $html_str .= "<th style='text-align: left;'>Assign Question To:</th>";
            $html_str .= "<td>".select_tag('setup[assign_question_to]', options_for_select($assign_to, null), array('id' => 'setup_assign_question_to'))."</td>";
        $html_str .= "</tr>";

        $html_str .= "<tr>";
            $html_str .= "<th style='text-align: left;'>".$columns['use_rating_comment'].":</th>";
            $html_str .= "<td>".checkbox_tag('setup[use_rating_comment]', null, array('id' => 'use_rating_comment'))."</td>";
        $html_str .= "</tr>";

        $html_str .= "<tr>";
            $html_str .= "<th style='text-align: left;'>Are corrective Actions required:</th>";
            $html_str .= "<td>".checkbox_tag('setup[use_corrective_action]', null, array('id' => 'use_corrective_action'))."</td>";
        $html_str .= "</tr>";

        $html_str .= "<tr>";
            $html_str .= "<th style='text-align: left;'>".$columns['are_corrective_action_owner_identified'].":</th>";
            $html_str .= "<td>".checkbox_tag('setup[are_corrective_action_owner_identified]', null, array('id' => 'are_corrective_action_owner_identified'))."</td>";
        $html_str .= "</tr>";

        $html_str .= "<tr>";
            $html_str .= "<th style='text-align: left;'>Rating table to be used to display overall rating:</th>";
            $html_str .= "<td>".select_tag('setup[rating_type]', options_for_select(RatingScale::getRatingScaleTypesList(), null), array('id' => 'rating_type'))."</td>";
        $html_str .= "</tr>";

        $html_str .= "</table>";
        echo $html_str;
        exit();
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ScoreCardSetupController();
    return $class->execute($_REQUEST['action']);
}

?>
