<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ScoreCardTypeController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $contractObj = new ScoreCardType();
        $response    = $contractObj->getScoreCardTypes();
        echo json_encode($response);
        exit();
    }

    function save($options = array())
    {
        $contract_typeObj             = new ScoreCardType();
        $contract_type                = $this->parameters;
        $contract_type['insert_user'] = $_SESSION['tid'];
        $contract_type['created_at']  = date('Y-m-d H:i:s');
        $contract_type['status']      = 1;
        $contract_typeObj->attach(new SetupAuditLog());
        $result                       = $contract_typeObj->save($contract_type, 'Score card type');
        if($result)
        {
            $message = array('text' => 'Score card type successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving contract type', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $contract_typeObj             = new ScoreCardType();
        $contract_type                = $this->parameters;
        $contract_type['insert_user'] = $_SESSION['tid'];
        $contract_type['created_at']  = date('Y-m-d H:i:s');
        $contract_typeObj->attach(new SetupAuditLog());
        $result                       = $contract_typeObj->update_where($contract_type, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Score card type successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the contract type', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error saving contract type', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ScoreCardTypeController();
    return $class->execute($_REQUEST['action']);
}

?>