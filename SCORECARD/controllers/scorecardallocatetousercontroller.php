<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ScoreCardAllocateToUserController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getUserQuestionForm()
    {
        $parameters = array();
        if(isset($this->parameters['users']))
        {
            parse_str($this->parameters['users'], $parameters);
        }



        $actionObj = new Action();
        $score_card_questions = $actionObj->getActionsListByScoreCardId($this->parameters['score_card_id']);

        $userObj = new User();
        $users   = $userObj->getList();
        $pre_table_str = '';
        $after_table = array();
        if(isset($parameters['users']))
        {
            $parameters['users'][] = $this->parameters['user_id'];
        } else {
            if(isset($this->parameters['user_id'])) $parameters['users'][] = $this->parameters['user_id'];
        }
        if(isset($parameters['questions']))
        {
            $parameters['questions'][] = $this->parameters['question_id'];
        } else {
            if(isset($this->parameters['question_id']))  $parameters['questions'][] = $this->parameters['question_id'];
        }
        if(isset($parameters['questions']) && !empty($parameters['questions']))
        {
            foreach($parameters['questions'] as $index => $question)
            {
                $after_table[$index]['question'] = $score_card_questions[$question];
                unset($score_card_questions[$question]);
                $pre_table_str .= "<input type='hidden' name='questions[]' id='questions_".$question."' value='".$question."' />";
            }
        }
        if(isset($parameters['users']) && !empty($parameters['users']))
        {
            foreach($parameters['users'] as $index => $user)
            {

                $after_table[$index]['user'] = $users[$user];
               unset($users[$user]);

               $pre_table_str .= "<input type='hidden' name='users[]' id='users_".$user."' value='".$user."' />";
            }
        }
        $table_str  = "<form method='post' id='allocate-user-to-question'>";
        $table_str  .= "<table>";
           $table_str .= "<tr>";
             $table_str .= "<th class='th2'>Question</th>";
             $table_str .= "<th class='th2'>User</th>";
             $table_str .= "<th class='th2'>Allocate Question to User</th>";
           $table_str .= "</tr>";
           $table_str .= "<tr>";
             $table_str .= "<td>";
               $table_str .= select_tag('score_card_setup[question_id]', $score_card_questions, array('id' => 'question_id'));
             $table_str .= "</td>";
             $table_str .= "<td>";
               $table_str .= select_tag('score_card_setup[user_id]', $users, array('id' => 'user_id'));
             $table_str .= "</td>";
             $table_str .= "<td>";
               $table_str .= "<input type='submit' name='allocate_user' id='allocate' value='Allocate' />";
             $table_str .= "</td>";
           $table_str .= "</tr>";
           if(!empty($after_table))
           {
               foreach($after_table as $table_td_str)
               {
                  $table_str .= "<tr>";
                   $table_str .= "<td>".$table_td_str['question']."</td>";
                   $table_str .= "<td>".$table_td_str['user']."</td>";
                   $table_str .= "<td></td>";
                   //$table_str .= "<td><a href='#'>Remove</a></td>";
                  $table_str .= "</tr>";
               }
           }
           $table_str .= $pre_table_str;
        $table_str .= "</table>";
        $table_str .= "</form>";
        echo $table_str;
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ScoreCardAllocateToUserController ();
    return $class->execute($_REQUEST['action']);
}

?>