<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ActionAssuranceController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }


    public function getActionAssurances($options = array())
    {
        $actionAssuranceObj = new ActionAssurance();
        $option_sql           = $actionAssuranceObj->prepareSql($this->parameters);

        $action_assurances    = $actionAssuranceObj->getActionAssurances($option_sql);

        $_option_sql           = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $total_assurance      = $actionAssuranceObj->getTotalActionAssurance($_option_sql);

        $action_assurances  = $actionAssuranceObj->prepareActionAssurance($action_assurances, $total_assurance, $this->parameters);
        echo json_encode($action_assurances);
        exit();
    }


    function saveActionAssurance($options = array())
    {
        parse_str($this->parameters['assurance_data'], $action_assurance);
        $assurance_documents = array();
        if(isset($action_assurance['action_assurance_document']) && !empty($action_assurance['action_assurance_document']))
        {
           $assurance_documents = $action_assurance['action_assurance_document'];
           unset($action_assurance['action_assurance_document']);
        }
        $actionAssuranceObj = new ActionAssurance();
        $action_assurance = $this->bind($action_assurance);
        $response            = array();
        if($actionAssuranceObj->isValid($action_assurance))
        {
            $action_assurance['insertuser'] = $_SESSION['tid'];
            $action_assurance['action_id']  = $this->parameters['action_id'];
            $action_assurance['created_at']  = date('Y-m-d H:i:s');
            //$contract_payment['status']      = 1;
            $result                          = $actionAssuranceObj->save($action_assurance);
            if($result)
            {
                if(isset($assurance_documents) && !empty($assurance_documents))
                {
                    $actionAssuranceDocumentObj = new ActionAssuranceDocument();
                    foreach($assurance_documents as $filename => $file_description)
                    {
                        if(Attachment::isFile($filename, 'action'))
                        {
                            $docs = array('filename' => $filename, 'description' => $file_description, 'assurance_id' => $result);
                            $actionAssuranceDocumentObj->save($docs);
                        }
                    }
                    unset($_SESSION['uploads']);
                }
                $response = array('text' => 'Action assurance successfully saved', 'error' => false, 'id' => $result);
            } else {
                $response = array('text' => 'There was an error saving action assurance', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }

    function updateActionAssurance($options = array())
    {
        parse_str($this->parameters['assurance_data'], $action_assurance);
        $assurance_documents = array();
        if(isset($action_assurance['action_assurance_document']) && !empty($action_assurance['action_assurance_document']))
        {
           $assurance_documents  = $action_assurance['action_assurance_document'];
           unset($action_assurance['action_assurance_document']);
        }
        if(isset($action_assurance['deleted_assurance_document']) && !empty($action_assurance['deleted_assurance_document']))
        {
           unset($action_assurance['deleted_assurance_document']);
        }

        $actionAssuranceObj = new ActionAssurance();
        $action_assurance   = $this->bind($action_assurance);
        $response               = array();
        if($actionAssuranceObj->isValid($action_assurance))
        {
            $actionAssuranceObj->attach(new ActionAssuranceAuditLog());
            $result   = $actionAssuranceObj->update_where($action_assurance, array('id' => $this->parameters['id']));
            if(isset($assurance_documents) && !empty($assurance_documents))
            {
                $actionAssuranceDocumentObj = new ActionAssuranceDocument();
                foreach($assurance_documents as $filename => $file_description)
                {
                    if(Attachment::isFile($filename, 'action'))
                    {
                        $docs    = array('filename' => $filename, 'description' => $file_description, 'assurance_id' => $this->parameters['id']);
                        $result += $actionAssuranceDocumentObj->save($docs);
                    }
                }
                unset($_SESSION['uploads']);
            }
            if($result > 0)
            {
                $response = array('text' => 'Action assurance successfully updated', 'error' => false, 'id' => $result, 'updated' => true);
            } elseif($result == 0) {
                $response = array('text' => 'No change was made to the action assurance', 'error' => false, 'updated' => false);
            } else {
                $response = array('text' => 'There was an error updating assurance', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }


    function bind($action_assurance)
    {
        if(isset($action_assurance['date_tested']))
        {
            $date_tested = strtotime($action_assurance['date_tested']);
            if($date_tested > 0)
            {
                $action_assurance['date_tested'] = date('Y-m-d', $date_tested);
            }
        }
        return $action_assurance;
    }

    public function deleteActionAssurance()
    {
        $actionAssuranceObj = new ActionAssurance();
        $res                = $actionAssuranceObj->update_where(array('status' => 2), array('id' => $this->parameters['id']));
        $response = array();
        if($res)
        {
            $response = array('text' => 'Action assurance successfully deleted', 'error' => false);
        } else {
            $response = array('text' => 'Error deleting the action assurance', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    public function removeActionAttachment()
    {
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('action_assurance_attachment', 'action_assurance_attachment');
        $response = $attObj->removeFile($file, 'new_action_assurance_document');
        echo json_encode($response);
    }

    public function saveAttachment()
    {
        $attObj    = new Attachment('action_assurance_attachment', 'action_assurance_attachment');
        $results   = $attObj->upload('new_action_assurance_document');
        echo json_encode($results);
    }


    public function deleteAttachment()
    {
        $actionAssuranceDocumentObj = new ActionAssuranceDocument();
        $res      = $actionAssuranceDocumentObj->update_where( array('deleted_at' => date('Y-m-d H:i:s') ), array('id' => $this->parameters['document_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Attachment successfully deleted', 'error' => false);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the attachment', 'error' => false);
        } else {
            $response = array('text' => 'An error occurred deleting the attachment', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    public function download()
    {
        header("Content-type:".$_GET['content']);
        header("Content-disposition: attachment; filename=".$_GET['new']);
        readfile("../../files/".$_GET['company']."/".$_SESSION['modref']."/".$_GET['folder']."/".$_GET['file']);
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ActionAssuranceController();
    return $class->execute($_REQUEST['action']);
}

?>