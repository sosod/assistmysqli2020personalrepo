<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class NotificationRuleController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $notification_ruleObj = new NotificationRule();
        $response    = $notification_ruleObj->getAll();
        echo json_encode($response);
        exit();
    }

    function save($options = array())
    {
        $notification_ruleObj             = new NotificationRule();
        $notification_rule                = $this->parameters;
        if(isset($notification_rule['date']) && !empty($notification_rule['date']))
        {
            $notification_rule['date'] = date('Y-m-d', strtotime($notification_rule['date']));
        }
        $notification_rule['insert_user'] = $_SESSION['tid'];
        $notification_rule['created_at']  = date('Y-m-d H:i:s');
        $notification_rule['status']      = 1;
        $result                       = $notification_ruleObj->save($notification_rule);
        if($result)
        {
            $message = array('text' => 'Notification notification rule successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving notification rule', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $notification_ruleObj                      = new NotificationRule();
        $notification_rule                = $this->parameters;
        if(isset($notification_rule['date']) && !empty($notification_rule['date']))
        {
            $notification_rule['date'] = date('Y-m-d', strtotime($notification_rule['date']));
        }
        $notification_rule['insert_user'] = $_SESSION['tid'];
        $notification_rule['created_at']  = date('Y-m-d H:i:s');
        $notification_ruleObj->attach(new SetupAuditLog());
        $result                       = $notification_ruleObj->update_where($notification_rule, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Notification rule successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the notification rule', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error saving notification rule', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new NotificationRuleController();
    return $class->execute($_REQUEST['action']);
}

?>