<?php
require_once '../contract_autoloader.php';
require_once '../contract_helper.php';
class ContractAssuranceController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }


    public function getContractAssurances($options = array())
    {
        $contractAssuranceObj = new ContractAssurance();
        $option_sql           = $contractAssuranceObj->prepareSql($this->parameters);

        $contract_assurances    = $contractAssuranceObj->getContractAssurances($option_sql);

        $_option_sql           = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $total_assurance      = $contractAssuranceObj->getTotalContractAssurance($_option_sql);

        $contract_assurances  = $contractAssuranceObj->prepareContractAssurance($contract_assurances, $total_assurance, $this->parameters);
        echo json_encode($contract_assurances);
        exit();
    }


    function saveContractAssurance($options = array())
    {
        parse_str($this->parameters['assurance_data'], $contract_assurance);
        $assurance_documents = array();
        if(isset($contract_assurance['contract_assurance_docs']) && !empty($contract_assurance['contract_assurance_docs']))
        {
           $assurance_documents = $contract_assurance['contract_assurance_docs'];
           unset($contract_assurance['contract_assurance_docs']);
        }
        $contractAssuranceObj = new ContractAssurance();
        $contract_assurance = $this->bind($contract_assurance);
        $response            = array();
        if($contractAssuranceObj->isValid($contract_assurance))
        {
            $contract_assurance['contract_id'] = $this->parameters['contract_id'];
            $contract_assurance['insert_user'] = $_SESSION['tid'];
            $contract_assurance['created_at']  = date('Y-m-d H:i:s');
            //$contract_payment['status']      = 1;
            $result                          = $contractAssuranceObj->save($contract_assurance);
            if($result)
            {
                if(isset($assurance_documents) && !empty($assurance_documents))
                {
                    $contractAssuranceDocumentObj = new ContractAssuranceDocument();
                    foreach($assurance_documents as $filename => $file_description)
                    {
                        if(Attachment::isFile($filename))
                        {
                            $docs = array('filename' => $filename, 'description' => $file_description, 'assurance_id' => $result);
                            $contractAssuranceDocumentObj->save($docs);
                        }
                    }
                    unset($_SESSION['uploads']);
                }
                $response = array('text' => 'Contract assurance successfully saved', 'error' => false, 'id' => $result);
            } else {
                $response = array('text' => 'There was an error saving contract assurance', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }

    function updateContractAssurance($options = array())
    {
        parse_str($this->parameters['assurance_data'], $contract_assurance);
        $assurance_documents = array();
        if(isset($contract_assurance['contract_assurance_docs']) && !empty($contract_assurance['contract_assurance_docs']))
        {
           $assurance_documents  = $contract_assurance['contract_assurance_docs'];
           unset($contract_assurance['contract_assurance_docs']);
        }
        if(isset($contract_assurance['deleted_assurance_document']) && !empty($contract_assurance['deleted_assurance_document']))
        {
           unset($contract_assurance['deleted_assurance_document']);
        }

        $contractPaymentObj = new ContractAssurance();
        $contract_assurance   = $this->bind($contract_assurance);
        $response               = array();
        if($contractPaymentObj->isValid($contract_assurance))
        {
            $contractPaymentObj->attach(new ContractAssuranceAuditLog());
            $result   = $contractPaymentObj->update_where($contract_assurance, array('id' => $this->parameters['id']));
            if(isset($assurance_documents) && !empty($assurance_documents))
            {
                $contractAssuranceDocumentObj = new ContractAssuranceDocument();
                foreach($assurance_documents as $filename => $file_description)
                {
                    if(Attachment::isFile($filename))
                    {
                        $docs    = array('filename' => $filename, 'description' => $file_description, 'assurance_id' => $this->parameters['id']);
                        $result += $contractAssuranceDocumentObj->save($docs);
                    }
                }
                unset($_SESSION['uploads']);
            }
            if($result > 0)
            {
                $response = array('text' => 'Contract assurance successfully updated', 'error' => false, 'id' => $result, 'updated' => true);
            } elseif($result == 0) {
                $response = array('text' => 'No change was made to the contract assurance', 'error' => false, 'updated' => false);
            } else {
                $response = array('text' => 'There was an error updating assurance', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }


    function bind($contract_assurance)
    {
        if(isset($contract_assurance['date_tested']))
        {
            $date_tested = strtotime($contract_assurance['date_tested']);
            if($date_tested > 0)
            {
                $contract_assurance['date_tested'] = date('Y-m-d', $date_tested);
            }
        }
        return $contract_assurance;
    }

    public function deleteContractAssurance()
    {
        $contractAssuranceObj = new ContractAssurance();
        $res                = $contractAssuranceObj->update_where(array('status' => 2), array('id' => $this->parameters['id']));
        $response = array();
        if($res)
        {
            $response = array('text' => 'Contract assurnace successfully deleted', 'error' => false);
        } else {
            $response = array('text' => 'Error deleting the contract assurnace', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    public function removeContractPaymentAttachment()
    {
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('contract_assurance_attachment', 'contract_assurance_attachment');
        $response = $attObj->removeFile($file, 'new_contract_assurance_document');
        echo json_encode($response);
    }

    public function saveAttachment()
    {
        $attObj    = new Attachment('contract_assurance_attachment', 'contract_assurance_attachment');
        $results   = $attObj->upload('new_contract_assurance_document');
        echo json_encode($results);
    }


    public function deleteAttachment()
    {
        $contractAssuranceDocumentObj = new ContractAssuranceDocument();
        $res      = $contractAssuranceDocumentObj->update_where( array('deleted_at' => date('Y-m-d H:i:s') ), array('id' => $this->parameters['document_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Attachment successfully deleted', 'error' => false);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the attachment', 'error' => false);
        } else {
            $response = array('text' => 'An error occurred deleting the attachment', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    public function download()
    {
        header("Content-type:".$_GET['content']);
        header("Content-disposition: attachment; filename=".$_GET['new']);
        readfile("../../files/".$_GET['company']."/".$_SESSION['modref']."/".$_GET['folder']."/".$_GET['file']);
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ContractAssuranceController();
    return $class->execute($_REQUEST['action']);
}

?>