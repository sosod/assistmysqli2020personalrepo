<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class DeliverableController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $deliverableObj = new Deliverable();
        $option_sql     = $deliverableObj->prepareSql($this->parameters);
        $response       = $deliverableObj->getList(false, false, $option_sql, $this->parameters);
        echo json_encode($response);
        exit();
    }

    public function getMainDeliverableList()
    {
        $deliverableObj = new Deliverable();
        $response       = $deliverableObj->getMainDeliverableWithSubList($this->parameters['score_card_id']);
        echo json_encode($response);
        exit();
    }

    public function getDeliverables($options = array())
    {
        $deliverableObj    = new Deliverable();
        $option_sql        = $deliverableObj->prepareSql($this->parameters);
        $deliverables      = $deliverableObj->getDeliverables($option_sql);
        $_option_sql       = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $total_deliverable = $deliverableObj->getTotalDeliverables($_option_sql);
        $deliverables      = $deliverableObj->prepareDeliverable($deliverables, $total_deliverable, $this->parameters);
        echo json_encode($deliverables);
        exit();
    }


    function saveDeliverable($options = array())
    {
        $deliverableObj  = new Deliverable();
        $deliverable     = $this->bind($this->parameters, array(), $deliverableObj);
        $score_cardObj     = new ScoreCard();
        $score_card        = $score_cardObj->getScoreCardDetail($deliverable['score_card_id']);
        $message         = array();
        if($deliverableObj->isValid($deliverable, $score_card))
        {
            $deliverable['insert_user'] = $_SESSION['tid'];
            $deliverable['created_at']  = date('Y-m-d H:i:s');
            $deliverable['status']      = 1;
            $result                     = $deliverableObj->save($deliverable);
            if($result)
            {
                //set_flash('success', 'deliverable', 'Deliverable successfully saved');
               $message = array('text' => 'Heading successfully saved', 'error' => false);
            } else {
               //set_flash('error', 'deliverable', 'There was an error saving deliverable');
               $message = array('text' => 'There was an error saving heading', 'error' => true);
            }
        } else {
            $error   = get_flash('deliverable');
            $message = array('text' => $error, 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    function editDeliverable()
    {
        $deliverableObj  = new Deliverable();
        $deliverable     = $this->bind($this->parameters);
        $scoreCardObj     = new ScoreCard();
        $score_card        = $scoreCardObj->getScoreCardDetail($deliverable['score_card_id']);
        $score_card_update = array();
        $message         = array();
        if($deliverableObj->isValid($deliverable, $score_card))
        {
            $deliverableObj->attach(new DeliverableAuditLog());
            $result                       = $deliverableObj->update_where($deliverable, array('id' => $this->parameters['id']));
            if($result)
            {
                $scoreCardObj->updateActivityOnManage($score_card);
                //set_flash('success', 'deliverable', 'Deliverable successfully saved');
                $message = array('text' => 'Heading successfully updated', 'error' => false, 'updated' => true);
            } elseif($result == 0){
                $message = array('text' => 'No change was made to the heading', 'error' => false, 'updated' => false);
            } else {
                //set_flash('error', 'deliverable', 'There was an error saving deliverable');
                $message = array('text' => 'There was an error updating the heading', 'error' => true);
            }
        } else {
            $error   = get_flash('deliverable');
            $message = array('text' => $error, 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function updateDeliverable()
    {
        $deliverableObj   = new Deliverable();
        parse_str($this->parameters['deliverable_data'], $posted_data);

        $deliverable           = $deliverableObj->getDeliverable($posted_data['deliverable_id']);

        $deliverable_documents = array();
        if(isset($posted_data['deliverable_document']))  $deliverable_documents = $posted_data['deliverable_document'];

        $deliverable_data = $posted_data['deliverable'];
        $deliverable_data = $this->bind($deliverable_data, $posted_data);

        $deliverableObj->attach(new DeliverableAuditLog());
        $result       = $deliverableObj->update_where($deliverable_data, array('id' => $posted_data['deliverable_id']));
        if(isset($posted_data['parent_id']) && !empty($posted_data['parent_id']))
        {
           $parent_data['status_id'] = $deliverable_data['status_id'];
           $result = $deliverableObj->update_where($deliverable_data, array('id' => $posted_data['parent_id']));
        }
        if(!empty($deliverable_documents))
        {
            $deliverableDocumentObj = new DeliverableDocument();
            foreach($deliverable_documents as $filename => $document)
            {
              $doc = array('filename' => $filename, 'deliverable_id' => $posted_data['deliverable_id'], 'description' => $document);
              $result += $deliverableDocumentObj->save($doc);
            }
            unset($_SESSION['uploads']);
        }
        if($result)
        {
            if(isset($deliverable_data['status_id']) && $deliverable_data['status_id'] == 3)
            {
                //send mail to ScoreCard Manager
            }
            $score_cardObj = new ScoreCard();
            $score_card    = $score_cardObj->getScoreCardDetail($deliverable['score_card_id']);
            $score_cardObj->updateActivityOnManage($score_card);

            $message = array('text' => 'Heading successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the heading', 'error' => false, 'updated' => false);
        }else {
            $message = array('text' => 'There was an error saving heading', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

    function bind($deliverable, $posted_deliverable = array(), $deliverableObj = null)
    {
        if(isset($deliverable['deadline_date']))
        {
            $deadline_date = strtotime($deliverable['deadline_date']);
            if($deadline_date > 0)
            {
                $deliverable['deadline_date'] = date('Y-m-d', $deadline_date);
            }
        }

        if(isset($deliverable['remind_on']))
        {
            $remind_on = strtotime($deliverable['remind_on']);
            if($remind_on > 0)
            {
                $deliverable['remind_on'] = date('Y-m-d', $remind_on);
            }
        }
        if(isset($deliverable['status_id']) && $deliverable['status_id'] == 5)
        {
            $deliverable['status'] = 1 + Deliverable::AWAITING;
        }
/*        if(isset($posted_deliverable['approval']) && $posted_deliverable['approval'] == 1)
        {
            $deliverable['status'] = 1 + Deliverable::AWAITING;
        }*/
        if((isset($deliverable['type_id']) && $deliverable['type_id'] == 1) && is_object($deliverableObj))
        {
            $parent_deliverable  = $deliverableObj->getDeliverable($deliverable['parent_id']);
            if($parent_deliverable['category_id'] != $deliverable['category_id'])
            {
               $deliverable['category_id'] = $parent_deliverable['category_id'];
            }
        }

        unset($deliverable['deliverable_id']);

       return $deliverable;
    }

    public function deleteDeliverable()
    {
        $deliverableObj = new Deliverable();
        $deliverableObj->attach(new DeliverableAuditLog());
        $res = $deliverableObj->update_where(array('status' => Deliverable::DELETED), array('id' => $this->parameters['deliverable_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Heading successfully deleted', 'error' => false, 'updated' => true);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the heading', 'error' => false, 'updated' => false);
        } else {
            $response = array('text' => 'Error occurred deleting a heading', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    function getDeliverableHtml()
    {
       $deliverableObj   = new Deliverable();
       $deliverable      = $deliverableObj->getDeliverableDetail($this->parameters['id']);

       $columnsObj          = new DeliverableColumns();
       $deliverable_columns = $columnsObj->getHeaders();

       $deliverableTable = "<table width='100%'>";
       if(!empty($deliverable))
       {
            $deliverableTable .= "<tr>";
              $deliverableTable .= "<td colspan='2'><h4>Heading Details</h4></td>";
            $deliverableTable .= "</tr>";
            $deliverableTable .= "<tr>";
              $deliverableTable .= "<th>".$deliverable_columns['deliverable_id']."</th>";
              $deliverableTable .= "<td>".$deliverable['id']."</td>";
            $deliverableTable .= "</tr>";
            $deliverableTable .= "<tr>";
              $deliverableTable .= "<th>".$deliverable_columns['deliverable_name'].":</th>";
              $deliverableTable .= "<td>".$deliverable['name']."</td>";
            $deliverableTable .= "</tr>";
            $deliverableTable .= "<tr>";
              $deliverableTable .= "<th>".$deliverable_columns['description'].":</th>";
              $deliverableTable .= "<td>".$deliverable['description']."</td>";
            $deliverableTable .= "</tr>";
            $deliverableTable .= "<tr>";
              $deliverableTable .= "<th>".$deliverable_columns['deliverable_category'].":</th>";
              $deliverableTable .= "<td>".$deliverable['deliverable_category']."</td>";
            $deliverableTable .= "</tr>";
            $deliverableTable .= "<tr>";
              $deliverableTable .= "<th>".$deliverable_columns['deliverable_owner'].":</th>";
              $deliverableTable .= "<td>".$deliverable['deliverable_owner']."</td>";
            $deliverableTable .= "</tr>";
            $deliverableTable .= "<tr>";
              $deliverableTable .= "<th>".$deliverable_columns['deliverable_deadline_date'].":</th>";
              $deliverableTable .= "<td>".$deliverable['deadline_date']."</td>";
            $deliverableTable .= "</tr>";
            $deliverableTable .= "<tr>";
              $deliverableTable .= "<th>".$deliverable_columns['main_deliverable'].":</th>";
              $deliverableTable .= "<td>".$deliverable['parent_deliverable']."</td>";
            $deliverableTable .= "</tr>";
       }
       $deliverableTable.= "<table>";
       echo $deliverableTable;
       exit();
    }

    public function getApprovalDeliverables()
    {
        $actionObj  = new Deliverable();

        $this->parameters['type'] = 'awaiting';
        $awaiting_sql = $actionObj->prepareSql($this->parameters);

        $this->parameters['type'] = 'approved';
        $approved_sql = $actionObj->prepareSql($this->parameters);


        $deliverables          = $actionObj->getDeliverables($awaiting_sql);
        $awaiting_deliverables = $actionObj->prepareDeliverable($deliverables, 0, $this->parameters);

        $deliverables          = $actionObj->getDeliverables($approved_sql);
        $approved_deliverables = $actionObj->prepareDeliverable($deliverables, 0, $this->parameters);



        $columnsObj             = new DeliverableColumns();
        //$_option_sql    = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $deliverables['awaiting']  = $awaiting_deliverables;
        $deliverables['approved'] = $approved_deliverables;
        $deliverables['columns']   = $columnsObj->getHeaders();
        echo json_encode($deliverables);
        exit();
    }

    public function getDeliverableUpdateLog()
    {

        $actionObj      = new DeliverableAuditLog($this->parameters['id']);
        $audit_log_html = $actionObj->displayLog($this->parameters['id'], true);
        echo $audit_log_html;
    }

    public function approveDeliverable()
    {
        $deliverableObj = new Deliverable();
        $deliverable    = $deliverableObj->getDeliverableDetail($this->parameters['deliverable_id']);
        $message     = array();
        if($deliverable)
        {
            if(($deliverable['status'] && Deliverable::AWAITING) == Deliverable::AWAITING)
            {
                $deliverable['status'] = $deliverable['status'] - Deliverable::AWAITING;
            }
            $update_data['status']   = $deliverable['status'] + Deliverable::APPROVED;
            $result                  = $deliverableObj->update_where($update_data, array('id' => $this->parameters['deliverable_id']));
            if($result)
            {
                $message = array('text' => 'Heading successfully approved', 'error' => false, 'updated' => true);
            } elseif($result == 0) {
                $message = array('text' => 'No change was made to the heading', 'error' => false, 'updated' => true);
            }else {
                $message = array('text' => 'There was an error approving heading', 'error' => true, 'updated' => false);
            }
        }
        echo json_encode($message);
        exit();
    }


    public function undoApproveDeliverable()
    {
        $actionObj = new Deliverable();
        $action  = $actionObj->getDeliverableDetail($this->parameters['deliverable_id']);
        $message     = array();
        if($action)
        {
            if(($action['status'] && Deliverable::APPROVED) == Deliverable::APPROVED)
            {
                $action['status'] = $action['status'] - Deliverable::APPROVED;
            }
            $update_data['status']   = $action['status'] + Deliverable::AWAITING;
            $result                  = $actionObj->update_where($update_data, array('id' => $this->parameters['deliverable_id']));
            if($result)
            {
                $message = array('text' => 'Heading successfully disapproved', 'error' => false, 'updated' => true);
            } elseif($result == 0) {
                $message = array('text' => 'No change was made to the heading', 'error' => false, 'updated' => true);
            }else {
                $message = array('text' => 'There was an error disapproving heading', 'error' => true, 'updated' => false);
            }
        }
        echo json_encode($message);
        exit();
    }


    public function removeDeliverableAttachment()
    {
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('deliverable_attachment', 'deliverable_attachment');
        $response = $attObj->removeFile($file, 'deliverable_docs');
        echo json_encode($response);
    }

    public function saveAttachment()
    {
        $attObj    = new Attachment('deliverable_attachment', 'deliverable_attachment');
        $results   = $attObj->upload('deliverable_docs');
        echo json_encode($results);
    }

    public function deleteDeliverableAttachment()
    {
        $deliverableDocumentObj = new DeliverableDocument();
        $res      = $deliverableDocumentObj->update_where( array('deleted_at' => date('Y-m-d H:i:s') ), array('id' => $this->parameters['doc_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Attachment successfully deleted', 'error' => false);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the attachment', 'error' => false);
        } else {
            $response = array('text' => 'An error occurred deleting the attachment', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }


}

if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new DeliverableController();
    return $class->execute($_REQUEST['action']);
}
exit();
?>