<?php
require_once '../contract_autoloader.php';
require_once '../contract_helper.php';
class TemplateSettingController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $templateSettingObj = new TemplateSetting();
        $option_sql  = $templateSettingObj->prepareSql($this->parameters);
        $response    = $templateSettingObj->getList(null, $option_sql);
        echo json_encode($response);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new TemplateSettingController();
    return $class->execute($_REQUEST['action']);
}

?>