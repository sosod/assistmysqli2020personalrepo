<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ActionAuthorizationController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }


    public function getActionAuthorizations($options = array())
    {
        $actionAuthorizationObj = new ActionAuthorization();
        $option_sql           = $actionAuthorizationObj->prepareSql($this->parameters);

        $action_authorization    = $actionAuthorizationObj->getActionAuthorizations($option_sql);

        $_option_sql           = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $total_authorization      = $actionAuthorizationObj->getTotalActionAuthorization($_option_sql);

        $action_authorization  = $actionAuthorizationObj->prepareActionAuthorization($action_authorization, $total_authorization, $this->parameters);
        echo json_encode($action_authorization);
        exit();
    }


    function saveActionAuthorization($options = array())
    {
        parse_str($this->parameters['authorization_data'], $action_authorization);
        $authorization_documents = array();
        if(isset($action_authorization['action_authorization_document']) && !empty($action_authorization['action_authorization_document']))
        {
            $authorization_documents = $action_authorization['action_authorization_document'];
            unset($action_authorization['action_authorization_document']);
        }
        $actionAuthorizationObj = new ActionAuthorization();
        $action_authorization = $this->bind($action_authorization);
        $response            = array();

        if($actionAuthorizationObj->isValid($action_authorization))
        {
            $action_authorization['action_id'] = $this->parameters['action_id'];
            $action_authorization['insert_user']    = $_SESSION['tid'];
            $action_authorization['created_at']    = date('Y-m-d H:i:s');
            //$contract_payment['status']      = 1;
            $result                          = $actionAuthorizationObj->save($action_authorization);
            if($result)
            {
                if(isset($authorization_documents) && !empty($authorization_documents))
                {
                    $actionAuthorizationDocumentObj = new ActionAuthorizationDocument();
                    foreach($authorization_documents as $filename => $file_description)
                    {
                        if(Attachment::isFile($filename, 'action'))
                        {
                            $docs = array('filename' => $filename, 'description' => $file_description, 'authorization_id' => $result);
                            $actionAuthorizationDocumentObj->save($docs);
                        }
                    }
                    unset($_SESSION['uploads']);
                }
                $response = array('text' => 'Action authorization successfully saved', 'error' => false, 'id' => $result);
            } else {
                $response = array('text' => 'There was an error saving action authorization', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }

    function updateActionAuthorization($options = array())
    {
        parse_str($this->parameters['authorization_data'], $action_authorization);
        $authorization_documents = array();
        if(isset($action_authorization['action_authorization_document']) && !empty($action_authorization['action_authorization_document']))
        {
            $authorization_documents  = $action_authorization['action_authorization_document'];
            unset($action_authorization['action_authorization_document']);
        }
        if(isset($action_authorization['deleted_authorization_document']) && !empty($action_authorization['deleted_authorization_document']))
        {
            unset($action_authorization['deleted_authorization_document']);
        }

        $actionAuthorizationObj = new ActionAuthorization();
        $action_authorization   = $this->bind($action_authorization);
        $response               = array();
        if($actionAuthorizationObj->isValid($action_authorization))
        {
            $actionAuthorizationObj->attach(new ActionAuthorizationAuditLog());
            $result   = $actionAuthorizationObj->update_where($action_authorization, array('id' => $this->parameters['id']));
            if(isset($authorization_documents) && !empty($authorization_documents))
            {
                $actionAuthorizationDocumentObj = new ActionAuthorizationDocument();
                foreach($authorization_documents as $filename => $file_description)
                {
                    if(Attachment::isFile($filename, 'action'))
                    {
                        $docs    = array('filename' => $filename, 'description' => $file_description, 'authorization_id' => $this->parameters['id']);
                        $result += $actionAuthorizationDocumentObj->save($docs);
                    }
                }
                unset($_SESSION['uploads']);
            }
            if($result > 0)
            {
                $response = array('text' => 'Action authorization successfully updated', 'error' => false, 'id' => $result, 'updated' => true);
            } elseif($result == 0) {
                $response = array('text' => 'No change was made to the action authorization', 'error' => false, 'updated' => false);
            } else {
                $response = array('text' => 'There was an error updating authorization', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }


    function bind($action_authorization)
    {
        if(isset($action_authorization['date_tested']))
        {
            $date_tested = strtotime($action_authorization['date_tested']);
            if($date_tested > 0) $action_authorization['date_tested'] = date('Y-m-d', $date_tested);
        }
        return $action_authorization;
    }

    public function deleteActionAuthorization()
    {
        $actionAuthorizationObj = new ActionAuthorization();
        $res                = $actionAuthorizationObj->update_where(array('deleted_at' => date('Y-m-d H:i:s')), array('id' => $this->parameters['id']));
        $response = array();
        if($res)
        {
            $response = array('text' => 'Action authorization successfully deleted', 'error' => false, 'updated' => true);
        } else {
            $response = array('text' => 'Error deleting the action authorization', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    public function removeActionAttachment()
    {
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('action_authorization_attachment', 'action_authorization_attachment');
        $response = $attObj->removeFile($file, 'new_action_authorization_document');
        echo json_encode($response);
    }

    public function saveAttachment()
    {
        $attObj    = new Attachment('action_authorization_attachment', 'action_authorization_attachment');
        $results   = $attObj->upload('new_action_authorization_document');
        echo json_encode($results);
    }


    public function deleteAttachment()
    {
        $actionAuthorizationDocumentObj = new ActionAuthorizationDocument();
        $res      = $actionAuthorizationDocumentObj->update_where( array('deleted_at' => date('Y-m-d H:i:s') ), array('id' => $this->parameters['document_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Attachment successfully deleted', 'error' => false);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the attachment', 'error' => false);
        } else {
            $response = array('text' => 'An error occurred deleting the attachment', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    public function download()
    {
        header("Content-type:".$_GET['content']);
        header("Content-disposition: attachment; filename=".$_GET['new']);
        readfile("../../files/".$_GET['company']."/".$_SESSION['modref']."/".$_GET['folder']."/".$_GET['file']);
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ActionAuthorizationController();
    return $class->execute($_REQUEST['action']);
}

?>