<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class RatingScaleController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $response =  array();
        if(isset($this->parameters['rating_scale_type']) && !empty($this->parameters['rating_scale_type']))
        {
            switch($this->parameters['rating_scale_type'])
            {
              case "symbol":
                $symbolRatingScaleObj = new SymbolRatingScale();
                $response = $symbolRatingScaleObj->getSymbolRatingScales();
              break;
              case "percentage":
                $percentageRatingScaleObj = new PercentageRatingScale();
                $response = $percentageRatingScaleObj->getPercentageRatingScales();
              break;
              case "faces":
                $faceRatingScaleObj = new FaceRatingScale();
                $response = $faceRatingScaleObj->getFaceRatingScales();
              break;
              case "numeric":
                $numericRatingScaleObj = new NumericalRatingScale();
                $response =  $numericRatingScaleObj->getNumericRatingScales();
              break;
              case "color":
                $colorRatingScale = new ColorRatingScale();
                $response = $colorRatingScale->getColorRatingScales();
              break;
              case "yes_no":
                $yesNoRatingScale = new YesNoRatingScale();
                $response =  $yesNoRatingScale->getYesNoRatingScales();
              break;
              case "user_defined":
                 $userDefinedRatingScale = new UserDefinedRatingScale();
                 $response =  $userDefinedRatingScale->getUserDefinedRatingScales();
              break;
              default:
                 $numericRatingScaleObj = new NumericalRatingScale();
                 $response = $numericRatingScaleObj->getNumericRatingScales();
            }
        }
        //$ratingScaleObj = new RatingScale();
        //$response       = $ratingScaleObj->getAll();
        echo json_encode($response);
        exit();
    }

    function save($options = array())
    {
        $rating_scaleObj             = new RatingScale();
        $rating_scale                = $this->parameters;
        $rating_scale['insert_user'] = $_SESSION['tid'];
        $rating_scale['created_at']  = date('Y-m-d H:i:s');
        $rating_scale['status']      = 1;
        $rating_scaleObj->attach(new RatingScaleAuditLog());
        $result                       = $rating_scaleObj->save($rating_scale);
        if($result)
        {
            $message = array('text' => 'Rating scale successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving rating scale', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        if(isset($this->parameters['rating_scale_type']) && !empty($this->parameters['rating_scale_type']))
        {
            switch($this->parameters['rating_scale_type'])
            {
                case "symbol":
                    $ratingScaleObj = new SymbolRatingScale();
                    break;
                case "percentage":
                    $ratingScaleObj = new PercentageRatingScale();
                    break;
                case "faces":
                    $ratingScaleObj = new FaceRatingScale();
                    break;
                case "numeric":
                    $ratingScaleObj = new NumericalRatingScale();
                    break;
                case "color":
                    $ratingScaleObj = new ColorRatingScale();
                    break;
                case "yes_no":
                    $ratingScaleObj = new YesNoRatingScale();
                    break;
                case "user_defined":
                    $ratingScaleObj = new UserDefinedRatingScale();
                    //$response =  $userDefinedRatingScale->getUserDefinedRatingScales();
                    break;
                default:
                    $ratingScaleObj = new NumericalRatingScale();
            }
        }
        unset($this->parameters['rating_scale_type']);
        $rating_scale                = $this->parameters;
        $rating_scale['insert_user'] = $_SESSION['tid'];
        $rating_scale['created_at']  = date('Y-m-d H:i:s');
        $ratingScaleObj->attach(new RatingScaleAuditLog());

        $result                       = $ratingScaleObj->update_where($rating_scale, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Rating scale successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the rating scale', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error saving rating scale', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

    public function getActionRatingTypeList()
    {
        $actionObj = new Action();
        $action = $actionObj->getActionDetail($this->parameters['action_id']);

        if(isset($action['rating_scale_type']) && !empty($action['rating_scale_type']))
        {
            switch($action['rating_scale_type'])
            {
                case "symbol":
                    $symbolRatingScaleObj = new SymbolRatingScale();
                    $response = $symbolRatingScaleObj->getList();
                    break;
                case "percentage":
                    $percentageRatingScaleObj = new PercentageRatingScale();
                    $response = $percentageRatingScaleObj->getList();
                    break;
                case "faces":
                    $faceRatingScaleObj = new FaceRatingScale();
                    $response = $faceRatingScaleObj->getList();
                    break;
                case "numeric":
                    $numericRatingScaleObj = new NumericalRatingScale();
                    $response =  $numericRatingScaleObj->getList();
                    break;
                case "color":
                    $colorRatingScale = new ColorRatingScale();
                    $response = $colorRatingScale->getList();
                    break;
                case "yes_no":
                    $yesNoRatingScale = new YesNoRatingScale();
                    $response =  $yesNoRatingScale->getList();
                    break;
                case "user_defined":
                    $userDefinedRatingScale = new UserDefinedRatingScale();
                    $response =  $userDefinedRatingScale->getList();
                    break;
                default:
                    $numericRatingScaleObj = new NumericalRatingScale();
                    $response = $numericRatingScaleObj->getList();
            }
        }
        echo json_encode($response);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new RatingScaleController();
    return $class->execute($_REQUEST['action']);
}

?>