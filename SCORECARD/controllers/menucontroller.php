<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class MenuController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getMenu($folder = '')
    {
        $menuObj     = new Menu();
        $folder_name = '';
        if(isset($folder) && !empty($folder))
        {
            $folder_name = $folder;
        } else {
            $folder_name = $this->folder;
        }
        $menu_items  = $menuObj->getSubMenu($folder_name);
        if(!empty($menu_items))
        {
            $this->displayMenu($menu_items);
        }
    }

    public function getAll()
    {
        $menuObj = new Menu();
        $menus   = $menuObj->getAllMenu();
        $menu_list = array();
        foreach($menus as $menu)
        {
            $section = '';
            if(!empty($menu['folder']))
            {
               $section = ucwords(str_replace('_', ' >> ', $menu['folder']));
            }
            $menu_list[$menu['id']] = array('client_terminology' => $menu['client_terminology'], 'name' => $menu['name'], 'section' => $section);
        }

        echo json_encode($menu_list);
        exit();
    }


    public function update()
    {
        $menuObj             = new Menu();
        $menu                = $this->parameters;
        $menu['insert_user'] = $_SESSION['tid'];
        $menu['updated_at']  = date('Y-m-d H:i:s');
        $menuObj->attach(new SetupAuditLog());
        $result              = $menuObj->update_where($menu, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Menu successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the menu', 'error' => false, 'updated' => false);
        }else {
            $message = array('text' => 'There was an error saving menu', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

}

if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new MenuController();
    return $class->execute($_REQUEST['action']);
}