<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class AssessmentFrequencyController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $frequencyObj = new AssessmentFrequency();
        $option_sql   = '';
        if(isset($this->parameters['financial_year']))  $option_sql = " AND financial_year_id = '".$this->parameters['financial_year']."' ";
        $response    = $frequencyObj->getAll(false, $option_sql);
        echo json_encode($response);
        exit();
    }

    function save($options = array())
    {
        $frequencyObj             = new AssessmentFrequency();
        $frequency                = $this->parameters;
        if(isset($frequency['date']) && !empty($frequency['date'])) $frequency['date'] = date('Y-m-d', strtotime($frequency['date']));
        $frequency['insert_user'] = $_SESSION['tid'];
        $frequency['created_at']  = date('Y-m-d H:i:s');
        $frequency['status']      = 1;
        $result                       = $frequencyObj->save($frequency);
        if($result)
        {
            $message = array('text' => 'Assessment frequency successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving assessment frequency', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $frequencyObj             = new AssessmentFrequency();
        $frequency                = $this->parameters;
        if(isset($frequency['date']) && !empty($frequency['date']))  $frequency['date'] = date('Y-m-d', strtotime($frequency['date']));
        $frequency['insert_user'] = $_SESSION['tid'];
        //$frequency['created_at']  = date('Y-m-d H:i:s');
        $frequencyObj->attach(new SetupAuditLog());
        $result                       = $frequencyObj->update_where($frequency, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Assessment frequency successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the assessment frequency', 'error' => false, 'updated' => true);
        }else {
            $message = array('text' => 'There was an error saving assessment frequency', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new AssessmentFrequencyController();
    return $class->execute($_REQUEST['action']);
}

?>