<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class FinancialYearController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $financialYearObj = new FinancialYear();
        $response         = $financialYearObj->getAll();
        echo json_encode($response);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new FinancialYearController ();
    return $class->execute($_REQUEST['action']);
}

?>