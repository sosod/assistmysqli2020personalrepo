<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ScoreCardStatusController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($options = array())
    {
        $statusObj = new ScoreCardStatus();
        $response    = $statusObj->getAll();
        echo json_encode($response);
        exit();
    }

    function save($options = array())
    {
        $statusObj                      = new ScoreCardStatus();
        $score_card_status                = $this->parameters;
        $score_card_status['insert_user'] = $_SESSION['tid'];
        $score_card_status['created_at']  = date('Y-m-d H:i:s');
        $score_card_status['status']      = 1;
        $statusObj->attach(new SetupAuditLog());
        $result                       = $statusObj->save($score_card_status, 'Score Card Status');
        if($result)
        {
            $message = array('text' => 'Score card status successfully saved', 'error' => false);
        } else {
            $message = array('text' => 'There was an error saving score card status', 'error' => true);
        }
        echo json_encode($message);
        exit();
    }

    public function update()
    {
        $statusObj                      = new ScoreCardStatus();
        $score_card_status                = $this->parameters;
        $score_card_status['insert_user'] = $_SESSION['tid'];
        $score_card_status['created_at']  = date('Y-m-d H:i:s');
        $statusObj->attach(new SetupAuditLog());
        $result                       = $statusObj->update_where($score_card_status, array('id' => $this->parameters['id']));
        if($result)
        {
            $message = array('text' => 'Score card status successfully updated', 'error' => false, 'updated' => true);
        } elseif($result == 0) {
            $message = array('text' => 'No change was made to the score card status', 'error' => false, 'updated' => true);
        } else {
            $message = array('text' => 'There was an error saving score card status', 'error' => true, 'updated' => false);
        }
        echo json_encode($message);
        exit();
    }

}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ScoreCardStatusController ();
    return $class->execute($_REQUEST['action']);
}

?>