<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ScoreCardAllocateToDirectorateController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }
    public function getDirectorateQuestionForm()
    {
        $parameters = array();
        if(isset($this->parameters['directorates'])) parse_str($this->parameters['directorates'], $parameters);

        $actionObj = new Action();
        $score_card_questions = $actionObj->getActionsListByScoreCardId($this->parameters['score_card_id']);

        $scoreCardOwnerObj = new ScoreCardOwner();
        $directorates      = $scoreCardOwnerObj->getList();

        $pre_table_str = '';
        $after_table = array();
        if(isset($parameters['directorates']))
        {
            $parameters['directorates'][] = $this->parameters['directorate_id'];
        } else {
            if(isset($this->parameters['directorate_id'])) $parameters['directorates'][] = $this->parameters['directorate_id'];
        }
        if(isset($parameters['questions']))
        {
            $parameters['questions'][] = $this->parameters['question_id'];
        } else {
            if(isset($this->parameters['question_id']))  $parameters['questions'][] = $this->parameters['question_id'];
        }
        if(isset($parameters['questions']) && !empty($parameters['questions']))
        {
            foreach($parameters['questions'] as $index => $question)
            {
                $after_table[$index]['question'] = $score_card_questions[$question];
                unset($score_card_questions[$question]);
                $pre_table_str .= "<input type='hidden' name='questions[]' id='questions_".$question."' value='".$question."' />";
            }
        }
        if(isset($parameters['directorates']) && !empty($parameters['directorates']))
        {
            foreach($parameters['directorates'] as $index => $user)
            {

                $after_table[$index]['directorate'] = $directorates[$user];
                unset($directorates[$user]);
                $pre_table_str .= "<input type='hidden' name='directorates[]' id='directorates_".$user."' value='".$user."' />";
            }
        }
        $table_str  = "<form method='post' id='allocate-directorate-to-question'>";
        $table_str  .= "<table>";
        $table_str .= "<tr>";
        $table_str .= "<th class='th2'>Question</th>";
        $table_str .= "<th class='th2'>Directorate/Sub-Directorate</th>";
        $table_str .= "<th class='th2'>Allocate Question to Directorate</th>";
        $table_str .= "</tr>";
        $table_str .= "<tr>";
        $table_str .= "<td>";
        $table_str .= select_tag('score_card_setup[question_id]', $score_card_questions, array('id' => 'question_id'));
        $table_str .= "</td>";
        $table_str .= "<td>";
        $table_str .= select_tag('score_card_setup[directorate_id]', $directorates, array('id' => 'directorate_id'));
        $table_str .= "</td>";
        $table_str .= "<td>";
        $table_str .= "<input type='submit' name='allocate_directorate' id='allocate_directorate' value='Allocate' />";
        $table_str .= "</td>";
        $table_str .= "</tr>";
        if(!empty($after_table))
        {
            foreach($after_table as $table_td_str)
            {
                $table_str .= "<tr>";
                $table_str .= "<td>".$table_td_str['question']."</td>";
                $table_str .= "<td>".$table_td_str['directorate']."</td>";
                $table_str .= "<td></td>";
                //$table_str .= "<td><a href='#'>Remove</a></td>";
                $table_str .= "</tr>";
            }
        }
        $table_str .= $pre_table_str;
        $table_str .= "</table>";
        $table_str .= "</form>";
        echo $table_str;
        exit();
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ScoreCardAllocateToDirectorateController ();
    return $class->execute($_REQUEST['action']);
}

?>