<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class DeliverableAuthorizationController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }


    public function getDeliverableAuthorizations($options = array())
    {
        $deliverableAuthorizationObj = new DeliverableAuthorization();
        $option_sql           = $deliverableAuthorizationObj->prepareSql($this->parameters);

        $deliverable_authorization    = $deliverableAuthorizationObj->getDeliverableAuthorizations($option_sql);

        $_option_sql           = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $total_authorization      = $deliverableAuthorizationObj->getTotalDeliverableAuthorization($_option_sql);

        $deliverable_authorization  = $deliverableAuthorizationObj->prepareDeliverableAuthorization($deliverable_authorization, $total_authorization, $this->parameters);
        echo json_encode($deliverable_authorization);
        exit();
    }


    function saveDeliverableAuthorization($options = array())
    {
        parse_str($this->parameters['authorization_data'], $deliverable_authorization);
        $authorization_documents = array();
        if(isset($deliverable_authorization['deliverable_authorization_document']) && !empty($deliverable_authorization['deliverable_authorization_document']))
        {
            $authorization_documents = $deliverable_authorization['deliverable_authorization_document'];
            unset($deliverable_authorization['deliverable_authorization_document']);
        }
        $deliverableAuthorizationObj = new DeliverableAuthorization();
        $deliverable_authorization = $this->bind($deliverable_authorization);
        $response            = array();

        if($deliverableAuthorizationObj->isValid($deliverable_authorization))
        {
            $deliverable_authorization['deliverable_id'] = $this->parameters['deliverable_id'];
            $deliverable_authorization['insert_user']    = $_SESSION['tid'];
            $deliverable_authorization['created_at']    = date('Y-m-d H:i:s');
            //$contract_payment['status']      = 1;
            $result                          = $deliverableAuthorizationObj->save($deliverable_authorization);
            if($result)
            {
                if(isset($authorization_documents) && !empty($authorization_documents))
                {
                    $deliverableAuthorizationDocumentObj = new DeliverableAuthorizationDocument();
                    foreach($authorization_documents as $filename => $file_description)
                    {
                        if(Attachment::isFile($filename, 'deliverable'))
                        {
                            $docs = array('filename' => $filename, 'description' => $file_description, 'authorization_id' => $result);
                            $deliverableAuthorizationDocumentObj->save($docs);
                        }
                    }
                    unset($_SESSION['uploads']);
                }
                $response = array('text' => 'Deliverable authorization successfully saved', 'error' => false, 'id' => $result);
            } else {
                $response = array('text' => 'There was an error saving deliverable authorization', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }

    function updateDeliverableAuthorization($options = array())
    {
        parse_str($this->parameters['authorization_data'], $deliverable_authorization);
        $authorization_documents = array();
        if(isset($deliverable_authorization['deliverable_authorization_document']) && !empty($deliverable_authorization['deliverable_authorization_document']))
        {
            $authorization_documents  = $deliverable_authorization['deliverable_authorization_document'];
            unset($deliverable_authorization['deliverable_authorization_document']);
        }
        if(isset($deliverable_authorization['deleted_authorization_document']) && !empty($deliverable_authorization['deleted_authorization_document']))
        {
            unset($deliverable_authorization['deleted_authorization_document']);
        }

        $deliverableAuthorizationObj = new DeliverableAuthorization();
        $deliverable_authorization   = $this->bind($deliverable_authorization);
        $response               = array();
        if($deliverableAuthorizationObj->isValid($deliverable_authorization))
        {
            $deliverableAuthorizationObj->attach(new DeliverableAuthorizationAuditLog());
            $result   = $deliverableAuthorizationObj->update_where($deliverable_authorization, array('id' => $this->parameters['id']));
            if(isset($authorization_documents) && !empty($authorization_documents))
            {
                $deliverableAuthorizationDocumentObj = new DeliverableAuthorizationDocument();
                foreach($authorization_documents as $filename => $file_description)
                {
                    if(Attachment::isFile($filename, 'deliverable'))
                    {
                        $docs    = array('filename' => $filename, 'description' => $file_description, 'authorization_id' => $this->parameters['id']);
                        $result += $deliverableAuthorizationDocumentObj->save($docs);
                    }
                }
                unset($_SESSION['uploads']);
            }
            if($result > 0)
            {
                $response = array('text' => 'Deliverable authorization successfully updated', 'error' => false, 'id' => $result, 'updated' => true);
            } elseif($result == 0) {
                $response = array('text' => 'No change was made to the deliverable authorization', 'error' => false, 'updated' => false);
            } else {
                $response = array('text' => 'There was an error updating authorization', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }


    function bind($deliverable_authorization)
    {
        if(isset($deliverable_authorization['date_tested']))
        {
            $date_tested = strtotime($deliverable_authorization['date_tested']);
            if($date_tested > 0) $deliverable_authorization['date_tested'] = date('Y-m-d', $date_tested);
        }
        return $deliverable_authorization;
    }

    public function deleteDeliverableAuthorization()
    {
        $deliverableAuthorizationObj = new DeliverableAuthorization();
        $res                = $deliverableAuthorizationObj->update_where(array('deleted_at' => date('Y-m-d H:i:s')), array('id' => $this->parameters['id']));
        $response = array();
        if($res)
        {
            $response = array('text' => 'Deliverable authorization successfully deleted', 'error' => false);
        } else {
            $response = array('text' => 'Error deleting the deliverable authorization', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    public function removeDeliverableAttachment()
    {
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('deliverable_authorization_attachment', 'deliverable_authorization_attachment');
        $response = $attObj->removeFile($file, 'new_deliverable_authorization_document');
        echo json_encode($response);
    }

    public function saveAttachment()
    {
        $attObj    = new Attachment('deliverable_authorization_attachment', 'deliverable_authorization_attachment');
        $results   = $attObj->upload('new_deliverable_authorization_document');
        echo json_encode($results);
    }


    public function deleteAttachment()
    {
        $deliverableAuthorizationDocumentObj = new DeliverableAuthorizationDocument();
        $res      = $deliverableAuthorizationDocumentObj->update_where( array('deleted_at' => date('Y-m-d H:i:s') ), array('id' => $this->parameters['document_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Attachment successfully deleted', 'error' => false);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the attachment', 'error' => false);
        } else {
            $response = array('text' => 'An error occurred deleting the attachment', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    public function download()
    {
        header("Content-type:".$_GET['content']);
        header("Content-disposition: attachment; filename=".$_GET['new']);
        readfile("../../files/".$_GET['company']."/".$_SESSION['modref']."/".$_GET['folder']."/".$_GET['file']);
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new DeliverableAuthorizationController();
    return $class->execute($_REQUEST['action']);
}

?>