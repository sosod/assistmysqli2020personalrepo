<?php
@session_start();
class BaseController
{

    static protected $methods = array();

    var $parameters           = array();

    var $raw_params           = array();

    var $scripts              = array();

    var $layout               = true;

    protected $request_uri    = array();

    protected $query_string    = array();

    protected $folder          = '';

    protected $page_name       = '';

    protected $uri_pieces      = array();

    protected $page            = '';

    public function __construct()
    {
        $this->raw_params         = $_REQUEST;
        $this->parameters         = $this->clean($_REQUEST);
        $this->request_uri        = $_SERVER['REQUEST_URI'];
        $this->query_string		  = $_SERVER['QUERY_STRING'];
        $this->getUrlParts();

    }

    //split the url into parts
    private function getUrlParts()
    {
        $this->uri_pieces = preg_split('[\\/]', $this->request_uri, -1, PREG_SPLIT_NO_EMPTY);
        $pieces    		  = count($this->uri_pieces);
        $this->folder     = "";
        if($pieces > 0)
        {
            foreach($this->uri_pieces as $key => $val)
            {
                if(strpos($val, ".") > 0)
                {
                    $this->page      = $val;
                    $this->page_name = substr($val,0, strpos($val, "."));
                    $this->folder 	 = strtolower($this->uri_pieces[$pieces-2]);
                } else {
                    $this->page_name = $this->uri_pieces[$pieces-1];
                    $this->page      = $this->page_name;
                    $this->folder 	 = strtolower($this->uri_pieces[$pieces-1]);
                }
            }
        }
    }

    private function clean($parameters)
    {
        $vars = array();
        unset($parameters['uri_pieces']);
        foreach($parameters as $key => $var)
        {
           if(!in_array($key, array('add', 'action', 'edit', 'update', 'delete', 'save')))
           {
              if(is_array($var))
              {
                 $vars[$key] = $this->clean($var);
              } else {
                 $vars[$key] = trim($var);
              }
           }
        }
       return $vars;
    }

    public function execute($method)
    {
        $class =  get_class($this);
        return call_user_func_array(array(new $class, $method), $_REQUEST);
    }

    public function __set($key, $value)
    {
       $this->parameters[$key] = $value;
    }

    public function __get($key)
    {
       if(isset($this->parameters[$key]))
       {
          return $this->parameters[$key];
       }
       if(isset($_REQUEST[$key]))
       {
            return $_REQUEST[$key];
       }
      return false;
    }

    public function getParameter($key)
    {
        if(isset($this->parameters[$key]))
        {
           return $this->parameters[$key];
        }
       return false;
    }

    public function __call($method, $args)
    {
        if (in_array($method, self::$methods))
        {
            return call_user_func_array($method, $args);
        }
    }

    public function redirect($url, $delay = 0, $statusCode = 302)
    {
        $url = "http://".$_SERVER['HTTP_HOST']."/".$_SESSION['modlocation']."/".$url;
        header("Location:".$url);
    }
}