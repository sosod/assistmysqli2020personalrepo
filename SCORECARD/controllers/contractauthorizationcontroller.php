<?php
require_once '../contract_autoloader.php';
require_once '../score_card_helper.php';
class ContractAuthorizationController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getContractAuthorizations($options = array())
    {
        $contractAuthorizationObj = new ContractAuthorization();
        $option_sql           = $contractAuthorizationObj->prepareSql($this->parameters);

        $contract_authorization    = $contractAuthorizationObj->getContractAuthorizations($option_sql);

        $_option_sql           = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
        $total_assurance      = $contractAuthorizationObj->getTotalContractAuthorization($_option_sql);

        $contract_authorization  = $contractAuthorizationObj->prepareContractAuthorization($contract_authorization, $total_assurance, $this->parameters);
        echo json_encode($contract_authorization);
        exit();
    }


    function saveContractAuthorization($options = array())
    {
        parse_str($this->parameters['authorization_data'], $contract_authorization);
        $assurance_documents = array();
        if(isset($contract_authorization['contract_authorization_docs']) && !empty($contract_authorization['contract_authorization_docs']))
        {
            $assurance_documents = $contract_authorization['contract_authorization_docs'];
            unset($contract_authorization['contract_authorization_docs']);
        }
        $contractAuthorizationObj = new ContractAuthorization();
        $contract_authorization = $this->bind($contract_authorization);
        $response            = array();
        if($contractAuthorizationObj->isValid($contract_authorization))
        {
            $contract_authorization['contract_id'] = $this->parameters['contract_id'];
            $contract_authorization['insert_user'] = $_SESSION['tid'];
            $contract_authorization['created_at']  = date('Y-m-d H:i:s');
            //$contract_payment['status']      = 1;
            $result                          = $contractAuthorizationObj->save($contract_authorization);
            if($result)
            {
                if(isset($assurance_documents) && !empty($assurance_documents))
                {
                    $contractAuthorizationDocumentObj = new ContractAuthorizationDocument();
                    foreach($assurance_documents as $filename => $file_description)
                    {
                        if(Attachment::isFile($filename))
                        {
                            $docs = array('filename' => $filename, 'description' => $file_description, 'authorization_id' => $result);
                            $contractAuthorizationDocumentObj->save($docs);
                        }
                    }
                    unset($_SESSION['uploads']);
                }
                $response = array('text' => 'Contract authorization successfully saved', 'error' => false, 'id' => $result);
            } else {
                $response = array('text' => 'There was an error saving contract authorization', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }

    function updateContractAuthorization($options = array())
    {
        parse_str($this->parameters['authorization_data'], $contract_authorization);
        $assurance_documents = array();
        if(isset($contract_authorization['contract_authorization_docs']) && !empty($contract_authorization['contract_authorization_docs']))
        {
            $assurance_documents  = $contract_authorization['contract_authorization_docs'];
            unset($contract_authorization['contract_authorization_docs']);
        }
        if(isset($contract_authorization['deleted_assurance_document']) && !empty($contract_authorization['deleted_assurance_document']))
        {
            unset($contract_authorization['deleted_assurance_document']);
        }

        $contractPaymentObj = new ContractAuthorization();
        $contract_authorization   = $this->bind($contract_authorization);
        $response               = array();
        if($contractPaymentObj->isValid($contract_authorization))
        {
            $contractPaymentObj->attach(new ContractAuthorizationAuditLog());
            $result   = $contractPaymentObj->update_where($contract_authorization, array('id' => $this->parameters['id']));
            if(isset($assurance_documents) && !empty($assurance_documents))
            {
                $contractAuthorizationDocumentObj = new ContractAuthorizationDocument();
                foreach($assurance_documents as $filename => $file_description)
                {
                    if(Attachment::isFile($filename))
                    {
                        $docs    = array('filename' => $filename, 'description' => $file_description, 'authorization_id' => $this->parameters['id']);
                        $result += $contractAuthorizationDocumentObj->save($docs);
                    }
                }
                unset($_SESSION['uploads']);
            }
            if($result > 0)
            {
                $response = array('text' => 'Contract assurance successfully updated', 'error' => false, 'id' => $result, 'updated' => true);
            } elseif($result == 0) {
                $response = array('text' => 'No change was made to the contract assurance', 'error' => false, 'updated' => false);
            } else {
                $response = array('text' => 'There was an error updating assurance', 'error' => true);
            }
        }
        echo json_encode($response);
        exit();
        //$this->redirect('new/index.php');
    }


    function bind($contract_authorization)
    {
        if(isset($contract_authorization['date_tested']))
        {
            $date_tested = strtotime($contract_authorization['date_tested']);
            if($date_tested > 0)
            {
                $contract_authorization['date_tested'] = date('Y-m-d', $date_tested);
            }
        }
        return $contract_authorization;
    }

    public function deleteContractAuthorization()
    {
        $contractAuthorizationObj = new ContractAuthorization();
        $res                = $contractAuthorizationObj->update_where(array('deleted_at' => date('Y-m-d H:i:s')), array('id' => $this->parameters['id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Contract authorization successfully deleted', 'error' => false, 'updated' => true);
        } else {
            $response = array('text' => 'Error deleting the contract authorization', 'error' => true);
        }
        echo json_encode($response);
        exit();
    }

    public function removeContractPaymentAttachment()
    {
        $file     = $_POST['attachment'].".".$_POST['ext'];
        $attObj   = new Attachment('contract_authorization_attachment', 'contract_authorization_attachment');
        $response = $attObj->removeFile($file, 'new_contract_authorization_document');
        echo json_encode($response);
    }

    public function saveAttachment()
    {
        $attObj    = new Attachment('contract_authorization_attachment', 'contract_authorization_attachment');
        $results   = $attObj->upload('new_contract_authorization_document');
        echo json_encode($results);
    }


    public function deleteAttachment()
    {
        $contractAuthorizationDocumentObj = new ContractAuthorizationDocument();
        $res      = $contractAuthorizationDocumentObj->update_where( array('deleted_at' => date('Y-m-d H:i:s') ), array('id' => $this->parameters['document_id']));
        $response = array();
        if($res > 0)
        {
            $response = array('text' => 'Attachment successfully deleted', 'error' => false);
        } elseif($res == 0){
            $response = array('text' => 'No change made to the attachment', 'error' => false);
        } else {
            $response = array('text' => 'An error occurred deleting the attachment', 'error' => false);
        }
        echo json_encode($response);
        exit();
    }

    public function download()
    {
        header("Content-type:".$_GET['content']);
        header("Content-disposition: attachment; filename=".$_GET['new']);
        readfile("../../files/".$_GET['company']."/".$_SESSION['modref']."/".$_GET['folder']."/".$_GET['file']);
    }
}


if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $class = new ContractAuthorizationController();
    return $class->execute($_REQUEST['action']);
}

?>