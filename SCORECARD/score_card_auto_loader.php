<?php
function score_card_auto_load($class_name)
{
    $filename = strtolower($class_name);
    //echo "Looking for ".$filename;
    if( file_exists( "../class/".$filename.".php" ) )
    {
        require_once( "../class/".$filename.".php" );
    } elseif( file_exists( "../controllers/".$filename.".php" ) ) {
        require_once( "../controllers/".$filename.".php" );
    } elseif(file_exists("../../library/dbconnect/".$filename.".php")) {
        require_once( "../../library/dbconnect/".$filename.".php" );
    } elseif("../../library/class/assist_helper.php") {
        require_once( "../../library/class/assist_helper.php" );
    } /*elseif('../../library/class/'.$filename.'.php') {
        require_once("../../library/class/assist_email_summary.php");
    } */else {
        require_once( "../../library/".$filename.".php" );
    }
}
spl_autoload_register('score_card_auto_load');
?>
