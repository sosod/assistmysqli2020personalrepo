<?php
function include_basic_files()
{
    include_once("../inc_ignite.php");
    $helper     = new ASSIST_HELPER();
    //include_css();
}

function display_menu_list($folder, $page = 'index')
{
    $menuObj  = new Menu();
    $menu_items = $menuObj->getSubMenu($folder);

    $userObj      = new User();
    $user_setting = $userObj->getSettingByUserId($_SESSION['tid']);

    $menu_list = array();
    foreach($menu_items as $key => $menu)
    {
        if(((int)$user_setting['status'] & (int)$menu['user_access_status']) == (int)$menu['user_access_status'])
        {
            if($menu['action_file'] == 'assessment' && !AssessmentFrequency::isOpen())
            {
                 continue;
            }
            $activeUrl = "";
            $file_name = '';
            if(!empty($menu['action_file']))
            {
                $url  = strtolower($menu['action_file']).".php";
                $file_name = strtolower($menu['action_file']);
            } else {
                $url  = "index.php";
                $file_name = 'index';
            }
            $action    = '';
            if($file_name == $page)
            {
                $activeUrl = "Y";
            }
            $display                = ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology']);
            $menu_list[$menu['id']] = array("id" => $menu['id'], "url"	=> $url, "active"  => $activeUrl, "display" => $display) ;
        }
    }
    echo echoNavigation(1, $menu_list);
}

function display_sub_menu_list($folder, $section, $page = 'index')
{
    $name       = $folder."_".$section;
    $menuObj    = new Menu();
    $menu_items = $menuObj->getSubMenu($name);

    $userObj      = new User();
    $user_setting = $userObj->getSettingByUserId($_SESSION['tid']);

    $menu_list  = array();
    $page_valid = FALSE;
    //debug($menu_items);
    foreach($menu_items as $key => $menu)
    {
        if(((int)$user_setting['status'] & (int)$menu['user_access_status']) == (int)$menu['user_access_status'])
        {
            $page_valid = TRUE;
            $activeUrl  = "";
            $file_name = "";
            if(!empty($menu['action_file']))
            {
                $url  = strtolower($menu['action_file']).".php";
                $file_name = strtolower($menu['action_file']);
            } else {
                $url  = "index.php";
                $file_name = 'index';
            }
            if($file_name == $page)
            {
                $activeUrl = "Y";
            }
            $display                = ($menu['client_terminology'] == "" ? $menu['ignite_terminology'] : $menu['client_terminology']);
            $menu_list[$menu['id']] = array("id" => $menu['id'], "url"	=> $url, "active"  => $activeUrl, "display" => $display) ;
        }
    }
    echo echoNavigation(2, $menu_list);
    return $page_valid;
}

function page_navigation_links($links = array())
{
    $link_str  = '';
    if(isset($links) && !empty($links))
    {
        $total = count($links);
        $i     = 1;
        foreach($links as $link => $title)
        {
           $link_str .= "<a href=".$link.">".$title."</a> ".($total == $i ? '' : ' >> ');
           $i++;
        }
    }
    echo "<h3>".$link_str."</h3>";
}

function display_title()
{

}

function include_js($page_js_scripts = array())
{
    $scripts = array('../js/jscolor.js', '../js/common.js');
    if(!empty($page_js_scripts))
    {
        if(is_array($page_js_scripts))
        {
            foreach($page_js_scripts as $js_script)
            {
                $scripts[] = "../js/".$js_script;
            }
        }
    }
    ASSIST_HELPER::echoPageHeader('1.9.2', $scripts );
    $main_scripts = array(); //array('/library/jquery-ui-1.9.1/js/jquery-1.8.2.js', '/library/jquery-ui-1.9.1/js/jquery-ui-1.9.1.custom.js', '../js/jscolor.js', '../js/common.js');
/*    if(!empty($main_scripts))
    {
        if(is_array($main_scripts))
        {
           foreach ($main_scripts as $script)
           {
              echo "<script type='text/javascript' src=".$script."></script>";
           }
        }
    }
    if(!empty($page_js_scripts))
    {
        if(is_array($page_js_scripts))
        {
            foreach($page_js_scripts as $js_script)
            {
                echo "<script type='text/javascript' src=../js/".$js_script."></script>";
            }
        }
    }*/
}

function escape_javascript($javascript = '')
{
    $javascript = preg_replace('/\r\n|\n|\r/', "\\n", $javascript);
    $javascript = preg_replace('/(["\'])/', '\\\\\1', $javascript);

    return $javascript;
}

function include_css($css = array())
{
    $main_styles = array('/library/jquery-ui-1.9.1/css/jquery-ui-1.9.1.custom.css', '/assist.css');
    if(!empty($main_styles))
    {
        if(is_array($main_styles))
        {
            foreach ($main_styles as $style)
            {
                echo "<link rel='stylesheet' href=".$style." type='text/css' />";
            }
        }
    }
}

function toUserId($id)
{
    return str_pad($id, 4, '0', STR_PAD_LEFT);
}

function serializeEncode($data)
{
    return base64_encode(serialize($data));
}

function serializeDecode($data)
{
    return @unserialize(base64_decode($data));
}

function decodeDirectorate( $array )
{
    return array(
        "dirtxt" => html_entity_decode($array['dirtxt'], ENT_QUOTES, "ISO-8859-1") ,
        "subid"  => $array['subid'],
        "active" => $array['active']
    );
}

function is_valid_date($end_date, $start_date)
{
    if(strtotime($start_date) > strtotime($end_date))
    {
        return FALSE;
    } else {
        return TRUE;

    }
}

function set_flash($status, $key, $message)
{
    if($status == 'success')
    {
        if(!isset($_SESSION[$key]))
        {
            $_SESSION['flash_success'][$key]['_success'] = $message;
        }
    } else if($status == 'notice' || $status == 'warning'){
        if(!isset($_SESSION[$key]))
        {
            $_SESSION['flash_notice'][$key]['_notice'] = $message;
        }
    } elseif($status == 'error'){
        if(!isset($_SESSION[$key]))
        {
            $_SESSION['flash_errors'][$key]['_error'] = $message;
        }
    }
}

function get_flash($key)
{
    $flash_str   = '';

    if(isset($_SESSION['flash_errors']) && !empty($_SESSION['flash_errors']))
    {
        if(isset($_SESSION['flash_errors'][$key]) && !empty($_SESSION['flash_errors'][$key]))
        {
            $flash_str = '';
            foreach($_SESSION['flash_errors'][$key] as $_key => $e_str)
            {
                $flash_str .= $e_str."<br /><br />";
            }
            unset($_SESSION['flash_errors'][$key]);
        }
        unset($_SESSION['flash_errors']);
    }
    if(isset($_SESSION['flash_success']) && !empty($_SESSION['flash_success']))
    {
        if(isset($_SESSION['flash_success'][$key]) && !empty($_SESSION['flash_success'][$key]))
        {
            $flash_str = '';
            foreach($_SESSION['flash_success'][$key] as $_key => $s_str)
            {
                $flash_str .= $s_str."<br />";
            }
            unset($_SESSION['flash_success'][$key]);
        }
        unset($_SESSION['flash_success']);
    }

    if(isset($_SESSION['flash_notice']) && !empty($_SESSION['flash_notice']))
    {
        if(isset($_SESSION['flash_notice'][$key]) && !empty($_SESSION['flash_notice'][$key]))
        {
            $flash_str = '';
            foreach($_SESSION['flash_notice'][$key] as $_key => $n_str)
            {
                $flash_str .= $n_str."<br />";
            }
            unset($_SESSION['flash_notice'][$key]);
        }
        unset($_SESSION['flash_notice']);
    }
    return $flash_str;
}

function display_flash($key)
{
    $error_str   = '';
    $success_str = '';
    $notice_str  = '';

    if(isset($_SESSION['flash_errors']) && !empty($_SESSION['flash_errors']))
    {
        if(isset($_SESSION['flash_errors'][$key]) && !empty($_SESSION['flash_errors'][$key]))
        {
            foreach($_SESSION['flash_errors'][$key] as $_key => $e_str)
            {
                $error_str .= $e_str."<br /><br />";
            }
            unset($_SESSION['flash_errors'][$key]);
        }
        unset($_SESSION['flash_errors']);
    }
    if(isset($_SESSION['flash_success']) && !empty($_SESSION['flash_success']))
    {
        if(isset($_SESSION['flash_success'][$key]) && !empty($_SESSION['flash_success'][$key]))
        {
            foreach($_SESSION['flash_success'][$key] as $_key => $s_str)
            {
                $success_str .= $s_str."<br />";
            }
            unset($_SESSION['flash_success'][$key]);
        }
        unset($_SESSION['flash_success']);
    }

    if(isset($_SESSION['flash_notice']) && !empty($_SESSION['flash_notice']))
    {
        if(isset($_SESSION['flash_notice'][$key]) && !empty($_SESSION['flash_notice'][$key]))
        {
            foreach($_SESSION['flash_notice'][$key] as $_key => $n_str)
            {
                $notice_str .= $n_str."<br />";
            }
            unset($_SESSION['flash_notice'][$key]);
        }
        unset($_SESSION['flash_notice']);
    }
    $print_str = '';
    if(!empty($success_str))
    {
        $print_str  = "<p class='ui-state ui-state-widget ui-state-ok' style='width:auto; padding: 8px 35px 8px 14px; border: 1px solid #006400;'>";
        $print_str .= "<span class='ui-icon ui-icon-check ui-icon-ok' style='float:left'></span>";
        $print_str .= "<span>".$success_str."</span>";
        $print_str .= "</p>";
    }
    if(!empty($notice_str))
    {
        $print_str  = "<p class='ui-state ui-state-widget ui-state-info' style='width:auto; padding: 8px 35px 8px 14px;>";
        $print_str .= "<span class='ui-icon ui-icon-info ui-icon-info' style='float:left'></span>";
        $print_str .= "<span>".$notice_str."</span>";
        $print_str .= "</p>";
    }

    if(!empty($error_str))
    {
        $print_str = "<p class='ui-state ui-state-widget ui-state-error' style='width:auto; padding: 8px 35px 8px 14px;'>";
        $print_str .= "<span class='ui-icon ui-icon-closethick ui-icon-error' style='float:left'></span>";
        $print_str .= "<span>".$error_str."</span>";
        $print_str .= "</p>";
    }
    echo $print_str;
}

function debug($data)
{
    if(is_array($data) || is_object($data))
    {
        print "<pre>";
          print_r($data);
        print "</pre>";
    } else {
        print "<pre>";
          var_dump($data);
        print "</pre>";
    }
}

function tag($name, $options = array(), $open = false)
{
    if (!$name)
    {
        return '';
    }

    return '<'.$name._tag_options($options).(($open) ? '>' : ' />');
}

function content_tag($name, $content = '', $options = array())
{
    if (!$name)
    {
        return '';
    }

    return '<'.$name._tag_options($options).'>'.$content.'</'.$name.'>';
}

function cdata_section($content)
{
    return "<![CDATA[$content]]>";
}

function escape_once($html)
{
    return fix_double_escape(htmlspecialchars($html, ENT_COMPAT, 'utf-8'));
}

/**
 * Fixes double escaped strings.
 *
 * @param  string HTML string to fix
 * @return string escaped string
 */
function fix_double_escape($escaped)
{
    return preg_replace('/&amp;([a-z]+|(#\d+)|(#x[\da-f]+));/i', '&$1;', $escaped);
}

function _tag_options($options = array())
{
    $options = _parse_attributes($options);

    $html = '';
    foreach ($options as $key => $value)
    {
        $html .= ' '.$key.'="'.escape_once($value).'"';
    }

    return $html;
}

function _parse_attributes($string)
{
    //return is_array($string) ? $string : sfToolkit::stringToArray($string);
    return is_array($string) ? $string : $string;
}

function _get_option(&$options, $name, $default = null)
{
    if (array_key_exists($name, $options))
    {
        $value = $options[$name];
        unset($options[$name]);
    }
    else
    {
        $value = $default;
    }

    return $value;
}


function options_for_select($options = array(), $selected = '', $html_options = array())
{
    $html_options = _parse_attributes($html_options);

    if (is_array($selected))
    {
        $selected = array_map('strval', array_values($selected));
    }

    $html = '';
    $html .= content_tag('option', '--please select--', array('value' => ''))."\n";
    if ($value = _get_option($html_options, 'include_custom'))
    {
        $html .= content_tag('option', '--please select--', array('value' => ''))."\n";
    }
    else if (_get_option($html_options, 'include_blank'))
    {
        $html = '';
        //$html .= content_tag('option', '', array('value' => ''))."\n";
    }

    foreach ($options as $key => $value)
    {
        $option_options = array('value' => $key);

        if (
            (is_array($selected) && in_array(strval($key), $selected, true))
            ||
            (strval($key) == strval($selected))
        )
        {
            $option_options['selected'] = 'selected';
        }

        $html .= content_tag('option', $value, $option_options)."\n";
    }

    return $html;
}

/**
 * Returns an HTML <form> tag that points to a valid action, route or URL as defined by <i>$url_for_options</i>.
 *
 * By default, the form tag is generated in POST format, but can easily be configured along with any additional
 * HTML parameters via the optional <i>$options</i> parameter. If you are using file uploads, be sure to set the
 * <i>multipart</i> option to true.
 *
 * <b>Options:</b>
 * - multipart - When set to true, enctype is set to "multipart/form-data".
 *
 * <b>Examples:</b>
 *   <code><?php echo form_tag('@myroute'); ?></code>
 *   <code><?php echo form_tag('/module/action', array('name' => 'myformname', 'multipart' => true)); ?></code>
 *
 * @param  string valid action, route or URL
 * @param  array optional HTML parameters for the <form> tag
 * @return string opening HTML <form> tag with options
 */
function form_tag($url_for_options = '', $options = array())
{
    $options = _parse_attributes($options);

    $html_options = $options;
    if (!isset($html_options['method']))
    {
        $html_options['method'] = 'post';
    }

    if (_get_option($html_options, 'multipart'))
    {
        $html_options['enctype'] = 'multipart/form-data';
    }

    $html_options['action'] = url_for($url_for_options);

    return tag('form', $html_options, true);
}

/**
 * Returns a <select> tag, optionally comprised of <option> tags.

 * <b>Options:</b>
 * - multiple - If set to true, the select tag will allow multiple options to be selected at once.
 *
 * <b>Examples:</b>
 * <code>
 *  $person_list = array(1 => 'Larry', 2 => 'Moe', 3 => 'Curly');
 *  echo select_tag('person', options_for_select($person_list, $sf_params->get('person')), array('class' => 'full'));
 * </code>
 *
 * <code>
 *  echo select_tag('department', options_for_select($department_list), array('multiple' => true));
 * </code>
 *
 * <code>
 *  echo select_tag('url', options_for_select($url_list), array('onChange' => 'Javascript:this.form.submit();'));
 * </code>
 *
 * @param  string field name
 * @param  mixed contains a string of valid <option></option> tags, or an array of options that will be passed to options_for_select
 * @param  array  additional HTML compliant <select> tag parameters
 * @return string <select> tag optionally comprised of <option> tags.
 * @see options_for_select, content_tag
 */
function select_tag($name, $option_tags = null, $options = array())
{
    $options = _convert_options($options);
    $id      = $name;
    if (isset($options['multiple']) && $options['multiple'] && substr($name, -2) !== '[]')
    {
        $name .= '[]';
    }
    if (is_array($option_tags))
    {
        $option_tags = options_for_select($option_tags);
    }

    return content_tag('select', $option_tags, array_merge(array('name' => $name, 'id' => get_id_from_name($id)), $options));
}


/**
 * Returns an XHTML compliant <input> tag with type="text".
 *
 * The input_tag helper generates your basic XHTML <input> tag and can utilize any standard <input> tag parameters
 * passed in the optional <i>$options</i> parameter.
 *
 * <b>Examples:</b>
 * <code>
 *  echo input_tag('name');
 * </code>
 *
 * <code>
 *  echo input_tag('amount', $sf_params->get('amount'), array('size' => 8, 'maxlength' => 8));
 * </code>
 *
 * @param  string field name
 * @param  string selected field value
 * @param  array  additional HTML compliant <input> tag parameters
 * @return string XHTML compliant <input> tag with type="text"
 */
function input_tag($name, $value = null, $options = array())
{
    return tag('input', array_merge(array('type' => 'text', 'name' => $name, 'id' => get_id_from_name($name, $value), 'value' => $value), _convert_options($options)));
}

/**
 * Returns an XHTML compliant <input> tag with type="hidden".
 *
 * Similar to the input_tag helper, the input_hidden_tag helper generates an XHTML <input> tag and can utilize
 * any standard <input> tag parameters passed in the optional <i>$options</i> parameter.  The only difference is
 * that it creates the tag with type="hidden", meaning that is not visible on the page.
 *
 * <b>Examples:</b>
 * <code>
 *  echo input_hidden_tag('id', $id);
 * </code>
 *
 * @param  string field name
 * @param  string populated field value
 * @param  array  additional HTML compliant <input> tag parameters
 * @return string XHTML compliant <input> tag with type="hidden"
 */
function input_hidden_tag($name, $value = null, $options = array())
{
    $options = _parse_attributes($options);

    $options['type'] = 'hidden';
    return input_tag($name, $value, $options);
}

/**
 * Returns an XHTML compliant <input> tag with type="file".
 *
 * Similar to the input_tag helper, the input_hidden_tag helper generates your basic XHTML <input> tag and can utilize
 * any standard <input> tag parameters passed in the optional <i>$options</i> parameter.  The only difference is that it
 * creates the tag with type="file", meaning that next to the field will be a "browse" (or similar) button.
 * This gives the user the ability to choose a file from there computer to upload to the web server.  Remember, if you
 * plan to upload files to your website, be sure to set the <i>multipart</i> option form_tag helper function to true
 * or your files will not be properly uploaded to the web server.
 *
 * <b>Examples:</b>
 * <code>
 *  echo input_file_tag('filename', array('size' => 30));
 * </code>
 *
 * @param  string field name
 * @param  array  additional HTML compliant <input> tag parameters
 * @return string XHTML compliant <input> tag with type="file"
 * @see input_tag, form_tag
 */
function input_file_tag($name, $options = array())
{
    $options = _parse_attributes($options);

    $options['type'] = 'file';
    return input_tag($name, null, $options);
}

/**
 * Returns an XHTML compliant <input> tag with type="password".
 *
 * Similar to the input_tag helper, the input_hidden_tag helper generates your basic XHTML <input> tag and can utilize
 * any standard <input> tag parameters passed in the optional <i>$options</i> parameter.  The only difference is that it
 * creates the tag with type="password", meaning that the text entered into this field will not be visible to the end user.
 * In most cases it is replaced by  * * * * * * * *.  Even though this text is not readable, it is recommended that you do not
 * populate the optional <i>$value</i> option with a plain-text password or any other sensitive information, as this is a
 * potential security risk.
 *
 * <b>Examples:</b>
 * <code>
 *  echo input_password_tag('password');
 *  echo input_password_tag('password_confirm');
 * </code>
 *
 * @param  string field name
 * @param  string populated field value
 * @param  array  additional HTML compliant <input> tag parameters
 * @return string XHTML compliant <input> tag with type="password"
 * @see input_tag
 */
function input_password_tag($name = 'password', $value = null, $options = array())
{
    $options = _parse_attributes($options);

    $options['type'] = 'password';
    return input_tag($name, $value, $options);
}

/**
 * Returns an XHTML compliant <input> tag with type="checkbox".
 *
 * When creating multiple checkboxes with the same name, be sure to use an array for the
 * <i>$name</i> parameter (i.e. 'name[]').  The checkbox_tag is smart enough to create unique ID's
 * based on the <i>$value</i> parameter like so:
 *
 * <samp>
 *  <input type="checkbox" name="status[]" id="status_3" value="3" />
 *  <input type="checkbox" name="status[]" id="status_4" value="4" />
 * </samp>
 *
 * <b>Examples:</b>
 * <code>
 *  echo checkbox_tag('newsletter', 1, $sf_params->get('newsletter'));
 * </code>
 *
 * <code>
 *  echo checkbox_tag('option_a', 'yes', true, array('class' => 'style_a'));
 * </code>
 *
 * <code>
 *  // one request variable with an array of checkbox values
 *  echo checkbox_tag('choice[]', 1);
 *  echo checkbox_tag('choice[]', 2);
 *  echo checkbox_tag('choice[]', 3);
 *  echo checkbox_tag('choice[]', 4);
 * </code>
 *
 * <code>
 *  // assuming you have Prototype.js enabled, you could do this
 *  echo checkbox_tag('show_tos', 1, false, array('onclick' => "Element.toggle('tos'); return false;"));
 * </code>
 *
 * @param  string field name
 * @param  string checkbox value (if checked)
 * @param  bool   is the checkbox checked? (1 or 0)
 * @param  array  additional HTML compliant <input> tag parameters
 * @return string XHTML compliant <input> tag with type="checkbox"
 */
function checkbox_tag($name, $value = '1', $checked = false, $options = array())
{
    $html_options = array_merge(array('type' => 'checkbox', 'name' => $name, 'id' => get_id_from_name($name, $value), 'value' => $value), _convert_options($options));

    if ($checked)
    {
        $html_options['checked'] = 'checked';
    }

    return tag('input', $html_options);
}

/**
 * Returns an XHTML compliant <input> tag with type="radio".
 *
 * <b>Examples:</b>
 * <code>
 *  echo ' Yes '.radiobutton_tag('newsletter', 1);
 *  echo ' No '.radiobutton_tag('newsletter', 0);
 * </code>
 *
 * @param  string field name
 * @param  string radio button value (if selected)
 * @param  bool   is the radio button selected? (1 or 0)
 * @param  array  additional HTML compliant <input> tag parameters
 * @return string XHTML compliant <input> tag with type="radio"
 */
function radiobutton_tag($name, $value, $checked = false, $options = array())
{
    $html_options = array_merge(array('type' => 'radio', 'name' => $name, 'id' => get_id_from_name($name.'[]', $value), 'value' => $value), _convert_options($options));

    if ($checked)
    {
        $html_options['checked'] = 'checked';
    }

    return tag('input', $html_options);
}

/**
 * Returns two XHTML compliant <input> tags to be used as a free-text date fields for a date range.
 *
 * Built on the input_date_tag, the input_date_range_tag combines two input tags that allow the user
 * to specify a from and to date.
 * You can easily implement a JavaScript calendar by enabling the 'rich' option in the
 * <i>$options</i> parameter.  This includes a button next to the field that when clicked,
 * will open an inline JavaScript calendar.  When a date is selected, it will automatically
 * populate the <input> tag with the proper date, formatted to the user's culture setting.
 *
 * <b>Note:</b> The <i>$name</i> parameter will automatically converted to array names.
 * For example, a <i>$name</i> of "date" becomes date[from] and date[to]
 *
 * <b>Options:</b>
 * - rich - If set to true, includes an inline JavaScript calendar can auto-populate the date field with the chosen date
 * - before - string to be displayed before the input_date_range_tag
 * - middle - string to be displayed between the from and to tags
 * - after - string to be displayed after the input_date_range_tag
 *
 * <b>Examples:</b>
 * <code>
 *  $date = array('from' => '2006-05-15', 'to' => '2006-06-15');
 *  echo input_date_range_tag('date', $date, array('rich' => true));
 * </code>
 *
 * <code>
 *  echo input_date_range_tag('date', null, array('middle' => ' through ', 'rich' => true));
 * </code>
 *
 * @param  string field name
 * @param  array  dates: $value['from'] and $value['to']
 * @param  array  additional HTML compliant <input> tag parameters
 * @return string XHTML compliant <input> tag with optional JS calendar integration
 * @see input_date_tag
 */
function input_date_range_tag($name, $value, $options = array())
{
    $options = _parse_attributes($options);

    $before = _get_option($options, 'before', '');
    $middle = _get_option($options, 'middle', '');
    $after  = _get_option($options, 'after', '');

    return $before.
    input_date_tag($name.'[from]', isset($value['from']) ? $value['from'] : null, $options).
    $middle.
    input_date_tag($name.'[to]',   isset($value['to'])   ? $value['to']   : null, $options).
    $after;
}


/**
 * Returns an XHTML compliant <input> tag with type="submit".
 *
 * By default, this helper creates a submit tag with a name of <em>commit</em> to avoid
 * conflicts with other parts of the framework.  It is recommended that you do not use the name
 * "submit" for submit tags unless absolutely necessary. Also, the default <i>$value</i> parameter
 * (title of the button) is set to "Save changes", which can be easily overwritten by passing a
 * <i>$value</i> parameter.
 *
 * <b>Examples:</b>
 * <code>
 *  echo submit_tag();
 * </code>
 *
 * <code>
 *  echo submit_tag('Update Record');
 * </code>
 *
 * @param  string field value (title of submit button)
 * @param  array  additional HTML compliant <input> tag parameters
 * @return string XHTML compliant <input> tag with type="submit"
 */
function submit_tag($value = 'Save changes', $options = array())
{
    return tag('input', array_merge(array('type' => 'submit', 'name' => 'commit', 'value' => $value), _convert_options_to_javascript(_convert_options($options))));
}

/**
 * Returns an XHTML compliant <input> tag with type="reset".
 *
 * By default, this helper creates a submit tag with a name of <em>reset</em>.  Also, the default
 * <i>$value</i> parameter (title of the button) is set to "Reset" which can be easily overwritten
 * by passing a <i>$value</i> parameter.
 *
 * <b>Examples:</b>
 * <code>
 *  echo reset_tag();
 * </code>
 *
 * <code>
 *  echo reset_tag('Start Over');
 * </code>
 *
 * @param  string field value (title of reset button)
 * @param  array  additional HTML compliant <input> tag parameters
 * @return string XHTML compliant <input> tag with type="reset"
 */
function reset_tag($value = 'Reset', $options = array())
{
    return tag('input', array_merge(array('type' => 'reset', 'name' => 'reset', 'value' => $value), _convert_options($options)));
}

/**
 * Returns an XHTML compliant <input> tag with type="image".
 *
 * The submit_image_tag is very similar to the submit_tag, the only difference being that it uses an image
 * for the submit button instead of the browser-generated default button. The image is defined by the
 * <i>$source</i> parameter and must be a valid image, either local or remote (URL). By default, this
 * helper creates a submit tag with a name of <em>commit</em> to avoid conflicts with other parts of the
 * framework.  It is recommended that you do not use the name "submit" for submit tags unless absolutely necessary.
 *
 * <b>Examples:</b>
 * <code>
 *  // Assuming your image is in the /web/images/ directory
 *  echo submit_image_tag('my_submit_button.gif');
 * </code>
 *
 * <code>
 *  echo submit_image_tag('http://mydomain.com/my_submit_button.gif');
 * </code>
 *
 * @param  string path to image file
 * @param  array  additional HTML compliant <input> tag parameters
 * @return string XHTML compliant <input> tag with type="image"
 */
function submit_image_tag($source, $options = array())
{
    if (!isset($options['alt']))
    {
        $path_pos = strrpos($source, '/');
        $dot_pos = strrpos($source, '.');
        $begin = $path_pos ? $path_pos + 1 : 0;
        $nb_str = ($dot_pos ? $dot_pos : strlen($source)) - $begin;
        $options['alt'] = ucfirst(substr($source, $begin, $nb_str));
    }

    return tag('input', array_merge(array('type' => 'image', 'name' => 'commit', 'src' => image_path($source)), _convert_options_to_javascript(_convert_options($options))));
}

/**
 * Returns a <label> tag with <i>$label</i> for the specified <i>$id</i> parameter.
 *
 * @param  string id
 * @param  string label or title
 * @param  array  additional HTML compliant <label> tag parameters
 * @return string <label> tag with <i>$label</i> for the specified <i>$id</i> parameter.
 */
function label_for($id, $label, $options = array())
{
    $options = _parse_attributes($options);

    return content_tag('label', $label, array_merge(array('for' => get_id_from_name($id, null)), $options));
}

/**
 * Returns a formatted ID based on the <i>$name</i> parameter and optionally the <i>$value</i> parameter.
 *
 * This function determines the proper form field ID name based on the parameters. If a form field has an
 * array value as a name we need to convert them to proper and unique IDs like so:
 * <samp>
 *  name[] => name (if value == null)
 *  name[] => name_value (if value != null)
 *  name[bob] => name_bob
 *  name[item][total] => name_item_total
 * </samp>
 *
 * <b>Examples:</b>
 * <code>
 *  echo get_id_from_name('status[]', '1');
 * </code>
 *
 * @param  string field name
 * @param  string field value
 * @return string <select> tag populated with all the languages in the world.
 */
function get_id_from_name($name, $value = null)
{
    // check to see if we have an array variable for a field name
    if (strstr($name, '['))
    {
        $name = str_replace(array('[]', '][', '[', ']'), array((($value != null) ? '_'.$value : ''), '_', '_', ''), $name);
    }

    return $name;
}


/**
 * Converts specific <i>$options</i> to their correct HTML format
 *
 * @param  array options
 * @return array returns properly formatted options
 */
function _convert_options($options)
{
    $options = _parse_attributes($options);

    foreach (array('disabled', 'readonly', 'multiple') as $attribute)
    {
        if (array_key_exists($attribute, $options))
        {
            if ($options[$attribute])
            {
                $options[$attribute] = $attribute;
            }
            else
            {
                unset($options[$attribute]);
            }
        }
    }

    return $options;
}

function _convert_include_custom_for_select($options, &$select_options)
{
    if (_get_option($options, 'include_blank'))
    {
        $select_options[''] = '';
    }
    else if ($include_custom = _get_option($options, 'include_custom'))
    {
        $select_options[''] = $include_custom;
    }
}


function object_input_date_tag($object, $method, $options = array(), $default_value = null)
{
    $options = _parse_attributes($options);

    $value = _get_object_value($object, $method, $default_value, $param = 'Y-m-d G:i');

    return input_date_tag(_convert_method_to_name($method, $options), $value, $options);
}


function textarea_tag($name, $content = null, $options = array())
{
    $options = _parse_attributes($options);

    if ($size = _get_option($options, 'size'))
    {
        list($options['cols'], $options['rows']) = explode('x', $size, 2);
    }

    return content_tag('textarea', escape_once((is_object($content)) ? $content->__toString() : $content), array_merge(array('name' => $name, 'id' => get_id_from_name(_get_option($options, 'id', $name), null)), _convert_options($options)));
}
/**
 * Returns a textarea html tag.
 *
 * @param object An object.
 * @param string An object column.
 * @param array Textarea options.
 * @param bool Textarea default value.
 *
 * @return string An html string which represents a textarea tag.
 *
 */
function object_textarea_tag($object, $method, $options = array(), $default_value = null)
{
    $options = _parse_attributes($options);

    $value = _get_object_value($object, $method, $default_value);

    return textarea_tag(_convert_method_to_name($method, $options), $value, $options);
}

/**
 * Accepts a container of objects, the method name to use for the value,
 * and the method name to use for the display.
 * It returns a string of option tags.
 *
 * NOTE: Only the option tags are returned, you have to wrap this call in a regular HTML select tag.
 */
function objects_for_select($options = array(), $value_method, $text_method = null, $selected = null, $html_options = array())
{
    $select_options = array();
    foreach ($options as $option)
    {
        // text method exists?
        if ($text_method && !is_callable(array($option, $text_method)))
        {
            $error = sprintf('Method "%s" doesn\'t exist for object of class "%s"', $text_method, _get_class_decorated($option));
            throw new Exception($error);
        }

        // value method exists?
        if (!is_callable(array($option, $value_method)))
        {
            $error = sprintf('Method "%s" doesn\'t exist for object of class "%s"', $value_method, _get_class_decorated($option));
            throw new Exception($error);
        }

        $value = $option->$value_method();
        $key = ($text_method != null) ? $option->$text_method() : $value;

        $select_options[$value] = $key;
    }

    return options_for_select($select_options, $selected, $html_options);
}

function _get_options_from_objects($objects, $text_method = null)
{
    $select_options = array();

    if ($objects)
    {
        // construct select option list
        $first = true;
        foreach ($objects as $tmp_object)
        {
            if ($first)
            {
                // multi primary keys handling
                $multi_primary_keys = is_array($tmp_object->getPrimaryKey()) ? true : false;

                // which method to call?
                $methodToCall = '';
                foreach (array($text_method, '__toString', 'toString', 'getPrimaryKey') as $method)
                {
                    if (is_callable(array($tmp_object, $method)))
                    {
                        $methodToCall = $method;
                        break;
                    }
                }

                $first = false;
            }

            $key   = $multi_primary_keys ? implode('/', $tmp_object->getPrimaryKey()) : $tmp_object->getPrimaryKey();
            $value = $tmp_object->$methodToCall();

            $select_options[$key] = $value;
        }
    }

    return $select_options;
}

?>