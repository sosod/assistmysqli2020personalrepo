<?php
class OtherScore extends Model
{

    static $table = 'other_score';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active)
        {
            $option_sql = ' AND status <> 0';
        }
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

    function getOtherScore($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id");
        return $response;
    }

    function getList($active = false)
    {
        $scores = $this->getAll($active);
        $list           = array();
        foreach($scores as $index => $val)
        {
            $list[$val['id']] = $val['definition']." - ".$val['score'];
        }
        return $list;
    }

}

?>