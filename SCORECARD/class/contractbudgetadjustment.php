<?php
/**
	This file manages , creates the actions
	* @author 	: admire
	
**/
class ContractBudgetAdjustment extends Model
{
    /**
    @var int
     **/
    protected $id;
    /**
    @var int
     **/
    protected $contract_id;
    /**
    @var char
     **/
    protected $description;
    /**
    @var int
     **/
    protected $adjustment_date;
    /**
    @var int
     **/
    protected $reason_for_adjustment;
    /**
    @var date
     **/
    protected $attachments;
    /**
    @var date
     **/
    protected $insert_user;
    /**
    @var int
     **/
    protected $created_at;
    /**
    @var char
     **/
    protected $updated_at;
    /**
    @var date
     **/
    protected $status;

    const ACTIVE          = 1;

    const DELETED         = 2;

    const REFTAG            = 'CA';

    static $table           = 'contract_budget_adjustment';

	function __construct()
	{
		parent::__construct();
	}
	
	function getList()
	{
        $sql = "SELECT * FROM #_".static::$table." WHERE 1  ";
        $results = $this->db->get($sql);
        $list    = array();
        if($results)
        {
            foreach($results as $index => $action)
            {
               $list[$action['id']] = $action['name'];
            }
        }
       return $list;
	}
     public function isValid($contract_adjustment)
     {
        $errors = array();
        return TRUE;
     }


	function prepareSql($options = array())
	{
        $sql_str = '';
        if(isset($options['financial_year']) && !empty($options['financial_year']))
        {
            $sql_str .=  " AND C.financial_year_id = ".$options['financial_year']." ";
        }
        if(isset($options['contract_id']) && !empty($options['contract_id']))
        {
            $sql_str .=  " AND CA.contract_id = ".$options['contract_id']." ";
        }

        $sql_str .= " AND CA.status & ".self::DELETED." <> ".self::DELETED." ";
        if(isset($options['limit']) && !empty($options['limit']))
        {
            $sql_str .=  " LIMIT ".(int)$options['start'].",".(int)$options['limit'];
        }
      return $sql_str;
	}

    function getContactAdjustmentDetail($id)
    {
        $results = $this->db->getRow("SELECT CA.*, date_format(CA.adjustment_date, '%d-%b-%Y') AS adjustment_date, date_format(CA.created_at, '%d-%b-%Y') AS created_at, date_format(CA.updated_at, '%d-%b-%Y') AS updated_at,
                                     C.id AS contract_id, C.name AS contract
                                     FROM #_".static::$table." CA
                                     INNER JOIN #_contract C ON C.id = CA.contract_id
                                     WHERE CA.id = '".$id."' ");
        return $results;
    }

	function getContractAdjustments($option_sql = '')
	{
        $results = $this->db->get("SELECT  CA.*, date_format(CA.adjustment_date, '%d-%b-%Y') AS adjustment_date, date_format(CA.created_at, '%d-%b-%Y') AS created_at, date_format(CA.updated_at, '%d-%b-%Y') AS updated_at,
                                   C.id AS contract_id, C.name AS contract
                                   FROM #_".static::$table." CA
                                   INNER JOIN #_contract C ON C.id = CA.contract_id
                                   WHERE 1 ".$option_sql);
        return $results;
	}

    function getTotalContractAdjustments($option_sql = '')
    {
        $result = $this->db->getRow("SELECT COUNT(*) AS total
                                     FROM #_".static::$table." CA
                                     INNER JOIN #_contract C ON C.id = CA.contract_id
                                     WHERE 1 ".$option_sql);
        return $result['total'];
    }

    public function prepareContractAdjustment($contract_adjustments, $total, $options = array())
    {
        $adjustment_data['supplier_budgets']  = array();
        $adjustment_data['budget_total']      = 0;
        $adjustment_data['adjustments']       = array();
        $adjustment_data['total_adjustments'] = $total;
        $adjustment_data['suppliers']         = array();
        $contact_supplier_budgets_obj        = new ContractSupplierBudget();
        $contractBudgetAdjustmentDocumentObj  = new ContractBudgetAdjustmentDocument();

        foreach($contract_adjustments as $index => $adjustment)
        {
            $supplier_budget_summary                = $contact_supplier_budgets_obj->getContractSupplierBudgetsSummary($adjustment['id']);
            $adjustment_data['suppliers']           = $supplier_budget_summary['suppliers'];
            $adjustment_data['supplier_budgets'][$adjustment['id']]    = $supplier_budget_summary;
            $adjustment_data['adjustments'][$adjustment['id']]['total'] = ASSIST_HELPER::format_float($supplier_budget_summary['total_adjustment_budget']);
            $adjustment_data['adjustments'][$adjustment['id']]['adjustment_date'] = $adjustment['adjustment_date'];
            $adjustment_data['adjustments'][$adjustment['id']]['reason_for_adjustment'] = $adjustment['reason_for_adjustment'];
            $adjustment_data['adjustments'][$adjustment['id']]['name'] = $adjustment['name'];

            $documents                                           = $contractBudgetAdjustmentDocumentObj->getAll($adjustment['id']);
            $adjustment_data['adjustments'][$adjustment['id']]['attachments'] = Attachment::displayDownloadableLinks($documents, 'contract');;
            $adjustment_data['adjustments'][$adjustment['id']]['editable_attachments'] = Attachment::displayEditableAttachments($documents, 'contract');;

            $adjustment_data['adjustments'][$adjustment['id']]['status'] = $adjustment['status'];
            $adjustment_data['budget_total']                              += $supplier_budget_summary['total_adjustment_budget'];
        }
        foreach($adjustment_data['supplier_budgets'] as $adjustment_id => $supplier_budgets)
        {
            foreach($supplier_budgets['supplier_budget'] as $supplier_id => $budget)
            {
                if(!isset($adjustment_data['supplier_total'][$supplier_id]))
                {
                    $adjustment_data['supplier_total'][$supplier_id] = $budget;
                } else {
                    $adjustment_data['supplier_total'][$supplier_id] += $budget;
                }
            }
        }
        $total_budget = 0;
        foreach($adjustment_data['supplier_total'] as $supplier_id => $budget)
        {
            $adjustment_data['supplier_total'][$supplier_id] = ASSIST_HELPER::format_float($budget);
            $total_budget += $budget;
        }
       $adjustment_data['budget_total'] =  ASSIST_HELPER::format_float($total_budget);
       $adjustment_data['total_suppliers'] = count($adjustment_data['suppliers']);
       return $adjustment_data;
    }




}
?>