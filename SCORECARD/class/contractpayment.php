<?php
/**
This file manages , creates the actions
 * @author 	: admire

 **/
class ContractPayment extends Model
{
    /**
    @var int
     **/
    protected $id;
    /**
    @var int
     **/
    protected $contract_id;
    /**
    @var char
     **/
    protected $name;
    /**
    @var int
     **/
    protected $payment_date;
    /**
    @var int
     **/
    protected $certificate_number;
    /**
    @var date
     **/
    protected $attachments;
    /**
    @var date
     **/
    protected $insert_user;
    /**
    @var int
     **/
    protected $created_at;
    /**
    @var char
     **/
    protected $updated_at;
    /**
    @var date
     **/
    protected $status;

    const ACTIVE          = 1;

    const DELETED         = 2;

    const REFTAG          = 'CP';

    static $table           = 'contract_payment';

    function __construct()
    {
        parent::__construct();
    }

    function getList()
    {
        $sql = "SELECT * FROM #_".static::$table." WHERE 1  ";
        $results = $this->db->get($sql);
        $list    = array();
        if($results)
        {
            foreach($results as $index => $action)
            {
                $list[$action['id']] = $action['name'];
            }
        }
        return $list;
    }
    public function isValid($contract_adjustment)
    {
        $errors = array();
        return TRUE;
    }


    function prepareSql($options = array())
    {
        $sql_str = '';
        if(isset($options['financial_year']) && !empty($options['financial_year']))
        {
            $sql_str .=  " AND C.financial_year_id = ".$options['financial_year']." ";
        }
        if(isset($options['contract_id']) && !empty($options['contract_id']))
        {
            $sql_str .=  " AND CP.contract_id = ".$options['contract_id']." ";
        }

        $sql_str .= " AND CP.status & ".self::DELETED." <> ".self::DELETED." ";
        if(isset($options['limit']) && !empty($options['limit']))
        {
            $sql_str .=  " LIMIT ".(int)$options['start'].",".(int)$options['limit'];
        }
        return $sql_str;
    }

    function getContractPaymentDetail($id)
    {
        $results = $this->db->getRow("SELECT CP.*, date_format(CP.payment_date , '%d-%b-%Y') AS payment_date, date_format(CP.created_at, '%d-%b-%Y') AS created_at, date_format(CP.updated_at, '%d-%b-%Y') AS updated_at,
                                      C.id AS contract_id, C.name AS contract
                                      FROM #_".static::$table." CP
                                      INNER JOIN #_contract C ON C.id = CP.contract_id
                                      WHERE CP.id = '".$id."' ");
        return $results;
    }

    function getContractPayments($option_sql = '')
    {
        $results = $this->db->get("SELECT  CP.*, date_format(CP.payment_date , '%d-%b-%Y') AS payment_date, date_format(CP.created_at, '%d-%b-%Y') AS created_at, date_format(CP.updated_at, '%d-%b-%Y') AS updated_at,
                                   C.id AS contract_id, C.name AS contract
                                   FROM #_".static::$table." CP
                                   INNER JOIN #_contract C ON C.id = CP.contract_id
                                   WHERE 1 ".$option_sql);
        return $results;
    }

    function getTotalContractPayments($option_sql = '')
    {
        $result = $this->db->getRow("SELECT COUNT(*) AS total
                                     FROM #_".static::$table." CP
                                     INNER JOIN #_contract C ON C.id = CP.contract_id
                                     WHERE 1 ".$option_sql);
        return $result['total'];
    }

    public function prepareContractPayment($contract_payments, $total, $options = array())
    {

        $payment_data['supplier_payments']  = array();
        $payment_data['payment_total']      = 0;
        $payment_data['payments']       = array();
        $payment_data['total_payments'] = $total;
        $payment_data['suppliers']         = array();
        $contactSupplierPaymentObj        = new ContractSupplierPayment();
        $contractPaymentDocumentObj       = new ContractPaymentDocument();
        foreach($contract_payments as $index => $payment)
        {
            $supplier_payment_summary        = $contactSupplierPaymentObj->getContractSupplierPaymentSummary($payment['id']);
            if(!empty($supplier_payment_summary['suppliers']))
            {
                $payment_data['suppliers']       = $supplier_payment_summary['suppliers'];
                $payment_data['supplier_payments'][$payment['id']]    = $supplier_payment_summary;
                $payment_data['payments'][$payment['id']]['total'] = ASSIST_HELPER::format_float($supplier_payment_summary['total_payment']);
                $payment_data['payments'][$payment['id']]['payment_date'] = $payment['payment_date'];
                $payment_data['payments'][$payment['id']]['certificate_number'] = $payment['certificate_number'];
                $payment_data['payments'][$payment['id']]['name'] = $payment['name'];

                $documents                                                        = $contractPaymentDocumentObj->getAll($payment['id']);
                $payment_data['payments'][$payment['id']]['attachments']          = Attachment::displayDownloadableLinks($documents, 'contract');;
                $payment_data['payments'][$payment['id']]['editable_attachments'] = Attachment::displayEditableAttachments($documents, 'contract');;

                $payment_data['payments'][$payment['id']]['status']               = $payment['status'];
                $payment_data['payment_total']                          += ASSIST_HELPER::format_float($supplier_payment_summary['total_payment']);
            }
        }

        foreach($payment_data['supplier_payments'] as $payment_id => $supplier_payment)
        {
            foreach($supplier_payment['supplier_payment'] as $supplier_id => $amount)
            {
                if(!isset($payment_data['supplier_total'][$supplier_id]))
                {
                    $payment_data['supplier_total'][$supplier_id] = $amount;
                } else {
                    $payment_data['supplier_total'][$supplier_id] += $amount;
                }
            }
        }
        $total_budget = 0;
        if(isset($payment_data['supplier_total']))
        {
            foreach($payment_data['supplier_total'] as $supplier_id => $supplier_total)
            {
                $payment_data['supplier_total'][$supplier_id] = ASSIST_HELPER::format_float($supplier_total);
                $total_budget += $supplier_total;
            }
        }
        $payment_data['payment_total']= ASSIST_HELPER::format_float($total_budget);
        $payment_data['total_suppliers'] = count($payment_data['suppliers']);

        return $payment_data;
    }
}
?>