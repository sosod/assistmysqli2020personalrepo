<?php
class ContractDocument extends Model
{

    static $table = 'contract_document';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($contract_id = null)
    {
        $option_sql  = '';
        if($contract_id)
        {
            $option_sql = " AND contract_id = ".$contract_id." ";
        }
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getStatus($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
        $contract_documents = $this->getAll($active);
        $list              = array();
        foreach($contract_documents as $index => $val)
        {
            $list[$val['contract_id']] = $val['filename']." ".$val['description'];
        }
        return $list;
    }

}

?>