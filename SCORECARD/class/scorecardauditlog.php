<?php
class ScoreCardAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $scoreCardObj     = new ScoreCard();
        $object_data     = $scoreCardObj->getScoreCardDetail($where['id']);
        $posted_contract = array();
        $changes         = array();

        if(isset($_REQUEST['score_card']))
        {
            parse_str($_REQUEST['score_card'], $posted_contract);
            $posted_deliverable_statuses = array();
            $posted_supplier_budgets     = array();
            $posted_documents            = array();
            $posted_contract_data        = $posted_contract['score_card'];
            if(isset($posted_contract_data['documents']))
            {
                $posted_documents   = $posted_contract_data['documents'];
                $attachment_changes = $this->processDocumentChange($posted_documents, $where['id']);
                if(!empty($attachment_changes))
                {
                   $changes['attachment'] = $attachment_changes;
                }
                unset($posted_contract_data['documents']);
            }
        }
        unset($object_data['created_at']);
        if(!empty($object_data))
        {
            foreach($object_data as $field => $val)
            {
                if(isset($post_data[$field]) && $val != $post_data[$field])
                {
                    if($field == 'status')
                    {

                        if( ($post_data[$field] & ScoreCard::AUTHORIZED) == ScoreCard::AUTHORIZED ){
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was authorized";
                        } elseif( ($post_data[$field] & ScoreCard::CLOSED) == ScoreCard::CLOSED ){
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deactivated";
                        } elseif( ($val & ScoreCard::CLOSED) == ScoreCard::CLOSED && ( ($post_data[$field] & ScoreCard::CLOSED) != ScoreCard::CLOSED ) ){
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was activated";
                        } elseif($post_data[$field] == 2 && (($val & 1) == 1)) {
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deleted";
                        } else if( ($post_data[$field] == 0 && $val == 1)  || ( ( ( ($post_data[$field] & ScoreCard::CONFIRMED) == ScoreCard::CONFIRMED) &&  (($val & ScoreCard::ACTIVATED) == ScoreCard::ACTIVATED) ) )) {
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deactivated";
                        } else if($post_data[$field] == 1 && $val == 0) {
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was activated";
                        } elseif( (($post_data[$field] & ScoreCard::CONFIRMED) == ScoreCard::CONFIRMED) &&  (($val & ScoreCard::ACTIVE) == ScoreCard::ACTIVE)) {
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was confirmed";
                        } elseif( (($post_data[$field] & ScoreCard::ACTIVATED) == ScoreCard::ACTIVATED) &&  (($val & ScoreCard::CONFIRMED) == ScoreCard::CONFIRMED)) {
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was activated";
                        }
                    } elseif($field == 'status_id') {
                        $statusObj     = new ScoreCardStatus();
                        $statuses       = $statusObj->getList();
                        $from        = '';
                        $to          = '';
                        foreach($statuses  as $status_id => $status)
                        {
                            if($status_id == $val)  $from = $status;
                            if($status_id == $post_data[$field])  $to = $status;
                        }
                        if($from != $to)  $changes['currentstatus'] = $to;
                        if($from != $to)  $changes[$field] = array('from' => $from, 'to' => $to);
                    } elseif($field == 'owner_id'){

                    } else if($field == 'financial_year_id') {
                        $financialYearObj = new FinancialYear();
                        $financial_years  = $financialYearObj->getList();
                        $from             = '';
                        $to               = '';
                        foreach($financial_years as $financial_year_id => $financial_year)
                        {
                            if($financial_year_id == $val)  $from = $financial_year;
                            if($financial_year_id == $post_data[$field])  $to = $financial_year;
                        }
                        if($from != $to)  $changes[$field] = array('from' => $from, 'to' => $to);
                    } else if($field == 'type_id') {
                        $typesObj         = new ScoreCardType();
                        $score_card_types  = $typesObj->getList();
                        $from             = '';
                        $to               = '';
                        foreach($score_card_types as $score_card_type_id => $score_card_type)
                        {
                            if($score_card_type_id == $val)  $from = $score_card_type;
                            if($score_card_type_id == $post_data[$field])  $to = $score_card_type;
                        }
                        if($from != $to)  $changes[$field] = array('from' => $from, 'to' => $to);
                    } else if($field == 'category_id') {
                        $categoryObj         = new ScoreCardCategory();
                        $score_card_categories = $categoryObj->getList();
                        $from            = '';
                        $to              = '';
                        foreach($score_card_categories as $score_card_category_id => $score_card_category)
                        {
                            if($score_card_category_id == $val)  $from = $score_card_category;
                            if($score_card_category_id == $post_data[$field])  $to = $score_card_category;
                        }
                        if($from != $to) $changes[$field] = array('from' => $from, 'to' => $to);
                    } elseif(in_array($field, array('manager_id', 'authorisor_id'))){
                        $userObj     = new User();
                        $users       = $userObj->getList();
                        $from        = '';
                        $to          = '';
                        foreach($users as $user_id => $user)
                        {
                            if($user_id == $val) $from = $user;
                            if($user_id == $post_data[$field]) $to = $user;
                        }
                        if($from != $to) $changes[$field] = array('from' => $from, 'to' => $to);
                    } else {
                        $from = (empty($val) ? '-' : $val);
                        $to   = (empty($post_data[$field]) ? '-' : $post_data[$field]);
                        if($from != $to) $changes[$field] = array('from' => $from, 'to' => $to);
                    }
                }
            }
        }
        if(!empty($changes))
        {
            if(isset($_REQUEST['response']))
            {
                $changes['response']        = $_REQUEST['response'];
                static::$table              = $table_name."_update_logs";
            } else {
                static::$table              = $table_name."_edit_logs";
            }
            if(!isset($changes['currentstatus'])) $changes['currentstatus'] = $object_data['score_card_status'];
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['score_card_id'] = $where['id'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }


    public function processDocumentChange($posted_documents, $score_card_id)
    {
        $documentObj                  = new ScoreCardDocument();
        $score_card_documents           = $documentObj->getAll($score_card_id);
        foreach($score_card_documents as $index => $document)
        {
            if(!isset($posted_documents[$document['filename']]))
            {
               foreach($posted_documents as $index => $doc)
               {
                   $changes[$index] =  'Attachment '.$doc." has been added";
               }
            }
        }
        return $changes;
    }

    public function getScoreCardActivityLog($score_card_id)
    {
        $columnObj   = new ScoreCardColumn();
        $columns     = $columnObj->getHeaderList();

        $score_card_edits = DBConnect::getInstance()->get("SELECT id, score_card_id, changes, created_at, updated_at AS insertdate FROM #_score_card_edit_logs WHERE score_card_id = '".$score_card_id."' ");
        $score_card_edits  = self::logs($score_card_edits, $columns);

        $score_card_updates = DBConnect::getInstance()->get("SELECT id, score_card_id, changes, created_at, updated_at AS insertdate FROM #_score_card_update_logs WHERE score_card_id = '".$score_card_id."' ");
        $score_card_updates  = self::logs($score_card_updates, $columns);

        $logs = array_merge($score_card_updates, $score_card_edits);
        uasort($logs, function($a, $b){
            return ($a['key'] > $b['key'] ? 1 : -1);
        });
        return $logs;
    }

    public function deleteScoreCardSettingsLog($score_card)
    {
        $changes = array();
        $changes['_score_card'] = 'Score card Ref #'.$score_card['id']." has been deleted";
        if(!empty($changes))
        {
            static::$table              = "score_card_edit_logs";
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['score_card_id'] = $score_card['id'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }
}
