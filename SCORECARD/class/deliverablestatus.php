<?php
class DeliverableStatus extends Model
{

    static $table = 'deliverable_status';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active)
        {
            $option_sql = ' AND status <> 0';
        }
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

    function getStatus($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
       $contract_statuses = $this->getAll($active);
       $list              = array();
        foreach($contract_statuses as $index => $val)
        {
            $list[$val['id']] = $list[$val['id']] = ($val['client_terminology'] == ''  ? $val['name'] : $val['client_terminology']);
        }
       return $list;
    }

    function getStatusAssessedList($active)
    {
        $contract_statuses = $this->getAll($active);
        $list              = array();
        foreach($contract_statuses as $index => $val)
        {
            if($val['id'] != 1) $list[$val['id']] = ($val['client_terminology'] == ''  ? $val['name'] : $val['client_terminology']);
        }
        return $list;
    }

}

?>