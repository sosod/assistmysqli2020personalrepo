<?php
class TaskStatus extends Model
{

    static $table = 'task_status';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active)
        {
            $option_sql = ' AND status <> 0';
        }
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

    function getStatus($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
       $task_statuses = $this->getAll($active);
       $list              = array();
        foreach($task_statuses as $index => $val)
        {
            $list[$val['id']] = $val['name'];
        }
       return $list;
    }

}

?>