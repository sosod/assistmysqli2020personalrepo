<?php
class ActionAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $actionObj    = new Action();
        $object_data  = $actionObj->getActionDetail($where['id']); //DBConnect::getInstance()->getRow("SELECT * FROM #_".$table_name." WHERE id = '".$where['id']."' ");
        unset($object_data['created_at']);
        $action_data = array();
        if(isset($_REQUEST['update_data'])) parse_str($_REQUEST['update_data'], $action_data);

        if(isset($action_data['action_document']))
        {
            $posted_documents   = $action_data['action_document'];
            $attachment_changes = $this->processDocumentChange($posted_documents, $where['id']);
            if(!empty($attachment_changes)) $changes['attachment'] = $attachment_changes;
            unset($action_data['action_document']);
        }
        if(isset($action_data['deleted_action_attachment']))
        {
            $posted_deleted_documents   = $action_data['deleted_action_attachment'];
            $deleted_attachment_changes = $this->processDeletedDocumentChange($posted_deleted_documents, $where['id']);
            if(!empty($deleted_attachment_changes))
            {
                $changes['attachment'] = (isset($changes['attachment']) ?  array_merge($changes['attachment'], $deleted_attachment_changes) : $deleted_attachment_changes);
            }
            unset($action_data['deleted_action_attachment']);
        }
        if(isset($_SESSION['action_declined_'.$where['id']]) && $_SESSION['action_declined_'.$where['id']] == true)
        {
            $changes['_action_ref_'] = "Action ref #".$where['id']." declined";
            unset($_SESSION['action_declined_'.$where['id']]);
        }
        if(isset($_SESSION['action_approved_'.$where['id']]) && $_SESSION['action_approved_'.$where['id']] == true)
        {
            $changes['_action_ref_'] = "Action ref #".$where['id']." approved";
            unset($_SESSION['action_approved_'.$where['id']]);
        }
        //debug($_SESSION);
        if(isset($_SESSION['action_unapproved_'.$where['id']]) && $_SESSION['action_unapproved_'.$where['id']] == true)
        {
            $changes['_action_ref_'] = "Action ref #".$where['id']." unapproved";
            unset($_SESSION['action_unapproved_'.$where['id']]);
        }
        //debug($changes);
        //exit();
        if(!empty($object_data))
        {
            foreach($object_data as $field => $val)
            {
                if(isset($post_data[$field]) && $val != $post_data[$field])
                {
                    if($field == 'status')
                    {
                        if( ($post_data['status'] & Action::COMPLETED) == Action::COMPLETED)
                        {
                           $changes[$field] = " Action Ref ".$where['id']." assessment was completed";
                        }
                        if( (($val & Action::COMPLETED) == Action::COMPLETED) &&  (($post_data['status'] & Action::COMPLETED) != Action::COMPLETED) )
                        {
                            $changes[$field] = " Action Ref ".$where['id']." assessment was unconfimed";
                        }

                        if($post_data[$field] == 2 && $val == 1)
                        {
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deleted";
                        } else if($post_data[$field] == 0 && $val == 1) {
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deactivated";
                        } else if($post_data[$field] == 1 && $val == 0) {
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was activated";
                        }
                    } elseif($field == 'status_id'){
                        $statusObj     = new ActionStatus();
                        $statuses       = $statusObj->getList();
                        $from        = '';
                        $to          = '';
                        foreach($statuses  as $status_id => $status)
                        {
                            if($status_id == $val)
                            {
                                $from = $status;
                            }
                            if($status_id == $post_data[$field])
                            {
                                $to = $status;
                            }
                        }
                        if($from != $to)
                        {
                            $changes['currentstatus'] = $to;
                        }
                        $changes[$field] = array('from' => $from, 'to' => $to);
                    } elseif($field == 'owner_id'){
                        $userObj     = new User();
                        $users       = $userObj->getList();
                        $from        = '';
                        $to          = '';
                        foreach($users as $user_id => $user)
                        {
                            if($user_id == $val)
                            {
                                $from = $user;
                            }
                            if($user_id == $post_data[$field])
                            {
                                $to = $user;
                            }
                        }
                        $changes[$field] = array('from' => $from, 'to' => $to);
                    } else if(in_array($field, array('remind_on', 'deadline_date', 'action_on'))) {
                        $from = strtotime($val);
                        $to   = strtotime($post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => date('d-M-Y', $from), 'to' => date('d-M-Y', $to));
                        }
                    } else {
                        $from = (empty($val) ? '-' : $val);
                        $to   = (empty($post_data[$field]) ? '-' : $post_data[$field]);

                        $changes[$field] = array('from' => $from, 'to' => $to);
                    }
                }
            }
        }

        if(!empty($changes))
        {
            if((isset($action_data['response']) || isset($_REQUEST['response'])) && (!empty($action_data['response']) || !empty($_REQUEST['response'])) )
            {
               static::$table              = $table_name."_update_logs";
            } else {
                static::$table              = $table_name."_edit_logs";
            }
            if(!isset($changes['currentstatus']))
            {
                $changes['currentstatus'] = $object_data['action_status'];
            }
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['action_id']   = $where['id'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

    function getActionActivityLog($action_id)
    {
        $columnObj   = new DeliverableColumns();
        $columns     = $columnObj->getHeaderList();

        $action_edits = DBConnect::getInstance()->get("SELECT id, action_id, changes, created_at, updated_at AS insertdate FROM #_action_edit_logs WHERE action_id = '".$action_id."' ");
        $action_edits = self::logs($action_edits, $columns);

        $action_updates = DBConnect::getInstance()->get("SELECT id, action_id, changes, created_at, updated_at AS insertdate FROM #_deliverable_update_logs WHERE action_id = '".$action_id."' ");
        $action_updates  = self::logs($action_updates, $columns);

        $logs = array_merge($action_updates, $action_edits);
        uasort($logs, function($a, $b){
            return ($a['key'] > $b['key'] ? 1 : -1);
        });
        return $logs;
    }

    function processDocumentChange($posted_documents, $action_id)
    {

        $changes = array();
        foreach($posted_documents as $document_key => $document_description)
        {
            $changes[] = " Attachment ".$document_description." has been added";
        }
        return $changes;
    }

    function processDeletedDocumentChange($posted_deleted_document, $action_id)
    {
        $changes = array();
        if(!empty($posted_deleted_document))
        {
            $actionDocumentObj = new ActionDocument();
            foreach($posted_deleted_document as $delete_index => $document_id)
            {
                $document = $actionDocumentObj->getActionDocument($document_id);
                $changes[] = ' Attachment '.$document['description']." has been deleted ";
            }
        }
        return $changes;
    }
}
