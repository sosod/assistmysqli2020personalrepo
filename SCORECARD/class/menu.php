<?php
/**
This file updates and creates menu items
 * @uthor : admire
 **/
class Menu extends Model
{
    /**
    @var int
     **/
    protected $id;
    /**
    @var int
     **/
    protected $parent_id;
    /**
    @var char
     **/
    protected $name;
    /**
    @var char
     **/
    protected $client_name;
    /**
    @var char
     **/
    protected $description;
    /**
    @var char
     **/
    protected $folder;
    /**
    @var int
     **/
    protected $position;
    /**
    @var date
     **/
    protected $insertdate;
    /**
    @var char
     **/
    protected $insertuser;

    static $table = 'menu';

    function __construct()
    {
        parent::__construct();
    }

    public function getSubMenu( $name )
    {
        $response = $this->db->get("SELECT * FROM #_menu WHERE folder = '".$name."' AND parent_id <> 0 ");
        return $response;
    }

    function getParentMenu($folder)
    {
        $response = $this->db->get("SELECT * FROM #_menu WHERE folder = '".$folder."' ");
        return $response;
    }

    function getMenu( $id )
    {
        $response = $this->db->getRow("SELECT * FROM #_menu WHERE id = $id ");
        return $response;
    }


    function saveMenu( $id, $name)
    {
        $data    = $this->getMenu( $id );
        $logsObj = new Logs( array("client_name" => $name ) , $data, "menu");
        $logsObj -> createLog();
        $response = $this -> update( "menu" , array("client_name" => $name ), "id=$id");
        return $response;
    }

    public function getAllMenu()
    {
        $response = $this->db->get("SELECT * FROM #_menu WHERE 1 ");
        return $response;
    }

}
?>