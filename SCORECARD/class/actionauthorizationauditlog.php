<?php
class ActionAuthorizationAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $contractAuthorizationObj = new ActionAuthorization();
        $object_data          = $contractAuthorizationObj->getActionAuthorizationDetail($where['id']);
        $changes              = array();
/*        debug($where);
        debug($post_data);
        debug($object_data);
        echo $table_name;
        exit();*/
        $ref['ref'] = 'Authorization Ref '.$where['id'];
        if(isset($_REQUEST['authorization_data']))
        {
            parse_str($_REQUEST['authorization_data'], $action_authorization);
            if(isset($action_authorization['action_authorization_document']))
            {
                $posted_documents   = $action_authorization['action_authorization_document'];
                $attachment_changes = $this->processDocumentChange($posted_documents, $where['id']);
                if(!empty($attachment_changes))
                {
                    $changes['attachment'] = $attachment_changes;
                }
                unset($action_authorization['action_authorization_document']);
            }
            if(isset($action_authorization['deleted_authorization_document']))
            {
                $posted_deleted_documents   = $action_authorization['deleted_authorization_document'];
                $deleted_attachment_changes = $this->processDeletedDocumentChange($posted_deleted_documents, $where['id']);
                if(!empty($deleted_attachment_changes))
                {
                    $changes['attachment'] = (isset($changes['attachment']) ?  array_merge($changes['attachment'], $deleted_attachment_changes) : $deleted_attachment_changes);
                }
                unset($action_authorization['deleted_authorization_document']);
            }
        }

        if(!empty($object_data))
        {
            foreach($object_data as $field => $current_val)
            {
                if(isset($post_data[$field]) && $current_val != $post_data[$field])
                {
                    if($field == 'date_tested')
                    {
                        $from = strtotime($current_val);
                        $to   = strtotime($post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => date('d-M-Y', $from), 'to' => date('d-M-Y', $to));
                        }
                    } else {
                        $from = (empty($current_val) ? '-' : $current_val);
                        $to   = (empty($post_data[$field]) ? '-' : $post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    }
                }
            }
        }
        if(!empty($changes))
        {
            $changes                    = array_merge($ref, $changes);
            static::$table              = $table_name."_logs";
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['authorization_id']  = $where['id'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

    function processDocumentChange($posted_documents, $authorization_id)
    {

        $changes = array();
        foreach($posted_documents as $document_key => $document_description)
        {
            $changes[] = " Attachment ".$document_description." has been added";
        }
       return $changes;
    }

    function processDeletedDocumentChange($posted_deleted_document, $authorization_id)
    {
        $changes = array();
        if(!empty($posted_deleted_document))
        {
            $contractAuthorizationDocumentObj = new ActionAuthorizationDocument();
            foreach($posted_deleted_document as $delete_index => $document_id)
            {
                $document = $contractAuthorizationDocumentObj->getActionAuthorizationDocument($document_id);
                $changes[] = ' Attachment '.$document['description']." has been deleted ";
            }
        }
        return $changes;
    }


}
