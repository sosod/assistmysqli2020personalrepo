<?php
class SubDirectorate extends Model
{

    static $table = 'dirsub';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active) $option_sql = ' AND status <> 0';
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE active = true ORDER BY dirsort ".$option_sql);
        return $response;
    }

    function getSubDirectorateByDirectorateId($id)
    {
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE subdirid = ".$id." AND active = true ORDER BY subhead DESC, subsort ASC");
        return $response;
    }

    function getList($active = false)
    {
        $contract_statuses = $this->getAll($active);
        $list              = array();
        foreach($contract_statuses as $index => $val)
        {
            $list[$val['id']] = $list[$val['id']] = ($val['client_terminology'] == ''  ? $val['name'] : $val['client_terminology']);
        }
        return $list;
    }

    function getStatusAssessedList($active)
    {
        $contract_statuses = $this->getAll($active);
        $list              = array();
        foreach($contract_statuses as $index => $val)
        {
            if($val['id'] != 1) $list[$val['id']] = ($val['client_terminology'] == ''  ? $val['name'] : $val['client_terminology']);
        }
        return $list;
    }

}

?>