<?php
class DeliverableAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $deliverableObj = new Deliverable();
        $object_data    = $deliverableObj->getDeliverableDetail($where['id']);

        $deliverableColumnObj = new DeliverableColumns();
        $columns               = $deliverableColumnObj->getHeaders();

        unset($object_data['created_at']);
        if(isset($_REQUEST['deliverable_data']))
        {
            parse_str($_REQUEST['deliverable_data'], $deliverable_data);

            if(isset($deliverable_data['deliverable_document']))
            {
                $posted_documents   = $deliverable_data['deliverable_document'];
                $attachment_changes = $this->processDocumentChange($posted_documents, $where['id']);
                if(!empty($attachment_changes))
                {
                    $changes['attachment'] = $attachment_changes;
                }
                unset($deliverable_data['deliverable_document']);
            }
            if(isset($deliverable_data['deleted_deliverable_attachment']))
            {
                $posted_deleted_documents   = $deliverable_data['deleted_deliverable_attachment'];
                $deleted_attachment_changes = $this->processDeletedDocumentChange($posted_deleted_documents, $where['id']);
                if(!empty($deleted_attachment_changes))
                {
                    $changes['attachment'] = (isset($changes['attachment']) ?  array_merge($changes['attachment'], $deleted_attachment_changes) : $deleted_attachment_changes);
                }
                unset($deliverable_data['deleted_deliverable_attachment']);
            }
        }

        if(!empty($object_data))
        {
            foreach($object_data as $field => $val)
            {
                if(isset($post_data[$field]) && $val != $post_data[$field])
                {
                    if($field == 'status')
                    {
                        if($post_data[$field] == 2 && $val == 1)
                        {
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deleted";
                        } else if($post_data[$field] == 0 && $val == 1) {
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deactivated";
                        } else if($post_data[$field] == 1 && $val == 0) {
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was activated";
                        }
                    } elseif($field == 'status_id') {
                        $statusObj     = new DeliverableStatus();
                        $statuses       = $statusObj->getList();
                        $from        = '';
                        $to          = '';
                        foreach($statuses  as $status_id => $status)
                        {
                            if($status_id == $val)
                            {
                                $from = $status;
                            }
                            if($status_id == $post_data[$field])
                            {
                                $to = $status;
                            }
                        }
                        if($from != $to)
                        {
                            $changes['currentstatus'] = $to;
                        }
                        $changes[$field] = array('from' => $from, 'to' => $to);
                    } elseif($field == 'owner_id') {
                        $userObj     = new User();
                        $users       = $userObj->getList();
                        $from        = '';
                        $to          = '';
                        foreach($users as $user_id => $user)
                        {
                            if($user_id == $val)
                            {
                                $from = $user;
                            }
                            if($user_id == $post_data[$field])
                            {
                                $to = $user;
                            }
                        }
                        $changes['deliverable_owner'] = array('from' => $from, 'to' => $to);
                    } else if($field == 'remind_on' || $field == 'deadline_date') {
                        $from = strtotime($val);
                        $to   = strtotime($post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => date('d-M-Y', $from), 'to' => date('d-M-Y', $to));
                        }
                    } elseif($field == 'is_main_deliverable'){

                    } else {
                        $from = (empty($val) ? '-' : $val);
                        $to   = (empty($post_data[$field]) ? '-' : $post_data[$field]);
                        if($from != $to)
                        {
                           $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    }
                }
            }
        }
        if(!empty($changes))
        {
            if(isset($deliverable_data['response']))
            {
                $changes['response']  = $deliverable_data['response'];
                static::$table        = $table_name."_update_logs";
            } else {
                static::$table        = $table_name."_edit_logs";
            }
            if(!isset($changes['currentstatus']))
            {
                $changes['currentstatus'] = $object_data['deliverable_status'];
            }
            $changes['user']               = $_SESSION['tkn'];
            $insert_data['deliverable_id'] = $where['id'];
            $insert_data['insert_user']    = $_SESSION['tid'];
            $insert_data['changes']        = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }


    public function getDeliverableActivityLog($deliverable_id, $update_log = true, $edit_log = true)
    {
        $columnObj   = new DeliverableColumns();
        $columns     = $columnObj->getHeaderList();
        $deliverable_updates = array();
        $deliverable_edits   = array();
        if($edit_log)
        {
            $deliverable_edits = DBConnect::getInstance()->get("SELECT id, deliverable_id, changes, created_at, updated_at AS insertdate FROM #_deliverable_edit_logs WHERE deliverable_id = '".$deliverable_id."' ");
            $deliverable_edits = self::logs($deliverable_edits, $columns);
        }
        if($update_log)
        {
            $deliverable_updates = DBConnect::getInstance()->get("SELECT id, deliverable_id, changes, created_at, updated_at AS insertdate FROM #_deliverable_update_logs WHERE deliverable_id = '".$deliverable_id."' ");
            $deliverable_updates  = self::logs($deliverable_updates, $columns);
        }

        $logs = array_merge($deliverable_updates, $deliverable_edits);
        uasort($logs, function($a, $b){
            return ($a['key'] > $b['key'] ? 1 : -1);
        });
        return $logs;
    }


    function processDocumentChange($posted_documents, $deliverable_id)
    {
        $changes = array();
        foreach($posted_documents as $document_key => $document_description)
        {
            $changes[] = " Attachment ".$document_description." has been added";
        }
        return $changes;
    }

    function processDeletedDocumentChange($posted_deleted_document, $action_id)
    {
        $changes = array();
        if(!empty($posted_deleted_document))
        {
            $deliverableDocumentObj = new DeliverableDocument();
            foreach($posted_deleted_document as $delete_index => $document_id)
            {
                $document = $deliverableDocumentObj->getDeliverableDocument($document_id);
                $changes[] = ' Attachment '.$document['description']." has been deleted ";
            }
        }
        return $changes;
    }

    function displayLog($deliverable_id, $update_log = false, $edit_log = false)
    {
        $logs = self::getDeliverableActivityLog($deliverable_id, $update_log, $edit_log);
        return self::printLog($logs);
    }
}
