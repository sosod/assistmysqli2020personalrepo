<?php
class ContractDeliverableStatus extends Model
{

    static $table = 'contract_deliverable_status';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($contract_id = null)
    {
        $option_sql  = '';
        if($contract_id)
        {
            $option_sql = " AND contract_id = ".$contract_id." ";
        }
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getStatus($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
        $contract_deliverable_statuses = $this->getAll($active);
        $list              = array();
        foreach($contract_deliverable_statuses as $index => $val)
        {
            $list[$val['contract_id']] = $val['deliverable_status_id'];
        }
        return $list;
    }

    public function getAllStatusByContractId($contract_id)
    {
        $contract_deliverable_statues = $this->getAll($contract_id);
        $list                         = array();
        foreach($contract_deliverable_statues as $index => $deliverable_status)
        {
           if($deliverable_status['deliverable_status_id'] != 1) $list[$deliverable_status['deliverable_status_id']] = $deliverable_status['is_active'];
        }
        return $list;
    }

    public function getContractDeliverableStatusesAssessed($contract_id)
    {
        $results = $this->db->get("SELECT DS.name AS status_name, CD.deliverable_status_id, CD.is_active, CD.contract_id FROM #_".static::$table." CD
                                   INNER JOIN #_deliverable_status DS ON DS.id = CD.deliverable_status_id
                                   WHERE CD.contract_id = '".$contract_id."' GROUP BY CD.deliverable_status_id
                                 ");
        $result_str = '';
        foreach($results as $index => $result)
        {
            if($result['deliverable_status_id'] != 1 && $result['is_active'] == 1) $result_str .= $result['status_name']." , ";
        }
        return rtrim($result_str, ', ');
    }

}

?>