<?php
class Supplier extends Model
{

    static $table = 'supplier';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active)
        {
            $option_sql = ' AND S.status <> 0';
        }
        $response = $this->db->get("SELECT S.*, SC.name AS supplier_category FROM #_".static::$table." S
                                    LEFT JOIN #_supplier_category SC ON SC.id = S.category_id
                                    WHERE S.status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

    function getStatus($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
       $suppliers = $this->getAll($active);
       $list                       = array();
        foreach($suppliers as $index => $val)
        {
            $list[$val['id']] = $val['name'];
        }
       return $list;
    }

}

?>