<?php
class FaceRatingScale extends RatingScale
{

    static $table = 'faces_rating_scale';

    function __construct()
    {
        parent::__construct();
    }

    function getFaceRatingScales()
    {
        $rating_scales = $this->getAll();
        $headers = array('id' => 'Ref', 'numeric_rating' => 'Score Assigned to Faces Rating', 'face' => 'Face Display on Drop Down', 'faces_rating_description' => 'Faces Rating Description', 'color' => 'Colour', 'status' => 'Status');
        $values  = array();
        foreach($rating_scales as  $rating_scale)
        {
            $values[$rating_scale['id']]['id'] = $rating_scale['id'];
            $values[$rating_scale['id']]['numeric_rating'] = $rating_scale['numerical_rating'];
            $values[$rating_scale['id']]['rating_definition'] = "<img src='/library/images/image_".$rating_scale['face'].".png' width='30' height='30' />";
            $values[$rating_scale['id']]['faces_rating_description'] = $rating_scale['faces_rating_description'];
            $values[$rating_scale['id']]['color'] = "<span style='background-color: #".$rating_scale['color']."; padding:4px 7px;'>".$rating_scale['color']."</span>";
            $values[$rating_scale['id']]['status'] = (($rating_scale['status'] & 1) == 1 ? 'Active' : 'InActive');
        }
        return array('headers' => $headers, 'values' => $values, 'rating_scales' => $rating_scales);
    }

    function getList($is_active = true)
    {
        $rating_scales = $this->getAll($is_active);
        $list = array();
        foreach($rating_scales as $rating_scale)
        {
            $list[$rating_scale['id']] = "<img src='/library/images/image_".$rating_scale['face'].".png' width='30' height='30' />";
        }
        return $list;
    }

    function getNumericalRating($is_active = true)
    {
        $rating_scales = $this->getAll($is_active);
        $list = array();
        foreach($rating_scales as $rating_scale)
        {
            $list[$rating_scale['id']] = $rating_scale['numerical_rating'];
        }
        return $list;
    }
}

?>