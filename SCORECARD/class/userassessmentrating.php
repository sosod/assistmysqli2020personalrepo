<?php
class UserAssessmentRating extends Model
{

    static $table = 'user_assessment_rating';

    function __construct()
    {
        parent::__construct();
    }

    public function getAssessmentByScoreCardId($score_card_id)
    {
        $results = $this->db->get("SELECT * FROM #_".self::$table." WHERE score_card_id = '".$score_card_id."' ");
        $list = array();
        foreach($results as $result)
        {
            $list[$result['user_id']] = $result;
        }
        return $list;
    }

}
?>