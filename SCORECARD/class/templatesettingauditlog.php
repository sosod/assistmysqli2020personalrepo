<?php
class TemplateSettingAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        if(isset($where['template_id']))
        {
            $object_data  = DBConnect::getInstance()->get("SELECT * FROM #_".$table_name." WHERE template_id = '".$where['template_id']."' ");
        } else {
            $object_data  = DBConnect::getInstance()->get("SELECT * FROM #_".$table_name." WHERE id = '".$where['id']."' ");
        }
        $template_data = array();
        if(isset($_REQUEST['template_setting_data']))
        {
            parse_str($_REQUEST['template_setting_data'], $template_data);
        }
        $changes = array();
        if(!empty($object_data))
        {
          foreach($object_data as $field => $val)
          {
            if(isset($template_data) && !empty($template_data))
            {
                if(isset($template_data['required'][$val['deliverable_id']]))
                {
                    if($template_data['required'][$val['deliverable_id']] != $val['is_required'])
                    {
                        $changes['_ref_'.$val['id']] = "Deliverable ref D".$val['deliverable_id']." required changed from ".($this->yesNoChange($template_data['required'][$val['deliverable_id']]))." to ".($this->yesNoChange($val['is_required']));
                    }
                }
                if(isset($template_data['editable'][$val['deliverable_id']]))
                {
                    if($template_data['editable'][$val['deliverable_id']] != $val['is_editable'])
                    {
                        $changes['_ref_'.$val['id']] = "Deliverable ref D".$val['deliverable_id']." editable changed from ".($this->yesNoChange($template_data['editable'][$val['deliverable_id']]))." to ".($this->yesNoChange($val['is_editable']));
                    }
                }
            }
            if(isset($object_data['name']))
            {
               if($object_data['name'] != $_REQUEST['name'])
               {
                  $changes['name'] = array('from' => $object_data['name'], 'to' => $_REQUEST['name']);
               }
            }
            if(isset($object_data['status']))
            {
              if($object_data['status'] != $_REQUEST['status'])

                  if($_REQUEST['status'] == 2)
                  {
                      $changes['_status'] = "Contract Template deleted";
                  } elseif($_REQUEST['status'] == 0){
                      $changes['_status'] = "Contract Template deactivated";
                  } elseif($_REQUEST['status'] == 1){
                      $changes['_status'] = "Contract Template activated";
                  }
              }
            }
        }
        if(!empty($changes))
        {
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['ref_id']      = (isset($where['template_id']) ? $where['template_id'] : $where['id']);
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            static::$table              = $table_name."_logs";
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

    function yesNoChange($val)
    {
        $change_str = "";
        if($val == 1)
        {
            $change_str = "Yes";
        } else {
            $change_str = "No";
        }
        return $change_str;
    }

}
