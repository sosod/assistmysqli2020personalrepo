<?php
class RecipientUser extends Model
{

    static $table = 'score_card_recipient_user';

    function __construct()
    {
        parent::__construct();
    }

    function getAll()
    {
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ");
        return $response;
    }

    function getStatus($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }
}

?>