<?php
/**
	This file manages , creates the contracts
	* @author 	: admire
	
**/
class Contract extends Model
{
	/*
		@var int
	*/	
	protected $id;
	/*
		@var char
	*/	
	protected $name;	
	/*
		@var double
	*/	
	protected $budget;
	/*
		@var char
	*/	
	protected $comments;
	/*
		@var int
	*/	
	protected $type;
	/*
		@var int
	*/	
	protected $category;
	/*
		@var int
	*/	
	protected $supplier;
	/*
		@var date
	*/	
	protected $signnature_of_tender;
	/*
		@var date
	*/	
	protected $signature_of_sla;
	/*
		@var date
	*/	
	protected $completion_date;
	/*
		@var char
	*/	
	protected $responsible_person;
	/*
		@var char
	*/	
	protected $template;
	/*
		@var int
	*/	
	protected $is_template;
	/*
		@var int
	*/	
	protected $assessment_frequency;
	/*
		@var int
	*/	
	protected $assessment_frequency_date;
	/*
		@var date
	*/	
	protected $assessment_frequency_time;
	/*
		@var int
	*/	
	protected $notification_frequency;
	/*
		@var char
	*/	
	protected $attachments;
	/*
		@var char
	*/	
	protected $insert_user;
	/*
		@var date
	*/	
	protected $insertdate;

	protected $contractdefaults;

    const ACTIVE                = 1;
    const DELETED               = 2;
	const HAS_DELIVERABLES 		= 4;
	const HAS_SUB_DELIVERABLES  = 8;
	const WEIGHTS_ASSIGNED		= 16;
	const DELIVERED				= 32;
	const QUALITY 				= 64;
	const OTHER 				= 128;
	const TEMPLATE				= 256;
	const CONFIRMED             = 512;
    const ACTIVATED             = 1024;
    const AUTHORIZED 		    = 2048;
	const APPROVED				= 4096;

    const REFTAG                = 'C';

    static $table = 'contract';

	function __construct()
	{
		parent::__construct();
	}
	
	function getList($is_template = false, $option_sql = '')
	{
        $sql = "SELECT * FROM #_".static::$table." C WHERE 1  ";
        if($is_template)
        {
           $sql .= " AND C.is_template = 1";
        }
        if(!empty($option_sql))
        {
            $sql .= $option_sql;
        }
        $results = $this->db->get($sql);
        $list    = array();
        if($results)
        {
            foreach($results as $index => $contract)
            {
               $list[$contract['id']] = $contract['name'];
            }
        }
       return $list;
	}
     public function isValid($contract)
     {
         $errors = array();
         if(isset($contract['name']) && empty($contract['name']))
         {
            $errors['contract_name'] = 'Contract Name is required';
         }
         if(isset($contract['financial_year_id']) && empty($contract['financial_year_id']))
         {
             $errors['financial_year'] = 'Financial year is required';
         }

         if(isset($contract['type_id']) && empty($contract['type_id']))
         {
             $errors['type'] = 'Contract type is required';
         }

         if(isset($contract['category_id']) && empty($contract['category_id']))
         {
             $errors['type'] = 'Contract category is required';
         }

         if(isset($contract['supplier_id']) && empty($contract['supplier_id']))
         {
             $errors['type'] = 'Contract supplier is required';
         }

         if(isset($contract['budget']) && empty($contract['budget']))
         {
             $errors['budget'] = 'Contract budget is required';
         }

         if(isset($contract['budget']) && !empty($contract['budget']))
         {
             if(!is_numeric($contract['budget']))
             {
                 $errors['budget'] = 'Please enter a valid budget value';
             }
         }

         if(isset($contract['signature_of_tender']) && empty($contract['signature_of_tender']))
         {
             $errors['signature_of_tender'] = 'Signature of tender is required';
         }

         if(isset($contract['signature_of_sla']) && empty($contract['signature_of_sla']))
         {
             $errors['signature_of_sla'] = 'Signature of sla is required';
         }

         if(isset($contract['completion_date']) && empty($contract['completion_date']))
         {
             $errors['completion_date'] = 'Completion date is required';
         }

         if(!is_valid_date($contract['completion_date'], $contract['signature_of_sla']))
         {
             $errors['_signature_of_sla']  = "Date of signature of contract/sla cannot not be after the completion date of the contract";
         }

         if(!is_valid_date($contract['completion_date'], $contract['signature_of_tender']))
         {
             $errors['_signature_of_tender']  = "Date of signature of tender cannot be after the completion date of the contract";
         }

         if(isset($contract['manager_id']) && empty($contract['manager_id']))
         {
             $errors['manager'] = 'Contract manager is required';
         }

         if(isset($contract['owner_id']) && empty($contract['owner_id']))
         {
             //$errors['owner'] = 'Contract owner is required';
         }

         if(isset($contract['authorisor_id']) && empty($contract['authorisor_id']))
         {
             $errors['authorisor'] = 'Contract authorisor is required';
         }

         if(isset($errors) && !empty($errors))
         {
            $_SESSION['flash_errors']['contract'] = $errors;
            return FALSE;
         }
       return TRUE;
     }


	function prepareSql($options = array())
	{
        $sql_str = '';
        if(isset($options['contract_id']) && !empty($options['contract_id']))
        {
            $sql_str .=  " AND C.id = ".$options['contract_id']." ";
        }
        if(isset($options['contract_owner']) && !empty($options['contract_owner']))
        {
            $sql_str .=  " AND C.owner_id = ".$options['contract_owner']." ";
        }
        if(isset($options['page']) && $options['page'] == 'activate')
        {
        } else {
            if(isset($options['financial_year']) && !empty($options['financial_year']))
            {
                $sql_str .=  " AND C.financial_year_id = ".$options['financial_year']." ";
            } else {
                $sql_str .=  " AND C.financial_year_id = '".User::getDefaultFinancialYear()."' ";
            }
        }
/*        if(isset($options['financial_year']) && !empty($options['financial_year']))
        {
            $sql_str .=  " AND C.financial_year_id = ".$options['financial_year']." ";
        }*/
        if(isset($options['status']) && $options['status'] == 'new')
        {
            $sql_str .=  " AND C.status & ".Contract::ACTIVE." = ".Contract::ACTIVE." ";
            $sql_str .=  " AND C.status & ".Contract::CONFIRMED." <> ".Contract::CONFIRMED." ";
            $sql_str .=  " AND C.status & ".Contract::ACTIVATED." <> ".Contract::ACTIVATED." ";
        }
        if(isset($options['section']) && $options['section'] == 'new')
        {
            $sql_str .=  " AND C.status & ".Contract::ACTIVE." = ".Contract::ACTIVE." ";
            if(isset($options['page']) && $options['page'] == 'template')
            {
                $sql_str .=  " AND C.status & ".Contract::CONFIRMED." <> ".Contract::CONFIRMED." ";
                $sql_str .=  " AND C.status & ".Contract::ACTIVATED." = ".Contract::ACTIVATED." ";
            } else {
                $sql_str .=  " AND C.status & ".Contract::CONFIRMED." <> ".Contract::CONFIRMED." ";
                $sql_str .=  " AND C.status & ".Contract::ACTIVATED." <> ".Contract::ACTIVATED." ";
            }
        }
        if(isset($options['section']) && $options['section'] == 'manage')
        {
            $sql_str .=  " AND C.status & ".Contract::ACTIVE." = ".Contract::ACTIVE." ";
            $sql_str .=  " AND C.status & ".Contract::CONFIRMED." <> ".Contract::CONFIRMED." ";
            $sql_str .=  " AND C.status & ".Contract::ACTIVATED." = ".Contract::ACTIVATED." ";
        }

        if(isset($options['section']) && $options['section'] == 'admin')
        {
            if($options['page'] == 'authorized_contracts')
            {
               $sql_str .=  " AND C.status & ".Contract::AUTHORIZED." = ".Contract::AUTHORIZED." ";
            } else {
                $sql_str .=  " AND C.status & ".Contract::AUTHORIZED." <> ".Contract::AUTHORIZED." ";
            }
        } else {
            $sql_str .=  " AND C.status & ".Contract::AUTHORIZED." <> ".Contract::AUTHORIZED." ";
        }

        if(isset($options['page']) && !empty($options['page']))
        {
           if($options['page'] == 'assessment')
           {
             //$deliverableObj = new Deliverable();
             //$deliverable_to_assess = $deliverableObj->getDeliverableToAssess();
             $sql_str .= " AND C.assessment_frequency_date	<= NOW() AND C.is_contract_assessed = 1";
             $sql_str .=  " AND C.manager_id  = '".$_SESSION['tid']."' ";
           } elseif($options['page'] == 'authorization'){
             $sql_str .=  " AND C.authorisor_id  = '".$_SESSION['tid']."' AND C.status_id = 3 ";
           } elseif($options['page'] == 'confirmation') {
             $sql_str .=  " AND C.status & ".Contract::ACTIVE." = ".Contract::ACTIVE." ";
             $sql_str .=  " AND C.status & ".Contract::CONFIRMED." <> ".Contract::CONFIRMED." ";
             $sql_str .=  " AND C.status & ".Contract::ACTIVATED." <> ".Contract::ACTIVATED." ";
           } elseif($options['page'] == 'payment' || $options['page'] == 'budget') {
             $sql_str .=  " AND C.status & ".Contract::ACTIVE." = ".Contract::ACTIVE." ";
             $sql_str .=  " AND C.status & ".Contract::CONFIRMED." <> ".Contract::CONFIRMED." ";
             $sql_str .=  " AND C.status & ".Contract::ACTIVATED." = ".Contract::ACTIVATED." ";
           } elseif($options['page'] == 'activate'){
              if($options['type'] == 'awaiting')
              {
                $sql_str .=  " AND C.status & ".Contract::ACTIVE." = ".Contract::ACTIVE." ";
                $sql_str .=  " AND C.status & ".Contract::CONFIRMED." = ".Contract::CONFIRMED." ";
                $sql_str .=  " AND C.status & ".Contract::ACTIVATED." <> ".Contract::ACTIVATED." ";
              } else {
                $sql_str .=  " AND C.status & ".Contract::ACTIVE." = ".Contract::ACTIVE." ";
                $sql_str .=  " AND C.status & ".Contract::CONFIRMED." <> ".Contract::CONFIRMED." ";
                $sql_str .=  " AND C.status & ".Contract::ACTIVATED." = ".Contract::ACTIVATED." ";
              }
           }
        }
/*        if(isset($options['page']) && $options['page'] == 'confirmation')
        {
            $sql_str .=  " AND C.status & ".Contract::ACTIVE." = ".Contract::ACTIVE." ";
            $sql_str .=  " AND C.status & ".Contract::CONFIRMED." <> ".Contract::CONFIRMED." ";
            $sql_str .=  " AND C.status & ".Contract::ACTIVATED." <> ".Contract::ACTIVATED." ";
        }*/
        if(isset($options['limit']) && !empty($options['limit']))
        {
            $sql_str .=  " LIMIT ".(int)$options['start'].",".(int)$options['limit'];
        }
      return $sql_str;
	}

    function getContractDetail($id)
    {
        $result = $this->db->getRow("SELECT C.*, C.name AS contract_name, C.id AS contract_id, FN.value AS financial_year, CT.name AS contract_type, CC.name AS contract_category,
                                       PT.name AS payment_term, PF.name AS payment_frequency, CONCAT(CM.tkname, ' ', CM.tksurname) AS contract_manager,
                                       CONCAT(CA.tkname, ' ', CA.tksurname) AS contract_authorisor, T.name AS contract_template, date_format(C.completion_date, '%d-%b-%Y') AS completion_date,
                                       date_format(C.signature_of_tender, '%d-%b-%Y') AS signature_of_tender, date_format(C.signature_of_sla, '%d-%b-%Y') AS signature_of_sla,
                                       date_format(C.assessment_frequency_date, '%d-%b-%Y') AS assessment_frequency_date, date_format(C.assessment_frequency_date, '%d-%b-%Y') AS contract_assessment_frequency_start_date, date_format(C.remind_on, '%d-%b-%Y') AS remind_on,
                                       CS.name AS contract_status, CAF.name AS contract_assessment_frequency, CONCAT_WS('-', D.dirtxt, DS.subtxt) AS contract_owner, C.assessment_frequency_date AS frequency_date
                                       FROM #_".static::$table." C
                                       INNER JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = C.financial_year_id
                                       INNER JOIN #_contract_type CT ON CT.id = C.type_id
                                       INNER JOIN #_contract_category CC ON CC.id = C.category_id
                                       LEFT JOIN #_contract_status CS ON CS.id = C.status_id
                                       LEFT JOIN #_assessment_frequency CAF ON CAF.id = C.assessment_frequency_id
                                       LEFT JOIN #_contract_payment_term PT ON PT.id = C.payment_term_id
                                       LEFT JOIN #_contract_payment_frequency PF ON PF.id = C.payment_frequency_id
                                       LEFT JOIN assist_".$_SESSION['cc']."_timekeep CM ON CM.tkid = C.manager_id
                                       LEFT JOIN assist_".$_SESSION['cc']."_timekeep CA ON CA.tkid = C.authorisor_id
                                       LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref = C.owner_id
                                       LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = C.owner_id
                                       LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
                                       LEFT JOIN #_".static::$table." T ON T.id = C.template_id
                                       WHERE C.id = '".$id."' ");
        return $result;
    }

	function getContracts($option_sql = '')
	{
       $results = $this->db->get("SELECT C.*, C.name AS contract_name,  C.id AS contract_id, FN.value AS financial_year, CT.name AS contract_type, CC.name AS contract_category,
                                  PT.name AS payment_term, CAF.name AS contract_assessment_frequency, CS.name AS contract_status, PF.name AS payment_frequency,
                                  CONCAT(CM.tkname, ' ', CM.tksurname) AS contract_manager, CONCAT(CA.tkname, ' ', CA.tksurname) AS contract_authorisor, date_format(C.remind_on, '%d-%b-%Y') AS remind_on,
                                  date_format(C.completion_date, '%d-%b-%Y') AS completion_date, CONCAT_WS('-', D.dirtxt, DS.subtxt) AS contract_ownerr, T.name AS contract_template, date_format(C.assessment_frequency_date, '%d-%b-%Y') AS contract_assessment_frequency_start_date
                                  FROM #_".static::$table." C
                                  INNER JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = C.financial_year_id
                                  INNER JOIN #_contract_type CT ON CT.id = C.type_id
                                  INNER JOIN #_contract_category CC ON CC.id = C.category_id
                                  LEFT JOIN #_contract_payment_term PT ON PT.id = C.payment_term_id
                                  LEFT JOIN #_assessment_frequency CAF ON CAF.id = C.assessment_frequency_id
                                  LEFT JOIN #_contract_status CS ON CS.id = C.status_id
                                  LEFT JOIN #_contract_payment_frequency PF ON PF.id = C.payment_frequency_id
                                  INNER JOIN assist_".$_SESSION['cc']."_timekeep CM ON CM.tkid = C.manager_id
                                  INNER JOIN assist_".$_SESSION['cc']."_timekeep CA ON CA.tkid = C.authorisor_id
                                  INNER JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref = C.owner_id
                                  INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = C.owner_id
                                  LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
                                  LEFT JOIN #_".static::$table." T ON T.id = C.template_id
                                  WHERE 1 $option_sql");

/*        $results = $this->db->get("SELECT C.*, C.name AS contract_name,  C.id AS contract_id, FN.value AS financial_year, CT.name AS contract_type, CC.name AS contract_category,
                                   PT.name AS payment_term, PF.name AS payment_frequency, CONCAT(CM.tkname, ' ', CM.tksurname) AS contract_manager,
                                   CONCAT(CA.tkname, ' ', CA.tksurname) AS contract_authorisor, T.name AS contract_template, date_format(C.remind_on, '%d-%b-%Y') AS remind_on,
                                   date_format(C.completion_date, '%d-%b-%Y') AS completion_date, CS.name AS contract_status, CAF.name AS contract_assessment_frequency,
                                   CONCAT_WS('-', D.dirtxt, DS.subtxt) AS contract_owner
                                   FROM #_".static::$table." C
                                   INNER JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = C.financial_year_id
                                   INNER JOIN #_contract_type CT ON CT.id = C.type_id
                                   INNER JOIN #_contract_category CC ON CC.id = C.category_id
                                   LEFT JOIN #_contract_payment_term PT ON PT.id = C.payment_term_id
                                   LEFT JOIN #_assessment_frequency CAF ON CAF.id = C.assessment_frequency_id
                                   LEFT JOIN #_contract_status CS ON CS.id = C.status_id
                                   LEFT JOIN #_contract_payment_frequency PF ON PF.id = C.payment_frequency_id
                                   INNER JOIN assist_".$_SESSION['cc']."_timekeep CM ON CM.tkid = C.manager_id
                                   INNER JOIN assist_".$_SESSION['cc']."_timekeep CA ON CA.tkid = C.authorisor_id
                                   LEFT JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref = C.owner_id
                                   LEFT JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = C.owner_id
                                   LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
                                   LEFT JOIN #_".static::$table." T ON T.id = C.template_id
                                   WHERE 1 ".$option_sql);*/
        return $results;
	}

    function getTotalContracts($option_sql = '')
    {
        $results = $this->db->getRow("SELECT COUNT(DISTINCT(C.id)) AS total
                                      FROM #_".static::$table." C
                                      INNER JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = C.financial_year_id
                                      INNER JOIN #_contract_type CT ON CT.id = C.type_id
                                      INNER JOIN #_contract_category CC ON CC.id = C.category_id
                                      LEFT JOIN #_contract_payment_term PT ON PT.id = C.payment_term_id
                                      LEFT JOIN #_assessment_frequency CAF ON CAF.id = C.assessment_frequency_id
                                      LEFT JOIN #_contract_status CS ON CS.id = C.status_id
                                      LEFT JOIN #_contract_payment_frequency PF ON PF.id = C.payment_frequency_id
                                      INNER JOIN assist_".$_SESSION['cc']."_timekeep CM ON CM.tkid = C.manager_id
                                      INNER JOIN assist_".$_SESSION['cc']."_timekeep CA ON CA.tkid = C.authorisor_id
                                      INNER JOIN ".$_SESSION['dbref']."_dir_admins DA ON DA.ref = C.owner_id
                                      INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DS.subid = C.owner_id
                                      LEFT JOIN ".$_SESSION['dbref']."_dir D ON D.dirid = DS.subdirid
                                      LEFT JOIN #_".static::$table." T ON T.id = C.template_id
                                      WHERE 1 ".$option_sql);
        return $results['total'];
    }

    static function isContractResponsiblePerson($owner_id, $manager_id)
    {
        if(ContractOwner::isContractOwner($owner_id) || $manager_id == $_SESSION['tid'])
        {
            return true;
        }
      return false;
    }

    public function prepareContract($contracts, $total_contract, $options = array())
    {
        $columnsObj = new ContractColumns((isset($options['section']) ? $options['section'] : ''));
        $columns    = $columnsObj->getHeaders();
        $contract_data['contracts']         = array();
        $contract_data['contract_detail']   = array();
        $contract_data['columns']           = array();
        $contract_data['total']             = $total_contract;
        $contract_data['contract_owners']   = array();
        $contract_data['canEdit']           = array();
        $contract_data['canUpdate']         = array();
        $contract_data['canAddDeliverable'] = array();
        $contract_data['canAddAction']      = array();
        $contract_data['canBudget']         = array();
        $contract_data['canPayment']        = array();
        $contract_data['main_deliverables'] = array();
        $contract_data['actions']           = array();
        $contract_data['financial_year']    = (empty($options['financial_year']) ? User::getDefaultFinancialYear() : $options['financial_year']);
        $contract_ids                       = array();
        $contractDocumentObj                = new ContractDocument();
        $contractBudgetObj                  = new ContractSupplierBudget();
        $contractPaymentObj                 = new ContractSupplierPayment();

        if($contracts && !empty($contracts))
        {
            foreach($contracts as $index => $contract)
            {
                $contract_data['contract_detail'][$contract['id']]   = $contract;
                $contract_ids[]                                      = $contract['id'];
                $contract_data['canEdit'][$contract['id']]           = true;
                if(self::isContractResponsiblePerson($contract['owner_id'], $contract['manager_id'])) $contract_data['canUpdate'][$contract['id']] = true;
                $contract_data['canAddDeliverable'][$contract['id']] = ($contract['has_deliverable'] ? true : false);
                $contract_data['canAddAction'][$contract['id']]      = true;
                $contract_data['canBudget'][$contract['id']]         = true;
                $contract_data['canPayment'][$contract['id']]        = true;
                foreach($columns as $field => $val)
                {
                    if($field == 'attachments')
                    {
                        $documents                                           = $contractDocumentObj->getAll($contract['id']);
                        $contract_data['contracts'][$contract['id']][$field] = Attachment::displayDownloadableLinks($documents, 'contract');;
                        $contract_data['columns'][$field]                    = $val;
                    } elseif($field == 'contract_progress') {
                        $contract_progress                                   = Action::getActionStatsSummary($contract['id']);
                        $average_progress = (float)$contract_progress['average_progress'];
                        $progress = ASSIST_HELPER::format_percent($average_progress, 2);
                        $contract_data['contracts'][$contract['id']][$field] = $progress;
                        $contract_data['columns'][$field]                    = $val;
                    } elseif($field == 'contract_budget'){
                        $budget = ASSIST_HELPER::format_float((float)$contractBudgetObj->getContractBudget($contract['id']));
                        $contract_data['contracts'][$contract['id']][$field] = ($budget == null ? 0 : $budget);
                        $contract_data['columns'][$field]                    = $val;
                    } elseif($field == 'contract_payment'){
                        $payment = ASSIST_HELPER::format_float((float)$contractPaymentObj->getContractPayment($contract['id']));
                        $contract_data['contracts'][$contract['id']][$field] = ($payment == null ? 0 : $payment);
                        $contract_data['columns'][$field]                    = $val;
                    } elseif(isset($contract[$field])) {
                        if($field == 'contract_id')
                        {
                            $contract_data['contracts'][$contract['id']][$field] = Contract::REFTAG."".$contract[$field];
                            $contract_data['columns'][$field]                    = $val;
                        } else {
                            $contract_data['contracts'][$contract['id']][$field] = $contract[$field];
                            $contract_data['columns'][$field]                    = $val;
                        }
                    }
                }
            }
        }
        $deliverableObj                = new Deliverable();
        $deliverables                  = $deliverableObj->getDeliverableByContractIds($contract_ids);
        //debug($deliverables);
        //exit();
        $contract_data['deliverables']  = $deliverableObj->sortByMainDeliverable($deliverables);
        $contract_data['total_columns'] = count($contract_data['columns']);
        if($contract_data['total_columns'] == 0)
        {
            $contract_data['columns']       = $columns;
            $contract_data['total_columns'] = count($columns);
        }
        return $contract_data;
    }


    public function getContractDetailList($contract, $options = array())
    {
        $columnsObj     = new ContractColumns();
        $columns        = $columnsObj->getHeaders();
        $contract_list  = array();

        $contract_list['contract_id']['header'] = $columns['contract_id'];
        $contract_list['contract_id']['value']  = $contract['contract_id'];

        $contract_list['reference_number']['header'] = $columns['reference_number'];
        $contract_list['reference_number']['value']  = $contract['reference_number'];

        $contract_list['contract_name']['header'] = $columns['contract_name'];
        $contract_list['contract_name']['value']  = $contract['contract_name'];

/*        $contract_list['contract_comment']['header'] = $columns['contract_comment'];
        $contract_list['contract_comment']['value']  = $contract['contract_comment'];*/

        $contract_list['contract_type']['header'] = $columns['contract_type'];
        $contract_list['contract_type']['value']  = $contract['contract_type'];

        $contract_list['contract_category']['header'] = $columns['contract_category'];
        $contract_list['contract_category']['value']  = $contract['contract_category'];

        $contract_list['payment_term']['header'] = $columns['payment_term'];
        $contract_list['payment_term']['value']  = $contract['payment_term'];

        $contract_list['payment_frequency']['header'] = $columns['payment_frequency'];
        $contract_list['payment_frequency']['value']  = $contract['payment_frequency'];

        $contract_list['risk_name']['header'] = $columns['risk_name'];
        $contract_list['risk_name']['value']  = $contract['risk_name'];

        $contract_list['risk_reference']['header'] = $columns['risk_reference'];
        $contract_list['risk_reference']['value']  = $contract['risk_reference'];

        $contract_list['account_code']['header'] = $columns['account_code'];
        $contract_list['account_code']['value']  = $contract['account_code'];

        $contract_list['signature_of_tender']['header'] = $columns['signature_of_tender'];
        $contract_list['signature_of_tender']['value']  = $contract['signature_of_tender'];

        $contract_list['signature_of_sla']['header'] = $columns['signature_of_sla'];
        $contract_list['signature_of_sla']['value']  = $contract['signature_of_sla'];

        $contract_list['completion_date']['header'] = $columns['completion_date'];
        $contract_list['completion_date']['value']  = $contract['completion_date'];

        $contract_list['contract_manager']['header'] = $columns['contract_manager'];
        $contract_list['contract_manager']['value']  = $contract['contract_manager'];

        $contract_list['contract_authorisor']['header'] = $columns['contract_authorisor'];
        $contract_list['contract_authorisor']['value']  = $contract['contract_authorisor'];

        $contract_list['contract_template']['header'] = $columns['contract_template'];
        $contract_list['contract_template']['value']  = $contract['contract_template'];

        $contract_list['is_contract_assessed']['header'] = $columns['is_contract_assessed'];
        $contract_list['is_contract_assessed']['value']  =  $contract['is_contract_assessed'] ? 'Yes' : 'No';;

        $contract_list['deliverable_status_assessed']['header'] = $columns['deliverable_status_assessed'];
        $deliverableStatusObj        = new ContractDeliverableStatus();
        $contractDeliverableStatuses = $deliverableStatusObj->getContractDeliverableStatusesAssessed($contract['id']);
        $contract_list['deliverable_status_assessed']['value'] = $contractDeliverableStatuses;

        $contract_list['weight_assigned']['header'] = 'Are weights applied for assessment';
        $contract_list['weight_assigned']['value'] = $contract['weight_assigned'] ? 'Yes' : 'No';

        $contract_list['contract_assessment_frequency']['header'] = $columns['contract_assessment_frequency'];
        $contract_list['contract_assessment_frequency']['value']  = $contract['contract_assessment_frequency'];

        $contract_list['contract_assessment_frequency_start_date']['header'] = $columns['contract_assessment_frequency_start_date'];
        $contract_list['contract_assessment_frequency_start_date']['value']  = $contract['assessment_frequency_date'];

        $contractBudgetObj                  = new ContractSupplierBudget();
        $contractPaymentObj                 = new ContractSupplierPayment();
        $budget = ASSIST_HELPER::format_float((float)$contractBudgetObj->getContractBudget($contract['id']));

        $contract_list['contract_budget']['header'] =  $columns['contract_budget'];
        $contract_list['contract_budget']['value'] = ($budget == null ? 0 : $budget);

        $payment = ASSIST_HELPER::format_float((float)$contractPaymentObj->getContractPayment($contract['id']));


        $contract_list['contract_payment']['header'] =  $columns['contract_payment'];
        $contract_list['contract_payment']['value'] = ($payment == null ? 0 : $payment);
        return $contract_list;
    }


    public function getContractProgressSummary($contract_id)
    {
        $total_deliverable = Deliverable::getContractTotalDeliverable($contract_id);
        $action_summary    = Action::getActionStatsSummary($contract_id);
        $progress          = 0;
        if($total_deliverable != 0)
        {
            $progress = ($action_summary['total_progress']/$total_deliverable);
        }
       return ASSIST_HELPER::format_percent($progress, 2);
    }

    public function getLastAssessmentDate($contract_id)
    {
        $result =$this->db->getRow("SELECT date_format(DA.updated_at, '%d-%b-%Y') AS assessment_date FROM #_deliverable_assessment DA
                                    INNER JOIN #_deliverable D ON D.id = DA.deliverable_id
                                    WHERE D.contract_id = '".$contract_id."' ORDER BY DA.updated_at  DESC
                                 ");
        return (empty($result) ? '' : $result['assessment_date']);
    }

    public function saveContractTemplateData($template_id, $contract_id)
    {
        $templateSettingsObj        = new TemplateSetting();
        $contract_template_settings = $templateSettingsObj->getDeliverableTemplateSetting($template_id);
        $deliverableObj             =  new Deliverable();
        foreach($contract_template_settings as $deliverable_id => $setting)
        {
            if($setting['is_required'] == 1)
            {
               $deliverableObj->copySave($contract_id, $deliverable_id);
            }
        }

    }

    public static function allowToComplete($contract_id)
    {
      if(Deliverable::allCompleted($contract_id) && Deliverable::overallProgress($contract_id) == 100)  return true;
      return false;
    }

    public function updateActivityOnManage($contract)
    {
        if($contract && !empty($contract))
        {
           if( (($contract['status'] & Contract::ACTIVATED) == Contract::ACTIVATED) && ($contract['has_manage_activity'] != 1) )
           {
              $contract_update['has_manage_activity'] = date('Y-m-d H:i:s');
              $this->update_where($contract_update, array('id' => $contract['id']));
           }
        }
       return false;
    }
}
?>