<?php
class Glossary extends Model
{

    static $table = 'glossary';

    function __construct()
    {
        parent::__construct();
    }

    function getGlossaries($active = false)
    {
        $option_sql = '';
        if($active) $option_sql = ' AND G.status <> 0';
        $results = $this->db->get("SELECT G.*, N.score_card_terminology FROM #_".static::$table." G
                                    INNER JOIN #_naming N ON N.id = G.naming_id
                                    WHERE G.status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $results;
    }

    function getGlossary($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id");
        return $response;
    }

    function getList($active = false)
    {
        $glossaries = $this->getGlossaries($active);
       $list           = array();
        foreach($glossaries as $index => $val)
        {
            $list[$val['id']] = $val['name'];
        }
       return $list;
    }

}

?>