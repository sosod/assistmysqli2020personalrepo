<?php
class PercentageRatingScale extends RatingScale
{

    static $table = 'percentage_rating_scale';

    function __construct()
    {
        parent::__construct();
    }


    function getPercentageRatingScales()
    {
        $rating_scales = $this->getAll();
        $headers = array('id' => 'Ref', 'numeric_rating' => 'Score Assigned to Percentage Rating', 'percentage' => 'Percentage Display on Drop Down', 'percentage_rating_description' => 'Percentage Rating Description' ,  'color' => 'Colour', 'status' => 'Status');
        $values  = array();
        foreach($rating_scales as  $rating_scale)
        {
            $values[$rating_scale['id']]['id'] = $rating_scale['id'];
            $values[$rating_scale['id']]['numeric_rating'] = $rating_scale['numerical_rating'];
            $values[$rating_scale['id']]['rating_definition'] = ($rating_scale['numerical_rating'] == 'n/a' ? $rating_scale['percentage'] :  $rating_scale['percentage']."%");
            $values[$rating_scale['id']]['percentage_rating_description'] = $rating_scale['percentage_rating_description'];
            $values[$rating_scale['id']]['color'] = "<span style='background-color: #".$rating_scale['color']."; padding:4px 7px;'>".$rating_scale['color']."</span>";
            $values[$rating_scale['id']]['status'] = (($rating_scale['status'] & 1) == 1 ? 'Active' : 'InActive');
            $values[$rating_scale['id']]['_status'] = $rating_scale['status'];
        }
        return array('headers' => $headers, 'values' => $values, 'rating_scales' => $rating_scales);
    }

    function getList($is_active = true)
    {
        $rating_scales = $this->getAll($is_active);
        $list = array();
        foreach($rating_scales as $rating_scale)
        {
            if($rating_scale['numerical_rating'] == 'n/a')
            {
               $list[$rating_scale['id']] = $rating_scale['percentage'];
            } else {
               $list[$rating_scale['id']] = $rating_scale['percentage']."%";
            }
        }
        return $list;
    }

    function getNumericalRating($is_active = true)
    {
        $rating_scales = $this->getAll($is_active);
        $list = array();
        foreach($rating_scales as $rating_scale)
        {
            $list[$rating_scale['id']] = $rating_scale['numerical_rating'];
        }
        return $list;
    }
}

?>