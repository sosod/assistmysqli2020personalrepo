<?php
class SummaryNotificationUser extends Model
{
    static $table = "summary_notification_user";

    function __construct()
    {
        parent::__construct();
    }

    function getSummaryNotificationUsers($option_sql = "")
    {
        $results = $this->db->get("SELECT SMU.*, CONCAT(TK.tkname, ' ', TK.tksurname) AS user FROM #_".static::$table." SMU
                                   INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = SMU.user_id
                                   WHERE 1 ".$option_sql);
        return $results;

    }

    public function getRecipients($notification_id)
    {
        $option_sql = " AND summary_notification_id = '".$notification_id."' ";
        $recipients = $this->getSummaryNotificationUsers($option_sql);

        $recipient_list = array();
        foreach($recipients as $index => $recipient)
        {
            $recipient_list[$recipient['user_id']] = $recipient['user'];
        }
        return $recipient_list;
    }


}
?>
