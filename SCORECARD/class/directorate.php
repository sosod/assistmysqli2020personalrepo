<?php
class Directorate extends Model
{

    static $table = 'dir';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active) $option_sql = ' AND status <> 0';
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE active = true ORDER BY dirsort ".$option_sql);
        return $response;
    }

    function getDirectorate($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE dirid = $id ");
        return $response;
    }

    function getMaxRow()
    {
        $response = $this->db->getRow("SELECT MAX(dirid) AS d FROM #_".static::$table." WHERE 1");
        return $response;
    }

    function getOrderedDirectorates()
    {
        $results = $this->db->get("SELECT * FROM #_".static::$table." WHERE active = true ORDER BY dirsort");
        return $results;
    }

    function getList($active = false)
    {
        $contract_statuses = $this->getAll($active);
        $list              = array();
        foreach($contract_statuses as $index => $val)
        {
            $list[$val['id']] = $list[$val['id']] = ($val['client_terminology'] == ''  ? $val['name'] : $val['client_terminology']);
        }
        return $list;
    }

    function getStatusAssessedList($active)
    {
        $contract_statuses = $this->getAll($active);
        $list              = array();
        foreach($contract_statuses as $index => $val)
        {
            if($val['id'] != 1) $list[$val['id']] = ($val['client_terminology'] == ''  ? $val['name'] : $val['client_terminology']);
        }
        return $list;
    }

}

?>