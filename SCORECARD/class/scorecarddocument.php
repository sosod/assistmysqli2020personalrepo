<?php
class ScoreCardDocument extends Model
{

    static $table = 'score_card_document';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($score_card_id = null)
    {
        $option_sql  = '';
        if($score_card_id) $option_sql = " AND score_card_id = ".$score_card_id." ";
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getStatus($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
        $score_card_documents = $this->getAll($active);
        $list              = array();
        foreach($score_card_documents as $index => $val)
        {
            $list[$val['score_card_id']] = $val['filename']." ".$val['description'];
        }
        return $list;
    }

}

?>