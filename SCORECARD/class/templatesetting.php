<?php
class TemplateSetting extends Model
{

    static $table = 'contract_template_setting';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($deliverable_id = null)
    {
        $option_sql = '';
        if($deliverable_id)  $option_sql = " AND deliverable_id = ".$deliverable_id." ";
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getTemplateSetting($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($deliverable_id = null)
    {
        $template_settings = $this->getAll();
        $list              = array();
        foreach($template_settings as $index => $val)
        {
            $list[$val['id']] = $val['name'];
        }
        return $list;
    }

    public function getContractTemplateSettings($contract_id)
    {
        $results = $this->db->get("SELECT CT.id AS template_id, CT.name AS template_name, TS.id, TS.deliverable_id,
                                   TS.is_required, TS.is_editable
                                   FROM #_".static::$table." TS
                                   INNER JOIN #_contract_template CT ON CT.id = TS.template_id
                                   WHERE CT.contract_id = '".$contract_id."'
                                  ");
        return $results;
    }

    public function getDeliverableTemplateSetting($contract_id)
    {
        $template_settings = $this->getContractTemplateSettings($contract_id);
        $deliverable_settings = array();
        foreach($template_settings as $index => $template_setting)
        {
            $deliverable_settings[$template_setting['deliverable_id']] = array('is_required' => $template_setting['is_required'], 'is_editable' => $template_setting['is_editable']);
        }
       return $deliverable_settings;
    }

}

?>