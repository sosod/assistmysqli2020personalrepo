<?php
class ScoreCardType extends Model
{

    static $table = 'score_card_type';

    function __construct()
    {
        parent::__construct();
    }

    function getScoreCardTypes($active = false)
    {
        $option_sql = '';
        if($active)
        {
            $option_sql = ' AND status <> 0';
        }
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

    function getScoreCardType($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_contract_type WHERE id = $id");
        return $response;
    }

    function getActiveScoreCardType()
    {
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::ACTIVE." = ".DBConnect::ACTIVE."");
        return $response;
    }

    function getList($active = false)
    {
       $contract_types = $this->getScoreCardTypes($active);
       $list           = array();
        foreach($contract_types as $index => $val)
        {
            $list[$val['id']] = $val['name'];
        }
       return $list;
    }

}

?>