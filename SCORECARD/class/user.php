<?php
class User extends Model
{
    //defining user access constants
    const ACTIVE		     = 1;//1000000000
    const DELETED 		 	 = 2;//1000000000
    const CREATE_SCORE_CARD  = 4;   //00000010
    const CREATE_DELIVERABLE = 8;   //00000100
    const UPDATE_ALL	     = 16;  //00001000
    const EDIT_ALL		     = 32;	//00010000
    const REPORTS			 = 64;  //00100000
    const ASSURANCE			 = 128; //01000000
    const SETUP 		     = 256; //10000000
    const SCORE_CARD_ADMIN	 = 512; //100000000
    const MODULE_ADMIN       = 2048; //00000001

    static $table            = 'user';

    function __construct()
    {
        parent::__construct();
    }

    function getUsers($status = 1)
    {
        $option_sql  = '';
        if($status > 1)
        {
            $option_sql = " AND UA.status & ".(int)$status." =  ".(int)$status;
        }
        $response = $this->db->get("SELECT TK.tkid, TK.tkname, TK.tksurname, TK.tkstatus, TK.tkemail, UA.status, UA.id AS setting_id,  CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								    FROM assist_".$_SESSION['cc']."_timekeep TK
								    INNER JOIN assist_".$_SESSION['cc']."_menu_modules_users MU ON TK.tkid = MU.usrtkid
								    LEFT JOIN ".$_SESSION['dbref']."_".self::$table." UA ON UA.user_id = TK.tkid
								    WHERE tkid <> 0000 AND tkstatus = 1
								    AND MU.usrmodref = '".$_SESSION['modref']."'
								    $option_sql
								    ORDER BY TK.tkname , TK.tksurname
								 ");
        return $response;
    }

    public function getUsersList()
    {
        $users = $this->getUsers();
        $list  = array();
        foreach($users as $user)
        {
            $list[$user['tkid']] = $user;
        }
      return $list;
    }

    public function getList($active = false, $status = 1)
    {
        $users_list = $this->getUsers($status);
        $list       = array();
        foreach($users_list as $index => $user)
        {
            if($active && $user['setting_id'])
            {
               $list[$user['tkid']]  = $user['tkname']." ".$user['tksurname'];
            } else {
                $list[$user['tkid']]  = $user['tkname']." ".$user['tksurname'];
            }
        }
       return $list;
    }

    function getUserSettings()
    {
        $response = $this->db->get("SELECT UA.id, UA.status, CONCAT(TK.tkname, ' ', TK.tksurname) AS user
								    FROM #_".self::$table." UA
								    INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UA.user_id
								    WHERE status & ".User::DELETED." <> ".User::DELETED." ");
        return $response;
    }

    function getUser($user_id)
    {
        $response = $this->getRow("SELECT CONCAT(TK.tkname, ' ', TK.tksurname) AS user, TK.tkemail as email
									FROM assist_".$_SESSION['cc']."_timekeep TK WHERE tkid  = '".$user_id."' ");
        return $response;
    }

    function getUserSetting($id)
    {
        $response = $this->db->getRow("SELECT UA.id, UA.status, CONCAT(TK.tkname, ' ', TK.tksurname) AS user FROM #_".self::$table." UA
								       INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UA.user_id
								       WHERE id = $id ");
        return $response;
    }

    function getSettingByUserId($user_id)
    {
        $response = $this->db->getRow("SELECT UA.id, UA.status, CONCAT(TK.tkname, ' ', TK.tksurname) AS user, UA.financial_year_id,
                                       CONCAT(FN.value, ' (', FN.start_date, ' - ', FN.end_date, ')' ) AS default_financial_year FROM #_".self::$table." UA
								       INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UA.user_id
								       LEFT JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = UA.financial_year_id
								       WHERE UA.user_id = '".$user_id."' ");
        return $response;
    }

    function getUserAccess()
    {
        $response = $this -> get("SELECT * FROM #_".self::$table." WHERE user_id = '".$_SESSION['tid']."' ");
        return $response;
    }

    function getByUserId()
    {
        $response = $this -> getRow("SELECT * FROM #_".self::$table." WHERE user_id = '".$_SESSION['tid']."'");
        return $response;
    }

    function updateUserAccess( $id, $data)
    {
        $response = $this -> update(self::$table, $data, "id=$id");
        return $response;
    }

    public static function calculateUserSettingStatus($userStatuses)
    {
        $userAccessConstants = self::userConstants();
        $status  = 0;
        foreach($userAccessConstants as $constant => $constantVal)
        {
            $constantName = strtolower($constant);
            if(isset($userStatuses[$constantName]))
            {
                if($userStatuses[$constantName] == 1)
                {
                    $status += $constantVal;
                }
            }
        }
        return $status;
    }
    /*
     get an array of the user-access class constants
    */
    public static function userConstants()
    {
        $oClass              = new ReflectionClass("User");
        $userAccessConstants = $oClass->getConstants();
        return $userAccessConstants;
    }

    public static function getDefaultFinancialYear()
    {
        $mod_ref = $_SESSION['modref'];
        $user    = $_SESSION['tid'];
        if(!isset($_SESSION[$mod_ref][$user]['default_financial_year']))
        {
            $userObj      = new User();
            $user_setting = $userObj->getSettingByUserId($_SESSION['tid']);
            $_SESSION[$mod_ref][$user]['default_financial_year'] = $user_setting['financial_year_id'];
            return $user_setting['financial_year_id'];
        } else {
            return $_SESSION[$mod_ref][$user]['default_financial_year'];
        }
    }
}
?>
