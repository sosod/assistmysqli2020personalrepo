<?php
/**
This file manages , creates the actions
 * @author 	: admire

 **/
class ContractAuthorization extends Model
{
    /**
    @var int
     **/
    protected $id;
    /**
    @var int
     **/
    protected $contract_id;
    /**
    @var char
     **/
    protected $response;
    /**
    @var int
     **/
    protected $sign_off_id;
    /**
    @var date
     **/
    protected $date_tested;
    /**
    @var int
     **/
    protected $status;

    /**
    @var date
     **/
    protected $deleted_at;
    /**
    @var date
     **/
    protected $insert_user;
    /**
    @var int
     **/
    protected $created_at;
    /**
    @var char
     **/
    protected $updated_at;

    const ACTIVE          = 1;

    const REFTAG          = 'CA';

    static $table         = 'contract_authorization';

    function __construct()
    {
        parent::__construct();
    }

    function getList()
    {
        $sql = "SELECT * FROM #_".static::$table." WHERE 1  ";
        $results = $this->db->get($sql);
        $list    = array();
        if($results)
        {
            foreach($results as $index => $action)
            {
                $list[$action['id']] = $action['name'];
            }
        }
        return $list;
    }
    public function isValid($contract_authorization)
    {
        $errors = array();
        return TRUE;
    }


    function prepareSql($options = array())
    {
        $sql_str = '';
        if(isset($options['contract_id']) && !empty($options['contract_id']))
        {
            $sql_str .=  " AND CA.contract_id = ".$options['contract_id']." ";
        }

        $sql_str .= " AND CA.deleted_at IS NULL ";
        if(isset($options['limit']) && !empty($options['limit']))
        {
            $sql_str .=  " LIMIT ".(int)$options['start'].",".(int)$options['limit'];
        }
        return $sql_str;
    }

    function getContractAuthorizationDetail($id)
    {
        $results = $this->db->getRow("SELECT CA.*, date_format(CA.date_tested, '%d-%b-%Y') AS date_tested, date_format(CA.created_at, '%d-%b-%Y') AS created_at,
                                      date_format(CA.updated_at, '%d-%b-%Y') AS updated_at, C.id AS contract_id, C.name AS contract
                                      FROM #_".static::$table." CA
                                      INNER JOIN #_contract C ON C.id = CA.contract_id
                                      WHERE CA.id = '".$id."' ");
        return $results;
    }

    function getContractAuthorizations($option_sql = '')
    {
        $results = $this->db->get("SELECT CA.*, date_format(CA.date_tested, '%d-%b-%Y') AS date_tested, date_format(CA.created_at, '%d-%b-%Y') AS created_at,
                                   date_format(CA.updated_at, '%d-%b-%Y') AS updated_at, C.id AS contract_id, C.name AS contract,  CONCAT(TK.tkname, ' ', TK.tksurname) AS authorization_provider
                                   FROM #_".static::$table." CA
                                   INNER JOIN #_contract C ON C.id = CA.contract_id
                                   LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = CA.insert_user
                                   WHERE 1 ".$option_sql);
        return $results;
    }

    function getTotalContractAuthorization($option_sql = '')
    {
        $result = $this->db->getRow("SELECT COUNT(*) AS total
                                     FROM #_".static::$table." CA
                                     INNER JOIN #_contract C ON C.id = CA.contract_id
                                     WHERE 1 ".$option_sql);
        return $result['total'];
    }

    public function prepareContractAuthorization($contract_authorizations, $total, $options)
    {
        $authorization_data['authorizations'] = array();
        $authorization_data['total']      = $total;
        $contractAuthorizationDocumentObj   = new ContractAuthorizationDocument();
        foreach($contract_authorizations as $index => $authorization)
        {
            $authorization_data['authorizations'][$authorization['id']]['id']           = $authorization['id'];
            $authorization_data['authorizations'][$authorization['id']]['date_tested']  = $authorization['date_tested'];
            $authorization_data['authorizations'][$authorization['id']]['response']  = $authorization['response'];
            $authorization_data['authorizations'][$authorization['id']]['authorization_provider']  = $authorization['authorization_provider'];
            $authorization_data['authorizations'][$authorization['id']]['authorization_status'] = ($authorization['sign_off_id'] ? 'Yes' : "No");
            $authorization_data['authorizations'][$authorization['id']]['date_time']    = date('Y-m-d H:i:s');
            $documents                                                        = $contractAuthorizationDocumentObj->getAll($authorization['id']);
            $authorization_data['authorizations'][$authorization['id']]['attachments']          = Attachment::displayDownloadableLinks($documents, 'contract');;
            $authorization_data['authorizations'][$authorization['id']]['editable_attachments'] = Attachment::displayEditableAttachments($documents, 'contract');;
        }
        return $authorization_data;
    }
}
?>