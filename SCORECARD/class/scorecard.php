<?php
/**
	This file manages , creates the scorecards
	* @author 	: admire
	
**/
class ScoreCard extends Model
{
	/*
		@var int
	*/	
	protected $id;
	/*
		@var char
	*/	
	protected $name;	
	/*
		@var double
	*/	
	protected $budget;
	/*
		@var char
	*/	
	protected $comments;
	/*
		@var int
	*/	
	protected $type;
	/*
		@var int
	*/	
	protected $category;
	/*
		@var int
	*/	
	protected $supplier;
	/*
		@var date
	*/	
	protected $signnature_of_tender;
	/*
		@var date
	*/	
	protected $signature_of_sla;
	/*
		@var date
	*/	
	protected $completion_date;
	/*
		@var char
	*/	
	protected $responsible_person;
	/*
		@var char
	*/	
	protected $template;
	/*
		@var int
	*/	
	protected $is_template;
	/*
		@var int
	*/	
	protected $assessment_frequency;
	/*
		@var int
	*/	
	protected $assessment_frequency_date;
	/*
		@var date
	*/	
	protected $assessment_frequency_time;
	/*
		@var int
	*/	
	protected $notification_frequency;
	/*
		@var char
	*/	
	protected $attachments;
	/*
		@var char
	*/	
	protected $insert_user;
	/*
		@var date
	*/	
	protected $insert_date;

	protected $score_card_defaults;

    const ACTIVE                = 1;
    const DELETED               = 2;
	const CONFIRMED             = 4;
    const ACTIVATED             = 8;
    const AVAILABLE				= 16;
    const AUTHORIZED 		    = 32;
	const APPROVED				= 64;
    const CLOSED				= 128;

    const REFTAG                = 'SC';

    static $table = 'score_card';

	function __construct()
	{
		parent::__construct();
	}
	
	function getList($option_sql = '')
	{
        $sql     = "SELECT * FROM #_".static::$table." SC WHERE 1 AND SC.score_card_setup_id = 0 $option_sql";
        $results = $this->db->get($sql);
        $list    = array();

        if($results)
        {
            foreach($results as $index => $score_card)
            {
                $shortDescription = "";
                if(isTextTooLong($score_card['name']))
                 {
                    $shortDescription = cutText($score_card['name'], $score_card['id']."_name");
                } else {
                    $shortDescription = $score_card['name'];
                }
                $list[$score_card['id']]['full_name'] = $score_card['name'];
                $list[$score_card['id']]['short_description'] = $shortDescription;
            }
        }
       return $list;
	}

    public function getScoreCardSetupList($is_active_score_card, $option_sql)
    {
        if($is_active_score_card)  $option_sql .= " AND SC.score_card_setup_id <> '' ";
        $results = $this->getScoreCardsWithSetup($option_sql);
        $list    = array();
        if($results)
        {
            foreach($results as $index => $score_card)
            {
                $shortDescription = "";
                if(isTextTooLong($score_card['name']))
                {
                    $shortDescription = cutText($score_card['name'], $score_card['id']."_name");
                } else {
                    $shortDescription = $score_card['name'];
                }
                $list[$score_card['id']] = "(SC".$score_card['id'].") ".$shortDescription." - ".$score_card['setup_reference'];
            }
        }
        return $list;
    }

    public function getActivatedList($option_sql)
    {
        $option_sql .= " AND SC.status &  ".ScoreCard::ACTIVATED." = ".ScoreCard::ACTIVATED." ";
        $results = $this->getScoreCards($option_sql);
        $list    = array();
        if($results)
        {
            foreach($results as $index => $score_card)
            {
                $shortDescription = "";
                if(isTextTooLong($score_card['name']))
                {
                    $shortDescription = cutText($score_card['name'], $score_card['id']."_name");
                } else {
                    $shortDescription = $score_card['name'];
                }
                //$list[$score_card['id']]['full_name'] = $score_card['name'];
                //$list[$score_card['id']]['short_description'] = $shortDescription;
                $list[$score_card['id']] = "(SC".$score_card['id'].") ".$shortDescription." - ".$score_card['reference_number'];
            }
        }
        return $list;
    }

     public function isValid($score_card)
     {
         $errors = array();
         if(isset($score_card['name']) && empty($score_card['name']))
         {
            $errors['score_card_name'] = 'Score card Name is required';
         }
         if(isset($score_card['financial_year_id']) && empty($score_card['financial_year_id']))
         {
             $errors['financial_year'] = 'Financial year is required';
         }

         if(isset($score_card['type_id']) && empty($score_card['type_id']))
         {
             $errors['type'] = 'Score card type is required';
         }

         if(isset($score_card['category_id']) && empty($score_card['category_id']))
         {
             $errors['type'] = 'Score card category is required';
         }
         if(isset($score_card['manager_id']) && empty($score_card['manager_id']))
         {
             $errors['manager'] = 'Score card manager is required';
         }

         if(isset($score_card['owner_id']) && empty($score_card['owner_id']))
         {
             //$errors['owner'] = 'ScoreCard owner is required';
         }

         if(isset($errors) && !empty($errors))
         {
            $_SESSION['flash_errors']['score_card'] = $errors;
            return FALSE;
         }
       return TRUE;
     }


	function prepareSql($options = array())
	{
        $sql_str = '';
        if(isset($options['score_card_id']) && !empty($options['score_card_id']))
        {
            $sql_str .=  " AND SC.id = ".$options['score_card_id']." ";
        }
        if(isset($options['score_card_owner']) && !empty($options['score_card_owner']))
        {
            $sql_str .=  " AND SC.owner_id = ".$options['score_card_owner']." ";
        }
/*        if(isset($options['financial_year']) && !empty($options['financial_year']))
        {
            $sql_str .=  " AND SC.financial_year_id = ".$options['financial_year']." ";
        }*/
        if(isset($options['status']) && $options['status'] == 'new')
        {
            $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
            $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";
            $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVATED." <> ".ScoreCard::ACTIVATED." ";
            $sql_str .=  " AND SC.score_card_setup_id  = '' ";
        }
        if(isset($options['section']) && $options['section'] == 'new')
        {
            $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
            if(isset($options['page']) && $options['page'] == 'template')
            {
                $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";
                $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVATED." = ".ScoreCard::ACTIVATED." ";
            } else {
                $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";
                $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVATED." <> ".ScoreCard::ACTIVATED." ";
            }
        }
        if(isset($options['section']) && $options['section'] == 'manage')
        {
            $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
            $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";
            $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVATED." = ".ScoreCard::ACTIVATED." ";
            $sql_str .=  " AND SC.status & ".ScoreCard::AVAILABLE." = ".ScoreCard::AVAILABLE." ";
        }

        if(isset($options['section']) && $options['section'] == 'admin')
        {
            if($options['page'] == 'setup_score_card')
            {
                $sql_str .= " AND SC.status & ".ScoreCard::ACTIVATED." = ".ScoreCard::ACTIVATED." ";
                $sql_str .= " AND SC.status & ".ScoreCard::AVAILABLE." <> ".ScoreCard::AVAILABLE." ";
            }
            if(isset($options['page']))
            {
                if($options['page'] == 'edit')
                {
                    $sql_str .= " AND SC.status & ".ScoreCard::AVAILABLE." <> ".ScoreCard::AVAILABLE." ";
                }
            }

        }

        if(isset($options['page']) && !empty($options['page']))
        {
           $exclude_financial_year = array('activate', 'setup_score_card', 'unconfirm');
           if(!in_array($options['page'], $exclude_financial_year))
           {
               if(isset($options['financial_year']) && !empty($options['financial_year']))
               {
                   $sql_str .=  " AND SC.financial_year_id = ".$options['financial_year']." ";
               } else {
                   $sql_str .=  " AND SC.financial_year_id = '".User::getDefaultFinancialYear()."' ";
               }
           }
           if(in_array($options['page'], array('setup_score_card', 'update'))  )
           {
             $sql_str .= " AND SC.status & ".ScoreCard::CLOSED." <> ".ScoreCard::CLOSED." ";
           }
           if($options['page'] == 'edit_setting')
           {
                $sql_str .= " AND SC.status & ".ScoreCard::AVAILABLE." = ".ScoreCard::AVAILABLE." ";
           }

           if($options['page'] == 'assessment')
           {
             //$deliverableObj = new Deliverable();
             //$deliverable_to_assess = $deliverableObj->getDeliverableToAssess();
             $sql_str .= " AND SC.assessment_frequency_date	<= NOW() AND SC.is_score_card_assessed = 1";
             $sql_str .=  " AND SC.manager_id  = '".$_SESSION['tid']."' ";
           } elseif($options['page'] == 'confirmation') {
             $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
             $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";
             $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVATED." <> ".ScoreCard::ACTIVATED." ";
           } elseif($options['page'] == 'payment' || $options['page'] == 'budget') {
             $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
             $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";
             $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVATED." = ".ScoreCard::ACTIVATED." ";
           } elseif($options['page'] == 'activate'){
              $sql_str .=  " AND SC.score_card_setup_id  = '' ";
              if($options['type'] == 'awaiting')
              {
                $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
                $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." = ".ScoreCard::CONFIRMED." ";
                $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVATED." <> ".ScoreCard::ACTIVATED." ";
              } else {
                $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
                $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";
                $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVATED." = ".ScoreCard::ACTIVATED." ";
              }
           }
        }
        $sql_str .=  " AND SC.status & ".ScoreCard::DELETED." <> ".ScoreCard::DELETED." ";
/*        if(isset($options['page']) && $options['page'] == 'confirmation')
        {
            $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
            $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";
            $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVATED." <> ".ScoreCard::ACTIVATED." ";
        }*/
        if(isset($options['limit']) && !empty($options['limit']))
        {
            $sql_str .=  " LIMIT ".(int)$options['start'].",".(int)$options['limit'];
        }
      return $sql_str;
	}

    public function getScoreCard($score_card_id)
    {
        $result = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = '".$score_card_id."' ");
        return $result;
    }

    public function getScoreCardsWithSetup($option_sql = '')
    {
        $results = $this->db->get("SELECT SC.*, SC.name AS score_card_name, SC.id AS score_card_id, FN.value AS financial_year, CT.name AS score_card_type, CC.name AS score_card_category,
                                  CONCAT(CM.tkname, ' ', CM.tksurname) AS score_card_manager,  date_format(SC.remind_on, '%d-%b-%Y') AS remind_on, CS.name AS score_card_status,
                                  CONCAT_WS('-', DR.dirtxt, DS.subtxt) AS score_card_owner, SCS.name AS setup_reference, SCS.description AS setup_description,  SCS.frequency,
                                  date_format(SCS.start_date, '%d-%b-%Y') AS start_date, date_format(SCS.end_date, '%d-%b-%Y') AS end_date, SCS.id AS setup_id
                                  FROM #_".static::$table." SC
                                  INNER JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = SC.financial_year_id
                                  INNER JOIN #_score_card_type CT ON CT.id = SC.type_id
                                  INNER JOIN #_score_card_category CC ON CC.id = SC.category_id
                                  INNER JOIN #_score_card_setup SCS ON SCS.id = SC.score_card_setup_id
                                  LEFT JOIN #_score_card_status CS ON CS.id = SC.status_id
                                  LEFT JOIN assist_".$_SESSION['cc']."_timekeep CM ON CM.tkid = SC.manager_id
                                  LEFT JOIN assist_".$_SESSION['cc']."_list_dirsub DS ON DS.subid = SC.owner_id
                                  LEFT JOIN assist_".$_SESSION['cc']."_list_dir DR ON DR.dirid = DS.subdirid
                                  WHERE 1 $option_sql
                                 ");
        return $results;
    }

    public function getScoreCardWithSetup($score_card_id = null, $setup_id = null)
    {
        $sql = "SELECT SC.*, SC.name AS score_card_name, SC.id AS score_card_id, FN.value AS financial_year, CT.name AS score_card_type, CC.name AS score_card_category,
                  CONCAT(CM.tkname, ' ', CM.tksurname) AS score_card_manager,  date_format(SC.remind_on, '%d-%b-%Y') AS remind_on, CS.name AS score_card_status,
                  CONCAT_WS('-', DR.dirtxt, DS.subtxt) AS score_card_owner, SCS.name AS setup_reference, SCS.description AS setup_description,  SCS.frequency,
                  date_format(SCS.start_date, '%d-%b-%Y') AS start_date, date_format(SCS.end_date, '%d-%b-%Y') AS end_date, SCS.id AS setup_id, SCS.rating_type, SCS.allocate_to,
                  SCS.assign_question_to, SCS.use_rating_comment, SCS.use_corrective_action, SCS.are_corrective_action_owner_identified, SCS.score_card_id AS template_id,
                  SCS.link_to, LS.name AS score_card_linked
                  FROM #_".static::$table." SC
                  INNER JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = SC.financial_year_id
                  INNER JOIN #_score_card_type CT ON CT.id = SC.type_id
                  INNER JOIN #_score_card_category CC ON CC.id = SC.category_id
                  INNER JOIN #_score_card_setup SCS ON SCS.id = SC.score_card_setup_id
                  LEFT JOIN #_score_card_setup LS ON LS.id = SCS.link_to
                  LEFT JOIN #_score_card_status CS ON CS.id = SC.status_id
                  LEFT JOIN assist_".$_SESSION['cc']."_timekeep CM ON CM.tkid = SC.manager_id
                  LEFT JOIN assist_".$_SESSION['cc']."_list_dirsub DS ON DS.subid = SC.owner_id
                  LEFT JOIN assist_".$_SESSION['cc']."_list_dir DR ON DR.dirid = DS.subdirid
                  WHERE 1 ";
        if($score_card_id) $sql .= " AND SC.id = '".$score_card_id."' ";
        if($setup_id) $sql .= " AND SCS.id = '".$setup_id."' ";

        $result = $this->db->getRow($sql);
        return $result;
    }

    public function getScoreCardsWithSetupByScoreCardId($score_card_id)
    {
        $result = $this->db->get("SELECT SC.*, SC.name AS score_card_name, SC.id AS score_card_id, FN.value AS financial_year, CT.name AS score_card_type, CC.name AS score_card_category,
                                  CONCAT(CM.tkname, ' ', CM.tksurname) AS score_card_manager,  date_format(SC.remind_on, '%d-%b-%Y') AS remind_on, CS.name AS score_card_status,
                                  CONCAT_WS('-', DR.dirtxt, DS.subtxt) AS score_card_owner, SCS.name AS setup_reference, SCS.description AS setup_description,  SCS.frequency,
                                  date_format(SCS.start_date, '%d-%b-%Y') AS start_date, date_format(SCS.end_date, '%d-%b-%Y') AS end_date, SCS.id AS setup_id, SCS.rating_type, SCS.allocate_to,
                                  SCS.assign_question_to, SCS.use_rating_comment, SCS.use_corrective_action, SCS.are_corrective_action_owner_identified
                                  FROM #_".static::$table." SC
                                  INNER JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = SC.financial_year_id
                                  INNER JOIN #_score_card_type CT ON CT.id = SC.type_id
                                  INNER JOIN #_score_card_category CC ON CC.id = SC.category_id
                                  INNER JOIN #_score_card_setup SCS ON SCS.id = SC.score_card_setup_id
                                  LEFT JOIN #_score_card_status CS ON CS.id = SC.status_id
                                  LEFT JOIN assist_".$_SESSION['cc']."_timekeep CM ON CM.tkid = SC.manager_id
                                  LEFT JOIN assist_".$_SESSION['cc']."_list_dirsub DS ON DS.subid = SC.owner_id
                                  LEFT JOIN assist_".$_SESSION['cc']."_list_dir DR ON DR.dirid = DS.subdirid
                                  WHERE SCS.score_card_id = '".$score_card_id."'
                                 ");
        return $result;
    }

    public function getScoreCardsWithSetupByScoreCardIdList($score_card_id)
    {
       $results = $this->getScoreCardsWithSetupByScoreCardId($score_card_id);
       $list = array();
       foreach($results as $result)
       {
           $list[$result['setup_id']] = "(SC".$result['id'].")".$result['setup_reference'];
       }
       return $list;
    }

    public function getScoreCardWithSetupBySetupId($setup_id)
    {
        //$option_sql = " AND SCS.id = '".$setup_id."'  ";
        $result = $this->getScoreCardWithSetup(null, $setup_id);
        return $result;
    }

    function getScoreCardDetail($id)
    {
        $result = $this->db->getRow("SELECT SC.*, SC.name AS score_card_name, SC.id AS score_card_id, FN.value AS financial_year, CT.name AS score_card_type, CC.name AS score_card_category,
                                     CONCAT(CM.tkname, ' ', CM.tksurname) AS score_card_manager,  date_format(SC.remind_on, '%d-%b-%Y') AS remind_on, CS.name AS score_card_status,
                                     CONCAT_WS('-', DR.dirtxt, DS.subtxt) AS score_card_owner
                                     FROM #_".static::$table." SC
                                     INNER JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = SC.financial_year_id
                                     INNER JOIN #_score_card_type CT ON CT.id = SC.type_id
                                     INNER JOIN #_score_card_category CC ON CC.id = SC.category_id
                                     LEFT JOIN #_score_card_status CS ON CS.id = SC.status_id
                                     LEFT JOIN assist_".$_SESSION['cc']."_timekeep CM ON CM.tkid = SC.manager_id
                                      LEFT JOIN assist_".$_SESSION['cc']."_list_dirsub DS ON DS.subid = SC.owner_id
                                      LEFT JOIN assist_".$_SESSION['cc']."_list_dir DR ON DR.dirid = DS.subdirid
                                     WHERE SC.id = '".$id."' ");
        return $result;
    }

	function getScoreCards($option_sql = '')
	{
       $results = $this->db->get("SELECT SC.*, SC.name AS score_card_name,  SC.id AS score_card_id, FN.value AS financial_year, CT.name AS score_card_type, CC.name AS score_card_category,
                                  CS.name AS score_card_status, CONCAT(CM.tkname, ' ', CM.tksurname) AS score_card_manager, date_format(SC.remind_on, '%d-%b-%Y') AS remind_on,
                                  CONCAT_WS('-', DR.dirtxt, DS.subtxt) AS score_card_owner
                                  FROM #_".static::$table." SC
                                  INNER JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = SC.financial_year_id
                                  INNER JOIN #_score_card_type CT ON CT.id = SC.type_id
                                  INNER JOIN #_score_card_category CC ON CC.id = SC.category_id
                                  LEFT JOIN #_score_card_status CS ON CS.id = SC.status_id
                                  INNER JOIN assist_".$_SESSION['cc']."_timekeep CM ON CM.tkid = SC.manager_id
                                  LEFT JOIN assist_".$_SESSION['cc']."_list_dirsub DS ON DS.subid = SC.owner_id
                                  LEFT JOIN assist_".$_SESSION['cc']."_list_dir DR ON DR.dirid = DS.subdirid
                                  WHERE 1 $option_sql");
        return $results;

	}

    function getTotalScoreCards($option_sql = '')
    {
        $results = $this->db->getRow("SELECT COUNT(DISTINCT(SC.id)) AS total
                                      FROM #_".static::$table." SC
                                      INNER JOIN assist_".$_SESSION['cc']."_master_financialyears FN ON FN.id = SC.financial_year_id
                                      INNER JOIN #_score_card_type CT ON CT.id = SC.type_id
                                      INNER JOIN #_score_card_category CC ON CC.id = SC.category_id
                                      LEFT JOIN #_score_card_status CS ON CS.id = SC.status_id
                                      INNER JOIN assist_".$_SESSION['cc']."_timekeep CM ON CM.tkid = SC.manager_id
                                      LEFT JOIN assist_".$_SESSION['cc']."_list_dirsub DS ON DS.subid = SC.owner_id
                                      LEFT JOIN assist_".$_SESSION['cc']."_list_dir DR ON DR.dirid = DS.subdirid
                                      WHERE 1 ".$option_sql);
        return $results['total'];
    }

    static function isScoreCardResponsiblePerson($owner_id, $manager_id)
    {
      if(ScoreCardOwner::isScoreCardOwner($owner_id) || $manager_id == $_SESSION['tid']) return true;
      return false;
    }

    public function prepareScoreCard($score_cards, $total_contract, $options = array())
    {
        $columnsObj = new ScoreCardColumn((isset($options['section']) ? $options['section'] : ''));
        $columns    = $columnsObj->getHeaders();
        $score_card_data['scorecards']         = array();
        $score_card_data['score_card_detail']   = array();
        $score_card_data['columns']           = array();
        $score_card_data['total']             = $total_contract;
        $score_card_data['score_card_owners']   = array();
        $score_card_data['canEdit']           = array();
        $score_card_data['canUpdate']         = array();
        $score_card_data['canAddDeliverable'] = array();
        $score_card_data['canAddAction']      = array();
        $score_card_data['main_deliverables'] = array();
        $score_card_data['actions']           = array();
        $score_card_data['financial_year']    = (empty($options['financial_year']) ? User::getDefaultFinancialYear() : $options['financial_year']);
        $score_card_ids                       = array();
        $scoreCardDocumentObj                 = new ScoreCardDocument();

        if($score_cards && !empty($score_cards))
        {
            foreach($score_cards as $index => $score_card)
            {
                $score_card_data['score_card_detail'][$score_card['id']]  = $score_card;
                $score_card_data['score_card_detail'][$score_card['id']]['has_manage_activity'] = $this->hasActivity($score_card);
                $score_card_ids[]                                         = $score_card['id'];
                $score_card_data['canEdit'][$score_card['id']]            = true;
                if(self::isScoreCardResponsiblePerson($score_card['owner_id'], $score_card['manager_id'])) $score_card_data['canUpdate'][$score_card['id']] = true;
                $score_card_data['canAddDeliverable'][$score_card['id']]  = ($score_card['has_deliverable'] ? true : false);
                $score_card_data['canAddAction'][$score_card['id']]       = true;
                foreach($columns as $field => $val)
                {
                    if($field == 'attachment')
                    {
                        $documents                                           = $scoreCardDocumentObj->getAll($score_card['id']);
                        $score_card_data['scorecards'][$score_card['id']][$field] = Attachment::displayDownloadableLinks($documents, 'scorecard');;
                        $score_card_data['columns'][$field]                    = $val;
                    } elseif($field == 'score_card_progress') {
                        $score_card_progress                                   = Action::getActionStatsSummary($score_card['id']);
                        $average_progress = (float)$score_card_progress['average_progress'];
                        $progress = ASSIST_HELPER::format_percent($average_progress, 2);
                        $score_card_data['scorecards'][$score_card['id']][$field] = $progress;
                        $score_card_data['columns'][$field]                    = $val;
                    } elseif(isset($score_card[$field])) {
                        if($field == 'score_card_id')
                        {
                            $score_card_data['scorecards'][$score_card['id']][$field] = ScoreCard::REFTAG."".$score_card[$field];
                            $score_card_data['columns'][$field]                    = $val;
                        } elseif(in_array($field, array('has_sub_deliverable', 'weight_assigned')) ){
                            $score_card_data['scorecards'][$score_card['id']][$field] = ($score_card[$field] == 1 ? 'Yes' : 'No');
                            $score_card_data['columns'][$field]                    = $val;
                        } else {
                           if( in_array($field, array('description', 'comment', 'score_card_name')) ) {

                                $shortDescription = "";
                                if(isTextTooLong($score_card[$field]))
                                {
                                    $shortDescription = cutString($score_card[$field], $score_card['id']."_score_card");
                                } else {
                                    $shortDescription = $score_card[$field];
                                }
                               $score_card_data['scorecards'][$score_card['id']][$field] = $shortDescription;
                               $score_card_data['columns'][$field]                    = $val;
                           } else {
                               $score_card_data['scorecards'][$score_card['id']][$field] = $score_card[$field];
                               $score_card_data['columns'][$field]                    = $val;
                           }
                        }
                    }
                }
            }
        }
        $deliverableObj                = new Deliverable();
        $deliverables                  = $deliverableObj->getDeliverableByScoreCardIds($score_card_ids);
        //debug($deliverables);
        //exit();
        $score_card_data['deliverables']  = $deliverableObj->sortByMainDeliverable($deliverables);
        $score_card_data['total_columns'] = count($score_card_data['columns']);
        if($score_card_data['total_columns'] == 0)
        {
            $score_card_data['columns']       = $columns;
            $score_card_data['total_columns'] = count($columns);
        }
        return $score_card_data;
    }


    public function hasActivity($score_card)
    {
       $result = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE score_card_setup_id = '".$score_card['id']."' ");
       if(!empty($result)) return true;
       return false;
    }

    public function getScoreCardDetailList($score_card, $options = array())
    {
        $columnsObj     = new ScoreCardColumn();
        $columns        = $columnsObj->getHeaders();

        //debug($score_card);
        //debug($columns);

        $score_card_list  = array();

        $score_card_list['financial_year']['header'] = $columns['financial_year'];
        $score_card_list['financial_year']['value']  = $score_card['financial_year'];


        $score_card_list['score_card_id']['header'] = $columns['score_card_id'];
        $score_card_list['score_card_id']['value']  = $score_card['score_card_id'];

        $score_card_list['reference_number']['header'] = $columns['reference_number'];
        $score_card_list['reference_number']['value']  = $score_card['reference_number'];

        $score_card_list['score_card_name']['header'] = $columns['score_card_name'];
        $score_card_list['score_card_name']['value']  = $score_card['score_card_name'];

        $score_card_list['score_card_comment']['header'] = $columns['comment'];
        $score_card_list['score_card_comment']['value']  = $score_card['comment'];

        $score_card_list['score_card_type']['header'] = $columns['score_card_type'];
        $score_card_list['score_card_type']['value']  = $score_card['score_card_type'];

        $score_card_list['score_card_category']['header'] = $columns['score_card_category'];
        $score_card_list['score_card_category']['value']  = $score_card['score_card_category'];

        $score_card_list['score_card_manager']['header'] = $columns['score_card_manager'];
        $score_card_list['score_card_manager']['value']  = $score_card['score_card_manager'];

        $score_card_list['weight_assigned']['header'] = 'Are weights applied for assessment';
        $score_card_list['weight_assigned']['value'] = $score_card['weight_assigned'] ? 'Yes' : 'No';

        $score_card_list['score_card_owner']['header'] = $columns['score_card_owner'];
        $score_card_list['score_card_owner']['value']  = $score_card['score_card_owner'];

        $score_card_list['has_sub_deliverable']['header'] = $columns['has_sub_deliverable'];
        $score_card_list['has_sub_deliverable']['value']  = ($score_card['has_sub_deliverable'] == 1 ? 'Yes' : 'No');

        if(isset($score_card['score_card_linked']))
        {
            $score_card_list['link_to']['header'] = $columns['score_card_linked'];
            $score_card_list['link_to']['value']  = $score_card['score_card_linked'];
        }
        if(isset($score_card['setup_description']))
        {
            $score_card_list['setup_description']['header'] = $columns['score_card_instruction'];
            $score_card_list['setup_description']['value']  = $score_card['setup_description'];
        }
        return $score_card_list;
    }


    public function getScoreCardProgressSummary($score_card_id)
    {
        $total_deliverable = Deliverable::getScoreCardTotalDeliverable($score_card_id);
        $action_summary    = Action::getActionStatsSummary($score_card_id);
        $progress          = 0;
        if($total_deliverable != 0)
        {
            $progress = ($action_summary['total_progress']/$total_deliverable);
        }
       return ASSIST_HELPER::format_percent($progress, 2);
    }

    public function getLastAssessmentDate($score_card_id)
    {
        $result =$this->db->getRow("SELECT date_format(DA.updated_at, '%d-%b-%Y') AS assessment_date FROM #_deliverable_assessment DA
                                    INNER JOIN #_deliverable D ON D.id = DA.deliverable_id
                                    WHERE D.score_card_id = '".$score_card_id."' ORDER BY DA.updated_at  DESC
                                 ");
        return (empty($result) ? '' : $result['assessment_date']);
    }

    public static function allowToComplete($score_card_id)
    {
      if(Deliverable::allCompleted($score_card_id) && Deliverable::overallProgress($score_card_id) == 100)  return true;
      return false;
    }

    public function updateActivityOnManage($score_card)
    {
        if($score_card && !empty($score_card))
        {
           if( (($score_card['status'] & ScoreCard::ACTIVATED) == ScoreCard::ACTIVATED) && ($score_card['has_manage_activity'] != 1) )
           {
              $score_card_update['has_manage_activity'] = date('Y-m-d H:i:s');
              $this->update_where($score_card_update, array('id' => $score_card['id']));
           }
        }
       return false;
    }

    public function getScoreCardHtml($score_card_id, $start_table = true)
    {
        $columnsObj = new ScoreCardColumn();
        $columns    = $columnsObj->getHeaders();

        $scoreCardObj = new ScoreCard();
        $score_card    = $scoreCardObj->getScoreCardDetail($score_card_id);
        $score_card_html = "";
        if($start_table)  $score_card_html = "<table width='100%'>";
        if(!empty($score_card))
        {
            if($start_table)
            {
              $score_card_html .= "<tr>";
                $score_card_html .= "<td colspan='2'><h4>ScoreCard Details</h4></td>";
              $score_card_html .= "</tr>";
            }
            $score_card_html .= "<tr>";
              $score_card_html .= "<th style='text-align: left;'>".$columns['score_card_id'].":</th>";
              $score_card_html .= "<td>".$score_card['score_card_id']."</td>";
            $score_card_html .= "</tr>";
            $score_card_html .= "<tr>";
                $score_card_html .= "<th style='text-align: left;'>".$columns['financial_year'].":</th>";
                $score_card_html .= "<td>".$score_card['financial_year']."</td>";
            $score_card_html .= "</tr>";
            $score_card_html .= "<tr>";
              $score_card_html .= "<th style='text-align: left;'>".$columns['reference_number'].":</th>";
              $score_card_html .= "<td>".$score_card['reference_number']."</td>";
            $score_card_html .= "</tr>";
            $score_card_html .= "<tr>";
              $score_card_html .= "<th style='text-align: left;'>".$columns['score_card_name'].":</th>";
              $score_card_html .= "<td>".$score_card['name']."</td>";
            $score_card_html .= "</tr>";
            $score_card_html .= "<tr>";
              $score_card_html .= "<th style='text-align: left;'>".$columns['comment'].":</th>";
              $score_card_html .= "<td>".$score_card['comment']."</td>";
            $score_card_html .= "</tr>";
            $score_card_html .= "<tr>";
              $score_card_html .= "<th style='text-align: left;'>".$columns['score_card_type'].":</th>";
              $score_card_html .= "<td>".$score_card['score_card_type']."</td>";
            $score_card_html .= "</tr>";
            $score_card_html .= "<tr>";
              $score_card_html .= "<th style='text-align: left;'>".$columns['score_card_category'].":</th>";
              $score_card_html .= "<td>".$score_card['score_card_category']."</td>";
            $score_card_html .= "</tr>";
            $score_card_html .= "<tr>";
              $score_card_html .= "<th style='text-align: left;'>".$columns['score_card_manager'].":</th>";
              $score_card_html .= "<td>".$score_card['score_card_manager']."</td>";
            $score_card_html .= "</tr>";
            $score_card_html .= "<tr>";
              $score_card_html .= "<th style='text-align: left;'>".$columns['score_card_owner'].":</th>";
              $score_card_html .= "<td>".$score_card['score_card_owner']."</td>";
            $score_card_html .= "</tr>";
            $score_card_html .= "<tr>";
              $score_card_html .= "<th style='text-align: left;'>".$columns['has_sub_deliverable'].":</th>";
              $score_card_html .= "<td>".($score_card['has_sub_deliverable'] == 1 ? 'Yes' : 'No')."</td>";
            $score_card_html .= "</tr>";
            $score_card_html .= "<tr>";
              $score_card_html .= "<th style='text-align: left;'>".$columns['weight_assigned'].":</th>";
              $score_card_html .= "<td>".($score_card['weight_assigned'] == 1 ? 'Yes' : 'No')."</td>";
            $score_card_html .= "</tr>";
/*            $score_card_html .= "<tr>";
              $score_card_html .= "<th style='text-align: left;'>".$columns['score_card_status'].":</th>";
              $score_card_html .= "<td>".$score_card['score_card_status']."</td>";
            $score_card_html .= "</tr>";*/
        }
        if($start_table) $score_card_html.= "<table>";
        return $score_card_html;
    }

    public function copyAndActivate($score_card_setup, $score_card_setup_id, $recipient_user = array())
    {
        $score_card    = $this->getScoreCard($score_card_setup['score_card_id']);
        $score_card_id = 0;
        if(!empty($score_card))
        {
            unset($score_card['id']);
            $score_card['financial_year_id']   = $score_card_setup['financial_year_id'];
            $score_card['score_card_setup_id'] = $score_card_setup_id;
            $score_card['status']              = $score_card['status'] + ScoreCard::AVAILABLE;
            $score_card['created_at']          = date('Y-m-d H:i:s');
            $score_card['updated_at']          = date('Y-m-d H:i:s');
            $score_card_id = $this->save($score_card);
            $deliverableObj             =  new Deliverable();
            if($score_card_setup['allocate_to'] == 'all')
            {
               $deliverableObj->copySave($score_card_setup['score_card_id'], $score_card_id, $score_card, $recipient_user);
            }
        }
        return $score_card_id;
    }

    public function copyAndActivateToUser($score_card_setup, $score_card_setup_id, $question_to_user = array())
    {
        $score_card    = $this->getScoreCard($score_card_setup['score_card_id']);
        $score_card_id = 0;
        if(!empty($score_card))
        {
            unset($score_card['id']);
            $score_card['financial_year_id']   = $score_card_setup['financial_year_id'];
            $score_card['score_card_setup_id'] = $score_card_setup_id;
            $score_card['status']              = $score_card['status'] + ScoreCard::AVAILABLE;
            $score_card['created_at']          = date('Y-m-d H:i:s');
            $score_card['updated_at']          = date('Y-m-d H:i:s');
            $score_card_id = $this->save($score_card);
            $deliverableObj             =  new Deliverable();
            $deliverableObj->copySaveQuestionToUser($score_card_setup['score_card_id'], $score_card_id, $question_to_user);
        }
        return $score_card_id;
    }

}
?>