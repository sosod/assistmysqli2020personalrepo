<?php
class ContractAssessment extends Model
{

    static $table = 'contract_assessment';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($contract_id = null)
    {
        $option_sql = '';
        if($contract_id)  $option_sql = " AND contract_id = ".$contract_id." ";
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getContractAssessment($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    public function getActiveContractAssessment($contract_id)
    {
        $result = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE contract_id = '".$contract_id."' AND completed_at IS NULL ");
        return $result;
    }

    public function getDoneContractAssessment($contract_id)
    {
        $result = $this->db->getRow("SELECT COUNT(*) AS total_assessment  FROM #_".static::$table." WHERE contract_id = '".$contract_id."' AND completed_at IS NOT NULL ");
        return $result;
    }

    function getList($contract_id = null)
    {
        $deliverable_assessments = $this->getAll();
        $list              = array();
        foreach($deliverable_assessments as $index => $val)
        {
            $list[$val['id']] = array('completed_at' => array(), 'status' => array(), 'contract_id' => array());
        }
        return $list;
    }

    public function assessmentCountNow($contract)
    {
        $assessmentFrequencyObj = new AssessmentFrequency();
        $assessment_frequency   = $assessmentFrequencyObj->getContractAssessmentFrequency($contract['assessment_frequency_id']);

        $datetime1 = new DateTime('now');
        $datetime2 = new DateTime($contract['frequency_date']);
        $interval = $datetime1->diff($datetime2);
        if($assessment_frequency['key'] == 'months')
        {
           return floor($interval->m/$assessment_frequency['period']);
        } elseif($assessment_frequency['key'] == 'weeks'){
            $weeks = floor($interval->days/($assessment_frequency['period'] * 7));
          return $weeks;
        }
    }

    public function getNextContractAssessmentDate($contract)
    {
        $assessmentFrequencyObj = new AssessmentFrequency();
        $assessment_frequency   = $assessmentFrequencyObj->getContractAssessmentFrequency($contract['assessment_frequency_id']);
        $frequency_date =  strtotime($contract['frequency_date']);
        if($assessment_frequency['key'] == 'months')
        {
          $next_date = date('Y-m-d', strtotime("+".$assessment_frequency['period']." month", $frequency_date));
          return $next_date;
        } elseif($assessment_frequency['key'] == 'weeks'){
          $next_date = date('Y-m-d', strtotime("+".$assessment_frequency['period']." week", $frequency_date));
          return $next_date;
        }
       return false;
    }

    public function getContractAssessmentReport($contract_id)
    {
       $assessments              = $this->getAll($contract_id);
       $contract_assessments     = array();
       $tmp                     = array();
       $deliverableAssessmentObj = new DeliverableAssessment();
       $deliverableObj           = new Deliverable();
       $deliverables             = $deliverableObj->getDeliverableByContractId($contract_id);
       foreach($assessments as $index => $assessment)
       {
         $tmp['assessments'][] = $deliverableAssessmentObj->getDeliverableAssessmentByContractAssessmentId($assessment['id']);
         $tmp['contract_assessment'] = $assessment;
       }
       foreach($tmp['assessments'] as $a_index => $assessment)
       {
         foreach($assessment as $deliverable_id => $deliverable_assessment)
         {
             if(isset($deliverables[$deliverable_id])) $contract_assessments['deliverables'][$deliverable_id] = $deliverables[$deliverable_id];
             $contract_assessments['assessments'][$deliverable_id][$a_index] = $deliverable_assessment;
         }
       }
       return $contract_assessments;
    }

}

?>