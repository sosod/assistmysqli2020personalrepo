<?php
/**
This file manages , creates the contracts
 * @author 	: admire

 **/
class ContractTemplate extends Model
{

    static $table = "contract_template";


    public function getList()
    {
        $contract_templates = $this->getTemplates();
        $list = array();
        foreach($contract_templates as $index => $contract_template)
        {
           $list[$contract_template['id']] = $contract_template['name'];
        }
       return $list;
    }

    function prepareSql($options = array())
    {
        $sql_str = '';
        if(isset($options['contract_id']) && !empty($options['contract_id']))
        {
            $sql_str .=  " AND C.id = ".$options['contract_id']." ";
        }
        if(isset($options['financial_year']) && !empty($options['financial_year']))
        {
            $sql_str .=  " AND C.financial_year_id = ".$options['financial_year']." ";
        }
        if(isset($options['limit']) && !empty($options['limit']))
        {
            //$sql_str .=  " LIMIT ".(int)$options['start'].",".(int)$options['limit'];
        }
        return $sql_str;
    }

    public function getTemplates($option_sql = "")
    {
        $results = $this->db->get("SELECT T.id, T.name, T.contract_id, T.status, date_format(T.created_at, '%d-%b-%Y') AS created_at
                                   FROM #_contract_template T
                                   INNER JOIN #_contract C ON C.id = T.contract_id
                                   WHERE 1 $option_sql
                                   ");
        return $results;
    }

    public function getTemplateByContractId($contract_id)
    {
        $results = $this->db->getRow("SELECT T.name, T.contract_id, T.status
                                      FROM #_contract_template
                                      INNER JOIN #_contract C ON C.id = T.contract_id
                                      WHERE T.contract_id = '".$contract_id."' ");
        return $results;
    }

    public function getTemplateDetail($template_id)
    {
        $results = $this->db->get("SELECT T.name, T.contract_id, T.status
                                   FROM #_contract_template
                                   INNER JOIN #_contract C ON C.id = T.contract_id
                                   WHERE T.id = '".$template_id."'
                                   ");
        return $results;
    }

    public function getContractTemplateDetail($contract_id, $options = array())
    {
       $results = $this->db->getRow("SELECT CT.id AS template_id, CT.name AS template_name FROM #_".static::$table." CT WHERE CT.contract_id = '".$contract_id."' ");
       return $results;
    }

    public function getTemplate($template_id)
    {
        $result = $this->db->getRow("SELECT T.name, T.name AS template_name, T.contract_id, T.status, T.id AS template_id
                                   FROM #_contract_template T
                                   INNER JOIN #_contract C ON C.id = T.contract_id
                                   WHERE T.id = '".$template_id."'
                                   ");
        return $result;
    }

}
?>