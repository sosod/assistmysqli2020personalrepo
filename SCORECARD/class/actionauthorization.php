<?php
/**
This file manages , creates the actions
 * @author 	: admire

 **/
class ActionAuthorization extends Model
{
    /**
    @var int
     **/
    protected $id;
    /**
    @var int
     **/
    protected $action_id;
    /**
    @var char
     **/
    protected $response;
    /**
    @var int
     **/
    protected $sign_off_id;
    /**
    @var date
     **/
    protected $date_tested;
    /**
    @var int
     **/
    protected $status;

    /**
    @var date
     **/
    protected $deleted_at;
    /**
    @var date
     **/
    protected $insert_user;
    /**
    @var int
     **/
    protected $created_at;
    /**
    @var char
     **/
    protected $updated_at;

    const ACTIVE          = 1;

    const REFTAG          = 'DA';

    static $table         = 'action_authorization';

    function __construct()
    {
        parent::__construct();
    }

    function getList()
    {
        $sql = "SELECT * FROM #_".static::$table." WHERE 1  ";
        $results = $this->db->get($sql);
        $list    = array();
        if($results)
        {
            foreach($results as $index => $action)
            {
                $list[$action['id']] = $action['name'];
            }
        }
        return $list;
    }
    public function isValid($contract_authorization)
    {
        $errors = array();
        return TRUE;
    }


    function prepareSql($options = array())
    {
        $sql_str = '';
        if(isset($options['action_id']) && !empty($options['action_id']))
        {
            $sql_str .=  " AND DA.action_id = ".$options['action_id']." ";
        }

        $sql_str .= " AND DA.deleted_at IS NULL ";
        if(isset($options['limit']) && !empty($options['limit']))
        {
            $sql_str .=  " LIMIT ".(int)$options['start'].",".(int)$options['limit'];
        }
        return $sql_str;
    }

    function getActionAuthorizationDetail($id)
    {
        $results = $this->db->getRow("SELECT DA.*, date_format(DA.date_tested, '%d-%b-%Y') AS date_tested, date_format(DA.created_at, '%d-%b-%Y') AS created_at,
                                      date_format(DA.updated_at, '%d-%b-%Y') AS updated_at
                                      FROM #_".static::$table." DA
                                      WHERE DA.id = '".$id."' ");
        return $results;
    }

    function getActionAuthorizations($option_sql = '')
    {
        $results = $this->db->get("SELECT DA.*, date_format(DA.date_tested, '%d-%b-%Y') AS date_tested, date_format(DA.created_at, '%d-%b-%Y') AS created_at,
                                   date_format(DA.updated_at, '%d-%b-%Y') AS updated_at, CONCAT(TK.tkname, ' ', TK.tksurname) AS authorization_provider
                                   FROM #_".static::$table." DA
                                   LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = DA.insert_user
                                   WHERE 1 ".$option_sql);
        return $results;
    }

    function getTotalActionAuthorization($option_sql = '')
    {
        $result = $this->db->getRow("SELECT COUNT(*) AS total
                                     FROM #_".static::$table." DA
                                     WHERE 1 ".$option_sql);
        return $result['total'];
    }

    public function prepareActionAuthorization($contract_authorizations, $total, $options)
    {
        $authorization_data['authorizations'] = array();
        $authorization_data['total']      = $total;
        $contractAssuranceDocumentObj   = new ActionAuthorizationDocument();
        foreach($contract_authorizations as $index => $authorization)
        {
            $authorization_data['authorizations'][$authorization['id']]['id']           = $authorization['id'];
            $authorization_data['authorizations'][$authorization['id']]['date_tested']  = $authorization['date_tested'];
            $authorization_data['authorizations'][$authorization['id']]['response']  = $authorization['response'];
            $authorization_data['authorizations'][$authorization['id']]['authorization_provider']  = $authorization['authorization_provider'];
            $authorization_data['authorizations'][$authorization['id']]['authorization_status'] = ($authorization['sign_off_id'] ? 'Yes' : "No");
            $authorization_data['authorizations'][$authorization['id']]['date_time']    = date('Y-m-d H:i:s');
            $documents                                                        = $contractAssuranceDocumentObj->getAll($authorization['id']);
            $authorization_data['authorizations'][$authorization['id']]['attachments']          = Attachment::displayDownloadableLinks($documents, 'action');;
            $authorization_data['authorizations'][$authorization['id']]['editable_attachments'] = Attachment::displayEditableAttachments($documents, 'action');;
        }
        return $authorization_data;
    }
}
?>