<?php
class NotificationRule extends Model
{

    static $table = 'notification_rules';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active)
        {
            $option_sql = ' AND A.status <> 0';
        }
        $response = $this->db->get("SELECT A.*, date_format(A.date, '%d-%M-%Y') AS date, date_format(A.date, '%d-%b-%Y') AS asses_date FROM #_".static::$table." A WHERE A.status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

    function getNotificationRule($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id");
        return $response;
    }

    function getActiveNotificationRule()
    {
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::ACTIVE." = ".DBConnect::ACTIVE."");
        return $response;
    }

    function getList($active = false)
    {
        $notification_rules = $this->getAll($active);
        $list           = array();
        foreach($notification_rules as $index => $val)
        {
            $list[$val['id']] = $val['name'];
        }
       return $list;
    }

}

?>