<?php
/**
	This file manages , creates the actions
	* @author 	: admire
	
**/
class ContractSupplier extends Model
{
    /**
    @var int
     **/
    protected $id;
    /**
    @var int
     **/
    protected $supplier_id;
    /**
    @var char
     **/
    protected $contract_id;
    /**
    @var int
     **/
    protected $created_at;
    /**
    @var char
     **/
    protected $updated_at;
    /**
    @var date
     **/
    protected $status;

    const ACTIVE     = 1;

    const DELETED    = 2;

    const ADJUSTED   = 4;

    const REFTAG     = 'CS';

    static $table    = 'contract_supplier';

	function __construct()
	{
		parent::__construct();
	}
	
	function getList()
	{
        $sql = "SELECT * FROM #_".static::$table." WHERE 1  ";
        $results = $this->db->get($sql);
        $list    = array();
        if($results)
        {
            foreach($results as $index => $action)
            {
               $list[$action['id']] = $action['name'];
            }
        }
       return $list;
	}

    function getContractSuppliers($contract_id)
    {
        $results = $this->db->get("SELECT CS.*, S.name AS supplier
                                   FROM #_".static::$table." CS
                                   INNER JOIN #_supplier S ON S.id = CS.supplier_id
                                   WHERE CS.contract_id = '".$contract_id."'
                                 ");
        return $results;
    }

    function getSuppliersByContractId($contract_id)
    {
        $contract_suppliers = $this->getContractSuppliers($contract_id);
        $suppliers          = array();
        foreach($contract_suppliers as $index => $contract_supplier)
        {
            $suppliers[$contract_supplier['supplier_id']] = $contract_supplier['supplier'];
        }
      return $suppliers;
    }

}
?>