<?php
class ScoreCardOwner extends Model
{

    static $table = 'dirsub';

    function __construct()
    {
        parent::__construct();
    }

/*    function getAll()
    {
        $results  = $this->db->get("SELECT subid, CONCAT_WS('-', DR.dirtxt, DS.subtxt) AS dirtxt, DR.active
                                   FROM ".$_SESSION['dbref']."_dir DR
                                   INNER JOIN ".$_SESSION['dbref']."_dirsub DS ON DR.dirid = DS.subdirid
                                   WHERE DR.active = 1 AND DS.active = 1
                                ");
        $result = array_map("decodeDirectorate", $results);
        return $result;
    }*/

    function getAll()
    {
        $results  = $this->db->get("SELECT subid, CONCAT_WS(' - ', DR.dirtxt, DS.subtxt) AS dirtxt,  IF( CONCAT( DR.diryn, DS.subyn ) =  'YY', 1, 0 ) AS active
                                   FROM assist_".$_SESSION['cc']."_list_dir DR
                                   INNER JOIN assist_".$_SESSION['cc']."_list_dirsub DS ON DR.dirid = DS.subdirid
                                   WHERE DR.diryn = 'Y' AND DS.subyn = 'Y'
								   ORDER BY DR.dirsort, DR.dirtxt, DS.subsort, DS.subtxt
                                ");

        $result = array_map("decodeDirectorate", $results);
        return $result;
    }

    function getAllAdmins($sub_id=0)
    {
        $results  = $this->db->get("SELECT DA.id, DA.ref, DA.tkid, CONCAT(TK.tkname,' ', TK.tksurname) as user
									FROM ".$_SESSION['dbref']."_dir_admins DA
									INNER JOIN assist_".$_SESSION['cc']."_timekeep TK
									  ON TK.tkid = DA.tkid
									  AND TK.tkstatus = 1
									WHERE DA.active = 1 "
        .($sub_id>0 ? " AND DA.ref = ".$sub_id : "").
        " ORDER BY DA.ref, TK.tkname, TK.tksurname
    ");
        $result = array();
        foreach($results as $r) {
            $r['user'] = ASSIST_HELPER::decode($r['user']);
            if(!isset($result[$r['ref']]) || !is_array($result[$r['ref']])) { $result[$r['ref']] = array(); }
            $result[$r['ref']][$r['id']] = $r;
        }
        return $result;
    }


    function getScoreCardOwner($id)
    {
        $contract_owner =  $this->db->get("SELECT * FROM ".$_SESSION['dbref']."_".static::$table." WHERE  id = ".$id." ");
        return $contract_owner;
    }

    function getList($active = false)
    {
        $contract_owners = $this->getAll($active);
        $list              = array();
        foreach($contract_owners as $index => $val)
        {
            $list[$val['subid']] = $val['dirtxt'];
        }
        return $list;
    }

    function getSubDirAdmins($sub_dir_id = null)
    {
    }

    public static function isScoreCardOwner($sub_dir_id)
    {
        $sub_dir_admins = DBConnect::getInstance()->getRow("SELECT * FROM #_dir_admins DA
                                                            WHERE tkid = '".$_SESSION['tid']."' AND DA.ref = '".$sub_dir_id."'
                                                           ");
        if(!empty($sub_dir_admins))
        {
            return true;
        }
       return false;
    }

/*    public static function getUserSubDirectorates($user_id)
    {
        $sub_dir_admins = DBConnect::getInstance()->get("SELECT ref FROM #_dir_admins DA WHERE tkid = '".$user_id."' ");
        $sub_ids = array();
        if(!empty($sub_dir_admins))
        {
            foreach($sub_dir_admins as $index => $dir_admin)
            {
                $sub_ids[] = $dir_admin['ref'];
            }
            return implode(',', $sub_ids);
        }
        return 0;
    }*/

    public static function isContractOwner($sub_dir_id,$tkid="")
    {
        if(strlen($tkid)==0) { $tkid = $_SESSION['tid']; }
        $sub_dir_admins = DBConnect::getInstance()->getRow("SELECT * FROM #_dir_admins DA
                                             WHERE tkid = '".$tkid."' AND DA.ref = '".$sub_dir_id."' AND DA.active = 1
                                            ");
        if(!empty($sub_dir_admins))
        {
            return true;
        }
        return false;
    }

    public static function getUserSubDirectorates($user_id)
    {
        $sub_dir_admins = DBConnect::getInstance()->get("SELECT ref FROM #_dir_admins DA WHERE tkid = '".$user_id."' AND DA.active = 1");
        $sub_ids = array();
        if(!empty($sub_dir_admins))
        {
            foreach($sub_dir_admins as $index => $dir_admin)
            {
                $sub_ids[] = $dir_admin['ref'];
            }
            return implode(',', $sub_ids);
        }
        return 0;
    }



    function getEveryAdmin()
    {
        $results  = $this->db->get("SELECT DA.id, DA.ref, DA.tkid, CONCAT(TK.tkname,' ', TK.tksurname) as user , DA.active
									FROM ".$_SESSION['dbref']."_dir_admins DA
									INNER JOIN assist_".$_SESSION['cc']."_timekeep TK
									  ON TK.tkid = DA.tkid
									  AND TK.tkstatus = 1
									ORDER BY DA.ref, TK.tkname, TK.tksurname
                                ");
        $result = array();
        foreach($results as $r) {
            $r['user'] = ASSIST_HELPER::decode($r['user']);
            if(!isset($result[$r['ref']]) || !is_array($result[$r['ref']])) { $result[$r['ref']] = array(); }
            $result[$r['ref']][$r['id']] = $r;
        }
        return $result;
    }


//Function to create contract owner access to sub-directorate
    public function addOwner($sub_id,$user_id) {
        if($this->isContractOwner($sub_id,$user_id)) {
            return array("error","The user is already a Score Card Owner of the selected Sub-Directorate.");
        } else {
            $add = DBConnect::getInstance()->insert("dir_admins",array('tkid'=>$user_id,'active'=>1,'type'=>"SUB",'ref'=>$sub_id,'insertdate'=>"now()",'insertuser'=>$_SESSION['tid']));
            //CREATE AUDIT LOG HERE TO RECORD ADDITION OF OWNER
            //->
            return array("ok","The user has been added successfully.");
        }
    }
//Function to deactivate contract owner access to sub-directorate
    public function deleteOwner($sub_id,$user_id) {
        $del = DBConnect::getInstance()->update("dir_admins",array('active'=>0)," ref = ".$sub_id." AND type = 'SUB' AND tkid = '".$user_id."' ");
        //CREATE AUDIT LOG HERE TO RECORD DEACTIVATION OF OWNER
        //->
        return array("ok","The user has been deactivated successfully.");
    }
//Function to restore contract owner access to sub-directorate (undo deactivate)
    public function restoreOwner($sub_id,$user_id) {
        $del = DBConnect::getInstance()->update("dir_admins",array('active'=>1)," ref = ".$sub_id." AND type = 'SUB' AND tkid = '".$user_id."' ");
        //CREATE AUDIT LOG HERE TO RECORD RESTORATION OF OWNER
        //->
        return array("ok","The user has been restored successfully.");
    }

}

?>