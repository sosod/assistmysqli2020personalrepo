<?php
/**
This file manages , creates the actions
 * @author 	: admire

 **/
class ActionUpdateLog extends Model
{
    static $table           = 'action_update_logs';

    private $action_id      = null;

    function __construct($action_id = null)
    {
        if($action_id) $this->action_id = $action_id;
        parent::__construct();
    }

    function getLogs()
    {
        $sql = "SELECT * FROM #_".static::$table." AL WHERE 1  ";
        if($this->action_id) $sql .= " AND AL.action_id = '".$this->action_id."' ";
        $sql .= " ORDER BY AL.updated_at DESC";
        $results = $this->db->get($sql);
        return $results;
    }

    function displayLog()
    {
        $logs  = $this->getLogs();
        $table_html = '';
        if(!empty($logs))
        {
            foreach($logs as $index => $log)
            {
                $changes     = serializeDecode($log['changes']);
                $table_html .= "<tr>";
                if(!empty($changes))
                {
                    $current_status = '';
                    $user           = '';
                    if(isset($changes['currentstatus']))
                    {
                      $current_status = $changes['currentstatus'];
                      unset($changes['currentstatus']);
                    }
                    if(isset($changes['user']))
                    {
                        $user = $changes['user'];
                        unset($changes['user']);
                    }
                    $table_html .= "<td>".date('Y-m-d H:i:s', strtotime($log['updated_at']))."</td>";
                    $table_html .= "<td>".$user."</td>";
                    $changes_str = '';
                    foreach($changes as $change_field => $change)
                    {
                        if($change_field == "attachment")
                        {
                            if(is_array($change) )
                            {
                                foreach($change as $a => $aVal) $changes_str .= $aVal."<br />";
                            } else {
                                $changes_str .= $change."<br />";
                            }
                        } else if($change_field == "response") {
                            $changes_str .= "Added response : <span class='change'><i>".$change."</i></span><br />";
                        } else {
                            if(is_array($change))
                            {
                                if(isset($change['from']) && isset($change['to']))
                                {
                                    $field        = str_replace('id', '', $change_field);
                                    $field        = ucwords(str_replace('_','', $field));
                                    $changes_str .= $field." changed from ".$change['from']." to ".$change['to']."<br />";
                                }
                            } else {
                                $changes_str .= "<span class='change'><i>".$change."</span></i><br />";
                            }
                        }

                    }
                    $table_html .= "<td>".$changes_str."</td>";
                    $table_html .= "<td>".$current_status."</td>";
                }
                $table_html .= "</tr>";
            }
        } else {
            $table_html .= "<tr>";
            $table_html .= "<td colspan='4'>There is no activity log found for this action</td>";
            $table_html .= "</tr>";
        }
       return $table_html;
    }



}
?>