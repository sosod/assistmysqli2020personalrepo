<?php
class ScoreCardSetup extends Model
{

    static $table = 'score_card_setup';

    function __construct()
    {
        parent::__construct();
    }

    public function prepareSql()
    {
        $sql_str  = "";
        if(isset($options['setup__id']) && !empty($options['setup__id']))
        {
            $sql_str .=  " AND S.id = ".$options['deliverable_id']." ";
        }
        if(isset($options['score_card_id']) && !empty($options['score_card_id']))
        {
            $sql_str .=  " AND S.score_card_id = ".$options['score_card_id']." ";
        }
        //echo $sql_str;
        if(isset($options['limit']) && !empty($options['limit']))
        {
            $sql_str .=  " LIMIT ".(int)$options['start'].",".(int)$options['limit'];
        }
        return $sql_str;
    }

    public function getActivateDetail($score_card, $options)
    {
        $headingObj = new Deliverable();
        $headings   = $headingObj->getDeliverableByScoreCardId($score_card['id']);
        $categoryObj = new DeliverableCategory();
        $categories = $categoryObj->getList();

        $categorized_headings = array();
        foreach($headings as $heading)
        {
            $categorized_headings[$heading['category_id']][$heading['id']] = $heading;
        }
        $questionObj = new Action();
        $questions   = $questionObj->getActionsByScoreCardId($score_card['id']);

        $data                         = $questionObj->prepareAction($questions, count($questions), $options);
        $data['headings']             = $headings;
        $data['categorized_headings'] = $categorized_headings;
        $data['categories']           = $categories;
        return $data;
    }

    public function getScoreCardSetup($id)
    {
        $result = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = '".$id."' ");
        return $result;

    }


}

?>