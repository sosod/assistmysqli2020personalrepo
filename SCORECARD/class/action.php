<?php
/**
	This file manages , creates the actions
	* @author 	: admire
	
**/
/*require_once("../library/class/assist_dbconn.php");
require_once("../library/class/assist_db.php");
require_once("../library/class/assist_helper.php");*/

class Action extends Model
{
    /**
    @var int
     **/
    protected $id;
    /**
    @var int
     **/
    protected $deliverable_id;
    /**
    @var char
     **/
    protected $name;
    /**
    @var char
     **/
    protected $owner;
    /**
    @var char
     **/
    protected $measurable;
    /**
    @var int
     **/
    protected $status;
    /**
    @var int
     **/
    protected $progress;
    /**
    @var date
     **/
    protected $deadline;
    /**
    @var date
     **/
    protected $remind_on;
    /**
    @var int
     **/
    protected $active;
    /**
    @var char
     **/
    protected $insertuser;
    /**
    @var date
     **/
    protected $insertdate;

    const ACTIVE          = 1;

    const DELETED         = 2;

    const AWAITING        = 4;

    const DECLINED        = 8;

    const COMPLETED         = 16;

    const REFTAG            = 'A';

    static $table           = 'action';

	function __construct()
	{
		parent::__construct();
	}

	function getList()
	{
        $sql     = "SELECT * FROM #_".static::$table." WHERE 1  ";
        $results = $this->db->get($sql);
        $list    = array();
        if($results)
        {
            foreach($results as $index => $action)
            {
               $list[$action['id']] = $action['name'];
            }
        }
       return $list;
	}
     public function isValid($action, $deliverable)
     {
         $errors = array();
         if(isset($action['name']) && empty($action['name']))
         {
            $errors['action'] = 'Action name is required';
         }

         if(isset($action['owner_id']) && empty($action['owner_id']))
         {
             $errors['owner'] = 'Action owner is required';
         }

         if(isset($action['deadline_date']))
         {
             if(empty($action['deadline_date']))
             {
                 $errors['deadline_date'] = 'Deadline date is required';
             }
             if(!is_valid_date($deliverable['deadline_date'], $action['deadline_date']))
             {
                 $errors['_deadline_data'] = 'Action deadline date cannot be after deliverable deadline date';
             }

             if(isset($action['remind_on']) && !is_valid_date($action['deadline_date'], $action['remind_on']))
             {
                 $errors['_remind_on']  = "Remind on date of the action cannot be after the deadline date";
             }
         }

         if(isset($errors) && !empty($errors))
         {
            $_SESSION['flash_errors']['action'] = $errors;
            return FALSE;
         }
       return TRUE;
     }


	function prepareSql($options = array())
	{
        $sql_str  = " AND A.status & ".Action::DELETED." <> ".Action::DELETED."  ";
        if(isset($options['deliverable_id']) && !empty($options['deliverable_id']))
        {
            $sql_str .=  " AND D.id = ".$options['deliverable_id']." ";
        }
        if(isset($options['owner_id']) && !empty($options['owner_id']))
        {
            $sql_str .=  " AND A.owner_id = '".$options['owner_id']."' ";
        }
        if(isset($options['action_owner']) && !empty($options['action_owner']))
        {
            //$sql_str .=  " AND D.owner_id = ".$options['owner_id']." ";
        }
        if( isset($options['page']) && !in_array($options['page'], array('unconfirm') ))
        {
            if(isset($options['financial_year']) && !empty($options['financial_year']))
            {
               $sql_str .=  " AND SC.financial_year_id = ".$options['financial_year']." ";
            } else {
               $sql_str .=  " AND SC.financial_year_id = '".User::getDefaultFinancialYear()."' ";
            }
        }
        if(isset($options['score_card_id']) && !empty($options['score_card_id']))
        {
            $sql_str .=  " AND SC.id = ".$options['score_card_id']." ";
        }
        if(isset($options['section']) && $options['section'] == 'manage')
        {
            if(isset($options['page']) && $options['page'] == 'update')
            {
                $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
                $sql_str .=  " AND SC.status & ".ScoreCard::AVAILABLE." = ".ScoreCard::AVAILABLE." ";
                $sql_str .=  " AND A.status & ".Action::COMPLETED." <> ".Action::COMPLETED." ";
                $sql_str .=  " AND A.owner_id  = '".$_SESSION['tid']."' ";
                $sql_str .=  " AND SCS.start_date <= '".date('Y-m-d')."' AND SCS.end_date >= '".date('Y-m-d')."' ";
            } else if($options['page'] == 'edit') {
                $sql_str .=  " AND D.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
                $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";
                $sql_str .=  " AND SC.status & ".ScoreCard::AVAILABLE." = ".ScoreCard::AVAILABLE." ";
                $sub_dirs = ScoreCardOwner::getUserSubDirectorates($_SESSION['tid']);
                //$sql_str .=  " AND (SC.owner_id IN (".$sub_dirs.") OR SC.manager_id  = '".$_SESSION['tid']."') ";
                //$sql_str .=  " AND (D.owner_id  = '".$_SESSION['tid']."' OR (SC.owner_id IN (".$sub_dirs.") OR SC.manager_id  = '".$_SESSION['tid']."')) ";
            } else {
                $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
                $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";
                $sql_str .=  " AND SC.status & ".ScoreCard::AVAILABLE." = ".ScoreCard::AVAILABLE." ";
            }
        }
        if(isset($options['section']) && $options['section'] == 'new')
        {
            $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
            $sql_str .=  " AND SC.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";
            $sql_str .=  " AND SC.status & ".ScoreCard::ACTIVATED." <> ".ScoreCard::ACTIVATED." ";
        }
        if(isset($options['section']) && $options['section'] == 'admin')
        {
            if(isset($options['page']))
            {
                if($options['page'] == 'edit')
                {
                   $sql_str .=  " AND SC.status & ".ScoreCard::AVAILABLE." <> ".ScoreCard::AVAILABLE." ";
                } elseif($options['page'] == 'unconfirm'){
                   $sql_str .=  " AND A.status & ".Action::COMPLETED." = ".Action::COMPLETED." ";
                }
            }
        }
        //$sql_str .= " GROUP BY A.id ";
        if(isset($options['limit']) && !empty($options['limit']))
        {
            //$sql_str .=  " LIMIT ".(int)$options['start'].",".(int)$options['limit'];
        }
      return $sql_str;
	}

    function getActionDetail($id)
    {
        $result = $this->db->getRow("SELECT A.*, A.id AS action_id, CONCAT(AO.tkname, ' ', AO.tksurname) AS action_owner, A.name AS action_name,
                                     A.deliverable AS action_deliverable, S.name AS action_status, A.progress AS action_progress, date_format(A.deadline_date, '%d-%b-%Y') AS action_deadline_date,
                                     date_format(A.remind_on, '%d-%b-%Y') AS action_remind_on, A.remind_on, A.deadline_date,
                                     date_format(A.action_on, '%d-%b-%Y') AS action_on, SC.id AS score_card_id
                                     FROM #_".static::$table." A
                                     INNER JOIN #_deliverable D ON D.id = A.deliverable_id
                                     INNER JOIN #_score_card SC ON SC.id = D.score_card_id
                                     INNER JOIN #_action_status S ON S.id = A.status_id
                                     LEFT JOIN assist_".$_SESSION['cc']."_timekeep AO ON AO.tkid = A.owner_id
                                     WHERE A.id = '".$id."' ");
        return $result;
    }

	function getActions($option_sql = '')
	{
        $results = $this->db->get("SELECT A.*, A.id AS action_id, CONCAT(AO.tkname, ' ', AO.tksurname) AS action_owner, A.name AS action_name,
                                   A.deliverable AS action_deliverable, S.name AS action_status, A.progress AS action_progress, date_format(A.deadline_date, '%d-%b-%Y') AS action_deadline_date,
                                   date_format(A.remind_on, '%d-%b-%Y') AS action_remind_on, A.remind_on, A.deadline_date,
                                   date_format(A.action_on, '%d-%b-%Y') AS action_on, SC.id AS score_card_id
                                   FROM #_".static::$table." A
                                   INNER JOIN #_deliverable D ON D.id = A.deliverable_id
                                   INNER JOIN #_score_card SC ON SC.id = D.score_card_id
                                   LEFT JOIN #_action_status S ON S.id = A.status_id
                                   LEFT JOIN assist_".$_SESSION['cc']."_timekeep AO ON AO.tkid = A.owner_id
                                   WHERE 1 ".$option_sql);
        return $results;
	}

    function getQuestionWithAssessment($option_sql)
    {
        $results = $this->db->get("SELECT A.*, A.id AS action_id, CONCAT(AO.tkname, ' ', AO.tksurname) AS action_owner, A.name AS action_name,
                                   A.deliverable AS action_deliverable, S.name AS action_status, A.progress AS action_progress, date_format(A.deadline_date, '%d-%b-%Y') AS action_deadline_date,
                                   date_format(A.remind_on, '%d-%b-%Y') AS action_remind_on, A.remind_on, A.deadline_date, date_format(A.action_on, '%d-%b-%Y') AS action_on, SC.id AS score_card_id,
                                   AA.id AS assessment_id, AA.rating_id,  AA.rating_id AS rating, AA.comment AS rating_comment, AA.corrective_action, CONCAT(TK.tkname, ' ', TK.tksurname) AS owner, AA.action_owner AS asessment_action_owner,
                                   SCS.are_corrective_action_owner_identified, SCS.use_corrective_action, SCS.use_rating_comment, SCS.is_movement_tracked, AA.updated_at AS assessment_completed_at, A.parent_id
                                   FROM #_".static::$table." A
                                   INNER JOIN #_deliverable D ON D.id = A.deliverable_id
                                   INNER JOIN #_score_card SC ON SC.id = D.score_card_id
                                   INNER JOIN #_score_card_setup SCS ON SCS.id = SC.score_card_setup_id
                                   INNER JOIN #_action_status S ON S.id = A.status_id
                                   LEFT JOIN assist_".$_SESSION['cc']."_timekeep AO ON AO.tkid = A.owner_id
                                   LEFT JOIN #_action_assessment AA ON AA.action_id = A.id
                                   LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = AA.action_owner
                                   WHERE 1 ".$option_sql);
        return $results;
    }

    public function getQuestionWithAssessmentByScoreCardId($score_card_id)
    {
        $option_sql = " AND SC.id = '".$score_card_id."' ";
        $results = $this->getQuestionWithAssessment($option_sql);
        return $results;
    }

    function getTotalQuestionWithAssessment($option_sql = '')
    {
        $results = $this->db->getRow("SELECT COUNT(*) AS total
                                   FROM #_".static::$table." A
                                   INNER JOIN #_deliverable D ON D.id = A.deliverable_id
                                   INNER JOIN #_score_card SC ON SC.id = D.score_card_id
                                   INNER JOIN #_score_card_setup SCS ON SCS.id = SC.score_card_setup_id
                                   INNER JOIN #_action_status S ON S.id = A.status_id
                                   LEFT JOIN assist_".$_SESSION['cc']."_timekeep AO ON AO.tkid = A.owner_id
                                   LEFT JOIN #_action_assessment AA ON AA.action_id = A.id
                                   LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = AA.action_owner
                                   WHERE 1 ".$option_sql);
        return $results['total'];
    }

    public function getOverallRating($total_questions, $option_sql, $score_card)
    {
        $results = $this->db->get("SELECT A.rating_scale_type, AA.rating_id, AA.comment
                                   FROM #_".static::$table." A
                                   INNER JOIN #_deliverable D ON D.id = A.deliverable_id
                                   INNER JOIN #_score_card SC ON SC.id = D.score_card_id
                                   INNER JOIN #_score_card_setup SCS ON SCS.id = SC.score_card_setup_id
                                   INNER JOIN #_action_status S ON S.id = A.status_id
                                   LEFT JOIN assist_".$_SESSION['cc']."_timekeep AO ON AO.tkid = A.owner_id
                                   LEFT JOIN #_action_assessment AA ON AA.action_id = A.id
                                   LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = AA.action_owner
                                   WHERE 1 ".$option_sql);
        $numerical_rating   = 0;
        $overall_rating_str = '';
        if(!empty($results))
        {
            $ratingScaleObj = new RatingScale();
            $rating_scale_types = RatingScale::getAllRatingTypes();

            foreach($results as $result)
            {
               if(isset($rating_scale_types[$result['rating_scale_type']]))
               {
                  $n_rating = $rating_scale_types[$result['rating_scale_type']]['numerical_rating'];
                  $val      = (isset($n_rating[$result['rating_id']]) ? $n_rating[$result['rating_id']] : 0);
                  if(is_numeric($val))
                  {
                      $numerical_rating += $val;
                  }
               }
            }


            $overall_rating = ($total_questions > 0 ? ($numerical_rating/$total_questions) : 0);
            if(isset($rating_scale_types[$score_card['rating_type']]))
            {
                $overall_rating_str = $ratingScaleObj->rateScore($overall_rating, $rating_scale_types[$score_card['rating_type']], $score_card['rating_type']);
            }
        }
        return $overall_rating_str;
    }

    public static function calculateRating($results, $total_questions, $score_card)
    {
        $numerical_rating   = 0;
        $overall_rating_str = '';
        if(!empty($results))
        {
            $ratingScaleObj = new RatingScale();
            $rating_scale_types = RatingScale::getAllRatingTypes();

            foreach($results as $result)
            {
                if(isset($rating_scale_types[$result['rating_scale_type']]))
                {
                    $n_rating = $rating_scale_types[$result['rating_scale_type']]['numerical_rating'];
                    $val      = (isset($n_rating[$result['rating_id']]) ? $n_rating[$result['rating_id']] : 0);
                    if(is_numeric($val))
                    {
                        $numerical_rating += $val;
                    }
                }
            }

            $overall_rating = ($total_questions > 0 ? ($numerical_rating/$total_questions) : 0);
            if(isset($rating_scale_types[$score_card['rating_type']]))
            {
                $overall_rating_str = $ratingScaleObj->rateScore($overall_rating, $rating_scale_types[$score_card['rating_type']], $score_card['rating_type']);
            }
        }
        //echo $numerical_rating." -- ".var_dump($overall_rating_str);
        return $overall_rating_str;
    }


    function getTotalActions($option_sql = '')
    {
        $result = $this->db->getRow("SELECT COUNT(*) AS total
                                     FROM #_".static::$table." A
                                     INNER JOIN #_deliverable D ON D.id = A.deliverable_id
                                     INNER JOIN #_score_card SC ON SC.id = D.score_card_id
                                     INNER JOIN #_action_status S ON S.id = A.status_id
                                     LEFT JOIN assist_".$_SESSION['cc']."_timekeep AO ON AO.tkid = A.owner_id
                                     WHERE 1 ".$option_sql);
        return $result['total'];
    }

    public function prepareAction($actions, $total_action, $options)
    {
        $columnsObj = new ActionColumns($options['section']);
        $columns    = $columnsObj->getHeaders();
        $columns    = $columnsObj->getHeaders();

        $userObj = new User();
        $action_data['actions']      = array();
        $action_data['columns']           = array();
        $action_data['total']             = $total_action;
        $action_data['canEdit']           = array();
        $action_data['canUpdate']         = array();
        $action_data['canAddAction']      = array();
        $action_data['actions']           = array();
        $action_data['setup']             = array();
        $action_data['questions']         = array();
        $rating_position                  = 0;
        $action_data['owners']            = $userObj->getList();
        $action_data['financial_year']    = (empty($options['financial_year']) ? User::getDefaultFinancialYear() : $options['financial_year']);
        $template_empty_fields            = array();
        if($options['section'] == 'template') $template_empty_fields = array('action_owner' => '', 'action_progress' => '0.00%', 'action_on' => '', 'action_status' => 'New', 'action_deadline_date' => '', 'action_remind_on' => '');
        $ratingScaleObj = new RatingScale();
        $rating_scale_types = RatingScale::getRatingScaleTypes();
        //debug($rating_scale_types);
        foreach($actions as $index => $action)
        {
            $action_data['setup'][$action['id']] = $action;
            $action_data['heading_questions'][$action['deliverable_id']][$action['id']] = $action;
            $action_data['canEdit'][$action['id']]           = true;
            $action_data['canUpdate'][$action['id']]         = true;
            $action_data['canAddAction'][$action['id']]      = true;
            if(isset($options['section']) && $options['section'] == 'admin')
            {
                if(($action['status'] & Action::COMPLETED) == Action::COMPLETED)
                {
                  $action_data['canUndoApprove'][$action['id']] = true;
                  $action_data['canUpdate'][$action['id']]      = false;
                }
            }
            if(isset($action['rating_id']) && isset($action['rating_comment']))
            {
                $rating_list = $ratingScaleObj->getRatingsByType($action['rating_scale_type']);
                $rating_str  = (isset($rating_list[$action['rating_id']])  ? $rating_list[$action['rating_id']] : '');
/*                $action_data['assessments'][$action['id']] = array('rating'            => $rating_str,
                                                                   'comment'           => $action['rating_comment'],
                                                                   'action_owner'      => $action['owner'],
                                                                   'corrective_action' => $action['corrective_action']
                                                                   );*/
            }
            $i = 0;
            foreach($columns as $field => $val)
            {
                $i++;
                if(array_key_exists($field, $template_empty_fields))
                {
                    $action_data['actions'][$action['id']][$field] = $template_empty_fields[$field];
                    $action_data['columns'][$field]                = $val;
                } else {
                    if(isset($action[$field]))
                    {
                        if($field == 'action_id')
                        {
                            $action_data['questions'][$action['deliverable_id']][$action['id']][$field] = Action::REFTAG."".$action[$field];
                            $action_data['actions'][$action['id']][$field] = Action::REFTAG."".$action[$field];
                            $action_data['columns'][$field]                    = $val;
                        } elseif($field == 'rating_scale_type') {
                            $action_data['questions'][$action['deliverable_id']][$action['id']][$field] = (isset($rating_scale_types[$action[$field]]) ? $rating_scale_types[$action[$field]]['name'] : '');
                            $action_data['actions'][$action['id']][$field] = (isset($rating_scale_types[$action[$field]]) ? $rating_scale_types[$action[$field]]['name'] : '');
                            $action_data['columns'][$field]                    = $val;
                        } elseif($field == 'action_progress') {
                            $action_data['questions'][$action['deliverable_id']][$action['id']][$field] = ASSIST_HELPER::format_percent($action['progress'], 2);
                            $action_data['actions'][$action['id']][$field] = ASSIST_HELPER::format_percent($action['progress'], 2);
                            $action_data['columns'][$field]                = $val;
                        } elseif($field == 'rating') {
                            $rating_list = $ratingScaleObj->getRatingsByType($action['rating_scale_type']);
                            $rating_str  = (isset($rating_list[$action['rating']])  ? $rating_list[$action['rating']] : '');

                            $action_data['questions'][$action['deliverable_id']][$action['id']][$field] = $rating_str;
                            $action_data['actions'][$action['id']][$field]                              = $rating_str;
                            $action_data['columns'][$field]                                             = $val;
                            $rating_position   = $i;
                        } elseif(in_array($field, array('action_deliverable', 'action_name')) ){
                            $shortDescription = "";
                            if(isTextTooLong($action[$field]))
                            {
                               $shortDescription = cutString($action[$field], $action['id']."_".$field);
                            } else {
                               $shortDescription = $action[$field];
                            }
                            //$shortDescription = $action[$field];
                            $action_data['questions'][$action['deliverable_id']][$action['id']][$field] = $shortDescription;
                            $action_data['actions'][$action['id']][$field] = $shortDescription;
                            $action_data['columns'][$field]                    = $val;
                        } else {
                            $action_data['questions'][$action['deliverable_id']][$action['id']][$field] = $action[$field];
                            $action_data['actions'][$action['id']][$field] = $action[$field];
                            $action_data['columns'][$field]                    = $val;
                        }
                    }
                }
            }
        }
        $total_columns                = count($action_data['columns']);
        $action_data['columns']       = ($total_columns == 0 ? $columns : $action_data['columns']);
        $action_data['total_columns'] = ($total_columns == 0 ? count($columns) : $total_columns);
        $action_data['rating_position'] = $rating_position;
        $action_data['action_detail'] = $actions;
        //$rating_list = $ratingScaleObj->getRatingsByType($action['rating_scale_type']);
        //$action_data['overall_rating_str'] = ($total_columns == 0 ? count($columns) : $total_columns);
        return $action_data;
    }

    public function getActionsByDeliverableIds($deliverable_ids)
    {
        $results = $this->getActions(" AND A.deliverable_id IN (".implode(',', $deliverable_ids).")");
        return $results;
    }

    public function getActionsByDeliverableId($deliverable_id)
    {
       $results = $this->db->get("SELECT * FROM #_".static::$table." WHERE deliverable_id = '".$deliverable_id."' ");
       return $results;
    }

    public function getActionsByScoreCardId($score_card_id)
    {
        $option_sql = '';
        if(!empty($score_card_id)) $option_sql = " AND SC.id = '".$score_card_id."'  ";
        $results = $this->getActions($option_sql);
        return $results;
    }

    public function getActionsByScoreCardIdAndOwnerId($score_card_id, $owner_id)
    {
        $option_sql = '';
        $option_sql = " AND SC.id = '".$score_card_id."'  AND A.owner_id = '".$owner_id."' ";
        $results = $this->getActions($option_sql);
        return $results;
    }


    function getActionsListByScoreCardId($score_card_id)
    {
        $results = $this->getActionsByScoreCardId($score_card_id);
        $list    = array();
        foreach($results as $result)
        {
           $list[$result['id']] = $result['name'];
        }
       return $list;
    }


    function getActionSummary($score_card_id = null, $deliverable_id = null, $option_sql = '')
    {
        $option_sql = '';
        if($deliverable_id) $option_sql  = " AND A.deliverable_id = '".$deliverable_id."' ";
        if($score_card_id) $option_sql  = " AND D.score_card_id = '".$score_card_id."' ";
        $result = $this->db->getRow("SELECT  COUNT(A.id) AS total_actions, AVG(A.progress) AS average_progress, SUM(A.progress) AS total_progress
                                        FROM #_action A
                                        INNER JOIN #_deliverable D ON D.id = A.deliverable_id
                                        INNER JOIN #_score_card SC ON SC.id = D.score_card_id
                                        WHERE 1 $option_sql
                                      ");
        return $result;
    }

    public static function getActionStatsSummary($score_card_id = null, $deliverable_id = null)
    {
        $option_sql = '';
        if($deliverable_id)  $option_sql  = " AND A.deliverable_id = '".$deliverable_id."' ";
        if($score_card_id)  $option_sql  = " AND D.score_card_id = '".$score_card_id."' ";
        $result = DBConnect::getInstance()->getRow("SELECT COUNT(A.id) AS total_actions, AVG(A.progress) AS average_progress, SUM(A.progress) AS total_progress
                                                    FROM #_action A
                                                    INNER JOIN #_deliverable D ON D.id = A.deliverable_id
                                                    INNER JOIN #_score_card SC ON SC.id = D.score_card_id
                                                    WHERE 1 $option_sql
                                                  ");
        return $result;
    }

    public function getDeliverableIdByActionProgress($score_card_id)
    {
        $option_sql = " AND SC.id = '".$score_card_id."' GROUP BY A.deliverable_id HAVING AVG(A.progress) > 50 ";
        $results    = $this->db->get("SELECT A.deliverable_id, COUNT(A.id) AS total_actions, AVG(A.progress) AS average_progress, SUM(A.progress) AS total_progress
                                      FROM #_action A
                                      INNER JOIN #_deliverable D ON D.id = A.deliverable_id
                                      INNER JOIN #_score_card SC ON SC.id = D.score_card_id
                                      WHERE 1 $option_sql
                                      ");
        $deliverable_ids = array();
        if(!empty($results))
        {
            foreach($results as $index => $stat)
            {
                $deliverable_ids[] = $stat['deliverable_id'];
            }
        }
        return implode(',', $deliverable_ids);
    }



    public function copySave($deliverable_id, $new_deliverable_id, $score_card, $recipient_user)
    {
        $userObj = new User();
        $users   = $userObj->getUsersList();

        $actionColumnObj = new ActionColumns();
        $headers         = $actionColumnObj->getHeaders();
        unset($headers['rating_comment']);
        unset($headers['rating']);
        unset($headers['owner']);
        unset($headers['corrective_action']);
        $actions =  $this->getActionsByDeliverableId($deliverable_id);

        $send_to_str = "";
        if(!empty($actions))
        {
            foreach($recipient_user as $user => $owner_id)
            {
                $user_actions = array();
                foreach($actions as $index => $action)
                {
                    $action_id = $action['id'];
                    unset($action['id']);
                    $action['deliverable_id'] = $new_deliverable_id;
                    $action['created_at']     = date('Y-m-d H:i:s');
                    $action['status_id']      = 1;
                    $action['status']         = 1;
                    $action['progress']       = 0;
                    $action['owner_id']       = $owner_id;
                    $action['parent_id']      = $action_id;
                    $action_id                = $this->save($action);

                    $_action['action_id'] = $action_id;
                    $_action['action_name'] = $action['name'];
                    $_action['action_deliverable'] = $action['deliverable'];
                    $_action['rating_scale_type'] = $action['rating_scale_type'];

                    $owner = (isset($users[$owner_id]) ? $users[$owner_id   ]['user'] : '');
                    $_action['action_owner'] = $owner;
                    $user_actions[$owner_id][$action_id] = $_action;

                }
            }
        }
    }

    public function copySaveQuestionToUser($deliverable_id, $new_deliverable_id, $question_users)
    {
        $actions =  $this->getActionsByDeliverableId($deliverable_id);
        if(!empty($actions))
        {
            foreach($actions as $index => $action)
            {
                if(isset($question_users[$action['id']]))
                {
                    $action_id = $action['id'];
                    unset($action['id']);
                    $action['deliverable_id'] = $new_deliverable_id;
                    $action['created_at']     = date('Y-m-d H:i:s');
                    $action['status_id']      = 1;
                    $action['status']         = 1;
                    $action['progress']       = 0;
                    $action['parent_id']      = $action_id;
                    $action['owner_id']       = $question_users[$action_id];
                    $this->save($action);
                }
            }
        }
    }

}
?>
