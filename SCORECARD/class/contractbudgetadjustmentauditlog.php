<?php
class ContractBudgetAdjustmentAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $contractAdjustmentObj = new ContractBudgetAdjustment();
        $object_data           = $contractAdjustmentObj->getContactAdjustmentDetail($where['id']);
        $changes               = array();
/*        debug($where);
        debug($post_data);
        debug($object_data);
        echo $table_name;*/
        $ref['ref'] = 'Budget Adjustment Ref '.$where['id'];
        if(isset($_REQUEST['contract_data']))
        {
            parse_str($_REQUEST['contract_data'], $contract_adjustment);
            //debug($contract_adjustment);
            if(isset($contract_adjustment['supplier_budget']))
            {
                $posted_supplier_budgets = $contract_adjustment['supplier_budget'];
                $supplier_budget_changes = $this->processSupplierBudgetChange($posted_supplier_budgets, $where['id']);
                if(!empty($supplier_budget_changes))
                {
                    $changes['supplier_budget'] = $supplier_budget_changes;
                }
                unset($contract_adjustment['suppler_budget']);
            }
            if(isset($contract_adjustment['contract_adjustment_documents']))
            {
                $posted_documents   = $contract_adjustment['contract_adjustment_documents'];
                $attachment_changes = self::processDocumentChange($posted_documents, $where['id']);
                if(!empty($attachment_changes))
                {
                    $changes['attachment'] = $attachment_changes;
                }
                unset($contract_adjustment['documents']);
            }
            if(isset($contract_adjustment['deleted_attachment']))
            {
                $posted_deleted_documents   = $contract_adjustment['deleted_attachment'];
                $deleted_attachment_changes = $this->processDeletedDocumentChange($posted_deleted_documents, $where['id']);
                if(!empty($deleted_attachment_changes))
                {
                    $changes['attachment'] = (isset($changes['attachment']) ?  array_merge($changes['attachment'], $deleted_attachment_changes) : $deleted_attachment_changes);
                }
                unset($contract_adjustment['deleted_attachment']);
            }
        }

        if(!empty($object_data))
        {
            foreach($object_data as $field => $current_val)
            {
                if(isset($post_data[$field]) && $current_val != $post_data[$field])
                {
                    if($field == 'adjustment_date')
                    {
                        $from = strtotime($current_val);
                        $to   = strtotime($post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => date('d-M-Y', $from), 'to' => date('d-M-Y', $to));
                        }
                    } else {
                        $from = (empty($current_val) ? '-' : $current_val);
                        $to   = (empty($post_data[$field]) ? '-' : $post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    }
                }
            }
        }
        if(!empty($changes))
        {
            $changes                    = array_merge($ref, $changes);
            static::$table              = $table_name."_logs";
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['adjustment_id']   = $where['id'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

    function processSupplierBudgetChange($posted_supplier_budget, $adjustment_id)
    {
        $contractBudgetAdjustmentObj = new ContractSupplierBudget();
        $results                     = $contractBudgetAdjustmentObj->getContractSupplierBudgetsByAdjustmentId($adjustment_id);
        $changes = array();
        foreach($posted_supplier_budget as $supplier_id => $budget_adjustment)
        {
           if(isset($results[$budget_adjustment['id']]))
           {
              if($results[$budget_adjustment['id']]['amount'] != $budget_adjustment['amount'])
              {
                 $changes[$supplier_id] = 'Budget for supplier '.$results[$budget_adjustment['id']]['supplier']." changed from ".$results[$budget_adjustment['id']]['amount']." to ".$budget_adjustment['amount']."\r\n\n";
              }
           }
        }
        return $changes;
    }

    static function processDocumentChange($posted_documents, $budget_adjustment_id)
    {

        $changes = array();
        foreach($posted_documents as $document_key => $document_description)
        {
            $changes[] = " Attachment ".$document_description." has been added";
        }
       return $changes;
    }

    function processDeletedDocumentChange($posted_deleted_document, $budget_adjustmnet_id)
    {
        $changes = array();
        if(!empty($posted_deleted_document))
        {
            $budgetAdjustmentDocumentObj = new ContractBudgetAdjustmentDocument();
            foreach($posted_deleted_document as $delete_index => $document_id)
            {
                $document = $budgetAdjustmentDocumentObj->getContractBudgetAdjustmentDocument($document_id);
                $changes[] = ' Attachment '.$document['description']." has been deleted ";
            }
        }
        return $changes;
    }

    public static function newAdjustment($contract_adjustment, $contract_budget_adjustments_documents, $supplier_budgets, $adjustment_id)
    {
        $contractSupplierObj = new ContractSupplier();
        $contract_suppliers  = $contractSupplierObj->getSuppliersByContractId($contract_adjustment['contract_id']);

        $changes['_adjustment'] = 'New Budget Adjustment ref #'.$adjustment_id;
        $changes['_budget_info'] = 'Budget Info '.$contract_adjustment['name'];
        $changes['_adjustment_date'] = 'Adjustment Date '.date('d-M-Y', strtotime($contract_adjustment['adjustment_date']));
        $changes['_reason_for_adjustment'] = 'Reason for Adjustment '.$contract_adjustment['reason_for_adjustment'];

        if(isset($supplier_budgets) && !empty($supplier_budgets))
        {
            foreach($supplier_budgets as $index => $supplier_budget)
            {
                $changes['_supplier_budget_'.$index] = 'Added '.ASSIST_HELPER::format_float($supplier_budget['amount'])." to ".$contract_suppliers[$supplier_budget['supplier_id']];
            }
        }
        $changes = array_merge($changes, self::processDocumentChange($contract_budget_adjustments_documents, null));
        if(!empty($changes))
        {
            //$changes                    = array_merge($adjustment_id, $changes);
            static::$table              = "contract_budget_adjustment_logs";
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }


}
