<?php
class ContractPaymentDocument extends Model
{

    static $table = 'contract_payment_document';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($payment_id = null)
    {
        $option_sql  = ' AND deleted_at IS NULL ';
        if($payment_id) $option_sql .= " AND payment_id= ".$payment_id." ";
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getContractPaymentDocument($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
        $payment_documents = $this->getAll($active);
        $list                        = array();
        foreach($payment_documents as $index => $val)
        {
            $list[$val['payment_id']] = $val['filename']." ".$val['description'];
        }
        return $list;
    }

}

?>