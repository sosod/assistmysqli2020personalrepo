<?php
class UserEvaluation extends Model
{

    static $table = 'user_evaluation_periods';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($option_sql = '')
    {
        $response = $this->db->get("SELECT UE.*, date_format(UE.open_from, '%d-%b-%Y') AS evaluation_open_from, CONCAT(TK.tkname, ' ', TK.tksurname) AS user,
                                    date_format(UE.open_to, '%d-%b-%Y') AS evaluation_open_to, SC.name AS score_card
                                    FROM #_".static::$table." UE
                                    INNER JOIN #_score_card SC ON SC.id = UE.score_card_id
                                    INNER JOIN assist_".$_SESSION['cc']."_timekeep TK  ON TK.tkid = UE.user_id
                                    WHERE UE.status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql
                                  ");
        return $response;
    }

    function getUserEvaluation($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id");
        return $response;
    }


    function getList($active = false)
    {
        $weights = $this->getAll($active);
        $list           = array();
        foreach($weights as $index => $val)
        {
            $list[$val['id']] = $val['start_date']." - ".$val['end_date'];
        }
        return $list;
    }

    function is_open($options = array())
    {
        $userObj = new User();
        $user    = $userObj -> get_user($options);
        $results = $this->db->get("SELECT EP.id, DATE_FORMAT(EP.start_date, '%d-%b-%Y') AS start_date,
                                     DATE_FORMAT(EP.end_date, '%d-%b-%Y') AS end_date,
                                     DATE_FORMAT(EP.open_from, '%d-%b-%Y') AS open_from,
                                     DATE_FORMAT(EP.open_to, '%d-%b-%Y') AS open_to, EF.name,
                                     EP.evaluation_frequency_id, EP.status, M.evaluation_frequency
                                     FROM #_list_evaluationfrequencies EF
                                     INNER JOIN #_list_evaluation_periods EP ON EF.id = EP.evaluation_frequency_id
                                     INNER JOIN #_matrix M ON M.evaluation_frequency = EP.evaluation_frequency_id
                                     WHERE EF.evaluationyear = '".$user['default_year']."' AND M.category_id = '".$user['categoryid']."'
                                    ");
        $evaluate = FALSE;
        //debug($results);
        if(!empty($results))
        {
            foreach($results as $index => $evaluation)
            {
                if(!empty($evaluation['open_from']) && !empty($evaluation['open_to']))
                {
                    $from  = strtotime($evaluation['open_from']);
                    $to    = strtotime($evaluation['open_to']);
                    $today = strtotime(date("Y-m-d"));
                    if($today >= $from && $today <= $to)
                    {
                        $modref = $_SESSION['modref'];
                        $_SESSION['evaluation_period'] = $evaluation;
                        $evaluate = TRUE;
                    }
                }
            }
        }
        return $evaluate;
    }
}

?>