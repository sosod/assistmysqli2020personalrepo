<?php
class DeliverableDocument extends Model
{

    static $table = 'deliverable_document';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($deliverable_id = null)
    {
        $option_sql  = ' AND deleted_at IS NULL ';;
        if($deliverable_id)  $option_sql .= " AND deliverable_id = ".$deliverable_id." ";
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getDeliverableDocument($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
        $deliverable_documents = $this->getAll($active);
        $list              = array();
        foreach($deliverable_documents as $index => $val)
        {
            $list[$val['action_id']] = $val['filename']." ".$val['description'];
        }
        return $list;
    }

}

?>