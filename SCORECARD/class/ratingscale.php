<?php
class RatingScale extends Model
{

    static $table = 'rating_scale';

    function __construct()
    {
        parent::__construct();
    }

    public static function getRatingScaleType($type)
    {
       $rating_types = self::getRatingScaleTypes();
       if($rating_types[$type])
       {
            return $rating_types[$type]['name'];
       }
       return ucwords(str_replace('_', ' ', $type));
    }

    public static function getRatingScaleTypes()
    {
        $rating_types['numeric'] = array('name' => 'Numeric');
        $rating_types['symbol'] = array('name' => 'Symbol');
        $rating_types['percentage'] = array('name' => 'Percentages');
        $rating_types['faces'] = array('name' => 'Faces');
        $rating_types['color'] = array('name' => 'Colour');
        $rating_types['yes_no'] = array('name' => 'Yes-No');
        $rating_types['user_defined'] = array('name' => 'User Defined');
        return $rating_types;
    }

    public static function getRatingScaleTypesList()
    {
        $list = array();
        foreach(self::getRatingScaleTypes() as $key => $rating_scale)
        {
           $list[$key] = $rating_scale['name'];
        }
       return $list;
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active) $option_sql = ' AND status <> 0';
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

    public function getRatingScale($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id");
        return $response;
    }

    public function getRatingsByType($rating_scale_type)
    {
        $response =  array();
        switch($rating_scale_type)
        {
            case "symbol":
                $symbolRatingScaleObj = new SymbolRatingScale();
                $response = $symbolRatingScaleObj->getList();
                break;
            case "percentage":
                $percentageRatingScaleObj = new PercentageRatingScale();
                $response = $percentageRatingScaleObj->getList();
                break;
            case "faces":
                $faceRatingScaleObj = new FaceRatingScale();
                $response = $faceRatingScaleObj->getList();
                break;
            case "numeric":
                $numericRatingScaleObj = new NumericalRatingScale();
                $response =  $numericRatingScaleObj->getList();
                break;
            case "color":
                $colorRatingScale = new ColorRatingScale();
                $response = $colorRatingScale->getList();
                break;
            case "yes_no":
                $yesNoRatingScale = new YesNoRatingScale();
                $response =  $yesNoRatingScale->getList();
                break;
            default:
                $numericRatingScaleObj = new NumericalRatingScale();
                $response = $numericRatingScaleObj->getList();
        }
        return $response;
    }

    public function getNumericalRatingsByType($rating_scale_type)
    {
        $response =  array();
        switch($rating_scale_type)
        {
            case "symbol":
                $symbolRatingScaleObj = new SymbolRatingScale();
                $response = $symbolRatingScaleObj->getList();
                break;
            case "percentage":
                $percentageRatingScaleObj = new PercentageRatingScale();
                $response = $percentageRatingScaleObj->getList();
                break;
            case "faces":
                $faceRatingScaleObj = new FaceRatingScale();
                $response = $faceRatingScaleObj->getList();
                break;
            case "numeric":
                $numericRatingScaleObj = new NumericalRatingScale();
                $response =  $numericRatingScaleObj->getList();
                break;
            case "color":
                $colorRatingScale = new ColorRatingScale();
                $response = $colorRatingScale->getList();
                break;
            case "yes_no":
                $yesNoRatingScale = new YesNoRatingScale();
                $response =  $yesNoRatingScale->getList();
                break;
            default:
                $numericRatingScaleObj = new NumericalRatingScale();
                $response = $numericRatingScaleObj->getList();
        }
        return $response;
    }

    public static function getAllRatingTypes()
    {
        $rating_types = array();
        $numericRatingScaleObj   = new NumericalRatingScale();
        $rating_types['numeric']['name']     = 'Numeric';
        $rating_types['numeric']['numerical_rating'] = $numericRatingScaleObj->getNumericalRating();;
        $rating_types['numeric']['list'] = $numericRatingScaleObj->getList();;

        $symbolRatingScaleObj = new SymbolRatingScale();
        $rating_types['symbol']['numerical_rating'] = $symbolRatingScaleObj->getNumericalRating();
        $rating_types['symbol']['name']             = 'Symbol';
        $rating_types['symbol']['list']             = $symbolRatingScaleObj->getList();

        $percentageRatingScaleObj = new PercentageRatingScale();
        $rating_types['percentage']['numerical_rating'] = $percentageRatingScaleObj->getNumericalRating();
        $rating_types['percentage']['name']             = 'Symbol';
        $rating_types['percentage']['list']             = $percentageRatingScaleObj->getList();

        $faceRatingScaleObj    = new FaceRatingScale();
        $rating_types['faces']['numerical_rating'] = $faceRatingScaleObj->getNumericalRating();
        $rating_types['faces']['name']             = 'Faces';
        $rating_types['faces']['list']             = $faceRatingScaleObj->getList();

        $colorRatingScale      = new ColorRatingScale();
        $rating_types['color']['numerical_rating'] = $colorRatingScale->getNumericalRating();
        $rating_types['color']['name']             = 'Colour';
        $rating_types['color']['list']             = $colorRatingScale->getList();

        $yesNoRatingScale       = new YesNoRatingScale();
        $rating_types['yes_no']['numerical_rating'] = $yesNoRatingScale->getNumericalRating();
        $rating_types['yes_no']['name']             = 'Yes-No';
        $rating_types['yes_no']['list'] = $yesNoRatingScale->getNumericalRating();

/*        $yesNoRatingScale       = new UserDefined();
        $yesNo                  =  $yesNoRatingScale->getList();
        $yesNo['name']          = 'User Defined';
        $rating_types['user_defined'] = $yesNo;*/
       return $rating_types;
    }

    function rateScore($overall_rating, $rating_scale_type, $rating_type)
    {
        $from = 0;
        $_ratings = array();
        $rating_str = "";
        foreach($rating_scale_type['numerical_rating'] as $index => $numerical_value)
        {
            //$from     = ($numerical_value > 0 ? $numerical_value - 0.5 : 0);
            //$to       = $numerical_value + 0.5;
            $to         = ($numerical_value + 0.4);
            $_ratings[$index] = array('from' => $from, 'to' => $to, 'rating_id' => $index, 'numerical_val' => $numerical_value);
            $from             = $to +  0.1;
        }
        $overall_rating = round($overall_rating, 1);
        foreach($_ratings as $index => $rating)
        {
            if($overall_rating >= $rating['from'] && $overall_rating <= $rating['to'])
            {
                $rating_str = $rating_scale_type['list'][$index];
                //echo $rating['numerical_val']."\r\n\n\n";
            }
        }
       return $rating_str;
    }


}

?>