<?php
class SetupAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $object_data  = DBConnect::getInstance()->getRow("SELECT * FROM #_".$table_name." WHERE id = '".$where['id']."' ");

        unset($object_data['created_at']);
        if(!empty($object_data))
        {
            foreach($object_data as $field => $val)
            {
                if(isset($post_data[$field]) && $val != $post_data[$field])
                {
                   if($field == 'status')
                   {
                       if($post_data[$field] == 2 && $val == 1)
                       {
                           $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deleted";
                       } else if($post_data[$field] == 0 && $val == 1) {
                           $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deactivated";
                       } else if($post_data[$field] == 1 && $val == 0) {
                           $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was activated";
                       }
                   } elseif($field == 'category_id'){
                       $supplier_categoriesObj = new SupplierCategory();
                       $categories  = $supplier_categoriesObj->getAll();
                       $from        = '';
                       $to          = '';
                       foreach($categories as $index => $category)
                       {
                          if($category['id'] == $val)
                          {
                             $from = $category['name'];
                          }
                          if($category['id'] == $post_data[$field])
                          {
                               $to = $category['name'];
                          }
                       }
                       $changes[$field] = array('from' => $from, 'to' => $to);
                   } else {
                       $changes["_".$field] = array('from' => $val, 'to' => $post_data[$field]);
                   }
                }
            }
        }
        if(!empty($changes))
        {
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['ref_id']      = $where['id'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            static::$table              = $table_name."_logs";
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

    public function saveNewObject($inserted_data, $table, $ref, $object_str)
    {
       $new_object = array();
       if(!empty($inserted_data))
       {
          $object_str = (empty($object_str) ? ucwords(str_replace('_', ' ', $table)) : $object_str);
          $new_object['_'.$table] = " New ".$object_str.", ref ".$ref." added ";
          foreach($inserted_data as $key => $val)
          {
             if(!in_array($key, array('created_at', 'status', 'insert_user')))
             {
                $new_object['_'.$key] = ucwords(str_replace('_', ' ', $key))." - ".$val;
             }
          }
       }
       if(!empty($new_object))
       {
          static::$table              = $table."_logs";
          $new_object['user']         = $_SESSION['tkn'];
          $insert_data['insert_user'] = $_SESSION['tid'];
          $insert_data['changes']     = serializeEncode($new_object);
          if(!empty($insert_data))
          {
                parent::saveData($insert_data);
          }
       }
    }

}
