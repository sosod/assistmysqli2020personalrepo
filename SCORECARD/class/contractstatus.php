<?php
class ContractStatus extends Model
{

    static $table = 'contract_status';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active)
        {
            $option_sql = ' AND status <> 0';
        }
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

    function getStatus($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
       $contract_statuses = $this->getAll($active);
       $list              = array();
        foreach($contract_statuses as $index => $val)
        {
          $list[$val['id']] = ($val['client_terminology'] == ''  ? $val['name'] : $val['client_terminology']);
        }
       return $list;
    }

    public function getContractUpdateStatusesList($contract_id)
    {
        $contract_statues = $this->getAll(true);
        $list = array();
        foreach($contract_statues as $index => $val)
        {
            if($val['id'] == 3)
            {
               if(Contract::allowToComplete($contract_id))  $list[$val['id']] = ($val['client_terminology'] == ''  ? $val['name'] : $val['client_terminology']);
            } else {
               $list[$val['id']] = ($val['client_terminology'] == ''  ? $val['name'] : $val['client_terminology']);
            }
        }
        return $list;
    }

}

?>