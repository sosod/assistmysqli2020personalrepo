<?php
class FinancialYear extends Model
{

    static $table = 'master_financialyears';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active)
        {
            $option_sql = ' AND status <> 0';
        }
        $financial_years =  $this->db->get("SELECT * FROM assist_".$_SESSION['cc']."_".static::$table." WHERE 1");
        return $financial_years;
    }

    function getFinancialYear($id)
    {
        $financial_year =  $this->db->get("SELECT * FROM assist_".$_SESSION['cc']."_".static::$table." WHERE  id = ".$id." ");
        return $financial_year;
    }

    function getList($active = false)
    {
        $financial_years = $this->getAll($active);
        $list              = array();
        foreach($financial_years as $index => $val)
        {
            $list[$val['id']] = $val['value'];
        }
        return $list;
    }

    public static function getCurrentFinancialYear()
    {
        $financial_year= DBConnect::getInstance()->getRow("SELECT * FROM assist_".$_SESSION['cc']."_".static::$table." WHERE YEAR(start_date) >= '".date('Y')."' ");
        return $financial_year;
    }

}

?>