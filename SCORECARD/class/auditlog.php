<?php
abstract class AuditLog extends Model
{

     function __construct()
     {
          parent::__construct();
     }

     abstract function notify($postData, $where, $table_name);

    public static function logs($logs, $columns)
    {
        $changes    = array();
        if(!empty($logs))
        {
            foreach($logs as $l_index => $log)
            {
                $key                         = strtotime($log['insertdate']);
                $changes[$key]['key']        = $key;
                $changes[$key]['date_loged'] = date("d-M-Y H:i:s", $key);
                $changesArr                  = @unserialize(base64_decode($log['changes']));
                if(!empty($changesArr))
                {
                    if(isset($changesArr['user']))
                    {
                        $changes[$key]['user'] = $changesArr['user'];
                        unset($changesArr['user']);
                    }
                    if(isset($changesArr['currentstatus']))
                    {
                        $changes[$key]['status'] = $changesArr['currentstatus'];
                        unset($changesArr['currentstatus']);
                    }

                    //extract other changes
                    if( isset($changesArr) && !empty($changesArr))
                    {
                        $message = self:: _getChanges($changesArr, $columns);
                        if(!empty($message))
                        {
                            $changes[$key]['changeMessage']  = $message;
                        } else {
                            unset($changes[$key]);
                        }
                    } else {
                        $changes[$key]['changeMessage'] = "";
                    }
                }
            }
        }
        return $changes;
    }

    public static function _getChanges($changesArr, $columns)
    {
        $changeMessage = "";
        foreach($changesArr as $index => $valArr)
        {
            if( $index == "user")
            {
                continue;
            } else if($index == "attachment") {
                if(is_array($valArr))
                {
                    $changeMessage .= Attachment::displayAttachments($valArr);
                } else {
                    $changeMessage .= $valArr."<br />";
                }
            } else if($index == "response") {
                if( is_array($valArr))
                {
                    if( isset($valArr['to']) && isset($valArr['from']))
                    {
                        $key_str = "";
                        if(isset($columns[$index]))
                        {
                            $key_str = $columns[$index];
                        } else {
                            $key_str = ucwords(str_replace("_", " ", $index));
                        }
                        $changeMessage .= $key_str." was changed to <span class='change'><i>".$valArr['to']."</i></span> from <span><i>".$valArr['from']."</i></span><br />";
                    } else {
                        $changeMessage .= "<span class='change'><i>".$valArr."</i></span><br />";
                    }
                } else {
                    $changeMessage .= "Added response : <span class='change'><i>".$valArr."</i></span><br />";
                }
            } else {
                if(is_array($valArr))
                {
                    if(array_key_exists("to", $valArr) && array_key_exists("from", $valArr))
                    {
                        $key_str = "";
                        if(isset($columns[$index]))
                        {
                            $key_str = $columns[$index];
                        } else {
                            $key_str = ucwords(str_replace("_", " ", $index));
                        }
                        $changeMessage .= $key_str." was changed <b>to</b> <span class='change'><i>".$valArr['to']."</i></span> <b>from</b> <span class='change'><i>".$valArr['from']."</i></span><br />";
                    } else {
                        $changeMessage .= self::_getChanges($valArr , $columns);
                    }
                } else {
                    $changeMessage .= "<span class='change'><i>".$valArr."</span></i><br />";
                }
            }
        }
        return $changeMessage;
    }

    function printLog($logs, $columns = array())
    {
        $table_html = '';
        if(!empty($logs))
        {
            foreach($logs as $index => $log)
            {
                $changes     = serializeDecode($log['changes']);
                $table_html .= "<tr>";
                if(!empty($changes))
                {
                    $current_status = '';
                    $user           = '';
                    if(isset($changes['currentstatus']))
                    {
                        $current_status = $changes['currentstatus'];
                        unset($changes['currentstatus']);
                    }
                    if(isset($changes['user']))
                    {
                        $user = $changes['user'];
                        unset($changes['user']);
                    }
                    $table_html .= "<td>".date('Y-m-d', strtotime($log['updated_at']))."</td>";
                    $table_html .= "<td>".$user."</td>";
                    $changes_str = '';
                    foreach($changes as $change_field => $change)
                    {
                        if(isset($change['from']) && isset($change['to']))
                        {
                            $field        = str_replace('id', '', $change_field);
                            $field        = ucwords(str_replace('_','', $field));
                            $changes_str .= $field." changed from ".$change['from']." to ".$change['to']."<br />";
                        }
                    }
                    $table_html .= "<td>".$changes_str."</td>";
                    $table_html .= "<td>".$current_status."</td>";
                }
                $table_html .= "</tr>";
            }
        } else {
            $table_html .= "<tr>";
            $table_html .= "<td colspan='4'>There is no activity log found.</td>";
            $table_html .= "</tr>";
        }
        return $table_html;
    }

}
?>
