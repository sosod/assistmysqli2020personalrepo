<?php
class ActionAuthorizationDocument extends Model
{

    static $table = 'action_authorization_document';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($action_id = null)
    {
        $option_sql  = ' AND deleted_at IS NULL ';;
        if($action_id)  $option_sql .= " AND authorization_id = ".$action_id." ";
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getActionAuthorizationDocument($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
        $documents = $this->getAll($active);
        $list              = array();
        foreach($documents as $index => $val)
        {
            $list[$val['action_id']] = $val['filename']." ".$val['description'];
        }
        return $list;
    }

}

?>