<?php
class AssessmentFrequency extends Model
{

    static $table = 'assessment_frequency';

    static $assessment_frequencies = array();

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false, $option_sql = '')
    {
        if($active) $option_sql .= ' AND A.status <> 0';

        $response = $this->db->get("SELECT A.* FROM #_".static::$table." A WHERE A.status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

    function getContractAssessmentFrequency($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id");
        return $response;
    }

    function getActiveAssessmentFrequency()
    {
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::ACTIVE." = ".DBConnect::ACTIVE."");
        return $response;
    }

    function getList($active = false)
    {
        $assessment_frequencies = $this->getAll($active);
        $list           = array();
        foreach($assessment_frequencies as $index => $val)
        {
            $list[$val['id']] = $val['name'];
        }
       return $list;
    }

    public static function isOpen()
    {
        $current_financial_year = FinancialYear::getCurrentFinancialYear();
        return true;
    }

}

?>