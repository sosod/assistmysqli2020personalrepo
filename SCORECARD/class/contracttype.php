<?php
class ContractType extends Model
{

    static $table = 'contract_type';

    function __construct()
    {
        parent::__construct();
    }

    function getContractTypes($active = false)
    {
        $option_sql = '';
        if($active)
        {
            $option_sql = ' AND status <> 0';
        }
        $response = $this->db->get("SELECT * FROM #_contract_type WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

    function getContractType($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_contract_type WHERE id = $id");
        return $response;
    }

    function getActiveContractType()
    {
        $response = $this->db->get("SELECT * FROM #_contract_type WHERE status & ".DBConnect::ACTIVE." = ".DBConnect::ACTIVE."");
        return $response;
    }

    function getList($active = false)
    {
       $contract_types = $this->getContractTypes($active);
       $list           = array();
        foreach($contract_types as $index => $val)
        {
            $list[$val['id']] = $val['name'];
        }
       return $list;
    }

}

?>