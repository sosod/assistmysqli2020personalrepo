<?php
class ReminderAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $reminder  = DBConnect::getInstance()->getRow("SELECT * FROM #_".$table_name." WHERE id = '".$where['id']."' ");

        unset($reminder['created_at']);
        $changes    = array();
        if(!empty($reminder))
        {
            if($reminder['score_card_days'] !== $post_data['score_card_days'])
            {
                $from                        = $reminder['score_card_days'];
                $to                          = $post_data['score_card_days'];
                $update_data['score_card_days'] = $to;
                $changes['score_card_days']    = "Score Card reminder days before deadline changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
            }
        }

        if(!empty($changes))
        {
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['reminder_id'] = $where['id'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            static::$table              = $table_name."_logs";
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

    public function saveNewObject($inserted_data, $table, $ref, $object_str)
    {
        $new_object = array();
        if(!empty($inserted_data))
        {
            $object_str = (empty($object_str) ? ucwords(str_replace('_', ' ', $table)) : $object_str);
            $new_object['_'.$table] = " New ".$object_str.", ref ".$ref." added ";
            foreach($inserted_data as $key => $val)
            {
                if(!in_array($key, array('created_at', 'status', 'insert_user')))
                {
                    $new_object['_'.$key] = ucwords(str_replace('_', ' ', $key))." - ".$val;
                }
            }
        }
        if(!empty($new_object))
        {
            static::$table              = $table."_logs";
            $new_object['user']         = $_SESSION['tkn'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($new_object);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

}
