<?php
class ScoreCardCategory extends Model
{

    static $table = 'score_card_category';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active) $option_sql = ' AND status <> 0';
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

    function getScoreCardCategory($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id");
        return $response;
    }

    function getActiveScoreCardCategory()
    {
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::ACTIVE." = ".DBConnect::ACTIVE."");
        return $response;
    }

    function getList($active = false)
    {
       $contract_categories = $this->getAll($active);
       $list           = array();
       foreach($contract_categories as $index => $val)
       {
            $list[$val['id']] = $val['name'];
       }
       return $list;
    }

}

?>