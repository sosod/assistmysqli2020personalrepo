<?php
class Reminder extends Model
{

    static $table = 'reminder';

	function __construct() 
	{
	    parent::__construct();				
	}
	
	function getReminder($id)
	{
		$result = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = '".$id."' ");
		return $result;           
	}
		
	function getAReminder()
	{
		$results = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE 1");
		return $results;
	}

	function deleteReminder( $id )
	{
	    $update_data         = array();
	    $changes            = array();
	    $notification       = $this->getReminder($id);
	    if(isset($_POST['active']))
	    {
	        if($_POST['active'] != $notification['active'] && $_POST['active'] == 0)
	        {
                $changes['active_']    = "Reminder setting deleted \r\n\n";
                $update_data['active']  = $_POST['active'];
	        }	    
	    } 
	    $res = 0;
	    if(!empty($changes))
	    {
	       $updates['reminder_id']     = $id;
	       $changes['user']            = $_SESSION['tkn'];
	       $_changes['ref_']           = "Ref #".$id." has been updated";
	       $changes                    = array_merge($_changes, $changes);
	       $updates['insert_user']      = $_SESSION['tid'];
	       $updates['changes']         = base64_encode(serialize($changes));
	       $this->db->insert('reminder_logs', $updates);
	       $res += $this->db->insertedId();
	    }
		$res = $this->update_where($update_data, array("id" => $id) );
		return $res;
	}

    function sendReminder($dbObj, $users)
    {
        $userActions         = array();
        $reminderSent        = 0;
        $sendTo              = array();
        $notification = $dbObj->mysql_fetch_one("SELECT * FROM ".$dbObj->getDBRef()."_reminder WHERE status = 1 ");
        $headers      = $this->getActionHeaders($dbObj);
        $statuses     = $this->getStatuses($dbObj);
        unset($headers['action_owner']);
        unset($headers['action_owner']);
        if(!empty($notification))
        {
            $summaryActions = array();
            $action_sql = "";
            if(isset($notification['score_card_days']))
            {
                $from       = date("Y-m-d", strtotime("+".$notification['score_card_days']." days"));
                $end_date   = date("Y-m-d", strtotime("+".$notification['score_card_days']." days"));
                $option_sql = " AND SCS.end_date =  '".$end_date."' ";
                $actions = $dbObj->mysql_fetch_all("SELECT A.*, A.id AS action_id, A.name AS action_name, A.rating_scale_type, A.deliverable AS action_deliverable, S.name AS action_status, A.progress AS action_progress, date_format(A.deadline_date, '%d-%b-%Y') AS action_deadline_date,
                                                   date_format(A.remind_on, '%d-%b-%Y') AS action_remind_on, A.remind_on, A.deadline_date, date_format(A.action_on, '%d-%b-%Y') AS action_on, SC.id AS score_card_id,
                                                   AA.rating_id AS rating, CONCAT(TK.tkname, ' ', TK.tksurname) AS owner, SCS.are_corrective_action_owner_identified, SCS.use_corrective_action, SCS.use_rating_comment, SCS.is_movement_tracked
                                                   FROM ".$dbObj->getDBRef()."_action A
                                                   INNER JOIN ".$dbObj->getDBRef()."_deliverable D ON D.id = A.deliverable_id
                                                   INNER JOIN ".$dbObj->getDBRef()."_score_card SC ON SC.id = D.score_card_id
                                                   INNER JOIN ".$dbObj->getDBRef()."_score_card_setup SCS ON SCS.id = SC.score_card_setup_id
                                                   INNER JOIN ".$dbObj->getDBRef()."_action_status S ON S.id = A.status_id
                                                   LEFT JOIN assist_".$dbObj->getCmpCode()."_timekeep AO ON AO.tkid = A.owner_id
                                                   LEFT JOIN ".$dbObj->getDBRef()."_action_assessment AA ON AA.action_id = A.id
                                                   LEFT JOIN assist_".$dbObj->getCmpCode()."_timekeep TK ON TK.tkid = AA.action_owner
                                                   WHERE 1 ".$option_sql
                                                  );

                if(!empty($actions))
                {
                    foreach($actions as $a_index => $action)
                    {
                        if(isset($users[$action['owner_id']]))
                        {
                            $action['owner_id']   = $users[$action['owner_id']]['name'];
                        }
                        //$action['rating_scale_type'] = RatingScale::getRatingScaleType($action['rating_scale_type']);
                        $summaryActions[$action['owner_id']]['actions'][$action['action_id']] = $action;
                    }
                }
               foreach($users as $user_id => $user)
                {
                    if(isset($summaryActions[$user_id]))
                    {
                        $emailObj       = new ASSIST_EMAIL_SUMMARY($user['email'], 'Scorecard Assist: Assessment Reminder', '', '', '', 'Summary Notification', 'Scorecard Assessment end date reminder', $headers, $summaryActions[$user_id]['actions'], "deadline_date");
                        if($emailObj -> sendEmail())
                        {
                            $reminderSent      += count($summaryActions[$user_id]['actions']);
                            $sendTo[$user_id]   = " reminder email sent to ".$user['email'];
                        }
                    }
                }
            }
        }
        $sendTo['totalSend'] = $reminderSent;
        return $sendTo;
    }

    function getRecieveWhat($recieve_what)
    {
        $what_str  = "";
        $today     = date("d-M-Y");
        if($recieve_what == "all_incomplete_actions")
        {
            $what_str = "  ";
        } elseif($recieve_what == "due_this_week") {
            $from     = date("Y-m-d", strtotime("+7 days"));
            $what_str = " AND STR_TO_DATE(A.deadline, '%d-%M-%Y') <=  STR_TO_DATE('".$from."', '%d-%M-%Y')
                         AND STR_TO_DATE(A.deadline, '%d-%M-%Y') >=  STR_TO_DATE('".$today."', '%d-%M-%Y')";
        } elseif($recieve_what == "overdue_or_due_this_week") {
            $to       = date("Y-m-d", strtotime("+7 days"));
            $what_str = "  AND STR_TO_DATE(A.deadline, '%d-%M-%Y') <=  STR_TO_DATE('".$to."', '%d-%M-%Y') ";
        } elseif($recieve_what == "due_today") {
            $what_str = " AND STR_TO_DATE(A.deadline, '%d-%M-%Y') =  STR_TO_DATE('".$today."', '%d-%M-%Y') ";
        } elseif($recieve_what == "overdue_or_due_today") {
            $what_str = " AND STR_TO_DATE(A.deadline, '%d-%M-%Y') <=  STR_TO_DATE('".$today."', '%d-%M-%Y') ";
        }
        return $what_str;
    }

    function getActionHeaders($dbObj)
    {
        $header_names = $dbObj -> mysql_fetch_all("SELECT * FROM ".$dbObj->getDBRef()."_naming WHERE type = 'action' ");
        $headers = array();
        foreach($header_names as $index => $header)
        {
            if( !in_array($header['name'], array('query_comments', 'action_comments', 'assurance', 'signoff')) )
            {
                $headers[$header['name']] = (!empty($header['client_terminology']) ? $header['client_terminology'] : $header['ignite_terminology']);
            }
        }
        return $headers;
    }

    function getStatuses($dbObj)
    {
        $response = $dbObj -> mysql_fetch_all("SELECT * FROM ".$dbObj->getDBRef()."_action_status WHERE status & 2 <> 2" );
        $results  = array();
        if(!empty($response))
        {
            foreach($response as $index => $status)
            {
                $results[$status['id']] = $status;
            }
        }
        return $results;
    }
}
?>
