<?php
class DeliverableColumns extends Naming
{

    public function __construct($page = '')
    {
        parent::__construct($page);
    }

    public function getColumns($option_sql = '')
    {
        $sql = $option_sql;
        if($this->section_sql)
        {
            $sql .= $this->section_sql;
        }
        $results = $this->db->get("SELECT * FROM #_".static::$table." WHERE type = 'deliverable' ".$sql."  ORDER BY position");
       return $results;
    }


}