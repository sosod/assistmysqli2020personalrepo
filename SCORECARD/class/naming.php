<?php
/**
	@author : admire
**/
class Naming extends Model
{
	/**
	 @var int
	**/
	protected $id;
	/**
	 @var char
	**/
	protected $name;
	/**
	 @var char
	**/
	protected $sla_terminology;
	/**
	 @var char
	**/
	protected $client_terminology;
	/**
	 @var int
	**/
	protected $insertuser;
	/**
	 @var int
	**/
	protected $position;
	/**
	 @var int
	**/
	protected $insert_date;

    protected $section_sql = '';
	
	protected $headerArray;

    static $table = 'naming';

    const NEW_PAGE    = 4;

    const MANAGE_PAGE = 8;
	
	function __construct($section = '')
	{
        if($section == 'new')
        {
            $this->section_sql = " AND status & ".Naming::NEW_PAGE." = ".Naming::NEW_PAGE;
        } else if($section == 'manage'){
            $this->section_sql = " AND status & ".Naming::MANAGE_PAGE." = ".Naming::MANAGE_PAGE;
        }

		parent::__construct();
	}

    function getAll()
    {
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ORDER BY position ASC");
        return $response;
    }

	function getNaming()
	{
		$response = $this -> get("SELECT * FROM #_naming WHERE 1 ORDER BY position ASC");
		$this->headerArray = $response;
		return $response;
	}
	
	function getHeaderNames()
	{
		$response = $this -> get("SELECT * FROM #_naming WHERE 1 ORDER BY position ASC");
		$this->headerArray = $response;
		return $response;
	}	

	function getName( $id )
	{
		$response = $this -> getRow("SELECT * FROM #_naming WHERE id = $id");
		return $response;
	}
	
	
	function saveNaming( $id, $name)
	{
 		$data    = $this->getName( $id );
        $logsObj = new Logs( array( "client_terminology" => $name ) , $data, "naming");
        $logsObj -> createLog();			
		$response = $this->update("naming", array( "client_terminology" => $name ), "id=$id");
		return $response;
	}

    function getColumns()
    {
        $response = $this->db->get("SELECT * FROM #_naming WHERE 1 ORDER BY position ASC");
        return $response;
    }

    function getHeaders()
    {
        $results = $this->getColumns();
        $headers  = array();
        foreach($results as $key => $val)
        {
            $headers[ $val['name'] ] = ($val['client_terminology'] == "" ? $val['sla_terminology'] : $val['client_terminology'] );
        }
        return $headers;
    }

	function getHeader()
	{
        $results = $this->getColumns();
		$headers  = array();
		foreach($results as $key => $val)
		{
			$headers[ $val['name'] ] = ($val['client_terminology'] == "" ? $val['sla_terminology'] : $val['client_terminology'] ); 
		}
		return $headers;	
	}
	
	
	
	function setHeader( $key )
	{
        $header = $this->db->getRow("SELECT * FROM #_naming WHERE name = ".$key." ");
        if(!empty($header))
        {
            return (empty($header['client_terminology']) ? $header['contract_terminology'] : $header['client_terminology']);
        }
        $header = $this->db->getRow("SELECT * FROM #_naming WHERE LIKE ".$key." ");
        if(!empty($header))
        {
            return (empty($header['client_terminology']) ? $header['contract_terminology'] : $header['client_terminology']);
        }
		return ucwords(str_replace('_', ' ', $key));
	}	
	
	function saveColumnsOrder( $activeColumns, $inactiveColumns)
    {
		$activeArray = array();
		$resInAct	 = 0;
		$resAct		 = 0;		
		foreach($activeColumns as $key => $id )
        {
			$resAct = $this->update("naming", array("active" => 1, "insertuser" => $_SESSION['tid']) ,"id = $id");
		}
		
		foreach($inactiveColumns as $key => $id )
        {
			$resInAct = $this->update("naming", array("active" => 0, "insertuser" => $_SESSION['tid']) ,"id = $id");
		}
	}

	public function setName( $key )
	{
		$header = $this->getHeader();
		if(isset($header[$key]))
        {
		   echo $header[$key];
		} else {
		   echo ucwords( str_replace("_", " ", $key));
		}
	}
	
	
}
?>