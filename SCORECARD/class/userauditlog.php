<?php
class UserAuditLog extends AuditLog
{
     
    function notify($postData, $where, $table_name)
    {
       $user     = array();
       $userObj  = new User();
       if(!empty($where))
       {
           $user_data  = $userObj->getUserSetting($where['id']);
       }
        //get all the user access constants uses in setting user access statuses
       $userAccessConstants = User::userConstants();
       $changes             = array();
       if($postData['status'] != $user_data['status'])
       { 
            foreach($userAccessConstants as $constant => $constantValue)
            {
                $constantName = ucfirst(strtolower(str_replace("_", " ",$constant)));
                  //if the user has the status alreay set , ie its already a yes, then check if its been changed to a no 
                if(($user_data['status'] & $constantValue) == $constantValue)
                {
                    if(($postData['status'] & $constantValue) != $constantValue)
                    {
                       $changes[$constant] =  $user_data['user']." ".$constantName." status has changed from <b>Yes</b> to <b>No</b> ";
                    }
                } else {
                    //if the user status constant was not already set , then it changed from No to Yes
                   if(($postData['status'] & $constantValue) == $constantValue)
                   {
                      if($constantValue == 2)
                      {
                         $changes[$constant] =  $user_data['user']." status has changed from <b>active</b> to <b>deleted</b>";
                      } else {
                         $changes[$constant] =  $user_data['user']." ".$constantName." status has changed from <b>No</b> to <b>Yes</b>";
                      }
                   }
                }
            }
       }              
       if(!empty($changes))
       { 
         $changes['user']            = $_SESSION['tkn'];
         $insert_data['ref_id']      = $where['id'];
         $insert_data['insert_user'] = $_SESSION['tid'];
         $insert_data['changes']     = serializeEncode($changes);
         static::$table              = "user_logs";
         if(!empty($insert_data))
         {
           parent::saveData($insert_data);
         }   
       }       
    }     
          
     
}
