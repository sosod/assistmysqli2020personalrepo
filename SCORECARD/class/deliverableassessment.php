<?php
class DeliverableAssessment extends Model
{

    static $table = 'deliverable_assessment';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($deliverable_id = null)
    {
        $option_sql = '';
        if($deliverable_id)  $option_sql = " AND action_id = ".$deliverable_id." ";
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getDeliverableAssessment($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($deliverable_id = null)
    {
        $deliverable_assessments = $this->getAll();
        $list              = array();
        foreach($deliverable_assessments as $index => $val)
        {
            $list[$val['id']] = array('delivered' => array(), 'quality' => array(), 'other' => array());
        }
        return $list;
    }

    public function getDeliverableAssessmentByContractAssessmentId($contract_assessment_id, $option_sql = '')
    {
        $results = $this->db->get("SELECT DA.deliverable_id, DA.delivered_comment, DDS.color AS delivered_color, DDS.score AS delivered_score, DA.quality_comment, QS.color AS quality_color, QS.score AS quality_score,
                                   DA.other_comment, OS.color AS other_color, OS.score AS other_score, date_format(DA.created_at, '%d-%b-%Y') AS assessment_created,
                                   date_format(DA.updated_at, '%d-%b-%Y') AS last_assessed_date, DA.id AS assessment_id, CA.completed_at, CA.contract_id
                                   FROM #_".static::$table." DA
                                   LEFT JOIN #_contract_assessment CA ON CA.id= DA.contract_assessment_id
                                   LEFT JOIN #_other_score OS ON OS.id = DA.other_id
                                   LEFT JOIN #_delivered_score DDS ON DDS.id = DA.delivered_id
                                   LEFT JOIN #_quality_score QS ON QS.id = DA.quality_id
                                   WHERE contract_assessment_id = '".$contract_assessment_id."' ".$option_sql);
        $deliverable_assessment = array();
        foreach($results as $index => $assessment)
        {
           $deliverable_assessment[$assessment['deliverable_id']] = $assessment;
        }
       return $deliverable_assessment;
    }

    public function getByAssessmentAndDeliverableId($contract_assessment_id, $deliverable_id)
    {
        $deliverable_assessment = $this->db->getRow("SELECT DA.id, DA.deliverable_id, DA.delivered_comment, DDS.color AS delivered_color, DDS.score AS delivered_score, DA.quality_comment, QS.color AS quality_color, QS.score AS quality_score,
                                   DA.other_comment, OS.color AS other_color, OS.score AS other_score, date_format(DA.created_at, '%d-%b-%Y') AS assessment_created,
                                   date_format(DA.updated_at, '%d-%b-%Y') AS last_assessed_date, DA.id AS assessment_id, CA.completed_at, CA.contract_id
                                   FROM #_".static::$table." DA
                                   LEFT JOIN #_contract_assessment CA ON CA.id= DA.contract_assessment_id
                                   LEFT JOIN #_other_score OS ON OS.id = DA.other_id
                                   LEFT JOIN #_delivered_score DDS ON DDS.id = DA.delivered_id
                                   LEFT JOIN #_quality_score QS ON QS.id = DA.quality_id
                                   WHERE contract_assessment_id = '".$contract_assessment_id."' AND deliverable_id = '".$deliverable_id."' ");
        return $deliverable_assessment;
    }

}

?>