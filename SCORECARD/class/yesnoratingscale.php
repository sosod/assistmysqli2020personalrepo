<?php
class YesNoRatingScale extends RatingScale
{

    static $table = 'yes_no_rating_scale';

    function __construct()
    {
        parent::__construct();
    }

    function getYesNoRatingScales()
    {
        $rating_scales = $this->getAll();
        $headers = array('id' => 'Ref', 'numeric_rating' => 'Score Assigned to Yes/No Rating', 'rating_definition' => 'Name Display on Drop Down', 'yes_no_rating_scale_description' => 'Yes-No Rating Scale Description'  , 'color' => 'Colour', 'status' => 'Status');
        $values  = array();
        foreach($rating_scales as  $rating_scale)
        {
            $values[$rating_scale['id']]['id'] = $rating_scale['id'];
            $values[$rating_scale['id']]['numeric_rating'] = $rating_scale['numerical_rating'];
            $values[$rating_scale['id']]['rating_definition'] = $rating_scale['name'];
            $values[$rating_scale['id']]['yes_no_rating_scale_description'] = $rating_scale['yes_no_rating_scale_description'];
            $values[$rating_scale['id']]['color'] = "<span style='background-color: #".$rating_scale['color']."; padding:4px 7px;'>".$rating_scale['color']."</span>";
            $values[$rating_scale['id']]['status'] = (($rating_scale['status'] & 1) == 1 ? 'Active' : 'InActive');
        }
        return array('headers' => $headers, 'values' => $values, 'rating_scales' => $rating_scales);
    }

    function getList($is_active = true)
    {
        $rating_scales = $this->getAll($is_active);
        $list = array();
        foreach($rating_scales as $rating_scale)
        {
            $list[$rating_scale['id']] = $rating_scale['name'];
        }
        return $list;
    }

    function getNumericalRating($is_active = true)
    {
        $rating_scales = $this->getAll($is_active);
        $list = array();
        foreach($rating_scales as $rating_scale)
        {
            $list[$rating_scale['id']] = $rating_scale['numerical_rating'];
        }
        return $list;
    }
}

?>