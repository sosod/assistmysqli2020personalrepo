<?php
@session_start();
if( ! ini_get('date.timezone') )
{
   date_default_timezone_set('GMT');
} 
class Attachment
{
	
	private $uploadDir   = "";
	
	private $files       = array();
	
	private $errors      = array();
	
	private $upload_type = "";
	
	//optional upload type defined by user
	private $type 		= "";
	
	function __construct($upload_type, $key = "attachment")
	{
	    $this->upload_type = $upload_type;
	    if(isset($_FILES) && !empty($_FILES))
	    {
		   $this->files  = $_FILES[$key];
	    }
	}

	function upload($uploads_session_key = "")
	{
	    if($this->_validateFile($this->files))
	    {
            $uploads_session_key = (empty($uploads_session_key) ? $this->upload_type : $uploads_session_key);
			$this->initDir();
	        $folder       = substr($this->upload_type, 0, strpos($this->upload_type, "_"));
			$name         = $this->files['name'];
			$tye          = $this->files['type'];
			$tmp          = $this->files['tmp_name'];
			$ext_arr      = explode(".", $name);
			$ext          = $ext_arr[count($ext_arr)-1];
			$file_ref     = $this->upload_type."_".date("YmdHis");
			$file         = $file_ref.".".$ext;
			$destination  = $this->uploadDir."/".$folder."/".$file;

			if(move_uploaded_file($tmp, $destination))
			{
				$_SESSION['uploads']['score_card_added'][$uploads_session_key][$file] = $name;
				$fileUploads = array();
				$files 		 = array_reverse($_SESSION['uploads']['score_card_added'][$uploads_session_key]);
				foreach($files as $key => $fValue)
				{
				   if(file_exists($this->uploadDir."/".$folder."/".$key))
				   {
					  $ref = substr($key, 0, strpos($key, '.'));
					  $ext = substr($key, strpos($key, '.')+1);
					  $fileUploads[$ref] = array('name' => $fValue, 'ref' => $ref, "key" => $key, "ext" => $ext);
				   } else {
					  unset($_SESSION['uploads']['score_card_added'][$uploads_session_key][$key]);
				   }
				}				
	
				$response = array(
							  'files'      => $fileUploads,
							  'file'       => $name,
							  'file_ref'   => $file_ref,
							  'text'       => $name.' successfully uploaded',
							  'error'      => false 
							);		
				return  $response;
			} else {
				return  array("error" => true, "text" => "Error uploading file");
			}
		} else {
			$this -> _errorMessage($this -> errors );
		}
	}
	
	private function initDir()
	{
	    try
	     { 
            $upload_type = substr($this->upload_type, 0, strpos($this->upload_type, "_"));
            @mkdir("../../files/".$_SESSION['cc']);
            if(is_dir("../../files/".$_SESSION['cc']))
            {
                if(!is_dir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']))
                {
                    
                    if(!mkdir("../../files/".$_SESSION['cc']."/".$_SESSION['modref']))
                    {
                       throw new Exception("An error occurred trying to create the module directory, please try again ");
                    } else {
                        $this->uploadDir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref'];
                        //var_dump(is_dir($this -> uploadDir."/".$this->upload_type));
                        if(!is_dir($this->uploadDir."/".$upload_type))
                        {
                            if(!is_dir($this->uploadDir))
                            {
                                if(mkdir($this->uploadDir))
                                {
                                   throw new Exception("An error occurred trying to create the main directory, please try again ");
                                }			
                            } 

                            if(is_dir($this->uploadDir))
                            {
                                if(!mkdir($this->uploadDir."/".$upload_type))
                                {
                                    if(!mkdir($this->uploadDir."/".$upload_type."/deleted"))
                                    {
                                       throw new Exception("An error occurred trying to create sub directory, please try again");
                                    }
                                   throw new Exception("An error occurred creating the sub directory, please try again ");
                                }				
                            }
                        }
                    }
                } else {
                    $this->uploadDir = "../../files/".$_SESSION['cc']."/".$_SESSION['modref'];
                    //var_dump(is_dir($this -> uploadDir."/".$this->upload_type));
                    if(!is_dir($this->uploadDir."/".$upload_type))
                    {
                        if(!is_dir($this->uploadDir))
                        {
                            if(mkdir($this->uploadDir))
                            {
                               throw new Exception("An error occurred trying to create the main directory, please try again ");
                            }			
                        } 

                        if(is_dir($this->uploadDir))
                        {
                            if(mkdir($this->uploadDir."/".$upload_type))
                            {
                                if(!mkdir($this->uploadDir."/".$upload_type."/deleted"))
                                {
                                   throw new Exception("An error occurred trying to create sub directory, please try again");
                                }
                            } else {
                               throw new Exception("An error occurred creating the sub directory, please try again ");
                            }				
                        }
                    } else {
                        if(!is_dir(@mkdir($this->uploadDir."/".$upload_type)))
                        {
                          @mkdir($this->uploadDir."/".$upload_type);
                        }
                        if(!is_dir($this->uploadDir."/".$upload_type."/deleted"))
                        {
                          @mkdir($this->uploadDir."/".$upload_type."/deleted");
                        }
                    }
                }
            } else {
                throw new Exception("There is no directory to upload files, contact administrator ");
            }
		} catch(Exception $e){
		   echo json_encode(array('text' => 'Exception : '.$e->getMessage() , "error" => true));
		   exit();
		}	
	}
	 
	private function _validateFile( $file )
	{
        if(isset($this->files['error']))
        {
            switch($this->files['error'])
            {
                case UPLOAD_ERR_OK:
                    return true;
                break;
                case UPLOAD_ERR_INI_SIZE:
                    $this->errors['ini_size'] = "Upload size exceeds maximum set";
                    return false;
                break;
                case UPLOAD_ERR_FORM_SIZE:
                    $this->errors['form_size'] = "Upload size exceeds maximum set in the form";
                    return false;
                break;
                case UPLOAD_ERR_PARTIAL:
                    $this->errors['partial']   = "Uploaded file was only partial";
                    return false;
                break;
                case UPLOAD_ERR_NO_FILE:
                    $this->errors['nofile']    = "There was no file chosen";
                    return false;
                break;
                case UPLOAD_ERR_CANT_WRITE:
                    $this->errors['cant_write'] = "Failed to write to disk";
                    return false;
                break;
                default:
                    return true;
                break;
            }
        }
	}
	
	// remove recently uploaded file from the file system
	function removeFile($file, $uploads_session_key = "")
	{
		$this->initDir();
		$folder         = substr($this->upload_type, 0, strpos($this->upload_type, "_"));
		$old_name       = $this->uploadDir."/".$folder."/".$file;
        $uploads_session_key = (!empty($uploads_session_key) ? $uploads_session_key : $this->upload_type);
		if(@unlink($old_name))
		{
            $filename = '';
		    if(isset($_SESSION['uploads']['score_card_added'][$uploads_session_key][$file]))
		    {
		        $filename = $_SESSION['uploads']['score_card_added'][$uploads_session_key][$file];
                unset($_SESSION['uploads']['score_card_added'][$uploads_session_key][$file]);
		    }
		    if(isset($_SESSION['uploads']['score_card_attachment_changes'][$file]))
		    {
                unset($_SESSION['uploads']['score_card_attachment_changes'][$file]);
		    }		    
			return array("text" => (empty($filename) ? 'File ' : $filename." attachment successfully removed"), "error" => false);
		} else {
            if(isset($_SESSION['uploads']['score_card_added'][$uploads_session_key][$file]))
            {
                $filename = $_SESSION['uploads']['score_card_added'][$uploads_session_key][$file];
                unset($_SESSION['uploads']['score_card_added'][$uploads_session_key][$file]);
                return array("text" => "File ".$filename.", successfully removed");
            } else {
                return array("text" => "An error occurred removing ".$file.", please try again");
            }
		}
	}
	
	//deletes previously saved file from the the file system and the database
	function deleteFile($file, $name, $uploads_key = "")
	{
		$this->initDir();
		$folder  = substr($this->upload_type, 0, strpos($this->upload_type, "_"));
		$attName     = "";
        $uploads_key = (empty($uploads_key) ? $this->upload_type : $uploads_key);
		$old_name    = $this->uploadDir."/".$folder."/".$file;
		$new_name    = $this->uploadDir."/".$folder."/deleted/".$file;
	    if(rename($old_name, $new_name))
	    {
		    $_SESSION['uploads']['score_card_deleted'][$uploads_key][$file] = $name;
		    return array("text" => $name." attachment successfully deleted", "error" => false); 
	    } else {
		    return array("text" => "Error occurred removing ".$name, "error" => true);
	    }		   
	}

    public static function isFile($filename, $folder = 'contract')
    {
        $dir     = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$folder."/".$filename;
        if(file_exists($dir)) return TRUE;

        return FALSE;
    }

	//check the difference made on the attachment , added, deleted from the system
	public static function processAttachmentChange($attachments, $uploaded_key, $type = "action")
	{
		$changes    = array();
		$att 	    = array();
		$dir        = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$type;
		$change_str = "";
		if(isset($attachments) && !empty($attachments))
		{
		   $att = unserialize(base64_decode($attachments)); 		
		}
		if(isset($_SESSION['uploads']['score_card_deleted']) && !empty($_SESSION['uploads']['score_card_deleted']))
		{
		    if(!empty($att))
		    {
			  foreach($att as $key => $val)
			  { 
				if(isset($_SESSION['uploads']['score_card_deleted'][$uploaded_key]))
				{  
                      //echo "Coming here with ".$val." to get it from ".$dir."/".$key."\r\n\n";				    
                    if(isset($_SESSION['uploads']['score_card_deleted'][$uploaded_key][$key]))
                    {			    
				      $changes[$key] = "Attachment <i>".$val."</i> has been deleted";
				      $change_str   .= "Attachment <i>".$val."</i> has been deleted \r\n";
				      unset($att[$key]);				      
                    }
				}
			  }
		    }
		}

		if(isset($_SESSION['uploads']['score_card_added'][$uploaded_key]) && !empty($_SESSION['uploads']['score_card_added'][$uploaded_key]))
		{
			foreach($_SESSION['uploads']['score_card_added'][$uploaded_key] as $key => $val)
			{
			   if(file_exists($dir."/".$key))
			   {			    
			      $att[$key] 	 = $val;
			      $changes[$key] = "Attachment <i>".$val."</i> has been added";  
			      $change_str   .= "Attachment <i>".$val."</i> has been added \r\n";
			   }
			}
		}
		
		if(isset($changes) && !empty($changes))
		{
			$_SESSION['uploads']['score_card_attachment_changes'] = $changes;
		}
		
		if(isset($att) && !empty($att))
		{
			$_SESSION['uploads']['attachments'] = base64_encode(serialize($att));
		}
	    return $change_str;
	}
	
	private function _errorMessage( $errorArr )
	{
		$errorsResponse = array();
		if( !empty($errorArr))
		{
			foreach($errorArr as $key => $eVal)
			{
				$errorsResponse[$key] = array("text" => $eVal, "error" => true);
			}
		}
		return $errorsResponse;
	}
	
	public static function makeAttachment($type)
	{
		$attachment = "";
		if(isset($_SESSION['uploads']) && !empty($_SESSION['uploads']))
		{
			if(isset($_SESSION['uploads'][$type]) && !empty($_SESSION['uploads'][$type]))	
			{
				$attachment = base64_encode(serialize($_SESSION['uploads'][$type]));
			}
			unset($_SESSION['uploads']);
		}
		return $attachment;
	}
	
	public static function getAttachment($attachment, $type )
	{
	   $attachments = array();
	   if( !empty($attachment))
	   {
		  $list_of_attachments	= unserialize(base64_decode($attachment));
		  if(!empty($list_of_attachments))
		  {
			foreach($list_of_attachments as $file => $name)
			{
				if(file_exists("../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$type."/".$file))
				{
					$attachments[$file] = $name;
				}
			}	
		  }
	   }
	   return $attachments;
	}	
	
	
	
    public static function getAttachmentsList($attachments, $type, $show_delete = TRUE)
    {
       //debug($attachments)
	   $dir    = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$type;
	   $li_str = "";
	   if(!empty($attachments))
	   {
		 $_attachments = unserialize(base64_decode($attachments));
		 if(!empty($_attachments))
		 {
			$li_str .= "<span id='result_message'></span><ul id='attachment_list'>";
			foreach($_attachments as $file => $name) 
			{
			    $id  = substr($file,0,strpos($file, ".")-1);
			    $_id = substr($file,0,strpos($file, "."));
			    $ext = substr($file, strpos($file, ".") + 1);		
			    if(file_exists($dir."/".$file))
			    {			        
			       $url = "controller.php?action=download&folder=".$type."&file=".$file."&content=".$ext."&mod=".$_SESSION['modref']."&company=".$_SESSION['cc']."&new=".$name."'";
			       //$url = "controller.php?action=download";
			       $li_str .= "<li id='li_".trim($_id)."' class='class_".trim($_id)."'><span id='parent_".$_id."'><a href='".$url."'>".$name."</a>&nbsp;&nbsp;
			       ";
			       if($show_delete)
			       {
                     $li_str .="<a href='#' alt='".$name."' file='".$file."' id='".$file."' title=".$_id." class='remove_attach'>Delete</a></span></li>";			        
			       }
			    }
			} 
		    $li_str .= "</ul>";
	      }
	   }
	   return $li_str;
    } 

    public static function displayAttachmentOnly($attachment, $type = "action", $list = FALSE)
    {
	   $dir     = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$type;
	   $att_str = "";
	   if(!empty($attachment))
	   {
		 $attachments = unserialize(base64_decode($attachment));
		 if(!empty($attachments))
		 {
			$att_str .= "<ul id='attachment_list'>";
			foreach($attachments as $file => $name) 
			{
			    $id  = substr($file,0,strpos($file, ".")-1);
			    $_id = substr($file,0,strpos($file, "."));
			    $ext = substr($file, strpos($file, ".") + 1);		
			    if(file_exists($dir."/".$file))
			    {
			        $url = "controller.php?action=download&folder=".$type."&file=".$file."&content=".$ext."&mod=".$_SESSION['modref']."&company=".$_SESSION['cc']."&new=".$name."'";
			       $att_str .= "<li id='_li_".trim($_id)."'><span id='parent_".$_id."'><a href='".$url."'>".$name."</a>&nbsp;&nbsp;</li>";
			    }
			} 
		    $att_str .= "</ul><p>";
	      }

	   } 
	   if($list)
	   {
	      return $att_str;
	   }
	  echo $att_str;
    }

	public static function displayAttachmentList($attached_documents, $type = "action", $list = FALSE)
	{
	   $dir     = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$type;
	   $att_str = "";
 	   $att_str .= "<p id='file_upload'></p>";
	   if(!empty($attached_documents))
	   {
			$att_str .= "<p><span id='result_message'></span><ul id='attachment_list'>";
			foreach($attached_documents as $index => $document)
			{
                $file  = $document['filename'];
                $_id = substr($document['filename'],0,strpos($file, "."));
                $ext = substr($document['filename'], strpos($document['filename'], ".") + 1);
			    if(file_exists($dir."/".$document['filename']))
			    {
			       $url = "../controllers/scorecardcontroller.php?action=download&folder=".$type."&file=".$file."&content=".$ext."&mod=".$_SESSION['modref']."&company=".$_SESSION['cc']."&new=".$document['description']."'";
			       $att_str .= "<li id='li_".trim($_id)."'><span id='parent_".$_id."'><a href='".$url."'>".$document['description']."</a>&nbsp;&nbsp;<a href='#' alt='".$document['description']."' file='".$document['description']."' id='".$file."' title=".$_id." ref=".$type." class='remove_attach'>Delete</a></span></li>";
			    }
			} 
		    $att_str  .= "</ul><p>";
	   } else {
	        $att_str .= "<p id='file_upload'>No files attached</p>";
	   }
	   if($list)
	   {
	      return $att_str;
	   }
	  echo $att_str;
	}		
	
	public static function displayAttachments($attachments, $type = "action", $list = TRUE)
	{
        $dir     = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$type;
        $att_str = "";
        if(!empty($attachments))
        {
            foreach($attachments as $file => $name) 
            {	
                if(file_exists($dir."/".$file))
                {
                    $_file_str = str_replace("Attachment", " ", $name);
                    $file_str  = substr($_file_str, 0, strpos($_file_str, 'has'));
			        $id        = substr($file,0,strpos($file, ".")-1);
			        $_id       = substr($file,0,strpos($file, "."));
			        $ext       = substr($file, strpos($file, ".") + 1);	
                    $url       = "controller.php?action=download&folder=".$type."&file=".$file."&content=".$ext."&mod=".$_SESSION['modref']."&company=".$_SESSION['cc']."&new=".trim(strip_tags($file_str))."'";
                   $att_str .= $name."&nbsp;&nbsp;<a href='".$url."'>Download</a>&nbsp;&nbsp;<br />";
                }
            }
        }
	   if($list)
	   {
	      return $att_str;
	   }
	  echo $att_str;	    
	}

    public static function displayDownloadableLinks($attachments, $type = 'action')
    {
        $dir     = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$type;
        $att_str = "";
        if(!empty($attachments))
        {
            foreach($attachments as $file_index => $file)
            {
                if(file_exists($dir."/".$file['filename']))
                {
                    $_file_str = str_replace("Attachment", " ", $file['filename']);
                    $file_str  = substr($_file_str, 0, strpos($_file_str, 'has'));
                    $id        = substr($file['filename'],0,strpos($file['filename'], ".")-1);
                    $_id       = substr($file['filename'],0,strpos($file['filename'], "."));
                    $ext       = substr($file['filename'], strpos($file['filename'], ".") + 1);
                    $url       = "../controllers/scorecardcontroller.php?action=download&folder=".$type."&file=".$file['filename']."&content=".$ext."&mod=".$_SESSION['modref']."&company=".$_SESSION['cc']."&new=".trim(strip_tags($file['description']))."'";
                    $att_str .= "&nbsp;&nbsp;<a href='".$url."'>".$file['description']."</a>&nbsp;&nbsp;<br />";
                }
            }
        }
        return $att_str;
    }


    public static function displayEditableAttachments($attached_documents, $folder = 'contract')
    {
        $dir     = "../../files/".$_SESSION['cc']."/".$_SESSION['modref']."/".$folder;
        $att_str = "";
        $att_str .= "<p id='file_upload'></p>";
        if(!empty($attached_documents))
        {
            $att_str .= "<p><span id='result_message'></span><ul id='attachment_list'>";
            foreach($attached_documents as $index => $document)
            {
                $file = $document['filename'];
                $_id  = substr($document['filename'],0,strpos($file, "."));
                $ext  = substr($document['filename'], strpos($document['filename'], ".") + 1);
                if(self::isFile($document['filename'], $folder))
                {
                    $url = "../controllers/scorecardcontroller.php?action=download&folder=".$folder."&file=".$file."&content=".$ext."&mod=".$_SESSION['modref']."&company=".$_SESSION['cc']."&new=".$document['description']."'";
                    $att_str .= "<li id='li_".trim($_id)."'><span id='".$document['id']."'><a href='".$url."'>".$document['description']."</a>&nbsp;&nbsp;<a href='#' alt='".$document['description']."' file='".$document['id']."' id='".$file."' title=".$_id." ref=".$folder." class='remove_attach'>Delete</a></span></li>";
                }
            }
            $att_str  .= "</ul><p>";
        } else {
            $att_str .= "<p id='file_upload'>No files attached</p>";
        }
        return $att_str;
    }

	public static function clear($type = "", $upload_key = "")
	{
	    if(empty($type))
	    {
           unset($_SESSION['uploads']);
	    } else {
	      if(isset($_SESSION['uploads']))
	      {
	        if(!empty($type))
	        {
	            if(isset($_SESSION['uploads'][$type]))
	            {
	                if(!empty($upload_key))
	                {
	                    if(isset($_SESSION['uploads'][$type][$upload_key]))
	                    {
	                        unset($_SESSION['uploads'][$type][$upload_key]);
	                    }
	                    unset($_SESSION['uploads'][$type]);
	                } else {
	                    unset($_SESSION['uploads'][$type]);
	                }
	            }
	        }
	      } 
	      if(isset($_SESSION['uploads']['score_card_attachment_changes']))
	      {
            unset($_SESSION['uploads']['score_card_attachment_changes']);
	      }
	      if(isset($_SESSION['uploads']['attachments']))
	      {
            unset($_SESSION['uploads']['attachments']);
	      }	      
	    }  
	} 
	
}
?>