<?php
class ContractReminder
{
  private $dbObj;
     
  private $users;
  
  private $legislations;
  
  
  private $legislationOwners = array();
     
  private $emailObj;

  function __construct($dbObj)
  {
     $this->dbObj = $dbObj;
  }   
 
  function sendReminders($users)
  {
     $contracts         = $this->getReminders();
     $contractSend      = 0;
     $contractReminders = array();
     $headers           = $this->getContractHeaders();
     $user_access       = array();
     $userContracts     = array();
     foreach($users as $user_id => $user)
     {
	     $dirAdminObj           = new Administrator($user_id);
	     $user_access[$user_id] = $dirAdminObj->getAdminAccess();
     }
     if(!empty($contracts))
     {
        foreach($contracts as $contractId => $contract)
        {
             if(!empty($user_access))
             {
                foreach($user_access as $user_id => $sub_ids)
                {
                  if(isset($sub_ids['update']))
                  {
                     if(isset($sub_ids['update'][$contract['owner_id']]))
                     {
                        $userContracts[$user_id]['contracts'][$contract['id']] = $contract;
                        $userContracts[$user_id]['user_data']             = $users[$user_id];
                     }
                  }                  
                }
             }
        }
     }
     if(!empty($userContracts))
     {
         foreach($userContracts as $user_id => $data)
         {
             $emailObj       = new ASSIST_EMAIL_SUMMARY($data['user_data']['email'], 'Contract Assist: Reminder for Contract', '', '', '', 'Contract Assist', 'Contract Reminders', $headers, $data['contracts'], "completion_date");
             if($emailObj -> sendEmail())
             {
                 $contractSend      += count($data['contracts']);
                 $contractReminders[$user_id] = " reminder email sent to ".$data['user_data']['email'];
             }
         }           
     }
     $contractReminders['totalSend'] = $contractSend;
     return $contractReminders;
  }
  
  function getReminders()
  {
     $today = date("Y-m-d");
     $results = array();
     $reminder_setting_sql = $this->getReminderSetting();
     $reminder_setting_sql = " AND C.remind_on = '".$today."' AND C.status <> 3 ".$reminder_setting_sql;
     $results  = $this->dbObj->mysql_fetch_all("SELECT DISTINCT(C.id) AS contract_id, C.id, C.risk_reference, C.reference_number, C.risk_name, C.account_code, C.owner_id, C.name AS contract_name, FN.value AS financial_year, CT.name AS contract_type, CC.name AS contract_category,
                                               PT.name AS payment_term, PF.name AS payment_frequency, CONCAT(CM.tkname, ' ', CM.tksurname) AS contract_manager,
                                               CONCAT(CA.tkname, ' ', CA.tksurname) AS contract_authorisor, T.name AS contract_template, date_format(C.completion_date, '%d-%b-%Y') AS completion_date,
                                               date_format(C.signature_of_tender, '%d-%b-%Y') AS signature_of_tender, date_format(C.signature_of_sla, '%d-%b-%Y') AS signature_of_sla,
                                               date_format(C.assessment_frequency_date, '%d-%b-%Y') AS assessment_frequency_date, date_format(C.assessment_frequency_date, '%d-%b-%Y') AS contract_assessment_frequency_start_date, date_format(C.remind_on, '%d-%b-%Y') AS remind_on,
                                               CS.name AS contract_status, CAF.name AS contract_assessment_frequency, CONCAT_WS('-', D.dirtxt, DS.subtxt) AS contract_owner, C.assessment_frequency_date AS frequency_date
                                               FROM ".$this->dbObj->getDBRef()."_contract C
                                               INNER JOIN assist_".$this->dbObj->getCmpCode()."_master_financialyears FN ON FN.id = C.financial_year_id
                                               INNER JOIN ".$this->dbObj->getDBRef()."_contract_type CT ON CT.id = C.type_id
                                               INNER JOIN ".$this->dbObj->getDBRef()."_contract_category CC ON CC.id = C.category_id
                                               LEFT JOIN ".$this->dbObj->getDBRef()."_contract_status CS ON CS.id = C.status_id
                                               LEFT JOIN ".$this->dbObj->getDBRef()."_assessment_frequency CAF ON CAF.id = C.assessment_frequency_id
                                               LEFT JOIN ".$this->dbObj->getDBRef()."_contract_payment_term PT ON PT.id = C.payment_term_id
                                               LEFT JOIN ".$this->dbObj->getDBRef()."_contract_payment_frequency PF ON PF.id = C.payment_frequency_id
                                               LEFT JOIN assist_".$this->dbObj->getCmpCode()."_timekeep CM ON CM.tkid = C.manager_id
                                               LEFT JOIN assist_".$this->dbObj->getCmpCode()."_timekeep CA ON CA.tkid = C.authorisor_id
                                               LEFT JOIN ".$this->dbObj->getDBRef()."_dir_admins DA ON DA.ref = C.owner_id
                                               LEFT JOIN ".$this->dbObj->getDBRef()."_dirsub DS ON DS.subid = C.owner_id
                                               LEFT JOIN ".$this->dbObj->getDBRef()."_dir D ON D.dirid = DS.subdirid
                                               LEFT JOIN ".$this->dbObj->getDBRef()."_contract T ON T.id = C.template_id
                                               WHERE 1  $reminder_setting_sql ");
     $contractList = array();
     foreach($results as $index => $contract)
     {
         $contractList[$contract['id']] = $contract;
     }
     return $contractList;
  }

   function getContractHeaders()
   {
    
     $header_names = $this->dbObj->mysql_fetch_all("SELECT * FROM ".$this->dbObj->getDBRef()."_naming  WHERE type = 'contract' ");
     $headers      = array();

     $except = array('attachments', 'contract_progress', 'contract_comment', 'contract_supplier', 'contract_budget', 'template_name',
                     'contract_notification_rules', 'contract_payment', 'is_contract_assessed', 'has_deliverable', 'has_sub_deliverable', 'criteria_to_assess',
                     'weight_assigned', 'deliverable_status_assessed', 'attach_a_document', 'supplier_budget'
                    );

     foreach($header_names as $index => $header)
     {
        if(!in_array($header['name'], $except))
        {
           $headers[$header['name']] = (!empty($header['client_terminology']) ? $header['client_terminology'] : $header['ignite_terminology']);
        }
     }
     return $headers;
      
   }
   
   function getReminderSetting()
   {
      $reminder     = $this->dbObj->mysql_fetch_one("SELECT * FROM ".$this->dbObj->getDBRef()."_reminder ");
      $reminder_sql = "";
      if(!empty($reminder))
      {
         $today         = date("d-M-Y");
         $days          = date("Y-m-d", strtotime("+".$reminder['contract_days']." days"));
         $reminder_sql .= " OR (STR_TO_DATE(C.completion_date, '%d-%M-%Y') >=  STR_TO_DATE('".$today."', '%d-%M-%Y')
                                 AND STR_TO_DATE(C.completion_date, '%d-%M-%Y') <=  STR_TO_DATE('".$days."', '%d-%M-%Y')
                                )  ";
      }
      return $reminder_sql;
   }
     
      
}
?>
