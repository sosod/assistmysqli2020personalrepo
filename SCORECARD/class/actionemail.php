<?php
/*require_once("../../library/class/assist_dbconn.php");
require_once("../../library/class/assist_db.php");
require_once("../../library/class/assist_helper.php");
include_once("../../library/dbconnect/dbconnect.php");*/
include_once("../../library/class/assist_email.php");
//include_once("../library/class/assist_email_summary.php");
class ActionEmail
{
    private $dbObj;

    private $emailObj;

    function __construct($db)
    {
       $this->dbObj    = $db;
    }

    public static function sendRequestApprovalEmail($action_columns, $action, $post_action_data)
    {
        $userActions  = array();
        $reminderSent = 0;
        $sendTo       = array();
        $message      = "";
        $action_email = $action_columns['action_id']." : ".$action['id']."\r\n";
        $action_email .= "Response : ".$post_action_data['response']."\r\n";
        $action_email .= $action_columns['action_name']." : ".$action['name']."\r\n";
        $action_email .= $action_columns['action_deliverable']." : ".$action['deliverable']."\r\n";
        $action_email .= $action_columns['action_owner']." : ".$action['action_owner']."\r\n";
        $action_email .= $action_columns['action_progress']." : ".$action['action_progress']."\r\n";
        $action_email .= $action_columns['action_status']." : ".$action['action_status']."\r\n";
        $action_email .= $action_columns['action_deadline_date']." : ".$action['action_deadline_date']."\r\n";
        $action_email .= $action_columns['action_remind_on']." : ".$action['action_remind_on']."\r\n\n";

        $action_email .= "Please log onto Ignite Assist in order to View further information on the Contract and or associated Actions \n";
        $to       = "anesutest@gmail.com";
        $subject  = "Requested action approval for action #".$action['id'];
        $emailObj = new ASSIST_EMAIL($to, $subject, $action_email);
        return $emailObj->sendEmail();
    }

    public static function sendActionDeclinedEmail($action_columns, $action, $post_action_data)
    {
        $message      = "";
        $action_email = $action_columns['action_id']." : ".$action['id']."\r\n";
        $action_email .= "Response : ".$post_action_data['response']."\r\n";
        $action_email .= $action_columns['action_name']." : ".$action['name']."\r\n";
        $action_email .= $action_columns['action_deliverable']." : ".$action['deliverable']."\r\n";
        $action_email .= $action_columns['action_owner']." : ".$action['action_owner']."\r\n";
        $action_email .= $action_columns['action_progress']." : ".$action['action_progress']."\r\n";
        $action_email .= $action_columns['action_status']." : ".$action['action_status']."\r\n";
        $action_email .= $action_columns['action_deadline_date']." : ".$action['action_deadline_date']."\r\n";
        $action_email .= $action_columns['action_remind_on']." : ".$action['action_remind_on']."\r\n\n";

        $action_email .= "Please log onto Ignite Assist in order to View further information on the Contract and or associated Actions \n";
        $to       = "anesutest@gmail.com";
        $subject  = "Action decline for action #".$action['id'];
        $emailObj = new ASSIST_EMAIL($to, $subject, $action_email);
        return $emailObj->sendEmail();
    }

    public static function sendActionApprovedEmail($action_columns, $action, $post_action_data)
    {
        $message      = "";
        $action_email = $action_columns['action_id']." : ".$action['id']."\r\n";
        $action_email .= "Response : ".$post_action_data['response']."\r\n";
        $action_email .= $action_columns['action_name']." : ".$action['name']."\r\n";
        $action_email .= $action_columns['action_deliverable']." : ".$action['deliverable']."\r\n";
        $action_email .= $action_columns['action_owner']." : ".$action['action_owner']."\r\n";
        $action_email .= $action_columns['action_progress']." : ".$action['action_progress']."\r\n";
        $action_email .= $action_columns['action_status']." : ".$action['action_status']."\r\n";
        $action_email .= $action_columns['action_deadline_date']." : ".$action['action_deadline_date']."\r\n";
        $action_email .= $action_columns['action_remind_on']." : ".$action['action_remind_on']."\r\n\n";

        $action_email .= "Please log onto Ignite Assist in order to View further information on the Contract and or associated Actions \n";
        $to       = "anesutest@gmail.com";
        $subject  = "Action approved for action #".$action['id'];
        $emailObj = new ASSIST_EMAIL($to, $subject, $action_email);
        return $emailObj->sendEmail();
    }
}
?>
  
