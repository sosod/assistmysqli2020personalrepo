<?php
class ContractSupplierPayment extends Model
{

    static $table = 'contract_supplier_payment';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($contract_id = null)
    {
        $option_sql  = '';
        if($contract_id)
        {
            $option_sql = " AND contract_id = ".$contract_id." ";
        }
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getStatus($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
        $contract_supplier_payment = $this->getAll($active);
        $list              = array();
        foreach($contract_supplier_payment as $index => $val)
        {
            $list[$val['contract_id']] = $val['supplier_id']." ".$val['payment'];
        }
        return $list;
    }

    function getContractSupplierPaymentByPaymentId($payment_id)
    {
        $results = $this->db->get("SELECT CP.*, date_format(CP.created_at, '%d-%b-%Y') AS created_at, date_format(CP.updated_at, '%d-%b-%Y') AS updated_at,
                                   S.name AS supplier
                                   FROM #_".static::$table." CP
                                   INNER JOIN #_contract_supplier CS ON CS.id = CP.contract_supplier_id
                                   INNER JOIN #_supplier S ON S.id = CS.supplier_id
                                   WHERE CP.payment_id = '".$payment_id." '
                                   ");
        return $results;
    }

    function getContractSupplierPaymentDetail($id)
    {
        $results = $this->db->getRow("SELECT CP.*, date_format(CP.created_at, '%d-%b-%Y') AS created_at, date_format(CP.updated_at, '%d-%b-%Y') AS updated_at
                                   FROM #_".static::$table." CP
                                   WHERE CP.id = '".$id."' ");
        return $results;
    }

    function getContractSupplierPayments($option_sql = '')
    {
        $results = $this->db->get("SELECT CP.*, date_format(CP.created_at, '%d-%b-%Y') AS created_at, date_format(CP.updated_at, '%d-%b-%Y') AS updated_at,
                                   CS.supplier_id, S.name AS supplier
                                   FROM #_".static::$table." CP
                                   INNER JOIN #_contract_supplier CS ON CS.id = CP.contract_supplier_id
                                   INNER JOIN #_supplier S ON S.id = CS.supplier_id
                                   WHERE 1 ".$option_sql);
        return $results;
    }

    function getTotalContractSupplierPayments($option_sql = '')
    {
        $result = $this->db->getRow("SELECT COUNT(*) AS total
                                   FROM #_".static::$table." CP
                                   INNER JOIN #_contract C ON C.id = CP.contract_id
                                   INNER JOIN #_supplier S ON S.id = CP.supplier_id
                                   WHERE 1 ".$option_sql);
        return $result['total'];
    }

    public function prepareContractPayment($contract_payments, $total, $options)
    {
        $payment_data['suppliers']            = array();
        $payment_data['supplier_payments']      = array();
        $payment_data['payments']      = array();
        $payment_data['payment_total']         = 0;
        $payment_data['supplier_total']       = 0;
        foreach($contract_payments as $index => $payment)
        {
            $key         = strtotime($payment['adjustment_date']) == 0 ? strtotime($payment['adjustment_date']) : strtotime($payment['created_at']);
            $supplier_id = $payment['supplier_id'];
            $payment_data['suppliers'][$supplier_id]             = $payment['supplier'];
            $payment_data['supplier_payments'][$key][$supplier_id] = $payment;
            $payment_data['payment_total']                       += $payment['amount'];
            $payment_data['supplier_total']                     += $payment['amount'];
        }
        return $payment_data;
    }

    public function getContractSupplierPaymentSummary($payment_id)
    {
        $option_sql  = " AND CP.payment_id = '".$payment_id."' ";
        $results     =  $this->getContractSupplierPayments($option_sql);
        $summary['total_payment']    = 0;
        $summary['suppliers']       = array();
        $summary['supplier_payment'] = array();
        if(!empty($results))
        {
            foreach($results as $index => $result)
            {
                $amount   = $result['amount'];
                $summary['total_payment']                          += $amount;
                $summary['suppliers'][$result['supplier_id']]       = $result['supplier'];
                $summary['supplier_payment'][$result['supplier_id']] = $amount;
            }
        }
        return $summary;
    }

    public function getContractPayment($contract_id)
    {
        $sql = "SELECT SUM(CSP.amount) AS payment FROM #_".static::$table." CSP
                INNER JOIN #_contract_supplier CS ON CS.id = CSP.contract_supplier_id
                INNER JOIN #_contract_payment CP ON CP.id = CSP.payment_id
                WHERE CS.contract_id = '".$contract_id."' AND CP.status & ".ContractPayment::DELETED." <> ".ContractPayment::DELETED."
                ";
        $result = $this->db->getRow($sql);
        return (float)$result['payment'];
    }

}

?>