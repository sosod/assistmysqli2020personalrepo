<?php
class ScoreCardActionReminder
{
    private $dbObj;

    private $emailObj;

    function __construct($db)
    {
        $this->dbObj = $db;
    }

    function sendReminders($users)
    {

        $actions = $this->getReminders();
        $headers = $this->getActionHeaders();

        $statusObj = new ActionStatus();
        $statuses  = $statusObj->getStatus();

        $rating_types = RatingScale::getRatingScaleTypes();

        $reminderSent = 0;
        $sendTo       = array();

        if(!empty($actions))
        {
            foreach($actions as $index => $val)
            {
                //if action owner exists , then send the email
                if(isset($users[$val['owner_id']]))
                {
                    //set variables
                    $emailStr = "";
                    $emailStr .= $headers['action_id'].": ".$val['id']."\r\n\n";
                    $emailStr .= $headers['action_name']." : ".$val['name']."\r\n\n";
                    $emailStr .= $headers['action_deliverable']." : ".$val['deliverable']."\r\n\n";
                    $emailStr .= $headers['rating_scale_type']." : ".(isset($rating_types[$val['rating_scale_type']]) ? $rating_types[$val['rating_scale_type']]['name'] : ucwords(str_replace('_', ' ', $val['rating_scale_type'])) )."\r\n\n";
                    //$emailStr .= $headers['status']." : ".(isset($statuses[$val['status']]) ? $statuses[$val['status']]['name'] :  "New")."\r\n\n";
                    //$emailStr .= $headers['deadline']." : ".$val['deadline']."\r\n\n";
                    /* page through each action and add to table */

                    $emailStr.="\r\nTo see all details please log onto Ignite Assist.";
                    $emailObj = new ASSIST_EMAIL();
                    $emailObj->setRecipient("anesuzina@gmail.com");
                    //$this ->  emailObj -> setRecipient($users[$val['owner']]['email']);					//REQUIRED
                    $emailObj->setSubject("Reminder for Scorecard Question ".$val['id']);				//REQUIRED
                    $emailObj->setBody($emailStr);			//REQUIRED
                    //send the email
                    if($emailObj->sendEmail())
                    {
                        $reminderSent         += 1;
                        $sendTo[$reminderSent] = " reminder send to ".$users[$val['owner_id']]['email']." for action ".$val['id'];
                    }
                }
            }
        }
        $sendTo['totalSend'] = $reminderSent;
        return $sendTo;
    }

    function getReminders()
    {
        $today = date("Y-m-d");
        $actions = $this->dbObj->mysql_fetch_all("SELECT * FROM ".$this->dbObj->getDBRef()."_action A
                                                  INNER JOIN ".$this->dbObj->getDBRef()."_deliverable D ON D.id = A.deliverable_id
                                                  INNER JOIN ".$this->dbObj->getDBRef()."_score_card SC ON SC.id = D.score_card_id
                                                  INNER JOIN ".$this->dbObj->getDBRef()."_score_card_setup S ON S.id = SC.score_card_setup_id
                                                  WHERE S.start_date  =  $today
                                                  AND A.status & 16 <> 16 ");
        return $actions;
    }

    function getActionHeaders()
    {
        $header_names = $this->dbObj->mysql_fetch_all("SELECT * FROM ".$this -> dbObj -> getDBRef()."_naming
                                                      WHERE type = 'action' ");
        $headers = array();
        foreach($header_names as $index => $header)
        {
            $headers[$header['name']] = (!empty($header['client_terminology']) ? $header['client_terminology'] : $header['ignite_terminology']);
        }
        return $headers;
    }

}
?>
  
