<?php
class ContractSupplierBudget extends Model
{

    static $table = 'contract_supplier_budget';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($contract_id = null)
    {
        $option_sql  = '';
        if($contract_id) $option_sql = " AND contract_id = ".$contract_id." ";
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getStatus($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
        $contract_supplier_budget = $this->getAll($active);
        $list              = array();
        foreach($contract_supplier_budget as $index => $val)
        {
            $list[$val['contract_id']] = $val['supplier_id']." ".$val['budget'];
        }
        return $list;
    }

    function getContractSupplierBudgetsByBudgetAdjustmentId($id)
    {
        $results = $this->db->get("SELECT CB.*, date_format(CB.created_at, '%d-%b-%Y') AS created_at, date_format(CB.updated_at, '%d-%b-%Y') AS updated_at,
                                   C.id AS contract_id, S.name AS supplier, C.name AS contract
                                   FROM #_".static::$table." CB
                                   INNER JOIN #_contract C ON C.id = CB.contract_id
                                   INNER JOIN #_supplier S ON S.id = CB.supplier_id
                                   WHERE CB.budget_adjustment_id = '".$id."' ");
        return $results;
    }

    function getContractSupplierBudgetsByAdjustmentId($id)
    {
        $results = $this->db->get("SELECT CB.*, date_format(CB.created_at, '%d-%b-%Y') AS created_at, date_format(CB.updated_at, '%d-%b-%Y') AS updated_at,
                                   S.name AS supplier
                                   FROM #_".static::$table." CB
                                   INNER JOIN #_contract_supplier CS ON CS.id = CB.contract_supplier_id
                                   INNER JOIN #_supplier S ON S.id = CS.supplier_id
                                   WHERE CB.adjustment_id = '".$id."'
                                   ");
        return $results;
    }

    function getContractSupplierBudgetDetail($id)
    {
        $results = $this->db->get("SELECT CB.*, date_format(CB.created_at, '%d-%b-%Y') AS created_at, date_format(CB.updated_at, '%d-%b-%Y') AS updated_at,
                                   C.id AS contract_id, S.name AS supplier, C.name AS contract
                                   FROM #_".static::$table." CB
                                   INNER JOIN #_contract C ON C.id = CB.contract_id
                                   INNER JOIN #_supplier S ON S.id = CB.supplier_id
                                   WHERE id = '".$id."' ");
        return $results;
    }

    function getContractSupplierBudgets($option_sql = '')
    {
        $results = $this->db->get("SELECT CB.*, date_format(CB.created_at, '%d-%b-%Y') AS created_at, date_format(CB.updated_at, '%d-%b-%Y') AS updated_at,
                                   C.id AS contract_id, S.name AS supplier, C.name AS contract, CS.supplier_id
                                   FROM #_".static::$table." CB
                                   INNER JOIN #_contract_supplier CS ON CS.id = CB.contract_supplier_id
                                   INNER JOIN #_contract C ON C.id = CS.contract_id
                                   INNER JOIN #_supplier S ON S.id = CS.supplier_id
                                   WHERE 1 ".$option_sql);
        return $results;
    }

    function getTotalContractSupplierBudgets($option_sql = '')
    {
        $result = $this->db->getRow("SELECT COUNT(*) AS total
                                   FROM #_".static::$table." CB
                                   INNER JOIN #_contract C ON C.id = CB.contract_id
                                   INNER JOIN #_supplier S ON S.id = CB.supplier_id
                                   WHERE 1 ".$option_sql);
        return $result['total'];
    }

    public function prepareContractBudget($contract_budgets, $total, $options)
    {
        $budget_data['suppliers']            = array();
        $budget_data['supplier_budgets']      = array();
        $budget_data['adjusted_budget']      = array();
        $budget_data['budget_total']         = 0;
        $budget_data['supplier_total']       = 0;
        foreach($contract_budgets as $index => $contract_budget)
        {
            $key         = strtotime($contract_budget['adjustment_date']) == 0 ? strtotime($contract_budget['adjustment_date']) : strtotime($contract_budget['created_at']);
            $supplier_id = $contract_budget['supplier_id'];
            $budget_data['suppliers'][$supplier_id]             = $contract_budget['supplier'];
            $budget_data['supplier_budgets'][$key][$supplier_id] = $contract_budget;
            $budget_data['budget_total']                       += $contract_budget['budget'];
            $budget_data['supplier_total']                     += $contract_budget['budget'];
        }
        return $budget_data;
    }

    public function getContractSupplierBudgetsSummary($adjustment_id)
    {
        $option_sql                 = " AND CB.adjustment_id = '".$adjustment_id."' ";
        $contract_supplier_budgets  =  $this->getContractSupplierBudgets($option_sql);
        $summary['total_adjustment_budget']    = 0;
        $summary['suppliers']       = array();
        $summary['supplier_budget'] = array();
        if(!empty($contract_supplier_budgets))
        {
            foreach($contract_supplier_budgets as $index => $result)
            {
                $summary['total_adjustment_budget']                += $result['amount'];
                $summary['suppliers'][$result['supplier_id']]       = $result['supplier'];
                $summary['supplier_budget'][$result['supplier_id']] = $result['amount'];
            }
        }
        return $summary;
    }

    public function getContractBudget($contract_id)
    {
        $sql = "SELECT SUM(CB.amount) AS budget FROM #_".static::$table." CB
                INNER JOIN #_contract_supplier CS ON CS.id = CB.contract_supplier_id
                LEFT JOIN #_contract_budget_adjustment CA ON CA.id = CB.adjustment_id
                WHERE CS.contract_id = '".$contract_id."' AND CA.status & ".ContractBudgetAdjustment::DELETED." <> ".ContractBudgetAdjustment::DELETED."
                ";
        $result = $this->db->getRow($sql);
        return $result['budget'];
    }

    public function getSupplierBudgetByContractId($contract_id)
    {
        $results = $this->db->get("SELECT CB.*, date_format(CB.created_at, '%d-%b-%Y') AS created_at, date_format(CB.updated_at, '%d-%b-%Y') AS updated_at,
                                   C.id AS contract_id, S.name AS supplier, C.name AS contract, CS.supplier_id
                                   FROM #_".static::$table." CB
                                   INNER JOIN #_contract_supplier CS ON CS.id = CB.contract_supplier_id
                                   INNER JOIN #_contract C ON C.id = CS.contract_id
                                   INNER JOIN #_supplier S ON S.id = CS.supplier_id
                                   WHERE CS.contract_id = '".$contract_id."' ");
        return $results;
    }
}

?>