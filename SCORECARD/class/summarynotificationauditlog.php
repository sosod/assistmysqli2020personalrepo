<?php
class SummaryNotificationAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $summary_notification  = DBConnect::getInstance()->getRow("SELECT * FROM #_".$table_name." WHERE id = '".$where['id']."' ");
        unset($summary_notification['created_at']);
        $changes     = array();
        $changes     = array();
        $userObj     = new User();
        $users 		 = $userObj->getList();
        if($summary_notification)
        {
            if((int)$summary_notification['recieve_email'] != (int)$post_data['recieve_email'])
            {
                $to                 = ((int)$post_data['recieve_email'] == 0 ? "No" : "Yes");
                $from               = ((int)$summary_notification['recieve_email'] == 0 ? "No" : "Yes");
                $changes['recieve'] = "Receive email changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
            }
            if($summary_notification['recieve_when'] != $post_data['recieve_when'])
            {
                if($summary_notification['recieve_when'] == "weekly")
                {
                    $when_str   = "Weekly";
                    if(isset($recieve_day[$summary_notification['recieve_day']]))
                    {
                        $when_str  .= "<em> on ".SummaryNotification::$recieve_days[$summary_notification['recieve_day']]."</em>";
                    }
                    $from = $when_str;
                } else {
                    $from  = "Daily";
                }
                if($post_data['recieve_when'] == "weekly")
                {
                    $when_str   = "Weekly";
                    if(isset(SummaryNotification::$recieve_days[$post_data['recieve_day']]))
                    {
                        $when_str  .= "<em> on ".SummaryNotification::$recieve_days[$post_data['recieve_day']]."</em>";
                    }
                    $to = $when_str;
                } else {
                    $to  = "Daily";
                }
                $changes['recieve_when']    = "Receive when changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
            }
            if($summary_notification['recieve_day'] != $post_data['recieve_day'])
            {
                $from = $to = "";
                if(isset(SummaryNotification::$recieve_days[$summary_notification['recieve_day']]))
                {
                    $from  = SummaryNotification::$recieve_days[$summary_notification['recieve_day']];
                }
                if(isset(SummaryNotification::$recieve_days[$post_data['recieve_day']]))
                {
                    $to  = SummaryNotification::$recieve_days[$post_data['recieve_day']];
                }
                $changes['recieve_day']    = "Receive day changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
            }
            if($summary_notification['recieve_what'] != $post_data['recieve_what'])
            {
                $to                         = SummaryNotification::$notification_types[$post_data['recieve_what']];
                $from                       = SummaryNotification::$notification_types[$summary_notification['recieve_what']];
                $changes['recieve_what']    = "Receive what changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
            }


            $changes_who = $this->processSummaryNotificationUserChange($_REQUEST['recieve_who'], $summary_notification);

            if($changes_who && !empty($changes_who))
            {
               $changes['recieve_who']    = $changes_who;
            }
        }
        if(!empty($changes))
        {
            $changes['user']                = $_SESSION['tkn'];
            $_changes['ref_']               = "Ref #".$where['id']." has been updated";
            $changes                        = array_merge($_changes, $changes);
            $insert_data['notification_id'] = $where['id'];
            $insert_data['insert_user']     = $_SESSION['tid'];
            $insert_data['changes']         = serializeEncode($changes);
            static::$table                  = $table_name."_logs";
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

    public function saveNewObject($inserted_data, $table, $ref, $object_str)
    {
        $new_object = array();
        if(!empty($inserted_data))
        {
            $object_str = (empty($object_str) ? ucwords(str_replace('_', ' ', $table)) : $object_str);
            $new_object['_'.$table] = " New ".$object_str.", ref ".$ref." added ";
            foreach($inserted_data as $key => $val)
            {
                if(!in_array($key, array('created_at', 'status', 'insert_user')))
                {
                    $new_object['_'.$key] = ucwords(str_replace('_', ' ', $key))." - ".$val;
                }
            }
        }
        if(!empty($new_object))
        {
            static::$table              = $table."_logs";
            $new_object['user']         = $_SESSION['tkn'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($new_object);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

    public function processSummaryNotificationUserChange($posted_data, $summary_notification)
    {
        $summaryNotificationUserObj = new SummaryNotificationUser();
        $summary_notification_users = $summaryNotificationUserObj->getRecipients($summary_notification['id']);

        $userObj   = new User();
        $user_list = $userObj->getList();

        $changes = "";
        foreach($summary_notification_users as $user_id => $user_name)
        {
            if(!in_array($user_id, $posted_data))
            {
                $changes["_".$user_id] = $user_name." has been deleted from the recipients ";
                $summaryNotificationUserObj->delete_where(array('summary_notification_id' => $summary_notification['id'], 'user_id' => $user_id));
            }
        }
        unset($posted_data[0]);
        foreach($posted_data as $index => $user_id)
        {
            if(!isset($summary_notification_users[$user_id]) && $user_id != '---')
            {
                $changes["_".$user_id] =  (isset($user_list[$user_id]) ? $user_list[$user_id] : 'Unspecified')." has been added to the recipients ";
                $summaryNotificationUserObj->save(array('summary_notification_id' => $summary_notification['id'], 'user_id' => $user_id));
            }
        }
        return $changes;
    }

}
