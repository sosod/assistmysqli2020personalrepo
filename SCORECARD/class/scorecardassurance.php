<?php
/**
This file manages , creates the actions
 * @author 	: admire

 **/
class ScoreCardAssurance extends Model
{
    /**
    @var int
     **/
    protected $id;
    /**
    @var int
     **/
    protected $score_card_id;
    /**
    @var char
     **/
    protected $response;
    /**
    @var int
     **/
    protected $sign_off_id;
    /**
    @var date
     **/
    protected $date_tested;
    /**
    @var int
     **/
    protected $status;

    /**
    @var date
     **/
    protected $deleted_at;
    /**
    @var date
     **/
    protected $insert_user;
    /**
    @var int
     **/
    protected $created_at;
    /**
    @var char
     **/
    protected $updated_at;

    const ACTIVE          = 1;

    const REFTAG          = 'CA';

    static $table           = 'score_card_assurance';

    function __construct()
    {
        parent::__construct();
    }

    function getList()
    {
        $sql = "SELECT * FROM #_".static::$table." WHERE 1  ";
        $results = $this->db->get($sql);
        $list    = array();
        if($results)
        {
            foreach($results as $index => $action)
            {
                $list[$action['id']] = $action['name'];
            }
        }
        return $list;
    }
    public function isValid($contract_assurance)
    {
        $errors = array();
        return TRUE;
    }


    function prepareSql($options = array())
    {
        $sql_str = '';
        if(isset($options['score_card_id']) && !empty($options['score_card_id']))
        {
            $sql_str .=  " AND CA.score_card_id = ".$options['score_card_id']." ";
        }

        $sql_str .= " AND CA.deleted_at IS NULL ";
        if(isset($options['limit']) && !empty($options['limit']))
        {
            $sql_str .=  " LIMIT ".(int)$options['start'].",".(int)$options['limit'];
        }
        return $sql_str;
    }

    function getContractAssuranceDetail($id)
    {
        $results = $this->db->getRow("SELECT CA.*, date_format(CA.date_tested, '%d-%b-%Y') AS date_tested, date_format(CA.created_at, '%d-%b-%Y') AS created_at,
                                      date_format(CA.updated_at, '%d-%b-%Y') AS updated_at, C.id AS score_card_id, C.name AS contract, CS.name AS assurance_status
                                      FROM #_".static::$table." CA
                                      INNER JOIN #_score_card C ON C.id = CA.score_card_id
                                      LEFT JOIN #_assurance_status CS ON CS.id = CA.sign_off_id
                                      WHERE CA.id = '".$id."' ");
        return $results;
    }

    function getScoreCardAssurances($option_sql = '')
    {
        $results = $this->db->get("SELECT CA.*, date_format(CA.date_tested, '%d-%b-%Y') AS date_tested, date_format(CA.created_at, '%d-%b-%Y') AS created_at,
                                   date_format(CA.updated_at, '%d-%b-%Y') AS updated_at, C.id AS score_card_id, C.name AS contract,
                                   CONCAT(TK.tkname, ' ', TK.tksurname) AS assurance_provider, CS.name AS assurance_status
                                   FROM #_".static::$table." CA
                                   INNER JOIN #_score_card C ON C.id = CA.score_card_id
                                   LEFT JOIN #_assurance_status CS ON CS.id = CA.sign_off_id
                                   LEFT JOIN assist_".$_SESSION['cc']."_timekeep TK ON TK.tkid = CA.insert_user
                                   WHERE 1 ".$option_sql);
        return $results;
    }

    function getTotalScoreCardAssurance($option_sql = '')
    {
        $result = $this->db->getRow("SELECT COUNT(*) AS total
                                     FROM #_".static::$table." CA
                                     INNER JOIN #_score_card C ON C.id = CA.score_card_id
                                     WHERE 1 ".$option_sql);
        return $result['total'];
    }

    public function prepareScoreCardAssurance($contract_assurances, $total, $options)
    {
        $assurance_data['assurances'] = array();
        $assurance_data['total']      = $total;
        $contractAssuranceDocumentObj   = new ScoreCardAssuranceDocument();
        foreach($contract_assurances as $index => $assurance)
        {
            $assurance_data['assurances'][$assurance['id']]['id']           = $assurance['id'];
            $assurance_data['assurances'][$assurance['id']]['date_tested']  = $assurance['date_tested'];
            $assurance_data['assurances'][$assurance['id']]['response']  = $assurance['response'];
            $assurance_data['assurances'][$assurance['id']]['assurance_provider']  = $assurance['assurance_provider'];
            $assurance_data['assurances'][$assurance['id']]['assurance_status'] = $assurance['assurance_status'];
            $assurance_data['assurances'][$assurance['id']]['date_time']    = date('Y-m-d H:i:s');
            $documents                                                        = $contractAssuranceDocumentObj->getAll($assurance['id']);
            $assurance_data['assurances'][$assurance['id']]['attachments']          = Attachment::displayDownloadableLinks($documents, 'contract');;
            $assurance_data['assurances'][$assurance['id']]['editable_attachments'] = Attachment::displayEditableAttachments($documents, 'contract');;
        }
        return $assurance_data;
    }
}
?>