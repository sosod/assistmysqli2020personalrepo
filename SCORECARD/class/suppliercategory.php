<?php
class SupplierCategory extends Model
{

    static $table = 'supplier_category';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($active = false)
    {
        $option_sql = '';
        if($active)
        {
            $option_sql = ' AND status <> 0';
        }
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::DELETE." <> ".DBConnect::DELETE." $option_sql ");
        return $response;
    }

        function getContractCategory($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id");
        return $response;
    }

    function getActiveContractCategory()
    {
        $response = $this->db->get("SELECT * FROM #_".static::$table." WHERE status & ".DBConnect::ACTIVE." = ".DBConnect::ACTIVE."");
        return $response;
    }

    function getList($active = false)
    {
       $supplier_categories = $this->getAll($active);
       $list           = array();
        foreach($supplier_categories as $index => $val)
        {
            $list[$val['id']] = $val['name'];
        }
       return $list;
    }

}

?>