<?php
class ActionAssessment extends Model
{

    static $table = 'action_assessment';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($action_id = null)
    {
        $option_sql = '';
        if($action_id)  $option_sql = " AND action_id = ".$action_id." ";
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getActionAssessment($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($action_id = null)
    {
        $action_assessments = $this->getAll();
        $list              = array();
        foreach($action_assessments as $index => $val)
        {
            $list[$val['id']] = array('delivered' => array(), 'quality' => array(), 'other' => array());
        }
        return $list;
    }

    public function getActionAssessmentByContractAssessmentId($contract_assessment_id, $option_sql = '')
    {
        $results = $this->db->get("SELECT DA.deliverable_id, DA.delivered_comment, DDS.color AS delivered_color, DDS.score AS delivered_score, DA.quality_comment, QS.color AS quality_color, QS.score AS quality_score,
                                   DA.other_comment, OS.color AS other_color, OS.score AS other_score, date_format(DA.created_at, '%d-%b-%Y') AS assessment_created,
                                   date_format(DA.updated_at, '%d-%b-%Y') AS last_assessed_date, DA.id AS assessment_id, CA.completed_at, CA.contract_id
                                   FROM #_".static::$table." DA
                                   LEFT JOIN #_contract_assessment CA ON CA.id= DA.contract_assessment_id
                                   LEFT JOIN #_other_score OS ON OS.id = DA.other_id
                                   LEFT JOIN #_delivered_score DDS ON DDS.id = DA.delivered_id
                                   LEFT JOIN #_quality_score QS ON QS.id = DA.quality_id
                                   WHERE contract_assessment_id = '".$contract_assessment_id."' ".$option_sql);
        $action_assessment = array();
        foreach($results as $index => $assessment)
        {
            $action_assessment[$assessment['deliverable_id']] = $assessment;
        }
        return $action_assessment;
    }

    public function getByAssessmentAndDeliverableId($contract_assessment_id, $action_id)
    {
        $action_assessment = $this->db->getRow("SELECT DA.id, DA.deliverable_id, DA.delivered_comment, DDS.color AS delivered_color, DDS.score AS delivered_score, DA.quality_comment, QS.color AS quality_color, QS.score AS quality_score,
                                   DA.other_comment, OS.color AS other_color, OS.score AS other_score, date_format(DA.created_at, '%d-%b-%Y') AS assessment_created,
                                   date_format(DA.updated_at, '%d-%b-%Y') AS last_assessed_date, DA.id AS assessment_id, CA.completed_at, CA.contract_id
                                   FROM #_".static::$table." DA
                                   LEFT JOIN #_contract_assessment CA ON CA.id= DA.contract_assessment_id
                                   LEFT JOIN #_other_score OS ON OS.id = DA.other_id
                                   LEFT JOIN #_delivered_score DDS ON DDS.id = DA.delivered_id
                                   LEFT JOIN #_quality_score QS ON QS.id = DA.quality_id
                                   WHERE contract_assessment_id = '".$contract_assessment_id."' AND deliverable_id = '".$action_id."' ");
        return $action_assessment;
    }

    public function getActionAssessmentReport($request, $score_card, $users, $directorates)
    {
        $questionObj = new Action();
        $questions =  $questionObj->getQuestionWithAssessmentByScoreCardId($score_card['id']);
        $questions_by_user = array();

        $rating_scale_types = RatingScale::getAllRatingTypes();

        foreach($questions as $question_id => $question)
        {
           $numerical_rating = 0;
           if(isset($rating_scale_types[$question['rating_scale_type']]))
           {
                $n_rating = $rating_scale_types[$question['rating_scale_type']]['numerical_rating'];
                $val      = (isset($n_rating[$question['rating_id']]) ? $n_rating[$question['rating_id']] : 0);
                if(is_numeric($val)) $numerical_rating = $val;
           }
           $report = array('name' => $question['name'],
                           'deliverable' => $question['deliverable'],
                           'rating' => $numerical_rating,
                           'rating_comment' => $question['rating_comment'],
                           'action_owner' => $question['action_owner'],
                           'corrective_action' => $question['corrective_action'],
                           'completed_at'      => $question['assessment_completed_at'],
                           'question_id'      => $question['id']
                           );
            $questions_by_user['questions'][$question['owner_id']][$question['deliverable_id']][] = $report;
            $questions_by_user['extra'][$question['owner_id']]['completed_at'] = $question['assessment_completed_at'];
            $questions_by_user['question_detail'][$question['owner_id']][$question_id] = $question;
        }
        //$questions_by_user['extra'][$question['owner_id']]['overall_rating'] = ;
        return $questions_by_user;
    }

    public function getLinkedQuestions($linked_score_card_id)
    {
        $questionObj = new Action();
        $linked_questions =  $questionObj->getQuestionWithAssessmentByScoreCardId($linked_score_card_id);
        $linked_question_by_parent = array();

        $rating_scale_types = RatingScale::getAllRatingTypes();


        foreach($linked_questions as $question_id => $question)
        {
            $numerical_rating = 0;
            if(isset($rating_scale_types[$question['rating_scale_type']]))
            {
                $n_rating = $rating_scale_types[$question['rating_scale_type']]['numerical_rating'];
                $val      = (isset($n_rating[$question['rating_id']]) ? $n_rating[$question['rating_id']] : 0);
                if(is_numeric($val)) $numerical_rating = $val;
            }
            $linked_question_by_parent[$question['owner_id']][$question['parent_id']] = array('name' => $question['name'],
                                                                       'deliverable' => $question['deliverable'],
                                                                       'rating' => $numerical_rating,
                                                                       'rating_comment' => $question['rating_comment'],
                                                                       'action_owner' => $question['action_owner'],
                                                                       'corrective_action' => $question['corrective_action'],
                                                                       'completed_at'      => $question['assessment_completed_at']
                                                                    );

        }
        return $linked_question_by_parent;
    }

}

?>