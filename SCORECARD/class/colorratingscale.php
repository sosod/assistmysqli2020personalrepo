<?php
class ColorRatingScale extends RatingScale
{

    static $table = 'colour_rating_scale';

    function __construct()
    {
        parent::__construct();
    }

    function getColorRatingScales()
    {
        $rating_scales = $this->getAll();
        $headers = array('id' => 'Ref', 'numeric_rating' => 'Score Assigned to Colour Rating', 'rating_definition' => 'Colour Display on Drop Down', 'colour_rating_description' => 'Colour Rating Description'  ,  'status' => 'Status');
        $values  = array();
        foreach($rating_scales as  $rating_scale)
        {
            $values[$rating_scale['id']]['id'] = $rating_scale['id'];
            $values[$rating_scale['id']]['numeric_rating'] = $rating_scale['numerical_rating'];
            $values[$rating_scale['id']]['rating_definition'] = "<span style='background-color: #".$rating_scale['color']."; padding:4px 7px;'>".$rating_scale['color']."</span>";
            $values[$rating_scale['id']]['colour_rating_description'] = $rating_scale['colour_rating_description'];
            $values[$rating_scale['id']]['status'] = (($rating_scale['status'] & 1) == 1 ? 'Active' : 'InActive');
        }
        return array('headers' => $headers, 'values' => $values, 'rating_scales' => $rating_scales);
    }

    function getList($is_active = true)
    {
        $rating_scales = $this->getAll($is_active);
        $list = array();
        foreach($rating_scales as $rating_scale)
        {
            $list[$rating_scale['id']] = "<span style='background-color: #".$rating_scale['color']."; padding:4px 7px;'>&nbsp;&nbsp;</span>";
        }
        return $list;
    }

    function getNumericalRating($is_active = true)
    {
        $rating_scales = $this->getAll($is_active);
        $list = array();
        foreach($rating_scales as $rating_scale)
        {
            $list[$rating_scale['id']] = $rating_scale['numerical_rating'];
        }
        return $list;
    }
}

?>