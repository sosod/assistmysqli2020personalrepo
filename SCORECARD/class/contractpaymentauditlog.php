<?php
class ContractPaymentAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $contractPaymentObj = new ContractPayment();
        $object_data         = $contractPaymentObj->getContractPaymentDetail($where['id']);
        $changes             = array();
        if(!empty($object_data))
        {
            $ref['ref'] = 'Payment Ref '.$where['id'];
            if(isset($_REQUEST['data']))
            {
                parse_str($_REQUEST['data'], $contract_payment);
                //debug($contract_payment);
                if(isset($contract_payment['supplier_payment']))
                {
                    $posted_supplier_payments = $contract_payment['supplier_payment'];
                    $supplier_payment_changes = $this->processSupplierPaymentChange($posted_supplier_payments, $where['id']);
                    if(!empty($supplier_payment_changes))
                    {
                        $changes['supplier_payment'] = $supplier_payment_changes;
                    }
                    unset($contract_payment['supplier_payment']);
                }
                if(isset($contract_payment['payment_documents']))
                {
                    $posted_documents   = $contract_payment['payment_documents'];
                    $attachment_changes = $this->processDocumentChange($posted_documents, $where['id']);
                    if(!empty($attachment_changes))
                    {
                        $changes['attachment'] = $attachment_changes;
                    }
                    unset($contract_payment['payment_documents']);
                }
                if(isset($contract_payment['deleted_attachment']))
                {
                    $posted_deleted_documents   = $contract_payment['deleted_attachment'];
                    $deleted_attachment_changes = $this->processDeletedDocumentChange($posted_deleted_documents, $where['id']);
                    if(!empty($deleted_attachment_changes))
                    {
                        $changes['attachment'] = (isset($changes['attachment']) ?  array_merge($changes['attachment'], $deleted_attachment_changes) : $deleted_attachment_changes);
                    }
                    unset($contract_payment['deleted_attachment']);
                }
            }
            foreach($object_data as $field => $current_val)
            {
                if(isset($post_data[$field]) && $current_val != $post_data[$field])
                {
                    if($field == 'payment_date')
                    {
                        $from = strtotime($current_val);
                        $to   = strtotime($post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => date('d-M-Y', $from), 'to' => date('d-M-Y', $to));
                        }
                    } else {
                        $from = (empty($current_val) ? '-' : $current_val);
                        $to   = (empty($post_data[$field]) ? '-' : $post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    }
                }
            }
            if(!empty($changes))
            {
                $changes                    = array_merge($ref, $changes);
                static::$table              = $table_name."_logs";
                $changes['user']            = $_SESSION['tkn'];
                $insert_data['payment_id']  = $where['id'];
                $insert_data['insert_user'] = $_SESSION['tid'];
                $insert_data['changes']     = serializeEncode($changes);
                if(!empty($insert_data))
                {
                    parent::saveData($insert_data);
                }
            }
        }
/*        debug($where);
        debug($post_data);
        debug($object_data);
        echo $table_name;*/
    }

    static function processSupplierPaymentChange($posted_supplier_payments, $payment_id)
    {
        $contractSupplierPaymentObj = new ContractSupplierPayment();
        $results                    = $contractSupplierPaymentObj->getContractSupplierPaymentByPaymentId($payment_id);
        $changes = array();
        foreach($posted_supplier_payments as $supplier_id => $supplier_payment)
        {
           if(isset($results[$supplier_payment['id']]))
           {
              if($results[$supplier_payment['id']]['amount'] != $supplier_payment['amount'])
              {
                 $changes[$supplier_id] = 'Payment for supplier '.$results[$supplier_payment['id']]['supplier']." changed from ".$results[$supplier_payment['id']]['amount']." to ".$supplier_payment['amount']."\r\n\n";
              }
           }
        }
        return $changes;
    }

    static function processDocumentChange($posted_documents, $budget_adjustment_id)
    {

        $changes = array();
        foreach($posted_documents as $document_key => $document_description)
        {
            $changes[] = " Attachment ".$document_description." has been added";
        }
       return $changes;
    }

    function processDeletedDocumentChange($posted_deleted_document, $budget_adjustmnet_id)
    {
        $changes = array();
        if(!empty($posted_deleted_document))
        {
            $budgetAdjustmentDocumentObj = new ContractBudgetAdjustmentDocument();
            foreach($posted_deleted_document as $delete_index => $document_id)
            {
                $document = $budgetAdjustmentDocumentObj->getContractBudgetAdjustmentDocument($document_id);
                $changes[] = ' Attachment '.$document['description']." has been deleted ";
            }
        }
        return $changes;
    }

    public static function newLog($post_data, $payment_documents, $contract_supplier_payment, $id, $contract_payment)
    {
        $changes    = array();
        $contractSupplierObj = new ContractSupplier();
        $contract_suppliers  = $contractSupplierObj->getSuppliersByContractId($contract_payment['contract_id']);
        $ref['ref'] = 'New Contract Payment added';
        parse_str($_REQUEST['data'], $payment_data);
        foreach($post_data as $field => $value)
        {
            $changes[$field] = ucwords(str_replace("_", " ", $field))." ".$value;
        }
        if(isset($payment_data['supplier_payment']) && !empty($payment_data['supplier_payment']))
        {
            foreach($payment_data['supplier_payment']  as $index => $supplier_payment)
            {
                $changes['_supplier_payment_'.$index] = " Added ".ASSIST_HELPER::format_float($supplier_payment['amount'])." to ".$contract_suppliers[$supplier_payment['supplier_id']];
            }
        }
        $changes = array_merge($changes, self::processDocumentChange($payment_documents, null));
        $changes = array_merge($changes, self::processSupplierPaymentChange($payment_documents, $id));
        if(!empty($changes))
        {
            $changes                    = array_merge($ref, $changes);
            static::$table              = "contract_payment_logs";
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

}
