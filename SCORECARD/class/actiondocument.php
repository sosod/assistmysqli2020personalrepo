<?php
class ActionDocument extends Model
{

    static $table = 'action_document';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($action_id = null)
    {
        $option_sql  = ' AND deleted_at IS NULL ';;
        if($action_id)  $option_sql .= " AND action_id = ".$action_id." ";
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getActionDocument($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
        $action_documents = $this->getAll($active);
        $list              = array();
        foreach($action_documents as $index => $val)
        {
            $list[$val['action_id']] = $val['filename']." ".$val['description'];
        }
        return $list;
    }

}

?>