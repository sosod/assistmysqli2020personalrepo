<?php
class SummaryNotification extends Model
{
    static $table = "summary_notification";

	function __construct() 
	{
	    parent::__construct();
	}

    public static $notification_types = array('all_incomplete_actions'   => 'All Incomplete Actions',
                                              'due_this_week'            => 'Actions due this week',
                                              'overdue_or_due_this_week' => 'Actions overdue or due this week',
                                              'due_today'                => 'Actions due today',
                                              'overdue_or_due_today'     =>  'Actions overdue or due today'
    );

    public static $recieve_days        = array(1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday');


    function getAllNotifications($options_sql = "")
    {
       $results = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$options_sql);
       return $results;
    }

	function getNotifications()
	{
        $option_sql = " AND status & 1 = 1 ";
        $notifications = $this->getAllNotifications($option_sql);

        $summaryNotificationUserObj = new SummaryNotificationUser();

		$data               = array();
		if(!empty($notifications))
		{
		   foreach($notifications as $index => $result)
		   {
              $recipient_list = $summaryNotificationUserObj->getRecipients($result['id']);
              $who            = array_keys($recipient_list);
		      $data[$result['id']]['recieve']       = $result['recieve_email'];
		      $data[$result['id']]['what']          = $result['recieve_what'];
		      $data[$result['id']]['who']           = $who;
		      $data[$result['id']]['when']          = $result['recieve_when'];
		      $data[$result['id']]['day']           = $result['recieve_day'];
		      $data[$result['id']]['status']        = $result['status'];
		      $data[$result['id']]['id']            = $result['id'];
		      $data[$result['id']]['recieve_email'] = ($result['recieve_email'] == 0 ? "No" : "Yes");
		      $data[$result['id']]['recieve_what']  = self::$notification_types[$result['recieve_what']];
		      if($result['recieve_when'] == "weekly")
		      {
		         $when_str   = "Weekly";
		         if(isset(self::$recieve_days[$result['recieve_day']]))
		         {
		            $when_str  .= "<em> on ".self::$recieve_days[$result['recieve_day']]."</em>";
		         }
		         $data[$result['id']]['recieve_when'] = $when_str;
		      } else {
		         $data[$result['id']]['recieve_when']  = "Daily";
		      }
		      
		      $data[$result['id']]['status']        = ($result['status'] == 0 ? "Inactive" : "Active");

			  $data[$result['id']]['recipients'] = $recipient_list;
			  $data[$result['id']]['recieve_who'] = implode("<br />", $recipient_list);
			  
		   }		
		}                          
		return $data;		
	}
	
	function getNotification($id)
	{
		$result = $this -> getRow("SELECT * FROM #_".static::$table." WHERE id = ".$id."
		                          ");
		$result['who'] = explode(";",$result['recieve_who']);
        $userObj       = new User();
        $user_list     = $userObj->getList();

		$result['recipients'] = array();
		foreach($result['who'] as $r) 
		{
			if(isset($users[$r]))  $result['recipients'][$r] = $user_list[$r]['user'];
		}
		$result['display_who'] = implode("<br />",$result['recipients']);
		return $result;           
	}
	
	function saveNotifications()
	{
		$insert_data = array(
				'recieve_email' 	=> $this -> recieve_email,
				'recieve_what' 		=> $this -> recieve_what,
				'recieve_when' 		=> $this -> recieve_when,				
				'recieve_day'		=> $this -> recieve_day,
				'recieve_who'		=> $this -> recieve_who,
				"insertuser"  		=> $_SESSION['tid'],				
		);
		$response = $this -> insert('summary_notifications', $insert_data );
		return $this -> insertedId();
	}

    function getNotitication($id)
    {
        $result = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = '".$id."' ");
        return $result;
    }

	function deleteNotification($id)
	{
	    $update_data         = array();
	    $changes            = array();
	    $notification       = $this->getNotitication($id);
	    if(isset($_POST['status']))
	    {
	        if($_POST['status'] != $notification['status'])
	        {
	            if($_POST['status'] == 0)
	            {
	                $changes['status_']    = "Notification setting deleted \r\n\n";
	                $updatedata['status']  = $_POST['status'];
	            }
	        }	    
	    } 
	    $res = 0;
	    if(!empty($changes))
	    {
	       $updates['notification_id'] = $id;
	       $changes['user']            = $_SESSION['tkn'];
	       $_changes['notification_id'] = "Ref #".$id." has been updated";
	       $changes                    = array_merge($_changes, $changes);
	       $updates['insert_user']      = $_SESSION['tid'];
	       $updates['changes']         = base64_encode(serialize($changes));
	       $this -> insert('summary_notification_logs', $updates);
	       $res += $this -> insertedId(); 
	    }
		$res = $this->update_where($updatedata, array("id" => $id));
		return $res;
	}
	
 
   function getSummaryNotificationActions($dbObj, $users)
   {
      $userActions         = array();
      $reminderSent        = 0; 
      $sendTo              = array();
       $summaryNotifications = $dbObj->mysql_fetch_all("SELECT * FROM ".$dbObj->getDBRef()."_summary_notification WHERE status = 1 ");
      $headers      = $this->getActionHeaders($dbObj);
      $statuses     = $this->getStatuses($dbObj);
      print "<pre>";
         print_r($summaryNotifications);
      print "</pre>";
      exit();
      if(!empty($summaryNotifications))
      {
         $summaryActions = array();
         foreach($summaryNotifications as $index => $notification)
         {
            $action_sql = "";
            if(isset($notification['recieve_email']))
            {
              if($notification['recieve_when'] == "daily")
              {
                 $action_sql .= $this->getRecieveWhat($notification['recieve_what']);
              } else if($notification['recieve_when'] == "weekly") {
                if(isset(self::$recieve_days[$notification['recieve_day']]))
                {
                   $action_sql .= $this->getRecieveWhat($notification['recieve_what']);
                }
              }
              $actions = $dbObj -> mysql_fetch_all("SELECT CONCAT('QA', A.id) AS action_ref,
                                                    A.action AS query_action,
                                                    A.deliverable, A.timescale, A.deadline,A.remindon, 
                                                    A.action_on, CONCAT(A.progress, '%') AS progress, 
                                                    A.status, A.action_status,
                                                    A.attachement, A.risk_id,
                                                    CONCAT(TK.tkname,' ',TK.tksurname) as action_owner
                                                    FROM ".$dbObj->getDBRef()."_actions A
                                                    INNER JOIN assist_".$dbObj->getCmpCode()."_timekeep TK
                                                    ON TK.tkid = A.action_owner
                                                    WHERE A.status <> 3 AND A.progress <> 100 
                                                    $action_sql 
                                                 ");
                                                        
              if(!empty($actions))
              {
                 foreach($actions as $a_index => $action)
                 {
                    if(isset($users[$action['action_owner']]))
                    {
                      $action['action_owner']   = $users[$action['action_owner']]['name'];
                    }
                    if(isset($statuses[$action['status']]))
                    {
                      $action['action_status']   = $statuses[$action['status']]['name'];
                    } else {
                      $action['action_status']   = "New";
                    }
                    $summaryActions['what']                           = self::$notification_types[$notification['recieve_what']];
                    $summaryActions['actions'][$action['action_ref']] = $action;
                 }
              }
              $who_recieve = explode(";",$notification['recieve_who']);
              if(!empty($who_recieve))
              {
                  foreach($who_recieve as $who_index => $who)
                  {
                      if(isset($users[$who]))
                      {
                         $emailObj       = new ASSIST_EMAIL_SUMMARY($users[$who]['email'], 'Contract Assist: Summary of '.$summaryActions['what'], '', '', '', 'Summary Notification', $summaryActions['what'], $headers, $summaryActions['actions'], "deadline_date");
                         if($emailObj -> sendEmail())
                         {
                             $reminderSent  += count($summaryActions['actions']);
                             $sendTo[$who]   = " reminder email sent to ".$users[$who]['email'];
                         }
                      }
                  }
              }        
            } 
         }
      }  
      $sendTo['totalSend'] = $reminderSent;      
      return $sendTo;      
   } 	
   
   function getRecieveWhat($recieve_what)
   {
       $what_str  = "";
       $today     = date("d-M-Y");  
       if($recieve_what == "all_incomplete_actions")
       {
           $what_str = "  ";
       } elseif($recieve_what == "due_this_week") {
           $from     = date("Y-m-d", strtotime("+7 days"));
           $what_str = " AND STR_TO_DATE(A.deadline, '%d-%M-%Y') <=  STR_TO_DATE('".$from."', '%d-%M-%Y') 
                         AND STR_TO_DATE(A.deadline, '%d-%M-%Y') >=  STR_TO_DATE('".$today."', '%d-%M-%Y')";
       } elseif($recieve_what == "overdue_or_due_this_week") {
           $to       = date("Y-m-d", strtotime("+7 days"));
           $what_str = "  AND STR_TO_DATE(A.deadline, '%d-%M-%Y') <=  STR_TO_DATE('".$to."', '%d-%M-%Y') ";
       } elseif($recieve_what == "due_today") {
           $what_str = " AND STR_TO_DATE(A.deadline, '%d-%M-%Y') =  STR_TO_DATE('".$today."', '%d-%M-%Y') ";
       } elseif($recieve_what == "overdue_or_due_today") {
           $what_str = " AND STR_TO_DATE(A.deadline, '%d-%M-%Y') <=  STR_TO_DATE('".$today."', '%d-%M-%Y') ";
       }
       return $what_str;
   }

   function getActionHeaders($dbObj)
   {
     $headernames = $dbObj -> mysql_fetch_all("SELECT * FROM ".$dbObj -> getDBRef()."_header_names 
                                                        WHERE type <> 'query' ");
     $headers = array();
     foreach($headernames as $index => $header)
     {
        if( !in_array($header['name'], array('query_comments', 'action_comments', 'assurance', 'signoff')) )
        {
        $headers[$header['name']] = (!empty($header['client_terminology']) ? $header['client_terminology'] : $header['ignite_terminology']);
        }
     }
     return $headers;
   }
    
	function getStatuses($dbObj)
	{
	   $response = $dbObj -> mysql_fetch_all("SELECT * FROM ".$dbObj -> getDBRef()."_action_status 
	                                                  WHERE status & 2 <> 2" );
       $results  = array();
       if(!empty($response))
       {
         foreach($response as $index => $status)
         {
            $results[$status['id']] = $status;
         }
       }
	   return $results;
   }   
	
}
?>
