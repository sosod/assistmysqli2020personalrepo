<?php
@session_start();
require_once __DIR__.'/../contract_autoloader.php';
abstract class Model
{

    private $table_name = "";

    protected static $table;

    protected         $db;

    private $parameters = array();

    private $updateObservers = array();

    function __construct($class_vars = array())
    {
        $this->parameters  = $class_vars;
        $this -> db 	   = new DBConnect( "", "", "", "", "");
    }

    public function getTable()
    {
        return (!empty($this->table_name) ? $this->table_name : static::$table);
    }

    public function setTable($value)
    {
        $this->table_name = $value;
    }

    //insert data to the specified table
    public static function saveData($data)
    {
        if(!empty($data))
        {
            DBConnect::getInstance()->insert(static::$table, $data);
        }
        return DBConnect::getInstance()->insertedId();
    }

    function save($data = array(), $object_str = '')
    {
        $insert_data = array();
        if(isset($this->parameters) && !empty($this->parameters))
        {
            $insert_data   = $this->parameters;
        }
        if(isset($data) && !empty($data) && is_array($data))
        {
            foreach($data as $key => $val)
            {
                $insert_data[$key] = $val;
            }
        }
        $table  = (!empty($this->tablename) ? $this->tablename : static::$table);
        $this->db->insert($table, $insert_data);
        $inserted_id     = $this->db->insertedId();
        if($inserted_id) $this->logNewObject($insert_data, $inserted_id, $object_str);
        return $inserted_id;
    }

    function __call($methodName, $args)
    {
        $row = "";
        if( substr($methodName, 0, 6) == "findOneBy")
        {
            $field = strtolower(substr($methodName, 6));
            $value = $args[0];
            $row   = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE ".$field." = '".$value."'");
        } elseif(preg_match("/^findAll/", $methodName)) {
            $field = preg_replace("/^findAll(\w*)$/", '${1}', $methodName);
            if(isset($args[0]))
            {
                $result = DBConnect::getInstance()->get("SELECT * FROM #_".static::$table." WHERE ".strtolower($field)." = '".$args[0]."'");
            }
        } else {
            $field = preg_replace("/^findBy(\w*)$/", '${1}', $methodName);
            if(isset($args[0]))
            {
               $result = DBConnect::getInstance()->getRow("SELECT * FROM #_".static::$table." WHERE ".strtolower($field)." = '".$args[0]."'");
            }
        }
        return $row;
    }

    public function attach(AuditLog $obj)
    {
        $this->updateObservers[] = $obj;
    }

    function update_where($data, $where)
    {
        //handle the logging of changes made
        $res = 0;
        $res = $this->notifyChanges($data, $where);
        $res += $this->db->updateData(static::$table, $data, $where);
        return $res;
    }

    function delete_where($where = array())
    {
        $res = 0;
        $where_str = "";
        if(is_array($where) && !empty($where))
        {
            foreach($where as $key => $value)
            {
               $where_str .= $key." = '".$value."' AND ";
            }
            $where_str = rtrim($where_str, 'AND ');
        }
        $res += $this->db->delete(static::$table, $where_str);
        return $res;
    }


    private function notifyChanges($data, $where)
    {
        $res = 0;
        if(isset($this->updateObservers) && !empty($this -> updateObservers))
        {
            foreach($this->updateObservers as $index => $obj)
            {
                if(method_exists($obj, 'notify'))
                {
                   $res += $obj->notify($data, $where, static::$table);
                }
            }
        }
        return $res;
    }

    private function logNewObject($data, $inserted_id, $object_str)
    {
        $res = 0;
        if(isset($this->updateObservers) && !empty($this -> updateObservers))
        {
            foreach($this->updateObservers as $index => $obj)
            {
               if(method_exists($obj, 'saveNewObject')) $res += $obj->saveNewObject($data, static::$table, $inserted_id, $object_str);
            }
        }
        return $res;
    }


    function update($id, $data ,Auditlog $audit_log, $obj = "", $naming = null)
    {
        $res	   = 0;
        $result = "";
        $table   = (!empty($this -> tablename) ? $this -> tablename : static::$table);
        if(is_object($obj))
        {
            //observer
            $result = $audit_log -> processChanges($obj, $data, $id, $naming);
        }
        if(isset($data['multiple']))
        {
            unset($data['multiple']);
        }
        if(is_array($result) && !empty($result))
        {
            $data = array_merge($data, $result);
        }
        // $this->db->assist_writeError("test","model.update");
        if(strstr($id, "="))
        {
            $this -> db -> update($table, $data, $id);
        } else {
            $this -> db -> update($table, $data, "id={$id}");
        }
        $affectedRows = $this -> db -> affected_rows();
        $res = (!empty($result) ? count($result) + $affectedRows : $affectedRows);
        return $res;
    }


    public static function setFlash($text, $status )
    {
        $response = array();
        if( $status > 0)
        {
            $response = array("text" => ucwords( str_replace("_", " ", $text ) )." updated successfully", "error" => false, "updated" => true);
        } elseif($status == 0){
            $response = array("text" => " No change made", "error" => false, "updated" => false);
        } else {
            $response = array("text" => "Error updating ".ucwords( str_replace("_", " ", $text ) )."", "error" => true);
        }
        return $response;
    }

    public static function saveMessage( $context, $status)
    {
        $response = array();
        if( $status > 0){
            $response = array("text" => ucwords(str_replace("_", " ", $context ) )." saved successfully", "error" => false, "id" => $status );
        } else {
            $response = array("text" => "Error saving ".ucwords(str_replace("_", " ", $context ) )."", "error" => true);
        }
        return $response;
    }


}
?>