<?php
class ContractAssessmentAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $contractAssuranceObj = new ContractAssessment();
        $object_data          = $contractAssuranceObj->getContractAssuranceDetail($where['id']);
        $changes              = array();
        debug($where);
        debug($post_data);
        debug($object_data);
        echo $table_name;
        exit();
        exit();
        $ref['ref'] = 'Assurance Ref '.$where['id'];
        if(isset($_REQUEST['assurance_data']))
        {
            parse_str($_REQUEST['assurance_data'], $contract_assurance);
            if(isset($contract_assurance['contract_assurance_docs']))
            {
                $posted_documents   = $contract_assurance['contract_assurance_docs'];
                $attachment_changes = $this->processDocumentChange($posted_documents, $where['id']);
                if(!empty($attachment_changes))
                {
                    $changes['attachment'] = $attachment_changes;
                }
                unset($contract_assurance['contract_assurance_docs']);
            }
            if(isset($contract_assurance['deleted_assurance_document']))
            {
                $posted_deleted_documents   = $contract_assurance['deleted_assurance_document'];
                $deleted_attachment_changes = $this->processDeletedDocumentChange($posted_deleted_documents, $where['id']);
                if(!empty($deleted_attachment_changes))
                {
                    $changes['attachment'] = (isset($changes['attachment']) ?  array_merge($changes['attachment'], $deleted_attachment_changes) : $deleted_attachment_changes);
                }
                unset($contract_assurance['deleted_assurance_document']);
            }
        }

        if(!empty($object_data))
        {
            foreach($object_data as $field => $current_val)
            {
                if(isset($post_data[$field]) && $current_val != $post_data[$field])
                {
                    if($field == 'date_tested')
                    {
                        $from = strtotime($current_val);
                        $to   = strtotime($post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => date('d-M-Y', $from), 'to' => date('d-M-Y', $to));
                        }
                    } else {
                        $from = (empty($current_val) ? '-' : $current_val);
                        $to   = (empty($post_data[$field]) ? '-' : $post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    }
                }
            }
        }
        if(!empty($changes))
        {
            $changes                    = array_merge($ref, $changes);
            static::$table              = $table_name."_logs";
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['assurance_id']  = $where['id'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

    public function completedOutstandingAssessment($posted_data, $contract, $next_assessment_date)
    {

      $changes['_ref'] = 'Contract ref #'.$contract['id'];
      $changes['message'] = 'Completed the outstanding assessment for period starting '.date('d-M-Y', strtotime($posted_data['contract_assessment_date']));
      $changes['assessment_frequency_date'] = array('from' => date('d-M-Y', strtotime($contract['assessment_frequency_date'])), 'to' => date('d-M-Y', strtotime($next_assessment_date)));

      static::$table              = "contract_assessment_logs";
      $changes['user']            = $_SESSION['tkn'];
      $insert_data['contract_id']  = $contract['id'];
      $insert_data['insert_user'] = $_SESSION['tid'];
      $insert_data['changes']     = serializeEncode($changes);
      if(!empty($insert_data))
      {
        parent::saveData($insert_data);
      }
    }

    public function newContractAssessment($assessment_data, $parameters)
    {
        $changes['_ref'] = 'New contract assessment for deliverable ref#'.$parameters['deliverable_id']." created";
        if(isset($assessment_data['delivered_comment']) && !empty($assessment_data['delivered_comment']))
        {
          $deliverableScoreObj = new DeliveredScore();
          $delivered_score     = $deliverableScoreObj->getDeliveredScore($assessment_data['delivered_id']);
          $changes['delivered_comment'] = 'Added quantitative comment '.$assessment_data['delivered_comment'];
          $changes['delivered_id'] = 'Added quantitative score '.$delivered_score['definition']." (".$delivered_score['score'].")";
        }
        if(isset($assessment_data['quality_comment']) && !empty($assessment_data['quality_comment']))
        {
          $qualityScoreObj = new QualityScore();
          $quality_score = $qualityScoreObj->getQualityScore($assessment_data['quality_id']);
          $changes['quality_comment'] = 'Added qualitative comment '.$assessment_data['quality_comment'];
          $changes['quality_id'] = 'Added qualitative score '.$quality_score['definition']." (".$quality_score['score'].") ";
        }
        if(isset($assessment_data['other_comment']) && !empty($assessment_data['other_comment']))
        {
          $otherScoreObj = new OtherScore();
          $other_score   = $otherScoreObj->getOtherScore($assessment_data['other_id']);
          $changes['other_comment'] = 'Added other comment '.$assessment_data['other_comment'];
          $changes['other_id'] = 'Added other score '.$assessment_data['other_id'];
        }

        static::$table              = "contract_assessment_logs";
        $changes['user']            = $_SESSION['tkn'];
        $insert_data['contract_id']  = $parameters['contract_id'];
        $insert_data['insert_user'] = $_SESSION['tid'];
        $insert_data['changes']     = serializeEncode($changes);
        if(!empty($insert_data))
        {
            parent::saveData($insert_data);
        }
    }

    function processDeletedDocumentChange($posted_deleted_document, $assurance_id)
    {
        $changes = array();
        if(!empty($posted_deleted_document))
        {
            $contractAssuranceDocumentObj = new ContractAssuranceDocument();
            foreach($posted_deleted_document as $delete_index => $document_id)
            {
                $document = $contractAssuranceDocumentObj->getContractAssuranceDocument($document_id);
                $changes[] = ' Attachment '.$document['description']." has been deleted ";
            }
        }
        return $changes;
    }


}
