<?php
class ContractBudgetAdjustmentDocument extends Model
{

    static $table = 'contract_budget_adjustment_document';

    function __construct()
    {
        parent::__construct();
    }

    function getAll($budget_adjustment_id = null)
    {
        $option_sql  = ' AND deleted_at IS NULL ';
        if($budget_adjustment_id) $option_sql .= " AND budget_adjustment_id = ".$budget_adjustment_id." ";
        $response   = $this->db->get("SELECT * FROM #_".static::$table." WHERE 1 ".$option_sql);
        return $response;
    }

    function getContractBudgetAdjustmentDocument($id)
    {
        $response = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = $id ");
        return $response;
    }

    function getList($active = false)
    {
        $budget_adjustment_documents = $this->getAll($active);
        $list                        = array();
        foreach($budget_adjustment_documents as $index => $val)
        {
            $list[$val['budget_adjustment_id']] = $val['filename']." ".$val['description'];
        }
        return $list;
    }

}

?>