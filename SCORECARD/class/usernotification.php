<?php
class UserNotification extends Model
{

	protected $id;
	
	protected $notification;
	
	protected $risk_id;
	
	protected $action_id;
	
	protected $recieve_email;
	
	protected $recieve_when;
	
	protected $recieve_what;
	
	protected $user_id;

	protected $recieve_day;

	protected $active;
	
	protected $inserdate;

    static $table = 'user_notification';
	
	function __construct() 
	{
	    parent::__construct();
		$this->notification  = (isset($_POST['notification']) ? $_POST['notification'] : "");
		$this->risk_id	   = (isset($_POST['risk_id']) ? $_POST['risk_id'] : "");
		$this->action_id	   = (isset($_POST['action_id']) ? $_POST['action_id'] : "");
		$this->recieve_email = (isset($_POST['recieve_email']) ? $_POST['recieve_email'] : "");
		$this->recieve_what  = (isset($_POST['recieve_what']) ? $_POST['recieve_what'] : "");
		$this->recieve_when  = (isset($_POST['recieve_when']) ? $_POST['recieve_when'] : "");
		$this->user_id	   = (isset($_SESSION['tid']) ? $_SESSION['tid'] : "");
		$this->recieve_day   = (isset($_POST['recieve_day']) ? $_POST['recieve_day'] : "");
	}

    public static $notification_types = array('all_incomplete_actions'   => 'All Incomplete Actions',
        'due_this_week'            => 'Actions due this week',
        'overdue_or_due_this_week' => 'Actions overdue or due this week',
        'due_today'                => 'Actions due today',
        'overdue_or_due_today'     =>  'Actions overdue or due today'
    );

    public static $recieve_days        = array(1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday');

	
	function getNotifications()
	{
	   $results = $this->db->get("SELECT * FROM #_".static::$table." WHERE user_id = '".$this->user_id."' AND status & 1 = 1");
       return $results;
	}
	
	function getNotification($id)
	{
		$result = $this->db->getRow("SELECT * FROM #_".static::$table." WHERE id = ".$id." ");
		return $result;           
	}
	
	function saveNotification()
	{
		$insert_data = array(
				'notification' 		=> $this->notification,
				'risk_id' 			=> $this->risk_id,
				'action_id' 		=> $this->action_id,
				'recieve_email' 	=> $this->recieve_email,
				'recieve_what' 		=> $this->recieve_what,
				'recieve_when' 		=> $this->recieve_when,
				'user_id'			=> $this->user_id,
				'recieve_day'		=> $this->recieve_day,
				"insertuser"  		=> $_SESSION['tid'],				
		);
		return $this->save($insert_data);
	}
	
		
	function getAProfile( $id )
	{
		$results = $this -> getRow("SELECT * FROM #_".static::$table." WHERE id = '".$id."'");
		return $results;
	}
	
	function updateNotification($id)
	{
	    $update_data        = array();
	    $changes            = array();
	    $notification       = $this -> getNotitication($id);
		$notification_types = array('all_incomplete_actions'   => 'All Incomplete Actions',
		                            'due_this_week'            => 'Actions due this week',
		                            'overdue_or_due_this_week' => 'Actions overdue or due this week',
		                            'due_today'                => 'Actions due today',
		                            'overdue_or_due_today'     =>  'Actions overdue or due today'
		                            );
        $recieve_day        = array(1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday');
	    if((int)$notification['recieve_email'] != (int)$this -> recieve_email)
	    {
	        $to                 = ((int)$this -> recieve_email == 0 ? "No" : "Yes");
	        $from               = ((int)$notification['recieve_email'] == 0 ? "No" : "Yes");
	        $changes['recieve'] = "Recieve email changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
            $update_data['recieve_email'] = $this->recieve_email;
	    }
	    if($notification['recieve_when'] != $this->recieve_when)
	    {
            if($notification['recieve_when'] == "weekly")
            {
               $when_str   = "Weekly";
               if(isset($recieve_day[$notification['recieve_day']]))
               {
                  $when_str  .= "<em> on ".$recieve_day[$notification['recieve_day']]."</em>";
               }
               $from = $when_str;
            } else {
               $from  = "Daily";
            }
            if($this->recieve_when == "weekly")
            {
               $when_str   = "Weekly";
               if(isset($recieve_day[$this->recieve_day]))
               {
                  $when_str  .= "<em> on ".$recieve_day[$this -> recieve_day]."</em>";
               }
               $to = $when_str;
            } else {
               $to  = "Daily";
            }
	        $changes['recieve_when']    = "Recieve when changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
            $update_data['recieve_when'] = $this->recieve_when;
	    }	    
	    if($notification['recieve_day'] != $this->recieve_day)
	    {
            $from = $to = "";
            if(isset($recieve_day[$notification['recieve_day']]))
            {
              $from  = $recieve_day[$notification['recieve_day']];    
            }
            if(isset($recieve_day[$this->recieve_day]))
            {
              $to  = $recieve_day[$this->recieve_day];
            }            
	        $changes['recieve_day']    = "Recieve day changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
            $update_data['recieve_day'] = $this -> recieve_day;
	    }
	    if($notification['recieve_what'] != $this -> recieve_what)
	    {
	        $to                         = $notification_types[$this->recieve_what];
	        $from                       = $notification_types[$notification['recieve_what']];
	        $changes['recieve_what']    = "Recieve what changed to <i>".$to."</i> from <i>".$from."</i>\r\n\n";
	        $updatedata['recieve_what'] = $this->recieve_what;
	    }   
	    $res = 0;
	    if(!empty($changes))
	    {
	       $updates['notification_id'] = $id;
	       $changes['user']            = $_SESSION['tkn'];
	       $_changes['ref_']           = "Ref #".$id." has been updated";
	       $changes                    = array_merge($_changes, $changes);
	       $updates['insertuser']      = $_SESSION['tid'];
	       $updates['changes']         = base64_encode(serialize($changes));
	       $this->save('user_notification_logs', $updates);
	       $res += $this -> insertedId(); 
	    }
		$res = $this->update_where($update_data, array('id' => $id));
		return $res;
	}
	
	function deleteNotification( $id )
	{
	    $update_data        = array();
	    $changes            = array();
	    $notification       = $this -> getNotitication($id);	    
	    if(isset($_POST['active']))
	    {
	        if($_POST['active'] != $notification['active'])
	        {
	            if($_POST['active'] == 0)
	            {
	                $changes['active_']    = "Notification setting deleted \r\n\n";
                    $update_data['active']  = $_POST['active'];
	            }
	        }	    
	    } 
	    $res = 0;
	    if(!empty($changes))
	    {
	       $updates['notification_id'] = $id;
	       $changes['user']            = $_SESSION['tkn'];
	       $_changes['ref_']           = "Ref #".$id." has been updated";
	       $changes                    = array_merge($_changes, $changes);
	       $updates['insertuser']      = $_SESSION['tid'];
	       $updates['changes']         = base64_encode(serialize($changes));
	       $this -> insert('user_notification_logs', $updates);
	       $res += $this -> insertedId(); 
	    }
		$res = $this->update_where($update_data, array('id' => $id) );
		return $res;
	}
	
}
?>
