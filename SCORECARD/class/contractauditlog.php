<?php
class ContractAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $contractObj     = new Contract();
        $object_data     = $contractObj->getContractDetail($where['id']);
        $posted_contract = array();
        $changes         = array();

        if(isset($_REQUEST['contract']))
        {
            parse_str($_REQUEST['contract'], $posted_contract);
            $posted_deliverable_statuses = array();
            $posted_supplier_budgets     = array();
            $posted_documents            = array();
            $posted_contract_data        = $posted_contract['contract'];
            if(isset($posted_contract_data['deliverable_status']))
            {
                $posted_deliverable_statuses = $posted_contract_data['deliverable_status'];
                $statuses_changes             = $this->processDeliverableStatusChange($posted_deliverable_statuses, $where['id']);
                if(!empty($statuses_changes))
                {
                    $changes['deliverable_status'] = $statuses_changes;
                }
                unset($posted_contract_data['deliverable_status']);
            }
            if(isset($posted_contract_data['suppler_budget']))
            {
                $posted_supplier_budgets = $posted_contract_data['suppler_budget'];
                $supplier_budget_changes = $this->processSupplierBudgetChange($posted_supplier_budgets, $where['id']);
                if(!empty($supplier_budget_changes))
                {
                   $changes['supplier_budget'] = $supplier_budget_changes;
                }
                unset($posted_contract_data['suppler_budget']);
            }
            if(isset($posted_contract_data['documents']))
            {
                $posted_documents   = $posted_contract_data['documents'];
                $attachment_changes = $this->processDocumentChange($posted_documents, $where['id']);
                if(!empty($attachment_changes))
                {
                   $changes['attachment'] = $attachment_changes;
                }
                unset($posted_contract_data['documents']);
            }
        }
        unset($object_data['created_at']);
        if(!empty($object_data))
        {
            foreach($object_data as $field => $val)
            {
                if(isset($post_data[$field]) && $val != $post_data[$field])
                {
                    if($field == 'status')
                    {

                        if( ($post_data[$field] & Contract::AUTHORIZED) == Contract::AUTHORIZED ){
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was authorized";
                        } elseif($post_data[$field] == 2 && $val == 1) {
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deleted";
                        } else if( ($post_data[$field] == 0 && $val == 1)  || ( ( ( ($post_data[$field] & Contract::CONFIRMED) == Contract::CONFIRMED) &&  (($val & Contract::ACTIVATED) == Contract::ACTIVATED) ) )) {
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deactivated";
                        } else if($post_data[$field] == 1 && $val == 0) {
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was activated";
                        } elseif( (($post_data[$field] & Contract::CONFIRMED) == Contract::CONFIRMED) &&  (($val & Contract::ACTIVE) == Contract::ACTIVE)) {
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was confirmed";
                        } elseif( (($post_data[$field] & Contract::ACTIVATED) == Contract::ACTIVATED) &&  (($val & Contract::CONFIRMED) == Contract::CONFIRMED)) {
                          $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was activated";
                        }
                    } elseif($field == 'status_id') {
                        $statusObj     = new ContractStatus();
                        $statuses       = $statusObj->getList();
                        $from        = '';
                        $to          = '';
                        foreach($statuses  as $status_id => $status)
                        {
                            if($status_id == $val)
                            {
                                $from = $status;
                            }
                            if($status_id == $post_data[$field])
                            {
                                $to = $status;
                            }
                        }
                        if($from != $to)
                        {
                            $changes['currentstatus'] = $to;
                        }
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    } elseif($field == 'owner_id'){

                    } else if($field == 'financial_year_id') {
                        $financialYearObj = new FinancialYear();
                        $financial_years  = $financialYearObj->getList();
                        $from             = '';
                        $to               = '';
                        foreach($financial_years as $financial_year_id => $financial_year)
                        {
                            if($financial_year_id == $val)
                            {
                                $from = $financial_year;
                            }
                            if($financial_year_id == $post_data[$field])
                            {
                                $to = $financial_year;
                            }
                        }
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    } else if($field == 'type_id') {
                        $typesObj         = new ContractType();
                        $contract_types  = $typesObj->getList();
                        $from             = '';
                        $to               = '';
                        foreach($contract_types as $contract_type_id => $contract_type)
                        {
                            if($contract_type_id == $val)
                            {
                                $from = $contract_type;
                            }
                            if($contract_type_id == $post_data[$field])
                            {
                                $to = $contract_type;
                            }
                        }
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    } else if($field == 'category_id') {
                        $categoryObj         = new ContractCategory();
                        $contract_categories = $categoryObj->getList();
                        $from            = '';
                        $to              = '';
                        foreach($contract_categories as $contract_category_id => $contract_category)
                        {
                            if($contract_category_id == $val)
                            {
                                $from = $contract_category;
                            }
                            if($contract_category_id == $post_data[$field])
                            {
                                $to = $contract_category;
                            }
                        }
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    } else if($field == 'payment_term_id') {
                        $paymentTermObj  = new PaymentTerm();
                        $payment_terms   = $paymentTermObj->getList();
                        $from            = '';
                        $to              = '';
                        foreach($payment_terms as $payment_term_id => $payment_term)
                        {
                            if($payment_term_id == $val)
                            {
                                $from = $payment_term;
                            }
                            if($payment_term_id == $post_data[$field])
                            {
                                $to = $payment_term;
                            }
                        }
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    } else if($field == 'payment_frequency_id') {
                        $paymentFrequencyObj = new PaymentFrequency();
                        $payment_frequencies = $paymentFrequencyObj->getList();
                        $from            = '';
                        $to              = '';
                        foreach($payment_frequencies as $payment_frequency_id => $payment_frequency)
                        {
                            if($payment_frequency_id == $val)
                            {
                                $from = $payment_frequency;
                            }
                            if($payment_frequency_id == $post_data[$field])
                            {
                                $to = $payment_frequency;
                            }
                        }
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    } elseif($field == 'assessment_frequency_id') {
                        $assessmentFrequencyObj = new AssessmentFrequency();
                        $assessment_frequencies    = $assessmentFrequencyObj->getList();
                        $from            = '';
                        $to              = '';
                        foreach($assessment_frequencies as $assessment_frequency_id => $assessment_frequency)
                        {
                            if($assessment_frequency_id == $val)
                            {
                                $from = $assessment_frequency;
                            }
                            if($assessment_frequency_id == $post_data[$field])
                            {
                                $to = $assessment_frequency;
                            }
                        }
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    } elseif(in_array($field, array('manager_id', 'authorisor_id'))){
                        $userObj     = new User();
                        $users       = $userObj->getList();
                        $from        = '';
                        $to          = '';
                        foreach($users as $user_id => $user)
                        {
                            if($user_id == $val)
                            {
                                $from = $user;
                            }
                            if($user_id == $post_data[$field])
                            {
                                $to = $user;
                            }
                        }
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    }  else if(in_array($field, array('completion_date', 'signature_of_sla', 'signature_of_tender', 'assessment_frequency_date'))) {
                        $from = strtotime($val);
                        $to   = strtotime($post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => date('d-M-Y', $from), 'to' => date('d-M-Y', $to));
                        }
                    } else {
                        $from = (empty($val) ? '-' : $val);
                        $to   = (empty($post_data[$field]) ? '-' : $post_data[$field]);
                        if($from != $to)
                        {
                            $changes[$field] = array('from' => $from, 'to' => $to);
                        }
                    }
                }
            }
        }
        if(!empty($changes))
        {
            if(isset($_REQUEST['response']))
            {
                $changes['response']        = $_REQUEST['response'];
                static::$table              = $table_name."_update_logs";
            } else {
                static::$table              = $table_name."_edit_logs";
            }
            if(!isset($changes['currentstatus'])) $changes['currentstatus'] = $object_data['contract_status'];
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['contract_id'] = $where['id'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

    public function processSupplierBudgetChange($posted_supplier_budgets, $contract_id)
    {
        $supplierBudgetObj = new ContractSupplierBudget();
        $supplier_budgets       = $supplierBudgetObj->getAll($contract_id);
        foreach($supplier_budgets as $index => $supplier_budget)
        {
 /*           if(isset($posted_deliverable_statuses[$deliverable_status]) && $posted_deliverable_statuses[$deliverable_status])
            {
                //$changes['attachment'][$index] =  'Attachment '.$doc." has been added";
            }
 */       }

    }

    public function processDeliverableStatusChange($posted_deliverable_statuses, $contract_id)
    {
        $contractDeliverableStatusObj = new ContractDeliverableStatus();
        $contract_deliverable_statues = $contractDeliverableStatusObj->getAllStatusByContractId($contract_id);

        foreach($contract_deliverable_statues as $index => $deliverable_status)
        {
            if(isset($posted_deliverable_statuses[$deliverable_status]) && $posted_deliverable_statuses[$deliverable_status])
            {
                //$changes['attachment'][$index] =  'Attachment '.$doc." has been added";
            }
        }
    }

    public function processDocumentChange($posted_documents, $contract_id)
    {
        $documentObj                  = new ContractDocument();
        $contract_documents           = $documentObj->getAll($contract_id);
        foreach($contract_documents as $index => $document)
        {
            if(!isset($posted_documents[$document['filename']]))
            {
               foreach($posted_documents as $index => $doc)
               {
                   $changes[$index] =  'Attachment '.$doc." has been added";
               }
            }
        }
        return $changes;
    }

    public function getContractActivityLog($contract_id)
    {
        $columnObj   = new ContractColumns();
        $columns     = $columnObj->getHeaderList();

        $contract_edits = DBConnect::getInstance()->get("SELECT id, contract_id, changes, created_at, updated_at AS insertdate FROM #_contract_edit_logs WHERE contract_id = '".$contract_id."' ");
        $contract_edits  = self::logs($contract_edits, $columns);

        $contract_updates = DBConnect::getInstance()->get("SELECT id, contract_id, changes, created_at, updated_at AS insertdate FROM #_contract_update_logs WHERE contract_id = '".$contract_id."' ");
        $contract_updates  = self::logs($contract_updates, $columns);

        $logs = array_merge($contract_updates, $contract_edits);
        uasort($logs, function($a, $b){
            return ($a['key'] > $b['key'] ? 1 : -1);
        });
        return $logs;
    }
}
