<?php
class ScoreCardSetupAuditLog extends AuditLog
{
    function notify($post_data, $where, $table_name)
    {
        $scoreCardSetupObj = new ScoreCardSetup();
        $object_data       = $scoreCardSetupObj->getScoreCardSetup($where['id']);
        $posted_contract = array();
        $changes         = array();

        unset($object_data['created_at']);
        unset($object_data['updated_at']);
        if(!empty($object_data))
        {
            foreach($object_data as $field => $val)
            {
                if(isset($post_data[$field]) && $val != $post_data[$field])
                {
                    if($field == 'status')
                    {

                       if($post_data[$field] == 2 && (($val & 1) == 1)) {
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deleted";
                        } else if( ($post_data[$field] == 0 && $val == 1)  || ( ( ( ($post_data[$field] & ScoreCard::CONFIRMED) == ScoreCard::CONFIRMED) &&  (($val & ScoreCard::ACTIVATED) == ScoreCard::ACTIVATED) ) )) {
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was deactivated";
                        } else if($post_data[$field] == 1 && $val == 0) {
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was activated";
                        } elseif( (($post_data[$field] & ScoreCard::CONFIRMED) == ScoreCard::CONFIRMED) &&  (($val & ScoreCard::ACTIVE) == ScoreCard::ACTIVE)) {
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was confirmed";
                        } elseif( (($post_data[$field] & ScoreCard::ACTIVATED) == ScoreCard::ACTIVATED) &&  (($val & ScoreCard::CONFIRMED) == ScoreCard::CONFIRMED)) {
                            $changes[$field] = "Ref ".$where['id']." <br />".ucwords(str_replace('_', ' ', $table_name))." was activated";
                        }
                    } else if($field == 'financial_year_id') {
                        $financialYearObj = new FinancialYear();
                        $financial_years  = $financialYearObj->getList();
                        $from             = '';
                        $to               = '';
                        foreach($financial_years as $financial_year_id => $financial_year)
                        {
                            if($financial_year_id == $val)  $from = $financial_year;
                            if($financial_year_id == $post_data[$field])  $to = $financial_year;
                        }
                        if($from != $to)  $changes[$field] = array('from' => $from, 'to' => $to);
                    } else if($field == 'link_to') {
                        $scoreCardObj = new ScoreCard();
                        $score_cards  = $scoreCardObj->getScoreCardsWithSetupByScoreCardIdList($object_data['score_card_id']);
                        $from             = '';
                        $to               = '';
                        foreach($score_cards as $score_card_id => $score_card)
                        {
                            if($score_card_id == $val)  $from = $score_card;
                            if($score_card_id == $post_data[$field])  $to = $score_card;
                        }
                        if($from != $to)  $changes[$field] = array('from' => (empty($from) ? ' - ' : $from), 'to' => (empty($to) ? '-' : $to));
                    } else if(in_array($field, array('is_movement_tracked', 'use_rating_comment', 'use_corrective_action', 'are_corrective_action_owner_identified')) ) {
                        $to             = (empty($post_data[$field]) ? 'No' : 'Yes');
                        $from               = (empty($val) ? 'No' : 'Yes');
                        if($from != $to)  $changes[$field] = array('from' => $from, 'to' => $to);
                    } elseif($field == 'rating_type') {
                        $ratingTypeObj = new RatingScale();
                        $rating_types  = $ratingTypeObj->getRatingScaleTypes();

                        $to   = (isset($rating_types[$post_data[$field]]) ? $rating_types[$post_data[$field]]['name'] : '-');
                        $from = (isset($rating_types[$val]) ? $rating_types[$val]['name'] : '-');

                        if($from != $to)  $changes[$field] = array('from' => $from, 'to' => $to);
                    } else {
                        $from = (empty($val) ? '-' : $val);
                        $to   = (empty($post_data[$field]) ? '-' : $post_data[$field]);
                        if($from != $to) $changes[$field] = array('from' => $from, 'to' => $to);
                    }
                }
            }
        }
        if(!empty($changes))
        {
            static::$table              = $table_name."_logs";
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['setup_id']    = $where['id'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }

    public function getScoreCardActivityLog($score_card_id)
    {
        $columnObj   = new ScoreCardColumn();
        $columns     = $columnObj->getHeaderList();

        $score_card_edits = DBConnect::getInstance()->get("SELECT id, score_card_id, changes, created_at, updated_at AS insertdate FROM #_score_card_edit_logs WHERE score_card_id = '".$score_card_id."' ");
        $score_card_edits  = self::logs($score_card_edits, $columns);

        $score_card_updates = DBConnect::getInstance()->get("SELECT id, score_card_id, changes, created_at, updated_at AS insertdate FROM #_score_card_update_logs WHERE score_card_id = '".$score_card_id."' ");
        $score_card_updates  = self::logs($score_card_updates, $columns);

        $logs = array_merge($score_card_updates, $score_card_edits);
        uasort($logs, function($a, $b){
            return ($a['key'] > $b['key'] ? 1 : -1);
        });
        return $logs;
    }

    public function deleteScoreCardSettingsLog($score_card)
    {
        $changes = array();
        $changes['_score_card'] = 'Score card Ref #'.$score_card['id']." has been deleted";
        if(!empty($changes))
        {
            static::$table              = "score_card_edit_logs";
            $changes['user']            = $_SESSION['tkn'];
            $insert_data['score_card_id'] = $score_card['id'];
            $insert_data['insert_user'] = $_SESSION['tid'];
            $insert_data['changes']     = serializeEncode($changes);
            if(!empty($insert_data))
            {
                parent::saveData($insert_data);
            }
        }
    }
}
