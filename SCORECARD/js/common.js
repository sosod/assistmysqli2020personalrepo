$(function() {
    $(".datepicker").datepicker({
        showOn		  : "both",
        buttonImage 	  : "/library/jquery/css/calendar.gif",
        buttonImageOnly  : true,
        changeMonth	  : true,
        changeYear	  : true,
        dateFormat 	  : "dd-M-yy",
        altField		  : "#startDate",
        altFormat		  : 'd_m_yy'
    });

    $(".historydate").datepicker({
        showOn			: "both",
        buttonImage 	     : "/library/jquery/css/calendar.gif",
        buttonImageOnly	: true,
        changeMonth		: true,
        changeYear		: true,
        dateFormat 		: "dd-M-yy",
        maxDate             : 0
    });

    $(".futuredate").datepicker({
        showOn			: "both",
        buttonImage 	     : "/library/jquery/css/calendar.gif",
        buttonImageOnly	: true,
        changeMonth		: true,
        changeYear		: true,
        dateFormat 		: "dd-M-yy",
        minDate             : +1
    });

    $("#clear_remind").click(function(e){
        $("#remind_on").val("");
        $("#reminder").val("");
        e.preventDefault();
    });

    $("table").find("th").css({"text-align":"left"});
    $("textarea").attr("cols", "50").attr("rows", "5");

    $(".viewmore").live("click", function(e){
        var id = this.id;
        var parentSpan = $(this).parent().attr("id");
        var longText = $(this).attr("title");
        var shortText = $("#"+parentSpan).text();
        var positionOfMore = shortText.lastIndexOf("more...");
        shortText = shortText.substr(0, positionOfMore);
        $("#"+parentSpan).html("");
        $("#"+parentSpan).html(longText+" <a href='#' class='less' id='less_"+id+"' style='color:orange;'>less...</a>");
        $("#less_"+id).bind("click", function(e){
            $("#"+parentSpan).html("");
            $("#"+parentSpan).html(shortText+" <a href='#' class='viewmore' id='"+id+"' title='"+longText+"' style='color:orange;'>more...</a>");
            e.preventDefault();
        })

        return false;
    });
})


function getWindowSize()
{
    var viewportwidth;
    var viewportheight;     // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
    if (typeof window.innerWidth != 'undefined')  {
        viewportwidth = window.innerWidth,
            viewportheight = window.innerHeight
    }    // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
    else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)  {
        viewportwidth = document.documentElement.clientWidth,
            viewportheight = document.documentElement.clientHeight
    }     // older versions of IE
    else {
        viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
            viewportheight = document.getElementsByTagName('body')[0].clientHeight
    }
    var ret = new Array();
    ret['width'] = viewportwidth;
    ret['height'] = viewportheight;
    return ret;
}