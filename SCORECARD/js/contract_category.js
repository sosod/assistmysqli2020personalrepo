$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    ContractCategory.get();
    $("#add_contract_category").click(function(e){
        ContractCategory.addContractCategory();
        e.preventDefault();
    });
	
});

var ContractCategory 	= {

		get				: function()
		{
            $(".contract_categories").remove();
			$.getJSON( "../controllers/contractcategorycontroller.php?action=getAll", function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#contract_category_table").append($("<tr />").addClass('contract_categories')
                        .append($("<td />", {colspan: 6, html: "There are no contract categories" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        ContractCategory.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#contract_category_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('contract_categories')
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.short_code}))
				.append($("<td />",{html:val.name}))
				.append($("<td />",{html:val.description}))
                .append($("<td />",{html:"<b>"+(val.status == 1 ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"category_edit_"+val.id, name:"category_edit_"+val.id }))
				 )
			  )

            $("#category_edit_"+val.id).live("click", function(e){
                self._updateContractCategory(val);
                e.preventDefault();
            });
			
		},

    addContractCategory          : function()
    {
        var self = this;
        if($("#add_contract_category_dialog").length > 0)
        {
            $("#add_contract_category_dialog").remove();
        }

        $("<div />",{id:"add_contract_category_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Contract Category Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"contract_type[short_code]", id:"short_code", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Contract Category Name :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"contract_type[name]", id:"name", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Contract Category Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"contract_type[description]", id:"description", cols:50, rows:8}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Contract Category",
                width          : 500,
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the contract short code");
                            return false;
                        } else if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the contract category name");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_contract_category_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_contract_category_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});
                }
            })
    },

    _updateContractCategory         : function(contract_type)
    {
        var self = this;
        if($("#edit_contract_category_dialog").length > 0)
        {
            $("#edit_contract_category_dialog").remove();
        }

        $("<div />",{id:"edit_contract_category_dialog"}).append($("<table />",{width:"100%"})
               .append($("<tr />")
                    .append($("<th />",{html:"Contract Category Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"short_code", id:"short_code", value:contract_type.short_code}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Contract Category Name :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:contract_type.name}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Contract Category Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8, text:contract_type.description}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Contract Category",
                width          : 500,
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the contract type short code");
                            return false;
                        } else if($("#name").val() == "") {
                            $("#message").addClass("ui-state-error").html("please enter the contract type name");
                            return false;
                        } else {
                            self.update(contract_type.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_contract_category_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(contract_type.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(contract_type.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(contract_type.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_contract_category_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((contract_type.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/contractcategorycontroller.php?action=update",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_contract_category_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/contractcategorycontroller.php?action=update",
        {
            id           : id,
            short_code : $("#short_code").val(),
            name:        $("#name").val(),
            description: $("#description").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_contract_category_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/contractcategorycontroller.php?action=save",
            {
                short_code : $("#short_code").val(),
                name:        $("#name").val(),
                description: $("#description").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_contract_category_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    ContractCategory.get()
                }
            }, 'json');
    }

}