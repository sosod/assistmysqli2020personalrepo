// JavaScript Document
$.widget("ui.activate", {
	
	options	: 	{
		tableId 		: "score_card_activate_"+(Math.floor(Math.random(56) * 34)),
		url				: "../controllers/scorecardcontroller.php?action=getActivationScoreCards",
		location		: "",
		start			: 0,
		limit			: 10,
		total			: 0,
		score_card_id	    : 0,
		current			: 1,
		isOwners		: [], 
		section         : "",
		viewAction      : ""
	} , 
	
	_init			: function()
	{
		this._getScoreCards();
	} , 
	
	_create			: function()
	{
		var self = this;
		var html = [];
		html.push("<table width='100%' class='noborder'>");
		  html.push("<tr>");
		    html.push("<td width='50%' class='noborder'>");
		      html.push("<table width='100%' id='awaiting_"+self.options.tableId+"'>");
      		    html.push("<tr>")
      		    html.push("<td colspan='6'><h4>Scorecards Awaiting Activation</h4></td>");
      		    html.push("</tr>");
		      html.push("</table>");
		    html.push("</td>");
		    html.push("<td class='noborder'>");
		      html.push("<table width='100%' id='activated_"+self.options.tableId+"'>");
      		    html.push("<tr width='50%'>");
      		    html.push("<td colspan='6'><h4>Scorecards Activated</h4></td>");
      		    html.push("</tr>");      		    
		      html.push("</table>");
		    html.push("</td>");		    
		  html.push("</tr>");	  
		html.push("</table>");
		$(self.element).append(html.join(' '));		
	} , 
	
	_getScoreCards		: function()
	{
		var self = this;
	     $("body").append($("<div />",{id:"contractLoadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
	     );				
		$.getJSON(self.options.location+""+self.options.url, {
			start 	: self.options.start,
			limit 	: self.options.limit,
			id		: self.options.score_card_id,
			section : self.options.section,
            page    : 'activate'
		}, function(scoreCardData){
		    $("#contractLoadingDiv").remove();
			$(".activated").remove();
			$(".awaiting").remove();
			self._displayHeaders(scoreCardData.columns);
			self._displayAwaiting(scoreCardData.awaiting.scorecards, scoreCardData.awaiting.score_card_detail);
			self._displayActivated(scoreCardData.activated.scorecards, scoreCardData.activated.score_card_detail);
		});
	} ,
	
	_displayAwaiting    : function(score_cards, score_card_detail)
	{
	   var self = this;
	   var html = [];
	   if($.isEmptyObject(score_cards))
	   {
	      html.push("<tr class='awaiting'><td colspan='6'>There are no scorecards awaiting activation</td></tr>");
	   } else {
           $.each(score_cards, function(score_card_id, contract){
              html.push("<tr class='awaiting'>");
                html.push("<td>"+contract.score_card_id+"</td>");
                html.push("<td>"+contract.reference_number+"</td>");
                html.push("<td>"+contract.score_card_name+"</td>");
                html.push("<td>"+contract.score_card_type+"</td>");
                html.push("<td>"+contract.score_card_category+"</td>");
                html.push("<td>");
                 html.push("<input type='button' name='view_score_card_"+score_card_id+"' id='view_score_card_"+score_card_id+"' value='View Scorecard' />");
                 html.push("<br />");
                 html.push("<input type='button' name='view_deliverables_"+score_card_id+"' id='view_deliverables_"+score_card_id+"' value='View Headings and Questions' />");
                 html.push("<br />");
                 html.push("<input type='button' name='unconfirm_"+score_card_id+"' id='unconfirm_"+score_card_id+"' value='Unconfirm' />");
                 html.push("<br />");
                 html.push("<input type='button' name='activate_"+score_card_id+"' id='activate_"+score_card_id+"' value='Activate ' />");
                 $("#activate_"+score_card_id).live("click", function(e){
                    self._activateScoreCard(score_card_id);
                    e.preventDefault();
                 });


               $("#view_score_card_"+score_card_id).live("click", function(e){
                   self._viewScoreCard(score_card_id);
                   e.preventDefault();
               });

               $("#view_deliverables_"+score_card_id).live("click", function(e){
                   self._viewDeliverables(score_card_id, score_card_detail[score_card_id]);
                   e.preventDefault();
               });

               $("#unconfirm_"+score_card_id).live("click", function(e){
                   self._unConfirmScorecard(score_card_id, score_card_detail[score_card_id]);
                   e.preventDefault();
               });
                html.push("</td>");
                
                
              html.push("</tr>");       
           });
       }
	   $("#awaiting_"+self.options.tableId).append(html.join(' ') );	
	   
	} ,
	
	_displayActivated			: function(score_cards, score_card_detail)
	{  
	   var self = this;
	   var html = [];
	   if($.isEmptyObject(score_cards))
	   {
	     html.push("<tr class='activated'><td colspan='6'>There are no scorecards activated</td></tr>");
	   } else {	   
           $.each(score_cards, function(score_card_id, contract){
              html.push("<tr class='activated'>");
                html.push("<td>"+contract.score_card_id+"</td>");
                html.push("<td>"+contract.reference_number+"</td>");
                html.push("<td>"+contract.score_card_name+"</td>");
                html.push("<td>"+contract.score_card_type+"</td>");
                html.push("<td>"+contract.score_card_category+"</td>");
               html.push("<td>");
               if(score_card_detail[score_card_id]['has_manage_activity'] == false || score_card_detail[score_card_id]['has_manage_activity'] == null)
               {
                   html.push("<input type='button' name='undo_activation_"+score_card_id+"' id='undo_activation_"+score_card_id+"' value='Undo Activation' />");
               } else {
                   html.push("<div class='ui-state-ok' style='border:0px; background-color:white;'>");
                     html.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                     html.push("<span style='float:left;'>Active . . .</span>");
                   html.push("</div>");
               }
               $("#undo_activation_"+score_card_id).live("click", function(e){
                   self._undoActivationScoreCard(score_card_id);
                   e.preventDefault();
               });
               html.push("</td>");
              html.push("</tr>");       
           });
       }
	   $("#activated_"+self.options.tableId).append(html.join(' ') );	
	} ,


    _viewScoreCard           : function(score_card_id)
    {
        $("<div />",{id:"score_card_"+score_card_id})
            .dialog({
                modal      : true,
                autoOpen   : true,
                title      : "Scorecard Details",
                position   : "left top",
                width      : 'auto',
                buttons    : {
                    "Ok"        : function()
                    {
                        $("#score_card_"+score_card_id).remove();
                    }
                } ,
                open       : function(ui, event)
                {
                    $("#action_contract").html("Loading scorecard  details ...");
                    $.get("../controllers/scorecardcontroller.php?action=getScoreCardDetailHtml",{
                        id  : score_card_id
                    } , function(contractHtml){
                        $("#score_card_"+score_card_id).html("");
                        $("#score_card_"+score_card_id).append( contractHtml );
                    },"html")
                },
                close		: function() {
                    $("#score_card_"+score_card_id).remove();
                }
            });
    } ,


    _viewDeliverables       : function(score_card_id, score_card_detail)
    {
        if($("#score_card_deliverable_"+score_card_id).length > 0)
        {
            $("#score_card_deliverable_"+score_card_id).remove();
        }

        var self = this;
        $("<div />",{id:"score_card_deliverable_"+score_card_id})
            .dialog({
                modal      : true,
                autoOpen   : true,
                title      : "Scorecard Heading Details",
                position   : "left top",
                width      : 'auto',
                buttons    : {
                    "Ok"        : function()
                    {
                        $("#score_card_deliverable_"+score_card_id).dialog('destroy').remove();
                    }
                } ,
                open       : function(ui, event)
                {
                    $("#score_card_deliverable_"+score_card_id).deliverable({scoreCardId: score_card_id, showSubDeliverable: true, showActions: true, autoLoad: true, allowFilter: false, actionFilter: false, financialYear:score_card_detail.financial_year_id });
                },
                close		: function() {
                    $("#score_card_deliverable_"+score_card_id).dialog('destroy').remove();
                }
            });

    } ,

    _activateScoreCard      : function(score_card_id)
	{
	    var self = this;
	    if($("#activate_dialog").length > 0)
	    {
	       $("#activate_dialog").remove();
	    }
	    var html = [];
	    html.push("<table>");
	      html.push("<tr>");
	        html.push("<th>Response</th>")
	        html.push("<td>");
	         html.push("<textarea id='response' name='response' cols='50' rows='8'></textarea>");
	        html.push("</td>");
	      html.push("</tr>");
	      html.push("<tr>");
	        html.push("<td colspan='2'>");
	         html.push("<p id='message'></p>")
	        html.push("</td>");
	      html.push("</tr>");
	    html.push("<table>");
	    
	    $("<div />",{id:"activate_dialog"}).append(html.join(' '))
	     .dialog({
	            autoOpen    : true,
	            modal       : true,
	            position    : "top",
	            title       : "Activate Scorecard SC"+score_card_id,
	            width       : 'auto',
	            buttons     : {
	                            "Activate"      : function()
	                            {
                                    $('#message').html("Activating scorecard  ....<img src='../images/loaderA32.gif' />");
	                                 $.post("../controllers/scorecardcontroller.php?action=activateScoreCard", {
	                                    score_card_id : score_card_id,
	                                    response    : $("#response").val()
	                                 }, function(response) {
	                                    if(response.error)
	                                    {
	                                      jsDisplayResult("error", "error", response.text);
	                                    } else {
	                                      jsDisplayResult("ok", "ok", response.text);
	                                      self._getScoreCards();
                                          $("#activate_dialog").dialog("destroy").remove();
	                                    }
	                                 },"json");
	                            } ,
	                            
	                            "Cancel"        : function()
	                            {
	                               $("#activate_dialog").dialog("destroy").remove();
	                            }
	            },
	            close       : function(event, ui)
	            {
	               $("#activate_dialog").dialog("destroy").remove();
	            }, 
	            open        : function(event, ui)
	            {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
	            }
	     });
	    
	},

    _undoActivationScoreCard      : function(score_card_id)
    {
        var self = this;
        if($("#undoactivate_dialog").length > 0)
        {
            $("#undoactivate_dialog").remove();
        }
        var html = [];
        html.push("<table>");
            html.push("<tr>");
                html.push("<th>Response</th>")
                html.push("<td>");
                html.push("<textarea id='response' name='response' cols='50' rows='8'></textarea>")
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td colspan='2'>");
                html.push("<p id='message'></p>")
                html.push("</td>");
            html.push("</tr>");
        html.push("<table>");

        $("<div />",{id:"undoactivate_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Undo Scorecard Activation SC"+score_card_id,
                width       : 'auto',
                buttons     : {
                    "Deactivate"      : function()
                    {
                        $('#message').html("Deactivating scorecard  ....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/scorecardcontroller.php?action=undoActivateScoreCard", {
                            score_card_id : score_card_id,
                            response    : $("#response").val()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getScoreCards();
                            }
                        },"json");
                        $("#undoactivate_dialog").dialog("destroy").remove();
                    } ,

                    "Cancel"        : function()
                    {
                        $("#undoactivate_dialog").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#activate_dialog").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
                }
            });

    },

    _unConfirmScorecard  : function(score_card_id, score_card)
    {
        var self = this;
        if($("#unconfirm_score_card_dialog").length > 0)
        {
            $("#unconfirm_score_card_dialog").remove();
        }
        var html = [];
        html.push("<p id='message'>Unconfirming Scorecard</p>")

        $("<div />",{id:"unconfirm_score_card_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Undo Scorecard Confirmation SC"+score_card_id,
                width       : 'auto',
                buttons     : {
                    "Unconfirm"      : function()
                    {
                        $('#message').html("Unconfirming scorecard  ....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/scorecardcontroller.php?action=undoConfirmScoreCard", {
                            score_card_id : score_card_id,
                            response    : $("#response").val()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getScoreCards();
                            }
                        },"json");
                        $("#unconfirm_score_card_dialog").dialog("destroy").remove();
                    } ,

                    "Cancel"        : function()
                    {
                        $("#unconfirm_score_card_dialog").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#unconfirm_score_card_dialog").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
                }
            });

    } ,

	_displayHeaders		: function(headers)
	{
		var self = this;
		var html = [];
		html.push("<tr class='activated'>");
        html.push("<th>"+headers.score_card_id+"</th>");
        html.push("<th>"+headers.reference_number+"</th>");
        html.push("<th>"+headers.score_card_name+"</th>");
        html.push("<th>"+headers.score_card_type+"</th>");	
        html.push("<th>"+headers.score_card_category+"</th>");
        html.push("<th></th>");
        html.push("</tr>");
		$("#awaiting_"+self.options.tableId).append(html.join(' '));
		$("#activated_"+self.options.tableId).append(html.join(' ')).find("#remove").remove();;
	} ,
	
	_displayPaging		: function(total, cols)
	{
		var self  = this;	
		var html  = [];
		var pages = 0;
		if(total%self.options.limit > 0)
		{
		   pages = Math.ceil( total/self.options.limit )
		} else {
		   pages = Math.floor( total/self.options.limit )
		}
        html.push("<tr>");
          html.push("<td colspan='"+cols+"'>");
            html.push("<input type='button' id='action_first_"+self.option.score_card_id+"' value=' |< ' />");
            html.push("<input type='button' id='action_previous_"+self.option.score_card_id+"' value=' < ' />");
             html.push("<span>&nbsp;&nbsp;&nbsp;");
              html.push("Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ?  1 :pages));
             html.push("&nbsp;&nbsp;&nbsp;</span>");
            html.push("<input type='button' id='action_next_"+self.option.score_card_id+"' value=' > ' />");
            html.push("<input type='button' id='action_last_"+self.option.score_card_id+"' value=' >| ' />");
          html.push("</td>");
        if(self.options.editAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.updateAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.assuranceAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAssurance)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }    
        if(self.options.viewAction)
        {
           html.push("<td>&nbsp;&nbsp;</td>");
        }   	   
        html.push("</tr>");
        $("#"+self.options.tableId+"_"+self.options.score_card_id).append(html.join(' '));                 
        if(self.options.current < 2)
        {
          $("#action_first_"+self.option.score_card_id).attr('disabled', 'disabled');
          $("#action_previous_"+self.option.score_card_id).attr('disabled', 'disabled');		     
        }
        if((self.options.current == pages || pages == 0))
        {
          $("#action_next_"+self.option.score_card_id).attr('disabled', 'disabled');
          $("#action_last_"+self.option.score_card_id).attr('disabled', 'disabled');		     
        }			 
		$("#action_next_"+self.option.score_card_id).bind("click", function(){
			self._getNext( self );
		});
		$("#action_last_"+self.option.score_card_id).bind("click",  function(){
			self._getLast( self );
		});
		$("#action_previous_"+self.option.score_card_id).bind("click",  function(){
			self._getPrevious( self );
		});
		$("#action_first_"+self.option.score_card_id).bind("click",  function(){
			self._getFirst( self );
		});			 
	} , 
		
		_getNext  			: function( $this ) {
			$this.options.current   = $this.options.current+1;
			$this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
			this._getScoreCards();
		},	
		
		_getLast  			: function( $this ) {
			$this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
			$this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );			
			this._getScoreCards();
		},
		
		_getPrevious    	: function( $this ) {
			$this.options.current   = parseFloat( $this.options.current ) - 1;
			$this.options.start 	= ($this.options.current-1)*$this.options.limit;		
			this._getScoreCards();
		},
		
		_getFirst  			: function( $this ) {
			$this.options.current   = 1;
			$this.options.start 	= 0;
			this._getScoreCards();
		}
	
	
});
