/**
 * Created with JetBrains PhpStorm.
 * User: admire
 * Date: 2013/09/24
 * Time: 6:15 AM
 * To change this template use File | Settings | File Templates.
 */
$(function(){

    $(".completion-rule").hide();
    $(".allocate-user-rule").hide();
    $(".movement_tracked").hide();
    $(".recurring_frequency").hide();


    $("#financial_year_id").click(function(){
        ScoreCardSetup.getActiveScoreCards( $(this).val() );
        return false;
    });

    $("#frequency_id").live("click", function(e){
        if($(this).val() == 'once_off')
        {
            $(".movement_tracked").hide();
            $(".recurring_frequency").hide();
            //ScoreCardSetup.displayOnceOffForm();
        } else if($("#frequency_id").val() == 'recurring') {
            $(".movement_tracked").show();
            $(".recurring_frequency").show();
            //ScoreCardSetup.displayRecurringForm();
        }
        return false;
    });

    $("#activate_score_card").live("click", function(e){
        ScoreCardSetup.activateScoreCard();
        e.preventDefault();
    });

    $("#completion_rule").click(function(e){
        $(".completion-rule").hide();
        if($(this).val() == 'all')
        {
            $(".all-users-list").show();
        } else {
            $(".allocate").show();
        }
        e.preventDefault();
    });

    $("#completion_rule_allocate_to").click(function(e){
        $(".allocate-rule").hide();
        if($(this).val() == 'action_owner')
        {
            $(".allocate-to-users-list").show();
            //ScoreCardSetup.displayUserQuestionForm();
        } else {
            //ScoreCardSetup.displayDirectorateQuestionForm();
            $(".allocate-to-directorate-list").show();
        }
        e.preventDefault();
    });

    $("#completion_rule_allocate_to").live('click', function(){

        if($(this).val() == 'action_owner')
        {
            $.get('../controllers/scorecardallocatetousercontroller.php?action=getUserQuestionForm',{
                score_card_id : $("#score_card_id").val()
            }, function( responseHtml ){
                $("#score_card_setup_user_question_form").html( responseHtml );
            }, 'html');
        } else {
            $.get('../controllers/scorecardallocatetodirectoratecontroller.php?action=getDirectorateQuestionForm',{
                score_card_id : $("#score_card_id").val()
            }, function( responseHtml ){
                $("#score_card_setup_directorate_question_form").html( responseHtml );
            }, 'html');
        }
        return false;
    });


    $("#allocate").live('click', function(){

        if($("#question_id").val() == '')
        {
            $("#message").html("Please select the question");
            return false;
        } if($("#user_id").val() == '') {
            $("#message").html("Please select the user");
            return false;
        } else {
            $.post('../controllers/scorecardallocatetousercontroller.php?action=getUserQuestionForm',{
                score_card_id : $("#score_card_id").val(),
                question_id   : $("#question_id").val(),
                user_id       : $("#user_id").val(),
                users         : $("#setup-score-card-form").serialize()
            }, function( responseHtml ) {
                $("#score_card_setup_user_question_form").html( responseHtml );
            }, 'html');
        }
        return false;
    });

    $("#allocate_directorate").live('click', function(){

        if($("#question_id").val() == '')
        {
            $("#message").html("Please select the question");
            return false;
        } if($("#directorate_id").val() == '') {
            $("#message").html("Please select the directorate");
            return false;
        } else {
            $.post('../controllers/scorecardallocatetodirectoratecontroller.php?action=getDirectorateQuestionForm',{
                score_card_id : $("#score_card_id").val(),
                question_id   : $("#question_id").val(),
                directorate_id: $("#directorate_id").val(),
                directorates  : $("#setup-score-card-form").serialize()
            }, function( responseHtml ) {
                $("#score_card_setup_directorate_question_form").html( responseHtml );
            }, 'html');
        }
        return false;
    });




});

var ScoreCardSetup    = {

    getActiveScoreCards         : function( financial_year )
    {
       var self = this;
        $("#score_card_id").html("<option>loading...</option>");
        $.getJSON("../controllers/scorecardcontroller.php?action=getAll",{
            section        : 'admin',
            page           : 'setup_score_card',
            financial_year : financial_year
        }, function( scoreCards ){
            var html = [];
            html.push("<option value=''>--please select--</option>");
            $.each(scoreCards, function(scoreCardId, scoreCard){
               html.push("<option value='"+scoreCardId+"'>"+scoreCard+"</option>");
            });
            $("#score_card_id").empty().append( html.join(' ') );
        });

    } ,

    displayRecurringForm        : function()
    {
/*
        var self = this;
        if($("#score_card_recurring_form").length > 0)
        {
            $("#score_card_recurring_form").remove();
        }
        var html = [];
        html.push("<table>");
            html.push("<tr>");
                html.push("<th>Frequency Name</th>");
                html.push("<th>Period Start Date</th>");
                html.push("<th>Period End Date</th>");
                html.push("<th>Period Open From</th>");
                html.push("<th>Period Open To</th>");
                html.push("<th>Status</th>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td><input name='frequency_name' id='frequency_name' value='' /></td>");
                html.push("<td><input name='period_start_date' id='period_start_date' value='' class='datepicker' readonly='readonly' /></td>");
                html.push("<td><input name='period_end_date' id='period_end_date' value='' class='datepicker' readonly='readonly' /></td>");
                html.push("<td><input name='period_open_from' id='period_open_from' value='' class='datepicker' readonly='readonly' /></td>");
                html.push("<td><input name='period_open_to' id='period_open_to' value='' class='datepicker' readonly='readonly' /></td>");
            html.push("<td></td>");
            html.push("</tr>");
            html.push("<tr>");
            html.push("</tr>");
            html.push("<tr>");
            html.push("</tr>");
            html.push("<tr>");
            html.push("</tr>");
            html.push("<tr>");
            html.push("</tr>");
            html.push("<tr>");
            html.push("</tr>");
        html.push("</table>");

        $("<div>", { id : "score_card_recurring_form", html: html.join(' ') })
            .dialog({
                modal   : true,
                width   : 'auto',
                title   : 'Recurring Frequency Setup',
                buttons : {
                    'Save'  : function()
                    {
                        $("#score_card_recurring_form").html("saving . . . ");
                    } ,

                    'Cancel'  : function()
                    {
                        $("#score_card_recurring_form").dialog('destroy').remove();
                    }
                } ,

                open       : function(event, ui)
                {
                    $(".datepicker").datepicker({
                        showOn: 'both',
                        buttonImage: '/library/jquery/css/calendar.gif',
                        buttonImageOnly: true,
                        dateFormat: 'dd-M-yy',
                        changeMonth:true,
                        changeYear:true
                    });
                } ,

                close      : function(event, ui)
                {
                    $("#score_card_recurring_form").dialog('destroy').remove();
                }

            });

*/

    } ,

    displayUserQuestionForm           : function()
    {
        var self = this;
        if($("#score_card_setup_user_question_form").length > 0)
        {
            $("#score_card_setup_user_question_form").remove();
        }
        var html = [];
        $("<div>", { id : "score_card_setup_user_question_form", html: html.join(' ') })
            .dialog({
                modal   : true,
                width   : 'auto',
                title   : 'Scorecard Setup User Question',
                position: 'top',
                buttons : {
                    'Done'  : function()
                    {
                        $("#score_card_setup_user_question_form").html("saving . . . ");
                        //console.log( $("#allocate-user-to-question").toJSON() );
                    } ,

                    'Cancel'  : function()
                    {
                        $("#score_card_setup_user_question_form").dialog('destroy').remove();
                    }
                } ,

                open       : function(event, ui)
                {
                    $(".datepicker").datepicker({
                        showOn: 'both',
                        buttonImage: '/library/jquery/css/calendar.gif',
                        buttonImageOnly: true,
                        dateFormat: 'dd-M-yy',
                        changeMonth:true,
                        changeYear:true
                    });
                    if($("#score_card_id").val() == null)
                    {
                        $("#score_card_setup_user_question_form").html('Please select the score card');
                        $("#score_card_id").focus()
                        $("#completion_rule_allocate_to").val('')
                        return false;
                    } else {
                        $.get('../controllers/scorecardallocatetousercontroller.php?action=getUserQuestionForm',{
                            score_card_id : $("#score_card_id").val()
                        }, function( responseHtml ){
                            $("#score_card_setup_user_question_form").html( responseHtml );

                            $("#allocate").live('click', function(){

                                 if($("#question_id").val() == '')
                                 {
                                     $("#message").html("Please select the question");
                                     return false;
                                 } if($("#user_id").val() == '') {
                                    $("#message").html("Please select the user");
                                    return false;
                                 } else {
                                    $.post('../controllers/scorecardallocatetousercontroller.php?action=getUserQuestionForm',{
                                       score_card_id : $("#score_card_id").val(),
                                       question_id   : $("#question_id").val(),
                                       user_id       : $("#user_id").val(),
                                       users         : $("#allocate-user-to-question").serialize()
                                    }, function( responseHtml ) {
                                        $("#score_card_setup_user_question_form").html( responseHtml );
                                    }, 'html');
                                 }
                                return false;
                            });


                        }, 'html');
                    }
                } ,

                close      : function(event, ui)
                {
                    $("#score_card_setup_user_question_form").dialog('destroy').remove();
                }

            });
    } ,


    displayDirectorateQuestionForm     : function()
    {
        var self = this;
        if($("#score_card_setup_directorate_question_form").length > 0)
        {
            $("#score_card_setup_directorate_question_form").remove();
        }
        var html = [];
        $("<div>", { id : "score_card_setup_directorate_question_form", html: html.join(' ') })
            .dialog({
                modal   : true,
                width   : 'auto',
                title   : 'Scorecard Setup Directorate/Sub-Directorate Question',
                buttons : {
                    'Done'  : function()
                    {
                        $("#score_card_setup_directorate_question_form").html("saving . . . ");
                    } ,

                    'Cancel'  : function()
                    {
                        $("#score_card_setup_directorate_question_form").dialog('destroy').remove();
                    }
                } ,

                open       : function(event, ui)
                {
                    $(".datepicker").datepicker({
                        showOn: 'both',
                        buttonImage: '/library/jquery/css/calendar.gif',
                        buttonImageOnly: true,
                        dateFormat: 'dd-M-yy',
                        changeMonth:true,
                        changeYear:true
                    });
                } ,

                close      : function(event, ui)
                {
                    $("#score_card_setup_directorate_question_form").dialog('destroy').remove();
                }

            });
    } ,

    activateScoreCard            : function()
    {
        jsDisplayResult("info", "info", "Activating scorecard . . .  <img src='../images/loaderA32.gif' />");
        $.getJSON("../controllers/scorecardsetupcontroller.php?action=setupScoreCard",{
            score_card : $("#setup-score-card-form").serialize()
        }, function(response) {
            if(response.error)
            {
                jsDisplayResult("error", "error", response.text);
            } else {
                jsDisplayResult("ok", "ok", response.text);
                $("input[type='text']").each(function(){
                    $(this).val('');
                });
                $('select').each(function(){
                    $(this).val('');
                });
                $('textarea').each(function(){
                    $(this).val('');
                });
            }
        });
    }

}

