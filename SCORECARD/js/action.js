/**
 * Created with JetBrains PhpStorm.
 * User: admire
 * Date: 2013/09/24
 * Time: 6:15 AM
 * To change this template use File | Settings | File Templates.
 */
$(function(){

    $("#add_more_supplier").click(function(e){

        e.preventDefault();
    });

    $("#save").click(function(e){

         if($("#name").val() == '') {
            $("#name").focus().select()
            jsDisplayResult("error", "error", 'Action name is required');
            return false;
        } else if($("#rating_scale_type").val() == '' ) {
            $("#rating_scale_type").focus().select()
            jsDisplayResult("error", "error", 'Rating scale type is required');
            return false;
        } else {
             jsDisplayResult("info", "info", "Saving action . . . <img  src='../images/loaderA32.gif' />");
             $.post("../controllers/actioncontroller.php?action=save",{
                name              : $("#name").val(),
                owner_id          : $("#owner_id").val(),
                deadline_date     : $("#action_deadline_date").val(),
                remind_on         : $("#remind_on").val(),
                deliverable_id    : $("#deliverable_id").val(),
                deliverable       : $("#deliverable").val(),
                rating_scale_type : $("#rating_scale_type").val()
             }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    jsDisplayResult("ok", "ok", response.text);
                    $("input[type='text']").each(function(){
                        $(this).val('');
                    });
                    $('select').each(function(){
                        $(this).val('');
                    });
                    $('textarea').each(function(){
                        $(this).val('');
                    });
                    $("#action_list").action({editDeliverable: true});
                }
             }, 'json');
        }

        e.preventDefault();
    })


    $("#save_edit_changes").click(function(e){

        if($("#name").val() == '') {
            $("#name").focus().select()
            jsDisplayResult("error", "error", 'Action name is required');
            return false;
        } else if($("#rating_scale_type").val() == '') {
            $("#rating_scale_type").focus().select()
            jsDisplayResult("error", "error", 'Rating Scale type is required');
            return false;
        } else {
            jsDisplayResult("info", "info", "Updating action . . .  <img  src='../images/loaderA32.gif' />");
            $.post("../controllers/actioncontroller.php?action=editAction", {
                name              : $("#name").val(),
                owner_id          : $("#owner_id").val(),
                deadline_date     : $("#action_deadline_date").val(),
                remind_on         : $("#remind_on").val(),
                id                : $("#action_id").val(),
                deliverable       : $("#deliverable").val(),
                deliverable_id    : $("#deliverable_id").val(),
                rating_scale_type : $("#rating_scale_type").val()
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text);
                    } else {
                        jsDisplayResult("info", "info", response.text);
                    }
                }
            }, 'json');
        }

        e.preventDefault();
    })

    $("#status_id").change(function(e) {
        var thisValue = $(this).val();
        if(thisValue == 3)
        {
            $("#progress").val('100').attr('readonly', 'readonly');
            $("#approval").removeAttr("disabled");
        } else {
            if( $("#progress").val() == 100 && $("#_progress").val() != 100)
            {
                $("#progress").val($("#_progress").val()).removeAttr('readonly');
            }
            $("#approval").attr("disabled", "disabled");
        }
        e.preventDefault();
    });

    $("#progress").blur(function(e) {
        var thisValue = $(this).val();
        if(thisValue == 100)
        {
            $("#status_id").val(3);
            $("#progress").attr('readonly', 'readonly');
            $("#approval").removeAttr("disabled");
        } else {
            if( $("#status_id").val() == 3 && $("#_status").val() != 3)
            {
                $("#status_id").val( $("#_status").val() );
            }
            $("#approval").attr("disabled", "disabled");
        }
        e.preventDefault();
    });

    $("#save_action_update").click(function(e){
        if($("#response").val() == '') {
            $("#response").focus().select()
            jsDisplayResult("error", "error", 'Response message is required');
            return false;
        } else if($("#status_id").val() == '' ) {
            $("#status_id").focus().select()
            jsDisplayResult("error", "error", 'Status is required');
            return false;
        } else if($("#action_on").val() == '') {
            $("#action_on").focus().select()
            jsDisplayResult("error", "error", 'Action on is required');
            return false;
        }  else if($("#progress").val() == '') {
            $("#progress").focus().select()
            jsDisplayResult("error", "error", 'Progress on is required');
            return false;
        } else if( isNaN( $("#progress").val() ) ) {
            jsDisplayResult("error", "error", "Progress must be a number ");
            message.html(" ");
            return false;
        } else if($("#progress").val() == "100" && $("#status_id").val() !== "3" ) {
            jsDisplayResult("error", "error", "Progress cannot be 100% , if the status in not addressed");
            return false;
        }  else {
            jsDisplayResult("info", "info", "Updating action . . . <img  src='../images/loaderA32.gif' />");
            $.getJSON("../controllers/actioncontroller.php?action=updateAction", {
                 update_data : $("#action_update_form").serialize()
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text);
                    } else {
                        jsDisplayResult("info", "info", response.text);
                    }
                }
            }, 'json');
        }
        e.preventDefault();
    });


    $("#view_deliverable").click(function(e){
        $("<div />",{id:"action_deliverable"})
            .dialog({
                modal      : true,
                autoOpen   : true,
                title      : "Action Deliverable Details",
                position   : "left top",
                width      : 'auto',
                buttons    : {
                    "Ok"        : function()
                    {
                        $("#action_deliverable").remove();
                    }
                } ,
                open       : function(ui, event)
                {
                    $("#action_deliverable").html("Loading deliverable details ...");
                    $.get("../controllers/deliverablecontroller.php?action=getDeliverableHtml",{
                        id  : $("#deliverable_id").val()
                    } , function(deliverableHtml){
                        $("#action_deliverable").html("");
                        $("#action_deliverable").append( deliverableHtml );
                    },"html")
                },
                close		: function() {
                    $("#action_deliverable").remove();
                }
            });
        e.preventDefault()
    });


    $("#view_score_card").click(function(e){
        $("<div />",{id:"score_card_details"})
            .dialog({
                modal      : true,
                autoOpen   : true,
                title      : "Score Card Details",
                position   : "left top",
                width      : 'auto',
                buttons    : {
                    "Ok"        : function()
                    {
                        $("#score_card_details").remove();
                    }
                } ,
                open       : function(ui, event)
                {
                    $("#score_card_details").html("Loading contract details ...");
                    $.get("../controllers/scorecardcontroller.php?action=getScoreCardDetailHtml",{
                        id  : $("#score_card_id").val()
                    } , function( scoreCardHtml
                        ){
                        $("#score_card_details").html("");
                        $("#score_card_details").append( scoreCardHtml );
                    },"html")
                },
                close		: function() {
                    $("#score_card_details").remove();
                }
            });
        e.preventDefault()
    });

    $(".remove_attach").live("click", function(e){
        var id       = this.id
        var doc_id   = $(this).parent().attr('id');
        var ext      = $(this).attr('title');
        var type     = $(this).attr('ref');
        var filename = $(this).attr("file");
        $.post('../controllers/actioncontroller.php?action=deleteActionAttachment',
            {
                doc_id     : doc_id,
                ext        : ext,
                action_id  : $("#action_id").val(),
                type       : type,
                filename   : filename
            }, function(response){
                if(response.error)
                {
                    $("#result_message").css({padding:"5px", clear:"both"}).html(response.text)
                } else {
                    $("#result_message").addClass('ui-state-ok').css({padding:"5px", clear:"both"}).html(response.text)
                    var deleted_doc = "<input type='hidden' name='deleted_action_attachment["+doc_id+"]' id='deleted_attachment_"+doc_id+"' value='"+doc_id+"' />";
                    $("#result_message").append(deleted_doc);
                    $("#li_"+ext).fadeOut();
                }
            },'json');
        e.preventDefault();
    });

    $(".upload_action").live("change", function(e){
        Action.uploadAttachment(this.id);
        e.preventDefault();
    });
});

function attachActionDocuments()
{
    $("#file_upload").html("uploading . . . <img  src='../images/loaderA32.gif' />");
    $.ajaxFileUpload
    (
        {
            url			  : '../controllers/actioncontroller.php?action=saveAttachment',
            secureuri	  : false,
            fileElementId : 'action_attachment',
            dataType	  : 'json',
            success       : function (response, status)
            {
                $("#file_upload").html("");
                if(response.error )
                {
                    $("#file_upload").html(response.text )
                } else {
                    $("#file_upload").html("");
                    if(response.error)
                    {
                        $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                    } else {
                        $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                        ).append($("<div />",{id:"files_uploaded"}))
                        if(!$.isEmptyObject(response.files))
                        {
                            var list = [];
                            list.push("<span>Files uploaded ..</span><br />");
                            $.each(response.files, function(ref, file) {
                                list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                                    list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                    list.push("<span style='margin-left:5px;'><a href='../controller/contractcontroller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                    list.push("<input type='hidden' name='action_document["+file.key+"]' value='"+file.name+"' id='action_document_"+ref+"' />");
                                list.push("</p>");
                            });
                            $("#files_uploaded").html(list.join(' '));

                            $(".delete").click(function(e){
                                var id = this.id
                                var ext = $(this).attr('title');
                                $.post('../controllers/actioncontroller.php?action=removeActionAttachment',
                                    {
                                        attachment : id,
                                        ext        : ext
                                    }, function(response){
                                        if(response.error)
                                        {
                                            $("#result_message").html(response.text)
                                        } else {
                                            $("#result_message").addClass('ui-state-ok').html(response.text)
                                            $("#li_"+id).fadeOut();
                                        }
                                    },'json');
                                e.preventDefault();
                            });
                            $("#action_attachment").val("");
                        }
                    }

                    if( $("#delmessage").length != 0)
                    {
                        $("#delmessage").remove()
                    }
                }
            },
            error: function (data, status, e)
            {
                $("#fileupload").html( "Ajax error -- "+e+", please try uploading again");
            }
        }
    )
    return false;
}


var Action  = {

    uploadAttachment        : function(element_id)
    {
        $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
        $.ajaxFileUpload
        (
            {
                url			  : '../controllers/actioncontroller.php?action=saveActionAttachment',
                secureuri	  : false,
                fileElementId : element_id,
                dataType	  : 'json',
                success       : function (response, status)
                {
                    $("#file_upload").html("");
                    if(response.error )
                    {
                        $("#file_upload").html(response.text )
                    } else {
                        if($("#result_message").length > 0)
                        {
                            $("#result_message").remove();
                        }

                        $("#file_upload").html("");
                        if(response.error)
                        {
                            $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                        } else {
                            $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                            ).append($("<div />",{id:"files_uploaded"}))
                            if(!$.isEmptyObject(response.files))
                            {
                                var list = [];
                                list.push("<span>Files uploaded ..</span><br />");
                                $.each(response.files, function(ref, file){
                                    list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                                    list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                    list.push("<span style='margin-left:5px;'><a href=''controller.php?action=downloadFile&file="+file.key+"' file='"+file.name+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                    list.push("</p>");
                                });
                                $("#files_uploaded").html(list.join(' '));

                                $(".delete").click(function(e){
                                    var id   = this.id
                                    var ext  = $(this).attr('title');
                                    var name = $(this).attr('file');
                                    $.post('controller.php?action=removeActionFile',
                                        {
                                            attachment : id,
                                            ext        : ext,
                                            name       : name,
                                            element    : element_id
                                        }, function(response){
                                            if(response.error)
                                            {
                                                $("#result_message").html(response.text)
                                            } else {
                                                $("#result_message").addClass('ui-state-ok').html(response.text)
                                                $("#li_"+id).fadeOut();
                                            }
                                        },'json');
                                    e.preventDefault();
                                });
                                $("#"+element_id).val("");
                            }
                        }

                    }
                },
                error: function (data, status, e)
                {
                    $("#fileupload").html( "Ajax error -- "+e+", please try uploading again");
                }
            }
        )
        return false;
    }


}