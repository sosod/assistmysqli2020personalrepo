$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    AssessmentFrequency.get();
    $("#add_assessment_frequency").click(function(e){
        AssessmentFrequency.addAssessmentFrequency();
        e.preventDefault();
    });
	
});

var AssessmentFrequency 	= {

		get				: function()
		{
            $(".assessment_frequencies").remove();
			$.getJSON( "../controllers/assessmentfrequencycontroller.php?action=getAll", function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#assessment_frequency_table").append($("<tr />").addClass('assessment_frequencies')
                        .append($("<td />", {colspan: 6, html: "There are no assessment frequencies" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        AssessmentFrequency.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#assessment_frequency_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('assessment_frequencies')
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.short_code}))
				.append($("<td />",{html:val.name}))
				.append($("<td />",{html:val.description}))
                .append($("<td />",{html:val.date}))
                .append($("<td />",{html:"<b>"+(val.status == 1 ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"frequency_edit_"+val.id, name:"frequency_edit_"+val.id }))
				 )
			  )

            $("#frequency_edit_"+val.id).live("click", function(e){
                self._updateAssessmentFrequency(val);
                e.preventDefault();
            });

		},

    addAssessmentFrequency          : function()
    {
        var self = this;
        if($("#add_assessment_frequency_dialog").length > 0)
        {
            $("#add_assessment_frequency_dialog").remove();
        }

        $("<div />",{id:"add_assessment_frequency_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"short_code", id:"short_code", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Name :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Date:"}))
                    .append($("<td />")
                        .append($("<input />",{type: "text", name:"date", id:"date"}).addClass('datepicker'))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Assessment Frequency",
                width          : 500,
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the short code");
                            return false;
                        } else if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the name");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_assessment_frequency_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_assessment_frequency_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});

                    $(".datepicker").datepicker({
                        showOn		  : "both",
                        buttonImage 	  : "/library/jquery/css/calendar.gif",
                        buttonImageOnly  : true,
                        changeMonth	  : true,
                        changeYear	  : true,
                        dateFormat 	  : "dd-M-yy",
                        altField		  : "#startDate",
                        altFormat		  : 'd_m_yy'
                    });
                }
            })
    },

    _updateAssessmentFrequency         : function(assessment_frequency)
    {
        var self = this;
        if($("#edit_assessment_frequency_dialog").length > 0)
        {
            $("#edit_assessment_frequency_dialog").remove();
        }

        $("<div />",{id:"edit_assessment_frequency_dialog"}).append($("<table />",{width:"100%"})
               .append($("<tr />")
                    .append($("<th />",{html:"Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"short_code", id:"short_code", value:assessment_frequency.short_code}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Name :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:assessment_frequency.name}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8, text:assessment_frequency.description}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Date:"}))
                    .append($("<td />")
                        .append($("<input />",{type: "text", name:"date", id:"date", value: assessment_frequency.asses_date}).addClass('datepicker'))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Assessment Frequency",
                width          : 500,
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the short code");
                            return false;
                        } else if($("#name").val() == "") {
                            $("#message").addClass("ui-state-error").html("please enter the name");
                            return false;
                        } else {
                            self.update(assessment_frequency.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_assessment_frequency_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(assessment_frequency.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(assessment_frequency.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(assessment_frequency.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_assessment_frequency_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((assessment_frequency.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});

                    $(".datepicker").datepicker({
                        showOn		  : "both",
                        buttonImage 	  : "/library/jquery/css/calendar.gif",
                        buttonImageOnly  : true,
                        changeMonth	  : true,
                        changeYear	  : true,
                        dateFormat 	  : "dd-M-yy",
                        altField		  : "#startDate",
                        altFormat		  : 'd_m_yy'
                    });
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/assessmentfrequencycontroller.php?action=update",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_assessment_frequency_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/assessmentfrequencycontroller.php?action=update",
        {
            id          : id,
            short_code  : $("#short_code").val(),
            name        : $("#name").val(),
            description : $("#description").val(),
            date        : $("#date").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_assessment_frequency_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/assessmentfrequencycontroller.php?action=save",
            {
                short_code : $("#short_code").val(),
                name:        $("#name").val(),
                description: $("#description").val(),
                date        : $("#date").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_assessment_frequency_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    AssessmentFrequency.get()
                }
            }, 'json');
    }

}