$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    Template.get();
    $("#add_contract_template").click(function(e){
        Template.addTemplate();
        e.preventDefault();
    });
	
});

var Template 	= {

		get				: function()
		{
            $(".contract_templates").remove();
			$.getJSON( "../controllers/contracttemplatecontroller.php?action=getContractTemplates",{
              contract_id : $("#contract_id").val()
            }, function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#contract_templates_table").append($("<tr />").addClass('contract_templates')
                        .append($("<td />", {colspan: 6, html: "There are no contract templates" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        Template.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#contract_templates_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('contract_templates')
                .append($("<td />",{html:val.id}))
                .append($("<td />",{html:val.name}))
                .append($("<td />",{html:val.created_at}))
                .append($("<td />",{html:"<b>"+((val.status & 1) == 1  ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"edit_"+val.id, name:"edit_"+val.id }))
				 )
                .append($("<td />")
                    .append($("<input />",{type:"submit", value:"Template Settings", id:"edit_setting_"+val.id, name:"edit_setting_"+val.id }))
                )
			  )

            $("#edit_"+val.id).live("click", function(e){
                self._updateTemplate(val);
                e.preventDefault();
            });

            $("#edit_setting_"+val.id).live("click", function(e){
                document.location.href = "template_setting.php?id="+val.id;
                e.preventDefault();
            });
			
		},

    addTemplate          : function()
    {
        var self = this;
        if($("#add_contract_template_dialog").length > 0)
        {
            $("#add_contract_template_dialog").remove();
        }

        $("<div />",{id:"add_contract_template_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Contract Template Name:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:''}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Contract Template",
                width          : 500,
                buttons        : {
                    "Save"       : function()
                    {
                        if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the status name");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_contract_template_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_contract_template_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});
                }
            })



    },

    _updateTemplate         : function(contract_template)
    {
        var self = this;
        if($("#edit_contract_template_dialog").length > 0)
        {
            $("#edit_contract_template_dialog").remove();
        }

        $("<div />",{id:"edit_contract_template_dialog"}).append($("<table />",{width:"100%"})
               .append($("<tr />")
                    .append($("<th />",{html:"Contract Template Name:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:contract_template.name}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Contract Template",
                width          : 500,
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the status name");
                            return false;
                        }  else {
                            $("#message").html("Updating . . .")
                            self.update(contract_template.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_contract_template_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(contract_template.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(contract_template.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(contract_template.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_contract_template_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((contract_template.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }
                    
                    if((contract_template.status & 4) == 4)
                    {
                        $(deleteBtn).css({"display":"none"});
                        $(deactivateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/contracttemplatecontroller.php?action=updateContractTemplate",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_contract_template_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/contracttemplatecontroller.php?action=updateContractTemplate",
        {
            id                  : id,
            name                : $("#name").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_contract_template_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/contracttemplatecontroller.php?action=saveContractTemplate",
            {
                name                : $("#name").val(),
                contract_id         : $("#contract_id").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_contract_template_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    Template.get()
                }
            }, 'json');
    }

}