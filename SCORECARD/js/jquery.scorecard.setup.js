$.widget('scorecard.setup', {

    options     : {
        tableRef        : 'setup_score_card_table_'+(Math.floor(Math.random(563) * 313)),
        url 	        : "../controllers/scorecardsetupcontroller.php?action=getSetupDetail",
        scoreCardId     : 0,
        financialYear   : 0,
        autoLoad        : false,
        page            : '',
        section         : '',
        editAction      : true,
        allocateToDirectorate : false,
        allocateToUser        : false
    } ,


    _init        : function()
    {
        if(this.options.autoLoad)
        {
            this._getSetupDetail();
        }
    },

    _create       : function()
    {
        var self = this;
        var html = [];
        html.push("<form name='setup_score_card_form' id='setup_score_card_form'>");
        html.push("<table class='noborder' id='setup-table'  width='100%'>");
            html.push("<tr>");
                html.push("<td class='noborder' colspan='2'>");
                    html.push("<table id='"+self.options.tableRef+"' class='noborder'>");
                        html.push("<tr>");
                            html.push("<th valign='top' style='text-align: left;'>Financial Year:</th>");
                            html.push("<td valign='top'>");
                                html.push("<select name='setup[financial_year_id]' id='confirm_financial_year_id'>");
                                html.push("</select>");
                            html.push("</td>");
                            html.push("<td valign='top' rowspan='2' class='noborder'>");
                                html.push("<p id='score_card_full_name' style='display: hidden; margin-top:0px;'></p>");
                            html.push("</td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th valign='top' style='text-align: left;'>Scorecard:</th>");
                            html.push("<td valign='top'>");
                                html.push("<select name='setup[score_card_id]' id='score_card_id'>");
                                html.push("</select>");
                            html.push("</td>");
                        html.push("</tr>");
                    html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>")
                html.push("<td class='noborder'>");
                    html.push("<table style='"+(self.options.autoLoad ? "" : "display:none")+";' class='details noborder' width='100%'>");
                        html.push("<tr>");
                            html.push("<td valign='top' class='noborder'  width='50%' colspan='2'>");
                                html.push("<table id='"+self.options.tableRef+"_score_card' width='100%'></table>");
                            html.push("</td>");
                            html.push("<td valign='top' class='noborder' id='score_card_setup' width='50%'></td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<td valign='top' class='noborder' colspan='3'>");
                                html.push("<table id='"+self.options.tableRef+"_deliverable' width='100%'></table>");
                            html.push("</td>");
                        html.push("</tr>");
                    html.push("</table>");
                html.push("</td>");
            html.push("</tr>")
        html.push("</table>");
        html.push("</form>");
        $(this.element).append(html.join(' '));

        $(".datepicker").datepicker({
            showOn: 'both',
            buttonImage: '/library/jquery/css/calendar.gif',
            buttonImageOnly: true,
            dateFormat: 'dd-M-yy',
            changeMonth:true,
            changeYear:true
        });

        self._loadFinancialYears();
        self._loadScoreCards();

        $("#score_card_id").live("change", function(e){
            $("#"+self.options.tableRef+"_score_card").html("");
            $("#"+self.options.tableRef+"_summary").html("");
            $("#"+self.options.tableRef+"_deliverable").html("");
            if($(this).val() !== "")
            {
                self.options.scoreCardId = $(this).val();
                self.options.showActions = true
                self._loadLinkToScoreCards();
                self._getSetupDetail();
                self._displaySetupForm()
                var longDescription = $("select#score_card_id :selected").attr("title");
                $("#score_card_full_name").show().html( longDescription ).addClass('ui-state-info').css({'padding': '5px;'});
                $(".details").show();
            } else {
                self.options.scoreCardId = 0;
                self._getSetupDetail();
                $(".details").hide();
            }
        });



    },

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#confirm_financial_year_id").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                if(self.options.financialYear == year.id )
                {
                    finHtml.push("<option value='"+year.id+"' selected='selected'>"+year.value+"</option>");
                } else {
                    finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
                }
            });
            $("#confirm_financial_year_id").html( finHtml.join(' ') );
        });
    } ,

    _loadScoreCards     : function()
    {
        var self = this;
        $("#score_card_id").html("");
        $.getJSON("../controllers/scorecardcontroller.php?action=getAdminList", {
            section:       'admin',
            page:          'setup_score_card'
        }, function(scorecards) {
            var scoreCardHtml = []
            scoreCardHtml.push("<option value=''>--please select--</option>");
            $.each(scorecards, function(score_card_id, score_card){
                if(self.options.scoreCardId == score_card_id)
                {
                    scoreCardHtml.push("<option value='"+score_card_id+"' selected='selected' title='"+score_card.full_name+"'>"+score_card.short_description+"</option>");
                } else {
                    scoreCardHtml.push("<option value='"+score_card_id+"' title='"+score_card.full_name+"'>"+score_card.short_description+"</option>");
                }
            });
            $("#score_card_id").html( scoreCardHtml.join(' ') );
        });
    } ,


    _loadLinkToScoreCards     : function()
    {
        var self = this;
        $("#setup_link_to").html("");
        $.getJSON("../controllers/scorecardsetupcontroller.php?action=getSetupScoreCardList", {
            section       : 'admin',
            page          : 'setup_score_card',
            score_card_id : self.options.scoreCardId
        }, function(scorecards){
            var scoreCardHtml = []
            scoreCardHtml.push("<option value=''>--please select--</option>");
            $.each(scorecards, function(score_card_id, score_card){
                scoreCardHtml.push("<option value='"+score_card_id+"'>"+score_card+"</option>");
            });
            $("#setup_link_to").html( scoreCardHtml.join(' ') );
        });
    } ,


    _loadUsers           : function()
    {
        var self = this;
        $("#setup_recipient_user").html("");
        $.getJSON("../controllers/usercontroller.php?action=getAll", function( users ){
            var userHtml = []
            $.each(users, function(user_id, user){
                userHtml.push("<option value='"+user_id+"'>"+user+"</option>");
            });
            $("#setup_recipient_user").html( userHtml.join(' ') );
        });
    } ,

    _getSetupDetail      : function()
    {
        var self = this;
        $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
        );
        $.getJSON(self.options.url,{
            section		      : self.options.section,
            page              : self.options.page,
            score_card_id     : self.options.scoreCardId
        },function(setupData) {
            $("#loadingDiv").remove();
            $(".score_card_message").remove();
            $(".confirm_button").remove();
            $(".setup-data").remove();
            if(setupData.hasOwnProperty('score_card'))
            {
                self._displayScoreCard(setupData.score_card);
            }
            if(setupData.hasOwnProperty('categorized_headings'))
            {
                var categoryHtml = [];
                self._displayQuestionsHeader(setupData.columns);
                $.each(setupData.categorized_headings, function(category_id, headings) {
                    categoryHtml.push("<tr class='setup-data'>");
                      categoryHtml.push("<td colspan='"+(setupData.total_columns+2)+"'><b>"+setupData['categories'][category_id]+"</b></td>");
                    categoryHtml.push("</tr>");
                    $.each(headings, function(header_id, header){

                        if(header.is_main_deliverable == 1)
                        {
                           categoryHtml.push("<tr class='setup-data'>");
                            categoryHtml.push("<td  class='noborder' colspan='"+(setupData.total_columns+2)+"'><b>"+header.name+"</b></td>");
                           categoryHtml.push("</tr>");
                        } else {
                           categoryHtml.push("<tr class='setup-data'>");
                            categoryHtml.push("<td class='setup-data'></td>");
                            categoryHtml.push("<td  class='noborder' colspan='"+(setupData.total_columns+1)+"'>&nbsp;&nbsp;<b>"+header.name+"</b></td>");
                           categoryHtml.push("</tr>");
                        }
                        if(header.has_sub_deliverable == 0)
                        {
                            if(setupData.questions.hasOwnProperty(header_id))
                            {
                                categoryHtml.push( self._displayQuestions(setupData.questions[header_id], setupData) );
                            } else {
                                categoryHtml.push("<tr class='setup-data'><td colspan='"+(setupData.total_columns+1)+"'>&nbsp;</td></tr>")
                            }
                        }
                    });
                    categoryHtml.push("</tr>");

                });
                $("#"+self.options.tableRef+"_deliverable").append( categoryHtml.join(' ')).css({'text-align':'left'});
            }


            //if(setupData.hasOwnProperty('canConfirm') || setupData.canConfirm == 1)
            //{
                var activateButton = [];
                activateButton.push("<tr class='setup-data details'>");
                  activateButton.push("<td colspan='"+(setupData.total_columns+1)+"' class='noborder'>");
                    activateButton.push("<input type='submit' name='activate_score_card_"+self.options.scoreCardId+"' id='activate_score_card_"+self.options.scoreCardId+"' value='Activate Scorecard' class='isubmit activate_button'  />");
                  activateButton.push("</td>");
                activateButton.push("</tr>");
                $("#"+self.options.tableRef+"_deliverable").append( activateButton.join('') ).css({'text-align':'left'});
                $("#activate_score_card_"+self.options.scoreCardId).live('click', function(e){
                    self._activateScoreCard();
                    e.preventDefault();
                });
            //}

        });
    } ,

    _displayScoreCard            : function(score_card)
    {
        var self = this;
        var scoreCardHtml = [];
        $.each(score_card, function(column, score_card) {
            scoreCardHtml.push("<tr class='setup-data'>")
            scoreCardHtml.push("<th style='text-align: left;'>"+score_card.header+":</th>");
            scoreCardHtml.push("<td>"+score_card.value+"</td>");
            scoreCardHtml.push("</tr>");
        });
        $("#"+self.options.tableRef+"_score_card").append( scoreCardHtml.join(' ')).css({'text-align':'left'});
    } ,


    _displayQuestionsHeader       : function(headers)
    {
        var self = this;
        var html = [];
        html.push("<tr class='setup-data'>");
        html.push("<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>");
        $.each( headers , function( index, head ){
            html.push("<th>"+head+"</th>");
        });
        if(self.options.allocateToDirectorate)
        {
           html.push("<th>Assign To Directorate/Sub-Directorate</th>");
        }
        if(self.options.allocateToUser)
        {
           html.push("<th>Assign To User</th>");
        }
        html.push("</tr>");
        $("#"+self.options.tableRef+"_deliverable").append(html.join(' '));
    },

    _displayQuestions           : function(questions, setupData)
    {
        var self = this;
        var questionHtml = [];
        $.each(questions, function(question_id, question){
            questionHtml.push("<tr class='setup-data'>");
            questionHtml.push("<td>&nbsp;&nbsp;</td>");
            $.each(question, function(field, val){
                questionHtml.push("<td>"+val+"</td>");
            });
            if(self.options.allocateToDirectorate)
            {
                questionHtml.push("<td style='text-align: right;'>");
                questionHtml.push("<select name='setup[allocate_to_directorate]["+question_id+"]' id='allocate_to_directorate_"+question_id+"'>");
                questionHtml.push("<option value=''>--please select--</option>");
                $.each(setupData.directorates, function(directorate_id, directorate_name){
                    questionHtml.push("<option value='"+directorate_id+"'>"+directorate_name+"</option>");
                });
                questionHtml.push("</select>");
                questionHtml.push("</td>");
            }
            if(self.options.allocateToUser)
            {
              questionHtml.push("<td style='text-align: right;'>");
                questionHtml.push("<select name='setup[allocate_to_user]["+question_id+"]' id='allocate_to_user_"+question_id+"'>");
                questionHtml.push("<option value=''>--please select--</option>");
                $.each(setupData.users, function(user_id, user){
                    questionHtml.push("<option value='"+user_id+"'>"+user+"</option>");
                });
                questionHtml.push("</select>");
              questionHtml.push("</td>");
            }
            questionHtml.push("</tr>");
        });
        return questionHtml.join(' ');
    } ,

    _activateScoreCard            : function()
    {
        var self = this;
        if($("#score_card_ref").val() == '')
        {
            $("#score_card_ref").focus().select()
            jsDisplayResult("error", "error", 'Scorecard reference is required');
            return false;
        } else if($("#setup_description").val() == ''){
            $("#setup_description").focus().select()
            jsDisplayResult("error", "error", 'Scorecard instruction is required');
            return false;
        } else if($("#start_date").val() == ''){
            $("#start_date").focus().select()
            jsDisplayResult("error", "error", 'Scorecard start date is required');
            return false;
        } else if($("#end_date").val() == ''){
            $("#end_date").focus().select()
            jsDisplayResult("error", "error", 'Scorecard end date is required');
            return false;
        } else if($("#score_card_allocate_to").val() == '') {
            $("#score_card_allocate_to").focus().select()
            jsDisplayResult("error", "error", 'Please select the users to allocate the scorecard questions');
            return false;
        } else if($("#rating_type").val() == '') {
            $("#rating_type").focus().select()
            jsDisplayResult("error", "error", 'Overall rating type is required');
            return false;
        } else {
            jsDisplayResult("info", "info", "activating score card . . . <img  src='../images/loaderA32.gif' />");
            $.getJSON("../controllers/scorecardsetupcontroller.php?action=setupScoreCard", {
                setup_data : $("#setup_score_card_form").serialize()
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    jsDisplayResult("ok", "ok", response.text);
                    $("input[type='text']").each(function(){
                        $(this).val('');
                    });
                    $('select').each(function(){
                        $(this).val('');
                    });
                    $('textarea').each(function(){
                        $(this).val('');
                    });
                    //setTimeout(function(){
                        //document.location.reload();
                    //}, 1000);
                }
            });
        }
        /*
        if($("#confirm_score_card_dialog_"+self.options.scoreCardId).length > 0)
        {
            $("#confirm_score_card_dialog_"+self.options.scoreCardId).remove();
        }
        $("<div />",{id:"confirm_score_card_dialog_"+self.options.scoreCardId, html:"I hereby:"})
            .append($("<ul />")
                .append($("<li />",{html:"Confirm that I have reviewed all the deliverables of the Score Card;"}))
                .append($("<li />",{html:"Declare that I have the required authority to confirm this Score Card."}))
            )
            .append($("<div />",{id:"message"}).css({"padding":"5px"}))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Activation of Score Card",
                buttons     : {
                    "Activate"   : function()
                    {
                        $("#message").addClass("ui-state-info").html("confirming score card ... <img src='../images/loaderA32.gif' />");
                        $.getJSON("../controllers/setupscorecardcontroller.php?action=setupScoreCard", {
                            setup_data : $("#setup_score_card_form").serialize()
                        }, function(response) {
                            if(response.error)
                            {
                                $("#message").html(response.text).addClass('ui-state-error');
                            } else {
                                $("#message").html(response.text).addClass('ui-state-ok');
                                setTimeout(function(){
                                    document.location.reload();
                                }, 1000)
                            }
                        });
                    },
                    "Cancel"    : function()
                    {
                        $("#confirm_score_card_dialog_"+self.options.scoreCardId).dialog("destroy").remove()
                    }
                } ,
                "close"     : function(event, ui)
                {
                    $("#confirm_score_card_dialog_"+self.options.scoreCardId).dialog("destroy").remove()
                },
                "open"      : function(event, ui)
                {
                    var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                    var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
                    var saveButton = buttons[0];
                    $(saveButton).css({"color":"#090"});
                }
            });
            */
    } ,

    _displaySetupForm       : function()
    {
        var self = this;
        $("#score_card_setup").html("loading . . . <img  src='../images/loaderA32.gif' />");
        $.get("../controllers/scorecardsetupcontroller.php?action=setupScoreCardForm", {
            score_card_id : self.options.scoreCardId
        }, function( responseHtml ) {
            $("#score_card_setup").html( responseHtml );

            $(".datepicker").datepicker({
                showOn: 'both',
                buttonImage: '/library/jquery/css/calendar.gif',
                buttonImageOnly: true,
                dateFormat: 'dd-M-yy',
                changeMonth:true,
                changeYear:true
            });

            $("#score_card_allocate_to").live('change', function(e){
                self.options.allocateToUser = false;
                self.options.allocateToDirectorate = false;
                $(".completion-rule").hide();
                if($(this).val() == 'all')
                {
                    $(".all-users-list").show();
                    self._loadUsers();
                } else {
                    $(".allocate").show();
                }
                self._getSetupDetail();
                e.preventDefault();
            });

            $("#setup_assign_question_to").click(function(e){
                $(".allocate-rule").hide();
                self.options.allocateToUser = false;
                self.options.allocateToDirectorate = false;
                if($(this).val() == 'action_owner')
                {
                    self.options.allocateToUser = true;
                    self.options.allocateToDirectorate = false;
                    $(".allocate-to-users-list").show();

                } else {
                    self.options.allocateToUser = false;
                    self.options.allocateToDirectorate = true;
                    $(".allocate-to-directorate-list").show();
                }
                self._getSetupDetail();
                e.preventDefault();
            });

        }, 'html');
    }

});