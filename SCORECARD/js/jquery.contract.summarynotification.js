$.widget("ui.summarynotification",{

    options     : {
    
    
    
    } ,
    
    _init        : function()
    {
        this._getNotifications()    
    }, 
    
    _create      : function()
    {
        var self = this;
        var html = [];
        html.push("<table id='summary-notification-table'>");
          html.push("<tr>")
            html.push("<td colspan='7'>");
               html.push("<input type='button' name='add_notification' id='add_notification' value='Add' />");
               $("#add_notification").live("click", function(e){
                 self._addNotification();
                 e.preventDefault();
               })
            html.push("</td>");
          html.push("</tr>")
          html.push("<tr>");
            html.push("<th>Ref</th>");
            html.push("<th>Receive Email</th>");
            html.push("<th>Receive When</th>");
            html.push("<th>Receive What</th>");
            html.push("<th>Recipient(s)</th>");
            html.push("<th>Status</th>");
            html.push("<th></th>");
          html.push("</tr>");
        html.push("</table>");
        $(self.element).append(html.join(' '));
    
    } ,
    
    _getNotifications   : function()
    {
        var self = this;
	     $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"250px", left:"300px", border:"0px solid #FFFFF", padding:"5px"})
	     );		            
        $.getJSON("../controllers/summarynotificationcontroller.php?action=getSummaryNotifications",{
        
        }, function(noticationData){
            $(".notification-tr").remove();
            $("#loadingDiv").remove();
            if($.isEmptyObject(noticationData))
            {
              var html = [];
              html.push("<tr class='notification-tr'>");
                html.push("<td colspan='7'>No summary notifications have been setup</td>");
              html.push("</tr>");
              $("#summary-notification-table").append(html.join(' '));            
            } else {
               self._display(noticationData);            
            }
        }); 
    },
    
    _display            : function(notifications)
    {
       var self = this;
       var html = [];
       $.each(notifications, function(index, notification){
          html.push("<tr class='notification-tr'>");
            html.push("<td>"+index+"</td>");
            html.push("<td>"+notification.recieve_email+"</td>");
            html.push("<td>"+notification.recieve_when+"</td>");
            html.push("<td>"+notification.recieve_what+"</td>");
            html.push("<td>"+notification.recieve_who+"</td>");
            html.push("<td><b>"+notification.status+"</b></td>");
            html.push("<td>");
              html.push("<input type='button' name='edit_"+notification.id+"' id='edit_"+notification.id+"' value='Edit' />");
              html.push("<input type='button' name='delete_"+notification.id+"' id='delete_"+notification.id+"' value='Delete' />");
              
              $("#edit_"+notification.id).live("click", function(e){
                self._updateNotification(notification);         
                e.preventDefault();
              })
              
              $("#delete_"+notification.id).live("click", function(e){
                if(confirm("Are you sure you want to delete this summary notification"))
                {
                    self._deleteNotification(notification.id);
                }
                e.preventDefault();
              })              
            html.push("</td>"); 
          html.push("</tr>");
       });
       $("#summary-notification-table").append(html.join(' '));
    } ,
    
    _addNotification    : function()
    {
        var self = this;
        var html = [];
        html.push("<table width='100%' class=form>");
          html.push("<tr>")
            html.push("<th>Receive?:</th>");
            html.push("<td>");
              html.push("<select name='recieve_email' id='recieve_email'>");
                html.push("<option value='1'>Yes</option>");
                html.push("<option value='0'>No</option>");
              html.push("</select>");
            html.push("</td>");
          html.push("</tr>");
          html.push("<tr>")
            html.push("<th>When?:</th>");
            html.push("<td>");
              html.push("<select name='recieve_when' id='recieve_when'>");
                html.push("<option value='weekly'>Weekly</option>");
                html.push("<option value='daily'>Daily</option>");
              html.push("</select>");
              html.push("<span class='on_weekly'>");
                html.push("&nbsp;&nbsp;on&nbsp;&nbsp;");
                html.push("<select name='recieve_day' id='recieve_day'>");
                  html.push("<option value='1'>Monday</option>");
                  html.push("<option value='2'>Tuesday</option>");
                  html.push("<option value='3'>Wednesday</option>");
                  html.push("<option value='4'>Thursday</option>");
                  html.push("<option value='5'>Friday</option>");
                html.push("</select>");
              html.push("</span>");
            html.push("</td>");
          html.push("</tr>");
          html.push("<tr>")
            html.push("<th>What?:</th>");
            html.push("<td>");
              html.push("<select name='recieve_what' id='recieve_what'>");
                html.push("<option value='all_incomplete_actions'>All Incomplete actions</option>");
                html.push("<option value='due_this_week'>Actions due this week</option>");
                html.push("<option value='overdue_or_due_this_week'>Actions overdue or due this week</option>");
                html.push("<option value='due_today'>Actions due today</option>");
                html.push("<option value='overdue_or_due_today'>Actions overdue or due today</option>");
              html.push("</select>");
            html.push("</td>");
          html.push("</tr>"); 
          html.push("<tr>")
            html.push("<th>Recipient(s):</th>");
            html.push("<td>");
              html.push("<select name='recieve_who[]' id='recieve_who' multiple size=10>");
              html.push("</select>");
            html.push("</td>");
          html.push("</tr>"); 
          html.push("<tr><td colspan='2'><p id='message'></p></td></tr>");
        html.push("</table>");
        if($("#notification-dialog").length > 0)
        {
          $("#notification-dialog").remove();
        }
        $("<div />",{id:"notification-dialog"}).append(html.join(' '))
          .dialog({
                autoOpen     : true,
                modal        : true,
                position     : "top",
                width        : "auto",
                title        : "Summary Notification",
                buttons      : {
                                 "Save"   : function()
                                 {
                                    self._saveNotification();
                                 } ,
                                 
                                 "Cancel"            : function()
                                 {
                                    $("#notification-dialog").dialog("destroy").remove();
                                 }
                } ,
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn   = btns[0];
                    var cancelBtn = btns[1];          

                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});      
                    
                    $("#recieve_when").live("change", function(){
                        $(".on_weekly").toggle();                    
                    });

                    $.get("../controllers/usercontroller.php?action=getAll", function(userData) {
                        var user_html = []
                        $.each( userData, function( index, user ){
                            user_html.push("<option value='"+index+"'>"+user+"</option>");
                        });
                        $("#recieve_who").append( user_html.join(' ') );
                    },"json");
                } ,
                close       : function(event, ui)
                {
                   $("#notification-dialog").dialog("destroy").remove();
                }
          })
    } ,
    
    _saveNotification   : function(notification)
    { 
		var recieve_who = [];
        recieve_who.push(' --- ');
        $("#recieve_who :selected").each(function() {
          recieve_who.push( $(this).val() );
        });
        var self = this;
		$("#message").addClass("ui-state-info").html("Saving notification ...<img src='../images/loaderA32.gif' >");
		$.post("../controllers/summarynotificationcontroller.php?action=save", {
			recieve_email : $("#recieve_email :selected").val(),
			recieve_when  : $("#recieve_when :selected").val(),
			recieve_day	  : $("#recieve_day :selected").val(),
			recieve_what  : $("#recieve_what :selected").val(),
            recieve_who   : recieve_who
 		}, function(response) {
 			if(response.error) 
 			{		 				
				jsDisplayResult("error", "error", response.text);
			} else {
				self._getNotifications();
				jsDisplayResult("ok", "ok", response.text);
			}	
			$("#notification-dialog").dialog("destroy").remove();
 		}, "json");	
    } ,
    
    _updateNotification     : function(notification)
    {
        var self = this;
        var html = [];
        html.push("<table width='100%' class=form>");
          html.push("<tr>")
            html.push("<th>Receive?:</th>");
            html.push("<td>");
              html.push("<select name='recieve_email' id='recieve_email'>");
                html.push("<option value='1' "+(notification.recieve == 1 ? "selected='selected'" : "")+">Yes</option>");
                html.push("<option value='0' "+(notification.recieve == 0 ? "selected='selected'" : "")+">No</option>");
              html.push("</select>");
            html.push("</td>");
          html.push("</tr>");
          html.push("<tr>")
            html.push("<th>When?:</th>");
            html.push("<td>");
              html.push("<select name='recieve_when' id='recieve_when'>");
                html.push("<option value='weekly' "+(notification.when == 'weekly' ? "selected='selected'" : "")+">Weekly</option>");
                html.push("<option value='daily' "+(notification.when == 'daily' ? "selected='selected'" : "")+">Daily</option>");
              html.push("</select>");
              html.push("<span class='on_weekly' "+(notification.when == 'daily' ? "style='display:none'" : "")+"'>");
                html.push("&nbsp;&nbsp;on&nbsp;&nbsp;");
                html.push("<select name='recieve_day' id='recieve_day'>");
                  html.push("<option value='1' "+(notification.day == 1 ? "selected='selected'" : "")+">Monday</option>");
                  html.push("<option value='2' "+(notification.day == 2 ? "selected='selected'" : "")+">Tuesday</option>");
                  html.push("<option value='3' "+(notification.day == 3 ? "selected='selected'" : "")+">Wednesday</option>");
                  html.push("<option value='4' "+(notification.day == 4 ? "selected='selected'" : "")+">Thursday</option>");
                  html.push("<option value='5' "+(notification.day == 5 ? "selected='selected'" : "")+">Friday</option>");
                html.push("</select>");
              html.push("</span>");
            html.push("</td>");
          html.push("</tr>");
          html.push("<tr>")
            html.push("<th>What?:</th>");
            html.push("<td>");
              html.push("<select name='recieve_what' id='recieve_what'>");
                html.push("<option value='all_incomplete_actions' "+(notification.what == 'all_incomplete_actions' ? "selected='selected'" : "")+">All Incomplete actions</option>");
                html.push("<option value='due_this_week' "+(notification.what == 'due_this_week' ? "selected='selected'" : "")+">Actions due this week</option>");
                html.push("<option value='overdue_or_due_this_week' "+(notification.what == 'overdue_or_due_this_week' ? "selected='selected'" : "")+">Actions overdue or due this week</option>");
                html.push("<option value='due_today' "+(notification.what == 'due_today' ? "selected='selected'" : "")+">Actions due today</option>");
                html.push("<option value='overdue_or_due_today' "+(notification.what == 'overdue_or_due_today' ? "selected='selected'" : "")+">Actions overdue or due today</option>");
              html.push("</select>");
            html.push("</td>");
          html.push("</tr>"); 
          html.push("<tr>")
            html.push("<th>Recipient(s):</th>");
            html.push("<td>");
              html.push("<select name='recieve_who' id='recieve_who' multiple size=10>");
              html.push("</select>");
            html.push("</td>");
          html.push("</tr>"); 
          html.push("<tr><td colspan='2'><p id='message'></p></td></tr>");
        html.push("</table>");
		//for(x in notification.who) {
			//$("#recieve_who option[value='"+notification.who[x]+"'").prop("selected", "selected");
		//}
        if($("#edit-notification-dialog").length > 0)
        {
          $("#edit-notification-dialog").remove();
        }
        $("<div />",{id:"edit-notification-dialog"}).append(html.join(' '))
          .dialog({
                autoOpen     : true,
                modal        : true,
                position     : "top",
                width        : "auto",
                title        : "Update Summary Notifications",
                buttons      : {
                                 "Update"   : function()
                                 {
                                    self._saveNotificationChanges(notification.id);
                                 } ,
                                 
                                 "Cancel"            : function()
                                 {
                                    $("#edit-notification-dialog").dialog("destroy").remove();
                                 }
                } ,
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn   = btns[0];
                    var cancelBtn = btns[1];          

                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});      
                    
                    $("#recieve_when").live("change", function(){
                        $(".on_weekly").toggle();                    
                    });
                    $.get("../controllers/usercontroller.php?action=getAll", function(userData) {
                        var user_html = []
                        $.each( userData, function( index, user ){
                            if((notification.who).indexOf(index) > -1)
                            {
                                user_html.push("<option value='"+index+"' selected='selected'>"+user+"</option>");
                            } else {
                                user_html.push("<option value='"+index+"'>"+user+"</option>");
                            }
                        });
                        $("#recieve_who").append( user_html.join(' ') );
                    },"json");
                } ,
                close       : function(event, ui)
                {
                   $("#edit-notification-dialog").dialog("destroy").remove();
                }
          })
    } ,
    
    _saveNotificationChanges        : function(id)
    {
        var self = this;
        var recieve_who = [];
        recieve_who.push(' --- ');
        $("#recieve_who :selected").each(function() {
            recieve_who.push( $(this).val() );
        });
		$("#message").addClass("ui-state-info").html("updating notification ...<img src='../images/loaderA32.gif' >");
		$.post("../controllers/summarynotificationcontroller.php?action=update", {
			recieve_email : $("#recieve_email :selected").val(),
			recieve_when  : $("#recieve_when :selected").val(),
			recieve_day	  : $("#recieve_day :selected").val(),
			recieve_what  : $("#recieve_what :selected").val(),
			recieve_who   : recieve_who,
			id            : id
 		}, function(response) {
 			if(response.error) 
 			{		 				
				jsDisplayResult("error", "error", response.text);
			} else {
			   if(response.updated)
			   {
                 self._getNotifications();
                 jsDisplayResult("ok", "ok", response.text);			   
			   } else {
			     jsDisplayResult("info", "info", response.text);
			   }
			}	
			$("#edit-notification-dialog").dialog("destroy").remove();
 		}, "json");	 
    } ,
    
    _deleteNotification        : function(id)
    {
        var self = this;
		jsDisplayResult("info", "info", "deleting summary notification ...<img src='../images/loaderA32.gif' >");
		$.post("../controllers/summarynotificationcontroller.php?action=deleteSummaryNotification", {
			status : 0,
			id     : id
 		}, function(response) {
 			if(response.error) 
 			{		 				
				jsDisplayResult("error", "error", response.text);
			} else {
			   if(response.updated)
			   {
                 self._getNotifications();
                 jsDisplayResult("ok", "ok", response.text);			   
			   } else {
			     jsDisplayResult("info", "info", response.text);
			   }
			}	
 		}, "json");	 
    }
    
     
    
});
