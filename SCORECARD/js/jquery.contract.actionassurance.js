$.widget("contract.actionassurance",{

    options     : {
        tableId		: "action_assurance_"+(Math.floor(Math.random(78)*89)),
        action_id	: 0,
        start		: 0,
        limit		: 10,
        current		: 1,
        total		: 1
    } ,

    _init       : function()
    {
        this._getActionAssurances();
    },

    _create     : function()
    {
        var self = this;
        var html = [];
        html.push("<table width='100%' id='"+self.options.tableId+"'>");
            html.push("<tr>");
                html.push("<td colspan='8'><h4>Action Assurances</h4></td>");
            html.push("</tr>");
        html.push("</table>");

        $(self.element).append(html.join(' '));
    } ,

    _getActionAssurances  : function()
    {
        var self = this;
        $("body").append($("<div />",{id:"contractLoadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
        );
        $.getJSON("../controllers/actionassurancecontroller.php?action=getActionAssurances", {
            action_id   : self.options.action_id,
            start       : self.options.start,
            limit       : self.options.limit
        }, function(actionAssurances) {
            $("#contractLoadingDiv").remove();
            $(".action_assurance").remove();
            self._displayPager(actionAssurances.total);
            self._headers();
            var html = [];
            html.push("<tr class='action_assurance'>");
                html.push("<td colspan='8'><input type='button' name='add' id='add' value='Add New' /></td>");
            html.push("</tr>");

            $("#add").live("click", function(e) {
                self._addActionAssurance();
                e.preventDefault();
            });

            if(!$.isEmptyObject(actionAssurances.assurances))
            {
                self._display(actionAssurances.assurances);
            } else {
                html.push("<tr class='action_assurance'>");
                    html.push("<td colspan='8'>There are no Action Assurances</td>");
                html.push("</tr>");
            }
            $("#"+self.options.tableId).append(html.join(' '));
        });
    },

    _display            : function(actionAssurances)
    {
        var self = this;
        var html = [];
        $.each(actionAssurances, function(index, assurance){
            html.push("<tr id='assurance_"+assurance.id+"' class='action_assurance'>");
                html.push("<td>"+assurance.id+"</td>");
                html.push("<td>"+assurance.date_time+"</td>");
                html.push("<td>"+assurance.date_tested+"</td>");
                html.push("<td>"+assurance.response+"</td>");
                html.push("<td>"+assurance.assurance_status+"</td>");
                html.push("<td>"+assurance.assurance_provider+"</td>");
                html.push("<td>"+assurance.attachments+"</td>");
                html.push("<td>");
                    html.push("<input type='button' name='edit_"+assurance.id+"' id='edit_"+assurance.id+"' value='Edit' />");
                    html.push("<input type='button' name='del_"+assurance.id+"' id='del_"+assurance.id+"' value='Delete' />")
                html.push("</td>");
            html.push("</tr>");
            $("#edit_"+assurance.id).live("click", function(e){
                self._editAssurance(assurance);
                e.preventDefault();
            });

            $("#del_"+assurance.id).live("click", function(e){
                if(confirm("Are you sure you want to delete this action assurance"))
                {
                    self._deleteAssurance(assurance);
                }
                e.preventDefault();
            });
        });
        $("#"+self.options.tableId).append(html.join(' '));
    } ,

    _headers            : function()
    {
        var self = this;
        var html = [];
        html.push("<tr class='action_assurance'>");
            html.push("<th>Ref</th>");
            html.push("<th>System Date and Time</th>");
            html.push("<th>Date Tested</th>");
            html.push("<th>Response</th>");
            html.push("<th>Status</th>");
            html.push("<th>Assurance Provider</th>");
            html.push("<th>Attachment</th>");
            html.push("<th>&nbsp;</th>");
        html.push("<tr>");
        $("#"+self.options.tableId).append(html.join(' '));
    } ,

    _displayPager       : function()
    {

    } ,

    _addActionAssurance      : function()
    {
        var self = this;
        var html = [];
        html.push("<form id='action_assurance_form' name='action_assurance_form'>");
            html.push("<table width='100%'>");
                html.push("<tr>");
                html.push("<th>Response:</th>");
                    html.push("<td>");
                        html.push("<textarea cols='50' rows='7' name='response' id='response'></textarea>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Action On:</th>");
                    html.push("<td>");
                        html.push("<input type='text' name='date_tested' id='date_tested' value='' readonly='readonly' class='datepicker' />");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                html.push("<th>Status:</th>");
                    html.push("<td>");
                        html.push("<select name='sign_off_id' id='sign_off_id'></select>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Attachment:</th>");
                    html.push("<td>");
                        html.push("<input type='file' name='action_assurance_attachment' id='action_assurance_attachment' class='action_upload' />");
                            html.push("<p class='ui-state ui-state-info'><small>You can attach more than 1 file.</small></p>");
                        html.push("<p id='file_upload'></span>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<td colspan='2'><span style='padding:5px' id='msg'></span></td>");
                html.push("</tr>");
            html.push("</table>");
        html.push("</form>");
        if($("#action_assurance_dialog").length > 0)
        {
            $("#action_assurance_dialog").remove();
        }

        $("<div />",{id:"action_assurance_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Action Assurance",
                width       : 'auto',
                buttons     : {
                    "Save"  : function()
                    {
                        $("#msg").addClass("ui-state-info").html("saving . . . <img  src='../images/loaderA32.gif' />");
                        if($("#response").val() == "")
                        {
                            $("#msg").addClass("ui-state-error").html("Please enter the response");
                        } else if($("#signoff").val() == "") {
                            $("#msg").addClass("ui-state-error").html("Please select the status");
                        } else {
                            $.post("../controllers/actionassurancecontroller.php?action=saveActionAssurance",{
                                assurance_data : $("#action_assurance_form").serialize(),
                                action_id      : self.options.action_id
                            },function(response) {
                                if(response.error)
                                {
                                    jsDisplayResult("error", "error", response.text);
                                } else {
                                    jsDisplayResult("ok", "ok", response.text);
                                    setTimeout(function(){
                                        self._getActionAssurances();
                                        $("#action_assurance_dialog").dialog("destroy").remove();
                                    }, 1000)
                                }
                            },"json");
                        }
                    } ,

                    "Cancel"    : function()
                    {
                        $("#action_assurance_dialog").dialog("destroy").remove();
                    }
                } ,
                open        : function(event, ui)
                {
                    $(".datepicker").datepicker({
                        showOn: 'both',
                        buttonImage: '/library/jquery/css/calendar.gif',
                        buttonImageOnly: true,
                        dateFormat: 'dd-M-yy',
                        changeMonth:true,
                        changeYear:true
                    });
                } ,
                close       : function(event, ui)
                {
                    $("#action_assurance_dialog").dialog("destroy").remove();
                }
            });
        self._loadActionAssuranceStatuses();
        $(".action_upload").live("change", function(){
            if($(this).val() !== "")
            {
                self._uploadAttachment(this.id);
            }
        });

    },

    _editAssurance      : function(assurance)
    {
        var self = this;
        var html = [];
        html.push("<form id='edit_action_assurance_form' name='edit_action_assurance_form'>");
            html.push("<table width='100%'>");
                html.push("<tr>");
                    html.push("<th>Response:</th>");
                    html.push("<td>");
                        html.push("<textarea cols='50' rows='7' name='response' id='response'>"+assurance.response+"</textarea>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Action On:</th>");
                    html.push("<td>");
                        html.push("<input type='text' name='date_tested' id='date_tested' value='"+assurance.date_tested+"' class='datepicker' />");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Status:</th>");
                    html.push("<td>");
                        html.push("<select name='sign_off_id' id='sign_off_id'></select>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Attachment:</th>");
                    html.push("<td>");
                        html.push("<input type='file' name='action_assurance_attachment' id='action_assurance_attachment' class='action_upload' />");
                        html.push("<p class='ui-state ui-state-info'><small>You can attach more than 1 file.</small></p>");
                        html.push("<p>"+assurance.editable_attachments+"</p>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<td colspan='2'><p style='padding:5px' id='msg'></p></td>");
                html.push("</tr>");
            html.push("</table>");
        html.push("</form>");
        if($("#edit_action_assurance_dialog").length > 0)
        {
            $("#edit_action_assurance_dialog").remove();
        }

        $("<div />",{id:"edit_action_assurance_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Edit Action Assurrance",
                width       : 'auto',
                buttons     : {
                    "Save Changes"  : function()
                    {
                        $("#msg").addClass("ui-state-info").html("updating . . . <img  src='../images/loaderA32.gif' />");
                        if($("#response").val() == "")
                        {
                            $("#msg").addClass("ui-state-error").html("Please enter the response");
                        } else {
                            $.post("../controllers/actionassurancecontroller.php?action=updateActionAssurance",{
                                assurance_data : $("#edit_action_assurance_form").serialize(),
                                id              : assurance.id
                            },function(response) {
                                if(response.error)
                                {
                                    jsDisplayResult("error", "error", response.text);
                                } else {
                                    if(response.updated)
                                    {
                                        jsDisplayResult("ok", "ok", response.text);
                                        self._getActionAssurances();
                                    } else {
                                        jsDisplayResult("info", "info", response.text);
                                    }
                                }
                                $("#edit_action_assurance_dialog").dialog("destroy").remove();
                            },"json");
                        }
                    } ,

                    "Cancel"    : function()
                    {
                        $("#edit_action_assurance_dialog").dialog("destroy").remove();
                    }
                } ,
                open        : function(event, ui)
                {
                    $(".datepicker").datepicker({
                        showOn: 'both',
                        buttonImage: '/library/jquery/css/calendar.gif',
                        buttonImageOnly: true,
                        dateFormat: 'dd-M-yy',
                        changeMonth:true,
                        changeYear:true
                    });
                } ,
                close       : function(event, ui)
                {
                    $("#edit_action_assurance_dialog").dialog("destroy").remove();
                }
            });
        self._loadActionAssuranceStatuses(assurance.signoff);
        $(".action_upload").live("change", function(){
            if($(this).val() !== "")
            {
                self._uploadAttachment(this.id);
            }
        });

        $(".remove_attach").live("click", function(e){
            var id       = this.id
            var ext      = $(this).attr('title');
            var type     = $(this).attr('ref');
            var filename = $(this).attr("file");
            var document_id = $(this).attr("file");
            $.post('../controllers/actionassurancecontroller.php?action=deleteAttachment',
                {
                    attachment  : id,
                    ext         : ext,
                    type        : type,
                    filename    : filename,
                    document_id : document_id,
                    id          : assurance.id
                }, function(response){
                    if(response.error)
                    {
                        $("#result_message").css({padding:"5px", clear:"both"}).html(response.text);
                    } else {
                        $("#result_message").addClass('ui-state-ok').css({padding:"5px", clear:"both"}).html(response.text);
                        $("#_li_"+ext).fadeOut();
                        var deleted_doc = "<input type='hidden' name='deleted_assurance_document["+document_id+"]' id='deleted_assurance_document_"+document_id+"' value='"+document_id+"' />";
                        $("#result_message").append(deleted_doc);
                    }
                },'json');
            e.preventDefault();
        });
    } ,

    _loadActionAssuranceStatuses      : function(id)
    {
        var self = this;
        $.getJSON("../controllers/assurancestatuscontroller.php?action=getAll", function(actionStatuses){
            var html = [];
            $("#sign_off_id").html('');
            html.push("<option value=''>--please select--</option>");
            $.each(actionStatuses, function(index, status){
                if(id == status.id)
                {
                    html.push("<option value='"+status.id+"' selected='selected'>"+status.client_terminology+"</option>");
                } else {
                    html.push("<option value='"+status.id+"'>"+status.client_terminology+"</option>");
                }
            });
            $("#sign_off_id").append(html.join(' '));
        });
    } ,

    _deleteAssurance        : function(assurance)
    {
        $.post("../controllers/actionassurancecontroller.php?action=updateActionAssurance",
            {
                status   : 2,
                id       : assurance.id
            },function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text);
                        $("#assurance_"+assurance.id).fadeOut();
                        self._getActionAssurances();
                    } else {
                        jsDisplayResult("info", "info", response.text);
                    }
                }
            },"json");
    },

    _uploadAttachment       : function(element_id)
    {
        var self = this;
        $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
        $.ajaxFileUpload
        (
            {
                url			  : '../controllers/actionassurancecontroller.php?action=saveAttachment',
                secureuri	  : false,
                fileElementId : 'action_assurance_attachment',
                dataType	  : 'json',
                success       : function (response, status)
                {
                    $("#file_upload").html("");
                    if(response.error )
                    {
                        $("#file_upload").html(response.text )
                    } else {
                        $("#file_upload").html("");
                        if(response.error)
                        {
                            $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                        } else {
                            if($("#result_message"))
                            {
                                $("#result_message").remove();
                            }
                            $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                            ).append($("<div />",{id:"files_uploaded"}))
                            if(!$.isEmptyObject(response.files))
                            {
                                var list = [];
                                list.push("<span>Files uploaded ..</span><br />");
                                $.each(response.files, function(ref, file) {
                                    list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                                        list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                        list.push("<span style='margin-left:5px;'><a href='../controller/contractcontroller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                        list.push("<input type='hidden' name='action_assurance_document["+file.key+"]' value='"+file.name+"' id='action_assurance_document_"+ref+"' />");
                                    list.push("</p>");
                                });
                                $("#files_uploaded").html(list.join(' '));

                                $(".delete").click(function(e){
                                    var id = this.id
                                    var ext = $(this).attr('title');
                                    $.post('../controllers/actionassurancecontroller.php?action=removeActionAssuranceAttachment',
                                    {
                                        attachment : id,
                                        ext        : ext
                                    }, function(response) {
                                        if(response.error)
                                        {
                                            $("#result_message").html(response.text)
                                        } else {
                                            $("#result_message").addClass('ui-state-ok').html(response.text)
                                            $("#li_"+id).fadeOut();
                                        }
                                    },'json');
                                    e.preventDefault();
                                });
                                $("#"+element_id).val("");
                            }
                        }



                        if( $("#delmessage").length != 0)
                        {
                            $("#delmessage").remove()
                        }
                        $("#fileloading").append($("<div />",{id:"upmessage"}).addClass("ui-state-ok").css({"padding":"0.7em"})
                            .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"float":"left", "margin-right":"0.3em"}))
                            .append($("<span />",{html:data.file+" "+data.text}))
                        )
                        $("#fileloading").append($("<ul />",{id:"attachment_list"}))
                        if(!$.isEmptyObject(data.files))
                        {
                            var list = [];
                            $.each(response.files, function(ref, file){
                                list.push("<p id='li_"+ref+"' style='padding:2px;' class='ui-state'>");
                                list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                list.push("<span style='margin-left:5px;'><a href='../class/request.php?action=main.php?controller=action&action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                list.push("</p>");
                                //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                            });
                            $("#attachment_list").html(list.join(' '));

                            $("#"+element_id).val("");
                        }
                    }
                },
                error: function (data, status, e)
                {
                    $("#file_upload").html( "Ajax error -- "+e+", please try uploading again");
                }
            }
        )
    }


})
