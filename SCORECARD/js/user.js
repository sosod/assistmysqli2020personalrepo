$(function(){

    User.get();

    $("#add").click(function(){

        if($("#user_id").val() == '')
        {
            jsDisplayResult("error", "error", "Please select the user to setup");
        } else {
            User.save( $("#user_id").val() );
        }
    });

    $("#edit_user").click(function(){
        User.update( );
    });

    $("#configure_financial_year").click(function(){
        var user_id = $("#user_id").val();
        User.configureUserEvaluationYear(user_id);
        return false;
    });

    $("#notification_rules").click(function(e){
        document.location.href = "notification_rule.php";
        return false;
    });

});



var User 	= {

    get				: function()
    {
        $(".users").remove();
        $("#user_id").empty()
        $("#user_id").append('<option value="">Loading....</option>');
        $.getJSON( "../controllers/usercontroller.php?action=getUserSetup", function(data) {
            var userHtml = [];
            if(data.available)
            {
                //$("#user_id").append('<option value="">Loading....</option>');
                userHtml.push("<option value=''>--please select--</option>");
                $.each(data.available, function(a_index, a_user){
                    userHtml.push("<option value='"+a_user.tkid+"'>"+a_user.tkname+" "+a_user.tksurname+"</option>");
                });
                $("#user_id").html( userHtml );
            }

            if($.isEmptyObject(data.setup))
            {
                $("#user_table").append($("<tr />").addClass('users')
                    .append($("<td />", {colspan: 6, html: "There are no users setup" }))
                )
            } else {
                $.each(data.setup, function( index, val){
                    User.display( val );
                });
            }
        })
    } ,

    display			: function( val )
    {
        var self = this;
        $("#user_table")
            .append($("<tr />",{id:"tr_"+val.id}).addClass('users')
                .append($("<td />",{html:val.id}))
                .append($("<td />",{html:val.user}))
                .append($("<td />",{html:((val.status & 2048) == 2048 ? 'Yes' : 'No')  }))
                .append($("<td />",{html:((val.status & 4) == 4 ? 'Yes' : 'No')}))
                .append($("<td />",{html:((val.status & 8) == 8 ? 'Yes' : 'No')}))
                .append($("<td />",{html:((val.status & 16) == 16 ? 'Yes' : 'No')}))
                .append($("<td />",{html:((val.status & 32) == 32 ? 'Yes' : 'No')}))
                .append($("<td />",{html:((val.status & 64) == 64 ? 'Yes' : 'No')}))
                .append($("<td />",{html:((val.status & 128) == 128 ? 'Yes' : 'No')}))
                .append($("<td />",{html:((val.status & 512) == 512 ? 'Yes' : 'No')}))
                .append($("<td />",{html:((val.status & 256) == 256 ? 'Yes' : 'No')}))
                .append($("<td />")
                    .append($("<input />",{type:"submit", value:"Edit", id:"edit_user_"+val.id, name:"edit_user_"+val.id }))
                )
            )

        $("#edit_user_"+val.id).live("click", function(e){
            document.location.href = "edit_user.php?id="+val.id;
            e.preventDefault();
        });

    },

    save          : function(user_id)
    {
        var self = this;
        jsDisplayResult("info", "info", 'Saving user settings . . .');
        $.getJSON('../controllers/usercontroller.php?action=save',{
            status  : $("#user_form").serialize()
        }, function(response){
            if(response.error)
            {
                jsDisplayResult("error", "error", response.text);
            } else {
                jsDisplayResult("ok", "ok", response.text);
                User.get();
                $('select').each(function(){
                    $(this).val('');
                });
            }
        });
    },

    update              : function(id)
    {
        var self = this;
        jsDisplayResult("info", "info", 'Updating user settings . . .');
        $.post( "../controllers/usercontroller.php?action=update",
        {
            data  : $("#edit-user-form").serialize()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
            }
        },"json");

    },
    configureUserEvaluationYear    : function(user, hasEvaluationYear)
    {
        var self = this;
        if($("#configure_evaluation_year").length > 0)
        {
            $("#configure_evaluation_year").remove();
        }

        $("<div />",{id:"configure_evaluation_year"})
            .append($("<form />",{id:"configure_evaluation_year"})
                .append($("<table />",{id:"table_evaluation_year", width:"100%"}))
            )
            .dialog({
                autoOpen  : true,
                modal     : true,
                position  : "top",
                width     : "auto",
                title     : "Setup Evaluation Year",
                buttons   : {
                    "Save Changes"  : function()
                    {
                        var checkedVal = $("input[name='set_default_year']:checked").val();
                        if( checkedVal == undefined)
                        {
                            $("#message").html("Please select the default evaluation year").addClass("ui-state-error").css({'padding':'5px'});
                            return false;
                        } else {
                            $("#message").addClass('ui-state-info').html("updating user settings . . . <img src='../images/loaderA32.gif' /> " );
                            $.post("../controllers/usercontroller.php?action=setDefaultFinancialYear",{
                                default_year  : $("input[name='set_default_year']:checked").val(),
                                user_id       : user
                            }, function(response) {
                                if(response.error )
                                {
                                    $("#message").html(response.text).addClass("ui-state-error").css({'padding':'5px'});
                                } else {
                                    if(response.updated)
                                    {
                                        jsDisplayResult("info", "info", response.text );
                                    } else {
                                        jsDisplayResult("ok", "ok", response.text );
                                    }
                                    setTimeout(function(){
                                        document.location.reload();
                                    }, 700);
                                    $("#configure_evaluation_year").dialog("destroy").remove();
                                }
                            },"json");
                        }
                    },
                    "Cancel"        : function()
                    {
                        $("#configure_evaluation_year").dialog("destroy").remove();
                    }
                },
                open     : function(event, ui)
                {
                    var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                    var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
                    var saveButton = buttons[0];
                    $(saveButton).css({"color":"#090"});
                    var defaultYear = $("#default_year_id").val()
                    $.post("../controllers/financialyearcontroller.php?action=getAll", {status:1}, function(responseData){
                        var yearsHtml  = [];
                        yearsHtml.push("<tr>");
                          yearsHtml.push("<th>Year</th>");
                          yearsHtml.push("<th>Start Date</th>");
                          yearsHtml.push("<th>End Date</th>");
                          yearsHtml.push("<th>Choose Default Financial Year</th>");
                        yearsHtml.push("<tr>");
                        if($.isEmptyObject(responseData))
                        {
                            yearsHtml.push("<tr>");
                              yearsHtml.push("<td colspan='4'>There are no financial years set yet</td>");
                            yearsHtml.push("<tr>");
                        } else {
                            $.each(responseData, function( index, financialYear) {
                                yearsHtml.push("<tr>");
                                  yearsHtml.push("<td>"+financialYear.value+"</td>");
                                  yearsHtml.push("<td>"+financialYear.start_date+"</td>");
                                  yearsHtml.push("<td>"+financialYear.end_date+"</td>");
                                  yearsHtml.push("<td>");
                                    if(financialYear.id == defaultYear)
                                    {
                                      yearsHtml.push("<input type='radio' name='set_default_year' value='"+financialYear.id+"' id='set_"+financialYear.id+"' checked='checked' />");
                                    } else {
                                      yearsHtml.push("<input type='radio' name='set_default_year' value='"+financialYear.id+"' id='set_"+financialYear.id+"' />");
                                    }
                                  yearsHtml.push("</td>");
                                yearsHtml.push("<tr>");
                            });
                        }
                        yearsHtml.push("<tr>");
                         yearsHtml.push("<td colspan='4'><span id='message'></span></td>");
                        yearsHtml.push("<tr>");
                        $("#table_evaluation_year").append( yearsHtml.join('') )
                        $("#table_evaluationyear tr:even").css({"background-color":"#F7F7F7"});
                    },"json");
                },
                close    : function()
                {
                    $("#configure_evaluation_year").dialog("destroy").remove();
                }
            });
    }

}