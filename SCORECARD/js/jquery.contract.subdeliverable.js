$.widget("ui.subdeliverable", {

    options		: {
        tableId			   : "deliverable_table_"+(Math.floor(Math.random(56) * 33)),
        url 			    : "../controllers/deliverablecontroller.php?action=getDeliverables",
        total 		 	    : 0,
        start			    : 0,
        current			    : 1,
        limit			    : 10,
        editDeliverable	    : false,
        updateDeliverable   : false,
        addAction           : false,
        editAction          : false,
        updateAction        : false,
        authorizeContract   : false,
        assessContract      : false,
        assureContract      : false,
        addDeliverable      :  false,
        viewContract        : false,
        view			    : "",
        section			    : "",
        page                : "",
        autoLoad            : true,
        showActions         : false,
        financial_year      : 0,
        deliverable_owner   : 0,
        contract_id         : 0,
        deliverable_id      : 0,
        parent_id           : 0
    } ,

    _init          : function()
    {
        if(this.options.autoLoad)
        {
            this._getSubDeliverables();
        }

    } ,

    _create       : function()
    {
        var self = this;
        var html = [];
        html.push("<table class='noborder' width='100%'>");
        if(self.options.section == "admin" || self.options.section == "manage")
        {
            if(self.options.page != "view")
            {
                html.push("<tr>");
                    html.push("<td class='noborder'>");
                    html.push("<table class='noborder' width='100%'>");
                        html.push("<tr>");
                        html.push("<th style='text-align:left;'>Financial Year:</th>");
                        html.push("<td class='noborder'>");
                            html.push("<select id='financial_year' name='financial_year' style='width:200px;'>");
                            html.push("<option value=''>--please select--</option>");
                            html.push("</select>");
                        html.push("</td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th style='text-align:left;'>Contract:</th>");
                                html.push("<td class='noborder'>");
                                    html.push("<select id='contract_id' name='contract_id' style='width:200px;'>");
                                    html.push("<option value=''>--please select--</option>");
                                    html.push("</select>");
                                html.push("</td>");
                            html.push("</tr>");
                            html.push("<tr>");
                            html.push("<th style='text-align:left;'>Main Deliverable:</th>");
                            html.push("<td class='noborder'>");
                                html.push("<select id='parent_id' name='parent_id' style='width:200px;'>");
                                html.push("<option value=''>--please select--</option>");
                                html.push("</select>");
                            html.push("</td>");
                        html.push("</tr>");
                    html.push("</table>");
                html.push("</td>");
                html.push("</tr>");
            }
        }
        html.push("<tr>");
        html.push("<td class='noborder'>");
        html.push("<table id='"+self.options.tableId+"' width='100%'></table>");
        html.push("</td>");
        html.push("</tr>");
        html.push("</table>");


        $(this.element).append(html.join(' '));
        self._loadFinancialYears();
        self._loadContracts();
        self._loadMainDeliverables();

        $("#financial_year").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.financial_year = $(this).val();
                self._getSubDeliverables();
            } else {
                self.options.financial_year  = 0;
                self._getSubDeliverables();
            }
        });

        $("#contract_id").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.contract_id = $(this).val();
                self._getSubDeliverables();
            } else {
                self.options.contract_id = 0;
                self._getSubDeliverables();
            }
        });

        $("#parent_id").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.parent_id = $(this).val();
                self._getSubDeliverables();
            } else {
                self.options.parent_id = 0;
                self._getSubDeliverables();
            }
        });
    } ,

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#financial_year").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
            });
            $("#financial_year").html( finHtml.join(' ') );
        });
    } ,

    _loadContracts              : function()
    {
        var self = this;
        $("#contract_id").empty();
        $.getJSON("../controllers/contractcontroller.php?action=getAll",{
            financial_year_id  : self.options.financial_year
        }, function(contracts){
            var contractHtml = [];
            contractHtml.push("<option value=''>--please select--</option>");
            $.each(contracts, function(contract_id, contract) {
                contractHtml.push("<option value='"+contract_id+"'>"+contract+"</option>");
            })
            $("#contract_id").html( contractHtml.join(' ') );
        });
    },

    _loadMainDeliverables              : function()
    {
        var self = this;
        $("#parent_id").empty();
        $.getJSON("../controllers/deliverablecontroller.php?action=getAll", {
            financial_year_id : self.options.financial_year,
            contract_id       : self.options.contract_id,
            parent_id         : self.options.parent_id
        } ,function(deliverables){
            var deliverableHtml = [];
            deliverableHtml.push("<option value=''>--please select--</option>");
            $.each(deliverables ,function(deliverable_id, deliverable) {
                deliverableHtml.push("<option value='"+deliverable_id+"'>"+deliverable+"</option>");
            })
            $("#parent_id").html( deliverableHtml.join(' ') );
        });
    },

    _getSubDeliverables      : function()
    {
        var self = this;
        $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
        );
        $.getJSON( self.options.url,{
            start		         : self.options.start,
            limit		         : self.options.limit,
            view		         : self.options.view,
            section		         : self.options.section,
            page                 : self.options.page,
            financial_year       : self.options.financial_year,
            deliverable_owner    : self.options.deliverable_owner,
            contract_id          : self.options.contract_id,
            parent_id            : self.options.parent_id,
            is_main_deliverable  : false
        },function(deliverableData) {
            $("#loadingDiv").remove();
            $("#"+self.options.tableId).html("");
            $(".more_less").remove();
            if(deliverableData.total != null)
            {
                self.options.total = deliverableData.total
            }
            //self._displayPaging(deliverableData.total_columns);
            //self._displayHeaders(deliverableData.columns);
            if($.isEmptyObject(deliverableData.deliverables))
            {
                var _html = [];
                _html.push("<tr>")
                _html.push("<td colspan='"+(deliverableData.total_columns+1)+"'>");
                _html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                if(deliverableData.hasOwnProperty('deliverable_message') && deliverableData.deliverable_message != '')
                {
                    _html.push("<span style='padding:5px; float:left;'>"+deliverableData.deliverbale_message+"</span>");
                } else {
                    _html.push("<span style='padding:5px; float:left;'>There are no deliverables found</span>");
                }
                _html.push("</p>");
                _html.push("</td>");
                _html.push("</tr>");
                $("#"+self.options.tableId).append( _html.join(' ') );
            } else {
                self._display(deliverableData.deliverables, deliverableData.actions, deliverableData.total_columns, deliverableData);
            }
        });
    } ,

    _display           : function(deliverables, actions, columns, deliverableData)
    {
        if($(".sub_deliverable_more_less").length > 0)
        {
            $(".sub_deliverable_more_less").remove();
        }
        if($(".sub_deliverable_actions").length > 0)
        {
            $(".sub_deliverable_actions").remove();
        }
        var self = this;
        var i    = 1;
        $.each(deliverables, function(index, deliverable){
            i++;
            var html = [];
            if(self.options.page == "view" && deliverableData.hasOwnProperty('canUpdate'))
            {
                if(deliverableData.canUpdate.hasOwnProperty(index) && deliverableData.canUpdate[index] == true)
                {
                    html.push("<tr>");
                } else {
                    html.push("<tr class='view-notowner'>");
                }
            } else {
                html.push("<tr>");
            }
            if(self.options.showActions)
            {
                html.push("<td>");
                    html.push("<a href='#' id='sub_deliverable_more_less_"+self.options.tableId+"_"+index+"' class='sub_deliverable_more_less'>");
                        html.push("<span class='ui-icon ui-icon-plus'></span>")
                    html.push("</a>");
                html.push("</td>");

            }
            $.each( deliverable, function( key , val){
                html.push("<td>"+val+"</td>");
            })
            if(self.options.updateDeliverable)
            {
                html.push("<td>&nbsp;");
                if(deliverableData.hasOwnProperty('canUpdate'))
                {
                    if(deliverableData.canUpdate.hasOwnProperty(index))
                    {
                        if(deliverableData.canUpdate[index] == true || self.options.section == "admin")
                        {
                            html.push("<input type='button' value='Update deliverable' id='update_"+index+"' id='update_"+index+"' />");
                            $("#update_"+index).live("click", function(){
                                document.location.href = "update_deliverable.php?id="+index;
                                return false;
                            });
                        }
                    }
                }
                html.push("</td>");
            }
            if(self.options.editDeliverable)
            {
                html.push("<td>&nbsp;");
                if(deliverableData.hasOwnProperty('canEdit'))
                {
                    if(deliverableData.canEdit.hasOwnProperty(index))
                    {
                        if(deliverableData.canEdit[index] == true)
                        {
                            html.push("<input type='button' value='Edit deliverable' id='edit_"+index+"' id='edit_"+index+"' />");
                            $("#edit_"+index).live("click", function(){
                                document.location.href = "edit_deliverable.php?id="+index;
                                return false;
                            });
                            //html.push("<input type='button' value='Edit Actions' id='edit_actions_"+index+"' id='edit_actions_"+index+"' />");
                        }
                    }
                }
                html.push("</td>");

                $("#edit_deliverable_"+index).live("click", function(){
                    document.location.href = "edit_deliverable.php?id="+index;
                    return false;
                });
            }
            if(self.options.addAction)
            {
                html.push("<td>");
                if(deliverableData.hasOwnProperty('canAddAction'))
                {
                    if(deliverableData.canAddAction.hasOwnProperty(index))
                    {
                        if(deliverableData.canAddAction[index] == true)
                        {
                            html.push("<input type='button' value='Add Action' id='add_action_"+index+"' id='add_action_"+index+"' />");
                            $("#add_action_"+index).live("click", function(){
                                document.location.href = "add_action.php?id="+index;
                                return false;
                            });

                        }
                    }
                }
                html.push("</td>");
            }
            if(self.options.viewDeliverable)
            {
                html.push("<td>&nbsp;");
                html.push("<input type='button' value='View deliverable Details' id='view_deliverable_"+index+"' id='view_deliverable_"+index+"' />");
                html.push("</td>");
                $("#view_deliverable_"+index).live("click", function(){
                    document.location.href = "view_deliverable.php?id="+index;
                    return false;
                });
            }
            html.push("</tr>");
            if(self.options.showActions)
            {
                if(!$.isEmptyObject(deliverables))
                {
                    if(deliverables.hasOwnProperty(index))
                    {
                        html.push("<tr id='sub_deliverable_actions_"+self.options.tableId+"_"+index+"' class='sub_deliverable_actions' style='display:none;'>");
                        html.push("<td id='show_sub_deliverable_action_"+self.options.tableId+"_"+index+"' colspan='"+(columns+2)+"'></td>");
                        html.push("</tr>");
                    }
                }
            }
            $("#"+self.options.tableId).append( html.join(' ') ).find('.remove_attach').remove();
            $("#"+self.options.tableId).find("#result_message").remove();

            $("#sub_deliverable_more_less_"+self.options.tableId+"_"+index).bind("click", function(e){
                if( $("#sub_deliverable_actions_"+self.options.tableId+"_"+index).is(":hidden") )
                {
                    $("#sub_deliverable_more_less_"+self.options.tableId+"_"+index+" span").removeClass("ui-icon-closethick").addClass("ui-icon-minus");
                } else {
                    $("#sub_deliverable_more_less_"+self.options.tableId+"_"+index+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");
                }
                $("#sub_deliverable_actions_"+self.options.tableId+"_"+index).toggle();
                e.preventDefault();
            });

            if(self.options.showActions)
            {
                if(!$.isEmptyObject(deliverables))
                {
                    if(deliverables.hasOwnProperty(index))
                    {
                        $("#show_sub_deliverable_action_"+self.options.tableId+"_"+index).action({deliverable_id:index, section:self.options.section, showActions:self.options.showActions, page:self.options.page});
                    }
                }
            }
        });
    },

    _displayHeaders     : function(headers)
    {

        var self = this;
        var html = [];
        html.push("<tr>");
        if(self.options.showActions)
        {
            html.push("<th>&nbsp;</th>");
        }
        $.each( headers , function( index, head ){
            html.push("<th>"+head+"</th>");
        });
        if(self.options.updateDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.editDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.addAction)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.viewDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }

        html.push("</tr>");
        $("#"+self.options.tableId).append(html.join(' '));
    },

    _displayPaging	: function(columns)
    {
        var self  = this;
        var html  = [];
        var pages;
        if( self.options.total%self.options.limit > 0){
            pages   = Math.ceil(self.options.total/self.options.limit);
        } else {
            pages   = Math.floor(self.options.total/self.options.limit);
        }
        var contract_id = self.options.contract_id+"_"+self.options.tableId;

        html.push("<tr>");
        if(self.options.showActions)
        {
            html.push("<td>&nbsp;</td>");
        }
        html.push("<td colspan='"+(columns + 1)+"' class='noborder'>");
            html.push("<input type='button' value=' |< ' id='sub_deliverable_first_"+contract_id+"' name='first' />");
            html.push("<input type='button' value=' < ' id='sub_deliverable_previous_"+contract_id+"' name='previous' />");
        if(pages != 0)
        {
            html.push("Page <select id='sub_deliverable_select_page' name='sub_deliverable_select_page'>");
            for(var p=1; p<=pages; p++)
            {
                html.push("<option value='"+p+"' "+(self.options.current == p ? "selected='selected'" : "")+">"+p+"</option>");
            }
            html.push("</select>");
        } else {
            html.push("Page <select id='sub_deliverable_select_page' name='sub_deliverable_select_page' disabled='disabled'>");
            html.push("</select>");
        }
        html.push(" of "+(pages == 0 || isNaN(pages) ? 0 : pages)+" pages");
        html.push("<input type='button' value=' > '  id='sub_deliverable_next_"+contract_id+"' name='next' />");
        html.push("<input type='button' value=' >| ' id='sub_deliverable_last_"+contract_id+"' name='last' />");
        if(self.options.assuranceActions)
        {
            html.push("&nbsp;");
        }
        if(self.options.updateDeliverable)
        {
            html.push("&nbsp;");
        }
        if(self.options.editDeliverable)
        {
            html.push("&nbsp;");
        }
        if(self.options.addAction)
        {
            html.push("&nbsp;");
        }
        if(self.options.viewDeliverable)
        {
            html.push("&nbsp;");
        }
                html.push("<span style='float:right;'>");
                html.push("<b>Quick Search for Deliverable:</b> D <input type='text' name='s_d_id' id='s_d_id' value='' />");
                html.push("&nbsp;&nbsp;&nbsp;");
                html.push("<input type='button' name='sub_deliverable_search' id='sub_deliverable_search' value='Go' />");
                html.push("</span>");
            html.push("</td>");
        html.push("</tr>");
        $("#"+self.options.tableId).append(html.join(' '));


        $("#sub_deliverable_select_page").live("change", function(){
            if($(this).val() !== "" && parseFloat($(this).val())>0)
            {
                self.options.start   = parseFloat($(this).val() -1) * parseFloat(self.options.limit);
                self.options.current = parseFloat($(this).val());
                self._getSubDeliverables();
            }
        });
        $("#sub_deliverable_search").live("click", function(e){
            var deliverable_id = $("#s_d_id").val();
            if(deliverable_id != "")
            {
                self.options.sub_deliverable_id = $("#s_d_id").val();
                self._getSubDeliverables();
            } else {
                $("#s_d_id").addClass('ui-state-error');
                $("#s_d_id").focus().select();
            }
            e.preventDefault();
        });

        if(self.options.current < 2)
        {
            $("#sub_deliverable_first_"+contract_id).attr('disabled', 'disabled');
            $("#sub_deliverable_previous_"+contract_id).attr('disabled', 'disabled');
        }
        if((self.options.current == pages || pages == 0))
        {
            $("#sub_deliverable_next_"+contract_id).attr('disabled', 'disabled');
            $("#sub_deliverable_last_"+contract_id).attr('disabled', 'disabled');
        }
        $("#sub_deliverable_next_"+contract_id).bind("click", function(evt){
            self._getNext( self );
        });
        $("#sub_deliverable_last_"+contract_id).bind("click",  function(evt){
            self._getLast( self );
        });
        $("#sub_deliverable_previous_"+contract_id).bind("click",  function(evt){
            self._getPrevious( self );
        });
        $("#sub_deliverable_first_"+contract_id).bind("click",  function(evt){
            self._getFirst( self );
        });
    } ,

    _getNext  			: function(self)
    {
        self.options.current = (self.options.current + 1);
        self.options.start   = (self.options.current-1) * self.options.limit;
        this._getSubDeliverables();
    },

    _getLast  			: function( self )
    {
        self.options.current =  Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
        self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );
        this._getSubDeliverables();
    },
    _getPrevious    	: function( self )
    {
        self.options.current  = parseFloat(self.options.current) - 1;
        self.options.start 	= (self.options.current-1) * self.options.limit;
        this._getSubDeliverables();
    },
    _getFirst  			: function( self )
    {
        self.options.current  = 1;
        self.options.start 	= 0;
        this._getSubDeliverables();
    }


})