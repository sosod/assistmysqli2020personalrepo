$.widget("ui.contract", {

    options		: {
        tableId			  : "contract_table_"+(Math.floor(Math.random(56) * 33)),
        url 			  : "../controllers/contractcontroller.php?action=getContracts",
        total 		 	  : 0,
        start			  : 0,
        current			  : 1,
        limit			  : 10,
        editContract	  : false,
        updateContract    : false,
        authorizeContract : false,
        contractAssessment: false,
        contractAssurance : false,
        assureContract    : false,
        addDeliverable    : false,
        editDeliverable	  : false,
        updateDeliverable : false,
        viewContract      : false,
        viewAction        : false,
        addAction         : false,
        updateBudget      : false,
        updatePayment     : false,
        confirmation      : false,
        makeTemplate      : false,
        undoAuthorizeContract : false,
        view			  : "",
        section			  : "",
        page              : "",
        autoLoad          : true,
        showActions       : false,
        financial_year    : 0,
        contract_owner    : 0,
        contract_id       : 0,
        actionFilter      : true,
        deliverableFilter : true
    } ,

    _init          : function()
    {
        if(this.options.autoLoad)
        {
            this._getContracts();
        }

    } ,

    _create       : function()
     {
         var self = this;
         var html = [];
         html.push("<table width='100%' class='noborder'>");
             html.push("<tr>");
                 html.push("<td class='noborder'>");
                     html.push("<table class='noborder'>");
                         html.push("<tr>");
                             html.push("<th style='text-align:left;'>Financial Year:</th>");
                             html.push("<td class='noborder'>");
                                 html.push("<select id='_financial_year' name='_financial_year' style='width:200px;'>");
                                 html.push("<option value=''>--please select--</option>");
                                 html.push("</select>");
                             html.push("</td>")
                         html.push("</tr>");
                     html.push("</table>");
                 html.push("</td>");
             html.push("</tr>");
             html.push("<tr>");
                 html.push("<td class='noborder'>");
                    html.push("<table id='"+self.options.tableId+"' width='100%' class='noborder'>");
                      html.push("<tr>");
                        html.push("<td>");
                          html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                            html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                            html.push("<span style='padding:5px; float:left;'>Please select the financial year</span>");
                          html.push("</p>");
                        html.push("</td>");
                      html.push("</tr>");
                    html.push("</table>");
                 html.push("</td>");
             html.push("</tr>");
         html.push("</table>");


         $(this.element).append(html.join(' '));
         self._loadFinancialYears();
         self._getContracts();
         //self._loadContractOwners();

         $("#_financial_year").live("change", function(){
             if($(this).val() !== "")
             {
                 self.options.financial_year = $(this).val();
                 self._getContracts();
             } else {
                 self.options.financial_year  = 0;
                 self._getContracts();
             }
         });

         $("#contract_owner").live("change", function(){
             if($(this).val() !== "")
             {
                 self.options.contract_owner = $(this).val();
                 self._getContracts();
             } else {
                 self.options.contract_owner = 0;
                 self._getContracts();
             }
         });

     } ,

    _loadContracts     : function()
    {
        var self = this;
        $("#confirmation_contract_list").html("");
        $.getJSON("../controllers/contractcontroller.php?action=getAll",{ status : 'new' }, function(contracts){
            var contractHtml = []
            contractHtml.push("<option value=''>--please select--</option>");
            $.each(contracts, function(contract_id, contract){
                contractHtml.push("<option value='"+contract_id+"'>"+contract+"</option>");
            });
            $("#confirmation_contract_list").html( contractHtml.join(' ') );
        });
    } ,

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#_financial_year").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
            });
            $("#_financial_year").html( finHtml.join(' ') );
        });
    } ,

    _loadContractOwners              : function()
    {
        var self = this;
        $("#contract_owner").empty();
        $.getJSON("../controllers/contractownercontroller.php?action=getAll", function(ownerData){
            var ownerHtml = [];
            ownerHtml.push("<option value=''>--please select--</option>");
            $.each(ownerData ,function(index, directorate) {
                ownerHtml.push("<option value='"+directorate.subid+"'>"+directorate.dirtxt+"</option>");
            })
            $("#contract_owner").html( ownerHtml.join(' ') );
        });
    },

     _getContracts      : function()
     {
         var self = this;
         $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
             .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
         );
         $.getJSON( self.options.url,{
             start		       : self.options.start,
             limit		       : self.options.limit,
             view		       : self.options.view,
             section		   : self.options.section,
             page              : self.options.page,
             financial_year    : self.options.financial_year,
             contract_owner    : self.options.contract_owner,
             contract_id       : self.options.contract_id
         },function(contractData) {
             $("#loadingDiv").remove();
             $("#"+self.options.tableId).html("");
             $(".more_less").remove();
             $(".confirm_contract").remove();
             $(".contract_message").remove();
             if(contractData.financial_year != 0 || contractData.financial_year != '')
             {
                 $("#_financial_year").val(contractData.financial_year);
                 self.options.financial_year = contractData.financial_year;
             }
             if(contractData.total != null)
             {
                 self.options.total = contractData.total
             }
             self._displayPaging(contractData.total_columns);
             self._displayHeaders(contractData.columns);
             if($.isEmptyObject(contractData.contracts))
             {
                 var _html = [];
                 _html.push("<tr>")
                     _html.push("<td colspan='"+(contractData.total_columns+1)+"'>");
                     _html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                         _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                         if(contractData.hasOwnProperty('contract_message') && contractData.contract_message != '')
                         {
                             _html.push("<span style='padding:5px; float:left;'>"+contractData.contract_message+"</span>");
                         } else {
                             if(self.options.page == 'confirmation' && self.options.contract_id == '')
                             {
                                 _html.push("<span style='padding:5px; float:left;'>Please select the contract to confirm</span>");
                             } else {
                                 _html.push("<span style='padding:5px; float:left;'>There are no contract found</span>");
                             }
                         }
                     _html.push("</p>");
                     _html.push("</td>");
                 _html.push("</tr>");
                 $("#"+self.options.tableId).append( _html.join(' ') );
             } else {
                 self._display(contractData.contracts, contractData.deliverables, contractData.total_columns, contractData);
             }
         });
     } ,

     _display           : function(contracts, deliverables, columns, contractData)
     {
         if($(".contract_more_less").length > 0)
         {
             $(".contract_more_less").remove();
         }
         if($(".contract_deliverables").length > 0)
         {
             $(".contract_deliverables").remove();
         }
         var self = this;
         var i    = 1;
         $.each(contracts, function(index, contract){
             i++;
             var html = [];
             if(self.options.page == "view" && contractData.hasOwnProperty('canUpdate'))
             {
                 if(contractData.canUpdate.hasOwnProperty(index) && contractData.canUpdate[index] == true)
                 {
                     html.push("<tr>");
                 } else {
                     html.push("<tr class='view-notowner'>");
                 }
             } else {
                 html.push("<tr>");
             }
             if(self.options.showActions)
             {
                 html.push("<td>");
                     html.push("<a href='#' id='contract_more_less_"+self.options.tableId+"_"+index+"' class='contract_more_less'>");
                         html.push("<span class='ui-icon ui-icon-plus'></span>")
                     html.push("</a>");
                 html.push("</td>");

             }
             $.each( contract, function( key , val){
                 if(key == 'contract_progress' || key == 'contract_budget' || key == 'contract_payment')
                 {
                     html.push("<td style='text-align: right;'>"+val+"</td>");
                 } else {
                     html.push("<td>"+val+"</td>");
                 }
             })
             html.push(self._displayUpdateContractBtn(contractData, index));
             html.push(self._displayEditContractBtn(contractData, index));
             html.push(self._displayAddDeliverableBtn(contractData, index));
             html.push(self._displayAddActionBtn(contractData, index));
             html.push(self._displayUpdateBudgetBtn(contractData, index));
             html.push(self._displayUpdatePaymentBtn(contractData, index));
             html.push(self._displayContractAssuranceBtn(index));
             html.push(self._displayContractAssessmentBtn(index));
             html.push(self._displayContractAuthorizationBtn(index));
             html.push(self._displayContractViewBtn(index));
             html.push(self._displayMakeTemplateBtn(contractData, index));
             html.push(self._displayAuthorizationBtn(contractData, index));
             html.push(self._displayUndoAuthorizationBtn(contractData, index));

             html.push("</tr>");
             if(self.options.showActions)
             {
                 if(!$.isEmptyObject(deliverables))
                 {
                     if(deliverables.hasOwnProperty(index))
                     {
                         html.push("<tr id='contract_deliverable_"+self.options.tableId+"_"+index+"' class='contract_deliverables' style='display:none;'>");
                             html.push("<td id='show_deliverable_"+self.options.tableId+"_"+index+"' colspan='"+(parseInt(columns)+2)+"'></td>");
                         html.push("</tr>");
                     }
                 }
             }


             $("#"+self.options.tableId).append( html.join(' ') ).find('.remove_attach').remove();
             $("#"+self.options.tableId).find("#result_message").remove();

             $("#contract_more_less_"+self.options.tableId+"_"+index).bind("click", function(e){
                 if( $("#contract_deliverable_"+self.options.tableId+"_"+index).is(":hidden") )
                 {
                     $("#contract_more_less_"+self.options.tableId+"_"+index+" span").removeClass("ui-icon-plus").addClass("ui-icon-minus");
                 } else {
                     $("#contract_more_less_"+self.options.tableId+"_"+index+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");
                 }
                 $("#contract_deliverable_"+self.options.tableId+"_"+index).toggle();
                 e.preventDefault();
             });

             if(self.options.showActions)
             {
                 if(!$.isEmptyObject(deliverables))
                 {
                     if(deliverables.hasOwnProperty(index))
                     {
                         $("#show_deliverable_"+self.options.tableId+"_"+index).deliverable({contract_id:index, section:self.options.section, viewDeliverable:self.options.viewDeliverable, viewAction:self.options.viewAction, page:self.options.page, showActions:self.options.showActions, financial_year:self.options.financial_year, autoLoad: true, actionFilter: self.options.actionFilter, allowFilter: self.options.deliverableFilter });
                     }
                 }
             }
         });

     },

    _displayMakeTemplateBtn: function(contractData, contract_id)
    {
        var self = this;
        var html = [];
        if(self.options.makeTemplate)
        {
            html.push("<td>&nbsp;");
            html.push("<input type='button' value='Templates' id='contract_templates_"+contract_id+"' name='contract_templates_"+contract_id+"' />");
            $("#contract_templates_"+contract_id).live("click", function(){
                document.location.href = "contract_templates.php?id="+contract_id;
                return false;
            });
            html.push("</td>");
            return html.join('');
        }
        return '';
    },

    _displayAuthorizationBtn: function(contractData, contract_id)
    {
        var self = this;
        var html = [];
        if(self.options.authorizeContract)
        {
            html.push("<td>&nbsp;");
              html.push("<input type='button' value='Authorise' id='authorize_contract_"+contract_id+"' name='authorize_contract"+contract_id+"' />");
              $("#authorize_contract_"+contract_id).live("click", function(){
                 //document.location.href = "authorize_contract.php?id="+contract_id;
                  self._authorizeContract(contract_id);
                 return false;
              });
            html.push("</td>");
            return html.join('');
        }
        return '';
    },

    _displayUndoAuthorizationBtn: function(contractData, contract_id)
    {
        var self = this;
        var html = [];
        if(self.options.undoAuthorizeContract)
        {
            html.push("<td>&nbsp;");
            html.push("<input type='button' value='Undo Authorise' id='undo_authorize_contract_"+contract_id+"' name='undo_authorize_contract_"+contract_id+"' />");
            $("#undo_authorize_contract_"+contract_id).live("click", function(){
                //document.location.href = "authorize_contract.php?id="+contract_id;
                self._undoAuthorizeContract(contract_id);
                return false;
            });
            html.push("</td>");
            return html.join('');
        }
        return '';
    },

    _authorizeContract:        function(contract_id)
    {
      var self = this;
        var self = this;
        var html = [];
        html.push("<form id='contract_authorization_form' name='contract_authorization_form'>");
            html.push("<table width='100%'>");
                html.push("<tr>");
                    html.push("<th>Response:</th>");
                    html.push("<td>");
                    html.push("<textarea cols='50' rows='7' name='response' id='response'></textarea>");
                    html.push("</td>");
                html.push("</tr>");
/*                html.push("<tr>");
                  html.push("<th>Authorisation Status:</th>");
                  html.push("<td>");
                    html.push("<select name='authorisation_status_id' id='authorisation_status_id'></select>");
                  html.push("</td>");
                html.push("</tr>");*/
                html.push("<tr>");
                    html.push("<td colspan='2'><span style='padding:5px' id='msg'></span></td>");
                html.push("</tr>");
            html.push("</table>");
        html.push("</form>");
        if($("#contract_authorization_dialog_"+contract_id).length > 0)
        {
            $("#contract_authorization_dialog_"+contract_ids).remove();
        }

        $("<div />",{id:"contract_authorization_dialog_"+contract_id}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Contract Authorisation C"+contract_id,
                width       : 'auto',
                buttons     : {
                    "Save"  : function()
                    {
                        $("#msg").addClass("ui-state-info").html("Saving . . . <img  src='../images/loaderA32.gif' />");
                        if($("#response").val() == "")
                        {
                            $("#msg").addClass("ui-state-error").html("Please enter the response");
                        } else {
                            $.post("../controllers/contractcontroller.php?action=saveContractAuthorization",{
                                response                : $("#response").val(),
                                authorisation_status_id : $("#authorisation_status_id").val(),
                                id                      : contract_id
                            },function(response) {
                                if(response.error)
                                {
                                    $("#msg").addClass("ui-state-error").html(response.text);
                                } else {
                                    jsDisplayResult("ok", "ok", response.text);
                                    setTimeout(function(){
                                        self._getContracts();
                                        $("#contract_authorization_dialog_"+contract_id).dialog("destroy").remove();
                                    }, 1000)
                                }
                            },"json");
                        }
                    } ,

                    "Cancel"    : function()
                    {
                        $("#contract_authorization_dialog_"+contract_id).dialog("destroy").remove();
                    }
                } ,
                open        : function(event, ui)
                {

                } ,
                close       : function(event, ui)
                {
                    $("#contract_authorization_dialog_"+contract_id).dialog("destroy").remove();
                }
            });
        self._loadActionAuthorizationStatuses();
    },

    _loadActionAuthorizationStatuses      : function(id)
    {
        var self = this;
        $.getJSON("../controllers/authorizationstatuscontroller.php?action=getAll", function(authorisationStatuses){
            var html = [];
            $("#authorisation_status_id").html('');
            html.push("<option value=''>--please select--</option>");
            $.each(authorisationStatuses, function(index, status){
                if(id == status.id)
                {
                    html.push("<option value='"+status.id+"' selected='selected'>"+status.client_terminology+"</option>");
                } else {
                    html.push("<option value='"+status.id+"'>"+status.client_terminology+"</option>");
                }
            });
            $("#authorisation_status_id").append(html.join(' '));
        });
    } ,
        _undoAuthorizeContract:        function(contract_id)
        {
            var self = this;
            var html = [];
            html.push("<form id='contract_undo_authorization_form' name='contract_undo_authorization_form'>");
                html.push("<table width='100%'>");
                    html.push("<tr>");
                        html.push("<th>Response:</th>");
                        html.push("<td>");
                            html.push("<textarea cols='50' rows='7' name='response' id='response'></textarea>");
                        html.push("</td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<td colspan='2'><span style='padding:5px' id='msg'></span></td>");
                    html.push("</tr>");
                html.push("</table>");
            html.push("</form>");
            if($("#contract_undo_authorization_dialog_"+contract_id).length > 0)
            {
                $("#contract_undo_authorization_dialog_"+contract_ids).remove();
            }

            $("<div />",{id:"contract_undo_authorization_dialog_"+contract_id}).append(html.join(' '))
                .dialog({
                    autoOpen    : true,
                    modal       : true,
                    position    : "top",
                    title       : "Contract Undo Authorization C"+contract_id,
                    width       : 'auto',
                    buttons     : {
                        "Save"  : function()
                        {
                            $("#msg").addClass("ui-state-info").html("Updating . . . <img  src='../images/loaderA32.gif' />");
                            if($("#response").val() == "")
                            {
                                $("#msg").addClass("ui-state-error").html("Please enter the response");
                            } else {
                                $.post("../controllers/contractcontroller.php?action=saveUndoContractAuthorization",{
                                    response : $("#response").val(),
                                    id       : contract_id
                                },function(response) {
                                    if(response.error)
                                    {
                                        $("#msg").addClass("ui-state-error").html(response.text);
                                    } else {
                                        jsDisplayResult("ok", "ok", response.text);
                                        setTimeout(function(){
                                            self._getContracts();
                                            $("#contract_undo_authorization_dialog_"+contract_id).dialog("destroy").remove();
                                        }, 1000)
                                    }
                                },"json");
                            }
                        } ,

                        "Cancel"    : function()
                        {
                            $("#contract_undo_authorization_dialog_"+contract_id).dialog("destroy").remove();
                        }
                    } ,
                    open        : function(event, ui)
                    {

                    } ,
                    close       : function(event, ui)
                    {
                        $("#contract_undo_authorization_dialog_"+contract_id).dialog("destroy").remove();
                    }
                });
        },

    _displayUpdateContractBtn: function(contractData, contract_id)
    {
        var self = this;
        var html = [];
        if(self.options.updateContract)
        {
            html.push("<td>&nbsp;");
            if(contractData.hasOwnProperty('canUpdate'))
            {
                if(contractData.canUpdate.hasOwnProperty(contract_id))
                {
                    if(contractData.canUpdate[contract_id] == true || self.options.section == "admin")
                    {
                        html.push("<input type='button' value='Update contract' id='update_"+contract_id+"' name='update_"+contract_id+"' />");
                        $("#update_"+contract_id).live("click", function(){
                            document.location.href = "update_contract.php?id="+contract_id;
                            return false;
                        });
                    }
                }
            }
            //html.push("<input type='button' value='Update Actions' id='update_actions_"+index+"' id='update_actions_"+index+"' />");
            html.push("</td>");
            return html.join('');
        }
        return '';
    },


    _displayEditContractBtn: function(contractData, contract_id)
    {
        var self = this;
        var html = [];
        if(self.options.editContract)
        {
            html.push("<td>&nbsp;");
            if(contractData.hasOwnProperty('canEdit'))
            {
                if(contractData.canEdit.hasOwnProperty(contract_id))
                {
                    if(contractData.canEdit[contract_id] == true)
                    {
                        html.push("<input type='button' value='Edit Contract' id='edit_"+contract_id+"' name='edit_"+contract_id+"' />");
                        $("#edit_"+contract_id).live("click", function(){
                            document.location.href = "edit_contract.php?id="+contract_id;
                            return false;
                        });
                    }
                }
            }

            if(contractData.hasOwnProperty('canAddDeliverable'))
            {
                if(contractData.canAddDeliverable.hasOwnProperty(contract_id))
                {
                    if(contractData.canAddDeliverable[contract_id] == true)
                    {
                        /*                             html.push("<input type='button' value='Add Deliverable' id='add_deliverable"+index+"' id='add_deliverable"+index+"' />");
                         $("#add_deliverable_"+index).on("click", function(){
                         document.location.href = "add_deliverable.php?id="+index;
                         return false;
                         });

                         html.push("<input type='button' value='Edit Actions' id='edit_actions_"+index+"' id='edit_actions_"+index+"' />");
                         $("#edit_actions_"+index).on("click", function(){
                         document.location.href = "edit_deliverable.php?id="+index;
                         return false;
                         });*/
                    }
                }
            }
            if(self.options.section == "new")
            {
                 html.push("<input type='button' value='Delete' id='delete_contract_"+contract_id+"' id='delete_contract_"+contract_id+"' class='idelete' />");
                 $("#delete_contract_"+contract_id).live("click", function(){
                    if(confirm('Are you sure you want to delete this contract'))
                    {
                        jsDisplayResult("info", "info", "Deleting deliverable . . . <img  src='../images/loaderA32.gif' />");
                        $.getJSON("../controllers/contractcontroller.php?action=deleteContract", {
                           contract_id : contract_id
                        }, function(response){
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                if(response.updated)
                                {
                                  jsDisplayResult("ok", "ok", response.text);
                                  self._getContracts();
                                } else {
                                  jsDisplayResult("info", "info", response.text);
                                }
                            }
                        });
                    }
                    return false;
                 });
            }
            html.push("</td>");
            return html.join(' ');
        }
        return '';
    },

    _displayAddDeliverableBtn: function(contractData, contract_id)
    {
        var self = this;
        var html = [];
        if(self.options.addDeliverable)
        {
            html.push("<td>&nbsp;");
            if(contractData.hasOwnProperty('canAddDeliverable'))
            {
                if(contractData.canAddDeliverable.hasOwnProperty(contract_id))
                {
                    if(contractData.canAddDeliverable[contract_id] == true)
                    {
                        html.push("<input type='button' value='Add Deliverable' id='add_deliverable_"+contract_id+"' name='add_deliverable_"+contract_id+"' />");
                        $("#add_deliverable_"+contract_id).live("click", function(){
                            document.location.href = "add_deliverable.php?id="+contract_id;
                            return false;
                        });
                    }
                }
            }
            html.push("</td>");
            return html.join(' ');
        }
        return '';
    },

    _displayUpdateBudgetBtn: function(contractData, contract_id)
    {
        var self = this;
        var html = [];
        if(self.options.updateBudget)
        {
            if(contractData.hasOwnProperty('canBudget'))
            {
                if(contractData.canBudget.hasOwnProperty(contract_id))
                {
                    if(contractData.canBudget[contract_id] == true)
                    {
                        html.push("<td>&nbsp;");
                        html.push("<input type='button' value='Update Budget' id='update_budget_"+contract_id+"' name='update_budget_"+contract_id+"' />");
                        $("#update_budget_"+contract_id).live("click", function(){
                            document.location.href = "update_contract_budget.php?id="+contract_id;
                            return false;
                        });
                        html.push("</td>");
                    }
                }
            }
            return html.join(' ');
        }
        return '';
    },

    _displayAddActionBtn  : function(contractData, contract_id)
    {
        var self = this;
        var html = [];
        if(self.options.addAction)
        {
            if(contractData.hasOwnProperty('canAddAction'))
            {
                if(contractData.canAddAction.hasOwnProperty(contract_id))
                {
                    if(contractData.canAddAction[contract_id] == true)
                    {
                        html.push("<td>&nbsp;");
                            html.push("<input type='button' value='Add Action' id='add_contract_action_"+contract_id+"' name='add_contract_action_"+contract_id+"' />");
                            $("#add_contract_action_"+contract_id).live("click", function(){
                                document.location.href = "add_contract_action.php?id="+contract_id;
                                return false;
                            });
                        html.push("</td>");
                    }
                }
            }
            return html.join(' ');
        }
        return '';
    },

    _displayUpdatePaymentBtn : function(contractData, contract_id)
    {
        var self = this;
        var html = [];
        if(self.options.updatePayment)
        {
            if(contractData.hasOwnProperty('canPayment'))
            {
                if(contractData.canPayment.hasOwnProperty(contract_id))
                {
                    if(contractData.canPayment[contract_id] == true)
                    {
                        html.push("<td>&nbsp;");
                            html.push("<input type='button' value='Update Payment' id='update_payment_"+contract_id+"' name='update_payment_"+contract_id+"' />");
                        $("#update_payment_"+contract_id).live("click", function(){
                            document.location.href = "update_contract_payment.php?id="+contract_id;
                            return false;
                        });
                        html.push("</td>");
                    }
                }
            }
            return html.join(' ')
        }
        return '';
    },

    _displayContractViewBtn : function(contract_id)
    {
        var self = this;
        var html = [];
        if(self.options.viewContract)
        {
            html.push("<td>&nbsp;");
                html.push("<input type='button' value='View Contract Details' id='view_contract_"+contract_id+"' name='view_contract_"+contract_id+"' />");
            html.push("</td>");
            $("#view_contract_"+contract_id).live("click", function(){
                document.location.href = "view_contract.php?id="+contract_id;
                return false;
            });
            return html.join('');
        }
        return '';
    },

        _displayContractAssuranceBtn : function(contract_id)
        {
            var self = this;
            var html = [];
            if(self.options.contractAssurance)
            {
                html.push("<td>&nbsp;");
                html.push("<input type='button' value='Assurance' id='contract_assurance_"+contract_id+"' name='contract_assurance_"+contract_id+"' />");
                $("#contract_assurance_"+contract_id).live("click", function(){
                    document.location.href = "contract_assurance.php?id="+contract_id;
                    return false;
                });
                html.push("</td>");
                return html.join(' ');
            }
            return '';
        },

      _displayContractAssessmentBtn : function(contract_id)
      {
          var self = this;
          var html = [];
          if(self.options.contractAssessment)
          {
              html.push("<td>&nbsp;");
                  html.push("<input type='button' value='Assess' id='asses_contract_"+contract_id+"' name='asses_contract_"+contract_id+"' />");
                  $("#asses_contract_"+contract_id).live("click", function(){
                      document.location.href = "contract_assessment.php?id="+contract_id;
                      return false;
                  });
              html.push("</td>");
              return html.join(' ');
          }
          return '';
      },

    _displayContractAuthorizationBtn : function(index)
    {
        var self = this;
        var html = [];
        if(self.options.contractAuthorization)
        {
            html.push("<td>&nbsp;");
            html.push("<input type='button' value='Authorization' id='authorize_contract_"+index+"' name='authorize_contract_"+index+"' />");
            $("#authorize_contract_"+index).live("click", function(){
                document.location.href = "contract_authorization.php?id="+index;
                return false;
            });
            html.push("</td>");
            return html.join(' ');
        }
        return '';
    },
     _displayHeaders     : function(headers)
     {

         var self = this;
         var html = [];
         html.push("<tr>");
         if(self.options.showActions)
         {
             html.push("<th>&nbsp;</th>");
         }
         $.each( headers , function( index, head ){
             html.push("<th>"+head+"</th>");
         });
         if(self.options.contractAuthorization)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.makeTemplate)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.contractAuthorization)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.contractAssessment)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.contractAssurance)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.updateContract)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.editContract)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.addDeliverable)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.viewContract)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.addAction)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.updateBudget)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.updatePayment)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.authorizeContract)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.undoAuthorizeContract)
         {
             html.push("<th>&nbsp;</th>");
         }


         html.push("</tr>");
         $("#"+self.options.tableId).append(html.join(' '));
     },

    _displayPaging	: function(columns)
     {
        var self  = this;
        var html  = [];
        var pages;
        if( self.options.total%self.options.limit > 0){
            pages   = Math.ceil(self.options.total/self.options.limit);
        } else {
            pages   = Math.floor(self.options.total/self.options.limit);
        }
        html.push("<tr>");
            if(self.options.showActions)
            {
                html.push("<td>&nbsp;</td>");
            }
            html.push("<td colspan='"+(columns + 1)+"'>");
            html.push("<input type='button' value=' |< ' id='first' name='first' />");
            html.push("<input type='button' value=' < ' id='previous' name='previous' />");
            if(pages != 0)
            {
                html.push("Page <select id='select_page' name='select_page'>");
                for(var p=1; p<=pages; p++)
                {
                    html.push("<option value='"+p+"' "+(self.options.current == p ? "selected='selected'" : "")+">"+p+"</option>");
                }
                html.push("</select>");
            } else {
                html.push("Page <select id='select_page' name='select_page' disabled='disabled'>");
                html.push("</select>");
            }
            html.push(" of "+(pages == 0 || isNaN(pages) ? 0 : pages)+" pages");
            //html.push(self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages));
            html.push("<input type='button' value=' > ' id='next' name='next' />");
            html.push("<input type='button' value=' >| ' id='last' name='last' />");
            if(self.options.assuranceActions)
            {
                html.push("&nbsp;");
            }
            if(self.options.updateContract)
            {
                html.push("&nbsp;");
            }
            if(self.options.editContract)
            {
                html.push("&nbsp;");
            }
            if(self.options.addAction)
            {
                html.push("&nbsp;");
            }
            if(self.options.viewContract)
            {
                html.push("&nbsp;");
            }

                html.push("<span style='float:right;'>");
                    html.push("<b>Quick Search for Contract:</b> C <input type='text' name='c_id' id='c_id' value='' />");
                    html.push("&nbsp;&nbsp;&nbsp;");
                    html.push("<input type='button' name='search' id='search' value='Go' />");
                html.push("</span>");
            html.push("</td>");
        html.push("</tr>");
        $("#"+self.options.tableId).append(html.join(' '));


        $("#select_page").live("change", function(){
            if($(this).val() !== "" && parseFloat($(this).val())>0)
            {
                self.options.start   = parseFloat($(this).val() -1) * parseFloat(self.options.limit);
                self.options.current = parseFloat($(this).val());
                self._getContracts();
            }
        });
        $("#search").live("click", function(e){
            var contract_id = $("#c_id").val();
            if(contract_id != "")
            {
                self.options.contract_id = $("#c_id").val();
                self._getContracts();
            } else {
                $("#c_id").addClass('ui-state-error');
                $("#c_id").focus().select();
            }
            e.preventDefault();
        });

        if(self.options.current < 2)
        {
            $("#first").attr('disabled', 'disabled');
            $("#previous").attr('disabled', 'disabled');
        }
        if((self.options.current == pages || pages == 0))
        {
            $("#next").attr('disabled', 'disabled');
            $("#last").attr('disabled', 'disabled');
        }
        $("#next").bind("click", function(evt){
            self._getNext( self );
        });
        $("#last").bind("click",  function(evt){
            self._getLast( self );
        });
        $("#previous").bind("click",  function(evt){
            self._getPrevious( self );
        });
        $("#first").bind("click",  function(evt){
            self._getFirst( self );
        });
    } ,

    _getNext  			: function(self)
    {
        self.options.current = (self.options.current + 1);
        self.options.start   = (self.options.current-1) * self.options.limit;
        this._getContracts();
    },

    _getLast  			: function( self )
    {
        self.options.current =  Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
        self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );
        this._getContracts();
    },
    _getPrevious    	: function( self )
    {
        self.options.current  = parseFloat(self.options.current) - 1;
        self.options.start 	= (self.options.current-1) * self.options.limit;
        this._getContracts();
    },
    _getFirst  			: function( self )
    {
        self.options.current  = 1;
        self.options.start 	= 0;
        this._getContracts();
    } ,


    _getContractDetail              : function()
    {
        var self = this;
        $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
        );
        var contract_id = self.options.contract_id;
        var _tableHtml = []
        _tableHtml.push("<tr>");
            _tableHtml.push("<td class='noborder' align='left'>");
              _tableHtml.push("<table id='contract_"+contract_id+"_detail' class='noborder'></table>");
            _tableHtml.push("</td>");
            _tableHtml.push("<td class='noborder' style='text-align: left;'>");
               _tableHtml.push("<table id='contract_"+contract_id+"_deliverable_weight_detail' width='100%' align='left'></table>");
            _tableHtml.push("</td>");
        _tableHtml.push("</tr>");
        _tableHtml.push("<tr>");
            _tableHtml.push("<td class='noborder' colspan='2' align='left'>");
               _tableHtml.push("<table id='contract_deliverable_"+contract_id+"_detail' width='100%'></table>");
            _tableHtml.push("</td>");
        _tableHtml.push("</tr>");
        $("#"+self.options.tableId).append(_tableHtml.join(' '));

        $.getJSON('../controllers/contractcontroller.php?action=getDetailContract',{
            start		      : self.options.start,
            limit		      : self.options.limit,
            view		      : self.options.view,
            section		      : self.options.section,
            page              : self.options.page,
            financial_year    : self.options.financial_year,
            contract_owner    : self.options.contract_owner,
            contract_id       : self.options.contract_id
        },function(contractData) {
            $("#loadingDiv").remove();
            $(".contract_message").remove();
            self._displayContractDetail( contractData.contract );
            self._displayCategorized( contractData );
            //$("#contract_"+contract_id+"_deliverable_weight_detail")._displayWeights( contractData.deliverables );
            //$("#contract_"+contract_id+"_deliverable_weight_detail")._displayCategorized( contractData.deliverables );
        });

    } ,

    _displayContractDetail          : function(contract)
    {

        var self = this;
        var contractHtml = [];
        contractHtml.push("<table class='noborder'>");
        $.each(contract, function(column, contract) {
            contractHtml.push("<tr>")
              contractHtml.push("<th style='text-align: left;'>"+contract.header+"</th>");
              contractHtml.push("<td>"+contract.value+"</td>");
            contractHtml.push("</tr>");
        });
        contractHtml.push("</table>");
        $("#contract_"+self.options.contract_id+"_detail").append( contractHtml.join(' ')).css({'text-align':'left'});
    },

    _displayCategorized             : function(contractData)
    {
        var self = this;
        var categoryHtml = [];
        var summaryHtml  = [];
        if(!$.isEmptyObject(contractData.category_deliverable))
        {
            self._displayDeliverableHeader(contractData.columns);
            $.each(contractData.category_deliverable,function(deliverable_category, deliverable_data){
                categoryHtml.push("<tr>");
                    categoryHtml.push("<td  class='noborder' colspan='"+(contractData.total_columns+1)+"'><b>"+deliverable_category+"</b></td>");
                categoryHtml.push("</tr>");
                categoryHtml.push("<tr>");
                    categoryHtml.push("<td  class='noborder' colspan='"+(contractData.total_columns+1)+"' id='ca'></td>");
                categoryHtml.push("</tr>");
                //$("#contract_deliverable_"+self.options.contract_id+"_detail").deliverable({editDeliverable: true, contract_id: self.options.contract_id, category_id: '', showSubDeliverable: true, showActions: true, editAction: true });
                //categoryHtml.push( self._displayDeliverables(contractData, deliverable_data, contractData.category_weights, contractData.total_actions) );
            });
            summaryHtml.push( self._displayDeliverableSummary(contractData.key_measures) );
        }
        //$("#contract_deliverable_"+self.options.contract_id+"_detail").append(categoryHtml.join(' ')).css({'text-align':'left'});
        //$("#contract_"+self.options.contract_id+"_deliverable_weight_detail").append(summaryHtml.join(' ')).css({'text-align':'left'});
    },

    _displayDeliverableHeader       : function(headers)
    {
        var self = this;
        var html = [];
        html.push("<tr>");
        html.push("<th>&nbsp;</th>");
        $.each( headers , function( index, head ){
            html.push("<th>"+head+"</th>");
        });
        html.push("<th>Number Of Actions</th>");
        if(self.options.updateDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.editDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.addAction)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.viewDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }
        html.push("</tr>");
        $("#contract_deliverable_"+self.options.contract_id+"_detail").append(html.join(' '));
    },

    _displayDeliverables            : function(contractData, deliverable_data, weights, action_data)
    {
       var self = this;
       var deliverableHtml = [];
        $.each(deliverable_data.deliverables, function(deliverable_id, deliverable){
            deliverableHtml.push("<tr>");
                deliverableHtml.push("<td>");
                    deliverableHtml.push("<a href='#' id='deliverable_more_less_"+self.options.tableId+"_"+deliverable_id+"' class='deliverable_more_less'>")
                       deliverableHtml.push("<span class='ui-icon ui-icon-plus'></span>")
                    deliverableHtml.push("</a>")
                deliverableHtml.push("</td>");
            $.each(deliverable, function(field, val){
                deliverableHtml.push("<td>"+val+"</td>");
            });
            if(action_data.hasOwnProperty(deliverable_id))
            {
                deliverableHtml.push("<td>"+action_data[deliverable_id]+"</td>");
            } else {
                deliverableHtml.push("<td>0</td>");
            }
            deliverableHtml.push(self._deliverableEditButton(contractData, deliverable_id));
            deliverableHtml.push("</tr>");
            if(deliverable_data.hasOwnProperty('sub_deliverable'))
            {
                if(deliverable_data['sub_deliverable'].hasOwnProperty(deliverable_id))
                {
                    $.each(deliverable_data['sub_deliverable'][deliverable_id], function(sub_deliverable_id, sub_deliverable){
                        deliverableHtml.push("<tr class='view-notowner'>");
                        deliverableHtml.push("<td>")
                            deliverableHtml.push("<a href='#' id='sub_deliverable_more_less_"+self.options.tableId+"_"+sub_deliverable_id+"' class='sub_deliverable_more_less'>")
                                deliverableHtml.push("<span class='ui-icon ui-icon-plus'></span>")
                            deliverableHtml.push("</a>");
                        deliverableHtml.push("</td>");
                        $.each(sub_deliverable, function(sub_field, sub_val) {
                            deliverableHtml.push("<td>"+sub_val+"</td>");
                        });
                        if(action_data.hasOwnProperty(sub_deliverable_id))
                        {
                            deliverableHtml.push("<td>"+action_data[sub_deliverable_id]+"</td>");
                        }
                        deliverableHtml.push(self._deliverableEditButton(contractData, sub_deliverable_id));
                        deliverableHtml.push("</tr>");
                    });
                }
            }
        })
/*        deliverableHtml.push("<tr>");
        $.each(weights, function(w_index, w_val){
            if(w_index == 'delivered_weight')
            {
                deliverableHtml.push("<td>"+w_val+"</td>");
            } else if(w_index == 'other_weight') {
                deliverableHtml.push("<td>"+w_val+"</td>");
            } else if(w_index == 'quality_weight')  {
                deliverableHtml.push("<td>"+w_val+"</td>");
            } else {
                deliverableHtml.push("<td></td>");
            }
        })
        deliverableHtml.push("</tr>");*/
        return deliverableHtml;
    } ,

    _displayDeliverableSummary                 : function(key_measures)
    {
        var self = this;
        var summaryHtml = []
        var total_weight = 0;
        summaryHtml.push("<tr>");
            summaryHtml.push("<th>Key Measures</th>");
            summaryHtml.push("<th>Weights</th>");
        summaryHtml.push("</tr>");
        $.each(key_measures, function(category, category_deliverable_summary){
            var total_category_weight = 0;
            summaryHtml.push("<tr>");
                summaryHtml.push("<td style='text-align: left;' colspan='2'><b>"+category+"</b></td>");
            summaryHtml.push("</tr>");
            $.each(category_deliverable_summary, function(deliverable_id, deliverable_summary){
                summaryHtml.push("<tr>");
                    summaryHtml.push("<td style='text-align: left;'>"+deliverable_summary.deliverable_name+"</td>");
                    summaryHtml.push("<td style='text-align: right;'>"+deliverable_summary.total_weight+"</td>");
                summaryHtml.push("</tr>");
                total_category_weight = parseInt(deliverable_summary.total_weight) + parseInt(total_category_weight);
            });
            total_weight          = parseInt(total_category_weight) + parseInt(total_weight);
            summaryHtml.push("<tr class='view-notowner'>");
              summaryHtml.push("<td style='text-align: left;'>Total for "+category+"</td>");
              summaryHtml.push("<td style='text-align: right;' colspan='2'><b>"+total_category_weight+"</b></td>");
            summaryHtml.push("</tr>");
        });
        summaryHtml.push("<tr>");
            summaryHtml.push("<td style='text-align: left;'>Score</td>");
            summaryHtml.push("<td style='text-align: right;' colspan='2'><b>"+total_weight+"</b></td>");
        summaryHtml.push("</tr>");
        return summaryHtml;
    },

    _updateButton        : function(deliverableData, deliverable_id)
    {
        var self = this;
        if(self.options.updateDeliverable)
        {
            html.push("<td>&nbsp;");
            if(deliverableData.hasOwnProperty('canUpdate'))
            {
                if(deliverableData.canUpdate.hasOwnProperty(deliverable_id))
                {
                    if(deliverableData.canUpdate[deliverable_id] == true || self.options.section == "admin")
                    {
                        html.push("<input type='button' value='Update deliverable' id='update_"+deliverable_id+"' name='update_"+deliverable_id+"' />");
                        $("#update_"+deliverable_id).live("click", function(){
                            document.location.href = "update_deliverable.php?id="+index;
                            return false;
                        });
                    }
                }
            }
            html.push("</td>");
            return html;
        }
        return '';
    }
})