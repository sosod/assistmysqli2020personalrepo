/**
 * Created with JetBrains PhpStorm.
 * User: admire
 * Date: 2013/09/24
 * Time: 6:15 AM
 * To change this template use File | Settings | File Templates.
 */
$(function(){


    $("#type_id").change(function(){
        var id = $(this).attr("id")
        Deliverable.loadDeliverables(id);
        return false;
    });

    $("#save").click(function(e){

        var is_main_deliverable = 0;
        var has_sub_deliverable = 0;
        if($("#is_main_deliverable").length > 0)
        {
            is_main_deliverable = 1;
            $("#is_main_deliverable").remove();
        }
        if($("#has_sub_deliverable").length > 0)
        {
            has_sub_deliverable = 1;
            $("#has_sub_deliverable").remove();
        }
         if($("#name").val() == '') {
            $("#name").focus().select()
            jsDisplayResult("error", "error", 'Heading name is required');
            return false;
        } else if($("#category_id").val() == '') {
            $("#category_id").focus().select()
            jsDisplayResult("error", "error", 'Category is required');
            return false;
        } else if($("#owner_id").val() == '' ) {
            $("#owner_id").focus().select()
            jsDisplayResult("error", "error", 'Heading owner is required');
            return false;
        } else if($("#type_id").val() == '' ) {
             $("#type_id").focus().select()
             jsDisplayResult("error", "error", 'Please select the heading type');
             return false;
         } else if($("#type_id").val() == 1 && $("#sub_parent_id").val() == ''){
             $("#sub_parent_id").focus().select()
             jsDisplayResult("error", "error", 'Please select the parent/main heading for this heading');
             return false;
         } else {
             jsDisplayResult("info", "info", "Saving deliverable . . . <img  src='../images/loaderA32.gif' />");
             $.getJSON("../controllers/deliverablecontroller.php?action=saveDeliverable",{
                name                 : $("#name").val(),
                description          : $("#description").val(),
                category_id          : $("#category_id").val(),
                type_id              : $("#type_id").val(),
                parent_id            : $("#sub_parent_id").val(),
                owner_id             : $("#owner_id").val(),
                score_card_id        : $("#score_card_id").val(),
                has_sub_deliverable  : has_sub_deliverable,
                is_main_deliverable  : is_main_deliverable,
                weight_id            : $("#weight_id :selected").val(),
             }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    jsDisplayResult("ok", "ok", response.text);
                    $("input[type='text']").each(function(){
                        $(this).val('');
                    });
                    $('select').each(function(){
                        $(this).val('');
                    });
                    $('textarea').each(function(){
                        $(this).val('');
                    });
                    $("#deliverable_list").deliverable({editDeliverable: true});
                }
             });
        }

        e.preventDefault();
    })


    $("#edit_deliverable_change").click(function(e){

        var is_main_deliverable = 0;
        var has_sub_deliverable = 0;
        if($("#is_main_deliverable").length > 0)
        {
            is_main_deliverable = 1;
            $("#is_main_deliverable").remove();
        }
        if($("#has_sub_deliverable").length > 0)
        {
            has_sub_deliverable = 1;
            $("#has_sub_deliverable").remove();
        }
        if($("#name").val() == '') {
            $("#name").focus().select()
            jsDisplayResult("error", "error", 'Heading name is required');
            return false;
        } else if($("#category_id").val() == '') {
            $("#category_id").focus().select()
            jsDisplayResult("error", "error", 'Category is required');
            return false;
        } else if($("#owner_id").val() == '' ) {
            $("#owner_id").focus().select()
            jsDisplayResult("error", "error", 'Heading owner is required');
            return false;
        } else if($("#type_id").val() == '' ) {
            $("#type_id").focus().select()
            jsDisplayResult("error", "error", 'Please select the heading type');
            return false;
        } else if($("#type_id").val() == 1 && $("#parent_id").val() == ''){
            $("#parent_id").focus().select()
            jsDisplayResult("error", "error", 'Please select the parent/main heading for this heading');
            return false;
        } else if($("#deliverable_deadline_date").val() == '') {
            $("#deliverable_deadline_date").focus().select()
            jsDisplayResult("error", "error", 'Deadline date is required');
            return false;
        } else {
            jsDisplayResult("info", "info", "Updating heading . . . <img  src='../images/loaderA32.gif' />");
            $.getJSON("../controllers/deliverablecontroller.php?action=editDeliverable",{
                name                 : $("#name").val(),
                description          : $("#description").val(),
                category_id          : $("#category_id").val(),
                type_id              : $("#type_id").val(),
                parent_id            : $("#parent_id").val(),
                owner_id             : $("#owner_id").val(),
                deadline_date        : $("#deliverable_deadline_date").val(),
                remind_on            : $("#remind_on").val(),
                score_card_id        : $("#score_card_id").val(),
                has_sub_deliverable  : has_sub_deliverable,
                is_main_deliverable  : is_main_deliverable,
                weight_id            : $("#weight_id").val(),
                id                   : $("#deliverable_id").val()
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    if(response.updated)
                    {
                       jsDisplayResult("ok", "ok", response.text);
                    } else {
                        jsDisplayResult("info", "info", response.text);
                    }
                }
            });
        }

        e.preventDefault();
    })


    $("#view_deliverable").click(function(e){
        $("<div />",{id:"action_deliverable"})
            .dialog({
                modal      : true,
                autoOpen   : true,
                title      : "Action Heading Details",
                position   : "left top",
                width      : 'auto',
                buttons    : {
                    "Ok"        : function()
                    {
                        $("#action_deliverable").remove();
                    }
                } ,
                open       : function(ui, event)
                {
                    $("#action_deliverable").html("Loading heading details . . . <img  src='../images/loaderA32.gif' />");
                    $.get("../controllers/deliverablecontroller.php?action=getDeliverableHtml",{
                        id  : $("#parent_id").val()
                    } , function(deliverableHtml){
                        $("#action_deliverable").html("");
                        $("#action_deliverable").append( deliverableHtml );
                    },"html")
                },
                close		: function() {
                    $("#action_deliverable").remove();
                }
            });
        e.preventDefault()
    });


    $("#view_score_card").click(function(e){
        $("<div />",{id:"score_card_details"})
            .dialog({
                modal      : true,
                autoOpen   : true,
                title      : "Scorecard Details",
                position   : "left top",
                width      : 'auto',
                buttons    : {
                    "Ok"        : function()
                    {
                        $("#score_card_details").remove();
                    }
                } ,
                open       : function(ui, event)
                {
                    $("#score_card_details").html("Loading scorecard details . . . <img  src='../images/loaderA32.gif' />");
                    $.get("../controllers/scorecardcontroller.php?action=getScoreCardDetailHtml",{
                        id  : $("#score_card_id").val()
                    } , function(scoreCardHtml ){
                        $("#score_card_details").html("");
                        $("#score_card_details").append( scoreCardHtml );
                    },"html")
                },
                close		: function()
                {
                    $("#score_card_details").remove();
                }
            });
        e.preventDefault()
    });

    $("#status_id").change(function(e) {
        var thisValue = $(this).val();
        if(thisValue == 5)
        {
            $("#approval").removeAttr("disabled");
        } else {
            $("#approval").attr("disabled", "disabled");
        }
        e.preventDefault();
    });

    $("#update_deliverable").click(function(e){
        if($("#response").val() == '') {
            $("#response").focus().select()
            jsDisplayResult("error", "error", 'Response message is required');
            return false;
        } else if($("#status_id").val() == '' ) {
            $("#status_id").focus().select()
            jsDisplayResult("error", "error", 'Status is required');
            return false;
        }  else {
            jsDisplayResult("info", "info", "Updating heading . . . <img  src='../images/loaderA32.gif' />");
            $.post("../controllers/deliverablecontroller.php?action=updateDeliverable", {
                deliverable_data : $("#deliverable_form").serialize()
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text);
                    } else {
                        jsDisplayResult("info", "info", response.text);
                    }
                }
            }, 'json');
        }
        e.preventDefault();
    });

    $(".remove_attach").live("click", function(e){
        var id       = this.id
        var doc_id   = $(this).parent().attr('id');
        var ext      = $(this).attr('title');
        var type     = $(this).attr('ref');
        var filename = $(this).attr("file");
        $.post('../controllers/deliverablecontroller.php?action=deleteDeliverableAttachment',
            {
                doc_id     : doc_id,
                ext        : ext,
                action_id  : $("#deliverable_id").val(),
                type       : type,
                filename   : filename
            }, function(response){
                if(response.error)
                {
                    $("#result_message").css({padding:"5px", clear:"both"}).html(response.text)
                } else {
                    $("#result_message").addClass('ui-state-ok').css({padding:"5px", clear:"both"}).html(response.text)
                    var deleted_doc = "<input type='hidden' name='deleted_deliverable_attachment["+doc_id+"]' id='deleted_attachment_"+doc_id+"' value='"+doc_id+"' />";
                    $("#result_message").append(deleted_doc);
                    $("#li_"+ext).fadeOut();
                }
            },'json');
        e.preventDefault();
    });

    $(".upload_action").live("change", function(e){
        Action.uploadAttachment(this.id);
        e.preventDefault();
    });

});

function attachDeliverableDocuments()
{
    $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
    $.ajaxFileUpload
    (
        {
            url			  : '../controllers/deliverablecontroller.php?action=saveAttachment',
            secureuri	  : false,
            fileElementId : 'deliverable_attachment',
            dataType	  : 'json',
            success       : function (response, status)
            {
                $("#file_upload").html("");
                if(response.error )
                {
                    $("#file_upload").html(response.text )
                } else {
                    $("#file_upload").html("");
                    if(response.error)
                    {
                        $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                    } else {
                        $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                        ).append($("<div />",{id:"files_uploaded"}))
                        if(!$.isEmptyObject(response.files))
                        {
                            var list = [];
                            list.push("<span>Files uploaded ..</span><br />");
                            $.each(response.files, function(ref, file) {
                                list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                                list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                list.push("<span style='margin-left:5px;'><a href='../controller/contractcontroller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                list.push("<input type='hidden' name='deliverable_document["+file.key+"]' value='"+file.name+"' id='deliverable_document_"+ref+"' />");
                                list.push("</p>");
                            });
                            $("#files_uploaded").html(list.join(' '));

                            $(".delete").click(function(e){
                                var id = this.id
                                var ext = $(this).attr('title');
                                $.post('../controllers/deliverablecontroller.php?action=removeDeliverableAttachment',
                                    {
                                        attachment : id,
                                        ext        : ext
                                    }, function(response){
                                        if(response.error)
                                        {
                                            $("#result_message").html(response.text)
                                        } else {
                                            $("#result_message").addClass('ui-state-ok').html(response.text)
                                            $("#li_"+id).fadeOut();
                                        }
                                    },'json');
                                e.preventDefault();
                            });
                            $("#deliverable_attachment").val("");
                        }
                    }

                    if( $("#delmessage").length != 0)
                    {
                        $("#delmessage").remove()
                    }
                }
            },
            error: function (data, status, e)
            {
                $("#fileupload").html( "Ajax error -- "+e+", please try uploading again");
            }
        }
    )
    return false;
}

var Deliverable =
{

        loadDeliverables        : function()
        {
            if( $("#type_id :selected").val() == 1)
            {
                $("#sub_parent_id").empty().html("<option value=''>Loading...</option>");
                $.getJSON('../controllers/deliverablecontroller.php?action=getMainDeliverableList', {
                    is_main_deliverable : 1,
                    score_card_id       : $("#score_card_id").val()
                }, function(parents){
                    $("#sub_parent_id").empty();
                    var parentHtml = [];
                    parentHtml.push("<option value=''>--please select--</option>");
                    $.each(parents, function(index, parent) {
                        parentHtml.push("<option value='"+index+"'>"+parent+"</option>");
                    });
                    $("#sub_parent_id").html( parentHtml.join(' ') );

                });
                if( $(".deliverable_weights").is(":hidden"))
                {
                    $(".deliverable_weights").show();
                }
                if($("#is_main_deliverable").length > 0)
                {
                    $("#is_main_deliverable").remove();
                }
                if($("#has_sub_deliverable").length > 0)
                {
                    $("#has_sub_deliverable").remove();
                }
            } else {
                var html = "<p>Does heading have subheading</p>";
                $("<div />").append(html)
                    .dialog({
                        autoOpen : true,
                        modal    : true,
                        with     : 'auto',
                        title    : 'Main Heading Configuration',
                        buttons  : {
                                    "Yes"    : function()
                                    {
                                        $("#deliverable-form").append("<input type='hidden' name='deliverable[has_sub_deliverable]' id='has_sub_deliverable' value='1' > ");
                                        $(".weights").hide();
                                        $(this).dialog("close")
                                    },
                                    "No"   : function()
                                    {
                                        //$("#deliverable-form").append("<input type='hidden' name='deliverable[is_main_deliverable]' id='is_main_deliverable' value='1' > ");
                                        $(".weights").show();
                                        $(this).dialog("close")
                                    }
                        } ,
                        open     : function(event, ui){
                            $("#deliverable-form").append("<input type='hidden' name='deliverable[is_main_deliverable]' id='is_main_deliverable' value='1' > ");
                        },
                        close     : function(event, ui){

                        }
                    })
                $("#sub_parent_id").empty().html("<option value=''>--please select--</option>");
            }

        }


}