$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    Supplier.get();
    $("#add_supplier").click(function(e){
        Supplier.addSupplier();
        e.preventDefault();
    });
	
});

var Supplier 	= {

		get				: function()
		{
            $(".suppliers").remove();
			$.getJSON( "../controllers/suppliercontroller.php?action=getAll", function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#supplier_table").append($("<tr />").addClass('suppliers')
                        .append($("<td />", {colspan: 6, html: "There are no suppliers" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        Supplier.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#supplier_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('suppliers')
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.short_code}))
                .append($("<td />",{html:val.supplier_code}))
				.append($("<td />",{html:val.name}))
				.append($("<td />",{html:val.supplier_category}))
                .append($("<td />",{html:"<b>"+(val.status == 1 ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"supplier_edit_"+val.id, name:"supplier_edit_"+val.id }))
				 )
			  )

            $("#supplier_edit_"+val.id).live("click", function(e){
                self._updateSupplier(val);
                e.preventDefault();
            });
			
		},

    addSupplier          : function()
    {
        var self = this;
        if($("#add_supplier_dialog").length > 0)
        {
            $("#add_supplier_dialog").remove();
        }

        $("<div />",{id:"add_supplier_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"short_code", id:"short_code", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Supplier Code :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"supplier_code", id:"supplier_code", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Supplier Name:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Supplier Category:"}))
                    .append($("<td />")
                        .append($("<select />",{name:"category_id", id:"category_id"}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Supplier",
                width          : 500,
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the supplier short code");
                            return false;
                        } else if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the supplier name");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_supplier_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_supplier_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    //load supplier categories
                    $.getJSON('../controllers/suppliercategorycontroller.php?action=getAll',{
                        status : 1
                    }, function(supplier_categories){
                        var s_html = [];
                        s_html.push("<option value=''>--please select--</option>");
                        $.each(supplier_categories, function(supplier_category_id, supplier_category){
                            s_html.push("<option value='"+supplier_category_id+"'>"+supplier_category.name+"</option>");
                        });
                        $("#category_id").html(s_html.join(' '));
                    });
                    $(saveBtn).css({"color":"#090"});
                }
            })
    },

    _updateSupplier         : function(supplier)
    {
        var self = this;
        if($("#edit_supplier_dialog").length > 0)
        {
            $("#edit_supplier_dialog").remove();
        }

        $("<div />",{id:"edit_supplier_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"short_code", id:"short_code", value:supplier.short_code}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Supplier Code :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"supplier_code", id:"supplier_code", value:supplier.supplier_code}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Supplier Name:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:supplier.name}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Supplier Category:"}))
                    .append($("<td />")
                        .append($("<select />",{name:"category_id", id:"category_id"}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Supplier",
                width          : 500,
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the supplier short code");
                            return false;
                        } else if($("#name").val() == "") {
                            $("#message").addClass("ui-state-error").html("please enter the supplier name");
                            return false;
                        } else {
                            self.update(supplier.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_supplier_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(supplier.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(supplier.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(supplier.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_supplier_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((supplier.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});

                    //load supplier categories
                    $.getJSON('../controllers/suppliercategorycontroller.php?action=getAll',{
                        status : 1
                    }, function(supplier_categories){
                        var s_html = [];
                        s_html.push("<option value=''>--please select--</option>");
                        $.each(supplier_categories, function(supplier_category_id, supplier_category){
                            if(supplier.category_id == supplier_category_id)
                            {
                                s_html.push("<option value='"+supplier_category_id+"' selected='selected'>"+supplier_category.name+"</option>");
                            } else {
                                s_html.push("<option value='"+supplier_category_id+"'>"+supplier_category.name+"</option>");
                            }
                        });
                        $("#category_id").html(s_html.join(' '));
                    });
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/suppliercontroller.php?action=update",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_supplier_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/suppliercontroller.php?action=update",
        {
            id            : id,
            short_code    : $("#short_code").val(),
            supplier_code : $("#supplier_code").val(),
            name          : $("#name").val(),
            category_id   : $("#category_id").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_supplier_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/suppliercontroller.php?action=save",
            {
                short_code    : $("#short_code").val(),
                supplier_code : $("#supplier_code").val(),
                name          : $("#name").val(),
                category_id   : $("#category_id").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_supplier_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    Supplier.get()
                }
            }, 'json');
    }

}