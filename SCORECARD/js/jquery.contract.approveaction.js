// JavaScript Document
$.widget("ui.approveaction", {

    options	: 	{
        tableId 		       : "approveaction_"+(Math.floor(Math.random(56) * 34)),
        url				       : "../controllers/actioncontroller.php?action=getApprovalActions",
        start			       : 0,
        limit			       : 10,
        total			       : 0,
        action_id	           : 0,
        financial_year	       : 0,
        contract_id	           : 0,
        owner_id               : 0,
        current			       : 1,
        isOwners		       : [],
        section                : "",
        viewAction             : ""
    } ,

    _init			: function()
    {
        this._getActions();
    } ,

    _create			: function()
    {
        var self = this;
        var html = [];
        html.push("<table class='noborder' width='100%'>");
            html.push("<tr>");
                html.push("<td class='noborder'>");
                    html.push("<table class='noborder'>");
                        html.push("<tr>");
                            html.push("<th style='text-align:left;'>Financial Year:</th>");
                            html.push("<td class='noborder'>");
                                html.push("<select id='financial_year' name='financial_year' style='width:200px;'>");
                                    html.push("<option value=''>--please select--</option>");
                                html.push("</select>");
                            html.push("</td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th style='text-align:left;'>Contract:</th>");
                            html.push("<td class='noborder'>");
                                html.push("<select id='contract_id' name='contract_id' style='width:200px;'>");
                                    html.push("<option value=''>--please select--</option>");
                                html.push("</select>");
                            html.push("</td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th style='text-align:left;'>Action Owner:</th>");
                            html.push("<td class='noborder'>");
                                html.push("<select id='owner_id' name='owner_id' style='width:200px;'>");
                                    html.push("<option value=''>--please select--</option>");
                                html.push("</select>");
                            html.push("</td>");
                        html.push("</tr>");
                    html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td class='noborder'>");
                    html.push("<table width='100%' class='noborder'>");
                        html.push("<tr>");
                            html.push("<td width='50%' class='noborder'>");
                                html.push("<table width='100%' id='awaiting_"+self.options.tableId+"'>");
                                html.push("<tr>")
                                    html.push("<td colspan='6'><h4>Actions Awaiting Approval</h4></td>");
                                html.push("</tr>");
                                html.push("</table>");
                                html.push("</td>");
                                html.push("<td class='noborder'>");
                                    html.push("<table width='100%' id='approved_"+self.options.tableId+"'>");
                                html.push("<tr width='50%'>");
                                    html.push("<td colspan='6'><h4>Actions Approved</h4></td>");
                                html.push("</tr>");
                                html.push("</table>");
                            html.push("</td>");
                        html.push("</tr>");
                    html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
        html.push("</table>");
        $(self.element).append(html.join(' '));

        self._loadFinancialYears();
        self._loadActionOwners();
        self._getActions();
        $("#financial_year").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.financial_year = $(this).val();
                self._loadContracts();
                self._getActions();
            } else {
                self.options.financial_year  = 0;
                self._loadContracts();
                self._getActions();
            }
        });

        $("#contract_id").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.contract_id = $(this).val();
                self._getActions();
            } else {
                self.options.contract_id = 0;
                self._getActions();
            }
        });

        $("#owner_id").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.owner_id = $(this).val();
                self._getActions();
            } else {
                self.options.owner_id = 0;
                self._getActions();
            }
        });
    } ,

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#financial_year").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
            });
            $("#financial_year").html( finHtml.join(' ') );
        });
    } ,

    _loadContracts              : function()
    {
        var self = this;
        $("#contract_id").empty();
        $.getJSON("../controllers/contractcontroller.php?action=getAll",{
            financial_year  : self.options.financial_year,
            section         : 'manage'
        }, function(contracts){
            var contractHtml = [];
            contractHtml.push("<option value=''>--please select--</option>");
            $.each(contracts, function(contract_id, contract) {
                if(contract_id == self.options.contract_id)
                {
                  contractHtml.push("<option value='"+contract_id+"' selected='selected'>"+contract+"</option>");
                } else {
                  contractHtml.push("<option value='"+contract_id+"'>"+contract+"</option>");
                }
            })
            $("#contract_id").html( contractHtml.join(' ') );
        });
    },

    _loadActionOwners              : function()
    {
        var self = this;
        $("#owner_id").empty();
        $.getJSON("../controllers/usercontroller.php?action=getAll", function(owners){
            var ownerHtml = [];
            ownerHtml.push("<option value=''>--please select--</option>");
            $.each(owners, function(owner_id, owner) {
                ownerHtml.push("<option value='"+owner_id+"'>"+owner+"</option>");
            })
            $("#owner_id").html( ownerHtml.join(' ') );
        });
    },


    _getActions		: function()
    {
        var self = this;
        $("body").append($("<div />",{id:"actionLoadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
        );
        $.getJSON( self.options.url, {
            start 	        : self.options.start,
            limit 	        : self.options.limit,
            id		        : self.options.action_id,
            contract_id     : self.options.contract_id,
            owner_id        : self.options.owner_id,
            financial_year  : self.options.financial_year,
            section         : self.options.section,
            page            : 'approve'
        }, function(actionData){
            $("#actionLoadingDiv").remove();
            $(".approval").remove();
            $(".approved").remove();
            $(".awaiting").remove();
            if(actionData.awaiting.financial_year != 0 || actionData.awaiting.financial_year != '')
            {
                self.options.financial_year = actionData.awaiting.financial_year;
                self._loadContracts();
                $("#financial_year").val(actionData.awaiting.financial_year);
            }
            self._displayHeaders(actionData.columns);
            self._displayAwaiting(actionData.awaiting.actions);
            self._displayApproved(actionData.approved.actions);
        });
    } ,

    _displayAwaiting    : function(actions)
    {
        var self = this;
        var html = [];
        if($.isEmptyObject(actions))
        {
            html.push("<tr class='awaiting'><td colspan='6'>There are no actions awaiting approval</td></tr>");
        } else {
            $.each(actions, function(action_id, action){
                html.push("<tr class='awaiting'>");
                html.push("<td>"+action.action_id+"</td>");
                html.push("<td>"+action.action_name+"</td>");
                html.push("<td>"+action.action_progress+"</td>");
                html.push("<td>"+action.action_on+"</td>");
                html.push("<td>"+action.action_owner+"</td>");
                html.push("<td>");
                    html.push("<input type='button' name='view_logs_"+action_id+"' id='view_logs_"+action_id+"' value='View Logs' />");
                    $("#view_logs_"+action_id).live("click", function(e){
                        self._viewLogs(action_id);
                        e.preventDefault();
                    });

                html.push("<input type='button' name='approve_"+action_id+"' id='approve_"+action_id+"' value='Approve' />");
                $("#approve_"+action_id).live("click", function(e){
                    self._approveContract(action_id);
                    e.preventDefault();
                });
                html.push("</td>");


                html.push("</tr>");
            });
        }
        $("#awaiting_"+self.options.tableId).append(html.join(' ') );

    } ,

    _displayApproved			: function(actions)
    {
        var self = this;
        var html = [];
        if($.isEmptyObject(actions))
        {
            html.push("<tr class='approved'><td colspan='6'>There are no actions approved</td></tr>");
        } else {
            $.each(actions, function(action_id, action){
                html.push("<tr class='approved'>");
                    html.push("<td>"+action.action_id+"</td>");
                    html.push("<td>"+action.action_name+"</td>");
                    html.push("<td>"+action.action_progress+"</td>");
                    html.push("<td>"+action.action_on+"</td>");
                    html.push("<td>"+action.action_owner+"</td>");
                    html.push("<td>");
                        html.push("<input type='button' name='undo_approve_"+action_id+"' id='undo_approve_"+action_id+"' value='Undo Approve' />");
                        $("#undo_approve_"+action_id).live("click", function(e){
                            self._undoApproveAction(action_id);
                            e.preventDefault();
                        });
                    html.push("</td>");

                html.push("</tr>");
            });
        }
        $("#approved_"+self.options.tableId).append(html.join(' ') );
    } ,

    _approveContract      : function(action_id)
    {
        var self = this;
        if($("#approve_action_dialog").length > 0)
        {
            $("#approve_action_dialog").remove();
        }
        var html = [];
        html.push("<table>");
            html.push("<tr>");
                html.push("<th>Response</th>")
                html.push("<td>");
                    html.push("<textarea id='response' name='response' cols='50' rows='8'></textarea>")
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td colspan='2'>");
                 html.push("<p id='message'></p>")
                html.push("</td>");
            html.push("</tr>");
        html.push("<table>");

        $("<div />",{id:"approve_action_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Approve Action A"+action_id,
                width       : 'auto',
                buttons     : {
                    "Approve"      : function()
                    {
                        $('#message').html("Approving action ....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/actioncontroller.php?action=approveAction", {
                            action_id : action_id,
                            response    : $("#response").val()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getActions();
                            }
                        },"json");
                        $("#approve_action_dialog").dialog("destroy").remove();
                    } ,

                    "Decline" : function()
                    {
                        $('#message').html("Approving action ....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/actioncontroller.php?action=declineAction", {
                            action_id : action_id,
                            response    : $("#response").val()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getActions();
                            }
                        },"json");
                        $("#approve_action_dialog").dialog("destroy").remove();
                    },

                    "Cancel"        : function()
                    {
                        $("#approve_action_dialog").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#approve_action_dialog").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn    = btns[0];
                    var declineBtn = btns[1];
                    var cancelBtn  = btns[2];
                    $(saveBtn).css({"color":"#090"});
                    $(declineBtn).css({"color":"red"});
                }
            });

    },

    _undoApproveAction: function(action_id)
    {
        var self = this;
        if($("#undo_approve_action_dialog").length > 0)
        {
            $("#undo_approve_action_dialog").remove();
        }
        var html = [];
        html.push("<table>");
            html.push("<tr>");
                html.push("<th>Response</th>")
                html.push("<td>");
                    html.push("<textarea id='response' name='response' cols='50' rows='8'></textarea>")
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td colspan='2'>");
                    html.push("<p id='message'></p>")
                html.push("</td>");
            html.push("</tr>");
        html.push("<table>");

        $("<div />",{id:"undo_approve_action_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Undo Approve Action A"+action_id,
                width       : 'auto',
                buttons     : {
                    "Undo Approve"      : function()
                    {
                        $('#message').html("Approving action ....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/actioncontroller.php?action=undoApproveAction", {
                            action_id : action_id,
                            response    : $("#response").val()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getActions();
                            }
                        },"json");
                        $("#undo_approve_action_dialog").dialog("destroy").remove();
                    } ,

                    "Cancel"        : function()
                    {
                        $("#undo_approve_action_dialog").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#undo_approve_action_dialog").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
                }
            });

    },

    _displayHeaders		: function(headers)
    {
        var self = this;
        var html = [];
        html.push("<tr class='approval'>");
            html.push("<th>"+headers.action_id+"</th>");
            html.push("<th>"+headers.action_name+"</th>");
            html.push("<th>"+headers.action_progress+"</th>");
            html.push("<th>"+headers.action_on+"</th>");
            html.push("<th>"+headers.action_owner+"</th>");
            html.push("<th></th>");
        html.push("</tr>");
        $("#awaiting_"+self.options.tableId).append(html.join(' '));
        $("#approved_"+self.options.tableId).append(html.join(' ')).find("#remove").remove();;
    } ,

    _viewLogs                : function(action_id)
     {
         var self = this;
         if($("#view_log_dialog").length > 0)
         {
             $("#view_log_dialog").remove();
         }
         var html = [];
         html.push("<table id='action_log_table'>");
             html.push("<tr>");
                 html.push("<th>Date/Time</th>");
                 html.push("<th>User</th>");
                 html.push("<th>Change Log</th>");
                 html.push("<th>Status</th>");
             html.push("</tr>");
         $("<div />",{id:"view_log_dialog"}).append(html.join(' '))
             .dialog({
                 autoOpen    : true,
                 modal       : true,
                 position    : "top",
                 title       : "Audit Log Action A"+action_id,
                 width       : 'auto',
                 buttons     : {
                     "Ok"      : function()
                     {
                         $("#view_log_dialog").dialog("destroy").remove();
                     }
                 },
                 close       : function(event, ui)
                 {
                     $("#view_log_dialog").dialog("destroy").remove();
                 },
                 open        : function(event, ui)
                 {
                     $.get("../controllers/actioncontroller.php?action=getActionUpdateLog", {
                         id : action_id
                     }, function(actionUpdates){
                         $("#action_log_table").append(actionUpdates);
                     },"html");
                     $(this).css("height", "auto");
                 }
             });
    },

    _displayPaging		: function(total, cols)
    {
        var self  = this;
        var html  = [];
        var pages = 0;
        if(total%self.options.limit > 0)
        {
            pages = Math.ceil( total/self.options.limit )
        } else {
            pages = Math.floor( total/self.options.limit )
        }
        html.push("<tr>");
        html.push("<td colspan='"+cols+"'>");
            html.push("<input type='button' id='action_first_"+self.option.action_id+"' value=' |< ' />");
            html.push("<input type='button' id='action_previous_"+self.option.action_id+"' value=' < ' />");
            html.push("<span>&nbsp;&nbsp;&nbsp;");
            html.push("Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ?  1 :pages));
            html.push("&nbsp;&nbsp;&nbsp;</span>");
            html.push("<input type='button' id='action_next_"+self.option.action_id+"' value=' > ' />");
            html.push("<input type='button' id='action_last_"+self.option.action_id+"' value=' >| ' />");
        html.push("</td>");
        if(self.options.editAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.updateAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.assuranceAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAssurance)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        html.push("</tr>");
        $("#"+self.options.tableId+"_"+self.options.action_id).append(html.join(' '));
        if(self.options.current < 2)
        {
            $("#action_first_"+self.option.action_id).attr('disabled', 'disabled');
            $("#action_previous_"+self.option.action_id).attr('disabled', 'disabled');
        }
        if((self.options.current == pages || pages == 0))
        {
            $("#action_next_"+self.option.action_id).attr('disabled', 'disabled');
            $("#action_last_"+self.option.action_id).attr('disabled', 'disabled');
        }
        $("#action_next_"+self.option.action_id).bind("click", function(){
            self._getNext( self );
        });
        $("#action_last_"+self.option.action_id).bind("click",  function(){
            self._getLast( self );
        });
        $("#action_previous_"+self.option.action_id).bind("click",  function(){
            self._getPrevious( self );
        });
        $("#action_first_"+self.option.action_id).bind("click",  function(){
            self._getFirst( self );
        });
    } ,

    _getNext  			: function( $this ) {
        $this.options.current   = $this.options.current+1;
        $this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
        this._getActions();
    },

    _getLast  			: function( $this ) {
        $this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
        $this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );
        this._getActions();
    },

    _getPrevious    	: function( $this ) {
        $this.options.current   = parseFloat( $this.options.current ) - 1;
        $this.options.start 	= ($this.options.current-1)*$this.options.limit;
        this._getActions();
    },

    _getFirst  			: function( $this ) {
        $this.options.current   = 1;
        $this.options.start 	= 0;
        this._getActions();
    }


});
