$.widget("ui.action", {

    options		: {
        tableId			  : "action_table_"+(Math.floor(Math.random(56) * 33)),
        url 			  : "../controllers/actioncontroller.php?action=getActions",
        total 		 	  : 0,
        start			  : 0,
        current			  : 1,
        limit			  : 10,
        editAction	      : false,
        updateAction      : false,
        authorizeAction   : false,
        actionAssurance   : false,
        viewAction        : false,
        view			  : "",
        section			  : "",
        page              : "",
        autoLoad          : true,
        financialYear     : 0,
        actionOwner       : 0,
        scoreCardId       : 0,
        deliverableId     : 0,
        actionId          : 0,
        thClass           : '',
        deliverableRef    : '',
        allowFilter       : true,
        updateAssessAction : false,
        openActions        : false,
        allowSeach         : true
    } ,

    _init          : function()
    {
        if(this.options.autoLoad)
        {
            this._getActions();
        }
        if(this.options.deliverableRef == '')
        {
            this.options.deliverableRef = deliverable_id;
        }
    } ,

    _create       : function()
     {
         var self = this;
         var html = [];
         html.push("<table class='noborder' width='100%'>");
         if(self.options.allowFilter)
         {
             html.push("<tr>");
                 html.push("<td class='noborder'>");
                     html.push("<table class='noborder'>");
                         html.push("<tr>");
                             html.push("<th style='text-align:left;'>Financial Year:</th>");
                             html.push("<td class='noborder'>");
                                 html.push("<select id='financial_year' name='financial_year' style='width:200px;'>");
                                   html.push("<option value=''>--please select--</option>");
                                 html.push("</select>");
                             html.push("</td>");
                         html.push("</tr>");
                         html.push("<tr>");
                             html.push("<th style='text-align:left;'>Scorecard:</th>");
                             html.push("<td class='noborder'>");
                                 html.push("<select id='action_score_card_id' name='action_score_card_id' style='width:200px;'>");
                                   html.push("<option value=''>--please select--</option>");
                                 html.push("</select>");
                             html.push("</td>");
                         html.push("</tr>");
                         html.push("<tr>");
                             html.push("<th style='text-align:left;'>Heading:</th>");
                             html.push("<td class='noborder'>");
                                 html.push("<select id='deliverable_id' name='deliverable_id' style='width:200px;'>");
                                   html.push("<option value=''>--please select--</option>");
                                 html.push("</select>");
                             html.push("</td>");
                         html.push("</tr>");
                     html.push("</table>");
                 html.push("</td>");
             html.push("</tr>");
         }
         self.options.tableId = self.options.tableId+"_"+self.options.deliverableRef;
             html.push("<tr>");
                 html.push("<td class='noborder'>");
                    html.push("<table id='"+self.options.tableId+"' width='100%'>");
                      html.push("<tr>")
                          html.push("<td>");
                              html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                                html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                                html.push("<span style='padding:5px; float:left;'>Please select the financial year</span>");
                             html.push("</p>");
                         html.push("</td>");
                     html.push("</tr>");
                    html.push("</table>");
                 html.push("</td>");
             html.push("</tr>");
         html.push("</table>");


         $(this.element).append(html.join(' '));
         self._loadFinancialYears();
         self._getActions();

         $("#financial_year").live("change", function(){
             if($(this).val() !== "")
             {
                 self.options.financialYear = $(this).val();
                 self._loadContracts();
                 self._loadDeliverables();
                 self._getActions();
             } else {
                 self.options.financialYear  = 0;
                 self._getActions();
             }
         });

         $("#action_score_card_id").live("change", function(){
             if($(this).val() !== "")
             {
                 self.options.scoreCardId = $(this).val();
                 self._loadDeliverables();
                 self._getActions();
             } else {
                 self.options.scoreCardId = 0;
                 self._getActions();
             }
         });

         $("#deliverable_id").live("change", function(){
             if($(this).val() !== "")
             {
                 self.options.deliverableId = $(this).val();
                 self._getActions();
             } else {
                 self.options.deliverableId = 0;
                 self._getActions();
             }
         });
     } ,

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#financial_year").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
            });
            $("#financial_year").html( finHtml.join(' ') );
        });
    } ,

    _loadContracts              : function()
    {
        var self = this;
        $("#action_score_card_id").empty();
        $.getJSON("../controllers/scorecardcontroller.php?action=getAll",{
            financial_year  : self.options.financialYear,
            section         : self.options.section,
            page            : self.options.page
        }, function(scorecards) {
            var contractHtml = [];
            contractHtml.push("<option value=''>--please select--</option>");
            $.each(scorecards, function(score_card_id, score_card) {
                if(self.options.scoreCardId == score_card_id)
                {
                  contractHtml.push("<option value='"+score_card_id+"' selected='selected'>"+score_card.short_description+"</option>");
                } else {
                  contractHtml.push("<option value='"+score_card_id+"'>"+score_card.short_description+"</option>");
                }
            })
            $("#action_score_card_id").html( contractHtml.join(' ') );
        });
    },

    _loadDeliverables              : function()
    {
        var self = this;
        $("#deliverable_id").empty();
        $("#deliverable_id").append($("<option />", {html:"loading ..."}));
        $.getJSON("../controllers/deliverablecontroller.php?action=getAll", {
            financial_year  : self.options.financialYear,
            score_card_id   : self.options.scoreCardId,
            parent_id       : self.options.parentId,
            section         : self.options.section,
            page            : self.options.page+"_action"
        } ,function(deliverables){
            var deliverableHtml = [];
            deliverableHtml.push("<option value=''>--please select--</option>");
            $.each(deliverables ,function(deliverable_id, deliverable) {
                deliverableHtml.push("<option value='"+deliverable_id+"'>"+deliverable+"</option>");
            })
            $("#deliverable_id").html( deliverableHtml.join(' ') );
        });
    },

    _getActions      : function()
     {
         var self = this;
         $("body").append($("<div />",{id:"loadingDiv", html:"Loading questions . . . <img src='../images/loaderA32.gif' />"})
             .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
         );
         $.getJSON( self.options.url,{
             start		       : self.options.start,
             limit		       : self.options.limit,
             view		       : self.options.view,
             section		   : self.options.section,
             page              : self.options.page,
             financial_year    : self.options.financialYear,
             score_card_id     : self.options.scoreCardId,
             deliverable_id    : self.options.deliverableId,
             action_id         : self.options.actionId
         },function(actionData) {
             $("#loadingDiv").remove();
             $("#"+self.options.tableId).html("");
             $(".more_less").remove();
             if(actionData.financial_year != 0 || actionData.financial_year != '')
             {
                 self.options.financialYear = actionData.financial_year;
                 self._loadContracts();
                 $("#financial_year").val(actionData.financial_year);
             }
             if(actionData.total != null)
             {
                 self.options.total = actionData.total
             }

             self._displayPaging(actionData.total_columns);
             self._displayHeaders(actionData.columns);
             if($.isEmptyObject(actionData.actions))
             {
                 var _html = [];
                 _html.push("<tr>")
                     _html.push("<td colspan='"+(actionData.total_columns+3)+"'>");
                     _html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                         _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                         if(actionData.hasOwnProperty('action_message') && actionData.action_message != '')
                         {
                             _html.push("<span style='padding:5px; float:left;'>"+actionData.action_message+"</span>");
                         } else {
                             _html.push("<span style='padding:5px; float:left;'>There are no questions found</span>");
                         }
                         //_html.push("<span style='padding:5px; float:left;'>"+actionData.action_message+"</span>");
                     _html.push("</p>");
                     _html.push("</td>");
                 _html.push("</tr>");
                 $("#"+self.options.tableId).append( _html.join(' ') );
             } else {
                 self._display(actionData.actions, actionData.columns, actionData);
             }
         });
     } ,

     _display           : function(actions, columns, actionData)
     {
         var self = this;
         var i    = 1;
         $.each(actions, function(index, action){
             i++;
             var html = [];
             if(self.options.page == "view" && actionData.hasOwnProperty('canUpdate'))
             {
                 if(actionData.canUpdate.hasOwnProperty(index) && actionData.canUpdate[index] == true)
                 {
                     html.push("<tr>");
                 } else {
                     html.push("<tr class='view-notowner'>");
                 }
             } else {
                 html.push("<tr>");
             }
             $.each( action, function( key , val){
                 if(key == 'action_progress')
                 {
                     html.push("<td style='text-align: right;'>"+val+"</td>");
                 } else {
                     html.push("<td>"+val+"</td>");
                 }
             })

             html.push(self._displayActionUpdateBtn(actionData, index));
             html.push(self._displayActionEditBtn(actionData, index));
             html.push(self._displayActionViewBtn(index));
             html.push(self._displayActionAssuranceBtn(index));
             html.push(self._displayActionAuthorizationeBtn(index));
             html.push(self._displayActionAssessUpdateBtn(index));

             html.push("</tr>");
             $("#"+self.options.tableId).append( html.join(' ') ).find('.remove_attach').remove();
             $("#"+self.options.tableId).find("#result_message").remove();
         });
     },

    _displayActionAuthorizationeBtn  : function(action_id)
    {
        var html = [];
        var self = this;
        if(self.options.authorizeAction)
        {
            html.push("<td>&nbsp;");
               html.push("<input type='button' value='Authorize' id='action_authorize_"+action_id+"' id='action_authorize_"+action_id+"' />");
            html.push("</td>");
            $("#action_authorize_"+action_id).live("click", function(){
                document.location.href = "authorize_action.php?id="+action_id;
                return false;
            });
            return html.join(' ');
        }
        return '';
    } ,

    _displayActionAssuranceBtn  : function(action_id)
    {
        var html = [];
        var self = this;
        if(self.options.actionAssurance)
        {
            html.push("<td>&nbsp;");
              html.push("<input type='button' value='Assurance' id='action_assurance_"+action_id+"' id='action_assurance_"+action_id+"' />");
            html.push("</td>");
            $("#action_assurance_"+action_id).live("click", function(){
                document.location.href = "action_assurance.php?id="+action_id;
                return false;
            });
            return html.join(' ');
        }
        return '';
    },

    _displayActionAssessUpdateBtn  : function(action_id)
    {
        var html = [];
        var self = this;
        if(self.options.updateAssessAction)
        {
            html.push("<td>&nbsp;");
            html.push("<input type='button' value='Update' id='update_action_"+action_id+"' name='update_action_"+action_id+"' />");
            html.push("</td>");
            $("#update_action_"+action_id).live("click", function(){
               self._updateAction( action_id );
               return false;
            });
            return html.join(' ');
        }
        return '';
    },

    _displayActionViewBtn  : function(action_id)
    {
        var html = [];
        var self = this;
        if(self.options.viewAction)
        {
            html.push("<td>&nbsp;");
            html.push("<input type='button' value='View Question Details' id='view_action_"+action_id+"' name='view_action_"+action_id+"' />");
            html.push("</td>");
            $("#view_action_"+action_id).live("click", function(){
                document.location.href = "view_action.php?id="+action_id;
                return false;
            });
        }
        return '';
    },

    _displayActionEditBtn  : function(actionData, action_id)
    {
        var html = [];
        var self = this;
        if(self.options.editAction)
        {
            html.push("<td>&nbsp;");
            if(actionData.hasOwnProperty('canEdit'))
            {
                if(actionData.canEdit.hasOwnProperty(action_id))
                {
                    if(actionData.canEdit[action_id] == true)
                    {
                        html.push("<input type='button' value='Edit Question' id='edit_action_"+action_id+"' name='edit_action_"+action_id+"' />");
                        $("#edit_action_"+action_id).live("click", function(){
                            if(self.options.page == "confirmation")
                            {
                               document.location.href = "edit_action.php?id="+action_id+"&from=confirmation&score_card_id="+self.options.scoreCardId;
                            } else {
                                document.location.href = "edit_action.php?id="+action_id;
                            }
                            return false;
                        });
                    }
                }
            }
            if(self.options.section == "new" && self.options.page != "confirmation")
            {
                html.push("<input type='button' value='Delete' id='delete_action_"+action_id+"' name='delete_action_"+action_id+"' class='idelete' />");
                $("#delete_action_"+action_id).live("click", function(){
                    if(confirm('Are you sure you want to delete this question'))
                    {
                        $.getJSON("../controllers/actioncontroller.php?action=deleteAction", {
                            action_id : action_id
                        }, function(response){
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                if(response.updated)
                                {
                                    jsDisplayResult("ok", "ok", response.text);
                                    self._getDeliverables();
                                } else {
                                    jsDisplayResult("info", "info", response.text);
                                }
                            }
                        });
                    }
                    return false;
                });
            }
            html.push("</td>");
            return html.join('');
        }
        return '';
    },

    _displayActionUpdateBtn  : function(actionData, action_id)
    {
        var html = [];
        var self = this;
        if(self.options.updateAction)
        {
            html.push("<td>&nbsp;");
            if(actionData.hasOwnProperty('canUpdate'))
            {
                if(actionData.canUpdate.hasOwnProperty(action_id))
                {
                    html.push("<input type='button' value='Update Question' id='update_"+action_id+"' name='update_"+action_id+"' />");
                    $("#update_"+action_id).live("click", function(){
                        document.location.href = "update_action.php?id="+action_id;
                        return false;
                    });
                }
            }
            if(actionData.hasOwnProperty('canUndoApprove'))
            {
                if(actionData.canUndoApprove.hasOwnProperty(action_id))
                {
                    if(actionData.canUndoApprove[action_id] == true && self.options.section == "admin")
                    {
                        html.push("<input type='button' value='Unlock' id='undo_approval_"+action_id+"' name='undo_approval_"+action_id+"' />");
                        $("#undo_approval_"+action_id).live("click", function(){
                            self._undoActionApproval(action_id);
                            return false;
                        });
                    }
                }
            }
            html.push("</td>");
            return html.join(' ');
        }
        return '';
    },
    _undoActionApproval: function(action_id)
    {
        var self = this;
        if($("#undo_approve_action_dialog").length > 0)
        {
            $("#undo_approve_action_dialog").remove();
        }
        var html = [];
        html.push("<table>");
            html.push("<tr>");
                html.push("<th>Response:</th>")
                html.push("<td>");
                    html.push("<textarea id='response' name='response' cols='50' rows='8'></textarea>")
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td colspan='2'>");
                    html.push("<p id='message'></p>")
                html.push("</td>");
            html.push("</tr>");
        html.push("<table>");

        $("<div />",{id:"undo_approve_action_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Undo Approve Question A"+action_id,
                width       : 'auto',
                buttons     : {
                    "Undo Approve"      : function()
                    {
                        $('#message').html("Approving question . . . .<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/actioncontroller.php?action=undoApproveAction", {
                            action_id : action_id,
                            response    : $("#response").val()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getActions();
                            }
                        },"json");
                        $("#undo_approve_action_dialog").dialog("destroy").remove();
                    } ,

                    "Cancel"        : function()
                    {
                        $("#undo_approve_action_dialog").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#undo_approve_action_dialog").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
                }
            });

    },

    _updateAction        : function(action_id)
    {
        var self = this;
        if($("#action_update_dialog").length > 0)
        {
            $("#action_update_dialog").remove();
        }
        var html = [];
        html.push("<table>");
            html.push("<tr>");
                html.push("<th>Rating:</th>");
                html.push("<td>");
                html.push("<select id='rating' name='rating'>");
                html.push("</select>");
                html.push("</td>");
            html.push("</tr>");
        html.push("<tr>");
            html.push("<th>Comment:</th>");
            html.push("<td>");
                html.push("<textarea id='comment' name='comment' cols='50' rows='8'></textarea>");
            html.push("</td>");
        html.push("</tr>");
        html.push("<tr>");
        html.push("<td colspan='2'>");
        html.push("<p id='message'></p>")
        html.push("</td>");
        html.push("</tr>");
        html.push("<table>");

        $("<div />",{id:"action_update_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Update Question A"+action_id,
                width       : 'auto',
                buttons     : {
                    "Update"      : function()
                    {
                        $('#message').html("Updating action ....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/actioncontroller.php?action=updateAssessAction", {
                            action_id : action_id,
                            response    : $("#response").val()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getActions();
                            }
                        },"json");
                        $("#action_update_dialog").dialog("destroy").remove();
                    } ,

                    "Cancel"        : function()
                    {
                        $("#action_update_dialog").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#action_update_dialog").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
                }
            });

    },

     _displayHeaders     : function(headers)
     {
         var self = this;
         var html = [];
         html.push("<tr class='"+self.options.thClass+"'>");
         $.each( headers , function( index, head ){
             html.push("<th class='"+self.options.thClass+"'>"+head+"</th>");
         });
         if(self.options.updateAction)
         {
             html.push("<th class='"+self.options.thClass+"'>&nbsp;</th>");
         }
         if(self.options.editAction)
         {
             html.push("<th class='"+self.options.thClass+"'>&nbsp;</th>");
         }
         if(self.options.viewAction)
         {
             html.push("<th class='"+self.options.thClass+"'>&nbsp;</th>");
         }
         if(self.options.actionAssurance)
         {
             html.push("<th class='"+self.options.thClass+"'>&nbsp;</th>");
         }
         if(self.options.updateAssessAction)
         {
             html.push("<th class='"+self.options.thClass+"'>&nbsp;</th>");
         }
         html.push("</tr>");
         $("#"+self.options.tableId).append(html.join(' '));
     },

    _displayPaging	: function(columns)
     {
        var self  = this;
        var html  = [];
        var pages;
        if( self.options.total%self.options.limit > 0){
            pages   = Math.ceil(self.options.total/self.options.limit);
        } else {
            pages   = Math.floor(self.options.total/self.options.limit);
        }
        var deliverable_id = self.options.deliverableId;
        html.push("<tr>");
            html.push("<td colspan='"+(columns + 3)+"' class='noborder'>");
            html.push("<input type='button' value=' |< ' id='action_first_"+deliverable_id+"' name='first' />");
            html.push("<input type='button' value=' < '  id='action_previous_"+deliverable_id+"' name='previous' />");
            if(pages != 0)
            {
                html.push("Page <select id='action_select_page' name='action_select_page'>");
                for(var p=1; p<=pages; p++)
                {
                    html.push("<option value='"+p+"' "+(self.options.current == p ? "selected='selected'" : "")+">"+p+"</option>");
                }
                html.push("</select>");
            } else {
                html.push("Page <select id='action_select_page' name='action_select_page' disabled='disabled'>");
                html.push("</select>");
            }
            html.push(" of "+(pages == 0 || isNaN(pages) ? 0 : pages)+" pages");
            html.push("<input type='button' value=' > ' id='action_next_"+deliverable_id+"' name='next' />");
            html.push("<input type='button' value=' >| ' id='action_last_"+deliverable_id+"' name='last' />");
            if(self.options.assuranceAction)
            {
                html.push("&nbsp;");
            }
            if(self.options.updateAction)
            {
                html.push("&nbsp;");
            }
            if(self.options.editAction)
            {
                html.push("&nbsp;");
            }
            if(self.options.addAction)
            {
                html.push("&nbsp;");
            }
            if(self.options.viewAction)
            {
                html.push("&nbsp;");
            }
                html.push("<span style='float:right;'>");
                    html.push("<b>Quick Search for Question:</b> Q <input type='text' name='a_id' id='a_id' value='' />");
                    html.push("&nbsp;&nbsp;&nbsp;");
                    html.push("<input type='button' name='action_search' id='action_search' value='Go' />");
                html.push("</span>");
            html.push("</td>");
        html.push("</tr>");
        $("#"+self.options.tableId).append(html.join(' '));


        $("#action_select_page").live("change", function(){
            if($(this).val() !== "" && parseFloat($(this).val())>0)
            {
                self.options.start   = parseFloat($(this).val() -1) * parseFloat(self.options.limit);
                self.options.current = parseFloat($(this).val());
                self._getActions();
            }
        });
        $("#action_search").live("click", function(e){
            var action_id = $("#a_id").val();
            if(action_id != "")
            {
                self.options.actionId = $("#d_id").val();
                self._getActions();
            } else {
                $("#a_id").addClass('ui-state-error');
                $("#a_id").focus().select();
            }
            e.preventDefault();
        });

        if(self.options.current < 2)
        {
            $("#action_first_"+deliverable_id).attr('disabled', 'disabled');
            $("#action_previous_"+deliverable_id).attr('disabled', 'disabled');
        }
        if((self.options.current == pages || pages == 0))
        {
            $("#action_next_"+deliverable_id).attr('disabled', 'disabled');
            $("#action_last_"+deliverable_id).attr('disabled', 'disabled');
        }
        $("#action_next_"+deliverable_id).bind("click", function(evt){
            self._getNext( self );
        });
        $("#action_last_"+deliverable_id).bind("click",  function(evt){
            self._getLast( self );
        });
        $("#action_previous_"+deliverable_id).bind("click",  function(evt){
            self._getPrevious( self );
        });
        $("#action_first_"+deliverable_id).bind("click",  function(evt){
            self._getFirst( self );
        });
    } ,

    _getNext  			: function(self)
    {
        self.options.current = (self.options.current + 1);
        self.options.start   = (self.options.current-1) * self.options.limit;
        this._getActions();
    },

    _getLast  			: function( self )
    {
        self.options.current =  Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
        self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );
        this._getActions();
    },
    _getPrevious    	: function( self )
    {
        self.options.current  = parseFloat(self.options.current) - 1;
        self.options.start 	= (self.options.current-1) * self.options.limit;
        this._getActions();
    },
    _getFirst  			: function( self )
    {
        self.options.current  = 1;
        self.options.start 	= 0;
        this._getActions();
    }


})