$(function(){

    //$(".textcounter").textcounter({maxLength:100});

    OtherScore.get();
    $("#add_other_score").click(function(e){
        OtherScore.addOtherScore();
        e.preventDefault();
    });

});

var OtherScore 	= {

    get				: function()
    {
        $(".other_scores").remove();
        $.getJSON( "../controllers/otherscorecontroller.php?action=getAll", function( data ) {
            if($.isEmptyObject(data))
            {
                $("#other_score_table").append($("<tr />").addClass('other_scores')
                    .append($("<td />", {colspan: 7, html: "There are no quality scores" }))
                )
            } else {
                $.each( data, function( index, val){
                    OtherScore.display( val );
                });
            }
        })
    } ,

    display			: function( val )
    {
        var self = this;
        $("#other_score_table")
            .append($("<tr />",{id:"tr_"+val.id}).addClass('other_scores')
                .append($("<td />",{html:val.id}))
                .append($("<td />",{html:val.score}))
                .append($("<td />",{html:val.definition}))
                .append($("<td />",{html:val.description}))
                .append($("<td />")
                    .append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"}))
                )
                .append($("<td />",{html:"<b>"+(val.status == 1 ? "Active"  : "Inactive" )+"</b>"}))
                .append($("<td />")
                    .append($("<input />",{type:"submit", value:"Edit", id:"score_edit_"+val.id, name:"score_edit_"+val.id }))
                )
            )

        $("#score_edit_"+val.id).live("click", function(e){
            self._updateOtherScore(val);
            e.preventDefault();
        });

    },

    addOtherScore          : function()
    {
        var self = this;
        if($("#add_other_score_dialog").length > 0)
        {
            $("#add_other_score_dialog").remove();
        }

        $("<div />",{id:"add_other_score_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Score:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"score", id:"score", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Definition :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"definition", id:"definition", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Color:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"color", id:"color", value:''}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Quality Score",
                width          : 500,
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#score").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the score");
                            return false;
                        } else if($("#definition").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the definition");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_other_score_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_other_score_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});

                    var myPicker = new jscolor.color(document.getElementById('color'), {})
                    myPicker.fromString('99FF33')  // now you can access API via 'myPicker' variable
                }
            })
    },

    _updateOtherScore         : function(other_score)
    {
        var self = this;
        if($("#edit_other_score_dialog").length > 0)
        {
            $("#edit_other_score_dialog").remove();
        }

        $("<div />",{id:"edit_other_score_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Score:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"score", id:"score", value:other_score.score}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Definition :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"definition", id:"definition", value:other_score.definition}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8, text:other_score.description}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Color:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"color", id:"color", value:other_score.color}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Quality Score",
                width          : 500,
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#score").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the score");
                            return false;
                        } else if($("#definition").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the definition");
                            return false;
                        }  else {
                            self.update(other_score.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_other_score_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(other_score.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(other_score.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(other_score.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_other_score_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((other_score.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});

                    var myPicker = new jscolor.color(document.getElementById('color'), {})
                    myPicker.fromString(other_score.color)  // now you can access API via 'myPicker' variable
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/otherscorecontroller.php?action=update",
            {
                id      : id,
                status  : status
            }, function(response){
                if(response.error)
                {
                    $("#message").addClass("ui-state-error").html( response.text )
                } else {
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text );
                        self.get();
                    } else {
                        jsDisplayResult("info", "info", response.text );
                    }
                    $("#edit_other_score_dialog").dialog("destroy").remove();
                }
            },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/otherscorecontroller.php?action=update",
            {
                id           : id,
                score       : $("#score").val(),
                definition   : $("#definition").val(),
                description  : $("#description").val(),
                color        : $("#color").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").addClass("ui-state-error").html( response.text )
                } else {
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text );
                        self.get();
                    } else {
                        jsDisplayResult("info", "info", response.text );
                    }
                    $("#edit_other_score_dialog").dialog("destroy").remove();
                }
            },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/otherscorecontroller.php?action=save",
            {
                score       : $("#score").val(),
                definition   : $("#definition").val(),
                description  : $("#description").val(),
                color        : $("#color").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_other_score_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    OtherScore.get()
                }
            }, 'json');
    }

}