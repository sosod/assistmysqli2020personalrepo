$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    Weight.get();
    $("#add_weight").click(function(e){
        Weight.addWeight();
        e.preventDefault();
    });
	
});

var Weight 	= {

		get				: function()
		{
            $(".weights").remove();
			$.getJSON( "../controllers/weightcontroller.php?action=getAll", function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#weight_table").append($("<tr />").addClass('weights')
                        .append($("<td />", {colspan: 7, html: "There are no weights" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        Weight.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#weight_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('weights')
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.weight}))
				.append($("<td />",{html:val.definition}))
				.append($("<td />",{html:val.description}))
                .append($("<td />")
                    .append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"}))
                )
                .append($("<td />",{html:"<b>"+(val.status == 1 ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"weight_edit_"+val.id, name:"weight_edit_"+val.id }))
				 )
			  )

            $("#weight_edit_"+val.id).live("click", function(e){
                self._updateWeight(val);
                e.preventDefault();
            });
			
		},

    addWeight          : function()
    {
        var self = this;
        if($("#add_weight_dialog").length > 0)
        {
            $("#add_weight_dialog").remove();
        }

        $("<div />",{id:"add_weight_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Weight:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"weight", id:"weight", value:"", disabled:'disabled'}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Definition :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"definition", id:"definition", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Color:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"color", id:"color", value:''}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Weight",
                width          : 'auto',
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#weight").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the weight");
                            return false;
                        } else if($("#definition").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the definition");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_weight_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_weight_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});

                    var myPicker = new jscolor.color(document.getElementById('color'), {})
                    myPicker.fromString('99FF33')  // now you can access API via 'myPicker' variable
                }
            })
    },

    _updateWeight         : function(weight)
    {
        var self = this;
        if($("#edit_weight_dialog").length > 0)
        {
            $("#edit_weight_dialog").remove();
        }

        $("<div />",{id:"edit_weight_dialog"}).append($("<table />",{width:"100%"})
               .append($("<tr />")
                    .append($("<th />",{html:"Weight:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"weight", id:"weight", value:weight.weight, disabled:'disabled'}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Definition :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"definition", id:"definition", value:weight.definition}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8, text:weight.description}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Color:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"color", id:"color", value:weight.color}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Weight",
                width          : 'auto',
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#weight").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the weight");
                            return false;
                        } else if($("#definition").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the definition");
                            return false;
                        }  else {
                            self.update(weight.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_weight_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(weight.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(weight.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(weight.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_weight_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((weight.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"display":"none"});

                    var myPicker = new jscolor.color(document.getElementById('color'), {})
                    myPicker.fromString(weight.color)  // now you can access API via 'myPicker' variable
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/weightcontroller.php?action=update",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_weight_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/weightcontroller.php?action=update",
        {
            id           : id,
            weight       : $("#weight").val(),
            definition   : $("#definition").val(),
            description  : $("#description").val(),
            color        : $("#color").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_weight_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/weightcontroller.php?action=save",
            {
                weight       : $("#weight").val(),
                definition   : $("#definition").val(),
                description  : $("#description").val(),
                color        : $("#color").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_weight_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    Weight.get()
                }
            }, 'json');
    }

}