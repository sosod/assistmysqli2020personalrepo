$.widget("scorecard.completequestion", {

    options		: {
        tableId			  : "question_table_"+(Math.floor(Math.random(56) * 33)),
        url 			  : "../controllers/actioncontroller.php?action=getActionAssessment",
        total 		 	  : 0,
        start			  : 0,
        current			  : 1,
        limit			  : 10,
        editAction	      : false,
        updateAction      : false,
        authorizeAction   : false,
        actionAssurance   : false,
        viewAction        : false,
        view			  : "",
        section			  : "",
        page              : "",
        autoLoad          : true,
        financialYear     : 0,
        actionOwner       : 0,
        scoreCardId       : 0,
        deliverableId     : 0,
        actionId          : 0,
        thClass           : '',
        deliverableRef    : '',
        allowFilter       : true,
        updateAssessAction      : false,
        unConfirmCompleteAction : false,
        questionOwnerId         : 0
    } ,

    _init          : function()
    {
        if(this.options.autoLoad)
        {
            this._getActions();
        }
    } ,

    _create       : function()
    {
        var self = this;
        var html = [];
        html.push("<table class='noborder' width='100%'>");
        if(self.options.allowFilter)
        {
            html.push("<tr>");
                html.push("<td class='noborder'>");
                    html.push("<table class='noborder'>");
                        if(self.options.unConfirmCompleteAction)
                        {
                            html.push("<tr>");
                                html.push("<th style='text-align:left;'>Action Owner:</th>");
                                html.push("<td class='noborder'>");
                                    html.push("<select id='question_owner' name='question_owner' style='width:200px;'>");
                                    html.push("<option value=''>--please select--</option>");
                                    html.push("</select>");
                                html.push("</td>");
                            html.push("</tr>");
/*                            html.push("<tr>");
                                html.push("<th style='text-align:left;'>Directorate/Sub-Directorate:</th>");
                                html.push("<td class='noborder'>");
                                    html.push("<select id='sub_directorate' name='sub_directorate' style='width:200px;'>");
                                        html.push("<option value=''>--please select--</option>");
                                    html.push("</select>");
                                html.push("</td>");
                            html.push("</tr>");*/
                            html.push("<tr>");
                                html.push("<th style='text-align:left;'>Scorecard:</th>");
                                html.push("<td class='noborder'>");
                                    html.push("<select id='score_card_id' name='score_card_id' style='width:200px;'>");
                                    html.push("<option value=''>--please select--</option>");
                                    html.push("</select>");
                                html.push("</td>");
                            html.push("</tr>");
                            self._loadUsers()
                        } else {
                        html.push("<tr>");
                            html.push("<th style='text-align:left;'>Financial Year:</th>");
                            html.push("<td class='noborder'>");
                                html.push("<select id='financial_year' name='financial_year' style='width:200px;'>");
                                html.push("<option value=''>--please select--</option>");
                                html.push("</select>");
                            html.push("</td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th style='text-align:left;'>Scorecard:</th>");
                            html.push("<td class='noborder'>");
                            html.push("<select id='score_card_id' name='score_card_id' style='width:200px;'>");
                            html.push("<option value=''>--please select--</option>");
                            html.push("</select>");
                            html.push("</td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th style='text-align:left;'>Heading:</th>");
                            html.push("<td class='noborder'>");
                                html.push("<select id='deliverable_id' name='deliverable_id' style='width:200px;'>");
                                html.push("<option value=''>--please select--</option>");
                                html.push("</select>");
                            html.push("</td>");
                        html.push("</tr>");
                        }
                    html.push("</table>");
                html.push("</td>");
                html.push("<td id='score-card-summary' style='display: none;' class='noborder'>");
                    html.push("<table class='noborder'>");
                        html.push("<tr>");
                            html.push("<th valign='top' style='text-align: left;'>Scorecard Reference:</th>");
                            html.push("<td valign='top'></td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th valign='top' style='text-align: left;'>Scorecard Instruction to user:</th>");
                            html.push("<td valign='top'></td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th valign='top' style='text-align: left;'>Frequency:</th>");
                            html.push("<td valign='top'></td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th valign='top' style='text-align: left;'>Start Date:</th>");
                            html.push("<td valign='top'></td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th valign='top' style='text-align: left;'>End Date:</th>");
                            html.push("<td valign='top'></td>");
                        html.push("</tr>");
                    html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
        }
        self.options.tableId = self.options.tableId+"_"+self.options.deliverableRef;
            html.push("<tr>");
                html.push("<td class='noborder' colspan='2'>");
                    html.push("<table id='"+self.options.tableId+"' width='100%'>");
                        html.push("<tr>");
                            html.push("<td>");
                                html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                                    html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                                    html.push("<span style='padding:5px; float:left;'>Please select the financial year</span>");
                                html.push("</p>");
                            html.push("</td>");
                        html.push("</tr>");
                    html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
        html.push("</table>");


        $(this.element).append(html.join(' '));
        self._loadFinancialYears();
        self._getActions();

        if($("#financial_year").val() != '')
        {
            self.options.financialYear = $("#financial_year").val();
            self._loadScoreCards();
            self._loadDeliverables();
            self._getActions();
        }

        $("#financial_year").live("change", function(){
            if($(this).val() != "")
            {
                self.options.financialYear = $(this).val();
                self._loadScoreCards();
                self._loadDeliverables();
                self._getActions();
            } else {
                self.options.financialYear  = 0;
                self._getActions();
                $("#score-card-summary").hide();
            }
        });

        $("#score_card_id").live("change", function(){
            if($(this).val() != "")
            {
                self.options.scoreCardId = $(this).val();
                self._loadDeliverables();
                self._getActions();
                $("#score-card-summary").show();
            } else {
                self.options.scoreCardId = 0;
                self._getActions();
                $("#score-card-summary").hide();
            }
        });

        $("#deliverable_id").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.deliverableId = $(this).val();
                self._getActions();
            } else {
                self.options.deliverableId = 0;
                self._getActions();
            }
        });

        $("#question_owner").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.questionOwnerId = $(this).val();
                self._getActions();
            } else {
                self.options.questionOwnerId = 0;
                self._getActions();
            }
        });

    } ,

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#financial_year").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
            });
            $("#financial_year").html( finHtml.join(' ') );
        });
    } ,

    _loadScoreCards              : function()
    {
        var self = this;
        $("#score_card_id").empty();
        $.getJSON("../controllers/scorecardcontroller.php?action=getSetupList",{
            financial_year  : self.options.financialYear,
            section         : self.options.section,
            page            : self.options.page
        }, function(score_cards) {
            var scoreCardHtml = [];
            scoreCardHtml.push("<option value=''>--please select--</option>");
            $.each(score_cards, function(score_card_id, score_card) {
                if(self.options.scoreCardId == score_card_id)
                {
                    scoreCardHtml.push("<option value='"+score_card_id+"' selected='selected'>"+score_card+"</option>");
                } else {
                    scoreCardHtml.push("<option value='"+score_card_id+"'>"+score_card+"</option>");
                }
            })
            $("#score_card_id").html( scoreCardHtml.join(' ') );
        });
    },

    _loadDeliverables              : function()
    {
        var self = this;
        $("#deliverable_id").empty();
        $("#deliverable_id").append($("<option />", {html:"loading ..."}));
        $.getJSON("../controllers/deliverablecontroller.php?action=getAll", {
            financial_year  : self.options.financialYear,
            score_card_id   : self.options.scoreCardId,
            parent_id       : self.options.parentId,
            section         : self.options.section,
            page            : self.options.page+"_action"
        } ,function(deliverables){
            var deliverableHtml = [];
            deliverableHtml.push("<option value=''>--please select--</option>");
            $.each(deliverables ,function(deliverable_id, deliverable) {
                deliverableHtml.push("<option value='"+deliverable_id+"'>"+deliverable+"</option>");
            })
            $("#deliverable_id").html( deliverableHtml.join(' ') );
        });
    },


    _loadUsers           : function()
    {
        var self = this;
        $("#question_owner").html("");
        $.getJSON("../controllers/usercontroller.php?action=getAll", function( users ){
            var userHtml = []
            userHtml.push("<option value=''>--please select--</option>");
            $.each(users, function(user_id, user){
                userHtml.push("<option value='"+user_id+"'>"+user+"</option>");
            });
            $("#question_owner").html( userHtml.join(' ') );
        });
    } ,

    _getActions      : function()
    {
        var self = this;
        $("body").append($("<div />",{id:"loadingDiv", html:"Loading actions . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
        );
        $.getJSON( self.options.url,{
            start		       : self.options.start,
            limit		       : self.options.limit,
            view		       : self.options.view,
            section		   : self.options.section,
            page              : self.options.page,
            financial_year    : self.options.financialYear,
            score_card_id     : self.options.scoreCardId,
            deliverable_id    : self.options.deliverableId,
            owner_id          : self.options.questionOwnerId
        },function(actionData) {
            $("#loadingDiv").remove();
            $("#"+self.options.tableId).html("");
            $(".more_less").remove();
            if((actionData.financial_year != 0 || actionData.financial_year != '') && (self.options.financialYear == '' || self.options.financialYear == null) )
            {
                self.options.financialYear = actionData.financial_year;
                self._loadScoreCards();
                $("#financial_year").val(actionData.financial_year);
            }
            if(actionData.total != null)
            {
                self.options.total = actionData.total
            }

            if(actionData.hasOwnProperty('score_card'))
            {
                self._displayScoreCardSetupSummary(actionData.score_card);
            }

            self._displayPaging(actionData.total_columns);
            self._displayHeaders(actionData.columns);
            if($.isEmptyObject(actionData.actions))
            {
                var _html = [];
                _html.push("<tr>")
                _html.push("<td colspan='"+(actionData.total_columns+5)+"'>");
                _html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                if(actionData.hasOwnProperty('action_message') && actionData.action_message != '')
                {
                    _html.push("<span style='padding:5px; float:left;'>"+actionData.action_message+"</span>");
                } else {
                    _html.push("<span style='padding:5px; float:left;'>There are no actions found</span>");
                }
                //_html.push("<span style='padding:5px; float:left;'>"+actionData.action_message+"</span>");
                _html.push("</p>");
                _html.push("</td>");
                _html.push("</tr>");
                $("#"+self.options.tableId).append( _html.join(' ') );
            } else {
                self._display(actionData.actions, actionData.columns, actionData);
            }

            if( !$.isEmptyObject(actionData.actions) && self.options.unConfirmCompleteAction && self.options.scoreCardId )
            {
                var unConfirmButton = [];
                var extraCols     = actionData.total_columns - (actionData.rating_position-1);
                unConfirmButton.push("<tr class='details'>");
                unConfirmButton.push("<td colspan='"+(actionData.rating_position - 1)+"' class='noborder'>");
                  unConfirmButton.push("<input type='submit' name='confirm_score_card_"+self.options.scoreCardId+"' id='confirm_score_card_"+self.options.scoreCardId+"' value='UnConfirm Scorecard' class='isubmit confirm_button'  />");
                unConfirmButton.push("</td>");
                unConfirmButton.push("<td>"+actionData.overall_rating+"</td>");
                if(extraCols > 0)
                {
                    unConfirmButton.push("<td colspan='"+extraCols+"'>&nbsp;</td>");
                }
                unConfirmButton.push("</tr>");
                $("#confirmation-table").append( unConfirmButton.join('') ).css({'text-align':'left'});
                $("#confirm_score_card_"+self.options.scoreCardId).live('click', function(e){
                    self._unconfirmCompleteScoreCard(actionData.score_card);
                    e.preventDefault();
                });
                $("#"+self.options.tableId).append( unConfirmButton.join(' ') );

            }


            if(actionData.hasOwnProperty('score_card') && !$.isEmptyObject(actionData.actions))
            {
                if(self.options.updateAssessAction)
                {
                    var confirmButton = [];
                    var extraCols     = actionData.total_columns - (actionData.rating_position-1);
                    confirmButton.push("<tr class='details'>");
                    confirmButton.push("<td colspan='"+(actionData.rating_position - 1)+"' class='noborder'>");
                    confirmButton.push("<input type='submit' name='confirm_score_card_"+self.options.scoreCardId+"' id='confirm_score_card_"+self.options.scoreCardId+"' value='Confirm Scorecard' class='isubmit confirm_button'  />");
                    confirmButton.push("</td>");
                    confirmButton.push("<td>"+actionData.overall_rating+"</td>");
                    if(extraCols > 0)
                    {
                       confirmButton.push("<td colspan='"+extraCols+"'>&nbsp;</td>");
                    }
                    confirmButton.push("</tr>");
                    $("#confirmation-table").append( confirmButton.join('') ).css({'text-align':'left'});
                    $("#confirm_score_card_"+self.options.scoreCardId).live('click', function(e){
                        self._confirmCompleteScoreCard(actionData.score_card);
                        e.preventDefault();
                    });
                    $("#"+self.options.tableId).append( confirmButton.join(' ') );
                }
            }
        });
    } ,

    _display           : function(actions, columns, actionData)
    {
        var self = this;
        var i    = 1;
        $.each(actions, function(index, action){
            i++;
            var html = [];
            if(self.options.page == "view" && actionData.hasOwnProperty('canUpdate'))
            {
                if(actionData.canUpdate.hasOwnProperty(index) && actionData.canUpdate[index] == true)
                {
                    html.push("<tr>");
                } else {
                    html.push("<tr class='view-notowner'>");
                }
            } else {
                html.push("<tr>");
            }
            $.each( action, function( key , val){
                if(key == 'action_progress')
                {
                    html.push("<td style='text-align: right;'>"+val+"</td>");
                } else {
                    html.push("<td>"+val+"</td>");
                }
            })
/*            if(actionData.hasOwnProperty('assessments'))
            {
                html.push("<td>"+actionData.assessments[index]['rating']+"</td>");
                html.push("<td>"+actionData.assessments[index]['comment']+"</td>");
            } else {
                html.push("<td></td>");
                html.push("<td></td>");
            }
            if(actionData['setup'][index]['use_corrective_action'] == 1)
            {
                html.push("<td>"+actionData.assessments[index]['corrective_action']+"</td>");
            } else {
                html.push("<td></td>");
            }
            if(actionData['setup'][index]['are_corrective_action_owner_identified'] == 1)
            {
                html.push("<td>"+actionData.assessments[index]['action_owner']+"</td>");
            } else {
                html.push("<td></td>");
            }*/

            html.push(self._displayActionAssessUpdateBtn(actionData, index));


            html.push("</tr>");
            $("#"+self.options.tableId).append( html.join(' ') ).find('.remove_attach').remove();
            $("#"+self.options.tableId).find("#result_message").remove();
        });
    },

    _displayActionAssessUpdateBtn  : function(actionData, action_id)
    {
        var html = [];
        var self = this;
        if(self.options.updateAssessAction)
        {
            html.push("<td>&nbsp;");
            html.push("<input type='button' value='Update Question' id='update_action_"+action_id+"' name='update_action_"+action_id+"' />");
            html.push("</td>");
            $("#update_action_"+action_id).live("click", function(){
                self._updateAction( action_id, actionData );
                return false;
            });
            return html.join(' ');
        }
        return '';
    },

    _updateAction        : function(action_id, actionData)
    {
        var self = this;
        if($("#action_update_dialog").length > 0)
        {
            $("#action_update_dialog").remove();
        }
        var actionDetail = actionData['action_detail'][action_id];
        var question = actionData['actions'][action_id];

        var html = [];
        html.push("<table>");
            html.push("<tr>");
                html.push("<td colspan='2'>");
                  html.push( "<p>"+question.action_name+"</p>" );
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Rating:</th>");
                html.push("<td id='rating_list'>");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Comment:</th>");
                html.push("<td>");
                    html.push("<textarea id='comment' name='comment' cols='50' rows='8'>"+question.rating_comment+"</textarea>");
                html.push("</td>");
            html.push("</tr>");
        if(actionData['setup'][action_id]['are_corrective_action_owner_identified'] == 1)
        {
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Corrective Action:</th>");
                html.push("<td>");
                    html.push("<textarea id='corrective_action' name='corrective_action' cols='50' rows='8'>"+question.corrective_action+"</textarea>");
                html.push("</td>");
            html.push("</tr>");
        }
        if(actionData['setup'][action_id]['use_corrective_action'] == 1)
        {
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Action Owner:</th>");
                html.push("<td>");
                  html.push("<select name='action_owner' id='action_owner'>");
                    html.push("<option value=''>--please select--</option>");
                    $.each(actionData['owners'], function(owner_id, owner){
                        if(owner_id == actionDetail.asessment_action_owner)
                        {
                            html.push("<option value='"+owner_id+"' selected='selected'>"+owner+"</option>");
                        } else {
                            html.push("<option value='"+owner_id+"'>"+owner+"</option>");
                        }
                    });
                    html.push("</select>");
                html.push("</td>");
            html.push("</tr>");
        }
        html.push("<tr>");
                html.push("<td colspan='2'>");
                    html.push("<p id='message'></p>")
                html.push("</td>");
            html.push("</tr>");
        html.push("<table>");

        $("<div />",{id:"action_update_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Update Question Q"+action_id,
                width       : 'auto',
                buttons     : {
                    "Update"      : function()
                    {
                        $('#message').html("Updating action ....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/actionassessmentcontroller.php?action=saveActionAssessment", {
                            action_id         : action_id,
                            comment           : $("#comment").val(),
                            corrective_action : $("#corrective_action").val(),
                            action_owner      : $("#action_owner").val(),
                            rating_id         : $("input[name='rating']:checked").val()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getActions();
                            }
                        },"json");
                        $("#action_update_dialog").dialog("destroy").remove();
                    } ,
                    " Update Assessment"      : function()
                    {
                        $('#message').html("Updating action ....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/actionassessmentcontroller.php?action=updateActionAssessment", {
                            action_id         : action_id,
                            comment           : $("#comment").val(),
                            corrective_action : $("#corrective_action").val(),
                            action_owner      : $("#action_owner").val(),
                            rating_id         : $("input[name='rating']:checked").val(),
                            id                : actionDetail.assessment_id
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                if(response.updated)
                                {
                                    jsDisplayResult("ok", "ok", response.text);
                                    self._getActions();
                                } else {
                                    jsDisplayResult("info", "info", response.text);
                                }
                            }
                        },"json");
                        $("#action_update_dialog").dialog("destroy").remove();
                    } ,
                    "Cancel"        : function()
                    {
                        $("#action_update_dialog").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#action_update_dialog").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn     = btns[0];
                    var updateBtn   = btns[1];
                    var cancelBtn   = btns[2];

                    if(actionDetail.assessment_id > 0)
                    {
                        $(saveBtn).css({"display":"none"});
                        //$(updateBtn).css({"display":"none"});
                    } else {
                        //$(saveBtn).css({"display":"none"});
                        $(updateBtn).css({"display":"none"});
                    }


                    $(saveBtn).css({"color":"#090"});
                    $(updateBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
                    $("#rating_list").html("").empty();
                    $.getJSON("../controllers/ratingscalecontroller.php?action=getActionRatingTypeList", {
                        action_id : action_id
                    }, function( ratingScales ) {
                        var html = [];
                        $.each( ratingScales, function(rating_scale_id, rating_scale){
                            if(actionDetail.rating_id == rating_scale_id)
                            {
                                html.push("<input type='radio' name='rating' id='rating_"+rating_scale_id+"' value='"+rating_scale_id+"' class='rating' checked='checked' />&nbsp;"+rating_scale+"&nbsp;&nbsp;");
                            } else {
                                html.push("<input type='radio' name='rating' id='rating_"+rating_scale_id+"' value='"+rating_scale_id+"' class='rating' />&nbsp;"+rating_scale+"&nbsp;&nbsp;");
                            }
                        });
                        $("#rating_list").html( html.join(' ') );
                    })
                }
            });

    },

    _displayHeaders     : function(headers)
    {
        var self = this;
        var html = [];
        html.push("<tr class='"+self.options.thClass+"'>");
        $.each( headers , function( index, head ){
            html.push("<th class='"+self.options.thClass+"'>"+head+"</th>");
        });
/*        html.push("<th class='"+self.options.thClass+"'>Rating</th>");
        html.push("<th class='"+self.options.thClass+"'>Comment</th>");
        html.push("<th class='"+self.options.thClass+"'>Corrective Action</th>");
        html.push("<th class='"+self.options.thClass+"'>Action Owner</th>");*/
        html.push("<th class='"+self.options.thClass+"'></th>");
        html.push("</tr>");
        $("#"+self.options.tableId).append(html.join(' '));
    },

    _displayPaging	: function(columns)
    {
        var self  = this;
        var html  = [];
        var pages;
        if( self.options.total%self.options.limit > 0){
            pages   = Math.ceil(self.options.total/self.options.limit);
        } else {
            pages   = Math.floor(self.options.total/self.options.limit);
        }
        var deliverable_id = self.options.deliverableId;
        html.push("<tr>");
        html.push("<td colspan='"+(columns + 5)+"' class='noborder'>");
        html.push("<input type='button' value=' |< ' id='action_first_"+deliverable_id+"' name='first' />");
        html.push("<input type='button' value=' < '  id='action_previous_"+deliverable_id+"' name='previous' />");
        if(pages != 0)
        {
            html.push("Page <select id='action_select_page' name='action_select_page'>");
            for(var p=1; p<=pages; p++)
            {
                html.push("<option value='"+p+"' "+(self.options.current == p ? "selected='selected'" : "")+">"+p+"</option>");
            }
            html.push("</select>");
        } else {
            html.push("Page <select id='action_select_page' name='action_select_page' disabled='disabled'>");
            html.push("</select>");
        }
        html.push(" of "+(pages == 0 || isNaN(pages) ? 0 : pages)+" pages");
        html.push("<input type='button' value=' > ' id='action_next_"+deliverable_id+"' name='next' />");
        html.push("<input type='button' value=' >| ' id='action_last_"+deliverable_id+"' name='last' />");
        if(self.options.assuranceAction)
        {
            html.push("&nbsp;");
        }
        if(self.options.updateAction)
        {
            html.push("&nbsp;");
        }
        if(self.options.editAction)
        {
            html.push("&nbsp;");
        }
        if(self.options.addAction)
        {
            html.push("&nbsp;");
        }
        if(self.options.viewAction)
        {
            html.push("&nbsp;");
        }
        html.push("<span style='float:right;'>");
        html.push("<b>Quick Search for Question:</b> Q <input type='text' name='a_id' id='a_id' value='' />");
        html.push("&nbsp;&nbsp;&nbsp;");
        html.push("<input type='button' name='action_search' id='action_search' value='Go' />");
        html.push("</span>");
        html.push("</td>");
        html.push("</tr>");
        $("#"+self.options.tableId).append(html.join(' '));


        $("#action_select_page").live("change", function(){
            if($(this).val() !== "" && parseFloat($(this).val())>0)
            {
                self.options.start   = parseFloat($(this).val() -1) * parseFloat(self.options.limit);
                self.options.current = parseFloat($(this).val());
                self._getActions();
            }
        });
        $("#action_search").live("click", function(e){
            var action_id = $("#a_id").val();
            if(action_id != "")
            {
                self.options.actionId = $("#d_id").val();
                self._getActions();
            } else {
                $("#a_id").addClass('ui-state-error');
                $("#a_id").focus().select();
            }
            e.preventDefault();
        });

        if(self.options.current < 2)
        {
            $("#action_first_"+deliverable_id).attr('disabled', 'disabled');
            $("#action_previous_"+deliverable_id).attr('disabled', 'disabled');
        }
        if((self.options.current == pages || pages == 0))
        {
            $("#action_next_"+deliverable_id).attr('disabled', 'disabled');
            $("#action_last_"+deliverable_id).attr('disabled', 'disabled');
        }
        $("#action_next_"+deliverable_id).bind("click", function(evt){
            self._getNext( self );
        });
        $("#action_last_"+deliverable_id).bind("click",  function(evt){
            self._getLast( self );
        });
        $("#action_previous_"+deliverable_id).bind("click",  function(evt){
            self._getPrevious( self );
        });
        $("#action_first_"+deliverable_id).bind("click",  function(evt){
            self._getFirst( self );
        });
    } ,

    _getNext  			: function(self)
    {
        self.options.current = (self.options.current + 1);
        self.options.start   = (self.options.current-1) * self.options.limit;
        this._getActions();
    },

    _getLast  			: function( self )
    {
        self.options.current =  Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
        self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );
        this._getActions();
    },
    _getPrevious    	: function( self )
    {
        self.options.current  = parseFloat(self.options.current) - 1;
        self.options.start 	= (self.options.current-1) * self.options.limit;
        this._getActions();
    },
    _getFirst  			: function( self )
    {
        self.options.current  = 1;
        self.options.start 	= 0;
        this._getActions();
    } ,

    _displayScoreCardSetupSummary  : function( score_card )
    {
        var self = this;
        var html = [];
        html.push("<table class='noborder' width='100%'>");
            html.push("<tr>");
                html.push("<th valign='top' style='text-align: left;'>Scorecard Reference:</th>");
                html.push("<td valign='top'>"+score_card.setup_reference+"</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th valign='top' style='text-align: left;'>Scorecard Instruction to user:</th>");
                html.push("<td valign='top'>"+score_card.setup_description+"</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th valign='top' style='text-align: left;'>Link To:</th>");
                html.push("<td valign='top'>"+score_card.score_card_linked+"</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th valign='top' style='text-align: left;'>Start Date:</th>");
                html.push("<td valign='top'>"+score_card.start_date+"</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th valign='top' style='text-align: left;'>End Date:</th>");
                html.push("<td valign='top'>"+score_card.end_date+"</td>");
            html.push("</tr>");
        html.push("</table>");
        $("#score-card-summary").html( html.join('') );
    } ,

    _confirmCompleteScoreCard       : function(scoreCard)
    {
        var self        = this;
        var confirmHtml = [];
        confirmHtml.push("<p class='ui-state'>");
            confirmHtml.push("<span>I, "+scoreCard.logged_user+" hereby;</span>");
            confirmHtml.push("<span></span>");
            confirmHtml.push("<span>");
                confirmHtml.push("<ul style='margin-left:10px'>");
                    confirmHtml.push("<li>Confirm that I have completed the Update of all the questions assigned to me on the "+scoreCard.score_card_name+" SC"+scoreCard.id  +". ;</li>");
                    confirmHtml.push("<li>Confirm that I have applied due care in completing the assigned questions and I acknowledge that I am personally accountable and responsible for the information captured.</li>");
                confirmHtml.push("</ul>");
            confirmHtml.push("</span>");
        confirmHtml.push("</p>");
        confirmHtml.push("<table width='100%' class='noborder'>");
            confirmHtml.push("<tr>");
                confirmHtml.push("<th class='noborder'>Overall Comment</th>");
                confirmHtml.push("<td class='noborder'>");
                  confirmHtml.push("<textarea name='overall_comment' id='overall_comment' rows='10' cols='80'></textarea>");
                confirmHtml.push("</td>");
            confirmHtml.push("</tr>");
        confirmHtml.push("</table>");

        if($("#confirm_complete_score_card_dialog_"+self.options.scoreCardId).length > 0)
        {
            $("#confirm_complete_score_card_dialog_"+self.options.scoreCardId).remove();
        }

        $("<div />",{id:"confirm_complete_score_card_dialog_"+self.options.scoreCardId, html: confirmHtml.join(' ') })
            .append($("<div />",{id:"message"}).css({"padding":"5px"}))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Scorecard complete",
                width       : 'auto',
                buttons     : {
                    "Confirm"   : function()
                    {
                        $("#message").addClass("ui-state-info").html("confirming scorecard ... <img src='../images/loaderA32.gif' />");
                        $.getJSON("../controllers/scorecardsetupcontroller.php?action=complete",{
                            score_card_id     : self.options.scoreCardId,
                            overall_comment   : $("#overall_comment").val()
                        }, function(response) {
                            if(response.error)
                            {
                                $("#message").html(response.text).addClass('ui-state-error');
                            } else {
                                $("#message").html(response.text).addClass('ui-state-ok');
                                setTimeout(function(){
                                    document.location.reload();
                                }, 1000)
                            }
                        });
                    },
                    "Cancel"    : function()
                    {
                        $("#confirm_complete_score_card_dialog_"+self.options.scoreCardId).dialog("destroy").remove()
                    }
                } ,
                "close"     : function(event, ui)
                {
                    $("#confirm_complete_score_card_dialog_"+self.options.scoreCardId).dialog("destroy").remove()
                },
                "open"      : function(event, ui)
                {
                    var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                    var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
                    var saveButton = buttons[0];
                    $(saveButton).css({"color":"#090"});
                }
            });
    } ,

    _unconfirmCompleteScoreCard    : function()
    {
        var self        = this;
        var confirmHtml = [];
/*        confirmHtml.push("<p class='ui-state'>");
        confirmHtml.push("<span>I, "+scoreCard.logged_user+" hereby;</span>");
        confirmHtml.push("<span></span>");
        confirmHtml.push("<span>");
        confirmHtml.push("<ul style='margin-left:10px'>");
        confirmHtml.push("<li>Confirm that I have completed the Update of all the questions assigned to me on the "+scoreCard.setup_description+" SC"+scoreCard.id  +". ;</li>");
        confirmHtml.push("<li>Confirm that I have applied due care in completing the assigned questions and i acknowledge that i am personally accountable and responsible for the information captured.</li>");
        confirmHtml.push("</ul>");
        confirmHtml.push("</span>");
        confirmHtml.push("</p>");
        confirmHtml.push("<table width='100%' class='noborder'>");
        confirmHtml.push("<tr>");
        confirmHtml.push("<th class='noborder'>Overall Comment</th>");
        confirmHtml.push("<td class='noborder'>");
        confirmHtml.push("<textarea name='overall_comment' id='overall_comment' rows='10' cols='80'></textarea>");
        confirmHtml.push("</td>");
        confirmHtml.push("</tr>");
        confirmHtml.push("</table>");*/
        confirmHtml.push("<p class='ui-state'>Unconfirming you score card</p>");
        if($("#confirm_complete_score_card_dialog_"+self.options.scoreCardId).length > 0)
        {
            $("#confirm_complete_score_card_dialog_"+self.options.scoreCardId).remove();
        }

        $("<div />",{id:"confirm_complete_score_card_dialog_"+self.options.scoreCardId, html: confirmHtml.join(' ') })
            .append($("<div />",{id:"message"}).css({"padding":"5px"}))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Unconfirm Scorecard",
                width       : 'auto',
                buttons     : {
                    "Unconfirm"   : function()
                    {
                        $("#message").addClass("ui-state-info").html("unconfirming scorecard ... <img src='../images/loaderA32.gif' />");
                        $.getJSON("../controllers/scorecardsetupcontroller.php?action=unconfirm",{
                            score_card_id  : self.options.scoreCardId,
                            owner_id       : self.options.questionOwnerId
                        }, function(response) {
                            if(response.error)
                            {
                                $("#message").html(response.text).addClass('ui-state-error');
                            } else {
                                $("#message").html(response.text).addClass('ui-state-ok');
                                setTimeout(function(){
                                    document.location.reload();
                                }, 1000);
                            }
                        });
                    },
                    "Cancel"    : function()
                    {
                        $("#confirm_complete_score_card_dialog_"+self.options.scoreCardId).dialog("destroy").remove()
                    }
                } ,
                "close"     : function(event, ui)
                {
                    $("#confirm_complete_score_card_dialog_"+self.options.scoreCardId).dialog("destroy").remove()
                },
                "open"      : function(event, ui)
                {
                    var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                    var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
                    var saveButton = buttons[0];
                    $(saveButton).css({"color":"#090"});
                }
            });
    }


})