// JavaScript Document
$(function(){

    $("#sortable tbody").sortable().disableSelection();

    $("#save_changes").live("click", function(){

        jsDisplayResult("info", "info", "Updating  ...  <img src='../images/loaderA32.gif' />");
        $.post("../controllers/namingcontroller.php?action=updateOrder",
            {
                data 		: $("#columns_sortable").serialize()
            },function( response ) {
                if( response.error )
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    jsDisplayResult("ok", "ok", response.text );
                }
            },"json");

        return false;
    });
});
