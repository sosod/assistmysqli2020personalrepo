$(function(){

});


function updateNaming(element)
{
    console.log( element )
    jsDisplayResult("info", "info", "Updating  ...  <img src='../images/loaderA32.gif' />");
    $.post("../controllers/namingcontroller.php?action=update", {
        client_terminology : $("#client_terminology_"+element.id).val(),
        id	 			   : element.id
    }, function( response ) {
        if( response.error ){
            jsDisplayResult("error", "error", response.text);
        } else {
            if(response.updated)
            {
                jsDisplayResult("ok", "ok", response.text );
            } else {
                jsDisplayResult("info", "info", response.text );
            }
        }
    },"json");
    return false;
}