$.widget('contract.template', {

    options     : {
        tableRef    : 'template_table_'+(Math.floor(Math.random(563) * 313)),
        url 	    : "../controllers/contracttemplatecontroller.php?action=getTemplateDetail",
        contract_id : 0,
        template_id : 0,
        autoLoad    : false,
        page        : '',
        section     : '',
        editAction  : true
    } ,


    _init        : function()
    {
        if(this.options.autoLoad)
        {
            this._getTemplateDetail();
        }
    },

    _create       : function()
    {
        var self = this;
        var html = [];
        html.push("<form id='template_setting_form' name='template_setting_form'><table id='"+self.options.tableRef+"_deliverable' width='100%'></table></form>");
        $(this.element).append(html.join(' '));
        self._getTemplateDetail();
    },

    _getTemplateDetail      : function()
    {
        var self = this;
        $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
        );
        $.getJSON(self.options.url,{
            section		      : self.options.section,
            page              : self.options.page,
            contract_id       : self.options.contract_id,
            template_id       : self.options.template_id
        },function(templateData) {
            $("#loadingDiv").remove();
            $(".contract_message").remove();
            $(".confirm_button").remove();
            $(".confirm-data").remove();
            if(templateData.hasOwnProperty('category_deliverable'))
            {
                var categoryHtml = [];
                if(templateData['contract_template'].hasOwnProperty('template_id'))
                {
                    categoryHtml.push("<tr class='confirm-data'>");
                        categoryHtml.push("<td colspan='12'>"+templateData.contract_template['template_name']+"</td>");
                    categoryHtml.push("</tr>");
                }
                categoryHtml.push( self._displayDeliverableHeader(templateData.columns, templateData.total_columns, templateData.contract_detail) );
                var total_d_weight = 0;
                var total_o_weight = 0;
                var total_q_weight = 0;
                if($(".weights").length > 0)
                {
                    $(".weights").remove();
                }
                categoryHtml.push("<tr class='weights'>");
                var colspan   = 9;
                var _colspan  = 9;
                if(templateData['contract_detail']['is_assessed_quantitative'] == 1)
                {
                    colspan += 1;
                    categoryHtml.push("<th class='th2'>Delivered</th>");
                }
                if(templateData['contract_detail']['is_assessed_qualitative'] == 1)
                {
                    colspan += 1;
                    categoryHtml.push("<th class='th2'>Quality</th>")

                }
                if(templateData['contract_detail']['is_assessed_other'] == 1)
                {
                    colspan += 1;
                    categoryHtml.push("<th class='th2'>Other</th>");
                }
                categoryHtml.push("</tr>");
                $.each(templateData.category_deliverable, function(category_id, deliverables){
                    categoryHtml.push("<tr class='confirm-data'>");
                        categoryHtml.push("<td  class='noborder' colspan='"+colspan+"'><b>"+templateData.categories[category_id]+"</b></td>");
                    categoryHtml.push("</tr>");
                    categoryHtml.push( self._displayDeliverables(deliverables, templateData.category_weights[category_id], templateData.total_actions, templateData, templateData.contract_detail) );
                    total_d_weight += parseInt(templateData.category_weights[category_id]['delivered_weight']);
                    total_o_weight += parseInt(templateData.category_weights[category_id]['other_weight']);
                    total_q_weight += parseInt(templateData.category_weights[category_id]['quality_weight']);
                });
                 categoryHtml.push("<tr style='background-color:#d3d3d3;' class='confirm-data'>");
                 categoryHtml.push("<td colspan='7'>&nbsp;</td>");
                 if(templateData['contract_detail']['is_assessed_quantitative'] == 1)
                 {
                     categoryHtml.push("<td style='text-align: right;'><b>"+total_d_weight+"</b></td>");
                 }
                 if(templateData['contract_detail']['is_assessed_qualitative'] == 1)
                 {
                     categoryHtml.push("<td style='text-align: right;'><b>"+total_q_weight+"</b></td>");
                 }
                 if(templateData['contract_detail']['is_assessed_other'] == 1)
                 {
                     categoryHtml.push("<td style='text-align: right;'><b>"+total_o_weight+"</b></td>");
                 }
                 categoryHtml.push("<td>&nbsp;</td>");
                 categoryHtml.push("<td>&nbsp;</td>");
                 categoryHtml.push("</tr>");
                if(templateData.hasOwnProperty('templates') && templateData['contract_template'].hasOwnProperty('template_id'))
                {
                  categoryHtml.push("<tr class='confirm-data'>");
                    categoryHtml.push("<td colspan='12'>");
                        categoryHtml.push("<input type='button' name='update_template' id='update_template' value='Update Template' />");
                        categoryHtml.push("<input type='hidden' name='template_id' id='template_id' value='"+templateData['contract_template']['template_id']+"' />");
                    categoryHtml.push("</td>");
                  categoryHtml.push("</tr>");
                } else {
                  categoryHtml.push("<tr class='confirm-data'>");
                    categoryHtml.push("<td colspan='12'><input type='button' name='save_template' id='save_template' value='Save as Template' /></td>");
                  categoryHtml.push("</tr>");
                }
                $("#"+self.options.tableRef+"_deliverable").append( categoryHtml.join(' ')).css({'text-align':'left'});

                self._displayDeliverableActions(templateData);

                $("#save_template").live('click', function(e){
                    self._saveTemplate();
                    e.preventDefault();
                });

                $("#update_template").live('click', function(e){
                    self._updateTemplate();
                    e.preventDefault();
                });
            }
        });
    } ,

    _saveTemplate               : function()
    {
        var self = this;
        var contract_id = self.options.contract_id;
        $("#message").addClass("ui-state-info").html();
        jsDisplayResult("info", "info", "Saving template settings ... <img src='../images/loaderA32.gif' />");
        $.getJSON("../controllers/contracttemplatecontroller.php?action=saveTemplateSetting",{
            template_setting_data    : $("#template_setting_form").serialize(),
            contract_id              : self.options.contract_id,
            template_id              : self.options.template_id
        }, function(response) {
            if(response.error)
            {
              jsDisplayResult("error", "error", response.text);
              //$("#message").html(response.text).addClass('ui-state-error');
            } else {
                jsDisplayResult("ok", "ok", response.text);
                $("#message").html(response.text).addClass('ui-state-ok');
                setTimeout(function(){
                    document.location.reload();
                }, 1000)
            }
        });

/*        var html = [];
        if($("#template_setting_dialog_"+contract_id).length > 0)
        {
            $("#template_setting_dialog_"+contract_id).remove();
        }
        html.push("<table>");
          html.push("<tr>");
            html.push("<th>Template Name</th>");
            html.push("<td><input name='template_name' id='template_name' value='' /></th>");
          html.push("</tr>");
          html.push("<tr>");
            html.push("<td colspan='2'><span id='message' style='padding:5px;'></span></th>");
          html.push("</tr>");
        html.push("</table>");
        $("<div />",{id:"template_setting_dialog_"+contract_id}).append(html.join(' ')).dialog({
            autoOpen    : true,
            modal       : true,
            position    : "center, top",
            width       : "auto",
            title       : "Assessment of Contract",
            buttons     : {
                "Save Template"   : function()
                {
                    if($("#template_name").val() == '')
                    {
                       $("#message").html("Please enter the template name").addClass('ui-state-error').css({'padding':'5px'});

                        return false;
                    } else {
                        $("#message").addClass("ui-state-info").html("Saving template settings ... <img src='../images/loaderA32.gif' />");
                        $.getJSON("../controllers/contracttemplatecontroller.php?action=saveTemplateSetting",{
                            template_setting_data    : $("#template_setting_form").serialize(),
                            contract_id              : self.options.contract_id,
                            template_name            : $("#template_name").val()
                        }, function(response) {
                            if(response.error)
                            {
                                $("#message").html(response.text).addClass('ui-state-error');
                            } else {
                                $("#message").html(response.text).addClass('ui-state-ok');
                                setTimeout(function(){
                                    document.location.reload();
                                }, 1000)
                            }
                        });
                    }
                },
                "Cancel"    : function()
                {
                    $("#template_setting_dialog_"+contract_id).dialog("destroy").remove()
                }
            } ,
            "close"     : function(event, ui)
            {
                $("#template_setting_dialog_"+contract_id).dialog("destroy").remove()
            },
            "open"      : function(event, ui)
            {
                var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
                var saveButton = buttons[0];
                $(saveButton).css({"color":"#090"});
            }
        });*/
    },

    _updateTemplate               : function()
    {
        var self = this;
        var contract_id = self.options.contract_id;
        var html = [];
        if($("#update_template_setting_dialog_"+contract_id).length > 0)
        {
            $("#update_template_setting_dialog_"+contract_id).remove();
        }
        jsDisplayResult("info", "info", "Updating template settings . . . <img src='../images/loaderA32.gif' />");
        $.getJSON("../controllers/contracttemplatecontroller.php?action=updateTemplateSetting",{
            template_setting_data    : $("#template_setting_form").serialize(),
            contract_id              : self.options.contract_id,
            template_id              : $("#template_id").val()
        }, function(response) {
            if(response.error)
            {
              jsDisplayResult("error", "error", response.text);
            } else {
              if(response.updated)
              {
                jsDisplayResult("ok", "ok", response.text);
              } else {
                jsDisplayResult("info", "info", response.text);
              }
            }
        });
/*        html.push("<table>");
        html.push("<tr>");
        html.push("<th>Template Name</th>");
        html.push("<td><input name='template_name' id='template_name' value='' /></th>");
        html.push("</tr>");
        html.push("<tr>");
        html.push("<td colspan='2'><span id='message' style='padding:5px;'></span></th>");
        html.push("</tr>");
        html.push("</table>");
        $("<div />",{id:"template_setting_dialog_"+contract_id}).append(html.join(' ')).dialog({
            autoOpen    : true,
            modal       : true,
            position    : "center, top",
            width       : "auto",
            title       : "Assessment of Contract",
            buttons     : {
                "Save Template"   : function()
                {
                    if($("#template_name").val() == '')
                    {
                        $("#message").html("Please enter the template name").addClass('ui-state-error').css({'padding':'5px'});

                        return false;
                    } else {
                        $("#message").addClass("ui-state-info").html("Saving template settings ... <img src='../images/loaderA32.gif' />");
                        $.getJSON("../controllers/templatesettingcontroller.php?action=saveTemplateSetting",{
                            template_setting_data    : $("#template_setting_form").serialize(),
                            contract_id              : self.options.contract_id,
                            template_name            : $("#template_name").val()
                        }, function(response) {
                            if(response.error)
                            {
                                $("#message").html(response.text).addClass('ui-state-error');
                            } else {
                                $("#message").html(response.text).addClass('ui-state-ok');
                                setTimeout(function(){
                                    document.location.reload();
                                }, 1000)
                            }
                        });
                    }
                },
                "Cancel"    : function()
                {
                    $("#template_setting_dialog_"+contract_id).dialog("destroy").remove()
                }
            } ,
            "close"     : function(event, ui)
            {
                $("#template_setting_dialog_"+contract_id).dialog("destroy").remove()
            },
            "open"      : function(event, ui)
            {
                var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
                var saveButton = buttons[0];
                $(saveButton).css({"color":"#090"});
            }
        });*/
    },

    _displayContract            : function(contract)
    {
        var self = this;
        var contractHtml = [];
        $.each(contract, function(column, contract) {
            contractHtml.push("<tr class='confirm-data'>")
            contractHtml.push("<th style='text-align: left;'>"+contract.header+"</th>");
            contractHtml.push("<td>"+contract.value+"</td>");
            contractHtml.push("</tr>");
        });
        $("#"+self.options.tableRef+"_contract").append( contractHtml.join(' ')).css({'text-align':'left'});
    } ,


    _displayDeliverableHeader       : function(headers, total_columns, contract_detail)
    {
        var self = this;
        var html = [];
        html.push("<tr class='confirm-data'>");
            html.push("<th rowspan='2'>&nbsp;</th>");
            html.push("<th rowspan='2'>Ref</th>");
            html.push("<th rowspan='2'>Description</th>");
            html.push("<th rowspan='2'>Owner</th>");
            html.push("<th rowspan='2'>Status</th>");
            html.push("<th rowspan='2'>Progress</th>");
            html.push("<th rowspan='2'>Number Of Actions</th>");
         var colspan_weight = 0;
         if(contract_detail['is_assessed_quantitative'] == 1)
         {
             colspan_weight += 1;;
         }
         if(contract_detail['is_assessed_qualitative'] == 1)
         {
             colspan_weight += 1;
         }
         if(contract_detail['is_assessed_other'] == 1)
         {
             colspan_weight += 1;
         }
         if(colspan_weight > 0)
         {
            html.push("<th colspan='"+colspan_weight+"'>Weights</th>");
         }
        if(self.options.updateDeliverable)
        {
            html.push("<th rowspan='2'>&nbsp;</th>");
        }
        if(self.options.editDeliverable)
        {
            html.push("<th rowspan='2'>&nbsp;</th>");
        }
        if(self.options.addAction)
        {
            html.push("<th rowspan='2'>&nbsp;</th>");
        }
        if(self.options.viewDeliverable)
        {
            html.push("<th rowspan='2'>&nbsp;</th>");
        }
        html.push("<th rowspan='2'>Required</th>");
        html.push("<th rowspan='2'>Editable</th>");
        html.push("</tr>");
        return html.join(' ');
    },

    _displayDeliverables        : function(deliverable_data, category_weights, action_data, templateData, contract_detail)
    {
        var self = this;
        var deliverableHtml = [];
        $.each(deliverable_data.deliverables, function(deliverable_id, deliverable){
            deliverableHtml.push("<tr class='confirm-data'>");
            if(templateData['settings'][deliverable_id]['has_sub'] == 0)
            {
                deliverableHtml.push("<td>");
                    deliverableHtml.push("<a href='#' id='deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id+"' class='deliverable_more_less'>");
                        deliverableHtml.push("<span class='ui-icon ui-icon-plus'></span>");
                    deliverableHtml.push("</a>");
                deliverableHtml.push("</td>");
            } else {
                deliverableHtml.push("<td></td>");
            }
            deliverableHtml.push("<td>"+deliverable.deliverable_id+"</td>");
            deliverableHtml.push("<td>"+deliverable.description+"</td>");
            deliverableHtml.push("<td></td>");
            deliverableHtml.push("<td>New</td>");
            deliverableHtml.push("<td style='text-align: right;'>0.00%</td>");
            if(action_data.hasOwnProperty(deliverable_id))
            {
                deliverableHtml.push("<td style='text-align: right;'>"+action_data[deliverable_id]+"</td>");
            } else {
                deliverableHtml.push("<td style='text-align: right;'>0</td>");
            }
            deliverableHtml.push( self._loadWeights(templateData, deliverable_id, templateData.contract_detail ) );
            deliverableHtml.push("</tr>");
            if(templateData.actions.hasOwnProperty(deliverable_id))
            {
                $.each(templateData.actions[deliverable_id], function(action_index, action_id){
                    deliverableHtml.push("<tr id='deliverable_actions_"+self.options.tableRef+"_"+deliverable_id+"' class='deliverable_actions confirm-data' style='display:none;'>");
                      deliverableHtml.push("<td id='show_deliverable_actions_"+self.options.tableRef+"_"+deliverable_id+"' colspan='"+(parseInt(templateData.total_columns)+3)+"'></td>");
                    deliverableHtml.push("</tr>");
                });
                //$("#show_deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).action({deliverable_id:deliverable_id, deliverableRef:'del_'+deliverable_id , section:self.options.section, thClass:'th2', editAction: self.options.editAction});
            }
            if(deliverable_data.hasOwnProperty('sub_deliverable'))
            {
                if(deliverable_data['sub_deliverable'].hasOwnProperty(deliverable_id))
                {
                    $.each(deliverable_data['sub_deliverable'][deliverable_id], function(sub_deliverable_id, sub_deliverable){
                        deliverableHtml.push("<tr class='view-notowner confirm-data'>");
                            deliverableHtml.push("<td>");
                                deliverableHtml.push("<a href='#' id='sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id+"' class='sub_deliverable_more_less'>");
                                    deliverableHtml.push("<span class='ui-icon ui-icon-plus'></span>");
                                deliverableHtml.push("</a>");
                            deliverableHtml.push("</td>");
                            deliverableHtml.push("<td>"+sub_deliverable.deliverable_id+"</td>");
                            deliverableHtml.push("<td>"+sub_deliverable.description+"</td>");
                            deliverableHtml.push("<td></td>");
                            deliverableHtml.push("<td>New</td>");
                            deliverableHtml.push("<td style='text-align: right;'>0.00%</td>");
                            if(action_data.hasOwnProperty(sub_deliverable_id))
                            {
                                deliverableHtml.push("<td style='text-align: right;'>"+action_data[sub_deliverable_id]+"</td>");
                            }
                            deliverableHtml.push( self._loadWeights(templateData, sub_deliverable_id, templateData.contract_detail ) );
                        deliverableHtml.push("</tr>");
                        if(templateData.actions.hasOwnProperty(sub_deliverable_id))
                        {
                            $.each(templateData.actions[sub_deliverable_id], function(action_index, action_id){
                                deliverableHtml.push("<tr id='sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id+"' class='deliverable_actions confirm-data' style='display:none;'>");
                                    deliverableHtml.push("<td id='show_sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id+"' colspan='"+(parseInt(templateData.total_columns) + 3)+"'></td>");
                                deliverableHtml.push("</tr>");
                            });
                            //$("#show_sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id).action({deliverable_id:sub_deliverable_id, deliverableRef:'sub_'+sub_deliverable_id , section:self.options.section, thClass:'th2', editAction: self.options.editAction});
                        }
                        $("#sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id).live("click", function(e){
                            if( $("#sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id).is(":hidden") )
                            {
                                $("#sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id+" span").removeClass("ui-icon-closethick").addClass("ui-icon-minus");
                                $("#sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id).show();
                            } else {
                                $("#sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");
                                $("#sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id).hide();
                            }
                            e.stopImmediatePropagation();
                        });

                    });
                }
            }
            $("#deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id).live("click", function(e){
                if( $("#deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).is(":hidden") )
                {
                    $("#deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id+" span").removeClass("ui-icon-closethick").addClass("ui-icon-minus");
                    $("#deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).show();
                } else {
                    $("#deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");
                    $("#deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).hide();
                }
                e.stopImmediatePropagation();
            });
        })
         deliverableHtml.push("<tr class='confirm-data'>");
             deliverableHtml.push("<td colspan='7'>&nbsp;</td>");
            if(contract_detail['is_assessed_quantitative'] == 1)
            {
               deliverableHtml.push("<td style='text-align: right;'>"+category_weights['delivered_weight']+"</td>");
            }
            if(contract_detail['is_assessed_qualitative'] == 1)
            {
               deliverableHtml.push("<td style='text-align: right;'>"+category_weights['quality_weight']+"</td>");
            }
            if(contract_detail['is_assessed_other'] == 1)
            {
               deliverableHtml.push("<td style='text-align: right;'>"+category_weights['other_weight']+"</td>");
            }
            deliverableHtml.push("<td>&nbsp;</td>");
            deliverableHtml.push("<td>&nbsp;</td>");
        deliverableHtml.push("</tr>");

        return deliverableHtml.join(' ');
    } ,

    _loadWeights  : function(templateData, deliverable_id, contract_detail)
    {
        var html = []
        if(contract_detail['is_assessed_quantitative'] == 1)
        {
            html.push("<td style='text-align: right;'>"+(templateData['deliverables'][deliverable_id]['delivered_weight'] == undefined ? 0 : templateData['deliverables'][deliverable_id]['delivered_weight'])+"</td>");
        }
        if(contract_detail['is_assessed_qualitative'] == 1)
        {
            html.push("<td style='text-align: right;'>"+(templateData['deliverables'][deliverable_id]['quality_weight'] == undefined ? 0 : templateData['deliverables'][deliverable_id]['quality_weight'])+"</td>");
        }
        if(contract_detail['is_assessed_other'] == 1)
        {
            html.push("<td style='text-align: right;'>"+(templateData['deliverables'][deliverable_id]['other_weight'] == undefined ? 0 : templateData['deliverables'][deliverable_id]['other_weight'])+"</td>");
        }
        html.push("<td>");
          html.push("<select name='required["+deliverable_id+"]' id='required_"+deliverable_id+"'>");
            if(templateData.hasOwnProperty('templates') && templateData['templates'].hasOwnProperty(deliverable_id))
            {
              html.push("<option value='1' "+(templateData['templates'][deliverable_id]['is_required'] == 1 ? 'selected="selected"' : '')+">Yes</option>");
              html.push("<option value='0' "+(templateData['templates'][deliverable_id]['is_required'] == 0 ? 'selected="selected"' : '')+">No</option>");
            } else {
              html.push("<option value='1'>Yes</option>");
              html.push("<option value='0'>No</option>");
            }
          html.push("</select>");
        html.push("</td>");
        html.push("<td>");
            html.push("<select name='editable["+deliverable_id+"]' id='editable_"+deliverable_id+"'>");
            if(templateData.hasOwnProperty('templates') && templateData['templates'].hasOwnProperty(deliverable_id))
            {
                html.push("<option value='1' "+(templateData['templates'][deliverable_id]['is_editable'] == 1 ? 'selected="selected"' : '')+">Yes</option>");
                html.push("<option value='0' "+(templateData['templates'][deliverable_id]['is_editable'] == 0 ? 'selected="selected"' : '')+">No</option>");
            } else {
                html.push("<option value='1'>Yes</option>");
                html.push("<option value='0' selected='selected'>No</option>");
            }
            html.push("</select>");
        html.push("</td>");
        $("#required_"+deliverable_id).live('change', function(){
            if($(this).val() == 1)
            {
                $("select#editable_"+deliverable_id).val(0);
            }
           return false;
        });
        return html.join('');
    },


    _displayDeliverableActions  : function(templateData)
    {
        var self = this;
        $.each(templateData.settings, function(deliverable_id, deliverable){
            if(deliverable.has_sub == 0)
            {
                if(templateData['actions'].hasOwnProperty(deliverable_id) && templateData['actions'][deliverable_id].length > 0)
                {
                    $("#show_deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).action({deliverable_id:deliverable_id, deliverableRef:'main_no_sub_'+deliverable_id, section:'template', thClass:'th2', editAction: false, financial_year:templateData.contract_detail.financial_year_id, allowFilter: false});
                }
            }

            if(deliverable.is_main == 0)
            {
                if(templateData['actions'].hasOwnProperty(deliverable_id) && templateData['actions'][deliverable_id].length > 0)
                {
                    $("#show_sub_deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).action({deliverable_id:deliverable_id, deliverableRef:'sub_'+deliverable_id, section:'template', thClass:'th2', editAction: false, financial_year:templateData.contract_detail.financial_year_id, allowFilter: false});
                }
            }
        });

    },


    _displaySummary             : function(key_measures, categories)
    {
        var self = this;
        var summaryHtml = []
        var total_weight = 0;
        summaryHtml.push("<tr class='confirm-data'>");
        summaryHtml.push("<th>Key Measures</th>");
        summaryHtml.push("<th>Weights</th>");
        summaryHtml.push("</tr>");
        $.each(key_measures, function(category, category_deliverable_summary){
            var total_category_weight = 0;
            summaryHtml.push("<tr class='confirm-data'>");
            summaryHtml.push("<td style='text-align: left;' colspan='2'><b>"+categories[category]+"</b></td>");
            summaryHtml.push("</tr>");
            $.each(category_deliverable_summary, function(deliverable_id, deliverable_summary){
                summaryHtml.push("<tr class='confirm-data'>");
                summaryHtml.push("<td style='text-align: left;'>"+deliverable_summary.deliverable_name+"</td>");
                summaryHtml.push("<td style='text-align: right;'>"+deliverable_summary.total_weight+"</td>");
                summaryHtml.push("</tr>");
                total_category_weight = parseInt(deliverable_summary.total_weight) + parseInt(total_category_weight);
            });
            total_weight          = parseInt(total_category_weight) + parseInt(total_weight);
            summaryHtml.push("<tr class='view-notowner confirm-data'>");
            summaryHtml.push("<td style='text-align: left;'>Total for "+categories[category]+"</td>");
            summaryHtml.push("<td style='text-align: right;' colspan='2'><b>"+total_category_weight+"</b></td>");
            summaryHtml.push("</tr>");
        });
        summaryHtml.push("<tr>");
        summaryHtml.push("<td style='text-align: left;'>Score</td>");
        summaryHtml.push("<td style='text-align: right;' colspan='2'><b>"+total_weight+"</b></td>");
        summaryHtml.push("</tr>");
        $("#"+self.options.tableRef+"_summary").append(summaryHtml.join(' ')).css({'text-align':'left'});
    },

    _deliverableAssessmentButton          : function(deliverableData, deliverable_id)
    {
        var self = this;
        if(self.options.editDeliverable)
        {
            var html = [];
            html.push("<td>&nbsp;");
            if(deliverableData.hasOwnProperty('canEdit'))
            {
                if(deliverableData.canEdit.hasOwnProperty(deliverable_id))
                {
                    if(deliverableData.canEdit[deliverable_id] == true)
                    {
                        html.push("<input type='button' value='Assess Deliverable' id='assess_deliverable_"+deliverable_id+"' id='assess_deliverable_"+deliverable_id+"' />");
                        $("#assess_deliverable_"+deliverable_id).live("click", function(){
                            self._assessmentDialog(deliverable_id);
                        });
                    }
                }
            }
            html.push("</td>");
            return html.join(' ');
        }
        return '';
    } ,

});