$(function(){

    $("#add_assessment_frequency").hide();

    AssessmentFrequency.getUsers();
    var section = $("#section").val()


    $("#users").change(function(){
        var userid = $(this).val();
        AssessmentFrequency.getEvaluationPeriods(userid);
        return false;
    });


    $("#financial_year_id").change(function(){
        var financial_year = $(this).val();
        $("#financial_yearid").val( financial_year )
        if( financial_year == ""  || financial_year == undefined)
        {
            $("#add_assessment_frequency").fadeOut();
        } else {
            if( section != "admin") {
                $("#add_assessment_frequency").fadeIn();
            }
        }
        AssessmentFrequency.get( financial_year );
        return false;
    });

    $("#add_new").click(function(){

        if($("#add_new_dialog").length > 0)
        {
            $("#add_new_dialog").remove();
        }

        $("<div />",{id:"add_new_dialog"})
            .append($("<table />")
                .append($("<tr />")
                    .append($("<th />",{html:"Assessment Frequency Name:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Number of Assessment Periods:"}))
                    .append($("<td />")
                        .append($("<select />",{id:"period", name:"period"})
                            .append($("<option />",{text:"--period--", value:""}))
                        )
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<p />")
                            .append($("<span />"))
                            .append($("<span />",{id:"message"}))
                        )
                    )
                )
            )
            .dialog({
                autoOpen	: true,
                title 		: "Add Assessment Frequency",
                modal		: true,
                width		: "500px",
                position	: "top",
                buttons		: {
                    "Save"		: function()
                    {
                        if( $("#name").val() == "")
                        {
                            $("#message").html("Please enter the name of assessment frequency")
                            return false;
                        } else if( $("#period :selected").val() == ""){
                            $("#message").html("Please select the period for this assessment period");
                            return false;
                        } else {
                            AssessmentFrequency.save();
                        }
                    } ,
                    "Cancel"	: function()
                    {
                        $(this).dialog("destroy");
                    }
                },
                open		: function( event, ui)
                {
                    var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                    var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
                    var saveButton = buttons[0];
                    $(saveButton).css({"color":"#090"});
                }

            });

        for( var i=1; i<=52; i++ )
        {
            $("#period").append($("<option />",{text:i, value:i}))
        }
        return false;
    })

});

var AssessmentFrequency		= {

    getUsers                 : function()
    {
        $.post("main.php?controller=user&action=getAll", { status : 1}, function( responseData ){
            $.each( responseData, function( index, user) {
                $("#users").append($("<option />",{value:user.tkid, text:user.tkname+" "+user.tksurname }))
            });
        },"json");
    } ,

    get			: function( financial_year)
    {
        $(".assessment_frequencies").remove();
        $.getJSON("../controllers/assessmentfrequencycontroller.php?action=getAll", {
            financial_year		: financial_year
        }, function( responseData ){
            if( $.isEmptyObject(responseData) )
            {
                $("#table_assessment_frequency").append($("<tr />").addClass("assessment_frequencies")
                    .append($("<td />",{colspan:"5", html:"There are no evaluation frequencies set for this evaluation year"}))
                )
            } else {
                $.each( responseData, function( index, data){
                    AssessmentFrequency.display( data)
                });
            }
        });
    }  ,

    save		: function()
    {
        $.post("../controllers/assessmentfrequencycontroller.php?action=save",{
            name 	          : $("#name").val(),
            period	          : $("#period :selected").val(),
            financial_year_id : $("#financial_year_id").val()
        }, function( response ) {
            if( response.error )
            {
                $("#message").html(response.text);
            } else {
                AssessmentFrequency.get( $("#financial_year_id").val() );
                jsDisplayResult("ok", "ok", response.text );
                $("#add_new_dialog").dialog("destroy");
                $("#add_new_dialog").remove();
            }
        },"json")
    } ,

    updateFreq		: function( index )
    {

        $.post("../controllers/assessmentfrequencycontroller.php?action=update", {
            id		: index,
            name	: $("#name").val(),
            period	: $("#period :selected").val()
        }, function( response ) {
            if( response.error )
            {
                $("#message").html(response.text)
            } else {
                AssessmentFrequency.get( $("#financial_year_id").val() )
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#editdialog").dialog("destroy");
                $("#editdialog").remove();
            }
        },"json");
    } ,

    display		: function(assessment_frequency)
    {
        $("#table_assessment_frequency")
            .append($("<tr />").addClass("assessment_frequencies")
                .append($("<td />",{html:assessment_frequency.id}))
                .append($("<td />",{html:assessment_frequency.name}))
                .append($("<td />",{html:assessment_frequency.period}))
                .append($("<td />",{html:((assessment_frequency.status & 1) == 1 ? "<b>Active</b>" : "<b>Inactive</b>") }))
                .append($("<td />")
                    .append($("<input />",{type:"button", id:"edit_"+assessment_frequency.id, name:"edit_"+assessment_frequency.id, value:"Edit"}))
                )
            )

        $("#openclose_"+assessment_frequency.id).click(function(){

            if( $("#openclosedialog_"+assessment_frequency.id).length > 0)
            {
                $("#openclosedialog_"+assessment_frequency.id).remove();
            }

            $("<div />",{id:"openclosedialog_"+assessment_frequency.id, html:"You are about to "+((assessment_frequency.status & 8) == 8 ? "open" : "close")+" this evaluation period "})
                .dialog({
                    autoOpen	: true,
                    modal		: true,
                    title 		: "Open Close",
                    position	: "top",
                    width		: "500px",
                    buttons		: {
                        "Open"		: function()
                        {
                            AssessmentFrequency.open( assessment_frequency.id );
                        } ,

                        "Close"		: function()
                        {
                            AssessmentFrequency.close( assessment_frequency.id);
                        } ,

                        "Cancel"	: function()
                        {
                            $(this).dialog("destroy")
                        }
                    } ,

                    open		: function( event , ui)
                    {
                        var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                        var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
                        var openButton = buttons[0];
                        var closeButton = buttons[1];
                        $(openButton).css({"color":"#090", display:((assessment_frequency.status & 8) == 8 ? "inline" : "none")});;
                        $(closeButton).css({"color":"#090", display:((assessment_frequency.status & 8) == 8 ? "none" : "inline")});
                    }
                });
            return false;
        })

        $("#edit_"+assessment_frequency.id).live("click", function(){
            if( $("#editdialog").length > 0)
            {
                $("#editdialog").remove();
            }

            $("<div />",{id:"editdialog"})
                .append($("<table />")
                    .append($("<tr />")
                        .append($("<th />",{html:"Evaluation Frequency Name:"}))
                        .append($("<td />")
                            .append($("<input />",{type:"text", name:"name", id:"name", value:assessment_frequency.name}))
                        )
                    )
                    .append($("<tr />")
                        .append($("<th />",{html:"Number of Evaluation Periods:"}))
                        .append($("<td />")
                            .append($("<select />",{id:"period", name:"period"})
                                .append($("<option />",{text:"--period--", value:""}))
                            )
                        )
                    )
                    .append($("<tr />")
                        .append($("<td />",{colspan:"2"})
                            .append($("<p />")
                                .append($("<span />"))
                                .append($("<span />",{id:"message"}))
                            )
                        )
                    )
                )
                .dialog({
                    autoOpen	: true,
                    title 		: "Edit Evaluation Frequencies",
                    modal		: true,
                    width		: "500px",
                    position 	: "top",
                    buttons		: {
                        "Update"		: function()
                        {
                            if( $("#name").val() == "")
                            {
                                $("#message").html("Please enter the name of assement frequency")
                                return false;
                            } else if( $("#period :selected").val() == ""){
                                $("#message").html("Please select the period for this assessment period");
                                return false;
                            } else {
                                AssessmentFrequency.updateFreq( assessment_frequency.id );
                            }
                        } ,
                        "Cancel"	: function()
                        {
                            $(this).dialog("destroy");
                        },

                        "Delete"	: function()
                        {
                            AssessmentFrequency.deleteFreq( assessment_frequency.id, 2 );
                        } ,

                        "Deactivate"  : function()
                        {
                            AssessmentFrequency.updateStatus( assessment_frequency.id, 0 );
                        } ,

                        "Activate"		: function()
                        {
                            AssessmentFrequency.updateStatus( assessment_frequency.id, 1 );
                        }

                    },
                    open		: function( event, ui)
                    {
                        var dialog 			 = $(event.target).parents(".ui-dialog.ui-widget");
                        var buttons 	 	 = dialog.find(".ui-dialog-buttonpane").find("button");
                        var saveButton 		 = buttons[0];
                        var deleteButton 	 = buttons[2]
                        var deactivateButton = buttons[3];
                        var activateButton   = buttons[4];
                        $(saveButton).css({"color":"#090"});
                        $(deleteButton).css({"color":"red", display:(assessment_frequency.used ? "none" : "inline")});
                        $(deactivateButton).css({"color":"red", display:((assessment_frequency.status & 1) == 1 ? "inline" : "none")});
                        $(activateButton).css({"color":"#090", display:((assessment_frequency.status & 1) == 1 ? "none" : "inline") });
                        for( var i=1; i<=52; i++ )
                        {
                            if(assessment_frequency.period == i)
                            {
                                $("#period").append($("<option />",{text:i, value:i, selected:"selected"}))
                            } else {
                                $("#period").append($("<option />",{text:i, value:i}))
                            }

                        }
                    }

                });

            return false;
        });

    } ,

    deleteFreq				: function( index)
    {

        if( confirm("Are you sure you want to delete this evaluation frequency"))
        {
            AssessmentFrequency.updateStatus( index, 2 );
        }
    } ,

    updateStatus			: function( index, status )
    {
        $.post("../controllers/assessmentfrequencycontroller.php?action=update", {
            id 		: index,
            status  : status
        }, function( response ){
            if( response.error )
            {
                $("#message").html(response.text)
            } else {
                AssessmentFrequency.get( $("#financial_year_id").val() );
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#editdialog").dialog("destroy");
                $("#editdialog").remove();
            }
        },"json");
    } ,

    open			: function( index )
    {
        $.post("../controllers/assessmentfrequencycontroller.php?action=update", {
            id 		: index
        }, function( response ){
            if( response.error )
            {
                $("#message").html(response.text)
            } else {
                AssessmentFrequency.get( $("#financial_year_id").val() );
                if(response.updated)
                {
                    jsDisplayResult("info", "info", response.text );
                } else {
                    jsDisplayResult("ok", "ok", response.text );
                }
                $("#openclosedialog_"+index).dialog("destroy");
                $("#openclosedialog_"+index).remove();
            }
        },"json");
    } ,

    close			: function( index )
    {
        $.post("../controllers/assessmentfrequencycontroller.php?action=update", {
            id 		: index
        }, function( response ){
            if( response.error )
            {
                $("#message").html(response.text)
            } else {
                AssessmentFrequency.get( $("#financial_year_id").val() );
                if(response.updated)
                {
                    jsDisplayResult("info", "info", response.text );
                } else {
                    jsDisplayResult("ok", "ok", response.text );
                }
                $("#openclosedialog_"+index).dialog("destroy");
                $("#openclosedialog_"+index).remove();
            }
        },"json");
    } ,

    getEvaluationPeriods           : function(user)
    {
        $.post("main.php?controller=evaluationfrequencies&action=getAll", {
            id 		: user
        }, function( response ){
            if( response.error )
            {
                $("#message").html(response.text)
            } else {
                //AssessmentFrequency.get( $("#financial_year").val() );
            }
        },"json");
    }

};
