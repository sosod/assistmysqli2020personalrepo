// JavaScript Document
$.widget("ui.approvedeliverable", {

    options	: 	{
        tableId 		       : "approve_deliverable_"+(Math.floor(Math.random(56) * 34)),
        url				       : "../controllers/deliverablecontroller.php?action=getApprovalDeliverables",
        start			       : 0,
        limit			       : 10,
        total			       : 0,
        deliverable_id	       : 0,
        financial_year	       : 0,
        contract_id	           : 0,
        owner_id               : 0,
        current			       : 1,
        isOwners		       : [],
        section                : "",
        page                   : "",
        viewDeliverable        : "",
    } ,

    _init			: function()
    {
        this._getDeliverables();
    } ,

    _create			: function()
    {
        var self = this;
        var html = [];
        html.push("<table class='noborder' width='100%'>");
            html.push("<tr>");
                html.push("<td class='noborder'>");
                    html.push("<table class='noborder'>");
                        html.push("<tr>");
                            html.push("<th style='text-align:left;'>Financial Year:</th>");
                            html.push("<td class='noborder'>");
                                html.push("<select id='financial_year' name='financial_year' style='width:200px;'>");
                                    html.push("<option value=''>--please select--</option>");
                                html.push("</select>");
                            html.push("</td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th style='text-align:left;'>Contract:</th>");
                            html.push("<td class='noborder'>");
                                html.push("<select id='contract_id' name='contract_id' style='width:200px;'>");
                                    html.push("<option value=''>--please select--</option>");
                                html.push("</select>");
                            html.push("</td>");
                        html.push("</tr>");
                        html.push("<tr>");
                            html.push("<th style='text-align:left;'>Deliverable Owner:</th>");
                            html.push("<td class='noborder'>");
                                html.push("<select id='owner_id' name='owner_id' style='width:200px;'>");
                                    html.push("<option value=''>--please select--</option>");
                                html.push("</select>");
                            html.push("</td>");
                        html.push("</tr>");
                    html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td class='noborder'>");
                    html.push("<table width='100%' class='noborder'>");
                        html.push("<tr>");
                            html.push("<td width='50%' class='noborder'>");
                                html.push("<table width='100%' id='awaiting_"+self.options.tableId+"'>");
                                html.push("<tr>")
                                    html.push("<td colspan='6'><h4>Deliverables Awaiting Approval</h4></td>");
                                html.push("</tr>");
                                html.push("</table>");
                                html.push("</td>");
                                html.push("<td class='noborder'>");
                                    html.push("<table width='100%' id='approved_"+self.options.tableId+"'>");
                                html.push("<tr width='50%'>");
                                    html.push("<td colspan='6'><h4>Deliverables Approved</h4></td>");
                                html.push("</tr>");
                                html.push("</table>");
                            html.push("</td>");
                        html.push("</tr>");
                    html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
        html.push("</table>");
        $(self.element).append(html.join(' '));

        self._loadFinancialYears();
        self._loadContracts();
        self._loadDeliverableOwners();

        $("#financial_year").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.financial_year = $(this).val();
                self._getDeliverables();
            } else {
                self.options.financial_year  = 0;
                self._getDeliverables();
            }
        });

        $("#contract_id").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.contract_id = $(this).val();
                self._getDeliverables();
            } else {
                self.options.contract_id = 0;
                self._getDeliverables();
            }
        });

        $("#owner_id").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.deliverable_id = $(this).val();
                self._getDeliverables();
            } else {
                self.options.deliverable_id = 0;
                self._getDeliverables();
            }
        });
    } ,

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#financial_year").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
            });
            $("#financial_year").html( finHtml.join(' ') );
        });
    } ,

    _loadContracts              : function()
    {
        var self = this;
        $("#contract_id").empty();
        $.getJSON("../controllers/contractcontroller.php?action=getAll",{
            financial_year_id  : self.options.financial_year
        }, function(contracts){
            var contractHtml = [];
            contractHtml.push("<option value=''>--please select--</option>");
            $.each(contracts, function(contract_id, contract) {
                contractHtml.push("<option value='"+contract_id+"'>"+contract+"</option>");
            })
            $("#contract_id").html( contractHtml.join(' ') );
        });
    },

    _loadDeliverableOwners              : function()
    {
        var self = this;
        $("#owner_id").empty();
        $.getJSON("../controllers/usercontroller.php?action=getAll", function(owners){
            var ownerHtml = [];
            ownerHtml.push("<option value=''>--please select--</option>");
            $.each(owners, function(owner_id, owner) {
                ownerHtml.push("<option value='"+owner_id+"'>"+owner+"</option>");
            })
            $("#owner_id").html( ownerHtml.join(' ') );
        });
    },


    _getDeliverables		: function()
    {
        var self = this;
        $("body").append($("<div />",{id:"deliverableLoadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
        );
        $.getJSON( self.options.url, {
            start 	        : self.options.start,
            limit 	        : self.options.limit,
            id		        : self.options.deliverable_id,
            contract_id     : self.options.contract_id,
            owner_id        : self.options.owner_id,
            financial_year  : self.options.financial_year,
            section         : self.options.section,
            page            : 'approve'
        }, function(deliverableData){
            $("#deliverableLoadingDiv").remove();
            $(".approval").remove();
            $(".approved").remove();
            $(".awaiting").remove();
            self._displayHeaders(deliverableData.columns);
            self._displayAwaiting(deliverableData.awaiting.deliverables);
            self._displayApproved(deliverableData.approved.deliverables);
        });
    } ,

    _displayAwaiting    : function(deliverables)
    {
        var self = this;
        var html = [];
        if($.isEmptyObject(deliverables))
        {
            html.push("<tr class='awaiting'><td colspan='6'>There are no deliverables awaiting approval</td></tr>");
        } else {
            $.each(deliverables, function(deliverable_id, deliverable){
                html.push("<tr class='awaiting'>");
                html.push("<td>"+deliverable.deliverable_id+"</td>");
                html.push("<td>"+deliverable.deliverable_name+"</td>");
                html.push("<td>"+deliverable.deliverable_progress+"</td>");
                html.push("<td>"+deliverable.deliverable_deadline_date+"</td>");
                html.push("<td>"+deliverable.deliverable_owner+"</td>");
                html.push("<td>");
                    html.push("<input type='button' name='view_logs_"+deliverable_id+"' id='view_logs_"+deliverable_id+"' value='View Logs' />");
                    $("#view_logs_"+deliverable_id).live("click", function(e){
                        self._viewLogs(deliverable_id);
                        e.preventDefault();
                    });

                html.push("<input type='button' name='approve_"+deliverable_id+"' id='approve_"+deliverable_id+"' value='Approve' />");
                $("#approve_"+deliverable_id).live("click", function(e){
                    self._approveContract(deliverable_id);
                    e.preventDefault();
                });
                html.push("</td>");


                html.push("</tr>");
            });
        }
        $("#awaiting_"+self.options.tableId).append(html.join(' ') );

    } ,

    _displayApproved			: function(deliverables)
    {
        var self = this;
        var html = [];
        if($.isEmptyObject(deliverables))
        {
            html.push("<tr class='approved'><td colspan='6'>There are no deliverables approved</td></tr>");
        } else {
            $.each(deliverables, function(deliverable_id, deliverable){
                html.push("<tr class='approved'>");
                    html.push("<td>"+deliverable.deliverable_id+"</td>");
                    html.push("<td>"+deliverable.deliverable_name+"</td>");
                    html.push("<td>"+deliverable.deliverable_progress+"</td>");
                    html.push("<td>"+deliverable.deliverable_deadline_date+"</td>");
                    html.push("<td>"+deliverable.deliverable_owner+"</td>");
                    html.push("<td>");
                        html.push("<input type='button' name='undo_approve_"+deliverable_id+"' id='undo_approve_"+deliverable_id+"' value='Undo Approve' />");
                        $("#undo_approve_"+deliverable_id).live("click", function(e){
                            self._undoApproveDeliverable(deliverable_id);
                            e.preventDefault();
                        });
                    html.push("</td>");

                html.push("</tr>");
            });
        }
        $("#approved_"+self.options.tableId).append(html.join(' ') );
    } ,

    _approveContract      : function(deliverable_id)
    {
        var self = this;
        if($("#approve_deliverable_dialog_"+deliverable_id).length > 0)
        {
            $("#approve_deliverable_dialog_"+deliverable_id).remove();
        }
        var html = [];
        html.push("<table>");
            html.push("<tr>");
                html.push("<th>Response</th>")
                html.push("<td>");
                    html.push("<textarea id='response' name='response' cols='50' rows='8'></textarea>")
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td colspan='2'>");
                 html.push("<p id='message'></p>")
                html.push("</td>");
            html.push("</tr>");
        html.push("<table>");

        $("<div />",{id:"approve_deliverable_dialog_"+deliverable_id}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Approve Deliverable D"+deliverable_id,
                width       : 'auto',
                buttons     : {
                    "Approve"      : function()
                    {
                        $('#message').html("Approving deliverable . . . <img src='../images/loaderA32.gif' />");
                        $.post("../controllers/deliverablecontroller.php?action=approveDeliverable", {
                            deliverable_id : deliverable_id,
                            response    : $("#response").val()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getDeliverables();
                            }
                        },"json");
                        $("#approve_deliverable_dialog_"+deliverable_id).dialog("destroy").remove();
                    } ,

                    "Cancel"        : function()
                    {
                        $("#approve_deliverable_dialog_"+deliverable_id).dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#approve_deliverable_dialog_"+deliverable_id).dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
                }
            });

    },

    _undoApproveDeliverable: function(deliverable_id)
    {
        var self = this;
        if($("#undo_approve_deliverable_dialog_"+deliverable_id).length > 0)
        {
            $("#undo_approve_deliverable_dialog_"+deliverable_id).remove();
        }
        var html = [];
        html.push("<table>");
            html.push("<tr>");
                html.push("<th>Response</th>")
                html.push("<td>");
                    html.push("<textarea id='response' name='response' cols='50' rows='8'></textarea>")
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td colspan='2'>");
                    html.push("<p id='message'></p>")
                html.push("</td>");
            html.push("</tr>");
        html.push("<table>");

        $("<div />",{id:"undo_approve_deliverable_dialog_"+deliverable_id}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Undo Approve Deliverable D"+deliverable_id,
                width       : 'auto',
                buttons     : {
                    "Undo Approve"      : function()
                    {
                        $('#message').html("Approving deliverable ....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/deliverablecontroller.php?action=undoApproveDeliverable", {
                            deliverable_id : deliverable_id,
                            response    : $("#response").val()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getDeliverables();
                            }
                        },"json");
                        $("#undo_approve_deliverable_dialog_"+deliverable_id).dialog("destroy").remove();
                    } ,

                    "Cancel"        : function()
                    {
                        $("#undo_approve_deliverable_dialog_"+deliverable_id).dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#undo_approve_deliverable_dialog_"+deliverable_id).dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
                }
            });

    },

    _displayHeaders		: function(headers)
    {
        var self = this;
        var html = [];
        html.push("<tr class='approval'>");
            html.push("<th>"+headers.deliverable_id+"</th>");
            html.push("<th>"+headers.deliverable_name+"</th>");
            html.push("<th>"+headers.deliverable_progress+"</th>");
            html.push("<th>"+headers.deliverable_deadline_date+"</th>");
            html.push("<th>"+headers.deliverable_owner+"</th>");
            html.push("<th></th>");
        html.push("</tr>");
        $("#awaiting_"+self.options.tableId).append(html.join(' '));
        $("#approved_"+self.options.tableId).append(html.join(' ')).find("#remove").remove();;
    } ,

    _viewLogs                : function(deliverable_id)
     {
         var self = this;
         if($("#view_activity_log_dialog_"+deliverable_id).length > 0)
         {
             $("#view_activity_log_dialog_"+deliverable_id).remove();
         }
         var html = [];
         html.push("<table id='deliverable_log_table'>");
             html.push("<tr>");
                 html.push("<th>Date/Time</th>");
                 html.push("<th>User</th>");
                 html.push("<th>Change Log</th>");
                 html.push("<th>Status</th>");
             html.push("</tr>");
         $("<div />",{id:"view_activity_log_dialog_"+deliverable_id}).append(html.join(' '))
             .dialog({
                 autoOpen    : true,
                 modal       : true,
                 position    : "top",
                 title       : "View Log Deliverable D"+deliverable_id,
                 width       : 'auto',
                 buttons     : {
                     "Ok"      : function()
                     {
                         $("#view_activity_log_dialog_"+deliverable_id).dialog("destroy").remove();
                     }
                 },
                 close       : function(event, ui)
                 {
                     $("#view_activity_log_dialog_"+deliverable_id).dialog("destroy").remove();
                 },
                 open        : function(event, ui)
                 {
                     $.get("../controllers/deliverablecontroller.php?action=getDeliverableUpdateLog", {
                         id : deliverable_id
                     }, function(deliverableUpdates){
                         $("#deliverable_log_table").append(deliverableUpdates);
                     },"html");
                     $(this).css("height", "auto");
                 }
             });
    },

    _displayPaging		: function(total, cols)
    {
        var self  = this;
        var html  = [];
        var pages = 0;
        if(total%self.options.limit > 0)
        {
            pages = Math.ceil( total/self.options.limit )
        } else {
            pages = Math.floor( total/self.options.limit )
        }
        html.push("<tr>");
        html.push("<td colspan='"+cols+"'>");
            html.push("<input type='button' id='deliverable_first_"+self.option.deliverable_id+"' value=' |< ' />");
            html.push("<input type='button' id='deliverable_previous_"+self.option.deliverable_id+"' value=' < ' />");
            html.push("<span>&nbsp;&nbsp;&nbsp;");
            html.push("Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ?  1 :pages));
            html.push("&nbsp;&nbsp;&nbsp;</span>");
            html.push("<input type='button' id='deliverable_next_"+self.option.deliverable_id+"' value=' > ' />");
            html.push("<input type='button' id='deliverable_last_"+self.option.deliverable_id+"' value=' >| ' />");
        html.push("</td>");
        if(self.options.editDeliverable)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.updateDeliverable)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.assuranceDeliverable)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAssurance)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewDeliverable)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        html.push("</tr>");
        $("#"+self.options.tableId+"_"+self.options.deliverable_id).append(html.join(' '));
        if(self.options.current < 2)
        {
            $("#deliverable_first_"+self.option.deliverable_id).attr('disabled', 'disabled');
            $("#deliverable_previous_"+self.option.deliverable_id).attr('disabled', 'disabled');
        }
        if((self.options.current == pages || pages == 0))
        {
            $("#deliverable_next_"+self.option.deliverable_id).attr('disabled', 'disabled');
            $("#deliverable_last_"+self.option.deliverable_id).attr('disabled', 'disabled');
        }
        $("#deliverable_next_"+self.option.deliverable_id).bind("click", function(){
            self._getNext( self );
        });
        $("#deliverable_last_"+self.option.deliverable_id).bind("click",  function(){
            self._getLast( self );
        });
        $("#deliverable_previous_"+self.option.deliverable_id).bind("click",  function(){
            self._getPrevious( self );
        });
        $("#deliverable_first_"+self.option.deliverable_id).bind("click",  function(){
            self._getFirst( self );
        });
    } ,

    _getNext  			: function( $this ) {
        $this.options.current   = $this.options.current+1;
        $this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
        this._getDeliverables();
    },

    _getLast  			: function( $this ) {
        $this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
        $this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );
        this._getDeliverables();
    },

    _getPrevious    	: function( $this ) {
        $this.options.current   = parseFloat( $this.options.current ) - 1;
        $this.options.start 	= ($this.options.current-1)*$this.options.limit;
        this._getDeliverables();
    },

    _getFirst  			: function( $this ) {
        $this.options.current   = 1;
        $this.options.start 	= 0;
        this._getDeliverables();
    }


});
