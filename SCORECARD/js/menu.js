$(function(){
    Menu.get();
});

var Menu 		= {

    get			: function()
    {
        $.getJSON("../controllers/menucontroller.php?action=getAll", function( response ) {
            $.each( response, function( index, menu) {
                $("#menu_table").append($("<tr />",{id:"tr_"+menu.id})
                    .append($("<td />",{html:index}))
                    .append($("<td />",{html:menu.name}))
                    .append($("<td />",{html:menu.section}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"menu_item_"+index, id:"menu_item_"+index, value:menu.client_terminology,size:"40"}))
                    )
                    .append($("<td />")
                        .append($("<input />",{type:"button", name:"update_"+index, id:"update_"+index, value:"Update"}))
                    )
                )

                $("#tr_"+index).hover(function(){
                    $(this).addClass("gray");
                }, function(){
                    $(this).removeClass("gray");
                });


                $("#update_"+index).on("click", function() {
                    jsDisplayResult("info", "info", "Updating  ...  <img src='../images/loaderA32.gif' />");
                    $.post("../controllers/menucontroller.php?action=update", {
                        client_terminology : $("#menu_item_"+index).val(),
                        id	 			   : index
                    }, function( response ) {
                        if( response.error ){
                            jsDisplayResult("error", "error", response.text);
                        } else {
                            if(response.updated)
                            {
                                jsDisplayResult("ok", "ok", response.text );
                            } else {
                                jsDisplayResult("info", "info", response.text );
                            }
                        }
                    },"json");
                    return false;
                });
            });
        });
    } ,

    update		: function()
    {
    }



}; 	
