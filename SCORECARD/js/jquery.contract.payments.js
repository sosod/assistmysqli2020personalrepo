// JavaScript Document
$.widget("contract.payments", {

    options	: 	{
        tableRef 		       : "contract_payments_"+(Math.floor(Math.random(56) * 34)),
        url				       : "../controllers/contractpaymentcontroller.php?action=getContractPayments",
        start			       : 0,
        limit			       : 10,
        total			       : 0,
        contract_id	           : 0,
        financial_year	       : 0,
        owner_id               : 0,
        current			       : 1,
        isOwners		       : [],
        section                : "",
        viewAction             : "",
        autoLoad               : false
    } ,

    _init			: function()
    {
        if(this.options.autoLoad)
        {
            this._getContractPayments();
        }
    } ,

    _create			: function()
    {
        var self = this;
        var html = [];

        html.push("<table class='noborder' width='100%'>");
            html.push("<tr>");
                html.push("<td class='noborder'>");
                html.push("<table class='noborder'>");
                    html.push("<tr>");
                        html.push("<th style='text-align:left;'>Financial Year:</th>");
                        html.push("<td class='noborder'>");
                            html.push("<select id='financial_year' name='financial_year' style='width:200px;'>");
                            html.push("<option value=''>--please select--</option>");
                            html.push("</select>");
                        html.push("</td>");
                        html.push("</tr>");
                        html.push("<tr>");
                        html.push("<th style='text-align:left;'>Contract:</th>");
                        html.push("<td class='noborder'>");
                            html.push("<select id='contract_id' name='contract_id' style='width:200px;'>");
                            html.push("<option value=''>--please select--</option>");
                            html.push("</select>");
                        html.push("</td>");
                    html.push("</tr>");
                html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td class='noborder'>");
                html.push("<table id='"+self.options.tableRef+"' width='100%'>");
                    html.push("<p class='ui-state-highlight-info contract_payments'>Please select financial year and contract to view payments</p>");
                html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
        html.push("</table>");
        $(self.element).append(html.join(' '));

        self._loadFinancialYears();

        $("#financial_year").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.financial_year = $(this).val();
                $(".contract_payments").remove();
                self._loadContracts();
            } else {
                $(".contract_payments").remove();
                self.options.financial_year  = 0;
            }
        });

        $("#contract_id").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.contract_id = $(this).val();
                self._getContractPayments();
            } else {
                self.options.contract_id = 0;
                self._getContractPayments();
            }
        });

    } ,

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#financial_year").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
            });
            $("#financial_year").html( finHtml.join(' ') );
        });
    } ,

    _loadContracts              : function()
    {
        var self = this;
        $("#contract_id").empty();
        $.getJSON("../controllers/contractcontroller.php?action=getAll",{
            financial_year : self.options.financial_year,
            page: "payment"
        }, function(contracts){
            var contractHtml = [];
            contractHtml.push("<option value=''>--please select--</option>");
            $.each(contracts, function(contract_id, contract) {
                contractHtml.push("<option value='"+contract_id+"'>"+contract+"</option>");
            })
            $("#contract_id").html( contractHtml.join(' ') );
        });
    },

    _getContractPayments		: function()
    {
        var self = this;
        $("body").append($("<div />",{id:"actionLoadingDiv", html:"Loading contract payments . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
        );

        $.getJSON( self.options.url, {
            start 	        : self.options.start,
            limit 	        : self.options.limit,
            id		        : self.options.action_id,
            contract_id     : self.options.contract_id,
            financial_year  : self.options.financial_year,
            section         : self.options.section,
            page            : 'budget'
        }, function(contractPayments){
            $("#actionLoadingDiv").remove();
            $(".contract_payments").remove();
            self._addNewPayment(contractPayments.suppliers, contractPayments.total_suppliers);
            self._displayHeaders(contractPayments.suppliers);
            if($.isEmptyObject(contractPayments.payments))
            {
                var _html = [];
                _html.push("<tr class='contract_payments'>")
                    _html.push("<td colspan='9'>");
                        _html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                            _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                                _html.push("There are no contract payments found");
                        _html.push("</p>");
                    _html.push("</td>");
                _html.push("</tr>");
                $("#"+self.options.tableRef).append( _html.join(' ') );
            } else {
                self._display(contractPayments.payments, contractPayments.supplier_payments, contractPayments.supplier_total, contractPayments.payment_total);
            }
        });
    } ,


    _addNewPayment     : function(contract_suppliers, total_suppliers)
    {
        var self =  this;
        var html = [];
        html.push("<tr class='contract_payments'>");
        html.push("<td colspan='"+(7 + parseInt(total_suppliers) )+"'>");+
        html.push("<input type='button' name='add_new_adjustment' id='add_new_adjustment' value='Add New Payment' />");
        $("#add_new_adjustment").live("click", function(e){
            self._addContractPayment(contract_suppliers);
            e.preventDefault();
        });
        html.push("</td>");
        html.push("</tr>");
        $("#"+self.options.tableRef).append(html.join(' ') );
    },

    _display    : function(contract_payments, supplier_payments, supplier_totals, payment_total)
    {
        var self = this;
        var html = [];
        $.each(contract_payments, function(contract_payment_id, contract_payment){
            html.push("<tr class='contract_payments'>");
            html.push("<td>"+contract_payment_id+"</td>");
            html.push("<td>"+contract_payment.name+"</td>");
            html.push("<td>"+contract_payment.payment_date+"</td>");
            html.push("<td>"+contract_payment.certificate_number+"</td>");
            html.push("<td>"+contract_payment.attachments+"</td>");
            html.push("<td class='th2'  style='text-align:right;'>"+contract_payment.total+"</td>");
            if(supplier_payments.hasOwnProperty(contract_payment_id))
            {
                $.each(supplier_payments[contract_payment_id]['supplier_payment'], function(index, supplier_payment){
                    html.push("<td style='text-align:right;'>"+supplier_payment+"</td>");
                });
            }
            html.push("<td>");
            html.push("<input type='button' name='edit_adjustment_"+contract_payment_id+"' id='edit_adjustment_"+contract_payment_id+"' value='Edit' />");
            $("#edit_adjustment_"+contract_payment_id).live("click", function(e){
                self._editContractPayment(contract_payment_id, contract_payment);
                e.preventDefault();
            });

            html.push("<input type='button' name='delete_"+contract_payment_id+"' id='delete_"+contract_payment_id+"' value='Delete' />");
            $("#delete_"+contract_payment_id).live("click", function(e){
                if(confirm('Are you sure you want to delete this contract adjustment'))
                {
                    self._deleteContractPayment(contract_payment_id, contract_payment);
                }
                e.preventDefault();
            });
            html.push("</td>");
            html.push("</tr>");
        });
        html.push("<tr class='contract_payments'>");
        html.push("<th colspan='5' class='th2'></th>");
        html.push("<th style='text-align:right;' class='th2'><b>"+payment_total+"</b></th>");
        $.each(supplier_totals, function(supplier_id, payment){
            html.push("<th style='text-align:right;' class='th2'><b>"+payment+"</b></th>");
        });
        html.push("<th class='th2'>&nbsp;</th>");
        html.push("</tr>");
        $("#"+self.options.tableRef).append(html.join(' ') );

    } ,

    _displayHeaders		: function(suppliers)
    {
        var self = this;
        var html = [];
        html.push("<tr class='contract_payments'>");
        html.push("<th>Ref</th>");
        html.push("<th>Payment Info</th>");
        html.push("<th>Payment Date</th>");
        html.push("<th>Certificate Number</th>");
        html.push("<th>POE Attached</th>");
        html.push("<th>Payment Amount (incl. VAT)</th>");
        $.each(suppliers, function(supplier_id, supplier){
            html.push("<th>"+supplier+"</th>");
        });
        html.push("<th>&nbsp;</th>");
        html.push("</tr>");
        $("#"+self.options.tableRef).append(html.join(' ')).find("#remove").remove();;
    } ,

    _addContractPayment  : function(contract_suppliers)
    {
        var self = this;
        if($("#add_contract_payment").length > 0)
        {
            $("#add_contract_payment").remove();
        }
        var html = [];
        html.push("<form method='post' name='contract_payment_form' id='contract_payment_form' >");
            html.push("<table>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Payment Reference Number:</th>");
                html.push("<td>");
                    html.push("<input type='text' id='name' name='name' value='' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Payment Date:</th>");
                html.push("<td>");
                    html.push("<input type='text' id='payment_date' name='payment_date' value='' class='payment_date_picker' readonly='readonly' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Certificate Number:</th>");
                html.push("<td>");
                    html.push("<input type='text' id='certificate_number' name='certificate_number' value='' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
            html.push("<th style='text-align: left;'>POE Attachment:</th>");
                html.push("<td>");
                    html.push("<div id='file_upload'></div>");
                    html.push("<input type='file' id='payment_attachment' name='payment_attachment' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Supplier Payments:</th>");
                html.push("<td>");
                    html.push("<div id='supplier_payments'></div>");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td colspan='2'>");
                    html.push("<p id='message'></p>");
                html.push("</td>");
            html.push("</tr>");
        html.push("</table>");
        html.push("</form>");
        $("<div />",{id:"add_contract_payment"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "New Contract Payment ",
                width       : 'auto',
                buttons     : {
                    "Save"      : function()
                    {
                        $('#message').html("Saving payment . . . <img src='../images/loaderA32.gif' />");
                        $.post("../controllers/contractpaymentcontroller.php?action=save", {
                            contract_id   : self.options.contract_id,
                            data          : $("#contract_payment_form").serialize()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getContractPayments();
                                $("#add_contract_payment").dialog("destroy").remove();
                            }
                        },"json");
                    } ,

                    "Cancel"        : function()
                    {
                        $("#add_contract_payment").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#add_contract_payment").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns          = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});

                    $(".payment_date_picker").datepicker({
                        showOn		    : "both",
                        buttonImage 	: "/library/jquery/css/calendar.gif",
                        buttonImageOnly : true,
                        changeMonth	    : true,
                        changeYear	    : true,
                        dateFormat 	    : "dd-M-yy",
                        altField		: "#startDate",
                        altFormat		: 'd_m_yy'
                    });
                    ;
                    $.get('../controllers/contractsuppliercontroller.php?action=getContractPaymentSupplierHtml',{
                        contract_id : self.options.contract_id
                    }, function(supplierPaymentHtml){
                        $("#supplier_payments").html( supplierPaymentHtml );
                    }, 'html');


                    $("#payment_attachment").live('change', function(e){
                        self._attachContractAdjustmentDocuments(this);
                        return false;
                    });
                }
            });
    },

    _editContractPayment      : function(contract_payment_id, contract_payment)
    {
        var self = this;
        if($("#edit_contract_payment_dialog").length > 0)
        {
            $("#edit_contract_payment_dialog").remove();
        }
        var html = [];
        html.push("<form method='post' name='edit_contract_payment_form' id='edit_contract_payment_form' >");
        html.push("<table>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Payment Reference Number:</th>");
                html.push("<td>");
                  html.push("<input type='text' id='name' name='name' value='"+contract_payment.name+"' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Payment Date:</th>");
                html.push("<td>");
                html.push("<input type='text' id='payment_date' name='payment_date' value='"+contract_payment.payment_date+"' class='payment_date_picker' readonly='readonly' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Certificate Number:</th>");
                html.push("<td>");
                html.push("<input type='text' id='certificate_number' name='certificate_number' value='"+contract_payment.certificate_number+"' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>POE Attachment:</th>");
                html.push("<td>");
                html.push( contract_payment.editable_attachments );
                html.push("<input type='file' id='payment_attachment' name='payment_attachment' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Supplier Payment:</th>");
                html.push("<td>");
                    html.push("<div id='supplier_payments'></div>");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td colspan='2'>");
                html.push("<p id='message'></p>");
                html.push("</td>");
            html.push("</tr>");
        html.push("</table>");
        html.push("</form>");
        $("<div />",{id:"edit_contract_payment_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Update Contract Payment #"+contract_payment_id,
                width       : 'auto',
                buttons     : {
                    "Save Changes"      : function()
                    {
                        $('#message').html("Updating payment . . . . <img src='../images/loaderA32.gif' />");
                        $.post("../controllers/contractpaymentcontroller.php?action=edit", {
                            contract_id      : self.options.contract_id,
                            id               : contract_payment_id,
                            data            : $("#edit_contract_payment_form").serialize()
                        }, function(response) {
                            if(response.error)
                            {
                                $("#message").html( response.text).addClass('ui-state-error').css({'padding': '5px'});
                            } else {
                                if(response.updated)
                                {
                                    jsDisplayResult("ok", "ok", response.text);
                                } else {
                                    jsDisplayResult("info", "info", response.text);
                                }
                                self._getContractPayments();
                                $("#edit_contract_payment_dialog").dialog("destroy").remove();
                            }
                        },"json");
                    } ,
                    "Cancel"        : function()
                    {
                        $("#edit_contract_payment_dialog").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#edit_contract_payment_dialog").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns          = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});

                    $(".payment_date_picker").datepicker({
                        showOn		    : "both",
                        buttonImage 	: "/library/jquery/css/calendar.gif",
                        buttonImageOnly : true,
                        changeMonth	    : true,
                        changeYear	    : true,
                        dateFormat 	    : "dd-M-yy",
                        altField		: "#startDate",
                        altFormat		: 'd_m_yy'
                    });
                    ;
                    $.get('../controllers/contractsuppliercontroller.php?action=getEditContractPaymentSupplierHtml',{
                        contract_id : self.options.contract_id,
                        payment_id  : contract_payment_id
                    }, function(supplierBudgetHtml){
                        $("#supplier_payments").html( supplierBudgetHtml );
                    }, 'html');

                    $("#payment_attachment").live('change', function(e){
                        self._attachContractAdjustmentDocuments(this);
                        return false;
                    });


                    $(".remove_attach").live('click', function(e){
                        var id          = this.id
                        var ext         = $(this).attr('title');
                        var type        = $(this).attr('ref');
                        var filename    = $(this).attr("alt");
                        var document_id = $(this).attr("file");
                        $.post('../controllers/contractpaymentcontroller.php?action=deleteContractPaymentDocument',
                            {
                                attachment  : id,
                                ext         : ext,
                                id          : id,
                                document_id : document_id,
                                type        : type,
                                filename    : filename
                            }, function(response) {
                                if(response.error)
                                {
                                    $("#result_message").css({padding:"5px", clear:"both"}).html( response.text )
                                } else {
                                    $("#result_message").addClass('ui-state-ok').css({padding:"5px", clear:"both"}).html( response.text )
                                    var deleted_doc = "<input type='hidden' name='deleted_payment_attachment["+document_id+"]' id='deleted_attachment_"+document_id+"' value='"+document_id+"' />";
                                    $("#result_message").append(deleted_doc);
                                    $("#li_"+ext).fadeOut();
                                }
                            },'json');
                        e.preventDefault();
                    });
                }
            });
    },

    _deleteContractPayment  : function(contract_payment_id)
    {
        var self = this;
        $.getJSON('../controllers/contractpaymentcontroller.php?action=deleteContractPayment', {
            id : contract_payment_id
        }, function(response){
            if(response.error)
            {
                jsDisplayResult("error", "error", response.text);
            } else {
                jsDisplayResult("ok", "ok", response.text);
                self._getContractPayments();
            }
        });
    },

    _displayPaging		: function(total, cols)
    {
        var self  = this;
        var html  = [];
        var pages = 0;
        if(total%self.options.limit > 0)
        {
            pages = Math.ceil( total/self.options.limit )
        } else {
            pages = Math.floor( total/self.options.limit )
        }
        html.push("<tr>");
        html.push("<td colspan='"+cols+"'>");
        html.push("<input type='button' id='action_first_"+self.option.action_id+"' value=' |< ' />");
        html.push("<input type='button' id='action_previous_"+self.option.action_id+"' value=' < ' />");
        html.push("<span>&nbsp;&nbsp;&nbsp;");
        html.push("Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ?  1 :pages));
        html.push("&nbsp;&nbsp;&nbsp;</span>");
        html.push("<input type='button' id='action_next_"+self.option.action_id+"' value=' > ' />");
        html.push("<input type='button' id='action_last_"+self.option.action_id+"' value=' >| ' />");
        html.push("</td>");
        if(self.options.editAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.updateAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.assuranceAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAssurance)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        html.push("</tr>");
        $("#"+self.options.tableId+"_"+self.options.action_id).append(html.join(' '));
        if(self.options.current < 2)
        {
            $("#action_first_"+self.option.action_id).attr('disabled', 'disabled');
            $("#action_previous_"+self.option.action_id).attr('disabled', 'disabled');
        }
        if((self.options.current == pages || pages == 0))
        {
            $("#action_next_"+self.option.action_id).attr('disabled', 'disabled');
            $("#action_last_"+self.option.action_id).attr('disabled', 'disabled');
        }
        $("#action_next_"+self.option.action_id).bind("click", function(){
            self._getNext( self );
        });
        $("#action_last_"+self.option.action_id).bind("click",  function(){
            self._getLast( self );
        });
        $("#action_previous_"+self.option.action_id).bind("click",  function(){
            self._getPrevious( self );
        });
        $("#action_first_"+self.option.action_id).bind("click",  function(){
            self._getFirst( self );
        });
    } ,

    _getNext  			: function( $this ) {
        $this.options.current   = $this.options.current+1;
        $this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
        this._getActions();
    },

    _getLast  			: function( $this ) {
        $this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
        $this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );
        this._getActions();
    },

    _getPrevious    	: function( $this ) {
        $this.options.current   = parseFloat( $this.options.current ) - 1;
        $this.options.start 	= ($this.options.current-1)*$this.options.limit;
        this._getActions();
    },

    _getFirst  			: function( $this ) {
        $this.options.current   = 1;
        $this.options.start 	= 0;
        this._getActions();
    },

    _attachContractAdjustmentDocuments : function(element_id)
    {
        $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
        $.ajaxFileUpload({
            url			  : '../controllers/contractpaymentcontroller.php?action=saveAttachment',
            secureuri	  : false,
            fileElementId : 'payment_attachment',
            dataType	  : 'json',
            success       : function (response, status)
            {
                $("#file_upload").html("");
                if(response.error )
                {
                    $("#file_upload").html(response.text )
                } else {
                    $("#file_upload").html("");
                    if(response.error)
                    {
                        $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                    } else {
                        $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')).append($("<div />",{id:"files_uploaded"}))
                        if(!$.isEmptyObject(response.files))
                        {
                            var list = [];
                            list.push("<span>Files uploaded ..</span><br />");
                            $.each(response.files, function(ref, file) {
                                list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                                list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                list.push("<span style='margin-left:5px;'><a href='../controller/contractpaymentcontroller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                list.push("<input type='hidden' name='payment_documents["+file.key+"]' value='"+file.name+"' id='payment_document_"+ref+"' />");
                                list.push("</p>");
                            });
                            $("#files_uploaded").html(list.join(' '));

                            $(".delete").click(function(e){
                                var id = this.id
                                var ext = $(this).attr('title');
                                $.post('../controllers/contractpaymentcontroller.php?action=removeContractPaymentAttachment',
                                    {
                                        attachment : id,
                                        ext        : ext
                                    }, function(response){
                                        if(response.error)
                                        {
                                            $("#result_message").html(response.text)
                                        } else {
                                            $("#result_message").addClass('ui-state-ok').html(response.text)
                                            $("#li_"+id).fadeOut();
                                        }
                                    },'json');
                                e.preventDefault();
                            });
                            $("#action_attachment").val("");
                        }
                    }

                    if( $("#delmessage").length != 0)
                    {
                        $("#delmessage").remove()
                    }
                }
            },
            error: function (data, status, e)
            {
                $("#file_upload").html( "Ajax error -- "+e+", please try uploading again");
            }
        });
    }


});
