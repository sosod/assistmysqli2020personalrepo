$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    DeliverableCategory.get();
    $("#add_deliverable_category").click(function(e){
        DeliverableCategory.addDeliverableCategory();
        e.preventDefault();
    });
	
});

var DeliverableCategory 	= {

		get				: function()
		{
            $(".deliverable_categories").remove();
			$.getJSON( "../controllers/deliverablecategorycontroller.php?action=getAll", function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#deliverable_category_table").append($("<tr />").addClass('deliverable_categories')
                        .append($("<td />", {colspan: 6, html: "There are no heading categories" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        DeliverableCategory.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#deliverable_category_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('deliverable_categories')
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.short_code}))
				.append($("<td />",{html:val.name}))
				.append($("<td />",{html:val.description}))
                .append($("<td />",{html:"<b>"+(val.status == 1 ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"category_edit_"+val.id, name:"category_edit_"+val.id }))
				 )
			  )

            $("#category_edit_"+val.id).live("click", function(e){
                self._updateDeliverableCategory(val);
                e.preventDefault();
            });
			
		},

    addDeliverableCategory          : function()
    {
        var self = this;
        if($("#add_deliverable_category_dialog").length > 0)
        {
            $("#add_deliverable_category_dialog").remove();
        }

        $("<div />",{id:"add_deliverable_category_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Heading Category Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"deliverable_type[short_code]", id:"short_code", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Heading Category Name :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"deliverable_type[name]", id:"name", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Heading Category Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"deliverable_type[description]", id:"description", cols:50, rows:8}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Heading Category",
                width          : 'auto',
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the heading short code");
                            return false;
                        } else if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the heading category name");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_deliverable_category_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_deliverable_category_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});
                }
            })
    },

    _updateDeliverableCategory         : function(deliverable_type)
    {
        var self = this;
        if($("#edit_deliverable_category_dialog").length > 0)
        {
            $("#edit_deliverable_category_dialog").remove();
        }

        $("<div />",{id:"edit_deliverable_category_dialog"}).append($("<table />",{width:"100%"})
               .append($("<tr />")
                    .append($("<th />",{html:"Heading Category Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"short_code", id:"short_code", value:deliverable_type.short_code}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Heading Category Name :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:deliverable_type.name}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Heading Category Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8, text:deliverable_type.description}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Heading Category",
                width          : 'auto',
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the heading type short code");
                            return false;
                        } else if($("#name").val() == "") {
                            $("#message").addClass("ui-state-error").html("please enter the heading type name");
                            return false;
                        } else {
                            self.update(deliverable_type.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_deliverable_category_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(deliverable_type.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(deliverable_type.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(deliverable_type.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_deliverable_category_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((deliverable_type.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/deliverablecategorycontroller.php?action=update",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_deliverable_category_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/deliverablecategorycontroller.php?action=update",
        {
            id           : id,
            short_code : $("#short_code").val(),
            name:        $("#name").val(),
            description: $("#description").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_deliverable_category_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/deliverablecategorycontroller.php?action=save",
            {
                short_code : $("#short_code").val(),
                name:        $("#name").val(),
                description: $("#description").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_deliverable_category_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    DeliverableCategory.get()
                }
            }, 'json');
    }

}