$.widget('scorecard.confirmation', {

    options     : {
        tableRef        : 'confirmation_table_'+(Math.floor(Math.random(563) * 313)),
        url 	        : "../controllers/scorecardcontroller.php?action=getConfirmationDetail",
        scoreCardId     : 0,
        financialYear   : 0,
        autoLoad        : false,
        page            : '',
        section         : '',
        editAction      : true
    } ,


    _init        : function()
    {
        if(this.options.autoLoad)
        {
            this._getConfirmationDetail();
        }
    },

    _create       : function()
    {
        var self = this;
        var html = [];
        html.push("<table class='noborder' id='confirmation-table'>");
          html.push("<tr>");
            html.push("<td class='noborder'>");
              html.push("<table id='"+self.options.tableRef+"' class='noborder'>");
                html.push("<tr>");
                    html.push("<th valign='top' style='text-align: left;'>Financial Year:</th>");
                    html.push("<td valign='top'>");
                        html.push("<select name='confirm_financial_year_id' id='confirm_financial_year_id'>");
                        html.push("</select>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th valign='top' style='text-align: left;'>Scorecard:</th>");
                    html.push("<td valign='top'>");
                        html.push("<select name='score_card_id' id='score_card_id'>");
                        html.push("</select>");
                    html.push("</td>");
                html.push("</tr>");
              html.push("</table>");
            html.push("</td>");
          html.push("</tr>");
          html.push("<tr>")
              html.push("<td class='noborder'>");
              html.push("<table style='"+(self.options.autoLoad ? "" : "display:none")+";' class='details noborder'>");
                html.push("<tr>");
                  html.push("<td valign='top' class='noborder' width='50%'>");
                    html.push("<table id='"+self.options.tableRef+"_score_card' width='100%'></table>");
                  html.push("</td>");
                  html.push("<td valign='top' class='noborder' width='50%'>");
                    html.push("<table id='"+self.options.tableRef+"_summary' width='100%'></table>");
                  html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                  html.push("<td valign='top' class='noborder' colspan='2'>");
                    html.push("<table id='"+self.options.tableRef+"_deliverable' width='100%'></table>");
                  html.push("</td>");
                html.push("</tr>");
              html.push("</table>");
            html.push("</td>");
          html.push("</tr>")
        html.push("</table>");
        $(this.element).append(html.join(' '));
        self._loadFinancialYears();
        if(self.options.autoLoad)
        {
           self._loadContracts(self.options.scoreCardId);
        }

        $("#confirm_financial_year_id").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.financialYear = $(this).val();
                self._loadContracts();
            } else {
                self.options.financialYear  = 0;
                self._loadContracts();
                $(".details").hide();
            }
        });

        $("#score_card_id").live("change", function(){
            $("#"+self.options.tableRef+"_score_card").html("");
            $("#"+self.options.tableRef+"_summary").html("");
            $("#"+self.options.tableRef+"_deliverable").html("");
            if($(this).val() !== "")
            {
                self.options.scoreCardId = $(this).val();
                self.options.showActions = true
                self._getConfirmationDetail();
                //var longDescription = $("select#score_card_id :selected").attr("title");
                //$("#score_card_description").show().html( longDescription ).addClass('ui-state-info').css({'padding': '5px;'});
                $(".details").show();
            } else {
                self.options.scoreCardId = 0;
                self._getConfirmationDetail();
                $(".details").hide();
            }
        });

    },

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#confirm_financial_year_id").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                if(self.options.financialYear == year.id )
                {
                   finHtml.push("<option value='"+year.id+"' selected='selected'>"+year.value+"</option>");
                } else {
                   finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
                }
            });
            $("#confirm_financial_year_id").html( finHtml.join(' ') );
        });
    } ,

    _loadContracts     : function()
    {
        var self = this;
        $("#score_card_id").html("");
        $.getJSON("../controllers/scorecardcontroller.php?action=getAll", {
            section:       'new',
            page:          'confirmation',
            financial_year: self.options.financialYear
        }, function(scorecards){
            var scoreCardHtml = []
            scoreCardHtml.push("<option value=''>--please select--</option>");
            $.each(scorecards, function(score_card_id, score_card){
                if(self.options.scoreCardId == score_card_id)
                {
                   scoreCardHtml.push("<option value='"+score_card_id+"' selected='selected' title='"+score_card.full_name+"'>"+score_card.short_description+"</option>");
                } else {
                   scoreCardHtml.push("<option value='"+score_card_id+"' title='"+score_card.full_name+"'>"+score_card.short_description+"</option>");
                }
            });
            $("#score_card_id").html( scoreCardHtml.join(' ') );
        });
    } ,

    _getConfirmationDetail      : function()
    {
        var self = this;
        $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
        );
        $.getJSON(self.options.url,{
            section		      : self.options.section,
            page              : self.options.page,
            score_card_id     : self.options.scoreCardId
        },function(confirmationData) {
            $("#loadingDiv").remove();
            $(".score_card_message").remove();
            $(".confirm_button").remove();
            $(".confirm-data").remove();
            if(confirmationData.hasOwnProperty('score_card'))
            {
               self._displayContract(confirmationData.score_card);
            }
            if(confirmationData.hasOwnProperty('key_measures'))
            {
                self._displaySummary(confirmationData.key_measures, confirmationData.categories);
            }
            if(confirmationData.hasOwnProperty('category_deliverable'))
            {
                var categoryHtml = [];
                self._displayDeliverableHeader(confirmationData.columns, confirmationData.total_columns );
                var total_d_weight = 0;
                var total_o_weight = 0;
                    var total_q_weight = 0;
                $.each(confirmationData.category_deliverable, function(category_id, deliverables){
                    categoryHtml.push("<tr class='confirm-data'>");
                        categoryHtml.push("<td  class='noborder' colspan='"+(confirmationData.total_columns+1)+"'><b>"+confirmationData.categories[category_id]+"</b></td>");
                    categoryHtml.push("</tr>");
                    categoryHtml.push( self._displayDeliverables(deliverables, confirmationData.category_weights[category_id], confirmationData.total_actions, confirmationData) );
                    total_d_weight += parseInt(confirmationData.category_weights[category_id]['delivered_weight']);
                    total_o_weight += parseInt(confirmationData.category_weights[category_id]['other_weight']);
                    total_q_weight += parseInt(confirmationData.category_weights[category_id]['quality_weight']);
                });
                categoryHtml.push("<tr style='background-color:#d3d3d3;' class='confirm-data'>");
                categoryHtml.push("<td>&nbsp;</td>");
                $.each(confirmationData.columns, function(key, headVal){
                    if(key == 'delivered_weight')
                    {
                        categoryHtml.push("<td style='text-align: right;'><b>"+total_d_weight+"</b></td>");
                    } else if(key == 'other_weight') {
                        categoryHtml.push("<td style='text-align: right;'><b>"+total_o_weight+"</b></td>");
                    } else if(key == 'quality_weight')  {
                        categoryHtml.push("<td style='text-align: right;'><b>"+total_q_weight+"</b></td>");
                    } else {
                        categoryHtml.push("<td></td>");
                    }
                });
                categoryHtml.push("<td>&nbsp;</td>");
                categoryHtml.push("<td>&nbsp;</td>");
                categoryHtml.push("</tr>");
                $("#"+self.options.tableRef+"_deliverable").append( categoryHtml.join(' ')).css({'text-align':'left'});

                self._displayDeliverableActions(confirmationData);
            }

            if(confirmationData.hasOwnProperty('canConfirm') && confirmationData.canConfirm == 1)
            {
                var confirmButton = [];
                confirmButton.push("<tr class='details'>");
                  confirmButton.push("<td colspan='2' class='noborder'>");
                    confirmButton.push("<input type='submit' name='confirm_score_card_"+self.options.scoreCardId+"' id='confirm_score_card_"+self.options.scoreCardId+"' value='Confirm Scorecard' class='confirm_button isubmit'  />");
                  confirmButton.push("</td>");
                confirmButton.push("</tr>");
                $("#confirmation-table").append( confirmButton.join('') ).css({'text-align':'left'});
                $("#confirm_score_card_"+self.options.scoreCardId).live('click', function(e){
                    self._confirmScoreCard();
                    e.preventDefault();
                });
            }

        });
    } ,

    _displayContract            : function(score_card)
    {
        var self = this;
        var scoreCardHtml = [];
        $.each(score_card, function(column, score_card) {
            scoreCardHtml.push("<tr class='confirm-data'>")
                scoreCardHtml.push("<th style='text-align: left;'>"+score_card.header+":</th>");
                scoreCardHtml.push("<td>"+score_card.value+"</td>");
            scoreCardHtml.push("</tr>");
        });
        $("#"+self.options.tableRef+"_score_card").append( scoreCardHtml.join(' ')).css({'text-align':'left'});
    } ,


    _displayDeliverableHeader       : function(headers)
    {
        var self = this;
        var html = [];
        html.push("<tr class='confirm-data'>");
        html.push("<th>&nbsp;</th>");
        $.each( headers , function( index, head ){
            html.push("<th>"+head+"</th>");
        });
        html.push("<th>Number Of Questions</th>");
        if(self.options.updateDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.editDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.addAction)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.viewDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }
        html.push("</tr>");
        $("#"+self.options.tableRef+"_deliverable").append(html.join(' '));
    },

    _displayDeliverables        : function(deliverable_data, category_weights, action_data, confirmationData)
    {
        var self = this;
        var deliverableHtml = [];
        $.each(deliverable_data.deliverables, function(deliverable_id, deliverable){
            if(confirmationData['settings'][deliverable_id]['has_sub'] == 0)
            {
               deliverableHtml.push("<tr class='confirm-data'>");
                 deliverableHtml.push("<td>");
                   deliverableHtml.push("<a href='#' id='deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id+"' class='deliverable_more_less'>");
                     deliverableHtml.push("<span class='ui-icon ui-icon-plus'></span>");
                   deliverableHtml.push("</a>");
                 deliverableHtml.push("</td>");
            } else {
               deliverableHtml.push("<tr>");
                   deliverableHtml.push("<td></td>");
            }
            $.each(deliverable, function(field, val){
                if(field == 'quality_weight' || field == 'delivered_weight' || field == 'other_weight'|| field == 'deliverable_progress')
                {
                    deliverableHtml.push("<td style='text-align: right;'>"+val+"</td>");
                } else{
                    deliverableHtml.push("<td>"+val+"</td>");
                }
            });
            if(action_data.hasOwnProperty(deliverable_id))
            {
                deliverableHtml.push("<td style='text-align: right;'>"+action_data[deliverable_id]+"</td>");
            } else {
                deliverableHtml.push("<td style='text-align: right;'>0</td>");
            }
            deliverableHtml.push(self._deliverableEditButton(confirmationData, deliverable_id));
            deliverableHtml.push("</tr>");
            if(confirmationData.actions.hasOwnProperty(deliverable_id))
            {
                $.each(confirmationData.actions[deliverable_id], function(action_index, action_id){
                    deliverableHtml.push("<tr id='deliverable_actions_"+self.options.tableRef+"_"+deliverable_id+"' class='deliverable_actions confirm-data' style='display:none;'>");
                        deliverableHtml.push("<td id='show_deliverable_actions_"+self.options.tableRef+"_"+deliverable_id+"' colspan='"+(parseInt(confirmationData.total_columns)+3)+"'></td>");
                    deliverableHtml.push("</tr>");
                })
            }
            if(deliverable_data.hasOwnProperty('sub_deliverable'))
            {
                if(deliverable_data['sub_deliverable'].hasOwnProperty(deliverable_id))
                {
                    $.each(deliverable_data['sub_deliverable'][deliverable_id], function(sub_deliverable_id, sub_deliverable){
                        deliverableHtml.push("<tr class='view-notowner confirm-data'>");
                            deliverableHtml.push("<td>");
                                deliverableHtml.push("<a href='#' id='sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id+"' class='sub_deliverable_more_less'>");
                                    deliverableHtml.push("<span class='ui-icon ui-icon-plus'></span>");
                                deliverableHtml.push("</a>");
                            deliverableHtml.push("</td>");
                        $.each(sub_deliverable, function(sub_field, sub_val) {
                            if(sub_field == 'quality_weight' || sub_field == 'delivered_weight' || sub_field == 'other_weight')
                            {
                                deliverableHtml.push("<td style='text-align: right;'>"+sub_val+"</td>");
                            } else{
                                deliverableHtml.push("<td>"+sub_val+"</td>");
                            }
                        });
                        if(action_data.hasOwnProperty(sub_deliverable_id))
                        {
                            deliverableHtml.push("<td style='text-align: right;'>"+action_data[sub_deliverable_id]+"</td>");
                        }
                        deliverableHtml.push(self._deliverableEditButton(confirmationData, sub_deliverable_id));
                        deliverableHtml.push("</tr>");
                        if(confirmationData.actions.hasOwnProperty(sub_deliverable_id))
                        {
                            $.each(confirmationData.actions[sub_deliverable_id], function(action_index, action_id){
                                deliverableHtml.push("<tr id='sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id+"' class='deliverable_actions confirm-data' style='display:none;'>");
                                    deliverableHtml.push("<td id='show_sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id+"' colspan='"+(parseInt(confirmationData.total_columns) + 3)+"'></td>");
                                deliverableHtml.push("</tr>");
                            });
                            //$("#show_sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id).action({deliverable_id:sub_deliverable_id, deliverableRef:'sub_'+sub_deliverable_id , section:self.options.section, thClass:'th2', editAction: self.options.editAction});
                        }
                        $("#sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id).live("click", function(e){
                            if( $("#sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id).is(":hidden") )
                            {
                                $("#sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id+" span").removeClass("ui-icon-closethick").addClass("ui-icon-minus");
                            } else {
                                $("#sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");
                            }
                            $("#sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id).toggle();
                            e.preventDefault();
                        });

                    });
                }
            }
            $("#deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id).live("click", function(e){
                if( $("#deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).is(":hidden") )
                {
                    $("#deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id+" span").removeClass("ui-icon-closethick").addClass("ui-icon-minus");
                } else {
                    $("#deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");
                }
                $("#deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).toggle();
                e.preventDefault();
            });
        })
        deliverableHtml.push("<tr class='confirm-data'>");
        deliverableHtml.push("<td>&nbsp;</td>");
        $.each(confirmationData.columns, function(key, headVal){
            if(key == 'delivered_weight')
            {
                deliverableHtml.push("<td style='text-align: right;'>"+category_weights['delivered_weight']+"</td>");
            } else if(key == 'other_weight') {
                deliverableHtml.push("<td style='text-align: right;'>"+category_weights['other_weight']+"</td>");
            } else if(key == 'quality_weight')  {
                deliverableHtml.push("<td style='text-align: right;'>"+category_weights['quality_weight']+"</td>");
            } else {
                deliverableHtml.push("<td style='text-align: right;'></td>");
            }
        });
        deliverableHtml.push("<td>&nbsp;</td>");
        deliverableHtml.push("<td>&nbsp;</td>");
        deliverableHtml.push("</tr>");

        return deliverableHtml.join(' ');
    } ,


    _displayDeliverableActions  : function(confirmationData)
    {
        var self = this;
        $.each(confirmationData.settings, function(deliverable_id, deliverable){
          if(deliverable.has_sub == 0)
          {
             if(confirmationData['actions'].hasOwnProperty(deliverable_id) && confirmationData['actions'][deliverable_id].length > 0)
             {
                $("#show_deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).action({deliverableId:deliverable_id, deliverableRef:'main_no_sub_'+deliverable_id, section:self.options.section, thClass:'th2', editAction: self.options.editAction, autoLoad: true, financialYear:self.options.financialYear, allowFilter: false, page: 'confirmation', scoreCardId: self.options.scoreCardId});
             }
          }

          if(deliverable.is_main == 0)
          {
              if(confirmationData['actions'].hasOwnProperty(deliverable_id) && confirmationData['actions'][deliverable_id].length > 0)
              {
                  $("#show_sub_deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).action({deliverableId:deliverable_id, deliverableRef:'sub_'+deliverable_id, section:self.options.section, thClass:'th2', editAction: self.options.editAction, autoLoad: true, financialYear:self.options.financialYear, allowFilter: false, page: 'confirmation', scoreCardId: self.options.scoreCardId });
              }
          }
        });

    },


    _displaySummary             : function(key_measures, categories)
    {
        var self = this;
        var summaryHtml = []
        var total_weight = 0;
        summaryHtml.push("<tr class='confirm-data'>");
        summaryHtml.push("<th>Key Measures</th>");
        summaryHtml.push("<th>Weights</th>");
        summaryHtml.push("</tr>");
        $.each(key_measures, function(category, category_deliverable_summary){
            var total_category_weight = 0;
            summaryHtml.push("<tr class='confirm-data'>");
            summaryHtml.push("<td style='text-align: left;' colspan='2'><b>"+categories[category]+"</b></td>");
            summaryHtml.push("</tr>");
            $.each(category_deliverable_summary, function(deliverable_id, deliverable_summary){
                summaryHtml.push("<tr class='confirm-data'>");
                summaryHtml.push("<td style='text-align: left;'>"+deliverable_summary.deliverable_name+"</td>");
                summaryHtml.push("<td style='text-align: right;'>"+(isNaN(deliverable_summary.total_weight) ? 0 : deliverable_summary.total_weight)+"</td>");
                summaryHtml.push("</tr>");
                total_category_weight = parseInt(deliverable_summary.total_weight) + parseInt(total_category_weight);
            });
            total_weight          = parseInt(total_category_weight) + parseInt(total_weight);
            summaryHtml.push("<tr class='view-notowner confirm-data'>");
            summaryHtml.push("<td style='text-align: left;'>Total for "+categories[category]+"</td>");
            summaryHtml.push("<td style='text-align: right;' colspan='2'><b>"+(isNaN(total_category_weight) ? 0: total_category_weight)+"</b></td>");
            summaryHtml.push("</tr>");
        });
        summaryHtml.push("<tr>");
        summaryHtml.push("<td style='text-align: left;'>Score</td>");
        summaryHtml.push("<td style='text-align: right;' colspan='2'><b>"+(isNaN(total_weight) ? 0 : total_weight)+"</b></td>");
        summaryHtml.push("</tr>");
        $("#"+self.options.tableRef+"_summary").append(summaryHtml.join(' ')).css({'text-align':'left'});
    },

    _deliverableEditButton          : function(deliverableData, deliverable_id)
    {
        var self = this;
        if(self.options.editDeliverable)
        {
            var html = [];
            html.push("<td>&nbsp;");
            if(deliverableData.hasOwnProperty('canEdit'))
            {
                if(deliverableData.canEdit.hasOwnProperty(deliverable_id))
                {
                    if(deliverableData.canEdit[deliverable_id] == true)
                    {
                        html.push("<input type='button' value='Edit heading' id='edit_deliverable_"+deliverable_id+"' id='edit_deliverable_"+deliverable_id+"' />");
                        $("#edit_deliverable_"+deliverable_id).live("click", function(){
                            //document.location.href = "edit_deliverable.php?id="+deliverable_id;
                            document.location.href = "edit_deliverable.php?id="+deliverable_id+"&from="+self.options.page+"&score_card_id="+self.options.scoreCardId;
                            return false;
                        });
                    }
                }
            }
            html.push("</td>");
            return html.join(' ');
        }
        return '';
    } ,

    _confirmScoreCard            : function()
    {
        var self = this;
        if($("#confirm_score_card_dialog_"+self.options.scoreCardId).length > 0)
        {
            $("#confirm_score_card_dialog_"+self.options.scoreCardId).remove();
        }

        $("<div />",{id:"confirm_score_card_dialog_"+self.options.scoreCardId, html:"I hereby:"})
            .append($("<ul />")
                .append($("<li />",{html:"Confirm that I have reviewed all the Headings of the Scorecard;"}))
                .append($("<li />",{html:"Confirm that I have reviewed all the Questions of the Scorecard;"}))
                .append($("<li />",{html:"Declare that I have the required authority to confirm this Scorecard."}))
            )
            .append($("<div />",{id:"message"}).css({"padding":"5px"}))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Scorecard confirm",
                buttons     : {
                    "Confirm"   : function()
                    {
                        $("#message").addClass("ui-state-info").html("confirming scorecard ... <img src='../images/loaderA32.gif' />");
                        $.getJSON("../controllers/scorecardcontroller.php?action=confirm",{
                            score_card_id     : self.options.scoreCardId
                        }, function(response) {
                            if(response.error)
                            {
                                $("#message").html(response.text).addClass('ui-state-error');
                            } else {
                                $("#message").html(response.text).addClass('ui-state-ok');
                                setTimeout(function(){
                                    document.location.reload();
                                }, 1000)
                            }
                        });
                    },
                    "Cancel"    : function()
                    {
                        $("#confirm_score_card_dialog_"+self.options.scoreCardId).dialog("destroy").remove()
                    }
                } ,
                "close"     : function(event, ui)
                {
                    $("#confirm_score_card_dialog_"+self.options.scoreCardId).dialog("destroy").remove()
                },
                "open"      : function(event, ui)
                {
                    var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                    var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
                    var saveButton = buttons[0];
                    $(saveButton).css({"color":"#090"});
                }
            });
    }

});