$.widget("contract.actionauthorization",{

    options     : {
        tableId		: "action_authorization_"+(Math.floor(Math.random(78)*89)),
        action_id	: 0,
        start		: 0,
        limit		: 10,
        current		: 1,
        total		: 1
    } ,

    _init       : function()
    {
        this._getActionAuthorization();
    },

    _create     : function()
    {
        var self = this;
        var html = [];
        html.push("<table width='100%' id='"+self.options.tableId+"'>");
            html.push("<tr>");
                html.push("<td colspan='8'><h4>Action Authorization</h4></td>");
            html.push("</tr>");
        html.push("</table>");

        $(self.element).append(html.join(' '));
    } ,

    _getActionAuthorization  : function()
    {
        var self = this;
        $("body").append($("<div />",{id:"contractLoadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
        );
        $.getJSON("../controllers/actionauthorizationcontroller.php?action=getActionAuthorizations", {
            action_id   : self.options.action_id,
            start       : self.options.start,
            limit       : self.options.limit
        }, function(actionAuthorization) {
            $("#contractLoadingDiv").remove();
            $(".action_authorization").html("");
            self._displayPager(actionAuthorization.total);
            self._headers();
            var html = [];
            html.push("<tr class='action_authorization'>");
                html.push("<td colspan='8'><input type='button' name='add' id='add' value='Add New' /></td>");
            html.push("</tr>");

            $("#add").live("click", function(e) {
                self._addActionAuthorization();
                e.preventDefault();
            });

            if(!$.isEmptyObject(actionAuthorization.authorizations))
            {
                self._display(actionAuthorization.authorizations);
            } else {
                html.push("<tr class='action_authorization'>");
                    html.push("<td colspan='8'>There are no Action Authorization</td>");
                html.push("</tr>");
            }
            $("#"+self.options.tableId).append(html.join(' '));
        });
    },

    _display            : function(actionAuthorization)
    {
        var self = this;
        var html = [];
        $.each(actionAuthorization, function(index, authorization){
            html.push("<tr id='authorization_"+authorization.id+"' class='action_authorization'>");
                html.push("<td>"+authorization.id+"</td>");
                html.push("<td>"+authorization.date_time+"</td>");
                html.push("<td>"+authorization.date_tested+"</td>");
                html.push("<td>"+authorization.response+"</td>");
                html.push("<td>"+authorization.authorization_status+"</td>");
                html.push("<td>"+authorization.authorization_provider+"</td>");
                html.push("<td>"+authorization.attachments+"</td>");
                html.push("<td>");
                    html.push("<input type='button' name='edit_"+authorization.id+"' id='edit_"+authorization.id+"' value='Edit' />");
                    html.push("<input type='button' name='del_"+authorization.id+"' id='del_"+authorization.id+"' value='Delete' />")
                html.push("</td>");
            html.push("</tr>");
            $("#edit_"+authorization.id).live("click", function(e){
                self._editAuthorization(authorization);
                e.preventDefault();
            });

            $("#del_"+authorization.id).live("click", function(e){
                if(confirm("Are you sure you want to delete this action authorization"))
                {
                    self._deleteAuthorization(authorization);
                }
                e.preventDefault();
            });
        });
        $("#"+self.options.tableId).append(html.join(' '));
    } ,

    _headers            : function()
    {
        var self = this;
        var html = [];
        html.push("<tr class='action_authorization'>");
            html.push("<th>Ref</th>");
            html.push("<th>System Date and Time</th>");
            html.push("<th>Date Tested</th>");
            html.push("<th>Response</th>");
            html.push("<th>Status</th>");
            html.push("<th>Authorization Provider</th>");
            html.push("<th>Attachment</th>");
            html.push("<th>&nbsp;</th>");
        html.push("<tr>");
        $("#"+self.options.tableId).append(html.join(' '));
    } ,

    _displayPager       : function()
    {

    } ,

    _addActionAuthorization      : function()
    {
        var self = this;
        var html = [];
        html.push("<form id='action_authorization_form' name='action_authorization_form'>");
            html.push("<table width='100%'>");
                html.push("<tr>");
                html.push("<th>Response:</th>");
                    html.push("<td>");
                        html.push("<textarea cols='50' rows='7' name='response' id='response'></textarea>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Action On:</th>");
                    html.push("<td>");
                        html.push("<input type='text' name='date_tested' id='date_tested' value='' readonly='readonly' class='datepicker' />");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                html.push("<th>Status:</th>");
                    html.push("<td>");
                        html.push("<select name='sign_off_id' id='sign_off_id'></select>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Attachment:</th>");
                    html.push("<td>");
                        html.push("<input type='file' name='action_authorization_attachment' id='action_authorization_attachment' class='action_upload' />");
                            html.push("<p class='ui-state ui-state-info'><small>You can attach more than 1 file.</small></p>");
                        html.push("<p id='file_upload'></span>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<td colspan='2'><span style='padding:5px' id='msg'></span></td>");
                html.push("</tr>");
            html.push("</table>");
        html.push("</form>");
        if($("#action_authorization_dialog").length > 0)
        {
            $("#action_authorization_dialog").remove();
        }

        $("<div />",{id:"action_authorization_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Action Authorization",
                width       : 'auto',
                buttons     : {
                    "Save"  : function()
                    {
                        $("#msg").addClass("ui-state-info").html("saving . . . <img  src='../images/loaderA32.gif' />");
                        if($("#response").val() == "")
                        {
                            $("#msg").addClass("ui-state-error").html("Please enter the response");
                        } else if($("#signoff").val() == "") {
                            $("#msg").addClass("ui-state-error").html("Please select the status");
                        } else {
                            $.post("../controllers/actionauthorizationcontroller.php?action=saveActionAuthorization",{
                                authorization_data : $("#action_authorization_form").serialize(),
                                action_id      : self.options.action_id
                            },function(response) {
                                if(response.error)
                                {
                                    jsDisplayResult("error", "error", response.text);
                                } else {
                                    jsDisplayResult("ok", "ok", response.text);
                                    setTimeout(function(){
                                        self._getActionAuthorization();
                                        $("#action_authorization_dialog").dialog("destroy").remove();
                                    }, 1000)
                                }
                            },"json");
                        }
                    } ,

                    "Cancel"    : function()
                    {
                        $("#action_authorization_dialog").dialog("destroy").remove();
                    }
                } ,
                open        : function(event, ui)
                {
                    $(".datepicker").datepicker({
                        showOn: 'both',
                        buttonImage: '/library/jquery/css/calendar.gif',
                        buttonImageOnly: true,
                        dateFormat: 'dd-M-yy',
                        changeMonth:true,
                        changeYear:true
                    });
                } ,
                close       : function(event, ui)
                {
                    $("#action_authorization_dialog").dialog("destroy").remove();
                }
            });
        self._loadActionAuthorizationStatuses();
        $(".action_upload").live("change", function(){
            if($(this).val() !== "")
            {
                self._uploadAttachment(this.id);
            }
        });

    },

    _editAuthorization      : function(authorization)
    {
        var self = this;
        var html = [];
        html.push("<form id='edit_action_authorization_form' name='edit_action_authorization_form'>");
            html.push("<table width='100%'>");
                html.push("<tr>");
                    html.push("<th>Response:</th>");
                    html.push("<td>");
                        html.push("<textarea cols='50' rows='7' name='response' id='response'>"+authorization.response+"</textarea>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Action On:</th>");
                    html.push("<td>");
                        html.push("<input type='text' name='date_tested' id='date_tested' value='"+authorization.date_tested+"' class='datepicker' />");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Status:</th>");
                    html.push("<td>");
                        html.push("<select name='sign_off_id' id='sign_off_id'></select>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Attachment:</th>");
                    html.push("<td>");
                        html.push("<input type='file' name='action_authorization_attachment' id='action_authorization_attachment' class='action_upload' />");
                        html.push("<p class='ui-state ui-state-info'><small>You can attach more than 1 file.</small></p>");
                        html.push("<p>"+authorization.editable_attachments+"</p>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<td colspan='2'><p style='padding:5px' id='msg'></p></td>");
                html.push("</tr>");
            html.push("</table>");
        html.push("</form>");
        if($("#edit_action_authorization_dialog").length > 0)
        {
            $("#edit_action_authorization_dialog").remove();
        }

        $("<div />",{id:"edit_action_authorization_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Edit Action Assurrance",
                width       : 'auto',
                buttons     : {
                    "Save Changes"  : function()
                    {
                        $("#msg").addClass("ui-state-info").html("updating . . . <img  src='../images/loaderA32.gif' />");
                        if($("#response").val() == "")
                        {
                            $("#msg").addClass("ui-state-error").html("Please enter the response");
                        } else {
                            $.post("../controllers/actionauthorizationcontroller.php?action=updateActionAuthorization",{
                                authorization_data : $("#edit_action_authorization_form").serialize(),
                                id              : authorization.id
                            },function(response) {
                                if(response.error)
                                {
                                    jsDisplayResult("error", "error", response.text);
                                } else {
                                    if(response.updated)
                                    {
                                        jsDisplayResult("ok", "ok", response.text);
                                        self._getActionAuthorization();
                                    } else {
                                        jsDisplayResult("info", "info", response.text);
                                    }
                                }
                                $("#edit_action_authorization_dialog").dialog("destroy").remove();
                            },"json");
                        }
                    } ,

                    "Cancel"    : function()
                    {
                        $("#edit_action_authorization_dialog").dialog("destroy").remove();
                    }
                } ,
                open        : function(event, ui)
                {
                    $(".datepicker").datepicker({
                        showOn: 'both',
                        buttonImage: '/library/jquery/css/calendar.gif',
                        buttonImageOnly: true,
                        dateFormat: 'dd-M-yy',
                        changeMonth:true,
                        changeYear:true
                    });
                } ,
                close       : function(event, ui)
                {
                    $("#edit_action_authorization_dialog").dialog("destroy").remove();
                }
            });
        self._loadActionAuthorizationStatuses(authorization.signoff);
        $(".action_upload").live("change", function(){
            if($(this).val() !== "")
            {
                self._uploadAttachment(this.id);
            }
        });

        $(".remove_attach").live("click", function(e){
            var id       = this.id
            var ext      = $(this).attr('title');
            var type     = $(this).attr('ref');
            var filename = $(this).attr("file");
            var document_id = $(this).attr("file");
            $.post('../controllers/actionauthorizationcontroller.php?action=deleteAttachment',
                {
                    attachment  : id,
                    ext         : ext,
                    type        : type,
                    filename    : filename,
                    document_id : document_id,
                    id          : authorization.id
                }, function(response){
                    if(response.error)
                    {
                        $("#result_message").css({padding:"5px", clear:"both"}).html(response.text);
                    } else {
                        $("#result_message").addClass('ui-state-ok').css({padding:"5px", clear:"both"}).html(response.text);
                        $("#_li_"+ext).fadeOut();
                        var deleted_doc = "<input type='hidden' name='deleted_authorization_document["+document_id+"]' id='deleted_authorization_document_"+document_id+"' value='"+document_id+"' />";
                        $("#result_message").append(deleted_doc);
                    }
                },'json');
            e.preventDefault();
        });
    } ,

    _loadActionAuthorizationStatuses      : function(id)
    {
        var self = this;
        $.getJSON("../controllers/authorizationstatuscontroller.php?action=getAll", function(actionStatuses){
            var html = [];
            $("#sign_off_id").html('');
            html.push("<option value=''>--please select--</option>");
            $.each(actionStatuses, function(index, status){
                if(id == status.id)
                {
                    html.push("<option value='"+status.id+"' selected='selected'>"+status.client_terminology+"</option>");
                } else {
                    html.push("<option value='"+status.id+"'>"+status.client_terminology+"</option>");
                }
            });
            $("#sign_off_id").append(html.join(' '));
        });
    } ,

    _deleteAuthorization        : function(authorization)
    {
        $.post("../controllers/actionauthorizationcontroller.php?action=deleteActionAuthorization",
            {
                status   : 2,
                id       : authorization.id
            },function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    $("#authorization_"+authorization.id).fadeOut();
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text);
                        self._getActionAuthorization();
                    } else {
                        jsDisplayResult("info", "info", response.text);
                    }
                }
            },"json");
    },

    _uploadAttachment       : function(element_id)
    {
        var self = this;
        $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
        $.ajaxFileUpload
        (
            {
                url			  : '../controllers/actionauthorizationcontroller.php?action=saveAttachment',
                secureuri	  : false,
                fileElementId : 'action_authorization_attachment',
                dataType	  : 'json',
                success       : function (response, status)
                {
                    $("#file_upload").html("");
                    if(response.error )
                    {
                        $("#file_upload").html(response.text )
                    } else {
                        $("#file_upload").html("");
                        if(response.error)
                        {
                            $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                        } else {
                            if($("#result_message"))
                            {
                                $("#result_message").remove();
                            }
                            $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                            ).append($("<div />",{id:"files_uploaded"}))
                            if(!$.isEmptyObject(response.files))
                            {
                                var list = [];
                                list.push("<span>Files uploaded ..</span><br />");
                                $.each(response.files, function(ref, file) {
                                    list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                                        list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                        list.push("<span style='margin-left:5px;'><a href='../controller/contractcontroller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                        list.push("<input type='hidden' name='action_authorization_document["+file.key+"]' value='"+file.name+"' id='action_authorization_document_"+ref+"' />");
                                    list.push("</p>");
                                });
                                $("#files_uploaded").html(list.join(' '));

                                $(".delete").click(function(e){
                                    var id = this.id
                                    var ext = $(this).attr('title');
                                    $.post('../controllers/actionauthorizationcontroller.php?action=removeActionAuthorizationAttachment',
                                    {
                                        attachment : id,
                                        ext        : ext
                                    }, function(response) {
                                        if(response.error)
                                        {
                                            $("#result_message").html(response.text)
                                        } else {
                                            $("#result_message").addClass('ui-state-ok').html(response.text)
                                            $("#li_"+id).fadeOut();
                                        }
                                    },'json');
                                    e.preventDefault();
                                });
                                $("#"+element_id).val("");
                            }
                        }



                        if( $("#delmessage").length != 0)
                        {
                            $("#delmessage").remove()
                        }
                        $("#fileloading").append($("<div />",{id:"upmessage"}).addClass("ui-state-ok").css({"padding":"0.7em"})
                            .append($("<span />").addClass("ui-icon").addClass("ui-icon-check").css({"float":"left", "margin-right":"0.3em"}))
                            .append($("<span />",{html:data.file+" "+data.text}))
                        )
                        $("#fileloading").append($("<ul />",{id:"attachment_list"}))
                        if(!$.isEmptyObject(data.files))
                        {
                            var list = [];
                            $.each(response.files, function(ref, file){
                                list.push("<p id='li_"+ref+"' style='padding:2px;' class='ui-state'>");
                                list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                list.push("<span style='margin-left:5px;'><a href='../class/request.php?action=main.php?controller=action&action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                list.push("</p>");
                                //list += "<li id='li_"+ref+"' style='list-style:none;'><span class='ui-state-icon ui-icon-check'></span><a href='../class/request.php?action=ClientAction.downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></li>";
                            });
                            $("#attachment_list").html(list.join(' '));

                            $("#"+element_id).val("");
                        }
                    }
                },
                error: function (data, status, e)
                {
                    $("#file_upload").html( "Ajax error -- "+e+", please try uploading again");
                }
            }
        )
    }


})
