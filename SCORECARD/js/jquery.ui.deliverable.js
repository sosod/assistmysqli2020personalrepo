$.widget("ui.deliverable", {

    options		: {
        tableId			   : "deliverable_table_"+(Math.floor(Math.random(56) * 33)),
        url 			    : "../controllers/deliverablecontroller.php?action=getDeliverables",
        total 		 	    : 0,
        start			    : 0,
        current			    : 1,
        limit			    : 20000,
        editDeliverable	    : false,
        updateDeliverable   : false,
        addAction           : false,
        editAction          : false,
        updateAction        : false,
        authorizeDeliverable: false,
        assessContract      : false,
        deliverableAssurance: false,
        addDeliverable      :  false,
        viewContract        : false,
        viewAction          : false,
        view			    : "",
        section			    : "",
        page                : "",
        autoLoad            : false,
        showActions         : false,
        financialYear       : 0,
        deliverableOwner    : 0,
        scoreCardId         : 0,
        deliverableId       : 0,
        parentId            : 0,
        showSubDeliverable  : true,
        allowFilter         : true,
        actionFilter        : true
    } ,

    _init          : function()
    {
        if(this.options.autoLoad)
        {
            this._getDeliverables();
        }

    } ,

    _create       : function()
     {
         var self = this;
         var html = [];
         self.options.tableId = self.options.tableId;
         if(self.options.scoreCardId)
         {
             self.options.tableId = self.options.tableId+"_"+self.options.scoreCardId;
         }
         html.push("<table class='noborder' width='100%'>");
             if(self.options.allowFilter)
             {
             html.push("<tr>");
                 html.push("<td class='noborder'>");
                     html.push("<table class='noborder'>");
                         html.push("<tr>");
                             html.push("<th style='text-align:left;'>Financial Year:</th>");
                             html.push("<td class='noborder'>");
                                 html.push("<select id='deliverable_financial_year' name='deliverable_financial_year' style='width:200px;'>");
                                 html.push("<option value=''>--please select--</option>");
                                 html.push("</select>");
                             html.push("</td>");
                         html.push("</tr>");
                         html.push("<tr>");
                             html.push("<th style='text-align:left;'>Scorecard:</th>");
                             html.push("<td class='noborder'>");
                                 html.push("<select id='score_card_id' name='score_card_id' style='width:200px;'>");
                                 html.push("<option value=''>--please select--</option>");
                                 html.push("</select>");
                             html.push("</td>");
                         html.push("</tr>");
                         html.push("<tr>");
                             html.push("<th style='text-align:left;'>Main Heading:</th>");
                             html.push("<td class='noborder'>");
                                 html.push("<select id='parent_id' name='parent_id' style='width:200px;'>");
                                 html.push("<option value=''>--please select--</option>");
                                 html.push("</select>");
                             html.push("</td>");
                         html.push("</tr>");
                     html.push("</table>");
                 html.push("</td>");
             html.push("</tr>");
            }
             html.push("<tr>");
                 html.push("<td class='noborder'>");
                    html.push("<table id='"+self.options.tableId+"' width='100%'>");
                      html.push("<tr>");
                         html.push("<td>");
                           html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                             html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                             html.push("<span style='padding:5px; float:left;'>Select financial year to view the deliverables</span>");
                           html.push("</p>");
                         html.push("</td>");
                      html.push("</tr>");
                    html.push("</table>");
                 html.push("</td>");
             html.push("</tr>");
         html.push("</table>");


         $(this.element).append(html.join(' '));
         self._loadFinancialYears();
         if(!self.options.autoLoad)
         {
            self._getDeliverables();
         }

         $("#deliverable_financial_year").live("change", function(){
             if($(this).val() != "")
             {
               self.options.financialYear = $(this).val();
               self._loadScoreCards();
               self._getDeliverables();
             } else {
               self.options.financialYear  = 0;
               self._loadScoreCards();
               self._getDeliverables();
             }
         });

         $("#score_card_id").live("change", function(){
             if($(this).val() != "")
             {
                 self.options.scoreCardId = $(this).val();
                 self._loadMainDeliverables();
                 self._getDeliverables();
             } else {
                 self.options.scoreCardId = 0;
                 self._loadMainDeliverables();
                 self._getDeliverables();
             }
         });

         $("#parent_id").live("change", function(){
             if($(this).val() != "")
             {
                 self.options.parentId = $(this).val();
                 self._getDeliverables();
             } else {
                 self.options.parentId = 0;
                 self._getDeliverables();
             }
         });
     } ,

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#deliverable_financial_year").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
            });
            $("#deliverable_financial_year").html( finHtml.join(' ') );
        });
    } ,

    _loadScoreCards              : function()
    {
        var self = this;
        $("#score_card_id").empty();
        $.getJSON("../controllers/scorecardcontroller.php?action=getAll",{
           financial_year  : self.options.financialYear,
           section         : self.options.section,
           page            : self.options.page
        }, function(score_cards) {
            var scoreCardHtml = [];
            scoreCardHtml.push("<option value=''>--please select--</option>");
            $.each(score_cards, function(score_card_id, score_card) {
                if(score_card_id == self.options.scoreCardId)
                {
                   scoreCardHtml.push("<option value='"+score_card_id+"' selected='selected'>"+score_card.short_description+"</option>");
                } else {
                   scoreCardHtml.push("<option value='"+score_card_id+"'>"+score_card.short_descriptionscore_card.short_description+"</option>");
                }
            })
            $("#score_card_id").html( scoreCardHtml.join(' ') );
        });
    },

    _loadMainDeliverables              : function()
    {
        var self = this;
        $("#parent_id").empty();
        $.getJSON("../controllers/deliverablecontroller.php?action=getAll", {
           financial_year      : self.options.financialYear,
           score_card_id       : self.options.scoreCardId,
           parent_id           : self.options.parentId,
           is_main_deliverable : 1,
           section             : self.options.section,
           page                : self.options.page
        } ,function(deliverables) {
            var deliverableHtml = [];
            deliverableHtml.push("<option value=''>--please select--</option>");
            $.each(deliverables ,function(deliverable_id, deliverable) {
                deliverableHtml.push("<option value='"+deliverable_id+"'>"+deliverable+"</option>");
            })
            $("#parent_id").html( deliverableHtml.join(' ') );
        });
    },

    _getDeliverables      : function()
     {
         var self = this;
         $("body").append($("<div />",{id:"loadingDiv", html:"Loading deliverables . . . <img src='../images/loaderA32.gif' />"})
             .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
         );
         $.getJSON( self.options.url,{
             start		          : self.options.start,
             limit		          : self.options.limit,
             view		          : self.options.view,
             section		      : self.options.section,
             page                 : self.options.page,
             financial_year       : self.options.financialYear,
             deliverable_owner    : self.options.deliverableOwner,
             score_card_id        : self.options.scoreCardId,
             deliverable_id       : self.options.deliverableId,
             is_main_deliverable  : 0,
             parent_id            : self.options.parentId
         },function(deliverableData) {
             $("#loadingDiv").remove();
             $("#"+self.options.tableId).html("");
             $(".more_less").remove();
             if(deliverableData.financial_year != 0 || deliverableData.financial_year != '')
             {
                 self.options.financialYear = deliverableData.financial_year;
                 self._loadScoreCards();
                 $("#deliverable_financial_year").val(deliverableData.financial_year);
             }
             if(deliverableData.total != null)
             {
                 self.options.total = deliverableData.total
             }

             self._displayPaging(deliverableData.total_columns);
             self._displayHeaders(deliverableData.columns);
             if($.isEmptyObject(deliverableData.deliverables))
             {
                 var _html = [];
                 _html.push("<tr>")
                     _html.push("<td colspan='"+(deliverableData.total_columns+2)+"'>");
                     _html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                         _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                         if(deliverableData.hasOwnProperty('deliverable_message') && deliverableData.deliverable_message != '')
                         {
                             _html.push("<span style='padding:5px; float:left;'>"+deliverableData.deliverbale_message+"</span>");
                         } else {
                             _html.push("<span style='padding:5px; float:left;'>There are no headings found</span>");
                         }
                     _html.push("</p>");
                     _html.push("</td>");
                 _html.push("</tr>");
                 $("#"+self.options.tableId).append( _html.join(' ') );
             } else {
                 self._display(deliverableData.deliverables, deliverableData.sub_deliverable, deliverableData.total_columns, deliverableData, deliverableData.actions);
             }
         });
     } ,

     _display           : function(deliverables, sub_deliverables, columns, deliverableData, actions)
     {
         if($(".deliverable_more_less").length > 0)
         {
             $(".deliverable_more_less").remove();
         }
         if($(".sub_deliverable").length > 0)
         {
             $(".sub_deliverable").remove();
         }
         var self = this;
         var i    = 1;
         $.each(deliverables, function(index, deliverable){
             i++;
             var html = [];
             if(self.options.page == "view" && deliverableData.hasOwnProperty('canUpdate'))
             {
                 if(deliverableData.canUpdate.hasOwnProperty(index) && deliverableData.canUpdate[index] == true)
                 {
                     html.push("<tr>");
                 } else {
                     html.push("<tr class='view-notowner'>");
                 }
             } else {
                html.push("<tr>");
             }
             if(self.options.showSubDeliverable)
             {
                 html.push("<td>");
                     if(deliverableData.settings[index]['has_sub'] == 1)
                     {
                         html.push("<a href='#' id='deliverable_more_less_"+self.options.tableId+"_"+index+"' class='deliverable_more_less'></a>");
                     } else {
                         html.push("<a href='#' id='deliverable_more_less_"+self.options.tableId+"_"+index+"' class='deliverable_more_less'>");
                            html.push("<span class='ui-icon ui-icon-plus'></span>");
                         html.push("</a>");
                     }
                 html.push("</td>");
             }
             $.each( deliverable, function(key , val){
                 if(key == 'quality_weight' || key == 'delivered_weight' || key == 'other_weight' || key == 'deliverable_progress')
                 {
                     html.push("<td style='text-align: right;'>"+val+"</td>");
                 } else{
                     html.push("<td>"+val+"</td>");
                 }
             });

             html.push( self._updateButton(deliverableData, index) );
             html.push( self._editButton(deliverableData, index) );
             html.push( self._addActionButton(deliverableData, index) );
             html.push( self._viewDeliverableButton(deliverableData, index)) ;
             html.push( self._deliverableAssuranceButton(deliverableData, index)) ;
             html.push( self._deliverableAuthorizationButton(deliverableData, index)) ;
             html.push("</tr>");
             if(self.options.showActions)
             {
                 if(!$.isEmptyObject(actions))
                 {
                     if(actions.hasOwnProperty(index))
                     {
                         html.push("<tr id='deliverable_actions_"+self.options.tableId+"_"+index+"' class='deliverable_actions' style='display:none;'>");
                            html.push("<td id='show_deliverable_actions_"+self.options.tableId+"_"+index+"' colspan='"+(parseInt(columns)+2)+"'></td>");
                         html.push("</tr>");
                     }
                 }
             }
             if(self.options.showSubDeliverable)
             {
                 if(!$.isEmptyObject(sub_deliverables))
                 {
                     if(sub_deliverables.hasOwnProperty(index))
                     {
                         $.each(sub_deliverables[index], function(sub_index, sub_deliverable){
                             html.push("<tr class='view-notowner'>");
                             if(self.options.showSubDeliverable)
                             {
                                 html.push("<td>");
                                    html.push("<a href='#' id='sub_deliverable_more_less_"+self.options.tableId+"_"+sub_index+"' class='sub_deliverable_more_less'>");
                                      html.push("<span class='ui-icon ui-icon-plus'></span>");
                                    html.push("</a>");
                                 html.push("</td>");
                             }
                             $.each(sub_deliverable, function(sub_field, sub_val){
                                 if(sub_field == 'quality_weight' || sub_field == 'delivered_weight' || sub_field == 'other_weight' || sub_field == 'deliverable_progress')
                                 {
                                     html.push("<td style='text-align: right;'>"+sub_val+"</td>");
                                 } else{
                                     html.push("<td>"+sub_val+"</td>");
                                 }
                             });
                             html.push( self._updateButton(deliverableData, sub_index) );
                             html.push( self._editButton(deliverableData, sub_index) );
                             html.push( self._addActionButton(deliverableData, sub_index) );
                             html.push( self._viewDeliverableButton(deliverableData, sub_index));
                             html.push( self._deliverableAssuranceButton(deliverableData, index)) ;
                             html.push("</tr>");
                             if(self.options.showActions)
                             {
                                 if(!$.isEmptyObject(actions))
                                 {
                                     if(actions.hasOwnProperty(sub_index))
                                     {
                                         html.push("<tr id='sub_deliverable_actions_"+self.options.tableId+"_"+sub_index+"' class='deliverable_actions' style='display:none;'>");
                                            html.push("<td id='show_sub_deliverable_actions_"+self.options.tableId+"_"+sub_index+"' colspan='"+(parseInt(columns)+2)+"'></td>");
                                         html.push("</tr>");
                                     }
                                 }
                             }

                             $("#sub_deliverable_more_less_"+self.options.tableId+"_"+sub_index).live("click", function(e){
                                 if( $("#sub_deliverable_actions_"+self.options.tableId+"_"+sub_index).is(":hidden") )
                                 {
                                     $("#sub_deliverable_more_less_"+self.options.tableId+"_"+sub_index+" span").removeClass("ui-icon-plus").stop().addClass("ui-icon-minus");
                                 } else {
                                     $("#sub_deliverable_more_less_"+self.options.tableId+"_"+sub_index+" span").removeClass("ui-icon-minus").stop().addClass("ui-icon-plus");
                                 }
                                 $("#sub_deliverable_actions_"+self.options.tableId+"_"+sub_index).toggle();
                                 e.preventDefault();
                             });
                         });
                     }
                 }
             }
             $("#"+self.options.tableId).append( html.join(' ') ).find('.remove_attach').remove();
             $("#"+self.options.tableId).find("#result_message").remove();

             $("#deliverable_more_less_"+self.options.tableId+"_"+index).live("click", function(e){
                 if( $("#deliverable_actions_"+self.options.tableId+"_"+index).is(":hidden") )
                 {
                     $("#deliverable_more_less_"+self.options.tableId+"_"+index+" span").removeClass("ui-icon-plus").addClass("ui-icon-minus");
                 } else {
                     $("#deliverable_more_less_"+self.options.tableId+"_"+index+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");
                 }
                 $("#deliverable_actions_"+self.options.tableId+"_"+index).toggle();
                 e.preventDefault();
             });

             if(self.options.showActions)
             {
                 if(!$.isEmptyObject(deliverables))
                 {
                     if(actions.hasOwnProperty(index))
                     {
                         var action_section = self.options.section;
                         if(self.options.updateDeliverable)
                         {
                             action_section = 'view'
                         }
                        $("#show_deliverable_actions_"+self.options.tableId+"_"+index).action({deliverableId:index, deliverableRef:'main_'+index, section:action_section, showActions:self.options.showActions, page:self.options.page, thClass:'th2', editAction: self.options.editAction, viewAction: self.options.viewAction, financialYear:self.options.financialYear, autoLoad: true, allowFilter : self.options.actionFilter});
                     }
                 }

                 if(!$.isEmptyObject(sub_deliverables))
                 {
                     if(sub_deliverables.hasOwnProperty(index))
                     {
                         $.each(sub_deliverables[index], function(sub_deliverable_index, sub_deliverable){
                             if(actions.hasOwnProperty(sub_deliverable_index))
                             {
                                var action_section = self.options.section;
                                if(self.options.updateDeliverable)
                                {
                                    action_section = 'view'
                                }
                                $("#show_sub_deliverable_actions_"+self.options.tableId+"_"+sub_deliverable_index).action({deliverableId:sub_deliverable_index, deliverableRef:'sub_'+sub_deliverable_index , section:action_section, showActions:self.options.showActions, page:self.options.page, thClass:'th2', editAction: self.options.editAction, viewAction: self.options.viewAction, financialYear:self.options.financialYear, autoLoad: true, allowFilter : self.options.actionFilter});
                             }
                         })
                     }
                 }
             }
         });
     },

    _updateButton        : function(deliverableData, deliverableId)
    {
        var self = this;
        if(self.options.updateDeliverable)
        {
            var html = []
            html.push("<td>&nbsp;");
            if(deliverableData.hasOwnProperty('canUpdate'))
            {
                if(deliverableData.canUpdate.hasOwnProperty(deliverableId))
                {
                    if(deliverableData.canUpdate[deliverableId] == true || self.options.section == "admin")
                    {
                        html.push("<input type='button' value='Update deliverable' id='update_"+deliverableId+"' id='update_"+deliverableId+"' />");
                        $("#update_"+deliverableId).live("click", function(){
                            document.location.href = "update_deliverable.php?id="+deliverableId;
                            return false;
                        });
                    }
                }
            }
            html.push("</td>");
            return html.join('');
        }
       return '';
    } ,

    _deliverableAuthorizationButton    : function(deliverableData, deliverableId)
    {
        var self = this;
        if(self.options.authorizeDeliverable)
        {
            var html = []
            html.push("<td>&nbsp;");
            html.push("<input type='button' value='Authorize' name='authorize_deliverabe_"+deliverableId+"' id='authorize_deliverabe_"+deliverableId+"' />");
            $("#authorize_deliverabe_"+deliverableId).live("click", function(){
                document.location.href = "authorize_deliverable.php?id="+deliverableId;
                return false;
            });
            html.push("</td>");
            return html.join('');
        }
        return '';
    },

    _deliverableAssuranceButton        : function(deliverableData, deliverableId)
    {
        var self = this;
        if(self.options.deliverableAssurance)
        {
            var html = []
            html.push("<td>&nbsp;");
                html.push("<input type='button' value='Assurance' name='assurance_"+deliverableId+"' id='assurance_"+deliverableId+"' />");
                $("#assurance_"+deliverableId).live("click", function(){
                    document.location.href = "deliverable_assurance.php?id="+deliverableId;
                    return false;
                });
            html.push("</td>");
            return html.join('');
        }
        return '';
    } ,

    _editButton          : function(deliverableData, deliverableId)
    {
        var self = this;
        if(self.options.editDeliverable)
        {
            var html = [];
            html.push("<td>&nbsp;");
            if(deliverableData.hasOwnProperty('canEdit'))
            {
                if(deliverableData.canEdit.hasOwnProperty(deliverableId))
                {
                    if(deliverableData.canEdit[deliverableId] == true)
                    {
                        html.push("<input type='button' value='Edit heading' name='edit_deliverable_"+deliverableId+"' id='edit_deliverable_"+deliverableId+"' />");
                        $("#edit_deliverable_"+deliverableId).live("click", function(){
                            document.location.href = "edit_deliverable.php?id="+deliverableId+"&from="+self.options.page+"&score_card_id="+self.options.scoreCardId;
                            return false;
                        });
                    }
                }
            }
            if(self.options.section == "new")
            {
                html.push("<input type='button' value='Delete' name='delete_deliverable_"+deliverableId+"' id='delete_deliverable_"+deliverableId+"' class='idelete' />");
                $("#delete_deliverable_"+deliverableId).live("click", function(){
                    if(confirm('Are you sure you want to delete this heading'))
                    {
                        $.getJSON("../controllers/deliverablecontroller.php?action=deleteDeliverable", {
                           deliverable_id : deliverableId
                        }, function(response){
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                if(response.updated)
                                {
                                    jsDisplayResult("ok", "ok", response.text);
                                    self._getDeliverables();
                                } else {
                                    jsDisplayResult("info", "info", response.text);
                                }
                            }
                        });
                    }
                    return false;
                });
            }
            html.push("</td>");
            return html.join('');
        }
        return '';
    },

    _addActionButton     : function(deliverableData, deliverableId)
    {
        var self = this;
        if(self.options.addAction)
        {
            var html = [];
            html.push("<td>");
            if(deliverableData.hasOwnProperty('canAddAction'))
            {
                if(deliverableData.canAddAction.hasOwnProperty(deliverableId))
                {
                    if(deliverableData.canAddAction[deliverableId] == true)
                    {
                        html.push("<input type='button' value='Add Question' name='add_action_"+deliverableId+"' id='add_action_"+deliverableId+"' />");
                        $("#add_action_"+deliverableId).live("click", function(){
                            document.location.href = "add_action.php?id="+deliverableId;
                            return false;
                        });

                    }
                }
            }
            html.push("</td>");
            return html.join('');
        }
        return '';
    },

    _viewDeliverableButton     : function(delierableData, deliverableId)
    {
        var self = this;
        if(self.options.viewDeliverable)
        {
            var html = [];
            html.push("<td>&nbsp;");
                html.push("<input type='button' value='View heading Details' name='view_deliverable_"+deliverableId+"' id='view_deliverable_"+deliverableId+"' />");
            html.push("</td>");
            $("#view_deliverable_"+deliverableId).live("click", function(){
                document.location.href = "view_deliverable.php?id="+deliverableId;
                return false;
            });
            return html.join(' ');
        }
        return '';
    },

     _displayHeaders     : function(headers)
     {

         var self = this;
         var html = [];
         html.push("<tr>");
         if(self.options.showSubDeliverable)
         {
             html.push("<th>&nbsp;</th>");
         }
         $.each( headers , function( index, head ){
             html.push("<th>"+head+"</th>");
         });
         if(self.options.deliverableAssurance)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.updateDeliverable)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.editDeliverable)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.addAction)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.viewDeliverable)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.authorizeDeliverable)
         {
             html.push("<th>&nbsp;</th>");
         }
         html.push("</tr>");
         $("#"+self.options.tableId).append(html.join(' '));
     },

    _displayPaging	: function(columns)
     {
        var self  = this;
        var html  = [];
        var pages;
        if( self.options.total%self.options.limit > 0){
            pages   = Math.ceil(self.options.total/self.options.limit);
        } else {
            pages   = Math.floor(self.options.total/self.options.limit);
        }
        var scoreCardId = self.options.scoreCardId+"_"+self.options.tableId;

        html.push("<tr>");
            if(self.options.showSubDeliverable)
            {
                html.push("<td>&nbsp;</td>");
            }
            html.push("<td colspan='"+(columns + 1)+"' class='noborder'>");
            html.push("<input type='button' value=' |< ' id='deliverable_first_"+scoreCardId+"' name='first' />");
            html.push("<input type='button' value=' < ' id='deliverable_previous_"+scoreCardId+"' name='previous' />");
            if(pages != 0)
            {
                html.push("Page <select id='deliverable_select_page' name='deliverable_select_page'>");
                for(var p=1; p<=pages; p++)
                {
                    html.push("<option value='"+p+"' "+(self.options.current == p ? "selected='selected'" : "")+">"+p+"</option>");
                }
                html.push("</select>");
            } else {
                html.push("Page <select id='deliverable_select_page' name='deliverable_select_page' disabled='disabled'>");
                html.push("</select>");
            }
            html.push(" of "+(pages == 0 || isNaN(pages) ? 0 : pages)+" pages");
            html.push("<input type='button' value=' > '  id='deliverable_next_"+scoreCardId+"' name='next' />");
            html.push("<input type='button' value=' >| ' id='deliverable_last_"+scoreCardId+"' name='last' />");
            if(self.options.assuranceActions)
            {
                html.push("&nbsp;");
            }
            if(self.options.updateContract)
            {
                html.push("&nbsp;");
            }
            if(self.options.editDeliverable)
            {
                html.push("&nbsp;");
            }
            if(self.options.addAction)
            {
                html.push("&nbsp;");
            }
            if(self.options.viewContract)
            {
                html.push("&nbsp;");
            }
                html.push("<span style='float:right;'>");
                    html.push("<b>Quick Search for Heading:</b> H <input type='text' name='d_id' id='d_id' value='' />");
                    html.push("&nbsp;&nbsp;&nbsp;");
                    html.push("<input type='button' name='deliverable_search' id='deliverable_search' value='Go' />");
                html.push("</span>");
            html.push("</td>");
        html.push("</tr>");
        $("#"+self.options.tableId).append(html.join(' '));


        $("#deliverable_select_page").live("change", function(){
            if($(this).val() !== "" && parseFloat($(this).val())>0)
            {
                self.options.start   = parseFloat($(this).val() -1) * parseFloat(self.options.limit);
                self.options.current = parseFloat($(this).val());
                self._getDeliverables();
            }
        });
        $("#deliverable_search").live("click", function(e){
            var deliverableId = $("#d_id").val();
            if(deliverableId != "")
            {
                self.options.deliverableId = $("#d_id").val();
                self._getDeliverables();
            } else {
                $("#d_id").addClass('ui-state-error');
                $("#d_id").focus().select();
            }
            e.preventDefault();
        });

        if(self.options.current < 2)
        {
            $("#deliverable_first_"+scoreCardId).attr('disabled', 'disabled');
            $("#deliverable_previous_"+scoreCardId).attr('disabled', 'disabled');
        }
        if((self.options.current == pages || pages == 0))
        {
            $("#deliverable_next_"+scoreCardId).attr('disabled', 'disabled');
            $("#deliverable_last_"+scoreCardId).attr('disabled', 'disabled');
        }
        $("#deliverable_next_"+scoreCardId).bind("click", function(evt){
            self._getNext( self );
        });
        $("#deliverable_last_"+scoreCardId).bind("click",  function(evt){
            self._getLast( self );
        });
        $("#deliverable_previous_"+scoreCardId).bind("click",  function(evt){
            self._getPrevious( self );
        });
        $("#deliverable_first_"+scoreCardId).bind("click",  function(evt){
            self._getFirst( self );
        });
    } ,

    _getNext  			: function(self)
    {
        self.options.current = (self.options.current + 1);
        self.options.start   = (self.options.current-1) * self.options.limit;
        this._getDeliverables();
    },

    _getLast  			: function( self )
    {
        self.options.current =  Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
        self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );
        this._getDeliverables();
    },
    _getPrevious    	: function( self )
    {
        self.options.current  = parseFloat(self.options.current) - 1;
        self.options.start 	= (self.options.current-1) * self.options.limit;
        this._getDeliverables();
    },
    _getFirst  			: function( self )
    {
        self.options.current  = 1;
        self.options.start 	= 0;
        this._getDeliverables();
    }


})