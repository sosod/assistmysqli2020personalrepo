$(function(){

    //$(".textcounter").textcounter({maxLength:100});

    $("#rating_scale_type").change(function(){
        RatingScale.get($(this).val());
    })

    $("#add_rating_scale").click(function(e){
        RatingScale.addRatingScale();
        e.preventDefault();
    });

});

var RatingScale 	= {

    get				: function( rating_scale_type )
    {
        $(".rating_scales").remove();
        $.getJSON( "../controllers/ratingscalecontroller.php?action=getAll", {
            rating_scale_type : rating_scale_type
        }, function( ratingScales ) {
            var ratingScaleHtml = [];
            if($.isEmptyObject(ratingScales))
            {
                ratingScaleHtml.push("<tr class='rating_scales'>");
                  ratingScaleHtml.push("<td colspan='8'>There are no rating scales</td>");
                ratingScaleHtml.push("</tr>");
            } else {
                ratingScaleHtml.push( RatingScale.displayHeaders(ratingScales.headers) );
                $.each(ratingScales.values, function( index, rating_scale){

                    ratingScaleHtml.push( RatingScale.display( rating_scale_type, rating_scale, ratingScales['rating_scales'][rating_scale.id] ) );
                });
            }
            $("#rating_scale_table").append( ratingScaleHtml.join(' ') );
        })
    } ,

    display			: function(rating_scale_type, rating_scale, scale)
    {
        var self = this;
        var html  = [];
        html.push("<tr class='rating_scales'>");
        $.each(rating_scale, function(index, val){
            if(index == 'status')
            {
               html.push("<td><b>"+val+"</b></td>");
            } else if(index !== '_status') {
               html.push("<td>"+val+"</td>");
            }
        });
          html.push("<td><input type='button' id='rating_scale_edit_"+rating_scale.id+"' name='rating_scale_edit_"+rating_scale.id+"' value='Edit' /></td>");
        html.push("</tr>");

        $("#rating_scale_edit_"+rating_scale.id).live("click", function(e){
            self._updateRatingScale(rating_scale_type, rating_scale, scale);
            e.preventDefault();
        });
        return html.join(' ');
    },

    displayHeaders          : function( headers )
    {
        var self = this;
        var html = [];
        html.push("<tr class='rating_scales'>");
            $.each(headers, function(index, head){
               html.push("<th>"+head+"</th>");
            });
            html.push("<th></th>");
        html.push("</tr>");
        return html.join(' ');
    } ,

    addRatingScale          : function()
    {
        var self = this;
        if($("#add_rating_scale_dialog").length > 0)
        {
            $("#add_rating_scale_dialog").remove();
        }

        $("<div />",{id:"add_rating_scale_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Rating:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"rating", id:"rating", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Definition :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"definition", id:"definition", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Color:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"color", id:"color", value:''}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Rating Scale",
                width          : 'auto',
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#rating").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the rating");
                            return false;
                        } else if($("#definition").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the definition");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_rating_scale_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_rating_scale_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});

                    var myPicker = new jscolor.color(document.getElementById('color'), {})
                    myPicker.fromString('99FF33')  // now you can access API via 'myPicker' variable
                }
            })
    },

    _updateRatingScale         : function(rating_scale_type, rating_scale, scale)
    {
        var self = this;
        var html = [];
        if($("#edit_rating_scale_dialog").length > 0)
        {
            $("#edit_rating_scale_dialog").remove();
        }
        var title = '';
        html.push("<div>");
          html.push("<table>");
            switch(rating_scale_type)
            {
                case 'symbol':
                    title = "Symbol";
                    html.push("<tr>");
                       html.push("<th>Symbol Rating</th>");
                       html.push("<td>"+rating_scale.numeric_rating+"</td>");
                    html.push("</tr>");
                    html.push("<tr>");
                      html.push("<th>Symbol Rating</th>");
                      html.push("<td><input type='text' name='symbol' id='symbol' value='"+scale.symbol+"' /></td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th>Symbol Rating Description</th>");
                        html.push("<td><textarea name='symbol_rating_description' id='symbol_rating_description'  cols='50' rows='8'>"+scale.symbol_rating_description+"</textarea></td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th>Color</th>");
                        html.push("<td><input type='text' name='color' id='color' value='"+scale.color+"' /></td>");
                    html.push("</tr>");
                    break;
                case 'percentage':
                   title = "Percentage";
                   html.push("<tr>");
                     html.push("<th>Percentage Rating</th>");
                     html.push("<td>"+rating_scale.numeric_rating+"</td>");
                   html.push("</tr>");
                   html.push("<tr>");
                      html.push("<th>Percentage</th>");
                      html.push("<td><input type='text' name='percentage' id='percentage' value='"+scale.percentage+"' /></td>");
                   html.push("</tr>");
                   html.push("<tr>");
                     html.push("<th>Percentage Rating Description</th>");
                     html.push("<td><textarea name='percentage_rating_description' id='percentage_rating_description'  cols='50' rows='8'>"+scale.percentage_rating_description+"</textarea></td>");
                   html.push("</tr>");
                   html.push("<tr>");
                     html.push("<th>Color</th>");
                     html.push("<td><input type='text' name='color' id='color' value='"+scale.color+"' /></td>");
                   html.push("</tr>");
                break;
                case 'faces':
                    html.push("<tr>");
                      html.push("<th>Faces Rating</th>");
                      html.push("<td>"+rating_scale.numeric_rating+"</td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th>Face Rating Description</th>");
                        html.push("<td><textarea name='faces_rating_description' id='faces_rating_description'  cols='50' rows='8'>"+rating_scale.faces_rating_description+"</textarea></td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th>Color</th>");
                        html.push("<td><input type='text' name='color' id='color' value='"+scale.color+"' /></td>");
                    html.push("</tr>");

                    title = "Faces";
                break;
                case 'numeric':
                    html.push("<tr>");
                        html.push("<th>Numeric Rating</th>");
                        html.push("<td>"+rating_scale.numeric_rating+"</td>");
                    html.push("</tr>");
                    html.push("<tr>");
                      html.push("<th>Rating displayed on DropDown</th>");
                      html.push("<td><input type='text' name='definition' id='definition' value='"+rating_scale.rating_definition+"' /></td>");
                    html.push("</tr>");
                    html.push("<tr>");
                      html.push("<th>Numeric Rating Description</th>");
                      html.push("<td><textarea name='description' id='description'  cols='50' rows='8'>"+rating_scale.rating_description+"</textarea></td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th>Color</th>");
                        html.push("<td><input type='text' name='color' id='color' value='"+scale.color+"' /></td>");
                    html.push("</tr>");
                    title = "Numeric";
                break;
                case 'color':
                    html.push("<tr>");
                       html.push("<th>Color Rating</th>");
                       html.push("<td>"+rating_scale.numeric_rating+"</td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th>Colour Rating Description</th>");
                        html.push("<td><textarea name='colour_rating_description' id='colour_rating_description'  cols='50' rows='8'>"+rating_scale.colour_rating_description+"</textarea></td>");
                    html.push("</tr>");
                    html.push("<tr>");
                      html.push("<th>Color</th>");
                      html.push("<td><input type='text' name='color' id='color' value='"+scale.color+"' /></td>");
                    html.push("</tr>");
                    title = "Colour";
                break;
                case 'yes_no':
                    html.push("<tr>");
                        html.push("<th>Yes-No Rating</th>");
                        html.push("<td>"+rating_scale.numeric_rating+"</td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th>Name</th>");
                        html.push("<td><input type='text' name='name' id='name' value='"+scale.name+"' /></td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th>Yes-No Rating Description</th>");
                        html.push("<td><textarea name='yes_no_rating_scale_description' id='yes_no_rating_scale_description'  cols='50' rows='8'>"+rating_scale.yes_no_rating_scale_description+"</textarea></td>");
                    html.push("</tr>");
                    html.push("<tr>");
                      html.push("<th>Color</th>");
                      html.push("<td><input type='text' name='color' id='color' value='"+scale.color+"' /></td>");
                    html.push("</tr>");
                    title = "Yes No";
                break;
                case 'user_defined':
                    html.push("<tr>");
                        html.push("<th>User Defined Rating</th>");
                        html.push("<td>"+rating_scale.numeric_rating+"</td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th>Name</th>");
                        html.push("<td><input type='text' name='definition' id='definition' value='"+scale.definition+"' /></td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th>Rating Description</th>");
                        html.push("<td><textarea name='description' id='description'  cols='50' rows='8'>"+scale.description+"</textarea></td>");
                    html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th>Color</th>");
                        html.push("<td><input type='text' name='color' id='color' value='"+scale.color+"' /></td>");
                    html.push("</tr>");
                    title = "Yes No";
                break;
            }

          html.push("</table>");
        html.push("</div>");




        $("<div />",{id:"edit_rating_scale_dialog", html: html.join(' ') }).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : title+" Rating Scale",
                width          : 'auto',
                buttons        : {
                    "Save Changes"       : function()
                    {
                        self.update(rating_scale.id, rating_scale_type);
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_rating_scale_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(rating_scale.id, 1, rating_scale_type);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(rating_scale.id, 0, rating_scale_type);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(rating_scale.id, 2, rating_scale_type);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_rating_scale_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((scale.status & 1) == 1)
                    {
                        $(activateBtn).css({"display":"none"});
                    } else {
                        $(deactivateBtn).css({"display":"none"});
                        //$(activateBtn).css({"display":"none"});
                    }

                    //if((rating_scale.status & 4) == 4)
                    //{
                        $(deleteBtn).css({"display":"none"});
                    //}

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});
                    if(scale.hasOwnProperty('color'))
                    {
                        var myPicker = new jscolor.color(document.getElementById('color'), {})
                        myPicker.fromString(scale.color)  // now you can access API via 'myPicker' variable
                    }
                }
            })


    } ,

    _updateStatus        : function(id, status, rating_scale_type)
    {
        var self = this;
        $.post( "../controllers/ratingscalecontroller.php?action=update",
            {
                id                : id,
                status            : status,
                rating_scale_type : rating_scale_type
            }, function(response){
                if(response.error)
                {
                    $("#message").addClass("ui-state-error").html( response.text )
                } else {
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text );
                        self.get(rating_scale_type);
                    } else {
                        jsDisplayResult("info", "info", response.text );
                    }
                    $("#edit_rating_scale_dialog").dialog("destroy").remove();
                }
            },"json");
    } ,

    update              : function(id, rating_scale_type)
    {
        var self = this;
        $.post( "../controllers/ratingscalecontroller.php?action=update",
            {
                id                            : id,
                rating                        : $("#rating").val(),
                definition                    : $("#definition").val(),
                description                   : $("#description").val(),
                color                         : $("#color").val(),
                percentage                    : $("#percentage").val(),
                symbol                        : $("#symbol").val(),
                symbol_rating_description     : $("#symbol_rating_description").val(),
                percentage_rating_description : $("#percentage_rating_description").val(),
                faces_rating_description      : $("#faces_rating_description").val(),
                colour_rating_description     : $("#colour_rating_description").val(),
                yes_no_rating_scale_description : $("#yes_no_rating_scale_description").val(),
                name                            : $("#name").val(),
                rating_scale_type             : rating_scale_type
            }, function(response) {
                if(response.error)
                {
                    $("#message").addClass("ui-state-error").html( response.text )
                } else {
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text );
                        self.get(rating_scale_type);
                    } else {
                        jsDisplayResult("info", "info", response.text );
                    }
                    $("#edit_rating_scale_dialog").dialog("destroy").remove();
                }
            },"json");

    } ,
    _save			: function( rating_scale_type )
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/ratingscalecontroller.php?action=save",
            {
                rating       : $("#rating").val(),
                definition   : $("#definition").val(),
                description  : $("#description").val(),
                color        : $("#color").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_rating_scale_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    RatingScale.get()
                }
            }, 'json');
    }

}