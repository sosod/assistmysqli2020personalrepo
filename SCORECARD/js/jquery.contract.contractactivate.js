// JavaScript Document
$.widget("ui.contractactivate", {
	
	options	: 	{
		tableId 		: "contractactivate_"+(Math.floor(Math.random(56) * 34)),
		url				: "../controllers/contractcontroller.php?action=getActivationContracts",
		location		: "",
		start			: 0,
		limit			: 10,
		total			: 0,
		contract_id	    : 0,
		current			: 1,
		isOwners		: [], 
		section         : "",
		viewAction      : ""
	} , 
	
	_init			: function()
	{
		this._getContracts();		
	} , 
	
	_create			: function()
	{
		var self = this;
		var html = [];
		html.push("<table width='100%' class='noborder'>");
		  html.push("<tr>");
		    html.push("<td width='50%' class='noborder'>");
		      html.push("<table width='100%' id='awaiting_"+self.options.tableId+"'>");
      		    html.push("<tr>")
      		    html.push("<td colspan='6'><h4>Contracts Awaiting Activation</h4></td>");
      		    html.push("</tr>");
		      html.push("</table>");
		    html.push("</td>");
		    html.push("<td class='noborder'>");
		      html.push("<table width='100%' id='activated_"+self.options.tableId+"'>");
      		    html.push("<tr width='50%'>");
      		    html.push("<td colspan='6'><h4>Contracts Activated</h4></td>");
      		    html.push("</tr>");      		    
		      html.push("</table>");
		    html.push("</td>");		    
		  html.push("</tr>");	  
		html.push("</table>");
		$(self.element).append(html.join(' '));		
	} , 
	
	_getContracts		: function()
	{
		var self = this;
	     $("body").append($("<div />",{id:"contractLoadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
		  .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
	     );				
		$.getJSON(self.options.location+""+self.options.url, {
			start 	: self.options.start,
			limit 	: self.options.limit,
			id		: self.options.contract_id,
			section : self.options.section,
            page    : 'activate'
		}, function(contractData){
		    $("#contractLoadingDiv").remove();
			$(".activated").remove();
			$(".awaiting").remove();
			self._displayHeaders(contractData.columns);
			self._displayAwaiting(contractData.awaiting.contracts, contractData.awaiting.contract_detail);
			self._displayActivated(contractData.activated.contracts, contractData.activated.contract_detail);
		});
	} ,
	
	_displayAwaiting    : function(contracts, contract_detail)
	{
	   var self = this;
	   var html = [];
	   if($.isEmptyObject(contracts))
	   {
	      html.push("<tr class='awaiting'><td colspan='6'>There are no contracts awaiting activation</td></tr>");
	   } else {
           $.each(contracts, function(contract_id, contract){
              html.push("<tr class='awaiting'>");
                html.push("<td>"+contract.contract_id+"</td>");
                html.push("<td>"+contract.reference_number+"</td>");
                html.push("<td>"+contract.contract_name+"</td>");
                html.push("<td>"+contract.contract_type+"</td>");
                html.push("<td>"+contract.contract_category+"</td>");
                html.push("<td>");
                 html.push("<input type='button' name='view_contract_"+contract_id+"' id='view_contract_"+contract_id+"' value='View Contract' />");
                 html.push("<br />");
                 html.push("<input type='button' name='view_deliverables_"+contract_id+"' id='view_deliverables_"+contract_id+"' value='View Deliverable and Action' />");
                 html.push("<br />");
                 html.push("<input type='button' name='activate_"+contract_id+"' id='activate_"+contract_id+"' value='Activate ' />");
                 $("#activate_"+contract_id).live("click", function(e){
                    self._activateContract(contract_id);
                    e.preventDefault();
                 });


               $("#view_contract_"+contract_id).live("click", function(e){
                   self._viewContract(contract_id);
                   e.preventDefault();
               });

               $("#view_deliverables_"+contract_id).live("click", function(e){
                   self._viewDeliverables(contract_id, contract_detail[contract_id]);
                   e.preventDefault();
               });
                html.push("</td>");
                
                
              html.push("</tr>");       
           });
       }
	   $("#awaiting_"+self.options.tableId).append(html.join(' ') );	
	   
	} ,
	
	_displayActivated			: function(contracts, contract_detail)
	{  
	   var self = this;
	   var html = [];
	   if($.isEmptyObject(contracts))
	   {
	     html.push("<tr class='activated'><td colspan='6'>There are no contracts activated</td></tr>");
	   } else {	   
           $.each(contracts, function(contract_id, contract){
              html.push("<tr class='activated'>");
                html.push("<td>"+contract.contract_id+"</td>");
                html.push("<td>"+contract.reference_number+"</td>");
                html.push("<td>"+contract.contract_name+"</td>");
                html.push("<td>"+contract.contract_type+"</td>");
                html.push("<td>"+contract.contract_category+"</td>");
               html.push("<td>");
               if(contract_detail[contract_id]['has_manage_activity'] == false || contract_detail[contract_id]['has_manage_activity'] == null)
               {
                   html.push("<input type='button' name='undo_activation_"+contract_id+"' id='undo_activation_"+contract_id+"' value='Undo Activation "+contract_detail[contract_id]['has_manage_activity']+"' />");
               } else {
                   html.push("<div class='ui-state-ok' style='border:0px; background-color:white;'>");
                     html.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                     html.push("<span style='float:left;'>Active . . .</span>");
                   html.push("</div>");
               }
               $("#undo_activation_"+contract_id).live("click", function(e){
                   self._undoActivationContract(contract_id);
                   e.preventDefault();
               });
               html.push("</td>");
              html.push("</tr>");       
           });
       }
	   $("#activated_"+self.options.tableId).append(html.join(' ') );	
	} ,


    _viewContract           : function(contract_id)
    {
        $("<div />",{id:"contract_"+contract_id})
            .dialog({
                modal      : true,
                autoOpen   : true,
                title      : "Contract Details",
                position   : "left top",
                width      : 'auto',
                buttons    : {
                    "Ok"        : function()
                    {
                        $("#contract_"+contract_id).remove();
                    }
                } ,
                open       : function(ui, event)
                {
                    $("#action_contract").html("Loading contract details ...");
                    $.get("../controllers/contractcontroller.php?action=getContractDetailHtml",{
                        id  : contract_id
                    } , function(contractHtml){
                        $("#contract_"+contract_id).html("");
                        $("#contract_"+contract_id).append( contractHtml );
                    },"html")
                },
                close		: function() {
                    $("#contract_"+contract_id).remove();
                }
            });
    } ,


    _viewDeliverables       : function(contract_id, contract_detail)
    {
        if($("#contract_deliverable_"+contract_id).length > 0)
        {
            $("#contract_deliverable_"+contract_id).remove();
        }

        var self = this;
        $("<div />",{id:"contract_deliverable_"+contract_id})
            .dialog({
                modal      : true,
                autoOpen   : true,
                title      : "Contract Deliverable Details",
                position   : "left top",
                width      : 'auto',
                buttons    : {
                    "Ok"        : function()
                    {
                        $("#contract_deliverable_"+contract_id).dialog('destroy').remove();
                    }
                } ,
                open       : function(ui, event)
                {
                    $("#contract_deliverable_"+contract_id).deliverable({contract_id: contract_id, showSubDeliverable: true, showActions: true, autoLoad: true, allowFilter: false, actionFilter: false, financial_year:contract_detail.financial_year_id });
                },
                close		: function() {
                    $("#contract_deliverable_"+contract_id).dialog('destroy').remove();
                }
            });

    } ,

    _activateContract      : function(contract_id)
	{
	    var self = this;
	    if($("#activate_dialog").length > 0)
	    {
	       $("#activate_dialog").remove();
	    }
	    var html = [];
	    html.push("<table>");
	      html.push("<tr>");
	        html.push("<th>Response</th>")
	        html.push("<td>");
	         html.push("<textarea id='response' name='response' cols='50' rows='8'></textarea>");
	        html.push("</td>");
	      html.push("</tr>");
	      html.push("<tr>");
	        html.push("<td colspan='2'>");
	         html.push("<p id='message'></p>")
	        html.push("</td>");
	      html.push("</tr>");
	    html.push("<table>");
	    
	    $("<div />",{id:"activate_dialog"}).append(html.join(' '))
	     .dialog({
	            autoOpen    : true,
	            modal       : true,
	            position    : "top",
	            title       : "Activate Contract C"+contract_id,
	            width       : 'auto',
	            buttons     : {
	                            "Activate"      : function()
	                            {
                                    $('#message').html("Activating contract ....<img src='../images/loaderA32.gif' />");
	                                 $.post("../controllers/contractcontroller.php?action=activateContract", {
	                                    contract_id : contract_id,
	                                    response    : $("#response").val()
	                                 }, function(response) {
	                                    if(response.error)
	                                    {
	                                      jsDisplayResult("error", "error", response.text);
	                                    } else {
	                                      jsDisplayResult("ok", "ok", response.text);
	                                      self._getContracts();
                                          $("#activate_dialog").dialog("destroy").remove();
	                                    }
	                                 },"json");
	                            } ,
	                            
	                            "Cancel"        : function()
	                            {
	                               $("#activate_dialog").dialog("destroy").remove();
	                            }
	            },
	            close       : function(event, ui)
	            {
	               $("#activate_dialog").dialog("destroy").remove();
	            }, 
	            open        : function(event, ui)
	            {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
	            }
	     });
	    
	},

    _undoActivationContract      : function(contract_id)
    {
        var self = this;
        if($("#undoactivate_dialog").length > 0)
        {
            $("#undoactivate_dialog").remove();
        }
        var html = [];
        html.push("<table>");
            html.push("<tr>");
                html.push("<th>Response</th>")
                html.push("<td>");
                html.push("<textarea id='response' name='response' cols='50' rows='8'></textarea>")
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td colspan='2'>");
                html.push("<p id='message'></p>")
                html.push("</td>");
            html.push("</tr>");
        html.push("<table>");

        $("<div />",{id:"undoactivate_dialog"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Undo Contract Activation C"+contract_id,
                width       : 'auto',
                buttons     : {
                    "Save Changes"      : function()
                    {
                        $('#message').html("Deactivating contract ....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/contractcontroller.php?action=undoActivateContract", {
                            contract_id : contract_id,
                            response    : $("#response").val()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getContracts();
                            }
                        },"json");
                        $("#undoactivate_dialog").dialog("destroy").remove();
                    } ,

                    "Cancel"        : function()
                    {
                        $("#undoactivate_dialog").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#activate_dialog").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});
                }
            });

    },

	_displayHeaders		: function(headers)
	{
		var self = this;
		var html = [];
		html.push("<tr class='activated'>");
        html.push("<th>"+headers.contract_id+"</th>");
        html.push("<th>"+headers.reference_number+"</th>");
        html.push("<th>"+headers.contract_name+"</th>");
        html.push("<th>"+headers.contract_type+"</th>");	
        html.push("<th>"+headers.contract_category+"</th>");
        html.push("<th></th>");
        html.push("</tr>");
		$("#awaiting_"+self.options.tableId).append(html.join(' '));
		$("#activated_"+self.options.tableId).append(html.join(' ')).find("#remove").remove();;
	} ,
	
	_displayPaging		: function(total, cols)
	{
		var self  = this;	
		var html  = [];
		var pages = 0;
		if(total%self.options.limit > 0)
		{
		   pages = Math.ceil( total/self.options.limit )
		} else {
		   pages = Math.floor( total/self.options.limit )
		}
        html.push("<tr>");
          html.push("<td colspan='"+cols+"'>");
            html.push("<input type='button' id='action_first_"+self.option.contract_id+"' value=' |< ' />");
            html.push("<input type='button' id='action_previous_"+self.option.contract_id+"' value=' < ' />");
             html.push("<span>&nbsp;&nbsp;&nbsp;");
              html.push("Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ?  1 :pages));
             html.push("&nbsp;&nbsp;&nbsp;</span>");
            html.push("<input type='button' id='action_next_"+self.option.contract_id+"' value=' > ' />");
            html.push("<input type='button' id='action_last_"+self.option.contract_id+"' value=' >| ' />");
          html.push("</td>");
        if(self.options.editAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.updateAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.assuranceAction)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAssurance)
        {
          html.push("<td>&nbsp;&nbsp;</td>");
        }    
        if(self.options.viewAction)
        {
           html.push("<td>&nbsp;&nbsp;</td>");
        }   	   
        html.push("</tr>");
        $("#"+self.options.tableId+"_"+self.options.contract_id).append(html.join(' '));                 
        if(self.options.current < 2)
        {
          $("#action_first_"+self.option.contract_id).attr('disabled', 'disabled');
          $("#action_previous_"+self.option.contract_id).attr('disabled', 'disabled');		     
        }
        if((self.options.current == pages || pages == 0))
        {
          $("#action_next_"+self.option.contract_id).attr('disabled', 'disabled');
          $("#action_last_"+self.option.contract_id).attr('disabled', 'disabled');		     
        }			 
		$("#action_next_"+self.option.contract_id).bind("click", function(){
			self._getNext( self );
		});
		$("#action_last_"+self.option.contract_id).bind("click",  function(){
			self._getLast( self );
		});
		$("#action_previous_"+self.option.contract_id).bind("click",  function(){
			self._getPrevious( self );
		});
		$("#action_first_"+self.option.contract_id).bind("click",  function(){
			self._getFirst( self );
		});			 
	} , 
		
		_getNext  			: function( $this ) {
			$this.options.current   = $this.options.current+1;
			$this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
			this._getContracts();
		},	
		
		_getLast  			: function( $this ) {
			$this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
			$this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );			
			this._getContracts();
		},
		
		_getPrevious    	: function( $this ) {
			$this.options.current   = parseFloat( $this.options.current ) - 1;
			$this.options.start 	= ($this.options.current-1)*$this.options.limit;		
			this._getContracts();			
		},
		
		_getFirst  			: function( $this ) {
			$this.options.current   = 1;
			$this.options.start 	= 0;
			this._getContracts();	
		}
	
	
});
