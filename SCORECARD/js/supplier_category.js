$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    SupplierCategory.get();
    $("#add_supplier_category").click(function(e){
        SupplierCategory.addSupplierCategory();
        e.preventDefault();
    });
	
});

var SupplierCategory 	= {

		get				: function()
		{
            $(".supplier_categories").remove();
			$.getJSON( "../controllers/suppliercategorycontroller.php?action=getAll", function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#supplier_category_table").append($("<tr />").addClass('supplier_categories')
                        .append($("<td />", {colspan: 6, html: "There are no supplier categories" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        SupplierCategory.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#supplier_category_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('supplier_categories')
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.short_code}))
				.append($("<td />",{html:val.name}))
				.append($("<td />",{html:val.description}))
                .append($("<td />",{html:val.notes}))
                .append($("<td />",{html:"<b>"+(val.status == 1 ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"category_edit_"+val.id, name:"category_edit_"+val.id }))
				 )
			  )

            $("#category_edit_"+val.id).live("click", function(e){
                self._updateSupplierCategory(val);
                e.preventDefault();
            });
			
		},

    addSupplierCategory          : function()
    {
        var self = this;
        if($("#add_supplier_category_dialog").length > 0)
        {
            $("#add_supplier_category_dialog").remove();
        }

        $("<div />",{id:"add_supplier_category_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Category Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"short_code", id:"short_code", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Category Name :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Notes :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"notes", id:"notes", cols:50, rows:8}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Supplier Category",
                width          : 500,
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the short code");
                            return false;
                        } else if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the supplier category name");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_supplier_category_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_supplier_category_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});
                }
            })
    },

    _updateSupplierCategory         : function(supplier_category)
    {
        var self = this;
        if($("#edit_supplier_category_dialog").length > 0)
        {
            $("#edit_supplier_category_dialog").remove();
        }

        $("<div />",{id:"edit_supplier_category_dialog"}).append($("<table />",{width:"100%"})
               .append($("<tr />")
                    .append($("<th />",{html:"Category Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"short_code", id:"short_code", value:supplier_category.short_code}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Category Name :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:supplier_category.name}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Category Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8, text:supplier_category.description}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Category Notes :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"notes", id:"notes", cols:50, rows:8, text:supplier_category.notes}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Supplier Category",
                width          : 500,
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the deliverable type short code");
                            return false;
                        } else if($("#name").val() == "") {
                            $("#message").addClass("ui-state-error").html("please enter the deliverable type name");
                            return false;
                        } else {
                            self.update(supplier_category.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_supplier_category_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(supplier_category.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(supplier_category.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(supplier_category.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_supplier_category_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((supplier_category.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/suppliercategorycontroller.php?action=update",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_supplier_category_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/suppliercategorycontroller.php?action=update",
        {
            id           : id,
            short_code : $("#short_code").val(),
            name:        $("#name").val(),
            description: $("#description").val(),
            notes       : $("#notes").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_supplier_category_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/suppliercategorycontroller.php?action=save",
            {
                short_code  : $("#short_code").val(),
                name        : $("#name").val(),
                description : $("#description").val(),
                notes       : $("#notes").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_supplier_category_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    SupplierCategory.get()
                }
            }, 'json');
    }

}