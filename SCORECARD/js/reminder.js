$(function(){

    $("#save_reminders").live("click", function(e){
        Reminder.saveReminders();
        e.preventDefault();
    });

    $("#update_reminders").live("click", function(e){
        Reminder.updateActionReminders();
        e.preventDefault();
    });
});

var Reminder = {

    saveReminders            : function()
    {
        jsDisplayResult("info", "info", "saving . . . <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/remindercontroller.php?action=save", {
            action_days      : $("#action_days").val(),
            deliverable_days : $("#deliverable_days").val(),
            score_card_days  : $("#score_card_days").val()
        }, function( response ) {
            if( response.error ) {
                jsDisplayResult("error", "error", response.text );
            } else {
                jsDisplayResult("ok", "ok",  response.text );
                document.location.reload();
            }
        },"json");
    } ,

    updateActionReminders       : function()
    {
        jsDisplayResult("info", "info", "updating . . . <img src='../images/loaderA32.gif' />");
        $.post("../controllers/remindercontroller.php?action=update", {
            action_days      : $("#action_days").val(),
            deliverable_days : $("#deliverable_days").val(),
            score_card_days  : $("#score_card_days").val(),
            id               : $("#id").val()
        }, function( response ) {
            if( response.error ) {
                jsDisplayResult("error", "error", response.text );
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok",  response.text );
                } else {
                    jsDisplayResult("info", "info",  response.text );
                }
            }
        },"json");
    }


}
