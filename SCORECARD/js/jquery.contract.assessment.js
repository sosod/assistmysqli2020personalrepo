$.widget('contract.assessment', {

    options     : {
        tableRef          : 'assessment_table_'+(Math.floor(Math.random(563) * 313)),
        url 	          : "../controllers/contractassessmentcontroller.php?action=getContractAssessment",
        contract_id       : 0,
        financial_year    : 0,
        autoLoad          : false,
        page              : '',
        section           : '',
        editAction        : true,
        assessDeliverable : false
    } ,


    _init        : function()
    {
        if(this.options.autoLoad)
        {
            this._getAssessmentDetail();
        }
    },

    _create       : function()
    {
        var self = this;
        var html = [];
        html.push("<table id='"+self.options.tableRef+"' class='noborder'>");
            html.push("<tr>");
                html.push("<td valign='top' class='noborder'>");
                    html.push("<table id='"+self.options.tableRef+"_contract' width='100%'></table>");
                html.push("</td>");
                html.push("<td valign='top' class='noborder'>");
                    html.push("<table id='"+self.options.tableRef+"_summary' width='100%'></table>");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td valign='top' class='noborder' colspan='2'>");
                html.push("<table id='"+self.options.tableRef+"_deliverable' width='100%'></table>");
                html.push("</td>");
            html.push("</tr>");
        html.push("</table>");
        $(this.element).append(html.join(' '));
        //self._getAssessmentDetail();

    },

    _getAssessmentDetail      : function()
    {
        var self = this;
        $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
        );
        $.getJSON(self.options.url,{
            section		      : self.options.section,
            page              : self.options.page,
            contract_id       : self.options.contract_id
        },function(assessmentData) {
            $("#loadingDiv").remove();
            $(".contract_message").remove();
            $(".confirm_button").remove();
            $(".confirm-data").remove();
            if(assessmentData.hasOwnProperty('contract'))
            {
                self._displayContract(assessmentData.contract);
            }
            if(assessmentData.hasOwnProperty('key_measures'))
            {
                self._displaySummary(assessmentData.key_measures, assessmentData.categories, assessmentData);
            }
            if(assessmentData.hasOwnProperty('category_deliverable'))
            {
                var categoryHtml = [];
                self._displayDeliverableHeader(assessmentData.columns, assessmentData.total_columns, assessmentData.contract_detail );
                var total_d_weight = 0;
                var total_o_weight = 0;
                var total_q_weight = 0;
                if($(".weights").length > 0)
                {
                    $(".weights").remove();
                }
                categoryHtml.push("<tr class='weights'>");
                if(assessmentData['contract_detail']['is_assessed_quantitative'] == 1)
                {
                    categoryHtml.push("<th class='th2'>Rating</th>");
                    categoryHtml.push("<th class='th2'>Comment</th>")
                }
                if(assessmentData['contract_detail']['is_assessed_qualitative'] == 1)
                {
                    categoryHtml.push("<th class='th2'>Rating</th>");
                    categoryHtml.push("<th class='th2'>Comment</th>")
                }
                if(assessmentData['contract_detail']['is_assessed_other'] == 1)
                {
                    categoryHtml.push("<th class='th2'>Rating</th>");
                    categoryHtml.push("<th class='th2'>Comment</th>")
                }
                categoryHtml.push("</tr>");
                $.each(assessmentData.category_deliverable, function(category_id, deliverables){
                    categoryHtml.push("<tr class='confirm-data'>");
                    categoryHtml.push("<td  class='noborder' colspan='15'><b>"+assessmentData.categories[category_id]+"</b></td>");
                    categoryHtml.push("</tr>");
                    categoryHtml.push( self._displayDeliverables(deliverables, assessmentData.category_weights[category_id], assessmentData.total_actions, assessmentData) );
                    total_d_weight += parseInt(assessmentData.category_weights[category_id]['delivered_weight']);
                    total_o_weight += parseInt(assessmentData.category_weights[category_id]['other_weight']);
                    total_q_weight += parseInt(assessmentData.category_weights[category_id]['quality_weight']);
                });
                /*                categoryHtml.push("<tr style='background-color:#d3d3d3;' class='confirm-data'>");
                 categoryHtml.push("<td>&nbsp;</td>");
                 $.each(assessmentData.columns, function(key, headVal){
                 if(key == 'delivered_weight')
                 {
                 categoryHtml.push("<td><b>"+total_d_weight+"</b></td>");
                 } else if(key == 'other_weight') {
                 categoryHtml.push("<td><b>"+total_o_weight+"</b></td>");
                 } else if(key == 'quality_weight')  {
                 categoryHtml.push("<td><b>"+total_q_weight+"</b></td>");
                 } else {
                 categoryHtml.push("<td></td>");
                 }
                 });
                 categoryHtml.push("<td>&nbsp;</td>");
                 categoryHtml.push("<td>&nbsp;</td>");
                 categoryHtml.push("</tr>");*/
                $("#"+self.options.tableRef+"_deliverable").append( categoryHtml.join(' ')).css({'text-align':'left'});

                self._displayDeliverableActions(assessmentData);
            } else {
                var noAssessmentHtml = [];
                noAssessmentHtml.push("<tr>");
                  noAssessmentHtml.push("<td>");
                    noAssessmentHtml.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                    noAssessmentHtml.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                    noAssessmentHtml.push("<span style='padding:5px; float:left;'>There are no deliverable available for assessment.</span>");
                    noAssessmentHtml.push("</td>");
                noAssessmentHtml.push("</tr>");
                noAssessmentHtml.push("<tr>");
                    noAssessmentHtml.push("<td>");
                        noAssessmentHtml.push("You have "+assessmentData.assessments_due+" assessments due");
                    noAssessmentHtml.push("</td>");
                noAssessmentHtml.push("</tr>");
                if(assessmentData.assessments_due > 0)
                {
                    noAssessmentHtml.push("<tr>");
                      noAssessmentHtml.push("<td>");
                       noAssessmentHtml.push("<input type='button' name='complete_outstanding' id='complete_outstanding' value='Complete Outstanding Assessments' />");
                      noAssessmentHtml.push("</td>");
                    noAssessmentHtml.push("</tr>");
                }
                $("#"+self.options.tableRef+"_deliverable").append( noAssessmentHtml.join(' ')).css({'text-align':'left'});

                $("#complete_outstanding").live('click', function(e){
                    jsDisplayResult("error", "error", "Saving assessment . . . <img src='../images/loaderA32.gif' />");
                    $.getJSON("../controllers/contractassessmentcontroller.php?action=completeOutstandingContractAssessment",{
                        contract_id            : self.options.contract_id
                    }, function(response) {
                        if(response.error)
                        {
                            jsDisplayResult('error', 'error', response.text);
                        } else {
                            jsDisplayResult('ok', 'ok', response.text);
                            setTimeout(function(){
                                document.location.reload();
                            }, 1000)
                        }
                    });
                   e.preventDefault();
                })
            }
            if(assessmentData.hasOwnProperty('contract_assessment'))
            {
                if(!$.isEmptyObject(assessmentData.contract_assessment))
                {
                    var _completeHtml = [];
                    _completeHtml.push("<tr>");
                        _completeHtml.push("<td>");
                            _completeHtml.push("<input type='submit' name='complete_assessment' id='complete_assessment' value='Complete Assessment'  />");
                            _completeHtml.push("<input type='hidden' name='contract_assessment_id' id='contract_assessment_id' value='"+assessmentData.contract_assessment.id+"'  />");
                        _completeHtml.push("</td>");
                    _completeHtml.push("</tr>");

                    //$("#"+self.options.tableRef).append( _completeHtml.join(' ')).css({'text-align':'left'});
                    $(self.element).append( _completeHtml.join(' '));
                    $("#complete_assessment").live('click', function(){
                        jsDisplayResult("error", "error", "Saving assessment . . . <img src='../images/loaderA32.gif' />");
                        $.getJSON("../controllers/contractassessmentcontroller.php?action=completeContractAssessment",{
                            contract_id            : self.options.contract_id,
                            contract_assessment_id : assessmentData.contract_assessment.id
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult('error', 'error', response.text);
                            } else {
                                jsDisplayResult('ok', 'ok', response.text);
                                setTimeout(function(){
                                    document.location.reload();
                                }, 1000)
                            }
                        });
                        return false;
                    });
                }
            }

        });
    } ,

    _displayContract            : function(contract)
    {
        var self = this;
        var contractHtml = [];
        $.each(contract, function(column, contract) {
            contractHtml.push("<tr class='confirm-data'>")
            contractHtml.push("<th style='text-align: left;'>"+contract.header+"</th>");
            if(column == 'contract_assessment_frequency_start_date')
            {
              contractHtml.push("<td style='color:green;'><b>"+contract.value+"</b></td>");
            } else {
              contractHtml.push("<td>"+contract.value+"</td>");
            }
            contractHtml.push("</tr>");
        });
        $("#"+self.options.tableRef+"_contract").append( contractHtml.join(' ')).css({'text-align':'left'});
    } ,


    _displayDeliverableHeader       : function(headers, total_columns, contract_detail)
    {
        var self = this;
        var html = [];
        html.push("<tr class='confirm-data'>");
        html.push("<th rowspan='2'>&nbsp;</th>");
        html.push("<th rowspan='2'>Ref</th>");
        html.push("<th rowspan='2'>Description</th>");
        html.push("<th rowspan='2'>Owner</th>");
        html.push("<th rowspan='2'>Status</th>");
        html.push("<th rowspan='2'>Progress</th>");
        html.push("<th rowspan='2'>Number Of Actions</th>");
        html.push("<th rowspan='2'>Date Last Assessed</th>");
        if(contract_detail['is_assessed_quantitative'] == 1)
        {
            html.push("<th colspan='2'>Quantitative</th>");
        }
        if(contract_detail['is_assessed_qualitative'] == 1)
        {
            html.push("<th colspan='2'>Qualitative</th>")
        }
        if(contract_detail['is_assessed_other'] == 1)
        {
            html.push("<th colspan='2'>Other</th>")
        }
        if(self.options.assessDeliverable)
        {
            html.push("<th rowspan='2'>&nbsp;</th>");
        }
        html.push("</tr>");
        $("#"+self.options.tableRef+"_deliverable").append(html.join(' '));
    },

    _displayDeliverables        : function(deliverable_data, category_weights, action_data, assessmentData)
    {
        var self = this;
        var deliverableHtml = [];
        $.each(deliverable_data.deliverables, function(deliverable_id, deliverable){
            deliverableHtml.push("<tr class='confirm-data'>");
            if(assessmentData['settings'][deliverable_id]['has_sub'] == 0)
            {
                deliverableHtml.push("<td>");
                deliverableHtml.push("<a href='#' id='deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id+"' class='deliverable_more_less' >");
                deliverableHtml.push("<span class='ui-icon ui-icon-plus'></span>");
                deliverableHtml.push("</a>");
                deliverableHtml.push("</td>");
            } else {
                deliverableHtml.push("<td></td>");
            }
            deliverableHtml.push("<td>"+deliverable.deliverable_id+"</td>");
            deliverableHtml.push("<td>"+deliverable.description+"</td>");
            deliverableHtml.push("<td>"+deliverable.deliverable_owner+"</td>");
            deliverableHtml.push("<td>"+deliverable.deliverable_status+"</td>");
            deliverableHtml.push("<td style='text-align: right;'>"+assessmentData['d_progress'][deliverable_id]+"</td>");
            if(action_data.hasOwnProperty(deliverable_id))
            {
                deliverableHtml.push("<td style='text-align: right;'>"+action_data[deliverable_id]+"</td>");
            } else {
                deliverableHtml.push("<td style='text-align: right;'>0</td>");
            }
            deliverableHtml.push( self._loadDeliverableAssessment(assessmentData, deliverable_id, assessmentData.contract_detail ) );
            deliverableHtml.push(self._deliverableAssessmentButton(assessmentData, deliverable_id));
            deliverableHtml.push("</tr>");
            if(assessmentData.actions.hasOwnProperty(deliverable_id))
            {
                $.each(assessmentData.actions[deliverable_id], function(action_index, action_id){
                    deliverableHtml.push("<tr id='deliverable_actions_"+self.options.tableRef+"_"+deliverable_id+"' class='deliverable_actions confirm-data' style='display:none;'>");
                    deliverableHtml.push("<td id='show_deliverable_actions_"+self.options.tableRef+"_"+deliverable_id+"' colspan='"+(parseInt(assessmentData.total_columns)+3)+"'></td>");
                    deliverableHtml.push("</tr>");
                });
                //$("#show_deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).action({deliverable_id:deliverable_id, deliverableRef:'del_'+deliverable_id , section:self.options.section, thClass:'th2', editAction: self.options.editAction});
            }
            if(deliverable_data.hasOwnProperty('sub_deliverable'))
            {
                if(deliverable_data['sub_deliverable'].hasOwnProperty(deliverable_id))
                {
                    $.each(deliverable_data['sub_deliverable'][deliverable_id], function(sub_deliverable_id, sub_deliverable){
                        deliverableHtml.push("<tr class='view-notowner confirm-data'>");
                        deliverableHtml.push("<td>");
                        deliverableHtml.push("<a href='#' id='sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id+"' class='sub_deliverable_more_less'>");
                        deliverableHtml.push("<span class='ui-icon ui-icon-plus'></span>");
                        deliverableHtml.push("</a>");
                        deliverableHtml.push("</td>");
                        deliverableHtml.push("<td>"+sub_deliverable.deliverable_id+"</td>");
                        deliverableHtml.push("<td>"+sub_deliverable.description+"</td>");
                        deliverableHtml.push("<td>"+sub_deliverable.deliverable_owner+"</td>");
                        deliverableHtml.push("<td>"+sub_deliverable.deliverable_status+"</td>");
                        deliverableHtml.push("<td style='text-align: right;'>"+assessmentData['d_progress'][sub_deliverable_id]+"</td>");
                        if(action_data.hasOwnProperty(sub_deliverable_id))
                        {
                            deliverableHtml.push("<td style='text-align: right;'>"+action_data[sub_deliverable_id]+"</td>");
                        }
                        deliverableHtml.push( self._loadDeliverableAssessment(assessmentData, sub_deliverable_id, assessmentData.contract_detail ) );
                        deliverableHtml.push(self._deliverableAssessmentButton(assessmentData, sub_deliverable_id));
                        deliverableHtml.push("</tr>");
                        if(assessmentData.actions.hasOwnProperty(sub_deliverable_id))
                        {
                            $.each(assessmentData.actions[sub_deliverable_id], function(action_index, action_id){
                                deliverableHtml.push("<tr id='sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id+"' class='deliverable_actions confirm-data' style='display:none;'>");
                                deliverableHtml.push("<td id='show_sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id+"' colspan='"+(parseInt(assessmentData.total_columns) + 3)+"'></td>");
                                deliverableHtml.push("</tr>");
                            });
                            //$("#show_sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id).action({deliverable_id:sub_deliverable_id, deliverableRef:'sub_'+sub_deliverable_id , section:self.options.section, thClass:'th2', editAction: self.options.editAction});
                        }
                        $("#sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id).live("click", function(e){
                            if( $("#sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id).is(":hidden") )
                            {
                                $("#sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id+" span").removeClass("ui-icon-closethick").addClass("ui-icon-minus");
                                $("#sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id).show();
                            } else {
                                $("#sub_deliverable_more_less_"+self.options.tableRef+"_"+sub_deliverable_id+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");
                                $("#sub_deliverable_actions_"+self.options.tableRef+"_"+sub_deliverable_id).hide();
                            }
                            e.preventDefault();
                        });

                    });
                }
            }
            $("#deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id).live("click", function(e){
                if( $("#deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).is(":hidden") )
                {
                    $("#deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id+" span").removeClass("ui-icon-closethick").addClass("ui-icon-minus");
                    $("#deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).show();
                } else {
                    $("#deliverable_more_less_"+self.options.tableRef+"_"+deliverable_id+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");
                    $("#deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).hide();
                }
                e.preventDefault();
            });
        })
        /*        deliverableHtml.push("<tr class='confirm-data'>");
         deliverableHtml.push("<td>&nbsp;</td>");
         $.each(assessmentData.columns, function(key, headVal){
         if(key == 'delivered_weight')
         {
         deliverableHtml.push("<td>"+category_weights['delivered_weight']+"</td>");
         } else if(key == 'other_weight') {
         deliverableHtml.push("<td>"+category_weights['other_weight']+"</td>");
         } else if(key == 'quality_weight')  {
         deliverableHtml.push("<td>"+category_weights['quality_weight']+"</td>");
         } else {
         deliverableHtml.push("<td></td>");
         }
         });
         deliverableHtml.push("<td>&nbsp;</td>");
         deliverableHtml.push("<td>&nbsp;</td>");
         deliverableHtml.push("</tr>");*/

        return deliverableHtml.join(' ');
    } ,

    _loadDeliverableAssessment  : function(assessmentData, deliverable_id, contract_detail)
    {
        var html = []

        if(assessmentData['has_assessment'][deliverable_id])
        {
            html.push("<td>"+assessmentData['assessments'][deliverable_id]['last_assessed_date']+"</td>");
            if(contract_detail['is_assessed_quantitative'] == 1)
            {
                html.push("<td>"+assessmentData['assessments'][deliverable_id]['delivered']['score']+"</td>");
                html.push("<td>"+assessmentData['assessments'][deliverable_id]['delivered']['comment']+"</td>");
            }
            if(contract_detail['is_assessed_qualitative'] == 1)
            {
                html.push("<td>"+assessmentData['assessments'][deliverable_id]['quality']['score']+"</td>");
                html.push("<td>"+assessmentData['assessments'][deliverable_id]['quality']['comment']+"</td>");
            }
            if(contract_detail['is_assessed_other'] == 1)
            {
                html.push("<td>"+assessmentData['assessments'][deliverable_id]['other']['score']+"</td>");
                html.push("<td>"+assessmentData['assessments'][deliverable_id]['other']['comment']+"</td>");
            }
        } else {
            html.push("<td>&nbsp;</td>");
            if(contract_detail['is_assessed_quantitative'] == 1)
            {
                html.push("<td>&nbsp;</td>");
                html.push("<td>&nbsp;</td>");
            }
            if(contract_detail['is_assessed_qualitative'] == 1)
            {
                html.push("<td>&nbsp;</td>");
                html.push("<td>&nbsp;</td>");
            }
            if(contract_detail['is_assessed_other'] == 1)
            {
                html.push("<td>&nbsp;</td>");
                html.push("<td>&nbsp;</td>");
            }
        }
        return html.join('');
    },


    _displayDeliverableActions  : function(assessmentData)
    {
        var self = this;
        $.each(assessmentData.settings, function(deliverable_id, deliverable){
            if(deliverable.has_sub == 0)
            {
                if(assessmentData['actions'].hasOwnProperty(deliverable_id) && assessmentData['actions'][deliverable_id].length > 0)
                {
                    $("#show_deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).action({deliverable_id:deliverable_id, deliverableRef:'main_no_sub_'+deliverable_id, section:'assessment', thClass:'th2', editAction: false, financial_year: assessmentData.contract_detail.financial_year_id, autoLoad: true, allowFilter: false});
                }
            }

            if(deliverable.is_main == 0)
            {
                if(assessmentData['actions'].hasOwnProperty(deliverable_id) && assessmentData['actions'][deliverable_id].length > 0)
                {
                    $("#show_sub_deliverable_actions_"+self.options.tableRef+"_"+deliverable_id).action({deliverable_id:deliverable_id, deliverableRef:'sub_'+deliverable_id, section:'assessment', thClass:'th2', editAction: false, financial_year: assessmentData.contract_detail.financial_year_id, autoLoad: true, allowFilter: false});
                }
            }
        });

    },


    _displaySummary             : function(key_measures, categories, assessmentData)
    {
        var self = this;
        var summaryHtml = []
        var total_weight = 0;
        summaryHtml.push("<tr class='confirm-data'>");
        summaryHtml.push("<th>Key Measures</th>");
        summaryHtml.push("<th>Weights</th>");
        summaryHtml.push("</tr>");
        $.each(key_measures, function(category, category_deliverable_summary){
            var total_category_weight = 0;
            summaryHtml.push("<tr class='confirm-data'>");
            summaryHtml.push("<td style='text-align: left;' colspan='2'><b>"+categories[category]+"</b></td>");
            summaryHtml.push("</tr>");
            $.each(category_deliverable_summary, function(deliverable_id, deliverable_summary){
                summaryHtml.push("<tr class='confirm-data'>");
                summaryHtml.push("<td style='text-align: left;'>"+deliverable_summary.deliverable_name+"</td>");
                summaryHtml.push("<td style='text-align: right;'>"+deliverable_summary.total_weight+"</td>");
                summaryHtml.push("</tr>");
                total_category_weight = parseInt(deliverable_summary.total_weight) + parseInt(total_category_weight);
            });
            total_weight          = parseInt(total_category_weight) + parseInt(total_weight);
            summaryHtml.push("<tr class='view-notowner confirm-data'>");
            summaryHtml.push("<td style='text-align: left;'>Total for "+categories[category]+"</td>");
            summaryHtml.push("<td style='text-align: right;' colspan='2'><b>"+total_category_weight+"</b></td>");
            summaryHtml.push("</tr>");
        });
        summaryHtml.push("<tr>");
        summaryHtml.push("<td style='text-align: left;'>Score</td>");
        summaryHtml.push("<td style='text-align: right;' colspan='2'><b>"+total_weight+"</b></td>");
        summaryHtml.push("</tr>");
        if(assessmentData.assessments_due > 0)
        {
            summaryHtml.push("<tr>");
                summaryHtml.push("<td colspan='2'>");
                summaryHtml.push("<p class='ui-state ui-state-error' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                summaryHtml.push("<span class='ui-icon ui-icon-closethick' style='float:left;'></span>");
                summaryHtml.push("<span style='padding:5px; float:left;'>You have "+assessmentData.assessments_due+" outstanding assessments.</span>");
                summaryHtml.push("</td>");
            summaryHtml.push("</tr>");
        }
        $("#"+self.options.tableRef+"_summary").append(summaryHtml.join(' ')).css({'text-align':'left'});
    },

    _deliverableAssessmentButton          : function(deliverableData, deliverable_id)
    {
        var self = this;
        if(self.options.assessDeliverable)
        {
            var html = [];
            html.push("<td>&nbsp;");
            if(deliverableData['has_assessment'][deliverable_id])
            {
                html.push("<input type='button' value='Edit Deliverable Assessment' id='edit_deliverable_assess_"+deliverable_id+"' name='edit_deliverable_assess_"+deliverable_id+"' />");
                $("#edit_deliverable_assess_"+deliverable_id).live("click", function(){
                    self._editAssessmentDialog(deliverable_id);
                });
            } else {
              if(deliverableData.canEdit.hasOwnProperty(deliverable_id))
              {
                 if(deliverableData.canEdit[deliverable_id] == true)
                 {
                    html.push("<input type='button' value='Assess Deliverable' id='assess_deliverable_"+deliverable_id+"' name='assess_deliverable_"+deliverable_id+"' />");
                    $("#assess_deliverable_"+deliverable_id).live("click", function(){
                        self._assessmentDialog(deliverable_id);
                    });
                 }
              }
            }
            html.push("</td>");
            return html.join(' ');
        }
        return '';
    } ,

    _assessmentDialog            : function(deliverable_id)
    {
        var self = this;
        if($("#deliverable_assessment_dialog_"+deliverable_id).length > 0)
        {
            $("#deliverable_assessment_dialog_"+deliverable_id).remove();
        }
        $("<div />",{id:"deliverable_assessment_dialog_"+deliverable_id}).dialog({
            autoOpen    : true,
            modal       : true,
            position    : "center, top",
            width       : "auto",
            title       : "Assessment of Contract",
            buttons     : {
                "Assessment"   : function()
                {
                    $("#message").addClass("ui-state-info").html("Saving deliverable assessment ... <img src='../images/loaderA32.gif' />");
                    $.getJSON("../controllers/deliverableassessmentcontroller.php?action=saveDeliverableAssessment",{
                        deliverable_id         : deliverable_id,
                        contract_id            : self.options.contract_id,
                        contract_assessment_id : $("#contract_assessment_id").val(),
                        assessment_data        : $("#assessment_form_"+deliverable_id).serialize()
                    }, function(response) {
                        if(response.error)
                        {
                            $("#message").html(response.text).addClass('ui-state-error');
                        } else {
                            $("#message").html(response.text).addClass('ui-state-ok');
                            setTimeout(function(){
                                document.location.reload();
                            }, 1000)
                        }
                    });
                },
                "Cancel"    : function()
                {
                    $("#deliverable_assessment_dialog_"+deliverable_id).dialog("destroy").remove()
                }
            } ,
            "close"     : function(event, ui)
            {
                $("#deliverable_assessment_dialog_"+deliverable_id).dialog("destroy").remove()
            },
            "open"      : function(event, ui)
            {

                $.get('../controllers/deliverableassessmentcontroller.php?action=getDeliverableAssessmentHtml', {
                    deliverable_id : deliverable_id,
                    contract_id    : self.options.contract_id
                }, function(responseHtml){
                    $("#deliverable_assessment_dialog_"+deliverable_id).html(  responseHtml )
                })
                var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
                var saveButton = buttons[0];
                $(saveButton).css({"color":"#090"});
            }
        });
    }
    ,

    _editAssessmentDialog            : function(deliverable_id)
    {
        var self = this;
        if($("#edit_deliverable_assessment_dialog_"+deliverable_id).length > 0)
        {
            $("#edit_deliverable_assessment_dialog_"+deliverable_id).remove();
        }
        $("<div />",{id:"edit_deliverable_assessment_dialog_"+deliverable_id}).dialog({
            autoOpen    : true,
            modal       : true,
            position    : "center, top",
            width       : "auto",
            title       : "Edit Assessment of Deliverable",
            buttons     : {
                "Save Assessment Changes"   : function()
                {
                    $("#message").addClass("ui-state-info").html("Updating deliverable assessment ... <img src='../images/loaderA32.gif' />");
                    $.getJSON("../controllers/deliverableassessmentcontroller.php?action=editDeliverableAssessment",{
                        deliverable_id         : deliverable_id,
                        contract_id            : self.options.contract_id,
                        contract_assessment_id : $("#contract_assessment_id").val(),
                        assessment_data        : $("#assessment_form_"+deliverable_id).serialize(),
                        id                     : $("#id").val()
                    }, function(response) {
                        if(response.error)
                        {
                            $("#message").html(response.text).addClass('ui-state-error');
                        } else {
                            $("#message").html(response.text).addClass('ui-state-ok');
                            setTimeout(function(){
                                document.location.reload();
                            }, 1000)
                        }
                    });
                },
                "Cancel"    : function()
                {
                    $("#edit_deliverable_assessment_dialog_"+deliverable_id).dialog("destroy").remove()
                }
            } ,
            "close"     : function(event, ui)
            {
                $("#edit_deliverable_assessment_dialog_"+deliverable_id).dialog("destroy").remove()
            },
            "open"      : function(event, ui)
            {
                $.get('../controllers/deliverableassessmentcontroller.php?action=getEditDeliverableAssessmentHtml', {
                    deliverable_id         : deliverable_id,
                    contract_id            : self.options.contract_id,
                    contract_assessment_id : $("#contract_assessment_id").val()
                }, function(responseHtml){
                    $("#edit_deliverable_assessment_dialog_"+deliverable_id).html(  responseHtml )
                })
                var dialog = $(event.target).parents(".ui-dialog.ui-widget");
                var buttons = dialog.find(".ui-dialog-buttonpane").find("button");
                var saveButton = buttons[0];
                $(saveButton).css({"color":"#090"});
            }
        });
    }

});