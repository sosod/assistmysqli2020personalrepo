$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    DeliveredWeight.get();
    $("#add_delivered_weight").click(function(e){
        DeliveredWeight.addDeliveredWeight();
        e.preventDefault();
    });
	
});

var DeliveredWeight 	= {

		get				: function()
		{
            $(".delivered_weights").remove();
			$.getJSON( "../controllers/deliveredweightcontroller.php?action=getAll", function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#delivered_weight_table").append($("<tr />").addClass('delivered_weights')
                        .append($("<td />", {colspan: 7, html: "There are no delivered weights" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        DeliveredWeight.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#delivered_weight_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('delivered_weights')
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.weight}))
				.append($("<td />",{html:val.definition}))
				.append($("<td />",{html:val.description}))
                .append($("<td />")
                    .append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"}))
                )
                .append($("<td />",{html:"<b>"+(val.status == 1 ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"weight_edit_"+val.id, name:"weight_edit_"+val.id }))
				 )
			  )

            $("#weight_edit_"+val.id).live("click", function(e){
                self._updateDeliveredWeight(val);
                e.preventDefault();
            });
			
		},

    addDeliveredWeight          : function()
    {
        var self = this;
        if($("#add_delivered_weight_dialog").length > 0)
        {
            $("#add_delivered_weight_dialog").remove();
        }

        $("<div />",{id:"add_delivered_weight_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Weight:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"weight", id:"weight", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Definition :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"definition", id:"definition", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Color:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"color", id:"color", value:''}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Delivered Weight",
                width          : 500,
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#weight").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the weight");
                            return false;
                        } else if($("#definition").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the definition");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_delivered_weight_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_delivered_weight_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});

                    var myPicker = new jscolor.color(document.getElementById('color'), {})
                    myPicker.fromString('99FF33')  // now you can access API via 'myPicker' variable
                }
            })
    },

    _updateDeliveredWeight         : function(delivered_weight)
    {
        var self = this;
        if($("#edit_delivered_weight_dialog").length > 0)
        {
            $("#edit_delivered_weight_dialog").remove();
        }

        $("<div />",{id:"edit_delivered_weight_dialog"}).append($("<table />",{width:"100%"})
               .append($("<tr />")
                    .append($("<th />",{html:"Weight:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"weight", id:"weight", value:delivered_weight.weight}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Definition :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"definition", id:"definition", value:delivered_weight.definition}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8, text:delivered_weight.description}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Color:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"color", id:"color", value:delivered_weight.color}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Delivered Weight",
                width          : 500,
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#weight").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the weight");
                            return false;
                        } else if($("#definition").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the definition");
                            return false;
                        }  else {
                            self.update(delivered_weight.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_delivered_weight_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(delivered_weight.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(delivered_weight.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(delivered_weight.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_delivered_weight_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((delivered_weight.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});

                    var myPicker = new jscolor.color(document.getElementById('color'), {})
                    myPicker.fromString(delivered_weight.color)  // now you can access API via 'myPicker' variable
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/deliveredweightcontroller.php?action=update",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_delivered_weight_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/deliveredweightcontroller.php?action=update",
        {
            id           : id,
            weight       : $("#weight").val(),
            definition   : $("#definition").val(),
            description  : $("#description").val(),
            color        : $("#color").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_delivered_weight_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/deliveredweightcontroller.php?action=save",
            {
                weight       : $("#weight").val(),
                definition   : $("#definition").val(),
                description  : $("#description").val(),
                color        : $("#color").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_delivered_weight_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    DeliveredWeight.get()
                }
            }, 'json');
    }

}