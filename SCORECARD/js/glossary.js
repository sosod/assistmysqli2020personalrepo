$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    Glossary.get();
    $("#add_glossary").click(function(e){
        if($("#category").val() != '')
        {
            Glossary.addGlossary( $("#category").val() );
        } else {
            jsDisplayResult("error", "error", 'Please enter the glossary category');
        }
        e.preventDefault();
    });
	
});

var Glossary 	= {

		get				: function( category_id )
		{
            $(".glossaries").remove();
			$.getJSON( "../controllers/glossarycontroller.php?action=getAll", {
                category : $("#category").val()
            }, function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#glossary_table").append($("<tr />").addClass('glossaries')
                        .append($("<td />", {colspan: 6, html: "There are no glossaries" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        Glossary.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#glossary_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('glossaries')
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.category}))
                .append($("<td />",{html:val.score_card_terminology}))
				.append($("<td />",{html:val.description}))
                .append($("<td />",{html:"<b>"+(val.status == 1 ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"glossary_table_edit_"+val.id, name:"glossary_table_edit_"+val.id }))
				 )
			  )

            $("#glossary_table_edit_"+val.id).live("click", function(e){
                self._updateGlossary(val);
                e.preventDefault();
            });

		},

    addGlossary          : function(category_id)
    {
        var self = this;
        if($("#add_glossary_table_dialog").length > 0)
        {
            $("#add_glossary_table_dialog").remove();
        }

        $("<div />",{id:"add_glossary_table_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Category:"}))
                    .append($("<td />")
                        .append($("<span />",{html: $("#category :selected").text() }))
                        .append($("<input />",{type:"hidden", name:"category_id", id:"category_id", value: category_id}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Naming:"}))
                    .append($("<td />")
                        .append($("<select />",{type:"text", name:"naming_id", id:"naming_id"}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Glossary",
                width          : 'auto',
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#naming_id").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please select the naming field");
                            return false;
                        } else if($("#description").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the glossary description");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_glossary_table_dialog").dialog("destroy").remove();
                    }
                },
                close          : function(event, ui)
                {
                    $("#add_glossary_table_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});

                    //load supplier categories
                    $.getJSON('../controllers/namingcontroller.php?action=getAll',{
                        type : category_id
                    }, function(naming){
                        var s_html = [];
                        s_html.push("<option value=''>--please select--</option>");
                        $.each(naming, function(ref_id, name_field){
                            s_html.push("<option value='"+ref_id+"'>"+name_field.score_card_terminology+"</option>");
                        });
                        $("#naming_id").html(s_html.join(' '));
                    });
                }
            })
    },

    _updateGlossary         : function(glossary)
    {
        var self = this;
        if($("#edit_glossary_table_dialog").length > 0)
        {
            $("#edit_glossary_table_dialog").remove();
        }

        $("<div />",{id:"edit_glossary_table_dialog"}).append($("<table />",{width:"100%"})
               .append($("<tr />")
                    .append($("<th />",{html:"Category:"}))
                    .append($("<td />")
                        .append($("<span />",{html: glossary.category }))
                        .append($("<input />",{type:"hidden", name:"category_id", id:"category_id", value:glossary.category_id}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Naming :"}))
                    .append($("<td />")
                        .append($("<select />",{ name:"naming_id", id:"naming_id"}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8, text:glossary.description}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Glossary",
                width          : 'auto',
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#naming_id").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please select the naming field");
                            return false;
                        } else if($("#description").val() == "") {
                            $("#message").addClass("ui-state-error").html("please enter the description");
                            return false;
                        } else {
                            self.update(glossary.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_glossary_table_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(glossary.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(glossary.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(glossary.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_glossary_table_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {


                    //load supplier categories
                    $.getJSON('../controllers/namingcontroller.php?action=getAll',{
                        type : glossary.category_id
                    }, function(naming){
                        var s_html = [];
                        s_html.push("<option value=''>--please select--</option>");
                        $.each(naming, function(ref_id, name_field){
                            if(glossary.naming_id === ref_id)
                            {
                                s_html.push("<option value='"+ref_id+"' selected='selected'>"+name_field.score_card_terminology+"</option>");
                            } else {
                                s_html.push("<option value='"+ref_id+"'>"+name_field.score_card_terminology+"</option>");
                            }
                        });
                        $("#naming_id").html(s_html.join(' '));
                    });

                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((glossary.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/glossarycontroller.php?action=update",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_glossary_table_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/glossarycontroller.php?action=update",
        {
            id           : id,
            category_id  : $("#category_id").val(),
            naming_id    : $("#naming_id").val(),
            description  : $("#description").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_glossary_table_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/glossarycontroller.php?action=save",
            {
                category_id  : $("#category").val(),
                naming_id    : $("#naming_id").val(),
                description  : $("#description").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_glossary_table_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    Glossary.get()
                }
            }, 'json');
    }

}