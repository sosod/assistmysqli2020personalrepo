$.widget("ui.scorecard", {

    options		: {
        tableId			    : "score_card_table_"+(Math.floor(Math.random(56) * 33)),
        url 			    : "../controllers/scorecardcontroller.php?action=getScoreCards",
        total 		 	    : 0,
        start			    : 0,
        current			    : 1,
        limit			    : 10,
        editScoreCard	    : false,
        editScoreCardSetting : false,
        updateScoreCard     : false,
        scoreCardAssessment : false,
        scoreCardAssurance  : false,
        assureScoreCard     : false,
        addDeliverable      : false,
        editDeliverable	    : false,
        deleteScoreCardSetting : false,
        updateDeliverable   : false,
        viewScoreCard       : false,
        setupScoreCard      : false,
        viewAction          : false,
        addAction           : false,
        confirmation        : false,
        view			    : "",
        section			    : "",
        page                : "",
        autoLoad            : true,
        showActions         : false,
        financialYear       : 0,
        scoreCardOwner      : 0,
        scoreCardId         : 0,
        actionFilter        : true,
        deliverableFilter   : true
    } ,

    _init          : function()
    {
        if(this.options.autoLoad)
        {
            this._getScoreCards();
        }

    } ,

    _create       : function()
     {
         var self = this;
         var html = [];
         html.push("<table width='100%' class='noborder'>");
             html.push("<tr>");
                 html.push("<td class='noborder'>");
                     html.push("<table class='noborder'>");
                         html.push("<tr>");
                             html.push("<th style='text-align:left;'>Financial Year:</th>");
                             html.push("<td class='noborder'>");
                                 html.push("<select id='_financial_year' name='_financial_year' style='width:200px;'>");
                                 html.push("<option value=''>--please select--</option>");
                                 html.push("</select>");
                             html.push("</td>")
                         html.push("</tr>");
                     html.push("</table>");
                 html.push("</td>");
             html.push("</tr>");
             html.push("<tr>");
                 html.push("<td class='noborder'>");
                    html.push("<table id='"+self.options.tableId+"' width='100%' class='noborder'>");
                      html.push("<tr>");
                        html.push("<td>");
                          html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                            html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                            html.push("<span style='padding:5px; float:left;'>Please select the financial year</span>");
                          html.push("</p>");
                        html.push("</td>");
                      html.push("</tr>");
                    html.push("</table>");
                 html.push("</td>");
             html.push("</tr>");
         html.push("</table>");


         $(this.element).append(html.join(' '));
         self._loadFinancialYears();
         self._getScoreCards();

         $("#_financial_year").live("change", function(){
             if($(this).val() !== "")
             {
                 self.options.financialYear = $(this).val();
                 self._getScoreCards();
             } else {
                 self.options.financialYear  = 0;
                 self._getScoreCards();
             }
         });

         $("#score_card_owner").live("change", function(){
             if($(this).val() !== "")
             {
                 self.options.score_cardOwner = $(this).val();
                 self._getScoreCards();
             } else {
                 self.options.score_cardOwner = 0;
                 self._getScoreCards();
             }
         });

     } ,

    _loadScoreCards     : function()
    {
        var self = this;
        $("#confirmation_score_card_list").html("");
        $.getJSON("../controllers/scorecardcontroller.php?action=getAll",{ status : 'new' }, function(score_cards){
            var scoreCardHtml = []
            scoreCardHtml.push("<option value=''>--please select--</option>");
            $.each(score_cards, function(scoreCardId, score_card){
                scoreCardHtml.push("<option value='"+scoreCardId+"'>"+score_card+"</option>");
            });
            $("#confirmation_score_card_list").html( scoreCardHtml.join(' ') );
        });
    } ,

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#_financial_year").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
            });
            $("#_financial_year").html( finHtml.join(' ') );
        });
    } ,

    _loadScoreCardOwners              : function()
    {
        var self = this;
        $("#score_card_owner").empty();
        $.getJSON("../controllers/score_cardownercontroller.php?action=getAll", function(ownerData){
            var ownerHtml = [];
            ownerHtml.push("<option value=''>--please select--</option>");
            $.each(ownerData ,function(index, directorate) {
                ownerHtml.push("<option value='"+directorate.subid+"'>"+directorate.dirtxt+"</option>");
            })
            $("#score_card_owner").html( ownerHtml.join(' ') );
        });
    },

     _getScoreCards      : function()
     {
         var self = this;
         $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
             .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
         );
         $.getJSON( self.options.url,{
             start		       : self.options.start,
             limit		       : self.options.limit,
             view		       : self.options.view,
             section		   : self.options.section,
             page              : self.options.page,
             financial_year    : self.options.financialYear,
             score_card_owner  : self.options.scoreCardOwner,
             score_card_id     : self.options.scoreCardId
         },function(scoreCardData) {
             $("#loadingDiv").remove();
             $("#"+self.options.tableId).html("");
             $(".more_less").remove();
             $(".confirm_score_card").remove();
             $(".score_card_message").remove();
             if(scoreCardData.financial_year != 0 || scoreCardData.financial_year != '')
             {
                 $("#_financial_year").val(scoreCardData.financial_year);
                 self.options.financialYear = scoreCardData.financial_year;
             }
             if(scoreCardData.total != null)
             {
                 self.options.total = scoreCardData.total
             }
             self._displayPaging(scoreCardData.total_columns);
             self._displayHeaders(scoreCardData.columns);
             if($.isEmptyObject(scoreCardData.scorecards))
             {
                 var _html = [];
                 _html.push("<tr>")
                     _html.push("<td colspan='"+(scoreCardData.total_columns+1)+"'>");
                     _html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                         _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                         if(scoreCardData.hasOwnProperty('score_card_message') && scoreCardData.score_card_message != '')
                         {
                             _html.push("<span style='padding:5px; float:left;'>"+scoreCardData.score_card_message+"</span>");
                         } else {
                             if(self.options.page == 'confirmation' && self.options.scoreCardId == '')
                             {
                                 _html.push("<span style='padding:5px; float:left;'>Please select the scorecard to confirm</span>");
                             } else {
                                 _html.push("<span style='padding:5px; float:left;'>There are no scorecards found</span>");
                             }
                         }
                     _html.push("</p>");
                     _html.push("</td>");
                 _html.push("</tr>");
                 $("#"+self.options.tableId).append( _html.join(' ') );
             } else {
                 self._display(scoreCardData.scorecards, scoreCardData.deliverables, scoreCardData.total_columns, scoreCardData);
             }
         });
     } ,

     _display           : function(score_cards, deliverables, columns, scoreCardData)
     {
         if($(".score_card_more_less").length > 0)
         {
             $(".score_card_more_less").remove();
         }
         if($(".score_card_deliverables").length > 0)
         {
             $(".score_card_deliverables").remove();
         }
         var self = this;
         var i    = 1;
         $.each(score_cards, function(index, score_card){
             i++;
             var html = [];
             if(self.options.page == "view" && scoreCardData.hasOwnProperty('canUpdate'))
             {
                 if(scoreCardData.canUpdate.hasOwnProperty(index) && scoreCardData.canUpdate[index] == true)
                 {
                     html.push("<tr>");
                 } else {
                     html.push("<tr class='view-notowner'>");
                 }
             } else {
                 html.push("<tr>");
             }
             if(self.options.showActions)
             {
                 html.push("<td>");
                     html.push("<a href='#' id='score_card_more_less_"+self.options.tableId+"_"+index+"' class='score_card_more_less'>");
                         html.push("<span class='ui-icon ui-icon-plus'></span>")
                     html.push("</a>");
                 html.push("</td>");

             }
             $.each( score_card, function( key , val){
                 if(key == 'score_card_progress' || key == 'score_card_budget' || key == 'score_card_payment')
                 {
                     html.push("<td style='text-align: right;'>"+val+"</td>");
                 } else {
                     html.push("<td>"+val+"</td>");
                 }
             })
             html.push(self._displayUpdateScoreCardBtn(scoreCardData, index));
             html.push(self._displayEditScoreCardBtn(scoreCardData, index));
             html.push(self._displayEditScoreCardSettingBtn(scoreCardData, index));
             html.push(self._displayAddDeliverableBtn(scoreCardData, index));
             html.push(self._displayAddActionBtn(scoreCardData, index));
             html.push(self._displayScoreCardAssuranceBtn(index));
             html.push(self._displayScoreCardRunBtn(index));
             html.push(self._displayScoreCardViewBtn(index));

             html.push("</tr>");
             if(self.options.showActions)
             {
                 if(!$.isEmptyObject(deliverables))
                 {
                     if(deliverables.hasOwnProperty(index))
                     {
                         html.push("<tr id='score_card_deliverable_"+self.options.tableId+"_"+index+"' class='score_card_deliverables' style='display:none;'>");
                             html.push("<td id='show_deliverable_"+self.options.tableId+"_"+index+"' colspan='"+(parseInt(columns)+2)+"'></td>");
                         html.push("</tr>");
                     }
                 }
             }


             $("#"+self.options.tableId).append( html.join(' ') ).find('.remove_attach').remove();
             $("#"+self.options.tableId).find("#result_message").remove();

             $("#score_card_more_less_"+self.options.tableId+"_"+index).bind("click", function(e){
                 if( $("#score_card_deliverable_"+self.options.tableId+"_"+index).is(":hidden") )
                 {
                     $("#score_card_more_less_"+self.options.tableId+"_"+index+" span").removeClass("ui-icon-plus").addClass("ui-icon-minus");
                 } else {
                     $("#score_card_more_less_"+self.options.tableId+"_"+index+" span").removeClass("ui-icon-minus").addClass("ui-icon-plus");
                 }
                 $("#score_card_deliverable_"+self.options.tableId+"_"+index).toggle();
                 e.preventDefault();
             });

             if(self.options.showActions)
             {
                 if(!$.isEmptyObject(deliverables))
                 {
                     if(deliverables.hasOwnProperty(index))
                     {
                         $("#show_deliverable_"+self.options.tableId+"_"+index).deliverable({scoreCardId:index, section:self.options.section, viewDeliverable:self.options.viewDeliverable, viewAction:self.options.viewAction, page:self.options.page, showActions:self.options.showActions, financialYear:self.options.financialYear, autoLoad: true, actionFilter: self.options.actionFilter, allowFilter: self.options.deliverableFilter });
                     }
                 }
             }
         });

     },

    _displayUpdateScoreCardBtn: function(scoreCardData, scoreCardId)
    {
        var self = this;
        var html = [];
        if(self.options.updateScoreCard)
        {
            html.push("<td>&nbsp;");
            if(scoreCardData.hasOwnProperty('canUpdate'))
            {
                if(scoreCardData.canUpdate.hasOwnProperty(scoreCardId))
                {
                    if(scoreCardData.canUpdate[scoreCardId] == true || self.options.section == "admin")
                    {
                        html.push("<input type='button' value='Update' id='update_"+scoreCardId+"' name='update_"+scoreCardId+"' />");
                        $("#update_"+scoreCardId).live("click", function(){
                            document.location.href = "update_score_card.php?id="+scoreCardId;
                            return false;
                        });
                    }
                }
            }
            //html.push("<input type='button' value='Update Actions' id='update_actions_"+index+"' id='update_actions_"+index+"' />");
            html.push("</td>");
            return html.join('');
        }
        return '';
    },


    _displayEditScoreCardBtn: function(scoreCardData, scoreCardId)
    {
        var self = this;
        var html = [];
        if(self.options.editScoreCard)
        {
            html.push("<td>&nbsp;");
            if(scoreCardData.hasOwnProperty('canEdit'))
            {
                if(scoreCardData.canEdit.hasOwnProperty(scoreCardId))
                {
                    if(scoreCardData.canEdit[scoreCardId] == true)
                    {
                        html.push("<input type='button' value='Edit' id='edit_"+scoreCardId+"' name='edit_"+scoreCardId+"' />");
                        $("#edit_"+scoreCardId).live("click", function(){
                            document.location.href = "edit_score_card.php?id="+scoreCardId;
                            return false;
                        });
                    }
                }
            }

            if(self.options.section == 'admin')
            {
                if( ( (scoreCardData['score_card_detail'][scoreCardId]['status'] & 128) != 128) )
                {
                    html.push("<input type='button' value='Deactivate' id='deactivate_"+scoreCardId+"' name='deactivate_"+scoreCardId+"' />");
                    $("#deactivate_"+scoreCardId).live("click", function(){
                        if(confirm('Are you sure you want to deactivate this score card'))
                        {
                            jsDisplayResult("info", "info", "Deleting scorecard . . . <img  src='../images/loaderA32.gif' />");
                            $.getJSON("../controllers/scorecardcontroller.php?action=deActivateScoreCard", {
                                score_card_id : scoreCardId
                            }, function(response){
                                if(response.error)
                                {
                                    jsDisplayResult("error", "error", response.text);
                                } else {
                                    jsDisplayResult("ok", "ok", response.text);
                                    self._getScoreCards();
                                }
                            });
                        }
                        return false;
                    });
                }
                if( ( (scoreCardData['score_card_detail'][scoreCardId]['status'] & 128) == 128)  )
                {
                    html.push("<input type='button' value='Activate' id='activate_"+scoreCardId+"' name='activate_"+scoreCardId+"' />");
                    $("#activate_"+scoreCardId).live("click", function(){
                        if(confirm('Are you sure you want to activate this score card'))
                        {
                            jsDisplayResult("info", "info", "Activating scorecard . . . <img  src='../images/loaderA32.gif' />");
                            $.getJSON("../controllers/scorecardcontroller.php?action=reActivateScoreCard", {
                                score_card_id : scoreCardId
                            }, function(response){
                                if(response.error)
                                {
                                    jsDisplayResult("error", "error", response.text);
                                } else {
                                    jsDisplayResult("ok", "ok", response.text);
                                    self._getScoreCards();
                                }
                            });
                        }
                        return false;
                    });
                }
            }

            if( self.options.section == "new" || self.options.section == "admin" )
            {
                 html.push("<input type='button' value='Delete' id='delete_score_card_"+scoreCardId+"' id='delete_score_card_"+scoreCardId+"' class='idelete' />");
                 $("#delete_score_card_"+scoreCardId).live("click", function(){
                    if(confirm('Are you sure you want to delete this score card'))
                    {
                        jsDisplayResult("info", "info", "Deleting scorecard . . . <img  src='../images/loaderA32.gif' />");
                        $.getJSON("../controllers/scorecardcontroller.php?action=deleteScoreCard", {
                           score_card_id : scoreCardId
                        }, function(response){
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                              jsDisplayResult("ok", "ok", response.text);
                              self._getScoreCards();
                            }
                        });
                    }
                    return false;
                 });
            }
            html.push("</td>");
            return html.join(' ');
        }
        return '';
    },

    _displayEditScoreCardSettingBtn: function(scoreCardData, scoreCardId)
    {
        var self = this;
        var html = [];
        if(self.options.editScoreCardSetting || self.options.deleteScoreCardSetting)
        {
            html.push("<td>&nbsp;");
            if(self.options.editScoreCardSetting)
            {
                html.push("<input type='button' value='Edit' id='edit_setting_"+scoreCardId+"' name='edit_setting_"+scoreCardId+"' />");
                $("#edit_setting_"+scoreCardId).live("click", function(){
                    document.location.href = "edit_score_card_setting.php?id="+scoreCardId;
                    return false;
                });
            }
            if(self.options.deleteScoreCardSetting)
            {
                html.push("<input type='button' value='Delete' id='delete_setting_"+scoreCardId+"' name='delete_setting_"+scoreCardId+"' class='idelete' />");
                $("#delete_setting_"+scoreCardId).live("click", function(){
                    if(confirm('Are you sure you want to delete this score card'))
                    {
                        jsDisplayResult("info", "info", "Deleting scorecard . . . <img  src='../images/loaderA32.gif' />");
                        $.getJSON("../controllers/scorecardsetupcontroller.php?action=deleteScoreCardSetting", {
                            score_card_id : scoreCardId
                        }, function(response){
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                               jsDisplayResult("ok", "ok", response.text);
                               self._getScoreCards();
                            }
                        });
                    }
                    return false;
                });
            }
            html.push("</td>");
            return html.join(' ');
        }
        return '';
    },

    _displayAddDeliverableBtn: function(scoreCardData, scoreCardId)
    {
        var self = this;
        var html = [];
        if(self.options.addDeliverable)
        {
            html.push("<td>&nbsp;");
            if(scoreCardData.hasOwnProperty('canAddDeliverable'))
            {
                if(scoreCardData.canAddDeliverable.hasOwnProperty(scoreCardId))
                {
                    if(scoreCardData.canAddDeliverable[scoreCardId] == true)
                    {
                        html.push("<input type='button' value='Add Heading' id='add_deliverable_"+scoreCardId+"' name='add_deliverable_"+scoreCardId+"' />");
                        $("#add_deliverable_"+scoreCardId).live("click", function(){
                            document.location.href = "add_deliverable.php?id="+scoreCardId;
                            return false;
                        });
                    }
                }
            }
            html.push("</td>");
            return html.join(' ');
        }
        return '';
    },


    _displayAddActionBtn  : function(scoreCardData, scoreCardId)
    {
        var self = this;
        var html = [];
        if(self.options.addAction)
        {
            if(scoreCardData.hasOwnProperty('canAddAction'))
            {
                if(scoreCardData.canAddAction.hasOwnProperty(scoreCardId))
                {
                    if(scoreCardData.canAddAction[scoreCardId] == true)
                    {
                        html.push("<td>&nbsp;");
                            html.push("<input type='button' value='Add Question' id='add_score_card_action_"+scoreCardId+"' name='add_score_card_action_"+scoreCardId+"' />");
                            $("#add_score_card_action_"+scoreCardId).live("click", function(){
                                document.location.href = "add_score_card_action.php?id="+scoreCardId;
                                return false;
                            });
                        html.push("</td>");
                    }
                }
            }
            return html.join(' ');
        }
        return '';
    },

    _displayScoreCardViewBtn : function(scoreCardId)
    {
        var self = this;
        var html = [];
        if(self.options.viewScoreCard)
        {
            html.push("<td>&nbsp;");
                html.push("<input type='button' value='View Scorecard Details' id='view_score_card_"+scoreCardId+"' name='view_score_card_"+scoreCardId+"' />");
            html.push("</td>");
            $("#view_score_card_"+scoreCardId).live("click", function(){
                document.location.href = "view_score_card.php?id="+scoreCardId;
                return false;
            });
            return html.join('');
        }
        return '';
    },

        _displayScoreCardAssuranceBtn : function(scoreCardId)
        {
            var self = this;
            var html = [];
            if(self.options.scoreCardAssurance)
            {
                html.push("<td>&nbsp;");
                html.push("<input type='button' value='Assurance' id='score_card_assurance_"+scoreCardId+"' name='score_card_assurance_"+scoreCardId+"' />");
                $("#score_card_assurance_"+scoreCardId).live("click", function(){
                    document.location.href = "score_card_assurance.php?id="+scoreCardId;
                    return false;
                });
                html.push("</td>");
                return html.join(' ');
            }
            return '';
        },

     _displayScoreCardRunBtn : function(scoreCardId)
     {
          var self = this;
          var html = [];
          if(self.options.setupScoreCard)
          {
              html.push("<td>&nbsp;");
                  html.push("<input type='button' value='Setup' id='setup_score_card_"+scoreCardId+"' name='setup_score_card_"+scoreCardId+"' />");
                  $("#setup_score_card_"+scoreCardId).live("click", function(){
                      document.location.href = "score_card_setup.php?id="+scoreCardId;
                      return false;
                  });
              html.push("</td>");
              return html.join(' ');
          }
          return '';
      },

     _displayHeaders     : function(headers)
     {

         var self = this;
         var html = [];
         html.push("<tr>");
         if(self.options.showActions)
         {
             html.push("<th>&nbsp;</th>");
         }
         $.each( headers , function( index, head ){
             html.push("<th>"+head+"</th>");
         });
         if(self.options.setupScoreCard)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.scoreCardAssurance)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.updateScoreCard)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.editScoreCardSetting)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.editScoreCard)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.addDeliverable)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.viewScoreCard)
         {
             html.push("<th>&nbsp;</th>");
         }
         if(self.options.addAction)
         {
             html.push("<th>&nbsp;</th>");
         }


         html.push("</tr>");
         $("#"+self.options.tableId).append(html.join(' '));
     },

    _displayPaging	: function(columns)
     {
        var self  = this;
        var html  = [];
        var pages;
        if( self.options.total%self.options.limit > 0){
            pages   = Math.ceil(self.options.total/self.options.limit);
        } else {
            pages   = Math.floor(self.options.total/self.options.limit);
        }
        html.push("<tr>");
            if(self.options.showActions)
            {
                html.push("<td>&nbsp;</td>");
            }
            html.push("<td colspan='"+(columns + 1)+"'>");
            html.push("<input type='button' value=' |< ' id='first' name='first' />");
            html.push("<input type='button' value=' < ' id='previous' name='previous' />");
            if(pages != 0)
            {
                html.push("Page <select id='select_page' name='select_page'>");
                for(var p=1; p<=pages; p++)
                {
                    html.push("<option value='"+p+"' "+(self.options.current == p ? "selected='selected'" : "")+">"+p+"</option>");
                }
                html.push("</select>");
            } else {
                html.push("Page <select id='select_page' name='select_page' disabled='disabled'>");
                html.push("</select>");
            }
            html.push(" of "+(pages == 0 || isNaN(pages) ? 0 : pages)+" pages");
            //html.push(self.options.current+"/"+(pages == 0 || isNaN(pages) ? 1 : pages));
            html.push("<input type='button' value=' > ' id='next' name='next' />");
            html.push("<input type='button' value=' >| ' id='last' name='last' />");
            if(self.options.updateScoreCard)
            {
                html.push("&nbsp;");
            }
            if(self.options.editScoreCard)
            {
                html.push("&nbsp;");
            }
            if(self.options.addAction)
            {
                html.push("&nbsp;");
            }
            if(self.options.viewScoreCard)
            {
                html.push("&nbsp;");
            }

                html.push("<span style='float:right;'>");
                    html.push("<b>Quick Search for Scorecard:</b> SC <input type='text' name='sc_id' id='sc_id' value='' />");
                    html.push("&nbsp;&nbsp;&nbsp;");
                    html.push("<input type='button' name='search' id='search' value='Go' />");
                html.push("</span>");
            html.push("</td>");
        html.push("</tr>");
        $("#"+self.options.tableId).append(html.join(' '));


        $("#select_page").live("change", function(){
            if($(this).val() !== "" && parseFloat($(this).val())>0)
            {
                self.options.start   = parseFloat($(this).val() -1) * parseFloat(self.options.limit);
                self.options.current = parseFloat($(this).val());
                self._getScoreCards();
            }
        });
        $("#search").live("click", function(e){
            var scoreCardId = $("#sc_id").val();
            if(scoreCardId != "")
            {
                self.options.scoreCardId = $("#sc_id").val();
                self._getScoreCards();
            } else {
                $("#sc_id").addClass('ui-state-error');
                $("#sc_id").focus().select();
            }
            e.preventDefault();
        });

        if(self.options.current < 2)
        {
            $("#first").attr('disabled', 'disabled');
            $("#previous").attr('disabled', 'disabled');
        }
        if((self.options.current == pages || pages == 0))
        {
            $("#next").attr('disabled', 'disabled');
            $("#last").attr('disabled', 'disabled');
        }
        $("#next").bind("click", function(evt){
            self._getNext( self );
        });
        $("#last").bind("click",  function(evt){
            self._getLast( self );
        });
        $("#previous").bind("click",  function(evt){
            self._getPrevious( self );
        });
        $("#first").bind("click",  function(evt){
            self._getFirst( self );
        });
    } ,

    _getNext  			: function(self)
    {
        self.options.current = (self.options.current + 1);
        self.options.start   = (self.options.current-1) * self.options.limit;
        this._getScoreCards();
    },

    _getLast  			: function( self )
    {
        self.options.current =  Math.ceil( parseFloat( self.options.total )/ parseFloat( self.options.limit ));
        self.options.start   = parseFloat(self.options.current-1) * parseFloat( self.options.limit );
        this._getScoreCards();
    },
    _getPrevious    	: function( self )
    {
        self.options.current  = parseFloat(self.options.current) - 1;
        self.options.start 	= (self.options.current-1) * self.options.limit;
        this._getScoreCards();
    },
    _getFirst  			: function( self )
    {
        self.options.current  = 1;
        self.options.start 	= 0;
        this._getScoreCards();
    } ,


    _getScoreCardDetail              : function()
    {
        var self = this;
        $("body").append($("<div />",{id:"loadingDiv", html:"Loading . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"300px", left:"350px", border:"0px solid #FFFFF", padding:"5px"})
        );
        var scoreCardId = self.options.scoreCardId;
        var _tableHtml = []
        _tableHtml.push("<tr>");
            _tableHtml.push("<td class='noborder' align='left'>");
              _tableHtml.push("<table id='score_card_"+scoreCardId+"_detail' class='noborder'></table>");
            _tableHtml.push("</td>");
            _tableHtml.push("<td class='noborder' style='text-align: left;'>");
               _tableHtml.push("<table id='score_card_"+scoreCardId+"_deliverable_weight_detail' width='100%' align='left'></table>");
            _tableHtml.push("</td>");
        _tableHtml.push("</tr>");
        _tableHtml.push("<tr>");
            _tableHtml.push("<td class='noborder' colspan='2' align='left'>");
               _tableHtml.push("<table id='score_card_deliverable_"+scoreCardId+"_detail' width='100%'></table>");
            _tableHtml.push("</td>");
        _tableHtml.push("</tr>");
        $("#"+self.options.tableId).append(_tableHtml.join(' '));

        $.getJSON('../controllers/scorecardcontroller.php?action=getDetailScoreCard',{
            start		      : self.options.start,
            limit		      : self.options.limit,
            view		      : self.options.view,
            section		      : self.options.section,
            page              : self.options.page,
            financialYear     : self.options.financialYear,
            scoreCardOwner    : self.options.scoreCardOwner,
            scoreCardId       : self.options.scoreCardId
        },function(scoreCardData) {
            $("#loadingDiv").remove();
            $(".score_card_message").remove();
            self._displayScoreCardDetail( scoreCardData.score_card );
            self._displayCategorized( scoreCardData );
            //$("#score_card_"+scoreCardId+"_deliverable_weight_detail")._displayWeights( scoreCardData.deliverables );
            //$("#score_card_"+scoreCardId+"_deliverable_weight_detail")._displayCategorized( scoreCardData.deliverables );
        });

    } ,

    _displayScoreCardDetail          : function(score_card)
    {

        var self = this;
        var scoreCardHtml = [];
        scoreCardHtml.push("<table class='noborder'>");
        $.each(score_card, function(column, score_card) {
            scoreCardHtml.push("<tr>")
              scoreCardHtml.push("<th style='text-align: left;'>"+score_card.header+"</th>");
              scoreCardHtml.push("<td>"+score_card.value+"</td>");
            scoreCardHtml.push("</tr>");
        });
        scoreCardHtml.push("</table>");
        $("#score_card_"+self.options.scoreCardId+"_detail").append( scoreCardHtml.join(' ')).css({'text-align':'left'});
    },

    _displayCategorized             : function(scoreCardData)
    {
        var self = this;
        var categoryHtml = [];
        var summaryHtml  = [];
        if(!$.isEmptyObject(scoreCardData.category_deliverable))
        {
            self._displayDeliverableHeader(scoreCardData.columns);
            $.each(scoreCardData.category_deliverable,function(deliverable_category, deliverable_data){
                categoryHtml.push("<tr>");
                    categoryHtml.push("<td  class='noborder' colspan='"+(scoreCardData.total_columns+1)+"'><b>"+deliverable_category+"</b></td>");
                categoryHtml.push("</tr>");
                categoryHtml.push("<tr>");
                    categoryHtml.push("<td  class='noborder' colspan='"+(scoreCardData.total_columns+1)+"' id='ca'></td>");
                categoryHtml.push("</tr>");
                //$("#score_card_deliverable_"+self.options.scoreCardId+"_detail").deliverable({editDeliverable: true, scoreCardId: self.options.scoreCardId, category_id: '', showSubDeliverable: true, showActions: true, editAction: true });
                //categoryHtml.push( self._displayDeliverables(scoreCardData, deliverable_data, scoreCardData.category_weights, scoreCardData.total_actions) );
            });
            summaryHtml.push( self._displayDeliverableSummary(scoreCardData.key_measures) );
        }
        //$("#score_card_deliverable_"+self.options.scoreCardId+"_detail").append(categoryHtml.join(' ')).css({'text-align':'left'});
        //$("#score_card_"+self.options.scoreCardId+"_deliverable_weight_detail").append(summaryHtml.join(' ')).css({'text-align':'left'});
    },

    _displayDeliverableHeader       : function(headers)
    {
        var self = this;
        var html = [];
        html.push("<tr>");
        html.push("<th>&nbsp;</th>");
        $.each( headers , function( index, head ){
            html.push("<th>"+head+"</th>");
        });
        html.push("<th>Number Of Actions</th>");
        if(self.options.updateDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.editDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.addAction)
        {
            html.push("<th>&nbsp;</th>");
        }
        if(self.options.viewDeliverable)
        {
            html.push("<th>&nbsp;</th>");
        }
        html.push("</tr>");
        $("#score_card_deliverable_"+self.options.scoreCardId+"_detail").append(html.join(' '));
    },

    _displayDeliverables            : function(scoreCardData, deliverable_data, weights, action_data)
    {
       var self = this;
       var deliverableHtml = [];
        $.each(deliverable_data.deliverables, function(deliverable_id, deliverable){
            deliverableHtml.push("<tr>");
                deliverableHtml.push("<td>");
                    deliverableHtml.push("<a href='#' id='deliverable_more_less_"+self.options.tableId+"_"+deliverable_id+"' class='deliverable_more_less'>")
                       deliverableHtml.push("<span class='ui-icon ui-icon-plus'></span>")
                    deliverableHtml.push("</a>")
                deliverableHtml.push("</td>");
            $.each(deliverable, function(field, val){
                deliverableHtml.push("<td>"+val+"</td>");
            });
            if(action_data.hasOwnProperty(deliverable_id))
            {
                deliverableHtml.push("<td>"+action_data[deliverable_id]+"</td>");
            } else {
                deliverableHtml.push("<td>0</td>");
            }
            deliverableHtml.push(self._deliverableEditButton(scoreCardData, deliverable_id));
            deliverableHtml.push("</tr>");
            if(deliverable_data.hasOwnProperty('sub_deliverable'))
            {
                if(deliverable_data['sub_deliverable'].hasOwnProperty(deliverable_id))
                {
                    $.each(deliverable_data['sub_deliverable'][deliverable_id], function(sub_deliverable_id, sub_deliverable){
                        deliverableHtml.push("<tr class='view-notowner'>");
                        deliverableHtml.push("<td>")
                            deliverableHtml.push("<a href='#' id='sub_deliverable_more_less_"+self.options.tableId+"_"+sub_deliverable_id+"' class='sub_deliverable_more_less'>")
                                deliverableHtml.push("<span class='ui-icon ui-icon-plus'></span>")
                            deliverableHtml.push("</a>");
                        deliverableHtml.push("</td>");
                        $.each(sub_deliverable, function(sub_field, sub_val) {
                            deliverableHtml.push("<td>"+sub_val+"</td>");
                        });
                        if(action_data.hasOwnProperty(sub_deliverable_id))
                        {
                            deliverableHtml.push("<td>"+action_data[sub_deliverable_id]+"</td>");
                        }
                        deliverableHtml.push(self._deliverableEditButton(scoreCardData, sub_deliverable_id));
                        deliverableHtml.push("</tr>");
                    });
                }
            }
        })
/*        deliverableHtml.push("<tr>");
        $.each(weights, function(w_index, w_val){
            if(w_index == 'delivered_weight')
            {
                deliverableHtml.push("<td>"+w_val+"</td>");
            } else if(w_index == 'other_weight') {
                deliverableHtml.push("<td>"+w_val+"</td>");
            } else if(w_index == 'quality_weight')  {
                deliverableHtml.push("<td>"+w_val+"</td>");
            } else {
                deliverableHtml.push("<td></td>");
            }
        })
        deliverableHtml.push("</tr>");*/
        return deliverableHtml;
    } ,

    _displayDeliverableSummary                 : function(key_measures)
    {
        var self = this;
        var summaryHtml = []
        var total_weight = 0;
        summaryHtml.push("<tr>");
            summaryHtml.push("<th>Key Measures</th>");
            summaryHtml.push("<th>Weights</th>");
        summaryHtml.push("</tr>");
        $.each(key_measures, function(category, category_deliverable_summary){
            var total_category_weight = 0;
            summaryHtml.push("<tr>");
                summaryHtml.push("<td style='text-align: left;' colspan='2'><b>"+category+"</b></td>");
            summaryHtml.push("</tr>");
            $.each(category_deliverable_summary, function(deliverable_id, deliverable_summary){
                summaryHtml.push("<tr>");
                    summaryHtml.push("<td style='text-align: left;'>"+deliverable_summary.deliverable_name+"</td>");
                    summaryHtml.push("<td style='text-align: right;'>"+deliverable_summary.total_weight+"</td>");
                summaryHtml.push("</tr>");
                total_category_weight = parseInt(deliverable_summary.total_weight) + parseInt(total_category_weight);
            });
            total_weight          = parseInt(total_category_weight) + parseInt(total_weight);
            summaryHtml.push("<tr class='view-notowner'>");
              summaryHtml.push("<td style='text-align: left;'>Total for "+category+"</td>");
              summaryHtml.push("<td style='text-align: right;' colspan='2'><b>"+total_category_weight+"</b></td>");
            summaryHtml.push("</tr>");
        });
        summaryHtml.push("<tr>");
            summaryHtml.push("<td style='text-align: left;'>Score</td>");
            summaryHtml.push("<td style='text-align: right;' colspan='2'><b>"+total_weight+"</b></td>");
        summaryHtml.push("</tr>");
        return summaryHtml;
    },

    _updateButton        : function(deliverableData, deliverable_id)
    {
        var self = this;
        if(self.options.updateDeliverable)
        {
            html.push("<td>&nbsp;");
            if(deliverableData.hasOwnProperty('canUpdate'))
            {
                if(deliverableData.canUpdate.hasOwnProperty(deliverable_id))
                {
                    if(deliverableData.canUpdate[deliverable_id] == true || self.options.section == "admin")
                    {
                        html.push("<input type='button' value='Update deliverable' id='update_"+deliverable_id+"' name='update_"+deliverable_id+"' />");
                        $("#update_"+deliverable_id).live("click", function(){
                            document.location.href = "update_deliverable.php?id="+index;
                            return false;
                        });
                    }
                }
            }
            html.push("</td>");
            return html;
        }
        return '';
    }
})