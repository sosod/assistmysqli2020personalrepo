/**
 * Created with JetBrains PhpStorm.
 * User: admire
 * Date: 2013/09/24
 * Time: 6:15 AM
 * To change this template use File | Settings | File Templates.
 */
$(function(){

    $("#add_more_supplier").click(function(e){
        var lastSupplier    = jQuery(".supplier_budget").last().attr("id");
        $.get('../controllers/contractcontroller.php?action=getSupplerBudgetHtml',{
            current_last : lastSupplier
        }, function(supplierBudgetHtml){
            $("#add_more_suppler_button").before( supplierBudgetHtml );
        }, 'html');
        e.preventDefault();
    });

    $("#financial_year_id").live("change", function(){
/*        $.getJSON("../controllers/assessmentfrequencycontroller.php?action=getAll", {
            financial_year		: $("#financial_year_id").val()
        }, function( assessment_frequencies ){
            var selectHtml = [];
            selectHtml.push("<option value=''>--please select--</option>");
            if(!$.isEmptyObject(assessment_frequencies) )
            {
                $.each( assessment_frequencies, function( index, assessment_frequency){
                    selectHtml.push("<option value='"+assessment_frequency.id+"'>"+assessment_frequency.name+"</option>");
                });
            }
            $("#assessment_frequency_id").html(selectHtml);
        });

        return false;*/
    });


    $("#assessment_frequency_id").live("change", function(){
        $("#assessment_frequency_date_picker").show();
        return false;
    });

    $(".assessed").hide();
    $(".weight").hide();
    $(".category").hide();
    $(".other-criteria").hide();

    $("#contract_is_contract_assessed").live('change', function(){
        if($(this).val() == 1)
        {
            $(".assessed").show();
            $(".edit-assessed").show();
        } else {
            $(".assessed").hide();
            $(".edit-assessed").hide();
        }
        e.preventDefault();
    });



    $("#contract_weight_assigned").live('change', function(e){
        if($(this).val() == 1)
        {
            $(".weight").show();
            $(".weights").show();
        } else {
            $(".weight").hide();
            $(".weights").hide();
        }
        e.preventDefault();
    });

    $("#other_weight").on('change', function(){
        $(".other-weight").toggle();
        e.preventDefault();
    });

    $("#contract_is_assessed_other").on('change', function(e){
        $(".other-criteria").toggle();
        $(".edit-other-criteria").toggle();
        e.preventDefault();
    });


    $("#template_id").live("change", function(){
        if($(this).val() == "")
        {
            $(".delStatus").show();
            $(".contractDefaults").show();
        } else{
            $(".contractDefaults").hide();
            $(".delStatus").hide();
        }
    })


    $("#save").click(function(e){

        if($("#financial_year_id").val() == '')
        {
            $("#financial_year_id").focus().select()
            jsDisplayResult("error", "error", 'Financial year is required');
            return false;
        } else if($("#name").val() == ''){
            $("#name").focus().select()
            jsDisplayResult("error", "error", 'Project name is required');
            return false;
        } else if($("#type_id").val() == ''){
            $("#type_id").focus().select()
            jsDisplayResult("error", "error", 'Contract type is required');
            return false;
        } else if($("#category_id").val() == ''){
            $("#category_id").focus().select()
            jsDisplayResult("error", "error", 'Contract category is required');
            return false;
        } else if($("#supplier_id").val() == ''){
            $("#supplier_id").focus().select()
            jsDisplayResult("error", "error", 'Contract supplier is required');
            return false;
        } else if($("#contract_signature_of_tender").val() == ''){
            $("#contract_signature_of_tender").focus().select()
            jsDisplayResult("error", "error", 'Signature of tender is required');
            return false;
        } else if($("#contract_signature_of_sla").val() == ''){
            $("#contract_signature_of_sla").focus().select()
            jsDisplayResult("error", "error", 'Signature of contract is required');
            return false;
        } else if($("#contract_completion_date").val() == ''){
            $("#contract_completion_date").focus().select()
            jsDisplayResult("error", "error", 'Completion date is required');
            return false;
        } else if($("#manager_id").val() == ''){
            $("#manager_id").focus().select()
            jsDisplayResult("error", "error", 'Contract manager is required');
            return false;
        }/* else if($("#owner_id").val() !== '') {
            $("#owner_id").focus().select()
            jsDisplayResult("error", "error", 'Contract owner  is required');
            return false;
        }*/ else {
            jsDisplayResult("info", "info", "Saving contract . . .  <img src='../images/loaderA32.gif' />");
            $.getJSON("../controllers/contractcontroller.php?action=save",{
                contract : $("#contract-form").serialize()
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    jsDisplayResult("ok", "ok", response.text);
                    if($("#template_id").val() != '')
                    {
                        jsDisplayResult("info", "info", "Saving tempalte data . . .  <img src='../images/loaderA32.gif' />");
                        document.location.href = "confirmation.php?id="+response.id;
                    } else {
                        jsDisplayResult("info", "info", "<img src='../images/loaderA32.gif' />");
                        setTimeout(function(){
                            document.location.href = 'index.php?saved=1';
                        }, 400);
                    }
                    $("input[type='text']").each(function(){
                        $(this).val('');
                    });
                    $('select').each(function(){
                        $(this).val('');
                    });
                    $('textarea').each(function(){
                        $(this).val('');
                    });
                }
            });
        }

        e.preventDefault();
    })

    $("#edit_contract").live("click", function(e){

        if($("#financial_year_id").val() == '')
        {
            $("#financial_year_id").focus().select()
            jsDisplayResult("error", "error", 'Financial year is required');
            return false;
        } else if($("#name").val() == ''){
            $("#name").focus().select()
            jsDisplayResult("error", "error", 'Project name is required');
            return false;
        } else if($("#type_id").val() == ''){
            $("#type_id").focus().select()
            jsDisplayResult("error", "error", 'Contract type is required');
            return false;
        } else if($("#category_id").val() == ''){
            $("#category_id").focus().select()
            jsDisplayResult("error", "error", 'Contract category is required');
            return false;
        } else if($("#supplier_id").val() == ''){
            $("#supplier_id").focus().select()
            jsDisplayResult("error", "error", 'Contract supplier is required');
            return false;
        } else if($("#contract_signature_of_tender").val() == ''){
            $("#contract_signature_of_tender").focus().select()
            jsDisplayResult("error", "error", 'Signature of tender is required');
            return false;
        } else if($("#contract_signature_of_sla").val() == ''){
            $("#contract_signature_of_sla").focus().select()
            jsDisplayResult("error", "error", 'Signature of contract is required');
            return false;
        } else if($("#contract_completion_date").val() == ''){
            $("#contract_completion_date").focus().select()
            jsDisplayResult("error", "error", 'Completion date is required');
            return false;
        } else if($("#manager_id").val() == ''){
            $("#manager_id").focus().select()
            jsDisplayResult("error", "error", 'Contract manager is required');
            return false;
        } /*else if($("#owner_id").val() == ''){
            $("#owner_id").focus().select()
            jsDisplayResult("error", "error", 'Contract owner  is required');
            return false;
        } */else {
            jsDisplayResult("info", "info", "Updating contract . . .  <img src='../images/loaderA32.gif' />");
            $.getJSON("../controllers/contractcontroller.php?action=editContract",{
                contract : $("#contract-form").serialize()
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    if(response.updated)
                    {
                       jsDisplayResult("ok", "ok", response.text);
                    } else {
                       jsDisplayResult("info", "info", response.text);
                    }
                }
            });
        }

        e.preventDefault();
    })

    $("#status_id").change(function(e) {
        var thisValue = $(this).val();
        if(thisValue == 3)
        {
            $("#approval").removeAttr("disabled");
        } else {
            $("#approval").attr("disabled", "disabled");
        }
        e.preventDefault();
    });



    $("#update_contract").click(function(e){
        if($("#response").val() == '') {
            $("#response").focus().select()
            jsDisplayResult("error", "error", 'Response message is required');
            return false;
        } else if($("#status_id").val() == '' ) {
            $("#status_id").focus().select()
            jsDisplayResult("error", "error", 'Status is required');
            return false;
        }  else {
            jsDisplayResult("info", "info", "Updating contract . . .  <img src='../images/loaderA32.gif' />");
            $.post("../controllers/contractcontroller.php?action=updateContract", {
                response       : $("#response").val(),
                status_id      : $("#status_id").val(),
                remind_on      : $("#remind_on").val(),
                id             : $("#contract_id").val(),
                approval       : ($("#approval").is(":checked") ? 1 : 0)
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text);
                    } else {
                        jsDisplayResult("info", "info", response.text);
                    }
                }
            }, 'json');
        }
        e.preventDefault();
    });


});

var Contract    = {


    contractDefaultsSetting : function(contract_id)
    {
        var html = [];
        html.push("<table width='100%'>");
          html.push("<tr>");
            html.push("<th>Does this contract have deliverables ?:</th>");
            html.push("<td style='text-align: left'>");
              html.push("<select name='contract_default[has_deliverable]' id='has_deliverable' >");
                html.push("<option value='1'>Yes</option>");
                html.push("<option value='0'>No</option>")
              html.push("</select>");
            html.push("</td>")
          html.push("</tr>");
          html.push("<tr>");
              html.push("<th>Does this contract have sub-deliverables ?:</th>");
            html.push("<td style='text-align: left'>");
              html.push("<select name='contract_default[has_sub_deliverable]' id='has_sub_deliverable'>");
                html.push("<option value='1'>Yes</option>");
                html.push("<option value='0'>No</option>")
              html.push("</select>");
            html.push("</td>")
          html.push("</tr>");
          html.push("<tr>");
           html.push("<th>Must weights be assigned to this contract ?:</th>");
            html.push("<td style='text-align: left'>");
             html.push("<select name='contract_default[weight_assigned]' id='weight_assigned'>");
               html.push("<option value='0'>No</option>");
               html.push("<option value='1'>Yes</option>");
            html.push("</select>");
           html.push("</td>")
         html.push("</tr>");
         html.push("<tr class='weight delivered'>");
           html.push("<th style='text-align: left'>Delivered ?:</th>");
           html.push("<td>");
              html.push("<select name='contract_default[delivered_weight_assigned]' id='delivered_weight'>");
                html.push("<option value='1'>Yes</option>");
                html.push("<option value='0'>No</option>");
           html.push("</select>");
           html.push("</td>")
         html.push("</tr>");
         html.push("<tr class='weight quality'>");
            html.push("<th style='text-align: left'>Quality ?:</th>");
            html.push("<td>");
                html.push("<select name='contract_default[quality_weight]' id='quality_weight'>");
                html.push("<option value='1'>Yes</option>");
                html.push("<option value='0'>No</option>")
                html.push("</select>");
            html.push("</td>")
         html.push("</tr>");
         html.push("<tr class='weight other'>");
            html.push("<th style='text-align: left'>Other ?:</th>");
            html.push("<td>");
                html.push("<select name='contract_default[other_weight]' id='other_weight'>");
                html.push("<option value='1'>Yes</option>");
                html.push("<option value='0'>No</option>")
                html.push("</select>");
            html.push("</td>");
         html.push("</tr>");
         html.push("<tr class='other-weight category'>");
            html.push("<th style='text-align: left'>Other Category ?:</th>");
            html.push("<td>");
                html.push("<input type='text' name='other_category' id='other_category'>");
            html.push("</td>")
         html.push("</tr>");
         html.push("<tr>");
             html.push("<td colspan='2'>");
                html.push("<span id='message'></span>");
             html.push("</td>")
         html.push("</tr>");
         html.push("<input type='hidden' name='contract_id' id='contract_id' value='"+contract_id+"' />");
        html.push("</table>");

        if($("#contract_default_"+contract_id).length > 0)
        {
            $("#contract_default_"+contract_id).remove();
        }

        $("<div />", {id: "contract_default_"+contract_id}).append(html.join(' '))
         .dialog({
              autoOpen  : true,
              modal     : true,
              title     : "Setup Contract Defaults",
              width     : "400",
              position  : "top, left",
              buttons   : {
                            "Save Contract Defaults"   : function()
                            {
                                $("#message").html('Saving contract defaults . . . .');
                                $.post("../controllers/contractcontroller.php?action=saveContractDefaults", {
                                    has_deliverable     : $("#has_deliverable :selected").val(),
                                    has_sub_deliverable : $("#has_sub_deliverable :selected").val(),
                                    weight_assigned     : $("#weight_assigned :selected").val(),
                                    delivered_weight    : $("#delivered_weight :selected").val(),
                                    quality_weight      : $("#quality_weight :selected").val(),
                                    other_weight        : $("#other_weight :selected").val(),
                                    other_category      : $("#other_category").val(),
                                    contract_id         : contract_id
                                }, function(response){
                                   if(response.error)
                                   {
                                       jsDisplayResult("error", "error", response.text);
                                   } else {
                                       jsDisplayResult("ok", "ok", response.text);
                                       setTimeout(function() {
                                         document.location.href = "deliverable.php";
                                       }, 750);
                                   }
                                },'json')
                            }
              } ,
              close     : function(event, ui)
              {
                  $("#contract_default_"+contract_id).dialog('destroy').remove();
              },
              open       : function(event, ui)
              {
                  $(".weight").hide();
                  $(".category").hide();

                  $("#weight_assigned").on('change', function(e){
                      $(".weight").toggle();
                      e.preventDefault();
                  });

                  $("#other_weight").on('change', function(){
                      $(".other-weight").toggle();
                      e.preventDefault();
                  });

              }
         })

    }


}


function attachContractDocuments()
{
    $("#file_upload").html("uploading  . . . <img  src='../images/loaderA32.gif' />");

    $.ajaxFileUpload
    (
        {
            url			  : '../controllers/contractcontroller.php?action=saveAttachment',
            secureuri	  : false,
            fileElementId : 'contact_attachment_document',
            dataType	  : 'json',
            success       : function (response, status)
            {
                $("#file_upload").html("");
                if(response.error )
                {
                    $("#file_upload").html(response.text )
                } else {
                    $("#file_upload").html("");
                    if(response.error)
                    {
                        $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                    } else {
                        $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                        ).append($("<div />",{id:"files_uploaded"}))
                        if(!$.isEmptyObject(response.files))
                        {
                            var list = [];
                            list.push("<span>Files uploaded ..</span><br />");
                            $.each(response.files, function(ref, file) {
                                list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                                    list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                    list.push("<span style='margin-left:5px;'><a href='../controller/contractcontroller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                    list.push("<input type='hidden' name='contract[documents]["+file.key+"]' value='"+file.name+"' id='contract_document_"+ref+"' />");
                                list.push("</p>");
                            });
                            $("#files_uploaded").html(list.join(' '));

                            $(".delete").live('click', function(e){
                                var id = this.id
                                var ext = $(this).attr('title');
                                $.post('../controllers/contractcontroller.php?action=removeContractAttachment',
                                    {
                                        attachment : id,
                                        ext        : ext
                                    }, function(response){
                                        if(response.error)
                                        {
                                            $("#result_message").html(response.text)
                                        } else {
                                            $("#result_message").addClass('ui-state-ok').html(response.text)
                                            $("#li_"+id).fadeOut();
                                        }
                                    },'json');
                                e.preventDefault();
                            });
                            $("#action_attachment").val("");
                        }
                    }

                    if( $("#delmessage").length != 0)
                    {
                        $("#delmessage").remove()
                    }
                }
            },
            error: function (data, status, e)
            {
                $("#fileupload").html( "Ajax error -- "+e+", please try uploading again");
            }
        }
    )
    return false;
}
