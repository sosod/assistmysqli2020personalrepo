$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    ScoreCardStatus.get();
    $("#add_score_card_status").click(function(e){
        ScoreCardStatus.addScoreCardStatus();
        e.preventDefault();
    });
	
});

var ScoreCardStatus 	= {

		get				: function()
		{
            $(".score_card_statuses").remove();
			$.getJSON( "../controllers/scorecardstatuscontroller.php?action=getAll", function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#score_card_status_table").append($("<tr />").addClass('score_card_statuses')
                        .append($("<td />", {colspan: 6, html: "There are no score card statuses" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        ScoreCardStatus.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#score_card_status_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('score_card_statuses')
                .append($("<td />",{html:val.id}))
                .append($("<td />",{html:val.name}))
                .append($("<td />",{html:val.client_terminology}))
                .append($("<td />")
                    .append($("<span />",{html:val.color}).css({"background-color":"#"+val.color, "padding":"5px"}))
                )
                .append($("<td />",{html:"<b>"+((val.status & 1) == 1  ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"status_edit_"+val.id, name:"status_edit_"+val.id }))
				 )
			  )

            $("#status_edit_"+val.id).live("click", function(e){
                self._updateScoreCardStatus(val);
                e.preventDefault();
            });
			
		},

    addScoreCardStatus          : function()
    {
        var self = this;
        if($("#add_score_card_status_dialog").length > 0)
        {
            $("#add_score_card_status_dialog").remove();
        }

        $("<div />",{id:"add_score_card_status_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"ScoreCard Status Name:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:''}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Scorecard Status Client Terminology:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"client_terminology", id:"client_terminology", value:''}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Scorecard Status Color:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"color", id:"color", value:''}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Scorecard Status",
                width          : 'auto',
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the scorecard name");
                            return false;
                        } else if($("#client_terminology").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the scorecard client terminology");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_score_card_status_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_score_card_status_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});

                    var myPicker = new jscolor.color(document.getElementById('color'), {})
                    myPicker.fromString('99FF33')  // now you can access API via 'myPicker' variable
                }
            })



    },

    _updateScoreCardStatus         : function(score_card_status)
    {
        var self = this;
        if($("#edit_score_card_status_dialog").length > 0)
        {
            $("#edit_score_card_status_dialog").remove();
        }

        $("<div />",{id:"edit_score_card_status_dialog"}).append($("<table />",{width:"100%"})
               .append($("<tr />")
                    .append($("<th />",{html:"Scorecard Status Name:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:score_card_status.name, disabled: 'disabled' }))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Scorecard Status Client Terminology:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"client_terminology", id:"client_terminology", value:score_card_status.client_terminology}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Scorecard Status Color:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"color", id:"color", value:score_card_status.color}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Scorecard Status",
                width          : 'auto',
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the score card status name");
                            return false;
                        } else if($("#client_terminology").val() == "") {
                            $("#message").addClass("ui-state-error").html("please enter the score card status client terminology");
                            return false;
                        } else {
                            self.update(score_card_status.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_score_card_status_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(score_card_status.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(score_card_status.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(score_card_status.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_score_card_status_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((score_card_status.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    if((score_card_status.status & 4) == 4)
                    {
                        $(deleteBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});

                    var myPicker = new jscolor.color(document.getElementById('color'), {})
                    myPicker.fromString(score_card_status.color)  // now you can access API via 'myPicker' variable
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/scorecardstatuscontroller.php?action=update",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_score_card_status_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/scorecardstatuscontroller.php?action=update",
        {
            id                  : id,
            name                : $("#name").val(),
            client_terminology  : $("#client_terminology").val(),
            color               : $("#color").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_score_card_status_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/scorecardstatuscontroller.php?action=save",
            {
                name                : $("#name").val(),
                client_terminology  : $("#client_terminology").val(),
                color               : $("#color").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_score_card_status_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    ScoreCardStatus.get()
                }
            }, 'json');
    }

}