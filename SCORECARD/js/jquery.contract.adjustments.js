// JavaScript Document
$.widget("contract.adjustments", {

    options	: 	{
        tableRef 		       : "contract_adjustments_"+(Math.floor(Math.random(56) * 34)),
        url				       : "../controllers/contractadjustmentcontroller.php?action=getContractAdjustments",
        start			       : 0,
        limit			       : 10,
        total			       : 0,
        contract_id	           : 0,
        financial_year	       : 0,
        owner_id               : 0,
        current			       : 1,
        isOwners		       : [],
        section                : "",
        viewAction             : "",
        autoLoad               : false
    } ,

    _init			: function()
    {
        if(this.options.autoLoad)
        {
            this._getContractBudgets();
        }
    } ,

    _create			: function()
    {
        var self = this;
        var html = [];
        var message = "";
        html.push("<table class='noborder' width='100%'>");
        if(self.options.section == 'admin')
        {
                html.push("<tr>");
                    html.push("<td class='noborder'>");
                        html.push("<table class='noborder'>");
                            html.push("<tr>");
                                html.push("<th style='text-align:left;'>Financial Year:</th>");
                                html.push("<td class='noborder'>");
                                html.push("<select id='financial_year' name='financial_year' style='width:200px;'>");
                                html.push("<option value=''>--please select--</option>");
                                html.push("</select>");
                                html.push("</td>");
                            html.push("</tr>");
                            html.push("<tr>");
                                html.push("<th style='text-align:left;'>Contract:</th>");
                                html.push("<td class='noborder'>");
                                html.push("<select id='contract_id' name='contract_id' style='width:200px;'>");
                                html.push("<option value=''>--please select--</option>");
                                html.push("</select>");
                                html.push("</td>");
                            html.push("</tr>");
                        html.push("</table>");
                    html.push("</td>");
                html.push("</tr>");
                message =  "<p class='ui-state-highlight-info contract_budgets'>Please select financial year and contract to view adjustments</p>";
        }
            html.push("<tr>");
                html.push("<td class='noborder'>");
                    html.push("<table id='"+self.options.tableRef+"' width='100%'>");
                     html.push(message);
                    html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
        html.push("</table>");
        $(self.element).append(html.join(' '));

        self._loadFinancialYears();

        $("#financial_year").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.financial_year = $(this).val();
                $(".contract_budgets").remove();
                self._loadContracts();
            } else {
                $(".contract_budgets").remove();
                self.options.financial_year  = 0;
            }
        });

        $("#contract_id").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.contract_id = $(this).val();
                self._getContractAdjustments();
            } else {
                self.options.contract_id = 0;
                self._getContractAdjustments();
            }
        });

    } ,

    _loadFinancialYears     : function()
    {
        var self = this;
        $("#financial_year").html("");
        $.getJSON("../controllers/financialyearcontroller.php?action=getAll",{ status : 1 }, function(financialyears){
            var finHtml = []
            finHtml.push("<option value=''>--please select--</option>");
            $.each(financialyears, function(index, year){
                finHtml.push("<option value='"+year.id+"'>"+year.value+"</option>");
            });
            $("#financial_year").html( finHtml.join(' ') );
        });
    } ,

    _loadContracts              : function()
    {
        var self = this;
        $("#contract_id").empty();
        $.getJSON("../controllers/contractcontroller.php?action=getAll",{
            financial_year : self.options.financial_year,
            page: 'budget'
        }, function(contracts){
            var contractHtml = [];
            contractHtml.push("<option value=''>--please select--</option>");
            $.each(contracts, function(contract_id, contract) {
                contractHtml.push("<option value='"+contract_id+"'>"+contract+"</option>");
            })
            $("#contract_id").html( contractHtml.join(' ') );
        });
    },

    _getContractAdjustments		: function()
    {
        var self = this;
        $("body").append($("<div />",{id:"actionLoadingDiv", html:"Loading contract budgets . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
        );

        $.getJSON( self.options.url, {
            start 	        : self.options.start,
            limit 	        : self.options.limit,
            id		        : self.options.action_id,
            contract_id     : self.options.contract_id,
            financial_year  : self.options.financial_year,
            section         : self.options.section,
            page            : 'budget'
        }, function(contractAdjustments){
            $("#actionLoadingDiv").remove();
            $(".contract_budgets").remove();
            self._addNewAdjustment(contractAdjustments.suppliers, contractAdjustments.total_suppliers);
            self._displayHeaders(contractAdjustments.suppliers);
            self._display(contractAdjustments.adjustments, contractAdjustments.supplier_budgets, contractAdjustments.supplier_total, contractAdjustments.budget_total);
        });
    } ,


    _addNewAdjustment     : function(contract_suppliers, total_suppliers)
    {
        var self =  this;
        var html = [];
        html.push("<tr class='contract_budgets'>");
            html.push("<td colspan='"+(7 + parseInt(total_suppliers) )+"'>");+
                html.push("<input type='button' name='add_new_adjustment' id='add_new_adjustment' value='Add New Adjustment' />");
                $("#add_new_adjustment").live("click", function(e) {
                    self._addContractAdjustment(contract_suppliers);
                    e.preventDefault();
                });
            html.push("</td>");
        html.push("</tr>");
        $("#"+self.options.tableRef).append(html.join(' ') );
    },

    _display    : function(contract_adjustments, supplier_budgets, supplier_totals, budget_total)
    {
        var self = this;
        var html = [];
        $.each(contract_adjustments, function(contract_adjustment_id, contract_adjustment){
            html.push("<tr class='contract_budgets'>");
            //html.push("<td>"+contract_adjustment.contract_adjustment_id+"</td>");
                html.push("<td>"+contract_adjustment_id+"</td>");
                html.push("<td>"+contract_adjustment.name+"</td>");
                html.push("<td>"+contract_adjustment.adjustment_date+"</td>");
                html.push("<td>"+contract_adjustment.reason_for_adjustment+"</td>");
                html.push("<td>"+contract_adjustment.attachments+"</td>");
                html.push("<td class='th2'  style='text-align:right;'>"+contract_adjustment.total+"</td>");
                if(supplier_budgets.hasOwnProperty(contract_adjustment_id))
                {
                    $.each(supplier_budgets[contract_adjustment_id]['supplier_budget'], function(index, supplier_budget){
                        html.push("<td style='text-align:right;'>"+supplier_budget+"</td>");
                    });
                }
                html.push("<td>");
                if((contract_adjustment.status & 5) == 5)
                {
                    html.push("<input type='button' name='edit_adjustment_"+contract_adjustment_id+"' id='edit_adjustment_"+contract_adjustment_id+"' value='Edit' />");
                    $("#edit_adjustment_"+contract_adjustment_id).live("click", function(e){
                        self._editAdjustment(contract_adjustment_id, contract_adjustment);
                        e.preventDefault();
                    });

                    html.push("<input type='button' name='delete_"+contract_adjustment_id+"' id='delete_"+contract_adjustment_id+"' value='Delete' />");
                    $("#delete_"+contract_adjustment_id).live("click", function(e){
                        if(confirm('Are you sure you want to delete this contract adjustment'))
                        {
                            self._deleteContractBudgetAdjustment(contract_adjustment_id, contract_adjustment);
                        }
                        e.preventDefault();
                    });
                }
                html.push("</td>");
            html.push("</tr>");
        });
        html.push("<tr class='contract_budgets'>");
            html.push("<th colspan='5' class='th2'></th>");
            html.push("<th style='text-align:right;' class='th2'><b>"+budget_total+"</b></th>");
            $.each(supplier_totals, function(supplier_id, budget){
                html.push("<th style='text-align:right;' class='th2'><b>"+budget+"</b></th>");
            });
            html.push("<th class='th2'>&nbsp;</th>");
        html.push("</tr>");
        $("#"+self.options.tableRef).append(html.join(' ') );

    } ,

    _displayHeaders		: function(suppliers)
    {
        var self = this;
        var html = [];
        html.push("<tr class='contract_budgets'>");
        html.push("<th>Ref</th>");
        html.push("<th>Budget Info</th>");
        html.push("<th>Adjusted Date</th>");
        html.push("<th>Reason for adjustment</th>");
        html.push("<th>POE Attached</th>");
        html.push("<th>Total Budget</th>");
        $.each(suppliers, function(supplier_id, supplier){
            html.push("<th>"+supplier+"</th>");
        });
        html.push("<th>&nbsp;</th>");
        html.push("</tr>");
        $("#"+self.options.tableRef).append(html.join(' ')).find("#remove").remove();;
    } ,

    _addContractAdjustment  : function(contract_suppliers)
    {
        var self = this;
        if($("#add_contract_adjustment").length > 0)
        {
            $("#add_contract_adjustment").remove();
        }
        var html = [];
        html.push("<form method='post' name='contract_adjustment' id='contract_adjustment' >");
        html.push("<table>");
            html.push("<tr>");
                    html.push("<th style='text-align: left;'>Budget Info:</th>");
                    html.push("<td>");
                        html.push("<input type='text' id='name' name='name' value='' />");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th style='text-align: left;'>Adjustment Date:</th>");
                    html.push("<td>");
                        html.push("<input type='text' id='adjustment_date' name='adjustment_date' value='' class='adjustment_date_picker' readonly='readonly' />");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th style='text-align: left;'>Reason for adjustment:</th>");
                    html.push("<td>");
                        html.push("<textarea id='reason_for_adjustment' name='reason_for_adjustment' cols='50' rows='8'></textarea>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th style='text-align: left;'>POE Attachment:</th>");
                    html.push("<td>");
                        html.push("<div id='file_upload'></div>");
                        html.push("<input type='file' id='contract_adjustment_attachment' name='contract_adjustment_attachment' />");
                    html.push("</td>");
                html.push("</tr>");
                    html.push("<tr>");
                        html.push("<th style='text-align: left;'>Supplier Budgets:</th>");
                        html.push("<td>");
                            html.push("<div id='supplier_budgets'></div>");
                        html.push("</td>");
                    html.push("</tr>");
                html.push("<tr>");
                html.push("<td colspan='2'>");
                    html.push("<p id='message'></p>")
                html.push("</td>");
            html.push("</tr>");
        html.push("<table>");

        $("<div />",{id:"add_contract_adjustment"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "New Contract Budget Adjustmnet ",
                width       : 'auto',
                buttons     : {
                    "Save"      : function()
                    {
                        $('#message').html("Saving budget  adjustment....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/contractadjustmentcontroller.php?action=save", {
                            contract_id      : self.options.contract_id,
                            contract_data    : $("#contract_adjustment").serialize()
                        }, function(response) {
                            if(response.error)
                            {
                                jsDisplayResult("error", "error", response.text);
                            } else {
                                jsDisplayResult("ok", "ok", response.text);
                                self._getContractAdjustments();
                                $("#add_contract_adjustment").dialog("destroy").remove();
                            }
                        },"json");
                    } ,

                    "Cancel"        : function()
                    {
                        $("#add_contract_adjustment").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#add_contract_adjustment").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns          = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});

                    $(".adjustment_date_picker").datepicker({
                        showOn		    : "both",
                        buttonImage 	: "/library/jquery/css/calendar.gif",
                        buttonImageOnly : true,
                        changeMonth	    : true,
                        changeYear	    : true,
                        dateFormat 	    : "dd-M-yy",
                        altField		: "#startDate",
                        altFormat		: 'd_m_yy'
                    });
;
                    $.get('../controllers/contractsuppliercontroller.php?action=getContractBudgetSupplierHtml',{
                        contract_id : self.options.contract_id
                    }, function(supplierBudgetHtml){
                        $("#supplier_budgets").html( supplierBudgetHtml );
                    }, 'html');

                    $("#contract_adjustment_attachment").live('change', function(e){
                        self._attachContractAdjustmentDocuments(this);
                        return false;
                    });

                }
            });
    },

    _editAdjustment      : function(contract_adjustment_id, contract_adjustment)
    {
        var self = this;
        if($("#edit_contract_adjustment").length > 0)
        {
            $("#edit_contract_adjustment").remove();
        }
        var html = [];
        html.push("<form method='post' name='contract_adjustment' id='contract_adjustment' >");
        html.push("<table>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Budget Info:</th>");
                html.push("<td>");
                    html.push("<input type='text' id='name' name='name' value='"+contract_adjustment.name+"' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Adjustment Date:</th>");
                html.push("<td>");
                    html.push("<input type='text' id='adjustment_date' name='adjustment_date' value='"+contract_adjustment.adjustment_date+"' class='adjustment_date_picker' readonly='readonly' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Reason for adjustment:</th>");
                html.push("<td>");
                    html.push("<textarea id='reason_for_adjustment' name='reason_for_adjustment' cols='50' rows='8'>"+contract_adjustment.reason_for_adjustment+"</textarea>");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>POE Attachment:</th>");
                html.push("<td>");
                    html.push( contract_adjustment.editable_attachments );
                    html.push("<input type='file' id='contract_adjustment_attachment' name='contract_adjustment_attachment' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th style='text-align: left;'>Supplier Budgets:</th>");
                html.push("<td>");
                    html.push("<div id='supplier_budgets'></div>");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td colspan='2'>");
                    html.push("<p id='message'></p>");
                html.push("</td>");
            html.push("</tr>");
        html.push("<table>");

        $("<div />",{id:"edit_contract_adjustment"}).append(html.join(' '))
            .dialog({
                autoOpen    : true,
                modal       : true,
                position    : "top",
                title       : "Update Contract Budget Adjustment #"+contract_adjustment_id,
                width       : 'auto',
                buttons     : {
                    "Save"      : function()
                    {
                        $('#message').html("Updating budget  adjustment....<img src='../images/loaderA32.gif' />");
                        $.post("../controllers/contractadjustmentcontroller.php?action=edit", {
                            contract_id      : self.options.contract_id,
                            id               : contract_adjustment_id,
                            contract_data    : $("#contract_adjustment").serialize()
                        }, function(response) {
                            if(response.error)
                            {
                                $("#message").html( response.text).addClass('ui-state-error').css({'padding': '5px'});
                            } else {
                                if(response.updated)
                                {
                                    jsDisplayResult("ok", "ok", response.text);
                                } else {
                                    jsDisplayResult("info", "info", response.text);
                                }
                                self._getContractAdjustments();
                                $("#edit_contract_adjustment").dialog("destroy").remove();
                            }
                        },"json");
                    } ,

                    "Cancel"        : function()
                    {
                        $("#edit_contract_adjustment").dialog("destroy").remove();
                    }
                },
                close       : function(event, ui)
                {
                    $("#add_contract_adjustment").dialog("destroy").remove();
                },
                open        : function(event, ui)
                {
                    var btns          = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});

                    $(".adjustment_date_picker").datepicker({
                        showOn		    : "both",
                        buttonImage 	: "/library/jquery/css/calendar.gif",
                        buttonImageOnly : true,
                        changeMonth	    : true,
                        changeYear	    : true,
                        dateFormat 	    : "dd-M-yy",
                        altField		: "#startDate",
                        altFormat		: 'd_m_yy'
                    });
                    ;
                    $.get('../controllers/contractsuppliercontroller.php?action=getEditContractBudgetSupplierHtml',{
                        contract_id            : self.options.contract_id,
                        contract_adjustment_id : contract_adjustment_id
                    }, function(supplierBudgetHtml){
                        $("#supplier_budgets").html( supplierBudgetHtml );
                    }, 'html');


                    $("#contract_adjustment_attachment").live('change', function(e){
                        self._attachContractAdjustmentDocuments(this);
                       return false;
                    });

                    $(".remove_attach").live('click', function(e){
                        var id          = this.id
                        var ext         = $(this).attr('title');
                        var type        = $(this).attr('ref');
                        var filename    = $(this).attr("alt");
                        var document_id = $(this).attr("file");
                        $.post('../controllers/contractadjustmentcontroller.php?action=deleteContractBudgetAdjustmentDocument',
                        {
                            attachment  : id,
                            ext         : ext,
                            id          : id,
                            document_id : document_id,
                            type        : type,
                            filename    : filename
                        }, function(response) {
                            if(response.error)
                            {
                                $("#result_message").css({padding:"5px", clear:"both"}).html( response.text )
                            } else {
                                $("#result_message").addClass('ui-state-ok').css({padding:"5px", clear:"both"}).html( response.text )
                                var deleted_doc = "<input type='hidden' name='deleted_attachment["+document_id+"]' id='deleted_attachment_"+document_id+"' value='"+document_id+"' />";
                                $("#result_message").append(deleted_doc);
                                $("#li_"+ext).fadeOut();
                            }
                        },'json');
                        e.preventDefault();
                    });
                }
            });
    },

    _deleteContractBudgetAdjustment  : function(contract_adjustment_id)
    {
        var self = this;
        $.getJSON('../controllers/contractadjustmentcontroller.php?action=deleteContractAdjustment', {
            id : contract_adjustment_id
        }, function(response){
            if(response.error)
            {
                jsDisplayResult("error", "error", response.text);
            } else {
                jsDisplayResult("ok", "ok", response.text);
                self._getContractAdjustments();
            }
        });
    },

    _displayPaging		: function(total, cols)
    {
        var self  = this;
        var html  = [];
        var pages = 0;
        if(total%self.options.limit > 0)
        {
            pages = Math.ceil( total/self.options.limit )
        } else {
            pages = Math.floor( total/self.options.limit )
        }
        html.push("<tr>");
        html.push("<td colspan='"+cols+"'>");
        html.push("<input type='button' id='action_first_"+self.option.action_id+"' value=' |< ' />");
        html.push("<input type='button' id='action_previous_"+self.option.action_id+"' value=' < ' />");
        html.push("<span>&nbsp;&nbsp;&nbsp;");
        html.push("Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ?  1 :pages));
        html.push("&nbsp;&nbsp;&nbsp;</span>");
        html.push("<input type='button' id='action_next_"+self.option.action_id+"' value=' > ' />");
        html.push("<input type='button' id='action_last_"+self.option.action_id+"' value=' >| ' />");
        html.push("</td>");
        if(self.options.editAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.updateAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.assuranceAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAssurance)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        html.push("</tr>");
        $("#"+self.options.tableId+"_"+self.options.action_id).append(html.join(' '));
        if(self.options.current < 2)
        {
            $("#action_first_"+self.option.action_id).attr('disabled', 'disabled');
            $("#action_previous_"+self.option.action_id).attr('disabled', 'disabled');
        }
        if((self.options.current == pages || pages == 0))
        {
            $("#action_next_"+self.option.action_id).attr('disabled', 'disabled');
            $("#action_last_"+self.option.action_id).attr('disabled', 'disabled');
        }
        $("#action_next_"+self.option.action_id).bind("click", function(){
            self._getNext( self );
        });
        $("#action_last_"+self.option.action_id).bind("click",  function(){
            self._getLast( self );
        });
        $("#action_previous_"+self.option.action_id).bind("click",  function(){
            self._getPrevious( self );
        });
        $("#action_first_"+self.option.action_id).bind("click",  function(){
            self._getFirst( self );
        });
    } ,

    _getNext  			: function( $this ) {
        $this.options.current   = $this.options.current+1;
        $this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
        this._getActions();
    },

    _getLast  			: function( $this ) {
        $this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
        $this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );
        this._getActions();
    },

    _getPrevious    	: function( $this ) {
        $this.options.current   = parseFloat( $this.options.current ) - 1;
        $this.options.start 	= ($this.options.current-1)*$this.options.limit;
        this._getActions();
    },

    _getFirst  			: function( $this ) {
        $this.options.current   = 1;
        $this.options.start 	= 0;
        this._getActions();
    },

    _attachContractAdjustmentDocuments : function(element_id)
    {
        $("#file_upload").html("uploading  ... <img  src='../images/loaderA32.gif' />");
        $.ajaxFileUpload({
            url			  : '../controllers/contractadjustmentcontroller.php?action=saveAttachment',
            secureuri	  : false,
            fileElementId : 'contract_adjustment_attachment',
            dataType	  : 'json',
            success       : function (response, status)
            {
                $("#file_upload").html("");
                if(response.error )
                {
                    $("#file_upload").html(response.text )
                } else {
                    $("#file_upload").html("");
                    if(response.error)
                    {
                        $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                    } else {
                        $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')).append($("<div />",{id:"files_uploaded"}))
                        if(!$.isEmptyObject(response.files))
                        {
                            var list = [];
                            list.push("<span>Files uploaded ..</span><br />");
                            $.each(response.files, function(ref, file) {
                                list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                                  list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                  list.push("<span style='margin-left:5px;'><a href='../controller/contractadjustmentcontroller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                  list.push("<input type='hidden' name='contract_adjustment_documents["+file.key+"]' value='"+file.name+"' id='contract_document_"+ref+"' />");
                                list.push("</p>");
                            });
                            $("#files_uploaded").html(list.join(' '));

                            $(".delete").click(function(e){
                                var id = this.id
                                var ext = $(this).attr('title');
                                $.post('../controllers/contractadjustmentcontroller.php?action=removeContractAdjustmentAttachment',
                                    {
                                        attachment : id,
                                        ext        : ext
                                    }, function(response){
                                        if(response.error)
                                        {
                                            $("#result_message").html(response.text)
                                        } else {
                                            $("#result_message").addClass('ui-state-ok').html(response.text)
                                            $("#li_"+id).fadeOut();
                                        }
                                    },'json');
                                e.preventDefault();
                            });
                            $("#action_attachment").val("");
                        }
                    }

                    if( $("#delmessage").length != 0)
                    {
                        $("#delmessage").remove()
                    }
                }
            },
            error: function (data, status, e)
            {
                $("#file_upload").html( "Ajax error -- "+e+", please try uploading again");
            }
        });
    }


});
