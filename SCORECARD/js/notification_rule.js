$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    NotificationRule.get();
    $("#add_notification_rule").click(function(e){
        NotificationRule.addNotificationRule();
        e.preventDefault();
    });
	
});

var NotificationRule 	= {

		get				: function()
		{
            $(".notification_rules").remove();
			$.getJSON( "../controllers/notificationrulecontroller.php?action=getAll", function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#notification_rule_table").append($("<tr />").addClass('notification_rules')
                        .append($("<td />", {colspan: 6, html: "There are no assessment frequencies" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        NotificationRule.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#notification_rule_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('notification_rules')
				.append($("<td />",{html:val.id}))
				.append($("<td />",{html:val.short_code}))
				.append($("<td />",{html:val.name}))
				.append($("<td />",{html:val.description}))
                .append($("<td />",{html:val.date}))
                .append($("<td />",{html:"<b>"+(val.status == 1 ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"notification_rule_edit_"+val.id, name:"notification_rule_edit_"+val.id }))
				 )
			  )

            $("#notification_rule_edit_"+val.id).live("click", function(e){
                self._updateNotificationRule(val);
                e.preventDefault();
            });

		},

    addNotificationRule          : function()
    {
        var self = this;
        if($("#add_notification_rule_dialog").length > 0)
        {
            $("#add_notification_rule_dialog").remove();
        }

        $("<div />",{id:"add_notification_rule_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"short_code", id:"short_code", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Name :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:""}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Date:"}))
                    .append($("<td />")
                        .append($("<input />",{type: "text", name:"date", id:"date"}).addClass('datepicker'))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Notification Frequency",
                width          : 500,
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the short code");
                            return false;
                        } else if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the name");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_notification_rule_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_notification_rule_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});

                    $(".datepicker").datepicker({
                        showOn		  : "both",
                        buttonImage 	  : "/library/jquery/css/calendar.gif",
                        buttonImageOnly  : true,
                        changeMonth	  : true,
                        changeYear	  : true,
                        dateFormat 	  : "dd-M-yy",
                        altField		  : "#startDate",
                        altFormat		  : 'd_m_yy'
                    });
                }
            })
    },

    _updateNotificationRule         : function(notification_rule)
    {
        var self = this;
        if($("#edit_notification_rule_dialog").length > 0)
        {
            $("#edit_notification_rule_dialog").remove();
        }

        $("<div />",{id:"edit_notification_rule_dialog"}).append($("<table />",{width:"100%"})
               .append($("<tr />")
                    .append($("<th />",{html:"Short Code:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"short_code", id:"short_code", value:notification_rule.short_code}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Name :"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:notification_rule.name}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Description :"}))
                    .append($("<td />")
                        .append($("<textarea />",{name:"description", id:"description", cols:50, rows:8, text:notification_rule.description}))
                    )
                )
                .append($("<tr />")
                    .append($("<th />",{html:"Date:"}))
                    .append($("<td />")
                        .append($("<input />",{type: "text", name:"date", id:"date", value: notification_rule.asses_date}).addClass('datepicker'))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Notification Frequency",
                width          : 500,
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#short_code").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the short code");
                            return false;
                        } else if($("#name").val() == "") {
                            $("#message").addClass("ui-state-error").html("please enter the name");
                            return false;
                        } else {
                            self.update(notification_rule.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_notification_rule_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(notification_rule.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(notification_rule.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(notification_rule.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_notification_rule_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((notification_rule.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});

                    $(".datepicker").datepicker({
                        showOn		  : "both",
                        buttonImage 	  : "/library/jquery/css/calendar.gif",
                        buttonImageOnly  : true,
                        changeMonth	  : true,
                        changeYear	  : true,
                        dateFormat 	  : "dd-M-yy",
                        altField		  : "#startDate",
                        altFormat		  : 'd_m_yy'
                    });
                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/notificationrulecontroller.php?action=update",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_notification_rule_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/notificationrulecontroller.php?action=update",
        {
            id          : id,
            short_code  : $("#short_code").val(),
            name        : $("#name").val(),
            description : $("#description").val(),
            date        : $("#date").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_notification_rule_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/notificationrulecontroller.php?action=save",
            {
                short_code : $("#short_code").val(),
                name:        $("#name").val(),
                description: $("#description").val(),
                date        : $("#date").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_notification_rule_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    NotificationRule.get()
                }
            }, 'json');
    }

}