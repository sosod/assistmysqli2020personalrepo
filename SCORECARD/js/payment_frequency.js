$(function(){
	
	//$(".textcounter").textcounter({maxLength:100});

    PaymentFrequency.get();
    $("#add_payment_frequency").click(function(e){
        PaymentFrequency.addPaymentFrequency();
        e.preventDefault();
    });
	
});

var PaymentFrequency 	= {

		get				: function()
		{
            $(".payment_frequencies").remove();
			$.getJSON( "../controllers/paymentfrequencycontroller.php?action=getAll", function( data ) {
                if($.isEmptyObject(data))
                {
                    $("#payment_frequency_table").append($("<tr />").addClass('payment_frequencies')
                        .append($("<td />", {colspan: 6, html: "There are no payment frequencies" }))
                    )
                } else {
                    $.each( data, function( index, val){
                        PaymentFrequency.display( val );
                    });
                }
			})
		} ,

		display			: function( val )
		{
            var self = this;
			$("#payment_frequency_table")
			  .append($("<tr />",{id:"tr_"+val.id}).addClass('payment_frequencies')
                .append($("<td />",{html:val.id}))
                .append($("<td />",{html:val.name}))
                .append($("<td />",{html:"<b>"+((val.status & 1) == 1  ? "Active"  : "Inactive" )+"</b>"}))
				.append($("<td />")
				  .append($("<input />",{type:"submit", value:"Edit", id:"payment_edit_"+val.id, name:"payment_edit_"+val.id }))
				 )
			  )

            $("#payment_edit_"+val.id).live("click", function(e){
                self._updatePaymentFrequency(val);
                e.preventDefault();
            });
			
		},

    addPaymentFrequency          : function()
    {
        var self = this;
        if($("#add_payment_frequency_dialog").length > 0)
        {
            $("#add_payment_frequency_dialog").remove();
        }

        $("<div />",{id:"add_payment_frequency_dialog"}).append($("<table />",{width:"100%"})
                .append($("<tr />")
                    .append($("<th />",{html:"Payment Frequency Name:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:''}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Payment Frequency",
                width          : 500,
                buttons        : {
                    "Save"       : function()
                    {

                        if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the payment frequency name");
                            return false;
                        } else {
                            self._save();
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#add_payment_frequency_dialog").dialog("destroy").remove();
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#add_payment_frequency_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];

                    $(saveBtn).css({"color":"#090"});

                }
            })



    },

    _updatePaymentFrequency         : function(payment_frequency)
    {
        var self = this;
        if($("#edit_payment_frequency_dialog").length > 0)
        {
            $("#edit_payment_frequency_dialog").remove();
        }

        $("<div />",{id:"edit_payment_frequency_dialog"}).append($("<table />",{width:"100%"})
               .append($("<tr />")
                    .append($("<th />",{html:"Payment Frequency Name:"}))
                    .append($("<td />")
                        .append($("<input />",{type:"text", name:"name", id:"name", value:payment_frequency.name}))
                    )
                )
                .append($("<tr />")
                    .append($("<td />",{colspan:"2"})
                        .append($("<span />",{id:"message"}).css({padding:"5px"}))
                    )
                )
            ).dialog({
                autoOpen       : true,
                modal          : true,
                position       : "top",
                title          : "Payment Frequency",
                width          : 500,
                buttons        : {
                    "Save Changes"       : function()
                    {
                        if($("#name").val() == "")
                        {
                            $("#message").addClass("ui-state-error").html("please enter the payment frequency name");
                            return false;
                        } else {
                            self.update(payment_frequency.id);
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#edit_payment_frequency_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(payment_frequency.id, 1);
                    } ,

                    "De-Activate"        : function()
                    {
                        self._updateStatus(payment_frequency.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(payment_frequency.id, 2);
                    }
                } ,
                close          : function(event, ui)
                {
                    $("#edit_payment_frequency_dialog").dialog("destroy").remove();
                } ,
                open           : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((payment_frequency.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }
                    
                    if((payment_frequency.status & 4) == 4)
                    {
                        $(deleteBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});

                }
            })


    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $.post( "../controllers/paymentfrequencycontroller.php?action=update",
        {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_payment_frequency_dialog").dialog("destroy").remove();
            }
        },"json");
    } ,

    update              : function(id)
    {
        var self = this;
        $.post( "../controllers/paymentfrequencycontroller.php?action=update",
        {
            id                  : id,
            name                : $("#name").val(),
            client_terminology  : $("#client_terminology").val(),
            color               : $("#color").val()
        }, function(response) {
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self.get();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#edit_payment_frequency_dialog").dialog("destroy").remove();
            }
        },"json");

    } ,
    _save			: function()
    {
        $("#message").html("Saving . . .  <img src='../images/loaderA32.gif' />");
        $.post( "../controllers/paymentfrequencycontroller.php?action=save",
            {
                name                : $("#name").val(),
                client_terminology  : $("#client_terminology").val(),
                color               : $("#color").val()
            }, function(response) {
                if(response.error)
                {
                    $("#message").html(response.text).addClass('ui-state-error').addClass('ui-state');
                } else {
                    $("#add_payment_frequency_dialog").remove();
                    jsDisplayResult("ok", "ok", response.text);
                    PaymentFrequency.get()
                }
            }, 'json');
    }

}