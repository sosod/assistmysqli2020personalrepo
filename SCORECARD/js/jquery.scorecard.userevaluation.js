// JavaScript Document
$.widget("scorecard.userevaluation", {

    options	: 	{
        tableRef 		       : "scorecard_userevaluation_"+(Math.floor(Math.random(56) * 34)),
        url				       : "../controllers/userevaluationcontroller.php?action=getUserEvaluations",
        start			       : 0,
        limit			       : 10,
        total			       : 0,
        owner_id	           : 0,
        score_card_id	       : 0,
        current			       : 1,
        isOwners		       : [],
        section                : "",
        autoLoad               : false
    } ,

    _init			: function()
    {

    } ,

    _create			: function()
    {
        var self = this;
        var html = [];

        html.push("<table class='noborder' width='100%'>");
            html.push("<tr>");
                html.push("<td class='noborder'>");
                html.push("<table class='noborder'>");
                    html.push("<tr>");
                        html.push("<th style='text-align:left;'>Employee:</th>");
                        html.push("<td class='noborder'>");
                            html.push("<select id='owner_id' name='owner_id' style='width:200px;'>");
                            html.push("<option value=''>--please select--</option>");
                            html.push("</select>");
                        html.push("</td>");
                    html.push("</tr>");
                html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<td class='noborder'>");
                html.push("<table id='"+self.options.tableRef+"'>");
                    html.push("<p class='ui-state-highlight-info user_evaluation'>Please select scorecard and employee to view evaluation periods</p>");
                html.push("</table>");
                html.push("</td>");
            html.push("</tr>");
        html.push("</table>");
        $(self.element).append(html.join(' '));
        self._loadUsers();

        $("#owner_id").live("change", function(){
            if($(this).val() !== "")
            {
                self.options.owner_id = $(this).val();
                self._getEvaluationPeriods();
            } else {
                $(".user_evaluation").remove();
                self.options.owner_id = 0;
            }
        });

    } ,

    _loadUsers           : function()
    {
        var self = this;
        $("#owner_id").html("");
        $.getJSON("../controllers/usercontroller.php?action=getAll", function( users ){
            var userHtml = []
            userHtml.push("<option value=''>--please select--</option>");
            $.each(users, function(user_id, user){
                userHtml.push("<option value='"+user_id+"'>"+user+"</option>");
            });
            $("#owner_id").html( userHtml.join(' ') );
        });
    } ,

    _getEvaluationPeriods		: function()
    {
        var self = this;
        $("body").append($("<div />",{id:"actionLoadingDiv", html:"Loading user evaluation . . . <img src='../images/loaderA32.gif' />"})
            .css({position:"absolute", "z-index":"9999", top:"400px", left:"200px", border:"0px solid #FFFFF", padding:"5px"})
        );

        $.getJSON( self.options.url, {
            start 	        : self.options.start,
            limit 	        : self.options.limit,
            user_id         : self.options.owner_id,
            financial_year  : self.options.financial_year,
            section         : self.options.section,
            page            : self.options.page
        }, function(userEvaluationPeriods){
            $("#actionLoadingDiv").remove();
            $(".user_evaluation").remove();
            self._addEvaluationPeriod();
            self._displayHeaders();
            if($.isEmptyObject(userEvaluationPeriods))
            {
                var _html = [];
                _html.push("<tr class='user_evaluation'>")
                    _html.push("<td colspan='9'>");
                        _html.push("<p class='ui-state ui-state-highlight' style='padding:10px; border:0; margin-left:30px; text-align:center;' >");
                            _html.push("<span class='ui-icon ui-icon-info' style='float:left;'></span>");
                                _html.push("There are no evaluation periods found");
                        _html.push("</p>");
                    _html.push("</td>");
                _html.push("</tr>");
                $("#"+self.options.tableRef).append( _html.join(' ') );
            } else {
                self._display(userEvaluationPeriods);
            }
        });
    } ,

    _addEvaluationPeriod    : function(score_cards)
    {
        var self = this;
        var html = [];
        html.push("<tr class='user_evaluation'>")
            html.push("<td colspan='6'>");
                html.push("<input type='button' name='add_new_evaluation_period' id='add_new_evaluation_period' value='Add New Evaluation Period' />");
                $("#add_new_evaluation_period").live("click", function(e){
                    self._addUserScoreCardEvaluationPeriod(score_cards);
                    e.preventDefault();
                });
            html.push("</td>");
        html.push("</tr>");
        $("#"+self.options.tableRef).append( html.join(' ') );
    },

    _display    : function(userEvaluationPeriods)
    {
        var self = this;
        var html = [];
        $.each(userEvaluationPeriods, function(evaluation_period_id, evaluation_period){
            html.push("<tr class='user_evaluation'>");
            html.push("<td>"+evaluation_period_id+"</td>");
            html.push("<td>"+evaluation_period.score_card+"</td>");
            html.push("<td>"+evaluation_period.evaluation_open_from+"</td>");
            html.push("<td>"+evaluation_period.evaluation_open_to+"</td>");
            html.push("<td>"+( (evaluation_period.status & 1) == 1 ? 'Active' : 'Inactive')+"</td>");
            html.push("<td>");
                html.push("<input type='button' name='edit_evaluation_period_"+evaluation_period_id+"' id='edit_evaluation_period_"+evaluation_period_id+"' value='Edit' />");
                $("#edit_evaluation_period_"+evaluation_period_id).live("click", function(e){
                    self._editEvaluationPeriod(evaluation_period_id, evaluation_period);
                    e.preventDefault();
                });
            html.push("</td>");
            html.push("</tr>");
        });
        $("#"+self.options.tableRef).append(html.join(' ') );

    } ,

    _displayHeaders		: function(suppliers)
    {
        var self = this;
        var html = [];
        html.push("<tr class='user_evaluation'>");
            html.push("<th>Ref</th>");
            html.push("<th>Scorecard</th>");
            html.push("<th>Open From</th>");
            html.push("<th>Open To</th>");
            html.push("<th>Status</th>");
            html.push("<th>&nbsp;</th>");
        html.push("</tr>");
        $("#"+self.options.tableRef).append(html.join(' ')).find("#remove").remove();;
    } ,

    _displayPaging		: function(total, cols)
    {
        var self  = this;
        var html  = [];
        var pages = 0;
        if(total%self.options.limit > 0)
        {
            pages = Math.ceil( total/self.options.limit )
        } else {
            pages = Math.floor( total/self.options.limit )
        }
        html.push("<tr>");
        html.push("<td colspan='"+cols+"'>");
            html.push("<input type='button' id='action_first_"+self.option.action_id+"' value=' |< ' />");
            html.push("<input type='button' id='action_previous_"+self.option.action_id+"' value=' < ' />");
                html.push("<span>&nbsp;&nbsp;&nbsp;");
                    html.push("Page "+self.options.current+"/"+(pages == 0 || isNaN(pages) ?  1 :pages));
                html.push("&nbsp;&nbsp;&nbsp;</span>");
            html.push("<input type='button' id='action_next_"+self.option.action_id+"' value=' > ' />");
            html.push("<input type='button' id='action_last_"+self.option.action_id+"' value=' >| ' />");
        html.push("</td>");
        if(self.options.editAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.updateAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.assuranceAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAssurance)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        if(self.options.viewAction)
        {
            html.push("<td>&nbsp;&nbsp;</td>");
        }
        html.push("</tr>");
        $("#"+self.options.tableId+"_"+self.options.action_id).append(html.join(' '));
        if(self.options.current < 2)
        {
            $("#action_first_"+self.option.action_id).attr('disabled', 'disabled');
            $("#action_previous_"+self.option.action_id).attr('disabled', 'disabled');
        }
        if((self.options.current == pages || pages == 0))
        {
            $("#action_next_"+self.option.action_id).attr('disabled', 'disabled');
            $("#action_last_"+self.option.action_id).attr('disabled', 'disabled');
        }
        $("#action_next_"+self.option.action_id).bind("click", function(){
            self._getNext( self );
        });
        $("#action_last_"+self.option.action_id).bind("click",  function(){
            self._getLast( self );
        });
        $("#action_previous_"+self.option.action_id).bind("click",  function(){
            self._getPrevious( self );
        });
        $("#action_first_"+self.option.action_id).bind("click",  function(){
            self._getFirst( self );
        });
    } ,

    _getNext  			: function( $this ) {
        $this.options.current   = $this.options.current+1;
        $this.options.start 	= parseFloat( $this.options.current - 1) * parseFloat( $this.options.limit );
        this._getActions();
    },

    _getLast  			: function( $this ) {
        $this.options.current   =  Math.ceil( parseFloat( $this.options.total )/ parseFloat( $this.options.limit ));
        $this.options.start 	= parseFloat($this.options.current-1) * parseFloat( $this.options.limit );
        this._getActions();
    },

    _getPrevious    	: function( $this ) {
        $this.options.current   = parseFloat( $this.options.current ) - 1;
        $this.options.start 	= ($this.options.current-1)*$this.options.limit;
        this._getActions();
    },

    _getFirst  			: function( $this ) {
        $this.options.current   = 1;
        $this.options.start 	= 0;
        this._getActions();
    } ,

    _addUserScoreCardEvaluationPeriod       : function(score_cards)
    {
        var self = this;
        var html = [];
        html.push("<form name=''>");
          html.push("<table>");
            html.push("<tr>");
              html.push("<th>Scorecard</th>");
              html.push("<td>");
                html.push("<select name='score_card_id' id='score_card_id'></select>");
              html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th>Open From</th>");
                html.push("<td>");
                html.push("<input type='text' name='open_from' id='open_from' class='period_date_picker' readonly='readonly' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th>Open To</th>");
                html.push("<td>");
                html.push("<input type='text' name='open_to' id='open_to' class='period_date_picker' readonly='readonly' />");
                html.push("</td>");
            html.push("</tr>");
            html.push("<tr>");
              html.push("<td colspan='2'><p id='message'></p></td>");
            html.push("</tr>");
          html.push("</table>");
        html.push("</form>");

        if($("#user_evaluation_period_dialog").length > 0)
        {
            $("#user_evaluation_period_dialog").remove();
        }
        $("<div />", {id : "user_evaluation_period_dialog", html : html.join(' ') })
          .dialog({
                autoOpen : true,
                modal    : true,
                title    : 'User Evaluation Period',
                position : 'top',
                width    : 'auto',
                buttons  : {
                            "Save"  : function()
                            {
                                if($("#score_card_id").val() == '')
                                {
                                    $('#message').html('Please select the score card').addClass('ui-state-error');
                                    return false;
                                } else if($("#open_from").val() == ''){
                                    $('#message').html('Please enter the open from').addClass('ui-state-error');
                                    return false;
                                } else if($("#open_to").val() == ''){
                                    $('#message').html('Please enter the open to').addClass('ui-state-error');
                                    return false;
                                } else {
                                    $('#message').html("saving user evaluation . . .<img src='../images/loaderA32.gif' />");
                                    $.post("../controllers/userevaluationcontroller.php?action=save", {
                                        score_card_id    : $("#score_card_id").val(),
                                        user_id          : self.options.owner_id,
                                        open_from        : $("#open_from").val(),
                                        open_to          : $("#open_to").val()
                                    }, function(response) {
                                        if(response.error)
                                        {
                                            $("#message").html( response.text).addClass('ui-state-error').css({'padding': '5px'});
                                        } else {
                                            jsDisplayResult("ok", "ok", response.text);
                                            self._getEvaluationPeriods();
                                            $("#user_evaluation_period_dialog").dialog("destroy").remove();
                                        }
                                    },"json");
                                }
                            } ,

                            "Cancel"  : function(){

                            }
                } ,
                open      : function(event, ui)
                {
                    var btns          = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    $(saveBtn).css({"color":"#090"});
                    $(cancelBtn).css({"color":"red"});

                    $(".period_date_picker").datepicker({
                        showOn		    : "both",
                        buttonImage 	: "/library/jquery/css/calendar.gif",
                        buttonImageOnly : true,
                        changeMonth	    : true,
                        changeYear	    : true,
                        dateFormat 	    : "dd-M-yy",
                        altField		: "#startDate",
                        altFormat		: 'd_m_yy'
                    });

                    $("#score_card_id").html("<option value=''>Loading . . .</option>");;
                    $.getJSON("../controllers/scorecardcontroller.php?action=getSetupList", {
                        section       : 'admin',
                        page          : self.options.page
                    }, function(scorecards){
                        var scoreCardHtml = []
                        scoreCardHtml.push("<option value=''>--please select--</option>");
                        $.each(scorecards, function(score_card_id, score_card){
                            scoreCardHtml.push("<option value='"+score_card_id+"'>"+score_card+"</option>");
                        });
                        $("#score_card_id").html( scoreCardHtml.join(' ') );
                    });
                } ,

                close     : function(event, ui)
                {

                }
            })
    } ,


    _editEvaluationPeriod       : function(evaluation_period_id, evaluation_period)
    {
        var self = this;
        var html = [];
        html.push("<form name=''>");
            html.push("<table>");
                html.push("<tr>");
                    html.push("<th>Scorecard</th>");
                    html.push("<td>");
                        html.push("<select name='score_card_id' id='score_card_id'></select>");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Open From</th>");
                    html.push("<td>");
                        html.push("<input type='text' name='open_from' id='open_from' class='period_date_picker' readonly='readonly' value='"+evaluation_period.evaluation_open_from+"' />");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<th>Open To</th>");
                    html.push("<td>");
                        html.push("<input type='text' name='open_to' id='open_to' class='period_date_picker' readonly='readonly'  value='"+evaluation_period.evaluation_open_to+"' />");
                    html.push("</td>");
                html.push("</tr>");
                html.push("<tr>");
                    html.push("<td colspan='2'><p id='message'></p></td>");
                html.push("</tr>");
            html.push("</table>");
        html.push("</form>");

        if($("#user_evaluation_period_dialog").length > 0)
        {
            $("#user_evaluation_period_dialog").remove();
        }
        $("<div />", {id : "user_evaluation_period_dialog", html : html.join(' ') })
            .dialog({
                autoOpen : true,
                modal    : true,
                title    : 'Update User Evaluation Period',
                position : 'top',
                width    : 'auto',
                buttons  : {
                    "Update"  : function()
                    {
                        if($("#score_card_id").val() == '')
                        {
                            $('#message').html('Please select the score card').addClass('ui-state-error');
                            return false;
                        } else if($("#open_from").val() == ''){
                            $('#message').html('Please enter the open from').addClass('ui-state-error');
                            return false;
                        } else if($("#open_to").val() == ''){
                            $('#message').html('Please enter the open to').addClass('ui-state-error');
                            return false;
                        } else {
                            $('#message').html("updating user evaluation . . .<img src='../images/loaderA32.gif' />");
                            $.post("../controllers/userevaluationcontroller.php?action=update", {
                                score_card_id    : $("#score_card_id").val(),
                                user_id          : self.options.owner_id,
                                open_from        : $("#open_from").val(),
                                open_to          : $("#open_to").val(),
                                id               : evaluation_period_id
                            }, function(response) {
                                if(response.error)
                                {
                                    $("#message").html( response.text).addClass('ui-state-error').css({'padding': '5px'});
                                } else {
                                    jsDisplayResult("ok", "ok", response.text);
                                    self._getEvaluationPeriods();
                                    $("#user_evaluation_period_dialog").dialog("destroy").remove();
                                }
                            },"json");
                        }
                    } ,

                    "Cancel"             : function()
                    {
                        $("#user_evaluation_period_dialog").dialog("destroy").remove();
                    } ,

                    "Activate"           : function()
                    {
                        self._updateStatus(evaluation_period.id, 1);
                    } ,

                    "Deactivate"        : function()
                    {
                        self._updateStatus(evaluation_period.id, 0);
                    } ,

                    "Delete"             : function()
                    {
                        self._updateStatus(evaluation_period.id, 2);
                    }
                } ,
                open      : function(event, ui)
                {
                    var btns = $(event.target).parents(".ui-dialog.ui-widget").find(".ui-dialog-buttonpane").find("button");
                    var saveBtn       = btns[0];
                    var cancelBtn     = btns[1];
                    var activateBtn   = btns[2];
                    var deactivateBtn = btns[3];
                    var deleteBtn     = btns[4];

                    if((evaluation_period.status & 1) == 0)
                    {
                        $(deactivateBtn).css({"display":"none"});
                    } else {
                        $(activateBtn).css({"display":"none"});
                    }

                    if((evaluation_period.status & 4) == 4)
                    {
                        $(deleteBtn).css({"display":"none"});
                        $(deactivateBtn).css({"display":"none"});
                    }

                    $(saveBtn).css({"color":"#090"});
                    $(deactivateBtn).css({"color":"red"});
                    $(activateBtn).css({"color":"#090"});
                    $(deleteBtn).css({"color":"red"});


                    $(".period_date_picker").datepicker({
                        showOn		    : "both",
                        buttonImage 	: "/library/jquery/css/calendar.gif",
                        buttonImageOnly : true,
                        changeMonth	    : true,
                        changeYear	    : true,
                        dateFormat 	    : "dd-M-yy",
                        altField		: "#startDate",
                        altFormat		: 'd_m_yy'
                    });

                    $("#score_card_id").html("<option value=''>Loading . . .</option>");;
                    $.getJSON("../controllers/scorecardcontroller.php?action=getSetupList", {
                        section       : 'admin',
                        page          : self.options.page
                    }, function(scorecards){
                        var scoreCardHtml = []
                        scoreCardHtml.push("<option value=''>--please select--</option>");
                        $.each(scorecards, function(score_card_id, score_card) {
                            if(evaluation_period.score_card_id == score_card_id)
                            {
                               scoreCardHtml.push("<option value='"+score_card_id+"' selected='selected'>"+score_card+"</option>");
                            } else {
                               scoreCardHtml.push("<option value='"+score_card_id+"'>"+score_card+"</option>");
                            }
                        });
                        $("#score_card_id").html( scoreCardHtml.join(' ') );
                    });
                } ,

                close     : function(event, ui)
                {

                }
            })
    } ,

    _updateStatus        : function(id, status)
    {
        var self = this;
        $('#message').html("updating user evaluation . . .<img src='../images/loaderA32.gif' />");
        $.post( "../controllers/userevaluationcontroller.php?action=update", {
            id      : id,
            status  : status
        }, function(response){
            if(response.error)
            {
                $("#message").addClass("ui-state-error").html( response.text )
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text );
                    self._getEvaluationPeriods();
                } else {
                    jsDisplayResult("info", "info", response.text );
                }
                $("#user_evaluation_period_dialog").dialog("destroy").remove();
            }
        },"json");
    }

});
