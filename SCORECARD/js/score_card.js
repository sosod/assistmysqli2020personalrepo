/**
 * Created with JetBrains PhpStorm.
 * User: admire
 * Date: 2013/09/24
 * Time: 6:15 AM
 * To change this template use File | Settings | File Templates.
 */
$(function(){

    $(".completion-rule").hide();
    $(".allocate-user-rule").hide();

    $("#save").click(function(e){

        if($("#financial_year_id").val() == '')
        {
            $("#financial_year_id").focus().select()
            jsDisplayResult("error", "error", 'Financial year is required');
            return false;
        } else if($("#name").val() == ''){
            $("#name").focus().select()
            jsDisplayResult("error", "error", 'Scorecard name is required');
            return false;
        } else if($("#type_id").val() == ''){
            $("#type_id").focus().select()
            jsDisplayResult("error", "error", 'Scorecard type is required');
            return false;
        } else if($("#category_id").val() == ''){
            $("#category_id").focus().select()
            jsDisplayResult("error", "error", 'Scorecard category is required');
            return false;
        } else if($("#manager_id").val() == ''){
            $("#manager_id").focus().select()
            jsDisplayResult("error", "error", 'Scorecard manager is required');
            return false;
        } else if($("#owner_id").val() == '') {
            $("#owner_id").focus().select()
            jsDisplayResult("error", "error", 'Scorecard owner  is required');
            return false;
        } else {
            jsDisplayResult("info", "info", "Saving scorecard . . .  <img src='../images/loaderA32.gif' />");
            $.getJSON("../controllers/scorecardcontroller.php?action=save",{
                score_card : $("#score-card-form").serialize()
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    jsDisplayResult("ok", "ok", response.text);
                    $("input[type='text']").each(function(){
                        $(this).val('');
                    });
                    $('select').each(function(){
                        $(this).val('');
                    });
                    $('textarea').each(function(){
                        $(this).val('');
                    });
                    $("#file_upload").html("");
                }
            });
        }

        e.preventDefault();
    })

    $("#edit_score_card").live("click", function(e){

        if($("#financial_year_id").val() == '')
        {
            $("#financial_year_id").focus().select()
            jsDisplayResult("error", "error", 'Financial year is required');
            return false;
        } else if($("#name").val() == ''){
            $("#name").focus().select()
            jsDisplayResult("error", "error", 'Scorecard name is required');
            return false;
        } else if($("#type_id").val() == ''){
            $("#type_id").focus().select()
            jsDisplayResult("error", "error", 'Scorecard type is required');
            return false;
        } else if($("#category_id").val() == ''){
            $("#category_id").focus().select()
            jsDisplayResult("error", "error", 'Scorecard category is required');
            return false;
        }  else if($("#manager_id").val() == ''){
            $("#manager_id").focus().select()
            jsDisplayResult("error", "error", 'Scorecard manager is required');
            return false;
        } /*else if($("#owner_id").val() == ''){
            $("#owner_id").focus().select()
            jsDisplayResult("error", "error", 'Score Card owner  is required');
            return false;
        } */else {
            jsDisplayResult("info", "info", "Updating scorecard . . .  <img src='../images/loaderA32.gif' />");
            $.getJSON("../controllers/scorecardcontroller.php?action=editScoreCard",{
                score_card : $("#score_card-form").serialize()
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    if(response.updated)
                    {
                       jsDisplayResult("ok", "ok", response.text);
                    } else {
                       jsDisplayResult("info", "info", response.text);
                    }
                }
            });
        }

        e.preventDefault();
    })

    $("#save_score_card_changes").click(function(){
        jsDisplayResult("info", "info", "Updating scorecard settings . . .  <img src='../images/loaderA32.gif' />");
        $.getJSON("../controllers/scorecardsetupcontroller.php?action=editScoreCardSetting",{
            score_card : $("#edit-score-card-form").serialize()
        }, function(response) {
            if(response.error)
            {
                jsDisplayResult("error", "error", response.text);
            } else {
                if(response.updated)
                {
                    jsDisplayResult("ok", "ok", response.text);
                } else {
                    jsDisplayResult("info", "info", response.text);
                }
            }
        });
       return false;
    });

    $("#status_id").change(function(e) {
        var thisValue = $(this).val();
        if(thisValue == 3)
        {
            $("#approval").removeAttr("disabled");
        } else {
            $("#approval").attr("disabled", "disabled");
        }
        e.preventDefault();
    });



    $("#update_score_card").click(function(e){
        if($("#response").val() == '') {
            $("#response").focus().select()
            jsDisplayResult("error", "error", 'Response message is required');
            return false;
        } else if($("#status_id").val() == '' ) {
            $("#status_id").focus().select()
            jsDisplayResult("error", "error", 'Status is required');
            return false;
        }  else {
            jsDisplayResult("info", "info", "Updating score card . . .  <img src='../images/loaderA32.gif' />");
            $.post("../controllers/scorecardcontroller.php?action=updateScoreCard", {
                response       : $("#response").val(),
                status_id      : $("#status_id").val(),
                remind_on      : $("#remind_on").val(),
                id             : $("#score_card_id").val(),
                approval       : ($("#approval").is(":checked") ? 1 : 0)
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    if(response.updated)
                    {
                        jsDisplayResult("ok", "ok", response.text);
                    } else {
                        jsDisplayResult("info", "info", response.text);
                    }
                }
            }, 'json');
        }
        e.preventDefault();
    });

    $("#frequency_id").live("click", function(e){
       if($(this).val() == 'once_off')
       {
           ScoreCard.displayOnceOffForm();
       } else if($("#frequency_id").val() == 'recurring') {
           ScoreCard.displayRecurringForm();
       }
       return false;
    });

    $("#activate_score_card").live("click", function(e){
        ScoreCard.activateScoreCard();
        e.preventDefault();
    });

    $("#completion_rule").click(function(e){
        $(".completion-rule").hide();
        if($(this).val() == 'all')
        {
            $(".all-users-list").show();
        } else {
            $(".allocate").show();
        }
        e.preventDefault();
    });

    $("#completion_rule_allocate_to").click(function(e){
        $(".allocate-rule").hide();
        if($(this).val() == 'action_owner')
        {
            $(".allocate-users-list").show();
        } else {
            $(".allocated-directorate-list").show();
        }
        e.preventDefault();
    });

    $(".remove_attach").live("click", function(e){
        var id   = this.id
        var ext  = $(this).attr('title');
        var name = $(this).attr('file');
        $.post('../controllers/scorecardcontroller.php?action=deleteScoreCardFile', {
            attachment      : id,
            ext             : ext,
            name            : name,
            score_card_id   : $("#id").val()
        }, function(response){
            if(response.error)
            {
                $("#result_message").css({padding:"5px", clear:"both"}).html(response.text)
            } else {
                $("#result_message").addClass('ui-state-ok').css({padding:"5px", clear:"both"}).html(response.text)
                $("#li_"+ext).fadeOut();
            }
        },'json');
        e.preventDefault();
    });


});

var ScoreCard    = {

    displayRecurringForm        : function()
    {
        var self = this;
        if($("#score_card_recurring_form").length > 0)
        {
           $("#score_card_recurring_form").remove();
        }
        var html = [];
        html.push("<table>");
            html.push("<tr>");
              html.push("<th>Frequency Name</th>");
              html.push("<td><input name='frequency_name' id='frequency_name' value='' /></td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th>Period Start Date</th>");
                html.push("<td><input name='period_start_date' id='period_start_date' value='' class='datepicker' readonly='readonly' /></td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th>Period End Date</th>");
                html.push("<td><input name='period_end_date' id='period_end_date' value='' class='datepicker' readonly='readonly' /></td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th>Period Open From</th>");
                html.push("<td><input name='period_open_from' id='period_open_from' value='' class='datepicker' readonly='readonly' /></td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th>Period Open To</th>");
                html.push("<td><input name='period_open_to' id='period_open_to' value='' class='datepicker' readonly='readonly' /></td>");
            html.push("</tr>");
            html.push("<tr>");
                html.push("<th>Status</th>");
                html.push("<td></td>");
            html.push("</tr>");
        html.push("</table>");

        $("<div>", { id : "score_card_recurring_form", html: html.join(' ') })
         .dialog({
            modal   : true,
            width   : 'auto',
            title   : 'Recurring Frequency Setup',
            buttons : {
                        'Save'  : function()
                        {
                          $("#score_card_recurring_form").html("saving . . . ");
                        } ,

                        'Cancel'  : function()
                        {
                            $("#score_card_recurring_form").dialog('destroy').remove();
                        }
            } ,

            open       : function(event, ui)
            {
                $(".datepicker").datepicker({
                    showOn: 'both',
                    buttonImage: '/library/jquery/css/calendar.gif',
                    buttonImageOnly: true,
                    dateFormat: 'dd-M-yy',
                    changeMonth:true,
                    changeYear:true
                });
            } ,

            close      : function(event, ui)
            {
                $("#score_card_recurring_form").dialog('destroy').remove();
            }

        });


    } ,

    displayOnceOffForm           : function()
    {


    } ,

    activateScoreCard            : function()
    {
        if($("#score_card_ref").val() == '')
        {
           $("#score_card_ref").focus().select()
           jsDisplayResult("error", "error", 'Scorecard is required');
           return false;
        } else if($("#setup_description").val() == ''){
            $("#setup_description").focus().select()
            jsDisplayResult("error", "error", 'Scorecard instruction is required');
            return false;
        } else if($("#start_date").val() == ''){
            $("#start_date").focus().select()
            jsDisplayResult("error", "error", 'Scorecard start date is required');
            return false;
        } else if($("#end_date").val() == ''){
            $("#end_date").focus().select()
            jsDisplayResult("error", "error", 'Scorecard end date is required');
            return false;
        } else if($("#score_card_allocate_to").val() == '') {
            $("#score_card_allocate_to").focus().select()
            jsDisplayResult("error", "error", 'Please select the users to allocate the scorecard questions');
            return false;
        } else if($("#setup_recipient_user").val() == '') {
            $("#setup_recipient_user").focus().select()
            jsDisplayResult("error", "error", 'Please select the users to allocate the scorecard questions');
            return false;
        } else if($("#rating_type").val() == '') {
            $("#rating_type").focus().select()
            jsDisplayResult("error", "error", 'Overall rating type is required');
            return false;
        } else {
            jsDisplayResult("info", "info", "Activating scorecard . . .  <img src='../images/loaderA32.gif' />");
            $.getJSON("../controllers/scorecardcontroller.php?action=setupScoreCard",{
                score_card : $("#setup-score-card-form").serialize()
            }, function(response) {
                if(response.error)
                {
                    jsDisplayResult("error", "error", response.text);
                } else {
                    jsDisplayResult("ok", "ok", response.text);
                    $("input[type='text']").each(function(){
                        $(this).val('');
                    });
                    $('select').each(function(){
                        $(this).val('');
                    });
                    $('textarea').each(function(){
                        $(this).val('');
                    });
                }
            });
        }
    }

}


function attachScoreCardDocuments()
{
    $("#file_upload").html("uploading  . . . <img  src='../images/loaderA32.gif' />");

    $.ajaxFileUpload
    (
        {
            url			  : '../controllers/scorecardcontroller.php?action=saveAttachment',
            secureuri	  : false,
            fileElementId : 'score_card_attachment_document',
            dataType	  : 'json',
            success       : function (response, status)
            {
                $("#file_upload").html("");
                if(response.error )
                {
                    $("#file_upload").html(response.text )
                } else {
                    $("#file_upload").html("");
                    if(response.error)
                    {
                        $("#file_upload").addClass('ui-state-error').css({padding:"5px"}).html(response.text);
                    } else {
                        $("#file_upload").append($("<div />",{id:"result_message", html:response.text}).css({padding:"5px"}).addClass('ui-state-ok')
                        ).append($("<div />",{id:"files_uploaded"}))
                        if(!$.isEmptyObject(response.files))
                        {
                            var list = [];
                            list.push("<span>Files uploaded ..</span><br />");
                            $.each(response.files, function(ref, file) {
                                list.push("<p id='li_"+ref+"' style='padding:1px;' class='ui-state'>");
                                    list.push("<span class='ui-icon ui-icon-check' style='float:left;'></span>");
                                    list.push("<span style='margin-left:5px;'><a href='../controllers/scorecardcontroller.php?action=downloadFile&file="+file.key+"'>"+file.name+"</a>&nbsp;&nbsp;<a style='color:red;' href='#' class='delete' id='"+ref+"' title='"+file.ext+"'>Remove</a></span>");
                                    list.push("<input type='hidden' name='score_card[documents]["+file.key+"]' value='"+file.name+"' id='score_card_document_"+ref+"' />");
                                list.push("</p>");
                            });
                            $("#files_uploaded").html(list.join(' '));

                            $(".delete").live('click', function(e){
                                var id = this.id
                                var ext = $(this).attr('title');
                                $.post('../controllers/scorecardcontroller.php?action=removeScore CardAttachment',
                                    {
                                        attachment : id,
                                        ext        : ext
                                    }, function(response){
                                        if(response.error)
                                        {
                                            $("#result_message").html(response.text)
                                        } else {
                                            $("#result_message").addClass('ui-state-ok').html(response.text)
                                            $("#li_"+id).fadeOut();
                                        }
                                    },'json');
                                e.preventDefault();
                            });
                            $("#action_attachment").val("");
                        }
                    }

                    if( $("#delmessage").length != 0)
                    {
                        $("#delmessage").remove()
                    }
                }
            },
            error: function (data, status, e)
            {
                $("#fileupload").html( "Ajax error -- "+e+", please try uploading again");
            }
        }
    )
    return false;
}
