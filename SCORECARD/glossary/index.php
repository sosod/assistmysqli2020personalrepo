<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array()); ?>
<?php display_title('Glossary'); ?>
<?php display_menu_list('glossary'); ?>
<?php page_navigation_links(array('index.php' => 'Glossary')); ?>
<?php
$glossaryObj = new Glossary();
$glossaries  = $glossaryObj->getGlossaries();
?>
<table class="noborder" id="table_glossary" width="100%">
    <tr>
        <th>Category</th>
        <th>Field Name</th>
        <th>Purpose</th>
    </tr>
    <?php foreach($glossaries as $index => $glossary): ?>
        <?php if($glossary['category_id'] == 'deliverable') $glossary['category_id'] = 'Heading'; ?>
        <?php if($glossary['category_id'] == 'action') $glossary['category_id'] = 'Question'; ?>
        <tr>
            <td><?php echo ucwords( str_replace('_', ' ', $glossary['category_id']) ); ?></td>
            <td><?php echo ucwords($glossary['score_card_terminology']); ?></td>
            <td><?php echo ucwords($glossary['description']); ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<?php displayGoBack("", ""); ?>
<input type="hidden" name="show" id="show" value="1" />