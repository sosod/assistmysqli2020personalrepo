<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('deliverable.js')); ?>
<?php display_menu_list('manage', 'view_deliverable'); ?>
<?php display_sub_menu_list('manage', 'view'); ?>
<?php
$page_title = "Add Deliverable";
$columnsObj = new ActionColumns();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$deliverableObj = new Deliverable();
$deliverable       = $deliverableObj->getDeliverableDetail($_GET['id']);

$deliverableStatusesObj = new DeliverableStatus();
$deliverable_statuses = $deliverableStatusesObj->getList(true);

$deliverableAuditLogObj = new DeliverableAuditLog();
$deliverable_logs          = $deliverableAuditLogObj->getDeliverableActivityLog($_GET['id']);

page_navigation_links(array('view.php' => 'View', 'view_deliverable.php?id='.$deliverable['id'] => 'Deliverable'));
display_flash('action');
JSdisplayResultObj("");
?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2">
                        <?php if($deliverable['parent_id'] != 0): ?>
                            <input type="button" id="view_deliverable" name="view_deliverable" value="View Deliverable" />
                        <?php endif; ?>
                        <input type="button" id="view_contract" name="view_contract" value="View Contract" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><h4>Deliverable Details</h4></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_id']; ?>:</th>
                    <td><?php echo $deliverable['id']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_name']; ?>:</th>
                    <td><?php echo $deliverable['name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['description']; ?>:</th>
                    <td><?php echo $deliverable['description']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_category']; ?>:</th>
                    <td><?php echo $deliverable['deliverable_category']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_owner']; ?>:</th>
                    <td><?php echo $deliverable['deliverable_owner']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_deadline_date']; ?>:</th>
                    <td><?php echo $deliverable['deadline_date']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['main_deliverable']; ?>:</th>
                    <td><?php echo $deliverable['parent_deliverable']; ?></td>
                </tr>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <table width="100%">
                <tr>
                    <th class="th2" colspan='4'>Activity Log</th>
                </tr>
                <tr>
                    <th>Date Logged</th>
                    <th>User</th>
                    <th>Change Log</th>
                    <th>Status</th>
                </tr>
                <?php if(isset($deliverable_logs) && !empty($deliverable_logs)): ?>
                   <?php foreach($deliverable_logs as $q_index => $log): ?>
                        <tr>
                            <td align="center"><?php echo $log['date_loged']; ?></td>
                            <td><?php echo $log['user']; ?></td>
                            <td><?php echo $log['changeMessage']; ?></td>
                            <td><?php echo $log['status']; ?></td>
                        </tr>
                    <?php endforeach;  ?>
                <?php else:  ?>
                    <tr>
                        <td align="center" colspan="4">There are no logs for this deliverable</td>
                    </tr>
                <?php endif;  ?>
            </table>
            <input type="hidden"id="contract_id" value="<?php echo $deliverable['contract_id']?>" />
        </td>
    </tr>
</table>