<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('score_card.js', 'ajaxfileupload.js')); ?>
<?php display_menu_list('manage', 'edit_score_cards'); ?>
<?php display_sub_menu_list('manage', 'edit', 'edit_score_cards'); ?>
<?php
$scoreCardObj = new ScoreCard();
$score_card    = $scoreCardObj->getScoreCardDetail($_GET['id']);

$columnsObj = new Naming();
$columns    = $columnsObj->getHeaders();

$typeObj        = new ScoreCardType();
$score_card_types = $typeObj->getList(true);

$categoryObj    = new ScoreCardCategory();
$score_card_categories = $categoryObj->getList(true);

$statusObj         = new ScoreCardStatus();
$score_card_statuses = $statusObj->getList(true);


$userObj = new User();
$score_card_users = $userObj->getList(true);

$scoreCardObj = new ScoreCard();
$score_card_templates = $scoreCardObj->getList(true);

$financialYearObj = new FinancialYear();
$financial_years  = $financialYearObj->getList(true);

$ownerObj  = new ContractOwner();
$score_card_owners = $ownerObj->getList(true);

$documentObj                  = new ScoreCardDocument();
$score_card_documents           = $documentObj->getAll($score_card['id']);

page_navigation_links(array('edit_contracts.php' => 'Edit Scorecards', 'edit_score_card.php?id='.$_GET['id'] => 'Edit Scorecard'));
display_flash('contract');
JSdisplayResultObj("");
?>
<?php JSdisplayResultObj(""); ?>
<form method="post" id="contract-form" enctype="multipart/form-data">
<table id="contract_table" width="50%">
<tr valign="top">
    <th>
        <?php echo label_for('contract_id', $columns['contract_id']); ?>:
    </th>
    <td>
        <?php echo label_for('contract_id', $score_card['contract_id']); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('financial_year', $columns['financial_year']); ?>:
    </th>
    <td>
        <?php echo select_tag('contract[financial_year_id]', options_for_select($financial_years, $score_card['financial_year_id']), array('id' => 'financial_year_id')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('reference_number', $columns['reference_number']); ?>:
    </th>
    <td>
        <?php echo input_tag('contract[reference_number]', $score_card['reference_number'], array('id' => 'reference_number')) ?>
    </td>
</tr>
<tr>
    <th valign="top">
        <?php echo label_for('contract_name', $columns['contract_name']); ?>:
    </th>
    <td>
        <?php echo textarea_tag('contract[name]', $score_card['name'], array('id' => 'name')); ?>
    </td>
</tr>

<tr valign="top">
    <th>
        <?php echo label_for('contract_comment', $columns['contract_comment']); ?>:
    </th>
    <td>
        <?php echo textarea_tag('contract[comment]', $score_card['comment'], array('id' => 'comment')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('contract_type', $columns['contract_type']); ?>:
    </th>
    <td>
        <?php echo select_tag('contract[type_id]', options_for_select($score_card_types, $score_card['type_id']), array('id' => 'type_id')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('contract_category', $columns['contract_category']); ?>:
    </th>
    <td>
        <?php echo select_tag('contract[category_id]', options_for_select($score_card_categories, $score_card['category_id']), array('id' => 'category_id')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('risk_name', $columns['risk_name']); ?>
    </th>
    <td>
        <?php echo input_tag('contract[risk_name]', $score_card['risk_name'], array()) ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('risk_reference', $columns['risk_reference']); ?>
    </th>
    <td>
        <?php echo input_tag('contract[risk_reference]', $score_card['risk_reference'], array()) ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('account_code', $columns['account_code']); ?>
    </th>
    <td>
        <?php echo input_tag('contract[account_code]', $score_card['account_code'], array()) ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('signature_of_tender', $columns['signature_of_tender']); ?>
    </th>
    <td>
        <?php echo input_tag('contract[signature_of_tender]', $score_card['signature_of_tender'], array('class' => 'datepicker', 'readonly' => 'readonly')) ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('signature_of_sla', $columns['signature_of_sla']); ?>
    </th>
    <td>
        <?php echo input_tag('contract[signature_of_sla]', $score_card['signature_of_sla'], array('class' => 'datepicker', 'readonly' => 'readonly')) ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('completion_date', $columns['completion_date']); ?>
    </th>
    <td>
        <?php echo input_tag('contract[completion_date]', $score_card['completion_date'], array('class' => 'datepicker', 'readonly' => 'readonly')) ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('contract_manager', $columns['contract_manager']); ?>
    </th>
    <td>
        <?php echo select_tag('contract[manager_id]', options_for_select($score_card_users, $score_card['manager_id']), array('id' => 'manager_id')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('contract_owner', $columns['contract_owner']); ?>
    </th>
    <td>
        <?php echo select_tag('contract[owner_id]', options_for_select($score_card_owners, $score_card['owner_id']), array('id' => 'owner_id')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('contract_authorisor', $columns['contract_authorisor']); ?>
    </th>
    <td>
        <?php echo select_tag('contract[authorisor_id]', options_for_select($score_card_users, $score_card['authorisor_id']), array('id' => 'authorisor_id')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('contract_template', $columns['contract_template']); ?>
    </th>
    <td>
        <?php echo select_tag('contract[template_id]', options_for_select($score_card_templates, $score_card['template_id']), array('id' => 'template_id')); ?>
    </td>
</tr>
<tr class="delStatus">
    <th><?php echo label_for('contract_deliverable_status_assessed', $columns['deliverable_status_assessed']); ?>:</th>
    <td>
        <table width="100%">
            <?php foreach($assessment_deliverable_statues as $status_id => $deliverable_statues): ?>
                <tr>
                    <th class="th2"><?php echo $deliverable_statues ?></th>
                    <td>
                        <select name="contract[deliverable_status][<?php echo $status_id; ?>]">
                            <option value="0" <?php echo ($score_card_deliverable_statues[$status_id] == 1 ? '' : "selected='selected'") ?> >No</option>
                            <option value="1" <?php echo ($score_card_deliverable_statues[$status_id] == 1 ? "selected='selected'" : '') ?>>Yes</option>
                        </select>
                        <?php /*echo input_hidden_tag("contract[deliverable_status][".$status_id."]", $status_id ); */?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </td>
</tr>
<tr>
    <th><?php echo $columns['contract_assessment_frequency']; ?>:
        <!--<em>
         If weekly , select day of week and time to be send<br />
         If monthly , select day of month and time to be send<br />
         If Quartely , select day and time to be send etc.
         </em> -->
    </th>
    <td>
        <?php echo select_tag('contract[assessment_frequency_id]', options_for_select($score_card_assessment_frequencies, $score_card['assessment_frequency_id']), array('id' => 'assessment_frequency_id')); ?>
    </td>
</tr>
<tr id="assessment_frequency_date_picker" <?php echo !empty($score_card['assessment_frequency_id']) ? "" : "style='display:none;'";  ?> class="assessment_frequency">
    <th><?php echo $columns['contract_assessment_frequency_start_date']; ?>:</th>
    <td>
        <?php echo input_tag('contract[assessment_frequency_date]', $score_card['assessment_frequency_date'], array('class' => 'datepicker', 'readonly' => 'readonly')) ?>
    </td>
</tr>
<tr>
    <th><?php echo $columns['attach_a_document']; ?>:</th>
    <td>
        <?php Attachment::displayAttachmentList($score_card_documents, $type = "contract"); ?>
        <div id="file_upload"></div>
        <input type="file"  name="contract_document" id="contact_attachment_document"  class="attachment" onChange="attachContractDocuments(this)" />
    </td>
</tr>
<tr>
    <th></th>
    <td>
        <input type="submit" id="edit_contract" name="edit_contract" value="Save Changes" class="save_contract isubmit" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="hidden" id="id" name="contract[id]" value="<?php echo $score_card['id']; ?>" />
        <input type="hidden" class="logid" name="contract_id" id="contract_id" value="<?php echo $_REQUEST['id'] ?>"  />
    </td>
</tr>
<tr>
    <td><?php displayGoBack("",""); ?></td>
    <td>
        <span style="float:right;">
            <?php displayAuditLogLink("contract_edit_logs" , false); ?>
        </span>
    </td>
</tr>
</table>
</form>