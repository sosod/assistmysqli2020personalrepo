<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.ui.contract.js', 'jquery.ui.deliverable.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('manage', 'authorization'); ?>
<?php display_sub_menu_list('manage', 'authorization', 'authorization'); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#contract").contract({authorizeContract:true, section:"manage", page:"authorization", autoLoad: false, showActions: true, deliverableFilter: false, actionFilter: false });
    });
</script>
<div id="contract"></div>