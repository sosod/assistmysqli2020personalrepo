<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.ui.scorecard.js', 'jquery.ui.deliverable.js', 'jquery.scorecard.subdeliverable.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('manage'); ?>
<?php page_navigation_links(array('index.php' => 'View')); ?>
<script language="javascript">
    $(function(){
        $("#score_card").scorecard({showActions:true, section:"manage", page:"view", viewScoreCard:true, viewDeliverable: true, viewAction:true, actionFilter:false, deliverableFilter: false, autoLoad: false});
    });
</script>
<div id="score_card"></div>