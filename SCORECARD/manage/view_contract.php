<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>

<?php include_js(array('contract.js')); ?>
<?php display_menu_list('manage', 'index'); ?>
<?php display_sub_menu_list('manage', 'view'); ?>
<?php
$page_title = "Add Deliverable";
$columnsObj = new ContractColumns();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$contractObj = new Contract();
$contract    = $contractObj->getContractDetail($_GET['id']);
//debug($contract);
$categoryObj            = new DeliverableCategory();
$deliverable_categories = $categoryObj->getList(true);

$deliverableObj         = new Deliverable();
$main_deliverables      = $deliverableObj->getList(true);

$deliveredWeightObj     = new DeliveredWeight();
$delivered_weights      = $deliveredWeightObj->getList(true);

$qualityWeightObj       = new QualityWeight();
$quality_weights        = $qualityWeightObj->getList(true);

$otherWeightObj         = new OtherWeight();
$other_weights          = $otherWeightObj->getList(true);

$contractStatusObj = new ContractStatus();
$contract_statuses = $contractStatusObj->getList(true);

page_navigation_links(array('index.php' => 'View', 'view_contract.php?id='.$contract['id'] => 'View Contract'));

display_flash('deliverable');
JSdisplayResultObj("");

$contractAuditLogObj = new ContractAuditLog();
$contract_logs       = $contractAuditLogObj->getContractActivityLog($_GET['id']);

$contract_list = $contractObj->getContractDetailList($contract);
?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2"><h3>Contract Details</h3></td>
                </tr>
                <?php foreach($contract_list as $key => $contract_detail): ?>
                    <tr>
                        <th><?php echo $contract_detail['header']; ?></th>
                        <td><?php echo $contract_detail['value']; ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <table width="100%">
                <tr>
                    <th class="th2" colspan='4'>Activity Log</th>
                </tr>
                <tr>
                    <th>Date Logged</th>
                    <th>User</th>
                    <th>Change Log</th>
                    <th>Status</th>
                </tr>
                <?php
                if(!empty($contract_logs))
                {
                    foreach($contract_logs as $q_index => $log)
                    {
                        ?>
                        <tr>
                            <td align="center"><?php echo $log['date_loged']; ?></td>
                            <td><?php echo $log['user']; ?></td>
                            <td><?php echo $log['changeMessage']; ?></td>
                            <td><?php echo $log['status']; ?></td>
                        </tr>
                    <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td align="center" colspan="4">There are no logs yet</td>
                    </tr>
                <?php
                }
                ?>
            </table>
        </td>
    </tr>
</table>