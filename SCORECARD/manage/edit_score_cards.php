<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.ui.scorecard.js')); ?>
<?php display_menu_list('manage', 'edit_score_cards'); ?>
<?php display_sub_menu_list('manage', 'edit', 'edit_score_cards'); ?>
<?php page_navigation_links(array('edit_contracts.php' => 'Edit Scorecards')); ?>
<script language="javascript">
    $(function(){
        $("#score_card").scorecard({editContract:true, section:"manage", page:"edit", autoLoad: false});
    });
</script>
<div id="score_card"></div>