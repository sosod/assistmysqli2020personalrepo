<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.contract.notification.js')); ?>
<?php display_menu_list('manage', 'my_profile'); ?>
<?php display_sub_menu_list('manage', 'my_profile'); ?>
<?php page_navigation_links(array('my_profile.php' => 'My Profile'));
      display_flash('contract');
      JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#notifications").notification();
    });
</script>
<?php JSdisplayResultObj(""); ?>
<div id="notifications"></div>
<?php displayAuditLogLink("user_notifications_logs", true) ?>

