<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.contract.deliverableassurance.js', 'ajaxfileupload.js', 'deliverable.js')); ?>
<?php display_menu_list('manage', 'assurance'); ?>
<?php display_sub_menu_list('manage', 'assurance', 'assurance_deliverables'); ?>
<?php
$page_title = "Add Deliverable";
$columnsObj = new ActionColumns();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$deliverableObj = new Deliverable();
$deliverable       = $deliverableObj->getDeliverableDetail($_GET['id']);

$deliverableStatusesObj = new DeliverableStatus();
$deliverable_statuses = $deliverableStatusesObj->getList(true);

$deliverableDocumentObj = new DeliverableDocument();
$deliverable_documents = $deliverableDocumentObj->getAll($deliverable['id']);
?>
<?php page_navigation_links(array('assurance.php' => 'Assurance', 'deliverable_assurance.php?id='.$deliverable['id'] => 'Heading'));
display_flash('action');
JSdisplayResultObj("");
?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2">
                        <?php if($deliverable['parent_id'] != 0): ?>
                            <input type="button" id="view_deliverable" name="view_deliverable" value="View Deliverable" />
                        <?php endif; ?>
                        <input type="button" id="view_contract" name="view_contract" value="View Contract" />
                        <input type="hidden" id="contract_id" name="contract_id" value="<?php echo $deliverable['contract_id']; ?>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><h4>Heading Details</h4></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_id']; ?>:</th>
                    <td><?php echo $deliverable['id']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_name']; ?>:</th>
                    <td><?php echo $deliverable['name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['description']; ?>:</th>
                    <td><?php echo $deliverable['description']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_category']; ?>:</th>
                    <td><?php echo $deliverable['deliverable_category']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_owner']; ?>:</th>
                    <td><?php echo $deliverable['deliverable_owner']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_deadline_date']; ?>:</th>
                    <td><?php echo $deliverable['deliverable_deadline_date']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_remind_on']; ?>:</th>
                    <td><?php echo $deliverable['deliverable_remind_on']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_type']; ?>:</th>
                    <td><?php echo ($deliverable['type_id'] == 0 ? 'Main' : 'Sub Deliverable'); ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_type']; ?>:</th>
                    <td><?php echo ($deliverable['type_id'] == 0 ? 'Main' : 'Sub Deliverable'); ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['main_deliverable']; ?>:</th>
                    <td><?php echo ($deliverable['type_id'] == 1 ? 'Yes' : 'No'); ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_status']; ?>:</th>
                    <td><?php echo $deliverable['deliverable_status']; ?></td>
                </tr>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <script language="javascript">
                $(function(){
                    $("#deliverable_assurance").deliverableassurance({section:"manage", page:"assurance", autoLoad: true, deliverable_id:<?php echo $_GET['id']; ?>});
                });
            </script>
            <div id="deliverable_assurance"></div>
            <?php displayAuditLogLink("deliverable_assurance_logs", false) ?>
        </td>
    </tr>
</table>