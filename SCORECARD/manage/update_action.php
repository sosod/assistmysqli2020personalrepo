<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('action.js', 'ajaxfileupload.js')); ?>
<?php display_menu_list('manage', 'update_actions'); ?>
<?php display_sub_menu_list('manage', 'update', 'update_actions'); ?>
<?php

$columnsObj = new ActionColumns();
$columns    = $columnsObj->getHeaders();

$actionObj  = new Action();
$action     = $actionObj->getActionDetail($_GET['id']);

$deliverableObj = new Deliverable();
$deliverable       = $deliverableObj->getDeliverableDetail($action['deliverable_id']);

$actionStatusObj = new ActionStatus();
$action_statuses = $actionStatusObj->getList(true);

$actionDocumentObj = new ActionDocument();
$action_documents = $actionDocumentObj->getAll($action['id']);

?>
<?php
page_navigation_links(array('update_actions.php' => 'Update Actions', 'update_action.php?id='.$_GET['id'] => 'Update Question'));
display_flash('action');
JSdisplayResultObj("");
?>
<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2">
                       <input type="button" id="view_deliverable" name="view_deliverable" value="View Deliverable" />
                       <input type="button" id="view_contract" name="view_contract" value="View Contract" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><h4>Action Details</h4></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_id']; ?>:</th>
                    <td><?php echo $action['id']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_name']; ?>:</th>
                    <td><?php echo $action['action_name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_deliverable']; ?>:</th>
                    <td><?php echo $action['action_deliverable']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_owner']; ?>:</th>
                    <td><?php echo $action['action_owner']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_progress']; ?>:</th>
                    <td><?php echo $action['action_progress']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_status']; ?>:</th>
                    <td><?php echo $action['action_status']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_deadline_date']; ?>:</th>
                    <td><?php echo $action['action_deadline_date']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_remind_on']; ?>:</th>
                    <td><?php echo $action['action_remind_on']; ?></td>
                </tr>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder">
           <form id="action_update_form" name="action_update_form">
                <table id="new_update" width="100%">
                    <tr>
                        <td colspan="2"><h4>New Update</h4></td>
                    </tr>
                    <tr>
                        <th>Description:</th>
                        <td>
                            <?php echo textarea_tag('response', null, array('id' => 'response')) ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Status:</th>
                        <td>
                            <?php echo select_tag('action[status_id]', options_for_select($action_statuses, $action['status_id']), array('id' => 'status_id')) ?>
                            <p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px; clear:both; margin-top:10px;"><small>Completed actions will be sent to the relevant person(s) for approval and cannot be updated further.</small>
                            </p>
                            <input type="hidden" name="_status" id="_status" value="<?php echo $action['status_id']; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Action On:</th>
                        <td>
                            <?php echo input_tag('action[action_on]', $action['action_on'], array('class' => 'historydate', 'readonly' => 'readonly', 'id' => 'action_on')) ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Progress:</th>
                        <td>
                            <input type="text" name="action[progress]" id="progress" value="<?php echo $action['progress']; ?>" />
                            <em style="color:#FF0000">%</em>
                            <input type="hidden" name="_progress" id="_progress" value="<?php echo $action['progress']; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>Remind On:</th>
                        <td>
                            <?php echo input_tag('action[remind_on]', $action['action_remind_on'], array('class' => 'datepicker', 'readonly' => 'readonly', 'id' => 'remind_on')) ?>
                            <a href="#" id="clear_remind">Clear Date</a>
                        </td>
                    </tr>
                    <tr>
                        <th>Attachment:</th>
                        <td>
                            <?php echo Attachment::displayEditableAttachments($action_documents, 'action'); ?>
                            <input id="action_attachment" name="action_attachment" type="file" onChange="attachActionDocuments(this)" />
                            <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
                            <small>You can attach more than 1 file.</small>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th>Send Approval Notification:</th>
                        <td>
                            <input type="checkbox" id="approval" name="approval"
                            <?php if($action['progress'] !== "100" || $action['status'] != 3) { ?> disabled="disabled" <?php } ?> value="1"  />

                            <p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px; clear:both; margin-top:10px;"><small>This will send an email notification to the relevant person(s) responsible for approving this Action.</small>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <input type="submit" name="save_action_update" id="save_action_update" value="Save" class="isubmit"  />
                            <input type="submit" name="cancel_action_update" id="cancel_action_update" value="Cancel" class="idelete"  />
                            <span style="float:right;">
                                <?php displayAuditLogLink("action_update_logs" , false); ?>
                            </span>
                            <input type="hidden" name="action_id" id="action_id" value="<?php echo $_REQUEST['id'] ?>"  />
                            <input type="hidden" name="deliverable_id" id="deliverable_id" value="<?php echo $action['deliverable_id']; ?>"  />
                            <input type="hidden" name="contract_id" id="contract_id" value="<?php echo $action['contract_id']; ?>"  />
                            <input type="hidden" class="logid" name="action_id" id="action_id" value="<?php echo $_REQUEST['id'] ?>"  />
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>
<script>
    $(function(){
        $("table#update").find("th").css({"text-align":"left"})
    });
</script>