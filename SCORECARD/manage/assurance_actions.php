<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.ui.action.js')); ?>
<?php display_menu_list('manage', 'assurance'); ?>
<?php display_sub_menu_list('manage', 'assurance', 'assurance_actions'); ?>
<?php page_navigation_links(array('assurance.php' => 'Assurance', 'assurance_actions.php' => 'Questions')); ?>
<script language="javascript">
    $(function(){
        $("#action_list").action({actionAssurance: true, section:'manage', page:"assurance", autoLoad: false});
    });
</script>
<div id="action_list"></div>