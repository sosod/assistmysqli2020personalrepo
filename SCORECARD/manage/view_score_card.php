<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('score_card.js')); ?>
<?php display_menu_list('manage', 'index'); ?>
<?php display_sub_menu_list('manage', 'view'); ?>
<?php
$columnsObj = new ScoreCardColumn();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$scoreCardObj = new ScoreCard();
$score_card    = $scoreCardObj->getScoreCardDetail($_GET['id']);
//debug($score_card);
$categoryObj            = new DeliverableCategory();
$deliverable_categories = $categoryObj->getList(true);

$deliverableObj         = new Deliverable();
$main_deliverables      = $deliverableObj->getList(true);

$weightObj = new Weight();
$weights   = $weightObj->getList(true);

$scoreCardStatusObj = new ScoreCardStatus();
$score_card_statuses = $scoreCardStatusObj->getList(true);

page_navigation_links(array('index.php' => 'View', 'view_score_card.php?id='.$score_card['id'] => 'View Scorecard'));

display_flash('deliverable');
JSdisplayResultObj("");

$scoreCardAuditLogObj = new ScoreCardAuditLog();
$score_card_logs       = $scoreCardAuditLogObj->getScoreCardActivityLog($_GET['id']);

$score_card_list = $scoreCardObj->getScoreCardDetailList($score_card);
?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2"><h3>Scorecard Details</h3></td>
                </tr>
                <?php foreach($score_card_list as $key => $score_card_detail): ?>
                    <tr>
                        <th><?php echo $score_card_detail['header']; ?></th>
                        <td><?php echo $score_card_detail['value']; ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <table width="100%">
                <tr>
                    <th class="th2" colspan='4'>Activity Log</th>
                </tr>
                <tr>
                    <th>Date Logged</th>
                    <th>User</th>
                    <th>Change Log</th>
                    <th>Status</th>
                </tr>
                <?php if(!empty($score_card_logs)): ?>
                    <?php foreach($score_card_logs as $q_index => $log): ?>
                        <tr>
                            <td align="center"><?php echo $log['date_loged']; ?></td>
                            <td><?php echo $log['user']; ?></td>
                            <td><?php echo $log['changeMessage']; ?></td>
                            <td><?php echo $log['status']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td align="center" colspan="4">There are no logs yet</td>
                    </tr>
                <?php endif;  ?>
            </table>
        </td>
    </tr>
</table>