<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.scorecard.completequestion.js')); ?>
<?php display_menu_list('manage', 'update_actions'); ?>
<?php display_sub_menu_list('manage', 'update', 'update_actions'); ?>
<?php page_navigation_links(array('update_actions.php' => 'Complete Questions')); ?>
<?php display_flash('action'); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#question_list").completequestion({updateAssessAction: true, section:'manage', page: "update", autoLoad: false});
    });
</script>
<div id="question_list"></div>