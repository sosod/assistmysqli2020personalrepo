<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('user.js')); ?>
<?php display_menu_list('manage', 'my_profile'); ?>
<?php display_sub_menu_list('manage', 'my_profile'); ?>
<?php page_navigation_links(array('my_profile.php' => 'My Profile'));
      display_flash('contract');
      JSdisplayResultObj("");
      $userObj = new User();
      $user    = $userObj->getSettingByUserId($_SESSION['tid']);
?>
<table border="1" width="60%" cellpadding="5" cellspacing="0" id="link_to_page">
    <tr>
        <th>Default Financial Year:</th>
        <td><?php echo (!empty($user['financial_year_id']) ? $user['default_financial_year'] : 'Not yet set'); ?></td>
        <td>
            <input type="button" name="configure_financial_year" id="configure_financial_year" value="Configure" class="setup_defaults"  />
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user['id']; ?>"  />
            <input type="hidden" name="default_year_id" id="default_year_id" value="<?php echo $user['financial_year_id']; ?>"  />
        </td>
    </tr>
    <!--<tr>
        <th>Define Notification Rules:</th>
        <td>Define Notification Rules</td>
        <td>
            <input type="button" name="notification_rules" id="notification_rules" value="Configure" class="setup_defaults"  />
        </td>
    </tr> -->
</table>
