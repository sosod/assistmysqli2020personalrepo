<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.ui.deliverable.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('manage', 'update_actions'); ?>
<?php display_sub_menu_list('manage', 'update', 'update_deliverables'); ?>
<?php
page_navigation_links(array('update_deliverables.php' => 'Update Deliverables'));
display_flash('action');
JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#deliverable_list").deliverable({updateDeliverable: true, page: 'update', section: 'manage', showActions: true, viewAction:true, actionFilter: false });
    });
</script>
<div id="deliverable_list"></div>
