<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.ui.scorecard.js')); ?>
<?php display_menu_list('manage', 'assurance'); ?>
<?php display_sub_menu_list('manage', 'assurance', 'assurance'); ?>
<?php page_navigation_links(array('assurance.php' => 'Assurance')); ?>
<?php display_flash('assurance'); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#scorecard").scorecard({viewAction:true, section:"manage", page:"assurance", scoreCardAssurance: true, autoLoad: false});
    });
</script>
<div id="scorecard"></div>