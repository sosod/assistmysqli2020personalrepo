<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('contract.js')); ?>
<?php display_menu_list('manage', 'update_actions'); ?>
<?php display_sub_menu_list('manage', 'update'); ?>
<?php
$page_title = "Add Deliverable";
$columnsObj = new ContractColumns();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$contractObj = new Contract();
$contract    = $contractObj->getContractDetail($_GET['id']);
//debug($contract);
$categoryObj            = new DeliverableCategory();
$deliverable_categories = $categoryObj->getList(true);

$deliverableObj         = new Deliverable();
$main_deliverables      = $deliverableObj->getList(true);

$deliveredWeightObj     = new DeliveredWeight();
$delivered_weights      = $deliveredWeightObj->getList(true);

$qualityWeightObj       = new QualityWeight();
$quality_weights        = $qualityWeightObj->getList(true);

$otherWeightObj         = new OtherWeight();
$other_weights          = $otherWeightObj->getList(true);

$contractStatusObj = new ContractStatus();
$contract_statuses = $contractStatusObj->getList(true);


$supplierBudgetObj      = new ContractSupplierBudget();
$supplier_budgets       = $supplierBudgetObj->getAll($contract['id']);

$supplierObj = new Supplier();
$contract_suppliers = $supplierObj->getList(true);

page_navigation_links(array('update_budget.php' => 'Update Budget', 'update_contract_budget.php?id='.$contract['id'] => 'Update Contract Budget'));

display_flash('deliverable');
JSdisplayResultObj("");
?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2"><h4>Contract Details</h4></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_id']; ?>:</th>
                    <td><?php echo $contract['contract_id']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['reference_number']; ?>:</th>
                    <td><?php echo $contract['reference_number']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_name']; ?>:</th>
                    <td><?php echo $contract['name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_comment']; ?>:</th>
                    <td><?php echo $contract['comment']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_type']; ?>:</th>
                    <td><?php echo $contract['contract_type']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_category']; ?>:</th>
                    <td><?php echo $contract['contract_category']; ?></td>
                </tr>
                <!--                <tr>
                    <th><?php /*echo $columns['contract_supplier']; */?>:</th>
                    <td><?php /*echo $contract['contract_supplier']; */?></td>
                </tr>
                <tr>
                    <th><?php /*echo $columns['contract_budget']; */?>:</th>
                    <td><?php /*echo $contract['budget']; */?></td>
                </tr>-->
                <tr>
                    <th><?php echo $columns['payment_term']; ?>:</th>
                    <td><?php echo $contract['payment_term']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['payment_frequency']; ?>:</th>
                    <td><?php echo $contract['payment_frequency']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['risk_name']; ?>:</th>
                    <td><?php echo $contract['risk_name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['payment_frequency']; ?>:</th>
                    <td><?php echo $contract['payment_frequency']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_manager']; ?>:</th>
                    <td><?php echo $contract['contract_manager']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_authorisor']; ?>:</th>
                    <td><?php echo $contract['contract_authorisor']; ?></td>
                </tr>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <form  method="post"  name="contract-budget-form" id="contract-budget-form" enctype="multipart/form-data">
                <table width="100%" id="suppler_budget_table">
                        <tr>
                            <td colspan="2"><h4>Update Contract</h4></td>
                        </tr>
                        <tr valign="top">
                            <th>
                                <?php echo label_for('response', 'Reason'); ?>
                            </th>
                            <td>
                                <?php echo textarea_tag('contract[response]', null, array('id' => 'response')) ?>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th>
                                <?php echo label_for('contract_budget_date', "Date"); ?>
                            </th>
                            <td>
                                <?php echo input_tag('contract[budget_date]', null, array('class' => 'datepicker', 'readonly' => 'readonly', 'id' => 'remind_on')) ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Attachment:</th>
                            <td>
                                <input id="deliverable_attachment_<?php echo $contract['id']; ?>" name="deliverable_attachment_<?php echo $contract['id']; ?>" type="file" class="upload_deliverable" />
                                <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
                                    <small>You can attach more than 1 file.</small>
                                </p>
                                <?php
                                //Attachment::displayAttachmentList($action['attachement']);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Budget Amount</th>
                            <td>
                                <table width="100%">
                                <?php $i = 1; ?>
                                    <?php foreach($supplier_budgets as $index => $supplier_budget): ?>
                                        <tr valign="top" class='supplier_budget' id="supplier_<?php echo $i; ?>">
                                            <th class="th2">
                                                <?php echo label_for('contract_supplier', $columns['contract_supplier']); ?>:
                                            </th>
                                            <td>
                                                <?php echo select_tag('contract[suppler_budget]['.$i.'][supplier_id]', options_for_select($contract_suppliers, $supplier_budget['supplier_id']), array('id' => 'supplier_id_'.$i)); ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th class="th2">
                                                <?php echo label_for('contract_budget', $columns['contract_budget']); ?>
                                            </th>
                                            <td>
                                                <?php echo input_tag('contract[suppler_budget]['.$i.'][budget]', $supplier_budget['budget'], array()) ?>
                                                <?php echo input_hidden_tag('contract[suppler_budget]['.$i.'][id]', $supplier_budget['id']); ?>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <input type="submit" name="update_contract_budget" id="update_contract_budget" value="Save Changes" class=" isubmit" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="hidden" name="contract[id]" id="id" value="<?php echo $_GET['id']?>" />
                                <input type="hidden" class="logid" name="contract_id" id="contract_id" value="<?php echo $_REQUEST['id'] ?>"  />
                                <span style="float:right;">
                                        <?php displayAuditLogLink("contract_update_logs" , false); ?>
                                </span>
                            </td>
                        </tr>
                </table>
            </form>
        </td>
    </tr>
</table>