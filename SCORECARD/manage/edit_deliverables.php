<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.ui.deliverable.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('manage', 'edit_score_cards'); ?>
<?php display_sub_menu_list('manage', 'edit', 'edit_deliverables'); ?>
<?php page_navigation_links(array('edit_deliverables.php' => 'Edit Deliverables'));
      display_flash('deliverable');
      JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#deliverable_list").deliverable({editDeliverable: true, section: 'manage', 'page': 'edit', showActions: true, actionFilter: false});
    });
</script>
<div id="deliverable_list"></div>
