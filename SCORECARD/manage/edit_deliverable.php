<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php display_title('Add New Deliverable'); ?>
<?php include_js(array('deliverable.js')); ?>
<?php display_menu_list('manage', 'edit_contracts'); ?>
<?php display_sub_menu_list('manage', 'edit', 'edit_deliverables'); ?>
<?php
$page_title = "Add Deliverable";
$columnsObj = new ContractColumns();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$categoryObj            = new DeliverableCategory();
$deliverable_categories = $categoryObj->getList(true);

$deliverableObj = new Deliverable();
$deliverable    = $deliverableObj->getDeliverableDetail($_GET['id']);

//debug($deliverable);

$contractObj = new Contract();
$contract    = $contractObj->getContractDetail($deliverable['contract_id']);

$deliverableObj         = new Deliverable();
$option_sql             = ' AND C.id = '.$deliverable['contract_id'];
$main_deliverables      = $deliverableObj->getList(true, false, $option_sql);

$deliveredWeightObj     = new DeliveredWeight();
$delivered_weights      = $deliveredWeightObj->getList(true);

$qualityWeightObj       = new QualityWeight();
$quality_weights        = $qualityWeightObj->getList(true);

$otherWeightObj         = new OtherWeight();
$other_weights          = $otherWeightObj->getList(true);

$userObj = new User();
$deliverable_users = $userObj->getList(true);

page_navigation_links(array('edit_deliverables.php' => 'Edit Deliverables', 'edit_deliverable.php?id='.$_GET['id'] => 'Edit Deliverable'));

display_flash('deliverable');
JSdisplayResultObj("");
?>
<form  method="post"  name="deliverable-form" id="deliverable-form" enctype="multipart/form-data">
    <table>
        <tr>
            <td colspan="2"><h4>Edit Deliverable</h4></td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_name', $deliverable_columns['deliverable_name']); ?>
            </th>
            <td>
                <?php echo textarea_tag('deliverable[name]', $deliverable['name'], array('id' => 'name')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('description', $deliverable_columns['description']); ?>
            </th>
            <td>
                <?php echo textarea_tag('deliverable[description]', $deliverable['description'], array('id' => 'description')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_category', $deliverable_columns['deliverable_category']); ?>
            </th>
            <td>
                <?php echo select_tag('deliverable[category_id]', options_for_select($deliverable_categories, $deliverable['category_id']), array('id' => 'category_id')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_type', $deliverable_columns['deliverable_type']); ?>
            </th>
            <td>
                <?php echo select_tag('deliverable[type_id]', options_for_select(array('0' => 'Main', '1' => 'Sub'), $deliverable['type_id']), array('id' => 'type_id')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('main_deliverable', $deliverable_columns['main_deliverable']); ?>
            </th>
            <td>
                <?php echo select_tag('deliverable[parent_id]', options_for_select($main_deliverables, $deliverable['parent_id']), array('id' => 'parent_id')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_owner', $deliverable_columns['deliverable_owner']); ?>
            </th>
            <td>
                <?php echo select_tag('deliverable[owner_id]', options_for_select($deliverable_users, $deliverable['owner_id']), array('id' => 'owner_id')) ?>
            </td>
        </tr>
        <?php if($contract['weight_assigned']): ?>
            <?php if($contract['is_assessed_quantitative']): ?>
                <tr valign="top" class='deliverable_weights' >
                    <th>
                        <?php echo label_for('delivered_weight', $deliverable_columns['delivered_weight']); ?>
                    </th>
                    <td>
                        <?php echo select_tag('deliverable[weight_id]', options_for_select($delivered_weights, $deliverable['weight_id']), array('id' => 'weight_id')) ?>
                    </td>
                </tr>
            <?php endif ?>
            <?php if($contract['is_assessed_qualitative']): ?>
                <tr valign="top" class='deliverable_weights'>
                    <th>
                        <?php echo label_for('quality_weight', $deliverable_columns['quality_weight']); ?>
                    </th>
                    <td>
                        <?php echo select_tag('deliverable[quality_weight_id]',  options_for_select($quality_weights, $deliverable['quality_weight_id']), array('id' => 'quality_weight_id')) ?>
                    </td>
                </tr>
            <?php endif ?>
            <?php if($contract['is_assessed_other']): ?>
                <tr valign="top" class='deliverable_weights'>
                    <th>
                        <?php echo label_for('other_weight', (!empty($contract['other_assessed_category']) ?  $contract['other_assessed_category'] : $deliverable_columns['other_weight'])); ?>
                    </th>
                    <td>
                        <?php echo select_tag('deliverable[other_weight_id]',  options_for_select($other_weights, $deliverable['other_weight_id']), array('id' => 'other_weight_id')) ?>
                    </td>
                </tr>
            <?php endif ?>
        <?php endif ?>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_deadline_date', $deliverable_columns['deliverable_deadline_date']); ?>
            </th>
            <td>
                <?php echo input_tag('deliverable[deadline_date]', $deliverable['deadline_date'], array('class' => 'datepicker', 'readonly' => 'readonly')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_remind_on', $deliverable_columns['deliverable_remind_on']); ?>
            </th>
            <td>
                <?php echo input_tag('deliverable[remind_on]', $deliverable['remind_on'], array('class' => 'datepicker', 'readonly' => 'readonly', 'id' => 'remind_on')) ?>
                <a href="#" id="clear_remind">Clear Date</a>
            </td>
        </tr>
        <tr>
            <th></th>
            <td>
                <input type="submit" name="edit_deliverable_change" id="edit_deliverable_change" value="Save Changes" class="isubmit" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="hidden" name="deliverable[contract_id]" id="contract_id" value="<?php echo $deliverable['contract_id']?>" />
                <input type="hidden" name="deliverable[id]" id="deliverable_id" value="<?php echo $_GET['id']?>" />
                <input type="hidden" name="log_id" id="deliverable_id" value="<?php echo $deliverable['id']?>" class="logid" />
            </td>
        </tr>
        <tr>
            <td class="noborder"><?php displayGoBack("",""); ?></td>
            <td class="noborder"><?php displayAuditLogLink("deliverable_edit_logs", false) ?></td>
        </tr>
    </table>
</form>