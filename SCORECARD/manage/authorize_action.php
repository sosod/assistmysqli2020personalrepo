<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.contract.actionauthorization.js', 'ajaxfileupload.js', 'action.js')); ?>
<?php display_menu_list('manage', 'authorization'); ?>
<?php display_sub_menu_list('manage', 'authorization', 'authorize_action'); ?>
<?php
$columnsObj = new ActionColumns();
$columns    = $columnsObj->getHeaders();

$actionObj  = new Action();
$action     = $actionObj->getActionDetail($_GET['id']);

$deliverableObj = new Deliverable();
$deliverable       = $deliverableObj->getDeliverableDetail($action['deliverable_id']);

$actionStatusObj = new ActionStatus();
$action_statuses = $actionStatusObj->getList(true);

$actionDocumentObj = new ActionDocument();
$action_documents = $actionDocumentObj->getAll($action['id']);

?>
<?php
page_navigation_links(array('authorization.php' => 'Authorization', 'action_authorization.php?contract_id='.$deliverable['contract_id'] => 'Actions', 'authorize_action.php?id='.$_GET['id'] => 'Action'));
display_flash('action');
JSdisplayResultObj("");
?>
<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2">
                        <input type="button" id="view_deliverable" name="view_deliverable" value="View Deliverable" />
                        <input type="button" id="view_contract" name="view_contract" value="View Contract" />
                        <input type="hidden" id="contract_id" name="contract_id" value="<?php  echo $deliverable['contract_id']; ?>" />
                        <input type="hidden" id="deliverable_id" name="deliverable_id" value="<?php  echo $deliverable['id']; ?>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><h4>Action Details</h4></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_id']; ?>:</th>
                    <td><?php echo $action['id']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_name']; ?>:</th>
                    <td><?php echo $action['action_name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_deliverable']; ?>:</th>
                    <td><?php echo $action['action_deliverable']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_owner']; ?>:</th>
                    <td><?php echo $action['action_owner']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_progress']; ?>:</th>
                    <td><?php echo $action['action_progress']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_status']; ?>:</th>
                    <td><?php echo $action['action_status']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_deadline_date']; ?>:</th>
                    <td><?php echo $action['action_deadline_date']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_remind_on']; ?>:</th>
                    <td><?php echo $action['action_remind_on']; ?></td>
                </tr>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder">
            <script language="javascript">
                $(function(){
                    $("#action_authorization").actionauthorization({section:"manage", page:"action_authorization", autoLoad: true, action_id:<?php echo $_GET['id']; ?>});
                });
            </script>
            <div id="action_authorization"></div>
            <?php displayAuditLogLink("action_authorization_logs", false) ?>
        </td>
    </tr>
</table>
<script>
    $(function(){
        $("table#update").find("th").css({"text-align":"left"})
    });
</script>