<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.ui.contract.js')); ?>
<?php display_menu_list('manage', 'update_actions'); ?>
<?php display_sub_menu_list('manage', 'update'); ?>
<?php
page_navigation_links(array('update_budget.php' => 'Update Budgets'));
display_flash('contract');
JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#contract").contract({updateBudget:true, section:"manage", page:"update"});
    });
</script>
<div id="contract"></div>