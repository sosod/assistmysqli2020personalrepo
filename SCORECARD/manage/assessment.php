<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<<?php include_js(array('jquery.ui.scorecard.js')); ?>
<?php display_menu_list('manage', 'assessment'); ?>
<?php page_navigation_links(array('assessment.php' => 'Assessment')); ?>
<?php display_flash('assessment'); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#scorecard").scorecard({contractAssessment:true, section:"manage", page:"assessment", autoLoad: false});
    });
</script>
<div id="scorecard"></div>