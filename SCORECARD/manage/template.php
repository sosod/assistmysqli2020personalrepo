<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.ui.contract.js')); ?>
<?php display_menu_list('manage'); ?>
<script language="javascript">
    $(function(){
        $("#contract").contract({viewAction:true, section:"manage", page:"view"});
    });
</script>
<div id="contract"></div>