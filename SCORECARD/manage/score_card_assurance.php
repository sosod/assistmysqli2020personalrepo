<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.contract.assurance.js', 'ajaxfileupload.js')); ?>
<?php display_menu_list('manage', 'assurance'); ?>
<?php display_sub_menu_list('manage', 'assurance', 'assurance'); ?>
<?php
$page_title = "Add Deliverable";
$columnsObj = new ScoreCardColumn();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$scoreCardObj = new ScoreCard();
$score_card    = $scoreCardObj->getScoreCardDetail($_GET['id']);

$score_card_list = $scoreCardObj->getScoreCardDetailList($score_card);
//debug($score_card);
$categoryObj            = new DeliverableCategory();
$deliverable_categories = $categoryObj->getList(true);

$deliverableObj         = new Deliverable();
$main_deliverables      = $deliverableObj->getList(true);

$deliveredWeightObj     = new Weight();
$delivered_weights      = $deliveredWeightObj->getList(true);

$scoreCardStatusObj = new ScoreCardStatus();
$score_card_statuses = $scoreCardStatusObj->getList(true);

page_navigation_links(array('assurances.php' => 'Assurances', 'score_card_assurance.php?id='.$score_card['id'] => 'Scorecard'));

display_flash('deliverable');
JSdisplayResultObj("");
?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">

                <tr>
                    <td colspan="2"><h3>Scorecard Details</h3></td>
                </tr>
                <?php foreach($score_card_list as $key => $score_card_detail): ?>
                    <tr>
                        <th><?php echo $score_card_detail['header']; ?></th>
                        <td><?php echo $score_card_detail['value']; ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <script language="javascript">
                $(function(){
                    $("#score_card_assurances").assurance({section:"manage", page:"assurance", score_card_id:<?php echo $_GET['id'] ?>});
                });
            </script>
            <div id="score_card_assurances"></div>
            <?php displayAuditLogLink("score_card_assurance_logs", false) ?>
        </td>
    </tr>
</table>