<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.ui.deliverable.js')); ?>
<?php display_menu_list('manage', 'assurance'); ?>
<?php display_sub_menu_list('manage', 'assurance', 'assurance_deliverables'); ?>
<?php page_navigation_links(array('assurance.php' => 'Assurance', 'assurance_deliverables.php' => 'Heading'));
display_flash('deliverable');
JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#deliverable_list").deliverable({deliverableAssurance: true, section: 'manage', 'page': 'assurance'});
    });
</script>
<div id="deliverable_list"></div>
