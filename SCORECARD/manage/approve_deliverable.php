<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.contract.approvedeliverable.js')); ?>
<?php display_menu_list('manage', 'approve'); ?>
<?php display_sub_menu_list('manage', 'approve', 'approve_deliverable'); ?>
<?php page_navigation_links(array('approve_deliverable.php' => 'Approve Deliverables')); ?>
<?php display_flash('activation'); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#contract").approvedeliverable({section: "manage"});
    });
</script>
<div id="contract"></div>