<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('action.js')); ?>
<?php display_menu_list('manage', 'view_action'); ?>
<?php display_sub_menu_list('manage', 'view'); ?>
<?php

$columnsObj = new ActionColumns();
$columns    = $columnsObj->getHeaders();

$actionObj  = new Action();
$action     = $actionObj->getActionDetail($_GET['id']);

$deliverableObj = new Deliverable();
$deliverable       = $deliverableObj->getDeliverableDetail($action['deliverable_id']);

$actionStatusObj = new ActionStatus();
$action_statuses = $actionStatusObj->getList(true);

$actionAuditLogObj = new ActionAuditLog();
$action_logs       = $actionAuditLogObj->getActionActivityLog($_GET['id']);
?>
<?php
page_navigation_links(array('view.php' => 'View', 'view_action.php?id='.$_GET['id'] => 'Question'));
display_flash('action');
JSdisplayResultObj("");
?>
<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2">
                        <input type="button" id="view_deliverable" name="view_deliverable" value="View Heafing" />
                        <input type="button" id="view_contract" name="view_contract" value="View Question" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><h4>Question Details</h4></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_id']; ?>:</th>
                    <td><?php echo $action['id']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_name']; ?>:</th>
                    <td><?php echo $action['action_name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_deliverable']; ?>:</th>
                    <td><?php echo $action['action_deliverable']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_owner']; ?>:</th>
                    <td><?php echo $action['action_owner']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_progress']; ?>:</th>
                    <td><?php echo $action['action_progress']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_status']; ?>:</th>
                    <td><?php echo $action['action_status']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['action_remind_on']; ?>:</th>
                    <td><?php echo $action['action_remind_on']; ?></td>
                </tr>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder">
            <table width="100%">
                <tr>
                    <th class="th2" colspan='4'>Activity Log</th>
                </tr>
                <tr>
                    <th>Date Logged</th>
                    <th>User</th>
                    <th>Change Log</th>
                    <th>Status</th>
                </tr>
                <?php if(isset($action_logs) && !empty($action_logs)): ?>
                    <?php foreach($action_logs as $q_index => $log): ?>
                        <tr>
                            <td align="center"><?php echo $log['date_loged']; ?></td>
                            <td><?php echo $log['user']; ?></td>
                            <td><?php echo $log['changeMessage']; ?></td>
                            <td><?php echo $log['status']; ?></td>
                        </tr>
                    <?php endforeach;  ?>
                <?php else:  ?>
                    <tr>
                        <td align="center" colspan="4">There are no logs for this deliverable</td>
                    </tr>
                <?php endif;  ?>
            </table>
            <input type="hidden" name="action_id" id="action_id" value="<?php echo $_REQUEST['id'] ?>"  />
            <input type="hidden" name="deliverable_id" id="deliverable_id" value="<?php echo $action['deliverable_id']; ?>"  />
            <input type="hidden" name="contract_id" id="contract_id" value="<?php echo $action['contract_id']; ?>"  />
        </td>
      </tr>
    </table>
<script>
    $(function(){
        $("table#update").find("th").css({"text-align":"left"})
    });
</script>