<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.ui.contract.js')); ?>
<?php display_menu_list('manage', 'update_actions'); ?>
<?php display_sub_menu_list('manage', 'update', 'update_contracts'); ?>
<?php
page_navigation_links(array('update_contracts.php' => 'Update Contracts'));
display_flash('contract');
JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#contract").contract({updateContract:true, section:"manage", page:"update", autoLoad: false});
    });
</script>
<div id="contract"></div>