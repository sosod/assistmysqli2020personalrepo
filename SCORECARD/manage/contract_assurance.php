<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.contract.assurance.js', 'ajaxfileupload.js')); ?>
<?php display_menu_list('manage', 'assurance'); ?>
<?php display_sub_menu_list('manage', 'assurance', 'assurance'); ?>
<?php
$page_title = "Add Deliverable";
$columnsObj = new ContractColumns();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$contractObj = new Contract();
$contract    = $contractObj->getContractDetail($_GET['id']);
//debug($contract);
$categoryObj            = new DeliverableCategory();
$deliverable_categories = $categoryObj->getList(true);

$deliverableObj         = new Deliverable();
$main_deliverables      = $deliverableObj->getList(true);

$deliveredWeightObj     = new DeliveredWeight();
$delivered_weights      = $deliveredWeightObj->getList(true);

$qualityWeightObj       = new QualityWeight();
$quality_weights        = $qualityWeightObj->getList(true);

$otherWeightObj         = new OtherWeight();
$other_weights          = $otherWeightObj->getList(true);

$contractStatusObj = new ContractStatus();
$contract_statuses = $contractStatusObj->getList(true);

page_navigation_links(array('assurances.php' => 'Assurances', 'contract_assurance.php?id='.$contract['id'] => 'Contract'));

display_flash('deliverable');
JSdisplayResultObj("");
?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2"><h4>Contract Details</h4></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_id']; ?>:</th>
                    <td><?php echo $contract['contract_id']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['reference_number']; ?>:</th>
                    <td><?php echo $contract['reference_number']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_name']; ?>:</th>
                    <td><?php echo $contract['name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_comment']; ?>:</th>
                    <td><?php echo $contract['comment']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_type']; ?>:</th>
                    <td><?php echo $contract['contract_type']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_category']; ?>:</th>
                    <td><?php echo $contract['contract_category']; ?></td>
                </tr>
                <!--                <tr>
                    <th><?php /*echo $columns['contract_supplier']; */?>:</th>
                    <td><?php /*echo $contract['contract_supplier']; */?></td>
                </tr>
                <tr>
                    <th><?php /*echo $columns['contract_budget']; */?>:</th>
                    <td><?php /*echo $contract['budget']; */?></td>
                </tr>-->
                <tr>
                    <th><?php echo $columns['payment_term']; ?>:</th>
                    <td><?php echo $contract['payment_term']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['payment_frequency']; ?>:</th>
                    <td><?php echo $contract['payment_frequency']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['risk_name']; ?>:</th>
                    <td><?php echo $contract['risk_name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_manager']; ?>:</th>
                    <td><?php echo $contract['contract_manager']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $columns['contract_authorisor']; ?>:</th>
                    <td><?php echo $contract['contract_authorisor']; ?></td>
                </tr>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <script language="javascript">
                $(function(){
                    $("#contract_assurances").assurance({section:"manage", page:"assurance", contract_id:<?php echo $_GET['id'] ?>});
                });
            </script>
            <div id="contract_assurances"></div>
            <?php displayAuditLogLink("contract_assurance_logs", false) ?>
        </td>
    </tr>
</table>