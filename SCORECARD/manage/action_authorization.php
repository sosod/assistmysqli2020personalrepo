<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.ui.action.js')); ?>
<?php display_menu_list('manage', 'authorization'); ?>
<?php display_sub_menu_list('manage', 'authorization', 'action_authorization'); ?>
<?php page_navigation_links(array('authorization.php' => 'Authorization', 'action_authorization.php' => 'Actions')); ?>
<script language="javascript">
    $(function(){
        $("#action_list").action({authorizeAction: true, section:'manage', page:"authorization", autoLoad: false});
    });
</script>
<div id="action_list"></div>