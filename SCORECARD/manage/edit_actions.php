<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.ui.action.js')); ?>
<?php display_menu_list('manage', 'edit_contracts'); ?>
<?php display_sub_menu_list('manage', 'edit', 'edit_actions'); ?>
<?php page_navigation_links(array('edit_actions.php' => 'Edit Actions')); ?>
<script language="javascript">
    $(function(){
        $("#action_list").action({editAction: true, section:'manage', page:"edit", autoLoad: false});
    });
</script>
<div id="action_list"></div>