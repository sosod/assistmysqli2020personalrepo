<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.contract.assessment.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('manage', 'assessment'); ?>
<?php page_navigation_links(array('assessment.php' => 'Assessment', 'contract_assessment.php?id='.$_GET['id'] => 'Contract Assessment')); ?>
<?php display_flash('assessment'); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#contract").assessment({assessment:true, section:"manage", page:"assessment", autoLoad: true, assessDeliverable: true, contract_id:<?php echo $_GET['id']; ?>});
    });
</script>
<div id="contract"></div>
<input type="hidden" class="logid" name="contract_id" id="contract_id" value="<?php echo $_GET['id']; ?>"  />
<?php displayAuditLogLink("contract_assessment_logs" , false); ?>