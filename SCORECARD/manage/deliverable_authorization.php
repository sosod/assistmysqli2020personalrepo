<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.ui.deliverable.js')); ?>
<?php display_menu_list('manage', 'authorization'); ?>
<?php display_sub_menu_list('manage', 'authorization', 'deliverable_authorization'); ?>
<?php page_navigation_links(array('authorization.php' => 'Authorization', 'deliverable_authorization.php' => 'Deliverables'));
display_flash('deliverable');
JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#deliverable_list").deliverable({authorizeDeliverable: true, section: 'manage', 'page': 'assurance'});
    });
</script>
<div id="deliverable_list"></div>
