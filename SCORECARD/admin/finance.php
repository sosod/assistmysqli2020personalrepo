<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.contract.adjustments.js', 'ajaxfileupload.js')); ?>
<?php display_menu_list('admin', 'finance'); ?>
<?php display_sub_menu_list('admin', 'finance', 'finance_budget'); ?>
<?php page_navigation_links(array('finance.php' => 'Finance')); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#contract_adjustments").adjustments({addNew:true, section:"admin", page:"view"});
    });
</script>
<div id="contract_adjustments"></div>
<?php displayAuditLogLink("contract_budget_adjustment_logs", false) ?>