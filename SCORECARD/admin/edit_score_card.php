<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('score_card.js', 'ajaxfileupload.js')); ?>
<?php display_menu_list('admin', 'edit_score_cards'); ?>
<?php display_sub_menu_list('admin', 'scorecard'); ?>
<?php
$scoreCardObj = new ScoreCard();
$score_card    = $scoreCardObj->getScoreCardDetail($_GET['id']);

$columnsObj = new Naming();
$columns    = $columnsObj->getHeaders();

$typeObj        = new ScoreCardType();
$score_card_types = $typeObj->getList(true);

$categoryObj    = new ScoreCardCategory();
$score_card_categories = $categoryObj->getList(true);

$statusObj         = new ScoreCardStatus();
$score_card_statuses = $statusObj->getList(true);

$userObj = new User();
$score_card_users = $userObj->getList(true);

$scoreCardObj = new ScoreCard();
$score_card_templates = $scoreCardObj->getList(true);

$financialYearObj = new FinancialYear();
$financial_years  = $financialYearObj->getList(true);

$ownerObj  = new ScoreCardOwner();
$score_card_owners = $ownerObj->getList(true);

$documentObj                  = new ScoreCardDocument();
$score_card_documents           = $documentObj->getAll($score_card['id']);
//debug($score_card_documents);
page_navigation_links(array('edit_score_cards.php' => 'Edit Scorecards', 'edit_score_card.php?id='.$_GET['id'] => 'Edit Scorecard'));
display_flash('contract');
JSdisplayResultObj("");
?>
<?php JSdisplayResultObj(""); ?>
<form method="post" id="score_card-form" enctype="multipart/form-data">
    <table id="score_card_table" width="50%">
        <tr valign="top">
            <th>
                <?php echo label_for('score_card_id', $columns['score_card_id']); ?>:
            </th>
            <td>
                <?php echo label_for('score_card_id', $score_card['score_card_id']); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('financial_year', $columns['financial_year']); ?>:
            </th>
            <td>
                <?php echo select_tag('score_card[financial_year_id]', options_for_select($financial_years, $score_card['financial_year_id']), array('id' => 'financial_year_id')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('reference_number', $columns['reference_number']); ?>:
            </th>
            <td>
                <?php echo input_tag('score_card[reference_number]', $score_card['reference_number'], array('id' => 'reference_number')) ?>
            </td>
        </tr>
        <tr>
            <th valign="top">
                <?php echo label_for('score_card_name', $columns['score_card_name']); ?>:
            </th>
            <td>
                <?php echo textarea_tag('score_card[name]', $score_card['name'], array('id' => 'name')); ?>
            </td>
        </tr>

        <tr valign="top">
            <th>
                <?php echo label_for('comment', $columns['comment']); ?>:
            </th>
            <td>
                <?php echo textarea_tag('score_card[comment]', $score_card['comment'], array('id' => 'comment')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('score_card_type', $columns['score_card_type']); ?>:
            </th>
            <td>
                <?php echo select_tag('score_card[type_id]', options_for_select($score_card_types, $score_card['type_id']), array('id' => 'type_id')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('score_card_category', $columns['score_card_category']); ?>:
            </th>
            <td>
                <?php echo select_tag('score_card[category_id]', options_for_select($score_card_categories, $score_card['category_id']), array('id' => 'category_id')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('score_card_manager', $columns['score_card_manager']); ?>:
            </th>
            <td>
                <?php echo select_tag('score_card[manager_id]', options_for_select($score_card_users, $score_card['manager_id']), array('id' => 'manager_id')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('score_card_owner', $columns['score_card_owner']); ?>:
            </th>
            <td>
                <?php echo select_tag('score_card[owner_id]', options_for_select($score_card_owners, $score_card['owner_id']), array('id' => 'owner_id')); ?>
            </td>
        </tr>
        <tr class="scoreCardDefaults">
            <th><?php echo label_for('score_card_default_has_sub_deliverable', $columns['has_sub_deliverable']); ?>:</th>
            <td>
                <select name="score_card[has_sub_deliverable]" id="contract_has_sub_deliverable">
                    <option value="0" <?php echo ($score_card['has_sub_deliverable'] ? '' : "selected='selected'") ?>>No</option>
                    <option value="1" <?php echo ($score_card['has_sub_deliverable'] ? "selected='selected'" : '') ?>>Yes</option>
                </select>
            </td>
        </tr>
        <tr class="scoreCardDefaults <?php echo ($score_card['is_score_card_assessed'] ? 'edit-assessed' : 'assessed'); ?>">
            <th><?php echo label_for('score_card_default_weight_assigned', $columns['weight_assigned']); ?>:</th>
            <td>
                <select name="score_card[weight_assigned]" id="score_card_weight_assigned">
                    <option value="0" <?php echo ($score_card['weight_assigned'] ? '' : "selected='selected'") ?>>No</option>
                    <option value="1" <?php echo ($score_card['weight_assigned'] ? "selected='selected'" : '') ?>>Yes</option>
                </select>
            </td>
        </tr>
        <tr>
            <th><?php echo $columns['attachment']; ?>:</th>
            <td>
                <?php Attachment::displayAttachmentList($score_card_documents, $type = "scorecard"); ?>
                <div id="file_upload"></div>
                <input type="file"  name="score_card_document" id="contact_attachment_document"  class="attachment" onChange="attachScoreCardDocuments(this)" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="submit" id="edit_score_card" name="edit_score_card" value="Save Changes" class="edit_score_card isubmit" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="hidden" id="id" name="score_card[id]" value="<?php echo $score_card['id']; ?>" />
                <input type="hidden" name="log_id" id="score_card_id" value="<?php echo $score_card['id']?>" class="logid" />
            </td>
        </tr>
        <tr>
            <td class="noborder"><?php displayGoBack("",""); ?></td>
            <td class="noborder"><?php displayAuditLogLink("score_card_edit_logs", false) ?></td>
        </tr>
    </table>
</form>