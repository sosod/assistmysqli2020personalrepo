<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.ui.deliverable.js')); ?>
<?php display_menu_list('admin', 'edit_score_cards'); ?>
<?php display_sub_menu_list('admin', 'edit', 'edit_deliverables'); ?>
<?php page_navigation_links(array('edit_deliverables.php' => 'Edit Headings'));
display_flash('deliverable');
JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#deliverable_list").deliverable({editDeliverable: true, page: 'edit',section: 'admin' });
    });
</script>
<div id="deliverable_list"></div>
