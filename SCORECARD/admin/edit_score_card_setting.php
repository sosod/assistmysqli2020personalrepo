<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('score_card.js', 'ajaxfileupload.js')); ?>
<?php display_menu_list('admin', 'edit_score_card_settings'); ?>
<?php display_sub_menu_list('admin', 'edit_score_card_settings'); ?>
<?php
page_navigation_links(array('edit_score_card_settings.php' => 'Edit Scorecard Settings', 'edit_score_card_setting.php?id='.$_GET['id'] => 'Edit Scorecard Setting'));
display_flash('deliverable');
JSdisplayResultObj("");


$columnsObj = new ScoreCardColumn();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$scoreCardObj = new ScoreCard();
$score_card    = $scoreCardObj->getScoreCardWithSetup($_GET['id']);

$scoreCardAuditLogObj = new ScoreCardAuditLog();
$score_card_logs       = $scoreCardAuditLogObj->getScoreCardActivityLog($_GET['id']);

$score_card_list = $scoreCardObj->getScoreCardDetailList($score_card);

$financialYearObj = new FinancialYear();
$financial_years  = $financialYearObj->getList(true);

$frequency_types  = array('once_off' => 'Once Off', 'recurring' => 'Recurring');

$userObj = new User();
$users   = $userObj->getList();

$scoreCardOwnerObj = new ScoreCardOwner();
$directorates      = $scoreCardOwnerObj->getList();

$scoreCardObj = new ScoreCard();
$score_cards  = $scoreCardObj->getScoreCardsWithSetupByScoreCardIdList($score_card['template_id']);
//debug($score_cards);
?>

<!--<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2"><h3>Scorecard Details</h3></td>
                </tr>
                <?php /*foreach($score_card_list as $key => $score_card_detail): */?>
                    <tr>
                        <th><?php /*echo $score_card_detail['header']; */?>:</th>
                        <td><?php /*echo $score_card_detail['value']; */?></td>
                    </tr>
                <?php /*endforeach; */?>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php /*displayGoBack("",""); */?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">-->
            <form method="post" name="edit-score-card-form" id="edit-score-card-form">
                <table>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('financial_year', $columns['financial_year']); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('score_card[financial_year_id]', options_for_select($financial_years, $score_card['financial_year_id']), array('id' => 'financial_year_id')); ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('score_card_ref', $columns['score_card_ref']); ?>:
                        </th>
                        <td>
                            <?php echo input_tag('score_card[name]', $score_card['setup_reference'], array('id' => 'setup_reference')) ?>
                        </td>
                    </tr>
                    <tr>
                        <th valign="top">
                            <?php echo label_for('score_card_instruction_to_user', $columns['score_card_instruction_to_user']); ?>:
                        </th>
                        <td>
                            <?php echo textarea_tag('score_card[description]', $score_card['setup_description'], array('id' => 'setup_description')); ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('link_to', $columns['link_to']); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('score_card[link_to]', options_for_select($score_cards, $score_card['link_to'], array('include_custom' => 'Do Not Link' )), array('id' => 'link_to')); ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('score_card_frequency', $columns['score_card_frequency']); ?>:
                        </th>
                        <td>
                            <table>
                                <tr>
                                    <th class="th2">Start Date</th>
                                    <td><?php echo input_tag('score_card[start_date]', $score_card['start_date'], array('id' => 'start_date', 'class' => 'datepicker', 'readonly' => 'readonly')) ?></td>
                                </tr>
                                <tr>
                                    <th class="th2">End Date</th>
                                    <td><?php echo input_tag('score_card[end_date]', $score_card['end_date'], array('id' => 'end_date', 'class' => 'datepicker', 'readonly' => 'readonly')) ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr valign="top">
                        <th>
                            <?php echo label_for('movement_tracked', $columns['movement_tracked']); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('score_card[is_movement_tracked]', options_for_select(array(0 => 'No', 1 => 'Yes'), $score_card['is_movement_tracked']), array('id' => 'is_movement_tracked')); ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('allocate_to', $columns['allocate_to']); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('score_card[allocate_to]', options_for_select(array('all' => 'All Recipients Update', 'allocated_criteria' => 'Recipients Update Allocated Criteria'), $score_card['allocate_to']), array('id' => 'allocate_to')); ?>
                        </td>
                    </tr>
<!--                    <tr valign="top" class="completion-rule all-users-list">
                        <th>
                            <?php /*echo label_for('users', 'Users'); */?>:
                        </th>
                        <td>
                            <?php /*echo select_tag('score_card[recipient_user]', options_for_select($users, null, array('include_please_select' => false)), array('id' => 'score_card_recipient_user', 'multiple' => true)); */?>
                        </td>
                    </tr>
                    <tr valign="top"  class="completion-rule allocate">
                        <th>
                            <?php /*echo label_for('completion_rule_allocate', 'Allocated to'); */?>:
                        </th>
                        <td>
                            <select name="score_card[completion_rule_allocate_to]" id="completion_rule_allocate_to">
                                <option value="">--please select--</option>
                                <option value="directorate">Directorate/Sub-Directorate</option>
                                <option value="action_owner">Action Owner</option>
                            </select>
                        </td>
                    </tr>
                    <tr valign="top" class="completion-rule allocate-rule allocate-users-list">
                        <th>
                            <?php /*echo label_for('users', 'Users'); */?>:
                        </th>
                        <td>
                            <?php /*echo select_tag('score_card[allocate_to_user]', options_for_select($users, null, array('include_please_select' => false)), array('id' => 'all_users', 'multiple' => true)); */?>
                        </td>
                    </tr>
                    <tr valign="top" class="completion-rule allocate-rule allocated-directorate-list">
                        <th>
                            <?php /*echo label_for('directorate', 'Directorate/Sub-Directorate'); */?>:
                        </th>
                        <td>
                            <?php /*echo select_tag('score_card[directorate_id]', options_for_select($directorates, null ), array('id' => 'directorate_id')); */?>
                        </td>
                    </tr>-->
                    <tr>
                        <th valign="top">
                            <?php echo label_for('use_rating_comment', $columns['use_rating_comment']); ?>:
                        </th>
                        <td>
                            <?php echo checkbox_tag('score_card[use_rating_comment]', $score_card['use_rating_comment'], array('id' => 'use_rating_comment')); ?>
                        </td>
                    </tr>
                    <tr>
                        <th valign="top">Use Corrective Action:
                            <?php //echo label_for('use_corrective_action', $columns['use_corrective_action']); ?>
                        </th>
                        <td>
                            <?php echo checkbox_tag('score_card[use_corrective_action]', $score_card['use_corrective_action'], (bool)$score_card['use_corrective_action'], array('id' => 'use_corrective_action', 'multiple' => true)); ?>
                        </td>
                    </tr>
                    <tr>
                        <th valign="top">
                            <?php echo label_for('are_corrective_action_owner_identified', $columns['are_corrective_action_owner_identified']); ?>:
                        </th>
                        <td>
                            <?php echo checkbox_tag('score_card[are_corrective_action_owner_identified]', $score_card['is_corrective_action_owner_identified'], array('id' => 'is_corrective_action_owner_identified')); ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('score_card_rating_type', 'Rating table to be Used to display overall rating'); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('score_card[rating_type]', options_for_select(RatingScale::getRatingScaleTypesList(), $score_card['rating_type']), array('id' => 'rating_type')); ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" id="save_score_card_changes" name="save_score_card_changes" value="Save Changes" class="activate_score_card isubmit" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="hidden" id="setup_id" name="score_card[id]" value="<?php echo $score_card['setup_id']; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="noborder"><?php displayGoBack("",""); ?></td>
                        <td class="noborder"><?php displayAuditLogLink("score_card_setup_logs", false) ?></td>
                    </tr>
                </table>
            </form>
<!--        </td>
    </tr>
</table>-->