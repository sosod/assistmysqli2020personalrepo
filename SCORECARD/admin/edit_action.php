<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('action.js')); ?>
<?php display_menu_list('admin', 'edit_contracts'); ?>
<?php display_sub_menu_list('admin', 'edit', 'edit_actions'); ?>
<?php

$columnsObj = new ActionColumns();
$columns    = $columnsObj->getHeaders();

$actionObj  = new Action();
$action     = $actionObj->getActionDetail($_GET['id']);

$deliverableObj = new Deliverable();
$deliverable       = $deliverableObj->getDeliverableDetail($action['deliverable_id']);

$userObj = new User();
$action_owners = $userObj->getList(true);
?>
<?php
page_navigation_links(array('edit_actions.php' => 'Edit Questions', 'edit_action.php?id='.$_GET['id'] => 'Edit Question'));
display_flash('action');
JSdisplayResultObj("");
?>

<form  method="post"  name="action-form" id="action-form" enctype="multipart/form-data">
    <table>
        <tr>
            <td colspan="2"><h4>Edit Question</h4></td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('action_name', $columns['action_name']); ?>
            </th>
            <td>
                <?php echo textarea_tag('action[name]', $action['action_name'], array('id' => 'name')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('action_deliverable', $columns['action_deliverable']); ?>
            </th>
            <td>
                <?php echo textarea_tag('action[deliverable]', $action['deliverable'], array('id' => 'deliverable')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('action_rating_scale_type', $columns['rating_scale_type']); ?>
            </th>
            <td>
                <?php echo select_tag('action[rating_scale_type]', options_for_select(Ratingscale::getRatingScaleTypesList(), $action['rating_scale_type']), array('id' => 'rating_scale_type')) ?>
            </td>
        </tr>
        <tr>
            <th></th>
            <td>
                <input type="submit" name="save_edit_changes" id="save_edit_changes" value="Save Changes" class="save_action isubmit" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="hidden" name="action[id]" id="action_id" value="<?php echo $_GET['id']?>" />
                <input type="hidden" name="action[deliverable_id]" id="deliverable_id" value="<?php echo $action['deliverable_id']?>" />
                <input type="hidden" name="log_id" id="action_id" value="<?php echo $action['id']?>" class="logid" />
            </td>
        </tr>
        <tr>
            <td class="noborder"><?php displayGoBack("",""); ?></td>
            <td class="noborder"><?php displayAuditLogLink("action_edit_logs", false) ?></td>
        </tr>
    </table>
</form>