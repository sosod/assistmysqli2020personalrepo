<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.scorecard.setup.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('admin', 'admin_score_cards'); ?>
<?php page_navigation_links(array('admin_score_cards.php' => 'Trigger Scorecard')); ?>
<?php

$columnsObj = new ScoreCardColumn();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();


$financialYearObj = new FinancialYear();
$financial_years  = $financialYearObj->getList(true);

$frequency_types  = array('once_off' => 'Once Off', 'recurring' => 'Recurring');

$userObj = new User();
$users   = $userObj->getList();

$scoreCardOwnerObj = new ScoreCardOwner();
$directorates      = $scoreCardOwnerObj->getList();


$financialYearObj = new FinancialYear();
$financial_years  = $financialYearObj->getList(true);

JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#score_card").setup({activate:true, section:"admin", page:"setup_score_card"});
    });
</script>
<div id="score_card"></div>
<?php
$print_str = "<p class='ui-state ui-state-widget ui-state-info score_card_message' style='width:auto; padding: 8px 35px 8px 14px;' >";
$print_str .= "<span class='ui-icon ui-icon-info ui-icon-info' style='float:left'></span>";
$print_str .= "<span>Please select the year and scorecard to activate</span>";
$print_str .= "</p>";
echo $print_str;
?>
