<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.scorecard.completequestion.js')); ?>
<?php display_menu_list('admin', 'update_actions'); ?>
<?php display_sub_menu_list('admin', 'update', 'update_actions'); ?>
<?php page_navigation_links(array('update_actions.php' => 'Update Questions')); ?>
<?php display_flash('action'); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        //$("#action_list").action({updateAction: true, section:'admin', page: "update", autoLoad: false});
        $("#question_list").completequestion({updateAssessAction: true, section:'admin', page: "update", autoLoad: false});
    });
</script>
<div id="question_list"></div>