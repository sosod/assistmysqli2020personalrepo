<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('contract.js', 'ajaxfileupload.js')); ?>
<?php display_menu_list('admin', 'edit_contract'); ?>
<?php display_sub_menu_list('admin', 'edit'); ?>
<?php
$contractObj = new Contract();
$contract    = $contractObj->getContractDetail($_GET['id']);

$columnsObj = new Naming();
$columns    = $columnsObj->getHeaders();

$typeObj        = new ContractType();
$contract_types = $typeObj->getList(true);

$categoryObj    = new ContractCategory();
$contract_categories = $categoryObj->getList(true);

$statusObj         = new ContractStatus();
$contract_statuses = $statusObj->getList(true);

$supplierObj = new Supplier();
$contract_suppliers = $supplierObj->getList(true);

$userObj = new User();
$contract_users = $userObj->getList(true);

$contractObj = new Contract();
$contract_templates = $contractObj->getList(true);

$financialYearObj = new FinancialYear();
$financial_years  = $financialYearObj->getList(true);

$paymentTermObj   = new PaymentTerm();
$payment_terms    = $paymentTermObj->getList(true);

$paymentFrequencyObj   = new PaymentFrequency();
$payment_frequencies    = $paymentFrequencyObj->getList(true);

$ownerObj  = new ContractOwner();
$contract_owners = $ownerObj->getList(true);

$assessmentFrequencyObj = new AssessmentFrequency();
$contract_assessment_frequencies = $assessmentFrequencyObj->getList(true);

$deliverableStatusObj = new DeliverableStatus();
$assessment_deliverable_statues  = $deliverableStatusObj->getStatusAssessedList(true);

$supplierBudgetObj      = new ContractSupplierBudget();
$supplier_budgets       = $supplierBudgetObj->getAll($contract['id']);

$contractDeliverableStatusObj = new ContractDeliverableStatus();
$contract_deliverable_statues = $contractDeliverableStatusObj->getAllStatusByContractId($contract['id']);

$documentObj                  = new ContractDocument();
$contract_documents           = $documentObj->getAll($contract['id']);

page_navigation_links(array('edit_contracts.php' => 'Edit Contracts', 'edit_contract.php?id='.$_GET['id'] => 'Edit Contract'));
display_flash('contract');
JSdisplayResultObj("");
?>
<?php JSdisplayResultObj(""); ?>
<form method="post" id="contract-form" enctype="multipart/form-data">
    <table id="contract_table" width="50%">
        <tr valign="top">
            <th>
                <?php echo label_for('contract_id', $columns['contract_id']); ?>:
            </th>
            <td>
                <?php echo label_for('contract_id', $contract['contract_id']); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('financial_year', $columns['financial_year']); ?>:
            </th>
            <td>
                <?php echo select_tag('contract[financial_year_id]', options_for_select($financial_years, $contract['financial_year_id']), array('id' => 'financial_year_id')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('reference_number', $columns['reference_number']); ?>:
            </th>
            <td>
                <?php echo input_tag('contract[reference_number]', $contract['reference_number'], array('id' => 'reference_number')) ?>
            </td>
        </tr>
        <tr>
            <th valign="top">
                <?php echo label_for('contract_name', $columns['contract_name']); ?>:
            </th>
            <td>
                <?php echo textarea_tag('contract[name]', $contract['name'], array('id' => 'name')); ?>
            </td>
        </tr>

        <tr valign="top">
            <th>
                <?php echo label_for('contract_comment', $columns['contract_comment']); ?>:
            </th>
            <td>
                <?php echo textarea_tag('contract[comment]', $contract['comment'], array('id' => 'comment')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('contract_type', $columns['contract_type']); ?>:
            </th>
            <td>
                <?php echo select_tag('contract[type_id]', options_for_select($contract_types, $contract['type_id']), array('id' => 'type_id')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('contract_category', $columns['contract_category']); ?>:
            </th>
            <td>
                <?php echo select_tag('contract[category_id]', options_for_select($contract_categories, $contract['category_id']), array('id' => 'category_id')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('risk_name', $columns['risk_name']); ?>
            </th>
            <td>
                <?php echo input_tag('contract[risk_name]', $contract['risk_name'], array()) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('risk_reference', $columns['risk_reference']); ?>
            </th>
            <td>
                <?php echo input_tag('contract[risk_reference]', $contract['risk_reference'], array()) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('account_code', $columns['account_code']); ?>
            </th>
            <td>
                <?php echo input_tag('contract[account_code]', $contract['account_code'], array()) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('signature_of_tender', $columns['signature_of_tender']); ?>
            </th>
            <td>
                <?php echo input_tag('contract[signature_of_tender]', $contract['signature_of_tender'], array('class' => 'datepicker', 'readonly' => 'readonly')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('signature_of_sla', $columns['signature_of_sla']); ?>
            </th>
            <td>
                <?php echo input_tag('contract[signature_of_sla]', $contract['signature_of_sla'], array('class' => 'datepicker', 'readonly' => 'readonly')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('completion_date', $columns['completion_date']); ?>
            </th>
            <td>
                <?php echo input_tag('contract[completion_date]', $contract['completion_date'], array('class' => 'datepicker', 'readonly' => 'readonly')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('contract_manager', $columns['contract_manager']); ?>
            </th>
            <td>
                <?php echo select_tag('contract[manager_id]', options_for_select($contract_users, $contract['manager_id']), array('id' => 'manager_id')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('contract_owner', $columns['contract_owner']); ?>
            </th>
            <td>
                <?php echo select_tag('contract[owner_id]', options_for_select($contract_owners, $contract['owner_id']), array('id' => 'owner_id')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('contract_authorisor', $columns['contract_authorisor']); ?>
            </th>
            <td>
                <?php echo select_tag('contract[authorisor_id]', options_for_select($contract_users, $contract['authorisor_id']), array('id' => 'authorisor_id')); ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('contract_template', $columns['contract_template']); ?>
            </th>
            <td>
                <?php echo select_tag('contract[template_id]', options_for_select($contract_templates, $contract['template_id']), array('id' => 'template_id')); ?>
            </td>
        </tr>

        <tr class="contractDefaults">
            <th><?php echo label_for('contract_default_has_deliverable', $columns['has_deliverable']); ?>:</th>
            <td>
                <select name="contract[has_deliverable]" id="contract_has_deliverable">
                    <option value="0" <?php echo ($contract['has_deliverable'] ? '' : "selected='selected'") ?>>No</option>
                    <option value="1" <?php echo ($contract['has_deliverable'] ? "selected='selected'" : '') ?>>Yes</option>
                </select>
            </td>
        </tr>
        <tr class="contractDefaults">
            <th><?php echo label_for('contract_default_has_sub_deliverable', $columns['has_sub_deliverable']); ?>:</th>
            <td>
                <select name="contract[has_sub_deliverable]" id="contract_has_sub_deliverable">
                    <option value="0" <?php echo ($contract['has_sub_deliverable'] ? '' : "selected='selected'") ?>>No</option>
                    <option value="1" <?php echo ($contract['has_sub_deliverable'] ? "selected='selected'" : '') ?>>Yes</option>
                </select>
            </td>
        </tr>
        <tr class="contractAssessment">
            <th><?php echo label_for('contract_default_is_contract_assessed', $columns['is_contract_assessed']); ?>:</th>
            <td>
                <select name="contract[is_contract_assessed]" id="contract_is_contract_assessed">
                    <option value="0"  <?php echo ($contract['is_contract_assessed'] ? '' : "selected='selected'") ?>>No</option>
                    <option value="1" <?php echo ($contract['is_contract_assessed'] ? "selected='selected'" : '') ?>>Yes</option>
                </select>
            </td>
        </tr>
        <tr class="delStatus <?php echo ($contract['is_contract_assessed'] ? 'edit-assessed' : 'assessed'); ?>" >
            <th><?php echo label_for('contract_deliverable_status_assessed', $columns['deliverable_status_assessed']); ?>:</th>
            <td>
                <table width="100%">
                    <?php foreach($assessment_deliverable_statues as $status_id => $deliverable_statues): ?>
                        <tr>
                            <th class="th2"><?php echo $deliverable_statues ?></th>
                            <td>
                                <select name="contract[deliverable_status][<?php echo $status_id; ?>]">
                                    <option value="0" <?php echo ($contract_deliverable_statues[$status_id] == 0 ? '' : "selected='selected'") ?> >No</option>
                                    <option value="1" <?php echo ($contract_deliverable_statues[$status_id] == 1 ? "selected='selected'" : '') ?>>Yes</option>
                                </select>
                                <?php //echo input_hidden_tag("contract[deliverable_status][".$status_id."]", $status_id ); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </td>
        </tr>
        <tr class="<?php echo ($contract['is_contract_assessed'] ? 'edit-assessed' : 'assessed'); ?>">
            <th><?php echo label_for('contract_default_delivered', $columns['criteria_to_assess']); ?></th>
            <td>
                <table width="100%">
                    <tr class="contractDefaults  <?php echo ($contract['is_contract_assessed'] ? '' : 'assessed'); ?> quantitative">
                        <th class="th2"><?php echo label_for('contract_default_is_assessed_quantitative', $columns['delivered_weight']); ?>:</th>
                        <td>
                            <select name="contract[is_assessed_quantitative]" id="contract_is_assessed_quantitative">
                                <option value="0" <?php echo ($contract['is_assessed_quantitative'] ? '' : "selected='selected'") ?>>No</option>
                                <option value="1" <?php echo ($contract['is_assessed_quantitative'] ? "selected='selected'" : '') ?>>Yes</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="contractDefaults <?php echo ($contract['is_contract_assessed'] ? '' : 'assessed'); ?>  qualitative">
                        <th class="th2"><?php echo label_for('contract_default_is_assessed_qualitative', $columns['quality_weight']); ?>:</th>
                        <td>
                            <select name="contract[is_assessed_qualitative]" id="contract_is_assessed_qualitative">
                                <option value="0" <?php echo ($contract['is_assessed_qualitative'] ? '' : "selected='selected'") ?>>No</option>
                                <option value="1" <?php echo ($contract['is_assessed_qualitative'] ? "selected='selected'" : '') ?>>Yes</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="contractDefaults <?php echo ($contract['is_contract_assessed'] ? '' : 'assessed'); ?> other">
                        <th class="th2"><?php echo label_for('contract_default_is_assess_other', $columns['other_weight']); ?>:</th>
                        <td>
                            <select name="contract[is_assessed_other]" id="contract_is_assessed_other">
                                <option value="0" <?php echo ($contract['is_assessed_other'] ? '' : "selected='selected'") ?>>No</option>
                                <option value="1" <?php echo ($contract['is_assessed_other'] ? "selected='selected'" : "") ?>>Yes</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="contractDefaults criteria other-criteria" style="display:<?php echo ($contract['is_assessed_other'] ? "table-row" : "none") ?>" >
                        <th class="th2"><?php echo label_for('contract_default_other_category', $columns['other_assessed_category']); ?>:</th>
                        <td>
                            <?php echo input_tag('contract[other_assessed_category]', $contract['other_assessed_category']); ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="contractDefaults <?php echo ($contract['is_contract_assessed'] ? 'edit-assessed' : 'assessed'); ?>">
            <th><?php echo label_for('contract_default_weight_assigned', $columns['weight_assigned']); ?>:</th>
            <td>
                <select name="contract[weight_assigned]" id="contract_weight_assigned">
                    <option value="0" <?php echo ($contract['weight_assigned'] ? '' : "selected='selected'") ?>>No</option>
                    <option value="1" <?php echo ($contract['weight_assigned'] ? "selected='selected'" : '') ?>>Yes</option>
                </select>
            </td>
        </tr>
        <!--        <tr class="contractDefaults <?php /*echo ($contract['weight_assigned'] ? '' : 'weight'); */?> weights delivered">
                    <th><?php /*echo label_for('contract_default_delivered', 'Delivered ?:'); */?></th>
                    <td>
                        <select name="contract[delivered_weight]" id="contract_delivered_weight_assigned">
                            <option value="0" <?php /*echo ($contract['delivered_weight'] ? '' : "selected='selected'"); */?>>No</option>
                            <option value="1" <?php /*echo ($contract['delivered_weight'] ? "selected='selected'" : ''); */?>>Yes</option>
                        </select>
                    </td>
                </tr>
                <tr class="contractDefaults  <?php /*echo ($contract['weight_assigned'] ? '' : 'weight'); */?> weights quality">
                    <th><?php /*echo label_for('contract_default_quality', 'Quality ?:'); */?></th>
                    <td>
                        <select name="contract[quality_weight]" id="contract_quality_weight">
                            <option value="0" <?php /*echo ($contract['quality_weight'] ? '' : "selected='selected'"); */?>>No</option>
                            <option value="1" <?php /*echo ($contract['quality_weight'] ? "selected='selected'" : ''); */?>>Yes</option>
                        </select>
                    </td>
                </tr>
                <tr class="contractDefaults other <?php /*echo ($contract['weight_assigned'] ? '' : 'weight'); */?> weights">
                    <th><?php /*echo label_for('contract_default_other', 'Other ?:'); */?></th>
                    <td>
                        <select name="contract[other_weight]" id="other_weight">
                            <option value="0" <?php /*echo ($contract['other_weight'] ? '' : "selected='selected'"); */?>>No</option>
                            <option value="1" <?php /*echo ($contract['other_weight'] ? "selected='selected'" : ''); */?>>Yes</option>
                        </select>
                    </td>
                </tr>
                <tr class="contractDefaults weights <?php /*echo ($contract['other_weight'] ? '' : 'other-weight category'); */?>">
                    <th><?php /*echo label_for('contract_default_other_category', 'Other Category ?:'); */?></th>
                    <td>
                        <?php /*echo input_tag('contract[other_category]', $contract['other_category']) */?>
                    </td>
                </tr>-->
        <tr class="<?php echo ($contract['is_contract_assessed'] ? 'edit-assessed' : 'assessed'); ?>">
            <th><?php echo $columns['contract_assessment_frequency']; ?>:
                <!--<em>
                 If weekly , select day of week and time to be send<br />
                 If monthly , select day of month and time to be send<br />
                 If Quartely , select day and time to be send etc.
                 </em> -->
            </th>
            <td>
                <?php echo select_tag('contract[assessment_frequency_id]', options_for_select($contract_assessment_frequencies, $contract['assessment_frequency_id']), array('id' => 'assessment_frequency_id')); ?>
            </td>
        </tr>
        <tr id="assessment_frequency_date_picker" <?php echo !empty($contract['assessment_frequency_id']) ? "" : "style='display:none;'";  ?> class="assessment_frequency <?php echo ($contract['is_contract_assessed'] ? 'edit-assessed' : 'assessed'); ?>">
            <th><?php echo $columns['contract_assessment_frequency_start_date']; ?>:</th>
            <td>
                <?php echo input_tag('contract[assessment_frequency_date]', $contract['assessment_frequency_date'], array('class' => 'datepicker', 'readonly' => 'readonly')) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $columns['attach_a_document']; ?>:</th>
            <td>
                <?php Attachment::displayAttachmentList($contract_documents, $type = "contract"); ?>
                <div id="file_upload"></div>
                <input type="file"  name="contract_document" id="contact_attachment_document"  class="attachment" onChange="attachContractDocuments(this)" />
            </td>
        </tr>
        <tr>
            <td><?php displayGoBack("",""); ?></td>
            <td>
                <input type="submit" id="edit_contract" name="edit_contract" value="Save Changes" class="save_contract isubmit" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="hidden" id="id" name="contract[id]" value="<?php echo $contract['id']; ?>" />
            </td>
        </tr>
    </table>
</form>