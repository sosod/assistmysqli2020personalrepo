<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.ui.contract.js')); ?>
<?php display_menu_list('admin', 'template'); ?>
<?php display_sub_menu_list('admin', 'template'); ?>
<?php page_navigation_links(array('index.php' => 'Admin', 'template.php' => 'Template')); ?>
<script language="javascript">
    $(function(){
        $("#contract").contract({makeTemplate:true, section:"admin", page:"template", autoLoad: false});
    });
</script>
<div id="contract"></div>