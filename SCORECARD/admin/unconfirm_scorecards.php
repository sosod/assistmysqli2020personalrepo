<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.scorecard.completequestion.js')); ?>
<?php display_menu_list('admin', 'unconfirm_scorecards'); ?>
<?php display_sub_menu_list('admin', 'unconfirm_scorecards', 'unconfirm_scorecards'); ?>
<?php page_navigation_links(array('unconfirm_scorecards.php' => 'Unconfirm Questions')); ?>
<?php display_flash('action'); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        //$("#action_list").action({updateAction: true, section:'admin', page: "update", autoLoad: false});
        $("#question_list").completequestion({unConfirmCompleteAction: true, section:'admin', page: "unconfirm", autoLoad: false});
    });
</script>
<div id="question_list"></div>