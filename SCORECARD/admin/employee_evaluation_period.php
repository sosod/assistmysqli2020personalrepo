<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.scorecard.userevaluation.js')); ?>
<?php display_menu_list('admin', 'edit_score_card_settings'); ?>
<?php display_sub_menu_list('admin', 'edit_score_card_settings', 'employee_evaluation_period'); ?>
<?php page_navigation_links(array('edit_score_card_settings.php' => 'Edit Scorecards Settings')); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#employee_evaluation").userevaluation({adminUpdate:true, section:"admin", page:"user_evaluation", autoLoad: false});
    });
</script>
<div id="employee_evaluation"></div>
<?php displayAuditLogLink("user_evaluation_periods_logs", true)?>