<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.ui.contract.js', 'jquery.ui.deliverable.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('admin', 'admin_contract_authorized'); ?>
<?php display_sub_menu_list('admin', 'admin_contract_authorized', 'admin_contract_authorized'); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#contract").contract({undoAuthorizeContract:true, section:"admin", page:"authorized_contracts", autoLoad: false, showActions: true, deliverableFilter: false, actionFilter: false });
    });
</script>
<div id="contract"></div>