<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.ui.action.js')); ?>
<?php display_menu_list('admin', 'edit_score_cards'); ?>
<?php display_sub_menu_list('admin', 'edit', 'edit_actions'); ?>
<?php page_navigation_links(array('edit_actions.php' => 'Edit Questions')); ?>
<script language="javascript">
    $(function(){
        $("#action_list").action({editAction: true, section:'admin', page:"edit", autoLoad: false});
    });
</script>
<div id="action_list"></div>