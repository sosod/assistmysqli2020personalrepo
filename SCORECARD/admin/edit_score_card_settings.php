<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.ui.scorecard.js')); ?>
<?php display_menu_list('admin', 'edit_score_card_settings'); ?>
<?php display_sub_menu_list('admin', 'edit_score_card_settings', 'edit_score_card_settings'); ?>
<?php page_navigation_links(array('edit_score_card_settings.php' => 'Edit Scorecards Settings')); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#score_card").scorecard({adminUpdate:true, section:"admin", page:"edit_setting", editScoreCardSetting:true, deleteScoreCardSetting : true, autoLoad: false});
    });
</script>
<div id="score_card"></div>