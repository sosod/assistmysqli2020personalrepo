<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.contract.summarynotification.js','menu.js')); ?>
<?php display_menu_list('admin', 'notifications'); ?>
<?php page_navigation_links(array('notifications.php' => 'Notifications', 'set_summary_notification.php' => 'Set Summary Notification')); ?>
<?php JSdisplayResultObj(""); ?>

<script language="javascript">
	$(function(){
		$("#summarynotification").summarynotification();
	});
</script>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
   <tr>
    <td colspan="3" class="noborder"><div id="summarynotification"></div></td>
   </tr>
    <tr>
        <td align="left" class="noborder"><?php displayGoBack("",""); ?></td>
        <td align="right" class="noborder"><?php displayAuditLogLink("summary_notification_logs", false); ?></td>
    </tr>
</table>

