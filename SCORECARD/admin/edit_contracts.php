<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.ui.contract.js')); ?>
<?php display_menu_list('admin', 'edit_contracts'); ?>
<?php display_sub_menu_list('admin', 'edit', 'contract'); ?>
<?php page_navigation_links(array('edit_contracts.php' => 'Edit Contracts')); ?>
<script language="javascript">
    $(function(){
        $("#contract").contract({adminUpdate:true, section:"admin", page:"view", editContract:true, autoLoad: false});
    });
</script>
<div id="contract"></div>