<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.contract.template.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('admin', 'make_template'); ?>
<?php display_sub_menu_list('admin', 'template'); ?>
<?php page_navigation_links(array('index.php' => 'Admin', 'template.php' => 'Template', 'template_setting.php?id='.$_GET['id'] => 'Settings')); ?>
<?php display_flash('template'); ?>
<?php JSdisplayResultObj(""); ?>
<?php $contractTemplateObj = new ContractTemplate(); ?>
<?php $contract_template   = $contractTemplateObj->getTemplate($_GET['id']); ?>
<script language="javascript">
    $(function(){
        $("#contract").template({section:"admin", autoLoad: true, template_id:<?php echo $_GET['id']; ?>, contract_id: <?php echo $contract_template['contract_id']; ?> });
    });
</script>
<div id="contract"></div>