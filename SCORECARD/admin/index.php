<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.ui.scorecard.js')); ?>
<?php display_menu_list('admin'); ?>
<?php display_sub_menu_list('admin', 'update', 'index'); ?>
<?php page_navigation_links(array('index.php' => 'Update', 'index.php' => 'Update Scorecards' )); ?>
<script language="javascript">
    $(function(){
        $("#score_card").scorecard({adminUpdate:true, section:"admin", page:"view", updateScoreCard:true, autoLoad: false});
    });
</script>
<div id="score_card"></div>