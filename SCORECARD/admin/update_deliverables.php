<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.ui.deliverable.js')); ?>
<?php display_menu_list('admin'); ?>
<?php display_sub_menu_list('admin', 'update', 'update_deliverables'); ?>
<?php
page_navigation_links(array('update_deliverables.php' => 'Update Deliverables'));
display_flash('action');
JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#deliverable_list").deliverable({updateDeliverable: true, page: 'update', section: 'admin' });
    });
</script>
<div id="deliverable_list"></div>
