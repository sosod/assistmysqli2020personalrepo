<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('deliverable.js')); ?>
<?php display_menu_list('admin', 'edit_score_cards'); ?>
<?php display_sub_menu_list('admin', 'edit', 'edit_deliverables'); ?>
<?php
$columnsObj = new ScoreCardColumn();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$categoryObj            = new DeliverableCategory();
$deliverable_categories = $categoryObj->getList(true);

$deliverableObj = new Deliverable();
$deliverable    = $deliverableObj->getDeliverableDetail($_GET['id']);

//debug($deliverable);

$scoreCardObj = new ScoreCard();
$score_card    = $scoreCardObj->getScoreCardDetail($deliverable['score_card_id']);


$deliverableObj    = new Deliverable();
$option_sql        = " AND SC.id = ".$deliverable['score_card_id']." AND D.has_sub_deliverable = 1 ";
$main_deliverables = ($deliverable['type_id'] == 0 ? array() : $deliverableObj->getList(true, false, $option_sql));


$weightObj = new Weight();
$weights   = $weightObj->getList(true);

$userObj = new User();
$deliverable_users = $userObj->getList(true);

page_navigation_links(array('edit_deliverables.php' => 'Edit Headings', 'edit_deliverable.php?id='.$_GET['id'] => 'Edit Heading'));

display_flash('deliverable');
JSdisplayResultObj("");
?>
<form  method="post"  name="deliverable-form" id="deliverable-form" enctype="multipart/form-data">
    <table>
        <tr>
            <td colspan="2"><h4>Edit Deliverable</h4></td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_name', $deliverable_columns['deliverable_name']); ?>
            </th>
            <td>
                <?php echo textarea_tag('deliverable[name]', $deliverable['name'], array('id' => 'name')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('description', $deliverable_columns['description']); ?>
            </th>
            <td>
                <?php echo textarea_tag('deliverable[description]', $deliverable['description'], array('id' => 'description')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_category', $deliverable_columns['deliverable_category']); ?>
            </th>
            <td>
                <?php echo select_tag('deliverable[category_id]', options_for_select($deliverable_categories, $deliverable['category_id']), array('id' => 'category_id')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_type', $deliverable_columns['deliverable_type']); ?>
            </th>
            <td>
                <?php echo select_tag('deliverable[type_id]', options_for_select(array('0' => 'Main', '1' => 'Sub'), $deliverable['type_id']), array('id' => 'type_id')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('main_deliverable', $deliverable_columns['main_deliverable']); ?>
            </th>
            <td>
                <?php echo select_tag('deliverable[parent_id]', options_for_select($main_deliverables, $deliverable['parent_id']), array('id' => 'parent_id')) ?>
            </td>
        </tr>
        <?php if($deliverableObj->isWeighed($score_card, $deliverable)): ?>
            <tr valign="top" class='deliverable_weights' >
                <th>
                    <?php echo label_for('weight', $deliverable_columns['weight']); ?>
                </th>
                <td>
                    <?php echo select_tag('deliverable[weight_id]', options_for_select($weights, $deliverable['weight_id']), array('id' => 'weight_id')) ?>
                </td>
            </tr>
        <?php endif ?>
        <tr>
            <th></th>
            <td>
                <?php if($deliverable['type_id'] == 0 && $deliverable['is_main_deliverable'] == 1): ?>
                    <input type="hidden" name="is_main_deliverable" value="<?php echo $deliverable['is_main_deliverable'] ?>"  id="is_main_deliverable" />
                <?php endif; ?>
                <?php if($deliverable['type_id'] == 0 && $deliverable['has_sub_deliverable'] == 1): ?>
                    <input type="hidden" name="has_sub_deliverable" value="<?php echo $deliverable['has_sub_deliverable'] ?>"  id="has_sub_deliverable" />
                <?php endif; ?>
                <input type="submit" name="edit_deliverable_change" id="edit_deliverable_change" value="Save Changes" class="isubmit" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="hidden" name="deliverable[score_card_id]" id="score_card_id" value="<?php echo $deliverable['score_card_id']?>" />
                <input type="hidden" name="deliverable[id]" id="deliverable_id" value="<?php echo $_GET['id']?>" />
                <input type="hidden" name="log_id" id="deliverable_id" value="<?php echo $deliverable['id']?>" class="logid" />
            </td>
        </tr>
        <tr>
            <td class="noborder"><?php displayGoBack("",""); ?></td>
            <td class="noborder"><?php displayAuditLogLink("deliverable_edit_logs", false) ?></td>
        </tr>
    </table>
</form>