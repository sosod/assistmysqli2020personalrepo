<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('score_card.js')); ?>
<?php display_menu_list('admin', 'admin_score_cards'); ?>
<?php display_sub_menu_list('admin', 'admin_score_cards'); ?>
<?php
page_navigation_links(array('admin_score_cards.php' => 'Scorecards', 'score_card_setup.php?id='.$_GET['id'] => 'Setup Scorecard'));

display_flash('deliverable');
JSdisplayResultObj("");


$columnsObj = new ScoreCardColumn();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$scoreCardObj = new ScoreCard();
$score_card    = $scoreCardObj->getScoreCardDetail($_GET['id']);
//debug($score_card);

$scoreCardAuditLogObj = new ScoreCardAuditLog();
$score_card_logs       = $scoreCardAuditLogObj->getScoreCardActivityLog($_GET['id']);

$score_card_list = $scoreCardObj->getScoreCardDetailList($score_card);

$financialYearObj = new FinancialYear();
$financial_years  = $financialYearObj->getList(true);

$frequency_types  = array('once_off' => 'Once Off', 'recurring' => 'Recurring');

$userObj = new User();
$users   = $userObj->getList();

$scoreCardOwnerObj = new ScoreCardOwner();
$directorates      = $scoreCardOwnerObj->getList();

?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2"><h3>Scorecard Details</h3></td>
                </tr>
                <?php foreach($score_card_list as $key => $score_card_detail): ?>
                    <tr>
                        <th><?php echo $score_card_detail['header']; ?></th>
                        <td><?php echo $score_card_detail['value']; ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <form method="post" name="setup-score-card-form" id="setup-score-card-form">
                <table width="100%">
                    <tr>
                        <td colspan='2'><h3>Schedule A Scorecard Run</h3></td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('frequency', $columns['frequency']); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('score_card[frequency_id]', options_for_select($frequency_types, $score_card['frequency_id']), array('id' => 'frequency_id')); ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('movement_tracked', $columns['movement_tracked']); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('score_card[is_movement_tracked]', options_for_select(array(0 => 'No', 1 => 'Yes'), $score_card['is_movement_tracked']), array('id' => 'is_movement_tracked')); ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('completion_rule', $columns['score_card_completion_rule']); ?>:
                        </th>
                        <td>
                            <select name="score_card[completion_rule]" id="completion_rule">
                                <option value="">--please select--</option>
                                <option value="all">All Recipients Update</option>
                                <option value="allocated_criteria">Recipients Update allocated criteria</option>
                            </select>
                        </td>
                    </tr>
                    <tr valign="top" class="completion-rule all-users-list">
                        <th>
                            <?php echo label_for('users', 'Users'); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('score_card[recipient_user]', options_for_select($users, null, array('include_please_select' => false)), array('id' => 'score_card_recipient_user', 'multiple' => true)); ?>
                        </td>
                    </tr>
                    <tr valign="top"  class="completion-rule allocate">
                        <th>
                            <?php echo label_for('completion_rule_allocate', 'Allocated to'); ?>:
                        </th>
                        <td>
                            <select name="score_card[completion_rule_allocate_to]" id="completion_rule_allocate_to">
                                <option value="">--please select--</option>
                                <option value="directorate">Directorate/Sub-Directorate</option>
                                <option value="action_owner">Action Owner</option>
                            </select>
                        </td>
                    </tr>
                    <tr valign="top" class="completion-rule allocate-rule allocate-users-list">
                        <th>
                            <?php echo label_for('users', 'Users'); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('score_card[allocate_to_user]', options_for_select($users, null, array('include_please_select' => false)), array('id' => 'all_users', 'multiple' => true)); ?>
                        </td>
                    </tr>
                    <tr valign="top" class="completion-rule allocate-rule allocated-directorate-list">
                        <th>
                            <?php echo label_for('directorate', 'Directorate/Sub-Directorate'); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('score_card[directorate_id]', options_for_select($directorates, null ), array('id' => 'directorate_id')); ?>
                        </td>
                    </tr>
                    <tr>
                        <th valign="top">
                            <?php echo label_for('use_rating_comment', $columns['use_rating_comment']); ?>:
                        </th>
                        <td>
                            <?php echo checkbox_tag('score_card[use_rating_comment]', $score_card['use_rating_comment'], array('id' => 'use_rating_comment')); ?>
                        </td>
                    </tr>
                    <tr>
                        <th valign="top">
                            Use Corrective Action
                            <?php //echo label_for('use_corrective_action', $columns['use_corrective_action']); ?>:
                        </th>
                        <td>
                            <?php echo checkbox_tag('score_card[use_corrective_action]', $score_card['use_corrective_action'], (bool)$score_card['use_corrective_action'], array('id' => 'use_corrective_action', 'multiple' => true)); ?>
                        </td>
                    </tr>
                    <tr>
                        <th valign="top">
                            <?php echo label_for('are_corrective_action_owner_identified', $columns['are_corrective_action_owner_identified']); ?>:
                        </th>
                        <td>
                            <?php echo checkbox_tag('score_card[is_corrective_action_owner_identified]', $score_card['is_corrective_action_owner_identified'], array('id' => 'is_corrective_action_owner_identified')); ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" id="activate_score_card" name="activate_score_card" value="Activate Score Card" class="activate_score_card isubmit" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="hidden" id="score_card_id" name="score_card[id]" value="<?php echo $score_card['id']; ?>" />
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>