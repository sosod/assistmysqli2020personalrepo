<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('deliverable.js')); ?>
<?php display_menu_list('admin'); ?>
<?php display_sub_menu_list('admin', 'update', 'update_deliverables'); ?>
<?php
$page_title = "Add Deliverable";
$columnsObj = new ActionColumns();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$deliverableObj = new Deliverable();
$deliverable       = $deliverableObj->getDeliverableDetail($_GET['id']);

$deliverableStatusesObj = new DeliverableStatus();
$deliverable_statuses = $deliverableStatusesObj->getList(true);

$deliverableDocumentObj = new DeliverableDocument();
$deliverable_documents = $deliverableDocumentObj->getAll($deliverable['id']);
?>
<?php page_navigation_links(array('update_deliverables.php' => 'Update Deliverables', 'update_deliverable.php?id='.$deliverable['id'] => 'Update Deliverable'));
display_flash('action');
JSdisplayResultObj("");
?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2">
                        <?php if($deliverable['parent_id'] != 0): ?>
                            <input type="button" id="view_deliverable" name="view_deliverable" value="View Deliverable" />
                        <?php endif; ?>
                        <input type="button" id="view_score_card" name="view_score_card" value="View Socre Card" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><h4>Deliverable Details</h4></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_id']; ?>:</th>
                    <td><?php echo $deliverable['id']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_name']; ?>:</th>
                    <td><?php echo $deliverable['name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['description']; ?>:</th>
                    <td><?php echo $deliverable['description']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_category']; ?>:</th>
                    <td><?php echo $deliverable['deliverable_category']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_owner']; ?>:</th>
                    <td><?php echo $deliverable['deliverable_owner']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_deadline_date']; ?>:</th>
                    <td><?php echo $deliverable['deadline_date']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['main_deliverable']; ?>:</th>
                    <td><?php echo $deliverable['parent_deliverable']; ?></td>
                </tr>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <form  method="post"  name="deliverable_form" id="deliverable_form" enctype="multipart/form-data">
                <table width="100%">
                    <tr>
                        <td colspan="2"><h4>Update Deliverable</h4></td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('response', 'Response'); ?>
                        </th>
                        <td>
                            <?php echo textarea_tag('response', null, array('id' => 'response')) ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('deliverable_status', 'Deliverable Status'); ?>
                        </th>
                        <td>
                            <?php echo select_tag('deliverable[status_id]', options_for_select($deliverable_statuses, $deliverable['status_id']), array('id' => 'status_id')) ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('deliverable_remind_on', $columns['action_remind_on']); ?>
                        </th>
                        <td>
                            <?php echo input_tag('deliverable[remind_on]', $deliverable['remind_on'], array('class' => 'datepicker', 'readonly' => 'readonly', 'id' => 'remind_on')) ?>
                            <a href="#" id="clear_remind">Clear Date</a>
                        </td>
                    </tr>
                    <tr>
                        <th>Attachment:</th>
                        <td>
                            <?php
                            echo Attachment::displayEditableAttachments($deliverable_documents, 'deliverable');
                            ?>
                            <input id="deliverable_attachment" name="deliverable_attachment" type="file" onChange="attachDeliverableDocuments(this)" />
                            <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
                                <small>You can attach more than 1 file.</small>
                            </p>

                        </td>
                    </tr>
                    <tr>
                        <th>Send Approval Notification:</th>
                        <td>
                            <input type="checkbox" id="approval" name="approval" <?php if($deliverable['status_id'] != 4) { ?> disabled="disabled" <?php } ?> value="1" />
                            <p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px; clear:both; margin-top:10px;"><small>This will send an email notification to the relevant person(s) responsible for approving this Deliverable.</small>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <input type="submit" name="update_deliverable" id="update_deliverable" value="Save Changes" class=" isubmit" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="hidden" name="deliverable_id" id="id" value="<?php echo $_GET['id']?>" />
                            <input type="hidden" name="deliverable[id]" id="id" value="<?php echo $_GET['id']?>" />
                            <input type="hidden"id="score_card_id" value="<?php echo $deliverable['score_card_id']?>" />
                            <?php if($deliverable['parent_id'] != 0): ?>
                                <input type="hidden" id="parent_id" name="parent_id" value="<?php echo $deliverable['parent_id']; ?>" />
                            <?php endif; ?>
                            <input type="hidden" class="logid" name="deliverable_id" id="deliverable_id" value="<?php echo $_REQUEST['id'] ?>"  />
                            <span style="float:right;">
                                    <?php displayAuditLogLink("deliverable_update_logs" , false); ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>