<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php display_title('View'); ?>
<?php include_js(array('jquery.contract.payments.js', 'ajaxfileupload.js')); ?>
<?php display_menu_list('admin', 'finance'); ?>
<?php display_sub_menu_list('admin', 'finance', 'finance_payment'); ?>
<?php page_navigation_links(array('finance.php' => 'Finance', 'finance_payment.php' => 'Payment')); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#contract_payment").payments({section:"admin", page:"payment"});
    });
</script>
<div id="contract_payment"></div>
<?php displayAuditLogLink("contract_payment_logs", false) ?>