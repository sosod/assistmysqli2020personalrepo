<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('score_card.js')); ?>
<?php display_menu_list('admin'); ?>
<?php display_sub_menu_list('admin', 'update'); ?>
<?php
$page_title = "Add Deliverable";
$columnsObj = new ScoreCardColumn();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$scoreCardObj = new ScoreCard();
$score_card    = $scoreCardObj->getScoreCardDetail($_GET['id']);
//debug($score_card);
$categoryObj            = new DeliverableCategory();
$deliverable_categories = $categoryObj->getList(true);

$deliverableObj         = new Deliverable();
$main_deliverables      = $deliverableObj->getList(true);

$weightObj = new Weight();
$weights   = $weightObj->getList(true);

$scoreCardStatusObj = new ScoreCardStatus();
$score_card_statuses = $scoreCardStatusObj->getList(true);

page_navigation_links(array('update_score_cards.php' => 'Update Scorecards', 'update_score_card.php?id='.$score_card['id'] => 'Update Scorecard'));

display_flash('deliverable');
JSdisplayResultObj("");
?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2"><h4>Scorecard Details</h4></td>
                </tr>
                <?php echo $scoreCardObj->getScoreCardHtml($_GET['id'], false); ?>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <form  method="post"  name="deliverable-form" id="deliverable-form" enctype="multipart/form-data">
                    <table width="100%">
                        <tr>
                            <td colspan="2"><h4>Update Scorecard</h4></td>
                        </tr>
                        <tr valign="top">
                            <th>
                                <?php echo label_for('response', 'Response'); ?>
                            </th>
                            <td>
                                <?php echo textarea_tag('score_card[response]', null, array('id' => 'response')) ?>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th>
                                <?php echo label_for('score_card_status', 'Contract Status'); ?>
                            </th>
                            <td>
                                <?php echo select_tag('score_card[status_id]', options_for_select($score_card_statuses, $score_card['status_id']), array('id' => 'status_id')) ?>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th>
                                <?php echo label_for('score_card_remind_on', "Remind On"); ?>
                            </th>
                            <td>
                                <?php echo input_tag('score_card[remind_on]', $score_card['remind_on'], array('class' => 'datepicker', 'readonly' => 'readonly', 'id' => 'remind_on')) ?>
                                <a href="#" id="clear_remind">Clear Date</a>
                            </td>
                        </tr>
                        <tr>
                            <th>Attachment:</th>
                            <td>
                                <input id="deliverable_attachment_<?php echo $score_card['id']; ?>" name="deliverable_attachment_<?php echo $score_card['id']; ?>" type="file" class="upload_deliverable" />
                                <p class="ui-state ui-state-info" style="padding:0 5px; clear:both; width:250px;">
                                    <small>You can attach more than 1 file.</small>
                                </p>
                                <?php
                                //Attachment::displayAttachmentList($action['attachement']);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Send Approval Notification:</th>
                            <td>
                                <input type="checkbox" id="approval" name="approval" <?php if($score_card['status_id'] != 4) { ?> disabled="disabled" <?php } ?> value="1" />
                                <p class="ui-state ui-state-info" style="padding-top:5px; padding-bottom:5px; clear:both; margin-top:10px;"><small>This will send an email notification to the relevant person(s) responsible for approving this Deliverable.</small>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <input type="submit" name="update_contract" id="update_contract" value="Save Changes" class=" isubmit" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="hidden" name="score_card[id]" id="id" value="<?php echo $_GET['id']?>" />
                                <input type="hidden" class="logid" name="score_card_id" id="score_card_id" value="<?php echo $_REQUEST['id'] ?>"  />
                                <span style="float:right;">
                                        <?php displayAuditLogLink("score_card_update_logs" , false); ?>
                                </span>
                            </td>
                        </tr>
                    </table>
            </form>
        </td>
    </tr>
</table>