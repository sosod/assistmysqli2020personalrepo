<?php
$scripts 	= array('notification.js');
$styles 	= array('colorpicker.css',   );
$page_title = "Action Notifications";
require_once("../inc/header.php");
$notificationObj = new QueryNotification();
$notification    = $notificationObj -> getNotitication();

?>
<?php JSdisplayResultObj(""); ?>
<form id="notification_form" name="notification_form">
<table id="admintable">
	<tr>
		<th>No</th>
		<th>Notification Name</th>
		<th>Report Type</th>
		<th>Days before deadline</th>
	</tr>
	<tr>
		<td>1</td>
		<td>Reminder e-mail: Automatic Scorecard Reminder</td>
		<td>All Users</td>
		<td>
		  <select id="days_before_deadline" name="days_before_deadline">
		    <?php 
		      for($i = 1; $i <= 31; $i++)
		      {
		    ?>
    		    <option value="<?php echo $i; ?>" 
    		    <?php 
    		      if(!empty($notification))
    		      {
    		        if(isset($notification['days_before_deadline']))
    		        {
    		           echo ($notification['days_before_deadline'] == $i ? "selected='selected'" : "");
    		        }
    		      }
    		    ?>>
    		      <?php echo $i; ?>
    		    </option>
		    <?php
		      }
		    ?>
		  </select>
		</td>
	</tr>	
	<tr>
		<td colspan="4" style="text-align:right;">
  		  <?php
  		   if(empty($notification))
  		   {
  		  ?>
  		    <input type="submit" name="save_action_reminders" id="save_action_reminders" value="Save" /> 
  		  <?php  		   
  		   } else {
  		  ?>
  		    <input type="submit" name="update_action_reminders" id="update_action_reminders" value="Save Changes" /> 
  		    <input type="hidden" name="id" id="id" value="<?php echo $notification['id']; ?>" /> 
  		  <?php
  		   } 
  		  ?>
		</td>
	</tr>
	<tr>
		<td colspan="4" style="text-align:right;">
  		<?php displayGoBack("",""); ?>
		</td>
	</tr>																														
</table>
</form>

