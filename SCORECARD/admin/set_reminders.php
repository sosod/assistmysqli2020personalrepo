<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php display_title('View'); ?>
<?php include_js(array('reminder.js')); ?>
<?php display_menu_list('admin', 'notifications'); ?>
<?php page_navigation_links(array('notifications.php' => 'Notifications', 'set_reminders.php' => 'Set Reminders')); ?>
<?php JSdisplayResultObj("");
$reminderObj = new Reminder();
$reminder    = $reminderObj -> getAReminder();
?>

<form id="notification_form" name="notification_form">
    <table id="admintable"  class="noborder">
        <tr>
            <th>No</th>
            <th>Reminder e-mail</th>
            <th>Reminder Type</th>
            <th>Days before deadline</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Automatic Action Reminder</td>
            <td>All Users</td>
            <td>
              <select id="action_days" name="query_days">
                <?php  for($i = 1; $i <= 31; $i++):  ?>
                    <option value="<?php echo $i; ?>"
                    <?php  if(!empty($reminder) && isset($reminder['action_days'])): ?>
                        <?php echo ($reminder['action_days'] == $i ? "selected='selected'" : ""); ?>
                     <?php endif; ?>>
                      <?php echo $i; ?>
                    </option>
                <?php endfor; ?>
              </select>
            </td>
        </tr>

        <tr>
            <td>2</td>
            <td>Automatic Scorecard Reminder</td>
            <td>All Users</td>
            <td>
              <select id="score_card_days" name="score_card_days">
                <?php  for($i = 1; $i <= 31; $i++): ?>
                    <option value="<?php echo $i; ?>"
                    <?php  if(!empty($reminder) && isset($reminder['score_card_days'])): ?>
                        <?php echo ($reminder['score_card_days'] == $i ? "selected='selected'" : ""); ?>
                    <?php endif; ?>>
                      <?php echo $i; ?>
                    </option>
                <?php endfor; ?>
              </select>
            </td>
        </tr>

        <tr>
            <td colspan="4" style="text-align:right;">
              <?php if(empty($reminder)): ?>
                <input type="submit" name="save_reminders" id="save_reminders" value="Save"  class="isubmit" />
              <?php else: ?>
                <input type="submit" name="update_reminders" id="update_reminders" value="Save Changes" />
                <input type="hidden" name="id" id="id" value="<?php echo $reminder['id']; ?>" />
              <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"  class="noborder" style="text-align:left;"><?php displayGoBack("",""); ?></td>
            <td colspan="2"  class="noborder" style="text-align:right;"><?php displayAuditLogLink("reminder_logs" , true); ?></td>
        </tr>
    </table>
</form>

