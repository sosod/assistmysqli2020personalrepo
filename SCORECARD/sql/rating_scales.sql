DROP TABLE IF EXISTS `assist_admire1_scd_faces_rating_scale`;
CREATE TABLE IF NOT EXISTS `assist_admire1_scd_faces_rating_scale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numerical_rating` int(11) NOT NULL,
  `face` varchar(100) NOT NULL,
  `color` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `insert_user` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_scd_numerical_rating_scale`
--

DROP TABLE IF EXISTS `assist_admire1_scd_numerical_rating_scale`;
CREATE TABLE IF NOT EXISTS `assist_admire1_scd_numerical_rating_scale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numerical_rating` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `rating_type` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `definition` text NOT NULL,
  `color` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `insert_user` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `assist_admire1_scd_numerical_rating_scale`
--

INSERT INTO `assist_admire1_scd_numerical_rating_scale` (`id`, `numerical_rating`, `rating`, `rating_type`, `description`, `definition`, `color`, `status`, `insert_user`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Rating 1', 'Rating One', 'Rating One', 'FFFDC4', 5, '0002', '2014-01-18 02:24:31', '2014-01-18 00:24:31'),
(2, 2, 2, 'Rating 2', 'Rating Two', 'Rating Two', 'E2FF24', 5, '0002', '2014-01-18 02:20:58', '2014-01-18 00:21:42'),
(3, 3, 3, 'Rating 3', 'Rating 3', 'Rating 3', 'D4A257', 5, '0002', '0000-00-00 00:00:00', '2014-01-18 00:21:42'),
(4, 4, 4, 'Rating 4', 'Rating 4', 'Rating 4', '99FF33', 5, '0002', '0000-00-00 00:00:00', '2014-01-18 00:21:42'),
(5, 5, 5, 'Rating 5', 'Rating 5', 'Rating 5', '99FF33', 5, '0002', '2014-01-18 00:00:00', '2014-01-18 00:21:42'),
(6, 6, 6, 'Rating 6', 'Rating 6', 'Rating 6', '21FF9F', 5, '0002', '0000-00-00 00:00:00', '2014-01-18 00:21:42'),
(7, 7, 7, 'Rating 7', 'Rating 7', 'Rating 7', '38FFC3', 5, '0002', '0000-00-00 00:00:00', '2014-01-18 00:21:42'),
(8, 8, 8, 'Rating 8', 'Rating 8', 'Rating 8', '0A2BFF', 5, '0002', '0000-00-00 00:00:00', '2014-01-18 00:21:42'),
(9, 9, 9, 'Rating 9', 'Rating 9', 'Rating 9', 'FF9442', 5, '0002', '0000-00-00 00:00:00', '2014-01-18 00:21:42'),
(10, 10, 10, 'Rating 10', 'Rating 10', 'Rating 10', '46FF21', 5, '0002', '0000-00-00 00:00:00', '2014-01-18 00:21:42');

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_scd_percentage_rating_scale`
--

DROP TABLE IF EXISTS `assist_admire1_scd_percentage_rating_scale`;
CREATE TABLE IF NOT EXISTS `assist_admire1_scd_percentage_rating_scale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numerical_rating` int(11) NOT NULL,
  `percentage` varchar(10) NOT NULL,
  `color` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `insert_user` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `assist_admire1_scd_percentage_rating_scale`
--

INSERT INTO `assist_admire1_scd_percentage_rating_scale` (`id`, `numerical_rating`, `percentage`, `color`, `status`, `insert_user`, `created_at`, `updated_at`) VALUES
(1, 1, '10', '0A2BFF', 1, '0002', '2013-11-30 00:00:00', '2014-01-21 03:37:38'),
(2, 2, '20', 'E2FF24', 1, '0002', '2013-12-05 00:00:00', '2014-01-21 03:37:38'),
(3, 3, '30', '99FF33', 1, '0002', '2014-01-31 00:00:00', '2014-01-21 03:37:38'),
(4, 4, '40', '21FF9F', 1, '0002', '2014-01-04 07:14:31', '2014-01-21 03:37:38'),
(5, 5, '50', '38FFC3', 1, '0002', '2014-01-04 07:14:46', '2014-01-21 03:37:38'),
(6, 6, '60', 'FFFFFF', 1, '00002', '2014-01-21 00:00:00', '2014-01-21 03:37:38'),
(7, 7, '70', 'FFFFF', 1, '0002', '2014-01-21 00:00:00', '2014-01-21 03:37:38'),
(8, 8, '80', 'FFFFF', 1, '0002', '2014-01-21 00:00:00', '2014-01-21 03:37:38'),
(9, 9, '90', 'FFFFF', 1, '0002', '2014-01-21 00:00:00', '2014-01-21 03:37:38'),
(10, 10, '100', 'FFFF', 1, '0002', '2014-01-21 00:00:00', '2014-01-21 03:37:38');

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_scd_symbol_rating_scale`
--

DROP TABLE IF EXISTS `assist_admire1_scd_symbol_rating_scale`;
CREATE TABLE IF NOT EXISTS `assist_admire1_scd_symbol_rating_scale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numerical_rating` int(11) NOT NULL,
  `symbol` varchar(5) NOT NULL,
  `color` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `insert_user` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `assist_admire1_scd_symbol_rating_scale`
--

INSERT INTO `assist_admire1_scd_symbol_rating_scale` (`id`, `numerical_rating`, `symbol`, `color`, `status`, `insert_user`, `created_at`, `updated_at`) VALUES
(1, 1, 'A', '0A2BFF', 1, '0002', '0000-00-00 00:00:00', '2014-01-21 03:10:55'),
(2, 2, 'B', 'E2FF24', 1, '0002', '2014-01-21 00:00:00', '2014-01-21 03:10:55'),
(3, 3, 'C', '46FF21', 1, '0002', '2013-12-05 00:00:00', '2014-01-21 03:10:55'),
(4, 4, 'D', '21FF9F', 1, '0002', '2014-01-21 00:00:00', '2014-01-21 03:10:55'),
(5, 5, 'E', '38FFC3', 1, '0002', '2014-01-21 00:00:00', '2014-01-21 03:10:55'),
(6, 6, 'F', 'FFFFFF', 1, '0002', '2014-01-21 00:00:00', '2014-01-21 03:10:55'),
(7, 7, 'G', 'FFFFFF', 1, '0002', '2014-01-21 00:00:00', '2014-01-21 03:10:55'),
(8, 8, 'H', 'FFFFFF', 1, '00002', '2014-01-21 00:00:00', '2014-01-21 03:10:55'),
(9, 9, 'I', 'FFFFFF', 1, '00002', '2014-01-21 00:00:00', '2014-01-21 03:10:55'),
(10, 10, 'J', 'FFFFFF', 1, '00002', '2014-01-21 00:00:00', '2014-01-21 03:10:55');

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_scd_yes_no_rating_scale`
--

DROP TABLE IF EXISTS `assist_admire1_scd_yes_no_rating_scale`;
CREATE TABLE IF NOT EXISTS `assist_admire1_scd_yes_no_rating_scale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numerical_rating` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `insert_user` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

