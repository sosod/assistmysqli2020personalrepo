<?php
				$head = array(
					'ref'=>array('text'=>"Ref", 'long'=>false, 'deadline'=>false),
					'deadline'=>array('text'=>"Deadline", 'long'=>false, 'deadline'=>true),
					'action'=>array('text'=>"Action Name", 'long'=>true, 'deadline'=>false),
					'action_owner'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'progress'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'action_status'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
					'link'=>array('text'=>"", 'long'=>false, 'deadline'=>false),
				);

    $sql = "SELECT name, client_terminology FROM ".$dbref."_naming";
    $rs = getRS($sql);
    while($row = mysql_fetch_array($rs))
    {
       if(isset($head[$row['name']]))  $head[$row['name']]['text'] = $row['client_terminology'];
    }
    mysql_close();
	$sql = "SELECT CONTR.id, CONTR.id AS action_id, CONTR.name AS action_name, CONTR.status, SCS.start_date AS action_deadline_date,
	        CONTR.progress AS action_progress, CONTR.status_id, S.name as status, S.name as action_status,
			CONCAT(TK.tkname,' ',TK.tksurname) AS action_owner, CONTR.owner_id
			FROM  ".$dbref."_action CONTR
			LEFT JOIN ".$dbref."_action_status S ON S.id = CONTR.status_id
			LEFT JOIN assist_".$cmpcode."_timekeep TK ON TK.tkid  = CONTR.owner_id
			INNER JOIN ".$dbref."_deliverable D ON D.id = CONTR.deliverable_id
			INNER JOIN ".$dbref."_score_card C ON C.id = D.score_card_id
			INNER JOIN ".$dbref."_score_card_setup SCS ON SCS.id = C.score_card_setup_id
			WHERE CONTR.owner_id = '$tkid' AND C.status & 16 = 16
			AND CONTR.status & 16 <> 16 AND SCS.start_date <= ($today + 3600*24*$next_due)
			";
//echo $sql;
            if(isset($action_profile))
            {
                switch($action_profile['field3'])
                {
                    case "dead_desc":	$sql.= " ORDER BY SCS.start_date DESC "; break;
                    default:
                    case "dead_asc":	$sql.= " ORDER BY SCS.start_date ASC "; break;
                }
            }
            $sql.= " LIMIT ".(isset($action_profile['field2']) ? $action_profile['field2'] : 20);
	$tasks = mysql_fetch_all_fld($sql,"id");
	$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
	foreach($tasks as $id => $task)
    {
		$deadline = "";
	    $stattusid = $task['status_id'];
		if($stattusid !== "3")
        {

			$deaddiff = $today - strtotime($task['action_deadline_date']);
			$diffdays = floor($deaddiff/(3600*24));
			if($deaddiff<0) {
				$diffdays*=-1;
				$days = $diffdays > 1 ? "days" : "day";
				$deadline =  "<span class=soon>Due in $diffdays $days</span>";
			} elseif($deaddiff==0) {
				$deadline =  "<span class=today><b>Due today</b></span>";
			} else {
				$days = $diffdays > 1 ? "days" : "day";
				$deadline = "<span class=overdue><b>$diffdays $days</b> overdue</span>";
			}
			$actions[$id] = array();
			$actions[$id]['ref'] = $id;
			$actions[$id]['deadline'] = $deadline;
			$actions[$id]['action'] = $task['action_name'];
			$actions[$id]['action_owner'] = $task['action_owner'];
			$actions[$id]['progress'] = $task['action_progress']."%";
			$actions[$id]['action_status'] = ($task['action_status'] == "" ? "New" : $task['action_status'] );
			$actions[$id]['link'] = "manage/update_actions.php";
		}
	}					
?>
