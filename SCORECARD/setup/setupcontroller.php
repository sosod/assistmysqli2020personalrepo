<?php
function __autoload($classname){
	if( file_exists( "../class/".strtolower($classname).".php" ) ){
		require_once( "../class/".strtolower($classname).".php" );
	} else if( file_exists( "../".strtolower($classname).".php" ) ) {
		require_once( "../".strtolower($classname).".php" );		
	}
}
class SetupController extends Controller
{
	//
	function __construct()
	{
		
	}
	
	function getMenu()
	{
		$this -> loadModel("Menu");
		$menuItems = $this -> model -> getMenuItems();
		print_r( $menuItems );
	}

}
$method 	=  $_GET['action'];
$controller = new SetupController();
if(method_exists($controller, $method)){
	$controller -> $method();
}
?>