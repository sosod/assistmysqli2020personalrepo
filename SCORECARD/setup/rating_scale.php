<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('rating_scale.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'rating_scale.php' => 'Rating Scale')); ?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder" width="75%">
    <tr>
        <td colspan="2" class="noborder">
            <table id="rating_scale_table" width="70%">
                <tr>
                    <td colspan="8" style="text-align: center;">
                        <select name="rating_scale_type" id="rating_scale_type">
                           <option value="">--please select--</option>
                           <?php foreach(RatingScale::getRatingScaleTypes() as $key => $rating_type): ?>
                               <option value="<?php echo $key; ?>"><?php echo $rating_type['name']; ?></option>
                           <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr class="rating_scales">
                    <td colspan="8">Please select the rating scale type</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("rating_scale_logs", true)?></td>
    </tr>
</table>

