<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('naming.js')); ?>
<?php display_menu_list('setup'); ?>
<?php display_sub_menu_list('setup', 'naming'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'deliverable_naming.php' => 'Heading Header Names')); ?>
<?php JSdisplayResultObj(""); ?>
<?php $columnsObj = new DeliverableColumns(); ?>
<?php $columns    = $columnsObj->getColumns(); ?>
<?php JSdisplayResultObj(""); ?>
<table id="menu_table">
    <tr>
        <td colspan="4" class="gray">
            <p>
                <b>Define the naming convention {Purpose : Client can define own naming convention}</b>
            </p>
        </td>
    </tr>
    <tr>
        <th>Ref</th>
        <th>Scorecard Assist Terminology</th>
        <th>Client Terminology</th>
        <th></th>
    </tr>
    <?php foreach($columns as $index => $column): ?>
        <tr>
            <td><?php echo $column['id']; ?></td>
            <td><?php echo $column['score_card_terminology']; ?></td>
            <td>
                <input id="client_terminology_<?php echo $column['id']; ?>" name="client_terminology__<?php echo $column['id']; ?>" value="<?php echo $column['client_terminology']; ?>" size="50" />
            </td>
            <td>
                <input type="button" name="update_contract_<?php echo $column['id'] ?>" id="<?php echo $column['id'] ?>" value="Update"  class="update_contract" onclick="updateNaming(this);" />
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<div>
    <?php displayGoBack("",""); ?>
</div>

