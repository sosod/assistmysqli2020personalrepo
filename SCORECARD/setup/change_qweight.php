<?php
$scripts = array("qualityweight.js");
require_once("../header.php");
$q 			   = new QualityWeight();
$qweight	   = $q -> getQualityWeight( $_GET['id'] ); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="qualityweight" name="qualityweight">
<table id="table_qualityweight">
	<tr>
		<th>Ref #:</th>
        <td><?php echo $_GET['id']?></td>	
     </tr>
    <tr>  
    	<th>Status:</th>    
    	<td>
        	<select name="status" id="status">
            	<option value="1" <?php if($qweight['active']==1) {?> selected<?php } ?>>Active</option>
            	<option value="0" <?php if($qweight['active']==0) {?> selected<?php } ?>>InActive</option>                
            </select>
        </td>    
    </tr>
    <tr>
    	<th></th>
    	<td><input type="submit" name="updatestatus" id="updatestatus" value="Update" />
        <input type="hidden" id="qualityweightid" name="qualityweightid" value="<?php echo $_GET['id']; ?>" />        
        </td>                             
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>    
</table>
</form>
</div>