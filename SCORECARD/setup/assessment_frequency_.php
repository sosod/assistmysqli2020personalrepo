<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('assessment_frequency.js')); ?>
<?php display_menu_list('setup'); ?>
<?php $scripts = array('assessment_frequency.js');  ?>
<?php
page_navigation_links(array('index.php' => 'Default', 'assessment_frequency.php' => 'Assessment Frequency'));
display_flash('assessment_frequency');
JSdisplayResultObj("");
?>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder">
            <table id="assessment_frequency_table">
                <tr>
                    <td colspan="7">
                        <input type="button" name="add_assessment_frequency" id="add_assessment_frequency" value="Add" />
                    </td>
                </tr>
                <tr>
                    <th>Ref</th>
                    <th>Short Code</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("assessment_frequency_logs", true)?></td>
    </tr>
</table>

