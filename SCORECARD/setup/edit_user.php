<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('user.js')); ?>
<?php display_menu_list('setup', 'user'); ?>
<?php display_sub_menu_list('setup', 'user'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'user.php' => 'User Access', 'edit_user.php?id='.$_GET['id'] => 'Edit User')); ?>
<?php
$scripts = array("user.js");

$userObj = new User();
$user    = $userObj->getUserSetting($_GET['id']);
?>
<?php JSdisplayResultObj(""); ?>
<div class="displayview">
    <form id="edit-user-form" name="edit-user-form" method="post">
        <table id="table_useraccess" width="50%">
            <tr>
                <th>Ref #</th>
                <td><?php echo $_GET['id']; ?></td>
            </tr>
            <tr>
                <th>User:</th>
                <td>
                    <?php echo $user['user']; ?>
                </td>
            </tr>
            <tr>
                <th>Module Admin:</th>
                <td>
                    <select id="module_admin" name="status[module_admin]">
                        <option value="1" <?php if(($user['status'] & 2048) == 2048 ){?> selected <?php } ?>>Yes</option>
                        <option value="0" <?php if(($user['status'] & 2048) == 0 ){?> selected <?php } ?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Create Scorecard:</th>
                <td>
                    <select id="create_score_card" name="status[create_score_card]">
                        <option value="1" <?php if(($user['status'] & 4) == 4 ){?> selected <?php } ?>>Yes</option>
                        <option value="0" <?php if(($user['status'] & 4) == 0 ){?> selected <?php } ?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Create Heading:</th>
                <td>
                    <select id="create_deliverable" name="status[create_deliverable]">
                        <option value="1" <?php if(($user['status'] & 8) == 8 ){?> selected <?php } ?>>Yes</option>
                        <option value="0" <?php if(($user['status'] & 8) == 0 ){?> selected <?php } ?>>No</option>
                    </select>
                </td>
            </tr>

            <tr>
                <th>Update All:</th>
                <td>
                    <select id="update_all" name="status[update_all]">
                        <option value="1" <?php if(($user['status'] & 16) == 16 ){?> selected <?php } ?>>Yes</option>
                        <option value="0" <?php if(($user['status'] & 16) == 0 ){?> selected <?php } ?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Edit All:</th>
                <td>
                    <select id="edit_all" name="status[edit_all]">
                        <option value="1" <?php if(($user['status'] & 32) == 32 ){?> selected <?php } ?>>Yes</option>
                        <option value="0" <?php if(($user['status'] & 32) == 0 ){?> selected <?php } ?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Reports:</th>
                <td>
                    <select id="reports" name="status[reports]">
                        <option value="1" <?php if(($user['status'] & 64) == 64 ){?> selected <?php } ?>>Yes</option>
                        <option value="0" <?php if(($user['status'] & 64) == 0 ){?> selected <?php } ?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Assurance:</th>
                <td>
                    <select id="assurance" name="status[assurance]">
                        <option value="1" <?php if(($user['status'] & 128) == 128 ){?> selected <?php } ?>>Yes</option>
                        <option value="0" <?php if(($user['status'] & 128) == 0 ){?> selected <?php } ?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Scorecard Admin:</th>
                <td>
                    <select id="score_card_admin" name="status[score_card_admin]">
                        <option value="1" <?php if(($user['status'] & 512) == 512 ){?> selected <?php } ?>>Yes</option>
                        <option value="0" <?php if(($user['status'] & 512) == 0 ){?> selected <?php } ?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Setup:</th>
                <td>
                    <select id="setup" name="status[setup]">
                        <option value="1" <?php if(($user['status'] & 256) == 256 ){?> selected <?php } ?>>Yes</option>
                        <option value="0" <?php if(($user['status'] & 256) == 0 ){?> selected <?php } ?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <input type="button" name="edit_user" id="edit_user" value="Edit" />
                    <input type="hidden" id="id" name="id" value="<?php echo $_GET['id']; ?>" />
                    <input type="hidden" id="ref_id"  value="<?php echo $_GET['id']; ?>" class="logid" />
                </td>
            </tr>
            <tr>
                <td class="noborder"><?php displayGoBack("", ""); ?></td>
                <td class="noborder"><?php displayAuditLogLink("user_logs", true); ?></td>
            </tr>
        </table>
    </form>
</div>
