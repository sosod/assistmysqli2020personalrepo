<?php
$scripts = array("financialyear.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form method="post" id="financialyear">
			<table id="table_financialyear">
				<tr>
			    	<th>Ref</th>
			    	<th>Last Day</th>
			    	<th>Start Day</th>
			    	<th>End Day</th>                        
			    	<th></th>        
			    </tr>
			    <tr>
			    	<td>#</td>
			    	<td>Year Ending : <input type="text" name="lastday" id="lastday" class="datepicker" value="" /></td>
			    	<td><input type="text" name="startdate" id="startdate" class="datepicker" value="" /></td>
			    	<td><input type="text" name="enddate" id="enddate" class="datepicker" value="" /></td>
			    	<td><input type="submit" name="save" id="save" value="Add" /></td>                                
			    </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder">
			<?php displayGoBack("", ""); ?>
		</td>
		<td class="noborder"><?php displayAuditLogLink("financial_years_logs", true)?></td>		
	</tr>
</table>
</div>
