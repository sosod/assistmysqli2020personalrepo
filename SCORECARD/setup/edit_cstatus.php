<?php
$scripts = array("jscolor.js","contractstatus.js");
require_once("../header.php");
$cstatus   = new ContractStatus();
$cstatuses = $cstatus -> getCStatus( $_GET['id'] );
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post">
<table id="table_contractstatus">
	<tr>
    	<th>Ref #:</th>
    	<td><?php echo $_GET['id']; ?></td>
     </tr>
     <tr>        
    	<th>Name:</th>
		<td><input type="text" name="name" id="name" value="<?php echo $cstatuses['name']; ?>" <?php echo (($cstatuses['status'] & DBConnect::DEFAULTS) == DBConnect::DEFAULTS) ? 'disabled="disabled"' : ""; ?> /></td>
	</tr>        
    <tr>
    	<th>&lt;Client&gt; Teminology:</th>                
    	<td><input type="text" name="client_terminology" id="client_terminology" value="<?php echo $cstatuses['client_terminology']; ?>" /></td>        
     </tr>
     <tr>
    	<th>Color:</th>
    	<td><input type="text" name="color" id="color" class="color" value="<?php echo $cstatuses['color']; ?>" /></td>        
     </tr>
     <tr>
    	<th></th>                        
    	<td>
        	<input type="submit" name="edit" id="edit" value="Edit"  />
        	<input type="hidden" name="statusid" id="statusid" value="<?php echo $_GET['id']; ?>"  />
        </td>        
     </tr>
     <tr>
     	<td class="noborder"><?php displayGoBack("", ""); ?></td>
     	<td class="noborder"></td>
     </tr>
</table>
</form>
</div>