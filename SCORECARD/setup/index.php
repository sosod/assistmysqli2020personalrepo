<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array()); ?>
<?php display_title('Defaults'); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup Defaults')); ?>
<table border="1" width="60%" cellpadding="5" cellspacing="0" id="link_to_page">
    <tr>
        <th>Menu and Headings:</th>
        <td>Define the Menu headings to be displayed in the module</td>
        <td><input type="button" name="menu" id="menu" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
        <th>Naming Convention:</th>
        <td>Define the Naming convention of the Scorecard management fields for the module </td>
        <td><input type="button" name="naming" id="naming" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
        <th>(Sub-)Directorate Structure:</th>
        <td>Configure the (Sub-)Directorate responsibilities </td>
        <td><input type="button" name="setup_dir" id="setup_dir" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
        <th>Scorecard Types:</th>
        <td>Configure the Scorecard Types</td>
        <td><input type="button" name="score_card_type" id="score_card_type" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
        <th>Scorecard Categories:</th>
        <td>Configure the Scorecard Categories</td>
        <td><input type="button" name="score_card_category" id="score_card_category" value="Configure" class="setup_defaults"  /></td>
    </tr>
<!--    <tr>
        <th>Scorecard Statuses:</th>
        <td>Configure the Scorecard Statuses</td>
        <td><input type="button" name="score_card_status" id="score_card_status" value="Configure" class="setup_defaults"  /></td>
    </tr>-->
    <tr>
        <th>Heading Reporting Categories:</th>
        <td>Configure the Heading Reporting Categories</td>
        <td><input type="button" name="deliverable_category" id="deliverable_category" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
        <th>Define Weights:</th>
        <td>Define the Weights to be available if required</td>
        <td><input type="button" name="weight" id="weight" value="Configure"  class="setup_defaults" /></td>
    </tr>
    <tr>
        <th>Define Rating Scales:</th>
        <td>Configure Rating Scales to be used in the module</td>
        <td><input type="button" name="rating_scale" id="rating_scale" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
        <th>Assurance Statuses:</th>
        <td>Define the Assurance statuses</td>
        <td><input type="button" name="assurance_status" id="assurance_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
<!--    <tr>
        <th>Authorisation Statuses:</th>
        <td>Define the Authorisation statuses</td>
        <td><input type="button" name="authorization_status" id="authorization_status" value="Configure" class="setup_defaults"  /></td>
    </tr>-->
    <tr>
        <th>Glossary:</th>
        <td>Define Glossary Terms</td>
        <td><input type="button" name="glossary" id="glossary" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
        <th>List Columns:</th>
        <td>Configure which columns display on the New/Manage pages</td>
        <td><input type="button" name="columns" id="columns" value="Configure" class="setup_defaults"  /></td>
    </tr>
</table>
<script>
    $(function(){

        $(".setup_defaults").live('click', function(){
            var location           = this.id;
            document.location.href = location+'.php';
            return false;
        })
    });
</script>