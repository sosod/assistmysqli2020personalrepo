<?php
$scripts = array("notifcationrules.js", "jquery-ui-timepicker-addon.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
	<tr>
		<td class="noborder" colspan="2">
			<form id="notifcationrules" name="notifcationrules">
			<table id="table_notifcations">
				<tr>
					<th>Ref</th>	
					<th>Short Code</th>	    
					<th>Notification Type</th>
					<th>Description of notification type</th>
					<th>Day;Time</th>
					<th></th>                        	           
					<th></th>
			        <th></th>         
			    </tr>
			    <tr>
			    	<td>#</td>
			    	<td><input type="text" name="shortcode" id="shortcode" value="" /></td> 
			    	<td><input type="text" name="type" id="type" value="" /></td>                
			    	<td><textarea name="description" id="description"></textarea></td>              
			    	<td><input type="text" name="date" id="date" value="" /></td>    
			    	<td><input type="submit" name="save" id="save"  value="Add" /></td>                             
			    	<td></td> 
			    	<td></td>                       
			    </tr>
			</table>
			</form>		
		</td>
	</tr>
	<tr>
		<td class="noborder"><?php displayGoBack("", ""); ?></td>
		<td class="noborder"><?php displayAuditLogLink("notification_rules_logs", true)?></td>
	</tr>
</table>
</div>
