<?php

require_once("../../library/class/assist_helper.php");
require_once("../../library/class/assist.php");
require_once("../../library/class/assist_dbconn.php");
require_once("../../library/class/assist_db.php");
require_once("../class/directorateadministrator.php");
//require_once("../../library/class/autoload.php");


$sections = array(
	'update'=>array(
		'text'=>"Update",
		'can_function'=>"canUpdate",
		'default'=>"Y",
	),
	'edit'=>array(
		'text'=>"Edit",
		'can_function'=>"canEdit",
		'default'=>"N",
	),
	'approve'=>array(
		'text'=>"Approve",
		'can_function'=>"canApprove",
		'default'=>"N",
	),
	'delete'=>array(
		'text'=>"Delete",
		'can_function'=>"canDelete",
		'default'=>"N",
	),
	'add_action'=>array(
		'text'=>"Add Action",
		'can_function'=>"canAddAction",
		'default'=>"N",
	),
	'add_object'=>array(
		'text'=>"Add Query",
		'can_function'=>"canAddObject",
		'default'=>"N",
	),
);
$active_settings = DirectorateAdministrator::get_class_constants();


$i = $_REQUEST['i'];
//$data = array(0=>":".$i.":".$_REQUEST['act'].":");
$data = array();
switch($_REQUEST['act']) {
	case "getEditData": $data = getEditData($i); 			break;
	case "ADD":			$data = addNewAdmin($_REQUEST);		break;
	case "EDIT":		$data = editAdmin($_REQUEST);		break;
	case "DELETE":			$data = deleteAdmin($_REQUEST);		break;
}

echo json_encode($data);








function getEditData($i) {
	$db = new ASSIST_DB("client");
	$cc = $db->getCmpCode();
	$dbref = $db->getDBRef();
	
	$sql = "SELECT * FROM ".$dbref."_dir_admins WHERE id = $i";
	$row = $db->mysql_fetch_one($sql);
	
	$me = new DirectorateAdministrator($row);
	
	$access = $me->getAccess();
	
	foreach($access as $key=>$a) {
		$row[$key] = $a===true ? "Y" : "N";
	}
	
	return $row;
	
}



function addNewAdmin($var) {
	$result = array(0=>"error",1=>"An error occurred.  Please try again.");
	$db = new ASSIST_DB("client");
	$cc = $db->getCmpCode();
	$dbref = $db->getDBRef();
	
	global $sections, $active_settings;
	
	$a = array(
		'id'		=> 0,
		'tkid'	=> $var['atkid'],
		'type'		=> ($var['ref']=="DIR" ? "DIR" : "SUB"),
		'ref'		=> ($var['ref']=="DIR" ? $var['d'] : $var['ref']),
		'active'	=> DirectorateAdministrator::ACTIVE,
		'insertdate'=>date("d-M-Y H:i:s"),
		'insertuser'=>$_SESSION['tid'],
	);
	
	foreach($sections as $key=>$s) {
		if($var[$key]=="Y") {
			$a['active']+=$active_settings[strtoupper($key)];
		}
	}
	
	$sql = "INSERT INTO ".$dbref."_dir_admins (".implode(",",array_keys($a)).") VALUES ('".implode("','",$a)."')";
	
	//$result[1] = $sql;
	$mar = $db->db_insert($sql);
	if($mar>0) {
		$result[0] = "ok";
		$result[1] = "Success!  New Administrator has been added.";
	}
	$result[1] = urlencode($result[1]);
	return $result;
}

function editAdmin($var) {
	$db = new ASSIST_DB("client");
	$me = new ASSIST();
	$cc = $me->getCmpCode();
	$dbref = $db->getDBRef();
	$result = array(0=>"error",1=>"An error occurred.  Please try again.");
	global $sections, $active_settings;
	
	$original = getEditData($var['i']);
	$admin1 = new DirectorateAdministrator($original);
	$a = array(
		'id'		=> $var['i'],
		'tkid'	=> $var['atkid'],
		'type'		=> ($var['ref']=="DIR" ? "DIR" : "SUB"),
		'ref'		=> ($var['ref']=="DIR" ? $var['d'] : $var['ref']),
		'active'	=> DirectorateAdministrator::ACTIVE,
	);
	
	foreach($sections as $key=>$s) {
		if($var[$key]=="Y") {
			$a['active']+=$active_settings[strtoupper($key)];
		}
	}
	$admin2 = new DirectorateAdministrator($a);
	
	$sql = "UPDATE ".$dbref."_dir_admins SET ";
	$u = array();
	foreach($a as $k=>$v) {
		$u[] = $k." = '".$v."'";
	}
	$sql.= implode(", ",$u)." WHERE id = ".$var['i'];
	//$result[1] = $sql;
	
	$mar = $db->db_update($sql);
	if($original['tkid']!=$a['tkid'] || $original['active']!=$a['active'] || $original['ref']!=$a['ref'] || $original['type']!=$a['type']) {
		$result[0] = "ok";
		$result[1] = "Success!  Administrator was updated.";
		$changes = array(
			'changes'=>array('ref_'=>"Ref #".$var['i']),
		);
		foreach($sections as $key=>$sec) {
			$c1 = call_user_func(array($admin1,$sec['can_function']));
			$c2 = call_user_func(array($admin2,$sec['can_function']));
			if($c1!=$c2) {
				$changes[$key] = array(
					'from'=>($c1===true ? "Yes" : "No"),
					'to'=>($c2===true ? "Yes" : "No"),
				);
			}
		}
		$changes['user'] = $me->getUserName();
		logMe($db,$dbref,$var['i'],$changes);
		
	} else {
		$result[0] = "info";
		$result[1] = "No change was found.";
	}
	$result[1] = urlencode($result[1]);
	return $result;
}

function deleteAdmin($var) {
	$db = new ASSIST_DB("client");
	$me = new ASSIST();
	$cc = $me->getCmpCode();
	$dbref = $db->getDBRef();
	$result = array(0=>"error",1=>"An error occurred.  Please try again.");
	global $sections, $active_settings;
	
	$original = getEditData($var['i']);
	
	$active = ($original['active'] - DirectorateAdministrator::ACTIVE) + DirectorateAdministrator::INACTIVE;
	
	$sql = "UPDATE ".$dbref."_dir_admins SET active = $active WHERE id = ".$var['i'];
	
	$mar = $db->db_update($sql);
		$result[0] = "ok";
		$result[1] = "Success!  Administrator was deleted.";
		$changes = array(
			'changes'=>array('ref_'=>"Ref #".$var['i']),
			'status'=>"Has been deleted",
			'user' => $me->getUserName()
		);
		logMe($db,$dbref,$var['i'],$changes);
		
	$result[1] = urlencode($result[1]);
	return $result;
}


function logMe($db,$dbref,$i,$changes) {
	$sql = "INSERT INTO ".$dbref."_dir_admins_logs (setup_id,changes,insertdate,insertuser) VALUES (".$i.",'".base64_encode(serialize($changes))."',now(),'".$_SESSION['tid']."')";
	$db->db_insert($sql);

}


?>