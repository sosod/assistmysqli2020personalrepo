<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('score_card_status.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'setup_dir.php' => ' (Sub-)Directorate responsibilities', 'setup_dir_add.php' => ' Add (Sub-)Directorate' )); ?>
<?php JSdisplayResultObj(""); ?>
<?php
$directorateObj = new Directorate();
$row   = $directorateObj->getMaxRow();
$d = (isset($row['d']) && is_numeric($row['d'])) ? $row['d']+1 : 1;
?>
<style type=text/css>
table th { text-align: left; }
</style>
<script type="text/javascript">
function Validate(me) {
    var dirtxt = me.dir.value;
	var sub1 = me.sub1.value;
    if(dirtxt.length == 0) {
        alert("Please enter the name of the Directorate.");
		return false;
    } else if(sub1.length == 0) {
		alert("Please enter the name of the Primary Sub-Directorate.");
		return false;
	} else {
		return true;
	}
}
</script>
<h3>Add Directorate</h3>
<form name=add method=post action=setup_dir.php onsubmit="return Validate(this);">
	<input type=hidden name=act value=ADD />
	<input type=hidden name=dsort value=<?php echo $d; ?> />
	<table cellpadding=3 cellspacing=0 width=650 id="table1">
		<tr>
			<th width=150>Directorate:</th>
			<td><input type=text size="50" name=dir maxlength=50>&nbsp;</td>
			<td width=110><small>Max 50 characters</small></td>
		</tr>
		<tr>
			<th >Primary Sub-Directorate:</th>
			<td><input type=text size="50" name=sub1 maxlength=50>&nbsp;</td>
			<td width=110>*required </td>
		</tr>
		<tr>
			<th valign=top >Sub-Directorates:</th>
			<td valign=top>
				<input type=text size="50" name=sub[] maxlength=50 /><br />
				<input type=text size="50" name=sub[] maxlength=50 /><br />
				<input type=text size="50" name=sub[] maxlength=50 /><br />
				<input type=text size="50" name=sub[] maxlength=50 /><br />
				<input type=text size="50" name=sub[] maxlength=50 />&nbsp;
			</td>
			<td valign=top><small>Max 50 characters</small></td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top colspan="2"><input type=submit value=" Add " /> <input type=reset value="Reset" /></td>
		</tr>
	</table>
</form>
<div style="clear:both">
    <?php displayGoBack('', ''); ?>
</div>

