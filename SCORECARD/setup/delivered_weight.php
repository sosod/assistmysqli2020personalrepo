<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('delivered_weight.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'delivered_weight.php' => 'Delivered Weight')); ?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder">
            <table id="delivered_weight_table">
                <tr>
                    <td colspan="7">
                        <input type="button" name="add_delivered_weight" id="add_delivered_weight" value="Add" />
                    </td>
                </tr>
                <tr>
                    <th>Ref</th>
                    <th>Weight</th>
                    <th>Definition</th>
                    <th>Description</th>
                    <th>Color</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("delivered_weight_logs", true)?></td>
    </tr>
</table>

