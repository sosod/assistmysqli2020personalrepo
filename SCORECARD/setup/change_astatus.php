<?php
$scripts = array("actionstatus.js");
require_once("../header.php");
$astatus = new ActionStatus();
$status  = $astatus ->  getActionStatus( $_GET['id'] );
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-actionstatus">
<table id="table_actionstatus">
	<tr>
    	<th>Ref #:</th>
    	<td><?php echo $_GET['id']; ?></td>
     </tr>
     <tr>        
    	<th>Action Status:</th>
		<td>
        	<select name="status" id="status">
            	<option value="1" <?php if(($status['status'] & 1) == 1 ){?> selected="selected" <?php } ?>>Active</option>
            	<option value="0" <?php if(($status['status'] & 1)== 0 ){?> selected="selected" <?php } ?>>Inactive</option>                
            </select>
        </td>
	</tr>  
	<tr>      
   		<td class="noborder"><?php displayGoBack("", ""); ?></td>                        
    	<td class="noborder">
        	<input type="submit" name="updatestatus" id="updatestatus" value="Update Status"  />
        	<input type="hidden" name="statusid" id="statusid" value="<?php echo $_GET['id']; ?>"  />
        </td>        
     </tr>
</table>
</form>
</div>