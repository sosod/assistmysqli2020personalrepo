<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('task_status.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'task_status.php' => 'Action Status')); ?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder">
            <table id="task_status_table">
<!--                <tr>
                    <td colspan="7">
                        <input type="button" name="add_task_status" id="add_task_status" value="Add" />
                    </td>
                </tr>-->
                <tr>
                    <th>Ref</th>
                    <th>Task Status Name</th>
                    <th>Color</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("task_status_logs", true)?></td>
    </tr>
</table>

