<?php
$scripts = array("deliveredwieght.js", "jscolor.js");
require_once("../header.php");
$dweight 		 = new DeliveredWieght();
$deliveredweight = $dweight -> getDeliveredWieght( $_GET['id']);
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="form-deliveredwieght" name="form-deliveredwieght">
<table id="table_deliveredwieght">
	<tr>
    <th>Ref #:</th>	
    <td><?php echo $_GET['id']; ?></td>    
    </tr>
    <tr>
        <th>Delivered Weight Status:</th>	    
        <td>
           <select id="status" name="status">
           	<option value="1" <?php if(($deliveredweight['status'] & 1) == 1) { ?> selected <?php } ?>>Active</option>
           	<option value="0" <?php if(($deliveredweight['status'] & 1) != 1) { ?> selected <?php } ?>>Inactive</option>            
           </select> 
        </td>
    </tr>
    <tr>
		<th></th>     
    	<td>
        <input type="submit" name="change_status" id="change_status"  value="Update" />
        <input type="hidden" name="delweightid" id="delweightid"  value="<?php echo $_GET['id']; ?>" />
        </td>                             
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>    
</table>
</form>
</div>