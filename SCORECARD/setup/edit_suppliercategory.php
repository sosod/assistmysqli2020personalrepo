<?php
$scripts = array("suppliercategory.js");
require_once("../header.php");
$assess	  = new SupplierCategory();
$suppcat  = $assess -> getSupplierCategory( $_GET['id']);
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-suppliercategory">
<table id="table_suppliercategory">
	<tr>
    	<th>Ref #:</th>
       	<td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr> 
    	<th>Short Code:</th>
    	<td><input type="text" name="shortcode" id="shortcode" value="<?php echo $suppcat['shortcode']; ?>" /></td>        
    </tr>
    <tr>
    	<th>Supplier Category Code:</th>    
    	<td><input type="text" name="name" id="name" value="<?php echo $suppcat['name']; ?>" /></td>              
    </tr>
    <tr>
    	<th>Supplier Category Description:</th>    
    	<td><textarea id="description" name="description"><?php echo $suppcat['description']; ?></textarea></td>
    </tr>        
    <tr>
        <th>Notes:</th>            
    	<td>
       		<textarea id="notes" name="notes"><?php echo $suppcat['notes']; ?></textarea>
       </td>   
    </tr>     	
	<tr>           
    	<th></th>      
    	<td>
	        <input type="submit" name="update" id="update" value="Update"  />
	       <input type="hidden" name="suppcatid" id="suppcatid" value="<?php echo $_GET['id']; ?>"  /> 
        </td>                                              
    </tr>    
    <tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td> 
    	<td class="noborder"></td> 
    </tr>
</table>
</form>
</div>