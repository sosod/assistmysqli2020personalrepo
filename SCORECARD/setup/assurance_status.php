<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('assurance_status.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'assurance_status.php' => 'Assurance Status')); ?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder">
            <table id="assurance_status_table">
                <tr>
                    <td colspan="7">
                        <input type="button" name="add_assurance_status" id="add_assurance_status" value="Add" />
                    </td>
                </tr>
                <tr>
                    <th>Ref</th>
                    <th>Name</th>
                    <th>Client Terminology</th>
                    <th>Color</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("assurance_status_logs", true)?></td>
    </tr>
</table>

