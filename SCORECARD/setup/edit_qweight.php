<?php
$scripts = array("qualityweight.js", "jscolor.js");
require_once("../header.php");
$q 			   = new QualityWeight();
$qweight = $q -> getQualityWeight( $_GET['id'] ); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="qualityweight" name="qualityweight">
<table id="table_qualityweight">
	<tr>
		<th>Ref #:</th>	
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>
		<th>Delivered Weight:</th>	    
		<td><input type="text" name="qweight" id="qweight" value="<?php echo $qweight['qweight']; ?>" /> </td>    
    </tr>
    <tr>
		<th>Delivered Weight Description:</th>
    	<td><input type="text" name="description" id="description" value="<?php echo $qweight['description']; ?>" /></td>                
    </tr>
    <tr>        
		<th>Delivered Weight Definition:</th>
		<td><textarea name="definition" id="definition"><?php echo $qweight['definition']; ?></textarea></td>                      
    </tr>
    <tr>
		<th>Color assigned to Delivered:</th>
    	<td><input type="text" name="color" id="color" class="color" value="<?php echo $qweight['color']; ?>" /></td> 
    </tr>
    <tr>   
    	<th></th>
    	<td>
    		<input type="submit" name="update" id="update"  value="Update" />
       	 	<input type="hidden" id="qualityweightid" name="qualityweightid" value="<?php echo $_GET['id']; ?>" />
        </td>                                               
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>    
</table>
</form>
</div>