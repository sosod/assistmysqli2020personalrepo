<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('glossary.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'glossary.php' => 'Glossary Rules')); ?>
<?php JSdisplayResultObj(""); ?>
<?php $category =  (isset($_GET['category']) ? $_GET['category'] : ''); ?>
<table class="noborder" width="100%" id="glossary_table">
    <tr>
       <td></td>
        <td>Select Glossary Category : &nbsp;&nbsp;&nbsp;
            <select id="category" name="category">
                <option value="">--please select--</option>
                <option value="score_card" <?php echo ($category == "score_card" ? "selected='selected'" : ""); ?>>Scorecard</option>
                <option value="deliverable" <?php echo ($category == "deliverable" ? "selected='selected'" : ""); ?>>Heading</option>
                <option value="action" <?php echo ($category == "action" ? "selected='selected'" : ""); ?>>Action</option>
            </select>
        </td>
        <td colspan="4">
            <input type="button" name="add_glossary" id="add_glossary" value="Add New" />
            <input type="hidden" name="_category" id="_category" value="<?php echo $category; ?>" />
        </td>
    </tr>
    <tr>
        <th>Ref</th>
        <th>Glossary Category</th>
        <th>Field Name</th>
        <th>Description</th>
        <th>Status</th>
        <th></th>
    </tr>
</table>
<table class="noborder" width='100%'>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("glossary_logs", true); ?></td>
    </tr>
</table>