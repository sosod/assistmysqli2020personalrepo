<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('notification_rule.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'notification_rules.php' => 'Notification Rules')); ?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder">
            <table id="notification_rule_table">
                <tr>
                    <td colspan="7">
                        <input type="button" name="add_notification_rule" id="add_notification_rule" value="Add" />
                    </td>
                </tr>
                <tr>
                    <th>Ref</th>
                    <th>Short Code</th>
                    <th>Notification Type</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("notification_rules_logs", true)?></td>
    </tr>
</table>

