<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('score_card_status.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'setup_dir.php' => ' (Sub-)Directorate responsibilities', 'setup_dir_edit.php?d='.$_GET['d'] => ' Edit (Sub-)Directorate responsibilities' )); ?>
<?php JSdisplayResultObj(""); ?>
<?php
$directorateObj = new Directorate();
$directorate   = $directorateObj->getDirectorate($_GET['d']);

$subDirectorateObj = new SubDirectorate();
$sub_directorates  = $subDirectorateObj->getSubDirectorateByDirectorateId($_GET['d']);
$sub_head = array();
foreach($sub_directorates as $dir)
{
   if($dir['subhead'] == 'Y') $sub_head = $dir;
}
?>
<script type=text/javascript>
function Validate(me) {
	return true;
}

function dirDel(d) {
	if(isNaN(parseInt(d))) {
		alert("ERROR!\nAn error occurred.  Please refresh the page and try again.");
	} else {
		if(confirm("Are you sure that you wish to delete this Directorate?\n\nThis action will also delete all associated Sub-Directorates\nand make any associated risks inaccessible.")==true) {
			document.location.href = "setup_dir.php?act=DEL&dirid="+d;
		}
	}
}
</script>
<h3>Edit Directorate</h3>
<?php //displayResColor($result); ?>
<form name=edit method=post action=setup_dir_edit.php onsubmit="return Validate(this);">
	<input type=hidden name=act value=EDIT />
	<input type=hidden name=dirid value=<?php echo $directorate['dirid']; ?>>
	<table cellpadding=3 cellspacing=0 width=650>
		<tr>
			<th width=150>Name:</th>
			<td><input type=text size="50" name=dir maxlength=50 value="<?php echo decode($directorate['dirtxt']); ?>">&nbsp;</td>
			<td width=110><small>Max 50 characters</small></td>
		</tr>
		<tr>
			<th >Primary Sub-Directorate:</th>
			<td><input type=text size="50" name=sub1 maxlength=50 value="<?php echo decode($sub_head['subtxt']); ?>">&nbsp;</td>
			<td width=110>*required </td>
		</tr>
		<tr>
			<th valign=top >Sub-Directorates:</th>
			<td valign=top colspan=2><table class=noborder >
			<?php foreach($sub_directorates as $s) : ?>
			    <tr>
                    <td class=noborder><?php echo $s['subtxt']; ?></td>
                    <td class="noborder right">&nbsp;<input type=button value=" Edit " onclick="document.location.href=setup_dir_sub_edit.php?s=<?php echo($s['subid']); ?>" /></td>
                </tr>
			<?php endforeach; ?>
				<tr>
                    <td class=noborder colspan=2>
                        <input type=button value="Add New" onclick="document.location.href='setup_dir_sub_add.php?d=<?php echo $directorate['dirid']; ?>';" />
                        <?php if(count($sub_directorates) > 1) : ?>
                           <input type=button value="Display Order" onclick="document.location.href=setup_dir_sub_order.php?d=<?php echo $directorate['dirid']; ?>';" />
                        <?php endif; ?>
                    </td>
                </tr>
			</table></td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top colspan="2">
                <input type=submit value="Save Changes" />
                <input type=reset value="Reset" /> <span style="float: right; margin-top: -1pt;">
                <input type=button value=" Delete~" onclick="dirDel(<?php echo $directorate['dirid']; ?>)" /></span>
            </td>
		</tr>
	</table>
</form>
<p style="font-size: 7.5pt; "><i>* This is an automatically generated Sub-Directorate that is required by the system and cannot be edited.<br />
~ This will delete both the Directorate and its associated Sub-Directorates.</i></p>
<div style="clear:both">
    <?php displayGoBack('', ''); ?>
</div>