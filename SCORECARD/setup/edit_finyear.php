<?php
$scripts = array("financialyear.js");
require_once("../header.php");
$fin 	  = new FinancialYear();
$finyears = $fin -> getFinYear( $_GET['id'] );
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="financialyear">
<table id="table_financialyear">
	<tr>
    	<th>Ref #:</th>
    	<td><?php echo $_GET['id']; ?></td>        
    </tr>
    <tr>
    	<th>Year Ending:</th>
    	<td><input type="text" name="lastday" id="lastday" class="datepicker" value="<?php echo $finyears['lastday']; ?>" /></td>        
    </tr>
    <tr>
    	<th>Start Day:</th>
    	<td><input type="text" name="startdate" id="startdate" class="datepicker" value="<?php echo $finyears['startdate']; ?>" /></td>        
    </tr>
    <tr>
    	<th>End Day:</th>                        
		<td><input type="text" name="enddate" id="enddate" class="datepicker" value="<?php echo $finyears['enddate']; ?>" /></td>        
    </tr>    
     <tr>  
     	<td class="noborder"><?php displayGoBack("", ""); ?></td>  
        <td class="noborder">
        <input type="submit" name="edit" id="edit" class="btnClass" value="Edit" />
		<input type="hidden" name="finid" id="finid" value="<?php echo $_GET['id']; ?>" />        
        </td>               
    </tr>
</table>
</form>
</div>