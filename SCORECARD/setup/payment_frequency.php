<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('payment_frequency.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'payment_frequency.php' => 'Contract Payment Frequency')); ?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder">
            <table id="payment_frequency_table">
                <tr>
                    <td colspan="7">
                        <input type="button" name="add_payment_frequency" id="add_payment_frequency" value="Add" />
                    </td>
                </tr>
                <tr>
                    <th>Ref</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("contract_payment_frequency_logs", true)?></td>
    </tr>
</table>

