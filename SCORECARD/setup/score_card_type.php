<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('score_card_type.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'score_card_type.php' => 'Scorecard Type')); ?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder">
            <table id="score_card_type_table">
                <tr>
                    <td colspan="7">
                        <input type="button" name="add_score_card_type" id="add_score_card_type" value="Add" />
                    </td>
                </tr>
                <tr>
                    <th>Ref</th>
                    <th>Short Code</th>
                    <th>Scorecard Type Name</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("score_card_type_logs", true)?></td>
    </tr>
</table>

