<?php
$scripts = array("contracttype.js");
require_once("../header.php");
$contacttype = new ContractType();
$cttype  = $contacttype -> getContractType( $_GET['id'] );
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-contacttype">
<table id="table_contacttype">
	<tr>
    	<th>Ref #:</th>
    	<td><?php echo $_GET['id']; ?></td>
     </tr>
	<tr>
    	<th>Short Code:</th>
    	<td><input type="text" name="shortcode" id="shortcode" value="<?php echo $cttype['shortcode']; ?>" <?php echo (($cttype['status'] & DBConnect::DEFAULTS) == DBConnect::DEFAULTS) ? 'disabled="disabled"' : ""; ?> /></td>
     </tr>     
	<tr>
    	<th>Name:</th>
    	<td><input type="text" name="name" id="name" value="<?php echo $cttype['name']; ?>" /></td>
     </tr>          
	<tr>
    	<th>Description:</th>
    	<td><textarea name="description" id="description"><?php echo $cttype['description']; ?></textarea></td>
     </tr>                 
     <tr>   
		<th></th>                     
    	<td>
        	<input type="submit" name="update" id="update" value="Update" />
        	<input type="hidden" name="typeid" id="typeid" value="<?php echo $_GET['id']; ?>"  />
        </td>        
     </tr>
     <tr>
  	     	<td class="noborder"><?php displayGoBack("", ""); ?></td>
  	     	<td class="noborder"></td>
     </tr>
</table>
</form>
</div>