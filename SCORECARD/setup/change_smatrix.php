<?php
$scripts = array("scorematrix.js");
require_once("../header.php");
$q 		  = new ScoreMatrix();
$smatrix  = $q -> getScoreMatrix( $_GET['id'] ); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="scorematrix" name="scorematrix">
<table id="table_scorematrix">
	<tr>
		<th>Ref #:</th>	
        <td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>
		<th>Delivered Score Matrix Status:</th>	    
		<td>
        	<select name="status" id="status">
            	<option value="1" <?php if(($smatrix['status'] & 1) == 1 ) {?> selected<?php } ?>>Active</option>
            	<option value="0" <?php if(($smatrix['status'] & 1) == 0 ) {?> selected<?php } ?>>InActive</option>                
            </select>        
        </td>    
    </tr>
    <tr>   
    	<th></th>
    	<td>
    		<input type="submit" name="updatestatus" id="updatestatus"  value="Update" />
        	<input type="hidden" id="scorematrixid" name="scorematrixid" value="<?php echo $_GET['id']; ?>" />
        </td>                                               
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>
</table>
</form>
</div>