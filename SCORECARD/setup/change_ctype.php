<?php
$scripts = array("contracttype.js");
require_once("../header.php");
$contacttype = new ContractType();
$cttype  = $contacttype -> getContractType( $_GET['id'] );
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-contacttype">
<table id="table_contacttype">
	<tr>
    	<th>Ref #:</th>
    	<td><?php echo $_GET['id']; ?></td>
     </tr>
     <tr>        
    	<th>Action Status:</th>
		<td>
        	<select name="status" id="status">
            	<option value="1" <?php if(($cttype['status'] & 1) == 1 ){?> selected="selected" <?php } ?>>Active</option>
            	<option value="0" <?php if(($cttype['status'] & 1)== 0 ){?> selected="selected" <?php } ?>>Inactive</option>                
            </select>
        </td>
	</tr>  
	<tr>      
   		<th></th>                      
    	<td>
        	<input type="submit" name="updatestatus" id="updatestatus" value="Update Status"  />
        	<input type="hidden" name="typeid" id="typeid" value="<?php echo $_GET['id']; ?>"  />
        </td>        
     </tr>
     <tr>
     	<td class="noborder"><?php displayGoBack("", ""); ?></td>  
     	<td class="noborder"></td>
     </tr>
</table>
</form>
</div>