<?php
$scripts = array("useraccess.js");
require_once("../header.php");
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form>
<table id="table_useraccess" class=list>
	<tr>
    	<th>Ref</th>
    	<th>User</th>
    	<!--  <th>Directorate</th>
    	<th>Sub-directorate</th>-->
    	<th>Module Admin</th>                                
    	<th>Contract Manager</th>                           
    	<th>Create Contracts</th>                           
    	<th>Create Deliverable</th>                
    	<th>Approval</th>
    	<th>View All</th>                        
    	<th>Edit All</th>        
    	<th>Reports</th>        
    	<th>Assurance</th>        
    	<th>Setup</th>        
    	<th></th>        
    </tr>
	<tr>
    	<td>#</td>
    	<td>
       	<select id="users" name="users">
        	<option value="">-users-</option>
        </select>
        </td>
    	<!-- <td>
       		<select id="directorate" name="directorate">
        		<option value="">-directorates-</option>
      	   </select>        
        </td>
    	<td>
       	<select id="sub_directorate" name="sub_directorate">
        	<option value="">-subdirectorate-</option>
        </select>        
        </td>
         -->
    	<td>
       	<select id="module_admin" name="module_admin">
  	        <option value="0">No</option>   
        	<option value="1">Yes</option>                     
        </select>                
        </td>                                
    	<td>
       	<select id="contract_manager" name="contract_manager">
  	        <option value="0">No</option>   
        	<option value="1">Yes</option>                        
        </select>                      
        </td>                           
    	<td>
       	<select id="create_contract" name="create_contract">
  	        <option value="0">No</option>   
        	<option value="1">Yes</option>                       
        </select>                    
        </td>                           
    	<td>
       	<select id="create_deliverable" name="create_deliverable">
  	        <option value="0">No</option>   
        	<option value="1">Yes</option>                       
        </select>          
        </td>                
    	<td>
       	<select id="approval" name="approval">
  	        <option value="0">No</option>   
        	<option value="1">Yes</option>                       
        </select>                  
        </td>
    	<td>
       	<select id="viewall" name="viewall">
  	        <option value="0">No</option>   
        	<option value="1">Yes</option>                       
        </select>          
        </td>                        
    	<td>
       	<select id="editall" name="editall">
  	        <option value="0">No</option>   
        	<option value="1">Yes</option>                       
        </select>         
        </td>        
    	<td>
       	<select id="reports" name="reports">
  	        <option value="0">No</option>   
        	<option value="1">Yes</option>                       
        </select>            
        </td>        
    	<td>
       	<select id="assurance" name="assurance">
  	        <option value="0">No</option>   
        	<option value="1">Yes</option>                       
        </select>         
        </td>        
    	<td>
       	<select id="setup" name="setup">
  	        <option value="0">No</option>   
        	<option value="1">Yes</option>                       
        </select>          
        </td>        
    	<td><input type="submit" name="add" id="add" value="Add" /></td>        
    </tr>    
</table>
<div><?php displayGoBack("", ""); ?></div>
</form>
</div>
