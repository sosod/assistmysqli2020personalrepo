<?php
$scripts = array("deliveredwieght.js", "jscolor.js");
require_once("../header.php");
$dweight 		 = new DeliveredWieght();
$deliveredweight = $dweight -> getDeliveredWieght( $_GET['id']);
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="form-deliveredwieght" name="form-deliveredwieght">
<table id="table_deliveredwieght">
	<tr>
    <th>Ref #:</th>	
    <td><?php echo $_GET['id']; ?></td>    
    </tr>
    <tr>
        <th>Delivered Weight From:</th>	    
        <td>
            <input type="text" name="dweight" id="dweight" size="3" value="<?php echo $deliveredweight['dweight']; ?>" /> 
        </td>
    </tr>
	<tr>    
        <th>Delivered Weight Description:</th>
        <td><input type="text" name="description" id="description" value="<?php echo $deliveredweight['description']; ?>" /></td>
    </tr>
    <tr>
        <th>Delivered Weight definition:</th>
        <td><textarea name="definition" id="definition"><?php echo $deliveredweight['definition']; ?></textarea></td>                  
    </tr>
    <tr>
        <th>Color assigned to delivered:</th>
        <td><input type="text" name="color" id="color" class="color" value="<?php echo $deliveredweight['color']; ?>" /></td>    
    </tr>       
    <tr>
    	<th></th>   
    	<td>
	        <input type="submit" name="update" id="update"  value="Update" />
	        <input type="hidden" name="delweightid" id="delweightid"  value="<?php echo $_GET['id']; ?>" />
        </td>                             
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>
</table>
</form>
</div>