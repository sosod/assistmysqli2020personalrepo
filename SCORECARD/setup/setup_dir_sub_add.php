<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('score_card_status.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'setup_dir.php' => ' (Sub-)Directorate responsibilities', 'setup_dir_edit.php?d='.$_GET['d'] => ' Edit (Sub-)Directorate responsibilities', 'setup_dir_sub_add.php?d='.$_GET['d'] => ' Add Sub-Directorate'  )); ?>
<?php JSdisplayResultObj(""); ?>
<?php
$directorateObj = new Directorate();
$directorate   = $directorateObj->getDirectorate($_GET['d']);

?>
<script type=text/javascript>
function Validate(me) {
	if(me.sub.value.length>0) {
		return true;
	} else {
		alert("Please enter a Sub-Directorate name.");
		return false;
	}
}
</script>
<h3>Add Sub-Directorate</h3>
<form name=edit method=post action=setup_dir_edit.php onsubmit="return Validate(this);">
	<input type=hidden name=act value=SUB_ADD />
	<input type=hidden name=dirid value=<?php echo $directorate['dirid']; ?>>
	<table cellpadding=3 cellspacing=0 width=650>
		<tr>
			<th width=150>Directorate:</th>
			<td><?php echo decode($directorate['dirtxt']); ?>&nbsp;</td>
			<td width=110></td>
		</tr>
		<tr>
			<th>Sub-Directorate:</th>
			<td><input type=text size="50" name=sub maxlength=50 value="">&nbsp;</td>
			<td width=110>*required </td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top colspan="2"><input type=submit value=" Save " /> <input type=reset value="Reset" /></td>
		</tr>
	</table>
</form>
<div style="clear:both">
    <?php displayGoBack('', ''); ?>
</div>