<?php
$scripts = array("glossary.js");
require_once("../header.php");
$gloss = new Glossary();
$glossary = $gloss -> getGlossary($_GET['id']);
?>
<div id="displayview" style="clear:both;">
<?php JSdisplayResultObj(""); ?>
<form name="form-glossary" id="form-glossary">
<table id="table_glossarys">
   		 <tr>
            <th>Ref #:</th>   
            <td><?php echo $_GET['id']; ?></td>  
        </tr> 
        <tr>
        	<th>Category:</th>
        	<td><input type="text" name="category" id="category" value="<?php echo $glossary['category']; ?>" /></td>        
        </tr>
        <tr>
        	<th>Terminology:</th>            
        	<td><textarea name="terminology" id="terminology"><?php echo $glossary['terminology']; ?></textarea></td>            
        </tr>
        <tr>
        	<th>Explanation:</th>
       	   <td><textarea name="explanation" id="explanation"><?php echo $glossary['explanation']; ?></textarea></td>            
        </tr>          
        <tr>
             <th></th>   
             <td>
             	<input type="submit" name="edit" id="edit" value="Update" />
             	<input type="hidden" name="glossaryid" id="glossaryid" value="<?php echo $_GET['id']; ?>" />
             </td>
        </tr> 
	    <tr>
	    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
	    		<td class="noborder"></td>
	    </tr>               
</table>
</form>
</div>