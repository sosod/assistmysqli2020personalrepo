<?php
$scripts = array("scorematrix.js", "jscolor.js");
require_once("../header.php");
$q 		  = new ScoreMatrix();
$smatrix = $q -> getScoreMatrix( $_GET['id'] ); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="scorematrix" name="scorematrix">
<table id="table_scorematrix">
	<tr>
		<th>Ref #:</th>	
        <td><?php echo $_GET['id']?></td>
    </tr>
    <tr>
		<th>Delivered Weight:</th>	    
		<td><input type="text" name="dscore" id="dscore" value="<?php echo $smatrix['dscore']; ?>" /></td>    
    </tr>
    <tr>
		<th>Delivered Weight Description:</th>
    	<td><input type="text" name="description" id="description" value="<?php echo $smatrix['description']; ?>" /></td>                
    </tr>
    <tr>        
		<th>Delivered Weight Definition:</th>
		<td><textarea name="definition" id="definition"><?php echo $smatrix['definition']; ?></textarea></td>                      
    </tr>
    <tr>
		<th>Color assigned to delivered:</th>
    	<td><input type="text" name="color" id="color" class="color" value="<?php echo $smatrix['color']; ?>" /></td> 
    </tr>
    <tr>   
    	<th></th>
    	<td>
    		<input type="submit" name="update" id="update"  value="Update" />
        	<input type="hidden" id="scorematrixid" name="scorematrixid" value="<?php echo $_GET['id']; ?>" />
        </td>                                               
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>    
</table>
</form>
</div>