<?php
$scripts = array( 'menu.js',  );
$styles = array( 'colorpicker.css','janet.css' );
$page_title = "Directorates";
require_once("../inc/header.php");
$dbref = strtolower($dbref);

$var = $_REQUEST;

$subid = $_REQUEST['s'];

if(!checkIntRef($subid)) {
	die("An error has occurred.  Please go back and try again.");
} else {
	$sql = "SELECT s.*, d.dirtxt FROM ".$dbref."_dirsub s INNER JOIN ".$dbref."_dir d ON s.subdirid = d.dirid WHERE s.subid = $subid AND s.active = true";
	include("inc_db_con.php");
		if(mysql_num_rows($rs)==0) {
			die("An error has occurred.  Please go back and try again.");
		} else {
			$sub = mysql_fetch_assoc($rs);
		}
	mysql_close($con);
}

?>
<script type=text/javascript>
function Validate(me) {
	if(me.sub.value.length>0) {
		return true;
	} else {
		alert("Please enter a Sub-Directorate name.");
		return false;
	}
}
function delSub(s) {
	if(!isNaN(parseInt(s))) {
		if(confirm("Are you sure you wish to delete this Sub-Directorate?\n\nWARNING: This action will make all risks associated with this Sub-Directorate inaccessible.")==true) {
			document.location.href = "setup_dir_edit.php?act=SUB_DEL&s="+s;
		}
	} else {
		alert("An error occurred.  Please go back and try again.");
	}
}
</script>
<h3>Edit Sub-Directorate</h3>
<form name=edit method=post action=setup_dir_edit.php onsubmit="return Validate(this);">
	<input type=hidden name=act value=SUB_EDIT />
	<input type=hidden name=subid value=<?php echo($subid); ?>>
	<table cellpadding=3 cellspacing=0 width=650>
		<tr>
			<th width=150>Directorate:</th>
			<td><?php echo decode($sub['dirtxt']); ?>&nbsp;</td>
			<td width=110></td>
		</tr>
		<tr>
			<th>Sub-Directorate - Original Name:</th>
			<td><?php echo decode($sub['subtxt']); ?>&nbsp;</td>
			<td width=110></td>
		</tr>
		<tr>
			<th>Sub-Directorate - New Name:</th>
			<td><input type=text size="50" name=sub maxlength=50 value="<?php echo decode($sub['subtxt']); ?>">&nbsp;</td>
			<td width=110>*required </td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top colspan="2"><input type=submit value="Save Changes" /> <input type=reset value="Reset" /> <span style="float: right; margin-top: -15pt;"><input type=button value=" Delete" onclick="delSub(<?php echo $subid;?>);" /></span></td>
		</tr>
	</table>
</form>
<?php
$urlback = "setup_dir_edit.php?d=".$sub['subdirid'];
include("inc_goback.php");
?>
