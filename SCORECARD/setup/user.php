<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('user.js')); ?>
<?php display_menu_list('setup', 'user'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'user.php' => 'User Access Settings')); ?>
<?php $user_obj     = new User();; ?>
<?php $user_list    = $user_obj->getUsers(); ?>
<?php $user_listing = $user_obj->getUserSettings(); ?>
<?php JSdisplayResultObj(""); ?>
<form id="user_form">
    <table id="user_table" class="list" width="100%">
        <tr>
            <th>Ref</th>
            <th>User</th>
            <th>Module Admin</th>
            <th>Create Scorecard</th>
            <th>Create Heading</th>
            <th>Update All</th>
            <th>Edit All</th>
            <th>Reports</th>
            <th>Assurance</th>
            <th>Scorecard Admin</th>
            <th>Setup</th>
            <th></th>
        </tr>
        <tr>
            <td>#</td>
            <td>
                <select id="user_id" name="user_id">
                    <option value="">--please select--</option>>
                </select>
            </td>
            <td>
                <select id="module_admin" name="status[module_admin]">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </td>
            <td>
                <select id="create_score_card" name="status[create_score_card]">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </td>
            <td>
                <select id="create_deliverable" name="status[create_deliverable]">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </td>
            <td>
                <select id="update_all" name="status[update_all]">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </td>
            <td>
                <select id="edit_all" name="status[edit_all]">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </td>
            <td>
                <select id="reports" name="status[reports]">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </td>
            <td>
                <select id="assurance" name="status[assurance]">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </td>
            <td>
                <select id="score_card_admin" name="status[score_card_admin]">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </td>
            <td>
                <select id="setup" name="status[setup]">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </td>
            <td><input type="button" name="add" id="add" value="Add" /></td>
        </tr>
    </table>
    <div><?php displayGoBack("", ""); ?></div>
</form>