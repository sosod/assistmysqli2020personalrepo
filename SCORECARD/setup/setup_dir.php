<?php error_reporting(-1);
require_once("../../library/class/assist_helper.php");
require_once("../../library/class/assist.php");
require_once("../../library/class/assist_dbconn.php");
require_once("../../library/class/assist_db.php");

//DEVELOPMENT CODE



//HEADING
?>


<?php @include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array()); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'setup_dir.php' => 'Scorecard Owner')); ?>


<?php
$User = new User();
$users = $User->getUsers();

$ContractOwner = new ScoreCardOwner();


if(isset($_REQUEST['act'])) {
	$u = $_REQUEST['u'];
	$s = $_REQUEST['sub'];
	switch($_REQUEST['act']) {
		case "ADD":
			$r = $ContractOwner->addOwner($s,$u);
			break;
		case "DEL":
			$r = $ContractOwner->deleteOwner($s,$u);
			break;
		case "RESTORE":
			$r = $ContractOwner->restoreOwner($s,$u);
			break;
	}
}








$dirs = $ContractOwner->getAll();
$admins = $ContractOwner->getEveryAdmin();
//ASSIST_HELPER::arrPrint($dirs);
//ASSIST_HELPER::arrPrint($admins);
?>
<?php ASSIST_HELPER::displayResult((isset($r) && is_array($r) ? $r : array())); ?>
<?php ASSIST_HELPER::displayResult(array("info","Please note: The list of Directorates and Sub-directorates come from the Master List which can only be amended by the Assist Administrator.  To change an item on this list, please contact your Assist Administrator.")); ?>
<style type=text/css>
	table.list th { text-align: center; }
</style>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder">

			<table class=list width=625 id=tbl_owner>
			<tr><th width=25>Ref</th><th width=350>Sub-Directorate</th><th width=250>Scorecard Owner(s)</th></tr>
			<?php 
			foreach($dirs as $d) {
				$sub_id = $d['subid'];
				$a = isset($admins[$sub_id]) ? $admins[$sub_id] : array();
				$rows = 1 + count($a);
				echo "
					<tr>
						<td rowspan=".$rows." class='center b'>".$sub_id."</td>
						<td rowspan=".$rows." class=b id=td_sub_".$sub_id.">".$d['dirtxt']."</td>
						";
				foreach($a as $ad) {
					if($ad['active']==true) {
						echo "<td><span id=admin_".$sub_id."_".$ad['tkid'].">".$ad['user']."</span> <span class=float style='margin-left: 10px;'><input type=button value=Deactivate class=del_owner ref='".$sub_id."' user='".$ad['tkid']."' /></span></td>";
					} else {
						echo "<td style='background-color: #efefef; color: #898989;'><span id=admin_".$sub_id."_".$ad['tkid'].">".$ad['user']."</span> <span class=float style='margin-left: 10px;'><input type=button value=Restore class=restore_owner ref='".$sub_id."' user='".$ad['tkid']."' /></span></td>";
					}
					echo "</tr><tr>";
				}
				echo "
					<td class=right><input type=button value=Add class=add_owner id=add_".$sub_id." ref=".$sub_id." /></td>
					</tr>";
			}
			?>
			</table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("index.php", ''); ?></td>
        <td class="noborder"><?php displayAuditLogLink("dir_admin_logs", true)?></td>
    </tr>
</table>


<div id=dlg_add title='Add User'><input type=hidden id=sub_id value='' />
<table class=form style='margin: 10px 10px 10px 10px'>
	<tr><th width=110>Sub-Directorate:</th><td id=td_sub></td></tr>
	<tr><th>Scorecard Owner:</th><td id=td_user><select id=sel_user><option value=X selected>--- SELECT ---</option><?php foreach($users as $u) { echo "<option value=".$u['tkid'].">".ASSIST_HELPER::decode($u['tkname']." ".$u['tksurname'])."</option>"; } ?></select></td></tr>
	<tr><th></th><td id=td_btn><input type=button id=add_owner value=Save class=isubmit /></td></tr>
</table>
</div>

<script type=text/javascript>
$(function() {
	$("#dlg_add").dialog({
		autoOpen: false,
		modal: true,
		width: "auto"
	});
	$("input:button.add_owner").click(function() {
		var i = $(this).attr("ref");
		$("#dlg_add #sub_id").val(i);
		var s = $("#tbl_owner #td_sub_"+i).html();
		$("#dlg_add  #td_sub").html(s);
		$("#dlg_add").dialog("open");
	});
	$("#dlg_add #add_owner").click(function() {
		var u = $("#dlg_add #sel_user").val();
		var s = $("#dlg_add #sub_id").val();
		if(u=="X") {
			alert("Please select the user's name from the drop down list before clicking 'Save'.");
		} else {
			document.location.href = "setup_dir.php?act=ADD&sub="+s+"&u="+u;
		}
	});
	$("input:button.del_owner").click(function() {
		var s = $(this).attr("ref");
		var u = $(this).attr("user");
		var tu = $("#tbl_owner #admin_"+s+"_"+u).html();
		var ts = $("#tbl_owner #td_sub_"+s).html();
		if(confirm("Are you sure you wish to deactivate Score Card Owner access of '"+tu+"' to "+ts+"?")==true) {
			document.location.href = "setup_dir.php?act=DEL&sub="+s+"&u="+u;
		}
	});
	$("input:button.restore_owner").click(function() {
		var s = $(this).attr("ref");
		var u = $(this).attr("user");
		var tu = $("#tbl_owner #admin_"+s+"_"+u).html();
		var ts = $("#tbl_owner #td_sub_"+s).html();
		if(confirm("Are you sure you wish to restore Score Card Owner access of '"+tu+"' to "+ts+"?")==true) {
			document.location.href = "setup_dir.php?act=RESTORE&sub="+s+"&u="+u;
		}
	});
});
</script>