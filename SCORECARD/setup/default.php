<?php
$scripts = array("setup/defaults.js");
require_once("../header.php");
?>
<div class="displayview">
<table border="1" width="60%" cellpadding="5" cellspacing="0" id="link_to_page">
    <tr>
    	<th>Menu and Headings:</th>
        <td>Define the Menu headings to be displayed in the module</td>
        <td><input type="button" name="menu" id="menu" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Naming Convention:</th>
        <td>Define the Naming convention of the SLA fieds for the module </td>
        <td><input type="button" name="naming" id="naming" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Financial Years:</th>
        <td>Configure the Financial years</td>
        <td><input type="button" name="financial_year" id="financial_year" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Contract Status:</th>
        <td>Configure the Contract Status</td>
        <td><input type="button" name="contract_status" id="contract_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Deliverable Status:</th>
        <td>Define Deliverable Status</td>
        <td><input type="button" name="deliverable_status" id="deliverable_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Action Status:</th>
        <td>Configure the Action Status</td>
        <td><input type="button" name="actions_tatus" id="action_status" value="Configure" class="setup_defaults"  /></td>
    </tr> 
    <tr>
    	<th>Task Status:</th>
        <td>Configure the Task status</td>
        <td><input type="button" name="task_status" id="task_status" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Contract Type:</th>
        <td>Configure the Contract Type</td>
        <td><input type="button" name="contract_type" id="contract_type" value="Configure" class="setup_defaults"  /></td>
    </tr>    
     <tr>
    	<th>Contract Category:</th>
        <td>Configure the Contract Category</td>
        <td><input type="button" name="contract_category" id="contract_category" value="Configure" class="setup_defaults"  /></td>
    </tr>
     <tr>
    	<th>Deliverable Category:</th>
        <td>Configure the Deliverable Category</td>
        <td><input type="button" name="deliverable_category" id="deliverable_category" value="Configure" class="setup_defaults"  /></td>
    </tr>    
    <tr>
    	<th>Assessment Frequency:</th>
        <td>Define the Assessment Frequency</td>
        <td><input type="button" name="assessment_frequency" id="assessment_frequency" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Supplier Categories:</th>
        <td>Define the Supplier Categories</td>
        <td><input type="button" name="supplier_category" id="supplier_category" value="Configure" class="setup_defaults"  /></td>
    </tr>    
    <tr>
    	<th>Suppliers:</th>
        <td>Define the Supplier</td>
        <td><input type="button" name="supplier" id="supplier" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Delivered Weight:</th>
        <td>Define the Delivered Weight</td>
        <td><input type="button" name="delivered_wieght" id="delivered_wieght" value="Configure"  class="setup_defaults" /></td>
    </tr>
    <tr>
    	<th>Define Quality Weight:</th>
        <td>Define Quality Weight</td>
        <td><input type="button" name="quality_weight" id="quality_weight" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Define Other Weight:</th>
        <td>Define Other Weight </td>
        <td><input type="button" name="other_weight" id="other_weight" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Define Delivered Score Matrix:</th>
        <td>Define Delivered Score Matrix</td>
        <td><input type="button" name="score_matrix" id="score_matrix" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Define Qaulity Score Matrix:</th>
        <td>Define Quality Score Matrix</td>
        <td><input type="button" name="quality_score" id="quality_score" value="Configure" class="setup_defaults"  /></td>
    </tr> 
	<tr>
    	<th>Define Other Score Matrix:</th>
        <td>Define Other Score Matrix</td>
        <td><input type="button" name="other_score" id="other_score" value="Configure"  class="setup_defaults" /></td>
    </tr>
    <tr>
    	<th>Define Notification Rules:</th>
        <td>Define Notification Rules</td>
        <td><input type="button" name="notifcation_rules" id="notifcation_rules" value="Configure" class="setup_defaults"  /></td>
    </tr>
    <tr>
    	<th>Glossary:</th>
        <td>Define Glossary Terms</td>
        <td><input type="button" name="glossary" id="glossary" value="Configure" class="setup_defaults"  /></td>
    </tr>        
    <tr>
    	<th>List Coloums:</th>
        <td>Configure which columns dipslay on the Manage pages</td>
        <td><input type="button" name="columns" id="columns" value="Configure" class="setup_defaults"  /></td>
    </tr>    
</table>
</div>