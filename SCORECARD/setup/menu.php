<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('menu.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'menu.php' => 'Menu')); ?>
<?php JSdisplayResultObj(""); ?>
<table id="menu_table">
    <tr>
        <td colspan="5" class="gray">
            <p>
                <b>Define the naming convention {Purpose : Client can define own naming convention}</b>
            </p>
        </td>
    </tr>
    <tr>
        <th>Ref</th>
        <th>Scorecard Assist Terminology</th>
        <th>Section</th>
        <th>Client Terminology</th>
        <th></th>
    </tr>
</table>
<div>
    <?php displayGoBack("",""); ?>
    <?php //displayAuditLogLink("menu_logs", true); ?>
</div>

