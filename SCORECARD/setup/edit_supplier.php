<?php
$scripts = array("supplier.js");
require_once("../header.php");
$sup 	  = new Supplier();
$supplier = $sup -> getSupply( $_GET['id']);

$suppcat   = new SupplierCategory();
$supcats  = $suppcat -> getSupplierCategories(); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post" id="form-contractcategory">
<table id="table_contractcategory">
	<tr>
    	<th>Ref #:</th>
       	<td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr> 
    	<th>Short Code:</th>
    	<td><input type="text" name="shortcode" id="shortcode" value="<?php echo $supplier['shortcode']; ?>" /></td>        
    </tr>
    <tr>
    	<th>Supplier Code:</th>    
    	<td><input type="text" name="supplier_code" id="supplier_code" value="<?php echo $supplier['supplier_code']; ?>" /></td>              
    </tr>
    <tr>
    	<th>Name of Supplier:</th>    
    	<td><input type="text" name="name" id="name" value="<?php echo $supplier['name']; ?>" /></td>
    </tr>        
    <tr>
        <th>Supplier Category:</th>            
    	<td>
        <select id="category" name="category">
      	<?php
			foreach( $supcats as $index => $supcat ){
		?>
        	<option value="<?php echo $supcat['id']; ?>" <?php if($supplier['category'] == $supcat['id']) {?> selected <?php } ?>><?php echo $supcat['name']; ?></option>        
        <?php
			}
		 ?>  	
        </select>
       </td>   
    </tr>     	
	<tr>   
    	<th></th>             
    	<td>
        	<input type="submit" name="updatesupplier" id="updatesupplier" value="Update"  />
       		<input type="hidden" name="supplierid" id="supplierid" value="<?php echo $_GET['id']; ?>"  /> 
        </td>                                              
    </tr>    
    <tr>
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>   
    	<td class="noborder"></td>
    </tr>
</table>
</form>
</div>