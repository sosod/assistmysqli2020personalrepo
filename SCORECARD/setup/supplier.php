<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('supplier.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'supplier.php' => 'Contract Supplier')); ?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder">
            <table id="supplier_table">
                <tr>
                    <td colspan="7">
                        <input type="button" name="add_supplier" id="add_supplier" value="Add" />
                    </td>
                </tr>
                <tr>
                    <th>Ref</th>
                    <th>Short Code</th>
                    <th>Supplier Code</th>
                    <th>Supplier Name</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("supplier_logs", true)?></td>
    </tr>
</table>

