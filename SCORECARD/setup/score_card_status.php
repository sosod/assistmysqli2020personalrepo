<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('score_card_status.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'score_card_status.php' => 'Scorecard Status')); ?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder">
            <table id="score_card_status_table">
<!--                <tr>
                    <td colspan="7">
                        <input type="button" name="add_score_card_status" id="add_score_card_status" value="Add" />
                    </td>
                </tr>-->
                <tr>
                    <th>Ref</th>
                    <th>Name</th>
                    <th>Client Terminology</th>
                    <th>Color</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("score_card_status_logs", true)?></td>
    </tr>
</table>

