<?php
$scripts = array("deliverablestatus.js");
require_once("../header.php");
$dstatus = new DeliverableStatus();
$status  = $dstatus -> getDelStatus( $_GET['id'] );
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form method="post">
<table id="table_contractstatus">
	<tr>
    	<th>Ref #:</th>
    	<td><?php echo $_GET['id']; ?></td>
    </tr>
    <tr>           
    	<th>Change Status:</th>
		<td>
        	<select id="status" name="status">
            	<option value="1" <?php if(($status['status'] & 1) == 1) {?> selected="selected"<?php } ?>>Active</option>
                <option value="0" <?php if(($status['status'] & 1) == 0) {?> selected="selected"<?php } ?>>Inactive</option>
            </select>
        </td>        
   </tr>
	<tr>	
    	<td class="noborder"><?php displayGoBack("", ""); ?></td>
    	<td class="noborder">
        	<input type="submit" name="change_status" id="change_status" value="Update"  />
        	<input type="hidden" name="delid" id="delid" value="<?php echo $_GET['id']; ?>"  />            
        </td>                                        
    </tr>    
</table>
</form>
</div>