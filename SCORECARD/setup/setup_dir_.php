<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('score_card_status.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'setup_dir.php' => ' (Sub-)Directorate responsibilities')); ?>
<?php JSdisplayResultObj(""); ?>
<?php
$directorateObj = new Directorate();
$directorates   = $directorateObj->getAll();

$subDirectorateObj = new SubDirectorate();
?>

<?php //displayResColor($result); ?>
<table cellpadding=3 cellspacing=0 width=650>
    <tr>
        <th class=center width=30>Ref</th>
        <th class=center >Name</th>
        <th class=center >Sub-Directorates</th>
        <th class=center width=50>&nbsp;</th>
    </tr>
    <?php if(empty($directorates)) : ?>
        <tr>
            <td colspan=4>No Directorates available.  Please click "Add New" to add Directorates.</td>
        </tr>
    <?php else: ?>
        <?php foreach($directorates as $directorate): ?>
         </tr>
                <th class=center><?php echo($directorate['dirid']); ?></th>
                <td><b><?php echo($directorate['dirtxt']); ?></b></td>
                <td>
                    <ul style="margin: 2 0 0 40;">
                        <?php $sub_directorates = $subDirectorateObj->getSubDirectorateByDirectorateId($directorate['dirid']) ?>
                        <?php foreach($sub_directorates as $sub_directorate) : ?>
                            <li><?php echo ($sub_directorate['subhead'] == 'Y' ? '*' : '').$sub_directorate['subtxt']; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </td>
                <td align="center" valign="top">
                    <input type=button value="Edit Directorate" onclick="editDir(<?php echo $directorate['dirid']; ?>)"><br />
                    <input type=button value="Edit Administrators" onclick="document.location.href = 'setup_admin_dir_config.php?d=<?php echo $directorate['dirid']; ?>';" />
                </td>
         </tr>
            <?php endforeach; ?>
        <?php endif; ?>
</table>
<p>
    <input type=button value="Add New" onclick="document.location.href = 'setup_dir_add.php';">
    <?php if(count($directorates) > 0) : ?>
       <input type=button value="Display Order" onclick="document.location.href = 'setup_dir_order.php';">
    <?php endif; ?>
</p>
<div style="clear:both">
    <?php displayGoBack('', ''); ?>
    <?php displayAuditLogLink("dir_admins_logs", true); ?>
</div>
<script type=text/javascript>
    function editDir(id)
    {
        var err = "N";
        if(!isNaN(parseInt(id)) && escape(id) == id)
        {
            id = parseInt(id);
            if(id>0)
            {
                document.location.href = "setup_dir_edit.php?d="+id;
            }
            else
            {
                err = "Y";
            }
        }
        else
        {
            err = "Y";
        }

        if(err == "Y")
            alert("An error has occurred.\nPlease reload the page and try again.");
    }
    function editSub(id) {
        var err = "N";
        if(!isNaN(parseInt(id)) && escape(id) == id)
        {
            id = parseInt(id);
            if(id>0)
            {
                document.location.href = "setup_dir_sub.php?d="+id;
            }
            else
            {
                err = "Y";
            }
        }
        else
        {
            err = "Y";
        }

        if(err == "Y")
            alert("An error has occurred.\nPlease reload the page and try again.");
    }
</script>