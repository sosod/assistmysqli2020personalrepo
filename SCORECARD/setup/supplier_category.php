<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('supplier_category.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'supplier_category.php' => 'Contract Supplier Category')); ?>
<?php JSdisplayResultObj(""); ?>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder">
            <table id="supplier_category_table">
                <tr>
                    <td colspan="7">
                        <input type="button" name="add_supplier_category" id="add_supplier_category" value="Add" />
                    </td>
                </tr>
                <tr>
                    <th>Ref</th>
                    <th>Short Code</th>
                    <th>Supplier Category Name</th>
                    <th>Description</th>
                    <th>Notes</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("supplier_category_logs", true)?></td>
    </tr>
</table>

