<?php
$scripts = array("qualityscore.js", "jscolor.js");
require_once("../header.php");
?>
<div class="displayview">
<span id="loading"></span>
<form id="qualityscore" name="qualityscore">
<table id="table_qualityscore">
	<tr>
		<th>Id</th>	
		<th>Quality Score </th>	    
		<th>Quality Score Description</th>
		<th>Quality Score Definition</th>
		<th>Color assigned to quality score</th>
		<th></th>                        	           
		<th></th>
		<th></th>                 
    </tr>
    <tr>
    	<td>#</td>
    	<td><input type="text" name="qscore" id="qscore" value="" /></td> 
    	<td><input type="text" name="description" id="description" value="" /></td>                
    	<td><textarea name="definition" id="definition" value=""></textarea></td>              
    	<td><input type="text" name="color" id="color" class="color" value="" /></td>    
    	<td><input type="submit" name="save" id="save"  value="Add" /></td>                             
    	<td></td> 
    	<td></td>                       
    </tr>
</table>
</form>
</div>