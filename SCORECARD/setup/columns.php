<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('columns.js')); ?>
<?php display_menu_list('setup'); ?>
<?php display_sub_menu_list('setup', 'columns'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'columns.php' => 'Scorecard Column Display Configuration')); ?>
<?php $columnsObj = new ScoreCardColumn(); ?>
<?php $columns    = $columnsObj->getColumns(); ?>
<?php JSdisplayResultObj(""); ?>
<form id="columns_sortable">
    <table id="sortable">
        <tr>
            <th>Scorecard Assist Terminology</th>
            <th>Display On New</th>
            <th>Display On Manage</th>
        </tr>
        <?php foreach($columns as $index => $column): ?>
            <tr>
                <td><?php echo $column['score_card_terminology']; ?></td>
                <td>
                    <input type="hidden" id="columns[<?php echo $column['id']; ?>]" name="columns[<?php echo $column['id']; ?>]" value="<?php echo $column['id']; ?>"  />
                    <input type="checkbox" id="new_<?php echo $column['id']; ?>" name="new[<?php echo $column['id']; ?>]" value="<?php echo $column['id']; ?>"  <?php echo (($column['status'] & 4) == 4 ?  "checked='checked'" : '' ); ?>  />
                </td>
                <td>
                    <input type="checkbox" id="manage_<?php echo $column['id']; ?>" name="manage[<?php echo $column['id']; ?>]" value="<?php echo $column['id']; ?>"  <?php echo (($column['status'] & 8) == 8 ?  "checked='checked'" : '' ); ?>  />
                </td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="3">
                <input type="submit" id="save_changes" name="save_changes" value="Save Changes"  />
            </td>
        </tr>
    </table>
</form>
<div>
    <?php displayGoBack("",""); ?>
</div>

