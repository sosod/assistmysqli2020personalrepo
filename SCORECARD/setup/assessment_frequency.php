<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('assessment_frequency_.js')); ?>
<?php display_menu_list('setup'); ?>
<?php $scripts = array('assessment_frequency.js');  ?>
<?php page_navigation_links(array('index.php' => 'Default', 'assessment_frequency.php' => 'Assessment Frequency'));
      display_flash('assessment_frequency');
      JSdisplayResultObj("");

$financialYearObj = new FinancialYear();
$financial_years  = $financialYearObj->getList(true);
?>
<table class="noborder">
    <tr>
        <td colspan="2" class="noborder gray" style="text-align:center;">
            <?php echo select_tag('financial_year_id', $financial_years, array('id' => 'financial_year_id')); ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="noborder">
            <table id="table_assessment_frequency">
                <tr id="add_assessment_frequency">
                    <td colspan="5">
                        <input type="button" name="add_new" id="add_new" value="Add New" />
                        <input type="hidden" name="financial_year_id" id="financial_year_id" value="" />
                    </td>
                </tr>
                <tr>
                    <th>Ref</th>
                    <th>Frequency Name</th>
                    <th>Number of Evaluation Periods</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                <tr>
                    <td colspan="5" class="assessment_frequencies">
                        Select the financial year to view the assessment frequencies
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="noborder"><?php displayGoBack("",""); ?></td>
        <td class="noborder"><?php displayAuditLogLink("list_assessment_frequency_logs", true)?></td>
    </tr>
</table>
