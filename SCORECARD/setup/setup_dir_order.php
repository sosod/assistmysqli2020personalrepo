<?php include_once(__DIR__.'/../includes.php'); ?>
<?php include_js(array('score_card_status.js')); ?>
<?php display_menu_list('setup'); ?>
<?php page_navigation_links(array('index.php' => 'Setup', 'setup_dir.php' => ' (Sub-)Directorate responsibilities', 'setup_dir_order.php' => ' (Sub-)Directorate Order' )); ?>
<?php JSdisplayResultObj(""); ?>
<?php
$directorateObj = new Directorate();
$dir   = $directorateObj->getOrderedDirectorates();
$d = (isset($row['d']) && is_numeric($row['d'])) ? $row['d']+1 : 1;
?>
<script type=text/javascript>
function Validate(me) {
	return true;
}
$(function(){
	$( "#sortable" ).sortable({
		placeholder: "ui-state-highlightsort"
	});
	$( "#sortable" ).disableSelection();
});
</script>
<h3>Order Directorates</h3>
<form name=edit method=post action=setup_dir.php onsubmit="return Validate(this);">
	<input type=hidden name=act value=ORDER />
	<table cellpadding=3 cellspacing=0 width=650>
		<tr>
			<th valign=top width=100>Directorates:</th>
			<td valign=top><ul id=sortable>
			<?php foreach($dir as $d) : ?>
			    <li class="ui-state-default" style="cursor:hand;">
                    <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>&nbsp;
                    <input type=hidden name=sort[] value="<?php echo $d['dirid']; ?>"><?php echo $d['dirtxt']; ?>
                </li>
            <?php endforeach; ?>
			</ul></td>
		</tr>
		<tr>
			<th valign=top>&nbsp;</th>
			<td valign=top><input type=submit value="Save Changes" /> <input type=reset value="Reset" /></td>
		</tr>
	</table>
</form>
<div style="clear:both">
    <?php displayGoBack('', ''); ?>
</div>