<?php
$scripts = array("qualityscore.js", "jscolor.js");
require_once("../header.php");
$q 		= new QualityScore();
$qscore = $q -> getQualityScore( $_GET['id'] ); 
?>
<div class="displayview">
<?php JSdisplayResultObj(""); ?>
<form id="qualityscore" name="qualityscore">
<table id="table_qualityscore">
	<tr>
		<th>Ref #:</th>	
        <td><?php echo $_GET['id']?></td>
    </tr>
    <tr>
		<th>Quality Score:</th>	    
		<td><input type="text" name="qscore" id="qscore" value="<?php echo $qscore['qscore']; ?>" /></td>    
    </tr>
    <tr>
		<th>Quality Score Description:</th>
    	<td><input type="text" name="description" id="description" value="<?php echo $qscore['description']; ?>" /></td>                
    </tr>
    <tr>        
		<th>Quality Score Definition:</th>
		<td><textarea name="definition" id="definition"><?php echo $qscore['definition']; ?></textarea></td>                      
    </tr>
    <tr>
		<th>Color assigned to quality score:</th>
    	<td><input type="text" name="color" id="color" class="color" value="<?php echo $qscore['color']; ?>" /></td> 
    </tr>
    <tr>   
    	<th></th>
    	<td>
    		<input type="submit" name="update" id="update"  value="Update" />
       	 	<input type="hidden" id="qualityscoreid" name="qualityscoreid" value="<?php echo $_GET['id']; ?>" />
        </td>                                               
    </tr>
    <tr>
    		<td class="noborder"><?php displayGoBack("", ""); ?></td>  
    		<td class="noborder"></td>
    </tr>    
</table>
</form>
</div>