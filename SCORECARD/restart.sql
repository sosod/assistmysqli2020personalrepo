TRUNCATE `assist_admire1_contr_action`;
TRUNCATE `assist_admire1_contr_action_assurance`;
TRUNCATE `assist_admire1_contr_action_assurance_document`;
TRUNCATE `assist_admire1_contr_action_assurance_logs`;
TRUNCATE `assist_admire1_contr_action_authorization`;
TRUNCATE `assist_admire1_contr_action_authorization_document`;
TRUNCATE `assist_admire1_contr_action_authorization_logs`;
TRUNCATE `assist_admire1_contr_action_document`;
TRUNCATE `assist_admire1_contr_action_edit_logs`;
TRUNCATE `assist_admire1_contr_action_update_logs`;
TRUNCATE `assist_admire1_contr_contract`;
TRUNCATE `assist_admire1_contr_contract_document`;
TRUNCATE `assist_admire1_contr_contract_assessment`;
TRUNCATE `assist_admire1_contr_contract_assurance`;
TRUNCATE `assist_admire1_contr_contract_assurance_document`;
TRUNCATE `assist_admire1_contr_contract_assurance_logs`;
TRUNCATE `assist_admire1_contr_contract_authorization`;
TRUNCATE `assist_admire1_contr_contract_authorization_document`;
TRUNCATE `assist_admire1_contr_contract_budget_adjustment`;
TRUNCATE `assist_admire1_contr_contract_budget_adjustment_document`;
TRUNCATE `assist_admire1_contr_contract_budget_adjustment_logs`;
TRUNCATE `assist_admire1_contr_contract_payment`;
TRUNCATE `assist_admire1_contr_contract_payment_document`;
TRUNCATE `assist_admire1_contr_contract_supplier_budget`;
TRUNCATE `assist_admire1_contr_contract_supplier_payment`;
TRUNCATE `assist_admire1_contr_contract_template`;
TRUNCATE `assist_admire1_contr_contract_template_logs`;
TRUNCATE `assist_admire1_contr_contract_template_setting`;
TRUNCATE `assist_admire1_contr_contract_template_setting_logs`;
TRUNCATE `assist_admire1_contr_contract_update_logs`;
TRUNCATE `assist_admire1_contr_deliverable`;
TRUNCATE `assist_admire1_contr_deliverable_assessment`;
TRUNCATE `assist_admire1_contr_deliverable_assurance`;
TRUNCATE `assist_admire1_contr_deliverable_assurance_document`;
TRUNCATE `assist_admire1_contr_deliverable_authorization`;
TRUNCATE `assist_admire1_contr_deliverable_authorization_document`;
TRUNCATE `assist_admire1_contr_deliverable_authorization_logs`;
TRUNCATE `assist_admire1_contr_deliverable_document`;
TRUNCATE `assist_admire1_contr_deliverable_edit_logs`;
TRUNCATE `assist_admire1_contr_deliverable_update_logs`;
TRUNCATE `assist_admire1_contr_contract_supplier`;
TRUNCATE `assist_admire1_contr_contract_deliverable_status`;

