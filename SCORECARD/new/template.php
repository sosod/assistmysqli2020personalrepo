<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.ui.contract.js')); ?>
<?php display_menu_list('new', 'template'); ?>
<?php
page_navigation_links(array('template.php' => 'Template'));
display_flash('template');
JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#contract").contract({makeTemplate:true, section:"new", page:"template"});
    });
</script>
<div id="contract"></div>