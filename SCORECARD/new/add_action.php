<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('action.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('new', 'action'); ?>
<?php
$page_title = "Add Deliverable";
$columnsObj = new ActionColumns();
$columns    = $columnsObj->getHeaders();


$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$deliverableObj = new Deliverable();
$deliverable       = $deliverableObj->getDeliverableDetail($_GET['id']);

$scoreCardObj = new ScoreCard();
$score_card   = $scoreCardObj->getScoreCardDetail($deliverable['score_card_id']);

$userObj = new User();
$action_owners = $userObj->getList(true);

$rating_scale_types = RatingScale::getRatingScaleTypesList();

?>
<?php page_navigation_links(array('action.php' => 'Scorecard', 'add_score_card_action.php?id='.$deliverable['score_card_id'] => 'Add Questions', 'add_action.php?id='.$_GET['id'] => 'Add Question'));
display_flash('action');
JSdisplayResultObj("");
?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2"><h4>Heading Details</h4></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_id']; ?>:</th>
                    <td><?php echo $deliverable['id']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_name']; ?>:</th>
                    <td><?php echo $deliverable['name']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['description']; ?>:</th>
                    <td><?php echo $deliverable['description']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_category']; ?>:</th>
                    <td><?php echo $deliverable['deliverable_category']; ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['main_deliverable']; ?>:</th>
                    <td><?php echo ($deliverable['is_main_deliverable'] ? 'Yes' : 'No'); ?></td>
                </tr>
                <tr>
                    <th><?php echo $deliverable_columns['deliverable_type']; ?>:</th>
                    <td><?php echo ($deliverable['type_id'] == 0 ? 'Main-Heading' : 'Sub-Heading'); ?></td>
                </tr>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <form  method="post"  name="action-form" id="action-form" enctype="multipart/form-data">
                <table width="100%">
                    <tr>
                        <td colspan="2"><h4>Add New Question</h4></td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('action_name', $columns['action_name']); ?>:
                        </th>
                        <td>
                            <?php echo textarea_tag('action[name]', null, array('id' => 'name')) ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('action_deliverable', $columns['action_deliverable']); ?>:
                        </th>
                        <td>
                            <?php echo textarea_tag('action[deliverable]', null, array('id' => 'deliverable')) ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('action_rating_scale_type', $columns['rating_scale_type']); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('action[rating_scale_type]', $rating_scale_types, array('id' => 'rating_scale_type')) ?>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <input type="submit" name="save" id="save" value="Save" class="save_action isubmit" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="hidden" name="action[deliverable_id]" id="deliverable_id" value="<?php echo $_GET['id']?>" />
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>
<script language="javascript">
    $(function(){
        $("#action_list").action({editAction: true, deliverableId: <?php echo $_GET['id']; ?>, financialYear: <?php echo $score_card['financial_year_id']; ?> , section: "new", allowFilter: false});
    });
</script>
<div id="action_list"></div>
