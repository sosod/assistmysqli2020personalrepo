<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('score_card.js', 'ajaxfileupload.js')); ?>
<?php display_menu_list('new'); ?>
<?php $page_valid = display_sub_menu_list('new', 'score_card'); ?>
<?php

$columnsObj = new Naming();
$columns    = $columnsObj->getHeaders();

$typeObj        = new ScoreCardType();
$score_card_types = $typeObj->getList(true);

$categoryObj    = new ScoreCardCategory();
$score_card_categories = $categoryObj->getList(true);

$statusObj         = new ScoreCardStatus();
$score_card_statuses = $statusObj->getList(true);

$userObj = new User();

$create_score_card_users  = $userObj->getList(true, User::CREATE_SCORE_CARD);
$create_deliverable_users = $userObj->getList(true, User::CREATE_DELIVERABLE);
$score_card_users         = $userObj->getList(true);


$financialYearObj = new FinancialYear();
$financial_years  = $financialYearObj->getList(true);

$ownerObj  = new ScoreCardOwner();
$score_card_owners = $ownerObj->getList(true);

page_navigation_links(array('index.php' => 'Create'));
display_flash('score_card');
JSdisplayResultObj("");
if($page_valid)
{
    if(isset($_GET['saved']) && $_GET['saved'] == 1)
    {
        $print_str  = "<p class='ui-state ui-state-widget ui-state-ok' style='width:auto; padding: 8px 35px 8px 14px; border: 1px solid #006400;'>";
            $print_str .= "<span class='ui-icon ui-icon-check ui-icon-ok' style='float:left'></span>";
            $print_str .= "<span>ScoreCard successfully saved</span>";
        $print_str .= "</p>";
        echo $print_str;
    }
?>
<?php JSdisplayResultObj(""); ?>
<form method="post" id="score-card-form" enctype="multipart/form-data">
<table id="score_card_table" width="60%">
<tr>
    <td colspan="2"><h4>Registration of a Scorecard on the Scorecard Register</h4></td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('financial_year', $columns['financial_year']); ?>:
    </th>
    <td>
        <?php echo select_tag('score_card[financial_year_id]', $financial_years, array('id' => 'financial_year_id')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('reference_number', $columns['reference_number']); ?>:
    </th>
    <td>
        <?php echo input_tag('score_card[reference_number]', null, array('id' => 'reference_number')) ?>
    </td>
</tr>
<tr>
    <th valign="top">
        <?php echo label_for('score_card_name', $columns['score_card_name']); ?>:
    </th>
    <td>
        <?php echo textarea_tag('score_card[name]', null, array('id' => 'name')); ?>
    </td>
</tr>

<tr valign="top">
    <th>
        <?php echo label_for('comment', $columns['comment']); ?>:
    </th>
    <td>
        <?php echo textarea_tag('score_card[comment]', '', array('id' => 'comment')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('score_card_type', $columns['score_card_type']); ?>:
    </th>
    <td>
        <?php echo select_tag('score_card[type_id]', $score_card_types, array('id' => 'type_id')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('score_card_category', $columns['score_card_category']); ?>:
    </th>
    <td>
        <?php echo select_tag('score_card[category_id]', $score_card_categories, array('id' => 'category_id')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('score_card_manager', $columns['score_card_manager']); ?>:
    </th>
    <td>
        <?php echo select_tag('score_card[manager_id]', $create_score_card_users, array('id' => 'manager_id')); ?>
    </td>
</tr>
<tr valign="top">
    <th>
        <?php echo label_for('score_card_owner', $columns['score_card_owner']); ?>:
    </th>
    <td>
        <?php echo select_tag('score_card[owner_id]', $score_card_owners, array('id' => 'owner_id')); ?>
    </td>
</tr>
<tr class="scoreCardDefaults">
    <th><?php echo label_for('score_card_default_has_sub_deliverable', $columns['has_sub_deliverable']); ?>:</th>
    <td>
        <select name="score_card[has_sub_deliverable]" id="contract_has_sub_deliverable">
            <option value="0">No</option>
            <option value="1">Yes</option>
        </select>
    </td>
</tr>
<tr class="assessed scoreCardDefaults">
    <th><?php echo label_for('score_card_default_weight_assigned', $columns['weight_assigned']); ?>:</th>
    <td>
        <select name="score_card[weight_assigned]" id="score_card_weight_assigned">
            <option value="0">No</option>
            <option value="1">Yes</option>
        </select>
    </td>
</tr>
<tr>
    <th><?php echo $columns['attachment']; ?>:</th>
    <td>
        <div id="file_upload"></div>
        <input type="file"  name="scorecard_document" id="score_card_attachment_document"  class="attachment" onChange="attachScoreCardDocuments(this)" />
    </td>
</tr>
<tr>
    <th></th>
    <td>
        <input type="submit" id="save" name="save" value="Save" class="save_cscore_card isubmit" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <!--<input type="submit" id="save_next" name="save_next" value="Save & Next" class="save_contract isubmit"/>-->
    </td>
</tr>
</table>
</form>
<?php
}
?>