<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.ui.scorecard.js')); ?>
<?php display_menu_list('new', 'action'); ?>
<?php
page_navigation_links(array('deliverable.php' => 'Scorecards'));
display_flash('contract');
JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#scorecard").scorecard({addAction:true, section:"new", page:"action", autoLoad: false});
    });
</script>
<div id="scorecard"></div>