<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('action.js')); ?>
<?php
$from_confirmation = false;
if(isset($_GET['from']) && $_GET['from'] == 'confirmation')
{
    $contract_id = $_GET['score_card_id'];
    $from_confirmation = true;
}
if($from_confirmation)
{
    display_menu_list('new', 'confirmation');
} else {
    display_menu_list('new', 'action');
}
?>
<?php

$columnsObj = new ActionColumns();
$columns    = $columnsObj->getHeaders();

$actionObj  = new Action();
$action     = $actionObj->getActionDetail($_GET['id']);

$deliverableObj = new Deliverable();
$deliverable       = $deliverableObj->getDeliverableDetail($action['deliverable_id']);

$userObj = new User();
$action_owners = $userObj->getList(true);

?>
<?php
if($from_confirmation)
{
    page_navigation_links(array('confirmation.php?score_card_id='.$_GET['score_card_id'] => 'Confirmation', 'edit_action.php?id='.$_GET['id']."&from=confirmation&score_card_id=".$contract_id => 'Edit Question'));
} else {
   page_navigation_links(array('action.php' => 'Scorecard', 'score_card_deliverables_add_action.php?id='.$deliverable['score_card_id'] => 'Add Questions', 'add_action.php?id='.$deliverable['id'] => 'Add Question',  'edit_action.php?id='.$_GET['id'] => 'Edit Question'));
}
display_flash('action');
JSdisplayResultObj("");
?>

<form  method="post"  name="action-form" id="action-form" enctype="multipart/form-data">
    <table>
        <tr>
            <td colspan="2"><h4>Edit Question</h4></td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('action_name', $columns['action_name']); ?>
            </th>
            <td>
                <?php echo textarea_tag('action[name]', $action['action_name'], array('id' => 'name')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('action_deliverable', $columns['action_deliverable']); ?>
            </th>
            <td>
                <?php echo textarea_tag('action[deliverable]', $action['deliverable'], array('id' => 'deliverable')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('action_rating_scale_type', $columns['rating_scale_type']); ?>
            </th>
            <td>
                <?php echo select_tag('action[rating_scale_type]', options_for_select(Ratingscale::getRatingScaleTypesList(), $action['rating_scale_type']), array('id' => 'rating_scale_type')) ?>
            </td>
        </tr>

        <tr>
            <th></th>
            <td>
                <input type="submit" name="save_edit_changes" id="save_edit_changes" value="Save Changes" class="save_action isubmit" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="hidden" name="action[id]" id="action_id" value="<?php echo $_GET['id']?>" />
                <input type="hidden" name="action[deliverable_id]" id="deliverable_id" value="<?php echo $action['deliverable_id']?>" />
                <input type="hidden" name="log_id" id="action_id" value="<?php echo $action['id']?>" class="logid" />
            </td>
        </tr>
        <tr>
            <td class="noborder">
                <?php if($from_confirmation): ?>
                    <?php displayGoBack('confirmation.php?score_card_id='.$contract_id, 'Go Back'); ?>
                <?php else: ?>
                    <?php displayGoBack("",""); ?>
                    <?php //displayGoBack('add_deliverable.php?id='.$contract['id'], 'Go Back'); ?>
                <?php endif; ?>
            </td>
            <td class="noborder"><?php displayAuditLogLink("action_edit_logs", false) ?></td>
        </tr>
    </table>
</form>