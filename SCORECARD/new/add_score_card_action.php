<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.ui.deliverable.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('new', 'action'); ?>
<?php page_navigation_links(array('action.php' => 'Scorecards', 'add_score_card_action.php?id='.$_GET['id'] => 'Add Questions'));
display_flash('contract');
JSdisplayResultObj("");
$scoreCardObj = new ScoreCard();
$score_card    = $scoreCardObj->getScoreCardDetail($_GET['id']);
?>
<script language="javascript">
    $(function(){
        $("#deliverable_list").deliverable({addAction: true, scoreCardId: <?php echo $_GET['id']; ?>, financialYear: <?php echo $score_card['financial_year_id']; ?>, showSubDeliverable: true, showActions: true, autoLoad: true, allowFilter: false, actionFilter: false, section: 'new'});
    });
</script>
<div id="deliverable_list"></div>