<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.scorecard.activate.js', 'jquery.ui.deliverable.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('new', 'activate'); ?>
<?php page_navigation_links(array('activate.php' => 'Activation')); ?>
<?php display_flash('activation'); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#score_card").activate();
    });
</script>
<div id="score_card"></div>