<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('deliverable.js')); ?>
<?php $from_confirmation = (isset($_GET['from']) && $_GET['from'] ? true : false); ?>
<?php if($from_confirmation): ?>
     <?php display_menu_list('new', 'confirmation'); ?>
<?php else: ?>
    <?php display_menu_list('new', 'deliverable'); ?>
<?php endif; ?>
<?php
$page_title = "Add Deliverable";
$columnsObj = new ScoreCardColumn();
$columns    = $columnsObj->getHeaders();

$columnsObj          = new DeliverableColumns();
$deliverable_columns = $columnsObj->getHeaders();

$deliverableObj = new Deliverable();
$deliverable    = $deliverableObj->getDeliverableDetail($_GET['id']);

$scoreCardObj = new ScoreCard();
$score_card     = $scoreCardObj->getScoreCardDetail($deliverable['score_card_id']);

$categoryObj            = new DeliverableCategory();
$deliverable_categories = $categoryObj->getList(true);

$deliverableObj    = new Deliverable();
$option_sql        = " AND SC.id = ".$deliverable['score_card_id']." AND D.has_sub_deliverable = 1 ";
$main_deliverables = ($deliverable['type_id'] == 0 ? array() : $deliverableObj->getList(true, false, $option_sql));

$weightObj     = new Weight();
$weights      = $weightObj->getList(true);

$userObj = new User();
$deliverable_users = $userObj->getList(true);
?>
<?php if($from_confirmation): ?>
     <?php $score_card_id = $_GET['score_card_id']; ?>
    <?php page_navigation_links(array('confirmation.php?score_card_id='.$_GET['score_card_id'] => 'Confirmation', 'edit_deliverable.php?id='.$_GET['id']."&from=confirmation&score_card_id=".$score_card_id => 'Edit Heading')); ?>
<?php else: ?>
    <?php page_navigation_links(array('deliverable.php' => 'Scorecards', 'add_deliverable.php?id='.$score_card['id'] => 'Add Deliverable', 'edit_deliverable.php?id='.$_GET['id'] => 'Edit Deliverable')); ?>
<?php endif; ?>
<?php display_flash('deliverable'); ?>
<?php JSdisplayResultObj(""); ?>
<form  method="post"  name="deliverable-form" id="deliverable-form" enctype="multipart/form-data">
    <table>
        <tr>
            <td colspan="2"><h4>Edit New Heading</h4></td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_id', $deliverable_columns['deliverable_id']); ?>
            </th>
            <td>
                <?php echo $deliverable['id']; ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_name', $deliverable_columns['deliverable_name']); ?>
            </th>
            <td>
                <?php echo textarea_tag('deliverable[name]', $deliverable['name'], array('id' => 'name')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('description', $deliverable_columns['description']); ?>
            </th>
            <td>
                <?php echo textarea_tag('deliverable[description]', $deliverable['description'], array('id' => 'description')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_category', $deliverable_columns['deliverable_category']); ?>
            </th>
            <td>
                <?php echo select_tag('deliverable[category_id]', options_for_select($deliverable_categories, $deliverable['category_id']), array('id' => 'category_id')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('deliverable_type', $deliverable_columns['deliverable_type']); ?>
            </th>
            <td>
                <?php echo select_tag('deliverable[type_id]', options_for_select(array('0' => 'Main', '1' => 'Sub'), $deliverable['type_id']), array('id' => 'type_id')) ?>
            </td>
        </tr>
        <tr valign="top">
            <th>
                <?php echo label_for('main_deliverable', $deliverable_columns['main_deliverable']); ?>
            </th>
            <td>
                <?php echo select_tag('deliverable[parent_id]', options_for_select($main_deliverables, $deliverable['parent_id']), array('id' => 'parent_id')) ?>
            </td>
        </tr>
<!--        <tr valign="top">
            <th>
                <?php /*echo label_for('deliverable_owner', $deliverable_columns['deliverable_owner']); */?>
            </th>
            <td>
                <?php /*echo select_tag('deliverable[owner_id]', options_for_select($deliverable_users, $deliverable['owner_id']), array('id' => 'owner_id')) */?>
            </td>
        </tr>-->
        <?php if($deliverableObj->isWeighed($score_card, $deliverable)): ?>
            <tr valign="top" class='deliverable_weights' >
                <th>
                    <?php echo label_for('weight', $deliverable_columns['weight']); ?>
                </th>
                <td>
                    <?php echo select_tag('deliverable[weight_id]', options_for_select($weights, $deliverable['weight_id']), array('id' => 'weight_id')) ?>
                </td>
            </tr>
        <?php endif ?>
        <tr>
            <th></th>
            <td>
                <?php if($deliverable['type_id'] == 0 && $deliverable['is_main_deliverable'] == 1): ?>
                    <input type="hidden" name="is_main_deliverable" value="<?php echo $deliverable['is_main_deliverable'] ?>"  id="is_main_deliverable" />
                <?php endif; ?>
                <?php if($deliverable['type_id'] == 0 && $deliverable['has_sub_deliverable'] == 1): ?>
                    <input type="hidden" name="has_sub_deliverable" value="<?php echo $deliverable['has_sub_deliverable'] ?>"  id="has_sub_deliverable" />
                <?php endif; ?>
                <input type="submit" name="edit_deliverable_change" id="edit_deliverable_change" value="Save Changes" class="isubmit" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="hidden" name="deliverable[score_card_id]" id="score_card_id" value="<?php echo $deliverable['score_card_id']?>" />
                <input type="hidden" name="deliverable[id]" id="deliverable_id" value="<?php echo $_GET['id']?>" />
                <input type="hidden" name="log_id" id="deliverable_id" value="<?php echo $deliverable['id']?>" class="logid" />
            </td>
        </tr>
        <tr>
            <td class="noborder">
                <?php if(isset($_GET['from']) && $_GET['from'] == 'confirmation'): ?>
                    <?php displayGoBack('confirmation.php?score_card_id='.$score_card['id'], 'Go Back'); ?>
                <?php else: ?>
                    <?php displayGoBack('add_deliverable.php?id='.$score_card['id'], 'Go Back'); ?>
                <?php endif; ?>
            </td>
            <td class="noborder"><?php displayAuditLogLink("deliverable_edit_logs", false) ?></td>
        </tr>
    </table>
</form>