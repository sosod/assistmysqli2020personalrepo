<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.ui.scorecard.js')); ?>
<?php display_menu_list('new', 'deliverable'); ?>
<?php page_navigation_links(array('deliverable.php' => 'Scorecards')); ?>
<?php display_flash('contract'); ?>
<?php JSdisplayResultObj(""); ?>
<script language="javascript">
    $(function(){
        $("#score_card").scorecard({addDeliverable:true, section:"new", page:"deliverable", autoLoad: false});
    });
</script>
<div id="score_card"></div>