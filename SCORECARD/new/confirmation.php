<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.scorecard.confirmation.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('new', 'confirmation'); ?>
<?php
page_navigation_links(array('confirmation.php' => 'Confirmation'));
display_flash('confirmation');
JSdisplayResultObj("");
$financial_year = 0;
if(isset($_GET['score_card_id']) && !empty($_GET['score_card_id']))
{
    $scoreCardObj   = new ScoreCard();
    $score_card     = $scoreCardObj->getScoreCardDetail($_GET['score_card_id']);
    $financial_year = $score_card['financial_year_id'];
}
?>
<script language="javascript">
    $(function(){
        $("#score_card").confirmation({confirmation:true, section:"new", page:"confirmation", autoLoad: <?php echo (isset($_GET['score_card_id']) ? 'true' : 'false' );  ?>, editDeliverable: true, scoreCardId: <?php echo (isset($_GET['score_card_id']) ? $_GET['score_card_id'] : 'false');  ?>, financialYear: <?php echo $financial_year;  ?>});
    });
</script>
<div id="score_card"></div>
<?php
$print_str = "<p class='ui-state ui-state-widget ui-state-info score_card_message' style='width:auto; padding: 8px 35px 8px 14px;' >";
    $print_str .= "<span class='ui-icon ui-icon-info ui-icon-info' style='float:left'></span>";
    $print_str .= "<span>Please select the year and scorecard to confirm</span>";
$print_str .= "</p>";
echo $print_str;
?>
