<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_js(array('jquery.ui.scorecard.js')); ?>
<?php display_menu_list('new'); ?>
<?php display_sub_menu_list('new', 'score_card', 'edit_score_cards'); ?>
<?php page_navigation_links(array('edit_score_cards.php' => 'Edit Scorecards'));
      display_flash('score_card');
      JSdisplayResultObj("");
?>
<script language="javascript">
    $(function(){
        $("#score_card").scorecard({editScoreCard:true, section:"new", page:"edit", autoLoad: false});
    });
</script>
<div id="score_card"></div>