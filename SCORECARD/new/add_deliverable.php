<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('deliverable.js', 'jquery.ui.deliverable.js', 'jquery.contract.subdeliverable.js', 'jquery.ui.action.js')); ?>
<?php display_menu_list('new', 'deliverable'); ?>
<?php
$page_title = "Add Deliverable";

$columnsObj = new Naming();
$columns    = $columnsObj->getHeaders();

$columnsObj = new DeliverableColumns();
$deliverable_columns    = $columnsObj->getHeaders();

$scoreCardObj = new ScoreCard();
$score_card   = $scoreCardObj->getScoreCardDetail($_GET['id']);

$categoryObj            = new DeliverableCategory();
$deliverable_categories = $categoryObj->getList(true);

$deliverableObj         = new Deliverable();
$main_deliverables      = $deliverableObj->getMainDeliverableWithSubList($score_card['id']);

$weightObj = new Weight();
$weights   = $weightObj->getList(true);

$userObj = new User();
$deliverable_users = $userObj->getList(true);

page_navigation_links(array('deliverable.php' => 'Scorecards', 'add_deliverable.php?id='.$score_card['id'] => 'Add  Heading'));

display_flash('deliverable');
JSdisplayResultObj("");
?>

<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="50%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <tr>
                    <td colspan="2"><h4>Scorecard Details</h4></td>
                </tr>
                <?php echo $scoreCardObj->getScoreCardHtml($_GET['id'], false); ?>
                <tr>
                    <td class="noborder" align="left" width="150">
                        <?php displayGoBack("",""); ?>
                    </td>
                    <td class="noborder"></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="50%">
            <form  method="post"  name="deliverable-form" id="deliverable-form" enctype="multipart/form-data">
                <table width="100%">
                    <tr>
                        <td colspan="2"><h4>Add New  Heading</h4></td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('deliverable_name', $deliverable_columns['deliverable_name']); ?>:
                        </th>
                        <td>
                            <?php echo textarea_tag('deliverable[name]', null, array('id' => 'name')) ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('description', $deliverable_columns['description']); ?>:
                        </th>
                        <td>
                            <?php echo textarea_tag('deliverable[description]', null, array('id' => 'description')) ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('deliverable_category', $deliverable_columns['deliverable_category']); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('deliverable[category_id]', $deliverable_categories, array('id' => 'category_id')) ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('deliverable_type', $deliverable_columns['deliverable_type']); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('deliverable[type_id]', array('0' => 'Main', '1' => 'Sub'), array('id' => 'type_id')) ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th>
                            <?php echo label_for('main_deliverable', $deliverable_columns['main_deliverable']); ?>:
                        </th>
                        <td>
                            <?php echo select_tag('deliverable[parent_id]', $main_deliverables, array('id' => 'sub_parent_id')) ?>
                        </td>
                    </tr>
<!--                    <tr valign="top">
                        <th>
                            <?php /*echo label_for('deliverable_owner', $deliverable_columns['deliverable_owner']); */?>
                        </th>
                        <td>
                            <?php /*echo select_tag('deliverable[owner_id]', $deliverable_users, array('id' => 'owner_id')) */?>
                        </td>
                    </tr>-->
                    <?php if($score_card['weight_assigned']): ?>
                        <tr valign="top" class='weights' >
                            <th>
                                <?php echo label_for('weight', $deliverable_columns['weight']); ?>:
                            </th>
                            <td>
                                <?php echo select_tag('deliverable[weight_id]', $weights, array('id' => 'weight_id')) ?>
                            </td>
                        </tr>
                    <?php endif ?>
                    <tr>
                        <th></th>
                        <td>
                            <input type="submit" name="save" id="save" value="Save" class="save_deliverable isubmit" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--                            <input type="submit" name="add_next" id="add_next" value="Save & Next" class="save_deliverable isubmit" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="submit" name="next" id="next" value="Next " />
                            <input type="hidden" name="page" value="add_deliverable"  id="page" />-->
                            <input type="hidden" name="score_card[score_card_id]" id="score_card_id" value="<?php echo $_GET['id']?>" />
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>
<script language="javascript">
    $(function(){
        $("#deliverable_list").deliverable({editDeliverable: true, scoreCardId: <?php echo $_GET['id']; ?>, financialYear: <?php echo $score_card['financial_year_id']; ?>, showSubDeliverable: true, showActions: true, editAction: true, section: "new", page: "add_deliverable", autoLoad: true, allowFilter: false, actionFilter: false });
    });
</script>
<div id="deliverable_list"></div>
