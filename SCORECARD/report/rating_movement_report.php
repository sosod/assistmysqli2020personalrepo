<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array()); ?>
<?php display_title('Report'); ?>
<?php display_menu_list('report', 'fixed'); ?>
<?php page_navigation_links(array('fixed.php' => 'Fixed Report')); ?>
<?php JSdisplayResultObj(""); ?>
<h2>Scorecard Movement Report as at <?php echo date('d F Y H:i'); ?></h2>
<?php
$userObj = new User();
$users   = $userObj->getList();

$scoreCardOwnerObj = new ScoreCardOwner();
$directorates      = $scoreCardOwnerObj->getList();

$scoreCardObj = new ScoreCard();
$score_card   = $scoreCardObj->getScoreCardWithSetup($_GET['score_card_id']);

$score_card_linked_to  = $scoreCardObj->getScoreCardWithSetupBySetupId($score_card['link_to']);

$questionAssessmentObj       = new ActionAssessment();
$questionObj       = new ActionAssessment();
$user_questions = $questionObj->getActionAssessmentReport($_GET, $score_card, $users, $directorates);

$headingObj = new Deliverable();
$question_headings   = $headingObj->getHeadings($score_card['id']);

$linked_score_card_id = ($_GET['second_score_card_id'] ? $_GET['second_score_card_id'] : $linked_score_card['id']);
$linked_results = $questionAssessmentObj->getLinkedQuestions($linked_score_card_id);

/*$score_card_list = $scoreCardObj->getScoreCardDetailList($score_card);;
//debug($score_card_detail);


$user_linked_questions = $questionObj->getActionAssessmentReport($_GET, $score_card_linked_to, $users, $directorates);

$headingObj = new Deliverable();
$question_headings   = $headingObj->getHeadings($score_card['id']);

$userAssessmentObj = new UserAssessmentRating();
$main_user_assessments  = $userAssessmentObj->getAssessmentByScoreCardId($score_card['id']);

$linked_user_assessments  = $userAssessmentObj->getAssessmentByScoreCardId($score_card_linked_to['id']);*/
//debug($linked_user_assessments);
//exit();
//debug($user_questions);
//echo "Main id - ".$_GET['score_card_id']." linked to ".$score_card['link_to']."<br /><br />";
//exit();
//debug($user_questions);
//exit();

//debug($question_headings);
//exit();
//$scoreCardObj = new ScoreCard();
//$score_card    = $scoreCardObj->getScoreCardWithSetup($_GET['score_card_id']);
//debug($assessment_report);
//$score_card_list = $scoreCardObj->getScoreCardDetailList($score_card);;
//debug($score_card_list);
//exit();
//$_option_sql    = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
//$total_questions = $assessmentReportObj->getTotalQuestionWithAssessment($_option_sql);
//$overall_rating = $assessmentReportObj->getOverallRating($total_questions, $option_sql, $score_card);

?>
<table width="100%">
    <tr>
        <td colspan="9">Scorecard Rating Movement</td>
    </tr>
    <tr>
        <th>Question</th>
        <th>Question Description</th>
        <th><?php echo 'SC'.$score_card['id']." ".$score_card['setup_reference'] ?></th>
        <th>Movement</th>
        <th><?php echo 'SC'.$score_card_linked_to['id']." ".$score_card_linked_to['setup_reference'] ?></th>
    </tr>
    <?php foreach($user_questions['questions'] as  $user_id => $heading_questions): ?>
        <?php $question_count = 0; ?>
        <tr>
            <td colspan="9"><b><?php echo (isset($users[$user_id]) ? $users[$user_id] : ''); ?></b></td>
        </tr>
        <?php foreach($question_headings['main'] as  $main_heading_id => $heading_name): ?>
            <tr>
                <td colspan="9">&nbsp;&nbsp;&nbsp;<h5><?php echo $heading_name; ?></h5></td>
            </tr>
            <?php if(isset($question_headings['headings_sub_headings'][$main_heading_id])): ?>
                <?php foreach($question_headings['headings_sub_headings'][$main_heading_id] as  $sub_heading_id => $sub_heading): ?>
                    <tr>
                        <td colspan="9">&nbsp;&nbsp;&nbsp;<b><?php echo $sub_heading; ?></b></td>
                    </tr>
                    <?php if(isset($heading_questions[$sub_heading_id])) : ?>
                        <?php foreach($heading_questions[$sub_heading_id] as  $question_id => $question) : ?>
                            <?php $parent_id = $user_questions['question_detail'][$user_id][$question['question_id']]['parent_id']; ?>
                            <?php
                                $rating_to = 0;
                                if(isset($linked_results[$user_id][$parent_id]))
                                {
                                    $rating_to = $linked_results[$user_id][$parent_id]['rating'];
                                }
                                $rating_movement = "";
                                if((int)$rating_to < (int)$question['rating']){
                                    $rating_movement = "<img src='/library/images/red_button_arrow_down.png' width='30' height='30' />";
                                } elseif((int)$rating_to == (int)$question['rating']){
                                    $rating_movement = "<img src='/library/images/yellow_button_arrow_left.png' width='30' height='30' />";
                                } elseif((int)$rating_to > (int)$question['rating']) {
                                    $rating_movement = "<img src='/library/images/green_button_arrow_right.png' width='30' height='30' />";
                                }
                            ?>
                            <?php $question_count++; ?>
                            <tr>
                                <td><?php echo $question['name']; ?></td>
                                <td><?php echo $question['deliverable']; ?></td>
                                <td><?php echo $question['rating']; ?></td>
                                <td><?php echo $rating_movement; ?></td>
                                <td><?php echo $rating_to; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <?php if(isset($heading_questions[$main_heading_id])) : ?>
                    <?php foreach($heading_questions[$main_heading_id] as  $question_id => $question) : ?>
                        <tr>
                            <?php $question_count++; ?>
                            <?php
                            $rating_to = 0;
                            if(isset($linked_results[$user_id][$parent_id]))
                            {
                                $rating_to = $linked_results[$user_id][$parent_id]['rating'];
                            }
                            $rating_movement = "";
                            if((int)$rating_to < (int)$question['rating']){
                                $rating_movement = "<img src='/library/images/red_button_arrow_down.png' width='30' height='30' />";
                            } elseif((int)$rating_to == (int)$question['rating']){
                                $rating_movement = "<img src='/library/images/yellow_button_arrow_left.png' width='30' height='30' />";
                            } elseif((int)$rating_to > (int)$question['rating']) {
                                $rating_movement = "<img src='/library/images/green_button_arrow_right.png' width='30' height='30' />";
                            }

                            ?>
                            <?php $parent_id = $user_questions['question_detail'][$user_id][$question['question_id']]['parent_id']; ?>
                            <td><?php echo $question['name']; ?></td>
                            <td><?php echo $question['deliverable']; ?></td>
                            <td><?php echo $question['rating']; ?></td>
                            <td><?php echo $rating_movement; ?></td>
                            <td><?php echo $rating_to; ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>
        <tr>
            <th colspan="2" class="th2">&nbsp;</th>
            <th class="th2">
                <?php $overall_rating = Action::calculateRating($user_questions['question_detail'][$user_id], $question_count, $score_card) ?>
                <?php //echo $overall_rating; ?>
            </th>
            <th class="th2"><?php echo $score_card['overall_comment']; ?></th>
            <th colspan="2" class="th2"><?php //echo (strtotime($user_questions['extra'][$user_id]['completed_at']) > 0 ? ' Confirmed at '.date('d F Y H:i', strtotime($user_questions['extra'][$user_id]['completed_at'])) : ''); ?>
                                        <?php //echo (isset($users[$score_card['completed_by']]) ? ' by  '.$users[$score_card['completed_by']] : ''); ?>
            </th>
        </tr>
        <tr>
            <td colspan="9" class='th2'>&nbsp;</td>
        </tr>
    <?php endforeach; ?>
</table>

