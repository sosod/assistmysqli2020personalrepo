<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array('jquery.contract.assessment.js', 'jquery.ui.action.js')); ?>
<?php display_title('Report'); ?>
<?php display_menu_list('report', 'fixed'); ?>
<?php page_navigation_links(array('fixed.php' => 'Fixed Report')); ?>
<?php JSdisplayResultObj(""); ?>
<h2>Assessment Report</h2>
<?php $assessmentReportObj = new ContractAssessment(); ?>
<?php $assessment_report = $assessmentReportObj->getContractAssessmentReport($_GET['contract_id']); ?>
<?php $contractObj = new Contract();
      $contract    = $contractObj->getContractDetail($_GET['contract_id']);
?>
<?php $contract_list = $contractObj->getContractDetailList($contract);;  ?>


<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="30%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <?php foreach($contract_list as $key => $contract_detail): ?>
                    <tr>
                        <th><?php echo $contract_detail['header']; ?></th>
                        <td><?php echo $contract_detail['value']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </td>
        <td valign="top" class="noborder" width="70%">
            <table width="100%">
                <tr>
                    <td colspan="9">Contract Assessments</td>
                </tr>
                <tr>
                    <th>Deliverable</th>
                    <th>Quantitative Comment</th>
                    <th>Quantitative Score</th>
                    <th>Qualitative Comment</th>
                    <th>Qualitative Score</th>
                    <th>Other Comment</th>
                    <th>Other Score</th>
                    <th>Assessed Date</th>
                </tr>
                <?php foreach($assessment_report['assessments'] as $deliverable_id => $assessments): ?>
                    <?php foreach($assessments as $index => $assessment): ?>
                        <tr>
                            <td>
                                <?php echo $assessment_report['deliverables'][$deliverable_id]['name']; ?>
                            </td>
                            <td><?php echo $assessment['delivered_comment']; ?></td>
                            <td><?php echo $assessment['delivered_score']; ?></td>
                            <td><?php echo $assessment['quality_comment']; ?></td>
                            <td><?php echo $assessment['quality_score']; ?></td>
                            <td><?php echo $assessment['other_comment']; ?></td>
                            <td><?php echo $assessment['other_score']; ?></td>
                            <td><?php echo (empty($assessment['completed_at']) ? $assessment['last_assessed_date'] : date('d-M-Y', strtotime($assessment['completed_at']))); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <th colspan="9" class="th2">&nbsp;</th>
                    </tr>
                <?php endforeach; ?>
            </table>
        </td>
    </tr>
</table>

