<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(); ?>
<?php display_title('Report'); ?>
<?php display_menu_list('report', 'fixed'); ?>
<?php page_navigation_links(array('financial_overview.php?contract_id='.$_GET['contract_id'] => 'Financial Overview')); ?>
<?php JSdisplayResultObj(""); ?>
<?php
$contractObj = new Contract();
$contract = $contractObj->getContractDetail($_GET['contract_id']);
$contractBudgetObj  = new ContractSupplierBudget();
$contractPaymentObj = new ContractSupplierPayment();
$_payment = $contractPaymentObj->getContractPayment($_GET['contract_id']);
$_budget  = $contractBudgetObj->getContractBudget($_GET['contract_id']);
$payment = ASSIST_HELPER::format_float($contractPaymentObj->getContractPayment($_GET['contract_id']));
$budget  = ASSIST_HELPER::format_float($contractBudgetObj->getContractBudget($_GET['contract_id']));
$variance = ASSIST_HELPER::format_float($_budget - $_payment);

$contractAdjustmentObj = new ContractBudgetAdjustment();
$sql                   = " AND contract_id = '".$_GET['contract_id']."' ";
$contract_adjustments  = $contractAdjustmentObj->getContractAdjustments($sql);;
$total_adjustment      = $contractAdjustmentObj->getTotalContractAdjustments($sql);
$contract_adjustments  = $contractAdjustmentObj->prepareContractAdjustment($contract_adjustments, $total_adjustment);


$contractPaymentObj = new ContractPayment();
$contract_payments  = $contractPaymentObj->getContractPayments($sql);
$total_adjustment      = $contractPaymentObj->getTotalContractPayments($sql);
$contract_payments  = $contractPaymentObj->prepareContractPayment($contract_payments, $total_adjustment);

?>
<table>
    <tr>
        <th>Project Name:</th>
        <td><?php echo $contract['name']; ?></td>
    </tr>
    <tr>
        <th>Project Reference Number:</th>
        <td><?php echo $contract['risk_reference']; ?></td>
    </tr>
    <tr>
        <th>Contract Date:</th>
        <td><?php echo $contract['signature_of_tender']; ?></td>
    </tr>
    <tr>
        <th>Contract Status:</th>
        <td><?php echo $contract['contract_status']; ?></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <th>Contract Budget:</th>
        <td style="text-align: right;"><?php echo $budget; ?></td>
    </tr>
    <tr>
        <th>Actual Payments:</th>
        <td style="text-align: right;"><?php echo $payment; ?></td>
    </tr>
    <tr>
        <th>Variance to Budget:</th>
        <td style="text-align: right;"><?php echo $variance; ?></td>
    </tr>
 </table>
<br /><br />
<table>
    <tr>
        <td colspan="<?php echo (5 + $contract_adjustments['total_suppliers']); ?>">Contracted Budget</td>
    </tr>
<?php if(isset($contract_adjustments['adjustments']) && !empty($contract_adjustments['adjustments'])): ?>
    <tr>
        <th>Budget Info</th>
        <th>Adjustment Date</th>
        <th>Reason for Adjustment</th>
        <th>POE Attached</th>
        <th>Total Budget(incl. VAT)</th>
        <?php foreach($contract_adjustments['suppliers'] as $supplier_id => $supplier): ?>
            <th><?php echo $supplier; ?></th>
        <?php endforeach; ?>
    </tr>
    <?php foreach($contract_adjustments['adjustments'] as $index => $contract_adjustment): ?>
        <tr>
            <td><?php echo $contract_adjustment['name'] ?></td>
            <td><?php echo $contract_adjustment['adjustment_date'] ?></td>
            <td><?php echo $contract_adjustment['reason_for_adjustment'] ?></td>
            <td><?php echo $contract_adjustment['attachments'] ?></td>
            <td style="text-align: right;"><?php echo $contract_adjustment['total'] ?></td>
            <?php foreach($contract_adjustments['suppliers'] as $supplier_id => $supplier): ?>
                <td style="text-align: right;" class='th2'><?php echo $contract_adjustments['supplier_budgets'][$index]['supplier_budget'][$supplier_id]; ?></td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="4"></td>
        <td style="text-align: right; background-color: #dfdfff;color: #000000; border: 1px solid #FFFFFF;font-size: 8.5pt; font-weight: bold; line-height: 12pt; padding: 5px 5px 5px 5px;border: 1px solid #ffffff; "><?php echo $contract_adjustments['budget_total']; ?></td>
        <?php foreach($contract_adjustments['suppliers'] as $supplier_id => $supplier): ?>
            <td  style="text-align: right; background-color: #dfdfff;color: #000000; border: 1px solid #FFFFFF;font-size: 8.5pt; font-weight: bold; line-height: 12pt; padding: 5px 5px 5px 5px;border: 1px solid #ffffff; "><?php echo $contract_adjustments['supplier_total'][$supplier_id]; ?></td>
        <?php endforeach; ?>
    </tr>
<?php endif; ?>
    <tr>
        <td colspan="<?php echo (5 + $contract_payments['total_suppliers']); ?>">Contract Payments</td>
    </tr>
    <tr>
        <th>Payment Info</th>
        <th>Payment Date</th>
        <th>Certificate Number</th>
        <th>POE Attached</th>
        <th>Payment Amount(incl. VAT)</th>
        <?php foreach($contract_payments['suppliers'] as $supplier_id => $supplier): ?>
            <th><?php echo $supplier; ?></th>
        <?php endforeach; ?>
    </tr>
    <?php foreach($contract_payments['payments'] as $index => $contract_payment): ?>
        <tr>
            <td><?php echo $contract_payment['name'] ?></td>
            <td><?php echo $contract_payment['payment_date'] ?></td>
            <td><?php echo $contract_payment['certificate_number'] ?></td>
            <td><?php echo $contract_payment['attachments'] ?></td>
            <td style="text-align: right;"><?php echo $contract_payment['total'] ?></td>
            <?php foreach($contract_payments['suppliers'] as $supplier_id => $supplier): ?>
                <td style="text-align: right;"><?php echo $contract_payments['supplier_payments'][$index]['supplier_payment'][$supplier_id]; ?></td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="4"></td>
        <td  style="text-align: right; background-color: #dfdfff;color: #000000; border: 1px solid #FFFFFF;font-size: 8.5pt; font-weight: bold; line-height: 12pt; padding: 5px 5px 5px 5px;border: 1px solid #ffffff; "><?php echo $contract_payments['payment_total']; ?></td>
        <?php foreach($contract_payments['suppliers'] as $supplier_id => $supplier): ?>
            <td  style="text-align: right; background-color: #dfdfff;color: #000000; border: 1px solid #FFFFFF;font-size: 8.5pt; font-weight: bold; line-height: 12pt; padding: 5px 5px 5px 5px;border: 1px solid #ffffff; " class='th2'><?php echo $contract_payments['supplier_total'][$supplier_id]; ?></td>
        <?php endforeach; ?>
    </tr>
</table>