<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array()); ?>
<?php display_title('Report'); ?>
<?php display_menu_list('report', 'fixed'); ?>
<?php page_navigation_links(array('fixed.php' => 'Fixed Report')); ?>
<?php JSdisplayResultObj(""); ?>
<?php
$scoreCardObj = new ScoreCard();
$sql_str = "";
/*$sql_str  =  " AND C.status & ".ScoreCard::ACTIVE." = ".ScoreCard::ACTIVE." ";
$sql_str .=  " AND C.status & ".ScoreCard::CONFIRMED." <> ".ScoreCard::CONFIRMED." ";*/
$sql_str .=  " AND SC.status & ".ScoreCard::ACTIVATED." = ".ScoreCard::ACTIVATED." ";
$score_cards   = $scoreCardObj->getScoreCards($sql_str);
//debug($score_cards);
//exit();
$financialYearObj = new FinancialYear();
$financial_years  = $financialYearObj->getList(true);
?>
<form name="fixed" id="fixed">
    <table id="reports_table">
        <tr>
            <th>Ref</th>
            <th>Name</th>
            <th>Description</th>
            <th>Report Format</th>
            <th>Filter</th>
            <th>
            </th>
        </tr>
        <tr>
            <td>1</td>
            <td>Scorecard Template Register</td>
            <td>The Scorecard Template Register details the Scorecard defaults as defined by the Scorecard Owner(s)</td>
            <td>On Screen</td>
            <td>
                <select name="score_card_register_financial_year_id" id="score_card_register_financial_year_id">
                    <option value="all">All</option>
                    <?php foreach($financial_years as $index => $val): ?>
                        <option value="<?php echo $index; ?>"><?php echo $val; ?></option>
                    <?php endforeach; ?>
                </select>            </td>
            <td><input type="submit" name="score_card_register" id="score_card_register" value="Generate" /></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Scorecard Report</td>
            <td>Overview of Scorecard Ratings for Triggered Scorecard</td>
            <td>On Screen</td>
            <td>
                <select name="score_card_report_score_card_id" id="score_card_report_score_card_id">
                    <option value="">--please select--</option>
                    <?php foreach($score_cards as $index => $val): ?>
                        <?php if(($val['status'] & Scorecard::AVAILABLE) == Scorecard::AVAILABLE) : ?>
                            <option value="<?php echo $val['id']; ?>"><?php echo "(SC".$val['id'].") ".$val['name']." - ".$val['reference_number']; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
            </td>
            <td><input type="submit" name="score_card_report" id="score_card_report" value="Generate" /></td>
        </tr>
        <tr>
            <td>3</td>
            <td>Rating Movement Report</td>
            <td>Overview of the movement of Ratings between two linked Scorecards</td>
            <td>On Screen</td>
            <td>
                <select name="rating_movement_report_score_card_id" id="rating_movement_report_score_card_id">
                    <option value="">--please select first Scorecard--</option>
                    <?php foreach($score_cards as $index => $val): ?>
                        <?php if(($val['status'] & Scorecard::AVAILABLE) == Scorecard::AVAILABLE) : ?>
                            <option value="<?php echo $val['id']; ?>"><?php echo "(SC".$val['id'].") ".$val['name']." - ".$val['reference_number']; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
                <br />
                <select name="second_rating_movement_report_score_card_id" id="second_rating_movement_report_score_card_id">
                    <option value="">--please select second Scorecard--</option>
                    <?php foreach($score_cards as $index => $val): ?>
                        <?php if(($val['status'] & Scorecard::AVAILABLE) == Scorecard::AVAILABLE) : ?>
                            <option value="<?php echo $val['id']; ?>"><?php echo "(SC".$val['id'].") ".$val['name']." - ".$val['reference_number']; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
            </td>
            <td><input type="submit" name="rating_movement_report" id="rating_movement_report" value="Generate" /></td>
        </tr>
<!--        <tr>
            <td>2</td>
            <td>Score Card Analysis Dashboard</td>
            <td>Overview of Score Cards by Classification and Status</td>
            <td>Bar & Pie Graph</td>
            <td></td>
            <td><input type="submit" name="score_card_analysis" id="score_card_analysis" class="generate" value="Generate" /></td>
        </tr>
        <tr>
            <td>3</td>
            <td>Score Card Financial Overview</td>
            <td>Overview Financial Overview of the Score Card per Score CardSupplier</td>
            <td>On Screen</td>
            <td>
                <select name="financial_overview_score_card_id" id="financial_overview_score_card_id">
                    <option value="">--please select--</option>
                   <?php /*foreach($score_cards as $index => $val): */?>
                       <option value="<?php /*echo $index; */?>"><?php /*echo $val; */?></option>
                   <?php /*endforeach; */?>
                </select>
            </td>
            <td><input type="submit" name="financial_overview" id="financial_overview" value="Generate" /></td>
        </tr>
        <tr>
            <td>4</td>
            <td>Score Card Analysis Dashboard per Department</td>
            <td>Overview of Contracts by Classification and Status per Departments</td>
            <td>Bar & Pie Graph</td>
            <td><select>Department</select></td>
            <td><input type="submit" name="score_card_analysis_per_department" id="score_card_analysis_per_department" class="generate" value="Generate" /></td>
        </tr>
        <tr>
            <td>5</td>
            <td>Deliverable Analysis Dashboard</td>
            <td>Overview of Deliverables by Score Card Classification and Action Status</td>
            <td>Bar & Pie Graph</td>
            <td></td>
            <td><input type="submit" name="deliverable_analysis" id="deliverable_analysis" class="generate" value="Generate" /></td>
        </tr>
        <tr>
            <td>6</td>
            <td>Deliverable Analysis Dashboard per Department</td>
            <td>Overview of Deliverables by Score Card Classification and Action Status per Department</td>
            <td>Bar & Pie Graph</td>
            <td><select>Department</select></td>
            <td><input type="submit" name="deliverable_analysis_per_department" id="deliverable_analysis_per_department" class="generate" value="Generate" /></td>
        </tr>
        <tr>
            <td>7</td>
            <td>Deliverable Analysis Dashboard Per Person</td>
            <td>Overview of Deliverables by Score Card Classification and Action Status per Person</td>
            <td>Bar & Pie Graph</td>
            <td></td>
            <td><input type="submit" name="deliverable_analysis_per_person" id="deliverable_analysis_per_person" class="generate" value="Generate" /></td>
        </tr>
        <tr>
            <td>8</td>
            <td>Action Analysis Dashboard</td>
            <td>Overview of Actions by Score Card Classification and Action Status</td>
            <td>Bar & Pie Graph</td>
            <td></td>
            <td><input type="submit" name="action_analysis" id="action_analysis" class="generate" value="Generate" /></td>
        </tr>
        <tr>
            <td>9</td>
            <td>Action Analysis Dashboard Per Person</td>
            <td>Overview of Actions by Score Card Classification and Action Status per Person</td>
            <td>Bar & Pie Graph</td>
            <td><select>Person</select></td>
            <td><input type="submit" name="action_analysis_per_person" id="action_analysis_per_person" class="generate" value="Generate" /></td>
        </tr>
        <tr>
            <td>10</td>
            <td>Assessment Report</td>
            <td>Overview of the Assessment Score Card of a Contract</td>
            <td>On Screen</td>
            <td>
                <select name="assessment_report_score_card_id" id="assessment_report_score_card_id">
                    <option value="">--please select--</option>
                    <?php /*foreach($score_cards as $index => $val): */?>
                        <option value="<?php /*echo $index; */?>"><?php /*echo $val; */?></option>
                    <?php /*endforeach; */?>
                </select>
            </td>
            <td><input type="submit" name="assessment_report" id="assessment_report" value="Generate" /></td>
        </tr>-->
    </table>
</form>
<script>
    $(function(){

        $("#financial_overview").click(function(){
           if($("#financial_overview_score_card_id").val() != '')
           {
              document.location.href = "financial_overview.php?score_card_id="+$("#financial_overview_score_card_id").val();
           }
           return false;
        });

        $("#assessment_report").click(function(){
            if($("#assessment_report_score_card_id").val()  != '')
            {
              document.location.href = "assessment_report.php?score_card_id="+$("#assessment_report_score_card_id").val();
            }
            return false;
        });

        $("#score_card_register").click(function(){
            document.location.href = "score_card_register.php?financial_year="+$("#score_card_register_financial_year_id").val();
            return false;
        });

        $("#score_card_report").click(function(){
            document.location.href = "score_card_report.php?score_card_id="+$("#score_card_report_score_card_id").val();
            return false;
        });

        $("#rating_movement_report").click(function(){
            document.location.href = "rating_movement_report.php?score_card_id="+$("#rating_movement_report_score_card_id").val()+"&second_score_card_id="+$("#second_rating_movement_report_score_card_id").val();
            return false;
        });

        $(".generate").click(function(e){
           var action = $(this).attr("id");
           document.location.href = action+".php";
            e.preventDefault();
        });
    });
</script>