<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(array()); ?>
<?php display_title('Report'); ?>
<?php display_menu_list('report', 'fixed'); ?>
<?php page_navigation_links(array('fixed.php' => 'Fixed Report')); ?>
<?php JSdisplayResultObj(""); ?>
<h2>Scorecard Report as at <?php echo date('d F Y H:i'); ?></h2>
<?php
//$scoreCardObj = new ScoreCard();
//$score_card    = $scoreCardObj->getScoreCardWithSetup($_GET['score_card_id']);

$userObj = new User();
$users   = $userObj->getList();

$scoreCardOwnerObj = new ScoreCardOwner();
$directorates      = $scoreCardOwnerObj->getList();

$scoreCardObj           = new ScoreCard();
$score_card             = $scoreCardObj->getScoreCardWithSetup($_GET['score_card_id']);

//$scoreCardSetupObj       = new ScoreCardSetup();
//$score_card_detail   = $scoreCardSetupObj->getActivateDetail($score_card, array('id' => $_GET['score_card_id']) );

$score_card_list = $scoreCardObj->getScoreCardDetailList($score_card);;
//debug($score_card_detail);

$questionObj       = new ActionAssessment();
$user_questions = $questionObj->getActionAssessmentReport($_GET, $score_card, $users, $directorates);
//debug($user_questions);
//exit();
$headingObj = new Deliverable();
$question_headings   = $headingObj->getHeadings($score_card['id']);
//debug($question_headings);
//exit();
//$scoreCardObj = new ScoreCard();
//$score_card    = $scoreCardObj->getScoreCardWithSetup($_GET['score_card_id']);
//debug($assessment_report);
//$score_card_list = $scoreCardObj->getScoreCardDetailList($score_card);;
//debug($score_card_list);
//exit();
//$_option_sql    = substr($option_sql, 0, strpos($option_sql, 'LIMIT'));
//$total_questions = $assessmentReportObj->getTotalQuestionWithAssessment($_option_sql);
//$overall_rating = $assessmentReportObj->getOverallRating($total_questions, $option_sql, $score_card);
$userAssessmentObj = new UserAssessmentRating();
$main_user_assessments  = $userAssessmentObj->getAssessmentByScoreCardId($score_card['id']);
?>


<table id="deliverable_action_table" class="noborder" width="100%">
    <tr>
        <td width="30%" valign="top" class="noborder">
            <table class="noborder" width="100%">
                <?php foreach($score_card_list as $key => $score_card_detail): ?>
                    <tr>
                        <th><?php echo $score_card_detail['header']; ?></th>
                        <td><?php echo $score_card_detail['value']; ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                   <th>Open Date</th>
                   <td><?php echo $score_card['start_date']; ?></td>
                </tr>
                <tr>
                    <th>Close Date</th>
                    <td><?php echo $score_card['end_date']; ?></td>
                </tr>
            </table>
        </td>
        <td valign="top" class="noborder" width="70%">
            <table width="100%">
                <tr>
                    <td colspan="9">Scorecard Assessments</td>
                </tr>
                <tr>
                    <th>Question</th>
                    <th>Question Description</th>
                    <th>Rating</th>
                    <th>Comments</th>
                    <th>Corrective Action</th>
                    <th>Action Owner</th>
                </tr>
                <?php foreach($user_questions['questions'] as  $user_id => $heading_questions): ?>
                    <?php $question_count = 0; ?>
                    <tr>
                        <td colspan="9"><b><?php echo (isset($users[$user_id]) ? $users[$user_id] : ''); ?></b></td>
                    </tr>
                    <?php foreach($question_headings['main'] as  $main_heading_id => $heading_name): ?>
                        <tr>
                           <td colspan="9">&nbsp;&nbsp;&nbsp;<h5><?php echo $heading_name; ?></h5></td>
                        </tr>
                        <?php if(isset($question_headings['headings_sub_headings'][$main_heading_id])): ?>
                            <?php foreach($question_headings['headings_sub_headings'][$main_heading_id] as  $sub_heading_id => $sub_heading): ?>
                                <tr>
                                    <td colspan="9">&nbsp;&nbsp;&nbsp;<b><?php echo $sub_heading; ?></b></td>
                                </tr>
                                <?php if(isset($heading_questions[$sub_heading_id])) : ?>
                                    <?php foreach($heading_questions[$sub_heading_id] as  $question_id => $question) : ?>
                                      <?php $question_count++; ?>
                                        <tr>
                                            <td><?php echo $question['name']; ?></td>
                                            <td><?php echo $question['deliverable']; ?></td>
                                            <td><?php echo $question['rating']; ?></td>
                                            <td><?php echo $question['rating_comment']; ?></td>
                                            <td><?php echo $question['corrective_action']; ?></td>
                                            <td><?php echo $question['action_owner']; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <?php if(isset($heading_questions[$main_heading_id])) : ?>
                                <?php foreach($heading_questions[$main_heading_id] as  $question_id => $question) : ?>
                                    <tr>
                                        <?php $question_count++; ?>
                                        <td><?php echo $question['name']; ?></td>
                                        <td><?php echo $question['deliverable']; ?></td>
                                        <td><?php echo $question['rating']; ?></td>
                                        <td><?php echo $question['rating_comment']; ?></td>
                                        <td><?php echo $question['corrective_action']; ?></td>
                                        <td><?php echo $question['action_owner']; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <tr>
                        <th colspan="2" class="th2">&nbsp;</th>
                        <th class="th2">
                            <?php $overall_rating = Action::calculateRating($user_questions['question_detail'][$user_id], $question_count, $score_card) ?>
                            <?php echo $overall_rating; ?>
                        </th>
                        <th class="th2">
                            <?php $completed_at = (isset($main_user_assessments[$user_id]) ? $main_user_assessments[$user_id]['completed_at'] : ''); ?>
                            <?php echo (isset($main_user_assessments[$user_id]) ? $main_user_assessments[$user_id]['comment'] : ''); ?>
                        </th>
                        <th colspan="2" class="th2">
                            <?php $completed_by = (isset($main_user_assessments[$user_id]) ? $main_user_assessments[$user_id]['completed_by'] : ''); ?>
                            <?php echo (strtotime($completed_at) > 0 ? 'Confirmed at '.date('d F Y H:i', strtotime($completed_at)) : ''); ?> <?php echo (isset($users[$completed_by]) ? ' by '.$users[$completed_by] : ''); ?>
                        </th>
                    </tr>
                    <tr>
                        <td colspan="9" class='th2'>&nbsp;</td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </td>
    </tr>
</table>

