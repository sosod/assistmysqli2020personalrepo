<?php @session_start(); ?>
<?php include_once('../includes.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(); ?>
<?php display_title('Report'); ?>
<?php display_menu_list('report', 'fixed'); ?>
<?php page_navigation_links(array('fixed.php' => 'Fixed Report')); ?>
<?php JSdisplayResultObj(""); ?>
<h2>Scorecard Template Register</h2>
<?php
$option_sql = '';
if(isset($_GET['financial_year']) && !empty($_GET['financial_year']))
{
   if($_GET['financial_year'] != 'all') $option_sql = " AND SC.financial_year_id = '".$_GET['financial_year']."' ";
}
$option_sql .= " AND SC.status & ".ScoreCard::ACTIVATED." = ".ScoreCard::ACTIVATED." ";
$scoreCardObj = new ScoreCard();
$score_cards = $scoreCardObj->getScoreCards($option_sql);

$total_score_card = $scoreCardObj->getTotalScoreCards($option_sql);
$score_card_register = $scoreCardObj->prepareScoreCard($score_cards, $total_score_card);


//debug($score_card_register);
//exit();
?>
<table>
    <tr>
       <th><?php echo $score_card_register['columns']['score_card_id'] ?></th>
        <th><?php echo $score_card_register['columns']['financial_year'] ?></th>
        <th><?php echo $score_card_register['columns']['reference_number'] ?></th>
        <th><?php echo $score_card_register['columns']['score_card_name'] ?></th>
        <th><?php echo $score_card_register['columns']['comment'] ?></th>
        <th><?php echo $score_card_register['columns']['score_card_type'] ?></th>
        <th><?php echo $score_card_register['columns']['score_card_category'] ?></th>
        <th><?php echo $score_card_register['columns']['score_card_manager']; ?></th>
        <th><?php echo $score_card_register['columns']['score_card_owner'] ?></th>
        <th><?php echo $score_card_register['columns']['score_card_status'] ?></th>
        <th><?php echo $score_card_register['columns']['weight_assigned'] ?></th>
        <th><?php echo 'Last Triggered Date' ?></th>
        <!--<th><?php /*echo $score_card_register['columns']['score_card_progress'] */?></th>-->
    </tr>
    <?php if(!empty($score_card_register['scorecards'])): ?>
        <?php foreach($score_card_register['scorecards'] as $score_card_id => $score_card): ?>
            <?php $status = $score_card_register['score_card_detail'][$score_card_id]['status']; ?>
            <tr>
                <td><?php echo $score_card_id ?></td>
                <td><?php echo $score_card['financial_year'] ?></td>
                <td><?php echo $score_card['reference_number'] ?></td>
                <td><?php echo $score_card['score_card_name'] ?></td>
                <td><?php echo $score_card['comment'] ?></td>
                <td><?php echo $score_card['score_card_type'] ?></td>
                <td><?php echo $score_card['score_card_category'] ?></td>
                <td><?php echo $score_card['score_card_manager'] ?></td>
                <td><?php echo $score_card['score_card_owner'] ?></td>
                <td><?php echo ($status & ScoreCard::AVAILABLE == ScoreCard::AVAILABLE ? 'Active' : 'In-Active'); ?></td>
                <td><?php echo ($score_card['weight_assigned'] == 1 ? 'Yes' : 'No'); ?></td>
                <td><?php echo date('d-M-Y', strtotime($score_card_register['score_card_detail'][$score_card_id]['updated_at']) ); ?></td>
<!--                <td><?php /*echo "&nbsp;";//echo $scoreCardObj->getLastAssessmentDate($score_card_id); */?></td>
                <td><?php /*echo $score_card['score_card_progress'] */?></td>-->
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td colspan="<?php echo count($score_card_register['columns']); ?>">No score cards found for the selected criteria</td>
        </tr>
    <?php endif; ?>
</table>
