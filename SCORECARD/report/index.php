<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(); ?>
<?php display_title('Report'); ?>
<?php display_menu_list('report'); ?>
<?php display_sub_menu_list('report', 'generate'); ?>
<?php page_navigation_links(array('report.php' => 'Generate Report')); ?>