<?php @session_start(); ?>
<?php include_once('../contract_autoloader.php'); ?>
<?php include_once('../contract_helper.php'); ?>
<?php include_basic_files(); ?>
<?php include_js(); ?>
<?php display_title('Report'); ?>
<?php display_menu_list('report', 'fixed'); ?>
<?php page_navigation_links(array('fixed.php' => 'Fixed Report')); ?>
<?php JSdisplayResultObj(""); ?>
<h2>Contract Register</h2>
<?php
$option_sql = '';
if(isset($_GET['financial_year']) && !empty($_GET['financial_year']))
{
   if($_GET['financial_year'] != 'all')
   {
     $option_sql = " AND C.financial_year_id = '".$_GET['financial_year']."' ";
   }

}
$contractObj = new Contract();
$contracts = $contractObj->getContracts($option_sql);
$total_contract = $contractObj->getTotalContracts($option_sql);
$contract_register = $contractObj->prepareContract($contracts, $total_contract);
?>
<table>
    <tr>
       <th><?php echo $contract_register['columns']['contract_id'] ?></th>
        <th><?php echo $contract_register['columns']['reference_number'] ?></th>
        <th><?php echo $contract_register['columns']['contract_name'] ?></th>
        <th><?php echo $contract_register['columns']['contract_type'] ?></th>
        <th><?php echo $contract_register['columns']['contract_category'] ?></th>
        <th><?php echo $contract_register['columns']['signature_of_sla'] ?></th>
        <th><?php echo $contract_register['columns']['financial_year'] ?></th>
        <th><?php echo $contract_register['columns']['contract_budget'] ?></th>
        <th><?php echo $contract_register['columns']['payment_term'] ?></th>
        <th><?php echo $contract_register['columns']['payment_frequency'] ?></th>
        <th><?php echo $contract_register['columns']['contract_manager'] ?></th>
        <th><?php echo $contract_register['columns']['contract_owner'] ?></th>
        <th><?php echo $contract_register['columns']['contract_status'] ?></th>
        <th><?php echo 'Last Assessment' ?></th>
        <th><?php echo $contract_register['columns']['contract_payment'] ?></th>
        <th><?php echo $contract_register['columns']['contract_progress'] ?></th>
        <th><?php echo $contract_register['columns']['completion_date'] ?></th>
    </tr>
    <?php if(!empty($contract_register['contracts'])): ?>
        <?php foreach($contract_register['contracts'] as $contract_id => $contract): ?>
            <tr>
                <td><?php echo $contract_id ?></td>
                <td><?php echo $contract['reference_number'] ?></td>
                <td><?php echo $contract['contract_name'] ?></td>
                <td><?php echo $contract['contract_type'] ?></td>
                <td><?php echo $contract['contract_category'] ?></td>
                <td><?php echo $contract['completion_date'] ?></td>
                <td><?php echo $contract['financial_year'] ?></td>
                <td style="text-align: right;"><?php echo $contract['contract_budget'] ?></td>
                <td><?php echo $contract['payment_term'] ?></td>
                <td><?php echo $contract['payment_frequency'] ?></td>
                <td><?php echo $contract['contract_manager'] ?></td>
                <td><?php echo $contract['contract_owner'] ?></td>
                <td><?php echo $contract['contract_status'] ?></td>
                <td><?php echo $contractObj->getLastAssessmentDate($contract_id); ?></td>
                <td style="text-align: right;"><?php echo $contract['contract_payment'] ?></td>
                <td><?php echo $contract['contract_progress'] ?></td>
                <td><?php echo $contract['completion_date'] ?></td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td colspan="<?php echo count($contract_register['columns']); ?>">No contracts found for the selected criteria</td>
        </tr>
    <?php endif; ?>
</table>
