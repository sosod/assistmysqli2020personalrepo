ALTER TABLE `assist_admire1_contr_contract_supplier_budget`  ADD `description` TEXT NOT NULL AFTER `budget`,  ADD `adjustment_date` DATE NOT NULL AFTER `description`,  ADD `reason_for_adjustment` TEXT NOT NULL AFTER `adjustment_date`,  ADD `attachments` TEXT NOT NULL AFTER `reason_for_adjustment`,  ADD `created_at` INT NOT NULL AFTER `attachments`,  ADD `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_at`

ALTER TABLE  `assist_admire1_contr_contract_supplier_budget` ADD  `insert_user` VARCHAR( 10 ) NOT NULL AFTER  `attachments`

ALTER TABLE  `assist_admire1_contr_contract_supplier_budget` CHANGE  `created_at`  `created_at` DATE NOT NULL

ALTER TABLE  `assist_admire1_contr_contract_supplier_budget` ADD  `status` INT NOT NULL DEFAULT  '1' AFTER  `insert_user`


DROP TABLE IF EXISTS `assist_admire1_contr_contract_supplier_payment`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_contract_supplier_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `insert_user` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


DROP TABLE IF EXISTS `assist_admire1_contr_contract_payment`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_contract_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `payment_date` date NOT NULL,
  `certificate_number` text NOT NULL,
  `attachments` text NOT NULL,
  `insert_user` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


_______________________________________________----------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS `assist_admire1_contr_action_authorization`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_action_authorization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response` text NOT NULL,
  `action_id` int(11) NOT NULL,
  `sign_off_id` int(11) NOT NULL,
  `date_tested` date NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insert_user` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_contr_action_authorization_document`
--

DROP TABLE IF EXISTS `assist_admire1_contr_action_authorization_document`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_action_authorization_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorization_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_contr_action_authorization_logs`
--

DROP TABLE IF EXISTS `assist_admire1_contr_action_authorization_logs`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_action_authorization_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorization_id` int(11) NOT NULL,
  `changes` longtext NOT NULL,
  `insert_user` varchar(10) NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_contr_contract_authorization`
--

DROP TABLE IF EXISTS `assist_admire1_contr_contract_authorization`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_contract_authorization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response` text NOT NULL,
  `contract_id` int(11) NOT NULL,
  `sign_off_id` int(11) NOT NULL,
  `date_tested` date NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insert_user` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_contr_contract_authorization_document`
--

DROP TABLE IF EXISTS `assist_admire1_contr_contract_authorization_document`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_contract_authorization_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorization_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_contr_contract_authorization_logs`
--

DROP TABLE IF EXISTS `assist_admire1_contr_contract_authorization_logs`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_contract_authorization_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorization_id` int(11) NOT NULL,
  `changes` longtext NOT NULL,
  `insert_user` varchar(10) NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_contr_deliverable_authorization`
--

DROP TABLE IF EXISTS `assist_admire1_contr_deliverable_authorization`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_deliverable_authorization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response` text NOT NULL,
  `deliverable_id` int(11) NOT NULL,
  `sign_off_id` int(11) NOT NULL,
  `date_tested` date NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insert_user` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_contr_deliverable_authorization_document`
--

DROP TABLE IF EXISTS `assist_admire1_contr_deliverable_authorization_document`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_deliverable_authorization_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorization_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `assist_admire1_contr_deliverable_authorization_logs`
--

DROP TABLE IF EXISTS `assist_admire1_contr_deliverable_authorization_logs`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_deliverable_authorization_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorization_id` int(11) NOT NULL,
  `changes` longtext NOT NULL,
  `insert_user` varchar(10) NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;


DROP TABLE IF EXISTS `assist_admire1_contr_authorization_status`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_authorization_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `client_terminology` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `insert_user` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;



CREATE TABLE IF NOT EXISTS `assist_admire1_contr_authorization_status_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_id` int(11) NOT NULL,
  `changes` longtext NOT NULL,
  `insert_user` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;# MySQL returned an empty result set (i.e. zero rows).



DROP TABLE IF EXISTS `assist_admire1_contr_assurance_status`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_assurance_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `client_terminology` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `insert_user` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


DROP TABLE IF EXISTS `assist_admire1_contr_assurance_status_logs`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_assurance_status_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_id` int(11) NOT NULL,
  `changes` longtext NOT NULL,
  `insert_user` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


DROP TABLE IF EXISTS `assist_admire1_contr_dir`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_dir` (
  `dirid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dirtxt` varchar(100) NOT NULL,
  `dirsort` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`dirid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;


INSERT INTO `assist_admire1_contr_dir` (`dirid`, `dirtxt`, `dirsort`, `active`) VALUES
(1, 'Corporate Services', 1, 1),
(2, 'Technical Services', 2, 1),
(3, 'Executive Office', 3, 1);


DROP TABLE IF EXISTS `assist_admire1_contr_dirsub`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_dirsub` (
  `subid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subtxt` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `subdirid` int(10) unsigned NOT NULL,
  `subsort` int(10) unsigned NOT NULL,
  `subhead` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`subid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

INSERT INTO `assist_admire1_contr_dirsub` (`subid`, `subtxt`, `active`, `subdirid`, `subsort`, `subhead`) VALUES
(1, 'HOD: Corporate Services', 1, 1, 1, 'Y'),
(2, 'Finance and Governance', 1, 1, 2, 'N'),
(3, 'Human Resources (HR)', 1, 1, 3, 'N'),
(4, 'Information Technology (IT)', 1, 1, 4, 'N'),
(5, 'HOD: Technical Services', 1, 2, 1, 'Y'),
(6, 'Water and Sanitation', 1, 2, 2, 'N'),
(7, 'Electricity and Sustainable Energy', 1, 2, 3, 'N'),
(8, 'HOD: Executive Office', 1, 3, 1, 'Y'),
(9, 'Infrastructure and Social Development', 1, 2, 4, 'N');

DROP TABLE IF EXISTS `assist_admire1_contr_dir_admins`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_dir_admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tkid` varchar(10) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL,
  `type` varchar(5) NOT NULL DEFAULT 'DIR',
  `ref` int(10) unsigned NOT NULL DEFAULT '0',
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `insertuser` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;


INSERT INTO `assist_admire1_contr_dir_admins` (`id`, `tkid`, `active`, `type`, `ref`, `insertdate`, `insertuser`) VALUES
(1, '0002', 0, 'DIR', 1, '0000-00-00 00:00:00', ''),
(2, '0002', 0, 'SUB', 1, '0000-00-00 00:00:00', ''),
(3, '0006', 508, 'SUB', 1, '0000-00-00 00:00:00', ''),
(4, '0005', 508, 'SUB', 2, '0000-00-00 00:00:00', ''),
(5, '0004', 508, 'SUB', 3, '0000-00-00 00:00:00', ''),
(6, '0003', 508, 'SUB', 4, '0000-00-00 00:00:00', ''),
(7, '0002', 508, 'DIR', 3, '0000-00-00 00:00:00', ''),
(8, '0002', 508, 'SUB', 8, '0000-00-00 00:00:00', ''),
(9, '0007', 508, 'DIR', 2, '0000-00-00 00:00:00', ''),
(10, '0007', 508, 'SUB', 5, '0000-00-00 00:00:00', ''),
(11, '0008', 508, 'SUB', 6, '0000-00-00 00:00:00', ''),
(12, '0009', 508, 'SUB', 7, '0000-00-00 00:00:00', ''),
(13, '0010', 508, 'SUB', 9, '0000-00-00 00:00:00', ''),
(14, '0006', 508, 'DIR', 1, '0000-00-00 00:00:00', '');

DROP TABLE IF EXISTS `assist_admire1_contr_dir_admins_logs`;
CREATE TABLE IF NOT EXISTS `assist_admire1_contr_dir_admins_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setup_id` int(11) NOT NULL,
  `changes` blob NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertuser` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

