<?php
$now    = strtotime(date("d F Y"));
$future = strtotime(date("d F Y",($today+($next_due*24*3600))));
$sql    = "SELECT SCS.end_date as taskdeadline, count(A.id) as c
		FROM assist_".$cmpcode."_".$modref."_action A
		INNER JOIN assist_".$cmpcode."_".$modref."_deliverable D ON D.id = A.deliverable_id
		INNER JOIN assist_".$cmpcode."_".$modref."_score_card C ON C.id = D.score_card_id
		INNER JOIN assist_".$cmpcode."_".$modref."_score_card_setup SCS ON SCS.id = C.score_card_setup_id
		WHERE SCS.start_date <= ".$future." AND A.owner_id = '$tkid'
		AND C.status & 16 = 16 AND A.status & 16 <> 16
		GROUP BY SCS.start_date";
$rs = getRS($sql);
while($row = mysql_fetch_array($rs)) {
	$d = strtotime($row['taskdeadline']);
	if($d < $now) {
		$count['past']+=$row['c'];
	} elseif($d==$now) {
		$count['present']+=$row['c'];
	} else {
		$count['future']+=$row['c'];
	}
}
mysql_close();
?>