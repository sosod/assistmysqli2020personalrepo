<?php
//AA-488 JC 12 Oct 2020 - tidy up page, change DB calls to ASSIST_DB class, add option to copy DBs for support purposes
require_once("../module/autoloader.php");
$helper = new ASSIST_MODULE_HELPER();
$tkid = $helper->getUserID();
$cmpcode = $helper->getCmpCode();
$mdb = new ASSIST_DB("master");
ASSIST_HELPER::echoPageHeader("1.10.0", array(), array("/assist_jquery.css?".time(), "/assist3.css?".time()));

/**
 * IASSIST USERS Are down below
 */

/* GET USER ACCESS SETTINGS */
$sql = "SELECT *, 'Y' as can_access FROM assist_admin WHERE tkid = '".$tkid."'";
$access = $helper->mysql_fetch_value_by_id($sql,"access","can_access");

/* GET COMPANY LIST */
$company = array();
if(strtoupper($_SESSION['cc'])=="IASSIST") {
	$sql = "SELECT cmpreseller, cmpcode, cmpname FROM assist_company WHERE cmpstatus = 'Y' AND cmpcode <> 'BLANK' ORDER BY cmpreseller, cmpcode";
} else {
	$sql = "SELECT cmpreseller, cmpcode, cmpname FROM assist_company WHERE cmpstatus = 'Y' AND cmpreseller = '".strtoupper($cmpcode)."' ORDER BY cmpcode";
}
$rows = $mdb->mysql_fetch_all($sql);
foreach($rows as $row) {
	$company[$row['cmpreseller']][] = $row;
}
unset($rows);

/* GET RESELLER INFO */
$resellers = array();
if(strtoupper($_SESSION['cc'])=="IASSIST") {
	$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname FROM `assist_company` c1
			INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND c2.cmpstatus = 'Y'
			WHERE c1.cmpstatus = 'Y'";
	$rows = $mdb->mysql_fetch_all($sql);
	foreach($rows as $row) {
		$resellers[$row['cmpcode']] = $row;
	}
	unset($rows);
} else {
	$resellers[strtoupper($_SESSION['cc'])] = array('cmpcode'=>strtoupper($_SESSION['cc']),'cmpname'=>"SELECT");
}


$dbs = array();
$db_prefix = $mdb->getDBPrefix();
$sql = "SHOW DATABASES LIKE '".$db_prefix."%'";
$dbs_raw = $mdb->mysql_fetch_all($sql);
foreach($dbs_raw as $i=>$x) {
	foreach($x as $z => $y) {
		$dbs[] = $y;
	}
}
?>
<style type=text/css>
    select {
        font-family: Lucida Console, Arial;
        font-size: 10pt;
    }
	ul li { 
		font-weight: bold;
	}
	p.pos_fixed {
		position:fixed;
		bottom:0px;
		right:5px;
	}
</style>
<h1>Assist Admin</h1>
<?php
ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>

<ul>
<?php if($access['user_list']=="Y") { ?>
<li id=li_user_list>User list:<br />
<select name=cc id=cc>
<!-- <option selected value=X>--- SELECT ---</option> -->
<?php
foreach($resellers as $rc => $res) {
	echo "<option value=X class=res>--- ".$res['cmpname']." ---</option>";
	foreach($company[$rc] as $cmp) {
		$cmpcode = strtolower($cmp['cmpcode']);
		if (in_array($db_prefix.$cmpcode, $dbs, true)) {
			echo "<option value=".$cmp['cmpcode'].">".$cmp['cmpcode'].": ".$cmp['cmpname']."</option>";
		}
	}
}
?>
</select> <input type=button value=" Go " />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=button value="Company details" id=cmp_view  />
<br />&nbsp;
<?php
} //user_list



if( (strtoupper($_SESSION['cc'])=="IGN0001" && $access['pms_user']=="Y") || (strtoupper($_SESSION['cc']) == "IASSIST" && ($_SESSION['tid']=="0001"))) {
?>
<li id=li_pmsps>Performance Assist:<br />
<select name=pmsps id=pmsps>
<?php
$modref = 'PMSPS'; $mod_table = 'user_access';
//foreach($resellers as $rc => $res) {
$rc = "IGN0001";
$res = $resellers[$rc];
$pmsps_exists = array();
	echo "<option value=X class=res>--- ".$res['cmpname']." ---</option>";
	foreach($company[$rc] as $cmp) {
		$cmpcode = strtolower($cmp['cmpcode']);
		if (in_array($db_prefix.$cmpcode, $dbs, true)) {
$cdb = new ASSIST_DB("client",$cmpcode);
			if($cdb->checkModuleExists($modref,$mod_table)===true) {
				$pmsps_exists[] = $cmpcode;
				echo "<option value=".$cmp['cmpcode'].">".$cmp['cmpcode'].": ".$cmp['cmpname']."</option>";
			}
unset($cdb);
		}
	}
//}
?>
</select> <select name=pmspsloc id=pmspsloc><option selected value=eval>Reset Eval</option><option value=user>User Report</option></select> <input type=button value=" Go " /><br />&nbsp;</li>
<li id=li_pmsps_export>Performance Assist KPA export:<br />
<select name=pmsps_export id=pmsps_export>
<?php
$modref = 'PMSPS'; $mod_table = 'kpa';
//foreach($resellers as $rc => $res) {
$rc = "IGN0001";
$res = $resellers[$rc];
	echo "<option value=X class=res>--- ".$res['cmpname']." ---</option>";
	foreach($company[$rc] as $cmp) {
		$cmpcode = strtolower($cmp['cmpcode']);
		//don't need to check DB/Module existence again - if it's in PMSPS_EXISTS array then it passed db check & module check in the previous loop
			if(in_array($cmpcode,$pmsps_exists)) {
				echo "<option value=".$cmp['cmpcode'].">".$cmp['cmpcode'].": ".$cmp['cmpname']."</option>";
			}
	}
//}
?>
</select> <input type=button value=" Go " /><br />&nbsp;</li>
<?php




} //pms_user

?>

</ul>

<script type=text/javascript>
$(function() {
var cmpcode = '<?php echo strtoupper($_SESSION['cc']); ?>';
var c = 0;
	$("select option").each(function() {
		if($(this).hasClass("res")) { $(this).attr("disabled","disabled").css("color","#000000"); c++; }
	});
	$("ul input:button, div input:button.isubmit").click(function() {
		if($(this).attr("id")=="cmp_view") {
			document.location.href = 'cmp_view.php';
		} else {
			if($(this).hasClass("isubmit")) {
				if($(this).hasClass("sdbp6")) {
					var p = "create_sdbp6";
					var cc = $(this).closest("div").find("select:first").val();
				} else if($(this).hasClass("sdbp5b")) {
					var p = "create_sdbp5b";
					var cc = $(this).closest("div").find("select:first").val();
				} else {
					var p = $(this).parent().parent().attr("id");
					var cc = $("#"+p+" select:eq(0)").val();
				}
			} else {
				var p = $(this).parent().attr("id");
				var cc = $("#"+p+" select:eq(0)").val();
			}
		//alert(p+":"+cc);
				if(cc.length == 7) {
					switch(p) {
						case "li_user_list":	document.location.href = 'pwd_list.php?cc='+cc; break;
						case "li_pmsps":		document.location.href = 'pmsps_'+$("#"+p+" select:eq(1)").val()+'.php?cc='+cc; break;
						case "li_pmsps_export":	document.location.href = 'pmsps_kpa_export.php?cc='+cc; break;
						// case "li_sdp10_time":	document.location.href = "../SDBP3/sdbp3_time_list.php?cc="+cc;	break;
						// case "li_sdp09_time":	document.location.href = "../SDBP2/sdbp2_time_list.php?cc="+cc;	break;
						// case "create_sdbp5b":	document.location.href = "SDBP5B_create.php?cc="+cc;	break;
						// case "create_sdbp6":	document.location.href = "SDBP6_create.php?cc="+cc;	break;
						default: alert("What now maestro? "+p); break;
					}
				}
		}
	});
	$("#btn_sdbp5_headings").click(function() {
		//alert("mr");
		document.location.href = 'SDBP5B_headings.php';
	});
});
</script>
<?php


/**
 * ACTION IT / IASSIST Admin Functions
 *
 */
$iassist_users = array("0001","0002","0010","0012","0014","0015");
/**
 * IASSIST admin users
 * 0001 => JC
 * 0002 => RK
 * 0010 => TM
 * 0012 => SS
 * 0014 => SD
 * 0015 => NM
 */


if(strtoupper($_SESSION['cc'])=="IASSIST" && in_array($tkid,$iassist_users)) {
	echo "<h2>Admin Functions</h2><p class='i'>[Action iT users only]</p>";
	//Set user-specific user access
	$access = array(
		'user_lock'=>array("0001"),
		'manage_modules'=>array("0001"),
		'dev_db_updates'=>array("0001"),
		'live_db_updates'=>array("0001"),
		'man'=>array("0001","0012"),
		'recent_updates'=>array("0001","0002","0012"),
		'inconsistency_report'=>array("0001"),
		'module_usage_report'=>array("0001"),
		'client_export'=>array("0001"),
		'support_db_copy'=>array("0001","0012","0010","0014","0015"),
		'module_usage_report_by_modloc'=>array("0001","0012","0014","0015"),
	);
	//Define sections
		//'dev_db_updates'=>"Update Development Databases",
//		'man'=>array('name'=>"Help Manuals",'extra_info'=>""),
//		'user_lock'=>array('name'=>"Lock Assist Support User",'extra_info'=>"Use this function to lock the support user of a specific reseller"),
	$sections = array(
		'support_db_copy'=>array('name'=>"Copy Database For Support",'extra_info'=>"Use this function to copy a client database to a support database."),
		'recent_updates'=>array('name'=>"Recent Updates",'extra_info'=>"Manage the Recent Updates"),
		'manage_modules'=>array('name'=>"Add modules/databases",'extra_info'=>"This is JC's raw function for adding modules & databases and copying DBs"),
		'live_db_updates'=>array('name'=>"Update Live Databases",'extra_info'=>"JC's page for bulk updating live client databases"),
		'inconsistency_report'=>array('name'=>"Database / Company inconsistencies",'extra_info'=>"A report on any inconsistencies between active databases & client statuses"),
		'module_usage_report_by_modloc'=>array('name'=>"Module Usage Report",'extra_info'=>"A raw report on module usage with number of clients & users"),
		'module_usage_report'=>array('name'=>"Module Usage Report (RAW)",'extra_info'=>"A raw report on modules (by modloc) per client and total count per module"),
		'client_export'=>array('name'=>"Client Data Export Report",'extra_info'=>""),
	);
//echo "<hr /><h3>".$tkid."</h3>";
	// Function to draw the clickable block
	function drawVisibleBlock($title,$lot,$text,$button_name,$color="blue") {
		$data = array(
			'display'=>"",
			'js'=>"",
		);

		$echo = "
		<div style='width:500px;' id=div_".$lot." class=div_segment page=".$lot." my_btn=btn_".$lot."_open>
		<h2 class='object_title $color' colorhistory='$color'>".$title."</h2>
				<Table class='form tbl-segment-description' width=100%>
			<tr>
				<td class='i'>".str_replace(chr(10),"<br />",$text)."</td>
			</tr>
		</Table>
		<p class=right><button class=btn_open id=btn_".$lot."_open>".$button_name."</button></p>
	</div>";
		$data['display'] = $echo;

		echo $data['display'];
		return $data['js'];
	}

	//User Interface
	//Container table
	?>
<table style="width:1100px;margin: 0 auto" id=tbl_container><tr><td style='text-align: center' class=container>
<?php
	//Draw the blocks per section user has access to
	$button_name = "Open";
	$js = "";
	$have_echoed_something = false;
	foreach($sections as $key => $s) {
		if(in_array($tkid,$access[$key])) {
			$have_echoed_something = true;
			if(!is_array($s)) {
				$s = array('name' => $s, 'colour' => "blue", 'extra_info' => "");
			} else {
				if(!isset($s['colour'])) {
					$s['colour'] = "blue";
				}
				if(!isset($s['extra_info'])) {
					$s['extra_info'] = "";
				}
			}
			$js .= drawVisibleBlock($s['name'], $key, $s['extra_info'], $button_name, $s['colour']);
		}
	}
	if($have_echoed_something===false) {
		echo "No sections available.  If you believe this to be in error, please contact Janet Currie of Action iT (Pty) Ltd.";
	}
	?>
	</td></tr></table>

	<script type="text/javascript">

	$(function() {
		<?php echo $js; ?>

		$("div.div_segment").button().click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var page = $(this).attr("page");
			var url = '../assist_admin/'+page+'.php';
			url = url.toLowerCase();
			document.location.href = url;
		});
		$("div.div_segment").hover(
			function() {
				$(this).removeClass("ui-button-state-blue").addClass("ui-button-state-orange");
				$me = $(this).find("#btn_"+$(this).attr("page")+"_open");
				$me.removeClass("ui-button-state-blue").addClass("ui-button-state-orange");
				$title = $(this).find("h2.object_title");
				var title_color = $title.attr("colorhistory");
				$title.removeClass(title_color).addClass("orange");
			},
			function() {
				$(this).removeClass("ui-button-state-orange").addClass("ui-button-state-blue");
				$me = $(this).find("#btn_"+$(this).attr("page")+"_open");
				$me.addClass("ui-button-state-blue").removeClass("ui-button-state-orange");
				$title = $(this).find("h2.object_title");
				var title_color = $title.attr("colorhistory");
				$title.addClass(title_color).removeClass("orange");
			}
		).css({"margin":"10px","background":"url()","padding-bottom":"30px"});//,"border-color":"#0099FF"});

		$("button.btn_open").button({
			icons:{primary:"ui-icon-newwin"}
		}).css({
			"position":"absolute","bottom":"5px","right":"5px"
		});
		$("button.btn_open").click(function(e) {
			e.preventDefault();
			$(this).parent("div.div_segment").trigger("click");
		});
		function formatButtons() {
			$("button.xbutton").children(".ui-button-text").css({"font-size":"80%","padding":"4px","padding-left":"24px"});
			$("button.xbutton").css({"margin":"2px"});
		}
		formatButtons();

		var description_height = 0;
		$(".tbl-segment-description").find("td").css({"vertical-align":"middle","text-align":"center","padding":"5px"});
		$(".tbl-segment-description").each(function() {
			var h = parseInt(AssistString.substr($(this).css("height"),0,-2));
			if(h>description_height) {
				description_height = h;
			}
		});
		$(".tbl-segment-description").css("height",(description_height+5)+"px");
		$("#tbl_container, #tbl_container td").css("border","1px solid #ffffff");
		// $(".div_segment").css("border","1px solid #9999ff").addClass("blue-border");
		$(".div_segment").removeClass("ui-state-default").addClass("ui-button-state-blue");
		$(".btn_open").removeClass("ui-state-default").addClass("ui-button-state-blue");
	});
	</script>
<?php
} //endif IASSIST Admin functions
?>

</body>

</html>
