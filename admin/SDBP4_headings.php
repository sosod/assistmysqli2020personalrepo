<?php
if(isset($_REQUEST['act']) && $_REQUEST['act']=="REVIEW") { ob_start(); }

function openLogImport($cc) {
	global $mod_ref;
	$cc =strtolower($cc);
	$ilf = "../files/$cc/".strtoupper($mod_ref)."/import/";
	$ilf.="import_log.csv";
	$import_log_file = fopen($ilf,"a");
	return $import_log_file;
}
//"date("Y-m-d H:i:s")","$_SERVER['REMOTE_ADDR']","action","$self","implode(",",$_REQUEST)"
function implodeArray($array) {
	$a = array();
	foreach($array as $key => $val) {
		if(is_array($val)) {
			$a[] = $key."=[".implode("|",implodeArray($val))."]";
		} else {
			$a[] = $key."=".$val;
		}
	}
	//arrPrint($a);
	return $a;
}
function logImport($handle, $action, $page) {
	$fdata = array();
	$fdata[] = date("Y-m-d H:i:s");
	$fdata[] = $_SERVER['REMOTE_ADDR'];
	$fdata[] = $action;
	$fdata[] = $page;
	$arg = implodeArray($_REQUEST);
	$arg[] = "cc=".$_SESSION['cc'];
	$arg[] = "tkid=".$_SESSION['tid'];
	$arg[] = "tkname=".$_SESSION['tkn'];
	fwrite($handle,"\"".implode("\",\"",$fdata)."\",\"".implode("\",\"",$arg)."\"\n");
}


require '../header.php';
$mod_year = "2012/2013";
$mod_ref = "sdp12";
$mod_loc = "SDBP4";
error_reporting(-1);

$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : "CHOOSE";

	echo "<h1>SDBIP ".$mod_year." > Copy Headings</h1>
	".($act!="SAVE" ? "<p>This page allows you to copy the field headings for SDBIP $mod_year from the source client to the destination client.</p>" : "");


switch($act) {
case "CHOOSE":

	/* GET VALID DATABASES */
	$dbs = array();
	$link = mysql_connect("localhost",$db_user,$db_pwd);
	$db_list = mysql_list_dbs($link);
	while ($row = mysql_fetch_object($db_list)) {
		 $dbs[] = $row->Database;
	}

	/* GET ACTIVE COMPANIES */
	$sql = "SELECT cmpreseller, cmpcode, cmpname FROM assist_company WHERE cmpstatus = 'Y' AND cmpcode <> 'BLANK' ORDER BY cmpcode, cmpname";
	$company = mysql_fetch_all_fld2($sql,"cmpreseller","cmpcode","A");

	/* GET RESELLERS IF IASSIST */
	$resellers = array();
	if(strtoupper($_SESSION['cc'])=="IASSIST") {
		$sql = "SELECT DISTINCT c1.cmpcode, c1.cmpname FROM `assist_company` c1
				INNER JOIN assist_company c2 ON c1.cmpcode = c2.cmpreseller AND c2.cmpstatus = 'Y'
				WHERE c1.cmpstatus = 'Y'";
		$resellers = mysql_fetch_all_fld($sql,"cmpcode","A");
	} else {
		$resellers[strtoupper($_SESSION['cc'])] = array('cmpcode'=>strtoupper($_SESSION['cc']),'cmpname'=>"SELECT");
	}


	echo "
	<form name=frm_review action=SDBP4_headings.php method=post >
	<input type=hidden name=act value=REVIEW />
	<P class=b>Source:<br /><select id=source name=source><option value=BLANK selected>Original Ignite Headings</option>";
		foreach($resellers as $rc => $res) {
			echo "<option value=X class=res>--- ".$res['cmpname']." ---</option>";
			foreach($company[$rc] as $cmp) {
				$cmpcode = strtolower($cmp['cmpcode']);
				if (in_array($db_other.$cmpcode, $dbs, true) && $cmpcode != $_SESSION['cc']) {
					//if(!getModuleRefExistance($modref,$mod_table)) {
					$modexists = getModuleRefExistance($mod_ref,$mod_table);
					if($modexists == 1) {
						echo "<option value=".$cmp['cmpcode'].">".$cmp['cmpcode'].": ".$cmp['cmpname']."</option>";
					//} else {
					//	echo "<option value=".$cmp['cmpcode']." class=has_module>".$cmp['cmpcode'].": ".$cmp['cmpname'].(isset($activity[$cmp['cmpcode']]) ? " (Created on ".date("d-M-Y",strtotime($activity[$cmp['cmpcode']]['activity_date']))." by ".($activity[$cmp['cmpcode']]['bp_cmpcode']==$_SESSION['cc'] ? $activity[$cmp['cmpcode']]['bp_tkname'] : $activity[$cmp['cmpcode']]['bp_cmpname']).")" : "")."</option>";
					}
				}
			}
		}
	echo "</select></p>";

	echo "<P class=b>Destination:<br /><select id=dest name=destination>";
		foreach($resellers as $rc => $res) {
			echo "<option value=X class=res>--- ".$res['cmpname']." ---</option>";
			foreach($company[$rc] as $cmp) {
				$cmpcode = strtolower($cmp['cmpcode']);
				if (in_array($db_other.$cmpcode, $dbs, true) && $cmpcode != $_SESSION['cc']) {
					$modexists = getModuleRefExistance($mod_ref,$mod_table);
					if($modexists == 1) {
					//if(!getModuleRefExistance($modref,$mod_table)) {
						echo "<option value=".$cmp['cmpcode']." class=".$cmp['cmpcode'].">".$cmp['cmpcode'].": ".$cmp['cmpname']."</option>";
					//} else {
					//	echo "<option value=".$cmp['cmpcode']." class=has_module>".$cmp['cmpcode'].": ".$cmp['cmpname'].(isset($activity[$cmp['cmpcode']]) ? " (Created on ".date("d-M-Y",strtotime($activity[$cmp['cmpcode']]['activity_date']))." by ".($activity[$cmp['cmpcode']]['bp_cmpcode']==$_SESSION['cc'] ? $activity[$cmp['cmpcode']]['bp_tkname'] : $activity[$cmp['cmpcode']]['bp_cmpname']).")" : "")."</option>";
					}
				}
			}
		}
	echo "</select></p>";

	echo "<p><input type=button value=Review class=isubmit /></p>
	</form>";

	?>

	<script type=text/javascript>
	$(function() {
	var cmpcode = '<?php echo strtoupper($_SESSION['cc']); ?>';
	var c = 0;
		$("select option").each(function() {
			if($(this).hasClass("res")) { $(this).attr("disabled","disabled").css("color","#000000"); c++; }
		});
		$("#source").change(function() {
			var s = $(this).val();
			$("#dest option").each(function() { if(!$(this).hasClass("res")) { $(this).attr("disabled",false); } });
			if(s!="X") {
				$("#dest option."+s).attr("disabled",true);
			}
		});
		$("input:button.isubmit").click(function() {
			
			if($("#source option:selected").attr("disabled")==true || $("#dest option:selected").attr("disabled")==true) {
				alert("Please select a valid Source and/or Destination client.");
			} else {
				$("form[name=frm_review]").submit();
			}
		});
	});
	</script>
<?php
	break;
case "REVIEW":
if(isset($_REQUEST['destination'])) {
	$import_log_file = openLogImport($_REQUEST['destination']);
	logImport($import_log_file, "SDBIP ".$mod_year." > Copy Headings > Compare against ".$_REQUEST['source']." [REVIEW]", "SDBP4_headings.php");
}
			function getCopyHeadings($cc) {
				global $mod_ref;
$mod_ref = strtolower($mod_ref);
				//echo "<P>GET HEADINGS:: ".$cc.
				$modref = strtolower($cc) == "blank" ? "sdbp4" : $mod_ref;
				$dbref = "assist_".strtolower($cc)."_".strtolower($modref);
				$sql = "SELECT sh.h_client, sh.h_ignite, shs.*, sh.h_type, sh.h_table, (shs.c_list + shs.fixed) as list_view
						FROM ".$dbref."_setup_headings sh
						INNER JOIN ".$dbref."_setup_headings_setup shs
						  ON sh.h_id = shs.head_id
						  AND shs.active = true
						WHERE
						  sh.h_active = true
						AND
						  sh.h_can_rename = true
						ORDER BY
						  shs.section,
						  shs.c_sort,
						  sh.h_id
						"; 
				$heads = array();
				$heads = mysql_fetch_all_fld2($sql,"section","id","O",strtolower($cc));
				return $heads;
			} 
	?>
	<style type=text/css>
		th.sec { background-color: #555555; text-align: left; }
		.ui-icon.ui-state-white  {background-image: url(/library/jquery/css/images/ui-icons_ffffff_256x240.png); }
	</style>
	<p><span class='b iinform'>Please note:</span><ul>
	<li>The "Default Headings" column shows the standard Ignite Assist headings that are loaded when the SDBIP is created.</li>
	<li>The "Source" column shows the client headings for the client selected as the Source on the previous page.  Any differences between the <u>Default Headings</u> and the <u>Source</u> headings are highlighted in <span class='ui-widget ui-state-info'>orange</span> for you information.</li>
	<li>The "Destination" column shows the client headings for the client selected as the Destination on the previous page.  Any differences between the <u>Destination</u> and <u>Source</u> headings will be highlighted in <span class='ui-widget ui-state-ok'>green</span>.</li>
	<li>Use the <span class='ui-icon ui-icon-circlesmall-plus' style='display: inline; margin-right: .3em;'>&nbsp;&nbsp;&nbsp;</span> and <span class='ui-icon ui-icon-circlesmall-minus' style='display: inline; margin-right: .3em;'>&nbsp;&nbsp;&nbsp;</span> icons at the top of each set of columns to increase/decrease the amount of information displayed.</li>
	<li>The Destination headings highlighted in green will be updated once you click the "Save Changes" button at the bottom of the page.</li>
	</ul></p>
<?php
	
	//arrPrint($_REQUEST);
	$all_heads = array();
	$ignite = "blank";
	$source = $_REQUEST['source'];
	$destination = $_REQUEST['destination'];
	$section = array(
		'KPI'=>array("Departmental SDBIP",""),
		'TOP'=>array("Top Layer SDBIP",""),
		'CAP'=>array("Capital Projects",""),
		'CF'=>array("Monthly Cashflows",""),
		'RS'=>array("Revenue By Source","")
	);
	$cmp_details = array('source'=>array(),'dest'=>array());
	/* GET ALL INFO */
	//GET SOURCE CMP INFO
	if($source=="BLANK") {
		$cmp_details['source'] = array('cmpcode'=>"IGNITE",'cmpname'=>"Default Headings");
	} else {
		$sql = "SELECT cmpcode, cmpname FROM assist_company WHERE cmpcode = '".strtoupper($source)."'";
		$cmp_details['source'] = mysql_fetch_one($sql,"A");
	}
	//GET DEST CMP INFO
	$sql = "SELECT cmpcode, cmpname FROM assist_company WHERE cmpcode = '".strtoupper($destination)."'";
	$cmp_details['dest'] = mysql_fetch_one($sql,"A");
	
	//GET HEADINGS FROM BLANK
	$all_heads['def'] = array();
	$all_heads['def'] = getCopyHeadings("blank");
	
	//GET HEADINGS FROM SOURCE
	$all_heads['src'] = array();
	$all_heads['src'] = getCopyHeadings($source);
	
	//GET HEADINGS FROM DESTINATION
	$all_heads['dest'] = array();
	$all_heads['dest'] = getCopyHeadings($destination);
//arrPrint($all_heads);

	$main_head = array(
		'def'	=> "Default Headings",
		'src'	=> "Source:<br />".$cmp_details['source']['cmpcode'].": ".$cmp_details['source']['cmpname']."",
		'dest'	=> "Destination:<br />".$destination.": ".$cmp_details['dest']['cmpname'].""
	);
	$sub_head = array(
		'h_client'=>"Heading",
		'c_required'=>"Required",
		'list_view'=>"List Display",
		'c_sort'=>"Display Order",
		'c_maxlen'=>"Text Length",
		'glossary'=>"Glossary"
	);
	$colspan=count($sub_head);


	/* DISPLAY LIST FOR REVIEW:
			Default headings are displayed as is.
			Source headings are compared to default and highlighted in orange where changes.
			Destination headings are compared to Source and highlighted in green where changes.
	*/
	echo "	
	<form name=frm_review action=SDBP4_headings.php method=post >
	<input type=hidden name=act value=SAVE />
	<input type=hidden name=source value='$source' />
	<input type=hidden name=destination value='$destination' />

	<table>
		<tr>";
	foreach($main_head as $key => $h) {
		if($key!="def") { echo "<th></th>"; }
		echo "<th colspan=$colspan id=th_".$key." style='vertical-align: top'><span id=spn_".$key."_hideme key=$key class='ui-icon ui-state-white ui-icon-circlesmall-minus click_to_hide' style='float: right;margin-right: .3em;'></span>".$h."</th>";
	}
	echo "</tr>";
	foreach($section as $x => $sec) {
		echo "<tr><th class=sec colspan=".($colspan*3+2).">".$sec[0]."</th></tr>";
		echo "<tr>"; foreach($main_head as $key => $h) {
			if($key!="def") { echo "<th class=th2></th>"; }
			foreach($sub_head as $sub_key => $sub) {
				if($sub_key!="h_client") { $hide = $key."_hideme"; } else { $hide = ""; }
				echo "<th class='th2 $hide '>$sub</th>";
			}
		} echo "</tr>";
		foreach($all_heads['def'][$x] as $k => $z) {
			foreach($main_head as $key => $h) {
				$my_head = $all_heads[$key][$x][$k];
				switch($key) {
				case "def":  
					$class = "b";	
					echo "<tr>"; 		
					$div = "";
					break;
				case "src":	 
					$class = "";	
					echo "<td></td>";	
					break;
				case "dest": 
					$class = "i";	
					echo "<td></td>";	
					break;
				}
				foreach($sub_head as $sub_key => $sub) {
					if($sub_key!="h_client") { $hide = $key."_hideme"; } else { $hide = ""; }
					$v = $my_head[$sub_key];
					$div = "";
						if($key=="dest" && $v!=$all_heads['src'][$x][$k][$sub_key]) {
							$div = "<div class='ui-state-ok'>";
						} elseif($key=="src" && $v!=$all_heads['def'][$x][$k][$sub_key]) {
							$div = "<div class='ui-state-info'>";
						}
					$style = "";
					switch($sub_key) {
					case "glossary": 
						$style = "style='text-size: 6pt;'";
						break;
					case "c_required": 
					case "list_view":
						$style = "style='text-align:center'";
						if((int)$v>0) { $v = "<div align=center><span class='ui-icon ui-icon-check' style='margin-right: .3em;'></span></div>"; } else { $v = "-"; }
						break;
					case "c_maxlen":
						if(!in_array($my_head['h_type'],array("TEXT","VC"))) {
							$v = "N/A";
						} elseif((int)$v==0) {
							$v = "> 500 characters";
						} else {
							$v = $v." characters";
						}
						break;
					case "c_sort":
						$style = "style='text-align:center'";
						break;
					}
					echo "<td class='$class $hide' $style > $div".$v."</div></td>";
				}
				if($key == "dest") { echo "</tr>"; }
			}
		}
	}
	echo "</table>
	<p><input type=submit value='Save Changes' class=isubmit /></p>
	</form>";
	?>
	<script type=text/javascript>
	$(function() {
		var colspan = <?php echo count($sub_head); ?>;
		$("span.click_to_hide").css("cursor","hand");
		$("span.click_to_hide").click(function() {
			var key = $(this).attr("key");
			var hideme = "table td."+key+"_hideme, table th."+key+"_hideme";
			if($(this).hasClass("ui-icon-circlesmall-minus")) {
				$(hideme).hide();
				$("#th_"+key).attr("colspan","1");
				$(this).toggleClass("ui-icon-circlesmall-minus ui-icon-circlesmall-plus");
			} else {
				$(hideme).show();
				$("#th_"+key).attr("colspan",colspan);
				$(this).toggleClass("ui-icon-circlesmall-plus ui-icon-circlesmall-minus");
			}
		});
		$("#spn_def_hideme").trigger("click");
	});
	</script>
	<?php
	$page = ob_get_contents();
	ob_end_flush();
	$page = str_replace('<script type ="text/javascript" src="/','<script type ="text/javascript" src="http://assist.ignite4u.co.za/',$page);
	$page = str_replace('href="/','href="http://assist.ignite4u.co.za/',$page);
	/*$fp = fopen("output.html","w");
	fwrite($fp,$page);
	fclose($fp);*/
// 	saveEcho($folder, $filename, $echo);
		$filename = "../files/".strtolower($destination)."/".strtoupper($mod_ref)."/import/HEAD_".date("YmdHis")."_".$act.".html";
        $file = fopen($filename,"w");
        fwrite($file,$page."</body></html>\n");
        fclose($file);
	break;
case "SAVE":
	if(isset($_REQUEST['destination'])) {
		$import_log_file = openLogImport($_REQUEST['destination']);
		logImport($import_log_file, "SDBIP ".$mod_year." > Copy Headings > Copy from ".$_REQUEST['source']." [SAVE]", "SDBP4_headings.php");
	}
	echo "<ol>";
	$source = $_REQUEST['source'];
	$src_dbref = "assist_".(strtoupper($source)=="BLANK" ? "blank_sdbp4" : strtolower($source)."_".$mod_ref);
	$destination = $_REQUEST['destination'];
	$dest_dbref = "assist_".strtolower($destination)."_".$mod_ref;
	//Confirm source is valid client & has necessary tables
	echo "<li>Validating Source client... ";
	$sql = "SHOW TABLES LIKE  '".$src_dbref."_setup_headings%'";
//echo $sql." :: ".$source;
	$src_tbls = mysql_fetch_all($sql,"O",strtolower($source));
//arrPrint($src_tbls);
	$src_db = $db_other.strtolower($source);
	if(count($src_tbls)!=2) {
		die("<span class=idelete>ERROR!</span> An error occurred while trying to access the source ($source) headings tables.  Copy could NOT continue.");
	}
	echo "<span class=isubmit>success!</span></li>
	<li>Validating Destination client...";
	//Confirm Destination is valid client & has necessary tables
	$sql = "SHOW TABLES LIKE  '".$dest_dbref."_setup_headings%'";
	$dest_tbls = mysql_fetch_all($sql,"O",strtolower($destination));
	$dest_db = $db_other.strtolower($destination);
	if(count($src_tbls)!=2) {
		die("<span class=idelete>ERROR!</span> An error occurred while trying to access the destination ($destination) headings tables.  Copy could NOT continue.");
	}
	echo "<span class=isubmit>success!</span></li>
	<li>Copying headings from source ($source) to destination ($destination)...";
	//Truncate destination tables
	$sql = array();
	$sql[] = "TRUNCATE ".$dest_db.".".$dest_dbref."_setup_headings";
	$sql[] = "TRUNCATE ".$dest_db.".".$dest_dbref."_setup_headings_setup";
	//Insert into destination select * from source
	$sql[] = "INSERT INTO ".$dest_db.".".$dest_dbref."_setup_headings SELECT * FROM ".$src_db.".".$src_dbref."_setup_headings";
	$sql[] = "UPDATE ".$dest_db.".".$dest_dbref."_setup_headings SET h_length = 200 WHERE h_length > 0";
	$sql[] = "INSERT INTO ".$dest_db.".".$dest_dbref."_setup_headings_setup SELECT * FROM ".$src_db.".".$src_dbref."_setup_headings_setup";
	foreach($sql as $s) {
		$rs = mysql_getRS($s,"O",strtolower($destination));
		unset($rs);
	}
	//Log
	$sql = "INSERT INTO ".$dest_db.".".$dest_dbref."_setup_log (slog_section, slog_ref, slog_tkid, slog_tkname, slog_date, slog_field, slog_transaction, slog_old, slog_new, slog_yn, slog_lsql) VALUES
	('SETUP','HEAD','".$_SESSION['tid']."','".code($_SESSION['tkn'])."',now(),'','Overwrote headings with those from $source','','','N','".addslashes(implode(chr(10),$sql))."')";
	db_insert($sql,"O",strtolower($destination));	
	echo "<span class=isubmit>success!</span></li>
	</ol>
	<p>Copy process complete.</p>"; displayGoBack("main.php","Back to Ignite Assist Admin");
	break;
}



?>
</body></html>