<?php
include("inc_ignite.php");
include("../inc_codehelper.php");
//error_reporting(-1);
$cmpcode = $_GET['cc'];
if(strlen($cmpcode)==0)
{
    echo("<script type=text/javascript>document.location.href = 'main.php';</script>");
}
else
{
    $cmpcode = strtolower($cmpcode);
}
include("../inc_db.php");
if(isset($_REQUEST['e'])) {
	$act = (isset($_REQUEST['act'])) ? ucwords($_REQUEST['act'])."ed" : "Reopened";
	$e = $_REQUEST['e'];
	if(checkIntRef($e)) {
		$sql = "UPDATE assist_".$cmpcode."_pmsps_eval_status SET status = 'pending', locked = 0 WHERE id = ".$e;
		include("inc_db_con.php");
		$sql = "INSERT INTO assist_".$cmpcode."_log (date,tkid,ref,transaction,tsql,told) VALUE ('$today','IA','PMSPS','".$act." evaluation status ".$e."','".code($sql)."','')";
		include("inc_db_con.php");
		$result[0] = "check";
		$result[1] = "Evaluation ".$e." ".strtolower($act).".";
	}
}

if(isset($_REQUEST['s'])) {
	$start = $_REQUEST['s'];
} else {
	$start = 0;
}

$t = $_REQUEST['t'];

$sql = "SELECT count(e.id) as c ";
$sql.= " FROM assist_".$cmpcode."_pmsps_eval_status e ";
$sql.= " INNER JOIN assist_".$cmpcode."_timekeep tk ON tk.tkid = e.user_id ";
$sql.= " INNER JOIN assist_".$cmpcode."_pmsps_eval_year y ON y.id = e.eval_year_id ";
$sql.= " WHERE tk.tkstatus <> 0 ";
if(strlen($t)>0) { 
	$tc = strFn("explode",strtolower(code($t))," ","");
	$sql.= " AND (tk.tkname LIKE '%";
	$sql.= strFn("implode",$tc,"%' OR tk.tkname LIKE '%","");
	$sql.= "%' OR tk.tksurname LIKE '%";
	$sql.= strFn("implode",$tc,"%' OR tk.tksurname LIKE '%","");
	$sql.= "%') "; 
}
include("inc_db_con.php");
$r = mysql_fetch_array($rs);
mysql_close($con);

$c = $r['c'];

if($start == 0) { 
	$back = 0; 
} else { 
	$back = $start - 50; 
	if($back<0) { $back = 0; }
}

if($c<50) { 
	$next = 0; 
	$end = 0; 
	$pages = 1;
	$page = 1;
} else {
	$pages = ceil($c/50);
	if($start > 0) {
		$page = ceil($start/50) + 1;
	} else {
		$page = 1;
	}
	$end = floor($c/50);
	if($end==$c/50) { $end--; }
	$end*=50;
	if($start==$end) {
		$next = 0;
	} else {
		$next = $start + 50;
	}
}


?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
<base target="main">
</head>
		<link type="text/css" href="/SDBP3/lib/jquery-ui-1.7.1.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="/SDBP3/lib/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="/SDBP3/lib/jquery-ui-1.7.1.custom.min.js"></script>
<link rel="stylesheet" href="/default.css" type="text/css">
<style type=text/css>
    table {
        border-collapse: collapse;
        border: 1px solid #ababab;
    }
    table td {
        border: 1px solid #ababab;
    }
    table th {
        border: 1px solid #ffffff;
		background-color: #CC0001;
    }
</style>
<script type=text/javascript>
function search() {
	var t = document.getElementById('t').value;
	if(t.length > 0) {
		document.location.href = 'pmsps_eval.php?cc=<?php echo $cmpcode; ?>&s=0&t='+t;
	}
}
function reset() {
		document.location.href = 'pmsps_eval.php?cc=<?php echo $cmpcode; ?>&s=0';
}
</script>
<body topmargin=10 leftmargin=10 bottommargin=0 rightmargin=5>
<?php
?>
<h1><b>Performance Assist: Reset Evaluation</b></h1>
<?php displayRes($result); ?>
<table cellpadding=3 cellspacing=0 width=800 style="margin-bottom: 10px;">
	<tr>
		<td>
			<?php if($start > 0) { ?>
				<input type=button value=" |< " onclick="document.location.href = 'pmsps_eval.php?cc=<?php echo $cmpcode; ?>&s=0';">&nbsp;
			<?php }
			if($back > 0 || $start > 0) { ?>
				<input type=button value=" < " onclick="document.location.href = 'pmsps_eval.php?cc=<?php echo $cmpcode; ?>&s=<?php echo $back; ?>';">&nbsp;
			<?php } ?>
				Page <?php echo $page; ?>/<?php echo $pages; ?>  <!-- showing Records <?php echo ($start+1)." - X of ".$c; ?> -->
			<?php if($next > 0) { ?>
				&nbsp;<input type=button value=" > " onclick="document.location.href = 'pmsps_eval.php?cc=<?php echo $cmpcode; ?>&s=<?php echo $next; ?>';">
			<?php }
			if($end > 0) { ?>
				&nbsp;<input type=button value=" >| " onclick="document.location.href = 'pmsps_eval.php?cc=<?php echo $cmpcode; ?>&s=<?php echo $end; ?>';">
			<?php } ?>
			<?php if(strlen($t)>0) { ?>
			<span style='float:right'><input type=button value="Reset Search" onclick=reset();></span>
			<?php } else { ?>
			<span style='float:right'>Search User: <input type=text id=t> <input type=button value=Go onclick=search();></span>
			<?php } ?>
		</td>
	</tr>
</table>
<table cellpadding=3 cellspacing=0 width=800>
<tr>
	<th>Ignite Ref</th>
	<th>User Name</th>
	<th>Year</th>
	<th>Quarter</th>
	<th>Status</th>
	<th>Action</th>
</tr>
<?php
$sql = "SELECT e.id, tk.tkname, tk.tksurname, tk.tkid, e.eval_year_id, e.eval_period, e.status, y.name, e.locked ";
$sql.= " FROM assist_".$cmpcode."_pmsps_eval_status e ";
$sql.= " INNER JOIN assist_".$cmpcode."_timekeep tk ON tk.tkid = e.user_id ";
$sql.= " INNER JOIN assist_".$cmpcode."_pmsps_eval_year y ON y.id = e.eval_year_id ";
$sql.= " WHERE tk.tkstatus <> 0 ";
if(strlen($t)>0) { 
	$sql.= " AND (tk.tkname LIKE '%";
	$sql.= strFn("implode",$tc,"%' OR tk.tkname LIKE '%","");
	$sql.= "%' OR tk.tksurname LIKE '%";
	$sql.= strFn("implode",$tc,"%' OR tk.tksurname LIKE '%","");
	$sql.= "%') "; 
}
$sql.= " ORDER BY tk.tkname, tk.tksurname, y.name, e.eval_period ";
$sql.= " LIMIT $start, 50";
include("inc_db_con.php");
while($row = mysql_fetch_assoc($rs)) {
	echo "<tr>";
		echo "<td>".$row['id']."</td>";
		echo "<td>".$row['tkname']." ".$row['tksurname']."</td>";
		echo "<td>".$row['name']."</td>";
		echo "<td style=\"text-align: center;\">".$row['eval_period']."</td>";
		echo "<td>".$row['status'].( (strtolower($row['status'])=="pending" && $row['locked']==1) ? " (locked)" : "")."</td>";
		echo "<td>";
			if(strtolower($row['status'])=="completed") {
				echo "<input type=button value=Reopen onclick=\"if(confirm('Are you sure you want to reopen evaluation ".$row['id']."?')) { document.location.href = 'pmsps_eval.php?cc=".$cmpcode."&e=".$row['id']."&act=reopen&t=$t'; }\">";
			} elseif(strtolower($row['status'])=="pending" && $row['locked']==1) {
				echo "<input type=button value=Unlock onclick=\"if(confirm('Are you sure you want to unlock evaluation ".$row['id']."?')) { document.location.href = 'pmsps_eval.php?cc=".$cmpcode."&e=".$row['id']."&act=unlock&t=$t'; }\">";
			}
		echo "</td>";
	echo "</tr>";
}
mysql_close($con);


?>
</table>
<p style="margin-top: 30px"><a href="main.php"><img src=/pics/tri_left.gif align=absmiddle border=0><span style="text-decoration: none;"> </span>Go Back</a></p>

</body>

</html>
