<?php


//require '../header.php';

require_once("../module/autoloader.php");
require_once("../inc_db.php");
require_once("../inc_db_conn.php");


$me = new ASSIST();
$me->echoPageHeader();

$mdb = new ASSIST_DB("master");

$mod_year = "2018/2019";
$mod_ref = "sdp18";
$mod_loc = "SDBP5B";

$dbs = array();
$link = mysql_connect("localhost",$db_user,$db_pwd);
$db_list = mysql_list_dbs($link);
while ($row = mysql_fetch_object($db_list)) {
     $dbs[] = $row->Database;
}


//print_r($dbs);









function verifyClientModule($client,&$verify) {
	global $dbs, $db_other, $mdb, $mod_loc,$mod_year, $mod_ref;
	echo "<ol>
	<li>Verifying that selected client is a valid Assist Client ... ";
	//get client name
	$sql = "SELECT cmpname FROM assist_company WHERE cmpcode = '$client' AND cmpstatus = 'Y'";
	$rows = $mdb->mysql_fetch_all($sql);
//	print_r($rows);
	if(count($rows)!=1) { $verify['client'] = false; }
	$cmp = $rows[0];
	$db_name = $db_other.strtolower($client);
	if(!in_array($db_name,$dbs)) { $verify['client'] = false; }
	if(!$verify['client']) {
		echo "<span class=idelete>failure</span> - client $client does not appear to be a valid client.  Please contact the Assist Helpdesk for assistance.</li>"; die();
	} else {
		echo "<span class=isubmit>success</span>.</li>";
	}
	echo "<li>Verifying that SDBIP ".$mod_year." has not already been created as a module for client $client ... ";
	$cdb = new ASSIST_DB("client",$client);
	$sql = "SELECT * FROM assist_menu_modules WHERE modlocation = '".$mod_loc."' AND modref = '".strtoupper($mod_ref)."'";
	$rows = $cdb->mysql_fetch_all($sql);
//print_r($rows);
	if(count($rows)!=0) { $verify['module'] = false; }
	if(!$verify['module']) {
		echo "<span class=idelete>failure</span> - client $client already has access to the SDBIP ".$mod_year." module.  It cannot be created again.</li>"; die();
	} else {
		echo "<span class=isubmit>success</span>.</li>";
	}
	
	return $cmp;

//	return array('verify'=>$verify,'cmp'=>$cmp);
}



function checkFolder($path) {
	global $cmpcode;
	if(strlen($cmpcode)>0) {
		$me =  $_SERVER['PHP_SELF'];
		$myloc = explode("/",$me);
		if(strlen($myloc[0])==0) { unset($myloc[0]); }
		$mc = count($myloc) - 1;
		$path = "files/".$cmpcode."/".$path;
		$chk = explode("/",$path);
		$chkloc = "";
		for($m=1;$m<count($myloc);$m++) { $chkloc.="../"; }
		foreach($chk as $c)
		{
			if(strlen($c)>0 && $c != "." && $c != "..") {
				$chkloc.= "/".$c;
				if(!is_dir($chkloc)) { mkdir($chkloc); }
			}
		}
	} else {
		die("An error has occurred.  Please go back and try again.");
	}
}








echo "<h1>Create New SDBIP ".$mod_year."</h1>";

$client = isset($_REQUEST['cc']) ? $_REQUEST['cc'] : die("<P>An error occurred while trying to determine the client.  Please go back and try again.</p>");
$action = isset($_REQUEST['act']) ? $_REQUEST['act'] : "VERIFY";


switch($action) {
case "CREATE":
	$err = 0;
	echo "<P class=b>Starting the SDBIP creation process...</p>";
	$verify = array('client'=>true,'module'=>true);
	//verify valid client
	verifyClientModule($client,$verify);
	//display message to request verification to create module
	if($verify['client'] && $verify['module']) {
		$db_name_client = $db_other.strtolower($client);
		$db_name_blank = $db_other."blank";
		//copy tables from blank
		echo "<li>Creating database structure...";
			//get blank tables
		$db_name = $db_name_blank;
		$tables = array();
		$sql = "SHOW TABLES LIKE 'assist_blank_".strtolower($mod_loc)."_%'";
//echo $sql;
		$rs = getRS($sql);
			while( ($tables[] = mysql_fetch_row($rs) ) || array_pop($tables));
		unset($rs);
//echo "<pre>"; print_r($tables); echo "</pre>";



		$db_name = $db_name_client;
		foreach($tables as $tbl) {
//echo "<p><B>".$tbl[0];
			$old_tbl = $tbl[0];
			$etbl = explode("_",$old_tbl); 
if($etbl[2]!=strtolower($mod_loc)) {
//	echo " :: ERROR - INVALID MODULE CODE ";
}else {
				//assist
				unset($etbl[0]); 
				//blank
				unset($etbl[1]); 
				//modlocation
				unset($etbl[2]);
			$new_tbl = "assist_".strtolower($client)."_".$mod_ref."_".implode("_",$etbl);
//echo "</b> :: ".$new_tbl;
			//delete existing table (in case of interrupted creation process)
			$sql = "DROP TABLE IF EXISTS `".$db_name_client."`.`".$new_tbl."`";
//echo "<br />".$sql;
			$rs = getRS($sql);
			unset($rs);
			//create table
			$sql = "CREATE TABLE `".$db_name_client."`.`".$new_tbl."` LIKE `".$db_name_blank."`.`".$old_tbl."`";
//echo "<br />".$sql;
			$rs = getRS($sql);
			unset($rs);
			//insert fixed system data
			$sql = "INSERT INTO `".$db_name_client."`.`".$new_tbl."` SELECT * FROM `".$db_name_blank."`.`".$old_tbl."`";
//echo "<br />".$sql;
			db_insert($sql);
}
//echo "</p>";
		}
		echo "<span class=isubmit>done</span>.</li>";

		//create files folder
		echo "<li>Creating folder structure...";
		$cmpcode = strtolower($client);
		$path = array();
		$path[] = strtoupper($mod_ref)."/exports";
		$path[] = strtoupper($mod_ref)."/import";
		$path[] = strtoupper($mod_ref)."/reports";
		$path[] = strtoupper($mod_ref)."/STASKS";
		foreach($path as $p) {
			checkFolder($p);
		}
		echo "<span class=isubmit>done</span>.</li>";
		
		
		echo "<li>Creating the menu line item... ";
		//create assist_menu_modules record with modyn = 'n'
		$sql = "INSERT INTO assist_menu_modules (modtext,modcontents,modlocation,modref,modyn,modadminyn,mod_billable) VALUES ('SDBIP ".$mod_year."','Y','".strtoupper($mod_loc)."','".strtoupper($mod_ref)."','Y','Y',0)";
		$modmenuid = db_insert($sql);
		//create assist_cmpcode_menu_modules record limited to 1 user
		$sql = "INSERT INTO assist_".strtolower($client)."_menu_modules (modmenuid,modcustom,moduserallowed,modusercount) VALUES ($modmenuid,'N',".($is_mnr==1 ? 1 : 0).",".($is_mnr==1 ? 1 : 0).")";
		$modid = db_insert($sql);
		echo "<span class=isubmit>done</span>.</li>";
		

		echo "</ol>";
		if($err == 0) {
			echo "<P class=b>Creation process completed successfully.</p><script type=text/javascript>document.location.href = 'main.php?r[]=ok&r[]=".urlencode("SDBIP ".$mod_year." has been successfully created on client ".$client.".")."';</script>";
		} else {
			echo "<P class=b>Creation process completed with errors.  Please read the messages above.</p>";
		}
		//update assist_client_module_activity_log
		$sql = "INSERT INTO assist_client_module_activity_log VALUES (null, '".strtoupper($client)."', '".strtoupper($mod_ref)."', '".strtoupper($mod_loc)."', '".strtoupper($_SESSION['cc'])."', '".$_SESSION['tid']."', '".$_SESSION['tkn']."','C',now(),1)";
		$logid = Adb_insert($sql);



	} else {
		die("<p class=idelete>An error occurred.</p>");
	}


	break;

case "VERIFY":
default:
	$verify = array('client'=>true,'module'=>true);
	//verify valid client
	$cmp = verifyClientModule($client,$verify);
	//display message to request verification to create module
	echo "</ol><p>&nbsp;</p>";
	if($verify['client'] && $verify['module']) {
		echo "<div id=div_verify style='margin: 0 auto; width: 50%; border: 1px solid #000099; padding: 10 10 10 10; margin-top: 20px'>
		<h1 class=center>SDBIP ".$mod_year."</h1>
		<h2 class=center>Create Module</h2>
		<p class=center style='margin-top: 25px;'>Please confirm that you wish to create the SDBIP ".$mod_year." module [version: $mod_loc] for:</p>
		<p class='center iinform' style='font-size: 14pt; line-height: 18pt'>".$cmp['cmpname']."<br />($client)</p>
		<p class=center>&nbsp;</p>
		<p class=center style='margin-bottom: 25px;'><input type=button value=Create class=isubmit style='margin-right: 30px' /><input type=button value=Cancel class=idelete style='margin-left: 30px' /></p>
		<form name=create_sdbip method=post action=".$mod_loc."_create.php>
		<input type=hidden name=cc value='$client' /><input type=hidden name=act value='' />
		</form>
		</div>
		<div id=div_dialog title='Processing...'></div>
		<script type=text/javascript>
		$(function() {
			$('div input:button').click(function() { 
				if($(this).val()=='Create' && $(this).hasClass('isubmit')) {
					if($('form[name=create_sdbip] input[name=cc]').val().length>0) {
						$('form[name=create_sdbip] input[name=act]').val('CREATE');
						alert('Please be patient while the create process runs.  It can take some time.');
						//$('#div_dialog').html('<p class=center><img src=\"../pics/ajax_loader_v2.gif\" /></p>').dialog();
						AssistHelper.processing();
						$('form[name=create_sdbip]').submit();
					} else {
						document.location.href = 'main.php?r[]=error&r[]=An+error+occurred+while+trying+to+create+the+SDBIP+for+".$client.".%20%20Please+try+again.';
					}
				} else {
					document.location.href = 'main.php?r[]=info&r[]=SDBIP+create+process+for+".$client."+has+been+cancelled.';
				}
			});
		});
		</script>
		";
	} else {
		die("<p class=idelete>An error occurred.</p>");
	}

	break;
}






unset($mdb);

?>
</body></html>