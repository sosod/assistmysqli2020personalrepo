<?php
include("inc_ignite.php");
require_once '../inc_db.php';
error_reporting(-1);


/***************************
*** DATABASE CONNECTIONS ***
***************************/
//HELP		=> dev_help			=> Ignite Assist Help db
//COMPANY	=> $db_name			=> Reseller's db	/	person logged in
//ASSIST	=> $db_assist		=> General db
//OTHER		=> $db_other.$cc	=> Client's db


function db_query($dbn,$sql) {
	global $db_user, $db_pwd, $db_name, $db_assist, $db_help, $db_other;

	switch($dbn) {
		case "H":	$db = $db_help; 	break;
		case "A":	$db = $db_assist; 	break;
		case "C":	$db = $db_name;		break;
		default:	$db = $db_other.strtolower($dbn);	break;
	}

	$con = mysql_connect("localhost",$db_user,$db_pwd);
	if(!$con) {
		$sqlerror = "<p>&nbsp;</p><p><b>---ERROR DETAILS BEGIN---</b></p><ul><li>Database error on CON: ".mysql_error()."</li><li>".$sql."</li><li>".$db."</li><li>Page: ".$_SERVER['PHP_SELF']."</li><li>Name: ".$_SERVER['SERVER_NAME']."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
		mail("igniteassistdev@gmail.com","DB Error CON",$sqlerror,"Content-type: text/html; charset=us-ascii");
		die("<h1>Error</h1><p>Unfortunately Ignite Assist has encountered an error.<br>An email has been sent to Ignite Reporting Systems with the error details.<br>Thank you.</p>");
	}
	$db_selected = mysql_select_db($db,$con);
	if(!$db_selected) {
		$sqlerror = "<p>&nbsp;</p><p><b>---ERROR DETAILS BEGIN---</b></p><ul><li>Database error on CON: ".mysql_error()."</li><li>".$sql."</li><li>".$db."</li><li>Page: ".$_SERVER['PHP_SELF']."</li><li>Name: ".$_SERVER['SERVER_NAME']."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
		mail("igniteassistdev@gmail.com","DB Error MSD",$sqlerror,"Content-type: text/html; charset=us-ascii");
		die("<h1>Error</h1><p>Unfortunately Ignite Assist has encountered an error.<br>An email has been sent to Ignite Reporting Systems with the error details.<br>Thank you.</p>");
	}

	$rs = mysql_query($sql,$con);
	if(!$rs)
	{
		$sqlerror = "<p>&nbsp;</p><p><b>---ERROR DETAILS BEGIN---</b></p><ul><li>Database error on CON: ".mysql_error()."</li><li>".$sql."</li><li>".$db."</li><li>Page: ".$_SERVER['PHP_SELF']."</li><li>Name: ".$_SERVER['SERVER_NAME']."</li></ul><p><b>---ERROR DETAILS END---</b></p>";
		mail("igniteassistdev@gmail.com","DB Error RS",$sqlerror,"Content-type: text/html; charset=us-ascii");
		die("<h1>Error</h1><p>Unfortunately Ignite Assist has encountered an error.<br>An email has been sent to Ignite Reporting Systems with the error details.<br>Thank you.</p>");
	}
	return $rs;
}

function mysql_fetch_all1($dbn,$sql,$fld) {
	$result = db_query($dbn,$sql);
    $resultArray = array();
    while($row = mysql_fetch_assoc($result)) {
		$resultArray[$row[$fld]] = $row;
	}
    return $resultArray;
}

function mysql_fetch_all2($dbn,$sql,$fld1,$fld2) {
	$result = db_query($dbn,$sql);
    $resultArray = array();
    while($row = mysql_fetch_assoc($result)) {
		$resultArray[$row[$fld1]][$row[$fld2]] = $row;
	}
    return $resultArray;
}

function db_sql($dbn,$sql) {
	$rs = db_query($dbn,$sql);
	switch(strtoupper(substr($sql,0,strpos($sql,' ')))) {
		case "INSERT": 					return mysql_insert_id(); 		break;
		case "UPDATE": case "DELETE":	return mysql_affected_rows();	break;
		default: 						return $rs;					break;
	}
}


?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
<base target="main">
</head>
<link rel="stylesheet" href="/assist.css" type="text/css">
<!-- <link rel="stylesheet" href="/SDBP4/inc/main.css" type="text/css"> -->
<style type=text/css>
    table {
        border-collapse: collapse;
        border: 1px solid #ababab;
    }
    table td {
        border: 1px solid #ababab;
    }
</style>
<body topmargin=10 leftmargin=10 bottommargin=0 rightmargin=5>
<?php
if(isset($_REQUEST['cc']) && strlen($_REQUEST['cc'])==7) {
	$cmpcode = $_REQUEST['cc'];
	$sql = "SELECT cmpname FROM assist_company WHERE cmpcode = '$cmpcode'";
	include("inc_db_con_assist.php");
		$cmp = mysql_fetch_array($rs);
	mysql_close($con);
	
	if(!isset($_REQUEST['act'])) {
		?>
		<h2 align=center>SDBIP 2011/2012</h2>
		<p>&nbsp;</p>
		<div align=center>
		<table width=450 style="border: 1px solid #ababab;"><tr><td>
		<form name=create action=sdbp4_create.php method=post><input type=hidden name=cc value="<?php echo $cmpcode; ?>" />
		<p align=center>Create SDBIP 2011/2012 on <?php echo $cmpcode; ?>: <?php echo $cmp['cmpname']; ?>?</p>
		<p align=center><input type=submit name=act value="  Yes  " class=isubmit /> <input type=submit name=act value="  No  " style="float: right;" class=ireset /></p>
		</form>
		</div>
		</td></tr></table>
		<?php
		echo "<p style=\"margin-top: 30px\"><a href=\"main.php\"><img src=/pics/tri_left.gif align=absmiddle border=0><span style=\"text-decoration: none;\"> </span>Go Back</a></p>";
	} else {
		if(trim($_REQUEST['act']) == "Yes") {
			$db_name = $db_other.strtolower($cmpcode);
			echo "<p>Loading SDBIP 2011/2012...";
$sql = "SHOW TABLES LIKE 'assist_".strtolower($cmpcode)."_sdp11_setup_log'";
$show = db_query("C",$sql);
if(mysql_num_rows($show)==0) {
			$import = fopen("sdbp4_create.txt","r");
			$data = array();
			while(!feof($import)) {
				$tmp = fgets($import);
				$tmp = str_replace("_janet12_","_".strtolower($cmpcode)."_",$tmp);
				$data[] = $tmp;
			}
			fclose($import);
			foreach($data as $sql) { $mar = db_sql($cmpcode,stripslashes($sql)); }
}
			echo "done.";
			echo "<p>Giving Ignite Support menu access...";
			$sql = "INSERT INTO assist_menu_modules (modid, modtext, modcontents, modlocation, modref, modyn, modadminyn) VALUES (null,'SDBIP 2011/2012','Y','SDBP4','SDP11','Y','Y')";
			$modid = db_sql($cmpcode,$sql);
			$sql = "INSERT INTO `assist_".strtolower($cmpcode)."_menu_modules` (`modid`, `modmenuid`, `modcustom`, `moduserallowed`, `modusercount`) VALUES (NULL, '".$modid."', 'N', '1', '1')";
			$mod_id = db_sql($cmpcode,$sql);
			$sql = "SELECT tkid FROM assist_".strtolower($cmpcode)."_timekeep WHERE tkuser = 'support' AND tkname = 'Ignite' AND tksurname = 'Support'";
			//include("inc_db_con.php");
			//$tk = mysql_fetch_assoc($rs);
			//$tk = $tk['tkid'];
			$tk = mysql_fetch_assoc(db_sql($cmpcode,$sql));
			if(strlen($tk['tkid'])==4) {
				$sql = "INSERT INTO `assist_".strtolower($cmpcode)."_menu_modules_users` (`usrid`, `usrmodid`, `usrtkid`, `usrmodref`) VALUES (NULL, '".$mod_id."', '".$tk['tkid']."', 'SDP11');";
				db_sql($cmpcode,$sql);
				echo "done.</p>";
			} else {
				echo "<span style=\"color: red; font-weight: bold;\">Ignite Support not found.</span><br />Please login as admin on the client database and manually grant the Ignite Support user access via the User > Edit function.</p>";
			}
			$db_name = $db_other."ign0001";
			$sql = "UPDATE assist_admin_sdbp4_create SET active = false, tkid = '".$_SESSION['tid']."', swhen = now() WHERE cmpcode = '".strtoupper($cmpcode)."'";
			db_sql("ign0001",$sql);
			echo "<p class=isubmit><b>SDBIP 2011/2012 successfully created.</b></p>";
		} else {
			echo "No";
		}
	}
	
	
	
} else {
	die("<p>An error has occurred.  Please go back and try again.</p><p style=\"margin-top: 30px\"><a href=\"main.php\"><img src=/pics/tri_left.gif align=absmiddle border=0><span style=\"text-decoration: none;\"> </span>Go Back</a></p>");
}


?>
</body>

</html>
