<?php
require_once("../module/autoloader.php");

//ASSIST_HELPER::arrPrint($_REQUEST);

$client_cmpcode = strtolower($_REQUEST['cc']);

$me = new ASSIST_MODULE_HELPER("client",$client_cmpcode,false,"PMSPS");
$me->setDBRef("PMSPS",$client_cmpcode);


/** *************************************** GET DATA TO POPULATE LISTS ******************************************** **/


//Get financial years
$eval_year_id = $_REQUEST['eval_year_id'];
$sql = "SELECT name FROM ".$me->getDBRef()."_eval_year WHERE id = ".$eval_year_id;
$eval_year_name = $me->mysql_fetch_one_value($sql,"name");

//get departments
$dept_id = $_REQUEST['dept_id'];
if($dept_id=="X") {
	$sql = "SELECT id, value as name FROM assist_".$client_cmpcode."_list_dept";
} else {
	$sql = "SELECT id, value as name FROM assist_".$client_cmpcode."_list_dept where id = ".$dept_id;
}
$dept = $me->mysql_fetch_value_by_id($sql,"id","name");
//echo "<hr /><h1 class='green'>Helllloooo  from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($dept);

//get job titles
$sql = "SELECT id, value as name FROM assist_".$client_cmpcode."_list_jobtitle";
$job_titles = $me->mysql_fetch_value_by_id($sql,"id","name");
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($job_titles);
//echo "<hr />";



//get users
$user_id = $_REQUEST['user_id'];
$sql = "SELECT tkid as id, CONCAT(tkname, ' ',tksurname) as name, tkstatus as status , tkdept as dept_id, tkdesig as job_title_id
		FROM assist_".$client_cmpcode."_timekeep 
		";
if($user_id=="X") {
	$status = "";
	switch($_REQUEST['rad_user_type']) {
		case "ALL":
			//no limit on user status for all users
			break;
		case "ACTIVE":
			$status = " WHERE tkstatus = 1 ";
			break;
		case "INACTIVE":
			$status = " WHERE tkstatus = 2 ";
			break;
		case "CURRENT":
		default:
			$status = " WHERE tkstatus > 0 ";
			break;
	}
	$sql.=$status;
	if($dept_id!="X") {
		$sql.= (strlen($status)>0?" AND ":" WHERE ")." tkdept = ".$dept_id;
	}
	$sql.= "";
} else {
	$sql.= "WHERE tkid = ".$user_id."";
}
$sql.= " ORDER BY tkname, tksurname";
$users = $me->mysql_fetch_all_by_id($sql,"id");
//echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//ASSIST_HELPER::arrPrint($users);
//echo "<hr />";



//IF THERE ARE USERS THEN GET DATA
if(count($users)>0) {
/** **************************** NOW GET RAW DATA ****************************** **/
	$process = true;
	switch($_REQUEST['src_table']) {
		case "kpa":
			$headings = array(
				'id'=>"System Ref",
				'eval_year_id'=>"Financial Year",
				'user_id'=>"User",
				'user_status'=>"User Status",
				'dept_id'=>"Department",
				'job_title_id'=>"Job Title",
				'type'=>"",
				'kpa_type'=>"KPA Type",
				'name'=>"",
				'description'=>"",
				'measurement'=>"",
				'comments'=>"",
				'weight'=>"",
				'outcome'=>"",
				'evidence'=>"",
				'sdbip_num'=>"SDBIP Num",
			);
			$sql = "SELECT * FROM ".$me->getDBRef()."_kpa WHERE eval_year_id = ".$eval_year_id." AND user_id IN ('".implode("','",array_keys($users))."') ORDER BY user_id, id";
			break;
		case "job_function":
			$headings = array(
				'id'=>"System Ref",
				'eval_year_id'=>"Financial Year",
				'user_id'=>"User",
				'user_status'=>"User Status",
				'dept_id'=>"Department",
				'job_title_id'=>"Job Title",
				'type'=>"",
				'name'=>"",
				'description'=>"",
			);
			$sql = "SELECT * FROM ".$me->getDBRef()."_job_function WHERE eval_year_id = ".$eval_year_id." AND user_id IN ('".implode("','",array_keys($users))."') ORDER BY user_id, id";
			break;
		case "kpa_org":
			$headings = array(
				'id'=>"System Ref",
				'eval_year_id'=>"Financial Year",
				'user_id'=>"User",
				'user_status'=>"User Status",
				'dept_id'=>"Department",
				'job_title_id'=>"Job Title",
				'type'=>"",
				'name'=>"",
				'objective'=>"",
				'kpi'=>"KPI",
				'measurement'=>"",
				'baseline'=>"",
				'target_unit'=>"",
				'target1'=>"",
				'target2'=>"",
				'target3'=>"",
				'target4'=>"",
				'weight'=>"",
				'comments'=>"",
				'group_num'=>"",
				'sdbip_num'=>"SDBIP Num",
				'outcome'=>"",
				'evidence'=>"",
			);
			$sql = "SELECT * FROM ".$me->getDBRef()."_kpa_org WHERE eval_year_id = ".$eval_year_id." AND user_id IN ('".implode("','",array_keys($users))."') ORDER BY user_id, id";
			break;
		default:
			$process = false;
			$headings = array();
			break;
	}
	if($process) {
		$rows = $me->mysql_fetch_all($sql);
		/** **************************** NOW POPULATE OUTPUT ARRAY ****************************** **/
		$raw_data = array();
		$c = 0;
		$raw_data[$c] = array();
		foreach($headings as $fld => $head) {
			$raw_data[$c][$fld] = strlen($head)>0?$head:ucwords(str_replace("_"," ",$fld));
		}
		$c++;
		foreach($rows as $row) {
			if(count($row)>0 && isset($row['user_id'])) {
				$user_id = $row['user_id'];
				foreach($headings as $fld => $head) {
					switch($fld) {
						case "eval_year_id":
							$d = $eval_year_name;
							break;
						case "user_id":
							$d = $users[$user_id]['name'];
							break;
						case "user_status":
							if($users[$user_id]['status']==1) {
								$status = "Active User";
							} elseif($users[$user_id]['status']>1) {
								$status = "Non-System User";
							} else {
								$status = "Terminated User";
							}
							$d = $status;
							break;
						case "dept_id":
							$d = $dept[$users[$user_id][$fld]];
							break;
						case "job_title_id":
							$d = $job_titles[$users[$user_id][$fld]];
							break;
						default:
							$d = ASSIST_HELPER::code($row[$fld]);
							break;
					}
					$raw_data[$c][$fld] = ASSIST_HELPER::decode(str_replace("&quot;",'""',$d));
				}
				$c++;
			}
		}
//		echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//		ASSIST_HELPER::arrPrint($raw_data);
//		echo "<hr />";

		/** **************************** NOW GENERATE OUTPUT ****************************** **/
		$fdata = "";
		foreach($raw_data as $raw) {
			$fdata.= "\"".implode("\",\"",$raw)."\"\r\n";
		}

	} else {
		$fdata = "\"No data found to export.  Please try different settings.\"";
	}
} else {
	$fdata = "\"No data found to export.  Please try different settings.\"";
}

//WRITE DATA TO FILE
$cmpcode = $_SESSION['cc'];
$modref = "PMSPS";
$me->checkFolder($modref."/export",1,false,$client_cmpcode);
$newfilename = $client_cmpcode."_".$modref."_".$_REQUEST['src_table']."_".date("Ymd_His").".csv";
$filename = "../files/".$cmpcode."/".$modref."/export/".$newfilename;
$file = fopen($filename,"w");
fwrite($file,$fdata."\n");
fclose($file);
//SEND FILE TO HEADER FOR DOWNLOAD DIALOG BOX
header('Content-type: text/plain');
header('Content-Disposition: attachment; filename="'.$newfilename.'"');
readfile($filename);
