<?php
    include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<style type=text/css>
table td {
    border-color: #ffffff;
    border-width: 0px;
}
.tdheader {
    border-left: 1px solid #ffffff;
    border-bottom: 1px solid #ffffff;
}
.tdheaderl { border-bottom: 1px solid #ffffff; }
.tdgeneral {
    border-bottom: 1px solid #ababab;
    border-left: 0px solid #ffffff;
    }
</style>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>SDBIP <?php echo($modtxt); ?>: Setup - Time Periods ~ Close</b></h1>
<p>&nbsp;</p>
<?php
$tid = $_GET['t'];
$act = $_GET['act'];

if(is_numeric($tid) && $tid > 0)
{

    $trow = array();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_time WHERE id = ".$tid;
    include("inc_db_con.php");
        $trow = mysql_fetch_array($rs);
    mysql_close();

    if(count($trow)>0)
    {
        if($act == "Y")
        {
            $sql = "UPDATE assist_".$cmpcode."_".$modref."_list_time SET ";
            $sql.= " active = 'N'";
            $sql.= " WHERE id = ".$tid;
            include("inc_db_con.php");
            //insert into sdbip log
            $tsql = $sql;
                $logref = $tid;
                $logtype = "STP";
                $logaction = "Closed time period '".date("d F Y",$trow['sval'])." - ".date("d F Y",$trow['eval'])."'";
                $logdisplay = "Y";
                include("inc_log.php");
        $told = "";
        $trans = $logaction." with id ".$tid;
        include("inc_transaction_log.php");
            ?>
            <p>Time period '<?php echo(date("d F Y",$trow['sval'])); ?> - <?php echo(date("d F Y",$trow['eval'])); ?>' has now been closed.<br>&nbsp;
            <br>Should you need to reopen the time period, please contact Ignite Advisory Services by using Ignite Help.</p>
            <?php
        }
        else
        {
            if($act=="C")
            {
                ?>
                <div align=center>
                <h2>Please Confirm</h2>
                <table cellpadding=15 cellspacing=0 style="border-color: #cc0001;">
                    <tr>
                        <td class=tdgeneral align=center>Are you sure you want to close the time period:<br><?php echo(date("d F Y",$trow['sval'])); ?> - <?php echo(date("d F Y",$trow['eval'])); ?>?
                        <br>&nbsp;<br><input type=button value=" Yes " onclick="document.location.href = 'setup_time_configure_close.php?act=Y&t=<?php echo($tid); ?>';"> <input type=button value=" No " onclick="document.location.href = 'setup_time_configure_close.php?act=N&t=<?php echo($tid); ?>';"></td>
                    </tr>
                </table>
                </div>
                <?php
            }
            else
            {
                ?>
                <p>Closure aborted.</p>
                <?php
            }
        }
    }
    else
    {
        ?>
        <p>An error has occurred.  Please go back and try again.</p>
        <?php
    }
}
else
{
    ?>
    <p>An error has occurred.  Please go back and try again.</p>
    <?php
}
?>

<?php
$urlback = "setup_time_list.php";
include("inc_goback.php");
?>
<p>&nbsp;</p>
</body>
</html>
