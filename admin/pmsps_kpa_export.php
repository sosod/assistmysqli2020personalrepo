<?php
require_once("../module/autoloader.php");
$client_cmpcode = strtolower($_REQUEST['cc']);
$me = new ASSIST_MODULE_HELPER("client",$client_cmpcode,false,"PMSPS");
$me->setDBRef("PMSPS",$client_cmpcode);
$me->echoPageHeader();

//Get financial years from PMSPS module
$sql = "SELECT id, name FROM ".$me->getDBRef()."_eval_year ORDER BY name DESC";
$financial_years = $me->mysql_fetch_value_by_id($sql,"id","name");

//get users
$sql = "SELECT tkid as id, CONCAT(tkname,' ',tksurname) as name, tkstatus as status FROM assist_".$client_cmpcode."_timekeep ORDER BY tkname,tksurname";
$users = $me->mysql_fetch_all_by_id($sql,"id");

//get departments
$sql = "SELECT id, value as name FROM assist_".$client_cmpcode."_list_dept ORDER BY value";
$departments = $me->mysql_fetch_value_by_id($sql,"id","name");

echo "
<h1>Performance Assist KPA export >> ".strtoupper($client_cmpcode)."</h1>
<form name='frm_export' action='pmsps_kpa_export_output.php' method='post'>
<input type='hidden' name='cc' value='$client_cmpcode' />
<p>&nbsp;</p>
<h2>Financial Year Filter (required)</h2>
<p class='b'>Financial Year: <select name='eval_year_id'>";
$selected = false;
foreach($financial_years as $id => $name) {
	echo "<option ".($selected===false?"selected=selected":"")." value='$id'>".$name."</option>";
	$selected = true;
}
echo "</select></p>
<p>&nbsp;</p>
<h2>Sub-Filters:</h2>
<p class='b'>User/Employee: <select name='user_id' id='user_id'><option selected value='X'>--- SELECT ---</option>";
foreach($users as $id => $user) {
	$name = $user['name'];
	if($user['status']==0) {
		$name.=" [terminated]";
	}elseif($user['status']>1) {
		$name.=" [non-system user]";
	}
	echo "<option value=".$id.">".$name."</option>";
}
echo "</select></p>
<p class='b'>Department: <select name='dept_id' id='dept_id'><option selected value='X'>--- SELECT ---</option>";
foreach($departments as $id => $name) {
	echo "<option value=".$id.">".$name."</option>";
}
echo "</select></p>
<p><span class='b'>User Type:</span> <input type='radio' name='rad_user_type' checked value='CURRENT' /> Active + Non-System Users &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='rad_user_type' value='ACTIVE' /> Active Users only &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='rad_user_type' value='INACTIVE' /> Non-System Users only &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='rad_user_type' value='ALL' /> All Users, including terminated users &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p class='i'>The User Type filter is only applicable if you don't select a specific user in the User/Employee filter.&nbsp;</p>";
ASSIST_HELPER::displayResult(array("info","If you find the process timing out before it can generate the output, try limiting the users to only active or non-system users or for a specific Department to reduce the amount of data being output."));
echo "<p>&nbsp;</p>
<h2>Output: Pick the Table for Output</h2>
<p class='b'>Table: <select name='src_table' id='src_table'><option selected value='kpa'>KPA</option><option value='job_function'>Job Function</option><option value='kpa_org'>KPA Org</option></select></p>
<p><button name='btn_submit' id='btn_go'> - Go - </button></p>
</form>
";






?>
<script type="text/javascript">
$(function() {
	$("#btn_go").button().click(function(e) {
		e.preventDefault();
		//if($("#user_id").val()=="X" && $("#dept_id").val()=="X") {
		//	alert("Please select a sub-filter (User or Department)");
		//} else {
			// AssistHelper.processing();
			$("form[name=frm_export]").submit();
		//}
	});
});
</script>
