<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>Unclaimed Deposits - Claim a deposit</b></h1>
<p>Please wait...</p>
<?php
//GET DATA
$udid = $_POST['udid'];
$claimtkid = $tkid;
$claimclient = $_POST['claimclient'];
$claimclientname = $_POST['claimclientname'];
$claimmatter = $_POST['claimmatter'];
$claimmattername = $_POST['claimmattername'];
$claimcomment = $_POST['claimcomment'];
$claimtypeid = $_POST['claimtypeid'];
//FORMAT VARIABLES
$day = date("d",$today);
$mon = date("n",$today);
$year = date("Y",$today);
$claimlistdate = mktime(0,0,0,$mon,$day,$year);
$claimclient = str_replace("'","&#39",$claimclient);
$claimmatter = str_replace("'","&#39",$claimmatter);
$claimclientname = str_replace("'","&#39",$claimclientname);
$claimmattername = str_replace("'","&#39",$claimmattername);
$claimcomment = str_replace("'","&#39",$claimcomment);

//ADD CLAIM RECORD
$sql = "INSERT INTO assist_".$cmpcode."_".$modref."_claim SET ";
$sql .= "claimudid = ".$udid.", ";
$sql .= "claimtkid = '".$claimtkid."', ";
$sql .= "claimclient = '".$claimclient."', ";
$sql .= "claimclientname = '".$claimclientname."', ";
$sql .= "claimmatter = '".$claimmatter."', ";
$sql .= "claimmattername = '".$claimmattername."', ";
$sql .= "claimcomment = '".$claimcomment."', ";
$sql .= "claimtypeid = ".$claimtypeid.", ";
$sql .= "claimdate = '".$today."', ";
$sql .= "claimuserid = '".$tkid."', ";
$sql .= "claimjournal = '', ";
$sql .= "claimlistdate = '".$claimlistdate."'";
include("inc_db_con.php");
    //Update transaction log
    $tsql = str_replace("'","|",$sql);
    $trans = "Deposit ".$udid." claimed: ".$sql2;
    $tref = "UD";
    include("inc_transaction_log.php");

//UPDATE DEPOSIT TABLE
$sql = "UPDATE assist_".$cmpcode."_".$modref."_deposit SET udclaimyn = 'Y' WHERE udid = ".$udid;
include("inc_db_con.php");

//GET INFORMATION FOR JOURNAL DOC
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_deposit, assist_".$cmpcode."_".$modref."_list_bank WHERE udbankid = bankid AND udid = ".$udid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $uddepdate = date("d F Y",$row['uddepdate']);
        $udbank = $row['banktext']." - ".$row['banktype'];
        $udreceipt = $row['udreceipt'];
        $udbankstat = $row['udbankstat'];
        $udnarration = $row['udnarration'];
        $udamount = $row['udamount'];
        $udbanknum = $row['banknum'];
    mysql_close();
    $sql = "SELECT tkname, tksurname FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$claimtkid."'";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $claimtkname = $row['tkname']." ".$row['tksurname'];
    mysql_close();
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_type WHERE typeid = ".$claimtypeid;
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
        $claimtype = $row['type'];
    mysql_close();
    
//CHECK EMAIL SETTING
$sql = "SELECT * FROM assist_".$cmpcode."_setup WHERE ref = '$modref' AND refid = 1";
include("inc_db_con.php");
$r = mysql_num_rows($rs);
if($r == 0)
{
    $udemail = "N";
}
else
{
    $row = mysql_fetch_array($rs);
    $udemail = $row['value'];
}

if($udemail != "N") //IF EMAIL SETTING IS NOT NO
{
    if($udemail == "A") //IF EMAIL SETTING IS ADMIN GET EMAIL ADDRESSES FOR ALL ADMINS
    {
        $sql = "SELECT t.tkemail FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_".$modref."_list_users u WHERE t.tkid = u.tkid AND u.yn = 'Y'";
        include("inc_db_con.php");
        $r = mysql_num_rows($rs);
        if($r > 1)
        {
            $row = mysql_fetch_array($rs);
            $to = $row['tkemail'];
            while($row = mysql_fetch_array($rs))
            {
                $to .= ", ".$row['tkemail'];
            }
        }
        else
        {
            $row = mysql_fetch_array($rs);
            $to = $row['tkemail'];
        }
        mysql_close();
    }
    else    //ELSE EMAIL SETTING IS NOT NO OR ADMIN THEN IT IS A SPECIFIED EMAIL ADDRESS
    {
        $to = $udemail;
    }
    //SET EMAIL VARIABLES
    $comment = str_replace(chr(10),"<br>",$claimcomment);
    $subject = "Deposit claimed";
    $message = "<font face=arial>";
    $message = $message."<p>An unclaimed deposit has been claimed.</p>";
    $message = $message."<p><b>The deposit details are:</b><br><small>";
    $message = $message."<b>Date Deposited:</b> ".$uddepdate."<br>";
    $message = $message."<b>Bank:</b> ".$udbank." (".$udbanknum.")<br>";
    $message = $message."<b>Receipt:</b> ".$udreceipt."<br>";
    $message = $message."<b>Bank statement:</b> ".$udbankstat."<br>";
    $message = $message."<b>Narration:</b> ".$udnarration."<br>";
    $message = $message."<b>Amount:</b> R ".number_format($udamount,2)."</p>";
    $message = $message."</small><p><b>The claim details are:</b><br><small>";
    $message = $message."<b>Claimed by:</b> ".$claimtkname."<br>";
    $message = $message."<b>Client number:</b> ".$claimclient."<br>";
    $message = $message."<b>Client name:</b> ".$claimclientname."<br>";
    $message = $message."<b>Matter number:</b> ".$claimmatter."<br>";
    $message = $message."<b>Matter name:</b> ".$claimmattername."<br>";
    $message = $message."<b>Type:</b> ".$claimtype."<br>";
    $message = $message."<b>Comment:</b> ".$comment."</p>";
//    $message = $message."<p>Attached is a copy of the unclaimed deposits journal form that was emailed to the user.</p>";
    $message = $message."<p>The user will be sent the unclaimed deposits journal document for this claim.</p>";
    $message = $message."<p>To view a report of all deposits claimed today please log on to Ignite Assist at <b><a href=http://assist.ignite4u.co.za style='color: #cc0001'>http://assist.ignite4u.co.za</a></b>.</p>";
    $message = $message."</small></font>";
    include("../assist_from.php");
    $header = "From: ".$from."\r\nReply-to: ".$from."\r\nContent-type: text/html; charset=us-ascii";
    //SEND ADMIN NOTIFICATION EMAIL
    mail($to,$subject,$message,$header);
        //Add transaction log
        $trans = "Deposit ".$udid." claim email sent to: ".$to;
        $tref = "UD";
        $tsql = $message;
        include("inc_transaction_log.php");
}
//START OUTPUTTING JOURNAL DOC TO BUFFER
ob_start();
?>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252"><title>Unclaimed Deposits Journal</title></head><style type=text/css>
<!--
.fc {color: #006600}
p {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
	color: #000000;
	margin: 10px 0px 10px;
	padding: 0px 0px 0px 5px;
}
h1 {
	font-size: 17pt;
	margin: 0px 0px 10px;
	padding: 0px 0px 2px 2px;
	font-family: "Comic Sans MS", Arial, Helvetica, sans-serif;
	color: #cc0001;
	letter-spacing: 0.07em;
	line-height: 22px;
	font-weight: bold;
	text-decoration: underline;
}
h2 {
	font-size: 14pt;
	margin: 20px 0px 10px;
	padding: 0px;
	font-family: "Comic Sans MS", Arial, Helvetica, sans-serif;
	color: #dd1a20;
	letter-spacing: 0.07em;
    font-weight: bold;
	line-height: 22px;
}
.TDgeneral {
	color: #000000;
	background-color: #FFFFFF;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	line-height: 12pt;
}

-->
</style>

<body topmargin=0>
<h1 class=fc style="margin-top: 5px">Unclaimed Deposits Journal</h1>
<table border="1" id="table1" cellspacing="0" cellpadding="3">
	<tr>
		<td colspan="3" class="tdgeneral">
		<h2 class=fc style="margin-bottom: 0px; margin-top: 5px">Unclaimed Deposit Detail</h2>
		</td>
	</tr>
	<tr>
		<td class="tdgeneral"  width="150"><b>Date Deposited:</b></td>
		<td colspan="2" class="tdgeneral" width=400><?php echo($uddepdate); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Date Claimed:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo(date("d F Y",$today)); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Bank:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo($udbank); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Receipt Number:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo($udreceipt); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Bank Statement Num.:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo($udbankstat); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Narration:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo($udnarration); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Amount:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo("R ".number_format($udamount,2)); ?>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral">
		<h2 class=fc style="margin-bottom: 0px; margin-top: 5px">Instruction to the Finance Department</h2>
		<p style="margin-top: 0px; margin-bottom: 0px"><i>Please allocate the funds identified to my file with the following
		details:</i></td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Client Number:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo($claimclient); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Client Name:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo($claimclientname); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>File / Matter Number:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo($claimmatter); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>File / Matter Name:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo($claimmattername); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Narration / Comment:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo($claimcomment); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Type:</b></td>
		<td colspan="2" class="tdgeneral"><?php echo($claimtype); ?>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral">
		<h2 class=fc style="margin-bottom: 0px; margin-top: 5px">Document Authorisation by the Professional</h2>
		</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Requested by:</td>
		<td colspan="2" class="tdgeneral"><?php echo($claimtkname); ?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="bottom" class="tdgeneral" width="150"><b>Authorised signature:</td>
		<td colspan="2" class="tdgeneral"><font size="1">&nbsp;<br>
		________________________________________</font></td>
	</tr>
	<tr>
		<td valign="bottom" class="tdgeneral" width="150"><b>Authorised by:</td>
		<td colspan="2" class="tdgeneral"><font size="1">&nbsp;<br>
		________________________________________</font></td>
	</tr>
	<tr>
		<td valign="bottom" class="tdgeneral" width="150"><b>Date:</td>
		<td colspan="2" class="tdgeneral"><font size="1">&nbsp;<br>
		________________________________________</font></td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral">
		<h2 class=fc style="margin-bottom: 0px; margin-top: 5px">Accounting by Finance Department</h2>
		</td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150">&nbsp;</td>
		<td align="center" class="tdgeneral" width="200"><b>Debit</b></td>
		<td align="center" class=tdgeneral width="200"><b>Credit</b></td>
	</tr>
	<tr>
		<td class="tdgeneral" width="150"><b>Account number:</td>
		<td class="tdgeneral"><?php echo($udbanknum); ?>&nbsp;</td>
		<td class="tdgeneral"><?php echo($claimmatter); ?>&nbsp;</td>
	</tr>
	<tr>
		<td valign="bottom" class="tdgeneral" width="150"><b>Processed by:</td>
		<td colspan="2" class="tdgeneral"><font size="1">&nbsp;<br>
		________________________________________</font></td>
	</tr>
	<tr>
		<td valign="bottom" class="tdgeneral" width="150"><b>Date:</td>
		<td colspan="2" class="tdgeneral"><font size="1">&nbsp;<br>
		________________________________________</font></td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral">
		<h2 class=fc style="margin-bottom: 0px; margin-top: 5px">Notes:</h2>
		</td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral">&nbsp;<p>&nbsp;</p>
		<p>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" class="tdgeneral"><i>Please ensure that the documentary proof of the
		claim of the unclaimed deposit is attached.</td>
	</tr>
</table>

</body>

</html>
<?php
//GET BUFFER FOR JOURNAL DOC
$journal = ob_get_clean();
//WRITE JOURNAL DOC TO FILE
    $fname = "ud_journal_".date("Ymd_Hi",$today).".html";
    $filename = "../files/".$cmpcode."/".$fname;
    $file = fopen($filename,"w");
    fwrite($file,$journal);
    fclose($file);
        //Add transaction log
        $trans = "Unclaimed Deposits Journal document created with file name: ".$filename;
        $tref = "UD";
        $tsql = "";
        include("inc_transaction_log.php");
//UPDATE CLAIM WITH JOURNAL NAME
$sql = "UPDATE assist_".$cmpcode."_".$modref."_claim SET claimjournal = '".$fname."' WHERE claimudid = ".$udid." AND claimdate = '".$today."' AND claimuserid = '".$tkid."'";
include("inc_db_con.php");

//CREATE FORM FOR REDIRECT TO MAIL ATTACHMENT FILE
echo("<form name=fil><input type=hidden name=f value=".$fname."><input type=hidden name=cc value=".$cmpcode."></form>");

?>
<script language=JavaScript>
var f = document.fil.f.value;
var cc = document.fil.cc.value;
if(f.length > 0)
{
    document.location.href = "../files/"+cc+"/ud_journal_mail.php?f="+f;
}
</script>
<p>&nbsp;</p>
</body>

</html>
