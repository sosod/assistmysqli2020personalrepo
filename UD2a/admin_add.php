<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function delDep(id) {
    if(confirm("Are you sure you want to delete deposit "+id+"?"))
    {
        document.location.href = "admin_del.php?i="+id;
    }
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<a name=top></a>
<h1 class=fc><b>Unclaimed Deposits - Admin: Add deposits</b></h1>
<?php
$err = "N";
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_bank WHERE bankyn = 'Y' ORDER BY banktext, banktype";
            include("inc_db_con.php");
            $b = mysql_num_rows($rs);
            if($b == 0)
            {
                $err = "Y";
            }
            else
            {
                $err = "N";
            }
            mysql_close();

if($err == "Y")
{
    echo("<p>No banks have been set up.  Please set up a bank before adding any unclaimed deposits.</p>");
}
else
{
?>
<form name=add method=post action=admin_add_process.php>
<table cellpadding=5 cellspacing=0 border=1>
    <tr>
        <td class=tdheader>Date Deposited<br><i><small>(DD MMM YYYY)</small></i></td>
        <td class=tdheader>Bank</td>
        <td class=tdheader>Receipt</td>
        <td class=tdheader>Bank<br>statement</td>
        <td class=tdheader>Narration</td>
        <td class=tdheader>Amount<br><i><small>(Numbers only)</small></i></td>
    </tr>
<?php
for($u=1;$u<11;$u++)
{
?>
    <tr>
        <td class=tdgeneral><input type=text size=2 name=udday[]><select name=udmon[]>
            <?php
                $nmon = date("n");
                $months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
                $m = 0;
                $m2 = 1;
                while($m<=11)
                {
                    if($m2 == $nmon)
                    {
                        echo("<option selected value=".$m2.">".$months[$m]."</option>");
                    }
                    else
                    {
                        echo("<option value=".$m2.">".$months[$m]."</option>");
                    }
                    $m++;
                    $m2++;
                }
            ?>
        </select><input type=text size=4 name=udyear[] value=<?php echo(date("Y"));?>></td>
        <td class=tdgeneral><select name=udbankid[]><option selected value=X>--- SELECT ---</option>
        <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_bank WHERE bankyn = 'Y' ORDER BY banktext, banktype";
            include("inc_db_con.php");
            while($row = mysql_fetch_array($rs))
            {
                echo("<option value=".$row['bankid'].">".$row['banktext']." - ".$row['banktype']."</option>");
            }
            mysql_close();
        ?>
            </select></td>
        <td class=tdgeneral>T <input type=text name=udreceipt[] size=5></td>
        <td class=tdgeneral>B/S <input type=text name=udbankstat[] size=5></td>
        <td class=tdgeneral><input type=text maxlength=50 size=30 name=udnarration[]></td>
        <td class=tdgeneral>R <input type=text size=10 name=udrand[]>.<input type=text size=2 name=udcents[] value=00></td>
    </tr>
<?php
}
?>
    <tr>
        <td class=tdgeneral colspan=6><input type=submit value=Add> <input type=reset value=Reset><Br>
        <i>Note: In order for a deposit to be added, <u>all</u> the fields in the relevant row must be completed (see below).</td>
    </tr>
    <tr>
        <td class=tdheader colspan=6>Example</td>
    </tr>
        <td class=tdgeneral><input type=text size=2 name=u1 value=1><select name=u2>
<option selected value=4>Apr</option>
        </select><input type=text size=4 name=u3 value=2008></td>
        <td class=tdgeneral>T <input type=text name=r size=5 value=18529></td>
        <td class=tdgeneral>B/S <input type=text name=bs size=5 value=352></td>
        <td class=tdgeneral><select name=u4>
        <?php
            $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_bank WHERE bankyn = 'Y' ORDER BY banktext, banktype";
            include("inc_db_con.php");
            $row = mysql_fetch_array($rs);
                echo("<option selected value=".$row['bankid'].">".$row['banktext']." - ".$row['banktype']."</option>");
            mysql_close();
        ?>
            </select></td>
        <td class=tdgeneral><input type=text maxlength=50 size=30 name=u5 value="Narration as per the bank statement"></td>
        <td class=tdgeneral>R <input type=text size=6 name=u6 value=12345 style="text-align: right">.<input type=text size=2 name=u7 value=10></td>
    </tr>
</table>
</form>
<?php
}
?>
<hr><a name=del></a>
<h1 class=fc><b>Unclaimed Deposits - Admin: Delete deposits</b></h1>
<p>&nbsp;</p>
<table cellpadding=5 cellspacing=0 border=1>
    <tr>
        <td class=tdheader>Date Deposited</td>
        <td class=tdheader>Bank</td>
        <td class=tdheader>Receipt</td>
        <td class=tdheader>Bank<br>Statement</td>
        <td class=tdheader>Narration</td>
        <td class=tdheader>Amount</td>
        <td class=tdheader>&nbsp;</td>
    </tr>
    <?php
    $u = 0;
    $totamt = 0;
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_deposit, assist_".$cmpcode."_".$modref."_list_bank WHERE udbankid = bankid AND udclaimyn = 'N' ORDER BY udsort ASC";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        $u++;
        $totamt = $totamt+$row['udamount'];
        ?>
        <tr>
            <td class=tdgeneral><?php echo(date("d M Y",$row['uddepdate'])); ?>&nbsp;</td>
            <td class=tdgeneral><?php echo($row['banktext']." - ".$row['banktype']); ?>&nbsp;&nbsp;</td>
            <td class=tdgeneral><?php echo($row['udreceipt']); ?>&nbsp;&nbsp;&nbsp;</td>
            <td class=tdgeneral><?php echo($row['udbankstat']); ?>&nbsp;&nbsp;&nbsp;</td>
            <td class=tdgeneral><?php echo($row['udnarration']); ?>&nbsp;&nbsp;&nbsp;</td>
            <td class=tdgeneral align=right>&nbsp;&nbsp;R <?php echo(number_format($row['udamount'],2)); ?>&nbsp;</td>
            <td class=tdheader>&nbsp;<input type=button value="Del " onclick="delDep(<?php echo($row['udid']); ?>)">&nbsp;</td>
        </tr>
        <?php
    }
    mysql_close();
    ?>
    <tr>
        <td class=tdheader style="text-align:right" colspan=5>Total amount unclaimed in <?php echo($u); ?> deposits:</td>
        <td class=tdheader align=right>&nbsp;R&nbsp;<?php echo(number_format($totamt,2)); ?>&nbsp;</td>
        <td class=tdheader>&nbsp;</td>
    </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

</body>

</html>
