<?php
include("inc_ignite.php");
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var client = me.claimclient.value;
    var matter = me.claimmatter.value;
    var clientn = me.claimclientname.value;
    var mattern = me.claimmattername.value;
    var typeid = me.claimtypeid.value
    if((client.length == 0 && matter.length == 0 && clientn.length == 0 && mattern.length == 0) || (typeid == "X"))
    {
        alert("Please fill in all the required fields.");
    }
    else
    {
        return true;
    }
    return false;
}
</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>

<h1 class=fc><b>Unclaimed Deposits - Claim a deposit</b></h1>
<?php
$udid = $_GET['i'];
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_deposit, assist_".$cmpcode."_".$modref."_list_bank WHERE udbankid = bankid AND udclaimyn = 'N' AND udid = ".$udid;
include("inc_db_con.php");
$u = mysql_num_rows($rs);
if($u != 1)
{
    echo("<p>An error has occured. Please go back and try again.</p>");
    $err = "Y";
    echo("<p>&nbsp;</p><p><img src=/pics/tri_left.gif align=absmiddle > <a href=main.php class=grey>Go back</a></p>");

}
else
{
    $udrow = mysql_fetch_array($rs);
    $err = "N";
}

if($err == "N")
{
?>
<form name=claim method=post action=main_claim_process.php language=jscript onsubmit="return Validate(this);">
<input type=hidden name=udid value=<?php echo($udid); ?>>
<table border="0" id="table2" cellspacing="0" cellpadding="15" bordercolor=006600>
	<tr>
		<td class=tdgeneral>

		<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td colspan="2" valign=top class=tdheader>Claim Details</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Claimed by:</b></td>
		<td class=tdgeneral>
<?php
/*
echo("<select name=claimtkid>");
$sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkstatus = 1 AND tkid <> '0000' ORDER BY tkname, tksurname";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    echo("<option");
    if($row['tkid']==$tkid)
    {
        echo(" selected");
    }
    echo(" value=".$row['tkid'].">".$row['tkname']." ".$row['tksurname']."</option>");
}
mysql_close();
echo("</select>");
*/
echo($tkname);
?>
        </td>
	</tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_journal_lines l, assist_".$cmpcode."_".$modref."_journal j WHERE j.jlineid = lineid AND j.jyn = 'Y'";
include("inc_db_con.php");
while($row = mysql_fetch_array($rs))
{
    $field[$row['linefield']] = "Y";
}
mysql_close();

if($field['claimclient'] == "Y")
{
?>
	<tr>
		<td class=tdgeneral><b>Client number:</b></td>
		<td class=tdgeneral><input type=text name=claimclient maxlength=50>&nbsp;</td>
	</tr>
<?php
}
if($field['claimclientname'] == "Y")
{
?>
	<tr>
		<td class=tdgeneral><b>Client name:</b></td>
		<td class=tdgeneral><input type=text name=claimclientname maxlength=100>&nbsp;</td>
	</tr>
<?php
}
if($field['claimmatter'] == "Y")
{
?>
	<tr>
		<td class=tdgeneral><b>Matter number:</b></td>
		<td class=tdgeneral><input type=text name=claimmatter maxlength=50>&nbsp;</td>
	</tr>
<?php
}
if($field['claimmattername'] == "Y")
{
?>
	<tr>
		<td class=tdgeneral><b>Matter name:</b></td>
		<td class=tdgeneral><input type=text name=claimmattername maxlength=100>&nbsp;</td>
	</tr>
<?php
}
if($field['claimcomment'] == "Y")
{
?>
	<tr>
		<td class=tdgeneral valign=top><b>Narration / Comment:</b></td>
		<td class=tdgeneral><textarea name=claimcomment rows=4 cols=30></textarea></td>
	</tr>
<?php
}
if($field['type'] == "Y")
{
?>
	<tr>
		<td class=tdgeneral><b>Type:</b></td>
		<td class=tdgeneral><select name=claimtypeid><option selected value=X>--- SELECT ---</option>
    <?php
    $sql = "SELECT * FROM assist_".$cmpcode."_".$modref."_list_type WHERE typeyn = 'Y' ORDER BY type";
    include("inc_db_con.php");
    while($row = mysql_fetch_array($rs))
    {
        echo("<option value=".$row['typeid'].">".$row['type']."</option>");
    }
    mysql_close();
    ?>
        </select></td>
	</tr>
<?php
}
?>
	<tr>
		<td colspan="2"><input type=submit value=Claim> <input type=button value=Cancel onclick="history.back();">&nbsp;</td>
	</tr>
</table>


		</td>
		<td valign=top>
		<table border="1" id="table1" cellspacing="0" cellpadding="4">
	<tr>
		<td colspan="2" class=tdheader>Deposit Details</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Date:</b></td>
		<td class=tdgeneral><?php echo(date("d F Y",$udrow['uddepdate'])); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Bank:</b></td>
		<td class=tdgeneral><?php echo($udrow['banktext']); ?> - <?php echo($udrow['banktype']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Receipt:</b></td>
		<td class=tdgeneral><?php echo($udrow['udreceipt']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Bank statement:&nbsp;</b></td>
		<td class=tdgeneral><?php echo($udrow['udbankstat']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Narration:</b></td>
		<td class=tdgeneral><?php echo($udrow['udnarration']); ?>&nbsp;</td>
	</tr>
	<tr>
		<td class=tdgeneral><b>Amount:</b></td>
		<td class=tdgeneral>R <?php echo(number_format($udrow['udamount'],2)); ?>&nbsp;</td>
	</tr>
	</table>

		</td>
	</tr>
</table></form>
<?php
}
?>

</body>

</html>
