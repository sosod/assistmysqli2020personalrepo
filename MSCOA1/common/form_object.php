<?php
$page_redirect_path_total = "manage_edit_object.php?object_id=".$idp_object_id."&tab=segments&action=SEGMENT&section=".$_REQUEST['object_type'];


if(strtoupper($page_action) == "EDIT") {
	$is_edit_page = true;
	$is_add_page = false;
	$is_view_page = false;
	$is_copy_page = false;
} else if(strtoupper($page_action)=="ADD") {
	$is_edit_page = false;
	$is_add_page = true;
	$is_view_page = false;
	$is_copy_page = false;
} else if(strtoupper($page_action)=="COPY") {
	$is_edit_page = false;
	$is_add_page = false;
	$is_view_page = false;
	$is_copy_page = true;
} else {
	$is_edit_page = false;
	$is_add_page = false;
	$is_view_page = true;
	$is_copy_page = false;
}


		$parent_object_type = "SEGMENT";
		$childObject = new MSCOA1_SEGMENTS($object_type);
		if($display_type=="default") {
			//$child_objects = $childObject->getObject(array('type'=>"LIST",'section'=>"NEW",'parent_id'=>$parent_object_id));
		}
		$parentObject = false;
		$child_object_type = $object_type;
//		$parent_id_name = $childObject->getParentFieldName();




$child_redirect = "manage_edit_object.php?object_type=".$object_type."&object_id=";
$child_name = $helper->getObjectName($childObject->getMyObjectName());



ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());

?>
<table class='tbl-container not-max'>
	<tr>
	<?php
	if($display_type=="default") {
		$td2_width = "48%";
	?>
		<td width=47%>
			<?php
			/** @var $displayObject MSCOA1_DISPLAY */
			$js.= $displayObject->drawDetailedView($parent_object_type, $parent_object_id, ($parent_object_type!="IDP"));
			//$js.= isset($go_back) ? $displayObject->drawPageFooter($helper->getGoBack($go_back)) : "";
			?>
		</td>
		<td width=5%>&nbsp;</td>
		<td width=<?php echo $td2_width; ?>><h2>New <?php echo $child_name; ?></h2>
	<?php
	} else {
		echo "<td>";
	}
			/** @var $displayObject MSCOA1_DISPLAY */
			$data = $displayObject->getObjectForm($child_object_type, $childObject, $child_object_id, $parent_object_type, $parentObject, $parent_object_id, $page_action, $page_redirect_path);
			echo $data['display'];
			$js.= $data['js'];
		?></td>
	</tr>
	<tr><td>
<?php
if($is_edit_page || $is_view_page) {
	$js.=$displayObject->drawPageFooter("",$childObject->getMyLogTable(),"log_object_type=".$object_type."&log_object_id=".$child_object_id,$child_object_id);
}
?>
	</td></tr>
</table>
<script type=text/javascript>
$(function() {
	<?php
	echo $js;
	?>
	$("input:button.btn_edit").click(function() {
		var i = $(this).attr("ref");
		document.location.href = '<?php echo $child_redirect; ?>'+i;
	});
	//console.log("<?php echo $display_type; ?>");

	<?php
	if($display_type=="dialog") {
		echo $displayObject->getIframeDialogJS($child_object_type,"dlg_child",$_REQUEST);
	}
	?>

});
</script>