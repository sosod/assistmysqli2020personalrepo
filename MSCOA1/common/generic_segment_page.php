<?php
/**********************
 * REQUIRES:
 *     $section = MANAGE;
 *     $action = EDIT || VIEW;
 */

include("inc_header.php");




?>
<table style="width:1100px;margin: 0 auto" id=tbl_container><tr><td style='text-align: center' class=container>
<?php
	$segmentObject = new MSCOA1_SEGMENTS();
	$segments = $segmentObject->getSegmentDetails();

	$segment_page = strtolower($section)."_".strtolower($action).(isset($sub_section) && strlen($sub_section)>0 ? "_".strtolower($sub_section) : "")."_object.php?".$_SERVER['QUERY_STRING']."&action=SEGMENT&section=";


if(isset($button_name)) {
	$button_name = $segmentObject->replaceActivityNames($button_name);
} else {
	$button_name = $segmentObject->replaceActivityNames("|open|");
}



	foreach($segments as $key => $s) {
		$js.= $displayObject->drawVisibleBlock($s['name'],$key, $s['help'],$button_name);
	}


	?>
	</td></tr></table>

	<script type="text/javascript">

	$(function() {
		<?php echo $js; ?>



		var scr_dimensions = AssistHelper.getWindowSize();
		var div_h = 0;
		var tbl_h = 0;
		$("div.div_segment").button().click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			var page = $(this).attr("page");
			var url = '<?php echo $segment_page; ?>'+page.toLowerCase();
			document.location.href = url;

		});
		$("div.div_segment").hover(
			function() {
				$me = $(this).find("#btn_"+$(this).attr("page")+"_open");
				$me.addClass("ui-state-hover");
			},
			function() {
				$me = $(this).find("#btn_"+$(this).attr("page")+"_open");
				$me.addClass("ui-state-default").removeClass("ui-state-hover");
			}
		).css({"margin":"10px","background":"url()","padding-bottom":"30px"});//,"border-color":"#0099FF"});

		$("button.btn_open").button({
			icons:{primary:"ui-icon-newwin"}
		}).css({
			"position":"absolute","bottom":"5px","right":"5px"
		});
		$("button.btn_open").click(function(e) {
			e.preventDefault();
			$(this).parent("div.div_segment").trigger("click");
		});
		function formatButtons() {
			$("button.xbutton").children(".ui-button-text").css({"font-size":"80%","padding":"4px","padding-left":"24px"});
			$("button.xbutton").css({"margin":"2px"});
		}
		formatButtons();
		<?php
		if(isset($_REQUEST['section'])) {
			echo "
			$('#btn_".$_REQUEST['section']."_open').trigger('click');
			";
		}
		?>

		var description_height = 0;
		$(".tbl-segment-description").find("td").css({"vertical-align":"middle","text-align":"center","padding":"5px"});
		$(".tbl-segment-description").each(function() {
			var h = parseInt(AssistString.substr($(this).css("height"),0,-2));
			if(h>description_height) {
				description_height = h;
			}
		});
		$(".tbl-segment-description").css("height",(description_height+5)+"px");
		$("#tbl_container, #tbl_container td").css("border","1px solid #ffffff");
		$(".div_segment").css("border","1px solid #9999ff");
	});
	</script>
