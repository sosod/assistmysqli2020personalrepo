<?php
include("inc_header.php");
$redirect = $page.".php?";
$qs = $_SERVER['QUERY_STRING'];
$q = explode("&",$qs);
foreach($q as $i => $s) {
	if(strlen($s)==0) {
		unset($q[$i]);
	} else {
		$e = explode("=",$s);
		if(count($e)!=2) {
			unset($q[$i]);
		} elseif($e[0]=="mscoa_version_to_display") {
			unset($q[$i]);
		}
	}
}
$query_string = implode("&",$q);
$redirect.=$query_string;



$display_limit = 100;
$levels_displayed = 0;
?>
<div style="position:absolute;top:10px;right:10px;"><button id="btn_export">Export</button></div>
<table  id=tbl_container><tr><td class=container>
<?php
if(!isset($is_add_page)) { $is_add_page = false; }
if(!isset($is_edit_page)) { $is_edit_page = false; }
if(!isset($is_view_page)) { $is_view_page = false; }

$idp_object_id = 0;
//ASSIST_HELPER::arrPrint($_REQUEST);

$dialog_url = "manage_object.php";
$query_string = array();
foreach($_REQUEST as $key => $r) {
	if($key!="r") {
		$query_string[] = $key."=".$r;
	}
}
$dialog_redirect = $original_url."?".implode("&",$query_string);

//needed for colspan counting
function drawTextTree($i,$s,$indent,$level) {
	global $segment_data;
	global $total_levels;

	if($total_levels<$level) {
		$total_levels = $level;
	}
		if($s['has_children']==true) {
			if(isset($segment_data[$i])) {
				$icon = "+";
			} else {
				$icon = "-";
			}
		} else {
			$icon = "*";
		}

		$echo= "<p>".$icon."".$s['name']."&nbsp;V&nbsp;E&nbsp;";
		if($s['has_children']==true) {
			$echo.= "AC";
			if(isset($segment_data[$i])) {
				$indent.="----";
				$level++;
				foreach($segment_data[$i] as $i2 => $s2) {
					drawTextTree($i2,$s2,$indent,$level);
				}
				$level--;
			}
		}
		$echo.= "</p>";

}



function drawTableTree($i,$s,$indent,$parent_active=true) {
	global $segment_data;
	global $total_levels;
	global $segmentObject;
	global $idp_object_id;
	global $is_edit_page;
	global $display_limit;
	global $levels_displayed;

	if($levels_displayed < $display_limit) {
	$is_active = $parent_active==true && $s['active']==true;

		if($s['has_children']==true) {
			if(isset($segment_data[$i])) {
				$icon = "ui-icon-triangle-1-se";
			} else {
				$icon = "ui-icon-triangle-1-e";
			}
		} else {
			$icon = "ui-icon-carat-1-sw";
		}
		$icon_color = $is_active ? "222222" : "888888";


		$has_edit_button = false;
		if($is_edit_page && $is_active) {
			$has_edit_button = true;
		}
		$has_restore_button = false;
		if($is_edit_page && !$is_active && $parent_active) {
			$has_restore_button = true;
		}

	$echo = "";
	//don't display on view page
	if($is_active || $is_edit_page) {
		$echo.= "<tr class='".($is_active==true?"":"not-active")."'>";
		for($x=0;$x<$indent;$x++) {
			$echo.="<td class=placeholder style='background: url(/pics/icons/vertical-line.png)'>&nbsp;</td>";
		}
		$echo.= "
		<td>".$segmentObject->getDisplayIcon($icon,"",$icon_color)."</td>
		<td colspan=".($total_levels-$indent).">".$s['name']." <span class='i float' >".(isset($s['nickname']) && strlen($s['nickname'])>0 ? "<span class='green' style='font-size:80%'>".$s['nickname']."</span>":"")." <span style='font-size:75%; color: #999999; padding-left:5px'>[".$s['ref']."]</span></span></td>";
		$echo.="<td><span class=float>";
		if($s['can_assign']==1 && $is_active==true) {
				$echo .= $segmentObject->getDisplayIcon("ui-icon-circle-check", " title='Can Post' ", "009900");
		}
		$echo.="</span></td>";
		$echo.="<td class=right>";
		if($s['has_children']==true && $is_edit_page && $is_active==true) {
			$echo.= "<button class='action-button btn_add' parent_id=".$s['id']."_".$idp_object_id." object_id='0'>".$segmentObject->replaceActivityNames("|add|")." Child</button>";
		}
		$echo.="</td>";
		$echo.="<td>&nbsp;<button class='action-button btn_view' parent_id=".$s['parent']."_".$idp_object_id." object_id=".$s['id'].">".$segmentObject->replaceActivityNames("|view|")."</button>
		".($has_edit_button?"&nbsp;<button class=' action-button btn_edit' parent_id=".$s['parent']."_".$idp_object_id." object_id=".$s['id'].">".$segmentObject->replaceActivityNames("|edit|")."</button>&nbsp;":"")."
		".($has_edit_button ? "&nbsp;<button class=' action-button btn_copy' parent_id=".$s['parent']."_".$idp_object_id." object_id=".$s['id'].">Copy</button>&nbsp;" : "")."
		".($has_restore_button?"&nbsp;<button class=' action-button btn_restore' parent_id=".$s['parent']."_".$idp_object_id." object_id=".$s['id'].">".$segmentObject->replaceActivityNames("|restore|")."</button>&nbsp;":"");
		$echo.= "</td></tr>";
		if($s['has_children']==true) {
			if(isset($segment_data[$i])) {
				$indent++;
					if($levels_displayed < $display_limit) {
				foreach($segment_data[$i] as $i2 => $s2) {
							if($levels_displayed < $display_limit) {
					$echo.=drawTableTree($i2,$s2,$indent,$is_active);
				}
						}
					}
				$indent--;
			}
		}
	}
	}
	//$levels_displayed++; //uncomment this line during development to only display a few lines & to speed up the page load //echo "<P>END OF FUNCTION: ".$levels_displayed;
	return $echo;

}

	$section = $_REQUEST['section']; //echo $section;
	$segmentObject = new MSCOA1_SEGMENTS($section);
	//echo "getList start";
	/** @var $current_version_displayed INT from inc_header */
	$segment_data = $segmentObject->getList(0,TRUE,$idp_object_id,true,true,$current_version_displayed);
	//echo "getList end";
	$segment_details = $segmentObject->getSegmentDetails(true);
	//ASSIST_HELPER::arrPrint($segment_data);

	//$total_levels = 0;

	/***
	 * Basic text version
	 */
	//needed for colspan counting
		$indent = "";
		$levels = 1;
if(isset($segment_data[0]) && count($segment_data[0])>0) {
	foreach($segment_data[0] as $i => $s) {
		if($levels_displayed<$display_limit) {
		drawTextTree($i,$s,$indent,$levels);
	}
	}
} else {

}
if($is_edit_page) {
//	ASSIST_HELPER::displayResult(array("info","Version Management: If line items are the same between versions, use the Edit function to add the new version to the line item.  If there are differences between versions then use the Copy function to clone the existing line item and save it to a new version without impacting the existing line item."));
	//echo "<span class=float style='position:relative;top:10px;right:10px'><button class='action-button btn_plus top-level' parent_id='0_".$idp_object_id."' object_id=0>Add ".$segment_details['name']."</button></span>";
}
	echo "
	<span class='float i'>Version: <select id='sel_version'>";
	foreach($version_list as $i => $v) {
		echo "<option ".($i==$current_version_displayed?"selected":"")." value=".$i.">".$v."</option>";
	}
	echo "
	</select><button id='btn_version_go'>Go</button></span>
	<h2>".$segment_details['name']."</h2>
	<p class=i>".str_replace(chr(10),"<br />",$segment_details['help'])."</p>";

	/**
	 * Table version
	 */
	echo "
	<table id=tbl_segment_tree >
	";
	if($is_edit_page || $is_add_page){
		echo "
		<tr>
			<td id=td_add colspan=1><span class=float ><button class='action-button btn_plus top-level' parent_id='0_".$idp_object_id."' object_id=0>".$segmentObject->replaceActivityNames("|add|")." Top Level</button>&nbsp;<button class='btn_import top-level' parent_id='0_".$idp_object_id."' object_id=0>".$segmentObject->replaceActivityNames("|import|")."</button></span></td>
		</tr>
		";
	}
	$echo = "";
if(isset($segment_data[0]) && count($segment_data[0])>0) {
	foreach($segment_data[0] as $i => $s) {
		$indent = 0;
		$echo.= drawTableTree($i,$s,$indent);
	}
} else {
	if($is_edit_page) {
		$echo.= "<tr><td>Please click the Add button above to start.</td></tr>";
	} else {
		$echo.= "<tr><td>No line items available to display.</td></tr>";
	}
}
	echo $echo."</table>";


	?>
</td></tr></table>
<div id=dlg_child class=dlg-child title="">
		<iframe id=ifr_form_display style="border:0px solid #000000;" src="">

		</iframe>
</div>
	<script type="text/javascript" >
		var current_action = "";
		$(function() {
			//format tree
			$("span").css("border","0px dashed #009900");
			$("#tbl_container, #tbl_container td").css({"border":"0px dashed #009900","margin":"0 auto"});
			$("#tbl_segment_tree, #tbl_segment_tree td").css({"border":"0px dashed #009900","margin":"0 auto"});

			$("tr.not-active td").css("color","#999999");

			var cells = $("#tbl_segment_tree tr:first").children("td").length;
			$("#tbl_segment_tree tr:gt(0)").each(function() {
				var c = $(this).children("td").length;
				if(c>cells) {
					cells = c;
				}
			});
			if(cells>1) {
				$("#tbl_segment_tree #td_add").prop("colspan",cells);
			}

			$("#tbl_segment_tree tr:gt(0)").hover(function() {
				$(this).css("background-color","#dedede");
				$(this).children("td.placeholder").css("background","url(/pics/icons/vertical-line-hover.png)");
			},function() {
				$(this).css("background-color","");
				$(this).children("td.placeholder").css("background","url(/pics/icons/vertical-line.png)");
			});

			$("button.btn_import").button({
				icons:{primary:"ui-icon-arrowreturnthick-1-n"}
			}).removeClass("ui-state-default").addClass("ui-button-bold-grey")
			.hover(
				function() { $(this).removeClass("ui-button-bold-grey").addClass("ui-button-bold-orange"); },
				function() { $(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-grey"); }
			).click(function(e) {
				e.preventDefault();
				document.location.href = "manage_edit_segments_import.php?section=<?php echo $section; ?>";
			});
			$("button.btn_plus").button({
				icons:{primary:"ui-icon-plus"}
			}).removeClass("ui-state-default").addClass("ui-button-bold-green")
			.hover(
				function() {
					$(this).removeClass("ui-button-bold-green").addClass("ui-button-bold-orange");
				},function() {
					$(this).removeClass("ui-button-bold-orange").addClass("ui-button-bold-green");
				}
			);
			$("button.btn_add").button({
				icons:{primary:"ui-icon-plus"}
			}).removeClass("ui-state-default").addClass("ui-button-minor-green")
			.hover(
				function() {
					$(this).removeClass("ui-button-minor-green").addClass("ui-button-minor-orange");
				},function() {
					$(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-green");
				}
			).click(function(e) {
				e.preventDefault();
			})
			.children(".ui-button-text")
			.css({"padding-top":"2px","padding-bottom":"0px"});

			$("button.btn_view").button({
				icons:{primary:"ui-icon-extlink"}
			}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.hover(
				function() { $(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange"); },
				function() { $(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey"); }
			).click(function(e) {
				e.preventDefault();

			}).children(".ui-button-text")
			.css({"padding-top":"0px","padding-bottom":"0px"});

			$("button.btn_edit").button({
				icons:{primary:"ui-icon-pencil"}
			}).removeClass("ui-state-default")
			.addClass("ui-button-minor-grey")
			.hover(
				function() { $(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange"); },
				function() { $(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey"); }
			).click(function(e) {
				e.preventDefault();
			})
			.children(".ui-button-text")
			.css({"padding-top":"2px","padding-bottom":"0px"});

			$("button.btn_copy").button({
				icons:{primary:"ui-icon-copy"}
			}).removeClass("ui-state-default")
			.addClass("ui-button-minor-grey")
			.hover(
				function() { $(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange"); },
				function() { $(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey"); }
			).click(function(e) {
				e.preventDefault();
			})
			.children(".ui-button-text")
			.css({"padding-top":"2px","padding-bottom":"0px"});

			$("button.btn_restore").button({
				icons:{primary:"ui-icon-arrowrefresh-1-e"}
			}).removeClass("ui-state-default")
			.addClass("ui-button-minor-grey")
			.hover(
				function() { $(this).removeClass("ui-button-minor-grey").addClass("ui-button-minor-orange"); },
				function() { $(this).removeClass("ui-button-minor-orange").addClass("ui-button-minor-grey"); }
			).click(function(e) {
				e.preventDefault();
			})
			.children(".ui-button-text")
			.css({"padding-top":"2px","padding-bottom":"0px"});


			var my_window = AssistHelper.getWindowSize();
			if(my_window['width']>800) {
				var my_width = 850;
			} else {
				var my_width = 800;
			}
			var my_height = my_window['height']-50;

			$("div.dlg-child").dialog({
				autoOpen: false,
				modal: true,
				width: my_width,
				height: my_height,
				beforeClose: function() {
					AssistHelper.closeProcessing();
				},
				open: function() {
					$(this).dialog('option','width',my_width);
					$(this).dialog('option','height',my_height);
				}
			});

			$("button.action-button").click(function() {
				AssistHelper.processing();
				var idp_id = <?php echo $idp_object_id; ?>;
				var i = $(this).attr("object_id");
				var p = $(this).attr("parent_id");
				var t = "<?php echo $section; ?>";
				var dta = "version=<?php echo $current_version_displayed; ?>&idp_object_id="+idp_id+"&object_type="+t+"&object_id="+i+"&parent_id="+p;
				if($(this).hasClass("btn_restore")) {
					var result = AssistHelper.doAjax("inc_controller.php?action=SEGMENTS.RESTORE",dta);
					if(result[0]=="ok") {
						AssistHelper.finishedProcessingWithRedirect(result[0],result[1],document.location.href);
					} else {
						AssistHelper.finishedProcessing(result[0],result[1]);
					}
				} else {
					$dlg = $("div#dlg_child");
					var act = "view";
					if($(this).hasClass("btn_edit")) {
						act = "edit";
					} else if($(this).hasClass("btn_add") || $(this).hasClass("btn_plus")) {
						act = "add";
					} else if($(this).hasClass("btn_copy")) {
						act = "copy";
					}
					current_action = act;
					var obj = t.toLowerCase();
					var heading = AssistString.ucwords(act);
					var url = "<?php echo $dialog_url; ?>?display_type=dialog&page_action="+act+"&"+dta;
					//console.log(url);
					$dlg.dialog("option","title",heading);
					if(act=="view") {
						$dlg.dialog("option","buttons",[ { text: "Close", click: function() { $( this ).dialog( "close" ); } } ]);
						var ifr_width = my_width-50;
						var ifr_height = my_height-100;
					} else {
						$dlg.dialog("option","buttons",[ ]);
						var ifr_width = my_width-30;
						var ifr_height = my_height-50;
					}
					$("#ifr_form_display").prop("width",(ifr_width)+"px").prop("height",(ifr_height)+"px").prop("src",url);
					//FOR DEVELOPMENT PURPOSES ONLY - DIALOG SHOULD BE OPENED BY PAGE INSIDE IFRAME ONCE IFRAME FULLY LOADED
					$dlg.dialog("open");
/*
				*/
				}
			});



			$("#btn_export").button({
				icons:{primary:"ui-icon-arrowreturnthick-1-s"}
			}).click(function() {
				document.location.href = "common/export_excel.php?section=<?php echo $section; ?>&version=<?php echo $current_version_displayed; ?>";
			});

			$("#btn_version_go").button({
				icons:{primary:"ui-icon-check"}
			}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.click(function(e) {
				e.preventDefault();
				AssistHelper.processing();
				var v = $("#sel_version").val();
				var p = "<?php echo $redirect; ?>&mscoa_version_to_display="+v;
				document.location.href = p;
			});

		});
	function dialogFinished(icon,result) {
		if(icon=="ok") {
			//alert(result)
			//document.location.href = '<?php echo $dialog_redirect; ?>&r[]=ok&r[]='+result;
			$dlg = $("div#dlg_child");
			$dlg.dialog("close");
			AssistHelper.processing();
			if(current_action=="copy") {
				AssistHelper.finishedProcessing(icon,result);
			} else {
			AssistHelper.finishedProcessingWithRedirect(icon,result,'<?php echo $dialog_redirect; ?>');
			}
		} else {
			AssistHelper.processing();
			AssistHelper.finishedProcessing(icon,result);
		}
	}

	</script>
	<?php



?>