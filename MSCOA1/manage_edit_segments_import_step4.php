<?php
//ASSIST_HELPER::arrPrint($_REQUEST);
$import_ref = $_REQUEST['import_ref'];
$sql = "SELECT * FROM ".$segmentObject->getDBRef()."_segment_import WHERE import_ref = '".$import_ref."'";
$data = $segmentObject->mysql_fetch_all_by_id($sql,"import_key");

if(count($data)>0) {

	$parent_ids_by_import_key = array();

	$copy_fields = array(
		"parent_id","ref","name","description","nickname","line_version","has_child","can_post"
	);

	echo "<p class=b>Importing:<ul>";
	foreach($data as $key => $row) {
		echo "<li>Row ".$key."...";
		if($row['parent_source'] == "NEW") {
			$row['parent_id'] = $parent_ids_by_import_key[$row['parent_id']];
		}
		$import_data = array();
		foreach($copy_fields as $fld) {
			$import_data[$fld] = $row[$fld];
		}
		
		$import_data['status'] = MSCOA1::ACTIVE;
		$import_data['insertdate'] = date("Y-m-d H:i:s");
		$import_data['insertuser'] = $segmentObject->getUserID();
		
		$sql = "INSERT INTO ".$segmentObject->getTableName()." SET ".$segmentObject->convertArrayToSQLForSave($import_data);
		$id = $segmentObject->db_insert($sql);
		$parent_ids_by_import_key[$key] = $id;
		
		echo "done.</li>";
		
	}
	echo "</ul>
	";
$sql = "UPDATE ".$segmentObject->getDBRef()."_segment_import SET import_status = 1 WHERE import_ref = '".$import_ref."'";
$segmentObject->db_update($sql);
	//ASSIST_HELPER::displayResult(array("ok","Import completed successfully."));

	echo "
	<script type=text/javascript>
	document.location.href = 'manage_edit_segments.php?section=".$section."&r[]=ok&r[]=Import completed successfully.';
	</script>";
	
} else {
	ASSIST_HELPER::displayResult(array("error","No data found to import - the holding table does not contain any data for reference: ".$import_ref.".  Please try again otherwise contact your Business Partner."));
	
}
?>