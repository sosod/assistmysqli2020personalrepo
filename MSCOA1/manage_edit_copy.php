<?php
/**
 * FROM CALLING PAGE:
 * object_type = segment name
 * page_action = copy
 * child_object_id = source line item being copied
 * parent_object_id = not in use
 */

$redirect_url = $page.".php?";
$r = array();
foreach($_REQUEST as $key => $value) {
	if($key!="r") {
		$r[] = $key."=".$value;
	}
}
$redirect_url.=implode("&",$r);


if(isset($_REQUEST['step'])) {
	$step = $_REQUEST['step'];
} else {
	$step = 1;
	$_REQUEST['step'] = 1;
}
$total_steps = 6;


//ASSIST_HELPER::arrPrint($_REQUEST);


$do_I_have_to_die_on_step_5 = false;
function step5Display($src_id=0,$title="",$level=0) {
	global $segment_data;
	global $total_children_display;
	global $do_I_have_to_die_on_step_5;

	if($total_children_display<300) {
		foreach($segment_data[$src_id] as $key => $line) {
			$total_children_display++;
			echo "
			<tr class='tr-hover'>
				<td><input type='checkbox' checked class=line-copy name='line_copy[]' value='$key' /></td>
				<td><input type='hidden' name='line_src_id[]' value='$key' /><input type='text' size=35 name='line_ref[]' value='".$line['ref']."' class='line-ref' /></td>
				<td><input type='text' class=line-name size=65 name='line_name[]' value='".$line['name']."' /><br /><span class='i' style='margin-left: 31px;'>Nickname: </span><input type='text' class='line-nickname' size=50 name='line_nickname[]' value='".$line['nickname']."' /></td>
			</tr>
			";

			if(isset($segment_data[$key])) {
				if($total_children_display<300) {
					$title .= (strlen($title) > 0 ? ":" : "").$line['name'];
					$level++;
					echo "<tr class='i level level-".$level."'><td colspan='3'>".$title."</td></tr>";
					step5Display($key, $title, $level);
					echo "<tr class='i level-end level-".$level."'><td colspan='3'>END: ".$title."</td></tr>";
				} else {
					$do_I_have_to_die_on_step_5 = true;
				}
			}
		}
	} else {
		$do_I_have_to_die_on_step_5 = true;
	}
}






/** ACTIONS REQUIRED FOR MORE THAN ONE STEP */
		//1. get versions
		$listObject = new MSCOA1_LIST("version");
		$list_items = $listObject->getActiveListItemsFormattedForSelect();
		ksort($list_items);
		$list_items = array_reverse($list_items,true);
		//2. get source line details
		$myObject = new MSCOA1_SEGMENTS($object_type);
		$row = $myObject->getRawObject($child_object_id);
		$parent_name = $myObject->getParentName($row['parent_id'],0,false);
		//start display line details & form
		echo "
		<h1>Copy Line to New Version<span class=float>Step $step of $total_steps </span></h1>
		";

		//if new_object_id exists then get those details as well
		if(isset($_REQUEST['new_object_id'])) {
			$new_object_id = $_REQUEST['new_object_id'];
			$new_row = $myObject->getRawObject($new_object_id);
			$new_parent_name = $myObject->getParentName($new_row['parent_id'],0,false);
		}

switch($step) {



	/** STEP 1 - Select which version you want to copy the line item to [no info saved to DB yet] */
	case 1:
		$next_step = 2;
		//3. filter for versions not used - WAIT??? That's not going to work if 6.4 has been added globally?! [JC] 22 March AA-347 - don't do this
		//3.b. Remove the version selected on the page that you are looking at when you click copy - clearly you can't copy it to the same version you are looking at
		if(isset($_REQUEST['version']) && ASSIST_HELPER::checkIntRef($_REQUEST['version'])) {
			unset($list_items[$_REQUEST['version']]);
		}
		//4. display line details & form
		echo "
		<p><span class='b'>Source Line: </span>".$parent_name.(strlen($parent_name)>0?": ":"").$row['name']." [".$row['ref']."]</p>
		<div class='center'>
		<p class='i'>Select the new version:</p>";
		foreach($list_items as $key => $value) {
			echo "<div class='div-version'><button class='btn-version' id='".$key."'>".$value."</button></div>";
		}
		echo "</div>";
		//5. JS & Next steps
		//echo "<p><button id='btn_next_".$step."' class='btn-next'>Next Step</button></p>";
		$js.="
		$('.div-version').css({'margin':'10px','padding':'10px'});
		$('.btn-version').button().removeClass('ui-state-default').addClass('ui-button-state-green').hover(
			function() {
				$(this).removeClass('ui-button-state-green').addClass('ui-button-state-orange');
			},function() {
				$(this).removeClass('ui-button-state-orange').addClass('ui-button-state-green');
			}
		).click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			document.location.href = '".$redirect_url."&new_version='+$(this).prop('id')+'&step=".$next_step."';
		});

		";

		break;











	/** STEP 2 - Select the parent you want to link the line to (based on the version selected in step 1) [no info saved to DB yet] */
	case 2:
		$next_step = 3;
		$new_version = $_REQUEST['new_version'];
		echo "
		<p><span class='b'>Source Line: </span>".$parent_name.(strlen($parent_name)>0?": ":"").$row['name']." [".$row['ref']."]</p>
		<p><span class='b'>New Version:</span> ".$list_items[$new_version]." [Step ".($step-1)."]</p>
		<div  style='margin-top:20px; border:1px solid #ababab'>
		<p class='i'>Select the new Parent:</p>";
			$extra_info = array();
			$extra_info['include_nickname'] = true;
			$extra_info['include_guid'] = true;
			$extra_info['version'] = $_REQUEST['new_version'];
			$extra_info['can_have_children'] = true;
			//can't tag myself as my own parent so don't include me in the list of possible parents
			$extra_info['ignore_line_id'] = $child_object_id;
			$extra_info['ignore_children_of_given_line_id'] = true;
			$extra_info['ignore_no_can_post_and_check_for_can_have_children'] = true;
			$parent_options = $myObject->getActiveListItemsFormattedForSelect($extra_info);
			echo "<p class='p-hover' key='0'><input type='radio' name='parent_id' id='chk_0' class='rad-parent' ".(count($parent_options)==0?"checked":"")." value='0' />&nbsp;N/A <span class='float i' style='font-size:75%; margin-right:10px;'>[Top Level]</span></p>";
			foreach($parent_options as $key => $value) {
				echo "<P class='p-hover' key='$key'><input type='radio' name='parent_id' id='chk_".$key."' class='rad-parent' value='".$key."' />&nbsp;".str_replace("[","<span class='i float' style='font-size:75%; margin-right:10px;'>[",$value)."</span></P>";
			}
		echo "
		</div>
		<p class='float'><button id='btn_next_".$step."' class='btn-next'>Next Step</button></p>";
		$js.="
		$('#btn_next_".$step."').click(function(e) {
			e.preventDefault();
			var new_parent = $('.rad-parent:checked').val();
			var new_version = ".$new_version.";
			var next_step = ".$next_step.";
			document.location.href = '".$redirect_url."&new_parent='+new_parent+'&new_version='+new_version+'&step=".$next_step."';
		});
		$('.p-hover').css({'padding':'3px','border':'1px solid #ffffff'}).hover(
			function() {
				$(this).css('background-color','#dedede').css('border','1px solid #dedede');
			},function() {
				$(this).css('background-color','').css('border','1px solid #ffffff');
			}
		).click(function() {
				if($('#chk_'+$(this).attr('key')).is(':checked')) {
					$('#chk_'+$(this).attr('key')).prop('checked',false);
				} else {
					$('#chk_'+$(this).attr('key')).prop('checked',true);
				}
		});
		

		";
		break;











	/** STEP 3 - Make any detail changes [now save - treat as an add BUT also go back and check if source line had the same version selected & remove those version] */
	case 3:
		$next_step = 4;
		$new_version = $_REQUEST['new_version'];
		$new_parent = $_REQUEST['new_parent'];
		echo "
		<p><span class='b'>Source Line: </span>".$parent_name.(strlen($parent_name)>0?": ":"").$row['name']." [".$row['ref']."]</p>
		<div  style='margin-top:20px;'>
		<p class='i'>Make any details changes:</p>";
		$data = $displayObject->getObjectForm($object_type, $myObject, $child_object_id, "SEGMENT", null, $new_parent, "copy", $redirect_url."&step=".$next_step."&",true,0,"new_object_id");
			echo $data['display'];
			$js.= $data['js'];

		echo "</div>";
		break;











	/** STEP 4 - Copy children as well? [no save] */
	case 4:
		$next_step = 5;
		//remove new version from source row if required.
		$original_version = $row['line_version'];
		$original_version = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$original_version);
		$original_version = ASSIST_HELPER::removeBlanksFromArray($original_version);
		$new_version = $new_row['line_version'];
		$new_version = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$new_version);
		$new_version = ASSIST_HELPER::removeBlanksFromArray($new_version);
		$new_k = array_keys($new_version);
		$new_v = $new_version[$new_k[0]];
		if(in_array($new_v,$original_version)) {
			foreach($original_version as $k => $o) {
				if($o==$new_v) {
					unset($original_version[$k]);
				}
			}
			$row['line_version'] = $original_version;
			$edit_var = $row;
			$edit_var['segment_section'] = $object_type;
			$edit_var['object_id'] = $_REQUEST['object_id'];
			unset($edit_var['id']);
			$r = $myObject->editObject($edit_var);
		}
	//
		echo "
		<p><span class='b'>New Line: </span>".$new_parent_name.(strlen($new_parent_name)>0?": ":"").$new_row['name']." [".$new_row['ref']."]</p>
		<div  style='margin-top:20px;' class='center'>
		<p class='i'>Copy Children as well?</p>
		<p style='margin:20px;'><button id='btn_yes'>Yes</button></p>
		<p style='margin:20px'><button id='btn_no'>No</button></p>
		
		
		";
		ASSIST_HELPER::displayResult(array("warn","WARNING: Only the first 300 children will be available to copy.  If there are more than 300 children, it is advisable to not copy here but to move down to the next level before copying children."));
		$js.="
		$('#btn_yes').button({icons:{primary:'ui-icon-check'}})
			.removeClass('ui-state-default').addClass('ui-button-state-green')
			.hover(
				function() {
					$(this).removeClass('ui-button-state-green').addClass('ui-button-state-orange');
				},function() {
					$(this).removeClass('ui-button-state-orange').addClass('ui-button-state-green');
				}
			)
			.click(function(e) {
				e.preventDefault();
				document.location.href = '".$redirect_url."&step=".$next_step."';
			});
		$('#btn_no').button({icons:{primary:'ui-icon-closethick'}})
			.removeClass('ui-state-default').addClass('ui-button-state-red')
			.hover(
				function() {
					$(this).removeClass('ui-button-state-red').addClass('ui-button-state-orange');
				},function() {
					$(this).removeClass('ui-button-state-orange').addClass('ui-button-state-red');
				}
			)
			.click(function(e) {
				e.preventDefault();
				window.parent.dialogFinished('ok','Line copied successfully.  To view the new line, change the Version drop down in the top right corner to \'".$list_items[$new_v]."\' and click the \'Go\' button');
			});
		
		";




		break;











	/** STEP 5 - Display GUID & Name boxes for all children to allow for mass changes [now save, treat each one as a separate add]  */
	case 5:
		$next_step = 6;
		$segment_data = $myObject->getList(0,TRUE,$idp_object_id,false,true);
		echo "
		<style type='text/css'>
			.line-name { padding: 3px; margin: 3px; }
			.line-nickname { padding: 3px; margin: 3px; font-style: italic; }
			.line-ref { padding: 3px; margin: 3px; }
			.line-copy { padding: 3px; margin: 3px; }
			.level , .level td { background-color: #eeeeff; font-size: 100%; line-height: 125%; padding: 5px; }
			.level-end , .level-end td { background-color: #ededed; font-size: 100%; line-height: 125%; padding: 5px; }
		</style>
		<p><span class='b'>New Line: </span>".$new_parent_name.(strlen($new_parent_name)>0?": ":"").$new_row['name']." [".$new_row['ref']."]</p>
		<div  style='margin-top:20px;'>
		<p class='i'>Confirm Children Details</p>
		<form name='frm_copy_children' method='post' action='".$redirect_url."&step=".$next_step."' target='_self'>
		<table>
		<tr>
			<th></th>
			<th>GUID</th>
			<th>Name / <br />Nickname</th>
		</tr>
		";
		$title = "";
		$total_children_display = 0;
		step5Display($child_object_id,$title);
//		foreach($segment_data[$child_object_id] as $key => $line) {
//			echo "
//			<tr class='tr-hover'>
//				<td><input type='checkbox' checked class=line-copy name='line_copy[]' value='$key' /></td>
//				<td><input type='hidden' name='line_src_id[]' value='$key' /><input type='text' size=35 name='line_ref[]' value='".$line['ref']."' class='line-ref' /></td>
//				<td><input type='text' class=line-name size=65 name='line_name[]' value='".$line['name']."' /><br /><span class='i' style='margin-left: 31px;'>Nickname: </span><input type='text' class='line-nickname' size=50 name='line_nickname[]' value='".$line['nickname']."' /></td>
//			</tr>
//			";
//			if(isset($segment_data[$key])) {
//				foreach($segment_data[$key] as $key2 => $line) {
//					echo "
//					<tr class='tr-hover2'>
//						<td><input type='checkbox' checked class=line-copy name='line_copy[]' value='$key2' /></td>
//						<td><input type='hidden' name='line_src_id[]' value='$key2' /><input type='text' size=35 name='line_ref[]' value='".$line['ref']."' class='line-ref' /></td>
//						<td><input type='text' class=line-name size=65 name='line_name[]' value='".$line['name']."' /><br /><span class='i' style='margin-left: 31px;'>Nickname: </span><input type='text' class='line-nickname' size=50 name='line_nickname[]' value='".$line['nickname']."' /></td>
//					</tr>
//					";
//				}
//			}
//		}
		echo "
		</table>
		<p><button id='btn_next_".$step."' class='btn-next'>Save Children</button></p>
		";
		if($do_I_have_to_die_on_step_5) {
			echo "<div width='450px' style='width:450px'>";
			ASSIST_HELPER::displayResult(array("warn","Not all children have been displayed here.  Some might not copy."));
			echo "<P><button id='btn_back_".$step."' class='btn-back'>Go Back</button></P>
			</div>";
		}
		echo "
		</form>";

		$js.="
		$('#btn_next_".$step."').click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			$('form[name=frm_copy_children]').submit();
		});
		$('#btn_back_".$step."').click(function(e) {
			e.preventDefault();
			AssistHelper.processing();
			document.location.href = '".$redirect_url."&step=".($step-1)."';
		});

		$('.tr-hover').hover(
			function() {
				$(this).css('background-color','#dedede');
			},function() {
				$(this).css('background-color','');
			}
		);
		$('.tr-hover2').css('background-color','#efefef').hover(
			function() {
				$(this).css('background-color','#dedede');
			},function() {
				$(this).css('background-color','#efefef');
			}
		);
		";


		break;








	/** STEP 6 - Save children END OF PROCESS */
	case 6:

		//get original details for anything not set on previous screen
		$original_records = $myObject->getRawObject($_REQUEST['line_copy']);
		//notify user that copy is in progress
		ASSIST_HELPER::displayResult(array("info","Copying in progress.  Please wait.  You will be notified when the process is complete."));
		//foreach line from form
		$new_parent_ids = array($child_object_id=>$_REQUEST['new_object_id']);
		foreach($_REQUEST['line_src_id'] as $key => $src_id) {
			if(in_array($src_id,$_REQUEST['line_copy'])) {
				$src = $original_records[$src_id];
				$parent_id = isset($new_parent_ids[$src['parent_id']]) ? $new_parent_ids[$src['parent_id']] : $_REQUEST['new_object_id'];
				$insert_data = array(
						'segment_section'=>$_REQUEST['object_type'],
						'parent_id'=>$parent_id,
						'ref'=>ASSIST_HELPER::code($_REQUEST['line_ref'][$key]),
						'name'=>ASSIST_HELPER::code($_REQUEST['line_name'][$key]),
						'description'=>$src['description'],
						'nickname'=>ASSIST_HELPER::code($_REQUEST['line_nickname'][$key]),
						'has_child'=>$src['has_child'],
						'can_post'=>$src['can_post'],
						'line_version'=>array($_REQUEST['new_version']),
				);
				$r = $myObject->addObject($insert_data);
				$new_parent_ids[$src_id] = $r['object_id'];
				echo "<div class='div-child-result'>"; ASSIST_HELPER::displayIconAsDiv($r[0]); echo "<div style='display:inline-block'><p>New line '".$insert_data['name']."' [".$myObject->getRefTag().$r['object_id']."] created successfully.</p></div></div>";
			}
		}
		//notify user that process is complete
		ASSIST_HELPER::displayResult(array("ok","All children copied successfully.  You may now close the dialog. "));
		echo "
		<span class=float><button id='btn_close'>Close</button></span>
		<script type='text/javascript'>
			$(function() {
				//scroll to the bottom of the page to display the finish message
				$('html, body').animate({ scrollTop: $(document).height() }, 2000);
				$('#btn_close').button({icons:{primary:'ui-icon-closethick'}}).removeClass('ui-state-default').addClass('ui-button-state-grey').click(function(e) {
					e.preventDefault();
					window.parent.$('#dlg_child').dialog('close');
				});
				
			});
		</script>";
		break;





	default:
		ASSIST_HELPER::displayResult(array("error","Sorry, I couldn't work out what you wanted me to do.  Please close the dialog and try again."));
		break;

}














?>
<script type=text/javascript>
$(function() {

		$('.btn-next').button({icons:{secondary:"ui-icon-arrow-1-e"}})
		.removeClass('ui-state-default').addClass('ui-button-state-green').addClass('float')
		.hover(
			function() {
				$(this).removeClass('ui-button-state-green').addClass('ui-button-state-orange');
			},function() {
				$(this).removeClass('ui-button-state-orange').addClass('ui-button-state-green');
			}
		);
		$('.btn-back').button({icons:{primary:"ui-icon-arrow-1-w"}})
		.removeClass('ui-state-default').addClass('ui-button-state-grey')
		.hover(
			function() {
				$(this).removeClass('ui-button-state-grey').addClass('ui-button-state-orange');
			},function() {
				$(this).removeClass('ui-button-state-orange').addClass('ui-button-state-grey');
			}
		);

	<?php
	echo $js;
	?>


	<?php
	if($display_type=="dialog" && $step==1) {
		echo $displayObject->getIframeDialogJS($child_object_type,"dlg_child",$_REQUEST);
	}
	?>

});
</script>
