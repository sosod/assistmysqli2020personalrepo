<?php
/**
 * @var MSCOA1_HEADINGS $headingObject - from inc_header
 * @var string $js - from inc_header
 */


$page_section = "MANAGE";
$page_action = "EDIT";





function drawRow($key,$row,$parent_name="") {
	global $headings;
	global $segment_data;
	global $displayObject;
	global $all_list_data;
	global $headingObject;
	global $js;
		echo "<tr>";
		foreach($headings['rows'] as $fld => $head) {
			$fld = $fld=="has_child"?"has_children":$fld;
			$fld = $fld=="can_post"?"can_assign":$fld;
			$v = $row[$fld];
			switch($head['type']) {
				case "BOOL":
					$x = $displayObject->getDataField("BOOL", $v);
					$v = $x['display'];
					$js.=$x['js'];
					$class = "center";
					break;
				case "LIST":
				case "MULTILIST":
					$list = $all_list_data[$fld];
					$raw = explode(";",$v);
					$output = array();
					foreach($raw as $i) {
						if(isset($list[$i])) {
							$output[] = $list[$i];
						}
					}
					if(count($output)==0) {
						$output[] = $headingObject->getUnspecified();
					}
					$v = implode(";<br />",$output).";";
					break;
				default:
					$class = "";
					break;
			}
			if($fld=="name") {
				echo "<td class='$class'>".$parent_name."</td>";
			}
			echo "<td class='$class'>".$v."</td>";
		}
		//status
		if($row['has_children']==false && $row['can_assign']==false) {
			$status = ASSIST_HELPER::getDisplayIconAsDiv("error")." Error - Dead end";
		} elseif($row['has_children']==true && (!isset($segment_data[$key]) || count($segment_data[$key])==0)) {
			$status = ASSIST_HELPER::getDisplayIconAsDiv("info")."Warning - No children have been added";
		} elseif($row['can_assign']==true && $row['has_children']==true) {
			$status = ASSIST_HELPER::getDisplayIconAsDiv("info")."Warning! Posting should be done at the lowest level.<br />'Can be assigned' should be turned off if a line item has children";
		} else {
			$status = ASSIST_HELPER::getDisplayIconAsDiv("ok")."OK";
		}
		echo "
			<td>$status</td>
		</tr>";
		if(isset($segment_data[$key]) && count($segment_data[$key])>0) {
			foreach($segment_data[$key] as $key2 => $row2) {
				$new_parent_name = (strlen($parent_name)>0?$parent_name." > ":"").$row['name'];
				drawRow($key2,$row2,$new_parent_name);
			}
		}

}






include("inc_header.php");


//ASSIST_HELPER::arrPrint($_REQUEST);


	$section = $_REQUEST['section']; //echo $section;
	$segmentObject = new MSCOA1_SEGMENTS($section);
	//echo "getList start";
	$segment_data = $segmentObject->getList(0,TRUE);
	//echo "getList end";
	$segment_details = $segmentObject->getSegmentDetails(true);
	//ASSIST_HELPER::arrPrint($segment_data[0]);

$headings = $headingObject->getMainObjectHeadings("SEGMENT");
unset($headings['rows']['parent_id']);
//ASSIST_HELPER::arrPrint($headings);


if(count($segment_data)>0 && isset($segment_data[0]) && count($segment_data[0])>0) {

	echo "
<table id=tbl_container><tr><td class=td-noborder style='text-align:right;'>";
?>
<!--<span class=float><span class=b>Filter by:</span> <?php echo ASSIST_HELPER::getDisplayIconAsDiv("error")." Errors"; ?> | <?php echo ASSIST_HELPER::getDisplayIconAsDiv("info")." Warnings"; ?> | <?php echo ASSIST_HELPER::getDisplayIconAsDiv("ok")." OK"; ?></span>-->
<!--<div id="set_error" style='display:inline-block'>
	<input type="radio" id="rad_error" name="error">
	<label for="rad_error">Errors</label>
</div><!--
<div id="set_warn" style='display:inline-block'>
	<input type="radio" id="rad_warn" name="warn">
	<label for="rad_warn">Warnings</label>
</div>
<div id="set_ok" style='display:inline-block'>
	<input type="radio" id="rad_ok" name="ok">
	<label for="rad_ok">OK</label>
</div>
<span class=float><button id=btn_error>Errors</button>
<!--	<button>Warnings</button><button>All ok</button>
</span>
	-->
<?php
//-->
echo "
</td></tr>
<tr><td class=td-noborder>
	<table id=tbl_list>
		<tr>
	";
$all_list_data = array();
	foreach($headings['rows'] as $fld => $head) {
		if($headingObject->isListField($head['type'])) {
			$listObject = new MSCOA1_LIST($head['list_table']);
			$all_list_data[$fld] = $listObject->getActiveListItemsFormattedForSelect();
			unset($listObject);
		}
		if($fld=="name") {
			echo "<th>Parent</th>";
		}
		$width="";
		if($head['type']=="BOOL") {
			$width="width='100px'";
		}
		echo "<th $width >".$head['name']."</th>";
	}
	echo "
		<th>Status</th>
	</tr>";
	foreach($segment_data[0] as $key => $row) {
		drawRow($key,$row);
	}
	echo "
	</table>
</td></tr></table>
<p class=i>Report drawn: ".date("d F Y H:i")."</p>
	";

} else {
	echo "<p>No items found to display.</p>";
}


?>

<script type=text/javascript>
$(function() {
	<?php echo $js; ?>
	$("#tbl_container, .td-noborder").css("border","0px solid #FFFFFF");
});
</script>
