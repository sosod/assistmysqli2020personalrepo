<?php
require_once("inc_header.php");

//Setting the url of the page sans queries
$noparams = explode("&",$_SERVER['REQUEST_URI']);
$pageonly = explode("/",$noparams[0]);
$thispage = $pageonly[2];

$list_id = $_REQUEST['l'];

$listObj = new MSCOA1_LIST($list_id);
//echo $listObj->getListTable();
$fields = $listObj->getFieldNames();
$required_fields = $listObj->getRequredFields();
$types = $listObj->getFieldTypes();
$items = $listObj->getListTable();
$list_data = $listObj->getListItemsForSetup();

//ASSIST_HELPER::arrPrint($fields);


ASSIST_HELPER::displayResult(isset($_REQUEST['r']) ? $_REQUEST['r'] : array());


if($list_id=="version" || $list_id=="list_version") {
	ASSIST_HELPER::displayResult(array("info","Changes made here will take up to 30 minutes to reflect for other users within Assist but will reflect immediately for you. Other users can force the changes to reflect by logging out and back in."));
}

?>
<div class="horscroll">
<table class='tbl-container not-max'><tr><td class=right>
<!-- <span class="float"><button id="btn_add">Add</button></span> -->
<h2 class="left"><?php echo $helper->replaceAllNames($headingObject->getAListHeading($list_id)); ?></h2>
<?php

?>
<form name=frm_list>
<input type="hidden" id="list_id" name="list_id" value="<?php echo $list_id; ?>" />
<input type="hidden" id="sort" name="sort" value="99" />
<table class=list id=tbl_list>
    <thead>
        <tr><?php
        	foreach($fields as $fld=>$val){
        		echo"<th>".$val.((isset($required_fields[$fld]) && $required_fields[$fld]===true)?"*":"")."</th>";
        	}
    	?>
            <!-- <th></th> -->
        </tr>
    </thead>
    <tbody>
        <?php
        //Add list
        		/*echo "<tr id=\"add_row\">";
        		foreach($fields as $fld=>$name){
        			echo"<td >";
					//echo $fld;
					if($fld == "status"){
						echo "<div class=center>Active</div><input type=\"hidden\" name='$fld' id='$fld' value=" . MSCOA1::ACTIVE . " />";
					}else if($types[$fld]=="LIST"){
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>$fld, 'name'=>$fld, 'class'=>"list_num", 'options'=>$list_items2,'require_me'=>true, 'unspecified'=>false));
					}else if($fld == "colour"){
						echo "<input type=\"hidden\" name='$fld' id=\"add_colour\" value=\"#FFFFFF\" />";
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>'clr_'.$fld, 'name'=>$fld,'require_me'=>(isset($required_fields[$fld]) && $required_fields[$fld]==true ? "1" : "0")));
					}elseif($types[$fld]=="BOOL_BUTTON"){
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>$fld, 'name'=>$fld,'require_me'=>(isset($required_fields[$fld]) && $required_fields[$fld]==true ? "1" : "0"),'form'=>"vertical"));
					}else{
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>$fld, 'name'=>$fld,'require_me'=>(isset($required_fields[$fld]) && $required_fields[$fld]==true ? "1" : "0")));
					}
        			echo"</td>";
					
        		}
				echo "<td class=center><input type=button name=btn_add value=Add /></td></tr>";
*/
    	?>
        <?php
			foreach($list_data as $key=>$val){
				echo"<tr id=\"tr_".$val['id']."\" ".((($val['status'] & MSCOA1::INACTIVE)==MSCOA1::INACTIVE) ? "class=inact" : "")." >";
				foreach($fields as $fld=>$head){
					$valkey = $fld;
					$valval = $val[$fld];	
					switch($valkey){
						case "sort":
							break;
						case "status":
							if(($valval & MSCOA1::SYSTEM_DEFAULT)==MSCOA1::SYSTEM_DEFAULT){
								echo "<td sys=1 class=\"center\">System Default";
							}else if(($valval & MSCOA1::ACTIVE)==MSCOA1::ACTIVE){
								echo "<td class=\"center\">Active";
							}else if(($valval & MSCOA1::INACTIVE)==MSCOA1::INACTIVE){
								echo "<td class=\"center\">Inactive";
							}else{
								echo "<td class=\"center\">";
							}
							
								echo "</td>";
							break;
						case "dir_id":
						case "function_id":
							if(isset($list_items[$valval])){
								echo "<td fld=\"".$valkey."\">".($list_items[$valval]['name'])."</td>";
							} else {
								echo "<td fld=\"".$valkey."\">".$helper->getUnspecified()."</td>";
							}
							break;
						case "core":
							echo "<td class=center fld=\"".$valkey."\">";
								$displayObject->drawBoolForDisplay($valval);
							echo "</td>";
							break;
						case "colour":
							echo "<td style=\"background-color:".$valval."\" fld=\"". $valkey ."\"></td>";
							break;
						default:	
							echo "<td fld=\"". $valkey ."\">".ASSIST_HELPER::decode($valval)."</td>";
						//$js .=" rows['".$val['id']."']['". $valkey ."'] = ". $valval ." ; ";
					}
				}/*
				if(($val['status']& MSCOA1::ACTIVE)==MSCOA1::ACTIVE){
				    echo "<td class=center><button id='".$val['id']."' class='btn-edit' can_delete='".($val['can_delete']==true ? 1 : 0)."'>Edit</button></td>";
				}else{
				    echo "<td class=center><input type=button value=Restore id='".$val['id']."' class='btn_restore' /></td>";
				}*/
				echo "</tr>";
			}	   
	   ?>

    </tbody>
</table>
</form>
</td></tr><tr><td>
	<?php $js.= $displayObject->drawPageFooter($helper->getGoBack('setup_defaults.php'),"list",array('section'=>$list_id)); ?>
</td></tr>
</table>
</div>
<div id=div_dialog title="Edit">
    <h1>Edit form</h1>
    <form name=edit_frm>
    <input type="hidden" name="id" id="ref" value=0 />
    <input type="hidden" name="list_id" id="list_id" value=<?php echo $list_id; ?> />
    <table id=ed_frm class=form>
    	<?php
    	
    	foreach($fields as $fld=>$name){
    		echo"<tr>
    				<th>".$name.((isset($required_fields[$fld]) && $required_fields[$fld]==true ? "*" : "")).":</th>
    				<td fld=edit_td_".$fld." id=\"edit_td_".$fld."\">";
					if($fld == "status"){
    					echo"Active";
					}else if($types[$fld]=="LIST"){
						echo"<p class=\"for_display\"></p>";
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>"edit_".$fld, 'name'=>$fld, 'options'=>$list_items2,'require_me'=>"1", 'unspecified'=>false));
					}else if($types[$fld]=="BOOL_BUTTON"){
						echo"<p class=\"for_display\"></p>";
						$js.= $displayObject->drawFormField($types[$fld], array('id'=>"edit_".$fld, 'name'=>$fld, 'require_me'=>"1"));
					}else{
						echo"<p class=\"for_display\"></p>";
						$js.=$displayObject->drawFormField($types[$fld],array('id'=>"edit_".$fld, 'name'=>$fld,'require_me'=>(isset($required_fields[$fld]) && $required_fields[$fld]==true ? "1" : "0")));
					}
				echo"</td>";
			echo"</tr>";
    	}

?>
        
    </table>
  	</form>
  	<?php ASSIST_HELPER::displayResult(array("info","Items can only be deleted if they are not in use elsewhere within in the module.")); ?>
</div>

<div id="div_dialog_restore" title="Restore">
    	<p id="restoration_p"></p>
</div>

<script type="text/javascript">
$(function () {
    <?php echo  $js;    ?>
	$("#btn_add").button({icons:{primary:"ui-icon-plus"}}).removeClass("ui-state-default").addClass("ui-button-state-green");
	$(".btn-edit").button({icons:{primary:"ui-icon-pencil"}}).removeClass("ui-state-default").addClass("ui-button-minor-grey")
			.click(function(e) {
				e.preventDefault();
				$("#div_dialog").dialog("open");
	});
	$("#div_dialog").dialog({
		autoOpen:false,
		modal:true
	});
	$("#tbl_list tr td").css("vertical-align","middle");
});
</script>
