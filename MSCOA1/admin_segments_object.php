<?php

include("inc_header.php");

//ASSIST_HELPER::arrPrint($_REQUEST);

$section = $_REQUEST['section'];
	$segmentObject = new MSCOA1_SEGMENTS();
	$segments = $segmentObject->getSegmentDetails($section);


//ASSIST_HELPER::arrPrint($segments);
?>
<h2><?php echo $segmentObject->replaceActivityNames("|edit|")." ".$segments['name']; ?></h2>
<table class='tbl-container not-max'><tr><td>
<form name=frm_edit>
	<input type=hidden value='<?php echo $section; ?>' name=section />
	<table class=form>
		<tr>
			<th>Name*:</th>
			<td><?php $js.=$displayObject->drawFormField("MEDVC",array('id'=>"name",'name'=>"name"),$segments['name']); ?></td>
		</tr>
		<tr>
			<th>Description:</th>
			<td><?php $js.=$displayObject->drawFormField("TEXT",array('id'=>"help",'name'=>"help",'rows'=>10,'cols'=>100),$segments['help']); ?></td>
		</tr>
		<tr>
			<th></th>
			<td><button id=btn_save><?php echo $segmentObject->replaceActivityNames("|save|"); ?></button></td>
		</tr>
	</table>
</form>
	</td></tr>
	<tr>
		<td><?php $js.= $displayObject->drawPageFooter($helper->getGoBack('admin_segments.php'),"setup",array('section'=>$section)); ?></td>
	</tr>
</table>




<script type="text/javascript">

$(function() {

	<?php echo $js; ?>

	$("#btn_save").button({

	}).click(function(e) {
		e.preventDefault();
		AssistHelper.processing();
		var error = false;
		if($("#name").val().length==0) {
			$("#name").addClass("required");
			error = true;
		} else {
			$("#name").removeClass("required");
		}

		if(error) {
			AssistHelper.finishedProcessing("error","Please complete the required fields highlighted in red.");
		} else {
			var dta = AssistForm.serialize($("form[name=frm_edit]"));
			var result = AssistHelper.doAjax("inc_controller.php?action=NAMES.saveSegmentDetails",dta);
			if(result[0]=="ok") {
				AssistHelper.finishedProcessingWithRedirect(result[0],result[1],"admin_segments.php");
			} else {
				AssistHelper.finishedProcessing(result[0],result[1]);
			}
		}



	});

});

</script>