<?php
/**
 * To manage the display of forms / tables and to provide a link to the Assist_module_display class
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */
class MSCOA1_DISPLAY extends ASSIST_MODULE_DISPLAY {

    //protected $mod_ref;
	protected $default_attach_buttons = true;

    public function __construct() {
    	$me = new MSCOA1();
		$an = $me->getAllActivityNames();
		$on = $me->getAllObjectNames();
		//$this->mod_ref = $me->getModRef();
		unset($me);

        parent::__construct($an,$on);
    }

	public function disableAttachButtons() { $this->default_attach_buttons = false; }
	public function enableAttachButtons() { $this->default_attach_buttons = true; }


	/***********************************************************************************************************************
	 * FORM FIELDS & specific data display
	 */


	public function getDataFieldNoJS($type,$val,$options=array()) {
		$d = $this->getDataField($type, $val,$options);
		return $d['display'];
	}
	/**
	 * (ECHO) displays the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 *
	 * @return JS (Echos HTML)
	 */
	public function drawDataField($type,$val,$options=array()) {
		$d = $this->getDataField($type,$val,$options);
		if(is_array($d)) {
			echo $d['display'];
		} else {
			echo $d;
		}
	}
	/**
	 * (RETURN) generates the html of a selected type of data (NOT FORM FIELD)
	 * @param (String) type = format of the field (Bool, Currency etc)
	 * @param (String) data = the actual value to be displayed
	 * @param (Array) options = any extra info = needed for TEXT & CURRENCY & DECIMAL PLACES
	 * 	 *
	 * @return (Array) array('display'=>HTML,'js'=>javascript)
	 */
	public function getDataField($type,$val,$options=array()) {
		$data = array('display'=>"",'js'=>"");
		switch($type) {
			case "PERC":
				$data['display'] = $this->getPercentageForDisplay($val);
				break;;
			case "NUM":
			case "CALC":
				$data['display'] = $this->getNumberForDisplay($val,2);
				break;;
			case "CURRENCY":
				if(strlen($val)==0 || is_null($val)) {  $val = 0;  }
				if(isset($options['symbol'])) {
					$data['display'] = $this->getCurrencyForDisplay($val,$options['symbol']);
				} else {
					$data['display'] = $this->getCurrencyForDisplay($val);
				}
				if(isset($options['right']) && $options['right']==true) {
					$data['display'] = "<div class=right >".$data['display']."</div>";
				}
				break;
			case "DATE":
				if(isset($options['include_time'])) {
					$data['display'] = $this->getDateForDisplay($val,$options['include_time']);
				} else {
					$data['display'] = $this->getDateForDisplay($val,false);
				}
				break;
			case "BOOL":
			case "BOOL_BUTTON":
				$data['display'] = $this->getBoolForDisplay($val,(isset($options['html']) ? $options['html'] : true));
				break;
			case "REF":
				$data['display'] = (isset($options['reftag']) ? $options['reftag'] : "").$val;
				break;
			case "ATTACH":
				$can_edit = isset($options['can_edit']) ? $options['can_edit'] : false;
				$object_type = isset($options['object_type']) ? $options['object_type'] : "X";
				$object_id = isset($options['object_id']) ? $options['object_id'] : "0";
				$buttons = isset($options['buttons']) ? $options['buttons'] : true;
				$data['display'] = $this->getAttachForDisplay($val,$can_edit,$object_type,$object_id,$buttons);
				break;
			//JS TO BE PROGRAMMED FOR LARGE TEXT - HIDE / SHOW
			default:
				if(is_array($val)) {
					$val = implode(";<br />",$val).";";
				}
				if(isset($options['html']) && $options['html']==true) {
					$val = str_ireplace(chr(10), "<br />", $val);
				} else {
					$val = $val;
				}
				$data = array('display'=>$val,'js'=>"");
				//$data = array('display'=>$type);
		}
		return $data;
	}


	/**
	 * (ECHO) displays html of selected form field and returns any required js
	 */
	public function drawFormField($type,$options=array(),$val="") {
		$ff = $this->createFormField($type,$options,$val);
		echo $ff['display'];
		return $ff['js'];
	}
	/**
	 * Returns string of selected form field
	 *
	 * @param *(String) type = the type of form field
	 * @param (Array) options = any additional properties
	 * @param (String) val = any existing value to be displayed
	 * @return (String) echo
	 */
	public function createFormField($type,$options=array(),$val="") {
		//echo "<br />cFF VAL:+".$val."+ ARR: "; print_r($options);
		switch($type){
			case "REF":
				$data = array('display'=>$val,'js'=>"");
				break;
			case "LABEL":
				$data=$this->getLabel($val,$options);
				break;
			case "SMLVC":
				$data=$this->getSmallInputText($val,$options);
				break;
			case "MEDVC":
				$data=$this->getMediumInputText($val,$options);
				break;
			case "LRGVC":
				if(isset($options['rows']) && isset($options['cols'])) {
					$rows = $options['rows']; unset($options['rows']);
					$cols = $options['cols']; unset($options['cols']);
					$data=$this->getLimitedTextArea($val,$options,$rows,$cols);
				} else {
				$data=$this->getLimitedTextArea($val,$options);
				}
				break;
			case "TEXT":
				if(isset($options['rows']) && isset($options['cols'])) {
					$rows = $options['rows']; unset($options['rows']);
					$cols = $options['cols']; unset($options['cols']);
					$data=$this->getTextArea($val,$options,$rows,$cols);
				} else {
					$data=$this->getTextArea($val,$options);
				}
				break;
			case "LIST":
				$items = $options['options'];
				unset($options['options']);
				$data=$this->getSelect($val,$options,$items);
				break;
			case "MULTILIST":
				$items = $options['options'];
				unset($options['options']);
				if(!is_array($val)) {
					$val2 = array();
					if(strlen($val)>0) {
						$val2[] = $val;
					}
				} else {
					$val2 = $val;
				}
				$data=$this->getMultipleSelect($val2,$options,$items);
				break;
			case "AUTO":
				$items = $options['options'];
				unset($options['options']);
				$data=$this->getAutoComplete($val,$options,$items);
				break;
			case "DATE": //echo "date!";
				$extra = $options['options'];
				unset($options['options']);
				$data=$this->getDatePicker($val,$options,$extra);
				break;
			case "COLOUR":
				$data=$this->getColour($val,$options);
				break;
			case "RATING":
				$data=$this->getRating($val, $options);
				break;
			case "CURRENCY": $size = 15; $class="right";
			case "PERC":
			case "PERCENTAGE": $size = !isset($size) ? 5 : $size;
			case "NUM": $size = !isset($size) ? 0 : $size;
				if(isset($options['symbol'])) {
					$symbol = $options['symbol'];
					$has_sym = true;
					unset($options['symbol']);
					$symbol_postfix = isset($options['symbol_postfix']) ? $options['symbol_postfix'] : false;
					unset($options['symbol_postfix']);
				} else {
					$has_sym = false;
				} //ASSIST_HELPER::arrPrint($options);
				if(isset($class)){ $options['class'] = (isset($options['class']) ? $options['class'] : "")." ".$class; }
                $data=$this->getNumInputText($val,$options,0,$size);
                if($type=="CURRENCY") {
					if(!$has_sym) {
						$data['display']="R&nbsp;".$data['display'];
					} elseif(strlen($symbol)>0) {
						if($symbol_postfix==true) {
							$data['display']=$data['display']."&nbsp;".$symbol;
						} else {
							$data['display']=$symbol."&nbsp;".$data['display'];
						}
					} else {
						//don't add a symbol
					}
                } elseif($type=="PERC" || $type=="PERCENTAGE") {
                      $data['display'].="&nbsp;%";
                }
                break;
			case "BOOL_BUTTON":
			case "BOOL":
				//echo "<br />cFF BB VAL:".$val." ARR: "; print_r($options);
				$data = $this->getBoolButton($val,$options);
				break;
			case "ATTACH"://echo "attach: ".$val;
				//$data = array('display'=>$val,'js'=>"");
				$data = $this->getAttachmentInput($val,$options);
				break;
			case "CALC" :
				$options['extra'] = explode("|",$options['extra']);
				$data = $this->getCalculationForm($val,$options);
				break;
			default:
				$data = array('display'=>$val,'js'=>"");
				break;
		}
		return $data;
	}


















    /**
	 * (ECHO) Function to display the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (ECHO) html to display log link
	 * @return (JS) javascript to process log link click
	 */
    public function drawPageFooter($left="",$log_table="",$var="",$log_id="") {
    	$data = $this->getPageFooter($left,$log_table,$var,$log_id);
		echo $data['display'];
		return $data['js'];
    }
    /**
	 * Function to create the activity log link onscreen
	 *
	 * @param (HTML) left = any html code to show on the left side of the screen
	 * @param (String) log_table = table to query for logs
	 * @param (QueryString) var = any additional variables to be passed to the log class
	 * @return (Array) 'display'=>HTML to display onscreen, 'js'=>javascript to process log link click
	 */
    public function getPageFooter($left="",$log_table="",$var="",$log_id="") {
    	$echo = "";
		$js = "";
    	if(is_array($var)) {
    		$x = $var;
			$d = array();
			unset($var);
			foreach($x as $f => $v) {
				$d[] = $f."=".$v;
			}
			$var = implode("&",$d);
    	}
		$echo = "
		<table width=100% class=tbl-subcontainer><tr>
			<td width=50%>".$left."</td>
			<td width=50%>".(strlen($log_table)>0 ? "<span id=".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log style=\"cursor: pointer;\" class=\"float color\" state=hide table='".$log_table."'>
				<img src=\"/pics/tri_down.gif\" id=log_pic style=\"vertical-align: middle; border-width: 0px;\"> <span id=log_txt style=\"text-decoration: underline;\">Display Activity Log</span>
			</span>" : "")."</td>
		</tr></table><div id=".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log></div>";
		if(strlen($log_table)>0){
			$js = "
			$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."disp_audit_log\").click(function() {
				var state = $(this).attr('state');
				if(state==\"show\"){
					$(this).find('img').prop('src','/pics/tri_down.gif');
					$(this).attr('state','hide');
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(\"\");
					$(this).find(\"#log_txt\").html('Display Activity Log');
				} else {
					$(this).find('img').prop('src','/pics/tri_up.gif');
					$(this).attr('state','show');
					var dta = '".$var."&log_table='+$(this).attr('table');
					var result = AssistHelper.doAjax('inc_controller.php?action=Log.Get',dta);
					//console.log(result);
					//alert(dta);
					$(\"#".(strlen($log_id)>0 ? $log_id."_" : "")."div_audit_log\").html(result[0]);
					$(this).find(\"#log_txt\").html('Hide Activity Log');
				}

			});
			";
		}
    	$data = array('display'=>$echo,'js'=>$js);
		return $data;
    }









	/***********************************************************************************************************************
	 * DETAILED OBJECT VIEWS
	 */




	/*****
	 * returns the details table for the object it is fed.
	 *
	 * @param (String) object_type = CONTRACT, DELIVERABLE, ACTION
	 * @param (INT) object_id = primary key
	 * @param (BOOL) include_parent_button = must the detailed view include an option to see the parent object details in a pop-up dialog
	 *
	 */
	public function getParentDetailedView($var) {
		$object_type = $var['object_type'];
		$child_type = $var['child_type'];
		$child_id = $var['child_id'];
		switch($object_type) {
			case "DELIVERABLE":
				$myObject = new MSCOA1_ACTION();
				break;
			case "CONTRACT":
				if($child_type=="ACTION") {
					$myObject = new MSCOA1_ACTION();
					$child_id = $myObject->getParentID($child_id);
				}
				$myObject = new MSCOA1_DELIVERABLE();
				break;
			case "FINANCE":
				$myObject = new MSCOA1_FINANCE();
				break;
		}
		$object_id = ($object_type == "FINANCE" ? $child_id : $myObject->getParentID($child_id));
		return $this->getDetailedView($object_type, $object_id,false,false,false,array(),false,false);
	}
	/**
	 * Draw the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 *
	 * @echo HTML
	 * @return JS
	 */
	public function drawDetailedView($object_type,$object_id,$include_parent_button=false,$sub_table=false,$view_all_button=false,$button=array(),$compact_view=false,$attachment_buttons="X") {
		if($attachment_buttons=="X") { $attachment_buttons = $this->default_attach_buttons; }
		$me = $this->getDetailedView($object_type, $object_id,$include_parent_button,$sub_table,$view_all_button,$button,$compact_view,$attachment_buttons);
		echo $me['display'];
		return $me['js'];
	}
	/**
	 * Generate the table view of one object
	 * @param object_type
	 * @param object_id
	 * @param include_parent_button=false
	 * @param sub_table=false
	 * @param view_all_button = false
	 * @param button = array()
	 * @param compact_view=false
	 *
	 * @return HTML
	 * @return JS
	 */
	public function getDetailedView($object_type,$object_id,$include_parent_button=false,$sub_table=false,$view_all_button=false,$button=array(),$compact_view=false,$attachment_buttons=true) {
		if(is_array($object_type)) {
			$var = $object_type;
			$object_id = $var['object_id'];
			$object_type = $var['object_type'];
		}
		//echo $object_type." :: ".$object_id;
		$js = "";
		$echo = "";
		//$echo = "abc :: ".$object_type." :: ".$object_id;
		//add code to get js from assist_module_display to trigger on attachment click for download
		if($attachment_buttons){
			$js.=$this->getAttachmentDownloadJS($object_type,$object_id);
		}
		$parent_buttons = "";
/*		switch(strtoupper($object_type)) {
			case "IDP":
				$myObject = new MSCOA1_ACTION();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getObjectName("deliverable")."' class='float btn_parent' id=DELIVERABLE /> <input type=button value='".$myObject->getContractObjectName()."' class='float btn_parent' id=CONTRACT />";
				}
				break;
			case "SUB-DELIVERABLE":
			case "DELIVERABLE":
				$myObject = new MSCOA1_DELIVERABLE();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getObjectName("contract")."' class='float btn_parent' id=CONTRACT />";
				}
				break;
			case "CONTRACT":
				$myObject = new MSCOA1_CONTRACT();
				if($include_parent_button) {
					$parent_buttons = "<input type=button value='".$myObject->getObjectName("FINANCE")."' class='float btn_parent' id=FINANCE />";
				}
				break;
			case "FINANCE":
				$myObject = new MSCOA1_FINANCE();
				break;
			case "TEMPCON":
				$myObject = new MSCOA1_TEMPLATE();
				break;
			default:
				$myObject = new MSCOA1();
				break;
		}
*/
$class_name = "MSCOA1_".$object_type;
		$myObject = new $class_name();

		if($view_all_button===true) {
			$parent_buttons.="<input class='float btn_view_all' type=button id='view_all' value='View All' />";
		}
		if($object_type=="FINANCE") {
			$result = $myObject->getDetailedObjectForDisplay($object_id);
			$js = $result['js'];
			$echo = $result['display'];
		} else {
			if($include_parent_button || $view_all_button) {
				$js.= "
						$('input:button.btn_view_all').button();
						$('input:button.btn_parent').button().click(function() {
							var my_window = AssistHelper.getWindowSize();
							var w = (my_window.width*".($object_type!="CONTRACT" ? "0.5" :"0.9").").toFixed(0);
							var h = (my_window.height*0.9).toFixed(0);
							var i = $(this).prop('id');
							var dta = 'child_type=".$object_type."&object_type='+i+'&child_id='+".$object_id."
							var x = AssistHelper.doAjax('inc_controller.php?action=Display.getParentDetailedView',dta);
							$('<div />',{html:x.display,title:x.title}).dialog({
								width: w,
								height: h,
								modal: true
							}).find('table.th2 th').addClass('th2');
						});";
			} //echo "cv: ".$compact_view.":";
			if($object_type != "TEMPCON"){
				$get_object_options = array('type'=>"DETAILS",'id'=>$object_id,'attachment_buttons'=>$attachment_buttons,'compact_view'=>($compact_view));
//				$object = $myObject->getObject(array('type'=>"DETAILS",'id'=>$object_id,'attachment_buttons'=>$attachment_buttons),($compact_view===true?array('page'=>"COMPACT"):array()));
				$object = $myObject->getObject($get_object_options);
				//ASSIST_HELPER::arrPrint($object);
			}else{
				$object = $myObject->getListObject($object_id, "CON");
			}

			$echo.="
				$parent_buttons
			<h2 class='".($sub_table !== false ? "sub_head":"")."'>".($object_type=="DELIVERABLE" && $sub_table ? "Sub-" : "").$myObject->getObjectName($object_type)." Details</h2>
					<table class='form ".($sub_table !== false ? "th2":"")."' width=100%>";
					foreach($object['head'] as $fld => $head) {
						if(isset($object['rows'][$fld])) {
							$val = $object['rows'][$fld];
							if(is_array($val)) {
								if(isset($val['display'])) {
									$js.=$val['js'];
									$val = $val['display'];
								} else {
									$v = "<table class=th2 width=100%>";
									foreach($val as $a) {
										foreach($a as $b => $c)
										$v.="<tr><th width=40%>".$b.":</th><td>".$c."</td></tr>";
									}
									$v.="</table>";
									$val = $v;
								}
							}
						} else { $val = $fld; }
						$echo.= "
						<tr>
							<th width=40%>".$myObject->replaceObjectNames($head['name']).":</th>
							<td ".($val==$fld ? "class=idelete" : "").">".$val."</td>
						</tr>";
					}
					//ASSIST_HELPER::arrPrint($button);
			if($button !== false && is_array($button) && count($button)>0){
				$echo.="
						<tr>
							<th width=40%></th>
							<td><input type=button value='".$button['value']."' class='".$button['class']."' ref=".$object_id." /></td>
						</tr>";
			}
			$echo.="</table>";
		}

		return array('display'=>$echo,'js'=>$js,'title'=>$myObject->getObjectName($object_type).($object_type!="FINANCE" ? " ".$myObject->getRefTag().$object_id : ""));
		//return array('display'=>$echo,'js'=>$js,'title'=>$myObject->getObjectName($object_type));
	}


















	/***********************************************************************************************************************
	 * LIST VIEWS
	 */




	/**
	 * Paging....
	 */
	public function drawPaging() {
		$me = $this->getPaging();
		echo $me['display'];
		return $me['js'];
	}
	public function getPaging($i,$options,$button,$object_type="",$object_options=array(),$add_button=array(false,"","")) {
		/**************
		$options = array(
			'totalrows'=> mysql_num_rows(),
			'totalpages'=> totalrows / pagelimit,
			'currentpage'=> start / pagelimit,
			'first'=> start==0 ? false : 0,
			'next'=> totalpages*pagelimit==start ? false : (start + pagelimit),
			'prev'=> start==0 ? false : (start - pagelimit),
			'last'=> start==totalpages*pagelimit ? false : (totalpages*pagelimit),
			'pagelimit'=>pagelimit,
		);
		 **********************************/
		$data = array('display'=>"",'js'=>"");
		$data['display'] = "
			<table width=100% style='margin-bottom: 5px;'>
				<tr>
					<td class='page_identifier center'><span id='$i'>
					<span style='float: left' id=spn_paging_buttons>
						<button class=firstbtn></button>
						<button class=prevbtn></button>
						Page <select class='page_picker'>";
			for($p=1;$p<=$options['totalpages'];$p++) {
				$data['display'].="<option ".($p==$options['currentpage'] ? "selected" : "")." value=$p>$p</option>";
			}
		$data['display'].="<option value=ALL>All</option></select> / ".$options['totalpages']."
						<button class=nextbtn></button>
						<button class=lastbtn></button>
					</span>";
		if(isset($add_button [0]) && $add_button[0]==true) {
			$ab_label = $this->replaceAllNames(isset($add_button[1]) && strlen($add_button[1])>0 ? $add_button[1] : "|add|");
			//$data['display'].="<span style='margin: 0 auto' id=spn_paging_add><button id=btn_paging_add class=abutton>".$ab_label."</button></span>";
			$data['display'].="<span style='float:right' id=spn_paging_add><button id=btn_paging_add class=abutton>".$ab_label."</button></span>";
			$data['js'].="
			$('#btn_paging_add').button({
				icons: {
					primary: \"ui-icon-circle-plus\"
				}
			}).click(function(e) {
				e.preventDefault();
				".$add_button[2]."
			}).removeClass(\"ui-state-default\").addClass(\"ui-button-state-ok\").children(\".ui-button-text\").css({\"padding-top\":\"0px\",\"padding-bottom\":\"0px\"})
			.hover(function() { $(this).removeClass(\"ui-button-state-ok\").addClass(\"ui-button-state-info\"); }, function() { $(this).removeClass(\"ui-button-state-info\").addClass(\"ui-button-state-ok\"); });
			";
		}
		$data['display'].="<!-- <span class=float id=spn_paging_search>
							Quick Search: <input type=text /> <input type=button value=Go disabled=true onclick='alert(\"whoops! still need to program the backend for this\")' />
						</span>
						<span style='float:right' class=idelete>STILL UNDER DEVELOPMENT</span> -->
					</span></td>
				</tr>
			</table>";
			$op = "options[set]=true";
			foreach($object_options as $key => $v) {
				$op.="&options[$key]=$v";
			}
		$data['js'].= "
		var paging_url = 'inc_controller.php?action=".$object_type.".GetListTableHTML';
		var paging_page_options = 'button[value]=".$button['value']."&button[class]=".$button['class']."&".$op."';
		var paging_parent_id = '$i';
		var paging_table_id = paging_parent_id+'_list_view';
		var paging_records = [];
		paging_records.totalrows = '".($options['totalrows'])."';
		paging_records.totalpages = '".($options['totalpages'])."';
		paging_records.currentpage = '".($options['currentpage'])."';
		paging_records.pagelimit = '".($options['pagelimit'])."';
		paging_records.first = '".($options['first']!== false ? $options['first'] : "false")."';
		paging_records.prev = '".($options['prev']!== false ? $options['prev'] : "false")."';
		paging_records.next = '".($options['next']!== false ? $options['next'] : "false")."';
		paging_records.last = '".($options['last']!== false ? $options['last'] : "false")."';

		$('#".$i."').paging({url:paging_url,page_options:paging_page_options,parent_id:paging_parent_id,table_id:paging_table_id,paging_options:paging_records});
		$('#".$i."').children('#spn_paging_buttons').find('button').click(function() {
			$('#".$i."').paging('buttonClick',$(this));
		});
		$('#".$i." select.page_picker').change(function() {
			$('#".$i."').paging('changePage',$(this).val());
		});
		";
		return $data;
	}


	/*****
	 * List View
	 */
	/**
	 * Function to draw the list table onscreen
	 * @param (Array) $child_objects = array of objects to display in table
	 * @param (Array) $button = details of button to display in last column
	 * @param (String) $object_type = the name of the object
	 * @param (Array) $object_options = getPaging options
	 * @param (Bool) $sub_table = is this table a child of another object true/false
	 * @param (Array) $add_button = array(0=>true/false, 1=>label if true)
	 */
	public function drawListTable($child_objects,$button=array(),$object_type="",$object_options=array(),$sub_table=false,$add_button=array(false,"","")) {
		//ASSIST_HELPER::arrPrint($child_objects);
		$me = $this->getListTable($child_objects,$button,$object_type,$object_options, $sub_table,$add_button);
		echo $me['display'];
		return $me['js'];
	}
	public function getListTable($child_objects,$button,$object_type="",$object_options=array(), $sub_table=false, $add_button = array(false,"","")) {
		$data = array('display'=>"<table class=tbl-container><tr><td>",'js'=>"");
		$page_id = (isset($button['pager']) && $button['pager'] !== false ? $button['pager'] : "paging_obj");
		$page_options = $child_objects['paging'];
		$display_paging = true;
		if($button===false) {
			$display_paging = false;
		} elseif(isset($button['pager']) && $button['pager']===false) {
			$display_paging = false;
		}
		if($display_paging){
			$paging = $this->getPaging($page_id,$page_options,$button,$object_type,$object_options,$add_button);
			$data['display'].=$paging['display'];
			$data['js'].=$paging['js'];
		}
		unset($button['pager']);
		$data['display'].="
			<table class='list ".($sub_table !== false ? "th2":"")."' id='".$page_id."_list_view' >
				<tr id='head_row'>
					";
					foreach($child_objects['head'] as $fld=>$head) {
						$data['display'].="<th>".$head['name']."</th>";
					}

				$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<th></th>" : "")."
				</tr>";
			$rows = $this->getListTableRows($child_objects, $button);
			$data['display'].=$rows['display']."</table>";
			$data['display'].="</td></tr></table>";
			$data['js'].=$rows['js']."

				//console.log($('#".$page_id."_list_view').css('width'));
			$(window).resize(function() {
				//console.log($('#".$page_id."_list_view').css('width'));
				//console.log($('.tbl-container:first').css('width'));

				var tbl_w = parseInt(AssistString.substr($('#".$page_id."_list_view').css('width'),0,-2));
				var btn_w = parseInt(AssistString.substr($('#".$page_id."_list_view tr:first th:last').css('width'),0,-2));
				var ref_w = parseInt(AssistString.substr($('#".$page_id."_list_view tr:first th:first').css('width'),0,-2));
				var con_w = parseInt(AssistString.substr($('.tbl-container:first').css('width'),0,-2));

				if(tbl_w < (con_w/2)) {
					tbl_w = Math.round(con_w/2);
					$('#".$page_id."_list_view').css('width',tbl_w+'px');
					$('#".$page_id."_list_view').css('margin','0 auto');
					$('#".$page_id."_list_view tr:first th:last').css('width',btn_w+'px');
					$('#".$page_id."_list_view tr:first th:first').css('width',ref_w+'px');
				}
				if(tbl_w < (con_w-30)) {
					con_w = tbl_w + 20;
					$('.tbl-container:first').css('width',con_w+'px');
				}

			});
			$(window).trigger('resize');
			";
		return $data;
	}
	public function getListTableRows($child_objects,$button) {
		$data = array('display'=>"",'js'=>"");
		if(count($child_objects['rows'])==0) {
			$data['display'] = "<tr><td colspan=".(count($child_objects['head'])+1).">No items found to display.</td></tr>";
		} else {
			foreach($child_objects['rows'] as $id => $obj) {
				$data['display'].="<tr>";
				foreach($child_objects['head'] as $fld=>$head) {
					$val = isset($obj[$fld]) ? $obj[$fld] : "";
					if(is_array($val)) {
						if(isset($val['display'])) {
							$data['display'].="<td>".$val['display']."</td>";
							$data['js'].=isset($val['js']) ? $val['js'] : "";
						} else {
							$data['display'].="<td class=idelete>".$fld."</td>";
						}
					} else {
						$data['display'].="<td>".$val."</td>";
					}
				}
				if(isset($button['type']) && $button['type']=="button") {
						$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><button ref=".$id." class='".$button['class']." ui_btn' />".$button['value']."</button></td>" : "");
					$data['js'].="
					$('button.ui_btn').button({
						icons: {primary: \"ui-icon-".(isset($button['icon']) ? $button['icon'] : "pencil")."\"},
					}).children(\".ui-button-text\").css({\"font-size\":\"80%\"}).click(function(e) {
						e.preventDefault();

					}); //.removeClass(\"ui-state-default\").addClass(\"ui-state-info\").css(\"color\",\"#fe9900\")
					";
				} else {
					$data['display'].=(($button !== false && is_array($button) && count($button)>0) ? "<td class=center><input type=button value='".$button['value']."' ref=".$id." class='".$button['class']."' /></td>" : "");
				}
				$data['display'].="</tr>";
			}
		}
		return $data;
	}















	/**
	 * Function generate the object form
	 * @param $form_object_type
	 * @param $formObject
	 * @param $form_object_id
	 * @param $parent_object_type
	 * @param $parentObject
	 * @param $parent_object_id
	 * @param $page_action
	 * @param $page_redirect_path
	 * @param $display_form = true (include form tag)
	 * @param $tbl_id = 0 (table id to differentiate between multiple forms on a single page)
	 */
	public function getObjectForm($form_object_type,$formObject,$form_object_id,$parent_object_type,$parentObject,$parent_object_id,$page_action,$page_redirect_path,$display_form=true,$tbl_id=0,$include_new_id_in_request=false) {
		//echo $formObject->getMyObjectType().":".$form_object_type;
		$is_segment_form=false;
		if(is_array($form_object_type)) {
			$var = $form_object_type;
			$form_object_type = $var['object_type'];
			$form_object_id = $var['object_id'];
			$page_action = $var['page_action'];
			$page_redirect_path = $var['page_redirect_path'];
			$parent_object_id = $var['parent_id'];
			switch($form_object_type) {
				case "SEGMENT": $is_segment_form = true;
				case "FUNCTION":
					$parent_object_type = null;
					$formObject = new MSCOA1_SEGMENTS($form_object_type);
					$parentObject = null;
					break;
			}
		}
		//echo "|".$formObject->getMyObjectName();
		if(stripos($page_action,".")!==false) {
			$pa = explode(".",$page_action);
			$page_action = $pa[1];
			$page_section = strtoupper($pa[0]);
		} else {
			$page_section = "MANAGE";
		}

		if($include_new_id_in_request!==false) {
			$new_id_in_request_key = $include_new_id_in_request;
			$include_new_id_in_request = true;
		} else {
			$new_id_in_request_key = "";
		}


		$headingObject = new MSCOA1_HEADINGS();

		$th_class = "";
		$tbl_class = "";

		$attachment_form = false;

		$data = array('display'=>"",'js'=>"");
		$js = $formObject->getExtraObjectFormJS();
		$echo = "";
		if($page_action=="UPDATE") {
			$headings = $headingObject->replaceObjectNames($headingObject->getUpdateObjectHeadings($form_object_type,"FORM"));
		} else {
			if($parent_object_type=="SEGMENT") {
				$is_segment_form = true;
				$head_object_type = $parent_object_type;
			} elseif(stripos($form_object_type,"ASSURANCE")===false) {
				$head_object_type = $form_object_type;
			} else {
				$head_object_type = "ASSURANCE";
			}
			$fld_prefix = $formObject->getTableField()."_";
			$headings = $headingObject->replaceObjectNames($headingObject->getMainObjectHeadings($head_object_type,"FORM",((strrpos(strtoupper($page_action),"ADD")!==FALSE)?"NEW":""),$fld_prefix));
		}
		//ASSIST_HELPER::arrPrint($headings);
		//ASSIST_HELPER::arrPrint($headingObject->getHeadingsForLog());
		if($is_segment_form) {
			$pa = "SEGMENTS.".$page_action;
			$page_action_for_delete = "SEGMENTS.DEACTIVATE";
		} else {
			$pa = ucwords($form_object_type).".".$page_action;
			$page_action_for_delete = ucwords($form_object_type).".DELETE";
		}

		$pd = $page_redirect_path;

		$is_view_page = (strrpos(strtoupper($page_action),"VIEW")!==FALSE);
		$is_edit_page = (strrpos(strtoupper($page_action),"EDIT")!==FALSE) || (strrpos(strtoupper($page_action),"COPY")!==FALSE);
		$is_update_page = (strrpos(strtoupper($page_action),"UPDATE")!==FALSE);
		$is_add_page = (strrpos(strtoupper($page_action),"ADD")!==FALSE);
		$is_copy_page = (strrpos(strtoupper($page_action),"COPY")!==FALSE);

		$copy_protected_fields = array();
		$copy_protected_heading_types = array();

		if($is_edit_page || $is_view_page) {
			$form_object = $formObject->getRawObject($form_object_id);
			$form_activity = "EDIT";
			if($is_copy_page) {
				$copy_protected_fields = $formObject->getCopyProtectedFields();
				$copy_protected_heading_types = $formObject->getCopyProtectedHeadingTypes();
				$form_activity = "COPY";
				$form_object['parent_id'] = $parent_object_id;
				$form_object['line_version'] = ASSIST_HELPER::JOIN_FOR_MULTI_LISTS.$_REQUEST['new_version'].ASSIST_HELPER::JOIN_FOR_MULTI_LISTS;
			}
		} elseif($is_update_page) {
			$form_object = $formObject->getRawUpdateObject($form_object_id);
			$form_activity = "UPDATE";
		} elseif($is_add_page) {
//			unset($headings['rows']['parent_id']);
		} else {
			$form_object = array();
			$form_activity = "NEW";
		}
		//ASSIST_HELPER::arrPrint($form_object);
		$form_name = "frm_object_".$form_object_type."_".$form_object_id."_".strtolower(str_replace(".","",$page_action));
//echo ":form_name:".$form_name.":";
		$js.="  //console.log('".$form_name."');
				var ".$form_name."_page_action = '".$pa."';
				var ".$form_name."_page_action_for_delete = '".$page_action_for_delete."';
				var ".$form_name."_page_direct = '".$pd."';

		".$this->getAttachmentDownloadJS($form_object_type, $form_object_id,$form_activity);

		$echo.="
			<div id=div_error class=div_frm_error>

			</div>
			".($display_form ? "<form name=".$form_name." method=post language=jscript enctype=\"multipart/form-data\">" : "")."
				<table class='form $tbl_class' width=100% id=tbl_object_form_".$tbl_id.">";
			$form_valid8 = true;
			$form_error = array();
			$parent_field_id = 0;
			foreach($headings['rows'] as $fld => $head) {
							//echo "<P>".$fld;ASSIST_HELPER::arrPrint($head);
						$is_parent_field = false;
				if($head['parent_id']==0) {
					$val = "";
					$h_type = $head['type'];
					if($h_type!="HEADING" && !in_array($h_type,$copy_protected_heading_types) && !in_array($fld,$copy_protected_fields)) {
						$display_me = true;
						$display_my_row = false;
						$is_version_field = ($fld=="line_version"?true:false);
						$options = array('id'=>$fld,'name'=>$fld,'req'=>$head['required']);
						if($head['required']==1) { $options['title'] = "This is a required field."; }
						if($h_type=="LIST" || $h_type=="MULTILIST") {
							if($display_me) {
								$list_items = array();
								$listObject = new MSCOA1_LIST($head['list_table']);
								//if($is_view_page) {
									//$list_items = $listObject->getAllListItemsFormattedForSelect();
								//} else {
									$list_items = $listObject->getActiveListItemsFormattedForSelect();
								//}
								if(isset($list_items['list_num'])) {
									$list_parent_association = $list_items['list_num'];
									$list_items = $list_items['options'];
									$options['list_num'] = $list_parent_association;
								}
								$options['options'] = $list_items;
								if(count($list_items)==0 && $head['required']==1) {
									$form_valid8 = false;
									$form_error[] = "The".$head['name']." list has not been populated but is a required field.";
									$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
								}

								if($h_type=="MULTILIST") {
									$options['name'].="[]";
									if($is_version_field && $parent_field_id>0 && !$is_copy_page) {
										$parent_val = $formObject->getParentVersions($parent_field_id);
										foreach($list_items as $i => $l) {
											if(!in_array($i,$parent_val)) {
												unset($options['options'][$i]);
											}
										}
									}
									if($is_edit_page || $is_update_page || $is_view_page || $is_copy_page) {
										$val = isset($form_object[$fld]) ? $form_object[$fld] : array();
										if($is_version_field) {
											$raw_version_id = $val;
										}
									} elseif($is_version_field) {	//not sure if this if is needed after changing how copy page works - might be related to previous variant?  Leaving it in for now just in case [JC] 22 March AA-347
										$val = $parent_val;
									} else {
										$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : array();
									}
									if(!is_array($val)) {
										$val = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$val);
										$val = ASSIST_HELPER::removeBlanksFromArray($val);
										}
									if(($is_view_page || $is_copy_page) && is_array($val) && count($val)>0) {
										foreach($val as $key => $x) {
											if($formObject->checkIntRef($x) && isset($options['options'][$x])) {
												$val[$key] = $options['options'][$x];
											} else {
												unset($val[$key]);
											}
										}
									} else {

									}
								} else {
									if($is_edit_page || $is_update_page) {
										$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "X";
									} elseif($is_view_page || $is_copy_page) {
										$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : $formObject->getUnspecified();
										if($formObject->checkIntRef($val)) {
											$val = isset($options['options'][$val]) ? $options['options'][$val] : $formObject->getUnspecified();
										}
									} else {
										$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
									}
								}
								unset($listObject);
							}
						} elseif(in_array($h_type,array("MASTER","USER"))) {
							$list_items = array();
							switch($h_type) {
								case "USER":
									$listObject = new MSCOA1_USERACCESS();
									$list_items = $listObject->getActiveUsersFormattedForSelect();
									break;
								case "MASTER":
									$listObject = new MSCOA1_MASTER($head['list_table']);
									$list_items = $listObject->getActiveItemsFormattedForSelect();
									break;
								default:
									echo $h_type;
									break;
							}
							$options['options'] = $list_items;
							$h_type = "LIST";
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
							} elseif($is_view_page || $is_copy_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : $formObject->getUnspecified();
								if($formObject->checkIntRef($val)) {
									$val = isset($options['options'][$val]) ? $options['options'][$val] : $formObject->getUnspecified();
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}
						} elseif(in_array($h_type,array("OBJECT","MULTIOBJECT"))) {
							$list_items = array();
							$list_object_type = $head['list_table'];
							$listObject = new $list_object_type(); //echo $list_object_type;
//							$list_items = $listObject->getLimitedActiveObjectsFormattedForSelect(array($parent_object_type=>$parent_object_id));
							if($list_object_type=="MSCOA1_COMPETENCY") {
								$result4Object = $listObject;
								$result4_objects = $result4Object->getOrderedObjects($parent_object_id);

								$os = array();
								foreach($result4_objects as $result4_id => $result) {
									$os[$result4_id] = array('value'=>$result['name']);
									$list_items[$result4_id] = $result['name'];
								}
								$options['options'] = $os;
								$options['class'] = strtolower($h_type."_".$fld);
								$options['name'].= "[]";
							} else {
								$options['options'] = $listObject->getLimitedActiveObjectsFormattedForSelect(array('FUNCTION'=>$parent_object_id));
							}

							//$options['options'] = $list_items;
							/*foreach($list_items as $i => $d) {
								$options['options'][$i] = array('value'=>$d);
							}*/
							$h_type = "LIST";
							if(count($list_items)==0 && $head['required']==1) {
								$form_valid8 = false;
								$form_error[] = "The ".$head['name']." list has not been populated but is a required field.";
								$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
							}
							if($is_edit_page || $is_update_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && ($form_object[$fld]>0 || !is_numeric($form_object[$fld])) ? $form_object[$fld] : "X";
							} elseif($is_view_page || $is_copy_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : $formObject->getUnspecified();
								if($formObject->checkIntRef($val)) {
									$val = isset($options['options'][$val]) ? $options['options'][$val] : $formObject->getUnspecified();
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "X";
							}






						} elseif($h_type=="SEGMENT") {
							if($is_copy_page) {
								$val = $parent_object_id;
							} else {
								$val = isset($form_object[$fld]) ? $form_object[$fld] : $parent_object_id;
									}
							if(strpos($val, "_") !== false) {
								$x = explode("_", $val);
								$val = (int)$x[0];
							}
							if($val>0) {
								//if copy field then use REQUEST else use raw object info
								$version = isset($_REQUEST['version']) ? $_REQUEST['version'] : $form_object['line_version'];
								$h_type = "AUTO";
								$options = array('id' => $fld, 'name' => $fld, 'req' => $head['required']);
								$options['alternative_display'] = true;
								$extra_info['include_nickname'] = true;
								$extra_info['include_guid'] = true;
								$extra_info['version'] = $version;
								$extra_info['can_have_children'] = true;
								if($is_copy_page) {
									$list_items = array($val=>$formObject->getParentName($val));
								} elseif($is_view_page || $is_add_page || $is_edit_page) {
									$list_items = $formObject->getAllListItemsFormattedForSelect($extra_info);
							} else {
									$list_items = $formObject->getActiveListItemsFormattedForSelect($extra_info);
								}
								$options['options'] = $list_items;
								$val_id = $val;
								$parent_field_id = $val;
								$val = $list_items[$val];
							} else {
								$val_id = 0;
								$val = "N/A [Top Level]";
							}
//							echo "<hr /><h1 class='green'>Helllloooo from ".$_SERVER['PHP_SELF']." in ".__CLASS__."=>".__FUNCTION__." @ ".__LINE__."</h1><hr />";
//echo "<h2>".$h_type." => ".$fld." => ".$val_id." => ".$parent_object_id." => ".$formObject->getMyObjectType()." => ".$val."</h2>";
//ASSIST_HELPER::arrPrint($list_items);
							//if view page or add page then display text & not allow changes
							if($is_add_page || $is_edit_page) {
								$is_parent_field = true;
							}


						} elseif($h_type=="DATE") {
							$options['options'] = array();
							$options['class'] = "";
							if($is_edit_page || $is_update_page || $is_view_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 && strtotime($form_object[$fld])>0 ? date("d-M-Y",strtotime($form_object[$fld])) : "";
								if($is_update_page) {
									$options['options']['maxDate'] = "'+0D'";
								}
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="CURRENCY" || $h_type=="PERC") {
							$options['extra'] = "processCurrency";
							if($is_edit_page || $is_update_page || $is_view_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "";
							}
						} elseif($h_type=="BOOL"){
							$h_type = "BOOL_BUTTON";
							$options['yes'] = "1";
							$options['no'] = "0";
							$options['extra'] = "boolButtonClickExtra";
							$options['small_size'] = true;
							if($is_edit_page || $is_update_page || $is_view_page) {
								$val = isset($form_object[$fld]) && strlen($form_object[$fld])>0 && $form_object[$fld]>0 ? $form_object[$fld] : "0";
							} else {
								$val = isset($head['default_value']) && strlen($head['default_value'])>0 ? $head['default_value'] : "1";
							}
						} elseif($h_type=="REF") {
							if($page_action=="Add" || $is_add_page || $is_copy_page) {
								$val = "System Generated";
							} else {
								$val = $formObject->getRefTag().$form_object_id;
							}
						} elseif($h_type=="ATTACH") {
							$attachment_form = true;
							$options['action'] = $pa;
							$options['page_direct'] = $pd;
							$options['can_edit'] = ($is_edit_page || $is_update_page);
							$options['object_type'] = $form_object_type;
							$options['object_id'] = $form_object_id;
							$options['page_activity'] = $form_activity;
							$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
							if(substr($val,0,2)!="a:") { $val = base64_decode($val); }
						} else {
							if($is_edit_page || $is_update_page || $is_view_page) {
								$val = isset($form_object[$fld]) ? $form_object[$fld] : "";
							} else {
								$val = isset($head['default_value']) ? $head['default_value'] : "";
							}
						}

						if($display_me) {
							if($is_view_page || $is_parent_field || ($is_copy_page && $is_version_field)) {
								$display = $this->getDataField($h_type, $val);
								if($is_add_page || $is_copy_page) {
									$vl = $is_parent_field ? $val_id : $raw_version_id;
									$display['display'].="<input type=hidden name='".$fld."' id='".$fld."' value='".$vl."' />";
								} elseif($is_edit_page) {
									//TODO Function to change parent line item via the manage_edit_move.php
//									$display['display'].="<button id='btn_move' object_id='".$form_object_id."'>Move</button>";
								}
							} else {
								if($h_type=="TEXT") {
									$options['cols'] = 80;
									$options['rows'] = 6;
								} elseif($h_type=="LRGVC") {
									$options['cols'] = 80;
									$options['rows'] = 3;
								}
								$display = $this->createFormField($h_type,$options,$val);
								if($is_version_field && $is_edit_page) {
									$display['display'].="<div class='i'>Note: <ul><li>Versions are limited to those associated with the parent line item;</li><li>Changing the Version here will automatically update all associated child line items regardless of what their versions are set to.</li></ul></div>";
								}
							}
							$js.=$display['js'];
							if($formObject->hasTarget()==true && !$is_edit_page && isset($form_object[$formObject->getTargetUnitFieldName()])) {
								if($fld==$formObject->getTargetFieldName() || $fld==$formObject->getActualFieldName()) {
									$display['display'].=" ".$form_object[$formObject->getTargetUnitFieldName()];
								}
							}

						}
					} elseif(in_array($fld,$copy_protected_fields) || in_array($h_type,$copy_protected_heading_types)) {
						$val = "";
						if($h_type=="ATTACH") {
							$val = "ATTACH";
						} else {
							$val = isset($form_object[$fld]) ? $form_object[$fld] : $fld;

							if($headingObject->isListField($head['type']) && $head['section']!="DELIVERABLE_ASSESS" && !in_array($head['type'],array("DEL_TYPE","DELIVERABLE"))) {
								$val = ( (!isset($form_object[$head['list_table']]) || is_null($form_object[$head['list_table']])) ? $this->getUnspecified() : $form_object[$head['list_table']]);
							} else {
								$v = $this->getDataField($head['type'], $val);
								$val = $v['display'];
							}
						}
						$display = array('display'=>$val);
					} else {
						if($is_edit_page || $is_update_page) {
							$pval = isset($form_object[$fld]) ? $form_object[$fld] : "";
						} else {
							$pval = isset($head['default_value']) ? $head['default_value'] : "";
						}
						$sub_head = $headings['sub'][$head['id']];
						if(isset($form_object[$fld]) && is_array($form_object[$fld])) {
							$sub_form_object = $form_object[$fld];
						} else {
							$sub_form_object = isset($form_object[$fld]) ? array($form_object[$fld]) : array();
						}
						if(count($sub_form_object)==0) {
							foreach($sub_head as $shead) {
								$sub_form_object[0][$shead['field']] = "";
							}
						}
						$td = "
						<div class=".$fld."><span class=spn_".$fld.">";
						$add_another[$fld] = false;
						foreach($sub_form_object as $sfo) {
							$td.="<span>";
							$td.="<table class=sub_form width=100%>";
							foreach($sub_head as $shead) {
								$sh_type = $shead['type'];
								$sfld = $shead['field'];
								$options = array('id'=>$sfld,'name'=>$sfld,'req'=>$head['required']);
								$val = "";
								if($sh_type=="LIST") {
									$list_items = array();
									$listObject = new MSCOA1_LIST($shead['list_table']);
									$list_items = $listObject->getActiveListItemsFormattedForSelect();
									$options['options'] = $list_items;
									if(count($list_items)==0 && $shead['required']==1) {
										$form_valid8 = false;
										$form_error[] = "The ".$shead['name']." list has not been populated but is a required field.";
										$options['class'] = (isset($options['class']) ? $options['class']." " : "")."required";
									}
									if(count($list_items)>1) {
										$add_another[$fld] = true;
									}
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : 0;
									}
									unset($listObject);
								} elseif($sh_type=="BOOL"){
									$sh_type = "BOOL_BUTTON";
									$options['yes'] = 1;
									$options['no'] = 0;
									$options['extra'] = "boolButtonClickExtra";
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									}
									if(strlen($val)==0) {
										$val = (isset($shead['default_value']) && strlen($shead['default_value'])>0) ? $shead['default_value'] : 0;
									}
								} elseif($sh_type=="CURRENCY") {
									$options['extra'] = "processCurrency";
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
									}
								} else {
									if($is_edit_page || $is_update_page) {
										$val = isset($sfo[$sfld]) ? $sfo[$sfld] : "";
									} else {
										$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
									}
								}
								$sdisplay = $this->createFormField($sh_type,$options,$val);
								$js.= $sdisplay['js'];
								$td.="
								<tr ".(strlen($shead['parent_link'])>0 ? "class='tr_".$shead['parent_link']."'" : "").">
									<th width=40% class=th2>".$shead['name'].":".((!$is_view_page && $shead['required']==1)?"*":"")."</th>
									<td>".$sdisplay['display']."</td>
								</tr>";
							}
						$td.= "
							</table></span>
							";
						}
						$td.="
							</span>
							".($add_another[$fld] ? "<p><input type=button value='Add Another' id=btn_".$fld." /></p>" : "")."
						</div>";
						$td_blank = "";
						if($add_another[$fld]) {
											$td_blank="<div id=".$fld."_blank><span>";
											$td_blank.="<table class=sub_form width=100%>";
											foreach($sub_head as $shead) {
												$sh_type = $shead['type'];
												$sfld = $shead['field'];
												if($fld=="contract_supplier") {
													$options = array('name'=>$sfld."[]",'req'=>$head['required']);
												} else {
													$options = array('id'=>$sfld,'name'=>$sfld,'req'=>$head['required']);
												}
												$val = "";
												if($sh_type=="LIST") {
													$list_items = array();
													$listObject = new MSCOA1_LIST($shead['list_table']);
													$list_items = $listObject->getActiveListItemsFormattedForSelect();
													$options['options'] = $list_items;
													$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : 0;
													unset($listObject);
												} elseif($sh_type=="BOOL"){
													$sh_type = "BOOL_BUTTON";
													$options['yes'] = 1;
													$options['no'] = 0;
													$options['extra'] = "boolButtonClickExtra";
													$val = (isset($shead['default_value']) && strlen($shead['default_value'])>0) ? $shead['default_value'] : 0;
												} elseif($sh_type=="CURRENCY") {
													$options['extra'] = "processCurrency";
													$val = isset($shead['default_value']) && strlen($shead['default_value'])>0 ? $shead['default_value'] : "";
												}
												$sdisplay = $this->createFormField($sh_type,$options,$val);
												$td_blank.="
												<tr ".(strlen($shead['parent_link'])>0 ? "class=\"tr_".$shead['parent_link']."\"" : "").">
													<th width=40% class=th2>".$shead['name'].":".((!$is_view_page && $shead['required']==1)?"*":"")."</th>
													<td>".$sdisplay['display']."</td>
												</tr>";
											}
										$td_blank.= "
											</table></span>
										</div>
											";
						}
						$display = array('display'=>$td.$td_blank);
					}
					if($display_me || $display_my_row) {
						$echo.= "
						<tr id=tr_".$fld." ".(strlen($head['parent_link'])>0 ? "class='tr_".$head['parent_link']."'" : "").">
							<th class='".$th_class."' ".($form_object_type!="ACTIVITY"?"width=40%":"").">".$head['name'].":".((!$is_view_page && !$is_parent_field && $head['required']==1)?"*":"")."</th>
							<td>".$display['display']."
							</td>
						</tr>";
					}
				}
			}
			if(!$form_valid8) {
				$js.="
				$('#div_error').html(error_html).dialog({modal: true,dialogClass:\"dlg_frm_error\"}).addClass('ui-corner-all').css({\"border\":\"2px solid #cc0001\",\"background-color\":\"#fefefe\"});
				AssistHelper.hideDialogTitlebar('id','div_error');
				AssistHelper.hideDialogCSS('class','dlg_frm_error');
				$('#div_error #btn_error').button().click(function() {
					$('#div_error').dialog(\"close\");
				}).blur().parent('p').addClass('float');
				$('html, body').animate({scrollTop:0});
				";
			}
				if(!$is_view_page) {
					$echo.="
					<tr>
						<th></th>
						<td>
						".($is_edit_page && !$is_copy_page ? "<button class='float delete-btn' id=del_btn>".$formObject->replaceActivityNames("|deactivate|")."</button>" : "")."
							<button id=btn_save class='save-btn'>".$formObject->replaceActivityNames("|save|")."</button>
							".(($is_segment_form) ? "
							<input type=hidden name=idp_id value='0' />
							<input type=hidden name=segment_section value='".$form_object_type."' />
							" : "")."
							<input type=hidden name=object_id value=".$form_object_id." />
							".((!in_array($form_object_type,array("FUNCTION","COMPETENCY")) && strrpos(strtoupper($page_action), "ADD")!==FALSE) ? "<input type=hidden name=".$formObject->getParentFieldName()." value=\"".$parent_object_id."\" />" : "")."
						</td>
					</tr>";
				}
			$echo.="
			</table>
		".($display_form ? "</form>" : "");
		$data['js'].="
				var error_html = \"<p>The following errors need to be attended to before any ".$formObject->getObjectName($formObject->getMyObjectName())."s can be added:</p> <ul><li>".implode('</li><li>',$form_error)."</li></ul><p>Where required lists have not been populated, please contact your Module Administrator.</p><p><input type=button value=OK class=isubmit id=btn_error /></p>\"


				".$js."

				";

		if($formObject->hasDeadline() && !is_null($parentObject)) {
			$parent_deadline = $parentObject->getDeadlineDate();
			$now = strtotime(date("Y-m-d"));
			$then  = strtotime($parent_deadline);
			$diff = ($then-$now)/(3600*24);
			$data['js'].=" $('#".$formObject->getDeadlineField()."').datepicker(\"option\",\"maxDate\",\"+".$diff."D\");";
		}
		$data['js'].="

				$('#btn_move').button({icons:{primary:\"ui-icon-arrowreturn-1-e\"}})
				.removeClass('ui-state-default').addClass('ui-button-minor-grey').addClass('float')
				.hover(
					function() {
						$(this).removeClass('ui-button-minor-grey').addClass('ui-button-minor-orange');
					},function() {
						$(this).removeClass('ui-button-minor-orange').addClass('ui-button-minor-grey');
					}
				).click(function(e) {
					e.preventDefault();
					var obj_id = ".$form_object_id.";
					var segment = '".$form_object_type."';
					var url = 'manage_edit_move.php?object_type='+segment+'&object_id='+obj_id;
					alert(url);
					document.location.href = url;
				});


				$(\"form[name=".$form_name."] select\").each(function() {
					$(this).trigger('change');

					//if select only has 1 option + unspecified then auto select the second option
					if($(this).children(\"option\").length==2) {
						if($(this).children(\"option:first\").val()==\"X\") {
							$(this).children(\"option:last\").prop(\"selected\",true);
						}
					}
				});

				$(\"form[name=".$form_name."] button.delete-btn\").button({
					icons:{primary:\"ui-icon-trash\"}
				}).removeClass('ui-state-default').addClass('ui-button-minor-red')
				.hover(
					function() {
						$(this).removeClass('ui-button-minor-red').addClass('ui-button-minor-orange');
					},function() {
						$(this).removeClass('ui-button-minor-orange').addClass('ui-button-minor-red');
					}
				).click(function() {
					$"."form = $(\"form[name=".$form_name."]\");
					if(confirm('Are you sure you wish to ".strtolower($formObject->replaceActivityNames("|deactivate|"))." this item?')) {
						MSCOA1Helper.processObjectForm($"."form,".$form_name."_page_action_for_delete,".$form_name."_page_direct);
					}
				});
				$(\"form[name=".$form_name."] button.save-btn\").button({
					icons:{primary:\"ui-icon-disk\"}
				}).removeClass('ui-state-default').addClass('ui-button-state-green')
				.hover(
					function() {
						$(this).removeClass('ui-button-state-green').addClass('ui-button-state-orange');
					},function() {
						$(this).removeClass('ui-button-state-orange').addClass('ui-button-state-green');
					}
				).click(function(e) {
					e.preventDefault();
					$"."form = $(\"form[name=".$form_name."]\");
					";
			$data['js'].="
					MSCOA1Helper.processObjectForm($"."form,".$form_name."_page_action,".$form_name."_page_direct,".($include_new_id_in_request?"true":"false").",'".($include_new_id_in_request?$new_id_in_request_key:"")."');
					";
		$data['js'].="
					return false;
				});

				function boolButtonClickExtra($"."btn) {
					var i = $"."btn.prop(\"id\");
					var us = i.lastIndexOf(\"_\");
					var tr = \"tr_\"+i.substr(0,us);
					var act = i.substr(us+1,3);
					if(act==\"yes\") {
						$(\"tr.\"+tr).show();
					} else {
						$(\"tr.\"+tr).hide();
					}
				}

				function processCurrency($"."inpt) {
					var h = $"."inpt.parents(\"tr\").children(\"th\").html();
					h = h.substr(0,h.lastIndexOf(\":\"));
					alert(\"Only numbers (0-9) and a period (.) are permitted in the \"+h+\" field.\");
				}
				";
				if(isset($add_another) && count($add_another)>0) {
					foreach($add_another as $key => $aa) {
						if($aa==true) {
							$data['js'].= "
							var ".$key." = $('#".$key."_blank').html();
							$('#".$key."_blank').hide();
							$('#btn_".$key."').click(function() {
								$('div.".$key." span').children('span:last').after(".$key.");
							});
							";
						}
					}
				}

				$data['js'].="

				$(\"button\").each(function() {
						if($(this).attr(\"button_status\")==\"active\") {
							$(this).trigger(\"click\");
						}
				});



			";


		$data['display'] = $echo;
		return $data;




	}





















	/*******************
	 * JS code for handling forms displayed inside iframe inside dialogs
	 *
	 * @param (String) object_type
	 * @param (Array) additional variables
	 * @return (String) JQuery
	 */
	public function getIframeDialogJS($object_type,$parent_dlg_id,$var) {
		$echo = "";

		$echo.= "
		var dlg_size_buffer = 50;
		var ifr_size_buffer = 50;
		window.parent.$('#".$parent_dlg_id."').dialog('open');
		";/*
		$echo.= "
		var my_height = $('table.tbl-container').css('height');
		if(AssistString.stripos(my_height,'px')>0) {
			my_height = parseInt(AssistString.substr(my_height,0,-2))+ifr_size_buffer;
			window.parent.$('#".$parent_dlg_id."').find('iframe').prop('height',(my_height)+'px');
			var dlg_height = window.parent.$('#".$parent_dlg_id."').dialog('option','height');
			var test_height = my_height+dlg_size_buffer;
			if(dlg_height > test_height) {
				window.parent.$('#".$parent_dlg_id."').dialog('option','height',(test_height));
				var check = !(AssistHelper.hasScrollbar(window.parent.$('#".$parent_dlg_id."')));
				while(!check) {
					window.parent.$('#".$parent_dlg_id."').dialog('option','height',(test_height));
					test_height+=dlg_size_buffer;
					if(!(dlg_height > test_height) || !(AssistHelper.hasScrollbar(window.parent.$('#".$parent_dlg_id."')))) {
						check = true;
					}
				}
			}
		}
		";*/
		return $echo;
	}
















































































	/**
	 * Returns filter for selecting parent objects according to section it is fed
	 *
	 * @param *(String) section = the current object whose parents need to be found
	 * @param (Array) options = an array of options to be displayed in the filter
	 * @return (String) echo
	 */
	public function getFilter($section, $array = array()) {
        $data = array('display'=>"",'js'=>"");
		$data['display']="
			<div id='filter'>";

		switch (strtoupper($section)) {
			case 'CONTRACT':
				$data['display'].="
							<table>
								<tr>
									<th>Select a financial year</th>
										<form id='filterform'>
											<td>
											<select id='yearpicker'>";
				foreach($array as $index=>$key){
					$data['display'].="<option start='".$key['start_date']."' end='".$key['end_date']."' value=".$key['id'].">".$key['value']."</option>";
				}
				$data['js']="
					fyear = 0;

					$('#yearpicker').change(function(){
						//window.fin_year = ($(this).prop('value'));
						var fyear = $(this).prop('value');
						refreshContract(fyear);
					});
				";
				break;

			case 'DELIVERABLE':
				$dsp = "contracts get";

				break;

			case 'ACTION':
				$dsp = "deliverables get";

				break;

			default:
				$dsp = "Invalid arguments supplied";
				break;
		}

		$data['display'].="
							</select>
						</td>
					</form>
				</table>
			</div>";

        return $data;
    }


	/**
	 * Returns list table for the object it is fed - still in early phase, only works for CONTRACTS for now -> doesn't return the javascript
	 *
	 * @param *(String) section = the current object whose list table you want to draw
	 * @param (Array) fyears = an array of financial years taken from MSCOA1_MASTER->getActiveItems();
	 * @param (Array) headings = an array of the headings to use for the top row of the table, taken from MSCOA1_HEADINGS->getMainObjectHeadings(eg "CONTRACT", "LIST", "NEW");
	 * @return (Array) array with ['display'] containing the html to be echo'd, and ['js'] to be put into the script (coming soon...)
	 */
    public function getListView($section, $fyears=array(), $headings=array()){
    	$data = array('display'=>"",'js'=>"");
			switch(strtoupper($section)){
				case "CONTRACT":
					$filter = $this->getFilter($section, $fyears);
					//For the financial year filter
					$data['display'] = $filter['display'];
					//For the list view table
					$data['display'].=
									'<table class=tbl-container><tr><td>
										<table class=list id=master_list>
											<tbody>
												<tr>
													<td colspan=42>
														<div id="page_selector">
															<input type="image" src="../pics/tri_left.gif" id=page_first />
															<input type="image" value="back" id=page_back />
															<select id="pages_list">
																<option value=1>1</option>
															</select>
															<input type="image" value="next" id=page_next />
															<input type="image" src="../pics/tri_right.gif" id=page_last />
															<input type="image" src="../pics/tri_down.gif" id=show_all />
														</div>
													</td>
												</tr>
												<tr id=head_list>';

												foreach($headings as $key=>$val){
													$string = "<th id='".$val['field']."' >".$val['name']."</th>";
													$data['display'].= $string;
												}
					$data['display'].=
													"<th class='last_head'></th>
												</tr>
												<tr id=hidden_row style=\"visibility: hidden\">
												</tr>
											</tbody>
										</table>
									</td></tr></table>";
					//For the javascript

					break;
				default:
					$data['display'] = "Lol, not for you";
					break;
			}

		return $data;
    }



	public function drawTreeTier($object_type,$level,$obj,$page_action,$extra_btn_class="",$can_i_view=true,$can_i_edit=true,$text="") {
		switch($page_action) {
			case "CONFIRM":
			case "VIEW":
				$this->drawTreeTierForView($object_type, $level, $obj,$extra_btn_class, $can_i_view,$text);
				break;
			default:
				$this->drawTreeTierForEdit($object_type, $level, $obj,$extra_btn_class, $can_i_view,$can_i_edit);
				break;
		}
	}

	public function drawTreeTierForEdit($object_type,$level,$obj,$extra_btn_class="",$can_i_view=true,$can_i_edit=true) {
		$placeholder_td = "";
		$pos_class = "";
		$icon_class = "";
		$has_btn = true;
		$btn_class = "expand-btn";
		$tr_class = "";
		switch($level) {
			case 1:
				$pos_class = "uni-pos";
				break;
			case 2:
				$pos_class = "bi-pos";
				$icon_class = "uni-icon";
				$placeholder_td = "";
				break;
			case 3:
				$pos_class = "tri-pos";
				$icon_class = "bi-icon";
				$placeholder_td = "<td></td>";
				break;
			case 4:
				$pos_class = "quad-pos";
				$icon_class = "tri-icon";
				$placeholder_td = "<td></td><td></td>";
				break;
		}
		switch($object_type) {
			case "tier1":
				$has_btn = false;
				$td_btn = "";
				$tr_class = "grand-parent";
				break;
			case "tier2":
				$tr_class = "parent";
				break;
			case "tier3":
				$tr_class = "sub-parent";
				break;
			case "tier4":
				$btn_class = "sub-btn";
				$tr_class = "child";
				break;
		}
		if($has_btn) {
			$td_btn = "<td class='tree-icon ".$icon_class."'><button class='ibutton ".$btn_class."'>X</button></td>";
		}
		echo "
		<tr class=".$tr_class.">
			".$placeholder_td."
			".$td_btn."
			<td class='".$pos_class."'>".$obj['name']." <span class=float style='font-size:75%; margin-left: 20px;'>[".$obj['reftag']."]</span></td>
			<td class='center td-button' object_type=".$object_type." object_id=".$obj['id'].">
				".($can_i_view ? "<button class='action-button $extra_btn_class view-btn' parent_id=".$obj['id'].">View</button>":"")."
				".($can_i_edit ? "<button class='action-button $extra_btn_class edit-btn' parent_id=".$obj['id'].">Edit</button>":"")."
			</td>
		</tr>
		";

	}

	public function drawTreeTierForView($object_type,$level,$obj,$extra_btn_class="",$can_i_view=true,$text="") {
		$placeholder_td = "";
		$pos_class = "";
		$icon_class = "";
		$has_btn = true;
		$btn_class = "expand-btn";
		$tr_class = "";
		switch($level) {
			case 1:
				$pos_class = "uni-pos";
				break;
			case 2:
				$pos_class = "bi-pos";
				$icon_class = "uni-icon";
				$placeholder_td = "";
				break;
			case 3:
				$pos_class = "tri-pos";
				$icon_class = "bi-icon";
				$placeholder_td = "<td></td>";
				break;
			case 4:
				$pos_class = "quad-pos";
				$icon_class = "tri-icon";
				$placeholder_td = "<td></td><td></td>";
				break;
		}
		switch($object_type) {
			case "tier1":
				$has_btn = false;
				$td_btn = "";
				$tr_class = "grand-parent";
				break;
			case "tier2":
				$tr_class = "parent";
				break;
			case "tier3":
				$tr_class = "sub-parent";
				break;
			case "tier4":
				$btn_class = "sub-btn";
				$tr_class = "child";
				break;
		}
		if($has_btn) {
			$td_btn = "<td class='tree-icon ".$icon_class."'><button class='ibutton ".$btn_class."'>X</button></td>";
		}
		echo "
		<tr class=".$tr_class.">
			".$placeholder_td."
			".$td_btn."
			<td class='".$pos_class."'>".$obj['name']." <span class=float style='font-size:75%;margin-left: 20px;'>$text".(strlen($text)>0?"&nbsp;\&nbsp;":"")."[".$obj['reftag']."]</span></span></td>
			<td class='center td-button' object_type=".$object_type." object_id=".$obj['id'].">
				".($can_i_view ? "<button class='action-button $extra_btn_class view-btn' parent_id=".$obj['id'].">View</button>":"")."
			</td>
		</tr>
		";

	}




	public function drawVisibleBlock($title,$lot,$text,$button_name) {
		$data = $this->getVisibleBlock($title,$lot,$text,$button_name);
		echo $data['display'];
		return $data['js'];
	}
	public function getVisibleBlock($title,$lot,$text,$button_name) {
		$data = array(
			'display'=>"",
			'js'=>"",
		);

		$echo = "
		<div style='width:500px;' id=div_".$lot." class=div_segment page=".$lot." my_btn=btn_".$lot."_open>
		<h3 class=object_title>".$title."</h3>
				<Table class='form tbl-segment-description' width=100%>
			<tr>
				<td class=b>".str_replace(chr(10),"<br />",$text)."</td>
			</tr>
		</Table>
		<p class=right><button class=btn_open id=btn_".$lot."_open>".$button_name."</button></p>
	</div>";



		$data['display'] = $echo;
		return $data;

	}








}
?>