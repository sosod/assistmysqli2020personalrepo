<?php
/**
 * To manage the NAMES of items
 *
 * INCOMPLETE!!!!! as at 16 Oct 2014
 *
 *
 * This class cannot be a child of the MSCOA1 class because that class instantiates this class which in turn creates in infinite loop.
 *
 * Created on: 1 September 2014
 * Authors: Janet Currie, Duncan Cosser
 *
 */

class MSCOA1_NAMES extends ASSIST_MODULE_HELPER {

	private $additional_headings = array(
		'admins'=>'contract_owner_id',
		'menu'=>"Menu",
		'headings'=>"Headings",
		'lists'=>"Lists",
	);



    const TABLE = "setup_names";
	const TABLE_FIELD = "name";
    /*************
     * CONSTANTS
     */
	const ACTIVE = 2;
    /**
     * Can a heading be renamed by the client
     */
    const CAN_RENAME = 16;
    /**
     * Is a heading the name of an object
     */
    const OBJECT_HEADING = 32;
    /**
     * Is a heading the name of an activity
     */
    const ACTIVITY_HEADING = 64;
    /**
     * Is a heading the name of an (mSCOA) segment
	 * Used in conjunction with OBJECT_HEADING
     */
    const SEGMENT_HEADING = 128;



    public function __construct($modref="") {
		parent::__construct("client",$_SESSION['cc'],false,$modref);
    }










	/***********************************
	 * CONTROLLER functions
	 */
	public function editObject($var) {
		$section = $var['section'];
		unset($var['section']);
		$mar = $this->editNames($section,$var);
		if($mar>0) {
			if($section=="object_names"){
				return array("ok","Object names updated successfully.");
			} else {
				return array("ok","Activity names updated successfully.");
			}
		} else {
			return array("info","No changes were found to be saved.");
		}
		return array("error",$mar);
	}




    /*******************
     * GET functions
     * */
    public function getTableName() { return $this->getDBRef()."_".self::TABLE; }

	protected function getFormattedNames($type,$with_help=false) {
		$names = array();
		$sql = "SELECT name_section as section, IF(LENGTH(name_client)>0,name_client,name_default) as name
				".($with_help ? ", name_help as help" : "")."
				FROM ".$this->getTableName()."
				WHERE (name_status & ".$type." = ".$type.")
				AND (name_status & ".self::ACTIVE." = ".self::ACTIVE.") "; //echo $sql;
		$rows = $this->mysql_fetch_all($sql);
		if($with_help) {
			foreach($rows as $r){
				$s = explode("_",$r['section']);
				$r['section'] = end($s);
				$names[end($s)] = $r;
			}
		} else {
			foreach($rows as $r){
				$s = explode("_",$r['section']);
				$names[end($s)] = $r['name'];
			}
		} //$this->arrPrint($names);
		return $names;
	}
   /**
    * Get the object names from the table in easy readable format
    */
	public function fetchObjectNames() {
		return $this->getFormattedNames(MSCOA1_NAMES::OBJECT_HEADING);
	}
   /**
    * Get the object names from the table in easy readable format
    */
	public function fetchActivityNames() {
		return $this->getFormattedNames(MSCOA1_NAMES::ACTIVITY_HEADING);
	}





	protected function getNamesFromDB($type,$sections=array(),$include_segment_names=true){
		$sql = "SELECT name_id as id, name_section as section, name_default as mdefault, name_client as client
				FROM ".$this->getTableName()."
				WHERE (name_status & ".$type." = ".$type.")
				AND (name_status & ".self::ACTIVE." = ".self::ACTIVE.") "
				.($include_segment_name==false ? "AND (name_status & ".MSCOA1_NAMES::SEGMENT_HEADING." <> ".MSCOA1_NAMES::SEGMENT_HEADING.") ":"")
				.(count($sections)>0 ? " AND name_section IN ('".implode("','",$sections)."')" : "");
		$rows = $this->mysql_fetch_all_by_id($sql,"section");
		return $rows;
		//return array($sql);
	}

	/**
	 * Get the object names from the table for editing in Setup
	 */
	public function getObjectNamesFromDB($sections=array(),$include_segment_names=true){
		return $this->getNamesFromDB(MSCOA1_NAMES::OBJECT_HEADING,$sections,$include_segment_names);
	}
	/**
	 * Get the activity names from the table for editing in Setup
	 */
	public function getActivityNamesFromDB($sections=array()){
		return $this->getNamesFromDB(MSCOA1_NAMES::ACTIVITY_HEADING,$sections);
	}

	public function fetchSegmentNames($with_help=false) {
		return $this->getFormattedNames(MSCOA1_NAMES::SEGMENT_HEADING,$with_help);
	}






    /****************************************
     * SET / UPDATE Functions
     */


    /**
	 * Function to edit object name / menu heading
	 */
    private function editNames($section,$var) {
    	if($section=="object_names") {
	   		$original_names = $this->getObjectNamesFromDB(array_keys($var));
	   		$log_section = "OBJECT";
			$field = "name_section";
    	} else {
    		$original_names = $this->getActivityNamesFromDB(array_keys($var));
			$log_section = "ACTIVITY";
			$field = "name_section";
    	}
    	$mar = 0;
    	$sql_start = "UPDATE ".$this->getTableName()." SET name_client = '";
		$sql_mid = "' WHERE $field = '";
		$sql_end = "' LIMIT 1";
		$c = array();
    	foreach($var as $section => $val){
    		$m = 0;
			$to = (strlen($val)>0 ? $val : $original_names[$section]['mdefault']);
			$from = $original_names[$section]['client'];
			if($to!=$from) {
    			$sql = $sql_start.$val.$sql_mid.$section.$sql_end;
				$m=$this->db_update($sql);
				if($m>0){
					if(isset($menu_section) && $menu_section=="object_names") {
						$s = explode("_",$section);
						$key = "|".end($s)."|";
					} else {
						$s = explode("_",$section);
						$key = "|".end($s)."|";
					}
					$c[$key] = array('to'=>$to,'from'=>$from);
					$mar+=$m;
				}
			}
    	}
		if($mar>0 && count($c)>0) {
			$changes = array_merge(array('user'=>$this->getUserName()),$c);
			$log_var = array(
				'section'	=> $log_section,
				'object_id'	=> $original_names[$section]['id'],
				'changes'	=> $changes,
				'log_type'	=> MSCOA1_LOG::EDIT,
			);
			$me = new MSCOA1();
			$me->addActivityLog("setup", $log_var);
		}

		return $mar;
    }


	public function saveSegmentDetails($var) {
		$section = $var['section'];
		$field = "segment_".$section;
		$name = $var['name'];
		$help = $var['help'];

		$old = $this->fetchSegmentNames(true);
		$old = $old[$section];

		if($name!=$old['name'] || $help!=$old['help']) {

			$c['|'.$section.'|'] = array();
			if($name!=$old['name']) {
				$c['|'.$section.'|']['name'] = array('to'=>$name,'from'=>$old['name']);
			}
			if($help!=$old['help']) {
				$c['|'.$section.'|']['description'] = array('to'=>$help,'from'=>$old['help']);
			}

			$sql = "UPDATE ".$this->getTableName()." SET name_client = '$name', name_help = '$help' WHERE name_section = '$field' LIMIT 1";
			$mar = $this->db_update($sql);
			if($mar>0) {
				$changes = array_merge(array('user'=>$this->getUserName()),$c);
				$log_var = array(
					'section'	=> $section,
					'object_id'	=> $original_names[$section]['id'],
					'changes'	=> $changes,
					'log_type'	=> MSCOA1_LOG::EDIT,
				);
				$me = new MSCOA1();
				$me->addActivityLog("setup", $log_var);
				return array("ok","$name successfully updated.");
			} else {
				return array("info","No changes found to be saved (Error Code: MAR).");
			}
		} else {
			return array("info","No changes found to be saved (Error Code: CHK).");
		}
		return array("error","An error occurred.  Please try again.");
	}





    /**********************************************
     * PROTECTED functions: functions available for use in class heirarchy
     */











     /***********************
     * PRIVATE functions: functions only for use within the class
     */





    public function __destruct() {
    	parent::__destruct();
    }



}


?>