<?php
/**
 * To manage the mSCOA Segments object
 *
 * Created on: 23 January 2018
 * Authors: Janet Currie
 *
 */

class MSCOA1_SEGMENTS extends MSCOA1 {

    protected $object_id = 0;
	protected $object_details = array();

	protected $status_field = "status";
	protected $id_field = "id";
	protected $parent_field = "parent_id";
	protected $secondary_parent_field = false;
	protected $name_field = "name";
	protected $alias_field = "nickname";
	protected $description_field = "description";
	protected $has_children_field = "has_child";
	protected $can_post_field = "can_post";
	protected $attachment_field = "attachment";

	protected $has_attachment = false;

    protected $ref_tag = "";

	protected $segments;
	protected $section;
	protected $section_set = false;

	protected $dynamic_object_type;
	protected $dynamic_parent_object_type;
	protected $dynamic_object_name;
	protected $dynamic_table;
    /*************
     * CONSTANTS
     */
    const OBJECT_TYPE = false;
    const OBJECT_NAME = false;
	const PARENT_OBJECT_TYPE = false;

    const TABLE = false;
    const TABLE_FLD = "";
    const REFTAG = "ERROR/CONST/REFTAG";

	const LOG_TABLE = "segment";

	/**
	 * Status Constants
	 */
    //const CONFIRMED = 32;
	//const ACTIVATED = 64;

    public function __construct($section="",$modref="") {
        parent::__construct($modref);

		$nameObj = new MSCOA1_NAMES();
		$this->segments = $nameObj->fetchSegmentNames(true);
		$this->section = $section;
		$this->object_form_extra_js = "";

		if(strlen($section)>0 && $section!==false) {
			$this->setSection($section);
		}

    }

	public function setSection($section) {
			$this->dynamic_table = self::LOG_TABLE."_".strtolower($section);
			$this->dynamic_parent_object_type = strtolower($section);
			$this->dynamic_object_type = strtolower($section);
			$this->dynamic_object_name = strtolower($section);
			$this->section = strtolower($section);
			$this->ref_tag = "SEG/".strtoupper($section);
			$this->section_set = true;
	}
	/********************************
	 * CONTROLLER functions
	 */
	public function addObject($var) {
		unset($var['object_id']); //remove incorrect field value from add form
		$segment_section = $var['segment_section'];
		unset($var['segment_section']);
		$this->setSection($segment_section);
		unset($var['idp_id']);
		foreach($var as $key => $v) {
			/* if($this->isDateField($key)) {
				$var[$key] = date("Y-m-d",strtotime($v));
			} else */
			if(is_array($v)) {
				$var[$key] = ASSIST_HELPER::JOIN_FOR_MULTI_LISTS.implode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$v).ASSIST_HELPER::JOIN_FOR_MULTI_LISTS;
			}
		}
		$var['status'] = MSCOA1::ACTIVE;
		$var['insertdate'] = date("Y-m-d H:i:s");
		$var['insertuser'] = $this->getUserID();


		$sql = "INSERT INTO ".$this->getTableName()." SET ".$this->convertArrayToSQLForSave($var);
		$id = $this->db_insert($sql);
		if($id>0) {
			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." created.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> MSCOA1_LOG::CREATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getRefTag().$id." has been successfully created.",
				'object_id'=>$id,
			);
				//'log_var'=>$log_var
			return $result;
		}

		return array("error","Testing: ".$sql." :: ".$this->dynamic_table." :: ".$segment_section);
	}

	public function checkForUniqueGUID($var) {
		$segment = $var['segment_section'];
		$this->setSection($segment);
		$guid = $var['guid'];
		$object_id = $var['object_id'];
		$version = isset($var['version']) ? $var['version'] : false;

		$sql = "SELECT * FROM ".$this->getTableName()." A
				WHERE A.ref = '$guid'
				".($version!==false?" AND line_version LIKE '%".ASSIST_HELPER::JOIN_FOR_MULTI_LISTS.$version.ASSIST_HELPER::JOIN_FOR_MULTI_LISTS."%' ":"")."
				AND NOT(".$this->getStatusSQL("REPORT","A",true,false,false).") "
				.($this->checkIntRef($object_id) ? " AND ".$this->getIDFieldName()." <> ".$object_id : "");
		$rows = $this->mysql_fetch_all($sql);
		if(count($rows)>0) {
			return array("error","The GUID is not unique");
		} else {
			return array("ok","The GUID is unique",$sql);
		}
	}




	public function editObject($var,$attach=array()) {
		$segment_section = $var['segment_section'];
		unset($var['segment_section']);
		$this->setSection($segment_section);
		$var['is_segment'] = true;
		unset($var['idp_id']);
		$result = $this->editMyObject($var,$attach);
		//$result = array("error",serialize($var));
		return $result;
	}

	/**
	 * Check whether an object is in use
	 */
	public function checkUsage($var) {
		//TO BE PROGRAMMED - 17 APRIL 2017 [JC]
		return false;
	}
	/**
	 * Check whether an object can be deleted
	 * 	calls this->checkUsage
	 */
	public function canIBeDeleted($var) {
		$err = false;
		if(is_array($var) && isset($var['object_id']) && ASSIST_HELPER::checkIntRef($var['object_id']) && (isset($var['section']) || $this->section_set)==true) {
			$object_id = $var['object_id'];
			if(isset($var['section'])) {
				$this->setSection($var['section']);
			}
		} elseif(!is_array($var) && ASSIST_HELPER::checkIntRef($var) && $this->section_set == true) {
			$object_id = $var;
		} else {
			$err = true;
			return "error";
		}

		if(!$err) {
			//CHECK HERE FOR EACH SEGMENT TYPE
			$can_delete = $this->checkUsage($object_id);
		}
		return $can_delete===true ? 1 : 0;
	}

	public function deleteObject($var) {
		$id = $var['object_id'];
		$this->setSection($var['segment_section']);
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::DELETED." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deleted.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> MSCOA1_LOG::DELETE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deleted.",
				'object_id'=>$id,
			);
			return $result;

	}

	public function deactivateObject($var) {
		$id = $var['object_id'];
		$this->setSection($var['segment_section']);
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::INACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." deactivated.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> MSCOA1_LOG::DEACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully deactivated.",
				'object_id'=>$id,
			);
			return $result;
	}

	public function restoreObject($var) {
		$id = $var['object_id'];
		$this->setSection($var['object_type']);
		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".self::ACTIVE." WHERE ".$this->getIDFieldName()." = ".$id;
		$this->db_update($sql);

			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag().$id." restored.",
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $id,
				'object_type'	=> $var['object_type'],
				'changes'		=> $changes,
				'log_type'		=> MSCOA1_LOG::RESTORE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$id." has been successfully restored.",
				'object_id'=>$id,
			);
			return $result;
	}

	public function confirmObject($var) {
		$result = array("info","functionality not required");
		/*
		$obj_id = $var['object_id'];
		$new_status = self::ACTIVE + self::CONFIRMED;
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);


			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag.$obj_id." confirmed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> MSCOA1_LOG::CONFIRM,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully confirmed.",
				'object_id'=>$obj_id,
			);

		*/
		return $result;
	}



	public function undoConfirmObject($var) {
		$result = array("info","functionality not required");
/*		$obj_id = $var['object_id'];
		$new_status = self::ACTIVE;
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);


			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag.$obj_id." unconfirmed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> MSCOA1_LOG::UNDOCONFIRM,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully unconfirmed.",
				'object_id'=>$obj_id,
			);
		/*
		*/

		return $result;
	}


	public function activateObject($var) {
		$result = array("info","functionality not required");
		/*
		$obj_id = $var['object_id'];
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		$new_status = $old_status + self::ACTIVATED;

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);


			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag.$obj_id." activated.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> MSCOA1_LOG::ACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." has been successfully activated.",
				'object_id'=>$obj_id,
			);

		*/
		return $result;
	}


	public function undoActivateObject($var) {
		$result = array("info","functionality not required");
		/*
		$obj_id = $var['object_id'];
		$old = $this->getRawObject($obj_id);
		$old_status = $old[$this->getStatusFieldName()];
		$new_status = $old_status - self::ACTIVATED;

		$sql = "UPDATE ".$this->getTableName()." SET ".$this->getStatusFieldName()." = ".$new_status." WHERE ".$this->getIDFieldName()." = ".$obj_id;
		$this->db_update($sql);


			$changes = array(
				'response'=>"|".$this->getMyObjectName()."| ".$this->getRefTag.$obj_id." activation reversed.",
				'changes'=>array('field'=>$this->getStatusFieldName(),'old'=>$old_status,'new'=>$new_status),
				'user'=>$this->getUserName(),
			);
			$log_var = array(
				'object_id'		=> $obj_id,
				'object_type'	=> $this->getMyObjectType(),
				'changes'		=> $changes,
				'log_type'		=> MSCOA1_LOG::UNDOACTIVATE,
			);
			$this->addActivityLog(strtolower($this->getMyLogTable()),$log_var);
			$result = array(
				0=>"ok",
				1=>"Reversal of prior activation of ".$this->getObjectName($this->getMyObjectName())." ".$this->getRefTag().$obj_id." successfull.",
				'object_id'=>$obj_id,
			);

		*/
		return $result;
	}
    /*******************************
     * GET functions
     * */

    public function getTableField() { return self::TABLE_FLD; }
    public function getTableName() { return $this->getDBRef()."_".(self::TABLE!==false?self::TABLE:$this->dynamic_table); }
    public function getDescriptionFieldName() { return $this->description_field; }
    public function getHasChildrenFieldName() { return $this->has_children_field; }
    public function getCanPostFieldName() { return $this->can_post_field; }
    public function getRefTag() { return $this->ref_tag; }
    public function getMyObjectType() { return (self::OBJECT_TYPE!==false?self::OBJECT_TYPE:$this->dynamic_object_type); }
    public function getMyObjectName() { return (self::OBJECT_NAME!==false?self::OBJECT_NAME:$this->dynamic_object_name); }
    public function getMyParentObjectType() { return (self::PARENT_OBJECT_TYPE!==false?self::PARENT_OBJECT_TYPE:$this->dynamic_parent_object_type); }
    public function getMyLogTable() { return self::LOG_TABLE; }
    public function getParentID($i) {
		if($this->parent_field!==false) {
			$sql = "SELECT ".$this->getParentFieldName()." FROM ".$this->getTableName()." WHERE ".$this->getIDFieldName()." = ".$i;
			return $this->mysql_fetch_one_value($sql, $this->getParentFieldName());
		} else {
			return false;
		}

	}

	public function getSegmentDetails($i="") {
		if($i===true && strlen($this->section)>0) {
			return $this->segments[$this->section];
		} elseif($i!==true && strlen($i)>0) {
			return $this->segments[$i];
		} elseif($i!==true) {
			return $this->segments;
		} else {
			return false;
		}
	}

	/**
	 * @param int $parent = which top level item to return (0 for all)
	 * @param bool $active_only = only get active items (form field) or all (view pages)
	 * @param int $idp_id = NO LONGER IN USE - kept for structural purposes
	 * @param bool $get_for_tree_view = format specifically for tree view pages
	 * @param bool $group_by_parent = return results in array grouped by parent or as is
	 * @return array
	 */
	public function getList($parent=0,$active_only=true,$idp_id=0,$get_for_tree_view=false,$group_by_parent=true,$version=false,$group_by_version=false,$limit_to_can_have_children_only=false) {
				//JC - removed check for specific IDP as no longer required now that it is a separate module.
				//left all idp_id variables in for expediency sake
				//19 March 2018
				//".(ASSIST_HELPER::checkIntRef($idp_id) ? " AND ".$this->getSecondaryParentFieldName()." = ".$idp_id : "")."
		$where_version = "";
		if($version!==false && $version!==true) {
			if(is_array($version)) {
				foreach($version as $v) {
					if($this->checkIntRef($v)) {
						$where_version.= " AND line_version LIKE '%".ASSIST_HELPER::JOIN_FOR_MULTI_LISTS.$v.ASSIST_HELPER::JOIN_FOR_MULTI_LISTS."%' ";
					}
				}
			} elseif($this->checkIntRef($version)==true) {
				$where_version = " AND line_version LIKE '%".ASSIST_HELPER::JOIN_FOR_MULTI_LISTS.$version.ASSIST_HELPER::JOIN_FOR_MULTI_LISTS."%' ";
			}
		}
		if($limit_to_can_have_children_only) {
			$where_children_only = " AND ".$this->getHasChildrenFieldName()." = 1 ";
		} else {
			$where_children_only = "";
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ref
				, ".$this->getNameFieldName()." as name
				, ".$this->getAliasFieldName()." as nickname
				, ".$this->getDescriptionFieldName()." as description
				, ".$this->getParentFieldName()." as parent
				, ".$this->getHasChildrenFieldName()." as has_children
				, ".$this->getCanPostFieldName()." as can_assign
				, line_version
				, IF(".$this->getStatusFieldName()." = ".self::ACTIVE.",1,0) as active
				FROM ".$this->getTableName()." S
				WHERE ".($get_for_tree_view ? $this->getTreeStatusSQL("S") : $this->getActiveStatusSQL("S"))."
				".($parent===false ? " AND ".$this->getParentFieldName()." = 0 " : "")."
				".($parent===true ? " AND ".$this->getParentFieldName()." > 0 " : "")."
				".(ASSIST_HELPER::checkIntRef($parent) && $parent!==true && $parent!==false ? " AND ".$this->getParentFieldName()." = ".$parent : "")."
				".$where_version."
				".$where_children_only."
				ORDER BY ".$this->getParentFieldName().", ".$this->getNameFieldName();
		$raw = $this->mysql_fetch_all_by_id($sql,"id");
		$data = array();
		if($group_by_parent==true && $group_by_version==false) {
			foreach($raw as $row) {
				$data[$row['parent']][$row['id']] = $row;
			}
		} elseif($group_by_version==true) {
			foreach($raw as $row) {
				$versions = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS, $row['line_version']);
				$versions = ASSIST_HELPER::removeBlanksFromArray($versions);
				$row['versions'] = $versions;
				foreach($versions as $ver) {
					if($group_by_parent==true) {
						$data[$ver][$row['parent']][$row['id']] = $row;
					} else {
						$data[$ver][$row['id']] = $row;
					}
				}
			}
		} else {
			$data = $raw;
		}
		return $data;
	}

	public function getAllListItemsFormattedForSelect($return_no_posting=false) {
		return $this->getLocalListItemsFormattedForSelect($return_no_posting,false);
	}
	public function getActiveListItemsFormattedForSelect($return_no_posting=false,$idp_id=0) { //echo $idp_id;
		return $this->getLocalListItemsFormattedForSelect($return_no_posting,true);
	}
	private function getLocalListItemsFormattedForSelect($return_no_posting=false,$active_only=false) {
    	if(is_array($return_no_posting)) {
    		$extra_info = $return_no_posting;
			$return_no_posting = isset($extra_info['return_no_posting']) ? $extra_info['return_no_posting'] : false;
			$include_nickname = isset($extra_info['include_nickname']) ? $extra_info['include_nickname'] : false;
			$include_guid = isset($extra_info['include_guid']) ? $extra_info['include_guid'] : false;
			$version = isset($extra_info['version']) ? $extra_info['version'] : false;
			$limit_to_can_have_children_only = isset($extra_info['can_have_children']) ? $extra_info['can_have_children'] : false;
			$ignore_no_can_post_and_check_for_can_have_children = isset($extra_info['ignore_no_can_post_and_check_for_can_have_children']) ? $extra_info['ignore_no_can_post_and_check_for_can_have_children'] : false;
			$ignore_line_id = false;
			$ignore_children_of_given_line_id = false;
//			$ignore_line_id = isset($extra_info['ignore_line_id']) ? $extra_info['ignore_line_id'] : false;
//			$ignore_children_of_given_line_id = isset($extra_info['ignore_children_of_given_line_id']) ? $extra_info['ignore_children_of_given_line_id'] : false;
		} else {
    		$include_guid = false;
    		$include_nickname = false;
    		$version = false;
    		$limit_to_can_have_children_only = false;
    		$ignore_no_can_post_and_check_for_can_have_children = false;
			$ignore_line_id = false;
			$ignore_children_of_given_line_id = false;
		}
    	if($version!==false && !is_array($version)) {
    		$v = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$version);
    		$version = $this->removeBlanksFromArray($v);
		}
		$data = $this->getList(0,$active_only,0,false,true,$version,true,$limit_to_can_have_children_only);
    	$no_can_post = array();
		$items = array();
		if($version!==false && is_array($version) && count($version)>1) {
			$results = array();
			foreach($version as $ver) {
				$data = isset($data[$ver]) ? $data[$ver] : array();
				if(isset($data[0]) && count($data[0]) > 0) {
					foreach($data[0] as $key => $d) {
						if($ignore_line_id===false || $key!=$ignore_line_id) {
							$title = array();
							$ret = $this->processList($data, $key, $d, $title, $items, $no_can_post,$include_nickname,$include_guid,$ignore_no_can_post_and_check_for_can_have_children,$ignore_line_id);
							//$title = $ret[0];
							$items = $ret[1];
	//						$no_can_post = $ret[2];
						}
					}
				}
				$results[$ver] = $items;
			}
			$k = array_keys($version);
			$x = 0;
			$start = $results[$k[$x]];
			$x++;
			$items = array_intersect_key($start,$results[$k[$x]]);
			unset($results[$k[$x]]);
			$x++;
			while($x<count($k)) {
				$start = $items;
				$items = array_intersect_key($start,$results[$k[$x]]);
				unset($results[$k[$x]]);
				$x++;
			}
		} else {
			if($version!==false) {
				if(!is_array($version)) {
					$data = isset($data[$version]) ? $data[$version] : array();
				} else {
					$k = array_keys($version);
					$version = $version[$k[0]];
					$data = isset($data[$version]) ? $data[$version] : array();
				}
			}
			//correct up to here
			if(isset($data[0]) && count($data[0]) > 0) {
				foreach($data[0] as $key => $d) {
					if($ignore_line_id===false || $key!=$ignore_line_id) {
						$title = array();
						$ret = $this->processList($data, $key, $d, $title, $items, $no_can_post,$include_nickname,$include_guid,$ignore_no_can_post_and_check_for_can_have_children,$ignore_line_id);
						//$title = $ret[0];
						$items = $ret[1];
//						$no_can_post = $ret[2];
					}
				}
			}
		}
		if($return_no_posting==true) {
			return array('options'=>$items,'disabled'=>$no_can_post);
		} else {
			return $items;
		}
	}





	//Used By SDBP6:FixedReport
	public function getListOfActiveItemFormattedInParentChild() {
    	$data = array();
		$parents = $this->getList(false,true,0,false,false);
		$data[0] = $parents;
		$children = $this->getList(true,true,0,false,true);
		//convert to children to single level per parent
		$items = array();
		if(count($data[0])>0) {
			foreach($data[0] as $parent_id => $parent) {
				if(isset($children[$parent_id]) && count($children[$parent_id])>0) {
					foreach($children[$parent_id] as $key => $d) {
						$title = array();
						$ret = $this->processList($children, $key, $d, $title, $items,false);
						//$title = $ret[0];
						$items = $ret[1];
					}
					$data[$parent_id] = $items;
				}
			}
		}

		return $data;
	}

	public function getItemsForExport($include_shortname=true,$only_can_assign=true,$version=0) {
    	$data1 = $this->getActiveListItemsFormattedForSelect(array('version'=>$version));
		$data2 = $this->getList(0,true,0,false,false,$version);
    	$data = array();
    	foreach($data1 as $i => $d) {
    		$data[$i] = array(
    			'guid'=>$data2[$i]['ref'],
				'long_name'=>$d,
				'short_name'=>$data2[$i]['name'],
			);
		}
    	return $data;
	}


	public function getParentVersions($object_id) {
		$row = $this->getRawObject($object_id);
		$v = explode(ASSIST_HELPER::JOIN_FOR_MULTI_LISTS,$row['line_version']);
		$v = $this->removeBlanksFromArray($v);
		return $v;
	}

	public function getParentName($parent_object_id=0,$object_id=0,$include_top=false) {
		//if parent_id = 0 & object_id has been passed then assume that you need to get the parent_id from the child line details (if object_id has not been passed then assume that line is top level & parent_id is actually 0)
		if(!$this->checkIntRef($parent_object_id) && $this->checkIntRef($object_id)) {
			$row = $this->getRawObject($object_id);
			$parent_object_id = $row['parent_id'];
		}
		//if parent_id is still 0 then assume that line is top level
		$p = array();
		while($parent_object_id>0) {
			$row = $this->getRawObject($parent_object_id);
			$p[] = $row['name'];
			$parent_object_id = $row['parent_id'];
		}
		if(count($p)>0) {
			$p = array_reverse($p);
			$parent_name = implode(": ",$p);
		} else {
			$parent_name = $include_top?"N/A [Top Level]":"";
		}
		return $parent_name;
	}

	/**
	 * @param $data - raw data for formatting - from getList function
	 * @param $key
	 * @param $d
	 * @param $title
	 * @param $items
	 * @param $no_can_post
	 * @param bool $include_nickname - include the nickname in the display name, where available - added 13 Jan 2020 [JC] AA-299 - needed to facilitate auto-complete search function
	 * @param bool $include_guid - include the nickname in the display name, where available - added 13 Jan 2020 [JC] AA-299 - needed to facilitate auto-complete search function
	 * @return array
	 */
	private function processList($data,$key,$d,$title,$items,$no_can_post,$include_nickname=false,$include_guid=false,$ignore_no_can_post_and_check_for_can_have_children=false,$ignore_line_id=false) {
		$title[] = str_replace(chr(10),'',trim($d['name']));
		if($d['can_assign']==false) { $no_can_post[] = $key; }
		if($d['can_assign']==true || ($ignore_no_can_post_and_check_for_can_have_children && $d['has_children']==true)) {
			/*
			 * a million trims and str_replace chr(10) added to catch funny characters in HES0001's mSCOA Library that kept breaking the auto complete JS - AA-299 [JC]
			 */
			$display_text = implode(": ",$title);
			if($include_nickname && $include_guid) {
				$ex = array();
				if(strlen($d['nickname'])>0) { $ex[] = str_replace(chr(10),'',trim($d['nickname'])); }
				$ex[] = str_replace(chr(10),'',trim($d['ref']));
				$display_text.= " [".implode(" / ",$ex)."]";
			} elseif($include_nickname && strlen($d['nickname'])>0) {
				$display_text.= " [".str_replace(chr(10),'',trim($d['nickname']))."]";
			} elseif($include_guid) {
				$display_text.= " [".str_replace(chr(10),'',trim($d['ref']))."]";
			}
//			$items[$key] = $this->cleanUpTextWithoutChangingEncoding($display_text);
			$display_text = str_replace(chr(10),'',$display_text);
			$display_text = str_replace('\n','',$display_text);
			$display_text = str_replace('\r','',$display_text);
			$items[$key] = trim($display_text);
		//REMOVED 13 Jan 2020 - AA-299
//			if(isset($d['nickname']) && strlen($d['nickname'])>0) {
//				$items[$key] = $d['nickname'];
//			} else {
//				$items[$key] = implode(": ",$title);
//			}
		}
		if(isset($data[$key]) && count($data[$key])>0) {
			foreach($data[$key] as $k => $e) {
				$ret = $this->processList($data, $k, $e, $title, $items, $no_can_post,$include_nickname,$include_guid,$ignore_no_can_post_and_check_for_can_have_children,$ignore_line_id);
				//$title = $ret[0];
				$items = $ret[1];
				$no_can_post = $ret[2];
			}
		}
		return array($title,$items,$no_can_post);
	}

	public function getNameForAnObject($id) {
		$row = $this->getRawObject($id);
		if(is_array($id)) {
			$d = array();
			foreach($row as $k => $r) {
				$d[] = $r[$this->getNameFieldName()];
			}
			$data = implode(chr(10),$d);
		} else {
			$data = $row[$this->getNameFieldName()];
		}
		return $data;
	}
	public function getAObject($id=0,$options=array()) {
		$row = $this->getRawObject($id);
		return $row;
	}

	public function getAListItemName($id,$name_only = true) {
		$list = $this->getAllListItemsFormattedForSelect();
		$data = array();
		if(is_array($id)) {
			foreach($id as $i) {
				$data[$i] = isset($list[$i]) ? $list[$i] : "Not available";
			}
			return $data;
		} elseif($name_only) {
			return isset($list[$id]) ? $list[$id] : $this->getUnspecified();
		} else {
			$data[$id] = isset($list[$id]) ? $list[$id] : "Not available";
			return $data;
		}
	}

	public function getHeading($id) {
		$row = $this->getRawObject($id);
		return $row[$this->getNameFieldName()].(strlen($row[$this->getTableField()."_ref"])>0 ? " (".$row[$this->getTableField()."_ref"].")":"");
	}

	/***
	 * Returns an unformatted array of an object
	 */
	public function getRawObject($obj_id) {
		if(is_array($obj_id)) {
			$where = $this->getIDFieldName()." IN (".implode(",",$obj_id).")";
			$multi = true;
		} else {
			$where = $this->getIDFieldName()." = ".$obj_id;
			$multi = false;
		}
		$sql = "SELECT * FROM ".$this->getTableName()." WHERE ".$where;
		if($multi) {
			$data = $this->mysql_fetch_all_by_id($sql,$this->getIDFieldName());
		} else {
			$data = $this->mysql_fetch_one($sql);
		}
		return $data;
	}



	public function getOrderedObjects($parent_parent_id=0,$parent_id=0){
		$res2 = array();
	/*	if($parent_id==0) {
			$parentObject = new MSCOA1_PARENT();
			$sql_from = "INNER JOIN ".$parentObject->getTableName()." B ON A.".$this->getParentFieldName()." = B.".$parentObject->getIDFieldName()." AND B.".$parentObject->getParentFieldName()." = ".$parent_parent_id." WHERE ";
		} else {
			$sql_from = "WHERE ".$this->getParentFieldName()." = ".$parent_id." AND ";
		}
		$sql = "SELECT ".$this->getIDFieldName()." as id
				, ".$this->getNameFieldName()." as name
				, CONCAT('".$this->getRefTag()."',".$this->getIDFieldName().") as reftag
				, ".$this->getParentFieldName()." as parent_id
				FROM ".$this->getTableName()." A
				".$sql_from."  ".$this->getActiveStatusSQL("A");
		$res2 = $this->mysql_fetch_all_by_id2($sql, "parent_id", "id");*/

		return $res2;
	}



	public function getListOfObjectsFormattedForSelect($active=true,$activated=true) {
		//$sql = "SELECT * FROM ".$this->getTableName()." WHERE "
	}


	/**
	 * Returns status check for Actions which have not been deleted
	 */
	public function getActiveStatusSQL($t="") {
		//Contracts where
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("ALL",$t,false,false,false);
	}
	public function getTreeStatusSQL($t="") {
		//Contracts where
			//status = active and
			//status <> deleted
		return $this->getStatusSQL("TREE",$t,false,false,false);
	}
		/**
	 * Returns status check for Actions are fully activated & available outside of NEW
	 */
	public function getReportingStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status = confirmed and
			//status = activated
		return $this->getStatusSQL("REPORT",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW pages up to Confirmation
	 */
	public function getNewStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("NEW",$t,false);
	}
	/**
	 * Returns status check for Contracts to display on NEW / ACTIVATE / WAITING
	 */
	public function getActivationStatusSQL($t="") {
		//Contracts where
			//activestatussql and
			//status <> confirmed and
			//status <> activated
		return $this->getStatusSQL("ACTIVATE",$t,false);
	}

	public function hasDeadline() { return false; }
	public function getDeadlineField() { return false; }



    /***
     * SET / UPDATE Functions
     */







    /****
     * PROTECTED functions: functions available for use in class heirarchy
     */
    /****
     * PRIVATE functions: functions only for use within the class
     */



}


?>