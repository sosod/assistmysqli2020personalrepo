<?php
//ASSIST_HELPER::arrPrint($_REQUEST);
//ASSIST_HELPER::arrPrint($_FILES);

$line_version = $_REQUEST['line_version'];
/* Handle the uploading of the file */
/* ----------------------------------------------------------------------------------------------------------------------- */

//Test that a valid file has been imported
$valid_file = true;
if(!isset($_FILES['mscoa_import_file']['name']) || strlen($_FILES['mscoa_import_file']['name'])==0) {
	$valid_file = false;
	$error = "No import file found.  Please go back and try again.";
} else {
	$name = $_FILES['mscoa_import_file']['name'];
	$test = explode(".",$name);
	if(count($test)<2) {
		$error = "Invalid file name - Assist could not detect a valid file extension.  Please go back and try again.";
		$valid_file = false;
	} else {
		$ext = array_pop($test);
		if(strtoupper($ext)!="CSV") {
			$error = "Invalid file type - only Comma Separated/Delimited Values (CSV) files permitted.  Please go back and try again.";
			$valid_file = false;
		}
	}
}
if($valid_file!==true) {
	ASSIST_HELPER::displayResult(array("error",$error));
	die();
}


//Upload the import file
$folder = $segmentObject->getAttachmentImportFolder();
$save_folder = $segmentObject->getSaveFolder();
$full_folder = $segmentObject->checkFolder($folder,1,true);
$file_name = $section."_".date("YmdHis");
$ext = ".".strtolower($ext);

//test file name is unique
$c = 1;
$test_name = $file_name.$ext;
while(file_exists($full_folder."/".$test_name)) {
	$file_name.="_".$c;
	$c++;
	$test_name = $file_name.$ext;
}
$file_name.=$ext;

//test file size before trying to upload
$max_file_size_allowed = $segmentObject->getMaxUploadFileSize();
if($_FILES['mscoa_import_file']['size']>$max_file_size_allowed) {
	ASSIST_HELPER::displayResult(array("error","Invalid file size - the file you have tried to upload is too big.  Please try again with a smaller file - only files up to ".$segmentObject->formatBytes($max_file_size_allowed,null,0)." are permitted."));
	die();
}

//if you've gotten this far, then upload the file
if (move_uploaded_file($_FILES['mscoa_import_file']['tmp_name'], $full_folder."/".$file_name)) {
	/*echo "
	<p><span class='b green'>Success!</span>  The file ". basename( $_FILES['mscoa_import_file']['name']). " has been uploaded.</p>
	<p>Assist is now processing the import file. Please do not close the window while this is happening...</p>";*/
} else {
	ASSIST_HELPER::displayResult(array("error","There was an unexpected error while trying to upload your file.  Please try again."));
	die();
}




/* ----------------------------------------------------------------------------------------------------------------------- */
/* now to process the import file into the mscoa structure */
/* ----------------------------------------------------------------------------------------------------------------------- */

//read the uploaded file
$import = fopen($full_folder."/".$file_name,"r");
$data = array();
while(!feof($import)) {
	$tmpdata = fgetcsv($import);
	if(count($tmpdata)>1) {
		$data[] = $tmpdata;
	}
	$tmpdata = array();
}
fclose($import);

//test that data exists in the file
if(count($data)<3) {
	ASSIST_HELPER::displayResult(array("error","No data was found to be imported.  Please check that your file is not empty."));
	die();
}
//remove heading & definition rows
unset($data[0]);
unset($data[1]);

//check for too many lines
if(count($data)>$max_rows_to_import) {
	ASSIST_HELPER::displayResult(array("error","Your file has too much data in it to be processed in a single upload.  Please break up your file into multiple smaller files of no more than $max_rows_to_import rows and try again."));
	die();
}

$has_data = false;

$processed_data = array();
$errors = array();
$status_count = array(
	'rows'=>array('ok'=>0,'warn'=>0,'error'=>0),
	'fields'=>array('ok'=>0,'warn'=>0,'error'=>0),
);
$parents = array();
$parent_by_guid = array();
$parent_names = array();
$guids_in_use = array();
$guids_by_id = array();


$import_data = array();
$import_ref = $file_name.".".$segmentObject->getUserID();


//before processing import, get current data to check paretns & unique guids
$segment_data = $segmentObject->getList(0,TRUE,0,true,true,$line_version);
//ASSIST_HELPER::arrPrint($segment_data);
foreach($segment_data as $key1 => $rows ) {
	foreach($rows as $key => $row) {
		$my_guid = $row['ref'];
		if(strlen($my_guid)>0) {
			$guids_in_use[$my_guid] = array('src'=>"database",'row'=>$key);
			$guids_by_id[$key] = $my_guid;
			if($row['parent']>0) {
				$has_parent = true;
				$parent_guid = $guids_by_id[$row['parent']];
			} else {
				$has_parent = false;
			}
			if($row['has_children']==true && $row['active']==true && ($has_parent==false || $parent_by_guid[$parent_guid]=="yes")) {
				$parent_by_guid[$my_guid] = "yes";
				$my_name = $row['name'];
				$parent_names[$my_guid] = ($has_parent && isset($parent_names[$parent_guid])?$parent_names[$parent_guid].$name_divider:"").$my_name;
			} else {
				$parent_by_guid[$my_guid] = "no";
			}
		}
	}
}

$ids_by_guid = array_flip($guids_by_id);
$new_ids_by_guid = array();

//process the import data
foreach($data as $key => $row) {
	$row = $segmentObject->removeBlanksFromArray($row,false,true);
	if(count($row)>0) {
/*
 * DATA VALIDATION during loop (DEVELOPMENT)
echo "<hr />";
		echo $row[1];
echo "<hr />";
ASSIST_HELPER::arrPrint($processed_data);
echo "<hr />";

ASSIST_HELPER::arrPrint($parent_by_guid);
echo "<hr />";
*/
		$has_data = true;
		$my_guid = isset($row[1]) ? trim($row[1]) : "";
		$my_parent = isset($row[0]) ? trim($row[0]) : "";
		$my_name = isset($row[2]) ? trim($row[2]) : "";
		$my_description = isset($row[3]) ? trim($row[3]) : "";
		$my_nickname = isset($row[4]) ? trim($row[4]) : "";
		$processed_data[$key] = array(
			'row'=>$key+1,
			'parent_guid'=>$my_parent,
			'parent_name'=>"",
			'guid'=>$my_guid,
			'name'=>$row[2],
			'description'=>$my_description,
			'nickname'=>$my_nickname,
			'has_subs'=>"",
			'can_assign'=>"",
			'import_status'=>array(
				'final_icon'=>"ok"
			),
		);
		$errors[$key] = array(
			'parent_guid'=>"",
			'guid'=>"",
			'name'=>"",
			'description'=>"",
			'has_subs'=>"",
			'can_assign'=>"",
		);
		$import_data[$key] = array(
			'parent_id'=>0,
			'ref'=>ASSIST_HELPER::code($my_guid),
			'name'=>ASSIST_HELPER::code($my_name),
			'description'=>ASSIST_HELPER::code($my_description),
			'nickname'=>ASSIST_HELPER::code($my_nickname),
			'has_child'=>0,
			'can_post'=>0,
			'import_ref'=>$import_ref,
			'parent_source'=>"",
			'import_key'=>$key,
			'import_status'=>0,
			'import_user'=>$segmentObject->getUserID(),
			'import_date'=>date("Y-m-d H:i:s"),
		);
		
		
		//check parent
		$data_key = 0;
		$fld = "parent_guid";
		$fld_name = "Parent GUID";
		$parent_check_later = false;
		$v = $my_parent;
		if(strlen($v)>0) {
			if(isset($parent_by_guid[$v]) && $parent_by_guid[$v]=="yes") {
				//all ok - set the parent name field for later children
				$processed_data[$key]['parent_name'] = $parent_names[$my_parent];
				$status_count['fields']['ok']++;
				if(isset($ids_by_guid[$v])) {
					$import_data[$key]['parent_id'] = $ids_by_guid[$v];
					$import_data[$key]['parent_source'] = "DB";
				} else {
					$import_data[$key]['parent_id'] = $new_ids_by_guid[$v];
					$import_data[$key]['parent_source'] = "NEW";
				}
			} elseif(!isset($parent_by_guid[$v])) {
				//parent guid is not set - this will need to be checked later
				$processed_data[$key]['import_status']['final_icon'] = "error";
				$processed_data[$key]['import_status'][$fld] = "Error - Selected Parent could not be found.  Please ensure that the Parent is loaded first.";
			} else {
				$errors[$key][$fld] = "error";
				$status_count['fields']['error']++;
				switch($parent_by_guid[$v]) {
					case "no":
						$processed_data[$key]['import_status']['final_icon'] = "error";
						$processed_data[$key]['import_status'][$fld] = "Error - Selected Parent is not permitted to have sub-levels / children";
						break;
					case "error":
						$processed_data[$key]['import_status']['final_icon'] = "error";
						$processed_data[$key]['import_status'][$fld] = "Error - Selected Parent is in an error state and sub-levels / children can not be assigned at this time.";
						break;
					default:
						$processed_data[$key]['import_status']['final_icon'] = "error";
						$processed_data[$key]['import_status'][$fld] = "Error - An unknown error occured with the selected Parent and sub-levels / children can not be assigned at this time. (Error code: ".$parent_by_guid[$v].")";
						break;
				}
			}
		} else {
			$status_count['fields']['ok']++;
		}

		//check guid - required & unique
		$data_key = 1;
		$fld = "guid";
		$fld_name = "Line Item GUID";
		$v = $my_guid;
		if(strlen($v)>0) {
			if(isset($guids_in_use[$v])) {
				$processed_data[$key]['import_status']['final_icon'] = "error";
				$errors[$key][$fld] = "error";
				$processed_data[$key]['import_status'][$fld] = "Error - $fld_name must be unique.  The $fld_name given is already in use (Where: ".($guids_in_use[$v]['src']=="import" ? "Row ".$guids_in_use[$v]['row']." of the import file" : "Database").") and cannot be reused.";
				$status_count['fields']['error']++;
			} else {
				$guids_in_use[$v] = array('src'=>"import",'row'=>$key);
				$status_count['fields']['ok']++;
			}
		} else {
			$processed_data[$key]['import_status']['final_icon'] = "error";
			$processed_data[$key]['import_status'][$fld] = "Error - $fld_name is a required field.";
			$errors[$key][$fld] = "error";
			$status_count['fields']['error']++;
		}


		//check required name
		$data_key = 2;
		$fld = "name";
		$fld_name = "Line Item Name";
		$v = $my_name;
		if(strlen($v)>0) {
			//all ok, do nothing
			$status_count['fields']['ok']++;
		} else {
			$processed_data[$key]['import_status']['final_icon'] = "error";
			$processed_data[$key]['import_status'][$fld] = "Error - $fld_name is a required field.";
			$errors[$key][$fld] = "error";
			$status_count['fields']['error']++;
		}

		//description = col 3
		//nickname = col 4

		//check has_subs
		if($parent_check_later===false) {
			$data_key = 5;
			$fld = "has_subs";
			$fld_name = "Has Children";
			$v = isset($row[$data_key]) ? strtoupper(trim($row[$data_key])) : "";
			$has_sublevels = "no";
			if(strlen($v)==0) {
				$processed_data[$key]['import_status'][$fld] = "Error: $fld_name is required.";
				$processed_data[$key]['import_status']['final_icon'] = "error";
				$errors[$key][$fld] = "error";
				$status_count['fields']['error']++;
				$has_sublevels = "error";
				$processed_data[$key][$fld] = isset($row[$data_key]) ? $row[$data_key] : "";
			} elseif($v!="NO" && $v!="N" && $v!="YES" && $v!="Y") {
				$processed_data[$key]['import_status'][$fld] = "Error: $fld_name is invalid - only No or Yes are permitted.";
				$processed_data[$key]['import_status']['final_icon'] = "error";
				$errors[$key][$fld] = "error";
				$status_count['fields']['error']++;
				$has_sublevels = "error";
				$processed_data[$key][$fld] = $row[$data_key];
			} else {
				if($v=="NO" || $v == "N") {
					$processed_data[$key][$fld] = ASSIST_HELPER::getDisplayIconAsDiv(false);
					$parent_by_guid[$my_guid]="no";
					$status_count['fields']['ok']++;
					$import_data[$key]['has_child'] = 0;
				} else {
					if($errors[$key]['parent_guid']==false && ($processed_data[$key]['parent_guid']=="" || (isset($parent_by_guid[$processed_data[$key]['parent_guid']]) && $parent_by_guid[$processed_data[$key]['parent_guid']]=="yes"))) {
						$processed_data[$key][$fld] = ASSIST_HELPER::getDisplayIconAsDiv(true);
						$parent_by_guid[$my_guid] = "yes";
						$parent_names[$my_guid] = (strlen($processed_data[$key]['parent_name'])?$processed_data[$key]['parent_name'].$name_divider:"").$my_name;
						$has_sublevels = "yes";
						$status_count['fields']['ok']++;
						$new_ids_by_guid[$my_guid] = $key;
						$import_data[$key]['has_child'] = 1;
					} else {
						$processed_data[$key]['import_status'][$fld] = "Error: $fld_name can not be allowed as selected Parent does not permit it.";
						$errors[$key][$fld] = "error";
						$status_count['fields']['error']++;
						$has_sublevels = "error";
						$processed_data[$key]['import_status']['final_icon'] = "error";
						$processed_data[$key][$fld] = $row[$data_key];
					}
				}
			}
		}

		//check can_assign
		$data_key = 6;
		$fld = "can_assign";
		$fld_name = "Can Assign to KPIs/Projects";
		$can_assign = "no";
		$v = isset($row[$data_key]) ? strtoupper(trim($row[$data_key])) : "";
		if(strlen($v)==0) {
			$processed_data[$key]['import_status'][$fld] = "Error: $fld_name is required.";
			$processed_data[$key]['import_status']['final_icon'] = "error";
			$errors[$key][$fld] = "error";
			$status_count['fields']['error']++;
			$processed_data[$key][$fld] = isset($row[$data_key]) ? $row[$data_key] : "";
		} elseif($v!="NO" && $v!="N" && $v!="YES" && $v!="Y") {
			$processed_data[$key]['import_status'][$fld] = "Error: $fld_name is invalid - only No or Yes are permitted.";
			$errors[$key][$fld] = "error";
			$status_count['fields']['error']++;
			$processed_data[$key]['import_status']['final_icon'] = "error";
			$processed_data[$key][$fld] = $row[$data_key];
		} else {
			if($v=="NO" || $v == "N") {
				$processed_data[$key][$fld] = ASSIST_HELPER::getDisplayIconAsDiv(false);
				$status_count['fields']['ok']++;
				$import_data[$key]['can_post'] = 0;
			} else {
				$processed_data[$key][$fld] = ASSIST_HELPER::getDisplayIconAsDiv(true);
				$can_assign = "yes";
				$status_count['fields']['ok']++;
				$import_data[$key]['can_post'] = 1;
			}
		}

		//check for and warn user about dead ends
		if($parent_check_later===false && $has_sublevels=="no" && $can_assign=="no") {
			$errors[$key]['has_subs'] = "warn";
			$errors[$key]['can_assign'] = "warn";
			$processed_data[$key]['import_status']['final_icon'] = ($processed_data[$key]['import_status']['final_icon']!="error" ? "warn" : $processed_data[$key]['import_status']['final_icon']);
			$processed_data[$key]['import_status']['other_error'] = "Warning - Both Has Sub-levels & Can Assign to KPIs/Projects cannot be No as this creates a dead end in the segment.  At least one of these options should be Yes.";
			$status_count['fields']['warn']++;
		}

		//if no error have been found then make final icon = k
		if(in_array($processed_data[$key]['import_status']['final_icon'],array("","ok")) && count($processed_data[$key]['import_status'])==1) {
			$processed_data[$key]['import_status']['final_icon'] = "ok";
			$processed_data[$key]['import_status']['final_result'] = "Ok to import";
			$status_count['rows']['ok']++;
		} elseif($processed_data[$key]['import_status']['final_icon']=="error") {
		//otherwise check for additional warnings
			$status_count['rows']['error']++;
			if(isset($parent_by_guid[trim($processed_data[$key]['guid'])]) && $parent_by_guid[trim($processed_data[$key]['guid'])]=="yes") {
				//don't allow children to be added to this item because it is in error state
				$parent_by_guid[trim($processed_data[$key]['guid'])] = "error";
			}
		} elseif($processed_data[$key]['import_status']['final_icon']=="warn") {
			$status_count['rows']['warn']++;
		}
		
		
	}
}
if($has_data!==true) {
	ASSIST_HELPER::displayResult(array("error","The file appears to be empty.  Please try again.  Remember that the import process ignores the first 2 rows as they are assumed to be headings."));
	die();
}

/*
REMOVED - 25 FEB 2018 [JC]
Makes process too complicated when uploading to temp table and then copying to live table.  Users are required to load Parents first.
//rerun parent guid check for those that previously failed, just in case their parent was read afterwards
foreach($processed_data as $key => $row) {
	if($row['parent_name']=="check_me_later") {
		$my_guid = $row['guid'];
		$my_parent = $row['parent_guid'];

		$fld = "parent_guid";
		$fld_name = "Parent GUID";
		$v = $my_parent;
			if(isset($parent_by_guid[$v]) && $parent_by_guid[$v]=="yes") {
				//all ok - set the parent name field for later children
				$processed_data[$key]['parent_name'] = $parent_names[$my_parent];
				$status_count['fields']['ok']++;
			} elseif(!isset($parent_by_guid[$v])) {
				//no parent found
				$processed_data[$key]['parent_name'] = "";
				$processed_data[$key]['import_status']['final_icon'] = "error";
				$processed_data[$key]['import_status'][$fld] = "Error - Selected Parent is not found.  Please check that you have captured a valid GUID.";
				$errors[$key][$fld] = "error";
				$status_count['fields']['error']++;
			} else {
				$errors[$key][$fld] = "error";
				$status_count['fields']['error']++;
				switch($parent_by_guid[$v]) {
					case "no":
						$processed_data[$key]['import_status']['final_icon'] = "error";
						$processed_data[$key]['import_status'][$fld] = "Error - Selected Parent is not permitted to have sub-levels / children";
						break;
					case "error":
						$processed_data[$key]['import_status']['final_icon'] = "error";
						$processed_data[$key]['import_status'][$fld] = "Error - Selected Parent is in an error state and sub-levels / children can not be assigned at this time.";
						break;
					default:
						$processed_data[$key]['import_status']['final_icon'] = "error";
						$processed_data[$key]['import_status'][$fld] = "Error - An unknown error occured with the selected Parent and sub-levels / children can not be assigned at this time. (Error code: ".$parent_by_guid[$v].")";
						break;
				}
			}
		//check has_subs
		$data_key = 4;
		$fld = "has_subs";
		$fld_name = "Has Sub-Levels";
		$v = strtoupper($data[$key][$data_key]);
		$has_sublevels = "no";
		if(strlen($v)==0) {
			$processed_data[$key]['import_status'][$fld] = "Error: $fld_name is required.";
			$processed_data[$key]['import_status']['final_icon'] = "error";
			$errors[$key][$fld] = "error";
			$status_count['fields']['error']++;
			$has_sublevels = "error";
			$processed_data[$key][$fld] = $data[$key][$data_key];
		} elseif($v!="NO" && $v!="N" && $v!="YES" && $v!="Y") {
			$processed_data[$key]['import_status'][$fld] = "Error: $fld_name is invalid - only No or Yes are permitted.";
			$processed_data[$key]['import_status']['final_icon'] = "error";
			$errors[$key][$fld] = "error";
			$status_count['fields']['error']++;
			$has_sublevels = "error";
			$processed_data[$key][$fld] = $data[$key][$data_key];
		} else {
			if($v=="NO" || $v == "N") {
				$processed_data[$key][$fld] = ASSIST_HELPER::getDisplayIconAsDiv(false);
				$parent_by_guid[$my_guid]="no";
				$status_count['fields']['ok']++;
			} else {
				if($errors[$key]['parent_guid']==false && $processed_data[$key]['import_status']['final_icon'] != "error" && ($processed_data[$key]['parent_guid']=="" || (isset($parent_by_guid[$processed_data[$key]['parent_guid']]) && $parent_by_guid[$processed_data[$key]['parent_guid']]=="yes"))) {
					$processed_data[$key][$fld] = ASSIST_HELPER::getDisplayIconAsDiv(true);
					$parent_by_guid[$my_guid] = "yes";
					$parent_names[$my_guid] = (strlen($processed_data[$key]['parent_name'])?$processed_data[$key]['parent_name'].$name_divider:"").$my_name;
					$has_sublevels = "yes";
					$status_count['fields']['ok']++;
				} else {
					if(!isset($parent_by_guid[$processed_data[$key]['parent_guid']])) {
						$processed_data[$key]['import_status'][$fld] = "Error: $fld_name can not be allowed as selected Parent can not be found.";
					} else {
						$processed_data[$key]['import_status'][$fld] = "Error: $fld_name can not be allowed as selected Parent does not permit it.";
					}
					$errors[$key][$fld] = "error";
					$status_count['fields']['error']++;
					$has_sublevels = "error";
					$processed_data[$key]['import_status']['final_icon'] = "error";
					$processed_data[$key][$fld] = $data[$key][$data_key];
				}
			}
		}
		//check for and warn user about dead ends
		if($has_sublevels=="no" && strtolower($data[$key][5])=="no") {
			$errors[$key]['has_subs'] = "warn";
			$errors[$key]['can_assign'] = "warn";
			$processed_data[$key]['import_status']['final_icon'] = ($processed_data[$key]['import_status']['final_icon']!="error" ? "warn" : $processed_data[$key]['import_status']['final_icon']);
			$processed_data[$key]['import_status']['other_error'] = "Warning - Both Has Sub-levels & Can Assign to KPIs/Projects cannot be No as this creates a dead end in the segment.  At least one of these options should be Yes.";
			$status_count['fields']['warn']++;
		}

	}

}
*/


/* If all okay then import to temp table to facilitate quick accept import step 4 */
if($status_count['rows']['error']==0) {
	foreach($import_data as $key => $row) {
		$row['line_version'] = ASSIST_HELPER::JOIN_FOR_MULTI_LISTS.$line_version.ASSIST_HELPER::JOIN_FOR_MULTI_LISTS;
		$sql = "INSERT INTO ".$segmentObject->getDBRef()."_segment_import SET ".$segmentObject->convertArrayToSQLForSave($row);
		$segmentObject->db_insert($sql);
	}
}


//ASSIST_HELPER::arrPrint($status_count);


/*
 * DATA VALIDATION during loop (DEVELOPMENT)

echo "<hr />";
 echo "end"
echo "<hr />";
ASSIST_HELPER::arrPrint($processed_data);
echo "<hr />";

ASSIST_HELPER::arrPrint($parent_by_guid);
echo "<hr />";
 */
?>
<table id=tbl_import style=''>
	<tr>
		<th>&nbsp;</th>
		<th>Row</th>
		<th>Parent GUID</th>
		<th>Parent Long Name</th>
		<th>Line Item GUID</th>
		<th>Line Item Name</th>
		<th>Line Item Description</th>
		<th>Line Item Nickname/Alias</th>
		<th>Has Children?</th>
		<th>Can be assigned to KPIs/Projects?</th>
		<th>Import Status</th>
	</tr>
	<?php
foreach($processed_data as $key => $row) {
	$final_icon = $row['import_status']['final_icon'];
	unset($row['import_status']['final_icon']);
		echo "
		<tr>
			<td><div class='div_message final_".$final_icon."'>".ASSIST_HELPER::getDisplayIconAsDiv($final_icon)."</div></td>
			<td class=center>".$row['row']."</td>";
		$fld = "parent_guid";
		echo "
			<td><div class='div_message ".(isset($errors[$key][$fld])?"".$errors[$key][$fld]:"")."'>".$row[$fld]."</div></td>
			<td>".$row['parent_name']."</td>
			";
		$fld = "guid";
		echo "
			<td><div class='div_message ".(isset($errors[$key][$fld])?"".$errors[$key][$fld]:"")."'>".$row[$fld]."</div></td>
			";
		$fld = "name";
		echo "
			<td><div class='div_message ".(isset($errors[$key][$fld])?"".$errors[$key][$fld]:"")."'>".$row[$fld]."</div></td>
			<td>".$row['description']."</td>
			<td>".$row['nickname']."</td>
			";
		$fld = "has_subs";
		echo "
			<td><div class='div_message ".(isset($errors[$key][$fld])?"".$errors[$key][$fld]:"")."'>".$row[$fld]."</div></td>
			";
		$fld = "can_assign";
		echo "
			<td><div class='div_message ".(isset($errors[$key][$fld])?"".$errors[$key][$fld]:"")."'>".$row[$fld]."</div></td>
			";
		echo "
			<td><div class='div_message final_".$final_icon."'>".implode("<br />",$row['import_status'])."</div></td>
		</tr>
		";
}
	?>
</table>
<div id=div_result style='width:500px;margin:10px auto;padding:10px;'>
	<p class='center b'>Import Result</p>
	<p>
		<?php
		if($status_count['rows']['error']>0) {
			echo "<p>There are ".$status_count['rows']['error']." rows in an error state".($status_count['rows']['warn']>0?" and ".$status_count['rows']['warn']." rows with warnings":"").".  The Import cannot be finalised.  Please correct the errors and try again.</p>
			<p class='b center'>WARNING: NO LINE ITEMS HAVE BEEN IMPORTED.";
		} else {
			if($status_count['rows']['warn']>0) {
				echo "<p>There are ".$status_count['rows']['warn']." rows with warnings.  The Import can be finalised however the warnings should be reviewed.</p>";
			} else {
				echo "<p class='center'>All ".$status_count['rows']['ok']." rows are ok to Import.</p>";
			}
			echo "
			<form name=frm_save method=post action=manage_edit_segments_import.php>
			<input type=hidden name=section value='$section' />
			<input type=hidden name=action value='STEP4' />
			<input type=hidden name=import_ref value='$import_ref' />
			<p class=center>
			<button id=btn_save>Accept Import</button>
			</p>
			</form>
			";
		}
		?>
	</p>

</div>
<script type="text/javascript">
	$(function() {
		<?php
		if($status_count['rows']['error']>0) {
			echo "$(\"#div_result\").addClass(\"ui-state-error\");";
		} elseif($status_count['rows']['warn']>0) {
			echo "$(\"#div_result\").addClass(\"ui-state-info\");";
		} else {
			echo "$(\"#div_result\").addClass(\"ui-state-ok\").css(\"background\",\"#ffffff\");";
		}
		?>

		$("#tbl_import").css({"height":"100%"});
		$("#tbl_import td").css({"padding":"0px"});
		$("#tbl_import div.div_message").css({"margin":"1px","padding":"2px","height":"87%"});
		$("#tbl_import div.error").addClass("ui-state-error");
		$("#tbl_import div.warn").addClass("ui-state-info");
		$("#tbl_import div.final_ok").addClass("ui-state-ok");
		$("#tbl_import div.final_warn").addClass("ui-state-info");
		$("#tbl_import div.final_error").addClass("ui-state-error");
		
		$("#btn_save").button({
			icons:{primary:"ui-icon-check"}
		}).removeClass("ui-state-default").addClass("ui-button-bold-green")
		.css("margin","0 auto")
		.click(function(e) {
			e.preventDefault();
			$("form[name=frm_save]").submit();
		});
	});
</script>
<?php





//$all_vars = get_defined_vars();
//unset($all_vars['_SESSION']);
//unset($all_vars['_SERVER']);
//ASSIST_HELPER::arrPrint($all_vars);
?>