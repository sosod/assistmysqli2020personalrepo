<?php
// IF PAGE IS ON LIVE CLIENT SERVER THEN INCLUDE DATADOG
$client_sites = array("assist.action4u.co.za", "ignite.assist.action4u.co.za", "pwc.assist.action4u.co.za");
$my_site = $_SERVER['HTTP_HOST'];
if (in_array($my_site, $client_sites) === true) {

	# Require the datadogstatsd.php library file
	require 'library/datadogstatsd.php';

	$datadog_apis = array(
		'assist.action4u.co.za' => "1e73d8cc8246676e371124aec41801e3",
		'ignite.assist.action4u.co.za' => "1e73d8cc8246676e371124aec41801e3",
		'pwc.assist.action4u.co.za' => "b9508331c2d67526d6b83fa674f934c2",
	);

	//connect
	$apiKey = $datadog_apis[$my_site];
	$appKey = "f4cc83d5e3eb5cf3ad958253f51d96838764a0a0";
	DataDogStatsD::configure($apiKey, $appKey);

	//set name of stat to send to DataDog
	//adjust name depending on client site
	$x = explode(".", $my_site);
	if ($x[0] != "assist") {
		$prefix = $x[0] . "-";
	} else {
		$prefix = "";
	}
	$stat_name = $prefix . "assist.index.visits";
	DataDogStatsD::decrement($stat_name);    //decrease the number of users currently on the system
	$stat_name = $prefix . "assist.logout.visits";
	DataDogStatsD::decrement($stat_name);    //decrease the number of users currently on the system

}

$cmpcode = strtolower($_SESSION['cc']);
$tkid = $_SESSION['tid'];
$tkn = $_SESSION['original_tkn'];
//print_r($_SESSION);
$site_code = $_SESSION['DISPLAY_INFO']['site_code'];

if (isset($_REQUEST['action'])) {
	$action = "OUT_" . $_REQUEST['action'];
} elseif (isset($_REQUEST['r'])) {
	$action = "OUT_" . base64_decode($_REQUEST['r']);
} else {
	$action = "OUT";
}

if (strlen($cmpcode) == 7) {
//	include 'inc_db.php';
//	include 'inc_db_conn.php';
	if (isset($_COOKIE['PHPSESSID']) && strlen($_COOKIE['PHPSESSID'])) {
		$unique_id = "PHPSESSID=" . $_COOKIE['PHPSESSID'];
	} elseif (isset($_SERVER["HTTP_COOKIE"]) && strlen($_SERVER["HTTP_COOKIE"])) {
		$unique_id = $_SERVER["HTTP_COOKIE"];
	} else {
		$unique_id = "";
	}
	include("module/autoloader.php");
	$me = new ASSIST_DB(); //Don't use ASSIST_MODULE_HELPER - gets into infinite loop with Assist->autoLogout function
	$me->db_insert("INSERT INTO assist_" . $cmpcode . "_timekeep_activity (id, date, tkid, tkname, action, active,sessid,remote_addr) VALUES (null,now(),'$tkid','$tkn','$action',1,'" . $unique_id . "|" . $_SERVER["HTTP_USER_AGENT"] . "','" . $_SERVER['REMOTE_ADDR'] . "')");
}
//echo "<pre>"; print_r($_REQUEST); print_r($_SESSION); echo "</pre>";
$_SESSION = array();
session_destroy();
?>
<script language=JavaScript>
	//alert("Fatal error.  Logging out.");
	parent.location.href = "<?php echo isset($_SERVER['HTTPS']) ? "https" : "http"; ?>://<?php echo $_SERVER["SERVER_NAME"] . "/index.php?" . $site_code; ?>";
</script>
<html>
<body>
<?php
echo "<p>Logging out and redirecting to: https://" . $_SERVER["SERVER_NAME"] . "/index.php?" . $site_code . ".</p>";
//echo("<P>IP - ".$ip."</p>");
//echo("<P>IL - ".$il."</p>");
?>
</body>
</html>
