<?php include("inc_ignite.php");


$msort = $_POST['msort'];
$mfilter = $_POST['mfilter']; //filter text
$mtfilter = $_POST['mtfilter']; //filter type
$gfilter = $_POST['gfilter']; //filter text
$gtfilter = $_POST['gtfilter']; //filter type
$gsort = $_POST['gsort']; //group sort
 ?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>

<link rel="stylesheet" href="/assist.css" type="text/css">
<link rel="stylesheet" href="/lib/default_blue.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Payroll Assist - My Documents</b></h1>
<h2 class=fc style="margin-top: 20px">My Documents</h2>
<p style="margin-top: -10px;"><small><i>These are your payroll documents.</i></small></p>
<form method=post action=doc.php>
<table cellpadding=3 cellspacing=0 class=noborder style="margin: -10px 0px 10px 3px">
<tr class=tdgeneral>
<td valign=top class=noborder><b>Filter:</b>&nbsp;</td>
<td class=noborder>Search for:<br><input type=text size=15 name=mfilter value="<?php echo($mfilter); ?>"></td>
<td class=noborder>Search in:<br><select name=mtfilter><option <?php if($mtfilter == "all" || strlen($mtfilter)==0) { echo("selected"); } ?> value=all></option><option <?php if($mtfilter == "T") { echo("selected"); } ?> value=T>Title</option><option <?php if($mtfilter == "D") { echo("selected"); } ?> value=D>Doc. Type</option></select></td>
<td class=noborder>Sort by:<br><select name=msort><option <?php if($msort == "all" || strlen($msort)==0) { echo("selected"); } ?> value=all></option><option <?php if($msort == "T") { echo("selected"); } ?> value=T>Title</option><option <?php if($msort == "D") { echo("selected"); } ?> value=D>Doc. Type</option><option <?php if($msort == "A") { echo("selected"); } ?> value=A>Date</option></select></td>
<td class="noborder bottom"><input type=submit value=" Go " style="margin-bottom: 2px;"></td>
</tr></table>

<table border=1 cellpadding=3 cellspacing=0 width=750 style="border-collapse: collapse; border-style: solid">
    <tr  style="line-height: 14pt">
        <th width=190>Document Type</th>
        <th >Title</th>
        <th width=140>Date</th>
    </tr>
    <?php
    $sql = "SELECT * FROM assist_".$cmpcode."_pay_documents d, assist_".$cmpcode."_pay_list_doctype t ";
    $sql.= "WHERE d.typeid = t.id AND d.yn = 'Y' AND filename <> '' AND tkid = '".$tkid."' AND filetype = 'I'";
    if(strlen($mfilter)>0)
    {
        $mfilter = str_replace("'","&#39",$mfilter);
        switch($mtfilter)
        {
            case "D":
                $sql.= " AND t.value LIKE '%".$mfilter."%' ";
                break;
            case "T":
                $sql.= " AND d.title LIKE '%".$mfilter."%' ";
                break;
            default:
                $sql.= " AND (t.value LIKE '%".$mfilter."%' OR d.title LIKE '%".$mfilter."%')";
                break;
        }
    }
    switch($msort)
    {
        case "A":
            $sql.= " ORDER BY d.date1, d.date2, d.title, t.value";
            break;
        case "D":
            $sql.= " ORDER BY d.title, d.date1, d.date2, t.value";
            break;
        case "T":
            $sql.= " ORDER BY t.value, d.title, d.date1, d.date2";
            break;
        default:
            $sql.= " ORDER BY t.value, d.title, d.date1, d.date2";
            break;
    }
    include("inc_db_con.php");
    if(mysql_num_rows($rs) > 0)
    {
        while($row = mysql_fetch_array($rs))
        {
            $filename = "../files/".$cmpcode."/".$row['filename'];
            if(file_exists($filename))
            {
    ?>
    <?php include("inc_tr.php"); ?>
        <td class=tdgeneral>&nbsp;<?php echo($row['value']); ?></td>
        <td class=tdgeneral>&nbsp;<a href="<?php echo($filename); ?>" target=_blank><?php echo($row['title']); ?></a></td>
        <td class=tdgeneral style="line-height: 10pt"><small><?php
            $datetype = $row['datetype'];
            switch($datetype)
            {
                case "D":
                    echo(date("d F Y",$row['date1']));
                    break;
                case "M":
                    echo(date("F Y",$row['date1']));
                    break;
                case "P":
                    echo(date("d F Y",$row['date1'])." -<br>".date("d F Y",$row['date2']));
                    break;
                default:
                    break;
            }
            ?>&nbsp;</small></td>
    </tr>
    <?php
            }
        }
    }
    else
    {
        ?><tr class=tdgeneral><td colspan=3>No documents to display.</td></tr><?php
    }
    mysql_close();
    ?>
</table>
    <?php
    $sql = "SELECT * FROM assist_".$cmpcode."_pay_documents d, assist_".$cmpcode."_pay_list_doctype t ";
    $sql.= "WHERE d.typeid = t.id AND d.yn = 'Y' AND filename <> '' AND tkid = '".$tkid."' AND filetype = 'G'";
    if(strlen($gfilter)>0)
    {
        $gfilter = str_replace("'","&#39",$gfilter);
        switch($gtfilter)
        {
            case "D":
                $sql.= " AND t.value LIKE '%".$gfilter."%' ";
                break;
            case "T":
                $sql.= " AND d.title LIKE '%".$gfilter."%' ";
                break;
            default:
                $sql.= " AND (t.value LIKE '%".$gfilter."%' OR d.title LIKE '%".$gfilter."%')";
                break;
        }
    }
    switch($gsort)
    {
        case "A":
            $sql.= " ORDER BY d.date1, d.date2, d.title, t.value";
            break;
        case "D":
            $sql.= " ORDER BY d.title, d.date1, d.date2, t.value";
            break;
        case "T":
            $sql.= " ORDER BY t.value, d.title, d.date1, d.date2";
            break;
        default:
            $sql.= " ORDER BY t.value, d.title, d.date1, d.date2";
            break;
    }
    include("inc_db_con.php");
?>
<h2 class=fc style="margin-top: 40px">Group Documents</h2>
<p style="margin-top: -10px;"><small><i>These are payroll documents that you need to distribute to your employees/staff members.</i></small></p>

<table cellpadding=3 cellspacing=0  class=noborder style="margin: -10px 0px 10px 3px">
<tr class=tdgeneral>
<td class=noborder valign=top><b>Filter:</b>&nbsp;</td>
<td class=noborder>Search for:<br><input type=text size=15 name=gfilter value="<?php echo($gfilter); ?>"></td>
<td class=noborder>Search in:<br><select name=gtfilter><option <?php if($gtfilter == "all" || strlen($gtfilter)==0) { echo("selected"); } ?> value=all></option><option <?php if($gtfilter == "T") { echo("selected"); } ?> value=T>Title</option><option <?php if($gtfilter == "D") { echo("selected"); } ?> value=D>Doc. Type</option></select></td>
<td class=noborder>Sort by:<br><select name=gsort><option <?php if($gsort == "all" || strlen($gsort)==0) { echo("selected"); } ?> value=all></option><option <?php if($gsort == "T") { echo("selected"); } ?> value=T>Title</option><option <?php if($gsort == "D") { echo("selected"); } ?> value=D>Doc. Type</option><option <?php if($gsort == "A") { echo("selected"); } ?> value=A>Date</option></select></td>
<td class="noborder bottom"><input type=submit value=" Go " class=button style="margin-bottom: 2px;"></td>
</tr></table>

<table border=1 cellpadding=3 cellspacing=0 width=750 style="border-collapse: collapse; border-style: solid">
    <tr  style="line-height: 14pt">
        <th width=190>Document Type</th>
        <th >Title</th>
        <th width=140>Date</th>
    </tr>

<?php
    if(mysql_num_rows($rs)==0)
    {
        if(strlen($gfilter)>0)
        {
?>
    <?php include("inc_tr.php"); ?>
        <td colspan=3 class=tdgeneral>No documents found.</td>
    </tr>
<?php
        }
    }
    else
    {
?>
<!-- <h2 class=fc style="margin-top: 40px">Group Documents</h2>
<p style="margin-top: -10px;"><small><i>These are payroll documents that you need to distribute to your employees/staff members.</i></small></p>

<table cellpadding=3 cellspacing=0 border=0 style="margin: -10px 0px 10px 3px">
<tr class=tdgeneral>
<td valign=top><b>Filter:</b>&nbsp;</td>
<td>Search for:<br><input type=text size=15 name=gfilter value="<?php echo($gfilter); ?>"></td>
<td>Search in:<br><select name=gtfilter><option <?php if($gtfilter == "all" || strlen($gtfilter)==0) { echo("selected"); } ?> value=all></option><option <?php if($gtfilter == "T") { echo("selected"); } ?> value=T>Title</option><option <?php if($gtfilter == "D") { echo("selected"); } ?> value=D>Doc. Type</option></select></td>
<td>Sort by:<br><select name=gsort><option <?php if($gsort == "all" || strlen($gsort)==0) { echo("selected"); } ?> value=all></option><option <?php if($gsort == "T") { echo("selected"); } ?> value=T>Title</option><option <?php if($gsort == "D") { echo("selected"); } ?> value=D>Doc. Type</option><option <?php if($gsort == "A") { echo("selected"); } ?> value=A>Date</option></select></td>
<td valign=bottom><input type=submit value=Go class=button style="padding: 2px 3px 2px 4px; border: solid 1px;"></td>
</tr></table>

<table border=1 cellpadding=3 cellspacing=0 width=90% style="border-collapse: collapse; border-style: solid">
    <tr class=tdheader style="line-height: 14pt">
        <td width=20%>Document Type</td>
        <td width=60%>Title</td>
        <td width=20%>Date</td>
    </tr> -->
    <?php
        while($row = mysql_fetch_array($rs))
        {
            $filename = "../files/".$cmpcode."/".$row['filename'];
            if(file_exists($filename))
            {
    ?>
    <?php include("inc_tr.php"); ?>
        <td class=tdgeneral>&nbsp;<?php echo($row['value']); ?></td>
        <td class=tdgeneral>&nbsp;<a href="<?php echo($filename); ?>" target=_blank><?php echo($row['title']); ?></a></td>
        <td class=tdgeneral><small><?php
            $datetype = $row['datetype'];
            switch($datetype)
            {
                case "D":
                    echo(date("d F Y",$row['date1']));
                    break;
                case "M":
                    echo(date("F Y",$row['date1']));
                    break;
                case "P":
                    echo(date("d F Y",$row['date1'])." -<br>".date("d F Y",$row['date2']));
                    break;
                default:
                    break;
            }
            ?>&nbsp;</small></td>
    </tr>
    <?php
            }
        }
    ?>
</table>
<?php
    }
    mysql_close();
?>
</form>
</body>

</html>
