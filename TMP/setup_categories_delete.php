<?php
    include("inc_ignite.php");

    $cateid = $_GET['c'];
    if(strlen($cateid) > 0)
    {
        //SET CATEGORY YN = N
        $sql = "UPDATE assist_".$cmpcode."_tmp_categories SET cateyn = 'N' WHERE cateid = ".$cateid;
        include("inc_db_con.php");
            //ADD TRANSACTION LOG
            $tsql = $sql;
            $tref = "TMP";
            $trans = "Deleted templates category ".$cateid;
            include("inc_transaction_log.php");

    //DELETE USER ACCESS
        $sql = "UPDATE assist_".$cmpcode."_tmp_list_users SET yn = 'N' WHERE cateid = ".$cateid;
        include("inc_db_con.php");
            $tsql = $sql;
            $tref = "TMP";
            $trans = "Delete user access to deleted category ".$cateid;
            include("inc_transaction_log.php");

    //DELETE TEMPLATES
        //GET TEMPLATES ASSOCIATED WITH CATE
        $sql = "SELECT * FROM assist_".$cmpcode."_tmp_content WHERE tmpcateid = ".$cateid;
        include("inc_db_con.php");
            $r = mysql_num_rows($rs);
            if($r > 0)
            {
                $m = 0;
                while($row = mysql_fetch_array($rs))
                {
                    $minrow[$m] = $row;
                }
            }
        mysql_close();

        if($r > 0)  //IF TEMPLATES RECORDS WERE FOUND
        {
            foreach($minrow as $row)
            {
                //SET TMP DETAILS
                $minid = $row['tmpid'];
                $minfileold = $row['tmpfilename'];
                $minfilenew = "deleted_".date("Ymd")."_".$minfileold;
                $minold = "../files/".$cmpcode."/".$minfileold;
                $minnew = "../files/".$cmpcode."/".$minfilenew;
                //UPDATE TEMPLATES RECORD SET YN = N
                $sql = "UPDATE assist_".$cmpcode."_tmp_content SET tmpyn = 'N' WHERE tmpid = ".$minid;
                include("inc_db_con.php");
                //RENAME FILE
                rename($minold,$minnew);
                    //CREATE TRANSACTION LOG
                    $tsql = $sql;
                    $tref = "TMP";
                    $trans = "Due to deletion of category ".$cateid." deleted template ".$minid." and renamed the file from ".$minfileold." to ".$minfilenew;
                    include("inc_transaction_log.php");
            }
        }
    }
    echo("<form name=dc><input type=hidden name=r value=Y></form>");
?>
<script language=JavaScript>
var dc = document.dc.r.value;
if(dc == "Y")
    document.location.href = "setup_categories.php";
</script>

