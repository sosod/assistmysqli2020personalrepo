<?php
    include("inc_ignite.php");

$catetitle = $_POST["catetitle"];
$catematkid = $_POST["catematkid"];

if(strlen($catetitle) > 0)  //IF DATA HAS BEEN SUBMITTED VIA THE FORM
{
    $catetitle = str_replace("'","&#39",$catetitle);
    //CREATE NEW CATEGORY RECORD
    $sql = "INSERT INTO assist_".$cmpcode."_tmp_categories SET ";
    $sql.= "catetitle = '".$catetitle."', ";
    $sql.= "catematkid = '".$catematkid."', ";
    $sql.= "cateyn = 'Y'";
    include("inc_db_con.php");
        //LOG THE NEW CATEGORY
        $tsql = $sql;
        $tref = "TMP";
        $trans = "Added new templates category: ".$catetitle;
        include("inc_transaction_log.php");
    //GET CATEID FOR NEW CATEGORY
    $sql = "SELECT * FROM assist_".$cmpcode."_tmp_categories WHERE catetitle = '".$catetitle."' AND catematkid = '".$catematkid."' AND cateyn = 'Y'";
    include("inc_db_con.php");
        $row = mysql_fetch_array($rs);
    mysql_close();
    $cateid = $row['cateid'];
    //GET LIST OF USERS TO TEMPLATES
    $sql = "SELECT usrtkid FROM assist_".$cmpcode."_menu_modules_users WHERE usrmodref = 'TMP'";
    include("inc_db_con.php");
        while($row = mysql_fetch_array($rs))
        {
            $carr .= "|".$row['usrtkid'];
        }
    mysql_close();
    $catetkid = explode("|",$carr);
    //CREATE VIEW ACCESS TO CATEGORY FOR ALL USERS
    foreach($catetkid as $ctkid)
    {
        if(strlen($ctkid) > 0)
        {
            $sql = "INSERT INTO assist_".$cmpcode."_tmp_list_users SET tkid = '".$ctkid."', cateid = ".$cateid.", yn = 'Y'";
            include("inc_db_con.php");
                //LOG THE VIEW ACCESS GRANTED TO THE NEW CATEGORY
                $tsql = $sql;
                $tref = "TMP";
                $trans = "Added view access for user ".$catematkid." to templates category ".$cateid;
                include("inc_transaction_log.php");
        }
    }
}

    include("inc_tmpadmin.php");
    
    
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function delCate(c) {
    if(confirm("Are you sure you want to remove category "+c+"?\n\nNote that all templates associated with this category will be deleted.")==true)
    {
        //alert("delete");
        document.location.href = "setup_categories_delete.php?c="+c;
    }
    else
    {
        //alert("abort");
    }
}

function editCate(c) {
    if(confirm("Are you sure you want to edit category "+c+"?\n\nNote that all templates associated with this category will be affected.")==true)
    {
        //alert("edit");
        document.location.href = "setup_categories_edit.php?c="+c;
    }
    else
    {
        //alert("abort");
    }
}

function Validate(me) {

    var ct = me.catetitle.value;
    var ma = me.catematkid.value;
    
    if(ct.length == 0)
    {
        alert("Please enter a category title.");
    }
    else
    {
        if(ma == "X")
        {
            alert("Please select the administrator for this new category.");
        }
        else
        {
            return true;
        }
    }
    return false;
}
</script>

<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Templates Assist: Setup - Categories</b></h1>
<form name=addcate action=setup_categories.php method=post onsubmit="return Validate(this);">
<table border=1 cellpadding=3 cellspacing=0>
    <tr>
        <td class=tdheader>Ref</td>
        <td class=tdheader>Category</td>
        <td class=tdheader>Admin User</td>
        <td class=tdheader>&nbsp;</td>
    </tr>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_tmp_categories WHERE cateyn = 'Y' ORDER BY catetitle";
include("inc_db_con.php");
$r = mysql_num_rows($rs);
if($r > 0)
{
    $ccount = 0;
    while($row = mysql_fetch_array($rs))
    {
        $caterow[$ccount] = $row;
        $ccount++;
    }
}
mysql_close();

$ccount = 0;
if($r > 0)
{
    foreach($caterow as $cate)
    {
?>
    <tr>
        <td class=tdgeneral valign=top align=center><?php echo($cate['cateid']); ?></td>
        <td class=tdgeneral><?php echo($cate['catetitle']); ?></td>
        <td class=tdgeneral><?php
                $sql = "SELECT * FROM assist_".$cmpcode."_timekeep WHERE tkid = '".$cate['catematkid']."'";
                include("inc_db_con.php");
                    $row = mysql_fetch_array($rs);
                    echo($row['tkname']." ".$row['tksurname']);
                mysql_close();
            ?></td>
        <td class=tdgeneral><?php echo("<input type=button value=Edit onclick=\"editCate(".$cate['cateid'].")\"> <input type=button value=Del onclick=\"delCate(".$cate['cateid'].")\">");?></td>
    </tr>
<?php
    }
}
?>
    <tr>
        <td class=tdgeneral valign=top align=center>&nbsp;</td>
        <td class=tdgeneral><input type=text name=catetitle size=30></td>
        <td class=tdgeneral><select name=catematkid><option selected value=X>--- SELECT ---</option>
            <?php
                //echo("<option value=0000>Ignite Assist Administrator</option>");
                $sql = "SELECT * FROM assist_".$cmpcode."_timekeep t, assist_".$cmpcode."_menu_modules_users u WHERE t.tkstatus = 1 AND t.tkid <> '0000' AND u.usrtkid = t.tkid AND u.usrmodref = 'TMP' ORDER BY t.tkname, t.tksurname";
                include("inc_db_con.php");
                while($row = mysql_fetch_array($rs))
                {
                    $id = $row['tkid'];
                    $tk = $row['tkname']." ".$row['tksurname'];
                    echo("<option value=".$id.">".$tk."</option>");
                }
                mysql_close();
            ?>
        </select></td>
        <td class=tdgeneral><input type=submit value=Add></td>
    </tr>
</table>
</form>

<?php
$helpfile = "../help/TMP_help_setupadmin.pdf";
if(file_exists($helpfile))
{
    echo("<p>&nbsp;</p><ul><li><a href=".$helpfile."><u>Help</u></a></li></ul>");
}
?>
</body>

</html>
