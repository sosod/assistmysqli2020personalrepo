<?php
include("inc_ignite.php");
include("inc_errorlog.php");

$cateid = $_GET['c'];

$sql = "SELECT * FROM assist_".$cmpcode."_tmp_categories WHERE cateid = ".$cateid;
include("inc_db_con.php");
$cate = mysql_fetch_array($rs);
mysql_close();

?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-za">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>www.Ignite4u.co.za</title>
</head>
<script language=JavaScript>
function Validate(me) {
    var tmptitle = me.tmptitle.value;
    var minfile = me.minfile.value;
    var mday = me.mday.value;
    var mmon = me.mmon.value;
    var myear = me.myear.value;
    var mintoc = me.mintoc.value;
    var valid8 = "false";
    
    if(tmptitle.length == 0)
    {
        alert("Please enter a title for this template.");
    }
    else
    {
        if(mday.length == 0 || mmon.length == 0 || myear.length == 0)
        {
            alert("Please indicate the effective date of the template.");
        }
        else
        {
            if(minfile.length == 0)
            {
                alert("Please select a file for upload.");
            }
            else
            {
            //var ftype = minfile.substr(minfile.lastIndexOf(".")+1,3);
            //if(ftype.toLowerCase() != "pdf")
            //{
            //    alert("Invalid file.  Only PDF documents may be uploaded.");
            //}
            //else
            //{
                valid8 = "true";
            //}
            }
        }
    }
    if(valid8 == "true")
    {
        if(mintoc.length == 0)
        {
            if(confirm("You have not entered the purpose of this template.\n\nAre you sure you wish to continue?") == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
    return false;
}

function showMin(mfn) {
    var url = "http://assist.ignite4u.co.za"+mfn;
    //alert(url);
    newwin = window.open(url,"","dependent=0 ,toolbar=1,location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=700,height=500")
}

function delMin(m) {
    if(confirm("Are you sure you want to delete template "+m+"?")==true)
    {
        //alert("delete");
        document.location.href = "update_delete.php?m="+m;
    }
    else
    {
        //alert("aborting");
    }
}

</script>
<link rel="stylesheet" href="/default.css" type="text/css">
<?php include("inc_style.php"); ?>
<base target="main">
<body topmargin=0 leftmargin=5 bottommargin=0 rightmargin=5>
<h1 class=fc><b>Templates Assist: Update</h1>
<h2 class=fc><?php echo($cate['catetitle']); ?></b></h2>
<form name=update action="update_process.php" method="post" enctype="multipart/form-data" lang=jscript onsubmit="return Validate(this);">
<table border=1 cellpadding=3 cellspacing=0>
    <tr>
        <td class=tdgeneral><b>Category:</b>&nbsp;</td>
        <td class=tdgeneral><?php echo($cate['catetitle']); ?><input type=hidden name=mincateid value=<?php echo($cate['cateid']);?>></td>
    </tr>
    <tr>
        <td class=tdgeneral><b>Title:</b>&nbsp;</td>
        <td class=tdgeneral><input type=text name=tmptitle size=35 maxlength=50></td>
    </tr>
    <tr>
        <td class=tdgeneral><b>Effective date:</b></td>
        <td class=tdgeneral><input type=text name=mday size=3 value=<?php echo(date("d")); ?>><select name=mmon><?php
		$mmon = date("n");
		for($m=1;$m<13;$m++)
		{
            $mdate = getdate(mktime(0,0,0,$m,1,0));
            $mmon2 = date("n",$mdate[0]);
            $msel = date("M",$mdate[0]);
            if($mmon == $mmon2)
            {
                echo("<option selected value=".$m.">".$msel."</option>");
            }
            else
            {
                echo("<option value=".$m.">".$msel."</option>");
            }
		}
            ?></select><input type=text size=5 name=myear value=<?php echo(date("Y")); ?>></td>
    </tr>
    <tr>
        <td class=tdgeneral valign=top><b>Purpose:</b>&nbsp;</td>
        <td class=tdgeneral><textarea name=mintoc rows=3 cols=30></textarea></td>
    </tr>
    <tr>
        <td class=tdgeneral><b>File:</b></td>
        <td class=tdgeneral><input type=file name=minfile></td>
    </tr>
    <tr>
        <td class=tdgeneral colspan=2><input type=submit value=Update> <input type=reset></td>
    </tr>
</table>
</form>
<?php
$sql = "SELECT * FROM assist_".$cmpcode."_tmp_content m WHERE tmpcateid = ".$cateid." AND tmpyn = 'Y' ORDER BY tmptitle ASC";
include("inc_db_con.php");
$m = mysql_num_rows($rs);
if($m > 0)
{
    echo("<hr class=fc>");
    echo("<table border=1 cellpadding=3 cellspacing=0>");
    echo("    <tr>");
    echo("        <td class=tdheader>Ref</td>");
    echo("        <td class=tdheader>Title</td>");
    echo("        <td class=tdheader>Date</td>");
    echo("        <td class=tdheader>Purpose</td>");
    echo("        <td class=tdheader>&nbsp;</td>");
    echo("    </tr>");
    while($row = mysql_fetch_array($rs))
    {
        $floc = "/files/".$cmpcode."/".$row['tmpfilename'];
        $floc2 = "../files/".$cmpcode."/".$row['tmpfilename'];
        if(file_exists($floc2))
        {
            echo("    <tr>");
            echo("        <td class=tdheader align=center valign=top>".$row['tmpid']."</td>");
            echo("        <td class=tdgeneral valign=top><a href=".$floc2." target=_blank>".$row['tmptitle']."</a></td>");
            echo("        <td class=tdgeneral valign=top>".date("d F Y",$row['tmpdate'])."</td>");
            echo("        <td class=tdgeneral valign=top>");
            if(strlen($row['tmppurpose']) > 0)
            {
                $mintoc = str_replace(chr(10),"<br>",$row['tmppurpose']);
                echo($mintoc);
            }
            else
            {
                echo("&nbsp;");
            }
            echo("</td>");
            echo("        <td class=tdgeneral valign=top><input type=button value=Delete onclick=\"delMin('".$row['tmpid']."')\"></td>");
            echo("    </tr>");
        }
    }
    echo("</table>");
}
mysql_close();
?>

</body>

</html>
