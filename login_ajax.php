<?php

require_once("module/autoloader.php");

if(isset($_REQUEST['second_validation']) && $_REQUEST['second_validation']*1==1 ) {
	$me = new ASSIST_LOGIN2($_REQUEST,true);
	$e = "ASSIST_LOGIN2";
	$stat_name = "assist.login.second";
/*
# Increment a counter.
$apiKey = "1e73d8cc8246676e371124aec41801e3";
$appKey = "f4cc83d5e3eb5cf3ad958253f51d96838764a0a0";
DataDogStatsD::configure($apiKey, $appKey);
DataDogStatsD::increment('ignite-assist.login.second');*/

} else {
	$me = new ASSIST_LOGIN($_REQUEST,true);
	$e = "ASSIST_LOGIN";
	$stat_name = "assist.login.first";
/*
# Increment a counter.
$apiKey = "1e73d8cc8246676e371124aec41801e3";
$appKey = "f4cc83d5e3eb5cf3ad958253f51d96838764a0a0";
DataDogStatsD::configure($apiKey, $appKey);
DataDogStatsD::increment('ignite-assist.login.first');*/


}
// IF PAGE IS ON LIVE CLIENT SERVER THEN INCLUDE DATADOG
//$client_sites = array("assist.action4u.co.za", "ignite.assist.action4u.co.za", "pwc.assist.action4u.co.za");


$datadog_apis = array(
	'assist.action4u.co.za' => "1e73d8cc8246676e371124aec41801e3",
	'ignite.assist.action4u.co.za' => "1e73d8cc8246676e371124aec41801e3",
	'pwc.assist.action4u.co.za' => "b9508331c2d67526d6b83fa674f934c2",
);
$my_site = $_SERVER['HTTP_HOST'];
if (isset($datadog_apis[$my_site])) {

	# Require the datadogstatsd.php library file
	require 'library/datadogstatsd.php';

	//connect
	$apiKey = $datadog_apis[$my_site];
	$appKey = "f4cc83d5e3eb5cf3ad958253f51d96838764a0a0";
	DataDogStatsD::configure($apiKey, $appKey);

	//set name of stat to send to DataDog
	//adjust name depending on client site
	$x = explode(".", $my_site);
	if ($x[0] != "assist") {
		$prefix = $x[0] . "-";
	} else {
		$prefix = "";
	}
	$stat_name = $prefix . $stat_name;
	DataDogStatsD::increment($stat_name);    //count valid logins

}






$data = $me->getValidationResult();

$data[] = $e;




echo json_encode($data);
?>